/*********************************************************************//*
                  FILE HEADER                                      
*//*********************************************************************//*
                                                                   
  FileName    :  EmployeeLoginRepository.java                                		 
                                                                   
  Author      :  THBS	MIS	                                         
                                                                   
  Date        :  25-Jun-2016                                           
                                                                   
  Description :  TODO			 									 
                 			                                         
                                                                   
                                                                   
*//*********************************************************************//*
 Date        Who     Version      Comments             			 
-------------------------------------------------------------------
 25-Jun-2016    THBS     1.0         Initial version created   		 
*//*********************************************************************//*
package com.thbs.mis.common.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.EmployeeLoginBO;

*//**
 * <Description EmployeeLoginRepository:> TODO
 * @author THBS
 * @version 1.0
 * @see 
 *//*
@Repository
public interface EmployeeLoginRepository extends GenericRepository<EmployeeLoginBO,Integer>
{
	
}
*/