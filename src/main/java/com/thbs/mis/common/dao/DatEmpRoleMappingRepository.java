package com.thbs.mis.common.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.DatEmpRoleMappingBO;

@Repository
public interface DatEmpRoleMappingRepository extends GenericRepository<DatEmpRoleMappingBO, Integer> {

	public List<DatEmpRoleMappingBO> findByFkEmployeeId(Integer employeeId);

	@Query("SELECT rep FROM DatEmpRoleMappingBO rep " + " WHERE rep.fkEmployeeId =:fkEmployeeId ")
	public DatEmpRoleMappingBO detailsFkEmployeeId(@Param("fkEmployeeId") Integer fkEmployeeId);
}
