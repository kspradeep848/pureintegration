package com.thbs.mis.common.dao;

import com.thbs.mis.common.bo.MasEmpSubStatusBO;

public interface MasEmpSubStatusRepository extends
		GenericRepository<MasEmpSubStatusBO, Long> {

	// Added by Kamal Anand for User Management
	public MasEmpSubStatusBO findByPkEmpSubStatusId(byte subStatusId);

	public MasEmpSubStatusBO findByPkEmpSubStatusIdAndFkEmpMainStatus(
			byte subStatusId, byte mainStatusId);
	// End of Addition by Kamal Anand for User Management

}
