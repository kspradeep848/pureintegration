/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  MisMenuRepository.java                            */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  15-Nov-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 28-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/ 
package com.thbs.mis.common.dao;



import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.MasMenuBO;

@Repository
public interface MasMenuRepository extends GenericRepository<MasMenuBO, Long>{
	
	
	/**
	 * <Description getAllMenuListForRoleId:> 
	 * @param int roleId
	 * @return Iterable<DatEmpDetailBO>
	 */
	
	@Query("SELECT m from MasMenuBO m "
			+ " INNER JOIN MasRoleMenuMappingBO r ON r.fkMenuId = m.pkMenuId "
			+ " WHERE m.activeStatus like 'ACTIVE' AND r.fkRoleId =?")
	Iterable<MasMenuBO> getAllMenuListForRoleId(int roleId);
	
	
	/**
	 * <Description getAllSubMenuListForParentMenuId:> 
	 * @param int menuId
	 * @return Iterable<DatEmpDetailBO>
	 */
	
	@Query("SELECT m from MasMenuBO m WHERE m.activeStatus like 'ACTIVE' AND m.menuParentId =?")
	Iterable<MasMenuBO> getAllSubMenuListForParentMenuId(int menuId);

	
	
}
