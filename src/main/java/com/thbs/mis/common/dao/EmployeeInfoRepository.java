/*********************************************************************//*
                  FILE HEADER                                      
*//*********************************************************************//*
                                                                   
  FileName    :  EmployRepository.java                                		 
                                                                   
  Author      :  THBS	MIS	                                         
                                                                   
  Date        :  25-Jun-2016                                           
                                                                   
  Description :  TODO			 									 
                 			                                         
                                                                   
                                                                   
*//*********************************************************************//*
 Date        Who     Version      Comments             			 
-------------------------------------------------------------------
 25-Jun-2016    THBS     1.0         Initial version created   		 
*//*********************************************************************//*
package com.thbs.mis.common.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.EmployeeInfoBO;

*//**
 * <Description EmployRepository:> TODO
 * @author THBS
 * @version 1.0
 * @see 
 *//*
@Repository
public interface EmployeeInfoRepository  extends GenericRepository<EmployeeInfoBO,Long>
{

	*//**
	 * <Description getAllEmployeesNameAndId:> TODO
	 * @return
	 *//*
	@Query("Select new com.thbs.mis.common.bo.EmployeeInfoBO(emp.pk_EmpRec_Idx,emp.empRec_EmpFName,emp.empRec_EmpLName,emp.datEmplogin1.pk_EmpLogin_EmpID) from EmployeeInfoBO emp where emp.fkNewEmpstatusId = 1")
	Iterable<EmployeeInfoBO> getAllEmployeesNameAndId();
	
	*//**
	 * <Description getAllReporteesForRMId:> TODO
	 * @return
	 *//*
	
	@Query("Select new com.thbs.mis.common.bo.EmployeeInfoBO(emp.pk_EmpRec_Idx,emp.empRec_EmpFName,emp.empRec_EmpLName,emp.datEmplogin1.pk_EmpLogin_EmpID,emp.datEmplogin3) from EmployeeInfoBO emp where emp.fkNewEmpstatusId = 1 and emp.datEmplogin3.pk_EmpLogin_EmpID = ?1")
	Iterable<EmployeeInfoBO> getAllReporteesForRMId(Integer rmId);
	
	
	EmployeeInfoBO findByEmpId(int empId);

}
*/