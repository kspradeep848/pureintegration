package com.thbs.mis.common.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.MasBusinessEntityBO;

@Repository
public interface MasBusinessEntityRepository extends GenericRepository<MasBusinessEntityBO, Integer>
{
	public MasBusinessEntityBO findByPkEntityId(Byte entityId);
}
