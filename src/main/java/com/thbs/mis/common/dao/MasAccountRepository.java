package com.thbs.mis.common.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.MasAccountBO;

@Repository
public interface MasAccountRepository extends
		GenericRepository<MasAccountBO, Long> {

	public MasAccountBO findByPkAccountId(Short accountId);
}
