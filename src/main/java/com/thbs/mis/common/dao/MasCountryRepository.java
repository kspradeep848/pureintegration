package com.thbs.mis.common.dao;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.bean.CountryBean;
import com.thbs.mis.common.bo.MasCountryBO;

public interface MasCountryRepository extends GenericRepository<MasCountryBO,Long>{
	
	public MasCountryBO findByPkCountryId(short pkCountryId);
	
	@Query("SELECT new com.thbs.mis.common.bean.CountryBean(country.pkCountryId, country.countryName, country.countryCode) "
			+ "FROM MasCountryBO country inner join MasClientAddressBO address "
			+ "ON country.pkCountryId = address.fkCountryId "
			+ "inner join MasClientBO client "
			+ "ON address.fkClientId = client.pkClientId "
			+ "where client.fkRegionId =:regionId")
	public Set<CountryBean> getCountriesBasedOnFkRegionId(@Param("regionId") Short regionId);
}
