package com.thbs.mis.common.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.MasVerticalBO;

@Repository
public interface MasVerticalRepository extends
		GenericRepository<MasVerticalBO, Integer> {

	public MasVerticalBO findByPkVerticalId(Short verticalId);
}
