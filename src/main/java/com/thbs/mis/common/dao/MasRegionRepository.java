package com.thbs.mis.common.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.MasRegionBO;

@Repository
public interface MasRegionRepository extends
		GenericRepository<MasRegionBO, Integer> {

	public MasRegionBO findByPkRegionId(Short regionId);
}
