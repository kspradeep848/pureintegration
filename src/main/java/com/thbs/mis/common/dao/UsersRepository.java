package com.thbs.mis.common.dao;

import com.thbs.mis.common.bo.UsersBO;

public interface UsersRepository extends GenericRepository<UsersBO, Long>{

	public UsersBO findByUserName(int empId);
	
}
