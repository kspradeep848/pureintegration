package com.thbs.mis.common.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.DatEmpModuleMappingBO;

@Repository
public interface ModuleMappingDetailsRepository extends GenericRepository<DatEmpModuleMappingBO, Long>   {
	
	
	/*@Query("SELECT emp FROM DatEmpModuleMappingBO emp JOIN DatEmpDetailBO detail "
			+ " ON detail.pkEmpId = emp.fkEmployeeId "
			+ " WHERE detail.fkEmpMainStatus = 1 AND "
			+ " emp.fkEmployeeId = :empId ")
	public DatEmpModuleMappingBO findByOnlyActiveEmployees(@Param("empId") Integer empIDList);	
		*/
	public DatEmpModuleMappingBO findByFkEmployeeId(int empId);
}