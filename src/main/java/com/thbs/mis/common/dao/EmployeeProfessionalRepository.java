package com.thbs.mis.common.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;

@Repository
public interface EmployeeProfessionalRepository extends GenericRepository<DatEmpProfessionalDetailBO, Long>,
		JpaSpecificationExecutor<DatEmpProfessionalDetailBO> {

	// Added by Kamal Anand for Training Module
	public DatEmpProfessionalDetailBO findByFkMainEmpDetailId(int empId);

	public List<DatEmpProfessionalDetailBO> findByFkEmpReportingMgrId(int mgrId);

	@Query("SELECT p FROM DatEmpProfessionalDetailBO p " + " JOIN DatEmpDetailBO e ON e.pkEmpId = p.fkMainEmpDetailId "
			+ " WHERE p.fkEmpReportingMgrId = :mgrId " + " AND e.fkEmpMainStatus = 1")
	public List<DatEmpProfessionalDetailBO> getActiveReportingMgr(@Param("mgrId") int mgrId);

	// End of Addition by Kamal Anand for Training Module

	// Added by Kamal Anand for Notifications
	@Query("SELECT p FROM DatEmpProfessionalDetailBO p  where p.fkMainEmpDetailId = "
			+ "(select prof.fkEmpReportingMgrId from DatEmpProfessionalDetailBO prof where prof.fkMainEmpDetailId = :empId)")
	public DatEmpProfessionalDetailBO getSkipLevelRmDetailsByEmpId(@Param("empId") int empId);
	// End of Addition by Kamal Anand for Notifications

	// Added by Adavayya Hiremath for UserManagement module

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpProfessionalDetailBO( "
			+ "dateEmpProf.fkMainEmpDetailId,dateEmpPer.empFirstName,dateEmpPer.empMiddleName,dateEmpPer.empLastName,"
			+ "emp_det.empType," + "masbu.pkBuUnitId,masbu.buUnitName, "
			+ "bootcamp.pkBootCampId,bootcamp.bootCampName, " + "dateEmpProf.fkEmpReportingMgrId) FROM "
			+ "DatEmpProfessionalDetailBO dateEmpProf JOIN DatEmpPersonalDetailBO dateEmpPer ON dateEmpProf.fkMainEmpDetailId = dateEmpPer.fkEmpDetailId JOIN "
			+ "DatEmpDetailBO emp_det ON dateEmpProf.fkMainEmpDetailId = emp_det.pkEmpId JOIN "
			+ "MasBuUnitBO masbu ON masbu.pkBuUnitId = dateEmpProf.fkEmpBuUnit JOIN "
			+ "MasBootCampBO bootcamp ON bootcamp.pkBootCampId = dateEmpProf.fkEmpBootCampId "
			+ "WHERE DATE(dateEmpProf.empDateOfJoining) between ?1 and ?2 ")
	public List<DatEmpProfessionalDetailBO> getEmployeeIdByDateOfJoiningDateRange(@Param("fromDate") Date fromDate,
			@Param("toDate") Date toDate);
	// End of Addition by Adavayya Hiremath for UserManagement module

	// Added by Smrithi for Confirmation module
	@Query("Select p from DatEmpProfessionalDetailBO p where p.fkMainEmpDetailId = "
			+ " (Select prof.fkEmpReportingMgrId from DatEmpProfessionalDetailBO prof where prof.fkMainEmpDetailId = "
			+ " (Select prof1.fkEmpReportingMgrId from DatEmpProfessionalDetailBO prof1 where prof1.fkMainEmpDetailId = :empId))")
	public DatEmpProfessionalDetailBO getSkipLevelMgrDetailsByEmpId(@Param("empId") int empId);

	public DatEmpProfessionalDetailBO findByFkEmpBuUnit(Short fkEmpBuUnit);
	// EOA by Smrithi for Confirmation module

	public List<DatEmpProfessionalDetailBO> findByEmpProfessionalEmailId(String emailID);
}
