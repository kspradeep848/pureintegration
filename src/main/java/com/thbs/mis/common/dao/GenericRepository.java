/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GenericRepository.java                                		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  16-Jun-2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.common.dao;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

/**
 * <Description GenericRepository:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */

@NoRepositoryBean
@Transactional(readOnly = true)
public interface GenericRepository<T, ID extends Serializable>
		extends CrudRepository<T, ID>
{

	/**
	 * <Description count:> TODO.
	 * 
	 * @return
	 * @see org.springframework.data.repository.CrudRepository#count()
	 */
	@Override
	 long count();

	/**
	 * <Description delete:> TODO.
	 * 
	 * @param arg0
	 * @see org.springframework.data.repository.CrudRepository#delete(java.io.Serializable)
	 */
	@Override
	@Transactional
	 void delete(ID arg0);

	/**
	 * <Description delete:> TODO.
	 * 
	 * @param arg0
	 * @see org.springframework.data.repository.CrudRepository#delete(java.lang.Iterable)
	 */
	@Override
	@Transactional
	 void delete(Iterable<? extends T> arg0);

	/**
	 * <Description delete:> TODO.
	 * 
	 * @param arg0
	 * @see org.springframework.data.repository.CrudRepository#delete(java.lang.Object)
	 */
	@Override
	@Transactional
	 void delete(T arg0);

	/**
	 * <Description deleteAll:> TODO.
	 * 
	 * @see org.springframework.data.repository.CrudRepository#deleteAll()
	 */
	@Override
	@Transactional
	 void deleteAll();

	/**
	 * <Description exists:> TODO.
	 * 
	 * @param arg0
	 * @return
	 * @see org.springframework.data.repository.CrudRepository#exists(java.io.Serializable)
	 */
	@Override
	 boolean exists(ID arg0);

	/**
	 * <Description findAll:> TODO.
	 * 
	 * @return
	 * @see org.springframework.data.repository.CrudRepository#findAll()
	 */
	@Override
	 Iterable<T> findAll();

	/**
	 * <Description findAll:> TODO.
	 * 
	 * @param arg0
	 * @return
	 * @see org.springframework.data.repository.CrudRepository#findAll(java.lang.Iterable)
	 */
	@Override
	 Iterable<T> findAll(Iterable<ID> arg0);

	/**
	 * <Description findOne:> TODO.
	 * 
	 * @param arg0
	 * @return
	 * @see org.springframework.data.repository.CrudRepository#findOne(java.io.Serializable)
	 */
	@Override
	 T findOne(ID arg0);

	/**
	 * <Description save:> TODO.
	 * 
	 * @param arg0
	 * @return
	 * @see org.springframework.data.repository.CrudRepository#save(java.lang.Iterable)
	 */
	@Override
	@Transactional
	 <S extends T> Iterable<S> save(Iterable<S> arg0);

	/**
	 * <Description save:> TODO.
	 * 
	 * @param arg0
	 * @return
	 * @see org.springframework.data.repository.CrudRepository#save(java.lang.Object)
	 */
	@Override
	@Transactional
	 <S extends T> S save(S arg0);

}
