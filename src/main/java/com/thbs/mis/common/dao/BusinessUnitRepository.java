/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  CommonRepository.java                             */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  24-Jun-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* 	Date          Who     Version      Comments            			 */
/*-------------------------------------------------------------------*/
/* 24-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.common.dao;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.thbs.mis.common.bo.MasBuUnitBO;
/**
 * <Description CommonRepository:> TODO
 * @author THBS
 * @version 1.0
 * @param <T>
 * @see 
 */
@Repository
public interface BusinessUnitRepository extends GenericRepository<MasBuUnitBO,Long>
{
	//List<MasBuUnitBO> findByActiveFlag(String activeFlag);
     
	//Added by prathibha for get all active BU units
	
	@Query("SELECT new com.thbs.mis.common.bo.MasBuUnitBO(bu.pkBuUnitId, bu.buUnitName ,bu.activeFlag, "
			+ "(SELECT COUNT(emp.pkEmpId) FROM DatEmpDetailBO emp "
			+ "JOIN DatEmpProfessionalDetailBO prof ON prof.fkMainEmpDetailId = emp.pkEmpId WHERE "
			+ "emp.fkEmpMainStatus=1 AND prof.fkEmpBuUnit=bu.pkBuUnitId),bu.fkBuHeadEmpId, "
			+ "CONCAT(per.empFirstName, ' ' ,per.empLastName)) FROM "
			+ "MasBuUnitBO bu JOIN DatEmpPersonalDetailBO per ON per.fkEmpDetailId = bu.fkBuHeadEmpId WHERE "
			+ "bu.activeFlag=?")
	public List<MasBuUnitBO>allActiveBU(String activeFlag);
	
	//EOC for get all active BU units
	
	
	
	//Added by prathibha for get all BU Information
	
	@Query("SELECT new com.thbs.mis.common.bo.MasBuUnitBO(bu.pkBuUnitId, bu.buUnitName ,"
			+ "(SELECT COUNT(emp.pkEmpId) FROM DatEmpDetailBO emp "
			+ "JOIN DatEmpProfessionalDetailBO prof ON prof.fkMainEmpDetailId = emp.pkEmpId WHERE "
			+ "emp.fkEmpMainStatus=1 AND prof.fkEmpBuUnit=bu.pkBuUnitId),bu.fkBuHeadEmpId, "
			+ "CONCAT(per.empFirstName, ' ' ,per.empLastName)) FROM "
			+ "MasBuUnitBO bu JOIN DatEmpPersonalDetailBO per ON per.fkEmpDetailId = bu.fkBuHeadEmpId WHERE "
			+ "bu.pkBuUnitId=?1 AND bu.activeFlag=?2")
	public List<MasBuUnitBO> viewBUInformation(short pkBuUnitId,String activeFlag);
	//EOC for  get all BU Information
	
	MasBuUnitBO findByPkBuUnitId(short pkBuUnitId);
	
	List<MasBuUnitBO> findByFkBuHeadEmpId(int fkBuHeadEmpId);
	@Query("SELECT b FROM MasBuUnitBO b WHERE b.fkBuHeadEmpId = :buHeadId AND b.activeFlag like 'ACTIVE' ")
	List<MasBuUnitBO> getAllActiveBusinessUnitsUnderBUHead(@Param("buHeadId") int buHeadId);
}