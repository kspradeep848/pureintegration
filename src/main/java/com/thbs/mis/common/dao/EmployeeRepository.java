/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmployeeRepository.java                           */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  29-Nov-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 29-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/ 
package com.thbs.mis.common.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;

@Repository
public interface EmployeeRepository extends GenericRepository<DatEmpPersonalDetailBO, Long>{
	
	
	/**
	 * <Description getAllActiveEmployeesNameAndId:> TODO
	 * @return List<DatEmpPersonalDetailBO>
	 */
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO("
			+ " emp.fkEmpDetailId, emp.empFirstName, emp.empLastName) "
			+ " FROM DatEmpPersonalDetailBO emp JOIN DatEmpDetailBO detail "
			+ " ON detail.pkEmpId = emp.fkEmpDetailId "
			+ " WHERE detail.fkEmpMainStatus = 1 AND"
			+ " detail.pkEmpId not in (:empIDList)")
	List<DatEmpPersonalDetailBO> getAllActiveEmployeesNameAndId(@Param("empIDList")List<Integer>empIDList);
	
	/**
	 * <Description getAllEmployeesNameAndId:> TODO
	 * @return List<DatEmpPersonalDetailBO>
	 */
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO("
			+ " emp.fkEmpDetailId, emp.empFirstName, emp.empLastName, detail.fkEmpMainStatus,"
			+ " prof.fkEmpReportingMgrId, prof.fkEmpBuUnit, masBu.fkBuHeadEmpId, masBu.buUnitName) "
			+ " FROM DatEmpPersonalDetailBO emp JOIN DatEmpDetailBO detail "
			+ " ON detail.pkEmpId = emp.fkEmpDetailId JOIN DatEmpProfessionalDetailBO prof ON "
			+ " prof.fkMainEmpDetailId = detail.pkEmpId JOIN MasBuUnitBO masBu ON"
			+ " masBu.pkBuUnitId = prof.fkEmpBuUnit AND "
		    + " detail.pkEmpId not in (:empIDList) AND detail.fkEmpMainStatus in(1,2)")
	List<DatEmpPersonalDetailBO> getAllEmployeesNameAndId(@Param("empIDList")List<Integer>empIDList);
	
	
	/**
	 * <Description getAllInactiveEmployeesNameAndId:> TODO
	 * @return List<DatEmpPersonalDetailBO>
	 */
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO("
			+ " emp.fkEmpDetailId, emp.empFirstName, emp.empLastName) "
			+ " FROM DatEmpPersonalDetailBO emp JOIN DatEmpDetailBO detail "
			+ " ON detail.pkEmpId = emp.fkEmpDetailId "
			+ " WHERE detail.fkEmpMainStatus = 2")
	List<DatEmpPersonalDetailBO> getAllInactiveEmployeesNameAndId();
	
	public DatEmpPersonalDetailBO findByFkEmpDetailId(int empId);
	
	
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO("
			+ " emp.fkEmpDetailId, emp.empFirstName, emp.empLastName, "
			+ " emp.empGender, emp.empDateOfBirth) FROM "
			+ " DatEmpPersonalDetailBO emp JOIN DatEmpDetailBO detail "
			+ " ON detail.pkEmpId = emp.fkEmpDetailId "
			+ " WHERE detail.fkEmpMainStatus = 1 "
			+ " AND DATE_FORMAT(emp.empDateOfBirth ,'%m-%d') = DATE_FORMAT(now(), '%m-%d')")
	List<DatEmpPersonalDetailBO> getAllTodaysBirthdayList();
	
	
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO("
			+ " emp.fkEmpDetailId, emp.empFirstName, emp.empLastName, "
			+ " emp.empGender, emp.empDateOfBirth) FROM "
			+ " DatEmpPersonalDetailBO emp JOIN DatEmpDetailBO detail "
			+ " ON detail.pkEmpId = emp.fkEmpDetailId "
			+ " WHERE detail.fkEmpMainStatus = 1 "
			+ " AND DATE_FORMAT(emp.empDateOfBirth ,'%m-%d') BETWEEN "
			+ " DATE_FORMAT(? , '%m-%d') AND DATE_FORMAT(? , '%m-%d') "
			+ " ORDER BY DATE_FORMAT(emp.empDateOfBirth ,'%m-%d')")
	List<DatEmpPersonalDetailBO> getUpcomingBirthdayList(Date firstDate, Date lastDate);
	
	
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO("
			+ " emp.fkEmpDetailId, emp.empFirstName, emp.empLastName, "
			+ " emp.empGender, emp.empDateOfBirth) FROM "
			+ " DatEmpPersonalDetailBO emp JOIN DatEmpDetailBO detail "
			+ " ON detail.pkEmpId = emp.fkEmpDetailId "
			+ " WHERE detail.fkEmpMainStatus = 1 "
			+ " AND DATE_FORMAT(emp.empDateOfBirth ,'%m-%d') BETWEEN "
			+ " DATE_FORMAT(? , '%m-%d') "
			+ " AND DATE_FORMAT(? , '%m-%d') "
			+ " ORDER BY DATE_FORMAT(emp.empDateOfBirth ,'%m-%d') desc")
	List<DatEmpPersonalDetailBO> getRecentBirthdayList(Date firstDate, Date lastDate);
	
	
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO("
			+ " emp.fkEmpDetailId, emp.empFirstName, emp.empLastName, "
			+ " emp.empGender, emp.empDateOfBirth) FROM "
			+ " DatEmpPersonalDetailBO emp JOIN DatEmpExcludeBirthdayAnniversaryBO exclude "
			+ " ON emp.fkEmpDetailId = exclude.fkExcludeBirthdayAnniversaryEmpId "
			+ " JOIN DatEmpDetailBO detail ON detail.pkEmpId = emp.fkEmpDetailId "
			+ " WHERE exclude.excludeBirthdayFlag like 'ENABLED'"
			+ " AND detail.fkEmpMainStatus = 1")
	List<DatEmpPersonalDetailBO> getAllExcludeEmpBirthdayList();
	
	
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO("
			+ " emp.fkEmpDetailId, emp.empFirstName, emp.empLastName, "
			+ " prof.empDateOfJoining, emp.empGender) FROM "
			+ " DatEmpPersonalDetailBO emp JOIN DatEmpExcludeBirthdayAnniversaryBO exclude "
			+ " ON emp.fkEmpDetailId = exclude.fkExcludeBirthdayAnniversaryEmpId "
			+ " JOIN DatEmpProfessionalDetailBO prof "
			+ " ON prof.fkMainEmpDetailId = emp.fkEmpDetailId "
			+ " JOIN DatEmpDetailBO detail ON detail.pkEmpId = emp.fkEmpDetailId "
			+ " WHERE exclude.excludeAnniversaryFlag like 'ENABLED'"
			+ " AND detail.fkEmpMainStatus = 1")
	List<DatEmpPersonalDetailBO> getAllExcludeEmpAnniversaryList();
	
	
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO("
			+ " emp.fkEmpDetailId, emp.empFirstName, emp.empLastName, "
			+ " prof.empDateOfJoining, emp.empGender) FROM DatEmpPersonalDetailBO emp "
			+ " JOIN DatEmpDetailBO detail ON detail.pkEmpId = emp.fkEmpDetailId "
			+ " JOIN DatEmpProfessionalDetailBO prof "
			+ " ON prof.fkMainEmpDetailId = emp.fkEmpDetailId "
			+ " WHERE detail.fkEmpMainStatus = 1 "
			+ " AND DATE_FORMAT(prof.empDateOfJoining ,'%m-%d') = DATE_FORMAT(now(), '%m-%d')")
	List<DatEmpPersonalDetailBO> getAllTodayAnniversaryList();
	
	
	
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO("
			+ " emp.fkEmpDetailId, emp.empFirstName, emp.empLastName, "
			+ " prof.empDateOfJoining, emp.empGender) FROM DatEmpPersonalDetailBO emp "
			+ " JOIN DatEmpDetailBO detail ON detail.pkEmpId = emp.fkEmpDetailId "
			+ " JOIN DatEmpProfessionalDetailBO prof "
			+ " ON prof.fkMainEmpDetailId = emp.fkEmpDetailId "
			+ " WHERE detail.fkEmpMainStatus = 1 "
			+ " AND DATE_FORMAT(prof.empDateOfJoining ,'%m-%d') BETWEEN "
			+ " DATE_FORMAT(? , '%m-%d') AND DATE_FORMAT(? , '%m-%d') "
			+ " ORDER BY DATE_FORMAT(prof.empDateOfJoining ,'%m-%d')")
	List<DatEmpPersonalDetailBO> getUpcomingAnniversaryList(Date firstDate, Date lastDate);
	
	
	
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO("
			+ " emp.fkEmpDetailId, emp.empFirstName, emp.empLastName, "
			+ " prof.empDateOfJoining, emp.empGender) FROM DatEmpPersonalDetailBO emp "
			+ " JOIN DatEmpDetailBO detail ON detail.pkEmpId = emp.fkEmpDetailId "
			+ " JOIN DatEmpProfessionalDetailBO prof "
			+ " ON prof.fkMainEmpDetailId = emp.fkEmpDetailId "
			+ " WHERE detail.fkEmpMainStatus = 1 "
			+ " AND DATE_FORMAT(prof.empDateOfJoining ,'%m-%d') BETWEEN "
			+ " DATE_FORMAT(? , '%m-%d') "
			+ " AND DATE_FORMAT(? , '%m-%d') "
			+ " ORDER BY DATE_FORMAT(prof.empDateOfJoining ,'%m-%d') desc")
	List<DatEmpPersonalDetailBO> getRecentAnniversaryList(Date firstDate, Date lastDate);
	
	
	/**
	 * <Description getAllPermanentAndGraduatedEmployees:> TODO
	 * @return List<DatEmpPersonalDetailBO>
	 */
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO("
			+ " emp.fkEmpDetailId, emp.empFirstName, emp.empLastName, des.empDesignationName) "
			+ " FROM DatEmpPersonalDetailBO emp JOIN DatEmpDetailBO detail "
			+ " ON detail.pkEmpId = emp.fkEmpDetailId "
			+ " JOIN DatEmpProfessionalDetailBO prof "
			+ " ON detail.pkEmpId = prof.fkMainEmpDetailId "
			//Added by Shyam for setting of designation in bean
			+ " JOIN MasEmpDesignationBO as des "
			+ " ON des.pkEmpDesignationId = prof.fkEmpDesignation "
			//End of addition by Shyam for setting of designation in bean
			+ " WHERE detail.empType = 1 "
			+ " AND prof.empConfirmationGraduateFlag = 1 AND "
			+ " detail.pkEmpId not in (:empIDList)")
	List<DatEmpPersonalDetailBO> getAllPermanentAndGraduatedEmployeesNameAndId(@Param("empIDList")List<Integer>empIDList);
	
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO("
			+ " emp.fkEmpDetailId, emp.empFirstName, emp.empLastName, des.empDesignationName) "
			+ " FROM DatEmpPersonalDetailBO emp JOIN DatEmpDetailBO detail "
			+ " ON detail.pkEmpId = emp.fkEmpDetailId "
			+ " JOIN DatEmpProfessionalDetailBO prof "
			+ " ON detail.pkEmpId = prof.fkMainEmpDetailId "
			//Added by Shyam for setting of designation in bean
			+ " JOIN MasEmpDesignationBO as des "
			+ " ON des.pkEmpDesignationId = prof.fkEmpDesignation "
			//End of addition by Shyam for setting of designation in bean
			+ " WHERE detail.empType = 1 "
			+ " AND prof.empConfirmationGraduateFlag = 1 AND "
			+ " detail.fkEmpMainStatus = 1 AND detail.fkEmpSubStatus in (5, 7) AND"
			+ " detail.pkEmpId not in (:empIDList)")
	List<DatEmpPersonalDetailBO> getAllPermanentAndGraduatedActiveEmployeesNameAndId(@Param("empIDList")List<Integer>empIDList);
	
}
