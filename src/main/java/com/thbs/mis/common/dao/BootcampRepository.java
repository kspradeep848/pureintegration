/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  BootcampRepository.java                           */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  14-Nov-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 14-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.common.dao;

import com.thbs.mis.common.bo.MasBootCampBO;


/**
 * <Description BootcampRepository:> TODO
 * @author THBS
 * @version 1.0
 * @see 
 */
public interface BootcampRepository  extends GenericRepository<MasBootCampBO, Integer>
{
	//Added by Kamal Anand for User Management
	public MasBootCampBO findByPkBootCampId(short bootCampId);
	//End of Addition by Kamal Anand for User Management
}
