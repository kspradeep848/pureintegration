package com.thbs.mis.common.dao;

import com.thbs.mis.common.bo.MasEmpMainStatusBO;

public interface MasEmpMainDetailRepository extends
		GenericRepository<MasEmpMainStatusBO, Long> {

	public MasEmpMainStatusBO findByPkEmpMainStatusId(byte mainStatusId);

}
