package com.thbs.mis.common.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.DatForexConversionsBO;

@Repository
public interface DatForexConversionsRepository extends GenericRepository<DatForexConversionsBO, Integer> {

	public DatForexConversionsBO findByFkForexCurrencyTypeId(Byte fkForexCurrencyTypeId);

	@Query("SELECT rep FROM DatForexConversionsBO rep WHERE "
			+ " :date BETWEEN rep.forexFromDate AND rep.forexToDate ")
	public List<DatForexConversionsBO> findDetailsByDates(@Param("date") Date date);

}
