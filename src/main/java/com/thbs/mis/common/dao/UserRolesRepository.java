package com.thbs.mis.common.dao;

import com.thbs.mis.common.bo.UserRolesBO;

public interface UserRolesRepository extends GenericRepository<UserRolesBO, Long>{

	public UserRolesBO findByUserName(int empId);
}
