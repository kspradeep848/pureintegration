/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  ParentLocationRepository.java                     */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  14-Nov-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 14-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.common.dao;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.bo.MasLocationParentBO;

/**
 * <Description ParentLocationRepository:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
public interface ParentLocationRepository extends GenericRepository<MasLocationParentBO, Integer> {

	// Added by Adavayya Hiremath for fetching Location details based on Country
	// Id
	// @Query("SELECT l FROM MasLocationParentBO l WHERE l.fkCountryId = :countryId ")
	List<MasLocationParentBO> findByfkCountryId(@Param("countryId") Short countryId);
	// EOA by Adavayya Hiremath for fetching Location details based on Country
	// Id
	//Added by Kamal Aand for User Management
	public MasLocationParentBO findByPkLocationParentId(short locationId);
	
	public MasLocationParentBO findByPkLocationParentIdAndFkCountryId(short locationId,short countryId);
	//End of Addition by Kamal Anand for User Management
}
