/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  DesignationRepository.java                        */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  14-Nov-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 14-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/


package com.thbs.mis.common.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.MasEmpDesignationBO;

/**
 * <Description DesignationRepository:> 
 * @author THBS
 * @version 1.0
 * @see 
 */
@Repository
public interface DesignationRepository extends GenericRepository <MasEmpDesignationBO,Long>
{
	//Added by Kamal Anand for User Management
	public MasEmpDesignationBO findByPkEmpDesignationId(short designationid);
	//End of Addition by Kamal Anand for User Management
	
	public List<MasEmpDesignationBO> findAllByOrderByEmpDesignationNameAsc();
}
