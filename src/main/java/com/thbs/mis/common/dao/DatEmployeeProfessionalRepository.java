package com.thbs.mis.common.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bean.EmployeeNameWithRMIdBean;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
@Repository
public interface DatEmployeeProfessionalRepository extends GenericRepository<DatEmpProfessionalDetailBO, Long>,
		JpaSpecificationExecutor<DatEmpProfessionalDetailBO> {

	public DatEmpProfessionalDetailBO findByFkMainEmpDetailId(int empId);

	/*
	 * public List<DatEmpProfessionalDetailBO>
	 * findByFkMainEmpDetailIdIn(List<Integer> status);
	 */

	// Added by Mani for GSR module service committed on 30-11-2016
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpProfessionalDetailBO(emp_pro.fkMainEmpDetailId, emp_pro.fkEmpReportingMgrId,"
			+ " emp_pro.hasReporteesFlag, emp_pro.fkEmpBuUnit, emp_pro.empProfessionalEmailId) "
			+ " from DatEmpProfessionalDetailBO emp_pro "
			+ " INNER JOIN DatEmpDetailBO emp_det ON emp_pro.fkMainEmpDetailId = emp_det.pkEmpId "
			+ " WHERE emp_det.fkEmpMainStatus = 1 and emp_pro.fkEmpReportingMgrId = :fkEmpReportingMgrId")
	List<DatEmpProfessionalDetailBO> findByFkEmpReportingMgrId(
			@Param("fkEmpReportingMgrId") Integer fkEmpReportingMgrId);

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpProfessionalDetailBO(emp_pro.fkMainEmpDetailId, emp_pro.fkEmpReportingMgrId,"
			+ " emp_pro.hasReporteesFlag, emp_pro.fkEmpBuUnit, emp_pro.empProfessionalEmailId) "
			+ " from DatEmpProfessionalDetailBO emp_pro "
			+ " INNER JOIN DatEmpDetailBO emp_det ON emp_pro.fkMainEmpDetailId = emp_det.pkEmpId "
			+ " WHERE emp_det.fkEmpMainStatus = 1 and emp_pro.fkEmpBuUnit = :fkEmpBuUnit")
	List<DatEmpProfessionalDetailBO> findByFkEmpBuUnit(@Param("fkEmpBuUnit") short fkEmpBuUnit);

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpProfessionalDetailBO(emp_pro.fkMainEmpDetailId, emp_pro.fkEmpReportingMgrId,"
			+ " emp_pro.hasReporteesFlag, emp_pro.fkEmpBuUnit, emp_pro.empProfessionalEmailId) "
			+ " from DatEmpProfessionalDetailBO emp_pro "
			+ " INNER JOIN DatEmpDetailBO emp_det ON emp_pro.fkMainEmpDetailId = emp_det.pkEmpId "
			+ " WHERE emp_det.fkEmpMainStatus = 1 and emp_pro.fkEmpDomainMgrId = :fkEmpDomainMgrId")
	List<DatEmpProfessionalDetailBO> findByFkEmpDomainMgrId(@Param("fkEmpDomainMgrId") Integer fkEmpDomainMgrId);

	// End of addition by Mani for GSR module service committed on 30-11-2016

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpProfessionalDetailBO(emp_pro.fkMainEmpDetailId, emp_pro.fkEmpReportingMgrId,"
			+ " emp_pro.hasReporteesFlag, emp_pro.fkEmpBuUnit, emp_pro.empProfessionalEmailId) "
			+ " from DatEmpProfessionalDetailBO emp_pro "
			+ " INNER JOIN DatEmpDetailBO emp_det ON emp_pro.fkMainEmpDetailId = emp_det.pkEmpId "
			+ " WHERE emp_det.fkEmpMainStatus = 1 ")
	List<DatEmpProfessionalDetailBO> getAllActiveEmployees();

	// Added by Kamal Anand for UAN module
	@Query(nativeQuery = true, value = "select fk_main_emp_detail_id from dat_emp_professional_detail where"
			+ " emp_date_of_joining >= :startDate and emp_date_of_joining <= :endDate")
	public List<Integer> getEmployeeIdByDateOfJoiningDateRange(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);
	// End of Addition by Kamal Anand for UAN module

	// Added by Shyam for Gsr Hierarchy structure under Hr login
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpProfessionalDetailBO( prof.fkMainEmpDetailId, "
			+ " empPersonal.empFirstName, empPersonal.empLastName, prof.fkEmpBuUnit, bu.buUnitName, "
			+ " prof.fkEmpReportingMgrId, prof.hasReporteesFlag, detail.fkEmpRoleId)"
			+ " FROM DatEmpProfessionalDetailBO prof "
			+ " JOIN DatEmpPersonalDetailBO empPersonal ON empPersonal.fkEmpDetailId = prof.fkMainEmpDetailId "
			+ " JOIN MasBuUnitBO bu ON bu.pkBuUnitId = prof.fkEmpBuUnit "
			+ " JOIN DatEmpDetailBO detail ON detail.pkEmpId = prof.fkMainEmpDetailId "
			+ " WHERE prof.fkMainEmpDetailId = :employeeId ")
	public DatEmpProfessionalDetailBO getEmployeeProfessionalDetail(@Param("employeeId") Integer employeeId);
	// End of addition by Shyam for Gsr Hierarchy structure under Hr login

	// Added by Shyam for new gsr goal creation
	@Query("SELECT d FROM DatEmpProfessionalDetailBO d WHERE d.fkMainEmpDetailId in ( :empId, :mgrId )")
	public List<DatEmpProfessionalDetailBO> getEmpProfDetailList(@Param("empId") int empId, @Param("mgrId") int mgrId);

	// End of addition by Shyam for new gsr goal creation
	public List<DatEmpProfessionalDetailBO> findByFkMainEmpDetailIdIn(List<Integer> empId);

	// added by mani
	@Query(nativeQuery = true, value = " select * from " + " 	dat_emp_professional_detail  pro1 "
			+ " JOIN dat_emp_detail emp1 on emp1.pk_emp_id = pro1.fk_main_emp_detail_id"
			+ " 	where  pro1.fk_emp_reporting_mgr_id = :fkEmpReportingMgrId "
			+ " and pro1.emp_confirmation_graduate_flag = 1" + " and emp1.fk_emp_sub_status in(5, 7)"
			+ " 	and (pro1.emp_date_of_confirmation <=  :startDate or pro1.emp_date_of_joining <= :startDate) "
			+ "  and emp1.fk_emp_main_status =1" + "  and pro1.effective_date_of_reporting_mgr_change IS  NULL "
			+ "  UNION " + "  select * from  dat_emp_professional_detail  pro2"
			+ "  JOIN dat_emp_detail emp2 on emp2.pk_emp_id = pro2.fk_main_emp_detail_id "
			+ "  where pro2.fk_emp_reporting_mgr_id = :fkEmpReportingMgrId"
			+ " AND pro2.emp_confirmation_graduate_flag = 1" + "  and emp2.fk_emp_main_status =1"
			+ " and emp2.fk_emp_sub_status in(5, 7)"
			+ " and pro2.effective_date_of_reporting_mgr_change <= :startDate ")
	List<DatEmpProfessionalDetailBO> getConfirmedReporteesByRmAndDateRange(@Param("startDate") Date startDate,
			@Param("fkEmpReportingMgrId") Integer fkEmpReportingMgrId);

	// added by mani
	@Query(nativeQuery = true, value = " select * from " + " 	dat_emp_professional_detail  pro1 "
			+ " JOIN dat_emp_detail emp1 on emp1.pk_emp_id = pro1.fk_main_emp_detail_id"
			+ " 	where  pro1.fk_emp_bu_unit = :buId " + " 	and pro1.emp_date_of_confirmation <=  :startDate "
			+ "   and emp1.fk_emp_main_status =1" + "  and pro1.effective_date_of_reporting_mgr_change IS  NULL "
			+ "  UNION " + "  select * from  dat_emp_professional_detail  pro2"
			+ "  JOIN dat_emp_detail emp2 on emp2.pk_emp_id = pro2.fk_main_emp_detail_id "
			+ "  where pro2.fk_emp_bu_unit = :buId " + "  and emp2.fk_emp_main_status =1"
			+ " and pro2.effective_date_of_reporting_mgr_change <= :startDate ")
	List<DatEmpProfessionalDetailBO> getConfirmedReporteesByBUAndDateRange(@Param("startDate") Date startDate,
			@Param("buId") Short buId);

	@Query(nativeQuery = true, value = " select * from " + " 	dat_emp_professional_detail  pro1 "
			+ " JOIN dat_emp_detail emp1 on emp1.pk_emp_id = pro1.fk_main_emp_detail_id"
			+ " 	where  pro1.fk_emp_domain_mgr_id = :fkEmpDomainMgrId "
			+ " and pro1.emp_confirmation_graduate_flag = 1" + " and emp1.fk_emp_sub_status  in(5, 7)"
			+ " 	and (pro1.emp_date_of_confirmation <=  :startDate or pro1.emp_date_of_joining <= :startDate) "
			+ "  and emp1.fk_emp_main_status =1" + "  and pro1.effective_date_of_reporting_mgr_change IS  NULL "
			+ "  UNION " + "  select * from  dat_emp_professional_detail  pro2"
			+ "  JOIN dat_emp_detail emp2 on emp2.pk_emp_id = pro2.fk_main_emp_detail_id "
			+ "  where pro2.fk_emp_domain_mgr_id = :fkEmpDomainMgrId" + " AND pro2.emp_confirmation_graduate_flag = 1"
			+ "  and emp2.fk_emp_main_status =1" + " and emp2.fk_emp_sub_status in(5, 7)"
			+ " and pro2.effective_date_of_reporting_mgr_change <= :startDate ")
	List<DatEmpProfessionalDetailBO> getConfirmedReporteesByDMAndDateRange(@Param("startDate") Date startDate,
			@Param("fkEmpDomainMgrId") Integer fkEmpDomainMgrId);

	@Query(nativeQuery = true, value = " select * from " + " 	dat_emp_professional_detail  pro1 "
			+ " JOIN dat_emp_detail emp1 on emp1.pk_emp_id = pro1.fk_main_emp_detail_id" + " 	where"
			// + " pro1.fk_emp_reporting_mgr_id = :fkEmpReportingMgrId "
			+ "  pro1.emp_confirmation_graduate_flag = 1" + " and emp1.fk_emp_sub_status  in( 5,7)"
			+ " 	and (pro1.emp_date_of_confirmation <=  :startDate or pro1.emp_date_of_joining <= :startDate) "
			+ "  and emp1.fk_emp_main_status =1" + "  and pro1.effective_date_of_reporting_mgr_change IS  NULL "
			+ "  UNION " + "  select * from  dat_emp_professional_detail  pro2"
			+ "  JOIN dat_emp_detail emp2 on emp2.pk_emp_id = pro2.fk_main_emp_detail_id " + "  where"
			// + " pro2.fk_emp_reporting_mgr_id = :fkEmpReportingMgrId"
			+ " pro2.emp_confirmation_graduate_flag = 1" + "  and emp2.fk_emp_main_status =1"
			+ " and emp2.fk_emp_sub_status in(5, 7)"
			+ " and pro2.effective_date_of_reporting_mgr_change <= :startDate ")
	List<DatEmpProfessionalDetailBO> getConfirmedReporteesByDateRange(@Param("startDate") Date startDate);

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpProfessionalDetailBO(emp_pro.fkMainEmpDetailId, emp_pro.fkEmpReportingMgrId,"
			+ " emp_pro.hasReporteesFlag, emp_pro.fkEmpBuUnit, emp_pro.empProfessionalEmailId) "
			+ " from DatEmpProfessionalDetailBO emp_pro "
			+ " INNER JOIN DatEmpDetailBO emp_det ON emp_pro.fkMainEmpDetailId = emp_det.pkEmpId "
			+ " WHERE emp_det.fkEmpMainStatus = 1 and emp_pro.fkEmpBuUnit in :fkEmpBuUnit "
			+ " AND emp_pro.empConfirmationGraduateFlag = 1 " + " ORDER BY emp_det.pkEmpId ")
	List<DatEmpProfessionalDetailBO> findByFkEmpBuUnitOnlyGraduated(@Param("fkEmpBuUnit") List<Short> fkEmpBuUnit);

	// Added by Kamal Anand
	@Query(name = "DatEmpProfessionalDetailBO.getRmAndSkipLevelRMInBU")
	List<EmployeeNameWithRMIdBean> getRmAndSkipLevelRmInBUByEmpId(@Param("empId") Integer empId,
			@Param("empIdList") List<Integer> empIdList);
	// End of Addition by Kamal Anand

	// Added by Kamal Anand for Notifications
	List<DatEmpProfessionalDetailBO> findByEmpProfessionalEmailIdIsNull();
	
	@Query("SELECT prof.fkMainEmpDetailId FROM DatEmpProfessionalDetailBO prof "
			+ "join DatEmpDetailBO det on det.pkEmpId = prof.fkMainEmpDetailId where det.fkEmpMainStatus = 1 "
			+ "and prof.fkEmpLevel in (select pkEmpLevelId from MasEmpLevelBO where empLevelName in(:levelNameList))")
	List<Integer> getEmpIdByRoleNameList(@Param("levelNameList") List<String> levelNameList);
	
	@Query("select prof.fkEmpReportingMgrId FROM DatEmpProfessionalDetailBO prof "
			+ "join DatEmpDetailBO det on det.pkEmpId = prof.fkEmpReportingMgrId "
			+ "where det.fkEmpMainStatus = 1 and prof.fkMainEmpDetailId in"
			+ "(SELECT p.fkMainEmpDetailId FROM DatEmpProfessionalDetailBO p "
			+ "join MasEmpLevelBO m on m.pkEmpLevelId = p.fkEmpLevel "
			+ "join DatEmpDetailBO d on d.pkEmpId = p.fkMainEmpDetailId "
			+ "where m.empLevelName in(:levelNameList) AND d.fkEmpMainStatus = 1) "
			+ "group by prof.fkEmpReportingMgrId")
	List<Integer> getRmIdByRoleNameList(@Param("levelNameList") List<String> levelNameList);
	// End of Addition by Kamal Anand for Notifications

	// added by rajesh for expense module
	@Query("SELECT prof from DatEmpProfessionalDetailBO prof "
			+ " JOIN DatEmpDetailBO emp ON emp.pkEmpId = prof.fkMainEmpDetailId " + " where "
			+ " emp.fkEmpRoleId IN (:empRoles) " + " AND emp.fkEmpMainStatus = 1 " + " AND emp.fkEmpSubStatus = 7 ")
	public List<DatEmpProfessionalDetailBO> fetchListOfEmpFinExp(@Param("empRoles") List<Short> empRoles);

	DatEmpProfessionalDetailBO findByFkMainEmpDetailId(Integer fkMainEmpDetailId);
	// EOA by rajesh

	//added by Deepa for expense notification
	@Query("select prof.fkMainEmpDetailId from DatEmpProfessionalDetailBO prof"
			+ " JOIN DatEmpDetailBO emp ON emp.pkEmpId = prof.fkMainEmpDetailId"
			+ " JOIN MasEmpRoleBO empRole ON empRole.pkEmpRoleId = emp.fkEmpRoleId "
			+ " WHERE empRole.empRoleKeyName IN (:financeRoleKeyNames) AND prof.fkEmpOrganizationId=:organizationId" 
			+ " AND emp.fkEmpMainStatus = 1 AND emp.fkEmpSubStatus = 7")
	public List<Integer> getFinEmpIds(@Param("financeRoleKeyNames") List<String> financeRoleKeyNameList,
			@Param("organizationId") Short organizationId);
	// EOA by Deepa for Notification
	
	
}
