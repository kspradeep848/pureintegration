package com.thbs.mis.common.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.bo.MasEmpRoleBO;

public interface MasEmpRolesRepository extends GenericRepository<MasEmpRoleBO, Long> {

	public List<MasEmpRoleBO> findAll();
	
	public MasEmpRoleBO findByPkEmpRoleId(short pkEmpRoleId);
	
	@Query("SELECT new com.thbs.mis.common.bo.MasEmpRoleBO(role.pkEmpRoleId, role.empRoleName, role.empRoleKeyName) from MasEmpRoleBO role where role.empRoleKeyName in:roleNames")
	public List<MasEmpRoleBO> findByEmpRoleNames(@Param("roleNames") List<String> roleNames);

}
