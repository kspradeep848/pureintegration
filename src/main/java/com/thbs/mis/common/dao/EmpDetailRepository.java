/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmpDetailRepository.java                          */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the Emp Detail Repository                */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016    THBS     1.0      Initial version created   	 	 */
/*********************************************************************/
package com.thbs.mis.common.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.NamedNativeQuery;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.bean.MyReporteesBean;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.usermanagement.bean.AdvancedSearchOutputBean;

public interface EmpDetailRepository
		extends GenericRepository<DatEmpDetailBO, Long>, JpaSpecificationExecutor<DatEmpDetailBO> {

	/**
	 * <Description getAllDirectReporteesWithRMIdForRMId:> TODO
	 * 
	 * @return List<DatEmpDetailBO>
	 */

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpDetailBO(emp_detail.pkEmpId, emp_personal.empFirstName,"
			+ " emp_personal.empLastName, emp_prof.fkEmpReportingMgrId, emp_prof.hasReporteesFlag) "
			+ " from DatEmpDetailBO emp_detail "
			+ " INNER JOIN DatEmpProfessionalDetailBO emp_prof ON emp_prof.fkMainEmpDetailId = emp_detail.pkEmpId "
			+ " INNER JOIN DatEmpPersonalDetailBO emp_personal ON emp_personal.fkEmpDetailId = emp_detail.pkEmpId "
			+ " WHERE emp_detail.fkEmpMainStatus = 1 " + " AND emp_prof.fkEmpReportingMgrId =:rmId AND "
			+ "emp_detail.pkEmpId not in (:empIDList)")
	List<DatEmpDetailBO> getAllDirectReporteesWithRMIdForRMId(@Param("rmId") Integer rmId,
			@Param("empIDList") List<Integer> empIDList);

	// Added by Kamal Anand for Training Module
	public List<DatEmpDetailBO> findByFkEmpMainStatus(byte status);
	// End of Addition by Kamal Anand for Training Module

	// Added by Pratibha for Training Module
	public List<DatEmpDetailBO> findByFkEmpRoleIdIn(List<Short> empRoles);
	// EOA by Pratibha for Training Module

	public DatEmpDetailBO findByPkEmpId(Integer empId);

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpDetailBO(emp_detail.pkEmpId, emp_personal.empFirstName,"
			+ " emp_personal.empLastName, emp_prof.fkEmpReportingMgrId, emp_prof.hasReporteesFlag) "
			+ " from DatEmpDetailBO emp_detail "
			+ " INNER JOIN DatEmpProfessionalDetailBO emp_prof ON emp_prof.fkMainEmpDetailId = emp_detail.pkEmpId "
			+ " INNER JOIN DatEmpPersonalDetailBO emp_personal ON emp_personal.fkEmpDetailId = emp_detail.pkEmpId "
			+ " WHERE emp_detail.fkEmpMainStatus = 1 " + " AND emp_prof.hasReporteesFlag = :hasReportee AND "
			+ "emp_detail.pkEmpId not in (:empIDList)")
	List<DatEmpDetailBO> getAllActiveManagers(@Param("hasReportee") String hasReportee,
			@Param("empIDList") List<Integer> empIDList);

	// added by Smrithi
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpDetailBO(e.pkEmpId,p.empFirstName,p.empLastName)"
			+ "from DatEmpDetailBO e " + "INNER JOIN DatEmpPersonalDetailBO p ON p.fkEmpDetailId = e.pkEmpId "
			+ "JOIN DatEmpProfessionalDetailBO emp_prof ON emp_prof.fkMainEmpDetailId = e.pkEmpId "
			+ "WHERE emp_prof.hasReporteesFlag =  :hasReporteesFlag AND " + "e.pkEmpId not in (:empIDList)")
	List<DatEmpDetailBO> getAllReportingManagers(@Param("hasReporteesFlag") String hasReporteesFlag,
			@Param("empIDList") List<Integer> empIDList);

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpDetailBO(emp_detail.pkEmpId, emp_personal.empFirstName,"
			+ " emp_personal.empLastName, emp_prof.fkEmpReportingMgrId, emp_prof.hasReporteesFlag) "
			+ " from DatEmpDetailBO emp_detail "
			+ " INNER JOIN DatEmpProfessionalDetailBO emp_prof ON emp_prof.fkMainEmpDetailId = emp_detail.pkEmpId "
			+ " INNER JOIN DatEmpPersonalDetailBO emp_personal ON emp_personal.fkEmpDetailId = emp_detail.pkEmpId "
			+ " WHERE emp_detail.fkEmpMainStatus = 1 " + " AND emp_prof.hasReporteesFlag = 'YES_HAS_REPORTEES'"
			+ " AND emp_prof.fkEmpBuUnit = :buUnitId " + " AND emp_detail.pkEmpId not in (:empIDList)")
	List<DatEmpDetailBO> getAllActiveManagersByBuUnitId(@Param("buUnitId") Short buUnitId,
			@Param("empIDList") List<Integer> empIDList);

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpDetailBO(emp_detail.pkEmpId, emp_personal.empFirstName,"
			+ " emp_personal.empLastName, emp_prof.fkEmpReportingMgrId, emp_prof.hasReporteesFlag) "
			+ " from DatEmpDetailBO emp_detail "
			+ " INNER JOIN DatEmpProfessionalDetailBO emp_prof ON emp_prof.fkMainEmpDetailId = emp_detail.pkEmpId "
			+ " INNER JOIN DatEmpPersonalDetailBO emp_personal ON emp_personal.fkEmpDetailId = emp_detail.pkEmpId "
			+ " WHERE emp_detail.fkEmpMainStatus = 1 " + " AND emp_prof.hasReporteesFlag = 'YES_HAS_REPORTEES'"
			+ " AND emp_detail.pkEmpId != :restrictEmpId "
			+ " AND (emp_prof.fkEmpBuUnit = (SELECT emp_prof1.fkEmpBuUnit FROM DatEmpProfessionalDetailBO emp_prof1 where emp_prof1.fkMainEmpDetailId = :empId))")
	/*
	 * +
	 * " OR emp_prof.fkEmpReportingMgrId =(SELECT emp_prof1.fkEmpReportingMgrId FROM DatEmpProfessionalDetailBO emp_prof1 where emp_prof1.fkMainEmpDetailId = :empId) )"
	 * )
	 */
	List<DatEmpDetailBO> getAllReportingManagersByEmpId(@Param("empId") Integer empId,
			@Param("restrictEmpId") Integer restrictEmpId);

	// Code in Purchase branch
	/*
	 * @Query("SELECT new com.thbs.mis.common.bo.DatEmpDetailBO(emp_detail.pkEmpId, emp_personal.empFirstName,"
	 * +
	 * " emp_personal.empLastName, emp_prof.fkEmpReportingMgrId, emp_prof.hasReporteesFlag) "
	 * + " from DatEmpDetailBO emp_detail " +
	 * " INNER JOIN DatEmpProfessionalDetailBO emp_prof ON emp_prof.fkMainEmpDetailId = emp_detail.pkEmpId "
	 * +
	 * " INNER JOIN DatEmpPersonalDetailBO emp_personal ON emp_personal.fkEmpDetailId = emp_detail.pkEmpId "
	 * + " WHERE emp_detail.fkEmpMainStatus = 1 " +
	 * " AND emp_prof.hasReporteesFlag = 'YES_HAS_REPORTEES'" +
	 * " AND emp_detail.pkEmpId != :restrictEmpId " +
	 * " AND emp_prof.fkEmpBuUnit = (SELECT emp_prof1.fkEmpBuUnit FROM DatEmpProfessionalDetailBO emp_prof1 where emp_prof1.fkMainEmpDetailId = :empId) AND "
	 * + " emp_detail.pkEmpId not in (:empIDList)") List<DatEmpDetailBO>
	 * getAllReportingManagersByEmpId(@Param("empId")Integer
	 * empId,@Param("restrictEmpId")Integer
	 * restrictEmpId,@Param("empIDList")List<Integer>empIDList);
	 */

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpDetailBO(emp_detail.pkEmpId, emp_personal.empFirstName,"
			+ " emp_personal.empLastName, emp_prof.fkEmpReportingMgrId, emp_prof.hasReporteesFlag) "
			+ " from DatEmpDetailBO emp_detail "
			+ " INNER JOIN DatEmpProfessionalDetailBO emp_prof ON emp_prof.fkMainEmpDetailId = emp_detail.pkEmpId "
			+ " INNER JOIN DatEmpPersonalDetailBO emp_personal ON emp_personal.fkEmpDetailId = emp_detail.pkEmpId "
			+ " WHERE emp_personal.fkEmpDetailId = ( SELECT masBuUnit.fkBuHeadEmpId from MasBuUnitBO masBuUnit where masBuUnit.pkBuUnitId = "
			+ " ( SELECT emp_prof1.fkEmpBuUnit FROM DatEmpProfessionalDetailBO emp_prof1 where emp_prof1.fkMainEmpDetailId = :empId )) "
			+ " AND emp_detail.fkEmpMainStatus = 1 " + " AND emp_prof.hasReporteesFlag = 'YES_HAS_REPORTEES'"
			+ " AND emp_detail.pkEmpId != :restrictEmpId ")
	List<DatEmpDetailBO> getBuHeadDetailsByEmpId(@Param("empId") Integer empId,
			@Param("restrictEmpId") Integer restrictEmpId);

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpDetailBO(emp_detail.pkEmpId, emp_personal.empFirstName,"
			+ " emp_personal.empLastName, emp_prof.fkEmpReportingMgrId, emp_prof.hasReporteesFlag) "
			+ " from DatEmpDetailBO emp_detail "
			+ " INNER JOIN DatEmpProfessionalDetailBO emp_prof ON emp_prof.fkMainEmpDetailId = emp_detail.pkEmpId "
			+ " INNER JOIN DatEmpPersonalDetailBO emp_personal ON emp_personal.fkEmpDetailId = emp_detail.pkEmpId "
			+ " WHERE emp_detail.fkEmpMainStatus = 1 " + " AND emp_prof.hasReporteesFlag = 'NO_REPORTEES'"
			+ " AND emp_detail.pkEmpId != ?2 "
			+ " AND emp_prof.fkEmpBuUnit = (SELECT emp_prof1.fkEmpBuUnit FROM DatEmpProfessionalDetailBO emp_prof1 where emp_prof1.fkMainEmpDetailId = ?1) ")
	List<DatEmpDetailBO> getAllEmployeesByEmpId(Integer empId, Integer restrictEmpId);

	// EOA by Smrithi
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpDetailBO(e.pkEmpId)" + " from DatEmpDetailBO e "
			+ " JOIN DatEmpProfessionalDetailBO pro ON pro.fkMainEmpDetailId = e.pkEmpId " + " WHERE e.empType = 1 "
			+ " AND e.fkEmpMainStatus = 1" + " AND e.fkEmpSubStatus = 7 " + " AND pro.empConfirmationGraduateFlag = 1 "
			+ " AND pro.empDateOfConfirmation != null" + " AND pro.empDateOfConfirmation <= ?1 "
			+ " AND e.pkEmpId NOT IN (1,2222,2223,7777,7778,8888,9999,9998,3001,9990,9993,9994,9995,9997)")
	Set<Integer> getAllGSREmployeeList(Date salbEndDate);

	/**
	 * <Description getAllDirectReporteesWithRMIdForRMId:> TODO
	 * 
	 * @return List<DatEmpDetailBO>
	 */

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpDetailBO(emp_detail.pkEmpId, emp_personal.empFirstName,"
			+ " emp_personal.empLastName, emp_prof.fkEmpReportingMgrId, emp_prof.hasReporteesFlag) "
			+ " from DatEmpDetailBO emp_detail "
			+ " INNER JOIN DatEmpProfessionalDetailBO emp_prof ON emp_prof.fkMainEmpDetailId = emp_detail.pkEmpId "
			+ " INNER JOIN DatEmpPersonalDetailBO emp_personal ON emp_personal.fkEmpDetailId = emp_detail.pkEmpId "
			+ " WHERE emp_detail.fkEmpMainStatus = 1 " + " AND emp_prof.empConfirmationGraduateFlag = 1 "
			+ " AND emp_prof.fkEmpDomainMgrId =:rmId AND " + " emp_detail.pkEmpId not in (:empIDList) "
			+ " ORDER BY emp_detail.pkEmpId")
	List<DatEmpDetailBO> getAllDirectReporteesWithDMIdForRMId(@Param("rmId") Integer rmId,
			@Param("empIDList") List<Integer> empIDList);

	/**
	 * <Description getAllDirectReporteesWithRMIdForRMId:> TODO
	 * 
	 * @return List<DatEmpDetailBO>
	 */

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpDetailBO(emp_detail.pkEmpId, emp_personal.empFirstName,"
			+ " emp_personal.empLastName, emp_prof.fkEmpReportingMgrId, emp_prof.hasReporteesFlag) "
			+ " from DatEmpDetailBO emp_detail "
			+ " INNER JOIN DatEmpProfessionalDetailBO emp_prof ON emp_prof.fkMainEmpDetailId = emp_detail.pkEmpId "
			+ " INNER JOIN DatEmpPersonalDetailBO emp_personal ON emp_personal.fkEmpDetailId = emp_detail.pkEmpId "
			+ " WHERE emp_detail.fkEmpMainStatus = 1 " + " AND emp_prof.empConfirmationGraduateFlag = 1 "
			+ " AND emp_prof.fkEmpReportingMgrId =:rmId AND " + " emp_detail.pkEmpId not in (:empIDList) "
			+ " ORDER BY emp_detail.pkEmpId")
	List<DatEmpDetailBO> getDirectReporteesWithDMIdForRMId(@Param("rmId") Integer rmId,
			@Param("empIDList") List<Integer> empIDList);

	@Query(name = "DatEmpDetailBO.getAllDirectReporteesWithRMIdForRMIdOnlyGraduatedForGSR")
	List<MyReporteesBean> getAllDirectReporteesWithRMIdForRMIdOnlyGraduatedForGSR(@Param("rmId") Integer rmId,
			@Param("empIDList") List<Integer> empIDList);

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpDetailBO(emp_detail.pkEmpId, emp_personal.empFirstName,"
			+ " emp_personal.empLastName, emp_prof.fkEmpReportingMgrId, emp_prof.hasReporteesFlag) "
			+ " from DatEmpDetailBO emp_detail "
			+ " INNER JOIN DatEmpProfessionalDetailBO emp_prof ON emp_prof.fkMainEmpDetailId = emp_detail.pkEmpId "
			+ " INNER JOIN DatEmpPersonalDetailBO emp_personal ON emp_personal.fkEmpDetailId = emp_detail.pkEmpId "
			+ " WHERE emp_detail.fkEmpMainStatus = 1 ")
	/*
	 * +
	 * " OR emp_prof.fkEmpReportingMgrId =(SELECT emp_prof1.fkEmpReportingMgrId FROM DatEmpProfessionalDetailBO emp_prof1 where emp_prof1.fkMainEmpDetailId = :empId) )"
	 * )
	 */
	List<DatEmpDetailBO> getEmpInfoByEmpId(@Param("empId") Integer empId);

	// @Query("SELECT new
	// com.thbs.mis.common.bo.DatEmpDetailBO(e.pkEmpId,p.empFirstName,p.empLastName)"
	// + " from DatEmpDetailBO e "
	// + " JOIN DatEmpProfessionalDetailBO p ON p.fkEmpDetailId = e.pkEmpId "
	// + " WHERE e.empType = 1 "
	// + " AND e.fkEmpMainStatus = 1"
	// + " AND e.fkEmpSubStatus = 7 "
	// + " AND p.empConfirmationGraduateFlag =1 "
	// + " p .fkEmpReportingMgrId = ? "
	// + " AND p.empDateOfConfirmation BETWEEN ?2 AND ?3 "
	// + " AND e.pkEmpId NOT IN
	// (1,2222,2223,7777,7778,8888,9999,9998,3001,9990,9993,9994,9995,9997)")
	// List<DatEmpDetailBO> getAllGSREmployeeByRM(Integer rmId,Date StartDate,
	// Date endDate);
	//
	// @Query("SELECT new
	// com.thbs.mis.common.bo.DatEmpDetailBO(e.pkEmpId,p.empFirstName,p.empLastName)"
	// + " from DatEmpDetailBO e "
	// + " JOIN DatEmpProfessionalDetailBO p ON p.fkEmpDetailId = e.pkEmpId "
	// + " WHERE e.empType = 1 "
	// + " AND e.fkEmpMainStatus = 1"
	// + " AND e.fkEmpSubStatus = 7 "
	// + " AND p.empConfirmationGraduateFlag =1 "
	// + " AND p.fkEmpDomainMgrId = ?1"
	// + " AND p.empDateOfConfirmation BETWEEN ?2 AND ?3 "
	// + " AND e.pkEmpId NOT IN
	// (1,2222,2223,7777,7778,8888,9999,9998,3001,9990,9993,9994,9995,9997)")
	// List<DatEmpDetailBO> getAllGSREmployeeByDM(Integer dmId,Date StartDate,
	// Date endDate);
	//
	// @Query("SELECT new
	// com.thbs.mis.common.bo.DatEmpDetailBO(e.pkEmpId,p.empFirstName,p.empLastName)"
	// + " from DatEmpDetailBO e "
	// + " JOIN DatEmpProfessionalDetailBO p ON p.fkEmpDetailId = e.pkEmpId "
	// + " WHERE e.empType = 1 "
	// + " AND e.fkEmpMainStatus = 1"
	// + " AND e.fkEmpSubStatus = 7 "
	// + " AND p.empConfirmationGraduateFlag =1 "
	// + " AND p.fkEmpBuUnit = ?1"
	// + " AND p.empDateOfConfirmation BETWEEN ?2 AND ?3 "
	// + " AND e.pkEmpId NOT IN
	// (1,2222,2223,7777,7778,8888,9999,9998,3001,9990,9993,9994,9995,9997)")
	// List<DatEmpDetailBO> getAllGSREmployeeByBu(Short buId,Date StartDate,
	// Date endDate);

	// Added by pratibha
	@Query("SELECT r from DatEmpDetailBO r " + " JOIN MasEmpRoleBO m on r.fkEmpRoleId = m.pkEmpRoleId "
			+ " WHERE r.pkEmpId = :empId " + " AND m.pkEmpRoleId in (:financeIds)")
	public DatEmpDetailBO getFinanceRoleDetails(@Param("empId") Integer empId,
			@Param("financeIds") List<Short> financeIds);

	@Query("SELECT r from DatEmpDetailBO r " + " JOIN MasEmpRoleBO m on r.fkEmpRoleId = m.pkEmpRoleId "
			+ " WHERE r.pkEmpId = :empId " + " AND m.pkEmpRoleId in (:systemAdminIds)")
	public DatEmpDetailBO getSystemAdminRoleDetails(@Param("empId") Integer empId,
			@Param("systemAdminIds") List<Short> systemAdminIds);

	@Query("SELECT r from DatEmpDetailBO r " + " JOIN MasEmpRoleBO m on r.fkEmpRoleId = m.pkEmpRoleId "
			+ " WHERE r.pkEmpId = :empId " + " AND m.pkEmpRoleId in (:systemAdminRoles)")
	public DatEmpDetailBO getSystemAdminRoleDetailsForAddAsset(@Param("empId") Integer empId,
			@Param("systemAdminRoles") List<Short> systemAdminRoles);

	@Query("SELECT r from DatEmpDetailBO r " + " WHERE r.pkEmpId = :empId " + " AND r.fkEmpMainStatus = 1")
	public DatEmpDetailBO getBuDetails(@Param("empId") Integer empId);

	/*
	 * @Query("SELECT r from DatEmpDetailBO r JOIN DatEmpProfessionalDetailBO p "
	 * + " ON p.fkMainEmpDetailId = r.pkEmpId WHERE p.fkEmpBuUnit = :nbuIt " +
	 * " AND r.fkEmpMainStatus = 1") public List<DatEmpDetailBO>
	 * getSysDetails(@Param("nbuIt") Short nbuIt);
	 */

	@Query("SELECT r from DatEmpDetailBO r " + " JOIN MasEmpRoleBO m on r.fkEmpRoleId = m.pkEmpRoleId "
			+ " AND r.fkEmpMainStatus = 1" + " AND m.pkEmpRoleId in (:systemAdminRolesList)")
	public List<DatEmpDetailBO> getSystemAdminRoleDetails(
			@Param("systemAdminRolesList") List<Short> systemAdminRolesList);
	// EOA pratibha

	// Added by Kamal Anand for Notifications
	public List<DatEmpDetailBO> findByFkEmpRoleId(short roleId);

	@Query("SELECT emp.pkEmpId from DatEmpDetailBO emp where emp.fkEmpRoleId in(:empRoles)")
	public List<Integer> findByFkEmpRoleIds(@Param("empRoles") List<Short> empRoles);
	// End of Addition by Kamal Anand for Notifications

	public DatEmpDetailBO findTop1ByEmpTypeAndPkEmpIdNotInOrderByPkEmpIdDesc(Byte empType, Integer[] empIds);

	// Added by Kamal Anand for User Management
	@Query(name = "UserManagement.advancedSearch")
	public List<AdvancedSearchOutputBean> getAdvancedSearchResults(@Param("empIdList") List<Integer> empIdList);

	public DatEmpDetailBO findByPkEmpIdAndFkEmpRoleIdIn(Integer empId, List<Short> roleIdList);
	// End of Addition by Kamal anand for User Management

	@Query("SELECT r from DatEmpDetailBO r " + " JOIN MasEmpRoleBO m on r.fkEmpRoleId = m.pkEmpRoleId "
			+ " WHERE r.pkEmpId = :empId " + " AND m.empRoleKeyName in (:systemAdminRoles)")
	public DatEmpDetailBO getSystemAdminRoleDetailsForUserManagmentModule(@Param("empId") Integer empId,
			@Param("systemAdminRoles") List<String> systemAdminRoleIdList);

	// Added by Kamal Anand for Notifications
	@Query("SELECT emp.pkEmpId from DatEmpDetailBO emp where emp.fkEmpRoleId in(:empRoles) and emp.fkEmpMainStatus = 1")
	public List<Integer> getActiveEmployeesForRoleId(@Param("empRoles") List<Short> empRoles);
	// End of Addition by Kamal Anand for Notifications

	// Added By Adavayya Hiremath for Project Managment

	@Query("SELECT DISTINCT new com.thbs.mis.common.bo.DatEmpDetailBO("
			+ "dateEmpDetail.pkEmpId,empPersonal.empFirstName,empPersonal.empLastName,empPersonal.empMiddleName) FROM "
			+ "DatEmpDetailBO dateEmpDetail JOIN DatEmpPersonalDetailBO empPersonal ON dateEmpDetail.pkEmpId = empPersonal.fkEmpDetailId JOIN "
			+ "DatEmpProfessionalDetailBO datEmpProfessionalDetail ON dateEmpDetail.pkEmpId = datEmpProfessionalDetail.fkMainEmpDetailId JOIN "
			+ "LevelBO level ON datEmpProfessionalDetail.fkEmpLevel = level.pkEmplevelLevelid"
			+ " WHERE level.empLevelName in (:empLevelNameList) AND datEmpProfessionalDetail.fkEmpBuUnit = :buId AND dateEmpDetail.fkEmpMainStatus = 1")
	List<DatEmpDetailBO> getAllMgrDetails(@Param("empLevelNameList") List<String> empLevelNameList, @Param("buId") Short buId);

	// End Of Addition By Adavayya Hiremath for project Management
	
	//Added by Balaji 
	@Query(" SELECT empDetail from DatEmpDetailBO empDetail where empDetail.fkEmpRoleId IN "
			+ " ( SELECT empRole.pkEmpRoleId from MasEmpRoleBO empRole where empRole.empRoleKeyName IN "
			+ "  :financeRoleKeyNames  ) "
			+ " AND empDetail.pkEmpId= :empId " )
	List<DatEmpDetailBO> getAllFinanceRolesByEmpId(@Param ("empId") Integer empId , @Param ("financeRoleKeyNames") List<String> financeRoleKeyNames);
	//EOA by Balaji 
	
	@Query("select empDetail from DatEmpDetailBO empDetail where empDetail.pkEmpId in (:empList)")
	public List<DatEmpDetailBO> getEmpList(@Param("empList") Set<Integer> empList);

	// Added by Rinta
	@Query(" SELECT empDetail.pkEmpId from DatEmpDetailBO empDetail"
			+ " JOIN DatEmpProfessionalDetailBO empProf ON empDetail.pkEmpId = empProf.fkMainEmpDetailId"
			+ " JOIN MasEmpRoleBO empRole ON empDetail.fkEmpRoleId = empRole.pkEmpRoleId"
			+ " where empRole.empRoleKeyName IN (:roleKeyNames)"
			+ " AND empDetail.fkEmpMainStatus = 1"
			+ " AND empProf.fkEmpOrganizationId =:orgId")
	List<Integer> getAllEmpIdByRoleKeyNames(@Param("roleKeyNames") List<String> roleKeyNames, @Param("orgId") Short orgId);
	// EOA by Rinta
}
