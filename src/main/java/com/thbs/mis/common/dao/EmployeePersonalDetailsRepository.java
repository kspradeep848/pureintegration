/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmployeePersonalDetailsRepository.java            */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the Employee Personal Details Repository */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016    THBS     1.0      Initial version created   	 	 */
/*********************************************************************/
package com.thbs.mis.common.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;

@Repository
public interface EmployeePersonalDetailsRepository extends GenericRepository<DatEmpPersonalDetailBO, Long>,JpaSpecificationExecutor<DatEmpPersonalDetailBO> {
	
	public DatEmpPersonalDetailBO findByFkEmpDetailId(Integer FkEmpDetailId);

	@Query("select new com.thbs.mis.common.bo.DatEmpPersonalDetailBO "
			+ "(emp.fkEmpDetailId, emp.empFirstName, emp.empLastName ,des.empDesignationName)"
			+ " FROM DatEmpPersonalDetailBO emp " + " INNER JOIN DatEmpProfessionalDetailBO pro "
			+ " ON pro.fkMainEmpDetailId = emp.fkEmpDetailId"
			+ " INNER JOIN MasEmpDesignationBO des ON des.pkEmpDesignationId = pro.fkEmpDesignation "
			+ " where emp.fkEmpDetailId IN (:FkEmpDetailId) " + " AND emp.fkEmpDetailId not in (:empIDList)")
	public List<DatEmpPersonalDetailBO> getBuHeadDetails(@Param("FkEmpDetailId") List<Integer> FkEmpDetailId,
			@Param("empIDList") List<Integer> empIDList);

	public DatEmpPersonalDetailBO findByPkEmpPersonalDetailId(Integer pkEmpPersonalDetailId);

	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO (personal.fkEmpDetailId, personal.empFirstName,personal.empLastName) from DatEmpPersonalDetailBO personal join DatEmpDetailBO empdetail on personal.fkEmpDetailId=empdetail.pkEmpId where empdetail.fkEmpRoleId in:roles and empdetail.fkEmpMainStatus = 1")
	public List<DatEmpPersonalDetailBO> findByEmpRoles(@Param("roles") List<Short> roles);

	
	//Added by Balaji
		@Query(" SELECT  new com.thbs.mis.common.bo.DatEmpPersonalDetailBO "
				+ " (personal.empFirstName , personal.empLastName , personal.fkEmpDetailId) "
				+ " FROM DatEmpPersonalDetailBO personal "
				+ " join  DatEmpProfessionalDetailBO prof on personal.fkEmpDetailId  = prof.fkMainEmpDetailId "
				+ " join DatEmpDetailBO detail on prof.fkMainEmpDetailId = detail.pkEmpId "
				+ " where  prof.fkEmpBuUnit = :buId and  detail.fkEmpRoleId = 5")
		public List<DatEmpPersonalDetailBO> getDmDetails(@Param("buId") Short buId);
	//EOA by Balaji
		
	//Added by Kamal Anand for User Management 
	@Query(nativeQuery = true,value = "SELECT * FROM dat_emp_personal_detail where "
			+ "if(emp_middle_name is not null,concat(emp_first_name,' ',emp_middle_name,' ',emp_last_name),concat(emp_first_name, emp_last_name)) like %:empName% "
			+ "or concat(emp_first_name,' ', emp_last_name) like %:empName% "
			+ "or emp_first_name like %:empName% "
			+ "or emp_last_name like %:empName% or "
			+ "emp_middle_name like %:empName% and fk_emp_detail_id in (:empIdList)")
	public List<DatEmpPersonalDetailBO> getEmployeeByEmpName(@Param("empName") String empName,@Param("empIdList") List<Integer> empIdList);
	//End of Addition by Kamal Anand for User Management


	// Added by Adavayya Hiremath for UserManagement module
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO( "
			+ " emp_per.empFirstName,emp_per.empMiddleName,emp_per.empLastName ) FROM "
			+ " DatEmpPersonalDetailBO emp_per INNER JOIN DatEmpProfessionalDetailBO emp_pro  ON "
			+ " emp_per.fkEmpDetailId = emp_pro.fkMainEmpDetailId WHERE emp_per.fkEmpDetailId = :fkEmpReportingMgrId ")
	public List<DatEmpPersonalDetailBO> findEmpDetailsByFkEmpReportingMgrId(
			@Param("fkEmpReportingMgrId") int fkEmpReportingMgrId);
	// EOA by Adavayya Hiremath for UserManagement module

	public List<DatEmpPersonalDetailBO> findByEmpPersonalEmailId(String personalEmail);
	
	@Query("SELECT personal.empPersonalEmailId from DatEmpPersonalDetailBO personal where personal.fkEmpDetailId = :empId and personal.empPersonalEmailId = :mailId")
	public List<DatEmpPersonalDetailBO> checkForDuplicateEmailIdOfSameEmployee(@Param("empId") int empId,@Param("mailId") String mailId);
	
	//Added By Rinta Mariam Jose
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO (personal.fkEmpDetailId, personal.empFirstName,personal.empLastName) from DatEmpPersonalDetailBO personal join DatEmpDetailBO empdetail on personal.fkEmpDetailId=empdetail.pkEmpId where personal.fkEmpDetailId =:empId")
	public DatEmpPersonalDetailBO getEmpNameByFkEmpDetailId(@Param("empId")Integer empId);
	
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpPersonalDetailBO (personal.fkEmpDetailId, personal.empFirstName,personal.empLastName) from DatEmpPersonalDetailBO personal join DatEmpDetailBO empdetail on personal.fkEmpDetailId=empdetail.pkEmpId where personal.fkEmpDetailId in:empId")
	public List<DatEmpPersonalDetailBO> getEmpNamesByFkEmpDetailIdIn(@Param("empId") Set<Integer> empId);
	// EOA by Rinta Mariam Jose
	
	// Added by Deepa
	@Query(" SELECT emp_personal from DatEmpPersonalDetailBO emp_personal"
			+ " JOIN DatEmpDetailBO empDetail ON empDetail.pkEmpId=emp_personal.fkEmpDetailId"
			+ " JOIN MasEmpRoleBO empRole ON empRole.pkEmpRoleId = empDetail.fkEmpRoleId"
			+ " WHERE empDetail.fkEmpMainStatus=1 AND empRole.empRoleKeyName IN :projectCordinatorRoleKeyNames")
	List<DatEmpPersonalDetailBO> getAllProjectCordinator(
			@Param("projectCordinatorRoleKeyNames") List<String> projectCordinatorRoleKeyNames);
	
	// Added by Deepa
		@Query(" SELECT emp_personal from DatEmpPersonalDetailBO emp_personal"
				+ " JOIN DatEmpDetailBO empDetail ON empDetail.pkEmpId=emp_personal.fkEmpDetailId"
				+ " JOIN MasEmpRoleBO empRole ON empRole.pkEmpRoleId = empDetail.fkEmpRoleId"
				+ " WHERE empDetail.fkEmpMainStatus=1 AND empRole.empRoleKeyName IN :projectUserAccessRoleKeyNames")
		List<DatEmpPersonalDetailBO> getAllProjectUserAccess(
				@Param("projectUserAccessRoleKeyNames") List<String> projectUserAccessRoleKeyNames);
		
	// EOA by Deepa
}
