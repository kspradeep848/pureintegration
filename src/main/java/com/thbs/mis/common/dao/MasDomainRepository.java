package com.thbs.mis.common.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.MasDomainBO;

@Repository
public interface MasDomainRepository extends
		GenericRepository<MasDomainBO, Integer> {
	
	public MasDomainBO findByPkDomainId(Short domainId);
}
