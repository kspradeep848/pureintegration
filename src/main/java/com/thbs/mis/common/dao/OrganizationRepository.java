/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  OrganizationRepository.java                       */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  15-Nov-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*   Date         Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 15-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.common.dao;

import com.thbs.mis.common.bo.MasOrganizationBO;

/**
 * <Description OrganizationRepository:> TODO
 * @author THBS
 * @version 1.0
 * @see 
 */
public interface OrganizationRepository extends GenericRepository<MasOrganizationBO, Integer>
{
	

}
