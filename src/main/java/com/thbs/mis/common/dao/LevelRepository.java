/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  LevelRepository.java                                		 */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  27-Jun-2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 27-Jun-2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.common.dao;

import com.thbs.mis.common.bo.MasEmpLevelBO;

/**
 * <Description LevelRepository:> TODO
 * @author THBS
 * @version 1.0
 * @see 
 */
public interface LevelRepository extends GenericRepository<MasEmpLevelBO,Integer>
{
	
	//Added by Kamal Anand for User Management
	public MasEmpLevelBO findByPkEmpLevelId(short levelId);
	//End of Addition by Kamal Anand for User Management
	
	
}
