/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  CountryRepository.java                            */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  15-Nov-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 15-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.common.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.bo.MasCountryBO;


/**
 * <Description CountryRepository:> TODO
 * @author THBS
 * @version 1.0
 * @see 
 */
public interface CountryRepository  extends GenericRepository<MasCountryBO, Integer>
{
  //Added by Kamal Anand for User Management
	public MasCountryBO findByPkCountryId(short countryId);
  //End of Addition by Kamal Anand for User Management
	
	@Query("Select country from MasCountryBO country where lower(country.countryName) like lower(:countryName)")
	public MasCountryBO getByCountryName(@Param("countryName") String countryName);
}
