package com.thbs.mis.common.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.bo.MasCurrencytypeBO;


public interface MasCurrencyTypeRepository  extends GenericRepository<MasCurrencytypeBO, Integer>{
	 
	List<MasCurrencytypeBO> findAll();
	
	@Query("SELECT  rep " + " FROM MasCurrencytypeBO  rep WHERE rep.pk_CurrencyType_ID=:currencyId")
	public MasCurrencytypeBO getByCurrencyId(@Param("currencyId") Short currencyId);
}
