/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  MisMenuRepository.java                            */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  15-Nov-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 28-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/ 
package com.thbs.mis.common.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.MasMenuBO;

@Repository
public interface MisMenuRepository extends GenericRepository<MasMenuBO, Long>{
	
	
	/**
	 * <Description getAllMenuListForRoleId:> 
	 * @param Integer roleId
	 * @return List<MasMenuBO>
	 */
	
	@Query("SELECT m from MasMenuBO m "
			+ " INNER JOIN MasRoleMenuMappingBO r ON r.fkMenuId = m.pkMenuId "
			+ " WHERE m.activeStatus like 'ACTIVE' AND r.fkRoleId =? ORDER BY m.menuOrderId")
	List<MasMenuBO> getAllMenuListForRoleId(Integer roleId);
	
	
	/**
	 * <Description getAllSubMenuListForParentMenuId:> 
	 * @param Integer menuId
	 * @return List<MasMenuBO>
	 */
	
	@Query("SELECT m from MasMenuBO m WHERE m.activeStatus like 'ACTIVE' AND m.menuParentId =? ORDER BY m.menuOrderId")
	List<MasMenuBO> getAllSubMenuListForParentMenuId(Integer menuId);
	
	
	/**
	 * 
	 * <Description getAllMenuListForEmployee:> Retrieves all the menu for the employee 
	 * @param empID : Employee Id
	 * @return List of menus for which employee has access
	 */
	@Query("SELECT m from MasMenuBO m "
			+ " INNER JOIN MasRoleMenuMappingBO r ON r.fkMenuId = m.pkMenuId "
			+ " WHERE m.activeStatus like 'ACTIVE' AND r.fkRoleId = (SELECT empDetail.fkEmpRoleId from DatEmpDetailBO empDetail where empDetail.pkEmpId=:empID) ORDER BY m.menuOrderId")
	List<MasMenuBO> getAllMenuListForEmployee(@Param("empID")Integer empID);
	
	
	
	
	
	/*@Query(nativeQuery=true, value="SELECT * from mas_menu m  INNER JOIN mas_role_menu_mapping r ON r.fk_menu_id = m.pk_menu_id  "
			+ " WHERE m.active_status like 'ACTIVE'  AND m.menu_parent_id=:parentMenuID AND "
			+ "r.fk_role_id = (SELECT empDetail.fk_emp_role_id from dat_emp_detail empDetail where empDetail.pk_emp_id=:empId) "

			+ "UNION "
+ " (SELECT * from mas_menu m  INNER JOIN mas_role_menu_mapping r ON r.fk_menu_id = m.pk_menu_id  "
+ " WHERE m.active_status like 'ACTIVE'  AND m.menu_parent_id=:parentMenuID  AND (is_reporting_mgr_menu = (select CASE empDetail.has_reportees_flag "
       + " WHEN 'YES_HAS_REPORTEES' THEN 'YES' "
       + " WHEN 'NO_REPORTEES' THEN 'NO' "
   + " END from dat_emp_professional_detail empDetail where empDetail.fk_main_emp_detail_id=:empId ))) "
+ "UNION "
+ "(SELECT * from mas_menu m  INNER JOIN mas_role_menu_mapping r ON r.fk_menu_id = m.pk_menu_id  "
+ " WHERE m.active_status like 'ACTIVE'  AND m.menu_parent_id=:parentMenuID  AND "
+ " r.fk_role_id = 1 AND (is_reporting_mgr_menu <> 'YES')) ORDER BY mas_menu_order_id") */
	
	@Query(nativeQuery=true, value="SELECT * from mas_menu m  INNER JOIN mas_role_menu_mapping r ON r.fk_menu_id = m.pk_menu_id "
			+ " WHERE m.active_status like 'ACTIVE'  AND m.menu_parent_id=:parentMenuID AND"
			+ " r.fk_role_id = (SELECT IF((empDetail.fk_emp_role_id=1),NULL,empDetail.fk_emp_role_id) AS emp_role from dat_emp_detail empDetail where empDetail.pk_emp_id=:empId)"
			+ " UNION "
			+ " (SELECT * from mas_menu m  INNER JOIN mas_role_menu_mapping r ON r.fk_menu_id = m.pk_menu_id "
			+ " WHERE m.active_status like 'ACTIVE'  AND m.menu_parent_id=:parentMenuID AND (is_reporting_mgr_menu = (select CASE empDetail.has_reportees_flag "
			+ " WHEN 'YES_HAS_REPORTEES' THEN 'YES' "
			+ " WHEN 'NO_REPORTEES' THEN 'NO' "
			+ " END from dat_emp_professional_detail empDetail where empDetail.fk_main_emp_detail_id=:empId )) "
			+ " AND r.fk_role_id = (SELECT IF((empDetail.fk_emp_role_id=1),NULL,empDetail.fk_emp_role_id) AS emp_role from dat_emp_detail empDetail where empDetail.pk_emp_id=:empId))"
			+ " UNION "
			+ " (SELECT * from mas_menu m  INNER JOIN mas_role_menu_mapping r ON r.fk_menu_id = m.pk_menu_id "
			+ " WHERE m.active_status like 'ACTIVE'  AND m.menu_parent_id=:parentMenuID AND"
			+ " r.fk_role_id = 1 AND is_reporting_mgr_menu <> 'YES')"
			+ " UNION "
			+ " (SELECT * from mas_menu m  INNER JOIN mas_role_menu_mapping r ON r.fk_menu_id = m.pk_menu_id "
			+ " WHERE m.active_status like 'ACTIVE'  AND m.menu_parent_id=:parentMenuID AND"
			+ " r.fk_role_id = 1 AND (is_reporting_mgr_menu = (select CASE empDetail.has_reportees_flag "
			+ " WHEN 'YES_HAS_REPORTEES' THEN 'YES' "
			+ " WHEN 'NO_REPORTEES' THEN 'NO' "
			+ " END from dat_emp_professional_detail empDetail where empDetail.fk_main_emp_detail_id=:empId)))"
			+ " ORDER BY mas_menu_order_id")

	List<MasMenuBO> getAllSubMenuListForEmployeeForParentMenuId(@Param("empId")Integer empID,@Param("parentMenuID")Integer parentMenuID);
	
	
	
	@Query(nativeQuery=true,value="SELECT * from mas_menu m  INNER JOIN mas_role_menu_mapping r ON r.fk_menu_id = m.pk_menu_id  "
			+ " WHERE m.active_status like 'ACTIVE'  AND m.menu_parent_id=0 AND m.is_parent=1 AND "
			+ "r.fk_role_id = (SELECT empDetail.fk_emp_role_id from dat_emp_detail empDetail where empDetail.pk_emp_id=:empId) "

			+ "UNION "
+ " (SELECT * from mas_menu m  INNER JOIN mas_role_menu_mapping r ON r.fk_menu_id = m.pk_menu_id  "
+ " WHERE m.active_status like 'ACTIVE'  AND m.menu_parent_id=0 AND m.is_parent=1  AND (is_reporting_mgr_menu = (select CASE empDetail.has_reportees_flag "
       + " WHEN 'YES_HAS_REPORTEES' THEN 'YES' "
       + " WHEN 'NO_REPORTEES' THEN 'NO' "
   + " END from dat_emp_professional_detail empDetail where empDetail.fk_main_emp_detail_id=:empId ))) "
+ "UNION "
+ "(SELECT * from mas_menu m  INNER JOIN mas_role_menu_mapping r ON r.fk_menu_id = m.pk_menu_id  "
+ " WHERE m.active_status like 'ACTIVE'  AND m.menu_parent_id=0 AND m.is_parent=1  AND "
+ " r.fk_role_id = 1 AND (is_reporting_mgr_menu <> 'YES')) ORDER BY mas_menu_order_id") 
	List<MasMenuBO> getAllTopLevelMenusForEmployee(@Param("empId")Integer empID); 

}
