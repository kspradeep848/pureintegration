package com.thbs.mis.common.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.MasBankPaymentDetailBO;

@Repository
public interface MasBankPaymentDetailRepository extends GenericRepository<MasBankPaymentDetailBO, Integer>
{

	public List<MasBankPaymentDetailBO> findByFkEntityId(byte entityId);
}
