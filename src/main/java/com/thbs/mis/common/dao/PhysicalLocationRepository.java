/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  PhyLocationRepository.java                        */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  14-Nov-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 14-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.common.dao;

import java.util.List;

import com.thbs.mis.common.bo.MasLocationPhysicalBO;

/**
 * <Description PhyLocationRepository:> TODO
 * @author THBS
 * @version 1.0
 * @see 
 */
public interface PhysicalLocationRepository extends GenericRepository<MasLocationPhysicalBO, Integer>
{
	MasLocationPhysicalBO findByPkLocationPhysicalId(short pkLocationPhysicalId);
	
	List<MasLocationPhysicalBO> findByFkLocationParentId(Short parentLocId);
}
