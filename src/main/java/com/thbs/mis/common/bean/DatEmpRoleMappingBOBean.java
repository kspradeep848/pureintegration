package com.thbs.mis.common.bean;

public class DatEmpRoleMappingBOBean {
	
	private Short employeeRoleId;
	private String roleName;
	
	public Short getEmployeeRoleId() {
		return employeeRoleId;
	}
	public void setEmployeeRoleId(Short employeeRoleId) {
		this.employeeRoleId = employeeRoleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	@Override
	public String toString() {
		return "DatEmpRoleMappingBOBean [employeeRoleId=" + employeeRoleId
				+ ", roleName=" + roleName + "]";
	}
	
	
	
}
