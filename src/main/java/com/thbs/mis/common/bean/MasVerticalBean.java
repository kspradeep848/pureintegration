package com.thbs.mis.common.bean;

public class MasVerticalBean {
	
	private short verticalId;

	private String verticalName;

	public short getVerticalId() {
		return verticalId;
	}

	public void setVerticalId(short verticalId) {
		this.verticalId = verticalId;
	}

	public String getVerticalName() {
		return verticalName;
	}

	public void setVerticalName(String verticalName) {
		this.verticalName = verticalName;
	}
	
	

	@Override
	public String toString() {
		return "MasVerticalBO [verticalId=" + verticalId
				+ ", verticalName=" + verticalName + "]";
	}
	

}
