package com.thbs.mis.common.bean;

public class DomainOutputBean {

	private Short domainId;
	private String domainName;

	public Short getDomainId() {
		return domainId;
	}

	public void setDomainId(Short domainId) {
		this.domainId = domainId;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	@Override
	public String toString() {
		return "DomainOutputBean [domainId=" + domainId + ", domainName=" + domainName + "]";
	}

}
