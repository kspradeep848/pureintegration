package com.thbs.mis.common.bean;

import java.util.Comparator;

public class AcountsManagerOutputBean implements Comparator<AcountsManagerOutputBean> {

	private Integer accountManagerId;
	private String accountManagerName;

	public Integer getAccountManagerId() {
		return accountManagerId;
	}

	public void setAccountManagerId(Integer accountManagerId) {
		this.accountManagerId = accountManagerId;
	}

	public String getAccountManagerName() {
		return accountManagerName;
	}

	public void setAccountManagerName(String accountManagerName) {
		this.accountManagerName = accountManagerName;
	}

	@Override
	public String toString() {
		return "AcountsManagerOutputBean [accountManagerId=" + accountManagerId + ", accountManagerName=" + accountManagerName + "]";
	}

	@Override
	public int compare(AcountsManagerOutputBean bean, AcountsManagerOutputBean bean1) {
		// TODO Auto-generated method stub
		return bean.getAccountManagerName().compareTo(bean1.getAccountManagerName());
	}

}
