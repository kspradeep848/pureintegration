/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmployeeLevelBean.java                            */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  20-Dec-2016                                       */
/*                                                                   */
/*  Description :  This bean related to Employee Level. This bean 	 */
/* 					have level id and level name. 			 		 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 20-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.common.bean;


public class EmployeeLevelBean {
	
	private short levelId;
	
	private String levelName;
	
	public short getLevelId() {
		return levelId;
	}
	public void setLevelId(short levelId) {
		this.levelId = levelId;
	}
	public String getLevelName() {
		return levelName;
	}
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	
	@Override
	public String toString() {
		return "EmployeeLevelBean [levelId=" + levelId + ", levelName="
				+ levelName + "]";
	}
	
}
