package com.thbs.mis.common.bean;

public class LocationByCountyBean 
{
	private int locationId;
	private String LocationName;
	
	public int getLocationId() {
		return locationId;
	}
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	public String getLocationName() {
		return LocationName;
	}
	public void setLocationName(String locationName) {
		LocationName = locationName;
	}
	@Override
	public String toString() {
		return "LocationByCountyBean [locationId=" + locationId + ", LocationName=" + LocationName + "]";
	}
	
}
