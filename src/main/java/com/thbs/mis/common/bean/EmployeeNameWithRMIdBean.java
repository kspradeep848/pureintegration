/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmployeeNameWithRMIdBean.java                     */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  19-Dec-2016                                       */
/*                                                                   */
/*  Description :  This bean have employee id, name, manager id      */
/* 					and reportees flag . 		 					 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 13-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.common.bean;



public class EmployeeNameWithRMIdBean {
	
	private int empId;
	
	private String empFirstName;
	
	private String empLastName;
	
	private int mgrId;
	
	private String reporteeFlag;

	private String empFullNameWithId;
	
	private String designation;
	
	//Added by Kamal Anand
	public EmployeeNameWithRMIdBean(int empId, String empFirstName,
			String empLastName, int mgrId, String reporteeFlag,
			String empFullNameWithId, String designation) {  
		super();
		this.empId = empId;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.mgrId = mgrId;
		this.reporteeFlag = reporteeFlag;
		this.empFullNameWithId = empFullNameWithId;
		this.designation = designation;    
	}
	
	public EmployeeNameWithRMIdBean() {
		super();
	}
	//End of Addition by Kamal Anand
	
	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpFirstName() {
		return empFirstName;
	}

	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}

	public String getEmpLastName() {
		return empLastName;
	}

	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}

	public int getMgrId() {
		return mgrId;
	}

	public void setMgrId(int mgrId) {
		this.mgrId = mgrId;
	}

	public String getReporteeFlag() {
		return reporteeFlag;
	}

	public void setReporteeFlag(String reporteeFlag) {
		this.reporteeFlag = reporteeFlag;
	}

	public String getEmpFullNameWithId() {
		return empFullNameWithId;
	}

	public void setEmpFullNameWithId(String empFullNameWithId) {
		this.empFullNameWithId = empFullNameWithId;
	}

	@Override
	public String toString() {
		return "EmployeeNameWithRMIdBean [empId=" + empId + ", empFirstName="
				+ empFirstName + ", empLastName=" + empLastName + ", mgrId="
				+ mgrId + ", reporteeFlag=" + reporteeFlag
				+ ", empFullNameWithId=" + empFullNameWithId + "]";
	}	

}
