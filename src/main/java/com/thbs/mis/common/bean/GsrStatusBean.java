package com.thbs.mis.common.bean;

public class GsrStatusBean {

	private String empSelfGoalClosed;
	private String reporteesGoalClosed;
	private String empLegacySelfGoalClosed;
	private String reporteesLegacyGoalClosed;

	
	public String getEmpLegacySelfGoalClosed() {
		return empLegacySelfGoalClosed;
	}
	public void setEmpLegacySelfGoalClosed(String empLegacySelfGoalClosed) {
		this.empLegacySelfGoalClosed = empLegacySelfGoalClosed;
	}
	public String getReporteesLegacyGoalClosed() {
		return reporteesLegacyGoalClosed;
	}
	public void setReporteesLegacyGoalClosed(String reporteesLegacyGoalClosed) {
		this.reporteesLegacyGoalClosed = reporteesLegacyGoalClosed;
	}
	public String getEmpSelfGoalClosed() {
		return empSelfGoalClosed;
	}
	public void setEmpSelfGoalClosed(String empSelfGoalClosed) {
		this.empSelfGoalClosed = empSelfGoalClosed;
	}
	public String getReporteesGoalClosed() {
		return reporteesGoalClosed;
	}
	public void setReporteesGoalClosed(String reporteesGoalClosed) {
		this.reporteesGoalClosed = reporteesGoalClosed;
	}
	@Override
	public String toString() {
		return "GsrStatusBean [empSelfGoalClosed=" + empSelfGoalClosed
				+ ", reporteesGoalClosed=" + reporteesGoalClosed
				+ ", empLegacySelfGoalClosed=" + empLegacySelfGoalClosed
				+ ", reporteesLegacyGoalClosed=" + reporteesLegacyGoalClosed
				+ "]";
	}

	
	
	
}
