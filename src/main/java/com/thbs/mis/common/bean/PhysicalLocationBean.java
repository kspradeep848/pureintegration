/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  PhysicalLocationBean.java                         */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  23-Dec-2016                                       */
/*                                                                   */
/*  Description :  This bean related to Physical Location. This bean */
/* 					have physical location id, physical location     */  
/* 					name and parent location id.      		 		 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 23-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.common.bean;

public class PhysicalLocationBean {
	
	private short physicalLocId;
	private short parentlocId;
	private String physicalLocName;
	
	public short getPhysicalLocId() {
		return physicalLocId;
	}
	public void setPhysicalLocId(short physicalLocId) {
		this.physicalLocId = physicalLocId;
	}
	public short getParentlocId() {
		return parentlocId;
	}
	public void setParentlocId(short parentlocId) {
		this.parentlocId = parentlocId;
	}
	public String getPhysicalLocName() {
		return physicalLocName;
	}
	public void setPhysicalLocName(String physicalLocName) {
		this.physicalLocName = physicalLocName;
	}
	
	@Override
	public String toString() {
		return "PhysicalLocationBean [physicalLocId=" + physicalLocId
				+ ", parentlocId=" + parentlocId + ", physicalLocName="
				+ physicalLocName + "]";
	}
	
	
}
