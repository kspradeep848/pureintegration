package com.thbs.mis.common.bean;

public class ModuleMappingOutputBean {
	
	private Integer fkEmployeeId;
	private Short firstModuleId;
	private String firstModuleName;
	private Short secondModuleId;
	private String secondModuleName;
	private Short thirdModuleId;
	private String thirdModuleName;
	private Short fourthModuleId;
	private String fourthModuleName;
	private Short fifthModuleId;
	private String fifthModuleName;
	
	public Integer getFkEmployeeId() {
		return fkEmployeeId;
	}
	public void setFkEmployeeId(Integer fkEmployeeId) {
		this.fkEmployeeId = fkEmployeeId;
	}
	public Short getFirstModuleId() {
		return firstModuleId;
	}
	public void setFirstModuleId(Short firstModuleId) {
		this.firstModuleId = firstModuleId;
	}
	public String getFirstModuleName() {
		return firstModuleName;
	}
	public void setFirstModuleName(String firstModuleName) {
		this.firstModuleName = firstModuleName;
	}
	public Short getSecondModuleId() {
		return secondModuleId;
	}
	public void setSecondModuleId(Short secondModuleId) {
		this.secondModuleId = secondModuleId;
	}
	public String getSecondModuleName() {
		return secondModuleName;
	}
	public void setSecondModuleName(String secondModuleName) {
		this.secondModuleName = secondModuleName;
	}
	public Short getThirdModuleId() {
		return thirdModuleId;
	}
	public void setThirdModuleId(Short thirdModuleId) {
		this.thirdModuleId = thirdModuleId;
	}
	public String getThirdModuleName() {
		return thirdModuleName;
	}
	public void setThirdModuleName(String thirdModuleName) {
		this.thirdModuleName = thirdModuleName;
	}
	public Short getFourthModuleId() {
		return fourthModuleId;
	}
	public void setFourthModuleId(Short fourthModuleId) {
		this.fourthModuleId = fourthModuleId;
	}
	public String getFourthModuleName() {
		return fourthModuleName;
	}
	public void setFourthModuleName(String fourthModuleName) {
		this.fourthModuleName = fourthModuleName;
	}
	public Short getFifthModuleId() {
		return fifthModuleId;
	}
	public void setFifthModuleId(Short fifthModuleId) {
		this.fifthModuleId = fifthModuleId;
	}
	public String getFifthModuleName() {
		return fifthModuleName;
	}
	public void setFifthModuleName(String fifthModuleName) {
		this.fifthModuleName = fifthModuleName;
	}
	@Override
	public String toString() {
		return "ModuleMappingOutputBean [fkEmployeeId=" + fkEmployeeId
				+ ", firstModuleId=" + firstModuleId + ", firstModuleName="
				+ firstModuleName + ", secondModuleId=" + secondModuleId
				+ ", secondModuleName=" + secondModuleName + ", thirdModuleId="
				+ thirdModuleId + ", thirdModuleName=" + thirdModuleName
				+ ", fourthModuleId=" + fourthModuleId + ", forthModuleName="
				+ fourthModuleName + ", fifthModuleId=" + fifthModuleId
				+ ", fifthModuleName=" + fifthModuleName + "]";
	}
	
}	