/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  ParentLocationBean.java                           */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  23-Dec-2016                                       */
/*                                                                   */
/*  Description :  This bean related to parent location. This bean   */
/* 					have parent location id, country id and parent   */  
/* 					location name.      		 		 			 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 23-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.common.bean;

public class ParentLocationBean {
	
	private short parentLocationId;
	private short countryId;
	private String parentLocationName;
	
	public short getParentLocationId() {
		return parentLocationId;
	}
	public void setParentLocationId(short parentLocationId) {
		this.parentLocationId = parentLocationId;
	}
	public short getCountryId() {
		return countryId;
	}
	public void setCountryId(short countryId) {
		this.countryId = countryId;
	}
	public String getParentLocationName() {
		return parentLocationName;
	}
	public void setParentLocationName(String parentLocationName) {
		this.parentLocationName = parentLocationName;
	}
	
	@Override
	public String toString() {
		return "ParentLocationBean [parentLocationId=" + parentLocationId
				+ ", countryId=" + countryId + ", parentLocationName="
				+ parentLocationName + "]";
	}

	
}
