package com.thbs.mis.common.bean;

public class GetAllCurrencyTypeOutputBean {
	
	private Short pkCurrencyTypeID;
	private String currencyTypeCode;
	private String currencyTypeDesc;
	
	public Short getPkCurrencyTypeID() {
		return pkCurrencyTypeID;
	}
	public void setPkCurrencyTypeID(Short pkCurrencyTypeID) {
		this.pkCurrencyTypeID = pkCurrencyTypeID;
	}
	public String getCurrencyTypeCode() {
		return currencyTypeCode;
	}
	public void setCurrencyTypeCode(String currencyTypeCode) {
		this.currencyTypeCode = currencyTypeCode;
	}
	public String getCurrencyTypeDesc() {
		return currencyTypeDesc;
	}
	public void setCurrencyTypeDesc(String currencyTypeDesc) {
		this.currencyTypeDesc = currencyTypeDesc;
	}
	@Override
	public String toString() {
		return "GetAllCurrencyTypeOutputBean [pkCurrencyTypeID="
				+ pkCurrencyTypeID + ", currencyTypeCode=" + currencyTypeCode
				+ ", currencyTypeDesc=" + currencyTypeDesc + "]";
	}
}
