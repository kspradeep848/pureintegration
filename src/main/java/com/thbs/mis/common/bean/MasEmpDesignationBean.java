package com.thbs.mis.common.bean;

import java.math.BigDecimal;

public class MasEmpDesignationBean {
	
	private short designationId;
	
	private String designationName;
	
	private BigDecimal standartRate;

	public short getDesignationId() {
		return designationId;
	}

	public void setDesignationId(short designationId) {
		this.designationId = designationId;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public BigDecimal getStandartRate() {
		return standartRate;
	}

	public void setStandartRate(BigDecimal standartRate) {
		this.standartRate = standartRate;
	}

	@Override
	public String toString() {
		return "MasEmpDesignationBean [designationId=" + designationId
				+ ", designationName=" + designationName + ", standartRate="
				+ standartRate + "]";
	}
}
