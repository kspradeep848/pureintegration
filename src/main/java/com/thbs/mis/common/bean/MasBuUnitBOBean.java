package com.thbs.mis.common.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class MasBuUnitBOBean  {
  
	
	@Min(1)
	@NotNull(message="bu id cannot be null")
	private short pkBuUnitId;
	private String activeFlag;
	private String buUnitName;
	private String comments;
	private Integer fkBuHeadEmpId;
	
	
	
	
	
	
	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getBuUnitName() {
		return buUnitName;
	}

	public void setBuUnitName(String buUnitName) {
		this.buUnitName = buUnitName;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getFkBuHeadEmpId() {
		return fkBuHeadEmpId;
	}

	public void setFkBuHeadEmpId(Integer fkBuHeadEmpId) {
		this.fkBuHeadEmpId = fkBuHeadEmpId;
	}

	public Short getPkBuUnitId() {
		return pkBuUnitId;
	}

	public void setPkBuUnitId(Short pkBuUnitId) {
		this.pkBuUnitId = pkBuUnitId;
	}

	@Override
	public String toString() {
		return "MasBuUnitBOBean [pkBuUnitId=" + pkBuUnitId + ", activeFlag="
				+ activeFlag + ", buUnitName=" + buUnitName + ", comments="
				+ comments + ", fkBuHeadEmpId=" + fkBuHeadEmpId + "]";
	}

		
	
	
	
}
