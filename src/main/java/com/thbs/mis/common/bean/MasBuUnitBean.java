package com.thbs.mis.common.bean;

public class MasBuUnitBean {
	private short buUnitId;
	private String buUnitName;
	public short getBuUnitId() {
		return buUnitId;
	}
	public void setBuUnitId(short buUnitId) {
		this.buUnitId = buUnitId;
	}
	public String getBuUnitName() {
		return buUnitName;
	}
	public void setBuUnitName(String buUnitName) {
		this.buUnitName = buUnitName;
	}
	@Override
	public String toString() {
		return "MasBuUnitBean [buUnitId=" + buUnitId + ", buUnitName="
				+ buUnitName + "]";
	}

}
