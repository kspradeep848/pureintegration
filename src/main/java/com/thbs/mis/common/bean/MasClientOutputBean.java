package com.thbs.mis.common.bean;

public class MasClientOutputBean {
	private short clientId;

	private String clientName;

	private String clientStatus;
	
	public MasClientOutputBean() {
		super();
	}

	public MasClientOutputBean(short clientId, String clientName,
			String clientStatus) {
		super();
		this.clientId = clientId;
		this.clientName = clientName;
		this.clientStatus = clientStatus;
	}
	
	public short getClientId() {
		return clientId;
	}

	public void setClientId(short clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientStatus() {
		return clientStatus;
	}

	public void setClientStatus(String clientStatus) {
		this.clientStatus = clientStatus;
	}

	@Override
	public String toString() {
		return "MasClientOutputBean [clientId=" + clientId
				+ ", clientName=" + clientName + ", clientStatus="
				+ clientStatus + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + clientId;
		result = prime * result
				+ ((clientName == null) ? 0 : clientName.hashCode());
		result = prime * result
				+ ((clientStatus == null) ? 0 : clientStatus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MasClientOutputBean other = (MasClientOutputBean) obj;
		if (clientId != other.clientId)
			return false;
		if (clientName == null) {
			if (other.clientName != null)
				return false;
		} else if (!clientName.equals(other.clientName))
			return false;
		if (clientStatus == null) {
			if (other.clientStatus != null)
				return false;
		} else if (!clientStatus.equals(other.clientStatus))
			return false;
		return true;
	}
	
}
