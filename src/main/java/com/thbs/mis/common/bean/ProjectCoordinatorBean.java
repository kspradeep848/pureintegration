package com.thbs.mis.common.bean;

public class ProjectCoordinatorBean implements Comparable<ProjectCoordinatorBean>
{
	private Integer projectCoordinatorId;
	private String projectCoordinatorName;
	private String projectCoordinator;
	
	public Integer getProjectCoordinatorId() 
	{
		return projectCoordinatorId;
	}
	
	public void setProjectCoordinatorId(Integer projectCoordinatorId) 
	{
		this.projectCoordinatorId = projectCoordinatorId;
	}
	
	public String getProjectCoordinatorName() 
	{
		return projectCoordinatorName;
	}
	
	public void setProjectCoordinatorName(String projectCoordinatorName) 
	{
		this.projectCoordinatorName = projectCoordinatorName;
	}
	
	public String getProjectCoordinator() {
		return projectCoordinator;
	}

	public void setProjectCoordinator(String projectCoordinator) {
		this.projectCoordinator = projectCoordinator;
	}

	@Override
	public int compareTo(ProjectCoordinatorBean arg0) 
	{
		return this.getProjectCoordinatorName().toUpperCase().compareTo(arg0.getProjectCoordinatorName().toUpperCase());
	}

	@Override
	public String toString() {
		return "ProjectCoordinatorBean [projectCoordinatorId="
				+ projectCoordinatorId + ", projectCoordinatorName="
				+ projectCoordinatorName + ", projectCoordinator="
				+ projectCoordinator + "]";
	}

	
}
