package com.thbs.mis.common.bean;

import java.util.List;

public class FCMTokenBean {
	private String userDeviceIdKey;
	private int empId;
	private List<Short> moduleIdList;

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public List<Short> getModuleIdList() {
		return moduleIdList;
	}

	public void setModuleIdList(List<Short> moduleIdList) {
		this.moduleIdList = moduleIdList;
	}

	public String getUserDeviceIdKey() {
		return userDeviceIdKey;
	}

	public void setUserDeviceIdKey(String userDeviceIdKey) {
		this.userDeviceIdKey = userDeviceIdKey;
	}

	@Override
	public String toString() {
		return "FCMTokenBean [userDeviceIdKey=" + userDeviceIdKey + "]";
	}

}
