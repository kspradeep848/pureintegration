/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  CountryBean.java                           		 */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  23-Dec-2016                                       */
/*                                                                   */
/*  Description :  This bean related to country. This bean have      */
/* 					country id, country name and country code.    	 */  
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 23-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.common.bean;


public class CountryBean {
	
	private Short countryId;
	
	private String countryName;
	
	private String countryCode;
	
	public CountryBean() {
		super();
	}
	public CountryBean(Short countryId, String countryName, String countryCode) {
		super();
		this.countryId = countryId;
		this.countryName = countryName;
		this.countryCode = countryCode;
	}
	public Short getCountryId() {
		return countryId;
	}
	public void setCountryId(Short countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	
	@Override
	public String toString() {
		return "CountryBean [countryId=" + countryId + ", countryName="
				+ countryName + ", countryCode=" + countryCode + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result
				+ ((countryId == null) ? 0 : countryId.hashCode());
		result = prime * result
				+ ((countryName == null) ? 0 : countryName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CountryBean other = (CountryBean) obj;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (countryId == null) {
			if (other.countryId != null)
				return false;
		} else if (!countryId.equals(other.countryId))
			return false;
		if (countryName == null) {
			if (other.countryName != null)
				return false;
		} else if (!countryName.equals(other.countryName))
			return false;
		return true;
	}

}
