package com.thbs.mis.common.bean;

import java.util.Comparator;

public class ManagerOutputBean implements Comparator<ManagerOutputBean> {

	private Integer empId;
	private String empName;
	
	public Integer getEmpId() {
		return empId;
	}
	
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	
	public String getEmpName() {
		return empName;
	}
	
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	@Override
	public String toString() {
		return "ManagerOutputBean [empId=" + empId + ", empName=" + empName
				+ "]";
	}
	
	@Override
	public int compare(ManagerOutputBean bean, ManagerOutputBean bean1) {
		// TODO Auto-generated method stub
		return bean.getEmpName().compareTo(bean1.getEmpName());
	}
}
