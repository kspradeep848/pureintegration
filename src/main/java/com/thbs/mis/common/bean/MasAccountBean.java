package com.thbs.mis.common.bean;

public class MasAccountBean {
	private short pkAccountId;
	private String accountName;
	
	
	public short getPkAccountId() {
		return pkAccountId;
	}


	public void setPkAccountId(short pkAccountId) {
		this.pkAccountId = pkAccountId;
	}


	public String getAccountName() {
		return accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	@Override
	public String toString() {
		return "ProjMasAccountBO [pkAccountId=" + pkAccountId
				+ ", accountName=" + accountName + "]";
	}

}
