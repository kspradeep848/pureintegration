/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmployeeBean.java                                		 */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  16-Jul-2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jul-2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.common.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.common.bo.MasLocationParentBO;
import com.thbs.mis.framework.util.CustomDateSerializer;

/**
 * <Description EmployeeBean:> TODO
 * @author THBS
 * @version 1.0
 * @see 
 */
public class EmployeeBean
{
	
	private String empId;
	private String pk_EmpLogin_EmpID;

	private String firstName;
	
	private String lastName;
	
	private int empRoleId;
	
	private String empRoleName;
	
	private String buName;
	private String hasReportee;
	
	private int buId;
	
	private String empLevel;
	
	private String reportingMgrId;
	
	private String empEmail;
	private MasLocationParentBO empMasParentLocationId;
	
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date empDoj;

	private Short empOrgId;
	

	public String getEmpId()
	{
		return empId;
	}

	public void setEmpId(String empId)
	{
		this.empId = empId;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public int getEmpRoleId()
	{
		return empRoleId;
	}

	public void setEmpRoleId(int empRoleId)
	{
		this.empRoleId = empRoleId;
	}

	public String getBuName()
	{
		return buName;
	}

	public void setBuName(String buName)
	{
		this.buName = buName;
	}
		

	public int getBuId()
	{
		return buId;
	}

	public void setBuId(int buId)
	{
		this.buId = buId;
	}

	public String getHasReportee() {
		return hasReportee;
	}

	public void setHasReportee(String hasReportee) {
		this.hasReportee = hasReportee;
	}
	
	

	public String getEmpRoleName()
	{
		return empRoleName;
	}

	public void setEmpRoleName(String empRoleName)
	{
		this.empRoleName = empRoleName;
	}

	public String getEmpLevel()
	{
		return empLevel;
	}

	public void setEmpLevel(String empLevel)
	{
		this.empLevel = empLevel;
	}

	public String getReportingMgrId()
	{
		return reportingMgrId;
	}

	public void setReportingMgrId(String reportingMgrId)
	{
		this.reportingMgrId = reportingMgrId;
	}

	public String getEmpEmail()
	{
		return empEmail;
	}

	public void setEmpEmail(String empEmail)
	{
		this.empEmail = empEmail;
	}

	public String getPk_EmpLogin_EmpID() {
		return pk_EmpLogin_EmpID;
	}

	public void setPk_EmpLogin_EmpID(String pk_EmpLogin_EmpID) {
		this.pk_EmpLogin_EmpID = pk_EmpLogin_EmpID;
	}

	public Date getEmpDoj() {
		return empDoj;
	}

	public void setEmpDoj(Date empDoj) {
		this.empDoj = empDoj;
	}

	public Short getEmpOrgId() {
		return empOrgId;
	}

	public void setEmpOrgId(Short empOrgId) {
		this.empOrgId = empOrgId;
	}

	
	public MasLocationParentBO getEmpMasParentLocationId() {
		return empMasParentLocationId;
	}

	public void setEmpMasParentLocationId(MasLocationParentBO empMasParentLocationId) {
		this.empMasParentLocationId = empMasParentLocationId;
	}

	@Override
	public String toString() {
		return "EmployeeBean [empId=" + empId + ", pk_EmpLogin_EmpID=" + pk_EmpLogin_EmpID + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", empRoleId=" + empRoleId + ", empRoleName=" + empRoleName + ", buName="
				+ buName + ", hasReportee=" + hasReportee + ", buId=" + buId + ", empLevel=" + empLevel
				+ ", reportingMgrId=" + reportingMgrId + ", empEmail=" + empEmail + ", empMasParentLocationId="
				+ empMasParentLocationId + ", empDoj=" + empDoj + ", empOrgId=" + empOrgId + "]";
	}
	
}
