package com.thbs.mis.common.bean;

public class MasBusinessEntityBean {
	
	private Byte entityId;

	private String entityName;

	public Byte getEntityId() {
		return entityId;
	}

	public void setEntityId(Byte entityId) {
		this.entityId = entityId;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	@Override
	public String toString() {
		return "MasBusinessEntityBO [entityId=" + entityId + ", entityName="
				+ entityName + "]";
	}

}
