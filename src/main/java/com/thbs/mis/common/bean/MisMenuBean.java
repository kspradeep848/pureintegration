/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  MisMenuBean.java                                  */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  26-Dec-2016                                       */
/*                                                                   */
/*  Description :  This bean related to MIS menu. It have menu id,   */
/* 					menu name, active status, parental flag, 		 */
/* 					menu description, parent id, menu url and module */
/*                 	id.		                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 26-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.common.bean;


public class MisMenuBean {
	
	private short menuId;
	
	private String menuName;
	
	private String activeStatus;
	
	private byte isParent;
	
	private String menuDescription;
	
	private int parentId;
	
	private short moduleId;
	
	private String menuUrl;
	
	private String menuKey;

	public short getMenuId() {
		return menuId;
	}

	public void setMenuId(short menuId) {
		this.menuId = menuId;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}

	public byte getIsParent() {
		return isParent;
	}

	public void setIsParent(byte isParent) {
		this.isParent = isParent;
	}

	public String getMenuDescription() {
		return menuDescription;
	}

	public void setMenuDescription(String menuDescription) {
		this.menuDescription = menuDescription;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public short getModuleId() {
		return moduleId;
	}

	public void setModuleId(short moduleId) {
		this.moduleId = moduleId;
	}

	public String getMenuUrl() {
		return menuUrl;
	}

	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}
	

	public String getMenuKey()
	{
		return menuKey;
	}

	public void setMenuKey(String menuKey)
	{
		this.menuKey = menuKey;
	}

	@Override
	public String toString()
	{
		return "MisMenuBean [menuId=" + menuId + ", menuName="
				+ menuName + ", activeStatus=" + activeStatus
				+ ", isParent=" + isParent + ", menuDescription="
				+ menuDescription + ", parentId=" + parentId
				+ ", moduleId=" + moduleId + ", menuUrl=" + menuUrl
				+ ", menuKey=" + menuKey + "]";
	}	
	
	
}
