/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmpBean.java                              		 */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  23-Dec-2016                                       */
/*                                                                   */
/*  Description :  This bean related to Employee. This bean have     */
/* 					employee id, employee first name and employee    */  
/*                  last name.                                       */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 23-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.common.bean;



public class EmployeesBean {
	
	private Integer employeeId;

	private String employeeFirstName;
	
	private String employeeLastName;
	
	private String empFullNameWithId;
	
	private String designation;

	private Byte empMainStatusId;
	
	private Integer reportingMgrId;
	
	private Short buUnit;
	
	private String buUnitName;
	
	private Integer buHeadId;
	

	public Byte getEmpMainStatusId() {
		return empMainStatusId;
	}

	public void setEmpMainStatusId(Byte empMainStatusId) {
		this.empMainStatusId = empMainStatusId;
	}

	public Integer getReportingMgrId() {
		return reportingMgrId;
	}

	public void setReportingMgrId(Integer reportingMgrId) {
		this.reportingMgrId = reportingMgrId;
	}

	public Short getBuUnit() {
		return buUnit;
	}

	public void setBuUnit(Short buUnit) {
		this.buUnit = buUnit;
	}

	public String getBuUnitName() {
		return buUnitName;
	}

	public void setBuUnitName(String buUnitName) {
		this.buUnitName = buUnitName;
	}

	public Integer getBuHeadId() {
		return buHeadId;
	}

	public void setBuHeadId(Integer buHeadId) {
		this.buHeadId = buHeadId;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeFirstName() {
		return employeeFirstName;
	}

	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}

	public String getEmployeeLastName() {
		return employeeLastName;
	}

	public void setEmployeeLastName(String employeeLastName) {
		this.employeeLastName = employeeLastName;
	}

	public String getEmpFullNameWithId() {
		return empFullNameWithId;
	}

	public void setEmpFullNameWithId(String empFullNameWithId) {
		this.empFullNameWithId = empFullNameWithId;
	}

	@Override
	public String toString() {
		return "EmployeesBean [employeeId=" + employeeId
				+ ", employeeFirstName=" + employeeFirstName
				+ ", employeeLastName=" + employeeLastName
				+ ", empFullNameWithId=" + empFullNameWithId + ", designation="
				+ designation + ", empMainStatusId=" + empMainStatusId
				+ ", reportingMgrId=" + reportingMgrId + ", buUnit=" + buUnit + ", buUnitName="
				+ buUnitName + ", buHeadId=" + buHeadId + "]";
	}

	
}