package com.thbs.mis.common.bean;

public class MasRegionBean {

	private short regionId;

	private String regionName;

	public short getRegionId() {
		return regionId;
	}

	public void setRegionId(short regionId) {
		this.regionId = regionId;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	@Override
	public String toString() {
		return "MasRegionBean [regionId=" + regionId + ", regionName="
				+ regionName + "]";
	}
	
}
