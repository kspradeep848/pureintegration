/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  BootcampBean.java                           		 */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  23-Dec-2016                                       */
/*                                                                   */
/*  Description :  This bean related to bootcamp. This bean have     */
/* 					bootcamp id, bootcamp name and bootcamp year.    */  
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 23-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.common.bean;


public class BootcampBean {
	
	private short bootcampId;
	
	private String bootcampName;
	
	private String bootcampYear;
	
	public short getBootcampId() {
		return bootcampId;
	}
	public void setBootcampId(short bootcampId) {
		this.bootcampId = bootcampId;
	}
	public String getBootcampName() {
		return bootcampName;
	}
	public void setBootcampName(String bootcampName) {
		this.bootcampName = bootcampName;
	}
	public String getBootcampYear() {
		return bootcampYear;
	}
	public void setBootcampYear(String bootcampYear) {
		this.bootcampYear = bootcampYear;
	}
	
	@Override
	public String toString() {
		return "BootcampBean [bootcampId=" + bootcampId + ", bootcampName="
				+ bootcampName + ", bootcampYear=" + bootcampYear + "]";
	}
	
	

}
