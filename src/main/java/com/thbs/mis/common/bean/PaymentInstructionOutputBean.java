package com.thbs.mis.common.bean;

public class PaymentInstructionOutputBean {

	private Short bankPaymentId;
	private String paymentBankName;
	private Byte currencyId;
	private String paymentInstructionDetails;

	public Short getBankPaymentId() {
		return bankPaymentId;
	}

	public void setBankPaymentId(Short bankPaymentId) {
		this.bankPaymentId = bankPaymentId;
	}

	public String getPaymentBankName() {
		return paymentBankName;
	}

	public void setPaymentBankName(String paymentBankName) {
		this.paymentBankName = paymentBankName;
	}

	public Byte getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Byte currencyId) {
		this.currencyId = currencyId;
	}

	public String getPaymentInstructionDetails() {
		return paymentInstructionDetails;
	}

	public void setPaymentInstructionDetails(String paymentInstructionDetails) {
		this.paymentInstructionDetails = paymentInstructionDetails;
	}

	@Override
	public String toString() {
		return "PaymentInstructionOutputBean [bankPaymentId=" + bankPaymentId + ", paymentBankName=" + paymentBankName
				+ ", currencyId=" + currencyId + ", paymentInstructionDetails=" + paymentInstructionDetails + "]";
	}

}
