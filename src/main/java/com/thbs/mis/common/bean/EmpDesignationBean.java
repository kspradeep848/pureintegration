/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmployeeLevelBean.java                            */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  23-Dec-2016                                       */
/*                                                                   */
/*  Description :  This bean related to Employee Designation.        */
/* 					This bean have designation id and                */ 
/* 					designation name. 			 		             */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 23-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.common.bean;


public class EmpDesignationBean {
	
	private short designationId;
	
	private String designationName;
	
	public short getDesignationId() {
		return designationId;
	}
	public void setDesignationId(short designationId) {
		this.designationId = designationId;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	
	@Override
	public String toString() {
		return "EmpDesignationBean [designationId=" + designationId
				+ ", designationName=" + designationName + "]";
	}
	
	

}
