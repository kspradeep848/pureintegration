package com.thbs.mis.common.bean;

import java.util.Date;

public class SeparationLegacyOutputBean {
	
	private Integer employeeId;
	private Integer separationStatusId;
	private Date lastWorkingDate;
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public Integer getSeparationStatusId() {
		return separationStatusId;
	}
	public void setSeparationStatusId(Integer separationStatusId) {
		this.separationStatusId = separationStatusId;
	}
	public Date getLastWorkingDate() {
		return lastWorkingDate;
	}
	public void setLastWorkingDate(Date lastWorkingDate) {
		this.lastWorkingDate = lastWorkingDate;
	}
	@Override
	public String toString() {
		return "SeparationLegacyOutputBean [employeeId=" + employeeId
				+ ", separationStatusId=" + separationStatusId
				+ ", lastWorkingDate=" + lastWorkingDate + "]";
	}


}
