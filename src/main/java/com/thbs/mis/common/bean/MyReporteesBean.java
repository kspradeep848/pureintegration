package com.thbs.mis.common.bean;

public class MyReporteesBean {
	
	private Integer empId;
	private String empFirstName;
	private String empLastName;
	private Integer empReportingMgrId;
	private String hasReporteesFlag;
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getEmpFirstName() {
		return empFirstName;
	}
	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}
	public String getEmpLastName() {
		return empLastName;
	}
	public MyReporteesBean(Integer empId, String empFirstName,
			String empLastName, Integer empReportingMgrId,
			String hasReporteesFlag) {
		super();
		this.empId = empId;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.empReportingMgrId = empReportingMgrId;
		this.hasReporteesFlag = hasReporteesFlag;
	}
	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}
	public Integer getEmpReportingMgrId() {
		return empReportingMgrId;
	}
	public void setEmpReportingMgrId(Integer empReportingMgrId) {
		this.empReportingMgrId = empReportingMgrId;
	}
	public String getHasReporteesFlag() {
		return hasReporteesFlag;
	}
	public void setHasReporteesFlag(String hasReporteesFlag) {
		this.hasReporteesFlag = hasReporteesFlag;
	}
	@Override
	public String toString() {
		return "MyReporteesBean [empId=" + empId + ", empFirstName="
				+ empFirstName + ", empLastName=" + empLastName
				+ ", empReportingMgrId=" + empReportingMgrId
				+ ", hasReporteesFlag=" + hasReporteesFlag + "]";
	}

}
