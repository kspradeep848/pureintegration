package com.thbs.mis.common.bean;

import java.util.List;

public class DatEmpRoleMappingOutputBean {
	
	
	private Integer employeeId;
	
	private List <DatEmpRoleMappingBOBean> employeeRoles;

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public List<DatEmpRoleMappingBOBean> getEmployeeRoles() {
		return employeeRoles;
	}

	public void setEmployeeRoles(List<DatEmpRoleMappingBOBean> employeeRoles) {
		this.employeeRoles = employeeRoles;
	}

	@Override
	public String toString() {
		return "DatEmpRoleMappingOutputBean [employeeId=" + employeeId
				+ ", employeeRoles=" + employeeRoles + "]";
	}


}
