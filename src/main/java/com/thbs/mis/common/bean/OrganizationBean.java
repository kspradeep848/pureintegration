/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  OrganizationBean.java                             */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  26-Dec-2016                                       */
/*                                                                   */
/*  Description :  This bean related to organization BO. This bean   */
/* 					have organization id and organization name.    	 */  
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 26-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.common.bean;

public class OrganizationBean {
	
	private short organizationIdl;
	
	private String organizationName;

	public short getOrganizationIdl() {
		return organizationIdl;
	}

	public void setOrganizationIdl(short organizationIdl) {
		this.organizationIdl = organizationIdl;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	@Override
	public String toString() {
		return "OrganizationBean [organizationIdl=" + organizationIdl
				+ ", organizationName=" + organizationName + "]";
	}
	
	

}
