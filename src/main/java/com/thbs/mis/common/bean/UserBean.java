/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  User.java           		                         */
/*                                                                   */
/*  Author      :  Anil kumar B K	                                 */
/*                                                                   */
/*  Date        :  09-Jan-2017                                       */
/*                                                                   */
/*  Description :  Bean class for Employee details    			     */
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 09-Jan-2017    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.common.bean;

import java.util.Date;

public class UserBean {

	
	private Integer pkEmpId;

	
	private String acceptedThbsPolicyFlag;

	
	private Date empPwdUpdatedOn;

	
	private Byte empType;

	
	private Byte fkEmpMainStatus;

	
	private Short fkEmpRoleId;

	
	private Byte fkEmpSubStatus;

	
	private Integer modifiedBy;

	
	private Date modifiedDate;

	
	private Date policyAcceptedDate;

	private Integer profileCreatedBy;

	private String requestType;
	
	private Date profileCreatedDate;
	
	
	//Personal details attributes
	
	
	private String empAadharNumber;
	
	private String empAlternateMobileNo;

	private String empBloodGroup;

	private Short empCitizenshipOne;
	
	private String empCitizenshipStatus;

	private Short empCitizenshipTwo;

	private Date empDateOfBirth;
	
	private String empFirstName;

	private Byte empGender;

	private String empLastName;

	private String empMiddleName;

	private String empMobileNo;

	private String empPermanentAddress;

	private String empPersonalEmailId;
	
	private String empPresentAddress;
	
	private String empQualification;
	
	private Date empSpouseDateOfBirth;

	private Integer fkEmpDetailId;
	
	
	// Professional bean attributes
	
	private Date effectiveDateOfReportingMgrChange;

	
	private Byte empConfirmationGraduateFlag;

	
	private Date empContractEndDate;

	
	private float empCostUnit;

	
	private Date empDateOfJoining;

	private String empExtensionNumber;

	
	private Byte empOnBenchFlag;

	
	private String empPhoneNumber;

	
	private float empPreviousExperience;

	
	private String empProfessionalEmailId;

	
	private Short fkEmpBootCampId;

	
	private Short fkEmpBuUnit;

	
	private Integer fkEmpCompetenceMgrId;

	
	private Short fkEmpDesignation;

	
	private Integer fkEmpDomainMgrId;

	
	private Short fkEmpLevel;

	
	private Short fkEmpLocationParentId;

	
	private Short fkEmpLocationPhysicalId;

	
	private Short fkEmpOrganizationId;

	
	private Integer fkEmpReportingMgrId;

	
	private Integer fkMainEmpDetailId;
	
	
	private Integer professionalDetailModifiedBy;
	
	
	private String hasReporteesFlag;
	
	private String role;
	
	private Integer personalDetailModifiedBy;
	
	private Date professionalDetailModifiedDate;
	
	private Date personalDetailModifiedDate;
	
	private Date empLastWorkingDate;

	private Integer consultantReferenceEmpId;

	
	
	
	public String getRequestType() {
		return requestType;
	}


	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}


	public Integer getConsultantReferenceEmpId() {
		return consultantReferenceEmpId;
	}


	public void setConsultantReferenceEmpId(Integer consultantReferenceEmpId) {
		this.consultantReferenceEmpId = consultantReferenceEmpId;
	}


	public Integer getPersonalDetailModifiedBy() {
		return personalDetailModifiedBy;
	}


	public void setPersonalDetailModifiedBy(Integer personalDetailModifiedBy) {
		this.personalDetailModifiedBy = personalDetailModifiedBy;
	}


	public Date getProfessionalDetailModifiedDate() {
		return professionalDetailModifiedDate;
	}


	public void setProfessionalDetailModifiedDate(
			Date professionalDetailModifiedDate) {
		this.professionalDetailModifiedDate = professionalDetailModifiedDate;
	}


	public Date getPersonalDetailModifiedDate() {
		return personalDetailModifiedDate;
	}


	public void setPersonalDetailModifiedDate(Date personalDetailModifiedDate) {
		this.personalDetailModifiedDate = personalDetailModifiedDate;
	}


	public Date getEmpLastWorkingDate() {
		return empLastWorkingDate;
	}


	public void setEmpLastWorkingDate(Date empLastWorkingDate) {
		this.empLastWorkingDate = empLastWorkingDate;
	}


	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}


	public Integer getPkEmpId() {
		return pkEmpId;
	}


	public void setPkEmpId(Integer pkEmpId) {
		this.pkEmpId = pkEmpId;
	}


	public String getAcceptedThbsPolicyFlag() {
		return acceptedThbsPolicyFlag;
	}


	public void setAcceptedThbsPolicyFlag(String acceptedThbsPolicyFlag) {
		this.acceptedThbsPolicyFlag = acceptedThbsPolicyFlag;
	}


	public Date getEmpPwdUpdatedOn() {
		return empPwdUpdatedOn;
	}


	public void setEmpPwdUpdatedOn(Date empPwdUpdatedOn) {
		this.empPwdUpdatedOn = empPwdUpdatedOn;
	}


	public Byte getEmpType() {
		return empType;
	}


	public void setEmpType(Byte empType) {
		this.empType = empType;
	}


	public Byte getFkEmpMainStatus() {
		return fkEmpMainStatus;
	}


	public void setFkEmpMainStatus(Byte fkEmpMainStatus) {
		this.fkEmpMainStatus = fkEmpMainStatus;
	}


	public Short getFkEmpRoleId() {
		return fkEmpRoleId;
	}


	public void setFkEmpRoleId(Short fkEmpRoleId) {
		this.fkEmpRoleId = fkEmpRoleId;
	}


	public Byte getFkEmpSubStatus() {
		return fkEmpSubStatus;
	}


	public void setFkEmpSubStatus(Byte fkEmpSubStatus) {
		this.fkEmpSubStatus = fkEmpSubStatus;
	}


	public Integer getModifiedBy() {
		return modifiedBy;
	}


	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	public Date getModifiedDate() {
		return modifiedDate;
	}


	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	public Date getPolicyAcceptedDate() {
		return policyAcceptedDate;
	}


	public void setPolicyAcceptedDate(Date policyAcceptedDate) {
		this.policyAcceptedDate = policyAcceptedDate;
	}


	public Integer getProfileCreatedBy() {
		return profileCreatedBy;
	}


	public void setProfileCreatedBy(Integer profileCreatedBy) {
		this.profileCreatedBy = profileCreatedBy;
	}


	public Date getProfileCreatedDate() {
		return profileCreatedDate;
	}


	public void setProfileCreatedDate(Date profileCreatedDate) {
		this.profileCreatedDate = profileCreatedDate;
	}


	public String getEmpAadharNumber() {
		return empAadharNumber;
	}


	public void setEmpAadharNumber(String empAadharNumber) {
		this.empAadharNumber = empAadharNumber;
	}


	public String getEmpAlternateMobileNo() {
		return empAlternateMobileNo;
	}


	public void setEmpAlternateMobileNo(String empAlternateMobileNo) {
		this.empAlternateMobileNo = empAlternateMobileNo;
	}


	public String getEmpBloodGroup() {
		return empBloodGroup;
	}


	public void setEmpBloodGroup(String empBloodGroup) {
		this.empBloodGroup = empBloodGroup;
	}


	public Short getEmpCitizenshipOne() {
		return empCitizenshipOne;
	}


	public void setEmpCitizenshipOne(Short empCitizenshipOne) {
		this.empCitizenshipOne = empCitizenshipOne;
	}


	public String getEmpCitizenshipStatus() {
		return empCitizenshipStatus;
	}


	public void setEmpCitizenshipStatus(String empCitizenshipStatus) {
		this.empCitizenshipStatus = empCitizenshipStatus;
	}


	public Short getEmpCitizenshipTwo() {
		return empCitizenshipTwo;
	}


	public void setEmpCitizenshipTwo(Short empCitizenshipTwo) {
		this.empCitizenshipTwo = empCitizenshipTwo;
	}


	public Date getEmpDateOfBirth() {
		return empDateOfBirth;
	}


	public void setEmpDateOfBirth(Date empDateOfBirth) {
		this.empDateOfBirth = empDateOfBirth;
	}


	public String getEmpFirstName() {
		return empFirstName;
	}


	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}



	public Byte getEmpGender() {
		return empGender;
	}


	public void setEmpGender(Byte empGender) {
		this.empGender = empGender;
	}


	public String getEmpLastName() {
		return empLastName;
	}


	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}


	public String getEmpMiddleName() {
		return empMiddleName;
	}


	public void setEmpMiddleName(String empMiddleName) {
		this.empMiddleName = empMiddleName;
	}


	public String getEmpMobileNo() {
		return empMobileNo;
	}


	public void setEmpMobileNo(String empMobileNo) {
		this.empMobileNo = empMobileNo;
	}


	public String getEmpPermanentAddress() {
		return empPermanentAddress;
	}


	public void setEmpPermanentAddress(String empPermanentAddress) {
		this.empPermanentAddress = empPermanentAddress;
	}


	public String getEmpPersonalEmailId() {
		return empPersonalEmailId;
	}


	public void setEmpPersonalEmailId(String empPersonalEmailId) {
		this.empPersonalEmailId = empPersonalEmailId;
	}


	public String getEmpPresentAddress() {
		return empPresentAddress;
	}


	public void setEmpPresentAddress(String empPresentAddress) {
		this.empPresentAddress = empPresentAddress;
	}


	public String getEmpQualification() {
		return empQualification;
	}


	public void setEmpQualification(String empQualification) {
		this.empQualification = empQualification;
	}


	public Date getEmpSpouseDateOfBirth() {
		return empSpouseDateOfBirth;
	}


	public void setEmpSpouseDateOfBirth(Date empSpouseDateOfBirth) {
		this.empSpouseDateOfBirth = empSpouseDateOfBirth;
	}


	public Integer getFkEmpDetailId() {
		return fkEmpDetailId;
	}


	public void setFkEmpDetailId(Integer fkEmpDetailId) {
		this.fkEmpDetailId = fkEmpDetailId;
	}


	public Date getEffectiveDateOfReportingMgrChange() {
		return effectiveDateOfReportingMgrChange;
	}


	public void setEffectiveDateOfReportingMgrChange(
			Date effectiveDateOfReportingMgrChange) {
		this.effectiveDateOfReportingMgrChange = effectiveDateOfReportingMgrChange;
	}


	public Byte getEmpConfirmationGraduateFlag() {
		return empConfirmationGraduateFlag;
	}


	public void setEmpConfirmationGraduateFlag(Byte empConfirmationGraduateFlag) {
		this.empConfirmationGraduateFlag = empConfirmationGraduateFlag;
	}


	public Date getEmpContractEndDate() {
		return empContractEndDate;
	}


	public void setEmpContractEndDate(Date empContractEndDate) {
		this.empContractEndDate = empContractEndDate;
	}


	public float getEmpCostUnit() {
		return empCostUnit;
	}


	public void setEmpCostUnit(float empCostUnit) {
		this.empCostUnit = empCostUnit;
	}


	public Date getEmpDateOfJoining() {
		return empDateOfJoining;
	}


	public void setEmpDateOfJoining(Date empDateOfJoining) {
		this.empDateOfJoining = empDateOfJoining;
	}


	public String getEmpExtensionNumber() {
		return empExtensionNumber;
	}


	public void setEmpExtensionNumber(String empExtensionNumber) {
		this.empExtensionNumber = empExtensionNumber;
	}


	public Byte getEmpOnBenchFlag() {
		return empOnBenchFlag;
	}


	public void setEmpOnBenchFlag(Byte empOnBenchFlag) {
		this.empOnBenchFlag = empOnBenchFlag;
	}


	public String getEmpPhoneNumber() {
		return empPhoneNumber;
	}


	public void setEmpPhoneNumber(String empPhoneNumber) {
		this.empPhoneNumber = empPhoneNumber;
	}


	public float getEmpPreviousExperience() {
		return empPreviousExperience;
	}


	public void setEmpPreviousExperience(float empPreviousExperience) {
		this.empPreviousExperience = empPreviousExperience;
	}


	public String getEmpProfessionalEmailId() {
		return empProfessionalEmailId;
	}


	public void setEmpProfessionalEmailId(String empProfessionalEmailId) {
		this.empProfessionalEmailId = empProfessionalEmailId;
	}


	public Short getFkEmpBootCampId() {
		return fkEmpBootCampId;
	}


	public void setFkEmpBootCampId(Short fkEmpBootCampId) {
		this.fkEmpBootCampId = fkEmpBootCampId;
	}


	public Short getFkEmpBuUnit() {
		return fkEmpBuUnit;
	}


	public void setFkEmpBuUnit(Short fkEmpBuUnit) {
		this.fkEmpBuUnit = fkEmpBuUnit;
	}


	public Integer getFkEmpCompetenceMgrId() {
		return fkEmpCompetenceMgrId;
	}


	public void setFkEmpCompetenceMgrId(Integer fkEmpCompetenceMgrId) {
		this.fkEmpCompetenceMgrId = fkEmpCompetenceMgrId;
	}


	public Short getFkEmpDesignation() {
		return fkEmpDesignation;
	}


	public void setFkEmpDesignation(Short fkEmpDesignation) {
		this.fkEmpDesignation = fkEmpDesignation;
	}


	public Integer getFkEmpDomainMgrId() {
		return fkEmpDomainMgrId;
	}


	public void setFkEmpDomainMgrId(Integer fkEmpDomainMgrId) {
		this.fkEmpDomainMgrId = fkEmpDomainMgrId;
	}


	public Short getFkEmpLevel() {
		return fkEmpLevel;
	}


	public void setFkEmpLevel(Short fkEmpLevel) {
		this.fkEmpLevel = fkEmpLevel;
	}


	public Short getFkEmpLocationParentId() {
		return fkEmpLocationParentId;
	}


	public void setFkEmpLocationParentId(Short fkEmpLocationParentId) {
		this.fkEmpLocationParentId = fkEmpLocationParentId;
	}


	public Short getFkEmpLocationPhysicalId() {
		return fkEmpLocationPhysicalId;
	}


	public void setFkEmpLocationPhysicalId(Short fkEmpLocationPhysicalId) {
		this.fkEmpLocationPhysicalId = fkEmpLocationPhysicalId;
	}


	public Short getFkEmpOrganizationId() {
		return fkEmpOrganizationId;
	}


	public void setFkEmpOrganizationId(Short fkEmpOrganizationId) {
		this.fkEmpOrganizationId = fkEmpOrganizationId;
	}


	public Integer getFkEmpReportingMgrId() {
		return fkEmpReportingMgrId;
	}


	public void setFkEmpReportingMgrId(Integer fkEmpReportingMgrId) {
		this.fkEmpReportingMgrId = fkEmpReportingMgrId;
	}


	public Integer getFkMainEmpDetailId() {
		return fkMainEmpDetailId;
	}


	public void setFkMainEmpDetailId(Integer fkMainEmpDetailId) {
		this.fkMainEmpDetailId = fkMainEmpDetailId;
	}


	public Integer getProfessionalDetailModifiedBy() {
		return professionalDetailModifiedBy;
	}


	public void setProfessionalDetailModifiedBy(Integer professionalDetailModifiedBy) {
		this.professionalDetailModifiedBy = professionalDetailModifiedBy;
	}


	public String getHasReporteesFlag() {
		return hasReporteesFlag;
	}


	public void setHasReporteesFlag(String hasReporteesFlag) {
		this.hasReporteesFlag = hasReporteesFlag;
	}


	@Override
	public String toString() {
		return "UserBean [pkEmpId=" + pkEmpId + ", acceptedThbsPolicyFlag="
				+ acceptedThbsPolicyFlag + ", empPwdUpdatedOn="
				+ empPwdUpdatedOn + ", empType=" + empType
				+ ", fkEmpMainStatus=" + fkEmpMainStatus + ", fkEmpRoleId="
				+ fkEmpRoleId + ", fkEmpSubStatus=" + fkEmpSubStatus
				+ ", modifiedBy=" + modifiedBy + ", modifiedDate="
				+ modifiedDate + ", policyAcceptedDate=" + policyAcceptedDate
				+ ", profileCreatedBy=" + profileCreatedBy
				+ ", profileCreatedDate=" + profileCreatedDate
				+ ", empAadharNumber=" + empAadharNumber
				+ ", empAlternateMobileNo=" + empAlternateMobileNo
				+ ", empBloodGroup=" + empBloodGroup + ", empCitizenshipOne="
				+ empCitizenshipOne + ", empCitizenshipStatus="
				+ empCitizenshipStatus + ", empCitizenshipTwo="
				+ empCitizenshipTwo + ", empDateOfBirth=" + empDateOfBirth
				+ ", empFirstName=" + empFirstName + ", empGender=" + empGender
				+ ", empLastName=" + empLastName + ", empMiddleName="
				+ empMiddleName + ", empMobileNo=" + empMobileNo
				+ ", empPermanentAddress=" + empPermanentAddress
				+ ", empPersonalEmailId=" + empPersonalEmailId
				+ ", empPresentAddress=" + empPresentAddress
				+ ", empQualification=" + empQualification
				+ ", empSpouseDateOfBirth=" + empSpouseDateOfBirth
				+ ", fkEmpDetailId=" + fkEmpDetailId
				+ ", effectiveDateOfReportingMgrChange="
				+ effectiveDateOfReportingMgrChange
				+ ", empConfirmationGraduateFlag="
				+ empConfirmationGraduateFlag + ", empContractEndDate="
				+ empContractEndDate + ", empCostUnit=" + empCostUnit
				+ ", empDateOfJoining=" + empDateOfJoining
				+ ", empExtensionNumber=" + empExtensionNumber
				+ ", empOnBenchFlag=" + empOnBenchFlag + ", empPhoneNumber="
				+ empPhoneNumber + ", empPreviousExperience="
				+ empPreviousExperience + ", empProfessionalEmailId="
				+ empProfessionalEmailId + ", fkEmpBootCampId="
				+ fkEmpBootCampId + ", fkEmpBuUnit=" + fkEmpBuUnit
				+ ", fkEmpCompetenceMgrId=" + fkEmpCompetenceMgrId
				+ ", fkEmpDesignation=" + fkEmpDesignation
				+ ", fkEmpDomainMgrId=" + fkEmpDomainMgrId + ", fkEmpLevel="
				+ fkEmpLevel + ", fkEmpLocationParentId="
				+ fkEmpLocationParentId + ", fkEmpLocationPhysicalId="
				+ fkEmpLocationPhysicalId + ", fkEmpOrganizationId="
				+ fkEmpOrganizationId + ", fkEmpReportingMgrId="
				+ fkEmpReportingMgrId + ", fkMainEmpDetailId="
				+ fkMainEmpDetailId + ", professionalDetailModifiedBy="
				+ professionalDetailModifiedBy + ", hasReporteesFlag="
				+ hasReporteesFlag + ", role=" + role
				+ ", personalDetailModifiedBy=" + personalDetailModifiedBy
				+ ", professionalDetailModifiedDate="
				+ professionalDetailModifiedDate
				+ ", personalDetailModifiedDate=" + personalDetailModifiedDate
				+ ", empLastWorkingDate=" + empLastWorkingDate
				+ ", consultantReferenceEmpId=" + consultantReferenceEmpId
				+ "]";
	}


	
	
}
