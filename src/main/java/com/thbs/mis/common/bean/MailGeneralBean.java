package com.thbs.mis.common.bean;

public class MailGeneralBean {

	private String toAddress;
	private String ccAddress;
	private String mailSubject;
	private String mailBody;
	public String getToAddress() {
		return toAddress;
	}
	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}
	public String getCcAddress() {
		return ccAddress;
	}
	public void setCcAddress(String ccAddress) {
		this.ccAddress = ccAddress;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public String getMailBody() {
		return mailBody;
	}
	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}
	@Override
	public String toString() {
		return "MailGeneralBean [toAddress=" + toAddress + ", ccAddress="
				+ ccAddress + ", mailSubject=" + mailSubject + ", mailBody="
				+ mailBody + "]";
	}
	
	
}
