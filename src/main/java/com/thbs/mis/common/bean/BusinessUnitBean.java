/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  BusinessUnitBean.java                             */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  20-Dec-2016                                       */
/*                                                                   */
/*  Description :  This bean related to Business Unit. This bean 	 */
/* 					have business unit id, business unit name and    */
/* 					BU head id. 		 					 		 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 20-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.common.bean;


public class BusinessUnitBean {
  	
	private short pkBuUnitId;
	private String buUnitName;
	//private String activeFlag;
	private Integer fkBuHeadEmpId;
	private Integer empTotalcount;
	private String buHeadName;
	
	

	/*public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}*/

	public void setPkBuUnitId(short pkBuUnitId) {
		this.pkBuUnitId = pkBuUnitId;
	}

	public Integer getFkBuHeadEmpId() {
		return fkBuHeadEmpId;
	}

	public void setFkBuHeadEmpId(Integer fkBuHeadEmpId) {
		this.fkBuHeadEmpId = fkBuHeadEmpId;
	}

	public Integer getEmpTotalcount() {
		return empTotalcount;
	}

	public void setEmpTotalcount(Integer empTotalcount) {
		this.empTotalcount = empTotalcount;
	}

	public String getBuHeadName() {
		return buHeadName;
	}

	public void setBuHeadName(String buHeadName) {
		this.buHeadName = buHeadName;
	}

	public String getBuUnitName() {
		return buUnitName;
	}

	public void setBuUnitName(String buUnitName) {
		this.buUnitName = buUnitName;
	}
	

	public Short getPkBuUnitId() {
		return pkBuUnitId;
	}

	public void setPkBuUnitId(Short pkBuUnitId) {
		this.pkBuUnitId = pkBuUnitId;
	}

	@Override
	public String toString() {
		return "BusinessUnitBean [pkBuUnitId=" + pkBuUnitId + ", buUnitName="
				+ buUnitName + ", fkBuHeadEmpId=" + fkBuHeadEmpId
				+ ", empTotalcount=" + empTotalcount + ", buHeadName="
				+ buHeadName + "]";
	}


	


}
