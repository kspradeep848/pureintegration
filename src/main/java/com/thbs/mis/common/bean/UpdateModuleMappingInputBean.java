package com.thbs.mis.common.bean;

public class UpdateModuleMappingInputBean {
	
	private int employeeId;
	private short firstModuleId;
	private short secondModuleId;
	private short thirdModuleId;
	private short forthModuleId;
	private short fifthModuleId;
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public Short getFirstModuleId() {
		return firstModuleId;
	}
	public void setFirstModuleId(short firstModuleId) {
		this.firstModuleId = firstModuleId;
	}
	public Short getSecondModuleId() {
		return secondModuleId;
	}
	public void setSecondModuleId(short secondModuleId) {
		this.secondModuleId = secondModuleId;
	}
	public Short getThirdModuleId() {
		return thirdModuleId;
	}
	public void setThirdModuleId(short thirdModuleId) {
		this.thirdModuleId = thirdModuleId;
	}
	public Short getForthModuleId() {
		return forthModuleId;
	}
	public void setForthModuleId(short forthModuleId) {
		this.forthModuleId = forthModuleId;
	}
	public Short getFifthModuleId() {
		return fifthModuleId;
	}
	public void setFifthModuleId(short fifthModuleId) {
		this.fifthModuleId = fifthModuleId;
	}
	
	@Override
	public String toString() {
		return "UpdateModuleMappingInputBean [employeeId=" + employeeId
				+ ", firstModuleId=" + firstModuleId + ", secondModuleId="
				+ secondModuleId + ", thirdModuleId=" + thirdModuleId
				+ ", forthModuleId=" + forthModuleId + ", fifthModuleId="
				+ fifthModuleId + "]";
	}
}
