package com.thbs.mis.common.bean;

public class DmDetailsOutputBean {
	
	private String empIdWithName;

	public String getEmpIdWithName() {
		return empIdWithName;
	}

	public void setEmpIdWithName(String empIdWithName) {
		this.empIdWithName = empIdWithName;
	}

	@Override
	public String toString() {
		return "DmDetailsOutputBean [empIdWithName=" + empIdWithName + "]";
	}
	
	

}
