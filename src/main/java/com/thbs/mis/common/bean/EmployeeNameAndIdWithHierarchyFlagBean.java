package com.thbs.mis.common.bean;


public class EmployeeNameAndIdWithHierarchyFlagBean {
	
	private Integer employeeId;
	private String empFirstName;
	private String empLastName;
	private String empFullNameWithId;
	private Integer managerId;
	private Boolean lastHierarchyOrderFlag;
	
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmpFirstName() {
		return empFirstName;
	}
	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}
	public String getEmpLastName() {
		return empLastName;
	}
	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}
	public String getEmpFullNameWithId() {
		return empFullNameWithId;
	}
	public void setEmpFullNameWithId(String empFullNameWithId) {
		this.empFullNameWithId = empFullNameWithId;
	}
	public Integer getManagerId() {
		return managerId;
	}
	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}
	public Boolean getLastHierarchyOrderFlag() {
		return lastHierarchyOrderFlag;
	}
	public void setLastHierarchyOrderFlag(Boolean lastHierarchyOrderFlag) {
		this.lastHierarchyOrderFlag = lastHierarchyOrderFlag;
	}
	@Override
	public String toString() {
		return "EmployeeNameAndIdWithReporteeDetailBean [employeeId="
				+ employeeId + ", empFirstName=" + empFirstName
				+ ", empLastName=" + empLastName + ", empFullNameWithId="
				+ empFullNameWithId + ", managerId=" + managerId
				+ ", lastHierarchyOrderFlag=" + lastHierarchyOrderFlag + "]";
	}
	
}
