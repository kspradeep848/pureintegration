package com.thbs.mis.common.bean;

public class RoleDetailsOutputBean {
	
	private short pkEmpRoleId;

	private String empRoleName;
	
	private String empRoleKeyName;

	public short getPkEmpRoleId() {
		return pkEmpRoleId;
	}

	public void setPkEmpRoleId(short pkEmpRoleId) {
		this.pkEmpRoleId = pkEmpRoleId;
	}

	public String getEmpRoleName() {
		return empRoleName;
	}

	public void setEmpRoleName(String empRoleName) {
		this.empRoleName = empRoleName;
	}

	public String getEmpRoleKeyName() {
		return empRoleKeyName;
	}

	public void setEmpRoleKeyName(String empRoleKeyName) {
		this.empRoleKeyName = empRoleKeyName;
	}

	@Override
	public String toString() {
		return "RoleDetailsOutputBean [pkEmpRoleId=" + pkEmpRoleId
				+ ", empRoleName=" + empRoleName + ", empRoleKeyName="
				+ empRoleKeyName + "]";
	}
	
	

}
