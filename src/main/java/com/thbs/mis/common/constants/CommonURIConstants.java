/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  CommonURIConstants.java                           */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  12-Dec-2016                                       */
/*                                                                   */
/*  Description :  Common uri used to fetch the service				 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 12-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.common.constants;

public class CommonURIConstants {
	
	/*	Please follow alphabetical order of uri */
	public static final String GET_ALL_BOOTCAMPS = "/common/bootcamps";
	public static final String GET_ALL_BU_ENTITY = "/common/businessEntity";
	public static final String GET_ALL_ACTIVE_BU = "/common/businessUnits";
	public static final String GET_BU_INFO_BY_BUID="/common/businessUnits/{buId}";
	public static final String GET_ALL_COUNTRIES = "/common/countries";
	public static final String GET_ALL_DESIGNATIONS = "/common/designations";
	public static final String GET_ALL_DIRECT_AND_INDIRECT_REPORTEES = "/common/directAndIndirectReportees/{rmId}";
	public static final String GET_ALL_DIRECT_REPORTEES = "/common/directReportees/{rmId}";
	public static final String GET_ALL_EMPLOYEES = "/common/employees";
	public static final String GET_ALL_ACTIVE_EMPLOYEES = "/common/employees/active";//unassigned task
	public static final String GET_ALL_INACTIVE_EMPLOYEES = "/common/employees/inactive";//unassigned task
	public static final String GET_ALL_EMP_LEVELS = "/common/levels";
	public static final String GET_ALL_MENU_BASEDON_ROLE = "/common/menuBasedOnRole/{roleId}";
	public static final String GET_ALL_ORGANIZATION = "/common/organizations";
	public static final String GET_ALL_PARENT_LOCATIONS = "/common/parentLocations";
	public static final String GET_ALL_PHYSICAL_LOCATIONS = "/common/physicalLocations";
	public static final String GET_ALL_REPORTING_MANAGERS = "/common/reportingManagers";
	public static final String GET_ALL_SUB_MENU_BASED_ON_MENU = "/common/subMenuBasedOnMenu/{menuId}";	
	public static final String GET_ALL_ROLES = "/common/roles";
	
	public static final String GET_LOCATION_DETAILS_COUNTRYID = "/common/locationDetails/{countryId}";
	public static final String GET_PHYSICAL_LOCATION_DETAILS_BY_PARENT_ID = "/common/physicallocationDetails/{countryId}";
	
	public static final String CREATE_USER = "/common/user";
	public static final String UPDATE_USER = "/common/user";
	public static final String GET_ALL_ACCOUNT = "/common/accounts";
	public static final String GET_ALL_MENU_FOR_EMPLOYEE="/common/menus/{empId}";
	
	public static final String GET_ALL_SUB_MENU_FOR_EMPLOYEE_ON_PARENT_MENU="/common/{empId}/menus/{parentMenuId}";
	
	public static final String GET_ALL_TOPLEVEL_FOR_EMPLOYEE="/common/{empId}/menus";
	
	public static final String GET_ALL_REPORTING_MANAGERS_BY_ROLE_ID = "/common/reportingManagers/role"; //added by Smrithi
	
	public static final String GET_ALL_RM_NAMES_BY_BUSINESS_UNIT_ID = "/common/rm/buunit/{buUnitId}"; //added by Smrithi
	
	public static final String GET_ALL_RM_NAMES_BY_EMPLOYEE_ID = " /common/reportingmgrs/bu/employee/{empId}"; //added by Smrithi
	
	public static final String GET_ALL_EMPLOYEE_NAMES_BY_EMPLOYEE_ID = " /common/employeenames/bu/employee/{empId}"; //added by Smrithi
	
	public static final String GET_ALL_PERMANENT_AND_GRADUATED_EMPLOYEES = "/common/permanent/graduated/emp"; //added by Rajesh Kumar
	
	public static final String GET_ALL_PERMANENT_AND_GRADUATED_ACTIVE_EMPLOYEES = "/common/permanent/graduated/active/emp"; //added by Rajesh Kumar
	
	public static final String IS_GSR_GOALS_CLOSED_FOR_RM_CHANGE = "/common/user/rmChange";
	public static final String IS_GSR_GOALS_CLOSED_FOR_RM_CHANGE_LEGACY = "/common/user/legacy/rmChange";
	
	public static final String GET_MY_REPORTEES = "/common/myReportees/login/{empId}";  // added by Pratibha

	public static final String COMMON_NOTIFICATIONS="/fcm/notification";
	
	public static final String GET_INDIRECT_EMP_BASED_ON_DM_ID_AND_GRADUATED_EMP = "/common/indirect/dm/{rmId}"; // Added by Rajesh Kumar
	
	public static final String GET_DIRECT_EMP_BASED_ON_RM_ID_AND_GRADUATED_EMP = "/common/direct/graduate/{rmId}";
	
	public static final String GET_BU_DETAILS = "/common/buUnitDetails/{empId}"; // added by Pratibha
	
	public static final String GET_EMP_INFO = " /common/employee/info/{empId}"; //added by Smrithi

	public static final String FCM_NOTIFICATIONS = "/fcm/pushnotifications"; 

	//Added by Kamal Anand
	public static final String GET_ALL_RM_AND_SKIPLEVELRM_IN_BU_BY_EMPID = "/common/bu/rm/{empId}";
	//End of Addition by Kamal Anand 
	
	public static final String GET_ALL_CORRENCY_TYPE = "/common/correncyType/all";

	public static final String GET_ALL_DOMAIN_MANAGERS_BY_BU = "/common/bu/dm/details/{buId}"; // added by Balaji 
	
	public static final String GET_ALL_ROLE_DETAILS = "/common/role/details"; // added by Balaji
	
	//Added by Lalith kumar
	public static final String GET_ALL_VERTICALS = "/common/verticals";
	//EOA by Lalith kumar
	
	
	//Added by prathibha for fetch user roles
	public static final String GET_EMPLOYEE_MAPPED_ROLES = "/common/roles/employee/{empId}";
	
	public static final String GET_FOREX_BY_CURRENCY_ID = "/common/forex/{fkForexCurrencyTypeId}"; //added by Rajesh
	
	 public static final String UPDATE_MODULE_MAPPING = "/common/moduleMapping/update"; // Added by pratibha
	 
	public static final String GET_FOREX_BY_DATE = "/common/forex/details/{date}"; //added by Rajesh
	
	public static final String DOWNLOAD_VIDEO = "/commom/download/{name}"; //added by Rajesh
	
	public static final String GET_ALL_PROJECT_COORDINATOR = "/common/projectcoordinator"; // added by Rinta
	
	public static final String GET_ALL_REGIONS = "/common/regions";
	
	public static final String ACCOUNTS_MANAGER = "/common/accountmanager/{buId}"; //Added by Adavayya
	
	public static final String DOMAIN_MANAGER = "/common/domainmanager/{buId}"; //Added by Adavayya
	
	public static final String DELIVERY_MANAGER = "/common/deliverymanager/{buId}"; //Added by Adavayya
	
	public static final String DOMAIN_DETAILS = "/common/domainDetails/buId/{buId}"; //Added by Adavayya
	
	public static final String PAYMENT_INSTRUCTION_DETAIL = "/common/paymentInstuctionDetails/entity/{entityId}"; //Added by Adavayya

	// Added by fareen
	
	public static final String GET_DESIGNATION_DETAILS = "/common/designationdetails"; // added by Rinta
	public static final String GET_ALL_RATE_TYPE ="/common/ratetype"; 
	public static final String GET_INVOICE_RATE_TYPE="/common/invoice/ratetype";
}
