/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  UserService.java           		                 */
/*                                                                   */
/*  Author      :  Anil kumar B K	                                 */
/*                                                                   */
/*  Date        :  09-Jan-2017                                       */
/*                                                                   */
/*  Description :  Service class for employee details		         */
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 09-Jan-2017    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.common.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.thbs.mis.common.bean.DatEmpRoleMappingBOBean;
import com.thbs.mis.common.bean.DatEmpRoleMappingOutputBean;
import com.thbs.mis.common.bean.GsrStatusBean;
import com.thbs.mis.common.bean.UserBean;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.DatEmpRoleMappingBO;
import com.thbs.mis.common.bo.UserRolesBO;
import com.thbs.mis.common.bo.UsersBO;
import com.thbs.mis.common.dao.DatEmpRoleMappingRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.common.dao.UserRolesRepository;
import com.thbs.mis.common.dao.UsersRepository;
import com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO;
import com.thbs.mis.confirmation.dao.ConfirmationCommonRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.DateTimeUtil;
import com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO;
import com.thbs.mis.gsr.bo.GsrDatEmpGoalBO;
import com.thbs.mis.gsr.bo.GsrSlabBO;
import com.thbs.mis.gsr.constants.GsrConstants;
import com.thbs.mis.gsr.dao.GSRDelegatedGoalRepository;
import com.thbs.mis.gsr.dao.GSREmployeegoalsRepository;
import com.thbs.mis.gsr.dao.GSRFinalRatingRepository;
import com.thbs.mis.gsr.dao.GsrRatingSpecification;
import com.thbs.mis.gsr.dao.GsrSlabRepository;
import com.thbs.mis.gsr.service.GSRGoalService;
import com.thbs.mis.legacymisdbSync.bean.MisUser;

@Service
@Transactional
public class UserService {

	/**
	 * Instance of <code>LOG</code>
	 */
	private static final AppLog LOG = LogFactory.getLog(UserService.class);

	
	
	/**
	 * Instance of <code>DatEmpDetailsRepository</code>
	 */
	@Autowired
	private EmpDetailRepository datEmpDetailsRepository;

	/**
	 * Instance of <code>DatEmpPersonalDetailsRepository</code>
	 */
	@Autowired
	private EmployeePersonalDetailsRepository datEmpPersonalDetailsRepository;
	
	/**
	 * Instance of <code>DatEmpProfessionalDetailsRepository</code>
	 */
	@Autowired
	private EmployeeProfessionalRepository datEmpProfessionalDetailsRepository;
	
	/**
	 * Instance of <code>DatEmpProfessionalDetailsRepository</code>
	 */
	@Autowired
	private UsersRepository usersRepository;
	
	/**
	 * Instance of <code>DatEmpProfessionalDetailsRepository</code>
	 */
	@Autowired
	private UserRolesRepository userRolesRepository;
	
	/**
	 * Instance of <code>GSREmployeegoalsRepository</code>
	 */
	@Autowired
	private GSREmployeegoalsRepository gsrGoalRepository;

	/**
	 * Instance of <code>GsrSlabRepository</code>
	 */
	@Autowired
	private GsrSlabRepository gsrSlabRepository;
	
	/**
	 * Instance of <code>GSRGoalService</code>
	 */
	@Autowired
	private GSRGoalService gsrGoalService;
	
	@Autowired
	private GSRDelegatedGoalRepository gsrDelegatedGoalRepository;
	
	/**
	 * Instance of <code>DatEmpRoleMappingRepository</code>
	 */
	//Added for user role details
	@Autowired
	private DatEmpRoleMappingRepository empRoleRepository;
	
	@Autowired
	private ConfirmationCommonRepository confirmRepo;
	
	@Autowired
	private GSRFinalRatingRepository finalRatingRepo;
	
	@Value("${legacy.service.url}")
	private String legacyUrl;
	
	/**
	 * <Description addUser:> This method is used to create user profile in MIS
	 * 
	 * @param user
	 * @return Boolean
	 * @throws DataAccessException
	 */
	public Boolean saveUser(UserBean user) throws DataAccessException {
		LOG.entering("saveUser");
		DatEmpDetailBO empBo = new DatEmpDetailBO();
		DatEmpPersonalDetailBO personalBo = new DatEmpPersonalDetailBO();
		DatEmpProfessionalDetailBO professionalBo = new DatEmpProfessionalDetailBO();
		UsersBO usersBO = new UsersBO();
		UserRolesBO userRolesBO = new UserRolesBO();
		Boolean success = false;
	
		try {
			empBo.setPkEmpId(user.getPkEmpId());
			empBo.setAcceptedThbsPolicyFlag(user.getAcceptedThbsPolicyFlag());
			//empBo.setEmpPwdUpdatedOn(user.getEmpPwdUpdatedOn());
			empBo.setEmpType(user.getEmpType());
			empBo.setFkEmpMainStatus(user.getFkEmpMainStatus());
			empBo.setFkEmpRoleId(getRole(user.getFkEmpRoleId()));
			
			empBo.setFkEmpSubStatus(user.getFkEmpSubStatus());
			empBo.setModifiedBy(user.getModifiedBy());
			//empBo.setPolicyAcceptedDate(user.getProfileCreatedDate());
			empBo.setModifiedDate(user.getModifiedDate());
			empBo.setProfileCreatedBy(user.getProfileCreatedBy());
			empBo.setProfileCreatedDate(user.getProfileCreatedDate());

			personalBo.setEmpAadharNumber(user.getEmpAadharNumber());
			//personalBo.setEmpAlternateMobileNo(user.getEmpAlternateMobileNo());
			personalBo.setEmpBloodGroup(user.getEmpBloodGroup());
			personalBo.setEmpCitizenshipOne(user.getEmpCitizenshipOne());
			personalBo.setEmpCitizenshipStatus(user.getEmpCitizenshipStatus());
			personalBo.setEmpCitizenshipTwo(user.getEmpCitizenshipTwo());
			personalBo.setEmpDateOfBirth(user.getEmpDateOfBirth());
			personalBo.setEmpFirstName(user.getEmpFirstName());
			personalBo.setEmpGender(user.getEmpGender());
			personalBo.setEmpLastName(user.getEmpLastName());
			personalBo.setEmpMiddleName(user.getEmpMiddleName());
			personalBo.setEmpMobileNo(user.getEmpMobileNo());
			personalBo.setEmpPermanentAddressLineOne(user.getEmpPermanentAddress());
			personalBo.setEmpPersonalEmailId(user.getEmpPersonalEmailId());
			personalBo.setEmpPresentAddressLineOne(user.getEmpPresentAddress());
			personalBo.setEmpQualification(user.getEmpQualification());
			personalBo.setEmpSpouseDateOfBirth(user.getEmpSpouseDateOfBirth());
			personalBo.setFkEmpDetailId(user.getFkEmpDetailId());

			professionalBo.setEffectiveDateOfReportingMgrChange(user
					.getEffectiveDateOfReportingMgrChange());
			professionalBo.setEmpConfirmationGraduateFlag(user
					.getEmpConfirmationGraduateFlag());
			professionalBo.setEmpContractEndDate(user.getEmpContractEndDate());
			professionalBo.setEmpCostUnit(user.getEmpCostUnit());
			professionalBo.setEmpDateOfJoining(user.getEmpDateOfJoining());
			professionalBo.setEmpExtensionNumber(user.getEmpExtensionNumber());
			professionalBo.setEmpOnBenchFlag(user.getEmpOnBenchFlag());
			professionalBo.setEmpPhoneNumber(user.getEmpPhoneNumber());
			professionalBo.setEmpPreviousExperience(user
					.getEmpPreviousExperience());
			professionalBo.setEmpProfessionalEmailId(user
					.getEmpProfessionalEmailId());
			professionalBo.setFkEmpBootCampId(user.getFkEmpBootCampId());
			professionalBo.setFkEmpBuUnit(user.getFkEmpBuUnit());
			professionalBo.setFkEmpCompetenceMgrId(user
					.getFkEmpCompetenceMgrId());
			professionalBo.setFkEmpDesignation(user.getFkEmpDesignation());
			professionalBo.setFkEmpDomainMgrId(user.getFkEmpDomainMgrId());
			professionalBo.setFkEmpLevel(user.getFkEmpLevel());
			professionalBo.setFkEmpLocationParentId(user
					.getFkEmpLocationParentId());
			professionalBo.setFkEmpLocationPhysicalId(user
					.getFkEmpLocationPhysicalId());
			professionalBo
					.setFkEmpOrganizationId(user.getFkEmpOrganizationId());
			professionalBo
					.setFkEmpReportingMgrId(user.getFkEmpReportingMgrId());
			professionalBo.setFkMainEmpDetailId(user.getFkMainEmpDetailId());
			professionalBo
					.setHasReporteesFlag(user.getHasReporteesFlag() == null ? "NO_REPORTEES"
							: "YES_HAS_REPORTEES");
			professionalBo.setConsultantReferenceEmpId(user.getConsultantReferenceEmpId());
			usersBO.setUserName(user.getPkEmpId());
			usersBO.setPassWord(user.getEmpProfessionalEmailId());
			usersBO.setEnabled((short)1);
			
			userRolesBO.setRole(user.getRole());
			userRolesBO.setRoleId(empBo.getFkEmpRoleId());
			userRolesBO.setUserName(user.getPkEmpId());
			
			DatEmpDetailBO datEmpDetails = datEmpDetailsRepository
					.findByPkEmpId(empBo.getPkEmpId());
			
			//Need to check this condition
			if (datEmpDetails != null) {

				throw new CommonCustomException("Employee already exist's");
			} else {

				datEmpDetailsRepository.save(empBo);
			}

			datEmpPersonalDetailsRepository.save(personalBo);

			
			
			
			DatEmpProfessionalDetailBO mgrEmpDetails = datEmpProfessionalDetailsRepository
					.findByFkMainEmpDetailId(user.getFkEmpReportingMgrId());
			if(mgrEmpDetails!=null){
			if (mgrEmpDetails.getHasReporteesFlag().equals(
					"NO_REPORTEES")) {

				mgrEmpDetails.setHasReporteesFlag("YES_HAS_REPORTEES");
				datEmpProfessionalDetailsRepository.save(mgrEmpDetails);
			}
			}else{
				throw new CommonCustomException("Reporting manager data does't exist's");
			}
			datEmpProfessionalDetailsRepository.save(professionalBo);

			usersRepository.save(usersBO);
			userRolesRepository.save(userRolesBO);
			success = true;
		} catch (Exception e) {
			throw new DataAccessException("Exception occured at addUser ", e);
		}
		LOG.endUsecase("saveUser");
		return success;

	}

	/**
	 * <Description addUser:> This method is used to update user profile in MIS
	 * 
	 * @param user
	 * @return Boolean
	 * @throws DataAccessException
	 */
	public Boolean updateUser(UserBean user) throws DataAccessException {
		LOG.entering("updateUser");
		DatEmpDetailBO empBo = new DatEmpDetailBO();
		DatEmpPersonalDetailBO personalBo = new DatEmpPersonalDetailBO();
		DatEmpProfessionalDetailBO professionalBo = new DatEmpProfessionalDetailBO();
		UsersBO usersBO = new UsersBO();
		UserRolesBO userRolesBO = new UserRolesBO();
		Boolean success = false;
		try {
			empBo.setPkEmpId(user.getPkEmpId());
			empBo.setAcceptedThbsPolicyFlag(user.getAcceptedThbsPolicyFlag());
			empBo.setEmpPwdUpdatedOn(user.getEmpPwdUpdatedOn());
			empBo.setEmpType(user.getEmpType());
			empBo.setFkEmpMainStatus(user.getFkEmpMainStatus());
			empBo.setFkEmpRoleId(getRole(user.getFkEmpRoleId()));
			empBo.setFkEmpSubStatus(user.getFkEmpSubStatus());
			empBo.setPolicyAcceptedDate(user.getPolicyAcceptedDate());
			

			personalBo.setEmpAadharNumber(user.getEmpAadharNumber());
			personalBo.setEmpAlternateMobileNo(user.getEmpAlternateMobileNo());
			personalBo.setEmpBloodGroup(user.getEmpBloodGroup());
			personalBo.setEmpCitizenshipOne(user.getEmpCitizenshipOne());
			personalBo.setEmpCitizenshipStatus(user.getEmpCitizenshipStatus());
			personalBo.setEmpCitizenshipTwo(user.getEmpCitizenshipTwo());
			personalBo.setEmpDateOfBirth(user.getEmpDateOfBirth());
			personalBo.setEmpFirstName(user.getEmpFirstName());
			personalBo.setEmpGender(user.getEmpGender());
			personalBo.setEmpLastName(user.getEmpLastName());
			personalBo.setEmpMiddleName(user.getEmpMiddleName());
			personalBo.setEmpMobileNo(user.getEmpMobileNo());
			personalBo.setEmpPermanentAddressLineOne(user.getEmpPermanentAddress());
			personalBo.setEmpPersonalEmailId(user.getEmpPersonalEmailId());
			personalBo.setEmpPresentAddressLineOne(user.getEmpPresentAddress());
			personalBo.setEmpQualification(user.getEmpQualification());
			personalBo.setEmpSpouseDateOfBirth(user.getEmpSpouseDateOfBirth());
			personalBo.setFkEmpDetailId(user.getFkEmpDetailId());
		
			
		
			
			professionalBo.setEmpConfirmationGraduateFlag(user
					.getEmpConfirmationGraduateFlag());
			professionalBo.setEmpContractEndDate(user.getEmpContractEndDate());
			professionalBo.setEmpCostUnit(user.getEmpCostUnit());
			professionalBo.setEmpDateOfJoining(user.getEmpDateOfJoining());
			professionalBo.setEmpExtensionNumber(user.getEmpExtensionNumber());
			professionalBo.setEmpOnBenchFlag(user.getEmpOnBenchFlag());
			professionalBo.setEmpPhoneNumber(user.getEmpPhoneNumber());
			professionalBo.setEmpPreviousExperience(user
					.getEmpPreviousExperience());
			professionalBo.setEmpProfessionalEmailId(user
					.getEmpProfessionalEmailId());
			professionalBo.setFkEmpBootCampId(user.getFkEmpBootCampId());
			professionalBo.setFkEmpBuUnit(user.getFkEmpBuUnit());
			professionalBo.setFkEmpCompetenceMgrId(user
					.getFkEmpCompetenceMgrId());
			professionalBo.setFkEmpDesignation(user.getFkEmpDesignation());
			professionalBo.setFkEmpDomainMgrId(user.getFkEmpDomainMgrId());
			professionalBo.setFkEmpLevel(user.getFkEmpLevel());
			professionalBo.setFkEmpLocationParentId(user
					.getFkEmpLocationParentId());
			professionalBo.setFkEmpLocationPhysicalId(user
					.getFkEmpLocationPhysicalId());
			professionalBo
					.setFkEmpOrganizationId(user.getFkEmpOrganizationId());
			professionalBo
					.setFkEmpReportingMgrId(user.getFkEmpReportingMgrId());
			professionalBo.setFkMainEmpDetailId(user.getFkMainEmpDetailId());
			professionalBo
					.setHasReporteesFlag(user.getHasReporteesFlag() == null ? "NO_REPORTEES"
							: "YES_HAS_REPORTEES");
			
			professionalBo.setEmpLastWorkingDate(user.getEmpLastWorkingDate());
			professionalBo.setConsultantReferenceEmpId(user.getConsultantReferenceEmpId());
			
			DatEmpDetailBO datEmpDetails = datEmpDetailsRepository
					.findByPkEmpId(empBo.getPkEmpId());
			if (datEmpDetails != null) {
				empBo.setProfileCreatedBy(datEmpDetails.getProfileCreatedBy());
				empBo.setProfileCreatedDate(datEmpDetails
						.getProfileCreatedDate());
				if(user.getRequestType()!=null){
				if(user.getRequestType().equalsIgnoreCase("normalUpdateUser")){
					empBo.setModifiedBy(datEmpDetails.getModifiedBy());
					empBo.setModifiedDate(datEmpDetails.getModifiedDate());
					
				}
				}else{
					empBo.setModifiedBy(user.getModifiedBy());
					empBo.setModifiedDate(user.getModifiedDate());
					
				}
				datEmpDetailsRepository.save(empBo);
			} else {

				throw new CommonCustomException("given employee not exist's");
			}
			DatEmpPersonalDetailBO personal = datEmpPersonalDetailsRepository
					.findByFkEmpDetailId(empBo.getPkEmpId());
			if (personal != null) {
				
				personalBo.setPkEmpPersonalDetailId(personal
						.getPkEmpPersonalDetailId());
				
					personalBo.setEmpPersonalDetailModifiedBy(user.getModifiedBy());
					personalBo.setEmpPersonalDetailModifiedDate(user.getModifiedDate());
				
				
				datEmpPersonalDetailsRepository.save(personalBo);
			} else {
				datEmpPersonalDetailsRepository.save(personalBo);
			}

			DatEmpProfessionalDetailBO professional = datEmpProfessionalDetailsRepository
					.findByFkMainEmpDetailId(empBo.getPkEmpId());

			DatEmpProfessionalDetailBO mgrEmpDetails = datEmpProfessionalDetailsRepository
					.findByFkMainEmpDetailId(user.getFkEmpReportingMgrId());
			
			DatEmpProfessionalDetailBO oldManagerDetail = datEmpProfessionalDetailsRepository
					.findByFkMainEmpDetailId(professional.getFkEmpReportingMgrId());
		
			if (professional != null) {
				professionalBo.setPkEmpProfessionalDetailId(professional
						.getPkEmpProfessionalDetailId());
			
				List<DatEmpProfessionalDetailBO> listOfReporteesForCurrentEmp =  datEmpProfessionalDetailsRepository.findByFkEmpReportingMgrId(empBo.getPkEmpId());
				
				if(user.getRequestType()!=null){
					
				if(user.getRequestType().equalsIgnoreCase("normalUpdateUser")){
					professionalBo.setProfessionalDetailModifiedBy(professional.getProfessionalDetailModifiedBy());
					professionalBo.setProfessionalDetailModifiedDate(professional.getProfessionalDetailModifiedDate());
				
				}
				}else{
					professionalBo.setProfessionalDetailModifiedBy(user.getModifiedBy());
					professionalBo.setProfessionalDetailModifiedDate(user.getModifiedDate());
					
				}
				if(listOfReporteesForCurrentEmp.size() > 0){
					professionalBo.setHasReporteesFlag("YES_HAS_REPORTEES");
				}
				
				if (professional.getFkEmpReportingMgrId() != user
						.getFkEmpReportingMgrId()) {

					if (mgrEmpDetails.getHasReporteesFlag().equals(
							"NO_REPORTEES")) {
						// professionalSpec.reportingManagerChange(user.getPkEmpId());
						mgrEmpDetails.setHasReporteesFlag("YES_HAS_REPORTEES");
						
						datEmpProfessionalDetailsRepository.save(mgrEmpDetails);
					}
					
					professionalBo.setEffectiveDateOfReportingMgrChange(user.getEffectiveDateOfReportingMgrChange());
					datEmpProfessionalDetailsRepository.save(professionalBo);
			
					
					if(oldManagerDetail!=null){
						List<DatEmpProfessionalDetailBO> listOfDetails =  datEmpProfessionalDetailsRepository.findByFkEmpReportingMgrId(oldManagerDetail.getFkMainEmpDetailId());
						
					
						if(listOfDetails.size() <= 0){
							
							oldManagerDetail.setHasReporteesFlag("NO_REPORTEES");
							datEmpProfessionalDetailsRepository.save(oldManagerDetail);
						}
					}
					boolean flag = false;
					flag = goalsCheckForRmChange(user.getPkEmpId(),oldManagerDetail.getFkMainEmpDetailId());
					LOG.info("update user after rm change : "+user.getPkEmpId()+" professional.getFkMainEmpDetailId()"+professional.getFkEmpReportingMgrId());
					LOG.info("user.getFkEmpReportingMgrId() : "+user.getFkEmpReportingMgrId());
					LOG.info("update user after rm change : "+flag);
					if(flag)
					{
					copyGoalsForRMChange(oldManagerDetail.getFkMainEmpDetailId(),user.getFkEmpReportingMgrId(), professional.getFkMainEmpDetailId());
					}
				
					
				}else{

					
				datEmpProfessionalDetailsRepository.save(professionalBo);
				}
			} 

			
			usersBO.setUserName(user.getPkEmpId());
			usersBO.setPassWord(user.getEmpProfessionalEmailId());
			usersBO.setEnabled((short)1);
			usersRepository.save(usersBO);
			
			UserRolesBO userRole =userRolesRepository.findByUserName(user.getPkEmpId());
			if(userRole!=null){
			userRolesBO.setPkUserRoleId(userRole.getPkUserRoleId());
			userRolesBO.setRole(user.getRole());
			userRolesBO.setRoleId(empBo.getFkEmpRoleId());
			userRolesBO.setUserName(user.getPkEmpId());
			userRolesRepository.save(userRolesBO);
			}
			
			success = true;
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception occured while upading user profile : ", e);
		}

		LOG.endUsecase("updateUser");
		return success;

	}

	public Short getRole(int role){
		
		Short newRole;
		switch(role){
		case 2 : 
		case 3 : 
		case 4 :
		case 9 :
		case 14 :
		case 17 :
		case 19 :
		case 20 :
		case 105 :
		case 159 :
		case 176 :
		case 185 : newRole = 4;break; 
	
		case 5 : newRole = 18;break; 
		case 10 : 
		case 11 :
		case 21 :
		case 32 :
		case 116 :
		case 221 :
		case 227 : newRole = 3;break;
		case 22 : newRole = 16;break; 
		case 25 : newRole = 23;break;
		case 88 : newRole = 22;break; 
		case 23 : newRole = 19;break; 
		case 30 : newRole = 13;break; 
		case 31 : newRole = 15;break; 
		case 36 : 
		case 63 : newRole = 8;break; 
		case 43 : newRole = 11;break; 
		case 44 :
		case 49 : newRole = 6;break; 
		case 45 : 
		case 46 : 
		case 93 : newRole = 10;break; 
		case 40 : 
		case 54 :
		case 66 :
		case 129 :
		case 156 :
		case 188 :  
		case 231 : newRole = 2;break;
		case 86 : newRole = 21;break;
		case 47 : newRole = 17;break; 
		case 53 : newRole = 9;break; 
		case 74 : newRole = 25;break; 
		case 101 : 
		case 118 : newRole = 7;break; 
		case 103 : newRole = 15;break; 
		case 128 : newRole = 5;break; 
		case 149 : newRole = 24;break; 
		
		case 192 : newRole = 12;break; 
		default : newRole = 1;break; 
		}
		return newRole;
		
	}

	/**
	 * <Description gsrGoalsCheckForRmChange:> This method is used to check goal status for manager change
	 * 
	 * @param user
	 * @return String
	 * @throws DataAccessException
	 */
	public String gsrGoalsCheckForRmChangeForLegacy(MisUser user) throws DataAccessException {

		String goalStatus = "";
		
		LOG.entering("gsrGoalsCheckForRmChange");
		try {

			Boolean isKpiGoalsClosed = false;
			Boolean isProjectGoalsClosed = false;
			Boolean isGoalsClosed = false;
			Boolean isPrevGoalsClosed = false;
			Boolean middleOfSlab = false;
			Boolean lastTwoWeekOfSlab = false;
			Boolean isDelegatedGoalsClosedInMid = false;
			Boolean isDelegatedGoalsClosedInLast = false;
			Boolean isReporteesKpiGoalsClosed = false;
			Boolean isReporteesProjectGoalsClosed = false;
			Boolean isReporteesGoalsClosed = false;
			
			Specification<GsrSlabBO> specSlab = GsrRatingSpecification
					.getCurrentSlab();
			GsrSlabBO gsrCurrentSlabBo = gsrSlabRepository.findOne(specSlab);

			Short currentSlab = gsrCurrentSlabBo.getPkGsrSlabId();

			Short prevSlab = (short) (currentSlab - 1);

			GsrSlabBO gsrPrevSlabBo = gsrSlabRepository
					.findByPkGsrSlabId(prevSlab);

			Calendar cal = Calendar.getInstance();
			Date lastDayOfSlab = gsrCurrentSlabBo.getGsrSlabEndDate();
			Date startDayOfSlab = gsrCurrentSlabBo.getGsrSlabStartDate();
			cal.setTime(gsrCurrentSlabBo.getGsrSlabEndDate());
			cal.add(Calendar.WEEK_OF_MONTH, -2);
			cal.add(Calendar.DAY_OF_MONTH, +1);
			Date lastTwoWeekStartDay = cal.getTime();

			cal.add(Calendar.DAY_OF_MONTH, -1);

			Date lastTwoWeekEndDay = cal.getTime();

			Date todayDate = DateTimeUtil.dateWithMinTimeofDay(new Date());
			Date endDate = DateTimeUtil.dateWithMinTimeofDay(lastTwoWeekEndDay);

			if (new Date().getTime() >= startDayOfSlab.getTime()
					&& todayDate.getTime() <= endDate.getTime()) {
				middleOfSlab = true;
			} else {
				lastTwoWeekOfSlab = true;
			}
	
			List<DatEmpProfessionalDetailBO> listOfReporteesBo = datEmpProfessionalDetailsRepository.findByFkEmpReportingMgrId(user.getEmpId());
			
			List<GsrDatEmpGoalBO> listOfReporteesGoals = new ArrayList<GsrDatEmpGoalBO>();
			List<Integer> listOfReportees = new ArrayList<Integer>();
			if(listOfReportees != null){
			
				for(DatEmpProfessionalDetailBO empDetails : listOfReporteesBo){
					listOfReportees.add(empDetails.getFkMainEmpDetailId());
				}
			}
		
			 listOfReporteesGoals = gsrGoalRepository
						.findByFkGsrDatEmpIdInAndFkGsrDatMgrIdAndFkGsrSlabId(
								listOfReportees, user.getEmpId(),
								currentSlab);
			
			List<GsrDatEmpGoalBO> listOfCurrentGoals = gsrGoalRepository
					.findByFkGsrDatEmpIdAndFkGsrDatMgrIdAndFkGsrSlabId(
							user.getEmpId(), user.getReportingMgrId(),
							currentSlab);
			List<GsrDatEmpGoalBO> listOfPrevGoals = gsrGoalRepository
					.findByFkGsrDatEmpIdAndFkGsrDatMgrIdAndFkGsrSlabId(
							user.getEmpId(), user.getReportingMgrId(),
							prevSlab);
			List<GsrDatEmpGoalBO> listOfDelegatedGoals = gsrGoalRepository.findByFkGsrDatEmpIdAndFkGsrDelegatedGoalIdIsNotNull(user.getEmpId());
		
			if(listOfDelegatedGoals.size() > 0){
				for(GsrDatEmpGoalBO goal : listOfDelegatedGoals){
					
					if (middleOfSlab) {
						
					
					if (goal.getFkGsrGoalStatus() != GsrConstants.GSR_MANAGER_FEEDBACK_PROVIDED
							&& goal.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
							&& goal.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
							&& goal.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED) {
						isDelegatedGoalsClosedInMid = true;
					}
					}else if(lastTwoWeekOfSlab){
						if (goal.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
								&& goal.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
								&& goal.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED) {
							isDelegatedGoalsClosedInLast = true;
						}
					}
				}
			}
			
		/*
			//To check reportees goals status
			if (listOfReporteesGoals.size() > 0) {
				for (GsrDatEmpGoalBO goalsBo : listOfReporteesGoals) {
					if (middleOfSlab) {
						if (goalsBo.getEmpGoalKpiWeightageId() != null
								&& goalsBo.getEmpGoalKpiWeightageId() != 0) {
							// KPI goals check
							if (goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_MANAGER_FEEDBACK_PROVIDED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED) {
								isReporteesKpiGoalsClosed = true;

							}

						} else {
							// Project goals check
							if (goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RATED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED) {
								isReporteesProjectGoalsClosed = true;

							}

						}

					} else if (lastTwoWeekOfSlab) {

						if (goalsBo.getEmpGoalKpiWeightageId() != null
								&& goalsBo.getEmpGoalKpiWeightageId() != 0) {
							// KPI goals check
							if (goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED) {

								isReporteesGoalsClosed = true;
							}

						} else {
							// Project goals check
							if (goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED) {
								isReporteesGoalsClosed = true;

							}

						}

					}

				}

			}*/
			
			/*
			if(isReporteesKpiGoalsClosed || isReporteesProjectGoalsClosed || isReporteesGoalsClosed){
				
				goalStatus = "Your reportees Goals are not closed";
			}
			else{*/
				
			//to check employee current goals status
			if (listOfCurrentGoals.size() > 0) {
				for (GsrDatEmpGoalBO goalsBo : listOfCurrentGoals) {
					if (middleOfSlab) {
						if (goalsBo.getEmpGoalKpiWeightageId() != null
								&& goalsBo.getEmpGoalKpiWeightageId() != 0) {
							// KPI goals check
							if (goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_MANAGER_FEEDBACK_PROVIDED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED) {
								isKpiGoalsClosed = true;

							}

						} else {
							// Project goals check
							if (goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RATED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED) {
								isProjectGoalsClosed = true;

							}

						}

					} else if (lastTwoWeekOfSlab) {

						if (goalsBo.getEmpGoalKpiWeightageId() != null
								&& goalsBo.getEmpGoalKpiWeightageId() != 0) {
							// KPI goals check
							if (goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED) {

								isGoalsClosed = true;
							}

						} else {
							// Project goals check
							if (goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED) {
								isGoalsClosed = true;

							}

						}

					}

				}

			}
			
			//to check employee previous goals status
			if(listOfPrevGoals.size() > 0){
				for (GsrDatEmpGoalBO goalsBo : listOfPrevGoals) {
					if (goalsBo.getEmpGoalKpiWeightageId() != null
							&& goalsBo.getEmpGoalKpiWeightageId() != 0) {
						// KPI goals check
						if (goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
								&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
								&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED) {

							isPrevGoalsClosed = true;
						}

					} else {
						// Project goals check
						if (goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
								&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
								&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED) {
							isPrevGoalsClosed = true;

						}

					}
				}
			}
			//}
			
			List<String> listOfStatus =new ArrayList<String>();
		
			
			
			if(isKpiGoalsClosed){
				goalStatus = "Your current Reporting Manager not provided the feedback";
				listOfStatus.add(goalStatus);
			} if(isProjectGoalsClosed){
				
				if(goalStatus.isEmpty()){
					goalStatus = "Goals are not rated";
				}else{
				goalStatus = goalStatus +","+ " Goals are not rated";
				}
				listOfStatus.add(goalStatus);
			} if(isGoalsClosed){
				goalStatus = "Goals are not closed";
				listOfStatus.add(goalStatus);
			}
			if(isPrevGoalsClosed){
				goalStatus = goalStatus +","+ "Previous gsr goals are not closed";
				listOfStatus.add(goalStatus);
			}
			
			if(isDelegatedGoalsClosedInMid){
				goalStatus = "Delegated manager not provided the feedback";
				listOfStatus.add(goalStatus);
			}
			if(isDelegatedGoalsClosedInLast){
				goalStatus = "Delegated manager not closed the goals";
				listOfStatus.add(goalStatus);
			}
			
			goalStatus = String.join(",", listOfStatus);
			
			LOG.info("goalStatus: "+goalStatus);
			
		} catch (Exception e) {
			
			
			e.printStackTrace();
			 throw new DataAccessException("Exception occured at gsrGoalsCheckForRmChange : "+e.getMessage());
		}
		LOG.exiting("gsrGoalsCheckForRmChange goalStatus"+goalStatus);
		return goalStatus;

	}
	
	
	/**
	 * <Description gsrGoalsCheckForRmChange:> This method is used to check goal status for manager change
	 * 
	 * @param user
	 * @return String
	 * @throws DataAccessException
	 */
	public List<GsrStatusBean> gsrGoalsCheckForRmChange(MisUser user) throws DataAccessException {

	
		LOG.entering("gsrGoalsCheckForRmChange");
		List<GsrStatusBean> listOfGsrStatusBean = new ArrayList<GsrStatusBean>();
		try {

		
			Boolean isGoalsClosed = false;
			Boolean isPrevGoalsClosed = false;
			Boolean isReporteesGoalsClosed = false;
			Boolean isEmployeeFinalRatingClosed = false;
			
			Specification<GsrSlabBO> specSlab = GsrRatingSpecification
					.getCurrentSlab();
			GsrSlabBO gsrCurrentSlabBo = gsrSlabRepository.findOne(specSlab);

			Short currentSlab = gsrCurrentSlabBo.getPkGsrSlabId();

			
			Calendar cal = Calendar.getInstance();
	
			cal.setTime(gsrCurrentSlabBo.getGsrSlabEndDate());
			cal.add(Calendar.WEEK_OF_MONTH, -2);
			cal.add(Calendar.DAY_OF_MONTH, +1);
		

			cal.add(Calendar.DAY_OF_MONTH, -1);

	
			List<DatEmpProfessionalDetailBO> listOfReporteesBo = datEmpProfessionalDetailsRepository.findByFkEmpReportingMgrId(user.getEmpId());
			
			DatEmpDetailBO empDetails = datEmpDetailsRepository.findByPkEmpId(user.getEmpId());
			
			GsrStatusBean reporteeGoalBean = new GsrStatusBean();
			GsrStatusBean legacyReporteeGoalBean = new GsrStatusBean();
	
			
			if(empDetails.getEmpType() == 1){
				ConfDatProbationConfirmationBO confirmationBo = confirmRepo.findByFkEmpId(user.getEmpId());
				if(confirmationBo !=null){
					if(confirmationBo.getFkConfStatusId() < 5){
						reporteeGoalBean.setEmpSelfGoalClosed("NA");
					}
				}	
			}else if(empDetails.getEmpType() == 2){
				reporteeGoalBean.setEmpSelfGoalClosed("NA");
			}else if(empDetails.getEmpType() == 3){
				reporteeGoalBean.setEmpSelfGoalClosed("NA");
			}else if(empDetails.getEmpType() == 4){
				reporteeGoalBean.setEmpSelfGoalClosed("NA");
			}
			
			List<GsrDatEmpGoalBO> listOfReporteesGoals = new ArrayList<GsrDatEmpGoalBO>();
			List<Integer> listOfReportees = new ArrayList<Integer>();
			
			if(listOfReporteesBo.size() > 0){
				
				for(DatEmpProfessionalDetailBO proDetails : listOfReporteesBo){
					listOfReportees.add(proDetails.getFkMainEmpDetailId());
				}
				
				
				 listOfReporteesGoals = gsrGoalRepository
							.findByFkGsrDatEmpIdInAndFkGsrDatMgrIdAndFkGsrSlabId(
									listOfReportees, user.getEmpId(),
									currentSlab);
				 
				 if (listOfReporteesGoals.size() > 0) {
						for (GsrDatEmpGoalBO goalsBo : listOfReporteesGoals) {
							if( goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
									&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED){
								isReporteesGoalsClosed = true;
							}	
						
						}
					}
				 
				 if(isReporteesGoalsClosed){
						
						//goalStatus = "Your reportees Goals are not closed";
						reporteeGoalBean.setReporteesGoalClosed("No");
					
					}else{
						//goalStatus = "Your reportees Goals are closed";
						reporteeGoalBean.setReporteesGoalClosed("Yes");
						
						
					}
				
			}else{
				
				reporteeGoalBean.setReporteesGoalClosed("NA");	
				
			}
		
			
			List<GsrDatEmpGoalBO> listOfCurrentGoals = gsrGoalRepository
					.findByFkGsrDatEmpIdAndFkGsrDatMgrIdAndFkGsrSlabId(
							user.getEmpId(), user.getReportingMgrId(),
							currentSlab);
			List<GsrDatEmpGoalBO> listOfPrevGoals = gsrGoalRepository
					.findByFkGsrDatEmpIdAndFkGsrDatMgrIdAndFkGsrSlabIdNotIn(
							user.getEmpId(), user.getReportingMgrId(),
							currentSlab);
			List<GsrDatEmpGoalBO> listOfDelegatedGoals = gsrGoalRepository.findByFkGsrDatEmpIdAndFkGsrDelegatedGoalIdIsNotNull(user.getEmpId());
		
			if(listOfPrevGoals.size() > 0){
				for (GsrDatEmpGoalBO goalsBo : listOfPrevGoals) {
					if (goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
							&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
							&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED) {

						isPrevGoalsClosed = true;
					}	
				}
				
			}
			
			
			if (listOfCurrentGoals.size() > 0) {
				for (GsrDatEmpGoalBO goalsBo : listOfCurrentGoals) {
					if(goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_CLOSED
							&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_RESOLVED
							&& goalsBo.getFkGsrGoalStatus() != GsrConstants.GSR_REJECTED){
						isGoalsClosed = true;
					}
				}
			}
		
			List<GsrDatEmpFinalRatingBO> listOffinalRatingBo = finalRatingRepo.findByFkGsrFinalRatingEmpIdAndFkGsrFinalRatingMgrId(user.getEmpId(),user.getReportingMgrId());
			
			if(listOffinalRatingBo.size() > 0){
			for(GsrDatEmpFinalRatingBO finalRating : listOffinalRatingBo){
				if(finalRating.getFinalRatingStatus() != 2 && finalRating.getFinalRatingStatus() != 4){
					isEmployeeFinalRatingClosed = true;
				}
			}
			}
			
			if(reporteeGoalBean.getEmpSelfGoalClosed()!=null){
		 if(!reporteeGoalBean.getEmpSelfGoalClosed().equalsIgnoreCase("NA")){
			
			if(isPrevGoalsClosed || isGoalsClosed || isEmployeeFinalRatingClosed){
				reporteeGoalBean.setEmpSelfGoalClosed("No");
				
			}else{
				reporteeGoalBean.setEmpSelfGoalClosed("Yes");
				
			}
			}
		
		 
		}else{
			
			
				if((isPrevGoalsClosed || isGoalsClosed) || isEmployeeFinalRatingClosed){
					reporteeGoalBean.setEmpSelfGoalClosed("No");
					
				}else{
					reporteeGoalBean.setEmpSelfGoalClosed("Yes");
					
				}
			}
			
		
			
			
			//Legacy GSR goal check
			RestTemplate rt = new RestTemplate();
		
			String url = legacyUrl+"legacyGsr/gsrgoalstatus/empId/{empId}/slabId/{slabId}";
			try{
				LOG.info("@@@@@@@@@url : " +url);
				Map<String, Object> params = new HashMap<>();
				params.put("empId", user.getEmpId());
				params.put("slabId", currentSlab);
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				
				legacyReporteeGoalBean = rt.getForObject(url, GsrStatusBean.class,params);
			
				
				
			}catch(ResourceAccessException r){
				 LOG.error("ResourceAccessException error:  " + r.toString());
			}
			 catch (HttpClientErrorException e)
	        {
	           
	            LOG.error("HttpClientErrorException error:  " + e.toString());
	        }
			
			if(legacyReporteeGoalBean!=null){
				reporteeGoalBean.setEmpLegacySelfGoalClosed(legacyReporteeGoalBean.getEmpLegacySelfGoalClosed());
				reporteeGoalBean.setReporteesLegacyGoalClosed(legacyReporteeGoalBean.getReporteesLegacyGoalClosed());
			}
			
			
			
			//listOfGsrStatusBean.add(selfGoalBean);
			listOfGsrStatusBean.add(reporteeGoalBean);
			
			
			
			
			
		} catch(Exception e) {
			
			
			e.printStackTrace();
			 throw new DataAccessException("Exception occured at gsrGoalsCheckForRmChange : "+e.getMessage());
		}
		LOG.exiting("gsrGoalsCheckForRmChange goalStatus"+listOfGsrStatusBean);
		return listOfGsrStatusBean;

	}

	/**
	 * <Description copyGoalsForRMChange:> This method is used to copy goals from current manager to changing manager
	 * 
	 * @param curRmId,newRmId,empId
	 * @return 
	 * @throws DataAccessException
	 */
	public List<Integer> copyGoalsForRMChange(Integer curRmId, Integer newRmId, Integer empId) throws DataAccessException{
		
		LOG.info("Entering copyGoalsForRMChange ");
	
		List<Integer> listOfNotAcceptedGoalId = new ArrayList<Integer>();
		try{
			
			Specification<GsrSlabBO> specSlab = GsrRatingSpecification
					.getCurrentSlab();
		
			GsrSlabBO gsrCurrentSlabBo = gsrSlabRepository.findOne(specSlab);
		
			
			Short currentSlab = gsrCurrentSlabBo.getPkGsrSlabId();
		
			
			
			
		List<GsrDatEmpGoalBO> listOfEmpGoals = gsrGoalRepository.findByFkGsrDatEmpIdAndFkGsrDatMgrIdAndFkGsrSlabId(empId,curRmId,currentSlab);
		/*listOfCopyGoals = gsrGoalRepository.findByFkGsrDatEmpIdAndFkGsrDatMgrIdAndFkGsrSlabId(empId,curRmId,currentSlab);*/
		DatEmpProfessionalDetailBO professional = datEmpProfessionalDetailsRepository.findByFkMainEmpDetailId(newRmId);
		
		

		
		if(listOfEmpGoals.size() > 0){
			
			GsrDatEmpGoalBO	gsrGoal;
		for(GsrDatEmpGoalBO goals : listOfEmpGoals){	
			gsrGoal = new GsrDatEmpGoalBO(); 
			
			if(goals.getEmpGoalKpiWeightageId()!=null && goals.getEmpGoalKpiWeightageId()!=0){
				
				
				if(goals.getFkGsrGoalStatus() == GsrConstants.GSR_MANAGER_FEEDBACK_PROVIDED){
				
				
				
				gsrGoal.setFkGsrDatEmpId(goals.getFkGsrDatEmpId());
				gsrGoal.setFkGsrDatMgrId(newRmId);
				gsrGoal.setFkGsrGoalStatus(GsrConstants.GSR_ACCOMPLISHMENTS_ENTERED);
				
				gsrGoal.setFkGsrSlabId(goals.getFkGsrSlabId());
				if(professional!=null){
				gsrGoal.setFkEmpGoalBuId(professional.getFkEmpBuUnit());
				gsrGoal.setFkEmpGoalDomainMgrId(professional.getFkEmpDomainMgrId());
				}
				gsrGoal.setEmpGoalKpiWeightageId(goals.getEmpGoalKpiWeightageId());
				gsrGoal.setGsrGoalWeightage(goals.getGsrGoalWeightage());
				gsrGoal.setGsrDatEmpGoalName(goals.getGsrDatEmpGoalName());
				gsrGoal.setGsrGoalClosedBy(goals.getGsrGoalClosedBy());
				gsrGoal.setGsrGoalCreatedBy(empId);
				gsrGoal.setGsrGoalCreatedDate(new Date());
				gsrGoal.setGsrGoalAccomplishment(goals.getGsrGoalAccomplishment());
				gsrGoal.setGsrGoalDescription(goals.getGsrGoalDescription());
				gsrGoal.setGsrGoalMeasurement(goals.getGsrGoalMeasurement());
				gsrGoal.setGsrGoalEndDate(goals.getGsrGoalEndDate());
				gsrGoal.setGsrGoalMgrRating(goals.getGsrGoalMgrRating());
				gsrGoal.setGsrGoalModifiedBy(null);
				gsrGoal.setGsrGoalModifiedDate(null);
				gsrGoal.setGsrGoalSelfRating(goals.getGsrGoalSelfRating());
				gsrGoal.setGsrGoalStartDate(goals.getGsrGoalStartDate());
				gsrGoalRepository.save(gsrGoal);
				}
			}
				
		}
	/*	
		for(GsrDatEmpGoalBO goal : listOfCopyGoals){
			
			if(goal.getFkGsrDelegatedGoalId() !=null){
				
				listOfGoalId.add(goal.getFkGsrDelegatedGoalId());
			//goal.setFkGsrDelegatedGoalId(null);
			}
			
		}*/
		
		
		
		/*if(listOfGoalId.size() > 0){
			
			listOfDelegatedGoals = gsrDelegatedGoalRepository.findByPkGsrDelegatedGoalIdIn(listOfGoalId);
			
			for(GsrDatEmpDelegatedGoalBO delegatedGoals : listOfDelegatedGoals)
			{
				if(delegatedGoals.getDelegationStatus() == 1)
				{
					listOfNotAcceptedGoalId.add(delegatedGoals.getPkGsrDelegatedGoalId());
				}
			}
		
		}*/
		
		/*LOG.info("listOfNotAcceptedGoalId : "+listOfNotAcceptedGoalId);

		for(GsrDatEmpGoalBO goal : listOfCopyGoals){
			
			for(Integer delegatedId : listOfNotAcceptedGoalId){
			if(delegatedId == goal.getFkGsrDelegatedGoalId()){
				LOG.info("goal.getFkGsrDelegatedGoalId() before "+goal.getFkGsrDelegatedGoalId());
				goal.setFkGsrDelegatedGoalId(null);
				LOG.info("goal.getFkGsrDelegatedGoalId() after "+goal.getFkGsrDelegatedGoalId());
				GsrDatEmpGoalBO goalBo = gsrGoalRepository.save(goal);
				LOG.info("goal.getFkGsrDelegatedGoalId() after save "+goalBo);
				if(goalBo.getFkGsrDelegatedGoalId() ==null){
					LOG.info("delegatedId : "+delegatedId);
					//gsrDelegatedGoalRepository.deleteByPkGsrDelegatedGoalId(delegatedId);
					listOfGoalsToDelete.add(delegatedId);
				}
			}
			}
		}*/
		}
	

	
		LOG.info("Existing copyGoalsForRMChange ");
		
		}catch(Exception e){
			
			 throw new DataAccessException("Exception occured at copyGoalsForRMChange"+e.getMessage());
		}
return listOfNotAcceptedGoalId;
		
	}
	
	
	/**
	 * <Description gsrGoalsCheckForRmChange:> This method is used to check goal status for manager change
	 * 
	 * @param user
	 * @return String
	 * @throws DataAccessException
	 */
	public boolean goalsCheckForRmChange(Integer empId,Integer rmId) throws DataAccessException {

		String goalStatus = "";
		Boolean isKpiGoalsClosed = false;
		LOG.entering("goalsCheckForRmChange123 empId"+empId +" rmId : "+rmId);
		try {

		
		
			Boolean middleOfSlab = false;
			Boolean lastTwoWeekOfSlab = false;
			
			Specification<GsrSlabBO> specSlab = GsrRatingSpecification
					.getCurrentSlab();
			GsrSlabBO gsrCurrentSlabBo = gsrSlabRepository.findOne(specSlab);

			Short currentSlab = gsrCurrentSlabBo.getPkGsrSlabId();

			Short prevSlab = (short) (currentSlab - 1);

			GsrSlabBO gsrPrevSlabBo = gsrSlabRepository
					.findByPkGsrSlabId(prevSlab);
			LOG.info("goalsCheckForRmChange :currentSlab "+currentSlab );
			Calendar cal = Calendar.getInstance();
			Date lastDayOfSlab = gsrCurrentSlabBo.getGsrSlabEndDate();
			Date startDayOfSlab = gsrCurrentSlabBo.getGsrSlabStartDate();
			cal.setTime(gsrCurrentSlabBo.getGsrSlabEndDate());
			cal.add(Calendar.WEEK_OF_MONTH, -2);
			cal.add(Calendar.DAY_OF_MONTH, +1);
			Date lastTwoWeekStartDay = cal.getTime();

			cal.add(Calendar.DAY_OF_MONTH, -1);

			Date lastTwoWeekEndDay = cal.getTime();

			Date todayDate = DateTimeUtil.dateWithMinTimeofDay(new Date());
			Date endDate = DateTimeUtil.dateWithMinTimeofDay(lastTwoWeekEndDay);

			if (new Date().getTime() >= startDayOfSlab.getTime()
					&& todayDate.getTime() <= endDate.getTime()) {
				middleOfSlab = true;
			} else {
				lastTwoWeekOfSlab = true;
			}
			LOG.info("goalsCheckForRmChange :middleOfSlab "+middleOfSlab );
			List<GsrDatEmpGoalBO> listOfCurrentGoals = gsrGoalRepository
					.findByFkGsrDatEmpIdAndFkGsrDatMgrIdAndFkGsrSlabId(
							empId, rmId,
							currentSlab);
			LOG.info("goalsCheckForRmChange :listOfCurrentGoals "+listOfCurrentGoals.size() );
			if (listOfCurrentGoals.size() > 0) {
				for (GsrDatEmpGoalBO goalsBo : listOfCurrentGoals) {
					
					if (middleOfSlab) {
						if (goalsBo.getEmpGoalKpiWeightageId() != null
								&& goalsBo.getEmpGoalKpiWeightageId() != 0) {
							
							// KPI goals check
							if (goalsBo.getFkGsrGoalStatus() == GsrConstants.GSR_MANAGER_FEEDBACK_PROVIDED) {
								
								
								isKpiGoalsClosed = true;

							}

						} 

					} 

				}

			}
			
	
			
			
			
		} catch (Exception e) {
			
			
			e.printStackTrace();
			 throw new DataAccessException("Exception occured at gsrGoalsCheckForRmChange : "+e.getMessage());
		}
		LOG.exiting("goalsCheckForRmChange goalStatus"+goalStatus);
		return isKpiGoalsClosed;

	}
	
	
	//Added by Prathibha on 17-11-2017 to fetch roles of logged user
	
	/**
	 *Method to fetch roles of logged-in Employee
	 *@param :Employee Id
	 *@return :DatEmpRoleMappingOutputBean
	 * 
	 */
	 
	public DatEmpRoleMappingOutputBean  getEmployeeRoles (Integer employeeId)throws CommonCustomException 
	{  
		DatEmpRoleMappingOutputBean empRoleOutputBean = new DatEmpRoleMappingOutputBean();
		List<DatEmpRoleMappingBO> empRoleMapBOList = new ArrayList<DatEmpRoleMappingBO>();
		List<DatEmpRoleMappingBOBean> rollMapBeanList = new ArrayList<DatEmpRoleMappingBOBean>();
		
		try{
			empRoleMapBOList = empRoleRepository.findByFkEmployeeId(employeeId);
		}catch (Exception e)
		{
			throw new CommonCustomException("Exception occured while fetching role details for this employee");
		}
		
		if (!empRoleMapBOList.isEmpty() )
		{
			for (DatEmpRoleMappingBO roleMap : empRoleMapBOList )
			{
				DatEmpRoleMappingBOBean rollMapBean = new DatEmpRoleMappingBOBean();
				rollMapBean.setEmployeeRoleId(roleMap.getFkRoleId()); 
				rollMapBean.setRoleName(roleMap.getMasEmpRoleBO().getEmpRoleKeyName());
				rollMapBeanList.add(rollMapBean);
			}
			
			empRoleOutputBean.setEmployeeId(employeeId);
			empRoleOutputBean.setEmployeeRoles(rollMapBeanList);
		}
		else 
		{
			throw new CommonCustomException("Your role not found, kindly get in touch with Nucleus Support team.");
			
		}
        return empRoleOutputBean;
	}
	
}
