package com.thbs.mis.common.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.client.RestTemplate;

import com.thbs.mis.common.bean.SeparationLegacyOutputBean;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

public class SeparationLegacyService {

	private static final AppLog LOG = LogFactory.getLog(SeparationLegacyService.class);
	
	
	public SeparationLegacyOutputBean  getEmployeeSeparationDetails(Integer employeeId, String separationLegacyUrl) 
			throws CommonCustomException
	{
		RestTemplate restTemplate = new RestTemplate();
		SeparationLegacyOutputBean outputBean = new SeparationLegacyOutputBean();
		List<SeparationLegacyOutputBean> listBean = new ArrayList<SeparationLegacyOutputBean>();
		String urlString = separationLegacyUrl + "legacy/separation/" + employeeId;
		//LOG.info("inputBean ::: " + inputBean);
		LOG.info("urlString ::: " + urlString);
		MISResponse misResponse = restTemplate.getForObject(urlString, MISResponse.class );
		
		if(misResponse != null)
		{
			listBean.addAll(((List<SeparationLegacyOutputBean>) misResponse.getResult()));
			
			LOG.info("listBean :: " + listBean);
			char[] temp = listBean.toString().toCharArray();
			String finalString = ""; 
			for(int i =0; i < temp.length ; i++)
			{
				LOG.info("temp[" + i + "] :: " + temp[i]);
				if(temp[i] == '[' ){}
				else if(temp[i] == ']'){}
				else if(temp[i] == '{'){}
				else if(temp[i] == '}'){}
				else{
					finalString = finalString + temp[i];
				}
			}
			LOG.info("finalString :: " + finalString);
			String[] arrayString = finalString.split(",");
			List<String> listString = new ArrayList<String>();
			for(int j=0; j<arrayString.length; j++)
			{
				String[] subArrayString = arrayString[j].split("=");
				for(int k=0; k<subArrayString.length; k++)
				{
					if(k==1){
						listString.add(subArrayString[k]);
					}
				}
			}
			LOG.info("listString   :: " + listString);
		
			int count = 0;
			for (String string : listString) 
			{
				if(count == 0){
					outputBean.setEmployeeId(Integer.valueOf(string));
				}else if(count == 1){
					outputBean.setSeparationStatusId(Integer.valueOf(string));
				}else if(count == 2){
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					try {
						outputBean.setLastWorkingDate(sdf.parse(string));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				++count;
			}
			//outputBean = (ClientAndProjectDetailsFromCompIdBean) misResponse.getResult();
		}else{
			throw new CommonCustomException("Unable to get separation details from legacy. Kindly contact to Nucleus Support Team.");
		}
		return outputBean;
	}
	
}
