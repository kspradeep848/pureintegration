package com.thbs.mis.common.service;

import java.util.List;

import com.thbs.mis.common.bean.User;

public interface DataService {
	public List<User> getUserList();
}
