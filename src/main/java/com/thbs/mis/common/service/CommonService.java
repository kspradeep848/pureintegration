/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  CommonService.java                                */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 23-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.common.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bean.AcountsManagerOutputBean;
import com.thbs.mis.common.bean.BootcampBean;
import com.thbs.mis.common.bean.BusinessUnitBean;
import com.thbs.mis.common.bean.CountryBean;
import com.thbs.mis.common.bean.DmDetailsOutputBean;
import com.thbs.mis.common.bean.DomainOutputBean;
import com.thbs.mis.common.bean.EmpDesignationBean;
import com.thbs.mis.common.bean.EmployeeBean;
import com.thbs.mis.common.bean.EmployeeLevelBean;
import com.thbs.mis.common.bean.EmployeeNameWithRMIdBean;
import com.thbs.mis.common.bean.EmployeesBean;
import com.thbs.mis.common.bean.GetAllCurrencyTypeOutputBean;
import com.thbs.mis.common.bean.LocationByCountyBean;
import com.thbs.mis.common.bean.MailGeneralBean;
import com.thbs.mis.common.bean.ManagerOutputBean;
import com.thbs.mis.common.bean.MasAccountBean;
import com.thbs.mis.common.bean.MasBuUnitBean;
import com.thbs.mis.common.bean.MasBusinessEntityBean;
import com.thbs.mis.common.bean.MasClientOutputBean;
import com.thbs.mis.common.bean.MasEmpDesignationBean;
import com.thbs.mis.common.bean.MasRegionBean;
import com.thbs.mis.common.bean.MasVerticalBean;
import com.thbs.mis.common.bean.MisMenuBean;
import com.thbs.mis.common.bean.ModuleMappingOutputBean;
import com.thbs.mis.common.bean.MyReporteesBean;
import com.thbs.mis.common.bean.OrganizationBean;
import com.thbs.mis.common.bean.ParentLocationBean;
import com.thbs.mis.common.bean.PaymentInstructionOutputBean;
import com.thbs.mis.common.bean.PhysicalLocationBean;
import com.thbs.mis.common.bean.ProjectCoordinatorBean;
import com.thbs.mis.common.bean.RoleDetailsOutputBean;
import com.thbs.mis.common.bean.UpdateModuleMappingInputBean;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpModuleMappingBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.DatForexConversionsBO;
import com.thbs.mis.common.bo.MasAccountBO;
import com.thbs.mis.common.bo.MasBankPaymentDetailBO;
import com.thbs.mis.common.bo.MasBootCampBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.bo.MasBusinessEntityBO;
import com.thbs.mis.common.bo.MasCountryBO;
import com.thbs.mis.common.bo.MasCurrencytypeBO;
import com.thbs.mis.common.bo.MasEmpDesignationBO;
import com.thbs.mis.common.bo.MasEmpLevelBO;
import com.thbs.mis.common.bo.MasEmpRoleBO;
import com.thbs.mis.common.bo.MasLocationParentBO;
import com.thbs.mis.common.bo.MasLocationPhysicalBO;
import com.thbs.mis.common.bo.MasMenuBO;
import com.thbs.mis.common.bo.MasOrganizationBO;
import com.thbs.mis.common.bo.MasRegionBO;
import com.thbs.mis.common.bo.MasVerticalBO;
import com.thbs.mis.common.dao.BootcampRepository;
import com.thbs.mis.common.dao.BusinessUnitRepository;
import com.thbs.mis.common.dao.CountryRepository;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.DatForexConversionsRepository;
import com.thbs.mis.common.dao.DesignationRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.EmployeeRepository;
import com.thbs.mis.common.dao.LevelRepository;
import com.thbs.mis.common.dao.MasAccountRepository;
import com.thbs.mis.common.dao.MasBankPaymentDetailRepository;
import com.thbs.mis.common.dao.MasBusinessEntityRepository;
import com.thbs.mis.project.dao.MasRateTypeRepository;
//import com.thbs.mis.common.dao.MasInvoiceRateTypeRepository;
//import com.thbs.mis.common.dao.MasRateTypeRepository;
import com.thbs.mis.project.dao.MasClientRepository;
import com.thbs.mis.project.dao.MasInvoiceRateTypeRepository;
import com.thbs.mis.common.dao.MasCurrencyTypeRepository;
import com.thbs.mis.common.dao.MasEmpRolesRepository;
import com.thbs.mis.common.dao.MasRegionRepository;
import com.thbs.mis.common.dao.MasVerticalRepository;
import com.thbs.mis.common.dao.MisMenuRepository;
import com.thbs.mis.common.dao.ModuleMappingDetailsRepository;
import com.thbs.mis.common.dao.OrganizationRepository;
import com.thbs.mis.common.dao.ParentLocationRepository;
import com.thbs.mis.common.dao.PhysicalLocationRepository;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.mail.EmailServiceUtil;
import com.thbs.mis.framework.mail.MailTemplate;
import com.thbs.mis.framework.report.Util.DownloadFileUtil;
import com.thbs.mis.project.bo.MasClientBO;
import com.thbs.mis.project.bo.MasInvoiceRateTypeBO;
import com.thbs.mis.project.bo.MasRateTypeBO;
import com.thbs.mis.project.bo.ProjDomainMappingWithBuBO;
import com.thbs.mis.project.dao.ProjDomainMappingWithBuRepository;

/**
 * <Description CommonService:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
@Service
public class CommonService {
	// Alphabetic order
	@Autowired
	private BootcampRepository bootcampRepos;
	@Autowired
	private BusinessUnitRepository buRepos;
	@Autowired
	private MasBusinessEntityRepository masbeRepos;
	@Autowired
	private CountryRepository countryRepos;
	@Autowired
	private DesignationRepository designationRepos;
	@Autowired
	private EmailServiceUtil misEmailService;
	@Autowired
	private EmpDetailRepository empDetailRepos;
	@Autowired
	private EmployeeRepository employeeRepos;
	@Autowired
	private LevelRepository levelRepos;
	@Autowired
	private MisMenuRepository menuRepos;
	@Autowired
	private OrganizationRepository organizationRepos;
	@Autowired
	private PhysicalLocationRepository phyLocationRepos;
	@Autowired
	private ParentLocationRepository parentLocationRepos;
	@Autowired
	private MasCurrencyTypeRepository masCurrencyTypeRepos;
	@Autowired
	private MasEmpRolesRepository masEmpRolesRepository;
	//Added by Lalith Kumar
	@Autowired
	private	MasVerticalRepository masVerticalRepository;//EOA by Lalith Kumar
	@Autowired
	private ModuleMappingDetailsRepository moduleMappingDetailsRepository;
	// Added by Mani for GSR module service committed on 30-11-2016
	@Autowired(required = true)
	private DatEmployeeProfessionalRepository datEmpPersonalDetailRepository;
	// End of addition by Mani for GSR module service committed on 30-11-2016
	@Autowired
	private EmployeePersonalDetailsRepository empPersonalRepository;
	@Autowired
	private DatForexConversionsRepository datForexConversionsRepository;
	@Autowired
    private MasAccountRepository masAccountRepository;
	@Autowired
	private MasRegionRepository masRegionRepository;
	@Autowired
	private DesignationRepository designationRepository;
	
	@Autowired
	private MasClientRepository masClientRepository;
	
	@Autowired
	private MasRateTypeRepository masRateTypeRepository;
	
	@Autowired
	private MasInvoiceRateTypeRepository masInvoiceRateTypeRepository;
	
	@Value("${role.projectCoordinator}")
	private short projectCoordinatorRoleId;

	@Autowired
	private ProjDomainMappingWithBuRepository projDomainMappRepo;
	@Autowired
	private MasBankPaymentDetailRepository bankPaymentInstructionRepository;

	/**
	 * @see : Global ArrayList is created for allDirectAndIndirectReporteesList
	 **/
	List<EmployeeNameWithRMIdBean> allDirectAndIndirectReporteesList;
	@Value("${common.account.manager.level.names}")
	private String accountManagerLevelNames;
	@Value("${common.domain.manager.level.names}")
	private String domainManagerLevelNames;
	@Value("${common.delivery.manager.level.names}")
	private String deliveryManagerLevelNames;
	@Value("${employee.not.include}")
	private String empIds;
	@Value("${video.path}")
	private String VIDIO_PATH;
	@Value("${project.coordinator.role.names}")
	private String projectCordinatorRoleKeyNames;
	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory.getLog(CommonService.class);

	public CommonService() {
		super();
	}

	// added by Smrithi
	/**
	 * <Description getAllActiveReportingManagers:> Fetch all active reporting
	 * managers based on role ID.
	 * 
	 * @param Short
	 *            roleId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws DataAccessException
	 */
	public List<EmployeeNameWithRMIdBean> getAllReportingManagerByBuUnitId(Short buUnitId) throws DataAccessException {
		List<EmployeeNameWithRMIdBean> allActiveManagersList = new ArrayList<EmployeeNameWithRMIdBean>();
		List<DatEmpDetailBO> reprotingManagerList;
		try {
			List<Integer> empIDList = new ArrayList<Integer>();
			String[] empIdArray = empIds.split(",");
			for (String s : empIdArray)
				empIDList.add(Integer.parseInt(s));
			reprotingManagerList = empDetailRepos.getAllActiveManagersByBuUnitId(buUnitId, empIDList);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of direct reportees :  ", e);
		}
		if (!reprotingManagerList.isEmpty()) {
			for (DatEmpDetailBO datEmpDetailBOObject : reprotingManagerList) {
				EmployeeNameWithRMIdBean reportingManagerBeanObject = new EmployeeNameWithRMIdBean();
				reportingManagerBeanObject.setEmpId(datEmpDetailBOObject.getPkEmpId());
				reportingManagerBeanObject.setEmpFirstName(datEmpDetailBOObject.getEmpFirstName());
				reportingManagerBeanObject.setEmpLastName(datEmpDetailBOObject.getEmpLastName());
				reportingManagerBeanObject.setMgrId(datEmpDetailBOObject.getEmpRMId());
				reportingManagerBeanObject.setReporteeFlag(datEmpDetailBOObject.getReporteeFlag());
				reportingManagerBeanObject.setEmpFullNameWithId(datEmpDetailBOObject.getEmpFirstName() + " "
						+ datEmpDetailBOObject.getEmpLastName() + "-" + datEmpDetailBOObject.getPkEmpId());
				allActiveManagersList.add(reportingManagerBeanObject);
			}
		}
		return allActiveManagersList;
	}

	/**
	 * <Description getAllActiveReportingManagers:> Fetch all active reporting
	 * managers based on emp ID.
	 * 
	 * @param Integer
	 *            empId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws DataAccessException
	 */
	public List<EmployeeNameWithRMIdBean> getAllReportingManagersByEmpId(Integer empID) throws DataAccessException {
		List<EmployeeNameWithRMIdBean> allActiveManagersList = new ArrayList<EmployeeNameWithRMIdBean>();
		List<DatEmpDetailBO> reprotingManagerList;
		try {
			reprotingManagerList = empDetailRepos.getAllReportingManagersByEmpId(empID, empID);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of direct reportees : ", e);
		}
		if (!reprotingManagerList.isEmpty()) {
			for (DatEmpDetailBO datEmpDetailBOObject : reprotingManagerList) {
				EmployeeNameWithRMIdBean reportingManagerBeanObject = new EmployeeNameWithRMIdBean();
				reportingManagerBeanObject.setEmpId(datEmpDetailBOObject.getPkEmpId());
				reportingManagerBeanObject.setEmpFirstName(datEmpDetailBOObject.getEmpFirstName());
				reportingManagerBeanObject.setEmpLastName(datEmpDetailBOObject.getEmpLastName());
				reportingManagerBeanObject.setMgrId(datEmpDetailBOObject.getEmpRMId());
				reportingManagerBeanObject.setReporteeFlag(datEmpDetailBOObject.getReporteeFlag());
				reportingManagerBeanObject.setEmpFullNameWithId(datEmpDetailBOObject.getEmpFirstName() + " "
						+ datEmpDetailBOObject.getEmpLastName() + "-" + datEmpDetailBOObject.getPkEmpId());
				allActiveManagersList.add(reportingManagerBeanObject);
			}
		} else {
			try {
				reprotingManagerList = empDetailRepos.getBuHeadDetailsByEmpId(empID, empID);
			} catch (Exception e) {
				throw new DataAccessException("Failed to retrieve list of direct reportees : ", e);
			}
			if (!reprotingManagerList.isEmpty()) {
				for (DatEmpDetailBO datEmpDetailBOObject : reprotingManagerList) {
					EmployeeNameWithRMIdBean reportingManagerBeanObject = new EmployeeNameWithRMIdBean();
					reportingManagerBeanObject.setEmpId(datEmpDetailBOObject.getPkEmpId());
					reportingManagerBeanObject.setEmpFirstName(datEmpDetailBOObject.getEmpFirstName());
					reportingManagerBeanObject.setEmpLastName(datEmpDetailBOObject.getEmpLastName());
					reportingManagerBeanObject.setMgrId(datEmpDetailBOObject.getEmpRMId());
					reportingManagerBeanObject.setReporteeFlag(datEmpDetailBOObject.getReporteeFlag());
					reportingManagerBeanObject.setEmpFullNameWithId(datEmpDetailBOObject.getEmpFirstName() + " "
							+ datEmpDetailBOObject.getEmpLastName() + "-" + datEmpDetailBOObject.getPkEmpId());
					allActiveManagersList.add(reportingManagerBeanObject);
				}
			}
		}
		return allActiveManagersList;
	}

	/**
	 * <Description getAllActiveReportingManagers:> Fetch all Employees based on
	 * emp ID.
	 * 
	 * @param Integer
	 *            empId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws DataAccessException
	 */
	public List<EmployeeNameWithRMIdBean> getAllEmployeesByEmpId(Integer empID) throws DataAccessException {
		List<EmployeeNameWithRMIdBean> allActiveManagersList = new ArrayList<EmployeeNameWithRMIdBean>();
		List<DatEmpDetailBO> reprotingManagerList;
		try {
			reprotingManagerList = empDetailRepos.getAllEmployeesByEmpId(empID, empID);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of employees :  ", e);
		}
		if (!reprotingManagerList.isEmpty()) {
			for (DatEmpDetailBO datEmpDetailBOObject : reprotingManagerList) {
				EmployeeNameWithRMIdBean reportingManagerBeanObject = new EmployeeNameWithRMIdBean();
				reportingManagerBeanObject.setEmpId(datEmpDetailBOObject.getPkEmpId());
				reportingManagerBeanObject.setEmpFirstName(datEmpDetailBOObject.getEmpFirstName());
				reportingManagerBeanObject.setEmpLastName(datEmpDetailBOObject.getEmpLastName());
				reportingManagerBeanObject.setMgrId(datEmpDetailBOObject.getEmpRMId());
				reportingManagerBeanObject.setReporteeFlag(datEmpDetailBOObject.getReporteeFlag());
				reportingManagerBeanObject.setEmpFullNameWithId(datEmpDetailBOObject.getEmpFirstName() + " "
						+ datEmpDetailBOObject.getEmpLastName() + "-" + datEmpDetailBOObject.getPkEmpId());
				allActiveManagersList.add(reportingManagerBeanObject);
			}
		}
		return allActiveManagersList;
	}

	// EOA by Smrithi
	/**
	 * <Description getAllBootcamps:> Fetch all boot-camp details
	 * 
	 * @param
	 * @return List<BootcampBean>
	 * @throws DataAccessException
	 */
	public List<BootcampBean> getAllBootcamps() throws DataAccessException {
		List<BootcampBean> bootcampBeanList = new ArrayList<BootcampBean>();
		List<MasBootCampBO> bootcampBO;
		try {
			bootcampBO = (List<MasBootCampBO>) bootcampRepos.findAll();
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of bootcamps : ", e);
		}
		if (!bootcampBO.isEmpty()) {
			for (MasBootCampBO masBootCampBO : bootcampBO) {
				BootcampBean bootcampBeanObject = new BootcampBean();
				bootcampBeanObject.setBootcampId(masBootCampBO.getPkBootCampId());
				bootcampBeanObject.setBootcampName(masBootCampBO.getBootCampName());
				bootcampBeanObject.setBootcampYear(masBootCampBO.getBootCampYear());
				bootcampBeanList.add(bootcampBeanObject);
			}
		}
		return bootcampBeanList;
	}

	/**
	 * <Description getAllBusinessUnits:> Get All Active Business Units
	 * 
	 * @return List<BusinessUnitBean>
	 * @throws DataAccessException
	 */
	public List<BusinessUnitBean> getAllActiveBusinessUnits() throws DataAccessException {
		List<MasBuUnitBO> businessUnits;
		List<BusinessUnitBean> activeBUList = new ArrayList<BusinessUnitBean>();
		try {
			// businessUnits = buRepos.findByActiveFlag("ACTIVE");
			businessUnits = buRepos.allActiveBU("ACTIVE");
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of all active business unit.", e);
		}
		if (!businessUnits.isEmpty()) {
			for (MasBuUnitBO masBuUnitBOObject : businessUnits) {
				BusinessUnitBean businessUnitBean = new BusinessUnitBean();
				businessUnitBean.setPkBuUnitId(masBuUnitBOObject.getPkBuUnitId());
				businessUnitBean.setBuUnitName(masBuUnitBOObject.getBuUnitName());
				// businessUnitBean.setActiveFlag(masBuUnitBOObject.getActiveFlag());
				businessUnitBean.setFkBuHeadEmpId(masBuUnitBOObject.getFkBuHeadEmpId());
				businessUnitBean.setBuHeadName(masBuUnitBOObject.getBuHeadName());
				businessUnitBean.setEmpTotalcount(masBuUnitBOObject.getEmpTotalCount());
				activeBUList.add(businessUnitBean);
			}
		}
		return activeBUList;
	}

	/**
	 * <Description getBusinessUnitByBuId:> Fetch an active business unit
	 * 
	 * @param Integer
	 *            buId
	 * @return BusinessUnitBean
	 * @throws DataAccessException
	 */
	// Added by prathibha
	/**
	 * @param buId
	 * @return
	 * @throws DataAccessException
	 */
	public List<BusinessUnitBean> getBusinessUnitByBuId(Integer buId) throws DataAccessException {
		List<BusinessUnitBean> buBeanList = new ArrayList<BusinessUnitBean>();
		List<MasBuUnitBO> masBusinessUnitList;
		short shortBUId = new Integer(buId).shortValue();
		try {
			// masBusinessUnitList =
			// buRepos.findByPkBuUnitIdAndActiveFlag(shortBUId, "ACTIVE");
			masBusinessUnitList = buRepos.viewBUInformation(shortBUId, "ACTIVE");
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve business unit information", e);
		}
		if (!masBusinessUnitList.isEmpty()) {
			for (MasBuUnitBO masBuUnitBO : masBusinessUnitList) {
				BusinessUnitBean businessUnitBean = new BusinessUnitBean();
				businessUnitBean.setPkBuUnitId(masBuUnitBO.getPkBuUnitId());
				businessUnitBean.setBuUnitName(masBuUnitBO.getBuUnitName());
				businessUnitBean.setEmpTotalcount(masBuUnitBO.getEmpTotalCount());
				businessUnitBean.setFkBuHeadEmpId(masBuUnitBO.getFkBuHeadEmpId());
				businessUnitBean.setBuHeadName(masBuUnitBO.getBuHeadName());
				buBeanList.add(businessUnitBean);
			}
		}
		return buBeanList;
	}

	/**
	 * <Description getAllCountry:> Fetch all countries
	 * 
	 * @param
	 * @return List<CountryBean>
	 * @throws DataAccessException
	 */
	public List<CountryBean> getAllCountry() throws DataAccessException {
		List<CountryBean> countryBeanList = new ArrayList<CountryBean>();
		List<MasCountryBO> countryBO;
		try {
			countryBO = (List<MasCountryBO>) countryRepos.findAll();
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of countries : ", e);
		}
		if (!countryBO.isEmpty()) {
			for (MasCountryBO masCountryBO : countryBO) {
				CountryBean countryBeanObject = new CountryBean();
				countryBeanObject.setCountryId(masCountryBO.getPkCountryId());
				countryBeanObject.setCountryName(masCountryBO.getCountryName());
				countryBeanList.add(countryBeanObject);
			}
		}
		return countryBeanList;
	}

	/**
	 * <Description getAllDesignation:> Fetch all designations
	 * 
	 * @param
	 * @return List<EmpDesignationBean>
	 * @throws DataAccessException
	 */
	public List<EmpDesignationBean> getAllDesignation() throws DataAccessException {
		List<EmpDesignationBean> empDesignationList = new ArrayList<EmpDesignationBean>();
		List<MasEmpDesignationBO> designationBO;
		try {
			designationBO = (List<MasEmpDesignationBO>) designationRepos.findAllByOrderByEmpDesignationNameAsc();// Updated
																													// by
																													// Shyam
																													// as
																													// per
																													// tester
																													// request
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of employee designations : ", e);
		}
		if (!designationBO.isEmpty()) {
			for (MasEmpDesignationBO masEmpDesignationBO : designationBO) {
				EmpDesignationBean empDesignationBeanObject = new EmpDesignationBean();
				empDesignationBeanObject.setDesignationId(masEmpDesignationBO.getPkEmpDesignationId());
				empDesignationBeanObject.setDesignationName(masEmpDesignationBO.getEmpDesignationName());
				empDesignationList.add(empDesignationBeanObject);
			}
		}
		return empDesignationList;
	}

	/**
	 * <Description getAllDirectAndIndirectReporteesWithRMIdUnderRM:> Fetch all
	 * direct and indirect reportees under a manager
	 * 
	 * @param int
	 *            reportingManagerId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws DataAccessException
	 * @throws BusinessException
	 */
	public List<EmployeeNameWithRMIdBean> getAllDirectAndIndirectReporteesWithRMIdUnderRM(Integer reportingManagerId)
			throws DataAccessException, BusinessException {
		List<EmployeeNameWithRMIdBean> allReporteesList = new ArrayList<EmployeeNameWithRMIdBean>();
		List<EmployeeNameWithRMIdBean> allDirectReporteesList = new ArrayList<EmployeeNameWithRMIdBean>();
		allDirectAndIndirectReporteesList = new ArrayList<EmployeeNameWithRMIdBean>();
		try {
			allDirectReporteesList = getAllDirectReporteesWithRMIdForRmId(reportingManagerId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve direct reportees :  ", e);
		}
		if (!allDirectReporteesList.isEmpty()) {
			for (EmployeeNameWithRMIdBean employeeNameWithRMIdObj : allDirectReporteesList) {
				if (!allReporteesList.contains(employeeNameWithRMIdObj))
					allReporteesList.add(employeeNameWithRMIdObj);
				if (employeeNameWithRMIdObj.getReporteeFlag().equalsIgnoreCase("YES_HAS_REPORTEES")) {
					List<EmployeeNameWithRMIdBean> allEmployeeList = new ArrayList<EmployeeNameWithRMIdBean>();
					allEmployeeList = findDirectAndIndirectReportees(employeeNameWithRMIdObj.getEmpId());
					if (!allEmployeeList.isEmpty()) {
						for (EmployeeNameWithRMIdBean employeeNameWithRMIdBean : allEmployeeList) {
							if (!allReporteesList.contains(employeeNameWithRMIdBean)) {
								allReporteesList.add(employeeNameWithRMIdBean);
							}
						}
					}
				}
			}
		} else {
			for (EmployeeNameWithRMIdBean employeeObject : allDirectReporteesList) {
				if (!allReporteesList.contains(employeeObject))
					allReporteesList.add(employeeObject);
			}
		}
		return allReporteesList;
	}

	/**
	 * <Description findDirectAndIndirectReportees>
	 * 
	 * @param int
	 *            reportingMgrId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws BusinessException
	 * 
	 **/
	private List<EmployeeNameWithRMIdBean> findDirectAndIndirectReportees(int reportingMgrId) throws BusinessException {
		List<EmployeeNameWithRMIdBean> tempList = new ArrayList<EmployeeNameWithRMIdBean>();
		try {
			tempList = getAllDirectReporteesWithRMIdForRmId(reportingMgrId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve reportees", e);
		}
		if (!tempList.isEmpty()) {
			for (EmployeeNameWithRMIdBean empNameWithRMIdObject : tempList) {
				if (!allDirectAndIndirectReporteesList.contains(empNameWithRMIdObject))
					allDirectAndIndirectReporteesList.add(empNameWithRMIdObject);
				if (empNameWithRMIdObject.getReporteeFlag().equalsIgnoreCase("YES_HAS_REPORTEES")) {
					List<EmployeeNameWithRMIdBean> allReporteesList = new ArrayList<EmployeeNameWithRMIdBean>();
					allReporteesList = findDirectAndIndirectReportees(empNameWithRMIdObject.getEmpId());
					if (!allReporteesList.isEmpty()) {
						for (EmployeeNameWithRMIdBean employeeObj : allReporteesList) {
							if (!allDirectAndIndirectReporteesList.contains(employeeObj))
								allDirectAndIndirectReporteesList.add(employeeObj);
						}
					}
				}
			}
		}
		return allDirectAndIndirectReporteesList;
	}

	/**
	 * <Description getAllDirectReporteesWithRMIdForRmId:> Fetch all direct
	 * reportees under a manager
	 * 
	 * @param Integer
	 *            rmId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws DataAccessException
	 */
	public List<EmployeeNameWithRMIdBean> getAllDirectReporteesWithRMIdForRmId(Integer rmId)
			throws DataAccessException {
		List<DatEmpDetailBO> reporteesList;
		List<EmployeeNameWithRMIdBean> allDirectReporteeList = new ArrayList<EmployeeNameWithRMIdBean>();
		try {
			List<Integer> empIDList = new ArrayList<Integer>();
			String[] empIdArray = empIds.split(",");
			for (String s : empIdArray)
				empIDList.add(Integer.parseInt(s));
			reporteesList = empDetailRepos.getAllDirectReporteesWithRMIdForRMId(rmId, empIDList);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of direct reportees :  ", e);
		}
		if (!reporteesList.isEmpty()) {
			for (DatEmpDetailBO datEmpDetailBOObject : reporteesList) {
				EmployeeNameWithRMIdBean employeeNameWithRMIdBeanObject = new EmployeeNameWithRMIdBean();
				employeeNameWithRMIdBeanObject.setEmpId(datEmpDetailBOObject.getPkEmpId());
				employeeNameWithRMIdBeanObject.setEmpFirstName(datEmpDetailBOObject.getEmpFirstName());
				employeeNameWithRMIdBeanObject.setEmpLastName(datEmpDetailBOObject.getEmpLastName());
				employeeNameWithRMIdBeanObject.setMgrId(datEmpDetailBOObject.getEmpRMId());
				employeeNameWithRMIdBeanObject.setReporteeFlag(datEmpDetailBOObject.getReporteeFlag());
				employeeNameWithRMIdBeanObject.setEmpFullNameWithId(datEmpDetailBOObject.getEmpFirstName() + " "
						+ datEmpDetailBOObject.getEmpLastName() + "-" + datEmpDetailBOObject.getPkEmpId());
				employeeNameWithRMIdBeanObject
						.setDesignation(getEmployeeDesignation(datEmpDetailBOObject.getPkEmpId()));
				allDirectReporteeList.add(employeeNameWithRMIdBeanObject);
			}
		}
		return allDirectReporteeList;
	}

	private String getEmployeeDesignation(Integer pkEmpId) {
		DatEmpProfessionalDetailBO empData = datEmpPersonalDetailRepository.findByFkMainEmpDetailId(pkEmpId);
		return empData.getMasEmpDesignationBO().getEmpDesignationName();
	}

	/**
	 * <Description getAllEmployeesList:> Fetch all employees
	 * 
	 * @param
	 * @return List<EmployeesBean>
	 * @throws DataAccessException
	 */
	public List<EmployeesBean> getAllEmployeesList() throws DataAccessException {
		List<EmployeesBean> employeeList = new ArrayList<EmployeesBean>();
		List<DatEmpPersonalDetailBO> emplist;
		try {
			List<Integer> empIDList = new ArrayList<Integer>();
			String[] empIdArray = empIds.split(",");
			for (String s : empIdArray)
				empIDList.add(Integer.parseInt(s));
			emplist = (List<DatEmpPersonalDetailBO>) employeeRepos.getAllEmployeesNameAndId(empIDList);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of employees :", e);
		}
		if (!emplist.isEmpty()) {
			for (DatEmpPersonalDetailBO datEmpPersonalDetailBO : emplist) {
				EmployeesBean empObject = new EmployeesBean();
				empObject.setEmpMainStatusId(datEmpPersonalDetailBO.getFkEmpMainStatus());
				empObject.setBuUnit(datEmpPersonalDetailBO.getFkEmpBuUnit());
				empObject.setBuUnitName(datEmpPersonalDetailBO.getBuUnitName());
				empObject.setBuHeadId(datEmpPersonalDetailBO.getFkBuHeadEmpId());
				empObject.setReportingMgrId(datEmpPersonalDetailBO.getFkEmpReportingMgrId());
				empObject.setEmployeeId(datEmpPersonalDetailBO.getFkEmpDetailId());
				empObject.setEmployeeFirstName(datEmpPersonalDetailBO.getEmpFirstName());
				empObject.setEmployeeLastName(datEmpPersonalDetailBO.getEmpLastName());
				empObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
						+ datEmpPersonalDetailBO.getEmpLastName() + "-" + datEmpPersonalDetailBO.getFkEmpDetailId());
				employeeList.add(empObject);
			}
		}
		return employeeList;
	}

	/**
	 * <Description getAllActiveEmployeeList:> Fetch all active employees
	 * 
	 * @param
	 * @return List<EmployeesBean>
	 * @throws DataAccessException
	 */
	public List<EmployeesBean> getAllActiveEmployeeList() throws DataAccessException {
		List<EmployeesBean> activeEmployeeList = new ArrayList<EmployeesBean>();
		List<DatEmpPersonalDetailBO> emplist;
		try {
			List<Integer> empIDList = new ArrayList<Integer>();
			String[] empIdArray = empIds.split(",");
			for (String s : empIdArray)
				empIDList.add(Integer.parseInt(s));
			emplist = employeeRepos.getAllActiveEmployeesNameAndId(empIDList);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of active employees :", e);
		}
		if (!emplist.isEmpty()) {
			for (DatEmpPersonalDetailBO datEmpPersonalDetailBO : emplist) {
				EmployeesBean activeEmpObject = new EmployeesBean();
				activeEmpObject.setEmployeeId(datEmpPersonalDetailBO.getFkEmpDetailId());
				activeEmpObject.setEmployeeFirstName(datEmpPersonalDetailBO.getEmpFirstName());
				activeEmpObject.setEmployeeLastName(datEmpPersonalDetailBO.getEmpLastName());
				activeEmpObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
						+ datEmpPersonalDetailBO.getEmpLastName() + "-" + datEmpPersonalDetailBO.getFkEmpDetailId());
				activeEmployeeList.add(activeEmpObject);
			}
		}
		return activeEmployeeList;
	}

	/**
	 * <Description getAllInactiveEmployeesList:> Fetch all inactive employees
	 * 
	 * @param
	 * @return List<EmployeesBean>
	 * @throws DataAccessException
	 */
	public List<EmployeesBean> getAllInactiveEmployeesList() throws DataAccessException {
		List<EmployeesBean> inactiveEmployeeList = new ArrayList<EmployeesBean>();
		List<DatEmpPersonalDetailBO> emplist;
		try {
			emplist = (List<DatEmpPersonalDetailBO>) employeeRepos.getAllInactiveEmployeesNameAndId();
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of inactive employees :", e);
		}
		if (!emplist.isEmpty()) {
			for (DatEmpPersonalDetailBO datEmpPersonalDetailBO : emplist) {
				EmployeesBean inactiveEmpObject = new EmployeesBean();
				inactiveEmpObject.setEmployeeId(datEmpPersonalDetailBO.getFkEmpDetailId());
				inactiveEmpObject.setEmployeeFirstName(datEmpPersonalDetailBO.getEmpFirstName());
				inactiveEmpObject.setEmployeeLastName(datEmpPersonalDetailBO.getEmpLastName());
				inactiveEmpObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
						+ datEmpPersonalDetailBO.getEmpLastName() + "-" + datEmpPersonalDetailBO.getFkEmpDetailId());
				inactiveEmployeeList.add(inactiveEmpObject);
			}
		}
		return inactiveEmployeeList;
	}

	/**
	 * <Description getAllLevels:> Get All Employee Levels
	 * 
	 * @return List<EmployeeLevelBean>
	 * @throws DataAccessException
	 */
	public List<EmployeeLevelBean> getAllLevels() throws DataAccessException {
		List<EmployeeLevelBean> empLevelBeanList = new ArrayList<EmployeeLevelBean>();
		List<MasEmpLevelBO> levelBO;
		try {
			levelBO = (List<MasEmpLevelBO>) levelRepos.findAll();
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of employee levels : ", e);
		}
		if (!levelBO.isEmpty()) {
			for (MasEmpLevelBO masEmpLevelBO : levelBO) {
				EmployeeLevelBean employeeLevelBeanObject = new EmployeeLevelBean();
				employeeLevelBeanObject.setLevelId(masEmpLevelBO.getPkEmpLevelId());
				employeeLevelBeanObject.setLevelName(masEmpLevelBO.getEmpLevelName());
				empLevelBeanList.add(employeeLevelBeanObject);
			}
		}
		return empLevelBeanList;
	}

	/**
	 * <Description getAllMenuBasedonRole:> Fetch all active menus under a role.
	 * 
	 * @param Integer
	 *            roleId
	 * @return List<MisMenuBean>
	 * @throws DataAccessException
	 */
	public List<MisMenuBean> getAllMenuBasedonRole(Integer roleId) throws DataAccessException {
		List<MisMenuBean> menuBeanList = new ArrayList<MisMenuBean>();
		List<MasMenuBO> menuList;
		try {
			menuList = menuRepos.getAllMenuListForRoleId(roleId);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of menu : ", e);
		}
		if (!menuList.isEmpty()) {
			for (MasMenuBO masMenuBO : menuList) {
				MisMenuBean misMenuBeanObject = new MisMenuBean();
				misMenuBeanObject.setMenuId(masMenuBO.getPkMenuId());
				misMenuBeanObject.setMenuName(masMenuBO.getMenuName());
				misMenuBeanObject.setActiveStatus(masMenuBO.getActiveStatus());
				misMenuBeanObject.setIsParent(masMenuBO.getIsParent());
				misMenuBeanObject.setMenuDescription(masMenuBO.getMenuDescription());
				misMenuBeanObject.setParentId(masMenuBO.getMenuParentId());
				misMenuBeanObject.setModuleId(masMenuBO.getModuleId());
				misMenuBeanObject.setMenuUrl(masMenuBO.getMenuUrl());
				misMenuBeanObject.setMenuKey(masMenuBO.getMenuKey());
				menuBeanList.add(misMenuBeanObject);
			}
		}
		return menuBeanList;
	}

	/**
	 * <Description getAllOrganization:> Fetch all organizations
	 * 
	 * @param
	 * @return List<OrganizationBean>
	 * @throws DataAccessException
	 */
	public List<OrganizationBean> getAllOrganization() throws DataAccessException {
		List<OrganizationBean> OrganizationBeanList = new ArrayList<OrganizationBean>();
		List<MasOrganizationBO> organizationBO;
		try {
			organizationBO = (List<MasOrganizationBO>) organizationRepos.findAll();
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of organizations : ", e);
		}
		if (!organizationBO.isEmpty()) {
			for (MasOrganizationBO masOrganizationBO : organizationBO) {
				OrganizationBean organizationBeanObject = new OrganizationBean();
				organizationBeanObject.setOrganizationIdl(masOrganizationBO.getPkOrganizationId());
				organizationBeanObject.setOrganizationName(masOrganizationBO.getOrganizationName());
				OrganizationBeanList.add(organizationBeanObject);
			}
		}
		return OrganizationBeanList;
	}

	/**
	 * <Description getAllParentLocation:> Fetch All Parent Locations
	 * 
	 * @param
	 * @return List<ParentLocationBean>
	 * @throws DataAccessException
	 */
	public List<ParentLocationBean> getAllParentLocation() throws DataAccessException {
		List<ParentLocationBean> parentLocationList = new ArrayList<ParentLocationBean>();
		List<MasLocationParentBO> parentLocationBO;
		try {
			parentLocationBO = (List<MasLocationParentBO>) parentLocationRepos.findAll();
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of parent location : ", e);
		}
		if (!parentLocationBO.isEmpty()) {
			for (MasLocationParentBO masLocationParentBO : parentLocationBO) {
				ParentLocationBean parentLocationObject = new ParentLocationBean();
				parentLocationObject.setParentLocationId(masLocationParentBO.getPkLocationParentId());
				parentLocationObject.setParentLocationName(masLocationParentBO.getLocationParentName());
				parentLocationObject.setCountryId(masLocationParentBO.getFkCountryId());
				parentLocationList.add(parentLocationObject);
			}
		}
		return parentLocationList;
	}

	/**
	 * <Description getAllPhysicalLocation:> Fetch All Physical Locations
	 * 
	 * @param
	 * @return List<PhysicalLocationBean>
	 * @throws DataAccessException
	 */
	public List<PhysicalLocationBean> getAllPhysicalLocation() throws DataAccessException {
		List<PhysicalLocationBean> physicalLocationList = new ArrayList<PhysicalLocationBean>();
		List<MasLocationPhysicalBO> phyLocationBO;
		try {
			phyLocationBO = (List<MasLocationPhysicalBO>) phyLocationRepos.findAll();
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of physical location :  ", e);
		}
		if (!phyLocationBO.isEmpty()) {
			for (MasLocationPhysicalBO masLocationPhysicalBO : phyLocationBO) {
				PhysicalLocationBean physicalLocBeanObject = new PhysicalLocationBean();
				physicalLocBeanObject.setPhysicalLocId(masLocationPhysicalBO.getPkLocationPhysicalId());
				physicalLocBeanObject.setPhysicalLocName(masLocationPhysicalBO.getLocationPhysicalName());
				physicalLocBeanObject.setParentlocId(masLocationPhysicalBO.getFkLocationParentId());
				physicalLocationList.add(physicalLocBeanObject);
			}
		}
		return physicalLocationList;
	}

	/**
	 * <Description getAllActiveReportingManagers:> Fetch all active reporting
	 * managers.
	 * 
	 * @param
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws DataAccessException
	 */
	public List<EmployeeNameWithRMIdBean> getAllActiveReportingManagers() throws DataAccessException {
		List<EmployeeNameWithRMIdBean> allActiveManagersList = new ArrayList<EmployeeNameWithRMIdBean>();
		List<DatEmpDetailBO> reprotingManagerList;
		try {
			List<Integer> empIDList = new ArrayList<Integer>();
			String[] empIdArray = empIds.split(",");
			for (String s : empIdArray)
				empIDList.add(Integer.parseInt(s));
			reprotingManagerList = empDetailRepos.getAllActiveManagers("YES_HAS_REPORTEES", empIDList);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of direct reportees :  ", e);
		}
		if (!reprotingManagerList.isEmpty()) {
			for (DatEmpDetailBO datEmpDetailBOObject : reprotingManagerList) {
				EmployeeNameWithRMIdBean reportingManagerBeanObject = new EmployeeNameWithRMIdBean();
				reportingManagerBeanObject.setEmpId(datEmpDetailBOObject.getPkEmpId());
				reportingManagerBeanObject.setEmpFirstName(datEmpDetailBOObject.getEmpFirstName());
				reportingManagerBeanObject.setEmpLastName(datEmpDetailBOObject.getEmpLastName());
				reportingManagerBeanObject.setMgrId(datEmpDetailBOObject.getEmpRMId());
				reportingManagerBeanObject.setReporteeFlag(datEmpDetailBOObject.getReporteeFlag());
				reportingManagerBeanObject.setEmpFullNameWithId(datEmpDetailBOObject.getEmpFirstName() + " "
						+ datEmpDetailBOObject.getEmpLastName() + "-" + datEmpDetailBOObject.getPkEmpId());
				allActiveManagersList.add(reportingManagerBeanObject);
			}
		}
		return allActiveManagersList;
	}

	/**
	 * <Description getAllSubmenuBasedOnMenuId:> Fetch all active sub-menus
	 * under a parent menu.
	 * 
	 * @param Integer
	 *            menuId
	 * @return List<MisMenuBean> menuList
	 * @throws DataAccessException
	 */
	public List<MisMenuBean> getAllSubmenuBasedOnMenuId(Integer menuId) throws DataAccessException {
		List<MisMenuBean> misSubMenuList = new ArrayList<MisMenuBean>();
		List<MasMenuBO> subMenuList;
		try {
			subMenuList = menuRepos.getAllSubMenuListForParentMenuId(menuId);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of sub-menu : ", e);
		}
		if (!subMenuList.isEmpty()) {
			for (MasMenuBO masMenuBO : subMenuList) {
				MisMenuBean misMenuBeanObject = new MisMenuBean();
				misMenuBeanObject.setMenuId(masMenuBO.getPkMenuId());
				misMenuBeanObject.setMenuName(masMenuBO.getMenuName());
				misMenuBeanObject.setActiveStatus(masMenuBO.getActiveStatus());
				misMenuBeanObject.setIsParent(masMenuBO.getIsParent());
				misMenuBeanObject.setMenuDescription(masMenuBO.getMenuDescription());
				misMenuBeanObject.setParentId(masMenuBO.getMenuParentId());
				misMenuBeanObject.setModuleId(masMenuBO.getModuleId());
				misMenuBeanObject.setMenuUrl(masMenuBO.getMenuUrl());
				misMenuBeanObject.setMenuKey(masMenuBO.getMenuKey());
				misSubMenuList.add(misMenuBeanObject);
			}
		}
		return misSubMenuList;
	}

	public boolean sendMailForGeneral(MailGeneralBean mailList) throws DataAccessException {
		try {
			List<String> to = new ArrayList<String>();
			List<String> cc = new ArrayList<String>();
			System.out.println("mail to" + mailList.getToAddress());
			System.out.println("mail cc" + mailList.getCcAddress());
			System.out.println("mail subject" + mailList.getMailBody());
			System.out.println("mail body" + mailList.getMailSubject());
			to.add(mailList.getToAddress());
			cc.add(mailList.getCcAddress());
			// Gets Template of type 1
			MailTemplate m1 = new MailTemplate();
			m1.setSubject(mailList.getMailSubject());
			m1.setBody(mailList.getMailBody());
			try {
				// Sends mail to reporting manager,notifying about
				// generation of employee code
				misEmailService.sendMail(to, cc, m1.getSubject(), m1.getBody());
				// Holds the message notifying email has been sent to
				// reporting manager
			} catch (Exception e) {
				e.printStackTrace();
				LOG.error(e.getMessage(), e);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	// Added by Mani for GSR module service committed on 30-11-2016
	/**
	 * @Description :: Retrieve an employee professional details by his/her
	 *              employee id
	 * @param Integer
	 *            empId
	 * @return DatEmpProfessionalDetailBO
	 * @throws DataAccessException
	 */
	public DatEmpProfessionalDetailBO getEmpoyeeDeatailsById(Integer empId) throws DataAccessException {
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBOData = new DatEmpProfessionalDetailBO();
		try {
			datEmpProfessionalDetailBOData = datEmpPersonalDetailRepository.findByFkMainEmpDetailId(empId);
		} catch (Exception e) {
			throw new DataAccessException("Unable to retrieve employee details");
		}
		return datEmpProfessionalDetailBOData;
	}

	/**
	 * @Description :: Retrieve list of employee professional details under
	 *              domain manager id.
	 * @param Integer
	 *            domainmanagerId
	 * @return List<DatEmpProfessionalDetailBO>
	 * @throws DataAccessException
	 */
	public List<DatEmpProfessionalDetailBO> getAllReporteesByDomainManagerId(Integer domainmanagerId)
			throws DataAccessException {
		List<DatEmpProfessionalDetailBO> reporteesDetailsList = new ArrayList<DatEmpProfessionalDetailBO>();
		try {
			reporteesDetailsList = datEmpPersonalDetailRepository.findByFkEmpDomainMgrId(domainmanagerId);
		} catch (Exception e) {
			throw new DataAccessException("Unable to retrieve Domain maanger Reportees  details");
		}
		return reporteesDetailsList;
	}

	/**
	 * @Description :: Retrieve list of direct employees under a reporting
	 *              manager
	 * @param Integer
	 *            rmId
	 * @return List<DatEmpProfessionalDetailBO>
	 * @throws DataAccessException
	 */
	public List<DatEmpProfessionalDetailBO> getAllDirectReporteesByReportingManagerId(Integer rmId)
			throws DataAccessException {
		List<DatEmpProfessionalDetailBO> employeeDetailsList = new ArrayList<DatEmpProfessionalDetailBO>();
		try {
			employeeDetailsList = datEmpPersonalDetailRepository.findByFkEmpReportingMgrId(rmId);
		} catch (Exception e) {
			throw new DataAccessException("Unable to retrieve Domain maanger Reportees  details");
		}
		return employeeDetailsList;
	}

	/**
	 * @Description ::
	 * @param Short
	 *            buId
	 * @return List<DatEmpProfessionalDetailBO>
	 * @throws DataAccessException
	 */
	public List<DatEmpProfessionalDetailBO> getAllEmployeesByBuId(Short buId) throws DataAccessException {
		List<DatEmpProfessionalDetailBO> employeeDetailsList = new ArrayList<DatEmpProfessionalDetailBO>();
		try {
			employeeDetailsList = datEmpPersonalDetailRepository.findByFkEmpBuUnit(buId);
		} catch (Exception e) {
			throw new DataAccessException("Unable to retrieve Domain maanger Reportees  details");
		}
		return employeeDetailsList;
	}

	// End of addition by Mani for GSR module service committed on 30-11-2016
	/**
	 * Description - Service to get BU Information by passing BU ID
	 * 
	 * @param pkBuUnitId
	 * @return MasBuUnitBO
	 * @throws DataAccessException
	 */
	public MasBuUnitBO getBuDetailsById(short pkBuUnitId) throws DataAccessException {
		MasBuUnitBO masBuUnitBO;
		try {
			masBuUnitBO = buRepos.findByPkBuUnitId(pkBuUnitId);
		} catch (Exception e) {
			throw new DataAccessException("Exception Occured : ", e);
		}
		return masBuUnitBO;
	}

	public EmployeeBean authenticateUser(int empId) throws DataAccessException {
		LOG.startUsecase("Entering to Authenticate User");
		EmployeeBean empInfo = null;
		try {
			LOG.info("Before Authenticate User --> ");
			DatEmpDetailBO empInfoBO = empDetailRepos.findByPkEmpId(empId);
			LOG.info("User Found :  " + empInfoBO);
			if (empInfoBO != null) {
				empInfo = new EmployeeBean();
				empInfo.setEmpRoleId(empInfoBO.getFkEmpRoleId());
				empInfo.setEmpRoleName(empInfoBO.getMasEmpRoleBO().getEmpRoleKeyName());
				DatEmpPersonalDetailBO empRec = employeeRepos.findByFkEmpDetailId(empInfoBO.getPkEmpId());
				empInfo.setEmpId(String.valueOf(empInfoBO.getPkEmpId()));
				empInfo.setFirstName(empRec.getEmpFirstName());
				empInfo.setLastName(empRec.getEmpLastName());
				DatEmpProfessionalDetailBO empProfessional = datEmpPersonalDetailRepository
						.findByFkMainEmpDetailId(empInfoBO.getPkEmpId());
				empInfo.setHasReportee(empProfessional.getHasReporteesFlag());
				empInfo.setBuName(empProfessional.getMasBuUnitBO().getBuUnitName());
				empInfo.setBuId(empProfessional.getMasBuUnitBO().getPkBuUnitId());
				empInfo.setEmpLevel(empProfessional.getMasEmpLevelBO().getEmpLevelName());
				empInfo.setReportingMgrId(empProfessional.getDatEmpDetailBO_rm_id().getPkEmpId().toString());
				empInfo.setEmpEmail(empProfessional.getEmpProfessionalEmailId());
				empInfo.setEmpDoj(empProfessional.getEmpDateOfJoining());
				empInfo.setEmpOrgId(empProfessional.getFkEmpOrganizationId());
				empInfo.setEmpMasParentLocationId(empProfessional.getMasLocationParentBO());
			}
		} catch (Exception e) {
			throw new DataAccessException("Exception Occured : ", e);
		}
		LOG.endUsecase("Exiting Authenticate User");
		return empInfo;
	}

	/*
	 * ADDED By Pradeep For LIVE Login to encrypt EmpId
	 *//*
		 * public EmployeeBean authenticateUser(String hexempId) throws
		 * DataAccessException {
		 * 
		 * LOG.startUsecase("Entering to Authenticate User"); EmployeeBean
		 * empInfo = null; try {
		 * 
		 * LOG.info("hex empID "+hexempId); int
		 * empId=Integer.decode("0x"+hexempId); LOG.info(" empID "+empId);
		 * LOG.info("Before Authenticate User --> "+empId); DatEmpDetailBO
		 * empInfoBO = empDetailRepos.findByPkEmpId(empId);
		 * 
		 * LOG.info("User Found :  " + empInfoBO);
		 * 
		 * if (empInfoBO != null) { empInfo = new EmployeeBean();
		 * empInfo.setEmpRoleId(empInfoBO.getFkEmpRoleId());
		 * empInfo.setEmpRoleName(empInfoBO.getMasEmpRoleBO().getEmpRoleName());
		 * DatEmpPersonalDetailBO empRec = employeeRepos
		 * .findByFkEmpDetailId(empInfoBO.getPkEmpId());
		 * empInfo.setEmpId(String.valueOf(empInfoBO.getPkEmpId()));
		 * empInfo.setPk_EmpLogin_EmpID(String.valueOf(empInfoBO.getPkEmpId()));
		 * empInfo.setFirstName(empRec.getEmpFirstName());
		 * empInfo.setLastName(empRec.getEmpLastName());
		 * DatEmpProfessionalDetailBO empProfessional =
		 * datEmpPersonalDetailRepository
		 * .findByFkMainEmpDetailId(empInfoBO.getPkEmpId());
		 * empInfo.setHasReportee(empProfessional.getHasReporteesFlag());
		 * empInfo.setBuName(empProfessional.getMasBuUnitBO() .getBuUnitName());
		 * empInfo.setBuId(empProfessional.getMasBuUnitBO() .getPkBuUnitId());
		 * empInfo
		 * .setEmpLevel(empProfessional.getMasEmpLevelBO().getEmpLevelName());
		 * empInfo .setReportingMgrId(empProfessional.getDatEmpDetailBO_rm_id().
		 * getPkEmpId ().toString());
		 * empInfo.setEmpEmail(empProfessional.getEmpProfessionalEmailId());
		 * empInfo.setEmpDoj(empProfessional.getEmpDateOfJoining());
		 * 
		 * }
		 * 
		 * } catch (Exception e) { throw new
		 * DataAccessException("Exception Occured : ", e);
		 * 
		 * } LOG.endUsecase("Exiting Authenticate User"); return empInfo; }
		 */
	/*
	 * public List<MisMenuBean> getAllMenusForEmployee(Integer empId) throws
	 * DataAccessException { List<MisMenuBean> menuBeanList = new
	 * ArrayList<MisMenuBean>(); List<MasMenuBO> menuList; try { menuList =
	 * menuRepos.getAllMenuListForEmployee(empId); } catch (Exception e) { throw
	 * new DataAccessException("Failed to retrieve list of menu : ", e); }
	 * 
	 * if(!menuList.isEmpty()) { for (MasMenuBO masMenuBO : menuList) {
	 * MisMenuBean misMenuBeanObject = new MisMenuBean();
	 * misMenuBeanObject.setMenuId(masMenuBO.getPkMenuId());
	 * misMenuBeanObject.setMenuName(masMenuBO.getMenuName());
	 * misMenuBeanObject.setActiveStatus(masMenuBO.getActiveStatus());
	 * misMenuBeanObject.setIsParent(masMenuBO.getIsParent());
	 * misMenuBeanObject.setMenuDescription(masMenuBO.getMenuDescription());
	 * misMenuBeanObject.setParentId(masMenuBO.getMenuParentId());
	 * misMenuBeanObject.setModuleId(masMenuBO.getModuleId());
	 * misMenuBeanObject.setMenuUrl(masMenuBO.getMenuUrl());
	 * menuBeanList.add(misMenuBeanObject); } }
	 * 
	 * return menuBeanList; }
	 */
	/**
	 * 
	 * <Description getAllSubMenuListForEmployeeForParentMenuId:> Retrieves all
	 * sub menus for the employee based on parent menu Id
	 * 
	 * @param empId
	 *            Employee Id
	 * 
	 * @param parentMenuId
	 *            Parent menu module id
	 * @return List of {@link MisMenuBean} containing menu information
	 * 
	 * @throws DataAccessException
	 *             In case of any exceptions
	 */
	public List<MisMenuBean> getAllSubMenuListForEmployeeForParentMenuId(Integer empId, Integer parentMenuId)
			throws DataAccessException {
		List<MisMenuBean> menuBeanList = new ArrayList<MisMenuBean>();
		List<MasMenuBO> menuList;
		try {
			menuList = menuRepos.getAllSubMenuListForEmployeeForParentMenuId(empId, parentMenuId);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of menu : ", e);
		}
		if (!menuList.isEmpty()) {
			for (MasMenuBO masMenuBO : menuList) {
				MisMenuBean misMenuBeanObject = new MisMenuBean();
				misMenuBeanObject.setMenuId(masMenuBO.getPkMenuId());
				misMenuBeanObject.setMenuName(masMenuBO.getMenuName());
				misMenuBeanObject.setActiveStatus(masMenuBO.getActiveStatus());
				misMenuBeanObject.setIsParent(masMenuBO.getIsParent());
				misMenuBeanObject.setMenuDescription(masMenuBO.getMenuDescription());
				misMenuBeanObject.setParentId(masMenuBO.getMenuParentId());
				misMenuBeanObject.setModuleId(masMenuBO.getModuleId());
				misMenuBeanObject.setMenuUrl(masMenuBO.getMenuUrl());
				misMenuBeanObject.setMenuKey(masMenuBO.getMenuKey());
				menuBeanList.add(misMenuBeanObject);
			}
		}
		return menuBeanList;
	}

	/**
	 * 
	 * <Description getAllTopLevelMenusForEmployee:> Retrieves all main menus
	 * for the employee
	 * 
	 * @param empId
	 *            Employee Id
	 *
	 * @return List of {@link MisMenuBean} containing menu information
	 * 
	 * @throws DataAccessException
	 *             In case of any exceptions
	 */
	public List<MisMenuBean> getAllTopLevelMenusForEmployee(Integer empId) throws DataAccessException {
		List<MisMenuBean> menuBeanList = new ArrayList<MisMenuBean>();
		List<MasMenuBO> menuList = new CopyOnWriteArrayList<MasMenuBO>();
		List<MasMenuBO> tempMenuList = new ArrayList<MasMenuBO>(5);
		DatEmpModuleMappingBO moduleMappingDetails = new DatEmpModuleMappingBO();
		List<MasMenuBO> mainmenuList = new ArrayList<MasMenuBO>();
		Map<Integer, MasMenuBO> menuMap = new TreeMap<Integer, MasMenuBO>();
		try {
			menuList = menuRepos.getAllTopLevelMenusForEmployee(empId);
			moduleMappingDetails = moduleMappingDetailsRepository.findByFkEmployeeId(empId);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of menu : ", e);
		}
		if (moduleMappingDetails != null) {
			for (MasMenuBO masMenuBO : menuList) {
				if (moduleMappingDetails.getFirstModule() == masMenuBO.getPkMenuId()) {
					// tempMenuList.add(0,masMenuBO);
					menuMap.put(1, masMenuBO);
				}

				if (moduleMappingDetails.getSecondModule() == masMenuBO.getPkMenuId()) {
					// tempMenuList.add(1,masMenuBO);
					menuMap.put(2, masMenuBO);
				}

				if (moduleMappingDetails.getThirdModule() == masMenuBO.getPkMenuId()) {
					// tempMenuList.add(2,masMenuBO);
					menuMap.put(3, masMenuBO);
				}

				if (moduleMappingDetails.getFourthModule() == masMenuBO.getPkMenuId()) {
					// tempMenuList.add(3,masMenuBO);
					menuMap.put(4, masMenuBO);

				}
				if (moduleMappingDetails.getFifthModule() == masMenuBO.getPkMenuId()) {
					// tempMenuList.add(4,masMenuBO);
					menuMap.put(5, masMenuBO);

				}
			}

			for (Map.Entry<Integer, MasMenuBO> entry : menuMap.entrySet()) {
				// LOG.info("tempMenuList:"+entry.getValue());
				tempMenuList.add(entry.getValue());
			}
			mainmenuList.addAll(tempMenuList);
			for (MasMenuBO menu : menuList) {
				boolean hasFound = false;
				for (int i = 0; i < tempMenuList.size(); i++) {
					if (menu.getPkMenuId() == tempMenuList.get(i).getPkMenuId()) {
						hasFound = true;
						break;
					} else {
						hasFound = false;
					}

				}
				if (!hasFound) {
					if (!mainmenuList.contains(menu))
						mainmenuList.add(mainmenuList.size(), menu);
				}
			}

			if (!mainmenuList.isEmpty()) {
				for (MasMenuBO masMenuBO : mainmenuList) {
					MisMenuBean misMenuBeanObject = new MisMenuBean();
					misMenuBeanObject.setMenuId(masMenuBO.getPkMenuId());
					misMenuBeanObject.setMenuName(masMenuBO.getMenuName());
					misMenuBeanObject.setActiveStatus(masMenuBO.getActiveStatus());
					misMenuBeanObject.setIsParent(masMenuBO.getIsParent());
					misMenuBeanObject.setMenuDescription(masMenuBO.getMenuDescription());
					misMenuBeanObject.setParentId(masMenuBO.getMenuParentId());
					misMenuBeanObject.setModuleId(masMenuBO.getModuleId());
					misMenuBeanObject.setMenuUrl(masMenuBO.getMenuUrl());
					misMenuBeanObject.setMenuKey(masMenuBO.getMenuKey());
					menuBeanList.add(misMenuBeanObject);
				}
			}
		} else {

			for (MasMenuBO masMenuBO : menuList) {
				MisMenuBean misMenuBeanObject = new MisMenuBean();
				misMenuBeanObject.setMenuId(masMenuBO.getPkMenuId());
				misMenuBeanObject.setMenuName(masMenuBO.getMenuName());
				misMenuBeanObject.setActiveStatus(masMenuBO.getActiveStatus());
				misMenuBeanObject.setIsParent(masMenuBO.getIsParent());
				misMenuBeanObject.setMenuDescription(masMenuBO.getMenuDescription());
				misMenuBeanObject.setParentId(masMenuBO.getMenuParentId());
				misMenuBeanObject.setModuleId(masMenuBO.getModuleId());
				misMenuBeanObject.setMenuUrl(masMenuBO.getMenuUrl());
				misMenuBeanObject.setMenuKey(masMenuBO.getMenuKey());
				menuBeanList.add(misMenuBeanObject);
			}

		}
		return menuBeanList;
	}

	/**
	 * 
	 * <Description getEmployeeLoginStatus:> Retrieves employee login status as
	 * active or inactive
	 * 
	 * @param empId
	 *            Employee Id
	 *
	 * @return boolean value
	 * 
	 * @throws DataAccessException
	 *             In case of any exceptions
	 */
	public Byte getEmployeeLoginStatus(Integer empId) throws DataAccessException {
		Byte status = 0;
		DatEmpDetailBO empBO = new DatEmpDetailBO();
		try {
			empBO = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve employee login status : ", e);
		}
		if (empBO != null) {
			status = empBO.getFkEmpMainStatus();
		}
		return status;
	}

	/**
	 * <Description getAllPermanentAndGraduatedEmployees:> Fetch All Permanent
	 * and Graduated Employees
	 * 
	 * @param
	 * @return List<EmployeesBean>
	 * @throws DataAccessException
	 */
	public List<EmployeesBean> getAllPermanentAndGraduatedEmployees() throws DataAccessException {
		List<EmployeesBean> permanentAndGraduatedList = new ArrayList<EmployeesBean>();
		List<DatEmpPersonalDetailBO> emplist;
		try {
			List<Integer> empIDList = new ArrayList<Integer>();
			String[] empIdArray = empIds.split(",");
			for (String s : empIdArray)
				empIDList.add(Integer.parseInt(s));
			emplist = employeeRepos.getAllPermanentAndGraduatedEmployeesNameAndId(empIDList);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of All Permanent and Graduated Employees :", e);
		}
		if (!emplist.isEmpty()) {
			for (DatEmpPersonalDetailBO datEmpPersonalDetailBO : emplist) {
				EmployeesBean permanentAndGraduatedObject = new EmployeesBean();
				permanentAndGraduatedObject.setEmployeeId(datEmpPersonalDetailBO.getFkEmpDetailId());
				permanentAndGraduatedObject.setEmployeeFirstName(datEmpPersonalDetailBO.getEmpFirstName());
				permanentAndGraduatedObject.setEmployeeLastName(datEmpPersonalDetailBO.getEmpLastName());
				permanentAndGraduatedObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
						+ datEmpPersonalDetailBO.getEmpLastName() + "-" + datEmpPersonalDetailBO.getFkEmpDetailId());
				// Added by Shyam for setting of designation into EmployeesBean
				permanentAndGraduatedObject.setDesignation(datEmpPersonalDetailBO.getEmpDesignationName());
				// End of addition by Shyam for setting of designation into
				// EmployeesBean
				permanentAndGraduatedList.add(permanentAndGraduatedObject);
			}
		}
		return permanentAndGraduatedList;
	}

	// added by pratibha
	/**
	 * 
	 * @param id
	 * @return List<EmployeesBean> Description:This service is implemented to
	 *         get the details of reportees.
	 */
	/*
	 * private List<DatEmpProfessionalDetailBO> getDomainReportees(Integer id) {
	 * return datEmpPersonalDetailRepository.findByFkEmpDomainMgrId(id); }
	 */
	/*
	 * private List<DatEmpProfessionalDetailBO> getBuReportees(Short id) {
	 * return datEmpPersonalDetailRepository.findByFkEmpBuUnit(id); }
	 */
	private List<EmployeesBean> empBeanList = new ArrayList<EmployeesBean>();

	public List<EmployeesBean> getMyReporteesByEmpId(Integer loginUserId)
			throws DataAccessException, BusinessException {
		try {
			this.empBeanList.clear();
			DatEmpDetailBO datEmpDetail = new DatEmpDetailBO();
			List<DatEmpProfessionalDetailBO> professionalDetailList = new ArrayList<DatEmpProfessionalDetailBO>();
			// final short DOMAIN_MANAGER = 5;
			short BU_HEAD = 6;
			try {
				datEmpDetail = empDetailRepos.findByPkEmpId(loginUserId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (datEmpDetail != null) {
				short roleId = datEmpDetail.getFkEmpRoleId();
				/*
				 * if (roleId == DOMAIN_MANAGER) { professionalDetailList =
				 * this.getDomainReportees(datEmpDetail .getPkEmpId()); if
				 * (professionalDetailList.size() > 0) { List<Integer>
				 * allemployeeIds = professionalDetailList.stream()
				 * .map(x->x.getFkMainEmpDetailId())
				 * .collect(Collectors.toList()); if(allemployeeIds.size() >0){
				 * 
				 * List<DatEmpPersonalDetailBO> empList =
				 * empPersonalRepository.findByFkEmpDetailIdIn(allemployeeIds);
				 * 
				 * if(empList.size()>0){ empList.forEach(empBo->{ EmployeesBean
				 * employeesBean = new EmployeesBean();
				 * employeesBean.setEmployeeFirstName(empBo.getEmpFirstName());
				 * employeesBean.setEmployeeLastName(empBo.getEmpLastName());
				 * employeesBean.setEmployeeId(empBo.getFkEmpDetailId());
				 * employeesBean
				 * .setEmpFullNameWithId(empBo.getEmpFirstName()+" "+
				 * empBo.getEmpLastName()+"-"+empBo.getFkEmpDetailId());
				 * employeesBean
				 * .setDesignation(this.getEmployeeDesignation(empBo
				 * .getFkEmpDetailId())); empBeanList.add(employeesBean); }); }
				 * }
				 * 
				 * } } else
				 */if (roleId == BU_HEAD) {
					List<Short> buIds = new ArrayList<Short>();
					for (MasBuUnitBO masBo : buRepos.findByFkBuHeadEmpId(loginUserId)) {
						buIds.add(masBo.getPkBuUnitId());
					}
					professionalDetailList = this.getBuReporteesOnlyGradruated(buIds);
					if (professionalDetailList.size() > 0) {
						List<Integer> allemployeeIds = professionalDetailList.stream()
								.map(x -> x.getFkMainEmpDetailId()).collect(Collectors.toList());
						if (allemployeeIds.size() > 0) {
							List<Integer> empIDList = new ArrayList<Integer>();
							String[] empIdArray = empIds.split(",");
							for (String s : empIdArray)
								empIDList.add(Integer.parseInt(s));
							List<DatEmpPersonalDetailBO> empList = empPersonalRepository
									.getBuHeadDetails(allemployeeIds, empIDList);
							if (empList.size() > 0) {
								empList.forEach(empBo -> {
									EmployeesBean employeesBean = new EmployeesBean();
									employeesBean.setEmployeeFirstName(empBo.getEmpFirstName());
									employeesBean.setEmployeeLastName(empBo.getEmpLastName());
									employeesBean.setEmployeeId(empBo.getFkEmpDetailId());
									employeesBean.setEmpFullNameWithId(empBo.getEmpFirstName() + " "
											+ empBo.getEmpLastName() + "-" + empBo.getFkEmpDetailId());
									employeesBean.setDesignation(this.getEmployeeDesignation(empBo.getFkEmpDetailId()));
									empBeanList.add(employeesBean);
								});
							}
						}
					}
				} else {
					List<EmployeeNameWithRMIdBean> allDirectAndIndirectReportees = this
							.getAllDirectAndIndirectGraduatedReporteesWithRMIdUnderRM(datEmpDetail.getPkEmpId());
					Collection<EmployeeNameWithRMIdBean> nonDuplicatedEmployees = allDirectAndIndirectReportees.stream()
							.<Map<Integer, EmployeeNameWithRMIdBean>>collect(HashMap::new,
									(m, e) -> m.put(e.getEmpId(), e), Map::putAll)
							.values();
					nonDuplicatedEmployees.forEach(empBo -> {
						EmployeesBean employeesBean = new EmployeesBean();
						employeesBean.setEmployeeFirstName(empBo.getEmpFirstName());
						employeesBean.setEmployeeLastName(empBo.getEmpLastName());
						employeesBean.setEmployeeId(empBo.getEmpId());
						employeesBean.setEmpFullNameWithId(
								empBo.getEmpFirstName() + " " + empBo.getEmpLastName() + "-" + empBo.getEmpId());
						employeesBean.setDesignation(this.getEmployeeDesignation(empBo.getEmpId()));
						empBeanList.add(employeesBean);
					});
				}
			}
			Collections.sort(empBeanList, new Comparator<EmployeesBean>() {
				public int compare(EmployeesBean o1, EmployeesBean o2) {
					return -o2.getEmployeeId().compareTo(o1.getEmployeeId());
				}
			});
		} catch (Exception e) {
			throw new BusinessException("Error Occured while fetching from DB", e);
		}
		return empBeanList;
	}

	/**
	 * 
	 * @param empId
	 * @return List<MasBuUnitBean>
	 * @throws CommonCustomException
	 *             Description:This Service has been implemented to fetch BU
	 *             details based on employee Id.
	 */
	public List<MasBuUnitBean> getBuUnitName(int empId) throws CommonCustomException {
		List<MasBuUnitBean> buUnitBeanList = new ArrayList<MasBuUnitBean>();
		List<MasBuUnitBO> masBuUnitList = new ArrayList<MasBuUnitBO>();
		DatEmpDetailBO empDetailBo = new DatEmpDetailBO();
		try {
			empDetailBo = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception ex) {
			throw new CommonCustomException("Failed to fetch details from db.");
		}
		if (empDetailBo == null) {
			throw new CommonCustomException("Invalid employee Id.");
		}
		try {
			masBuUnitList = buRepos.findByFkBuHeadEmpId(empId);
		} catch (Exception ex) {
			throw new CommonCustomException("Failed to fetch details from db.");
		}
		if (masBuUnitList.size() == 0) {
			throw new CommonCustomException("Invalid BuHead employee Id.");
		}
		for (MasBuUnitBO masBuUnitBO : masBuUnitList) {
			if (masBuUnitBO.getActiveFlag().equals("ACTIVE")) {
				MasBuUnitBean buUnitBean = new MasBuUnitBean();
				buUnitBean.setBuUnitId(masBuUnitBO.getPkBuUnitId());
				buUnitBean.setBuUnitName(masBuUnitBO.getBuUnitName());
				buUnitBeanList.add(buUnitBean);
			}
		}
		return buUnitBeanList;
	}

	/**
	 * 
	 * @return List<GetAllCurrencyTypeOutputBean>
	 * @throws CommonCustomException
	 *             Description:This Service has been implemented to fetch all
	 *             currency type details.
	 */
	public List<GetAllCurrencyTypeOutputBean> getAllCorrencyTypes() throws CommonCustomException {
		List<MasCurrencytypeBO> masCurrencyTypeBOList = new ArrayList<MasCurrencytypeBO>();
		List<GetAllCurrencyTypeOutputBean> getAllCurrencyTypeOutputBeanList = new ArrayList<GetAllCurrencyTypeOutputBean>();
		try {
			masCurrencyTypeBOList = masCurrencyTypeRepos.findAll();
		} catch (Exception ex) {
			throw new CommonCustomException("Failed to fetch details from db.");
		}
		for (MasCurrencytypeBO masCurrencytypeBO : masCurrencyTypeBOList) {
			GetAllCurrencyTypeOutputBean getAllCurrencyTypeOutputBean = new GetAllCurrencyTypeOutputBean();
			getAllCurrencyTypeOutputBean.setPkCurrencyTypeID(masCurrencytypeBO.getPk_CurrencyType_ID());
			getAllCurrencyTypeOutputBean.setCurrencyTypeCode(masCurrencytypeBO.getCurrencyTypeCode());
			getAllCurrencyTypeOutputBean.setCurrencyTypeDesc(masCurrencytypeBO.getCurrencyTypeDesc());
			getAllCurrencyTypeOutputBeanList.add(getAllCurrencyTypeOutputBean);
		}
		return getAllCurrencyTypeOutputBeanList;
	}

	public ModuleMappingOutputBean updateMappingDetails(UpdateModuleMappingInputBean updateModuleMappingInputBean)
			throws CommonCustomException {

		DatEmpModuleMappingBO datEmpModuleMappingBO = null;
		DatEmpModuleMappingBO tempDatEmpModuleMappingBO = new DatEmpModuleMappingBO();
		DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
		try {
			datEmpDetailBO = empDetailRepos.findByPkEmpId(updateModuleMappingInputBean.getEmployeeId());
		} catch (Exception ex) {
			throw new CommonCustomException("Failed to fetch employee details from db.");
		}

		if (datEmpDetailBO == null)
			throw new CommonCustomException("You are inactive employee");

		try {
			datEmpModuleMappingBO = moduleMappingDetailsRepository
					.findByFkEmployeeId(updateModuleMappingInputBean.getEmployeeId());
		} catch (Exception ex) {
			throw new CommonCustomException("Failed to fetch details from db.");

		}
		ModuleMappingOutputBean moduleMappingOutputBean = new ModuleMappingOutputBean();
		if (datEmpModuleMappingBO != null) {
			datEmpModuleMappingBO.setFkEmployeeId(Integer.valueOf(updateModuleMappingInputBean.getEmployeeId()));
			datEmpModuleMappingBO.setFirstModule(updateModuleMappingInputBean.getFirstModuleId());
			datEmpModuleMappingBO.setSecondModule(updateModuleMappingInputBean.getSecondModuleId());
			datEmpModuleMappingBO.setThirdModule(updateModuleMappingInputBean.getThirdModuleId());
			datEmpModuleMappingBO.setFourthModule(updateModuleMappingInputBean.getForthModuleId());
			datEmpModuleMappingBO.setFifthModule(updateModuleMappingInputBean.getFifthModuleId());
			tempDatEmpModuleMappingBO = moduleMappingDetailsRepository.save(datEmpModuleMappingBO);

		} else {
			datEmpModuleMappingBO = new DatEmpModuleMappingBO();
			datEmpModuleMappingBO.setFkEmployeeId(Integer.valueOf(updateModuleMappingInputBean.getEmployeeId()));
			datEmpModuleMappingBO.setFirstModule(updateModuleMappingInputBean.getFirstModuleId());
			datEmpModuleMappingBO.setSecondModule(updateModuleMappingInputBean.getSecondModuleId());
			datEmpModuleMappingBO.setThirdModule(updateModuleMappingInputBean.getThirdModuleId());
			datEmpModuleMappingBO.setFourthModule(updateModuleMappingInputBean.getForthModuleId());
			datEmpModuleMappingBO.setFifthModule(updateModuleMappingInputBean.getFifthModuleId());
			moduleMappingDetailsRepository.save(datEmpModuleMappingBO);

			tempDatEmpModuleMappingBO = moduleMappingDetailsRepository
					.findByFkEmployeeId(updateModuleMappingInputBean.getEmployeeId());
		}

		LOG.info("tempDatEmpModuleMappingBO :: " + tempDatEmpModuleMappingBO.toString());
		moduleMappingOutputBean.setFkEmployeeId(tempDatEmpModuleMappingBO.getFkEmployeeId());
		moduleMappingOutputBean.setFirstModuleId(tempDatEmpModuleMappingBO.getFirstModule());
		moduleMappingOutputBean.setFirstModuleName(tempDatEmpModuleMappingBO.getFirstMasMenuBO().getMenuName());
		moduleMappingOutputBean.setSecondModuleId(tempDatEmpModuleMappingBO.getSecondModule());
		moduleMappingOutputBean.setSecondModuleName(tempDatEmpModuleMappingBO.getSecondMasMenuBO().getMenuName());
		moduleMappingOutputBean.setThirdModuleId(tempDatEmpModuleMappingBO.getThirdModule());
		moduleMappingOutputBean.setThirdModuleName(tempDatEmpModuleMappingBO.getThirdMasMenuBO().getMenuName());
		moduleMappingOutputBean.setFourthModuleId(tempDatEmpModuleMappingBO.getFourthModule());
		moduleMappingOutputBean.setFourthModuleName(tempDatEmpModuleMappingBO.getFourthMasMenuBO().getMenuName());
		moduleMappingOutputBean.setFifthModuleId(tempDatEmpModuleMappingBO.getFifthModule());
		moduleMappingOutputBean.setFifthModuleName(tempDatEmpModuleMappingBO.getFifthMasMenuBO().getMenuName());

		return moduleMappingOutputBean;
	}

	// ended by pratibha

	// added by Rajesh for GSR only Graduated Emp
	public List<EmployeeNameWithRMIdBean> getAllDirectAndIndirectGraduatedReporteesWithRMIdUnderRM(
			Integer reportingManagerId) throws DataAccessException, BusinessException {
		List<EmployeeNameWithRMIdBean> allReporteesList = new ArrayList<EmployeeNameWithRMIdBean>();
		List<EmployeeNameWithRMIdBean> allDirectReporteesList = new ArrayList<EmployeeNameWithRMIdBean>();
		allDirectAndIndirectReporteesList = new ArrayList<EmployeeNameWithRMIdBean>();
		try {
			allDirectReporteesList = getAllDirectGraduatedReporteesWithRMIdForRmId(reportingManagerId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve direct reportees :  ", e);
		}
		if (!allDirectReporteesList.isEmpty()) {
			for (EmployeeNameWithRMIdBean employeeNameWithRMIdObj : allDirectReporteesList) {
				if (!allReporteesList.contains(employeeNameWithRMIdObj))
					allReporteesList.add(employeeNameWithRMIdObj);
				if (employeeNameWithRMIdObj.getReporteeFlag().equalsIgnoreCase("YES_HAS_REPORTEES")) {
					List<EmployeeNameWithRMIdBean> allEmployeeList = new ArrayList<EmployeeNameWithRMIdBean>();
					allEmployeeList = findDirectAndIndirectGraduatedReportees(employeeNameWithRMIdObj.getEmpId());
					if (!allEmployeeList.isEmpty()) {
						for (EmployeeNameWithRMIdBean employeeNameWithRMIdBean : allEmployeeList) {
							if (!allReporteesList.contains(employeeNameWithRMIdBean)) {
								allReporteesList.add(employeeNameWithRMIdBean);
							}
						}
					}
				}
			}
		} else {
			for (EmployeeNameWithRMIdBean employeeObject : allDirectReporteesList) {
				if (!allReporteesList.contains(employeeObject))
					allReporteesList.add(employeeObject);
			}
		}
		return allReporteesList;
	}

	public List<EmployeeNameWithRMIdBean> getAllDirectGraduatedReporteesWithRMIdForRmId(Integer rmId)
			throws DataAccessException {
		List<MyReporteesBean> reporteesList;
		List<EmployeeNameWithRMIdBean> allDirectReporteeList = new ArrayList<EmployeeNameWithRMIdBean>();
		try {
			List<Integer> empIDList = new ArrayList<Integer>();
			String[] empIdArray = empIds.split(",");
			for (String s : empIdArray)
				empIDList.add(Integer.parseInt(s));
			reporteesList = empDetailRepos.getAllDirectReporteesWithRMIdForRMIdOnlyGraduatedForGSR(rmId, empIDList);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of direct reportees :  ", e);
		}
		LOG.startUsecase("reporteesList size:" + reporteesList.size());
		if (!reporteesList.isEmpty()) {
			for (MyReporteesBean datEmpDetailBOObject : reporteesList) {
				EmployeeNameWithRMIdBean employeeNameWithRMIdBeanObject = new EmployeeNameWithRMIdBean();
				employeeNameWithRMIdBeanObject.setEmpId(datEmpDetailBOObject.getEmpId());
				employeeNameWithRMIdBeanObject.setEmpFirstName(datEmpDetailBOObject.getEmpFirstName());
				employeeNameWithRMIdBeanObject.setEmpLastName(datEmpDetailBOObject.getEmpLastName());
				employeeNameWithRMIdBeanObject.setMgrId(datEmpDetailBOObject.getEmpReportingMgrId());
				employeeNameWithRMIdBeanObject.setReporteeFlag(datEmpDetailBOObject.getHasReporteesFlag());
				employeeNameWithRMIdBeanObject.setEmpFullNameWithId(datEmpDetailBOObject.getEmpFirstName() + " "
						+ datEmpDetailBOObject.getEmpLastName() + "-" + datEmpDetailBOObject.getEmpId());
				employeeNameWithRMIdBeanObject.setDesignation(getEmployeeDesignation(datEmpDetailBOObject.getEmpId()));
				allDirectReporteeList.add(employeeNameWithRMIdBeanObject);
			}
		}
		return allDirectReporteeList;
	}

	/**
	 * <Description findDirectAndIndirectReportees>
	 * 
	 * @param int
	 *            reportingMgrId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws BusinessException
	 * 
	 **/
	private List<EmployeeNameWithRMIdBean> findDirectAndIndirectGraduatedReportees(int reportingMgrId)
			throws BusinessException {
		List<EmployeeNameWithRMIdBean> tempList = new ArrayList<EmployeeNameWithRMIdBean>();
		try {
			tempList = getAllDirectGraduatedReporteesWithRMIdForRmId(reportingMgrId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve reportees", e);
		}
		if (!tempList.isEmpty()) {
			for (EmployeeNameWithRMIdBean empNameWithRMIdObject : tempList) {
				if (!allDirectAndIndirectReporteesList.contains(empNameWithRMIdObject))
					allDirectAndIndirectReporteesList.add(empNameWithRMIdObject);
				if (empNameWithRMIdObject.getReporteeFlag().equalsIgnoreCase("YES_HAS_REPORTEES")) {
					List<EmployeeNameWithRMIdBean> allReporteesList = new ArrayList<EmployeeNameWithRMIdBean>();
					allReporteesList = findDirectAndIndirectGraduatedReportees(empNameWithRMIdObject.getEmpId());
					if (!allReporteesList.isEmpty()) {
						for (EmployeeNameWithRMIdBean employeeObj : allReporteesList) {
							if (!allDirectAndIndirectReporteesList.contains(employeeObj))
								allDirectAndIndirectReporteesList.add(employeeObj);
						}
					}
				}
			}
		}
		return allDirectAndIndirectReporteesList;
	}

	private List<DatEmpProfessionalDetailBO> getBuReporteesOnlyGradruated(List<Short> id) {
		return datEmpPersonalDetailRepository.findByFkEmpBuUnitOnlyGraduated(id);
	}

	// EOA by Rajesh
	// Added by Rajesh
	/**
	 * <Description getAllDirectAndIndirectReporteesWithRMIdUnderRM:> Fetch all
	 * direct and indirect reportees under a manager
	 * 
	 * @param int
	 *            reportingManagerId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws DataAccessException
	 * @throws BusinessException
	 */
	public List<EmployeeNameWithRMIdBean> getAllIndirectReporteeUnderDM(Integer reportingManagerId)
			throws DataAccessException, BusinessException {
		List<EmployeeNameWithRMIdBean> allReporteesList = new ArrayList<EmployeeNameWithRMIdBean>();
		List<EmployeeNameWithRMIdBean> allDirectReporteesList = new ArrayList<EmployeeNameWithRMIdBean>();
		allDirectAndIndirectReporteesList = new ArrayList<EmployeeNameWithRMIdBean>();
		LOG.startUsecase("GET Indirect Emp Based on DM ID");
		try {
			allDirectReporteesList = getAllDirectReporteesWithDMIdForRmId(reportingManagerId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve direct reportees :  ", e);
		}
		if (!allDirectReporteesList.isEmpty()) {
			for (EmployeeNameWithRMIdBean employeeNameWithRMIdObj : allDirectReporteesList) {
				if (!allReporteesList.contains(employeeNameWithRMIdObj)) {
					allReporteesList.add(employeeNameWithRMIdObj);
					if (employeeNameWithRMIdObj.getReporteeFlag().equalsIgnoreCase("YES_HAS_REPORTEES")) {
						List<EmployeeNameWithRMIdBean> allEmployeeList = new ArrayList<EmployeeNameWithRMIdBean>();
						allEmployeeList = findDirectAndIndirectConfirmedReportees(employeeNameWithRMIdObj.getEmpId());
						if (!allEmployeeList.isEmpty()) {
							for (EmployeeNameWithRMIdBean employeeNameWithRMIdBean : allEmployeeList) {
								if (!allReporteesList.contains(employeeNameWithRMIdBean)) {
									allReporteesList.add(employeeNameWithRMIdBean);
								}
							}
						}
					}
				}
			}
		} /*
			 * else { for (EmployeeNameWithRMIdBean employeeObject :
			 * allDirectReporteesList) { if
			 * (!allReporteesList.contains(employeeObject))
			 * allReporteesList.add(employeeObject); } }
			 */
		LOG.endUsecase("GET Indirect Emp Based on DM ID");
		return allReporteesList;
	}

	/**
	 * <Description getAllDirectReporteesWithRMIdForRmId:> Fetch all direct
	 * reportees under a manager
	 * 
	 * @param Integer
	 *            rmId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws DataAccessException
	 */
	public List<EmployeeNameWithRMIdBean> getAllDirectReporteesWithDMIdForRmId(Integer rmId)
			throws DataAccessException {
		List<DatEmpDetailBO> reporteesList;
		List<EmployeeNameWithRMIdBean> allDirectReporteeList = new ArrayList<EmployeeNameWithRMIdBean>();
		try {
			List<Integer> empIDList = new ArrayList<Integer>();
			String[] empIdArray = empIds.split(",");
			for (String s : empIdArray)
				empIDList.add(Integer.parseInt(s));
			reporteesList = empDetailRepos.getAllDirectReporteesWithDMIdForRMId(rmId, empIDList);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of direct reportees :  ", e);
		}
		if (!reporteesList.isEmpty()) {
			for (DatEmpDetailBO datEmpDetailBOObject : reporteesList) {
				EmployeeNameWithRMIdBean employeeNameWithRMIdBeanObject = new EmployeeNameWithRMIdBean();
				employeeNameWithRMIdBeanObject.setEmpId(datEmpDetailBOObject.getPkEmpId());
				employeeNameWithRMIdBeanObject.setEmpFirstName(datEmpDetailBOObject.getEmpFirstName());
				employeeNameWithRMIdBeanObject.setEmpLastName(datEmpDetailBOObject.getEmpLastName());
				employeeNameWithRMIdBeanObject.setMgrId(datEmpDetailBOObject.getEmpRMId());
				employeeNameWithRMIdBeanObject.setReporteeFlag(datEmpDetailBOObject.getReporteeFlag());
				employeeNameWithRMIdBeanObject.setEmpFullNameWithId(datEmpDetailBOObject.getEmpFirstName() + " "
						+ datEmpDetailBOObject.getEmpLastName() + "-" + datEmpDetailBOObject.getPkEmpId());
				employeeNameWithRMIdBeanObject
						.setDesignation(getEmployeeDesignation(datEmpDetailBOObject.getPkEmpId()));
				allDirectReporteeList.add(employeeNameWithRMIdBeanObject);
			}
		}
		return allDirectReporteeList;
	}

	/**
	 * <Description findDirectAndIndirectReportees>
	 * 
	 * @param int
	 *            reportingMgrId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws BusinessException
	 * 
	 **/
	private List<EmployeeNameWithRMIdBean> findDirectAndIndirectConfirmedReportees(int reportingMgrId)
			throws BusinessException {
		List<EmployeeNameWithRMIdBean> tempList = new ArrayList<EmployeeNameWithRMIdBean>();
		try {
			tempList = getAllDirectReporteesWithDMIdForRmId(reportingMgrId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve reportees", e);
		}
		if (!tempList.isEmpty()) {
			for (EmployeeNameWithRMIdBean empNameWithRMIdObject : tempList) {
				if (!allDirectAndIndirectReporteesList.contains(empNameWithRMIdObject))
					allDirectAndIndirectReporteesList.add(empNameWithRMIdObject);
				if (empNameWithRMIdObject.getReporteeFlag().equalsIgnoreCase("YES_HAS_REPORTEES")) {
					List<EmployeeNameWithRMIdBean> allReporteesList = new ArrayList<EmployeeNameWithRMIdBean>();
					allReporteesList = findDirectAndIndirectReportees(empNameWithRMIdObject.getEmpId());
					if (!allReporteesList.isEmpty()) {
						for (EmployeeNameWithRMIdBean employeeObj : allReporteesList) {
							if (!allDirectAndIndirectReporteesList.contains(employeeObj))
								allDirectAndIndirectReporteesList.add(employeeObj);
						}
					}
				}
			}
		}
		return allDirectAndIndirectReporteesList;
	}

	/**
	 * <Description getAllDirectReporteesWithRMIdForRmId:> Fetch all direct
	 * reportees under a manager
	 * 
	 * @param Integer
	 *            rmId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws DataAccessException
	 */
	public List<EmployeeNameWithRMIdBean> getDirectReporteesWithDMIdForRmId(Integer rmId) throws DataAccessException {
		List<DatEmpDetailBO> reporteesList;
		List<EmployeeNameWithRMIdBean> allDirectReporteeList = new ArrayList<EmployeeNameWithRMIdBean>();
		try {
			List<Integer> empIDList = new ArrayList<Integer>();
			String[] empIdArray = empIds.split(",");
			for (String s : empIdArray)
				empIDList.add(Integer.parseInt(s));
			reporteesList = empDetailRepos.getDirectReporteesWithDMIdForRMId(rmId, empIDList);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of direct reportees :  ", e);
		}
		if (!reporteesList.isEmpty()) {
			for (DatEmpDetailBO datEmpDetailBOObject : reporteesList) {
				EmployeeNameWithRMIdBean employeeNameWithRMIdBeanObject = new EmployeeNameWithRMIdBean();
				employeeNameWithRMIdBeanObject.setEmpId(datEmpDetailBOObject.getPkEmpId());
				employeeNameWithRMIdBeanObject.setEmpFirstName(datEmpDetailBOObject.getEmpFirstName());
				employeeNameWithRMIdBeanObject.setEmpLastName(datEmpDetailBOObject.getEmpLastName());
				employeeNameWithRMIdBeanObject.setMgrId(datEmpDetailBOObject.getEmpRMId());
				employeeNameWithRMIdBeanObject.setReporteeFlag(datEmpDetailBOObject.getReporteeFlag());
				employeeNameWithRMIdBeanObject.setEmpFullNameWithId(datEmpDetailBOObject.getEmpFirstName() + " "
						+ datEmpDetailBOObject.getEmpLastName() + "-" + datEmpDetailBOObject.getPkEmpId());
				employeeNameWithRMIdBeanObject
						.setDesignation(getEmployeeDesignation(datEmpDetailBOObject.getPkEmpId()));
				allDirectReporteeList.add(employeeNameWithRMIdBeanObject);
			}
		}
		return allDirectReporteeList;
	}

	// EOA by Rajesh
	// Added by Rajesh Kumar
	/**
	 * <Description getAllPermanentAndGraduatedEmployees:> Fetch All Permanent
	 * and Graduated Employees
	 * 
	 * @param
	 * @return List<EmployeesBean>
	 * @throws DataAccessException
	 */
	public List<EmployeesBean> getAllPermanentAndGraduatedActiveEmployees() throws DataAccessException {
		List<EmployeesBean> permanentAndGraduatedList = new ArrayList<EmployeesBean>();
		List<DatEmpPersonalDetailBO> emplist;
		try {
			List<Integer> empIDList = new ArrayList<Integer>();
			String[] empIdArray = empIds.split(",");
			for (String s : empIdArray)
				empIDList.add(Integer.parseInt(s));
			emplist = employeeRepos.getAllPermanentAndGraduatedActiveEmployeesNameAndId(empIDList);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of All Permanent and Graduated Employees :", e);
		}
		if (!emplist.isEmpty()) {
			for (DatEmpPersonalDetailBO datEmpPersonalDetailBO : emplist) {
				EmployeesBean permanentAndGraduatedObject = new EmployeesBean();
				permanentAndGraduatedObject.setEmployeeId(datEmpPersonalDetailBO.getFkEmpDetailId());
				permanentAndGraduatedObject.setEmployeeFirstName(datEmpPersonalDetailBO.getEmpFirstName());
				permanentAndGraduatedObject.setEmployeeLastName(datEmpPersonalDetailBO.getEmpLastName());
				permanentAndGraduatedObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
						+ datEmpPersonalDetailBO.getEmpLastName() + "-" + datEmpPersonalDetailBO.getFkEmpDetailId());
				// Added by Shyam for setting of designation into EmployeesBean
				permanentAndGraduatedObject.setDesignation(datEmpPersonalDetailBO.getEmpDesignationName());
				// End of addition by Shyam for setting of designation into
				// EmployeesBean
				permanentAndGraduatedList.add(permanentAndGraduatedObject);
			}
		}
		return permanentAndGraduatedList;
	}

	// EOA by Rajesh Kumar
	public List<EmployeeNameWithRMIdBean> getEmployeeDetailsByEmpId(Integer empId) throws DataAccessException {
		List<EmployeeNameWithRMIdBean> empInfoList = new ArrayList<EmployeeNameWithRMIdBean>();
		List<DatEmpDetailBO> empInfo;
		try {
			empInfo = empDetailRepos.getEmpInfoByEmpId(empId);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve  employee info ", e);
		}
		if (!empInfo.isEmpty()) {
			for (DatEmpDetailBO datEmpDetailBOObject : empInfo) {
				EmployeeNameWithRMIdBean reportingManagerBeanObject = new EmployeeNameWithRMIdBean();
				reportingManagerBeanObject.setEmpId(datEmpDetailBOObject.getPkEmpId());
				reportingManagerBeanObject.setEmpFirstName(datEmpDetailBOObject.getEmpFirstName());
				reportingManagerBeanObject.setEmpLastName(datEmpDetailBOObject.getEmpLastName());
				reportingManagerBeanObject.setMgrId(datEmpDetailBOObject.getEmpRMId());
				reportingManagerBeanObject.setReporteeFlag(datEmpDetailBOObject.getReporteeFlag());
				reportingManagerBeanObject.setEmpFullNameWithId(datEmpDetailBOObject.getEmpFirstName() + " "
						+ datEmpDetailBOObject.getEmpLastName() + "-" + datEmpDetailBOObject.getPkEmpId());
				empInfoList.add(reportingManagerBeanObject);
			}
		}
		return empInfoList;
	}

	// Added by Kamal Anand
	/**
	 * @category This service is used to get All Reporting Manager and Skiplevel
	 *           Manager in a BU to which Employee belong
	 * 
	 * @param Integer
	 *            empId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws DataAccessException
	 */
	public List<EmployeeNameWithRMIdBean> getRmAndSkipLevelRmInBU(Integer empId) throws DataAccessException {
		LOG.startUsecase("Entering getRmAndSkipLevelRmInBU service");
		List<EmployeeNameWithRMIdBean> empList = null;
		List<Integer> empIDList = new ArrayList<Integer>();
		String[] empIdArray = empIds.split(",");
		for (String s : empIdArray)
			empIDList.add(Integer.parseInt(s));
		try {
			empList = datEmpPersonalDetailRepository.getRmAndSkipLevelRmInBUByEmpId(empId, empIDList);
		} catch (Exception e) {
			throw new DataAccessException("Some Exception occurred while fetching details from Db");
		}
		LOG.endUsecase("Exiting getRmAndSkipLevelRmInBU service");
		return empList;
	}
	// End of Addition by Kamal Anand

	// Added by Balaji
	/**
	 * @category This service is used to get All Domain Managers in a BU
	 * 
	 * @param buId
	 * @return dmDetailsOutputBean
	 * @throws CommonCustomException
	 */
	public List<DmDetailsOutputBean> getDomainManagerDetails(Short buId) throws CommonCustomException {
		LOG.startUsecase("Entering getDomainManagerDetails service");
		List<DmDetailsOutputBean> dmDetailsOutputBean = new ArrayList<DmDetailsOutputBean>();
		List<DatEmpPersonalDetailBO> personalDetails = new ArrayList<DatEmpPersonalDetailBO>();
		MasBuUnitBO buDetails = new MasBuUnitBO();
		try {
			buDetails = buRepos.findByPkBuUnitId(buId);
		} catch (Exception e) {

			throw new CommonCustomException("Failed to fetch details of BU");
		}
		if (buDetails == null) {
			throw new CommonCustomException("Please enter Valid Business Unit ID");
		}
		try {
			personalDetails = empPersonalRepository.getDmDetails(buId);
		} catch (Exception e) {

			throw new CommonCustomException("Failed to fetch details of BU");
		}
		if (personalDetails == null) {
			throw new CommonCustomException("Please enter Valid Business Unit ID");
		}
		for (DatEmpPersonalDetailBO datEmpPersonalDetailBO : personalDetails) {

			DmDetailsOutputBean outputBean = new DmDetailsOutputBean();
			outputBean.setEmpIdWithName(datEmpPersonalDetailBO.getFkEmpDetailId() + " - "
					+ datEmpPersonalDetailBO.getEmpFirstName() + " " + datEmpPersonalDetailBO.getEmpLastName());

			dmDetailsOutputBean.add(outputBean);
		}
		LOG.endUsecase("Exiting getDomainManagerDetails service");
		return dmDetailsOutputBean;
	}

	/**
	 * @category This service is used to get All Role details
	 * 
	 * @return roleDetailsOutputBean
	 * @throws CommonCustomException
	 */
	public List<RoleDetailsOutputBean> getRoleDetails() throws CommonCustomException {
		LOG.startUsecase("Entering getRoleDetails service");

		List<RoleDetailsOutputBean> roleDetailsOutputBean = new ArrayList<RoleDetailsOutputBean>();
		List<MasEmpRoleBO> roleDetails = new ArrayList<MasEmpRoleBO>();

		try {
			roleDetails = masEmpRolesRepository.findAll();

		} catch (Exception e) {

			throw new CommonCustomException("Failed to fetch details of all Roles");
		}
		if (roleDetails == null) {

			throw new CommonCustomException("Failed to fetch role details");

		}
		for (MasEmpRoleBO masEmpRoleBO : roleDetails) {

			RoleDetailsOutputBean roleOutputBean = new RoleDetailsOutputBean();

			roleOutputBean.setPkEmpRoleId(masEmpRoleBO.getPkEmpRoleId());
			roleOutputBean.setEmpRoleName(masEmpRoleBO.getEmpRoleName());
			roleOutputBean.setEmpRoleKeyName(masEmpRoleBO.getEmpRoleKeyName());
			roleDetailsOutputBean.add(roleOutputBean);
		}

		LOG.endUsecase("Exiting getRoleDetails service");
		return roleDetailsOutputBean;
	}
	// EOA by Balaji

	// Addition By Adavayya Hiremath
	/**
	 * 
	 * <Description getLocationDetailsByCountryId:> This method is used to
	 * display Location Details
	 * 
	 * @param countryId
	 * @return MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	public List<LocationByCountyBean> getLocationDetails(Short countryId)
			throws DataAccessException, CommonCustomException {
		List<LocationByCountyBean> detailsBeanList = new ArrayList<LocationByCountyBean>();
		List<MasLocationParentBO> locParentBO;

		try {
			locParentBO = (List<MasLocationParentBO>) parentLocationRepos.findByfkCountryId(countryId);

		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of Location details : ", e);
		}
		if (locParentBO.size() > 0) {
			for (MasLocationParentBO masLocParentBo : locParentBO) {
				LocationByCountyBean locationBeanObject = new LocationByCountyBean();

				locationBeanObject.setLocationId(masLocParentBo.getPkLocationParentId());
				locationBeanObject.setLocationName(masLocParentBo.getLocationParentName());
				detailsBeanList.add(locationBeanObject);
			}
		} else {
			throw new CommonCustomException("No countries are Found for given Country Id ");
		}
		return detailsBeanList;
	}

	// EOA By Adavayya Hiremath

	// Added by Rajesh Kumar
	public List<DatForexConversionsBO> getForexConversion(Byte fkForexCurrencyTypeId)
			throws DataAccessException, CommonCustomException {
		List<DatForexConversionsBO> detailsBeanList = new ArrayList<DatForexConversionsBO>();
		DatForexConversionsBO datForexConversionsBO = new DatForexConversionsBO();
		try {
			datForexConversionsBO = datForexConversionsRepository.findByFkForexCurrencyTypeId(fkForexCurrencyTypeId);
			if (datForexConversionsBO != null) {
				detailsBeanList.add(datForexConversionsBO);
			}
		} catch (Exception e) {
			throw new CommonCustomException("Failed to retrieve list of forex details : ", e);
		}
		return detailsBeanList;
	}
	// EOA by Rajesh Kumar

	/**
	 * 
	 * <Description getLocationDetailsByCountryId:> This method is used to
	 * display Location Details
	 * 
	 * @param countryId
	 * @return MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	public List<LocationByCountyBean> getPhysicalLocationDetails(Short countryId)
			throws DataAccessException, CommonCustomException {
		List<LocationByCountyBean> detailsBeanList = new ArrayList<LocationByCountyBean>();
		List<MasLocationPhysicalBO> locPhysicalBO;
		LocationByCountyBean locationBeanObject;

		try {
			locPhysicalBO = (List<MasLocationPhysicalBO>) phyLocationRepos.findByFkLocationParentId(countryId);

		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of Location details : ", e);
		}
		if (locPhysicalBO.size() > 0) {
			for (MasLocationPhysicalBO masLocParentBo : locPhysicalBO) {
				locationBeanObject = new LocationByCountyBean();

				locationBeanObject.setLocationId(masLocParentBo.getPkLocationPhysicalId());
				locationBeanObject.setLocationName(masLocParentBo.getLocationPhysicalName());
				detailsBeanList.add(locationBeanObject);
			}
		} else {
			throw new CommonCustomException("No countries are Found for given Country Id ");
		}
		return detailsBeanList;
	}

	public List<DatForexConversionsBO> getForexDetailsByDate(Date date)
			throws DataAccessException, CommonCustomException {
		List<DatForexConversionsBO> detailsBeanList = new ArrayList<DatForexConversionsBO>();
		try {
			detailsBeanList = datForexConversionsRepository.findDetailsByDates(date);
		} catch (Exception e) {
			throw new CommonCustomException("Failed to retrieve list of forex details : ", e);
		}
		return detailsBeanList;
	}

	public Boolean downloadVideos(String name, HttpServletResponse response) throws CommonCustomException {
	boolean result = false;
	try {
		if (name != null && name != "") {
			String download =name ;
//				String download =name +".mp4";
				String basePath = VIDIO_PATH;
				// response.setContentType("video/mp4");
				DownloadFileUtil.downloadVideoFile(response, basePath, download);
				result = true;
			} else {
				throw new CommonCustomException("name is mandatory");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		return result;
	}

	public List<AcountsManagerOutputBean> getAllAccountmanagerDetails(Short buId) throws DataAccessException {
		List<AcountsManagerOutputBean> outputBeanList = new ArrayList<AcountsManagerOutputBean>();
		List<DatEmpDetailBO> acntMgrList;
		try {
			List<String> empLevelNameList = Arrays.asList(accountManagerLevelNames.split(","));
			acntMgrList = empDetailRepos.getAllMgrDetails(empLevelNameList,buId);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of Account Managers :  ", e);
		}

		if (!acntMgrList.isEmpty()) {

			for (DatEmpDetailBO datEmpDetailBOObject : acntMgrList) {
				AcountsManagerOutputBean accountManagerBeanObject = new AcountsManagerOutputBean();
				accountManagerBeanObject.setAccountManagerId(datEmpDetailBOObject.getPkEmpId());
				if (datEmpDetailBOObject.getEmpMiddleName() == null) {
					accountManagerBeanObject.setAccountManagerName(
							datEmpDetailBOObject.getEmpFirstName() + " " + datEmpDetailBOObject.getEmpLastName());
				} else {
					accountManagerBeanObject.setAccountManagerName(datEmpDetailBOObject.getEmpFirstName() + " "
							+ datEmpDetailBOObject.getEmpMiddleName() + " " + datEmpDetailBOObject.getEmpLastName());
				}
				outputBeanList.add(accountManagerBeanObject);
			}
			Collections.sort(outputBeanList, new AcountsManagerOutputBean());
		}
		
		return outputBeanList;
	}

	public List<ManagerOutputBean> getAllDomainmanagerDetails(Short buId) throws DataAccessException {

		List<ManagerOutputBean> allDomainManagersList = new ArrayList<ManagerOutputBean>();
		List<DatEmpDetailBO> domainManagerList;
		try {
			List<String> empLevelNameList = Arrays.asList(domainManagerLevelNames.split(","));
			
			domainManagerList = empDetailRepos.getAllMgrDetails(empLevelNameList,buId);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of direct reportees :  ", e);
		}

		if (!domainManagerList.isEmpty()) {
			for (DatEmpDetailBO empBO : domainManagerList) {
				ManagerOutputBean bean = new ManagerOutputBean();
				bean.setEmpId(empBO.getPkEmpId());
				if (empBO.getEmpMiddleName() == null) {
					bean.setEmpName(empBO.getEmpFirstName() + " " + empBO.getEmpLastName());
				} else {
					bean.setEmpName(
							empBO.getEmpFirstName() + " " + empBO.getEmpMiddleName() + " " + empBO.getEmpLastName());
				}
				allDomainManagersList.add(bean);
			}
			Collections.sort(allDomainManagersList, new ManagerOutputBean());
		}
		
		return allDomainManagersList;
	}

	public List<ManagerOutputBean> getAllDeliveryManagerDetails(Short buId) throws DataAccessException {
		List<ManagerOutputBean> allDeliveryManagersList = new ArrayList<ManagerOutputBean>();
		List<DatEmpDetailBO> deliveryManagerList;
		try {
			List<String> empLevelNameList = Arrays.asList(deliveryManagerLevelNames.split(","));
			
			deliveryManagerList = empDetailRepos.getAllMgrDetails(empLevelNameList,buId);;
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of direct reportees :  ", e);
		}
		if (!deliveryManagerList.isEmpty()) {
			for (DatEmpDetailBO empBO : deliveryManagerList) {
				ManagerOutputBean bean = new ManagerOutputBean();
				bean.setEmpId(empBO.getPkEmpId());
				if (empBO.getEmpMiddleName() == null) {
					bean.setEmpName(empBO.getEmpFirstName() + " " + empBO.getEmpLastName());
				} else {
					bean.setEmpName(
							empBO.getEmpFirstName() + " " + empBO.getEmpMiddleName() + " " + empBO.getEmpLastName());
				}
				allDeliveryManagersList.add(bean);
			}
			Collections.sort(allDeliveryManagersList, new ManagerOutputBean());
		}
		return allDeliveryManagersList;
	}
	
		
		//Added by Lalith Kumar
		public List<MasVerticalBean>  getAllVerticals() throws DataAccessException
		{
			List<MasVerticalBean> listMasVerticalBean= new ArrayList<MasVerticalBean>();
			try{
				List<MasVerticalBO> listMasVerticalBo= new ArrayList<MasVerticalBO>();
			try{
				listMasVerticalBo = (List<MasVerticalBO>) masVerticalRepository.findAll();
				
			} catch(Exception e)
			{
				throw new DataAccessException("Failed to retrive list of all Verticals from Database");
			}
			
			if(!listMasVerticalBo.isEmpty())
			{
			for(MasVerticalBO masVerticalBo:listMasVerticalBo)
			{
				MasVerticalBean masVerticalBean = new MasVerticalBean();
				
				masVerticalBean.setVerticalId(masVerticalBo.getPkVerticalId());
				masVerticalBean.setVerticalName(masVerticalBo.getVerticalName());
				
				listMasVerticalBean.add(masVerticalBean);
			}
			}
			}catch(Exception e)
			{
			
				throw new DataAccessException(e.getMessage());


			}
			
			
			return listMasVerticalBean;
			
		}

		//EOA by Lalith Kumar

	public List<MasBusinessEntityBean> getAllBusinessEntities() throws DataAccessException {
			
		List<MasBusinessEntityBean> listBean = new ArrayList<MasBusinessEntityBean>();    
		try{
			List<MasBusinessEntityBO> beUnitBO =new ArrayList<MasBusinessEntityBO>();
		    MasBusinessEntityBean beBean;
		try{
			beUnitBO= (List<MasBusinessEntityBO>) masbeRepos.findAll();
		}
		catch(Exception e){
			throw new DataAccessException("Failed to retrive BussinessEntity Details");
		}
			if(!beUnitBO.isEmpty())
			{
			for(MasBusinessEntityBO bu : beUnitBO){
				 beBean = new MasBusinessEntityBean();
				 beBean.setEntityId(bu.getPkEntityId());
				 beBean.setEntityName(bu.getEntityName());
				 listBean.add(beBean);
		  }
			}
		}catch(Exception e){
			throw new DataAccessException(e.getMessage());
		}
		return listBean;
	}

		


public List<MasAccountBean>  getAllAccounts() throws DataAccessException
{
	List<MasAccountBean> listAccountBean= new ArrayList<MasAccountBean>();
	try{
		List<MasAccountBO> listAccountBo= new ArrayList<MasAccountBO>();
	
	try{
		listAccountBo = (List<MasAccountBO>) masAccountRepository.findAll();
		
	} catch(Exception e)
	{
		throw new DataAccessException(" Failed to retrive Details");
	}
	if(!listAccountBo.isEmpty())
	{
	for(MasAccountBO accountBo:listAccountBo)
	{
	  MasAccountBean accountBean = new MasAccountBean();
	  accountBean.setPkAccountId(accountBo.getPkAccountId());
	  accountBean.setAccountName(accountBo.getAccountName());
	  listAccountBean.add(accountBean);	
	}
 }
	}catch(Exception e){
		throw new DataAccessException(e.getMessage());
	}
	return listAccountBean;
	
}

		//Added by Rinta Mariam Jose

	public List<ProjectCoordinatorBean> getAllProjectCoordinator() throws CommonCustomException, DataAccessException
	{
		List<DatEmpPersonalDetailBO> datEmpDetailBOList =  new ArrayList<DatEmpPersonalDetailBO>();
		List<ProjectCoordinatorBean> projectCoordinatorBeanList = new ArrayList<ProjectCoordinatorBean>();
		List<String> projectCordinatorlist = Stream.of(projectCordinatorRoleKeyNames.split(","))
		           .collect(Collectors.toList());
		try {
			datEmpDetailBOList = empPersonalRepository.getAllProjectCordinator(projectCordinatorlist);
		}catch (Exception e) {
			throw new DataAccessException("Failed to fetch details from DB",e);
		}
		if(datEmpDetailBOList.isEmpty()){
			throw new CommonCustomException("No Project Coordinators are Found.");
		}
		else{
			for(DatEmpPersonalDetailBO datEmpPersonalDetailBO : datEmpDetailBOList)
			{
				System.out.println(datEmpPersonalDetailBO.toString());
				ProjectCoordinatorBean projectCoordinatorBean = new ProjectCoordinatorBean();
				projectCoordinatorBean.setProjectCoordinatorId(datEmpPersonalDetailBO.getFkEmpDetailId());
				projectCoordinatorBean.setProjectCoordinatorName(datEmpPersonalDetailBO.getEmpFirstName()+" "+datEmpPersonalDetailBO.getEmpLastName());
				projectCoordinatorBean.setProjectCoordinator(projectCoordinatorBean.getProjectCoordinatorName()+"-"+projectCoordinatorBean.getProjectCoordinatorId());
				projectCoordinatorBeanList.add(projectCoordinatorBean);
			}
		
			Collections.sort(projectCoordinatorBeanList);

		}
		return projectCoordinatorBeanList;
		
	}
	
	public List<MasRegionBean> getAllRegions() throws CommonCustomException, DataAccessException
	{
		List<MasRegionBO> masRegionBOList =  null;
		List<MasRegionBean> masRegionBeanList = new ArrayList<MasRegionBean>();
		
		try{
			masRegionBOList =  (List<MasRegionBO>) masRegionRepository.findAll();
		}	catch (Exception e) 
		{
			e.printStackTrace();
			throw new DataAccessException("Failed to fetch the details: "+e.getMessage());
		}
			
		if(!masRegionBOList.isEmpty()){
			for(MasRegionBO masRegionBO : masRegionBOList)
			{
				MasRegionBean masRegionBean = new MasRegionBean();
				masRegionBean.setRegionId(masRegionBO.getPkRegionId());
				masRegionBean.setRegionName(masRegionBO.getRegionName());
				masRegionBeanList.add(masRegionBean);
			}
		}
		else {
			throw new CommonCustomException("No Regions are Found.");
		}
		
		return masRegionBeanList;
	}
	
		//EOA by Rinta Mariam Jose
	
	public List<DomainOutputBean> getAllDomainDetailsByBuId(Short buId) throws DataAccessException {

		List<DomainOutputBean> outputBeanList = new ArrayList<DomainOutputBean>();
		List<ProjDomainMappingWithBuBO> domainMapList;
		try
		{
			domainMapList = projDomainMappRepo.getAllDomainDetailByBuId(buId);
		}catch(Exception e)
		{
			throw new DataAccessException("Failed to retrieve list of Domain Details :  ", e);
		}
		if(!domainMapList.isEmpty())
		{
			for(ProjDomainMappingWithBuBO mappingBO : domainMapList)
			{
				DomainOutputBean bean = new DomainOutputBean();
				bean.setDomainId(mappingBO.getFkDomainId());
				bean.setDomainName(mappingBO.getDomainName());
				outputBeanList.add(bean);
			}
		}
		return outputBeanList;
	}

	public List<PaymentInstructionOutputBean> getAllPaymentInstructionDetails(byte entityId) throws DataAccessException {

		List<PaymentInstructionOutputBean> beanList = new ArrayList<PaymentInstructionOutputBean>();
		List<MasBankPaymentDetailBO> paymentBOList;
		try{
			paymentBOList = (List<MasBankPaymentDetailBO>) bankPaymentInstructionRepository.findByFkEntityId(entityId);
		}
		catch(Exception e)
		{
			throw new DataAccessException("Failed to retrieve Payment Instruction Details :  ", e);
		}
		if(!paymentBOList.isEmpty())
		{
			for(MasBankPaymentDetailBO paymentBO : paymentBOList)
			{
				PaymentInstructionOutputBean bean = new PaymentInstructionOutputBean();
				bean.setBankPaymentId(paymentBO.getPkBankPaymentId());
				bean.setPaymentBankName(paymentBO.getPaymentBankName());
				bean.setCurrencyId(paymentBO.getFkEntityId());
				bean.setPaymentInstructionDetails(paymentBO.getPaymentInstructionsDetail());
				beanList.add(bean);
			}
		}
		return beanList;
	}
			//Added by Rinta Mariam Jose

		public List<MasEmpDesignationBean> getAllEmpDesignation() throws CommonCustomException, DataAccessException
		{
			List<MasEmpDesignationBean> masEmpDesignationBeanList = new ArrayList<MasEmpDesignationBean>();
			List<MasEmpDesignationBO> masEmpDesignationBOList = null;
			
			try {
				masEmpDesignationBOList = (List<MasEmpDesignationBO>) designationRepository.findAll();
			} catch (Exception e) {
				e.printStackTrace();
				throw new DataAccessException("Failed to fetch the details: "+e.getMessage());				
			}
			
			if(!masEmpDesignationBOList.isEmpty())
			{
				for(MasEmpDesignationBO masEmpDesignationBO : masEmpDesignationBOList)
				{
					MasEmpDesignationBean masEmpDesignationBean = new MasEmpDesignationBean();
					masEmpDesignationBean.setDesignationId(masEmpDesignationBO.getPkEmpDesignationId());
					masEmpDesignationBean.setDesignationName(masEmpDesignationBO.getEmpDesignationName());
					masEmpDesignationBean.setStandartRate(null);
					masEmpDesignationBeanList.add(masEmpDesignationBean);
				}
			}
			else 
			{
				throw new CommonCustomException("Employee Designations aren't found.");
			}
			
			return masEmpDesignationBeanList;
		}
		
			//EOA by Rinta Mariam Jose
		
		public List<MasRateTypeBO> getAllRateType() throws CommonCustomException, DataAccessException
		{
			List<MasRateTypeBO> masRateTypeBOList = null;			
			try {
				masRateTypeBOList = (List<MasRateTypeBO>) masRateTypeRepository.findAll();
			} catch (Exception e) {
				throw new DataAccessException("Failed to fetch the details: ",e);				
			}
			return masRateTypeBOList;
		}
		
		public List<MasInvoiceRateTypeBO> getInvoiceRateType() throws CommonCustomException, DataAccessException
		{
			List<MasInvoiceRateTypeBO> masInvoiceRateTypeBOList = null;
			
			try {
				masInvoiceRateTypeBOList = (List<MasInvoiceRateTypeBO>) masInvoiceRateTypeRepository.findAll();
			} catch (Exception e) {
				throw new DataAccessException("Failed to fetch the details: ",e);				
			}
			return masInvoiceRateTypeBOList;
		}
}