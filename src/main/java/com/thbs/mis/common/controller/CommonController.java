/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  CommonController.java                             */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*   Date         Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 23-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.common.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.common.bean.AcountsManagerOutputBean;
import com.thbs.mis.common.bean.BootcampBean;
import com.thbs.mis.common.bean.BusinessUnitBean;
import com.thbs.mis.common.bean.CountryBean;
import com.thbs.mis.common.bean.DmDetailsOutputBean;
import com.thbs.mis.common.bean.DomainOutputBean;
import com.thbs.mis.common.bean.EmpDesignationBean;
import com.thbs.mis.common.bean.EmployeeBean;
import com.thbs.mis.common.bean.EmployeeLevelBean;
import com.thbs.mis.common.bean.EmployeeNameWithRMIdBean;
import com.thbs.mis.common.bean.EmployeesBean;
import com.thbs.mis.common.bean.FCMTokenBean;
import com.thbs.mis.common.bean.GetAllCurrencyTypeOutputBean;
import com.thbs.mis.common.bean.LocationByCountyBean;
import com.thbs.mis.common.bean.MailGeneralBean;
import com.thbs.mis.common.bean.ManagerOutputBean;
import com.thbs.mis.common.bean.MasAccountBean;
import com.thbs.mis.common.bean.MasBuUnitBean;
import com.thbs.mis.common.bean.MasBusinessEntityBean;
import com.thbs.mis.common.bean.MasClientOutputBean;
import com.thbs.mis.common.bean.MasEmpDesignationBean;
import com.thbs.mis.common.bean.MasRegionBean;
import com.thbs.mis.common.bean.MasVerticalBean;
import com.thbs.mis.common.bean.MisMenuBean;
import com.thbs.mis.common.bean.ModuleMappingOutputBean;
import com.thbs.mis.common.bean.OrganizationBean;
import com.thbs.mis.common.bean.ParentLocationBean;
import com.thbs.mis.common.bean.PaymentInstructionOutputBean;
import com.thbs.mis.common.bean.PhysicalLocationBean;
import com.thbs.mis.common.bean.ProjectCoordinatorBean;
import com.thbs.mis.common.bean.RoleDetailsOutputBean;
import com.thbs.mis.common.bean.UpdateModuleMappingInputBean;
import com.thbs.mis.common.bo.DatForexConversionsBO;
import com.thbs.mis.common.bo.EmployeeLoginBO;
import com.thbs.mis.common.constants.CommonURIConstants;
import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.example.constants.EmpRestURIConstants;
import com.thbs.mis.framework.Notification.FCMNotificationService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.exception.UserNotFoundException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.project.bo.MasInvoiceRateTypeBO;
import com.thbs.mis.project.bo.MasRateTypeBO;

/**
 * <Description CommonController:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */

@Api(value = "Common Operations", description = "Common Operations", basePath = "http://localhost:8080/", consumes = "Rest Service", position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "Common Services")
@Controller
public class CommonController {

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory.getLog(CommonController.class);

	@Autowired
	private CommonService commonSrvc;

	@Resource(name = "tokenServices")
	ConsumerTokenServices tokenServices;

	@Autowired
	TokenStore tokenStore;

	@Autowired
	FCMNotificationService fcmNotificationService;

	// added by Smrithi

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllReportingManager : Fetch All Active
	 *           Reporting Managers by BuUnit Id>
	 * 
	 * @param Short
	 *            BuUnit Id
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all active reporting managers. This service will be called when user wants to view list of reporting managers based on BuUnit ID in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all active reporting managers based on BuUnit ID."
					+ "<br>The Response will be the list of the reporting managers based on BuUnit Id.",

			nickname = "All Active Reporting Manager Details by BuUnit Id", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeeNameWithRMIdBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeNameWithRMIdBean.class),
			@ApiResponse(code = 201, message = "All active reporting managers are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Active Reporting Managers Details by BuUnit Id", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Active Reporting Managers Details by BuUnit Id", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_RM_NAMES_BY_BUSINESS_UNIT_ID, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAllReportingManagerByBuUnitId(@PathVariable Short buUnitId)
			throws BusinessException, DataAccessException {
		List<EmployeeNameWithRMIdBean> reportingManagersList = null;
		try {
			reportingManagersList = commonSrvc.getAllReportingManagerByBuUnitId(buUnitId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all reporting managers", e);
		}

		if (!reportingManagersList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All reporting managers are retrieved successfully", reportingManagersList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No reporting manager found", reportingManagersList));
		}

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllReportingManager : Fetch All Active
	 *           Reporting Managers by Emp Id>
	 * 
	 * @param Integer
	 *            empId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all active reporting managers. This service will be called when user wants to view list of reporting managers based on emp ID in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all active reporting managers based on emp ID."
					+ "<br>The Response will be the list of the reporting managers based on emp Id.",

			nickname = "All Active Reporting Manager Details by emp Id", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeeNameWithRMIdBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeNameWithRMIdBean.class),
			@ApiResponse(code = 201, message = "All active reporting managers are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Active Reporting Managers Details by role Id", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Active Reporting Managers Details by emp Id", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_RM_NAMES_BY_EMPLOYEE_ID, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAllReportingManagersByEmpId(@PathVariable Integer empId)
			throws BusinessException, DataAccessException {
		List<EmployeeNameWithRMIdBean> reportingManagersList = null;
		try {
			reportingManagersList = commonSrvc.getAllReportingManagersByEmpId(empId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all reporting managers", e);
		}

		if (!reportingManagersList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All reporting managers are retrieved successfully", reportingManagersList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No reporting manager found", reportingManagersList));
		}

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllReportingManager : Fetch All Employees by
	 *           Emp Id>
	 * 
	 * @param Integer
	 *            empId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all Employees based on emp ID. This service will be called when user wants to view list of all Employees based on emp ID in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all Employees based on emp ID."
					+ "<br>The Response will be the list of Employees based on emp Id.",

			nickname = "All Employee Details by emp Id", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeeNameWithRMIdBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeNameWithRMIdBean.class),
			@ApiResponse(code = 201, message = "All active reporting managers are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Active Reporting Managers Details by role Id", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Employee Details by emp Id", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_EMPLOYEE_NAMES_BY_EMPLOYEE_ID, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAllEmployeesByEmpId(@PathVariable Integer empId)
			throws BusinessException, DataAccessException {
		List<EmployeeNameWithRMIdBean> reportingManagersList = null;
		try {
			reportingManagersList = commonSrvc.getAllEmployeesByEmpId(empId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all employee details.", e);
		}

		if (!reportingManagersList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All employee details are retrieved successfully", reportingManagersList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No employee details found", reportingManagersList));
		}

	}

	// EOA by Smrithi

	/**
	 * @category This service is used for UI page to show this list in drop down
	 *           UI option. <Description getAllBootcamps : Fetch All Boot-camp
	 *           Details>
	 * @param
	 * @return List<BootcampBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all bootcamp details. This service will be called from the front-end when user want to show/view list of bootcamps in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all bootcamps."
					+ "<br>The Response will be the detailed of the bootcamp.",

			nickname = "All Bootcamp Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = BootcampBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BootcampBean.class),
			@ApiResponse(code = 201, message = "All bootcamps are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All bootcamp details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All bootcamp details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_BOOTCAMPS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllBootcamps() throws BusinessException, DataAccessException {
		List<BootcampBean> bootcampList = null;
		try {
			bootcampList = commonSrvc.getAllBootcamps();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all bootcamps", e);
		}

		if (!bootcampList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All bootcamps have been retrieved successfully", bootcampList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No bootcamp found", bootcampList));
		}
	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllEmployees : Fetch All Business Units>
	 * @param
	 * @return List<BusinessUnitBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */

	@ApiOperation(value = "Get all active business unit details. This service will be called from the front-end when user want to show/view list of business units in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all active business unit."
					+ "<br>The Response will be the detailed of the business units.",

			nickname = "Business Unit Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = BusinessUnitBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BusinessUnitBean.class),
			@ApiResponse(code = 201, message = "Business Unit details retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Business Unit details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Business Unit details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_ACTIVE_BU, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllBusinessUnits()
			throws BusinessException, DataAccessException {
		List<BusinessUnitBean> businessUnits = null;
		try {
			businessUnits = commonSrvc.getAllActiveBusinessUnits();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrive all active business units", e);
		}

		if (businessUnits.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No active BU found", businessUnits));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All active BUs have been retrieved successfully", businessUnits));
		}
	}

	// Added by Prathibha

	/***
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getBusinessUnitByBuId> : Fetch A Business Unit
	 *           Detail From Business Unit Id.
	 * @return List<BusinessUnitBean>
	 * @input Integer buId (path varaible)
	 * @throws BusinessException
	 * @throws DataAccessException
	 */

	@ApiOperation(value = "Get a business unit detail from a provided business unit id. This service will be called when user want to view a business unit detail from business unit id"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch a business unit detail from a provided business unit id."
					+ "<br>The Response will be the detailed of the business unit.",

			nickname = "All sub-menu based on menu", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MisMenuBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MisMenuBean.class),
			@ApiResponse(code = 201, message = "Business Unit detail retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Business Unit detail", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Business Unit detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_BU_INFO_BY_BUID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getBusinessUnitByBuId(
			@ApiParam(name = "BU Id", example = "1", value = "View Business Unit") @PathVariable Integer buId)
			throws BusinessException, DataAccessException {
		List<BusinessUnitBean> businessUnitList;

		if (buId <= 0) {

			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.SUCCESS, "INVALID BU ID"));
		}

		else {
			try {
				businessUnitList = commonSrvc.getBusinessUnitByBuId(buId);
			} catch (DataAccessException e) {
				throw new BusinessException("Failed to retrieve business unit", e);
			}

			if (!businessUnitList.isEmpty()) {
				return ResponseEntity.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
								"Business unit information has been retrieved successfully", businessUnitList));
			} else {

				return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.NOT_FOUND.value(),
						MISConstants.SUCCESS, "BU ID IS NOT ACTIVE", businessUnitList));
			}
		}

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllCountry : Fetch All Countries Details>
	 * @param
	 * @return List<CountryBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all countries detail. This service will be called when user want to view list of country in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all country."
					+ "<br>The Response will be the detailed of the country.",

			nickname = "All Country Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = CountryBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = CountryBean.class),
			@ApiResponse(code = 201, message = "All countries are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All countries details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All countries details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_COUNTRIES, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllCountry() throws BusinessException, DataAccessException {
		List<CountryBean> countryList = null;
		try {
			countryList = commonSrvc.getAllCountry();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all countries", e);
		}

		if (!countryList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All countries have been retrieved successfully", countryList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No country found", countryList));
		}
	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllDesignations : Fetch All Employee
	 *           Designations>
	 * @param
	 * @return List<EmpDesignationBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all designations detail. This service will be called when user want to view list of designations in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all designations."
					+ "<br>The Response will be the detailed of the designations.",

			nickname = "All Designations Detail", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmpDesignationBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmpDesignationBean.class),
			@ApiResponse(code = 201, message = "All designations are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Designations Detail", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Designations Detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_DESIGNATIONS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllDesignations()
			throws BusinessException, DataAccessException {
		List<EmpDesignationBean> designationsList = null;
		try {
			designationsList = commonSrvc.getAllDesignation();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all employee designations", e);
		}

		if (!designationsList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"All employee designations have been retrieved successfully", designationsList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No employee designation found", designationsList));
		}
	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllDirectAndIndirectReporteeUnderRM> : Fetch
	 *           All Reportees (including direct and indirect) of Reporting
	 *           Manager
	 * @param int
	 *            rmId
	 * @return List<DatEmpDetailBO>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Fetch All Direct And Indirect Reportees Of A Provided Reporting Manager. This service will be called when user wants to view list of direct and indirect reportees (which are under given reporting manager id) in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all direct and indirect reportees under provided reporting manager."
					+ "<br>The Response will be the detailed of the direct and indirect reportees under provided reporting manager.",

			nickname = "All direct and indirect reportees details under Reporting Manager", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeeNameWithRMIdBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeNameWithRMIdBean.class),
			@ApiResponse(code = 201, message = "All direct and indirect reportees are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All direct and indirect reportees", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All direct and indirect reportees", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_DIRECT_AND_INDIRECT_REPORTEES, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllDirectAndIndirectReporteeUnderRM(
			@ApiParam(name = "Reporting Manager ID", example = "1", value = "View Direct And Indirect Reportees of a Reporting Manager") @PathVariable Integer rmId)
			throws BusinessException, DataAccessException {
		List<EmployeeNameWithRMIdBean> allDirectReporteesList = new ArrayList<EmployeeNameWithRMIdBean>();
		try {
			allDirectReporteesList = commonSrvc.getAllDirectAndIndirectReporteesWithRMIdUnderRM(rmId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all direct and indirect reportees", e);
		}

		if (!allDirectReporteesList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"All direct and indirect reportees have been retrieved successfully",
							allDirectReporteesList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No (direct/indirect) reportee found", allDirectReporteesList));
		}

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllDirectReporteeUnderRM> : Fetch All Direct
	 *           Reportees Under Reporting Manager
	 * @param int
	 *            rmId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all direct reportees under Reporting Manager. This service will be called when user wants to view list of direct reportees under a provided reporting manager id"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all direct reportees under Reporting Manager."
					+ "<br>The Response will be the detailed of the direct reportees under Reporting Manager.",

			nickname = "All direct reportees details under Reporting Manager", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeeNameWithRMIdBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeNameWithRMIdBean.class),
			@ApiResponse(code = 201, message = "All direct reportees are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All direct reportees details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All direct reportees details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_DIRECT_REPORTEES, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllDirectReporteeUnderRM(
			@ApiParam(name = "Reporting Manager Employee ID", example = "1", value = "View Direct Reportees of a Reporting Manager") @PathVariable Integer rmId)
			throws BusinessException, DataAccessException {
		List<EmployeeNameWithRMIdBean> directReporteesList = null;
		try {
			directReporteesList = commonSrvc.getAllDirectReporteesWithRMIdForRmId(rmId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all direct reportees", e);
		}

		if (!directReporteesList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"All direct reportees have been retrieved successfully", directReporteesList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No direct reportee found", directReporteesList));
		}
	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllEmployees> : Fetch All Employees.
	 * @param
	 * @return List<EmployeesBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all employees. This service will be called when user wants to view list of all employees in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all employees."
					+ "<br>The Response will be the list of all employees.",

			nickname = "Get All Employees", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeesBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeesBean.class),
			@ApiResponse(code = 201, message = "All employees are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "An inactive employee detail", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "An inactive employee detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_EMPLOYEES, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllEmployees() throws BusinessException, DataAccessException {
		List<EmployeesBean> allEmployeesList = new ArrayList<EmployeesBean>();
		try {
			allEmployeesList = commonSrvc.getAllEmployeesList();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all employees", e);
		}

		if (allEmployeesList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No employee found", allEmployeesList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All employees have been retrieved successfully", allEmployeesList));
		}
	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllActiveEmployees> : Fetch All Active
	 *           Employees.
	 * @param
	 * @return List<EmployeesBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all active employees. This service will be called when the user wants to view list of active employees only in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all active employees."
					+ "<br>The Response will be the list of all active employees.",

			nickname = "Get All Active Employees", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeesBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeesBean.class),
			@ApiResponse(code = 201, message = "All active employees are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All active employee details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All active employee details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_ACTIVE_EMPLOYEES, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllActiveEmployees()
			throws BusinessException, DataAccessException {
		List<EmployeesBean> activeEmpBeanList = new ArrayList<EmployeesBean>();
		try {
			activeEmpBeanList = commonSrvc.getAllActiveEmployeeList();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all active employees", e);
		}

		if (!activeEmpBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All active employees have been retrieved successfully", activeEmpBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No active employee found", activeEmpBeanList));
		}
	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllInactiveEmployees> : Fetch All Inactive
	 *           Employees.
	 * @param
	 * @return List<EmployeesBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all inactive employees. This service will be called when user wants to view list of all inactive employees in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all inactive employees."
					+ "<br>The Response will be the list of all inactive employees.",

			nickname = "Get All Employees", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeesBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeesBean.class),
			@ApiResponse(code = 201, message = "All employees are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All inactive employee details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All inactive employee details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_INACTIVE_EMPLOYEES, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllInactiveEmployees()
			throws BusinessException, DataAccessException {
		List<EmployeesBean> inactiveEmployeeList = new ArrayList<EmployeesBean>();
		try {
			inactiveEmployeeList = commonSrvc.getAllInactiveEmployeesList();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all inactive employees", e);
		}

		if (!inactiveEmployeeList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"All inactive employees have been retrieved successfully", inactiveEmployeeList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No inactive employee found", inactiveEmployeeList));
		}

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllEmpLevels : Fetch All Employee Levels>
	 * @param
	 * @return List<EmployeeLevelBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all employee levels. This service will be called when user wants to view list of all employee levels in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all employee levels."
					+ "<br>The Response will be the detailed of the employee levels.",

			nickname = "All Employee Levels Detail", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeeLevelBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeLevelBean.class),
			@ApiResponse(code = 201, message = "Employee Levels retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Employee Levels Detail", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Employee Levels Detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_EMP_LEVELS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllEmpLevels() throws BusinessException, DataAccessException {
		List<EmployeeLevelBean> levelList = null;
		try {
			levelList = commonSrvc.getAllLevels();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all employee levels", e);
		}

		if (!levelList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All employee levels have been retrieved successfully", levelList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No employee level found", levelList));
		}

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllMenuBasedonRole : Fetch All Menu Details
	 *           based on Role.>
	 * @param Integer
	 * 
	 * @return List<MisMenuBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all menu based on role. This service will be called when user wants to view list of all menu under the mentioned role id"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all menu based on role."
					+ "<br>The Response will be the detailed of the menu according to emplyee's role.",

			nickname = "All menu based on role", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MisMenuBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MisMenuBean.class),
			@ApiResponse(code = 201, message = "All menu based on role are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All menu based on role details ", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All menu based on role details ", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_MENU_BASEDON_ROLE, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllMenuBasedonRole(
			@ApiParam(name = "Employee Role Id", example = "1", value = "View All Menu Which Comes Under Role") @PathVariable Integer roleId)
			throws BusinessException, DataAccessException {
		List<MisMenuBean> menuList = null;
		try {
			menuList = commonSrvc.getAllMenuBasedonRole(roleId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all menus under this role", e);
		}

		if (menuList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.NOT_FOUND.value(),
					MISConstants.SUCCESS, "No menu found for the provided role", menuList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All menus have been retrieved successfully", menuList));
		}

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllOrganization : Fetch All Organizations >
	 * @param
	 * @return List<OrganizationBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all Organization. This service will be called when user wnats to view all organizations in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all Organization."
					+ "<br>The Response will be the detailed of the Organization.",

			nickname = "All Organization Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = OrganizationBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = OrganizationBean.class),
			@ApiResponse(code = 201, message = "All organization are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All organization details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All organization details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_ORGANIZATION, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllOrganization()
			throws BusinessException, DataAccessException {
		List<OrganizationBean> organizationList = null;
		try {
			organizationList = commonSrvc.getAllOrganization();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all organizations", e);
		}

		if (!organizationList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All organizations have been retrieved successfully", organizationList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No organization found", organizationList));
		}

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllParentLocations : Fetch All Parent
	 *           Locations>
	 * 
	 * @param
	 * @return List<ParentLocationBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all parent locations. This service will be called when user wants to view list of all parent locations in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all parent locations."
					+ "<br>The Response will be the detailed of the parent locations.",

			nickname = "All Parent Locations Detail", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ParentLocationBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = ParentLocationBean.class),
			@ApiResponse(code = 201, message = "All parent location are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All parent locations detail", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All parent locations detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_PARENT_LOCATIONS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllParentLocation()
			throws BusinessException, DataAccessException {
		List<ParentLocationBean> parentLocationList = null;
		try {
			parentLocationList = commonSrvc.getAllParentLocation();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all parent location", e);
		}

		if (!parentLocationList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All parent locations have been retrieved successfully", parentLocationList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No parent location found", parentLocationList));
		}

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllPhysicalLocations : Fetch All Physical
	 *           Locations>
	 * 
	 * @param
	 * @return List<PhysicalLocationBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all physical locations. This service will be called when user wants to view list of physical locations in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all physical locations."
					+ "<br>The Response will be the detailed of the physical locations.",

			nickname = "All Physical Locations Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PhysicalLocationBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PhysicalLocationBean.class),
			@ApiResponse(code = 201, message = "All physical location are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Physical Locations Detail", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Physical Locations Detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_PHYSICAL_LOCATIONS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllPhysicalLocation()
			throws BusinessException, DataAccessException {
		List<PhysicalLocationBean> physicalLocList = null;
		try {
			physicalLocList = commonSrvc.getAllPhysicalLocation();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all physical location", e);
		}

		if (!physicalLocList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All physical locations have been retrieved successfully", physicalLocList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No physical location found", physicalLocList));
		}

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllReportingManager : Fetch All Active
	 *           Reporting Managers>
	 * 
	 * @param
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all active reporting managers. This service will be called when user wants to view list of reporting managers in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all active reporting managers."
					+ "<br>The Response will be the list of the reporting managers.",

			nickname = "All Active Reporting Manager Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeeNameWithRMIdBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeNameWithRMIdBean.class),
			@ApiResponse(code = 201, message = "All active reporting managers are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Active Reporting Managers Detail", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Active Reporting Managers Detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_REPORTING_MANAGERS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllReportingManager()
			throws BusinessException, DataAccessException {
		List<EmployeeNameWithRMIdBean> reportingManagersList = null;
		try {
			reportingManagersList = commonSrvc.getAllActiveReportingManagers();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all reporting managers", e);
		}

		if (!reportingManagersList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All reporting managers are retrieved successfully", reportingManagersList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No reporting manager found", reportingManagersList));
		}

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllSubMenuBasedOnParentMenuId> : Fetch All
	 *           Sub-Menu Based on It's Parent Menu Id.
	 * @param Integer
	 *            menuId
	 * @return List<MisMenuBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all sub-menu based on parent menu id. This service will be called when user wants to show list of sub-menu on the basis of their parent menu, in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all sub-menu based on parent menu id."
					+ "<br>The Response will be the detailed of the sub-menu based on parent menu id.",

			nickname = "All sub-menu based on menu", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MisMenuBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MisMenuBean.class),
			@ApiResponse(code = 201, message = "All sub-menu based on menu are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All sub-menu based on menu", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All sub-menu based on menu", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_SUB_MENU_BASED_ON_MENU, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllSubMenuBasedOnParentMenuId(
			@ApiParam(name = "Menu Id", example = "1", value = "View All Sub-menu Based On Parent Menu Id") @PathVariable Integer menuId)
			throws BusinessException, DataAccessException {
		List<MisMenuBean> submenuList = null;
		try {
			submenuList = commonSrvc.getAllSubmenuBasedOnMenuId(menuId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all sub-menus under this parent menu", e);
		}

		if (submenuList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.NOT_FOUND.value(),
					MISConstants.SUCCESS, "No sub-menu found", submenuList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All sub-menus have been retrieved successfully", submenuList));
		}

	}

	/**
	 * 
	 * @param mailList
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 */

	@ApiOperation(value = "General Send Mail."
			+ "This service will be called when mail has to be sent.", httpMethod = "POST", notes = "This Service has been implemented to send mail."
					+ "<br>The mail wll be sent." + "<br>The Response will be a boolean value (true or false).",

			nickname = "Mail Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = Boolean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = Boolean.class),
			@ApiResponse(code = 201, message = "Mail Sent Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The sent mail resource", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The sent mail resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = EmpRestURIConstants.MAIL_SEND_FOR_GENERAL, method = RequestMethod.POST)
	public boolean mailSendMethodForGeneralPurpose(
			@ApiParam(name = "Mail Details", example = "1", value = "All Mail details.") @RequestBody MailGeneralBean mailList)
			throws BusinessException, DataAccessException {
		LOG.startUsecase("Create Training Program");
		boolean result;
		try {
			result = commonSrvc.sendMailForGeneral(mailList);
			LOG.info("Training Addition Success :" + result);
		} catch (DataAccessException e) {
			e.printStackTrace();
			throw new DataAccessException("Exception occured", e);
		}
		LOG.endUsecase("Create Training Program");
		return result;

	}

	@RequestMapping(value = EmpRestURIConstants.EMP_LOGIN, method = RequestMethod.POST)
	public @ResponseBody EmployeeBean empLogin(@RequestBody EmployeeLoginBO emp)
			throws BusinessException, UserNotFoundException {
		LOG.startUsecase("Entering empLogin");
		LOG.info("Do empLogin");

		EmployeeBean employee = null;
		;
		boolean validateUserCredentialsFromLDAP = true;
		/*
		 * boolean validateUserCredentialsFromLDAP = authenticationFromLDAP(
		 * emp.getPk_EmpLogin_EmpID(), emp.getEmpLogin_EmpPassword());
		 * LOG.info("testing validateUserCredentialsFromLDAP " +
		 * validateUserCredentialsFromLDAP);
		 */

		if (validateUserCredentialsFromLDAP) {
			try {
				employee = commonSrvc.authenticateUser(emp.getPk_EmpLogin_EmpID());

				if (employee == null) {
					LOG.debug("Inside if in empLogin");
					throw new UserNotFoundException("Invalid User Name / Password");
				}
			} catch (DataAccessException e) {
				throw new BusinessException("Exception occured", e);
			}

		}
		LOG.endUsecase("Exiting empLogin");
		return employee;
	}

	/*
	 * ADDED By Pradeep For LIVE Login to encrypt EmpId
	 */
	/*
	 * @RequestMapping(value = EmpRestURIConstants.EMP_LOGIN, method =
	 * RequestMethod.POST) public @ResponseBody EmployeeBean
	 * empLogin(@RequestBody EmployeeBean emp) throws BusinessException,
	 * UserNotFoundException { LOG.startUsecase("Entering empLogin");
	 * LOG.info("Do empLogin");
	 * 
	 * EmployeeBean employee = null; ; boolean validateUserCredentialsFromLDAP
	 * =true; boolean validateUserCredentialsFromLDAP = authenticationFromLDAP(
	 * emp.getPk_EmpLogin_EmpID(), emp.getEmpLogin_EmpPassword());
	 * LOG.info("testing validateUserCredentialsFromLDAP " +
	 * validateUserCredentialsFromLDAP);
	 * 
	 * if (validateUserCredentialsFromLDAP) { try { employee =
	 * commonSrvc.authenticateUser(emp .getPk_EmpLogin_EmpID());
	 * 
	 * if (employee == null) { LOG.debug("Inside if in empLogin"); throw new
	 * UserNotFoundException( "Invalid User Name / Password"); } } catch
	 * (DataAccessException e) { throw new
	 * BusinessException("Exception occured", e); }
	 * 
	 * } LOG.endUsecase("Exiting empLogin"); return employee; }
	 */
	/*
	 * public boolean authenticationFromLDAP(int empID, String password) {
	 * LOG.startUsecase("Entering authenticationFromLDAP"); boolean result =
	 * false;
	 * 
	 * try { AndFilter filter = new AndFilter(); filter.and(new
	 * EqualsFilter("uid", empID)); result =
	 * ldapTemplate.authenticate("ou=people", filter.encode(), password);
	 * LOG.info("Inside authenticationFromLDAP method " + result); } catch
	 * (Exception e) {
	 * 
	 * e.printStackTrace(); } LOG.endUsecase("Exiting authenticationFromLDAP");
	 * return result;
	 * 
	 * }
	 */

	@RequestMapping(method = RequestMethod.POST, value = EmpRestURIConstants.TOKEN_REVOKE)
	public ResponseEntity<MISResponse> revokeToken(@PathVariable String tokenId) {

		boolean revokeToken = false;

		/*
		 * OAuth2RefreshToken refreshToken =
		 * tokenStore.readRefreshToken(tokenId);
		 * tokenStore.removeRefreshToken(refreshToken);
		 */

		revokeToken = tokenServices.revokeToken(tokenId);
		if (revokeToken == true) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Refresh Token Revoked."));
		} else {

			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Some Error Occured while revoking the refresh token"));
		}
		/*
		 * boolean result= this.revokerefreshToken(tokenId);
		 */

	}

	/*
	 * @RequestMapping(value = CommonURIConstants.GET_ALL_MENU_FOR_EMPLOYEE,
	 * method = RequestMethod.GET) public @ResponseBody
	 * ResponseEntity<MISResponse> getAllMenusForEmployee(
	 * 
	 * @ApiParam(name = "Employee Id", example = "1", value =
	 * "View All Menus for an employee")
	 * 
	 * @PathVariable Integer empId) throws BusinessException,
	 * DataAccessException { List<MisMenuBean> menuList = null; try { menuList =
	 * commonSrvc.getAllMenusForEmployee(empId); } catch (DataAccessException e)
	 * { throw new
	 * BusinessException("Failed to retrieve all menus for the employee", e); }
	 * 
	 * if(menuList.isEmpty()) { return
	 * ResponseEntity.status(HttpStatus.OK.value()).body( new
	 * MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.SUCCESS,
	 * "No menu found for the employee", menuList )) ; }else{ return
	 * ResponseEntity.status(HttpStatus.OK.value()).body( new
	 * MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
	 * "All menus have been retrieved successfully", menuList )) ; }
	 * 
	 * }
	 */

	/**
	 * 
	 * <Description getAllSubMenuListForEmployeeForParentMenuId:> Retrieves all
	 * sub menus for the employee based on parent menu Id
	 * 
	 * @param empId
	 *            Employee Id
	 * @param parentMenuId
	 *            Parent menu module id
	 * @return {@link ResponseBody} containing menu information
	 * 
	 * @throws BusinessException
	 *             In case of any exceptions
	 */

	@ApiOperation(value = "Get all sub-menu for an employee based on parent menu id." + " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all sub-menu based on parent menu id and employee id."
					+ "<br>The Response will be the detailes of the sub-menu based on parent menu id and employee id.",

			nickname = "All sub-menu based on menu", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MisMenuBean.class, tags = "Common")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MisMenuBean.class),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All sub-menu based on menu", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource Not Found", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_SUB_MENU_FOR_EMPLOYEE_ON_PARENT_MENU, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllSubMenuListForEmployeeForParentMenuId(
			@ApiParam(name = "Employee Id", example = "1", value = "View All Menus for an employee") @PathVariable Integer empId,
			@PathVariable Integer parentMenuId) throws BusinessException {
		List<MisMenuBean> menuList = null;
		try {
			menuList = commonSrvc.getAllSubMenuListForEmployeeForParentMenuId(empId, parentMenuId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all sub menus for the employee", e);
		}

		if (menuList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.NOT_FOUND.value(),
					MISConstants.SUCCESS, "No menu found for the employee", menuList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All menus have been retrieved successfully", menuList));
		}

	}

	/**
	 * 
	 * <Description getAllSubMenuListForEmployeeForParentMenuId:> Retrieves all
	 * main menus for the employee
	 * 
	 * @param empId
	 *            Employee Id
	 * 
	 * @return {@link ResponseBody} containing menu information
	 * 
	 * @throws BusinessException
	 *             In case of any exceptions
	 */
	@ApiOperation(value = "Get all main-menu for an employee" + " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all main-menu."
					+ "<br>The Response will be the details of the main-menu based on employee id.",

			nickname = "All sub-menu based on menu", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MisMenuBean.class, tags = "Common")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MisMenuBean.class),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All sub-menu based on menu", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource Not Found", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_TOPLEVEL_FOR_EMPLOYEE, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllTopLevelMenusForEmployee(
			@ApiParam(name = "Employee Id", example = "1", value = "View All Menus for an employee") @PathVariable Integer empId)
			throws BusinessException, DataAccessException {
		List<MisMenuBean> menuList = null;
		try {
			menuList = commonSrvc.getAllTopLevelMenusForEmployee(empId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all top menus for the employee", e);
		}

		if (menuList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.NOT_FOUND.value(),
					MISConstants.SUCCESS, "No menu found for the employee", menuList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All menus have been retrieved successfully", menuList));
		}

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllPermanentAndGraduatedEmployees> : Fetch All
	 *           Permanent and Graduated Employees.
	 * @param
	 * @return List<EmployeesBean>
	 * @throws BusinessException
	 *             In case of any exceptions
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get All Permanent and Graduated Employees. This service will be called when the user wants to view list of All Permanent and Graduated Employees only in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch All Permanent and Graduated Employees."
					+ "<br>The Response will be the list of All Permanent and Graduated Employees.",

			nickname = "Get All Permanent and Graduated Employees", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeesBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeesBean.class),
			@ApiResponse(code = 201, message = "All permanent and graduated employees are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Permanent and Graduated Employees details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Permanent and Graduated Employees details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_PERMANENT_AND_GRADUATED_EMPLOYEES, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllPermanentAndGraduatedEmployees()
			throws BusinessException, DataAccessException {
		List<EmployeesBean> permanentAndGraduatedBeanList = new ArrayList<EmployeesBean>();
		try {
			permanentAndGraduatedBeanList = commonSrvc.getAllPermanentAndGraduatedEmployees();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all permanent and graduated employees", e);
		}

		if (!permanentAndGraduatedBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"All permanent and graduated employees have been retrieved successfully",
							permanentAndGraduatedBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No permanent and graduated employee found", permanentAndGraduatedBeanList));
		}
	}

	// added by pratibha

	/**
	 * 
	 * @param empId
	 * @return List<EmployeesBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 *             Description:This service is implemented to get the details of
	 *             reportees.
	 */

	@ApiOperation(value = "Get all my reportees. This service will be called when user wants to view all the reportees based on emp ID in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all reportees based on emp ID."
					+ "<br>The Response will be the list of the reportees based on emp Id.",

			nickname = "All my reportees by emp Id", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeesBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeesBean.class),
			@ApiResponse(code = 201, message = "All my reportees retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All my reportees detail by Emp Id", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All my reportees detail by Emp Id", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_MY_REPORTEES, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getMyReporteesByEmpId(@PathVariable Integer empId)
			throws BusinessException, DataAccessException {
		List<EmployeesBean> reporteesList = new ArrayList<EmployeesBean>();
		try {
			reporteesList = commonSrvc.getMyReporteesByEmpId(empId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all reportees", e);
		}

		if (!reporteesList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All reportees are retrieved successfully", reporteesList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No reportee is found", reporteesList));
		}

	}

	/**
	 * 
	 * @param empId
	 * @return List<MasBuUnitBean>
	 * @throws CommonCustomException
	 *             Description:This Service has been implemented to fetch BU
	 *             details based on employee Id.
	 */
	@ApiOperation(value = "Get BU details. This service will be called when user wants to view the BU details of employee by employee Id."
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch BU details based on employee Id.",

			nickname = "Get BU details by emp Id", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MasBuUnitBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MasBuUnitBean.class),
			@ApiResponse(code = 201, message = "All my reportees retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "BU details by Emp Id", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "BU details by Emp Id", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_BU_DETAILS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getBuUnitName(@PathVariable int empId)
			throws CommonCustomException {
		List<MasBuUnitBean> buUnitBeanList = new ArrayList<MasBuUnitBean>();
		buUnitBeanList = commonSrvc.getBuUnitName(empId);

		if (buUnitBeanList == null) {

			throw new CommonCustomException("Invalid employee Id");
		}

		if (!buUnitBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Successfully retrieved the BuUnit details.", buUnitBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.FAILURE, "No data found."));

		}
	}

	/**
	 * 
	 * @return List<GetAllCurrencyTypeOutputBean>
	 * @throws CommonCustomException
	 *             Description:This Service has been implemented to fetch all
	 *             currency type details.
	 */
	@ApiOperation(value = "Get all currency types. This service will be called when user wants to view types of currency"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all the currency types."
					+ "<br>The Response will be the list of currency types.",

			nickname = "All my reportees by emp Id", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GetAllCurrencyTypeOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GetAllCurrencyTypeOutputBean.class),
			@ApiResponse(code = 201, message = "All my reportees retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Currency type Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Currency type Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_CORRENCY_TYPE, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllCorrencyTypes() throws CommonCustomException {
		List<GetAllCurrencyTypeOutputBean> GetAllCurrencyTypeOutputBeanList = new ArrayList<GetAllCurrencyTypeOutputBean>();
		GetAllCurrencyTypeOutputBeanList = commonSrvc.getAllCorrencyTypes();

		if (!GetAllCurrencyTypeOutputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"Successfully retrieved the currency type details.", GetAllCurrencyTypeOutputBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.FAILURE, "No data found."));

		}
	}

	@RequestMapping(value = CommonURIConstants.UPDATE_MODULE_MAPPING, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> updateMappingDetails(
			@RequestBody UpdateModuleMappingInputBean updateModuleMappingInputBean) throws CommonCustomException {
		ModuleMappingOutputBean moduleMappingOutputBean = new ModuleMappingOutputBean();
		List<ModuleMappingOutputBean> moduleMappingOutputBeanList = new ArrayList<ModuleMappingOutputBean>();
		moduleMappingOutputBean = commonSrvc.updateMappingDetails(updateModuleMappingInputBean);
		moduleMappingOutputBeanList.add(moduleMappingOutputBean);
		if (!moduleMappingOutputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"Successfully retrieved the module mapping details for the employee.",
							moduleMappingOutputBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.FAILURE, "No data found."));

		}
	}
	// ended by pratibha

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllDirectAndIndirectReporteeUnderRM> : Fetch
	 *           All Reportees (including direct and indirect) of Reporting
	 *           Manager
	 * @param int
	 *            rmId
	 * @return List<DatEmpDetailBO>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Fetch All Direct And Indirect Reportees Of A Provided Reporting Manager. This service will be called when user wants to view list of direct and indirect reportees (which are under given reporting manager id) in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all direct and indirect reportees under provided reporting manager."
					+ "<br>The Response will be the detailed of the direct and indirect reportees under provided reporting manager.",

			nickname = "GET_INDIRECT_EMP_BASED_ON_DM_ID_AND_GRADUATED_EMP", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeeNameWithRMIdBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeNameWithRMIdBean.class),
			@ApiResponse(code = 201, message = "All direct and indirect reportees are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All indirect reportees", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All indirect reportees", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_INDIRECT_EMP_BASED_ON_DM_ID_AND_GRADUATED_EMP, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllIndirectReporteeUnderDM(
			@ApiParam(name = "DM ID", example = "1", value = "View Indirect Reportees of a DM") @PathVariable Integer rmId)
			throws BusinessException, DataAccessException {
		LOG.startUsecase("GET Indirect Emp Based on DM ID");
		List<EmployeeNameWithRMIdBean> allDirectReporteesList = new ArrayList<EmployeeNameWithRMIdBean>();
		try {
			allDirectReporteesList = commonSrvc.getAllIndirectReporteeUnderDM(rmId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all direct and indirect reportees", e);
		}
		LOG.endUsecase("GET Indirect Emp Based on DM ID");
		if (!allDirectReporteesList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"All Indirect reportees have been retrieved successfully", allDirectReporteesList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No Indirect reportee found", allDirectReporteesList));
		}

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllDirectReporteeUnderRM> : Fetch All Direct
	 *           Reportees Under Reporting Manager
	 * @param int
	 *            rmId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all direct reportees under Reporting Manager. This service will be called when user wants to view list of direct reportees under a provided reporting manager id"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all direct reportees under Reporting Manager."
					+ "<br>The Response will be the detailed of the direct reportees under Reporting Manager.",

			nickname = "All direct reportees details under Reporting Manager", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeeNameWithRMIdBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeNameWithRMIdBean.class),
			@ApiResponse(code = 201, message = "All direct reportees are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All direct reportees details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All direct reportees details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_DIRECT_EMP_BASED_ON_RM_ID_AND_GRADUATED_EMP, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllDirectGraduatedReporteeUnderRM(
			@ApiParam(name = "Reporting Manager Employee ID", example = "1", value = "View Direct Reportees of a Reporting Manager") @PathVariable Integer rmId)
			throws BusinessException, DataAccessException {
		List<EmployeeNameWithRMIdBean> directReporteesList = null;
		try {
			directReporteesList = commonSrvc.getDirectReporteesWithDMIdForRmId(rmId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all direct reportees", e);
		}

		if (!directReporteesList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"All direct reportees have been retrieved successfully", directReporteesList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No direct reportee found", directReporteesList));
		}
	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllPermanentAndGraduatedEmployees> : Fetch All
	 *           Permanent and Graduated Employees.
	 * @param
	 * @return List<EmployeesBean>
	 * @throws BusinessException
	 *             In case of any exceptions
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get All Permanent and Graduated Employees. This service will be called when the user wants to view list of All Permanent and Graduated Employees only in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch All Permanent and Graduated Employees."
					+ "<br>The Response will be the list of All Permanent and Graduated Employees.",

			nickname = "Get All Permanent and Graduated Employees", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeesBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeesBean.class),
			@ApiResponse(code = 201, message = "All permanent and graduated employees are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Permanent and Graduated Employees details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Permanent and Graduated Employees details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_PERMANENT_AND_GRADUATED_ACTIVE_EMPLOYEES, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllPermanentAndGraduatedActiveEmployees()
			throws BusinessException, DataAccessException {
		List<EmployeesBean> permanentAndGraduatedBeanList = new ArrayList<EmployeesBean>();
		try {
			permanentAndGraduatedBeanList = commonSrvc.getAllPermanentAndGraduatedActiveEmployees();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all permanent and graduated employees", e);
		}

		if (!permanentAndGraduatedBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"All permanent and graduated Active employees have been retrieved successfully",
							permanentAndGraduatedBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"No permanent and graduated Active employee found", permanentAndGraduatedBeanList));
		}
	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllReportingManager : Fetch All Active
	 *           Reporting Managers by Emp Id>
	 * 
	 * @param Integer
	 *            empId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all active reporting managers. This service will be called when user wants to view list of reporting managers based on emp ID in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all active reporting managers based on emp ID."
					+ "<br>The Response will be the list of the reporting managers based on emp Id.",

			nickname = "All Active Reporting Manager Details by emp Id", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeeNameWithRMIdBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeNameWithRMIdBean.class),
			@ApiResponse(code = 201, message = "All active reporting managers are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Active Reporting Managers Details by role Id", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Active Reporting Managers Details by emp Id", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_EMP_INFO, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getEmployeeDetailsByEmpId(@PathVariable Integer empId)
			throws BusinessException, DataAccessException {
		List<EmployeeNameWithRMIdBean> reportingManagersList = null;
		try {
			reportingManagersList = commonSrvc.getEmployeeDetailsByEmpId(empId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all reporting managers", e);
		}

		if (!reportingManagersList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All reporting managers are retrieved successfully", reportingManagersList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No reporting manager found", reportingManagersList));
		}

	}

	@RequestMapping(value = CommonURIConstants.FCM_NOTIFICATIONS, method = RequestMethod.POST)
	public @ResponseBody String fcmNotification(@RequestBody FCMTokenBean userDeviceIdBean)
			throws BusinessException, DataAccessException, Exception {
		/*
		 * final String AUTH_KEY_FCM =
		 * "AAAAlDMVgbk:APA91bG6L9sIlcHu5VvfzYikXcFUqespNe-3OIqa3gDtBhpCd1rKyae2RLq1SbgciIe76tyqsBrYIyQ0QdAeJ4r1wwnXpBWuATPxa9FwnF52PC-yH3Zeedg3EnCqp_VAYquG482HxPdX";
		 * final String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";
		 * 
		 * String authKey = AUTH_KEY_FCM; // You FCM AUTH key String FMCurl =
		 * API_URL_FCM;
		 * 
		 * URL url = new URL(FMCurl); HttpURLConnection conn =
		 * (HttpURLConnection) url.openConnection();
		 * 
		 * conn.setUseCaches(false); conn.setDoInput(true);
		 * conn.setDoOutput(true);
		 * 
		 * conn.setRequestMethod("POST");
		 * conn.setRequestProperty("Authorization","key="+authKey);
		 * conn.setRequestProperty("Content-Type","application/json");
		 * 
		 * JSONObject json = new JSONObject();
		 * json.put("to",userDeviceIdBean.getUserDeviceIdKey().trim());
		 * JSONObject info = new JSONObject(); info.put("title",
		 * "Notificatoin Title"); // Notification title info.put("body",
		 * "Hello Test notification"); // Notification body info.put("icon",
		 * "https://media.glassdoor.com/sql/272465/torry-harris-business-solutions-squarelogo-1393001047610.png"
		 * ); json.put("notification", info);
		 * 
		 * OutputStreamWriter wr = new
		 * OutputStreamWriter(conn.getOutputStream());
		 * wr.write(json.toString()); wr.flush(); conn.getInputStream();
		 * System.out.println("Push Notification Sent Successfully"); return
		 * "Sent Successully";
		 */

		String s = fcmNotificationService.CommonNotification(userDeviceIdBean);

		return "Sent Successully";

	}

	// Added by Kamal Anand
	/**
	 * @category This service is used to get All Reporting Manager and Skiplevel
	 *           Manager in a BU to which Employee belong
	 * 
	 * @param Integer
	 *            empId
	 * @return List<EmployeeNameWithRMIdBean>
	 * @throws BusinessException
	 */
	@ApiOperation(value = "This service will be called when user wants to view all Reporting Managers and Skip Level Managers in the particular BU of an Employee"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all Reporting Managers and Skip Level Managers."
					+ "<br>The Response will be the list of the reporting managers based on emp Id.",

			nickname = "All Active Reporting Manager and Skip Level Manager Details by emp Id", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeNameWithRMIdBean.class),
			@ApiResponse(code = 201, message = "All active reporting managers are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Active Reporting Managers Details by Employee Id", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Active Reporting Managers Details by emp Id", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_RM_AND_SKIPLEVELRM_IN_BU_BY_EMPID, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getRmAndSkipLevelRmInBU(@PathVariable Integer empId) throws BusinessException {
		LOG.startUsecase("Entering getRmAndSkipLevelRmInBU controller");
		List<EmployeeNameWithRMIdBean> directReporteesList = null;
		try {
			directReporteesList = commonSrvc.getRmAndSkipLevelRmInBU(empId);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		if (directReporteesList != null) {
			LOG.endUsecase("Exiting getRmAndSkipLevelRmInBU controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Reporting Manager Detail List", directReporteesList));
		} else {
			LOG.endUsecase("Exiting getRmAndSkipLevelRmInBU controller");
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No reporting manager found"));
		}
	}
	// End of Addition by Kamal Anand

	// Added by Balaji
	/**
	 * @category This service is used to get All Domain Manager details in a BU
	 *           to which Employee belong
	 * 
	 * @param Short
	 *            buId
	 * @return List<DmDetailsOutputBean>
	 * @throws BusinessException
	 */
	@ApiOperation(value = "This service will be called when user wants to view all Domain Managers in the particular BU "
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all Domain Managers."
					+ "<br>The Response will be the list of the Domain managers based on Business Unit Id.",

			nickname = "All Active Domain Manager Details by BU Id", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = DmDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "All active Domain managers are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Active Domain Managers Details by BU Id", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Active Domain Managers Details by BU Id", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_DOMAIN_MANAGERS_BY_BU, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getDomainManagerDetails(@PathVariable Short buId) throws BusinessException {
		LOG.startUsecase("Entering getDomainManagerDetails controller");
		List<DmDetailsOutputBean> domainManagerList = null;
		try {
			domainManagerList = commonSrvc.getDomainManagerDetails(buId);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		if (!domainManagerList.isEmpty()) {
			LOG.endUsecase("Exiting getDomainManagerDetails controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Domain Managers Details List", domainManagerList));
		} else {
			LOG.endUsecase("Exiting getDomainManagerDetails controller");
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No Details found"));
		}
	}

	/**
	 * @category This service is used to get All Role details in a Organization
	 * 
	 * @return List<RoleDetailsOutputBean>
	 * @throws BusinessException
	 */
	@ApiOperation(value = "This service will be called when user wants to view all the details of Roles in Organization"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all the Role0 details."
					+ "<br>The Response will be the list of the reporting managers based on emp Id.",

			nickname = "Fetch all Roles Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeNameWithRMIdBean.class),
			@ApiResponse(code = 201, message = "All Role details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Role Details of Organization", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Role Details of Organization", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_ROLE_DETAILS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getRoleDetails() throws BusinessException {
		LOG.startUsecase("Entering getRoleDetails controller");
		List<RoleDetailsOutputBean> roleDetails = null;
		try {
			roleDetails = commonSrvc.getRoleDetails();
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		if (!roleDetails.isEmpty()) {
			LOG.endUsecase("Exiting getRoleDetails controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Role Details List", roleDetails));
		} else {
			LOG.endUsecase("Exiting getRoleDetails controller");
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No Details found"));
		}
	}

	// End of Addition by Balaji

	// Added By Addavayya Hiremath
	/**
	 * 
	 * <Description getLocationDetailsByCountryId:> This method is used to
	 * display Location Details
	 * 
	 * @param countryId
	 * @return MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	@ApiOperation(value = "Display Location Details." + " .", httpMethod = "GET",

			notes = "This Service has been implemented to display Location details"
					+ "<br>The Details will be shown on UI.",

			nickname = "Dsiplay Location Details ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = LocationByCountyBean.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Succefully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Display Location details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Display Location details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_LOCATION_DETAILS_COUNTRYID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getLocationDetailsByCountryId(@PathVariable Short countryId)
			throws BusinessException, DataAccessException, CommonCustomException {

		LOG.startUsecase("diplay Location Detail");
		List<LocationByCountyBean> locDetailList = null;

		try {
			locDetailList = commonSrvc.getLocationDetails(countryId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all Location Details", e);
		}

		LOG.endUsecase("diplay Location Detail completed");
		if (!locDetailList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All Location Details have been retrieved successfully", locDetailList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No Location Details Found", locDetailList));
		}
	}

	// END of Addition By Adavayya Hiremath

	// Added by Rajesh Kumar
	/**
	 * 
	 * <Description getforex:> This method is used to display forex Details
	 * 
	 * @param fkForexCurrencyTypeId
	 * @return MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	@ApiOperation(value = "Display Forex Details."
			+ " .", httpMethod = "GET", notes = "This Service has been implemented to display Forex details"
					+ "<br>The Details will be shown on UI.", nickname = "Display Forex Details ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = DatForexConversionsBO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Succefully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "CurrecyTypeId", description = "Display Forex details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "CurrencyTypeId", description = "Display Forex details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_FOREX_BY_CURRENCY_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getForexConversion(@PathVariable Byte fkForexCurrencyTypeId)
			throws BusinessException, DataAccessException, CommonCustomException {
		List<DatForexConversionsBO> datForexConversionsBO = new ArrayList<DatForexConversionsBO>();
		try {
			datForexConversionsBO = commonSrvc.getForexConversion(fkForexCurrencyTypeId);
		} catch (DataAccessException e) {
			throw new CommonCustomException("Failed to retrieve all forex Details", e);
		}
		if (!datForexConversionsBO.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All Forex Details have been retrieved successfully", datForexConversionsBO));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No Forex Details Found", new ArrayList()));
		}
	}
	// EOD of Rajesh Kumar

	/**
	 * 
	 * <Description getLocationDetailsByCountryId:> This method is used to
	 * display Location Details
	 * 
	 * @param countryId
	 * @return MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	@ApiOperation(value = "Display Location Details." + " .", httpMethod = "GET",

			notes = "This Service has been implemented to display Physical Location details"
					+ "<br>The Details will be shown on UI.",

			nickname = "Dsiplay Physical Location Details ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = LocationByCountyBean.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Succefully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Display Physical Location details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Display Physical Location details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_PHYSICAL_LOCATION_DETAILS_BY_PARENT_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getPhysicalLocationDetailsByParentId(@PathVariable Short countryId)
			throws BusinessException, DataAccessException, CommonCustomException {

		LOG.startUsecase("diplay Location Detail");
		List<LocationByCountyBean> locDetailList = null;

		try {
			locDetailList = commonSrvc.getPhysicalLocationDetails(countryId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all Location Details", e);
		}

		LOG.endUsecase("diplay Location Detail completed");
		if (!locDetailList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All Location Details have been retrieved successfully", locDetailList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No Location Details Found", locDetailList));
		}
	}

	/**
	 * 
	 * <Description getforex:> This method is used to display forex Details
	 * 
	 * @param fkForexCurrencyTypeId
	 * @return MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	@ApiOperation(value = "Display Forex Details."
			+ " .", httpMethod = "GET", notes = "This Service has been implemented to display Forex details"
					+ "<br>The Details will be shown on UI.", nickname = "Display Forex Details ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = DatForexConversionsBO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Succefully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "CurrecyTypeId", description = "Display Forex details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "CurrencyTypeId", description = "Display Forex details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_FOREX_BY_DATE, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getForexDetailsByDate(
			@PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date)
			throws BusinessException, DataAccessException, CommonCustomException {
		List<DatForexConversionsBO> datForexConversionsBO = new ArrayList<DatForexConversionsBO>();
		try {
			datForexConversionsBO = commonSrvc.getForexDetailsByDate(date);
		} catch (DataAccessException e) {
			throw new CommonCustomException("Failed to retrieve all forex Details", e);
		}
		if (!datForexConversionsBO.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"All Forex Details have been retrieved successfully based On date", datForexConversionsBO));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No Forex Details Found", new ArrayList()));
		}
	}

	/**
	 * 
	 * <Description getforex:> This method is used to display forex Details
	 * 
	 * @param fkForexCurrencyTypeId
	 * @return MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	@ApiOperation(value = "Download Videos."
			+ " .", httpMethod = "GET", notes = "This Service has been implemented to display Forex details"
					+ "<br>The Details will be shown on UI.", nickname = "Display Forex Details ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = DatForexConversionsBO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Succefully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "CurrecyTypeId", description = "Display Forex details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "CurrencyTypeId", description = "Display Forex details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.DOWNLOAD_VIDEO, method = RequestMethod.GET, produces = "video/mp4")
	public @ResponseBody ResponseEntity<MISResponse> downloadVideos(@PathVariable("name") String name,
			HttpServletResponse response) throws BusinessException, DataAccessException, CommonCustomException {
		Boolean download = commonSrvc.downloadVideos(name, response);
		if (download) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Video Downloaded"));
		} else
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(new MISResponse(
					HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "Unable to Download the Video"));

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllProjectStatus> : Fetch All ProjectStatus.
	 * @param
	 * @return List<ProjMasProjectStatusBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all Business Entities. This service will be called when user wants to view all business Entities in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all present Business Entities."
					+ "<br>The Response will be the list of Business Entities.",

			nickname = "Get All Business Entities", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MasBusinessEntityBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MasBusinessEntityBean.class),
			@ApiResponse(code = 201, message = "All Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Bussiness Entities", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Bussiness Entities", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })

	@RequestMapping(value = CommonURIConstants.GET_ALL_BU_ENTITY, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllBEntity() throws BusinessException, DataAccessException {
		List<MasBusinessEntityBean> beBean = null;
		try {
			beBean = commonSrvc.getAllBusinessEntities();

		} catch (DataAccessException e) {
			throw new BusinessException("Failed to fetch the business entities.", e);
		}
		if (!beBean.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Successfully fetched businessEntity", beBean));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No records found."));
		}

	}
	// Added by Lalith kumar for getting all Verticals

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllVerticals> : Fetch All Verticals.
	 * @param
	 * @return List<MasVerticalBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all Verticals. This service will be called when user wants to view list of Verticals in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all present Verticals."
					+ "<br>The Response will be the list of Verticals.",

			nickname = "Get All Verticals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MasVerticalBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MasVerticalBean.class),
			@ApiResponse(code = 201, message = "All Verticals are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Verticals", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Verticals", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_VERTICALS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllVerticals() throws BusinessException, DataAccessException {
		List<MasVerticalBean> masVerticalsBeanList = new ArrayList<MasVerticalBean>();
		try {
			masVerticalsBeanList = commonSrvc.getAllVerticals();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all Verticals", e);
		}

		if (!masVerticalsBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All Verticals have been retrieved successfully", masVerticalsBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No Verticals found", masVerticalsBeanList));
		}

	}

	// EOA by Lalith kumar

	// Added by Rinta Mariam Jose
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllProjectCoordinator : Fetch All project
	 *           coordinators>
	 * @param
	 * @return List<ProjectCoordinatorBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all project coordinators. This service will be called when user wants to view list of project coordinators in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all project coordinators."
					+ "<br>The Response will be the list of the project coordinators.",

			nickname = "All Project Coordinators Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ProjectCoordinatorBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = ProjectCoordinatorBean.class),
			@ApiResponse(code = 201, message = "All project coordinator details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Project Coordinator Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Project Coordinator Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })

	@RequestMapping(value = CommonURIConstants.GET_ALL_PROJECT_COORDINATOR, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllProjectCoordinators()
			throws BusinessException, DataAccessException, CommonCustomException {
		List<ProjectCoordinatorBean> projectCoordinatorBeanList = null;

		try {
			projectCoordinatorBeanList = commonSrvc.getAllProjectCoordinator();
		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch the details: " + e.getMessage());
		}

		if (!projectCoordinatorBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Successfully fetched the details", projectCoordinatorBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE,
							"Failed to fetch the details.", projectCoordinatorBeanList));
		}

	}

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllRegions : Fetch All Regions>
	 * @param
	 * @return List<MasRegionBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all regions. This service will be called when user wants to view list of regions in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all regions."
					+ "<br>The Response will be the list of the regions.",

			nickname = "All Region Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MasRegionBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MasRegionBean.class),
			@ApiResponse(code = 201, message = "All region details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Region Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Region Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })

	@RequestMapping(value = CommonURIConstants.GET_ALL_REGIONS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllRegions()
			throws BusinessException, DataAccessException, CommonCustomException {
		List<MasRegionBean> masRegionBeanList = null;

		try {
			masRegionBeanList = commonSrvc.getAllRegions();
		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch the details: " + e.getMessage());
		}

		if (!masRegionBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Successfully fetched the details", masRegionBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE,
							"Failed to fetch the details.", masRegionBeanList));
		}

	}
	// EOA by Rinta Mariam Jose

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllComponentTypes> : Fetch All ComponentTypes.
	 * @param
	 * @return List<ProjMasCompTypeBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all Accounts. This service will be called when user wants to view list of Accounts in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all present Accounts."
					+ "<br>The Response will be the list of Accounts.",

			nickname = "Get All Accounts", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MasAccountBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MasAccountBean.class),
			@ApiResponse(code = 201, message = "All Account Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Account Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Account Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_ACCOUNT, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllAccount() throws BusinessException, DataAccessException {
		List<MasAccountBean> listmasAccount = new ArrayList<MasAccountBean>();
		try {
			listmasAccount = commonSrvc.getAllAccounts();
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all inactive employees", e);
		}

		if (!listmasAccount.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Successfully fetched the details", listmasAccount));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No records found."));
		}

	}

	@RequestMapping(value = CommonURIConstants.ACCOUNTS_MANAGER, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAllAccountManager(@PathVariable Short buId)
			throws BusinessException, DataAccessException, CommonCustomException {
		List<AcountsManagerOutputBean> acntMgrList = null;
		try {
			acntMgrList = commonSrvc.getAllAccountmanagerDetails(buId);
		} catch (Exception e) {
			throw new CommonCustomException("Unable to fetch the details of account manager");
		}

		if (!acntMgrList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "successfully fetched the details", acntMgrList));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					MISConstants.FAILURE, "Failed to fetch the details", acntMgrList));
		}

	}

	@RequestMapping(value = CommonURIConstants.DOMAIN_MANAGER, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAllDomainManager(@PathVariable Short buId)
			throws BusinessException, DataAccessException, CommonCustomException {
		List<ManagerOutputBean> dmnMgrList = null;
		try {
			dmnMgrList = commonSrvc.getAllDomainmanagerDetails(buId);
		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch the details of domain manager");
		}

		if (!dmnMgrList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "successfully fetched the domain manager details", dmnMgrList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(new MISResponse(HttpStatus.NOT_FOUND.value(),
					MISConstants.FAILURE, "Failed to fetch the domain manager details.", dmnMgrList));
		}

	}

	@RequestMapping(value = CommonURIConstants.DELIVERY_MANAGER, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAllDeliveryManager(@PathVariable Short buId) throws BusinessException, DataAccessException, CommonCustomException {
		List<ManagerOutputBean> deliveryMgrList = null;
		try
		{
		deliveryMgrList = commonSrvc.getAllDeliveryManagerDetails(buId);
		}catch(Exception e)
		{
			throw new CommonCustomException("Unable to fetch the details of Delivery Manager");
		}

		if (!deliveryMgrList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "successfully fetched the delivery manager details", deliveryMgrList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(new MISResponse(HttpStatus.NOT_FOUND.value(),
					MISConstants.FAILURE, "Failed to fetch the delivery manager details.", deliveryMgrList));
		}

	}

	@RequestMapping(value = CommonURIConstants.DOMAIN_DETAILS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getDomainDetailsByBuUnitId(@PathVariable Short buId)
			throws BusinessException, DataAccessException, CommonCustomException {
		List<DomainOutputBean> domainOutputBeanList = null;
		try {
			domainOutputBeanList = commonSrvc.getAllDomainDetailsByBuId(buId);
		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch the domain Details");
		}

		if (!domainOutputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "successfully fetched the domain details", domainOutputBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(new MISResponse(HttpStatus.NOT_FOUND.value(),
					MISConstants.FAILURE, "Failed to fetch the domain details.", domainOutputBeanList));
		}

	}

	@RequestMapping(value = CommonURIConstants.PAYMENT_INSTRUCTION_DETAIL, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAllPaymentDetails(@PathVariable byte entityId)
			throws BusinessException, DataAccessException, CommonCustomException {
		List<PaymentInstructionOutputBean> paymentList = null;
		try{
		paymentList = commonSrvc.getAllPaymentInstructionDetails(entityId);
		}catch(Exception e)
		{
			throw new CommonCustomException("Unable to fetch details of payment Instruction Details of given entity Id");
		}

		if (!paymentList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "successfully fetched the payment instuction details.", paymentList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(new MISResponse(HttpStatus.NOT_FOUND.value(),
					MISConstants.FAILURE, "Failed to fetch the  payment instuction details.", paymentList));
		}

	}

	// Added by Rinta Mariam Jose
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllEmpDesignation : Fetch All Employee
	 *           Designation>
	 * @param
	 * @return List<MasEmpDesignationBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get Designation details. This service will be called when user wants to view Designation details in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch Designation details."
					+ "<br>The Response will be the list of Designation details.",

			nickname = "All Designation Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MasEmpDesignationBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MasEmpDesignationBean.class),
			@ApiResponse(code = 201, message = "All Designation details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Designation Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Designation Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })

	@RequestMapping(value = CommonURIConstants.GET_DESIGNATION_DETAILS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllEmpDesignation()
			throws BusinessException, DataAccessException, CommonCustomException {
		List<MasEmpDesignationBean> masEmpDesignationBeanList = null;

		try {
			masEmpDesignationBeanList = commonSrvc.getAllEmpDesignation();
		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch the details: " + e.getMessage());
		}

		if (!masEmpDesignationBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Successfully fetched the details", masEmpDesignationBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE,
							"Failed to fetch the details.", masEmpDesignationBeanList));
		}

	}
	
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllRateType : Fetch All Rate
	 *           Types>
	 * @param
	 * @return List<MasRateTypeBO>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get Rate Type details. This service will be called when user wants to view Rate Type details in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch Rate Type details."
					+ "<br>The Response will be the list of Rate Type details.",

			nickname = "All Rate Type Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MasRateTypeBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MasRateTypeBO.class),
			@ApiResponse(code = 201, message = "All Rate Type details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Rate Type Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Rate Type Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = CommonURIConstants.GET_ALL_RATE_TYPE, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllRateType()
			throws BusinessException, DataAccessException, CommonCustomException {
		List<MasRateTypeBO> masRateTypeBOList = null;

		try {
			masRateTypeBOList = commonSrvc.getAllRateType();
		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch the details: " + e.getMessage());
		}

		if (!masRateTypeBOList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Rate types fetched successfully", masRateTypeBOList));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE,
							"Failed to fetch the details."));
		}

	}
	
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getInvoiceRateType : Fetch All Invoice rate
	 *           types>
	 * @param
	 * @return List<MasInvoiceRateTypeBO>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get Invioce Rate Type details. This service will be called when user wants to view Invioce Rate Type details in UI"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch Invioce Rate Type details."
					+ "<br>The Response will be the list of Invioce Rate Type details.",

			nickname = "All Invioce Rate Type Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MasInvoiceRateTypeBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MasInvoiceRateTypeBO.class),
			@ApiResponse(code = 201, message = "All Invioce Rate Type details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Invioce Rate Type Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Invioce Rate Type Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })

	@RequestMapping(value = CommonURIConstants.GET_INVOICE_RATE_TYPE, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getInvoiceRateType()
			throws BusinessException, DataAccessException, CommonCustomException {
		List<MasInvoiceRateTypeBO> masInvoiceRateTypeBOList = null;

		try {
			masInvoiceRateTypeBOList = commonSrvc.getInvoiceRateType();
		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch the details: " + e.getMessage());
		}

		if (!masInvoiceRateTypeBOList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Invoice Rate types fetched successfully", masInvoiceRateTypeBOList));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE,
							"Failed to fetch the details."));
		}

	}
}
