/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  UserController.java           		             */
/*                                                                   */
/*  Author      :  Anil kumar B K	                                 */
/*                                                                   */
/*  Date        :  09-Jan-2017                                       */
/*                                                                   */
/*  Description :  Controller class for employee details		     */
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 09-Jan-2017    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.common.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thbs.mis.common.bean.DatEmpRoleMappingOutputBean;
import com.thbs.mis.common.bean.GsrStatusBean;
import com.thbs.mis.common.bean.UserBean;
import com.thbs.mis.common.constants.CommonURIConstants;
import com.thbs.mis.common.service.UserService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.legacymisdbSync.bean.MisUser;

/**
 * @author anil_krishnegowda
 *
 */
@Controller
public class UserController {
	
	/**
	 * Instance of <code>LOG</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(UserController.class);
	
	/**
	 * Instance of <code>UserService</code>
	 */
	@Autowired
	private UserService userService;
	
	/**
	 * <Description addUser:> This method is used to create user profile in MIS
	 * 
	 * @param user bean object
	 * @return ResponseEntity
	 * @throws DataAccessException
	 */
	@RequestMapping(value = CommonURIConstants.CREATE_USER, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> addUser(@RequestBody UserBean user){
		
		LOG.startUsecase("addUser ");
		Boolean success = false;
		try{
			success= userService.saveUser(user);
		}
		catch(Exception e){
			LOG.error("Exception has been occured while creating user profile");
		}
		LOG.endUsecase("addUser");
		
		if(success == true){
		
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS,
						"User profile created successfully"));
		}else{
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Some error occured while creating User profile."));
		}
		
	}	
	

	/**
	 * <Description addUser:> This method is used to update user profile in MIS
	 * 
	 * @param user
	 * @return ResponseEntity
	 * @throws DataAccessException
	 */
	@RequestMapping(value = CommonURIConstants.UPDATE_USER, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> updateUser(@RequestBody UserBean user){
		
		LOG.startUsecase("updateUser ");
		Boolean success = false;
		try{
			success= userService.updateUser(user);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		LOG.endUsecase("updateUser");
		
		if(success == true){
		
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS,
						"User profile updated successfully"));
		}else{
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Some error occured while updating User profile."));
		}
		
	}
	
	
	/**
	 * <Description addUser:> This method is used to check goal status for manager change
	 * 
	 * @param user
	 * @return ResponseEntity
	 * @throws DataAccessException
	 */
	@RequestMapping(value = CommonURIConstants.IS_GSR_GOALS_CLOSED_FOR_RM_CHANGE_LEGACY, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> gsrGoalsCheckForRmChangeForLegacy(@RequestBody MisUser user){
		
		LOG.startUsecase("gsrGoalsCheckForRmChange ");
		String status = "";
		List<String> goalStatus = new ArrayList<String>();
		try{
			status= userService.gsrGoalsCheckForRmChangeForLegacy(user);
			LOG.info("gsrGoalsCheckForRmChange status  "+status);
		if(!status.isEmpty())
			goalStatus.add(status);
			
			LOG.info("gsrGoalsCheckForRmChange status size "+goalStatus.size());
		}
		catch(Exception e){
			e.printStackTrace();
		}
		LOG.endUsecase("gsrGoalsCheckForRmChange");
		
		if(goalStatus.size() > 0){
		
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS,
						"Gsr goals are not closed",goalStatus));
		}else{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Gsr goals are closed",goalStatus));
		}
		
	}
	
	
	/**
	 * <Description addUser:> This method is used to check goal status for manager change
	 * 
	 * @param user
	 * @return ResponseEntity
	 * @throws DataAccessException
	 */
	@RequestMapping(value = CommonURIConstants.IS_GSR_GOALS_CLOSED_FOR_RM_CHANGE, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> gsrGoalsCheckForRmChange(@RequestBody MisUser user){
		
		LOG.startUsecase("gsrGoalsCheckForRmChange ");
		//String status = "";
		List<GsrStatusBean> listOfGsrStatusBean = new ArrayList<GsrStatusBean>();
		try{
			listOfGsrStatusBean= userService.gsrGoalsCheckForRmChange(user);
			
		/*if(!status.isEmpty())
			goalStatus.add(status);
			
			LOG.info("gsrGoalsCheckForRmChange status size "+goalStatus.size());*/
		}
		catch(Exception e){
			e.printStackTrace();
		}
		LOG.endUsecase("gsrGoalsCheckForRmChange");
		
	
		
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS,
						"Gsr goals status ",listOfGsrStatusBean));
		
	
		
	}
	//Added by prathibha to fetch user roles on 17-11-2017
	/**
	 * <Description>This method is used to fetch roles of loggedIn user
	 * @param :Employee Id
	 * @return :ResponseEntity
	 */
	@ApiOperation(tags = "Purchase", value = "Get all roles of  employee"
			+ "This service will be called from the front-end when employee logins to view his related roles", httpMethod = "GET", notes = "This Service has been implemented to view roles of employees"
			+ "<br>The Details will be fetched from database table"
			+ "<br> <b> Table : </b> </br>"
			+ "1)dat_emp_role_mapping table"
			+ "<br>",
     nickname = "Get employees mapped roles", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = DatEmpRoleMappingOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = DatEmpRoleMappingOutputBean.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response")})
	
	@RequestMapping(value = CommonURIConstants.GET_EMPLOYEE_MAPPED_ROLES, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getUserRoles(
			@PathVariable Integer empId) throws CommonCustomException,DataAccessException
	{
		LOG.entering("getRolesOfLogInUser");
		DatEmpRoleMappingOutputBean rollMappingBean = new DatEmpRoleMappingOutputBean();
		List<DatEmpRoleMappingOutputBean> empRoleList = new ArrayList<DatEmpRoleMappingOutputBean>();
		
		rollMappingBean = userService.getEmployeeRoles(empId);
		empRoleList.add(rollMappingBean); 
		
		return ResponseEntity.status(HttpStatus.OK.value()).body(
			new MISResponse(HttpStatus.OK.value(),MISConstants.SUCCESS,
				"Employee roles retrieved successfully",empRoleList));
		
	}
	
}
