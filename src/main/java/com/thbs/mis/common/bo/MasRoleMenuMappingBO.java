package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the dat_emp_detail database table.
 * 
 */
@Entity
@Table(name="mas_role_menu_mapping")
@NamedQuery(name="MasRoleMenuMappingBO.findAll", query="SELECT m FROM MasRoleMenuMappingBO m")
public class MasRoleMenuMappingBO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -384210212207779832L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="pk_role_menu_mapping_id")
	private short pkRoleMenuMappingId;

	@Column(name="fk_menu_id")
	private int fkMenuId;
	
	@Column(name="fk_role_id")
	private int fkRoleId;
	
	@Column(name="fk_menu_role_tab_id")
	private int fkMenuRoleTabId;
	
	
	@Column(name = "is_reporting_mgr_menu")
	private String isReportingMgrMenu;

	public MasRoleMenuMappingBO() {
	}

	public short getPkRoleMenuMappingId() {
		return pkRoleMenuMappingId;
	}

	public void setPkRoleMenuMappingId(short pkRoleMenuMappingId) {
		this.pkRoleMenuMappingId = pkRoleMenuMappingId;
	}

	public int getFkMenuId() {
		return fkMenuId;
	}

	public void setFkMenuId(int fkMenuId) {
		this.fkMenuId = fkMenuId;
	}

	public int getFkRoleId() {
		return fkRoleId;
	}

	public void setFkRoleId(int fkRoleId) {
		this.fkRoleId = fkRoleId;
	}

	public int getFkMenuRoleTabId() {
		return fkMenuRoleTabId;
	}

	public void setFkMenuRoleTabId(int fkMenuRoleTabId) {
		this.fkMenuRoleTabId = fkMenuRoleTabId;
	}
	
	

	public String getIsReportingMgrMenu()
	{
		return isReportingMgrMenu;
	}

	public void setIsReportingMgrMenu(String isReportingMgrMenu)
	{
		this.isReportingMgrMenu = isReportingMgrMenu;
	}

	@Override
	public String toString()
	{
		return "MasRoleMenuMappingBO [pkRoleMenuMappingId="
				+ pkRoleMenuMappingId + ", fkMenuId=" + fkMenuId
				+ ", fkRoleId=" + fkRoleId + ", fkMenuRoleTabId="
				+ fkMenuRoleTabId + ", isReportingMgrMenu="
				+ isReportingMgrMenu + "]";
	}
	
	
}
