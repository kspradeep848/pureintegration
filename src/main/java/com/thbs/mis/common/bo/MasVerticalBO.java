package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the proj_mas_verticals database table.
 * 
 */
@Entity
@Table(name="mas_verticals")
@NamedQuery(name="MasVerticalBO.findAll", query="SELECT p FROM MasVerticalBO p")
public class MasVerticalBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_vertical_id")
	private short pkVerticalId;

	@Column(name="vertical_name")
	private String verticalName;
	
	public MasVerticalBO() {
	}

	public short getPkVerticalId() {
		return this.pkVerticalId;
	}

	public void setPkVerticalId(short pkVerticalId) {
		this.pkVerticalId = pkVerticalId;
	}

	public String getVerticalName() {
		return this.verticalName;
	}

	public void setVerticalName(String verticalName) {
		this.verticalName = verticalName;
	}

	@Override
	public String toString() {
		return "MasVerticalBO [pkVerticalId=" + pkVerticalId
				+ ", verticalName=" + verticalName + "]";
	}

		
	
}