package com.thbs.mis.common.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mas_emp_level database table.
 * 
 */
@Entity
@Table(name="mas_emp_level")
@NamedQuery(name="MasEmpLevelBO.findAll", query="SELECT m FROM MasEmpLevelBO m")
public class MasEmpLevelBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_level_id")
	private short pkEmpLevelId;

	@Column(name="emp_level_name")
	private String empLevelName;

	public MasEmpLevelBO() {
	}

	public short getPkEmpLevelId() {
		return this.pkEmpLevelId;
	}

	public void setPkEmpLevelId(short pkEmpLevelId) {
		this.pkEmpLevelId = pkEmpLevelId;
	}

	public String getEmpLevelName() {
		return this.empLevelName;
	}

	public void setEmpLevelName(String empLevelName) {
		this.empLevelName = empLevelName;
	}

	@Override
	public String toString() {
		return "MasEmpLevelBO [pkEmpLevelId=" + pkEmpLevelId
				+ ", empLevelName=" + empLevelName + "]";
	}

}