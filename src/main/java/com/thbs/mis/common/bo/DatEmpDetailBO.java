package com.thbs.mis.common.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.common.bean.MyReporteesBean;
import com.thbs.mis.framework.util.CustomTimeSerializer;
import com.thbs.mis.project.bo.DatWorkorderAuditBO;
import com.thbs.mis.usermanagement.bean.AdvancedSearchOutputBean;

/**
 * The persistent class for the dat_emp_detail database table.
 * 
 */
@Entity
@Table(name = "dat_emp_detail")
@NamedQuery(name = "DatEmpDetailBO.findAll", query = "SELECT d FROM DatEmpDetailBO d")
@SqlResultSetMappings({
		@SqlResultSetMapping(name = "groupDetailsMappings", classes = {
				@ConstructorResult(targetClass = MyReporteesBean.class, columns = { @ColumnResult(name = "pk_emp_id"),
						@ColumnResult(name = "emp_first_name"), @ColumnResult(name = "emp_last_name"),
						@ColumnResult(name = "fk_emp_reporting_mgr_id"),
						@ColumnResult(name = "has_reportees_flag") }) }),
		// Added by Kamal Anand for User Management
		@SqlResultSetMapping(name = "advancedSearchResult", classes = {
				@ConstructorResult(targetClass = AdvancedSearchOutputBean.class, columns = {
						@ColumnResult(name = "pk_emp_id"), @ColumnResult(name = "empName"),
						@ColumnResult(name = "fk_emp_designation"), @ColumnResult(name = "emp_designation_name"),
						@ColumnResult(name = "location_parent_name"), @ColumnResult(name = "fk_emp_main_status"),
						@ColumnResult(name = "statusName"), @ColumnResult(name = "emp_date_of_joining"),
						@ColumnResult(name = "bu_unit_name") }) })
		// End of Addition by Kamal anand for User Management
})
@NamedNativeQueries({
		@NamedNativeQuery(name = "DatEmpDetailBO.getAllDirectReporteesWithRMIdForRMIdOnlyGraduatedForGSR", query = "SELECT a.pk_emp_id, a.emp_first_name, "
				+ " a.emp_last_name, a.fk_emp_reporting_mgr_id, a.has_reportees_flag "
				+ " from (select emp_detail.pk_emp_id, emp_personal.emp_first_name,  emp_personal.emp_last_name, emp_prof.fk_emp_reporting_mgr_id, emp_prof.has_reportees_flag from dat_emp_detail emp_detail "
				+ " INNER JOIN dat_emp_professional_detail emp_prof ON emp_prof.fk_main_emp_detail_id = emp_detail.pk_emp_id "
				+ " INNER JOIN dat_emp_personal_detail emp_personal ON emp_personal.fk_emp_detail_id = emp_detail.pk_emp_id "
				+ " WHERE  emp_prof.fk_emp_reporting_mgr_id =:rmId " + " And emp_detail.fk_emp_main_status = 1 "
				+ " AND emp_prof.emp_confirmation_graduate_flag = 1 " + " AND emp_detail.pk_emp_id not in(:empIDList) "
				+ " UNION " + " SELECT emp_detail.pk_emp_id, emp_personal.emp_first_name, "
				+ " emp_personal.emp_last_name, emp_prof.fk_emp_reporting_mgr_id, emp_prof.has_reportees_flag "
				+ "	from dat_emp_detail emp_detail "
				+ "	INNER JOIN dat_emp_professional_detail emp_prof ON emp_prof.fk_main_emp_detail_id = emp_detail.pk_emp_id "
				+ "	INNER JOIN dat_emp_personal_detail emp_personal ON emp_personal.fk_emp_detail_id = emp_detail.pk_emp_id "
				+ "	WHERE  emp_prof.fk_emp_domain_mgr_id =:rmId " + " And emp_detail.fk_emp_main_status = 1 "
				+ "	AND emp_prof.emp_confirmation_graduate_flag = 1 "
				+ " AND emp_detail.pk_emp_id not in(:empIDList)) as a"
				+ " order by a.pk_emp_id ", resultSetMapping = "groupDetailsMappings"),
		// Added by Kamal Anand for User Management
		@NamedNativeQuery(name = "UserManagement.advancedSearch", query = "select det.pk_emp_id,"
				+ "if(per.emp_middle_name is not null,concat(per.emp_first_name,' ',per.emp_middle_name,' ',per.emp_last_name), "
				+ "concat(per.emp_first_name,' ',per.emp_last_name)) as empName,	"
				+ "prof.fk_emp_designation,des.emp_designation_name,"
				+ "loc.location_parent_name,det.fk_emp_main_status,	"
				+ "if(det.fk_emp_main_status = 1,'ACTIVE',if(det.fk_emp_main_status = 2,'INACTIVE','WAITING FOR PROFILE CREATION')) as statusName,"
				+ "prof.emp_date_of_joining,bu.bu_unit_name	from dat_emp_detail det	"
				+ "join dat_emp_personal_detail per on per.fk_emp_detail_id = det.pk_emp_id	"
				+ "join dat_emp_professional_detail prof on prof.fk_main_emp_detail_id = det.pk_emp_id "
				+ "left join mas_emp_designation des on des.pk_emp_designation_id = prof.fk_emp_designation "
				+ "left join mas_location_parent loc on loc.pk_location_parent_id = prof.fk_emp_location_parent_id "
				+ "left join mas_bu_unit bu on bu.pk_bu_unit_id = prof.fk_emp_bu_unit "
				+ "where det.pk_emp_id in(:empIdList) order by det.pk_emp_id asc", resultSetMapping = "advancedSearchResult")
		// End of Addition by Kamal anand for User Management
})
public class DatEmpDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "pk_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private Integer pkEmpId;
	@Column(name = "accepted_thbs_policy_flag")
	private String acceptedThbsPolicyFlag;
	@JsonSerialize(using = CustomTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "emp_pwd_updated_on")
	private Date empPwdUpdatedOn;
	@Column(name = "emp_type")
	private Byte empType;
	@Column(name = "fk_emp_main_status")
	private Byte fkEmpMainStatus;
	@Column(name = "fk_emp_role_id")
	private Short fkEmpRoleId;
	@Column(name = "fk_emp_sub_status")
	private Byte fkEmpSubStatus;
	@Column(name = "modified_by")
	private Integer modifiedBy;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;
	@JsonSerialize(using = CustomTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "policy_accepted_date")
	private Date policyAcceptedDate;
	@Column(name = "profile_created_by")
	private Integer profileCreatedBy;
	@JsonSerialize(using = CustomTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "profile_created_date")
	private Date profileCreatedDate;
	@OneToOne
	@JoinColumn(name = "fk_emp_role_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpRoleBO masEmpRoleBO;
	@OneToOne
	@JoinColumn(name = "fk_emp_main_status", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpMainStatusBO masEmpMainStatusBO;
	@OneToOne
	@JoinColumn(name = "fk_emp_sub_status", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpSubStatusBO masEmpSubStatusBO;
	
	@Transient
	private String empFirstName;

	@Transient
	private String empLastName;

	@Transient
	private String empMiddleName;

	@Transient
	private Integer empRMId;

	@Transient
	private String reporteeFlag;

	public String getEmpFirstName() {
		return empFirstName;
	}

	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}

	public String getEmpLastName() {
		return empLastName;
	}

	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}

	public Integer getEmpRMId() {
		return empRMId;
	}

	public void setEmpRMId(Integer empRMId) {
		this.empRMId = empRMId;
	}

	public String getReporteeFlag() {
		return reporteeFlag;
	}

	public void setReporteeFlag(String reporteeFlag) {
		this.reporteeFlag = reporteeFlag;
	}

	/**
	 * <Description DatEmpDetailBO:> TODO
	 * 
	 * @param pkEmpId
	 * @param empFirstName
	 * @param empLastName
	 * @param empRMId
	 * @param hasReporteeFlag
	 */
	public DatEmpDetailBO(Integer pkEmpId, String empFirstName, String empLastName, Integer empRMId,
			String hasReporteeFlag) {
		super();
		this.pkEmpId = pkEmpId;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.empRMId = empRMId;
		this.reporteeFlag = hasReporteeFlag;
	}

	public DatEmpDetailBO(Integer pkEmpId, String empFirstName, String empLastName, String empMiddleName) {
		super();
		this.pkEmpId = pkEmpId;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.empMiddleName = empMiddleName;
	}

	public DatEmpDetailBO(Integer pkEmpId, String empFirstName, String empLastName) {
		super();
		this.pkEmpId = pkEmpId;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
	}

	public String getEmpMiddleName() {
		return empMiddleName;
	}

	public void setEmpMiddleName(String empMiddleName) {
		this.empMiddleName = empMiddleName;
	}

	public DatEmpDetailBO() {
	}

	public DatEmpDetailBO(Integer pkEmpId) {
		this.pkEmpId = pkEmpId;
	}

	public MasEmpRoleBO getMasEmpRoleBO() {
		return masEmpRoleBO;
	}

	public void setMasEmpRoleBO(MasEmpRoleBO masEmpRoleBO) {
		this.masEmpRoleBO = masEmpRoleBO;
	}

	public MasEmpMainStatusBO getMasEmpMainStatusBO() {
		return masEmpMainStatusBO;
	}

	public void setMasEmpMainStatusBO(MasEmpMainStatusBO masEmpMainStatusBO) {
		this.masEmpMainStatusBO = masEmpMainStatusBO;
	}

	public MasEmpSubStatusBO getMasEmpSubStatusBO() {
		return masEmpSubStatusBO;
	}

	public void setMasEmpSubStatusBO(MasEmpSubStatusBO masEmpSubStatusBO) {
		this.masEmpSubStatusBO = masEmpSubStatusBO;
	}

	public Integer getPkEmpId() {
		return this.pkEmpId;
	}

	public void setPkEmpId(Integer pkEmpId) {
		this.pkEmpId = pkEmpId;
	}

	public String getAcceptedThbsPolicyFlag() {
		return this.acceptedThbsPolicyFlag;
	}

	public void setAcceptedThbsPolicyFlag(String acceptedThbsPolicyFlag) {
		this.acceptedThbsPolicyFlag = acceptedThbsPolicyFlag;
	}

	public Date getEmpPwdUpdatedOn() {
		return this.empPwdUpdatedOn;
	}

	public void setEmpPwdUpdatedOn(Date empPwdUpdatedOn) {
		this.empPwdUpdatedOn = empPwdUpdatedOn;
	}

	public Byte getEmpType() {
		return this.empType;
	}

	public void setEmpType(Byte empType) {
		this.empType = empType;
	}

	public Byte getFkEmpMainStatus() {
		return this.fkEmpMainStatus;
	}

	public void setFkEmpMainStatus(Byte fkEmpMainStatus) {
		this.fkEmpMainStatus = fkEmpMainStatus;
	}

	public Short getFkEmpRoleId() {
		return this.fkEmpRoleId;
	}

	public void setFkEmpRoleId(Short fkEmpRoleId) {
		this.fkEmpRoleId = fkEmpRoleId;
	}

	public Byte getFkEmpSubStatus() {
		return this.fkEmpSubStatus;
	}

	public void setFkEmpSubStatus(Byte fkEmpSubStatus) {
		this.fkEmpSubStatus = fkEmpSubStatus;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Date getPolicyAcceptedDate() {
		return this.policyAcceptedDate;
	}

	public void setPolicyAcceptedDate(Date policyAcceptedDate) {
		this.policyAcceptedDate = policyAcceptedDate;
	}

	public Integer getProfileCreatedBy() {
		return this.profileCreatedBy;
	}

	public void setProfileCreatedBy(Integer profileCreatedBy) {
		this.profileCreatedBy = profileCreatedBy;
	}

	public Date getProfileCreatedDate() {
		return this.profileCreatedDate;
	}

	public void setProfileCreatedDate(Date profileCreatedDate) {
		this.profileCreatedDate = profileCreatedDate;
	}

	@Override
	public String toString() {
		return "DatEmpDetailBO [pkEmpId=" + pkEmpId + ", acceptedThbsPolicyFlag=" + acceptedThbsPolicyFlag
				+ ", empPwdUpdatedOn=" + empPwdUpdatedOn + ", empType=" + empType + ", fkEmpMainStatus="
				+ fkEmpMainStatus + ", fkEmpRoleId=" + fkEmpRoleId + ", fkEmpSubStatus=" + fkEmpSubStatus
				+ ", modifiedBy=" + modifiedBy + ", modifiedDate=" + modifiedDate + ", policyAcceptedDate="
				+ policyAcceptedDate + ", profileCreatedBy=" + profileCreatedBy + ", profileCreatedDate="
				+ profileCreatedDate + ", masEmpRoleBO=" + masEmpRoleBO + ", masEmpMainStatusBO=" + masEmpMainStatusBO
				+ ", masEmpSubStatusBO=" + masEmpSubStatusBO + ", empFirstName=" + empFirstName + ", empLastName="
				+ empLastName + ", empMiddleName=" + empMiddleName + ", empRMId=" + empRMId + ", reporteeFlag="
				+ reporteeFlag + "]";
	}

}