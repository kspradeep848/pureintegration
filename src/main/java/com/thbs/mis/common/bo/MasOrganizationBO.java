package com.thbs.mis.common.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mas_organization database table.
 * 
 */
@Entity
@Table(name="mas_organization")
@NamedQuery(name="MasOrganizationBO.findAll", query="SELECT m FROM MasOrganizationBO m")
public class MasOrganizationBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_organization_id")
	private short pkOrganizationId;

	@Column(name="organization_name")
	private String organizationName;

	public MasOrganizationBO() {
	}

	public short getPkOrganizationId() {
		return this.pkOrganizationId;
	}

	public void setPkOrganizationId(short pkOrganizationId) {
		this.pkOrganizationId = pkOrganizationId;
	}

	public String getOrganizationName() {
		return this.organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	@Override
	public String toString()
	{
		return "MasOrganizationBO [pkOrganizationId="
				+ pkOrganizationId + ", organizationName="
				+ organizationName + "]";
	}

}