package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * The persistent class for the mas_bu_unit database table.
 * 
 */
@Entity
@Table(name="mas_bu_unit")
@NamedQuery(name="MasBuUnitBO.findAll", query="SELECT m FROM MasBuUnitBO m")
public class MasBuUnitBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_bu_unit_id")
	private short pkBuUnitId;

	@Column(name="active_flag")
	private String activeFlag;

	@Column(name="bu_unit_name")
	private String buUnitName;

	private String comments;

	@Column(name="fk_bu_head_emp_id")
	private int fkBuHeadEmpId;
	
	public MasBuUnitBO() {
	}

	public short getPkBuUnitId() {
		return this.pkBuUnitId;
	}

	public void setPkBuUnitId(short pkBuUnitId) {
		this.pkBuUnitId = pkBuUnitId;
	}

	public String getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getBuUnitName() {
		return this.buUnitName;
	}

	public void setBuUnitName(String buUnitName) {
		this.buUnitName = buUnitName;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getFkBuHeadEmpId() {
		return this.fkBuHeadEmpId;
	}

	public void setFkBuHeadEmpId(int fkBuHeadEmpId) {
		this.fkBuHeadEmpId = fkBuHeadEmpId;
	}

	@Transient
	private Integer empTotalCount;
	
	@Transient
	private String buHeadName;
	
	public String getBuHeadName() {
		return buHeadName;
	}

	public void setBuHeadName(String buHeadName) {
		this.buHeadName = buHeadName;
	}

	public Integer getEmpTotalCount() {
		return empTotalCount;
	}

	public void setEmpTotalCount(Integer empTotalCount) {
		this.empTotalCount = empTotalCount;
	}
	
	//Added by prathibha for get BU Information
	
	public MasBuUnitBO(short pkBuUnitId,
			String buUnitName,Long empTotalCount,
			Integer fkBuHeadEmpId,String buHeadName)
	{
		this.pkBuUnitId=pkBuUnitId;
		this.buUnitName=buUnitName;
		this.empTotalCount=Integer.parseInt(empTotalCount.toString());
		this.fkBuHeadEmpId=fkBuHeadEmpId;
		this.buHeadName=buHeadName;
			
	}
   
	public MasBuUnitBO(short pkBuUnitId,
			String buUnitName,String activeFlag,Long empTotalCount,
			Integer fkBuHeadEmpId,String buHeadName)
	{
		this.pkBuUnitId=pkBuUnitId;
		this.buUnitName=buUnitName;
		this.activeFlag=activeFlag;
		this.empTotalCount=Integer.parseInt(empTotalCount.toString());
		this.fkBuHeadEmpId=fkBuHeadEmpId;
		this.buHeadName=buHeadName;
			
	}

	@Override
	public String toString() {
		return "MasBuUnitBO [pkBuUnitId=" + pkBuUnitId + ", activeFlag="
				+ activeFlag + ", buUnitName=" + buUnitName + ", comments="
				+ comments + ", fkBuHeadEmpId=" + fkBuHeadEmpId
				+ ", empTotalCount=" + empTotalCount + ", buHeadName="
				+ buHeadName + "]";
	}

		
	
}