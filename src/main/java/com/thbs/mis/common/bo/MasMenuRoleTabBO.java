package com.thbs.mis.common.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;




/**
 * The persistent class for the mas_menu_role_tab database table.
 * 
 */
@Entity
@Table(name="mas_menu_role_tab")
@NamedQuery(name="MasMenuRoleTabBO.findAll", query="SELECT m FROM MasMenuRoleTabBO m")
public class MasMenuRoleTabBO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="pk_menu_role_tab_id")
	private short pkMenuRoleTabId;

	@Column(name="menu_role_tab_description")
	private String menuRoleTabDescription;

	@Column(name="menu_role_tab_name")
	private String menuRoleTabName;

	public MasMenuRoleTabBO() {
		// TODO Auto-generated constructor stub
	}

	public short getPkMenuRoleTabId() {
		return this.pkMenuRoleTabId;
	}

	public void setPkMenuRoleTabId(short pkMenuRoleTabId) {
		this.pkMenuRoleTabId = pkMenuRoleTabId;
	}

	public String getMenuRoleTabDescription() {
		return this.menuRoleTabDescription;
	}

	public void setMenuRoleTabDescription(String menuRoleTabDescription) {
		this.menuRoleTabDescription = menuRoleTabDescription;
	}

	public String getMenuRoleTabName() {
		return this.menuRoleTabName;
	}

	public void setMenuRoleTabName(String menuRoleTabName) {
		this.menuRoleTabName = menuRoleTabName;
	}

	@Override
	public String toString() {
		return "MasMenuRoleTabBO [pkMenuRoleTabId=" + pkMenuRoleTabId
				+ ", menuRoleTabDescription=" + menuRoleTabDescription
				+ ", menuRoleTabName=" + menuRoleTabName + "]";
	}
	
}
