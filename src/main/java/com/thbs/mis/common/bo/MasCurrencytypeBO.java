package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the mas_currencytype database table.
 * 
 */
@Entity
@Table(name="mas_currencytype")
@NamedQuery(name="MasCurrencytypeBO.findAll", query="SELECT m FROM MasCurrencytypeBO m")
public class MasCurrencytypeBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private short pk_CurrencyType_ID;

	@Column(name = "currencyType_Code")
	private String currencyTypeCode;
	
	@Column(name = "currencyType_Desc")
	private String currencyTypeDesc;
	
	//bi-directional many-to-one association to PurchaseDatAssetQuote
	/*@OneToMany(mappedBy="masCurrencytype")
	private List<PurchaseDatAssetQuoteBO> purchaseDatAssetQuotes;*/

	public MasCurrencytypeBO() {
	}

	public short getPk_CurrencyType_ID() {
		return this.pk_CurrencyType_ID;
	}

	public void setPk_CurrencyType_ID(short pk_CurrencyType_ID) {
		this.pk_CurrencyType_ID = pk_CurrencyType_ID;
	}
	
	public String getCurrencyTypeCode() {
		return currencyTypeCode;
	}

	public void setCurrencyTypeCode(String currencyTypeCode) {
		this.currencyTypeCode = currencyTypeCode;
	}

	public String getCurrencyTypeDesc() {
		return currencyTypeDesc;
	}

	public void setCurrencyTypeDesc(String currencyTypeDesc) {
		this.currencyTypeDesc = currencyTypeDesc;
	}

	/*public List<PurchaseDatAssetQuoteBO> getPurchaseDatAssetQuotes() {
		return this.purchaseDatAssetQuotes;
	}

	public void setPurchaseDatAssetQuotes(List<PurchaseDatAssetQuoteBO> purchaseDatAssetQuotes) {
		this.purchaseDatAssetQuotes = purchaseDatAssetQuotes;
	}

	public PurchaseDatAssetQuoteBO addPurchaseDatAssetQuote(PurchaseDatAssetQuoteBO purchaseDatAssetQuote) {
		getPurchaseDatAssetQuotes().add(purchaseDatAssetQuote);
		purchaseDatAssetQuote.setMasCurrencytype(this);

		return purchaseDatAssetQuote;
	}

	public PurchaseDatAssetQuoteBO removePurchaseDatAssetQuote(PurchaseDatAssetQuoteBO purchaseDatAssetQuote) {
		getPurchaseDatAssetQuotes().remove(purchaseDatAssetQuote);
		purchaseDatAssetQuote.setMasCurrencytype(null);

		return purchaseDatAssetQuote;
	}*/

	@Override
	public String toString() {
		return "MasCurrencytypeBO [pk_CurrencyType_ID=" + pk_CurrencyType_ID
				+ ", currencyTypeCode=" + currencyTypeCode
				+ ", currencyTypeDesc=" + currencyTypeDesc
				+  "]";
	}
}