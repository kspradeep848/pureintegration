package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistence class for dat_emp_role_mapping table
 */

@Entity
@Table(name="dat_emp_role_mapping")
@NamedQuery(name="DatEmpRoleMappingBO.findAll", query="SELECT m FROM DatEmpRoleMappingBO m")
public class DatEmpRoleMappingBO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_role_mapping")
	private Integer pkEmpRoleMapping;
	
	@Column(name="fk_employee_id")
	private Integer fkEmployeeId;
	
	@Column(name="fk_role_id")
	private short fkRoleId;
	
	
	@OneToOne
	@JoinColumn(name="fk_employee_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO;
	
	
	@OneToOne
	@JoinColumn(name="fk_role_id",unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpRoleBO masEmpRoleBO;
	
	
	
	
	public DatEmpRoleMappingBO() {
	}

	public Integer getPkEmpRoleMapping() {
		return pkEmpRoleMapping;
	}

	public void setPkEmpRoleMapping(Integer pkEmpRoleMapping) {
		this.pkEmpRoleMapping = pkEmpRoleMapping;
	}

	public Integer getFkEmployeeId() {
		return fkEmployeeId;
	}

	public void setFkEmployeeId(Integer fkEmployeeId) {
		this.fkEmployeeId = fkEmployeeId;
	}

	public short getFkRoleId() {
		return fkRoleId;
	}

	public void setFkRoleId(short fkRoleId) {
		this.fkRoleId = fkRoleId;
	}

	public DatEmpDetailBO getDatEmpDetailBO() {
		return datEmpDetailBO;
	}

	public void setDatEmpDetailBO(DatEmpDetailBO datEmpDetailBO) {
		this.datEmpDetailBO = datEmpDetailBO;
	}

	public MasEmpRoleBO getMasEmpRoleBO() {
		return masEmpRoleBO;
	}

	public void setMasEmpRoleBO(MasEmpRoleBO masEmpRoleBO) {
		this.masEmpRoleBO = masEmpRoleBO;
	}

	
}
