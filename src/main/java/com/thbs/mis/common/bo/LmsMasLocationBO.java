package com.thbs.mis.common.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the lms_mas_location database table.
 * 
 */
@Entity
@Table(name="lms_mas_location")

public class LmsMasLocationBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int pk_Location_Idx;

	@Column(nullable=false)
	private int fk_Location_CountryID;

	@Column(nullable=false, length=80)
	private String location_Comments;

	@Column(nullable=false, length=80)
	private String location_LocName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date location_Updatedon;



	//bi-directional many-to-one association to EmployeeLoginBO
	@ManyToOne
	@JoinColumn(name="fk_Location_UpdatedByEmpId", nullable=false)
	private EmployeeLoginBO datEmplogin;

	public LmsMasLocationBO() {
	}

	public int getPk_Location_Idx() {
		return this.pk_Location_Idx;
	}

	public void setPk_Location_Idx(int pk_Location_Idx) {
		this.pk_Location_Idx = pk_Location_Idx;
	}

	public int getFk_Location_CountryID() {
		return this.fk_Location_CountryID;
	}

	public void setFk_Location_CountryID(int fk_Location_CountryID) {
		this.fk_Location_CountryID = fk_Location_CountryID;
	}

	public String getLocation_Comments() {
		return this.location_Comments;
	}

	public void setLocation_Comments(String location_Comments) {
		this.location_Comments = location_Comments;
	}

	public String getLocation_LocName() {
		return this.location_LocName;
	}

	public void setLocation_LocName(String location_LocName) {
		this.location_LocName = location_LocName;
	}

	public Date getLocation_Updatedon() {
		return this.location_Updatedon;
	}

	public void setLocation_Updatedon(Date location_Updatedon) {
		this.location_Updatedon = location_Updatedon;
	}

	

	

	public EmployeeLoginBO getDatEmplogin() {
		return this.datEmplogin;
	}

	public void setDatEmplogin(EmployeeLoginBO datEmplogin) {
		this.datEmplogin = datEmplogin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LmsMasLocationBO [pk_Location_Idx=" + pk_Location_Idx + ", fk_Location_CountryID="
				+ fk_Location_CountryID + ", location_Comments=" + location_Comments + ", location_LocName="
				+ location_LocName + ", location_Updatedon=" + location_Updatedon + ", datEmplogin=" + datEmplogin + "]";
	}
	
	

}