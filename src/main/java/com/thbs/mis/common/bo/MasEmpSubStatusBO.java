package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the mas_emp_sub_status database table.
 * 
 */
@Entity
@Table(name="mas_emp_sub_status")
@NamedQuery(name="MasEmpSubStatusBO.findAll", query="SELECT m FROM MasEmpSubStatusBO m")
public class MasEmpSubStatusBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_sub_status_id")
	private byte pkEmpSubStatusId;

	@Column(name="emp_sub_status_name")
	private String empSubStatusName;




	@Column(name="fk_emp_main_status_mapping")
	private byte fkEmpMainStatus;
	
	@OneToOne
	@JoinColumn(name="fk_emp_main_status_mapping",unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpMainStatusBO masEmpMainStatusBO;
	
	public MasEmpSubStatusBO() {
	}

	public byte getPkEmpSubStatusId() {
		return this.pkEmpSubStatusId;
	}

	public void setPkEmpSubStatusId(byte pkEmpSubStatusId) {
		this.pkEmpSubStatusId = pkEmpSubStatusId;
	}

	public String getEmpSubStatusName() {
		return this.empSubStatusName;
	}

	public void setEmpSubStatusName(String empSubStatusName) {
		this.empSubStatusName = empSubStatusName;
	}

	public byte getFkEmpMainStatus() {
		return this.fkEmpMainStatus;
	}

	public void setFkEmpMainStatus(byte fkEmpMainStatus) {
		this.fkEmpMainStatus = fkEmpMainStatus;
	}

	@Override
	public String toString()
	{
		return "MasEmpSubStatusBO [pkEmpSubStatusId="
				+ pkEmpSubStatusId + ", empSubStatusName="
				+ empSubStatusName + ", fkEmpMainStatus="
				+ fkEmpMainStatus + "]";
	}

}