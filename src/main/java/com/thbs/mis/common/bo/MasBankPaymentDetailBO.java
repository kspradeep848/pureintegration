package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the mas_bank_payment_details database table.
 * 
 */
@Entity
@Table(name="mas_bank_payment_details")
@NamedQuery(name="MasBankPaymentDetailBO.findAll", query="SELECT m FROM MasBankPaymentDetailBO m")
public class MasBankPaymentDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_bank_payment_id")
	private short pkBankPaymentId;

	@Column(name="payment_bank_name")
	private String paymentBankName;

	@Column(name="payment_instructions_detail")
	private String paymentInstructionsDetail;

	@Column(name="fk_entity_id")
	private byte fkEntityId;
	
	//bi-directional many-to-one association to MasCurrencytype
	@OneToOne
	@JoinColumn(name="fk_entity_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasBusinessEntityBO masBusinessEntityBO;
	
	public MasBankPaymentDetailBO() {
	}

	public short getPkBankPaymentId() {
		return this.pkBankPaymentId;
	}

	public void setPkBankPaymentId(short pkBankPaymentId) {
		this.pkBankPaymentId = pkBankPaymentId;
	}

	public String getPaymentBankName() {
		return this.paymentBankName;
	}

	public void setPaymentBankName(String paymentBankName) {
		this.paymentBankName = paymentBankName;
	}

	public String getPaymentInstructionsDetail() {
		return this.paymentInstructionsDetail;
	}

	public void setPaymentInstructionsDetail(String paymentInstructionsDetail) {
		this.paymentInstructionsDetail = paymentInstructionsDetail;
	}

	public MasBusinessEntityBO getMasBusinessEntityBO() {
		return masBusinessEntityBO;
	}

	public void setMasBusinessEntityBO(MasBusinessEntityBO masBusinessEntityBO) {
		this.masBusinessEntityBO = masBusinessEntityBO;
	}

	public byte getFkEntityId() {
		return fkEntityId;
	}

	public void setFkEntityId(byte fkEntityId) {
		this.fkEntityId = fkEntityId;
	}

	@Override
	public String toString() {
		return "MasBankPaymentDetailBO [pkBankPaymentId=" + pkBankPaymentId
				+ ", paymentBankName=" + paymentBankName
				+ ", paymentInstructionsDetail=" + paymentInstructionsDetail
				+ ", fkEntityId=" + fkEntityId + ", masBusinessEntityBO="
				+ masBusinessEntityBO + "]";
	}

	
		
}