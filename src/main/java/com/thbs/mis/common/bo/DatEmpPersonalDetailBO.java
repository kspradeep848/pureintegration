package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the dat_emp_personal_detail database table.
 * 
 */
@Entity
@Table(name="dat_emp_personal_detail")
@NamedQuery(name="DatEmpPersonalDetailBO.findAll", query="SELECT d FROM DatEmpPersonalDetailBO d")
public class DatEmpPersonalDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_personal_detail_id")
	private Integer pkEmpPersonalDetailId;

	@Column(name="emp_aadhar_number")
	private String empAadharNumber;

	@Column(name="emp_alternate_mobile_no")
	private String empAlternateMobileNo;

	@Column(name="emp_blood_group")
	private String empBloodGroup;

	@Column(name="emp_citizenship_country_one")
	private Short empCitizenshipOne;

	@Column(name="emp_citizenship_status")
	private String empCitizenshipStatus;

	@Column(name="emp_citizenship_country_two")
	private Short empCitizenshipTwo;

	@Temporal(TemporalType.DATE)
	@Column(name="emp_date_of_birth")
	private Date empDateOfBirth;

	@Column(name="emp_first_name")
	private String empFirstName;

	@Column(name="emp_gender")
	private Byte empGender;

	@Column(name="emp_last_name")
	private String empLastName;

	@Column(name="emp_middle_name")
	private String empMiddleName;

	@Column(name="emp_mobile_no")
	private String empMobileNo;

	@Column(name="emp_present_address_line_one")
	private String empPresentAddressLineOne;

	@Column(name="emp_present_address_line_two")
	private String empPresentAddressLineTwo;

	@Column(name="emp_personal_email_id")
	private String empPersonalEmailId;

	@Column(name="emp_qualification")
	private String empQualification;

	@Temporal(TemporalType.DATE)
	@Column(name="emp_spouse_date_of_birth")
	private Date empSpouseDateOfBirth;

	@Column(name="fk_emp_detail_id")
	private Integer fkEmpDetailId;
	
	@Column(name="emp_marital_status")
	private String empMaritalStatus;
	
	@Column(name="emp_present_address_city")
	private Short empPresentAddressCity;
	
	@Column(name="emp_present_address_state")
	private Short empPresentAddressState;
	
	@Column(name="emp_present_address_country")
	private Short empPresentAddressCountry;
	
	@Column(name="emp_permanent_address_line_one")
	private String empPermanentAddressLineOne;
	
	@Column(name="emp_permanent_address_line_two")
	private String empPermanentAddressLineTwo;
	
	@Column(name="emp_permanent_address_city")
	private Short empPermanentAddressCity;
	
	@Column(name="emp_permanent_address_state")
	private Short empPermanentAddressState;
	
	@Column(name="emp_permanent_address_country")
	private Short empPermanentAddressCountry;
	
	public String getEmpMaritalStatus() {
		return empMaritalStatus;
	}

	public void setEmpMaritalStatus(String empMaritalStatus) {
		this.empMaritalStatus = empMaritalStatus;
	}
	@Column(name="emp_personal_detail_modified_by")
	private Integer empPersonalDetailModifiedBy;
	
	@Column(name="emp_spouse_name")
	private String empSpouseName;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="modified_date")
	private Date empPersonalDetailModifiedDate;

	@OneToOne
	@JoinColumn(name="fk_emp_detail_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO;
	
	@OneToOne
	@JoinColumn(name="emp_personal_detail_modified_by",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpPersonalDetailModifiedBy;
	
	@Transient
	private String empFullNameWithId;
	
	@Transient
	private Date empDateOfJoining;
	
	//Added by Shyam for setting of designation in bean
	@Transient
	private String empDesignationName;
	//End of addition by Shyam for setting of designation in bean
	
	@Transient
	private Byte fkEmpMainStatus;
	
	@Transient
	private Integer fkEmpReportingMgrId;
	
	@Transient
	private Short fkEmpBuUnit;
	
	@Transient
	private String buUnitName;
	
	@Transient
	private int fkBuHeadEmpId;
	
	public DatEmpPersonalDetailBO(String empFirstName, String empMiddleName, String empLastName) {
		super();
		this.empFirstName = empFirstName;
		this.empMiddleName = empMiddleName;
		this.empLastName = empLastName;
	}
	
	public DatEmpPersonalDetailBO() {
	}
	
	/**
	 * <Description DatEmpPersonalDetailBO:> TODO
	 * @param pkEmpId
	 * @param empFirstName
	 * @param empLastName
	 */
	
	
	public DatEmpPersonalDetailBO(Integer fkEmpDetailId, String empFirstName, String empLastName,Byte fkEmpMainStatus, Integer fkEmpReportingMgrId, Short fkEmpBuUnit, int fkBuHeadEmpId, String buUnitName) {
		super();
		this.fkEmpDetailId = fkEmpDetailId;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.fkEmpMainStatus = fkEmpMainStatus;
		this.fkEmpReportingMgrId = fkEmpReportingMgrId;
		this.fkEmpBuUnit = fkEmpBuUnit;
		this.fkBuHeadEmpId = fkBuHeadEmpId;
		this.buUnitName = buUnitName;
	}
	
	public DatEmpPersonalDetailBO(Integer fkEmpDetailId, String empFirstName, String empLastName) {
		super();
		this.fkEmpDetailId = fkEmpDetailId;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
	}
	
	//Added by Shyam for setting of designation in bean
	public DatEmpPersonalDetailBO(Integer fkEmpDetailId, String empFirstName, String empLastName,
			String empDesignationName) {
		super();
		this.fkEmpDetailId = fkEmpDetailId;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.empDesignationName = empDesignationName;
	}
	//End of addition by Shyam for setting of designation in bean
	
	public DatEmpPersonalDetailBO(Integer fkEmpDetailId, String empFirstName, String empLastName,
			Byte empGender, Date empDateOfBirth){
		this.fkEmpDetailId = fkEmpDetailId;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.empGender = empGender;
		this.empDateOfBirth = empDateOfBirth;
		this.empFullNameWithId = empFirstName + " " + empLastName + "-" + fkEmpDetailId;
	}

	
	public DatEmpPersonalDetailBO(Integer fkEmpDetailId, String empFirstName, String empLastName,
			Date empDateOfJoining, Byte empGender){
		this.fkEmpDetailId = fkEmpDetailId;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.empGender = empGender;
		this.empDateOfJoining = empDateOfJoining;
		this.empFullNameWithId = empFirstName + " " + empLastName + "-" + fkEmpDetailId;
	}
	
	// Added by Balaji
	public DatEmpPersonalDetailBO (String empFirstName, String empLastName, Integer fkEmpDetailId){
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.fkEmpDetailId = fkEmpDetailId;
	}
	// EOA by Balaji 
	
	public String getBuUnitName() {
		return buUnitName;
	}

	public void setBuUnitName(String buUnitName) {
		this.buUnitName = buUnitName;
	}

	public int getFkBuHeadEmpId() {
		return fkBuHeadEmpId;
	}

	public void setFkBuHeadEmpId(int fkBuHeadEmpId) {
		this.fkBuHeadEmpId = fkBuHeadEmpId;
	}

	public Byte getFkEmpMainStatus() {
		return fkEmpMainStatus;
	}

	public void setFkEmpMainStatus(Byte fkEmpMainStatus) {
		this.fkEmpMainStatus = fkEmpMainStatus;
	}

	public Integer getFkEmpReportingMgrId() {
		return fkEmpReportingMgrId;
	}

	public void setFkEmpReportingMgrId(Integer fkEmpReportingMgrId) {
		this.fkEmpReportingMgrId = fkEmpReportingMgrId;
	}

	public Short getFkEmpBuUnit() {
		return fkEmpBuUnit;
	}

	public void setFkEmpBuUnit(Short fkEmpBuUnit) {
		this.fkEmpBuUnit = fkEmpBuUnit;
	}

	
	public DatEmpDetailBO getDatEmpPersonalDetailModifiedBy() {
		return datEmpPersonalDetailModifiedBy;
	}

	public String getEmpSpouseName() {
		return empSpouseName;
	}

	public void setEmpSpouseName(String empSpouseName) {
		this.empSpouseName = empSpouseName;
	}

	public void setDatEmpPersonalDetailModifiedBy(
			DatEmpDetailBO datEmpPersonalDetailModifiedBy) {
		this.datEmpPersonalDetailModifiedBy = datEmpPersonalDetailModifiedBy;
	}

	public Integer getEmpPersonalDetailModifiedBy() {
		return empPersonalDetailModifiedBy;
	}

	public void setEmpPersonalDetailModifiedBy(Integer empPersonalDetailModifiedBy) {
		this.empPersonalDetailModifiedBy = empPersonalDetailModifiedBy;
	}

	public Date getEmpPersonalDetailModifiedDate() {
		return empPersonalDetailModifiedDate;
	}

	public void setEmpPersonalDetailModifiedDate(Date empPersonalDetailModifiedDate) {
		this.empPersonalDetailModifiedDate = empPersonalDetailModifiedDate;
	}

	public Integer getPkEmpPersonalDetailId() {
		return this.pkEmpPersonalDetailId;
	}

	public void setPkEmpPersonalDetailId(Integer pkEmpPersonalDetailId) {
		this.pkEmpPersonalDetailId = pkEmpPersonalDetailId;
	}

	public String getEmpAadharNumber() {
		return this.empAadharNumber;
	}

	public void setEmpAadharNumber(String empAadharNumber) {
		this.empAadharNumber = empAadharNumber;
	}

	public String getEmpAlternateMobileNo() {
		return this.empAlternateMobileNo;
	}

	public void setEmpAlternateMobileNo(String empAlternateMobileNo) {
		this.empAlternateMobileNo = empAlternateMobileNo;
	}

	public String getEmpBloodGroup() {
		return this.empBloodGroup;
	}

	public void setEmpBloodGroup(String empBloodGroup) {
		this.empBloodGroup = empBloodGroup;
	}

	public Short getEmpCitizenshipOne() {
		return this.empCitizenshipOne;
	}

	public void setEmpCitizenshipOne(Short empCitizenshipOne) {
		this.empCitizenshipOne = empCitizenshipOne;
	}

	public String getEmpCitizenshipStatus() {
		return this.empCitizenshipStatus;
	}

	public void setEmpCitizenshipStatus(String empCitizenshipStatus) {
		this.empCitizenshipStatus = empCitizenshipStatus;
	}

	public Short getEmpCitizenshipTwo() {
		return this.empCitizenshipTwo;
	}

	public void setEmpCitizenshipTwo(Short empCitizenshipTwo) {
		this.empCitizenshipTwo = empCitizenshipTwo;
	}

	public Date getEmpDateOfBirth() {
		return this.empDateOfBirth;
	}

	public void setEmpDateOfBirth(Date empDateOfBirth) {
		this.empDateOfBirth = empDateOfBirth;
	}

	public String getEmpFirstName() {
		return this.empFirstName;
	}

	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}

	public Byte getEmpGender() {
		return this.empGender;
	}

	public void setEmpGender(Byte empGender) {
		this.empGender = empGender;
	}

	public String getEmpLastName() {
		return this.empLastName;
	}

	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}

	public String getEmpMiddleName() {
		return this.empMiddleName;
	}

	public void setEmpMiddleName(String empMiddleName) {
		this.empMiddleName = empMiddleName;
	}

	public String getEmpMobileNo() {
		return this.empMobileNo;
	}

	public void setEmpMobileNo(String empMobileNo) {
		this.empMobileNo = empMobileNo;
	}

	public String getEmpQualification() {
		return this.empQualification;
	}

	public void setEmpQualification(String empQualification) {
		this.empQualification = empQualification;
	}

	public Date getEmpSpouseDateOfBirth() {
		return this.empSpouseDateOfBirth;
	}

	public void setEmpSpouseDateOfBirth(Date empSpouseDateOfBirth) {
		this.empSpouseDateOfBirth = empSpouseDateOfBirth;
	}

	public Integer getFkEmpDetailId() {
		return this.fkEmpDetailId;
	}

	public void setFkEmpDetailId(Integer fkEmpDetailId) {
		this.fkEmpDetailId = fkEmpDetailId;
	}

	
	public DatEmpDetailBO getDatEmpDetailBO() {
		return datEmpDetailBO;
	}

	public void setDatEmpDetailBO(DatEmpDetailBO datEmpDetailBO) {
		this.datEmpDetailBO = datEmpDetailBO;
	}

	public String getEmpFullNameWithId() {
		return empFullNameWithId;
	}

	public void setEmpFullNameWithId(String empFullNameWithId) {
		this.empFullNameWithId = empFullNameWithId;
	}
	
	public Date getEmpDateOfJoining() {
		return empDateOfJoining;
	}

	public void setEmpDateOfJoining(Date empDateOfJoining) {
		this.empDateOfJoining = empDateOfJoining;
	}
	//Added by Shyam for setting of designation in bean
	public String getEmpDesignationName() {
		return empDesignationName;
	}

	public void setEmpDesignationName(String empDesignationName) {
		this.empDesignationName = empDesignationName;
	}
	//End of addition by Shyam for setting of designation in bean


	public String getEmpPresentAddressLineOne() {
		return empPresentAddressLineOne;
	}

	public void setEmpPresentAddressLineOne(String empPresentAddressLineOne) {
		this.empPresentAddressLineOne = empPresentAddressLineOne;
	}

	public String getEmpPresentAddressLineTwo() {
		return empPresentAddressLineTwo;
	}

	public void setEmpPresentAddressLineTwo(String empPresentAddressLineTwo) {
		this.empPresentAddressLineTwo = empPresentAddressLineTwo;
	}

	public String getEmpPermanentAddressLineOne() {
		return empPermanentAddressLineOne;
	}

	public void setEmpPermanentAddressLineOne(String empPermanentAddressLineOne) {
		this.empPermanentAddressLineOne = empPermanentAddressLineOne;
	}

	public String getEmpPermanentAddressLineTwo() {
		return empPermanentAddressLineTwo;
	}

	public void setEmpPermanentAddressLineTwo(String empPermanentAddressLineTwo) {
		this.empPermanentAddressLineTwo = empPermanentAddressLineTwo;
	}

	
	public String getEmpPersonalEmailId() {
		return empPersonalEmailId;
	}

	public void setEmpPersonalEmailId(String empPersonalEmailId) {
		this.empPersonalEmailId = empPersonalEmailId;
	}

	
	
	public Short getEmpPresentAddressCity() {
		return empPresentAddressCity;
	}

	public void setEmpPresentAddressCity(Short empPresentAddressCity) {
		this.empPresentAddressCity = empPresentAddressCity;
	}

	public Short getEmpPresentAddressState() {
		return empPresentAddressState;
	}

	public void setEmpPresentAddressState(Short empPresentAddressState) {
		this.empPresentAddressState = empPresentAddressState;
	}

	public Short getEmpPresentAddressCountry() {
		return empPresentAddressCountry;
	}

	public void setEmpPresentAddressCountry(Short empPresentAddressCountry) {
		this.empPresentAddressCountry = empPresentAddressCountry;
	}

	public Short getEmpPermanentAddressCity() {
		return empPermanentAddressCity;
	}

	public void setEmpPermanentAddressCity(Short empPermanentAddressCity) {
		this.empPermanentAddressCity = empPermanentAddressCity;
	}

	public Short getEmpPermanentAddressState() {
		return empPermanentAddressState;
	}

	public void setEmpPermanentAddressState(Short empPermanentAddressState) {
		this.empPermanentAddressState = empPermanentAddressState;
	}

	public Short getEmpPermanentAddressCountry() {
		return empPermanentAddressCountry;
	}

	public void setEmpPermanentAddressCountry(Short empPermanentAddressCountry) {
		this.empPermanentAddressCountry = empPermanentAddressCountry;
	}

	@Override
	public String toString() {
		return "DatEmpPersonalDetailBO [pkEmpPersonalDetailId="
				+ pkEmpPersonalDetailId + ", empAadharNumber="
				+ empAadharNumber + ", empAlternateMobileNo="
				+ empAlternateMobileNo + ", empBloodGroup=" + empBloodGroup
				+ ", empCitizenshipOne=" + empCitizenshipOne
				+ ", empCitizenshipStatus=" + empCitizenshipStatus
				+ ", empCitizenshipTwo=" + empCitizenshipTwo
				+ ", empDateOfBirth=" + empDateOfBirth + ", empFirstName="
				+ empFirstName + ", empGender=" + empGender + ", empLastName="
				+ empLastName + ", empMiddleName=" + empMiddleName
				+ ", empMobileNo=" + empMobileNo
				+ ", empPresentAddressLineOne=" + empPresentAddressLineOne
				+ ", empPresentAddressLineTwo=" + empPresentAddressLineTwo
				+ ", empPersonalEmailId=" + empPersonalEmailId
				+ ", empQualification=" + empQualification
				+ ", empSpouseDateOfBirth=" + empSpouseDateOfBirth
				+ ", fkEmpDetailId=" + fkEmpDetailId + ", empMaritalStatus="
				+ empMaritalStatus + ", empPresentAddressCity="
				+ empPresentAddressCity + ", empPresentAddressState="
				+ empPresentAddressState + ", empPresentAddressCountry="
				+ empPresentAddressCountry + ", empPermanentAddressLineOne="
				+ empPermanentAddressLineOne + ", empPermanentAddressLineTwo="
				+ empPermanentAddressLineTwo + ", empPermanentAddressCity="
				+ empPermanentAddressCity + ", empPermanentAddressState="
				+ empPermanentAddressState + ", empPermanentAddressCountry="
				+ empPermanentAddressCountry + ", empPersonalDetailModifiedBy="
				+ empPersonalDetailModifiedBy + ", empSpouseName="
				+ empSpouseName + ", empPersonalDetailModifiedDate="
				+ empPersonalDetailModifiedDate + ", datEmpDetailBO="
				+ datEmpDetailBO + ", datEmpPersonalDetailModifiedBy="
				+ datEmpPersonalDetailModifiedBy + ", empFullNameWithId="
				+ empFullNameWithId + ", empDateOfJoining=" + empDateOfJoining
				+ ", empDesignationName=" + empDesignationName
				+ ", fkEmpMainStatus=" + fkEmpMainStatus
				+ ", fkEmpReportingMgrId=" + fkEmpReportingMgrId
				+ ", fkEmpBuUnit=" + fkEmpBuUnit + ", buUnitName=" + buUnitName
				+ ", fkBuHeadEmpId=" + fkBuHeadEmpId + "]";
	}

	

}