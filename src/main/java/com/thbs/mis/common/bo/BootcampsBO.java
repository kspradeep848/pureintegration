package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the mas_bootcamps database table.
 * 
 */
@Entity
@Table(name="mas_bootcamps")

public class BootcampsBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_bootcamp_idx", unique=true, nullable=false)
	private int pkBootcampIdx;

	@Column(name="bootcamp_name", length=80)
	private String bootcampName;

	@Column(name="bootcamp_year", length=30)
	private String bootcampYear;

	

	public BootcampsBO() {
	}

	public int getPkBootcampIdx() {
		return this.pkBootcampIdx;
	}

	public void setPkBootcampIdx(int pkBootcampIdx) {
		this.pkBootcampIdx = pkBootcampIdx;
	}

	public String getBootcampName() {
		return this.bootcampName;
	}

	public void setBootcampName(String bootcampName) {
		this.bootcampName = bootcampName;
	}

	public String getBootcampYear() {
		return this.bootcampYear;
	}

	public void setBootcampYear(String bootcampYear) {
		this.bootcampYear = bootcampYear;
	}

	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BootcampsBO [pkBootcampIdx=" + pkBootcampIdx + ", bootcampName=" + bootcampName + ", bootcampYear="
				+ bootcampYear +  "]";
	}
	

}