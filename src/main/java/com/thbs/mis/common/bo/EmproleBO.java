package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the mas_emproles database table.
 * 
 */
@Entity
@Table(name="mas_emproles")

public class EmproleBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int pk_EmpRole_RoleID;

	@Column(nullable=false, length=250)
	private String empRole_RoleDesc;

	

	public EmproleBO() {
	}

	public int getPk_EmpRole_RoleID() {
		return this.pk_EmpRole_RoleID;
	}

	public void setPk_EmpRole_RoleID(int pk_EmpRole_RoleID) {
		this.pk_EmpRole_RoleID = pk_EmpRole_RoleID;
	}

	public String getEmpRole_RoleDesc() {
		return this.empRole_RoleDesc;
	}

	public void setEmpRole_RoleDesc(String empRole_RoleDesc) {
		this.empRole_RoleDesc = empRole_RoleDesc;
	}

	
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmproleBO [pk_EmpRole_RoleID=" + pk_EmpRole_RoleID + ", empRole_RoleDesc=" + empRole_RoleDesc
				+  "]";
	}
	

}