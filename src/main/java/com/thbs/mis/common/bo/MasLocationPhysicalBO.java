package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the mas_location_physical database table.
 * 
 */
@Entity
@Table(name="mas_location_physical")
@NamedQuery(name="MasLocationPhysicalBO.findAll", query="SELECT m FROM MasLocationPhysicalBO m")
public class MasLocationPhysicalBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_location_physical_id")
	private short pkLocationPhysicalId;

	@Column(name="fk_location_parent_id")
	private short fkLocationParentId;

	@Column(name="location_physical_name")
	private String locationPhysicalName;


	@OneToOne()
	@JoinColumn(name = "fk_location_parent_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasLocationParentBO locationParent;
	public MasLocationPhysicalBO() {
	}

	public short getPkLocationPhysicalId() {
		return this.pkLocationPhysicalId;
	}

	public void setPkLocationPhysicalId(short pkLocationPhysicalId) {
		this.pkLocationPhysicalId = pkLocationPhysicalId;
	}

	public short getFkLocationParentId() {
		return this.fkLocationParentId;
	}

	public void setFkLocationParentId(short fkLocationParentId) {
		this.fkLocationParentId = fkLocationParentId;
	}

	public String getLocationPhysicalName() {
		return this.locationPhysicalName;
	}

	public void setLocationPhysicalName(String locationPhysicalName) {
		this.locationPhysicalName = locationPhysicalName;
	}

	@Override
	public String toString()
	{
		return "MasLocationPhysicalBO [pkLocationPhysicalId="
				+ pkLocationPhysicalId + ", fkLocationParentId="
				+ fkLocationParentId + ", locationPhysicalName="
				+ locationPhysicalName + ", locationParent="
				+ locationParent + "]";
	}

}