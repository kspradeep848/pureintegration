package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the mas_work_flow database table.
 * 
 */
@Entity
@Table(name="mas_work_flow")
@NamedQuery(name="MasWorkFlowBO.findAll", query="SELECT m FROM MasWorkFlowBO m")
public class MasWorkFlowBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_work_flow_id")
	private int pkWorkFlowId;

	@Column(name="action_flag")
	private String actionFlag;

	@Column(name="fk_module_name_id")
	private int fkModuleNameId;

	@Column(name="work_flow_description")
	private String workFlowDescription;

	@Column(name="work_flow_key_name")
	private String workFlowKeyName;

	@OneToOne()
	@JoinColumn(name = "fk_module_name_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasModuleNameBO masModuleName;
	
	public MasWorkFlowBO() {
	}

	public int getPkWorkFlowId() {
		return this.pkWorkFlowId;
	}

	public void setPkWorkFlowId(int pkWorkFlowId) {
		this.pkWorkFlowId = pkWorkFlowId;
	}

	public String getActionFlag() {
		return this.actionFlag;
	}

	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}

	public int getFkModuleNameId() {
		return this.fkModuleNameId;
	}

	public void setFkModuleNameId(int fkModuleNameId) {
		this.fkModuleNameId = fkModuleNameId;
	}

	public String getWorkFlowDescription() {
		return this.workFlowDescription;
	}

	public void setWorkFlowDescription(String workFlowDescription) {
		this.workFlowDescription = workFlowDescription;
	}

	public String getWorkFlowKeyName() {
		return this.workFlowKeyName;
	}

	public void setWorkFlowKeyName(String workFlowKeyName) {
		this.workFlowKeyName = workFlowKeyName;
	}

	@Override
	public String toString()
	{
		return "MasWorkFlowBO [pkWorkFlowId=" + pkWorkFlowId
				+ ", actionFlag=" + actionFlag + ", fkModuleNameId="
				+ fkModuleNameId + ", workFlowDescription="
				+ workFlowDescription + ", workFlowKeyName="
				+ workFlowKeyName + ", masModuleName=" + masModuleName
				+ "]";
	}

}