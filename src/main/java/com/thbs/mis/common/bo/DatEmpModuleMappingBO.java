package com.thbs.mis.common.bo;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the dat_emp_module_mapping database table.
 * 
*/
@Entity
@Table(name="dat_emp_module_mapping")
@NamedQuery(name="DatEmpModuleMappingBO.findAll", query="SELECT d FROM DatEmpModuleMappingBO d")
public class DatEmpModuleMappingBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_dat_emp_module_mapping")
	private Integer pkDatEmpModuleMapping;

	@Column(name="fk_employee_id")
	private Integer fkEmployeeId;

	@Column(name="first_module")
	private short firstModule;
	
	@Column(name="second_module")
	private short secondModule;
	
	@Column(name="third_module")
	private short thirdModule;

	@Column(name="forth_module")
	private short fourthModule;
	
	@Column(name="fifth_module")
	private short fifthModule;
	
	@OneToOne
	@JoinColumn(name="first_module", unique = true, nullable = true, insertable = false, updatable = false)
	private MasMenuBO firstMasMenuBO;

	//bi-directional many-to-one association to MasModuleName
	@OneToOne
	@JoinColumn(name="second_module", unique = true, nullable = true, insertable = false, updatable = false)
	private MasMenuBO secondMasMenuBO;

	//bi-directional many-to-one association to MasModuleName
	@OneToOne
	@JoinColumn(name="third_module", unique = true, nullable = true, insertable = false, updatable = false)
	private MasMenuBO thirdMasMenuBO;

	//bi-directional many-to-one association to MasModuleName
	@OneToOne
	@JoinColumn(name="forth_module", unique = true, nullable = true, insertable = false, updatable = false)
	private MasMenuBO fourthMasMenuBO;

	//bi-directional many-to-one association to MasModuleName
	@OneToOne
	@JoinColumn(name="fifth_module", unique = true, nullable = true, insertable = false, updatable = false)
	private MasMenuBO fifthMasMenuBO;
	
	@OneToOne
	@JoinColumn(name="fk_employee_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO;

	public DatEmpModuleMappingBO() {
	}

	public Integer getPkDatEmpModuleMapping() {
		return pkDatEmpModuleMapping;
	}

	public void setPkDatEmpModuleMapping(Integer pkDatEmpModuleMapping) {
		this.pkDatEmpModuleMapping = pkDatEmpModuleMapping;
	}

	public Integer getFkEmployeeId() {
		return fkEmployeeId;
	}

	public void setFkEmployeeId(Integer fkEmployeeId) {
		this.fkEmployeeId = fkEmployeeId;
	}

	public short getFirstModule() {
		return firstModule;
	}

	public void setFirstModule(short firstModule) {
		this.firstModule = firstModule;
	}

	public short getSecondModule() {
		return secondModule;
	}

	public void setSecondModule(short secondModule) {
		this.secondModule = secondModule;
	}

	public short getThirdModule() {
		return thirdModule;
	}

	public void setThirdModule(short thirdModule) {
		this.thirdModule = thirdModule;
	}

	public short getFourthModule() {
		return fourthModule;
	}

	public void setFourthModule(short fourthModule) {
		this.fourthModule = fourthModule;
	}

	public short getFifthModule() {
		return fifthModule;
	}

	public void setFifthModule(short fifthModule) {
		this.fifthModule = fifthModule;
	}

	public MasMenuBO getFirstMasMenuBO() {
		return firstMasMenuBO;
	}

	public void setFirstMasMenuBO(MasMenuBO firstMasMenuBO) {
		this.firstMasMenuBO = firstMasMenuBO;
	}

	public MasMenuBO getSecondMasMenuBO() {
		return secondMasMenuBO;
	}

	public void setSecondMasMenuBO(MasMenuBO secondMasMenuBO) {
		this.secondMasMenuBO = secondMasMenuBO;
	}

	public MasMenuBO getThirdMasMenuBO() {
		return thirdMasMenuBO;
	}

	public void setThirdMasMenuBO(MasMenuBO thirdMasMenuBO) {
		this.thirdMasMenuBO = thirdMasMenuBO;
	}

	public MasMenuBO getFourthMasMenuBO() {
		return fourthMasMenuBO;
	}

	public void setFourthMasMenuBO(MasMenuBO fourthMasMenuBO) {
		this.fourthMasMenuBO = fourthMasMenuBO;
	}

	public MasMenuBO getFifthMasMenuBO() {
		return fifthMasMenuBO;
	}

	public void setFifthMasMenuBO(MasMenuBO fifthMasMenuBO) {
		this.fifthMasMenuBO = fifthMasMenuBO;
	}

	public DatEmpDetailBO getDatEmpDetailBO() {
		return datEmpDetailBO;
	}

	public void setDatEmpDetailBO(DatEmpDetailBO datEmpDetailBO) {
		this.datEmpDetailBO = datEmpDetailBO;
	}

	@Override
	public String toString() {
		return "DatEmpModuleMappingBO [pkDatEmpModuleMapping="
				+ pkDatEmpModuleMapping + ", fkEmployeeId=" + fkEmployeeId
				+ ", firstModule=" + firstModule + ", secondModule="
				+ secondModule + ", thirdModule=" + thirdModule
				+ ", fourthModule=" + fourthModule + ", fifthModule="
				+ fifthModule + ", firstMasMenuBO=" + firstMasMenuBO
				+ ", secondMasMenuBO=" + secondMasMenuBO + ", thirdMasMenuBO="
				+ thirdMasMenuBO + ", fourthMasMenuBO=" + fourthMasMenuBO
				+ ", fifthMasMenuBO=" + fifthMasMenuBO + ", datEmpDetailBO="
				+ datEmpDetailBO + "]";
	}
}