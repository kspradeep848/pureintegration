package com.thbs.mis.common.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mas_business_units database table.
 * 
 */
@Entity
@Table(name="mas_business_units")

public class BusinessUnitBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="bu_id", unique=true, nullable=false)
	private int buId;

	@Column(nullable=false)
	private byte active;

	@Column(name="bu_name", nullable=false, length=250)
	private String buName;

	@Column(length=250)
	private String comments;

	//bi-directional many-to-one association to EmployeeLoginBO
	@ManyToOne
	@JoinColumn(name="fk_bu_head", nullable=false)
	private EmployeeLoginBO datEmplogin1;

	//bi-directional many-to-one association to EmployeeLoginBO
	@ManyToOne
	@JoinColumn(name="fk_bu_hr_id")
	private EmployeeLoginBO datEmplogin2;

	public BusinessUnitBO() {
	}

	public int getBuId() {
		return this.buId;
	}

	public void setBuId(int buId) {
		this.buId = buId;
	}

	public byte getActive() {
		return this.active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public String getBuName() {
		return this.buName;
	}

	public void setBuName(String buName) {
		this.buName = buName;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public EmployeeLoginBO getDatEmplogin1() {
		return this.datEmplogin1;
	}

	public void setDatEmplogin1(EmployeeLoginBO datEmplogin1) {
		this.datEmplogin1 = datEmplogin1;
	}

	public EmployeeLoginBO getDatEmplogin2() {
		return this.datEmplogin2;
	}

	public void setDatEmplogin2(EmployeeLoginBO datEmplogin2) {
		this.datEmplogin2 = datEmplogin2;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BusinessUnitBO [buId=" + buId + ", active=" + active + ", buName=" + buName + ", comments=" + comments
				+ ", datEmplogin1=" + datEmplogin1 + ", datEmplogin2=" + datEmplogin2 + "]";
	}	
	

}