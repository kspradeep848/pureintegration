package com.thbs.mis.common.bo;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the mas_feedback_question_info database table.
 * 
 */
@Entity
@Table(name = "mas_feedback_question_info")
@NamedQuery(name = "MasFeedbackQuestionInfoBO.findAll", query = "SELECT m FROM MasFeedbackQuestionInfoBO m")
public class MasFeedbackQuestionInfoBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pk_feedback_question_info_id")
	private int pkFeedbackQuestionInfoId;

	@Column(name = "feedback_question_name")
	private String feedbackQuestionName;

	@Column(name = "feedback_question_status")
	private byte feedbackQuestionStatus;

	@Column(name = "feedback_question_type")
	private byte feedbackQuestionType;

	@Column(name = "fk_mas_work_flow_id")
	private short fkMasWorkFlowId;

	@Column(name = "feedback_question_order_no")
	private int feedbackQuestionOrder;

	public MasFeedbackQuestionInfoBO() {
	}

	public int getFeedbackQuestionOrder() {
		return feedbackQuestionOrder;
	}

	public void setFeedbackQuestionOrder(int feedbackQuestionOrder) {
		this.feedbackQuestionOrder = feedbackQuestionOrder;
	}

	public int getPkFeedbackQuestionInfoId() {
		return this.pkFeedbackQuestionInfoId;
	}

	public void setPkFeedbackQuestionInfoId(int pkFeedbackQuestionInfoId) {
		this.pkFeedbackQuestionInfoId = pkFeedbackQuestionInfoId;
	}

	public String getFeedbackQuestionName() {
		return this.feedbackQuestionName;
	}

	public void setFeedbackQuestionName(String feedbackQuestionName) {
		this.feedbackQuestionName = feedbackQuestionName;
	}

	public byte getFeedbackQuestionStatus() {
		return this.feedbackQuestionStatus;
	}

	public void setFeedbackQuestionStatus(byte feedbackQuestionStatus) {
		this.feedbackQuestionStatus = feedbackQuestionStatus;
	}

	public byte getFeedbackQuestionType() {
		return this.feedbackQuestionType;
	}

	public void setFeedbackQuestionType(byte feedbackQuestionType) {
		this.feedbackQuestionType = feedbackQuestionType;
	}

	public short getFkMasWorkFlowId() {
		return this.fkMasWorkFlowId;
	}

	public void setFkMasWorkFlowId(short fkMasWorkFlowId) {
		this.fkMasWorkFlowId = fkMasWorkFlowId;
	}
	
	@Override
	public String toString() {
		return "MasFeedbackQuestionInfoBO [pkFeedbackQuestionInfoId="
				+ pkFeedbackQuestionInfoId + ", feedbackQuestionName="
				+ feedbackQuestionName + ", feedbackQuestionStatus="
				+ feedbackQuestionStatus + ", feedbackQuestionType="
				+ feedbackQuestionType + ", fkMasWorkFlowId=" + fkMasWorkFlowId
				+ ", feedbackQuestionOrder=" + feedbackQuestionOrder + "]";
	}


}