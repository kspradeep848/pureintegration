package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the mas_organizations database table.
 * 
 */
@Entity
@Table(name="mas_organizations")

public class OrganizationBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_organization_idx", unique=true, nullable=false)
	private int pkOrganizationIdx;

	@Column(name="organization_desc", length=250)
	private String organizationDesc;

	@Column(name="organization_name", length=80)
	private String organizationName;

	

	public OrganizationBO() {
	}

	public int getPkOrganizationIdx() {
		return this.pkOrganizationIdx;
	}

	public void setPkOrganizationIdx(int pkOrganizationIdx) {
		this.pkOrganizationIdx = pkOrganizationIdx;
	}

	public String getOrganizationDesc() {
		return this.organizationDesc;
	}

	public void setOrganizationDesc(String organizationDesc) {
		this.organizationDesc = organizationDesc;
	}

	public String getOrganizationName() {
		return this.organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OrganizationBO [pkOrganizationIdx=" + pkOrganizationIdx + ", organizationDesc=" + organizationDesc
				+ ", organizationName=" + organizationName + "]";
	}

		
}