package com.thbs.mis.common.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mas_emp_main_status database table.
 * 
 */
@Entity
@Table(name="mas_emp_main_status")
@NamedQuery(name="MasEmpMainStatusBO.findAll", query="SELECT m FROM MasEmpMainStatusBO m")
public class MasEmpMainStatusBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_main_status_id")
	private byte pkEmpMainStatusId;

	@Column(name="emp_main_status_name")
	private String empMainStatusName;

	public MasEmpMainStatusBO() {
	}

	public byte getPkEmpMainStatusId() {
		return this.pkEmpMainStatusId;
	}

	public void setPkEmpMainStatusId(byte pkEmpMainStatusId) {
		this.pkEmpMainStatusId = pkEmpMainStatusId;
	}

	public String getEmpMainStatusName() {
		return this.empMainStatusName;
	}

	public void setEmpMainStatusName(String empMainStatusName) {
		this.empMainStatusName = empMainStatusName;
	}

	@Override
	public String toString()
	{
		return "MasEmpMainStatusBO [pkEmpMainStatusId="
				+ pkEmpMainStatusId + ", empMainStatusName="
				+ empMainStatusName + "]";
	}

}