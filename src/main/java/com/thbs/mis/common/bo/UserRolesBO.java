package com.thbs.mis.common.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "user_roles")
@NamedQuery(name = "UserRolesBO.findAll", query = "SELECT d FROM UserRolesBO d")
public class UserRolesBO {


	public UserRolesBO() {
		super();
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "pk_user_role_id")
	private Integer pkUserRoleId;

	@Column(name = "user_role_id")
	private Short roleId;
	
	@Column(name = "username")
	private Integer userName;
	
	@Column(name = "role")
	private String role;

	
	
	public Integer getPkUserRoleId() {
		return pkUserRoleId;
	}

	public void setPkUserRoleId(Integer pkUserRoleId) {
		this.pkUserRoleId = pkUserRoleId;
	}

	public Short getRoleId() {
		return roleId;
	}

	public void setRoleId(Short roleId) {
		this.roleId = roleId;
	}

	public Integer getUserName() {
		return userName;
	}

	public void setUserName(Integer userName) {
		this.userName = userName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((pkUserRoleId == null) ? 0 : pkUserRoleId.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		result = prime * result
				+ ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserRolesBO other = (UserRolesBO) obj;
		if (pkUserRoleId == null) {
			if (other.pkUserRoleId != null)
				return false;
		} else if (!pkUserRoleId.equals(other.pkUserRoleId))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserRolesBO [pkUserRoleId=" + pkUserRoleId + ", roleId="
				+ roleId + ", userName=" + userName + ", role=" + role + "]";
	}

	
	

	
	
}
