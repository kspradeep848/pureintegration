package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the mas_locations database table.
 * 
 */
@Entity
@Table(name="mas_locations")

public class LocationBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int pk_Location_Idx;

	@Column(length=250)
	private String location_LocAddress;

	@Column(nullable=false, length=80)
	private String location_LocName;

	

	public LocationBO() {
	}

	public int getPk_Location_Idx() {
		return this.pk_Location_Idx;
	}

	public void setPk_Location_Idx(int pk_Location_Idx) {
		this.pk_Location_Idx = pk_Location_Idx;
	}

	public String getLocation_LocAddress() {
		return this.location_LocAddress;
	}

	public void setLocation_LocAddress(String location_LocAddress) {
		this.location_LocAddress = location_LocAddress;
	}

	public String getLocation_LocName() {
		return this.location_LocName;
	}

	public void setLocation_LocName(String location_LocName) {
		this.location_LocName = location_LocName;
	}

	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LocationBO [pk_Location_Idx=" + pk_Location_Idx + ", location_LocAddress=" + location_LocAddress
				+ ", location_LocName=" + location_LocName +  "]";
	}
	
	

}