package com.thbs.mis.common.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mas_training_topic_name database table.
 * 
 */
@Entity
@Table(name="mas_training_topic_name")
@NamedQuery(name="MasTrainingTopicNameBO.findAll", query="SELECT m FROM MasTrainingTopicNameBO m")
public class MasTrainingTopicNameBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_training_topic_name_id")
	private int pkTrainingTopicNameId;

	@Column(name="training_topic_description")
	private String trainingTopicDescription;

	@Column(name="training_topic_name")
	private String trainingTopicName;

	public MasTrainingTopicNameBO() {
	}

	public int getPkTrainingTopicNameId() {
		return this.pkTrainingTopicNameId;
	}

	public void setPkTrainingTopicNameId(int pkTrainingTopicNameId) {
		this.pkTrainingTopicNameId = pkTrainingTopicNameId;
	}

	public String getTrainingTopicDescription() {
		return this.trainingTopicDescription;
	}

	public void setTrainingTopicDescription(String trainingTopicDescription) {
		this.trainingTopicDescription = trainingTopicDescription;
	}

	public String getTrainingTopicName() {
		return this.trainingTopicName;
	}

	public void setTrainingTopicName(String trainingTopicName) {
		this.trainingTopicName = trainingTopicName;
	}

	@Override
	public String toString()
	{
		return "MasTrainingTopicNameBO [pkTrainingTopicNameId="
				+ pkTrainingTopicNameId + ", trainingTopicDescription="
				+ trainingTopicDescription + ", trainingTopicName="
				+ trainingTopicName + "]";
	}

}