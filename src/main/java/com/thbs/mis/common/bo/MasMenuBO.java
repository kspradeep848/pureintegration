package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;



/**
 * The persistent class for the mas_menu database table.
 * 
 */
@Entity
@Table(name="mas_menu")
@NamedQuery(name="MasMenuBO.findAll", query="SELECT m FROM MasMenuBO m")
public class MasMenuBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3994213281473053673L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="pk_menu_id")
	private short pkMenuId;

	@Column(name="active_status")
	private String activeStatus;

	@Column(name="is_parent")
	private byte isParent;

	@Column(name="menu_description")
	private String menuDescription;

	@Column(name="menu_name")
	private String menuName;

	@Column(name="menu_parent_id")
	private int menuParentId;

	@Column(name="menu_url")
	private String menuUrl;
	
	@Column(name="fk_module_id")
	private short moduleId;
	
	@Column(name="mas_menu_order_id")
	private int menuOrderId;
	
	@Column(name="menu_key")
	private String menuKey;
	
	public MasMenuBO() {
	}

	public short getPkMenuId() {
		return pkMenuId;
	}

	public void setPkMenuId(short pkMenuId) {
		this.pkMenuId = pkMenuId;
	}

	public String getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}

	public byte getIsParent() {
		return isParent;
	}

	public void setIsParent(byte isParent) {
		this.isParent = isParent;
	}

	public String getMenuDescription() {
		return menuDescription;
	}

	public void setMenuDescription(String menuDescription) {
		this.menuDescription = menuDescription;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public int getMenuParentId() {
		return menuParentId;
	}

	public void setMenuParentId(int menuParentId) {
		this.menuParentId = menuParentId;
	}

	public String getMenuUrl() {
		return menuUrl;
	}

	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}


	public short getModuleId() {
		return moduleId;
	}


	public void setModuleId(short moduleId) {
		this.moduleId = moduleId;
	}	


	public int getMenuOrderId()
	{
		return menuOrderId;
	}

	public void setMenuOrderId(int menuOrderId)
	{
		this.menuOrderId = menuOrderId;
	}
	

	public String getMenuKey()
	{
		return menuKey;
	}

	public void setMenuKey(String menuKey)
	{
		this.menuKey = menuKey;
	}

	@Override
	public String toString()
	{
		return "MasMenuBO [pkMenuId=" + pkMenuId + ", activeStatus="
				+ activeStatus + ", isParent=" + isParent
				+ ", menuDescription=" + menuDescription
				+ ", menuName=" + menuName + ", menuParentId="
				+ menuParentId + ", menuUrl=" + menuUrl + ", moduleId="
				+ moduleId + ", menuOrderId=" + menuOrderId
				+ ", menuKey=" + menuKey + "]";
	}	
}
