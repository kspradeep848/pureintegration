package com.thbs.mis.common.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mas_boot_camp database table.
 * 
 */
@Entity
@Table(name="mas_boot_camp")
@NamedQuery(name="MasBootCampBO.findAll", query="SELECT m FROM MasBootCampBO m")
public class MasBootCampBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_boot_camp_id")
	private short pkBootCampId;

	@Column(name="boot_camp_name")
	private String bootCampName;

	@Column(name="boot_camp_year")
	private String bootCampYear;

	public MasBootCampBO() {
	}

	public short getPkBootCampId() {
		return this.pkBootCampId;
	}

	public void setPkBootCampId(short pkBootCampId) {
		this.pkBootCampId = pkBootCampId;
	}

	public String getBootCampName() {
		return this.bootCampName;
	}

	public void setBootCampName(String bootCampName) {
		this.bootCampName = bootCampName;
	}

	public String getBootCampYear() {
		return this.bootCampYear;
	}

	public void setBootCampYear(String bootCampYear) {
		this.bootCampYear = bootCampYear;
	}

	@Override
	public String toString()
	{
		return "MasBootCampBO [pkBootCampId=" + pkBootCampId
				+ ", bootCampName=" + bootCampName + ", bootCampYear="
				+ bootCampYear + "]";
	}

}