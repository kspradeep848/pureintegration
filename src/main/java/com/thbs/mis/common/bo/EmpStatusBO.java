package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the mas_emp_status database table.
 * 
 */
@Entity
@Table(name="mas_emp_status")

public class EmpStatusBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_empstatus_statusid", unique=true, nullable=false)
	private int pkEmpstatusStatusid;

	@Column(name="empstatus_status", nullable=false, length=30)
	private String empstatusStatus;

	

	public EmpStatusBO() {
	}

	public int getPkEmpstatusStatusid() {
		return this.pkEmpstatusStatusid;
	}

	public void setPkEmpstatusStatusid(int pkEmpstatusStatusid) {
		this.pkEmpstatusStatusid = pkEmpstatusStatusid;
	}

	public String getEmpstatusStatus() {
		return this.empstatusStatus;
	}

	public void setEmpstatusStatus(String empstatusStatus) {
		this.empstatusStatus = empstatusStatus;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmpStatusBO [pkEmpstatusStatusid=" + pkEmpstatusStatusid + ", empstatusStatus=" + empstatusStatus
				+  "]";
	}	

}