package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the mas_country database table.
 * 
 */
@Entity
@Table(name="mas_country")
@NamedQuery(name="MasCountryBO.findAll", query="SELECT m FROM MasCountryBO m")
public class MasCountryBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_country_id")
	private Short pkCountryId;

	@Column(name="country_name")
	private String countryName;
	
	@Column(name="country_code")
	private String countryCode;
	
	public MasCountryBO() {
	}

	public Short getPkCountryId() {
		return this.pkCountryId;
	}

	public void setPkCountryId(Short pkCountryId) {
		this.pkCountryId = pkCountryId;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public String toString() {
		return "MasCountryBO [pkCountryId=" + pkCountryId + ", countryName="
				+ countryName + ", countryCode=" + countryCode
			//	+ ", fkRegionId=" + fkRegionId + ", masRegionBo=" + masRegionBo
				+ "]";
	}	
}