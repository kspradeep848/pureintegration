package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the mas_business_entity database table.
 * 
 */
@Entity
@Table(name="mas_business_entity")
@NamedQuery(name="MasBusinessEntityBO.findAll", query="SELECT m FROM MasBusinessEntityBO m")
public class MasBusinessEntityBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_entity_id")
	private Byte pkEntityId;

	@Column(name="entity_name")
	private String entityName;

	public MasBusinessEntityBO() {
	}

	public Byte getPkEntityId() {
		return this.pkEntityId;
	}

	public void setPkEntityId(Byte pkEntityId) {
		this.pkEntityId = pkEntityId;
	}

	public String getEntityName() {
		return this.entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	@Override
	public String toString() {
		return "MasBusinessEntityBO [pkEntityId=" + pkEntityId
				+ ", entityName=" + entityName + "]";
	}
	

}