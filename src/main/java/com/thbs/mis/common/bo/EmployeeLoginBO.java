package com.thbs.mis.common.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the dat_emplogins database table.
 * 
 */
@Entity
@Table(name="dat_emplogins")

public class EmployeeLoginBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false, name="pk_EmpLogin_EmpID")
	private int pk_EmpLogin_EmpID;

	@Temporal(TemporalType.DATE)
	private Date contract_endDate;

	@Column(nullable=false, length=100)
	private String empLogin_EmpPassword;

	@Column(nullable=false)
	private byte empLogin_EmpStatus;

	@Column(name="emplogin_emptype", nullable=false)
	private int emploginEmptype;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="emppwd_updated_on", nullable=false)
	private Date emppwdUpdatedOn;

	//bi-directional many-to-one association to EmproleBO
	@ManyToOne
	@JoinColumn(name="fk_EmpLoginRole_EmpRoleID", nullable=false)
	private MasEmpRoleBO masEmprole;
	


	public EmployeeLoginBO() {
	}

	public int getPk_EmpLogin_EmpID() {
		return this.pk_EmpLogin_EmpID;
	}

	public void setPk_EmpLogin_EmpID(int pk_EmpLogin_EmpID) {
		this.pk_EmpLogin_EmpID = pk_EmpLogin_EmpID;
	}

	public Date getContract_endDate() {
		return this.contract_endDate;
	}

	public void setContract_endDate(Date contract_endDate) {
		this.contract_endDate = contract_endDate;
	}

	public String getEmpLogin_EmpPassword() {
		return this.empLogin_EmpPassword;
	}

	public void setEmpLogin_EmpPassword(String empLogin_EmpPassword) {
		this.empLogin_EmpPassword = empLogin_EmpPassword;
	}

	public byte getEmpLogin_EmpStatus() {
		return this.empLogin_EmpStatus;
	}

	public void setEmpLogin_EmpStatus(byte empLogin_EmpStatus) {
		this.empLogin_EmpStatus = empLogin_EmpStatus;
	}

	public int getEmploginEmptype() {
		return this.emploginEmptype;
	}

	public void setEmploginEmptype(int emploginEmptype) {
		this.emploginEmptype = emploginEmptype;
	}

	public Date getEmppwdUpdatedOn() {
		return this.emppwdUpdatedOn;
	}

	public void setEmppwdUpdatedOn(Date emppwdUpdatedOn) {
		this.emppwdUpdatedOn = emppwdUpdatedOn;
	}

	public MasEmpRoleBO getMasEmprole() {
		return this.masEmprole;
	}

	public void setMasEmprole(MasEmpRoleBO masEmprole) {
		this.masEmprole = masEmprole;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmployeeLoginBO [pk_EmpLogin_EmpID=" + pk_EmpLogin_EmpID + ", contract_endDate=" + contract_endDate
				+ ", empLogin_EmpPassword=" + empLogin_EmpPassword + ", empLogin_EmpStatus=" + empLogin_EmpStatus
				+ ", emploginEmptype=" + emploginEmptype + ", emppwdUpdatedOn=" + emppwdUpdatedOn + ", masEmprole="
				+ masEmprole + "]";
	}


}