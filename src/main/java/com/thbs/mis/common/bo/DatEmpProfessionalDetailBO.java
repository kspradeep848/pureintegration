package com.thbs.mis.common.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.thbs.mis.common.bean.EmployeeNameWithRMIdBean;

/**
 * The persistent class for the dat_emp_professional_detail database table.
 * 
 */
@Entity
@Table(name = "dat_emp_professional_detail")
@NamedQuery(name = "DatEmpProfessionalDetailBO.findAll", query = "SELECT d FROM DatEmpProfessionalDetailBO d")
// Added by Kamal Anand
@SqlResultSetMapping(name = "empDetailsMappings", classes = {
		@ConstructorResult(targetClass = EmployeeNameWithRMIdBean.class, columns = {
				@ColumnResult(name = "fk_main_emp_detail_id"), @ColumnResult(name = "emp_first_name"),
				@ColumnResult(name = "emp_last_name"), @ColumnResult(name = "fk_emp_reporting_mgr_id"),
				@ColumnResult(name = "has_reportees_flag"), @ColumnResult(name = "empFullNameWithId"),
				@ColumnResult(name = "emp_designation_name") }) })
@NamedNativeQueries({
		@NamedNativeQuery(name = "DatEmpProfessionalDetailBO.getRmAndSkipLevelRMInBU", query = "select b.fk_main_emp_detail_id,"
				+ "b.has_reportees_flag,b.emp_first_name,b.emp_last_name, b.fk_emp_reporting_mgr_id,b.empFullNameWithId,"
				+ "b.emp_designation_name from (select prof.fk_main_emp_detail_id,prof.has_reportees_flag,per.emp_first_name,per.emp_last_name,"
				+ " prof.fk_emp_reporting_mgr_id,concat(per.emp_first_name,' ',per.emp_last_name,'-',prof.fk_main_emp_detail_id)"
				+ " as empFullNameWithId, des.emp_designation_name  from dat_emp_professional_detail prof "
				+ " INNER JOIN dat_emp_personal_detail per on prof.fk_main_emp_detail_id = per.fk_emp_detail_id "
				+ " INNER JOIN dat_emp_detail det on prof.fk_main_emp_detail_id = det.pk_emp_id "
				+ " INNER JOIN mas_emp_designation des on des.pk_emp_designation_id = prof.fk_emp_designation where det.fk_emp_main_status = 1 "
				+ "and det.pk_emp_id not in(:empIdList) and prof.fk_main_emp_detail_id in "
				+ "(select fk_emp_reporting_mgr_id from dat_emp_professional_detail where fk_main_emp_detail_id = :empId)   "
				+ "UNION "
				+ "select prof1.fk_main_emp_detail_id,prof1.has_reportees_flag,per.emp_first_name,per.emp_last_name, prof1.fk_emp_reporting_mgr_id,"
				+ "concat(per.emp_first_name,' ',per.emp_last_name,'-',prof1.fk_main_emp_detail_id) as empFullNameWithId, "
				+ "des.emp_designation_name from dat_emp_professional_detail prof1 "
				+ " INNER JOIN dat_emp_personal_detail per on prof1.fk_main_emp_detail_id = per.fk_emp_detail_id "
				+ " INNER JOIN dat_emp_detail det on prof1.fk_main_emp_detail_id = det.pk_emp_id"
				+ " INNER JOIN mas_emp_designation des on des.pk_emp_designation_id = prof1.fk_emp_designation where det.fk_emp_main_status = 1 "
				+ "and det.pk_emp_id not in(:empIdList) and prof1.fk_main_emp_detail_id = "
				+ "(select fk_emp_reporting_mgr_id from dat_emp_professional_detail where fk_main_emp_detail_id = "
				+ "(select fk_emp_reporting_mgr_id from dat_emp_professional_detail where fk_main_emp_detail_id = :empId)) "
				+ "UNION "
				+ "select prof2.fk_main_emp_detail_id,prof2.has_reportees_flag,per.emp_first_name,per.emp_last_name, prof2.fk_emp_reporting_mgr_id,"
				+ "concat(per.emp_first_name,' ',per.emp_last_name,'-',prof2.fk_main_emp_detail_id) as empFullNameWithId, "
				+ "des.emp_designation_name  from dat_emp_professional_detail prof2  " + "INNER JOIN "
				+ "dat_emp_personal_detail per on prof2.fk_main_emp_detail_id = per.fk_emp_detail_id " + "INNER JOIN "
				+ "dat_emp_detail det on prof2.fk_main_emp_detail_id = det.pk_emp_id "
				+ "INNER JOIN mas_emp_designation des on des.pk_emp_designation_id = prof2.fk_emp_designation where det.fk_emp_main_status = 1 "
				+ "and det.pk_emp_id not in(:empIdList) and prof2.fk_emp_bu_unit = (select fk_emp_bu_unit from dat_emp_professional_detail "
				+ "where fk_main_emp_detail_id = :empId) and has_reportees_flag = 'YES_HAS_REPORTEES') as b", resultSetMapping = "empDetailsMappings") })
// End of Addition by Kamal Anand
public class DatEmpProfessionalDetailBO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_emp_professional_detail_id")
	private Integer pkEmpProfessionalDetailId;

	@Temporal(TemporalType.DATE)
	@Column(name = "effective_date_of_reporting_mgr_change")
	private Date effectiveDateOfReportingMgrChange;

	@Column(name = "emp_confirmation_graduate_flag")
	private Byte empConfirmationGraduateFlag;

	@Temporal(TemporalType.DATE)
	@Column(name = "emp_contract_end_date")
	private Date empContractEndDate;

	@Column(name = "emp_cost_unit")
	private Float empCostUnit;

	@Temporal(TemporalType.DATE)
	@Column(name = "emp_date_of_joining")
	private Date empDateOfJoining;

	@Column(name = "emp_extension_number")
	private String empExtensionNumber;

	@Column(name = "emp_on_bench_flag")
	private Byte empOnBenchFlag;

	@Column(name = "emp_phone_number")
	private String empPhoneNumber;

	@Column(name = "emp_previous_experience")
	private Float empPreviousExperience;

	@Column(name = "emp_professional_email_id")
	private String empProfessionalEmailId;

	@Column(name = "fk_emp_boot_camp_id")
	private Short fkEmpBootCampId;

	@Column(name = "fk_emp_bu_unit")
	private Short fkEmpBuUnit;

	@Column(name = "fk_emp_competence_mgr_id")
	private Integer fkEmpCompetenceMgrId;

	@Column(name = "fk_emp_designation")
	private Short fkEmpDesignation;

	@Column(name = "fk_emp_domain_mgr_id")
	private Integer fkEmpDomainMgrId;

	@Column(name = "fk_emp_level")
	private Short fkEmpLevel;

	@Column(name = "fk_emp_location_parent_id")
	private Short fkEmpLocationParentId;

	@Column(name = "fk_emp_location_physical_id")
	private Short fkEmpLocationPhysicalId;

	@Column(name = "fk_emp_organization_id")
	private Short fkEmpOrganizationId;

	@Column(name = "fk_emp_reporting_mgr_id")
	private Integer fkEmpReportingMgrId;

	@Column(name = "fk_main_emp_detail_id")
	private Integer fkMainEmpDetailId;

	@Column(name = "professional_detail_modified_by")
	private Integer professionalDetailModifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date professionalDetailModifiedDate;

	@Column(name = "has_reportees_flag")
	private String hasReporteesFlag;

	@Column(name = "consultant_reference_emp_id")
	private Integer consultantReferenceEmpId;

	@Column(name = "onsite_or_offshore")
	private String OnsiteOrOffshore;

	@Column(name = "emp_loc")
	private Integer EmpLoc;

	@Temporal(TemporalType.DATE)
	@Column(name = "emp_date_of_confirmation")
	private Date empDateOfConfirmation;

	public Date getEmpDateOfConfirmation() {
		return empDateOfConfirmation;
	}

	public void setEmpDateOfConfirmation(Date empDateOfConfirmation) {
		this.empDateOfConfirmation = empDateOfConfirmation;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "emp_last_working_date")
	private Date empLastWorkingDate;

	@Column(name = "probation_months")
	private Integer probationMonths;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "probation_updated_on")
	private Date probationUpdatedOn;

	@Column(name = "probation_updated_by")
	private Integer probationUpdatedBy;
	
	@Column(name = "tupe_status")
	private String tupeStatus;
	
	@Column(name = "tupe_organisation")
	private String tupeOrganisation;

	@OneToOne
	@JoinColumn(name = "fk_emp_boot_camp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasBootCampBO masBootCampBO;

	@OneToOne
	@JoinColumn(name = "fk_emp_bu_unit", unique = true, nullable = true, insertable = false, updatable = false)
	private MasBuUnitBO masBuUnitBO;

	@OneToOne
	@JoinColumn(name = "fk_emp_competence_mgr_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO_cm_id;

	@OneToOne
	@JoinColumn(name = "fk_emp_designation", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpDesignationBO masEmpDesignationBO;

	@OneToOne
	@JoinColumn(name = "fk_emp_domain_mgr_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO_dm_id;

	@OneToOne
	@JoinColumn(name = "fk_emp_level", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpLevelBO masEmpLevelBO;

	@OneToOne
	@JoinColumn(name = "fk_emp_location_parent_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasLocationParentBO masLocationParentBO;

	@OneToOne
	@JoinColumn(name = "fk_emp_location_physical_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasLocationPhysicalBO masLocationPhysicalBO;

	@OneToOne
	@JoinColumn(name = "fk_emp_organization_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasOrganizationBO masOrganizationBO;

	@OneToOne
	@JoinColumn(name = "fk_emp_reporting_mgr_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO DatEmpDetailBO_rm_id;

	@OneToOne
	@JoinColumn(name = "fk_main_emp_detail_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO DatEmpDetailBO_main_emp_id;

	@OneToOne
	@JoinColumn(name = "professional_detail_modified_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO DatEmpDetailBO_prof_detail_modified_by;

	public DatEmpProfessionalDetailBO(Integer fkMainEmpDetailId, Integer fkEmpReportingMgrId, String hasReporteesFlag,
			Short fkEmpBuUnit, String empProfessionalEmailId) {
		super();

		this.fkMainEmpDetailId = fkMainEmpDetailId;
		this.fkEmpReportingMgrId = fkEmpReportingMgrId;

		this.hasReporteesFlag = hasReporteesFlag;
		this.fkEmpBuUnit = fkEmpBuUnit;
		this.empProfessionalEmailId = empProfessionalEmailId;
	}

	// Added by Shyam for Gsr Hierarchy Structure under Hr login
	@Transient
	String empFirstName;
	@Transient
	String empLastName;
	@Transient
	String businessUnitName;
	@Transient
	Short empRoleId;

	@Transient
	Byte empType;
	@Transient
	Short pkBuUnitId;
	@Transient
	String buUnitName;
	@Transient
	short pkBootCampId;
	@Transient
	String bootCampName;
	@Transient
	private String empMiddleName;
	@Transient
	private String empDesignationName;
	@Transient
	private String locationParentName;
	@Transient
	private String empMobileNo;
	@Transient
	private String empAlternateMobileNo;
	@Transient
	private Byte fkEmpMainStatus;

	public DatEmpProfessionalDetailBO(Integer fkMainEmpDetailId, String empFirstName, String empMiddleName,
			String empLastName, String empProfessionalEmailId, String buUnitName, String empDesignationName,
			String locationParentName, String empMobileNo, String empAlternateMobileNo, Byte fkEmpMainStatus,
			String empExtensionNumber) {
		super();
		this.fkMainEmpDetailId = fkMainEmpDetailId;
		this.empFirstName = empFirstName;
		this.empMiddleName = empMiddleName;
		this.empLastName = empLastName;
		this.empProfessionalEmailId = empProfessionalEmailId;
		this.buUnitName = buUnitName;
		this.empDesignationName = empDesignationName;
		this.locationParentName = locationParentName;
		this.empMobileNo = empMobileNo;
		this.empAlternateMobileNo = empAlternateMobileNo;
		this.fkEmpMainStatus = fkEmpMainStatus;
		this.empExtensionNumber = empExtensionNumber;
	}

	public String getOnsiteOrOffshore() {
		return OnsiteOrOffshore;
	}

	public void setOnsiteOrOffshore(String onsiteOrOffshore) {
		OnsiteOrOffshore = onsiteOrOffshore;
	}

	public Integer getEmpLoc() {
		return EmpLoc;
	}

	public void setEmpLoc(Integer empLoc) {
		EmpLoc = empLoc;
	}

	public Date getProbationUpdatedOn() {
		return probationUpdatedOn;
	}

	public void setProbationUpdatedOn(Date probationUpdatedOn) {
		this.probationUpdatedOn = probationUpdatedOn;
	}

	public Integer getProbationUpdatedBy() {
		return probationUpdatedBy;
	}

	public void setProbationUpdatedBy(Integer probationUpdatedBy) {
		this.probationUpdatedBy = probationUpdatedBy;
	}

	public Byte getEmpType() {
		return empType;
	}

	public void setEmpType(Byte empType) {
		this.empType = empType;
	}

	public Short getPkBuUnitId() {
		return pkBuUnitId;
	}

	public void setPkBuUnitId(Short pkBuUnitId) {
		this.pkBuUnitId = pkBuUnitId;
	}

	public String getBuUnitName() {
		return buUnitName;
	}

	public void setBuUnitName(String buUnitName) {
		this.buUnitName = buUnitName;
	}

	public short getPkBootCampId() {
		return pkBootCampId;
	}

	public void setPkBootCampId(short pkBootCampId) {
		this.pkBootCampId = pkBootCampId;
	}

	public String getBootCampName() {
		return bootCampName;
	}

	public void setBootCampName(String bootCampName) {
		this.bootCampName = bootCampName;
	}

	public String getEmpMiddleName() {
		return empMiddleName;
	}

	public void setEmpMiddleName(String empMiddleName) {
		this.empMiddleName = empMiddleName;
	}

	public String getEmpDesignationName() {
		return empDesignationName;
	}

	public void setEmpDesignationName(String empDesignationName) {
		this.empDesignationName = empDesignationName;
	}

	public String getLocationParentName() {
		return locationParentName;
	}

	public void setLocationParentName(String locationParentName) {
		this.locationParentName = locationParentName;
	}

	public String getEmpMobileNo() {
		return empMobileNo;
	}

	public void setEmpMobileNo(String empMobileNo) {
		this.empMobileNo = empMobileNo;
	}

	public String getEmpAlternateMobileNo() {
		return empAlternateMobileNo;
	}

	public void setEmpAlternateMobileNo(String empAlternateMobileNo) {
		this.empAlternateMobileNo = empAlternateMobileNo;
	}

	public Byte getFkEmpMainStatus() {
		return fkEmpMainStatus;
	}

	public void setFkEmpMainStatus(Byte fkEmpMainStatus) {
		this.fkEmpMainStatus = fkEmpMainStatus;
	}

	public DatEmpProfessionalDetailBO(Integer fkMainEmpDetailId, String empFirstName, String empMiddleName,
			String empLastName, Byte empType, Short pkBuUnitId, String buUnitName, short pkBootCampId,
			String bootCampName, Integer fkEmpReportingMgrId) {
		super();
		this.fkMainEmpDetailId = fkMainEmpDetailId;
		this.empFirstName = empFirstName;
		this.empMiddleName = empMiddleName;
		this.empLastName = empLastName;
		this.empType = empType;
		this.pkBuUnitId = pkBuUnitId;
		this.buUnitName = buUnitName;
		this.pkBootCampId = pkBootCampId;
		this.bootCampName = bootCampName;
		this.fkEmpReportingMgrId = fkEmpReportingMgrId;
	}

	public DatEmpProfessionalDetailBO(Integer fkMainEmpDetailId, String empFirstName, String empLastName,
			Short businessUnitId, String buName, Integer fkEmpReportingMgrId, String hasReporteesFlag,
			Short empRoleId) {
		this.fkMainEmpDetailId = fkMainEmpDetailId;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.fkEmpBuUnit = businessUnitId;
		this.businessUnitName = buName;
		this.fkEmpReportingMgrId = fkEmpReportingMgrId;
		this.hasReporteesFlag = hasReporteesFlag;
		this.empRoleId = empRoleId;
	}

	// End of addition by Shyam for Gsr Hierarchy Structure under Hr login

	public Date getEmpLastWorkingDate() {
		return empLastWorkingDate;
	}

	public Integer getProbationMonths() {
		return probationMonths;
	}

	public void setProbationMonths(Integer probationMonths) {
		this.probationMonths = probationMonths;
	}

	public void setEmpLastWorkingDate(Date empLastWorkingDate) {
		this.empLastWorkingDate = empLastWorkingDate;
	}

	public Integer getConsultantReferenceEmpId() {
		return consultantReferenceEmpId;
	}

	public void setConsultantReferenceEmpId(Integer consultantReferenceEmpId) {
		this.consultantReferenceEmpId = consultantReferenceEmpId;
	}

	public String getHasReporteesFlag() {
		return hasReporteesFlag;
	}

	public void setHasReporteesFlag(String hasReporteesFlag) {
		this.hasReporteesFlag = hasReporteesFlag;
	}

	public Date getProfessionalDetailModifiedDate() {
		return professionalDetailModifiedDate;
	}

	public void setProfessionalDetailModifiedDate(Date professionalDetailModifiedDate) {
		this.professionalDetailModifiedDate = professionalDetailModifiedDate;
	}

	public DatEmpProfessionalDetailBO() {
	}

	public Integer getProfessionalDetailModifiedBy() {
		return professionalDetailModifiedBy;
	}

	public void setProfessionalDetailModifiedBy(Integer professionalDetailModifiedBy) {
		this.professionalDetailModifiedBy = professionalDetailModifiedBy;
	}

	public MasBootCampBO getMasBootCampBO() {
		return masBootCampBO;
	}

	public void setMasBootCampBO(MasBootCampBO masBootCampBO) {
		this.masBootCampBO = masBootCampBO;
	}

	public MasBuUnitBO getMasBuUnitBO() {
		return masBuUnitBO;
	}

	public void setMasBuUnitBO(MasBuUnitBO masBuUnitBO) {
		this.masBuUnitBO = masBuUnitBO;
	}

	public DatEmpDetailBO getDatEmpDetailBO_cm_id() {
		return datEmpDetailBO_cm_id;
	}

	public void setDatEmpDetailBO_cm_id(DatEmpDetailBO datEmpDetailBO_cm_id) {
		this.datEmpDetailBO_cm_id = datEmpDetailBO_cm_id;
	}

	public MasEmpDesignationBO getMasEmpDesignationBO() {
		return masEmpDesignationBO;
	}

	public void setMasEmpDesignationBO(MasEmpDesignationBO masEmpDesignationBO) {
		this.masEmpDesignationBO = masEmpDesignationBO;
	}

	public DatEmpDetailBO getDatEmpDetailBO_dm_id() {
		return datEmpDetailBO_dm_id;
	}

	public void setDatEmpDetailBO_dm_id(DatEmpDetailBO datEmpDetailBO_dm_id) {
		this.datEmpDetailBO_dm_id = datEmpDetailBO_dm_id;
	}

	public MasEmpLevelBO getMasEmpLevelBO() {
		return masEmpLevelBO;
	}

	public void setMasEmpLevelBO(MasEmpLevelBO masEmpLevelBO) {
		this.masEmpLevelBO = masEmpLevelBO;
	}

	public MasLocationParentBO getMasLocationParentBO() {
		return masLocationParentBO;
	}

	public void setMasLocationParentBO(MasLocationParentBO masLocationParentBO) {
		this.masLocationParentBO = masLocationParentBO;
	}

	public MasLocationPhysicalBO getMasLocationPhysicalBO() {
		return masLocationPhysicalBO;
	}

	public void setMasLocationPhysicalBO(MasLocationPhysicalBO masLocationPhysicalBO) {
		this.masLocationPhysicalBO = masLocationPhysicalBO;
	}

	public MasOrganizationBO getMasOrganizationBO() {
		return masOrganizationBO;
	}

	public void setMasOrganizationBO(MasOrganizationBO masOrganizationBO) {
		this.masOrganizationBO = masOrganizationBO;
	}

	public DatEmpDetailBO getDatEmpDetailBO_rm_id() {
		return DatEmpDetailBO_rm_id;
	}

	public void setDatEmpDetailBO_rm_id(DatEmpDetailBO datEmpDetailBO_rm_id) {
		DatEmpDetailBO_rm_id = datEmpDetailBO_rm_id;
	}

	public DatEmpDetailBO getDatEmpDetailBO_main_emp_id() {
		return DatEmpDetailBO_main_emp_id;
	}

	public void setDatEmpDetailBO_main_emp_id(DatEmpDetailBO datEmpDetailBO_main_emp_id) {
		DatEmpDetailBO_main_emp_id = datEmpDetailBO_main_emp_id;
	}

	public DatEmpDetailBO getDatEmpDetailBO_prof_detail_modified_by() {
		return DatEmpDetailBO_prof_detail_modified_by;
	}

	public void setDatEmpDetailBO_prof_detail_modified_by(DatEmpDetailBO datEmpDetailBO_prof_detail_modified_by) {
		DatEmpDetailBO_prof_detail_modified_by = datEmpDetailBO_prof_detail_modified_by;
	}

	public Integer getPkEmpProfessionalDetailId() {
		return this.pkEmpProfessionalDetailId;
	}

	public void setPkEmpProfessionalDetailId(Integer pkEmpProfessionalDetailId) {
		this.pkEmpProfessionalDetailId = pkEmpProfessionalDetailId;
	}

	public Date getEffectiveDateOfReportingMgrChange() {
		return this.effectiveDateOfReportingMgrChange;
	}

	public void setEffectiveDateOfReportingMgrChange(Date effectiveDateOfReportingMgrChange) {
		this.effectiveDateOfReportingMgrChange = effectiveDateOfReportingMgrChange;
	}

	public Byte getEmpConfirmationGraduateFlag() {
		return this.empConfirmationGraduateFlag;
	}

	public void setEmpConfirmationGraduateFlag(Byte empConfirmationGraduateFlag) {
		this.empConfirmationGraduateFlag = empConfirmationGraduateFlag;
	}

	public Date getEmpContractEndDate() {
		return this.empContractEndDate;
	}

	public void setEmpContractEndDate(Date empContractEndDate) {
		this.empContractEndDate = empContractEndDate;
	}

	public Float getEmpCostUnit() {
		return this.empCostUnit;
	}

	public void setEmpCostUnit(Float empCostUnit) {
		this.empCostUnit = empCostUnit;
	}

	public Date getEmpDateOfJoining() {
		return this.empDateOfJoining;
	}

	public void setEmpDateOfJoining(Date empDateOfJoining) {
		this.empDateOfJoining = empDateOfJoining;
	}

	public String getEmpExtensionNumber() {
		return this.empExtensionNumber;
	}

	public void setEmpExtensionNumber(String empExtensionNumber) {
		this.empExtensionNumber = empExtensionNumber;
	}

	public Byte getEmpOnBenchFlag() {
		return this.empOnBenchFlag;
	}

	public void setEmpOnBenchFlag(Byte empOnBenchFlag) {
		this.empOnBenchFlag = empOnBenchFlag;
	}

	public String getEmpPhoneNumber() {
		return this.empPhoneNumber;
	}

	public void setEmpPhoneNumber(String empPhoneNumber) {
		this.empPhoneNumber = empPhoneNumber;
	}

	public Float getEmpPreviousExperience() {
		return this.empPreviousExperience;
	}

	public void setEmpPreviousExperience(Float empPreviousExperience) {
		this.empPreviousExperience = empPreviousExperience;
	}

	public String getEmpProfessionalEmailId() {
		return this.empProfessionalEmailId;
	}

	public void setEmpProfessionalEmailId(String empProfessionalEmailId) {
		this.empProfessionalEmailId = empProfessionalEmailId;
	}

	public Short getFkEmpBootCampId() {
		return this.fkEmpBootCampId;
	}

	public void setFkEmpBootCampId(Short fkEmpBootCampId) {
		this.fkEmpBootCampId = fkEmpBootCampId;
	}

	public Short getFkEmpBuUnit() {
		return this.fkEmpBuUnit;
	}

	public void setFkEmpBuUnit(Short fkEmpBuUnit) {
		this.fkEmpBuUnit = fkEmpBuUnit;
	}

	public Integer getFkEmpCompetenceMgrId() {
		return this.fkEmpCompetenceMgrId;
	}

	public void setFkEmpCompetenceMgrId(Integer fkEmpCompetenceMgrId) {
		this.fkEmpCompetenceMgrId = fkEmpCompetenceMgrId;
	}

	public Short getFkEmpDesignation() {
		return this.fkEmpDesignation;
	}

	public void setFkEmpDesignation(Short fkEmpDesignation) {
		this.fkEmpDesignation = fkEmpDesignation;
	}

	public Integer getFkEmpDomainMgrId() {
		return this.fkEmpDomainMgrId;
	}

	public void setFkEmpDomainMgrId(Integer fkEmpDomainMgrId) {
		this.fkEmpDomainMgrId = fkEmpDomainMgrId;
	}

	public Short getFkEmpLevel() {
		return this.fkEmpLevel;
	}

	public void setFkEmpLevel(Short fkEmpLevel) {
		this.fkEmpLevel = fkEmpLevel;
	}

	public Short getFkEmpLocationParentId() {
		return this.fkEmpLocationParentId;
	}

	public void setFkEmpLocationParentId(Short fkEmpLocationParentId) {
		this.fkEmpLocationParentId = fkEmpLocationParentId;
	}

	public Short getFkEmpLocationPhysicalId() {
		return this.fkEmpLocationPhysicalId;
	}

	public void setFkEmpLocationPhysicalId(Short fkEmpLocationPhysicalId) {
		this.fkEmpLocationPhysicalId = fkEmpLocationPhysicalId;
	}

	public Short getFkEmpOrganizationId() {
		return this.fkEmpOrganizationId;
	}

	public void setFkEmpOrganizationId(Short fkEmpOrganizationId) {
		this.fkEmpOrganizationId = fkEmpOrganizationId;
	}

	public Integer getFkEmpReportingMgrId() {
		return this.fkEmpReportingMgrId;
	}

	public void setFkEmpReportingMgrId(Integer fkEmpReportingMgrId) {
		this.fkEmpReportingMgrId = fkEmpReportingMgrId;
	}

	public Integer getFkMainEmpDetailId() {
		return this.fkMainEmpDetailId;
	}

	public void setFkMainEmpDetailId(Integer fkMainEmpDetailId) {
		this.fkMainEmpDetailId = fkMainEmpDetailId;
	}

	public String getEmpFirstName() {
		return empFirstName;
	}

	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}

	public String getEmpLastName() {
		return empLastName;
	}

	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}

	public String getBusinessUnitName() {
		return businessUnitName;
	}

	public void setBusinessUnitName(String businessUnitName) {
		this.businessUnitName = businessUnitName;
	}

	public Short getEmpRoleId() {
		return empRoleId;
	}

	public void setEmpRoleId(Short empRoleId) {
		this.empRoleId = empRoleId;
	}

	
	public String getTupeStatus() {
		return tupeStatus;
	}

	public void setTupeStatus(String tupeStatus) {
		this.tupeStatus = tupeStatus;
	}

	public String getTupeOrganisation() {
		return tupeOrganisation;
	}

	public void setTupeOrganisation(String tupeOrganisation) {
		this.tupeOrganisation = tupeOrganisation;
	}

	@Override
	public String toString() {
		return "DatEmpProfessionalDetailBO [pkEmpProfessionalDetailId="
				+ pkEmpProfessionalDetailId
				+ ", effectiveDateOfReportingMgrChange="
				+ effectiveDateOfReportingMgrChange
				+ ", empConfirmationGraduateFlag="
				+ empConfirmationGraduateFlag + ", empContractEndDate="
				+ empContractEndDate + ", empCostUnit=" + empCostUnit
				+ ", empDateOfJoining=" + empDateOfJoining
				+ ", empExtensionNumber=" + empExtensionNumber
				+ ", empOnBenchFlag=" + empOnBenchFlag + ", empPhoneNumber="
				+ empPhoneNumber + ", empPreviousExperience="
				+ empPreviousExperience + ", empProfessionalEmailId="
				+ empProfessionalEmailId + ", fkEmpBootCampId="
				+ fkEmpBootCampId + ", fkEmpBuUnit=" + fkEmpBuUnit
				+ ", fkEmpCompetenceMgrId=" + fkEmpCompetenceMgrId
				+ ", fkEmpDesignation=" + fkEmpDesignation
				+ ", fkEmpDomainMgrId=" + fkEmpDomainMgrId + ", fkEmpLevel="
				+ fkEmpLevel + ", fkEmpLocationParentId="
				+ fkEmpLocationParentId + ", fkEmpLocationPhysicalId="
				+ fkEmpLocationPhysicalId + ", fkEmpOrganizationId="
				+ fkEmpOrganizationId + ", fkEmpReportingMgrId="
				+ fkEmpReportingMgrId + ", fkMainEmpDetailId="
				+ fkMainEmpDetailId + ", professionalDetailModifiedBy="
				+ professionalDetailModifiedBy
				+ ", professionalDetailModifiedDate="
				+ professionalDetailModifiedDate + ", hasReporteesFlag="
				+ hasReporteesFlag + ", consultantReferenceEmpId="
				+ consultantReferenceEmpId + ", OnsiteOrOffshore="
				+ OnsiteOrOffshore + ", EmpLoc=" + EmpLoc
				+ ", empDateOfConfirmation=" + empDateOfConfirmation
				+ ", empLastWorkingDate=" + empLastWorkingDate
				+ ", probationMonths=" + probationMonths
				+ ", probationUpdatedOn=" + probationUpdatedOn
				+ ", probationUpdatedBy=" + probationUpdatedBy
				+ ", tupeStatus=" + tupeStatus + ", tupeOrganisation="
				+ tupeOrganisation + ", masBootCampBO=" + masBootCampBO
				+ ", masBuUnitBO=" + masBuUnitBO + ", datEmpDetailBO_cm_id="
				+ datEmpDetailBO_cm_id + ", masEmpDesignationBO="
				+ masEmpDesignationBO + ", datEmpDetailBO_dm_id="
				+ datEmpDetailBO_dm_id + ", masEmpLevelBO=" + masEmpLevelBO
				+ ", masLocationParentBO=" + masLocationParentBO
				+ ", masLocationPhysicalBO=" + masLocationPhysicalBO
				+ ", masOrganizationBO=" + masOrganizationBO
				+ ", DatEmpDetailBO_rm_id=" + DatEmpDetailBO_rm_id
				+ ", DatEmpDetailBO_main_emp_id=" + DatEmpDetailBO_main_emp_id
				+ ", DatEmpDetailBO_prof_detail_modified_by="
				+ DatEmpDetailBO_prof_detail_modified_by + ", empFirstName="
				+ empFirstName + ", empLastName=" + empLastName
				+ ", businessUnitName=" + businessUnitName + ", empRoleId="
				+ empRoleId + ", empType=" + empType + ", pkBuUnitId="
				+ pkBuUnitId + ", buUnitName=" + buUnitName + ", pkBootCampId="
				+ pkBootCampId + ", bootCampName=" + bootCampName
				+ ", empMiddleName=" + empMiddleName + ", empDesignationName="
				+ empDesignationName + ", locationParentName="
				+ locationParentName + ", empMobileNo=" + empMobileNo
				+ ", empAlternateMobileNo=" + empAlternateMobileNo
				+ ", fkEmpMainStatus=" + fkEmpMainStatus + "]";
	}

	

}
