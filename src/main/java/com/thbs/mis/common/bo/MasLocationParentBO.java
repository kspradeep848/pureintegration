package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.thbs.mis.common.bo.MasCountryBO;


/**
 * The persistent class for the mas_location_parent database table.
 * 
 */
@Entity
@Table(name="mas_location_parent")
@NamedQuery(name="MasLocationParentBO.findAll", query="SELECT m FROM MasLocationParentBO m")
public class MasLocationParentBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_location_parent_id")
	private short pkLocationParentId;

	@Column(name="fk_country_id")
	private short fkCountryId;

	@Column(name="location_parent_name")
	private String locationParentName;

	@OneToOne()
	@JoinColumn(name = "fk_country_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasCountryBO countryBO;
	
	
	public MasLocationParentBO() {
	}

	public short getPkLocationParentId() {
		return this.pkLocationParentId;
	}

	public void setPkLocationParentId(short pkLocationParentId) {
		this.pkLocationParentId = pkLocationParentId;
	}

	public short getFkCountryId() {
		return this.fkCountryId;
	}

	public void setFkCountryId(short fkCountryId) {
		this.fkCountryId = fkCountryId;
	}

	public String getLocationParentName() {
		return this.locationParentName;
	}

	public void setLocationParentName(String locationParentName) {
		this.locationParentName = locationParentName;
	}

	@Override
	public String toString()
	{
		return "MasLocationParentBO [pkLocationParentId="
				+ pkLocationParentId + ", fkCountryId=" + fkCountryId
				+ ", locationParentName=" + locationParentName
				+ ", countryBO=" + countryBO + "]";
	}

}