package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the proj_mas_account database table.
 * 
 */
@Entity
@Table(name="mas_account")
@NamedQuery(name="MasAccountBO.findAll", query="SELECT p FROM MasAccountBO p")
public class MasAccountBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_account_id")
	private short pkAccountId;

	@Column(name="account_name")
	private String accountName;

	public MasAccountBO() {
	}

	public short getPkAccountId() {
		return this.pkAccountId;
	}

	public void setPkAccountId(short pkAccountId) {
		this.pkAccountId = pkAccountId;
	}

	public String getAccountName() {
		return this.accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	@Override
	public String toString() {
		return "MasAccountBO [pkAccountId=" + pkAccountId + ", accountName="
				+ accountName + "]";
	}

	

}