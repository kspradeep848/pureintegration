/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  LevelBO.java                                		 */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  27-Jun-2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 27-Jun-2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the mas_emp_status database table.
 * 
 */
@Entity
@Table(name="mas_emp_level")
public class LevelBO  implements Serializable 
{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_level_id", unique=true, nullable=false)
	private int pkEmplevelLevelid;

	@Column(name="emp_level_name", nullable=true, length=228)
	private String empLevelName;

	/**
	 * <Description getPkEmplevelLevelid:> TODO.
	 * @return pkEmplevelLevelid
	 */
	public int getPkEmplevelLevelid()
	{
		return pkEmplevelLevelid;
	}

	/**
	 * <Description setPkEmplevelLevelid:> TODO.
	 * @param pkEmplevelLevelid
	 */
	public void setPkEmplevelLevelid(int pkEmplevelLevelid)
	{
		this.pkEmplevelLevelid = pkEmplevelLevelid;
	}

	/**
	 * <Description getEmpLevelName:> 
	 * @return empLevelName
	 */

	public String getEmpLevelName() {
		return empLevelName;
	}
	
	/**
	 * <Description getEmpLevelName:>
	 * @param empLevelName
	 */
	public void setEmpLevelName(String empLevelName) {
		this.empLevelName = empLevelName;
	}

	/**
	 * <Description toString:> TODO.
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "LevelBO [pkEmplevelLevelid=" + pkEmplevelLevelid
				+ ", empLevelName=" + empLevelName + "]";
	}

}
