package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the mas_regions database table.
 * 
 */
@Entity
@Table(name="mas_regions")
@NamedQuery(name="MasRegionBO.findAll", query="SELECT m FROM MasRegionBO m")
public class MasRegionBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_region_id")
	private short pkRegionId;

	@Column(name="region_name")
	private String regionName;
	
	public MasRegionBO() {
	}

	public short getPkRegionId() {
		return this.pkRegionId;
	}

	public void setPkRegionId(short pkRegionId) {
		this.pkRegionId = pkRegionId;
	}

	public String getRegionName() {
		return this.regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	@Override
	public String toString() {
		return "MasRegionBO [pkRegionId=" + pkRegionId + ", regionName="
				+ regionName + "]";
	}

	
}