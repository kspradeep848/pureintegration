package com.thbs.mis.common.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mas_emp_role database table.
 * 
 */
@Entity
@Table(name="mas_emp_role")
@NamedQuery(name="MasEmpRoleBO.findAll", query="SELECT m FROM MasEmpRoleBO m")
public class MasEmpRoleBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_role_id")
	private short pkEmpRoleId;

	@Column(name="emp_role_name")
	private String empRoleName;
	
	@Column(name="emp_role_key_name")
	private String empRoleKeyName;

	public MasEmpRoleBO() {
	}
	
	public MasEmpRoleBO(short pkEmpRoleId, String empRoleName,
			String empRoleKeyName) {
		super();
		this.pkEmpRoleId = pkEmpRoleId;
		this.empRoleName = empRoleName;
		this.empRoleKeyName = empRoleKeyName;
	}
	
	public short getPkEmpRoleId() {
		return this.pkEmpRoleId;
	}

	public void setPkEmpRoleId(short pkEmpRoleId) {
		this.pkEmpRoleId = pkEmpRoleId;
	}

	public String getEmpRoleName() {
		return this.empRoleName;
	}

	public void setEmpRoleName(String empRoleName) {
		this.empRoleName = empRoleName;
	}	
	

	public String getEmpRoleKeyName()
	{
		return empRoleKeyName;
	}

	public void setEmpRoleKeyName(String empRoleKeyName)
	{
		this.empRoleKeyName = empRoleKeyName;
	}

	@Override
	public String toString()
	{
		return "MasEmpRoleBO [pkEmpRoleId=" + pkEmpRoleId
				+ ", empRoleName=" + empRoleName + ", empRoleKeyName="
				+ empRoleKeyName + "]";
	}	

}