package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the dat_emprecords database table.
 * 
 */
@Entity
@Table(name = "dat_emprecords")

public class EmployeeInfoBO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	

	/**
	 * <Description EmployeeInfoBO:> TODO
	 * @param pk_EmpRec_Idx
	 * @param empRec_EmpFName
	 * @param empRec_EmpLName
	 * @param pk_EmpLogin_EmpID
	 * @param datEmplogin3
	 */
	public EmployeeInfoBO(Integer pk_EmpRec_Idx, String empRec_EmpFName,String empRec_EmpLName,
			Integer pk_EmpLogin_EmpID,EmployeeLoginBO datEmplogin3)
	{
		super();
		this.pk_EmpRec_Idx = pk_EmpRec_Idx;
		this.empRec_EmpFName = empRec_EmpFName;
		this.empRec_EmpLName = empRec_EmpLName;
		this.datEmplogin1 = new EmployeeLoginBO();
		datEmplogin1.setPk_EmpLogin_EmpID(pk_EmpLogin_EmpID);
		this.datEmplogin3 = datEmplogin3;
		}

	
	/**
	 * <Description EmployeeInfoBO:> TODO
	 * @param pk_EmpRec_Idx
	 * @param empRec_EmpFName
	 * @param empRec_EmpLName
	 * @param pk_EmpLogin_EmpID
	 */
	public EmployeeInfoBO(Integer pk_EmpRec_Idx, String empRec_EmpFName,String empRec_EmpLName,
			Integer pk_EmpLogin_EmpID)
	{
		super();
		this.pk_EmpRec_Idx = pk_EmpRec_Idx;
		this.empRec_EmpFName = empRec_EmpFName;
		this.empRec_EmpLName = empRec_EmpLName;
		this.datEmplogin1 = new EmployeeLoginBO();
		datEmplogin1.setPk_EmpLogin_EmpID(pk_EmpLogin_EmpID);
		}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pk_EmpRec_Idx;

	@Column(nullable = false)
	private Integer citizenship1;

	@Column(nullable = false)
	private Integer citizenship2;

	@Column(nullable = false)
	private Integer citizenshipstatus;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "effective_date_of_rm_change")
	private Date effectiveDateOfRmChange;

	@Column(name = "emprec_consultant", length = 255)
	private String emprecConsultant;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "emprec_dob", nullable = false)
	private Date emprecDob;

	@Column(name = "emprec_empbloodgroup", length = 255)
	private String emprecEmpbloodgroup;

	@Column(name = "emprec_empcomptncymgrid")
	private Integer emprecEmpcomptncymgrid;

	private float empRec_EmpCostUnit;

	@Column(nullable = false, length = 80)
	private String empRec_EmpDesg;

	@Temporal(TemporalType.TIMESTAMP)
	private Date empRec_EmpDOJ;

	@Column(name = "emprec_empdomainmgr")
	private Integer emprecEmpdomainmgr;

	@Column(nullable = false, length = 80)
	private String empRec_EmpEmail;

	@Column(nullable = false)
	private float empRec_EmpExperience;

	@Column(name = "emprec_empextn", length = 255)
	private String emprecEmpextn;

	@Column(nullable = false, length = 30)
	private String empRec_EmpFName;

	@Column(length = 30)
	private String empRec_EmpInitials;

	@Column(nullable = false, length = 30)
	private String empRec_EmpLevel;

	@Column(length = 30)
	private String empRec_EmpLName;

	@Column(length = 30)
	private String empRec_EmpMName;

	@Column(name = "emprec_empmobileno", length = 255)
	private String emprecEmpmobileno;

	@Column(nullable = false)
	private byte empRec_EmpOnBenchFlag;

	@Column(name = "emprec_emppermanentaddr", length = 255)
	private String emprecEmppermanentaddr;

	@Column(nullable = false, length = 30)
	private String empRec_EmpPhone;

	@Column(name = "emprec_emppresentaddr", length = 255)
	private String emprecEmppresentaddr;

	@Column(nullable = false, length = 80)
	private String empRec_EmpQual;

	@Column(name = "emprec_empreportingmgrid")
	private Integer emprecEmpreportingmgrid;

	private Integer empRec_EmpThbsProfileID;

	@Column(name = "emprec_gender", length = 10)
	private String emprecGender;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "emprec_lastwrkday", nullable = false)
	private Date emprecLastwrkday;

	private Integer emprec_updatedBy;

	@Column(name = "fk_empsubstatus_substatusid", nullable = false)
	private Integer fkEmpsubstatusSubstatusid;

	@Column(name = "fk_mas_bu_units", nullable = false, insertable=false, updatable=false )
	private Integer fkMasBuUnits;

	@Column(name = "fk_new_empstatus_id", nullable = false)
	private Integer fkNewEmpstatusId;

	@Column(name = "is_pip", nullable = false)
	private byte isPip;

	private byte isgraduated;

	@Column(name = "personal_email", length = 100)
	private String personalEmail;

	@Temporal(TemporalType.TIMESTAMP)
	private Date spouseDOB;

	@Column(name = "fk_EmpRec_EmpID", nullable = false, insertable=false, updatable=false )
	private Integer empId;
	
	// bi-directional many-to-one association to EmployeeLoginBO
	@ManyToOne
	@JoinColumn(name = "fk_EmpRec_EmpID", nullable = false)
	private EmployeeLoginBO datEmplogin1;

	// bi-directional many-to-one association to EmployeeLoginBO
	@ManyToOne
	@JoinColumn(name = "fk_emprec_empcomptncymgrid")
	private EmployeeLoginBO datEmplogin2;

	// bi-directional many-to-one association to EmployeeLoginBO
	@ManyToOne
	@JoinColumn(name = "fk_emprec_empreportingmgrid")
	private EmployeeLoginBO datEmplogin3;

	// bi-directional many-to-one association to LmsMasLocationBO
	@ManyToOne
	@JoinColumn(name = "emprec_parentLocationId")
	private LmsMasLocationBO lmsMasLocation;

	// bi-directional many-to-one association to LmsMasLocationBO
	@ManyToOne
	@JoinColumn(name = "fk_mas_bu_units")
	private MasBuUnitBO masUnitBO;
	/*// bi-directional many-to-one association to BootcampsBO
	@ManyToOne
	@JoinColumn(name = "fk_emprec_bootcampid")	
	
	private BootcampsBO masBootcamp;*/

	// bi-directional many-to-one association to EmpStatusBO
	@ManyToOne
	@JoinColumn(name = "fk_empstatus_statusid", nullable = false)
	private MasEmpMainStatusBO masEmpStatus;

	// bi-directional many-to-one association to LocationBO
	@ManyToOne
	@JoinColumn(name = "fk_EmpRec_EmpLocationID", nullable = false)
	private LocationBO masLocation;

	// bi-directional many-to-one association to OrganizationBO
	@ManyToOne
	@JoinColumn(name = "fk_emprec_organizationid")
	private MasOrganizationBO masOrganization;

	public EmployeeInfoBO() {
	}

	public Integer getPk_EmpRec_Idx() {
		return this.pk_EmpRec_Idx;
	}

	public void setPk_EmpRec_Idx(Integer pk_EmpRec_Idx) {
		this.pk_EmpRec_Idx = pk_EmpRec_Idx;
	}

	public Integer getCitizenship1() {
		return this.citizenship1;
	}

	public void setCitizenship1(Integer citizenship1) {
		this.citizenship1 = citizenship1;
	}

	public Integer getCitizenship2() {
		return this.citizenship2;
	}

	public void setCitizenship2(Integer citizenship2) {
		this.citizenship2 = citizenship2;
	}

	public Integer getCitizenshipstatus() {
		return this.citizenshipstatus;
	}

	public void setCitizenshipstatus(Integer citizenshipstatus) {
		this.citizenshipstatus = citizenshipstatus;
	}

	public Date getEffectiveDateOfRmChange() {
		return this.effectiveDateOfRmChange;
	}

	public void setEffectiveDateOfRmChange(Date effectiveDateOfRmChange) {
		this.effectiveDateOfRmChange = effectiveDateOfRmChange;
	}

	public String getEmprecConsultant() {
		return this.emprecConsultant;
	}

	public void setEmprecConsultant(String emprecConsultant) {
		this.emprecConsultant = emprecConsultant;
	}

	public Date getEmprecDob() {
		return this.emprecDob;
	}

	public void setEmprecDob(Date emprecDob) {
		this.emprecDob = emprecDob;
	}

	public String getEmprecEmpbloodgroup() {
		return this.emprecEmpbloodgroup;
	}

	public void setEmprecEmpbloodgroup(String emprecEmpbloodgroup) {
		this.emprecEmpbloodgroup = emprecEmpbloodgroup;
	}

	public Integer getEmprecEmpcomptncymgrid() {
		return this.emprecEmpcomptncymgrid;
	}

	public void setEmprecEmpcomptncymgrid(Integer emprecEmpcomptncymgrid) {
		this.emprecEmpcomptncymgrid = emprecEmpcomptncymgrid;
	}

	public float getEmpRec_EmpCostUnit() {
		return this.empRec_EmpCostUnit;
	}

	public void setEmpRec_EmpCostUnit(float empRec_EmpCostUnit) {
		this.empRec_EmpCostUnit = empRec_EmpCostUnit;
	}

	public String getEmpRec_EmpDesg() {
		return this.empRec_EmpDesg;
	}

	public void setEmpRec_EmpDesg(String empRec_EmpDesg) {
		this.empRec_EmpDesg = empRec_EmpDesg;
	}

	public Date getEmpRec_EmpDOJ() {
		return this.empRec_EmpDOJ;
	}

	public void setEmpRec_EmpDOJ(Date empRec_EmpDOJ) {
		this.empRec_EmpDOJ = empRec_EmpDOJ;
	}

	public Integer getEmprecEmpdomainmgr() {
		return this.emprecEmpdomainmgr;
	}

	public void setEmprecEmpdomainmgr(Integer emprecEmpdomainmgr) {
		this.emprecEmpdomainmgr = emprecEmpdomainmgr;
	}

	public String getEmpRec_EmpEmail() {
		return this.empRec_EmpEmail;
	}

	public void setEmpRec_EmpEmail(String empRec_EmpEmail) {
		this.empRec_EmpEmail = empRec_EmpEmail;
	}

	public float getEmpRec_EmpExperience() {
		return this.empRec_EmpExperience;
	}

	public void setEmpRec_EmpExperience(float empRec_EmpExperience) {
		this.empRec_EmpExperience = empRec_EmpExperience;
	}

	public String getEmprecEmpextn() {
		return this.emprecEmpextn;
	}

	public void setEmprecEmpextn(String emprecEmpextn) {
		this.emprecEmpextn = emprecEmpextn;
	}

	public String getEmpRec_EmpFName() {
		return this.empRec_EmpFName;
	}

	public void setEmpRec_EmpFName(String empRec_EmpFName) {
		this.empRec_EmpFName = empRec_EmpFName;
	}

	public String getEmpRec_EmpInitials() {
		return this.empRec_EmpInitials;
	}

	public void setEmpRec_EmpInitials(String empRec_EmpInitials) {
		this.empRec_EmpInitials = empRec_EmpInitials;
	}

	public String getEmpRec_EmpLevel() {
		return this.empRec_EmpLevel;
	}

	public void setEmpRec_EmpLevel(String empRec_EmpLevel) {
		this.empRec_EmpLevel = empRec_EmpLevel;
	}

	public String getEmpRec_EmpLName() {
		return this.empRec_EmpLName;
	}

	public void setEmpRec_EmpLName(String empRec_EmpLName) {
		this.empRec_EmpLName = empRec_EmpLName;
	}

	public String getEmpRec_EmpMName() {
		return this.empRec_EmpMName;
	}

	public void setEmpRec_EmpMName(String empRec_EmpMName) {
		this.empRec_EmpMName = empRec_EmpMName;
	}

	public String getEmprecEmpmobileno() {
		return this.emprecEmpmobileno;
	}

	public void setEmprecEmpmobileno(String emprecEmpmobileno) {
		this.emprecEmpmobileno = emprecEmpmobileno;
	}

	public byte getEmpRec_EmpOnBenchFlag() {
		return this.empRec_EmpOnBenchFlag;
	}

	public void setEmpRec_EmpOnBenchFlag(byte empRec_EmpOnBenchFlag) {
		this.empRec_EmpOnBenchFlag = empRec_EmpOnBenchFlag;
	}

	public String getEmprecEmppermanentaddr() {
		return this.emprecEmppermanentaddr;
	}

	public void setEmprecEmppermanentaddr(String emprecEmppermanentaddr) {
		this.emprecEmppermanentaddr = emprecEmppermanentaddr;
	}

	public String getEmpRec_EmpPhone() {
		return this.empRec_EmpPhone;
	}

	public void setEmpRec_EmpPhone(String empRec_EmpPhone) {
		this.empRec_EmpPhone = empRec_EmpPhone;
	}

	public String getEmprecEmppresentaddr() {
		return this.emprecEmppresentaddr;
	}

	public void setEmprecEmppresentaddr(String emprecEmppresentaddr) {
		this.emprecEmppresentaddr = emprecEmppresentaddr;
	}

	public String getEmpRec_EmpQual() {
		return this.empRec_EmpQual;
	}

	public void setEmpRec_EmpQual(String empRec_EmpQual) {
		this.empRec_EmpQual = empRec_EmpQual;
	}

	public Integer getEmprecEmpreportingmgrid() {
		return this.emprecEmpreportingmgrid;
	}

	public void setEmprecEmpreportingmgrid(Integer emprecEmpreportingmgrid) {
		this.emprecEmpreportingmgrid = emprecEmpreportingmgrid;
	}

	public Integer getEmpRec_EmpThbsProfileID() {
		return this.empRec_EmpThbsProfileID;
	}

	public void setEmpRec_EmpThbsProfileID(Integer empRec_EmpThbsProfileID) {
		this.empRec_EmpThbsProfileID = empRec_EmpThbsProfileID;
	}

	public String getEmprecGender() {
		return this.emprecGender;
	}

	public void setEmprecGender(String emprecGender) {
		this.emprecGender = emprecGender;
	}

	public Date getEmprecLastwrkday() {
		return this.emprecLastwrkday;
	}

	public void setEmprecLastwrkday(Date emprecLastwrkday) {
		this.emprecLastwrkday = emprecLastwrkday;
	}

	public Integer getEmprec_updatedBy() {
		return this.emprec_updatedBy;
	}

	public void setEmprec_updatedBy(Integer emprec_updatedBy) {
		this.emprec_updatedBy = emprec_updatedBy;
	}

	public Integer getFkEmpsubstatusSubstatusid() {
		return this.fkEmpsubstatusSubstatusid;
	}

	public void setFkEmpsubstatusSubstatusid(Integer fkEmpsubstatusSubstatusid) {
		this.fkEmpsubstatusSubstatusid = fkEmpsubstatusSubstatusid;
	}

	public Integer getFkMasBuUnits() {
		return this.fkMasBuUnits;
	}

	public void setFkMasBuUnits(Integer fkMasBuUnits) {
		this.fkMasBuUnits = fkMasBuUnits;
	}

	public Integer getFkNewEmpstatusId() {
		return this.fkNewEmpstatusId;
	}

	public void setFkNewEmpstatusId(Integer fkNewEmpstatusId) {
		this.fkNewEmpstatusId = fkNewEmpstatusId;
	}

	public byte getIsPip() {
		return this.isPip;
	}

	public void setIsPip(byte isPip) {
		this.isPip = isPip;
	}

	public byte getIsgraduated() {
		return this.isgraduated;
	}

	public void setIsgraduated(byte isgraduated) {
		this.isgraduated = isgraduated;
	}

	public String getPersonalEmail() {
		return this.personalEmail;
	}

	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	public Date getSpouseDOB() {
		return this.spouseDOB;
	}

	public void setSpouseDOB(Date spouseDOB) {
		this.spouseDOB = spouseDOB;
	}

	public EmployeeLoginBO getDatEmplogin1() {
		return this.datEmplogin1;
	}

	public void setDatEmplogin1(EmployeeLoginBO datEmplogin1) {
		this.datEmplogin1 = datEmplogin1;
	}

	public EmployeeLoginBO getDatEmplogin2() {
		return this.datEmplogin2;
	}

	public void setDatEmplogin2(EmployeeLoginBO datEmplogin2) {
		this.datEmplogin2 = datEmplogin2;
	}

	public EmployeeLoginBO getDatEmplogin3() {
		return this.datEmplogin3;
	}

	public void setDatEmplogin3(EmployeeLoginBO datEmplogin3) {
		this.datEmplogin3 = datEmplogin3;
	}

	public LmsMasLocationBO getLmsMasLocation() {
		return this.lmsMasLocation;
	}

	public void setLmsMasLocation(LmsMasLocationBO lmsMasLocation) {
		this.lmsMasLocation = lmsMasLocation;
	}

	/*public BootcampsBO getMasBootcamp() {
		return this.masBootcamp;
	}

	public void setMasBootcamp(BootcampsBO masBootcamp) {
		this.masBootcamp = masBootcamp;
	}*/

	public MasEmpMainStatusBO getMasEmpStatus() {
		return this.masEmpStatus;
	}

	public void setMasEmpStatus(MasEmpMainStatusBO masEmpStatus) {
		this.masEmpStatus = masEmpStatus;
	}

	public LocationBO getMasLocation() {
		return this.masLocation;
	}

	public void setMasLocation(LocationBO masLocation) {
		this.masLocation = masLocation;
	}

	public MasOrganizationBO getMasOrganization() {
		return this.masOrganization;
	}

	public void setMasOrganization(MasOrganizationBO masOrganization) {
		this.masOrganization = masOrganization;
	}



	public MasBuUnitBO getMasUnitBO()
	{
		return masUnitBO;
	}


	public void setMasUnitBO(MasBuUnitBO masUnitBO)
	{
		this.masUnitBO = masUnitBO;
	}


	@Override
	public String toString()
	{
		return "EmployeeInfoBO [pk_EmpRec_Idx=" + pk_EmpRec_Idx
				+ ", citizenship1=" + citizenship1 + ", citizenship2="
				+ citizenship2 + ", citizenshipstatus="
				+ citizenshipstatus + ", effectiveDateOfRmChange="
				+ effectiveDateOfRmChange + ", emprecConsultant="
				+ emprecConsultant + ", emprecDob=" + emprecDob
				+ ", emprecEmpbloodgroup=" + emprecEmpbloodgroup
				+ ", emprecEmpcomptncymgrid=" + emprecEmpcomptncymgrid
				+ ", empRec_EmpCostUnit=" + empRec_EmpCostUnit
				+ ", empRec_EmpDesg=" + empRec_EmpDesg
				+ ", empRec_EmpDOJ=" + empRec_EmpDOJ
				+ ", emprecEmpdomainmgr=" + emprecEmpdomainmgr
				+ ", empRec_EmpEmail=" + empRec_EmpEmail
				+ ", empRec_EmpExperience=" + empRec_EmpExperience
				+ ", emprecEmpextn=" + emprecEmpextn
				+ ", empRec_EmpFName=" + empRec_EmpFName
				+ ", empRec_EmpInitials=" + empRec_EmpInitials
				+ ", empRec_EmpLevel=" + empRec_EmpLevel
				+ ", empRec_EmpLName=" + empRec_EmpLName
				+ ", empRec_EmpMName=" + empRec_EmpMName
				+ ", emprecEmpmobileno=" + emprecEmpmobileno
				+ ", empRec_EmpOnBenchFlag=" + empRec_EmpOnBenchFlag
				+ ", emprecEmppermanentaddr=" + emprecEmppermanentaddr
				+ ", empRec_EmpPhone=" + empRec_EmpPhone
				+ ", emprecEmppresentaddr=" + emprecEmppresentaddr
				+ ", empRec_EmpQual=" + empRec_EmpQual
				+ ", emprecEmpreportingmgrid="
				+ emprecEmpreportingmgrid
				+ ", empRec_EmpThbsProfileID="
				+ empRec_EmpThbsProfileID + ", emprecGender="
				+ emprecGender + ", emprecLastwrkday="
				+ emprecLastwrkday + ", emprec_updatedBy="
				+ emprec_updatedBy + ", fkEmpsubstatusSubstatusid="
				+ fkEmpsubstatusSubstatusid + ", fkMasBuUnits="
				+ fkMasBuUnits + ", fkNewEmpstatusId="
				+ fkNewEmpstatusId + ", isPip=" + isPip
				+ ", isgraduated=" + isgraduated + ", personalEmail="
				+ personalEmail + ", spouseDOB=" + spouseDOB
				+ ", empId=" + empId + ", datEmplogin1=" + datEmplogin1
				+ ", datEmplogin2=" + datEmplogin2 + ", datEmplogin3="
				+ datEmplogin3 + ", lmsMasLocation=" + lmsMasLocation
				+ ", masUnitBO=" + masUnitBO + ", masEmpStatus="
				+ masEmpStatus + ", masLocation=" + masLocation
				+ ", masOrganization=" + masOrganization + "]";
	}

}