package com.thbs.mis.common.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

@Entity
@Table(name = "dat_forex_conversions")

public class DatForexConversionsBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_forex_conversions_id", unique = true, nullable = false)
	private Integer pkForexConversionsId;

	@Column(name = "forex_from_date")
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date forexFromDate;

	@Column(name = "forex_to_date")
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date forexToDate;

	@Column(name = "forex_source")
	private String forexSource;

	@Column(name = "forex_entered_on")
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date forexEnteredOn;

	@Column(name = "forex_exchange_rate")
	private Float forexExchangeRate;

	@Column(name = "fk_forex_entered_by_emp_id")
	private Integer fkForexEnteredByEmpId;

	@Column(name = "fk_forex_currency_type_id")
	private Byte fkForexCurrencyTypeId;

	@Column(name = "is_updated")
	private Integer isUpdated;

	@OneToOne
	@JoinColumn(name = "fk_forex_currency_type_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasCurrencytypeBO masCurrencytypeBO;

	@OneToOne
	@JoinColumn(name = "fk_forex_entered_by_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO;

	public Integer getPkForexConversionsId() {
		return pkForexConversionsId;
	}

	public void setPkForexConversionsId(Integer pkForexConversionsId) {
		this.pkForexConversionsId = pkForexConversionsId;
	}

	public Date getForexFromDate() {
		return forexFromDate;
	}

	public void setForexFromDate(Date forexFromDate) {
		this.forexFromDate = forexFromDate;
	}

	public Date getForexToDate() {
		return forexToDate;
	}

	public void setForexToDate(Date forexToDate) {
		this.forexToDate = forexToDate;
	}

	public String getForexSource() {
		return forexSource;
	}

	public void setForexSource(String forexSource) {
		this.forexSource = forexSource;
	}

	public Date getForexEnteredOn() {
		return forexEnteredOn;
	}

	public void setForexEnteredOn(Date forexEnteredOn) {
		this.forexEnteredOn = forexEnteredOn;
	}

	public Float getForexExchangeRate() {
		return forexExchangeRate;
	}

	public void setForexExchangeRate(Float forexExchangeRate) {
		this.forexExchangeRate = forexExchangeRate;
	}

	public Integer getFkForexEnteredByEmpId() {
		return fkForexEnteredByEmpId;
	}

	public void setFkForexEnteredByEmpId(Integer fkForexEnteredByEmpId) {
		this.fkForexEnteredByEmpId = fkForexEnteredByEmpId;
	}

	public Byte getFkForexCurrencyTypeId() {
		return fkForexCurrencyTypeId;
	}

	public void setFkForexCurrencyTypeId(Byte fkForexCurrencyTypeId) {
		this.fkForexCurrencyTypeId = fkForexCurrencyTypeId;
	}

	public Integer getIsUpdated() {
		return isUpdated;
	}

	public void setIsUpdated(Integer isUpdated) {
		this.isUpdated = isUpdated;
	}

	public MasCurrencytypeBO getMasCurrencytypeBO() {
		return masCurrencytypeBO;
	}

	public void setMasCurrencytypeBO(MasCurrencytypeBO masCurrencytypeBO) {
		this.masCurrencytypeBO = masCurrencytypeBO;
	}

	public DatEmpDetailBO getDatEmpDetailBO() {
		return datEmpDetailBO;
	}

	public void setDatEmpDetailBO(DatEmpDetailBO datEmpDetailBO) {
		this.datEmpDetailBO = datEmpDetailBO;
	}

	@Override
	public String toString() {
		return "DatForexConversionsBO [pkForexConversionsId=" + pkForexConversionsId + ", forexFromDate="
				+ forexFromDate + ", forexToDate=" + forexToDate + ", forexSource=" + forexSource + ", forexEnteredOn="
				+ forexEnteredOn + ", forexExchangeRate=" + forexExchangeRate + ", fkForexEnteredByEmpId="
				+ fkForexEnteredByEmpId + ", fkForexCurrencyTypeId=" + fkForexCurrencyTypeId + ", isUpdated="
				+ isUpdated + ", masCurrencytypeBO=" + masCurrencytypeBO + ", datEmpDetailBO=" + datEmpDetailBO + "]";
	}
}
