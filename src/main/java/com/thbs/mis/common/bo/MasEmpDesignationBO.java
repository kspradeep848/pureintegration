package com.thbs.mis.common.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mas_emp_designation database table.
 * 
 */
@Entity
@Table(name="mas_emp_designation")
@NamedQuery(name="MasEmpDesignationBO.findAll", query="SELECT m FROM MasEmpDesignationBO m")
public class MasEmpDesignationBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_designation_id")
	private short pkEmpDesignationId;

	@Column(name="emp_designation_name")
	private String empDesignationName;

	public MasEmpDesignationBO() {
	}

	public short getPkEmpDesignationId() {
		return this.pkEmpDesignationId;
	}

	public void setPkEmpDesignationId(short pkEmpDesignationId) {
		this.pkEmpDesignationId = pkEmpDesignationId;
	}

	public String getEmpDesignationName() {
		return this.empDesignationName;
	}

	public void setEmpDesignationName(String empDesignationName) {
		this.empDesignationName = empDesignationName;
	}

	@Override
	public String toString()
	{
		return "MasEmpDesignationBO [pkEmpDesignationId="
				+ pkEmpDesignationId + ", empDesignationName="
				+ empDesignationName + "]";
	}

}