package com.thbs.mis.common.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the proj_mas_domains database table.
 * 
 */
@Entity
@Table(name="mas_domains")
@NamedQuery(name="MasDomainBO.findAll", query="SELECT p FROM MasDomainBO p")
public class MasDomainBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_domain_id")
	private short pkDomainId;

	@Column(name="domain_description")
	private String domainDescription;

	@Column(name="domain_name")
	private String domainName;

	@Column(name="is_active")
	private String isActive;
	
	public MasDomainBO() {
	}

	public short getPkDomainId() {
		return this.pkDomainId;
	}

	public void setPkDomainId(short pkDomainId) {
		this.pkDomainId = pkDomainId;
	}

	public String getDomainDescription() {
		return this.domainDescription;
	}

	public void setDomainDescription(String domainDescription) {
		this.domainDescription = domainDescription;
	}

	public String getDomainName() {
		return this.domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "MasDomainBO [pkDomainId=" + pkDomainId + ", domainDescription="
				+ domainDescription + ", domainName=" + domainName
				+ ", isActive=" + isActive + "]";
	}

		
}