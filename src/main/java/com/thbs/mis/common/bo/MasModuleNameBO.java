package com.thbs.mis.common.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mas_module_name database table.
 * 
 */
@Entity
@Table(name="mas_module_name")
@NamedQuery(name="MasModuleNameBO.findAll", query="SELECT m FROM MasModuleNameBO m")
public class MasModuleNameBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_module_name_id")
	private int pkModuleNameId;

	@Column(name="module_name")
	private String moduleName;

	public MasModuleNameBO() {
	}

	public int getPkModuleNameId() {
		return this.pkModuleNameId;
	}

	public void setPkModuleNameId(int pkModuleNameId) {
		this.pkModuleNameId = pkModuleNameId;
	}

	public String getModuleName() {
		return this.moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	@Override
	public String toString()
	{
		return "MasModuleNameBO [pkModuleNameId=" + pkModuleNameId
				+ ", moduleName=" + moduleName + "]";
	}

}