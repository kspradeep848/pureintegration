/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingTopicsRepository.java                     */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the training topics  repository	         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	 */
/*********************************************************************/

package com.thbs.mis.training.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.MasTrainingTopicNameBO;
import com.thbs.mis.common.dao.GenericRepository;


/**
 * <Description TrainingFeedBackRepository:> This class contains all the 
 * DAO related methods used for the training topics repository for Data Base operation.
 * @author THBS
 * @version 1.0
 * @see 
 */
@Repository
public interface TrainingTopicsRepository extends GenericRepository<MasTrainingTopicNameBO,Long>
{
	//Added by Smrithi
	public List<MasTrainingTopicNameBO> findAll();
	//EOA  by Smrithi
	
	//Added by Balaji
	MasTrainingTopicNameBO findByPkTrainingTopicNameId(int topicID);
	//End of Addition by Balaji
	
	
}
