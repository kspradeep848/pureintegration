/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingRequestStatusRepository.java              */
/*                                                                   */
/*  Author      :  Rajesh Kumar S                                    */
/*                                                                   */
/*  Date        :  15-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the TrainingRequestStatusRepository      */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date              Who           Version         Comments     	 */
/*-------------------------------------------------------------------*/
/* 14-Nov-2016    Rajesh Kumar S    1.0      Initial version created */
/*********************************************************************/
package com.thbs.mis.training.dao;

import java.util.List;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.training.bo.TrainingMasRequestStatusBO;

public interface TrainingRequestStatusRepository extends GenericRepository<TrainingMasRequestStatusBO,Long> {
	
	public List<TrainingMasRequestStatusBO> findAll();
	
	
	TrainingMasRequestStatusBO findByPkTrainingRequestStatusId(byte pkTrainingRequestStatusId);
}
