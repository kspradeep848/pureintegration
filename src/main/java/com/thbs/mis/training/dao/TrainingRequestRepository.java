/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingRequestRepository.java                    */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the training request  repository	     */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016    THBS     1.0      Initial version created   	 	 */
/*********************************************************************/
package com.thbs.mis.training.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.training.bo.TrainingDatRequestDetailBO;

/**
 * <Description ExampleRepository:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
@Repository
@Transactional(readOnly = true)
public interface TrainingRequestRepository extends
		GenericRepository<TrainingDatRequestDetailBO, Long>,
		JpaSpecificationExecutor<TrainingDatRequestDetailBO> {
	// Added by Kamal Anand
	public List<TrainingDatRequestDetailBO> findAll();

	public TrainingDatRequestDetailBO findByPkTrainingRequestId(
			int trainingRequestId);

	public List<TrainingDatRequestDetailBO> findByFkTrainingRequesterEmpIdAndIsTrainingProgramCreated(
			int mgrId, String isProgCreated);

	public List<TrainingDatRequestDetailBO> findByTrainingRequestedDateBetweenAndFkTrainingRequesterEmpIdNotNull(
			Date startDate, Date endDate);

	public List<TrainingDatRequestDetailBO> findByFkTrainingRequestStatusIdAndFkTrainingRequesterEmpIdNotNull(
			byte statusId);

	public List<TrainingDatRequestDetailBO> findByFkTrainingRequesterEmpIdAndTrainingRequestedDateBetween(
			int empId, Date startDate, Date endDate);

	public List<TrainingDatRequestDetailBO> findByFkTrainingRequesterEmpIdAndFkTrainingRequestStatusId(
			int empId, byte statusId);

	public List<TrainingDatRequestDetailBO> findByFkTrainingRequestStatusIdAndTrainingRequestedDateBetweenAndFkTrainingRequesterEmpIdNotNull(
			byte statusId, Date startDate, Date endDate);

	public List<TrainingDatRequestDetailBO> findByFkTrainingRequesterEmpIdAndFkTrainingRequestStatusIdAndTrainingRequestedDateBetween(
			int empId, byte statusId, Date startDate, Date endDate);

	public List<TrainingDatRequestDetailBO> findByFkTrainingRequestStatusIdAndFkTrainingRequesterEmpId(
			byte statusId, int mgrId);

	public List<TrainingDatRequestDetailBO> findByFkTrainingRequesterEmpId(
			Integer emiId);
	
	public List<TrainingDatRequestDetailBO> findByfkTrainingRequesterEmpIdNotNullAndFkTrainingRequestStatusId(byte statusId);
	
	public List<TrainingDatRequestDetailBO> findByfkTrainingRequesterEmpIdNotNull();
	
	public List<TrainingDatRequestDetailBO> findByfkTrainingRequesterEmpIdIsNull();

	public List<TrainingDatRequestDetailBO> findByFkTrainingRequestStatusIdAndIsTrainingProgramCreated(byte status,String isProgCreated);

	// End of Addition by Kamal Anand
	
	//Added by Kamal Anand for Notifications
	
	@Query("select t from TrainingDatRequestDetailBO t where t.fkTrainingRequestStatusId=? and t.isTrainingProgramCreated=? and t.trainingRequestedDate between ? and ?")
	public List<TrainingDatRequestDetailBO> findByStatusIsprogRequestDateBetween(byte status,String isProgCreated, Date startDate, Date endDate);

	@Query("select t from TrainingDatRequestDetailBO t where t.fkTrainingRequestStatusId=? and t.fkTrainingRequesterEmpId=? and t.trainingModifiedDate between ? and ?")
	public List<TrainingDatRequestDetailBO> findByStatusRequesterempIdAndtrainingModifiedDate(byte status,int empId, Date startDate, Date endDate);
	
	
	

}
