package com.thbs.mis.training.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.training.bo.TrainingDatNominationDetailBO;



/**
 * <Description TrainingFeedBackRepository:> This class contains all the 
 * DAO related methods used for the training repository for Data Base operation.
 * @author THBS
 * @version 1.0
 * @see 
 */
@Repository
public interface TrainingNominationRepositoryForFeedBack extends GenericRepository<TrainingDatNominationDetailBO,Long>, JpaSpecificationExecutor<TrainingDatNominationDetailBO>
{/*
	void delete(TrainingDatNominationDetailBO deleted);
	 
    List<TrainingDatNominationDetailBO> findAll();
    
    List<TrainingDatNominationDetailBO> findBy();
 
    // Optional<DatTrainingAttendeesFeedback> findOne(Long id);
 
    void flush();
 
    TrainingDatNominationDetailBO save(TrainingDatNominationDetailBO persisted);

*/}