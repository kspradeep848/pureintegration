package com.thbs.mis.training.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.training.bo.TrainingDatNominationDetailBO;

public interface TrainingNominationRepository extends
		GenericRepository<TrainingDatNominationDetailBO, Long> {
	// Added by Kamal Anand
	public List<TrainingDatNominationDetailBO> findByFkDatTrainingProgrammeIdAndNominationStatus(
			int programId, String nominationStatus);

	public List<TrainingDatNominationDetailBO> findByFkDatTrainingProgrammeIdAndAttendedTraining(
			int programId, String attendedStatus);

	public List<TrainingDatNominationDetailBO> findByFkDatTrainingProgrammeIdAndAttendedTrainingAndFkNominatedByEmpId(
			int programId, String attendedStatus, int empId);

	public List<TrainingDatNominationDetailBO> findByFkDatNominatedEmpIdAndAttendedTraining(
			int empId, String attendedStatus);

	public List<TrainingDatNominationDetailBO> findByFkDatTrainingProgrammeIdAndIsFeedbackProvided(
			int progId, String status);
	
	public List<TrainingDatNominationDetailBO> findByFkDatTrainingProgrammeIdAndIsFeedbackProvidedAndAttendedTrainingAndNominationStatus(int progId, String status,String attendedStatus,String nominationStatus);

	public TrainingDatNominationDetailBO findByFkDatNominatedEmpIdAndFkDatTrainingProgrammeId(
			int empId, int progId);
	
	public List<TrainingDatNominationDetailBO> findByFkDatTrainingProgrammeId(int progId);
	
	public TrainingDatNominationDetailBO findByFkDatTrainingProgrammeIdAndFkDatNominatedEmpIdAndAttendedTraining(int progId,int empId,String sttendedStatus);
	
	public TrainingDatNominationDetailBO findByFkDatTrainingProgrammeIdAndFkDatNominatedEmpIdAndNominationStatus(int progId,int empId,String nomStatus);
	
	@Query("select count(fkDatTrainingProgrammeId) from TrainingDatNominationDetailBO where fkDatTrainingProgrammeId = ? and nominationStatus = 'NOMINATED'")
	public Integer getCountOfFilledSeats(Integer progId);

	// End of Addition by Kamal Anand

	// Added by Pratibha
	public List<TrainingDatNominationDetailBO> findByFkDatNominatedEmpId(
			int empId);

	public List<TrainingDatNominationDetailBO> findByFkDatNominatedEmpIdAndNominationStatus(
			int empId, String status);

	public List<TrainingDatNominationDetailBO> findByFkDatTrainingProgrammeIdAndFkDatNominatedEmpId(
			int programId, int empId);
	// EOA by Pratibha
	
	//Notifications
	@Query("select n from TrainingDatNominationDetailBO n join TrainingDatProgrammeDetailBO p on n.fkDatTrainingProgrammeId=p.pkTrainingProgrammeId "
			+ "left join TrainingDatRequestDetailBO tr on p.fkTrainingRequestId=tr.pkTrainingRequestId left join MasTrainingTopicNameBO topic on topic.pkTrainingTopicNameId=tr.fkTrainingTopicNameId where n.fkDatNominatedEmpId=? "
			)
	public List<TrainingDatNominationDetailBO> findByNominatedEmpId(Integer empId);
	
	@Query("select n from TrainingDatNominationDetailBO n join TrainingDatProgrammeDetailBO p on n.fkDatTrainingProgrammeId=p.pkTrainingProgrammeId "
			+ "left join TrainingDatRequestDetailBO tr on p.fkTrainingRequestId=tr.pkTrainingRequestId where n.fkDatNominatedEmpId=? and n.attendedTraining=? and  p.fkProgrammeStatusId=2"
			)
	public List<TrainingDatNominationDetailBO> findByNominatedEmpIdForFeedbackAndTrainingProgrmCompleted(Integer empId,String attendedTraining);

	

	public List<TrainingDatNominationDetailBO> findByFkDatNominatedEmpIdAndIsFeedbackProvided(
			int empId, String status);

}
