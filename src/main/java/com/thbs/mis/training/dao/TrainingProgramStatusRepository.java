package com.thbs.mis.training.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.training.bo.TrainingMasProgrammeStatusBO;

@Repository
public interface TrainingProgramStatusRepository extends
		GenericRepository<TrainingMasProgrammeStatusBO, Long> {
	public List<TrainingMasProgrammeStatusBO> findAll();
	
	//Added by Kamal Anand
	public TrainingMasProgrammeStatusBO findByPkTrainingProgrammeStatusId(byte statusId);
	//End of Addition by Kamal Anand

}
