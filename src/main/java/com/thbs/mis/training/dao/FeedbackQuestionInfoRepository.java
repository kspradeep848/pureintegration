package com.thbs.mis.training.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.MasFeedbackQuestionInfoBO;
import com.thbs.mis.common.dao.GenericRepository;

@Repository
public interface FeedbackQuestionInfoRepository extends
		GenericRepository<MasFeedbackQuestionInfoBO, Long> {

	// Added by Kamal Anand
	public List<MasFeedbackQuestionInfoBO> findByFkMasWorkFlowIdAndFeedbackQuestionStatusAndFeedbackQuestionTypeIn(
			short workflowId, byte questionStatus, List<Byte> questionType);

	public MasFeedbackQuestionInfoBO findByFkMasWorkFlowIdAndFeedbackQuestionStatusAndFeedbackQuestionType(
			short workflowId, byte questionStatus, byte questionType);

	public List<MasFeedbackQuestionInfoBO> findByFeedbackQuestionStatusAndFkMasWorkFlowIdOrderByFeedbackQuestionOrderAsc(
			byte questionStatus,short workFlowId);

	// EOA by Kamal Anand

	// Added by Pratibha
	public List<MasFeedbackQuestionInfoBO> findByFeedbackQuestionStatusAndFeedbackQuestionTypeAndFkMasWorkFlowId(
			byte questionStatus, byte questionType, short workFlowId);

	public List<MasFeedbackQuestionInfoBO> findByFeedbackQuestionStatusAndFkMasWorkFlowId(
			byte questionStatus, short workFlowId);
	// EOA by Pratibha

}
