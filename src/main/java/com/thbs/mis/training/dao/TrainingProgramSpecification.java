package com.thbs.mis.training.dao;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.training.bean.TrainingProgramBean;
import com.thbs.mis.training.bo.TrainingDatProgrammeDetailBO;

public class TrainingProgramSpecification {

	public TrainingProgramSpecification() {
	}

	public Specification<TrainingDatProgrammeDetailBO> getTrainingProgramsByStatusAndDateRange(
			TrainingProgramBean trainingProgramBean) {
		return (root, query, cb) -> {

			return cb.or(cb.equal(root.get("fkProgrammeStatusId"),
					trainingProgramBean.getStatus()), cb.between(
					root.get("trainingStartDateTime"),
					trainingProgramBean.getStartDate(),
					trainingProgramBean.getEndDate()));
		};
	}
}
