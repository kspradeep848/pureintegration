/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingProgrammeRepository.java                  */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the Training Programme Repository        */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016    THBS     1.0      Initial version created   	 	 */
/*********************************************************************/
package com.thbs.mis.training.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.training.bo.TrainingDatProgrammeDetailBO;

/**
 * <Description TrainingProgrammeRepository:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
public interface TrainingProgrammeRepository extends
		GenericRepository<TrainingDatProgrammeDetailBO, Long>,
		JpaSpecificationExecutor<TrainingDatProgrammeDetailBO> {
	// Added by Kamal Anand
	public List<TrainingDatProgrammeDetailBO> findAll();

	public TrainingDatProgrammeDetailBO findByFkTrainingRequestId(
			int trainingRequestId);

	public TrainingDatProgrammeDetailBO findByPkTrainingProgrammeId(
			int trainingProgramId);

	public List<TrainingDatProgrammeDetailBO> findByTrainingStartDateTimeBetween(
			Date startDate, Date endDate);

	public List<TrainingDatProgrammeDetailBO> findByFkProgrammeStatusId(
			byte statusId);

	public List<TrainingDatProgrammeDetailBO> findByFkProgrammeStatusIdAndTrainingStartDateTimeBetween(
			byte status, Date startDate, Date endDate);

	public List<TrainingDatProgrammeDetailBO> findByProgrammeModifiedDateLessThanEqualAndFkProgrammeStatusId(
			Date startDate, byte status);

	@Query(nativeQuery = true, value = "SELECT * FROM training_dat_programme_detail p INNER JOIN training_dat_nomination_detail n on p.pk_training_programme_id = n.fk_dat_training_programme_id "
			+ " where p.training_start_date_time >= :startDate AND p.training_end_date_time <= :endDate AND n.fk_dat_nominated_emp_id = :empId AND nomination_status = 'NOMINATED'")
	public List<TrainingDatProgrammeDetailBO> getAllNominatedProgramsWithinDateRange(
			@Param("startDate") Date startDate, @Param("endDate") Date endDate,
			@Param("empId") int empId);

	// End of Addition by Kamal Anand

	// Added by Balaji
	TrainingDatProgrammeDetailBO findByPkTrainingProgrammeIdAndTrainingStartDateTimeGreaterThanEqualAndTrainingEndDateTimeLessThanEqual(
			Integer pkTrainingProgrammeId, Date trainingStartDateTime,
			Date trainingEndDateTime);

	TrainingDatProgrammeDetailBO findByPkTrainingProgrammeId(
			Integer pkTrainingProgrammeId);

	TrainingDatProgrammeDetailBO findByFkTrainingRequestIdAndFkProgrammeStatusId(
			Integer fkTrainingRequestId, byte programStatusId);

	TrainingDatProgrammeDetailBO findByPkTrainingProgrammeIdAndProgrammeModifiedDateGreaterThanEqualAndFkProgrammeStatusId(
			Integer pkTrainingProgrammeId, Date programmeModifiedDate, byte programStatus);
	
	@Query( nativeQuery = true, value = "SELECT * from training_dat_programme_detail programDetail JOIN training_dat_request_detail reqDetail "
			 + "ON programDetail.fk_training_request_id = reqDetail.pk_training_request_id "
			+" WHERE reqDetail.fk_training_requester_emp_id=:requesterId AND DATEDIFF(now(),programDetail.programme_modified_date)>=90 "
			+" AND programDetail.fk_programme_status_id=2 AND programDetail.programme_modified_date BETWEEN :startDate AND :endDate")
	public List<TrainingDatProgrammeDetailBO> getAllProgramDetailsByRmIdDateRange(@Param ("requesterId") Integer requesterId ,@Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	
	@Query( nativeQuery = true, value = "SELECT * from training_dat_programme_detail programDetail JOIN training_dat_request_detail reqDetail "
			 + "ON programDetail.fk_training_request_id = reqDetail.pk_training_request_id "
			+" WHERE reqDetail.fk_training_requester_emp_id=:requesterId AND DATEDIFF(now(),programDetail.programme_modified_date)>=90 "
			+" AND programDetail.fk_programme_status_id=2 ")
	public List<TrainingDatProgrammeDetailBO> getAllProgramDetailsByRmId(@Param ("requesterId") Integer requesterId );


	// End of Addition by Balaji
	
	//Notifications
	@Query(nativeQuery=true, value="select * from training_dat_programme_detail p join training_dat_request_detail r on p.fk_training_request_id=r.pk_training_request_id where r.fk_training_requester_emp_id=:rmId and p.fk_programme_status_id=2")
	public List<TrainingDatProgrammeDetailBO> findByprogramNotificationOnRmIDAndstatus(@Param ("rmId") Integer rmId);
	
	//Added by Kamal Anand for Notifications
	@Query("SELECT prog FROM TrainingDatProgrammeDetailBO prog "
			+ "join TrainingDatRequestDetailBO req on req.pkTrainingRequestId = prog.fkTrainingRequestId"
			+ " where req.fkTrainingRequesterEmpId = ? and req.isTrainingProgramCreated = 'YES' and req.fkTrainingRequestStatusId = ?")
	public List<TrainingDatProgrammeDetailBO> getProgramCreatedDetailsforRequestbyEmpId(Integer empId,byte status);
	//End of Addition by Kamal Anand for Notifications

}
