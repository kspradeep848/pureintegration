/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingFeedBackSpecifications.java               */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the Specification         */
/*                 used for the training repository	criteria query   */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 23-Jun-2016    THBS     1.0      Initial version created   	 	 */
/*********************************************************************/

package com.thbs.mis.training.dao;


import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.training.bean.TrainingFeedBackBean;
import com.thbs.mis.training.bo.TrainingDatAttendeeFeedbackBO;
import com.thbs.mis.training.bo.TrainingDatNominationDetailBO;



/**
 * 
 * <Description TrainingFeedBackSpecifications:> TODO
 * @author THBS
 * @version 1.0
 * @see
 */
 
public class TrainingFeedBackSpecifications {
 
    public TrainingFeedBackSpecifications() {}
    
    
    /**
     * 
     * <Description getTrainingFeedByempIdAndProgrammeId:> TODO
     * @param searchTerm
     * @return
     */
    public Specification<TrainingDatAttendeeFeedbackBO> getTrainingFeedByempIdAndProgrammeId(String searchTerm) {
        return (root, query, cb) -> {
            // String containsLikePattern = getContainsLikePattern(searchTerm);
            return cb.and(cb.equal(root.get("datEmplogin"), 1),
    				cb.equal(root.get("datTrainingProgrammeDetail"), 1));
        };
    }
    
 
    /**
     * 
     * <Description getTrainingFeedBackBetweenDates:> Create the specification for retrieving the 
     * feed back details from the DB according to the EmpId and the Programme ID.
     * 
     * @param dateRange contains the input date range.
     * 
     * @return The Specification Object for the TrainingAttendeesFeedbackBO.
     *  
     */
    
    public Specification<TrainingDatAttendeeFeedbackBO> getTrainingFeedByempIdAndProgrammeId(TrainingFeedBackBean dateRangeBean) {
        return (root, query, cb) -> {
            return cb.and(cb.equal(root.get("fkAttendeesEmpId"), dateRangeBean.getAttendeesEmpID()),
    				cb.equal(root.get("datTrainingProgrammeDetail"), dateRangeBean.getTrainingProgrammeId()));
        };
    }
    
    /**
     * 
     * <Description getTrainingFeedBackBetweenDates:> Create the specification for retrieving the 
     * feed back details from the DB according to the EmpId and the Program ID.
     * 
     * @param dateRange contains the input date range.
     * 
     * @return The Specification Object for the TrainingAttendeesFeedbackBO.
     *  
     */
    
    public Specification<TrainingDatNominationDetailBO> getNomiDetailByEmpIdAndAttendedTraining(TrainingFeedBackBean dateRangeBean) {
        return (root, query, cb) -> {
            return cb.and(cb.equal(root.get("fkNominatedEmpid"), dateRangeBean.getAttendeesEmpID()),
            		cb.equal(root.get("attendedTraining"), 1));
            
        }; 
    }
    
    
    /**
     * 
     * <Description getTrainingFeedBackBetweenDates:> Create the specification for retrieving the 
     * feed back details from the DB according to the Date range given
     * 
     * @param dateRange contains the input date range.
     * 
     * @return The Specification Object for the TrainingAttendeesFeedbackBO.
     *  
     */
    
    public Specification<TrainingDatAttendeeFeedbackBO> getTrainingFeedBackBetweenDates(TrainingFeedBackBean dateRange) {
        return (root, query, cb) -> {
            return cb.between(root.get("feedbackCreatedDate"), dateRange.getFromDate(), dateRange.getToDate());
        };
    }
  
    /**
     * 
     * <Description getTrainingFeedBackBetweenDates:> Create the specification for retrieving the 
     * feed back details from the DB according to the Date range given
     * 
     * @param dateRange contains the input date range.
     * 
     * @return The Specification Object for the TrainingAttendeesFeedbackBO.
     *  
     */
    
    public Specification<TrainingDatAttendeeFeedbackBO> getAllTrainingFeedBackByProgramId(TrainingFeedBackBean feedBackBeen) {
        return (root, query, cb) -> {
            return cb.equal(root.get("datTrainingProgrammeDetail"), feedBackBeen.getTrainingProgrammeId());
        };
    }
    
    public Specification<TrainingDatAttendeeFeedbackBO> getAllTrainingFeedBackByProgramIdSingle(TrainingFeedBackBean feedBackBeen) {
        return (root, query, cb) -> {
            return cb.equal(root.get("datTrainingProgrammeDetail"), feedBackBeen.getTrainingProgrammeId());
        };
    }
    /**
     * 
     * <Description getTrainingFeedBackBetweenDates:> Create the specification for retrieving the 
     * feed back details from the DB according to the EmpId and the Program ID.
     * 
     * @param dateRange contains the input date range.
     * 
     * @return The Specification Object for the TrainingAttendeesFeedbackBO.
     *  
     */
    
    public Specification<TrainingDatNominationDetailBO> getNomiDetailByEmpIdAndProgramId(TrainingFeedBackBean dateRangeBean) {
        return (root, query, cb) -> {
            return cb.and(cb.equal(root.get("fkNominatedEmpid"), dateRangeBean.getAttendeesEmpID()),
            		cb.equal(root.get("trainingProgramDetails"), dateRangeBean.getTrainingProgrammeId()));
            
        }; 
    }
    
}