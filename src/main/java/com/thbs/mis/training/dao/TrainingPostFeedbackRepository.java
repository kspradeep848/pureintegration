package com.thbs.mis.training.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.training.bo.TrainingDatPostApplicationFeedbackBO;

@Repository
public interface TrainingPostFeedbackRepository extends
		GenericRepository<TrainingDatPostApplicationFeedbackBO, Long> {

	// Added by Smrithi
	public List<TrainingDatPostApplicationFeedbackBO> findByFkDatTrainingProgrammeId(
			Integer progId);

	// EOA by Smrithi

	// Added by Balaji
	List<TrainingDatPostApplicationFeedbackBO> findByFkDatTrainingProgrammeIdAndFkManagerEmpId(
			Integer fkDatTrainingProgrammeId, Integer fkManagerEmpId);

	// End of Addition Balaji

	// Added by Kamal Anand
	List<TrainingDatPostApplicationFeedbackBO> findByFkDatTrainingProgrammeIdAndFkManagerEmpIdAndFkFeedbackQuestionInfoIdIn(
			int progId, int mgrId, List<Integer> questionId);

	List<TrainingDatPostApplicationFeedbackBO> findByFkDatTrainingProgrammeIdAndFkNominatedEmpId(
			int progId, int empId);
	// EOA by Kamal Anand

}
