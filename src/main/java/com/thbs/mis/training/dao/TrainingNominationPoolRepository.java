package com.thbs.mis.training.dao;

import java.util.ArrayList;
import java.util.List;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.training.bo.TrainingDatNominationPoolDetailBO;

public interface TrainingNominationPoolRepository extends
		GenericRepository<TrainingDatNominationPoolDetailBO, Long> {

	//Added by Kamal Anand
	public TrainingDatNominationPoolDetailBO findByFkNominatedEmpIdAndFkTrainingProgrammeId(
			int empId, int progId);
	
	public List<TrainingDatNominationPoolDetailBO> findByFkNominatedEmpIdNotIn(List<Integer> empIds);
	//End of Addition by Kamal Anand
	
	// Added by Balaji 
		List<TrainingDatNominationPoolDetailBO> findByFkNominatedEmpIdAndNominationStatusIn(Integer fkNominatedEmpId, List<Byte> nominationStatus);
		
		List<TrainingDatNominationPoolDetailBO> findByFkApproverEmpId(Integer fkApproverEmpId);
		
		List<TrainingDatNominationPoolDetailBO> findByFkApproverEmpIdAndNominationStatus(Integer fkApproverEmpId, byte nominationStatus);
		
		
		List<TrainingDatNominationPoolDetailBO> findByFkApproverEmpIdAndNominationStatusIn(Integer fkApproverEmpId, List<Byte> nominationStatus);
		

		//End of Addition by Balaji
		
		//Added by Pratibha
		public TrainingDatNominationPoolDetailBO findByFkTrainingProgrammeIdAndFkNominatedEmpId(
				Integer ProId, Integer EmpId);
		
		public TrainingDatNominationPoolDetailBO findByFkTrainingProgrammeId(
				Integer pgmId);
		//EOA by Pratibha

		public List<TrainingDatNominationPoolDetailBO> findByFkApproverEmpIdAndFkNominatedEmpIdAndNominationStatus(
				Integer fkApproverEmpId, Integer nominatedEmpId,
				byte nominationStatus);

		public List<TrainingDatNominationPoolDetailBO> findByFkApproverEmpIdAndFkNominatedEmpId(
				Integer fkApproverEmpId, Integer nominatedEmpId);

 
		
		
}
