package com.thbs.mis.training.dao;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.training.bean.TrainingRequestBean;
import com.thbs.mis.training.bo.TrainingDatRequestDetailBO;

public class TrainingRequestSpecification {

	public TrainingRequestSpecification() {
	}

	public Specification<TrainingDatRequestDetailBO> getTrainingRequestByStatusAndDateRange(
			TrainingRequestBean trainingRequestBean) {
		return (root, query, cb) -> {

			return cb.or(cb.equal(root.get("fkTrainingRequestStatusId"),
					trainingRequestBean.getStatus()), cb.between(
					root.get("trainingRequestedDate"),
					trainingRequestBean.getStartDate(),
					trainingRequestBean.getEndDate()));
		};
	}

	public Specification<TrainingDatRequestDetailBO> getTrainingRequestsByReportingMgrId(
			TrainingRequestBean bean) {

		return (root, query, cb) -> {
			return cb.equal(root.get("fkTrainingRequesterEmpId"),
					bean.getMgrId());
		};

	}

}
