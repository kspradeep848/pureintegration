/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingFeedBackRepository.java                   */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the training repository			         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	 */
/*********************************************************************/
package com.thbs.mis.training.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.training.bo.TrainingDatAttendeeFeedbackBO;

/**
 * <Description TrainingFeedBackRepository:> This class contains all the DAO
 * related methods used for the training repository for Data Base operation.
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
@Repository
public interface TrainingFeedBackRepository extends
		GenericRepository<TrainingDatAttendeeFeedbackBO, Long>,
		JpaSpecificationExecutor<TrainingDatAttendeeFeedbackBO> {
	// Added by Kamal Anand
	public List<TrainingDatAttendeeFeedbackBO> findByFkTrainingDatProgrammeIdAndFkAttendeeEmpId(
			int trainingProgId, int empId);

	public TrainingDatAttendeeFeedbackBO findByFkTrainingDatProgrammeIdAndFkAttendeeEmpIdAndFkDatFeedbackQuestionInfoId(
			int progId, int empId, int questionInfoId);
	
	public List<TrainingDatAttendeeFeedbackBO> findByFkTrainingDatProgrammeId(int progId);
	// End of Addition by Kamal Anand
	
	//Added by Kamal Anand for Notifications
	@Query("SELECT f FROM TrainingDatAttendeeFeedbackBO f where f.fkTrainingDatProgrammeId = ? and "
			+ "f.fkAttendeeEmpId in (select prof.fkMainEmpDetailId from DatEmpProfessionalDetailBO prof where "
			+ "prof.fkEmpReportingMgrId = ?) group by f.fkAttendeeEmpId")
	public List<TrainingDatAttendeeFeedbackBO> getFeedbackAttendeesByProgIdForRm(int progId,int empId);
	
	@Query("SELECT f FROM TrainingDatAttendeeFeedbackBO f where f.fkTrainingDatProgrammeId = ? group by f.fkAttendeeEmpId")
	public List<TrainingDatAttendeeFeedbackBO> getFeedbackAttendeesByProgId(int progId);
	//End of Addition by Kamal Anand for Notifications
}
