/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingPostFeedbackService.java               */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  24-10-2016                                        */
/*                                                                   */
/*  Description :  This class contains service methods               */
/*                   of TrainingPostFeedBack                         */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 24-10-2016, 2016     THBS     1.0        Initial version created */
/*********************************************************************/

package com.thbs.mis.training.service;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.MasFeedbackQuestionInfoBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.DateTimeUtil;
import com.thbs.mis.notificationframework.trainingnotification.service.TrainingNotificationService;
import com.thbs.mis.training.bean.AttendedListBean;
import com.thbs.mis.training.bean.AttendeeFeedbackOutputBean;
import com.thbs.mis.training.bean.EmployeePostApplicationFeedbackInputBean;
import com.thbs.mis.training.bean.FeedbackListBean;
import com.thbs.mis.training.bean.FeedbackResponsesBean;
import com.thbs.mis.training.bean.PostApplicationDetailedFeedbackBean;
import com.thbs.mis.training.bean.PostApplicationFeedbackInputBean;
import com.thbs.mis.training.bean.TrainingCoordinatorViewFeedbackBean;
import com.thbs.mis.training.bean.TrainingDetailedFeedbackOutputBean;
import com.thbs.mis.training.bean.TrainingManagerViewPostFeedbackBean;
import com.thbs.mis.training.bean.TrainingPostApplicationFeedbackBean;
import com.thbs.mis.training.bean.TrainingPostFeedbackInputBean;
import com.thbs.mis.training.bo.TrainingDatNominationDetailBO;
import com.thbs.mis.training.bo.TrainingDatPostApplicationFeedbackBO;
import com.thbs.mis.training.bo.TrainingDatProgrammeDetailBO;
import com.thbs.mis.training.constants.TrainingConstants;
import com.thbs.mis.training.dao.FeedbackQuestionInfoRepository;
import com.thbs.mis.training.dao.TrainingNominationRepository;
import com.thbs.mis.training.dao.TrainingPostFeedbackRepository;
import com.thbs.mis.training.dao.TrainingProgrammeRepository;

@Service
public class TrainingPostFeedBackService {

	private static final AppLog LOG = LogFactory
			.getLog(TrainingPostFeedBackService.class);

	@Autowired
	private TrainingPostFeedbackRepository postFeedbackRepository;

	@Autowired
	private TrainingProgrammeRepository trainingProgramRepository;

	@Autowired
	private FeedbackQuestionInfoRepository feedbackQuestionInfoRepository;

	@Autowired
	private EmployeePersonalDetailsRepository empPersonalDetailRepository;

	@Autowired
	private EmployeeProfessionalRepository empProfessionalDetailRepository;

	@Autowired
	private TrainingNominationRepository trainingNominationRepository;
	
	@Autowired
	private TrainingNotificationService notificationService;
	
	@Autowired
	private EmpDetailRepository empDetailRepository;
	
	@Value("${training.notification.roleid}")
	private String trainingTeamRoleIds;

	@Value("${active.question.status}")
	private byte activeQuestionStatus;

	@Value("${post.application.feedback.workflow}")
	private short postApplicationWorkFlowId;

	@Value("${overall.question.type}")
	private byte overallRatingQuestionType;

	@Value("${comments.question.type}")
	private byte commentsQuestionType;

	@Value("${training.program.completed.status}")
	private byte trainingProgramCompletedStatus;

	// Added by Smrithi
	/**
	 * @author THBS
	 * @param postApplicationFeedbackBean
	 * @return List<TrainingDatPostApplicationFeedbackBO> List of feedback
	 *         application post feedback
	 * @throws DataAccessException
	 * @throws ParseException
	 * @throws MethodArgumentNotValidException
	 * @Description: This method is used to create post attendees feedback.
	 */
	public List<TrainingDatPostApplicationFeedbackBO> createPostAttendeesFeedback(
			List<TrainingPostApplicationFeedbackBean> postApplicationFeedbackBean)
			throws DataAccessException, ParseException,
			MethodArgumentNotValidException {
		LOG.startUsecase("create Post Attendees Feedback Service");

		List<TrainingDatPostApplicationFeedbackBO> trainingPostApplicationFeedback = new ArrayList<TrainingDatPostApplicationFeedbackBO>();
		List<EmployeePostApplicationFeedbackInputBean> answerList = null;
		TrainingDatPostApplicationFeedbackBO trainingPostApplicationFeedback1;
		List<Short> trainingTeamRoleIdList = Arrays
				.asList(trainingTeamRoleIds.split(",")).stream()
				.map(Short::valueOf).collect(Collectors.toList());
		try {
			for (int i = 0; i < postApplicationFeedbackBean.size(); i++) {
				answerList = postApplicationFeedbackBean.get(i)
						.getEmployeeAnswerList();
				if (answerList.size() > 0) {
					for (EmployeePostApplicationFeedbackInputBean empAnswer : answerList) {
						trainingPostApplicationFeedback1 = new TrainingDatPostApplicationFeedbackBO();
						trainingPostApplicationFeedback1
								.setFkDatTrainingProgrammeId(postApplicationFeedbackBean
										.get(i).getDatTrainingProgrammeId());
						trainingPostApplicationFeedback1
								.setFkManagerEmpId(postApplicationFeedbackBean
										.get(i).getManagerEmpId());
						trainingPostApplicationFeedback1
								.setEvaluationCreatedDate(new Date());
						trainingPostApplicationFeedback1
								.setFkFeedbackQuestionInfoId(postApplicationFeedbackBean
										.get(i).getFeedbackQuestionInfoId());
						trainingPostApplicationFeedback1
								.setPostApplicationMgrAnswer(empAnswer
										.getAnswer());
						trainingPostApplicationFeedback1
								.setFkNominatedEmpId(empAnswer.getEmpId());
						postFeedbackRepository
								.save(trainingPostApplicationFeedback1);
						trainingPostApplicationFeedback
								.add(trainingPostApplicationFeedback1);
					}
				}
			}

		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new DataAccessException("Exception occured", e);
		}
		LOG.endUsecase("create Post Attendees Feedback Service");
		//Added by Kamal Anand for Notifications
		try {
			List<Integer> empIdList = empDetailRepository.getActiveEmployeesForRoleId(trainingTeamRoleIdList);
			if(empIdList != null)
 {
				empIdList
						.forEach(empId -> {
							try {
								notificationService
										.setPostApplicationFeedbackProvidedNotificationToTCScreen(
												empId,
												postApplicationFeedbackBean
														.get(0)
														.getDatTrainingProgrammeId());
							} catch (Exception e) {
								LOG.info("Some exception occurred while setting Create Training Request Notification");
							}
						});
			}
		} catch(Exception e){
			LOG.info("Exception occurred while getting empId for Training coordinator roles");
		}
		//End of Addition by Kamal Anand for Notifications
		return trainingPostApplicationFeedback;
	}

	// EOA by Smrithi

	// Added by Kamal Anand
	/**
	 * 
	 * @param feedbackInput
	 * @return <TrainingCoordinatorViewFeedbackBean>
	 * @throws DataAccessException
	 * @Description:This method is used to view Post Application Feedback for
	 *                   all Training Programs
	 */
	public Page<TrainingCoordinatorViewFeedbackBean> getAllManagerFeedback(
			TrainingPostFeedbackInputBean feedbackInput)
			throws DataAccessException {
		List<TrainingDatProgrammeDetailBO> progDetails = null;
		MasFeedbackQuestionInfoBO questionInfo;
		List<TrainingManagerViewPostFeedbackBean> feedBackList = new ArrayList<TrainingManagerViewPostFeedbackBean>();
		LOG.debug("View Post Application Feedback for all Training Programs");
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DAY_OF_MONTH, -90);
		Date currentDateMinus90Days = cal.getTime();

		Page finalPage;
		Pageable pageable = new PageRequest(feedbackInput.getPageNumber(),
				feedbackInput.getPageSize());
		try {

			Date currentDateMinus90DaysEndDate = DateUtils.truncate(
					currentDateMinus90Days, Calendar.DATE);

			LocalTime midnight1 = LocalTime.MAX;
			LocalDate date1 = currentDateMinus90DaysEndDate.toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDate();
			LocalDateTime todayMidnight1 = LocalDateTime.of(date1, midnight1);
			Date minus90DaysEndDate = Date.from(todayMidnight1.atZone(
					ZoneId.systemDefault()).toInstant());

			progDetails = trainingProgramRepository
					.findByProgrammeModifiedDateLessThanEqualAndFkProgrammeStatusId(
							minus90DaysEndDate, trainingProgramCompletedStatus);

			questionInfo = feedbackQuestionInfoRepository
					.findByFkMasWorkFlowIdAndFeedbackQuestionStatusAndFeedbackQuestionType(
							postApplicationWorkFlowId, activeQuestionStatus,
							overallRatingQuestionType);

			if (questionInfo == null)
				throw new DataAccessException(
						"Unable to get Feedback Question Details");

			if (feedbackInput.getStartDate() != null
					&& feedbackInput.getEndDate() != null) {
				Date truncatedStartDate = DateUtils.truncate(
						feedbackInput.getStartDate(), Calendar.DATE);
				Date truncatedEndDate = DateUtils.truncate(
						feedbackInput.getEndDate(), Calendar.DATE);
				feedbackInput.setStartDate(truncatedStartDate);

				LocalTime midnight = LocalTime.MAX;
				LocalDate date = truncatedEndDate.toInstant()
						.atZone(ZoneId.systemDefault()).toLocalDate();
				LocalDateTime todayMidnight = LocalDateTime.of(date, midnight);
				Date endDate = Date.from(todayMidnight.atZone(
						ZoneId.systemDefault()).toInstant());
				feedbackInput.setEndDate(endDate);
			}

			if (progDetails.size() > 0) {
				if (feedbackInput.getStatus() != null) {
					if (feedbackInput.getStartDate() != null
							&& feedbackInput.getEndDate() != null) {
						feedBackList = getPostApplicaitonFeedbackForAllTrainingProgramsOnStatusAndDateRange(
								progDetails, questionInfo, feedbackInput);
					} else if (feedbackInput.getStatus() != null) {
						feedBackList = getPostApplicationFeedbackForAllTrainingProgramsOnStatus(
								progDetails, feedbackInput.getStatus(),
								questionInfo);
					}
				}
				if (feedbackInput.getStatus() == null) {
					feedbackInput.setStatus("ALL");
					feedBackList = getPostApplicaitonFeedbackForAllTrainingProgramsOnStatusAndDateRange(
							progDetails, questionInfo, feedbackInput);
				}
			} /*else {
				throw new DataAccessException("No Program Details found");
			}*/
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		finalPage = new PageImpl(feedBackList, pageable, feedBackList.size());
		return finalPage;
	}

	/**
	 * 
	 * @param progDetails
	 * @param status
	 * @param questionInfo
	 * @return List<TrainingManagerViewPostFeedbackBean>
	 * @throws DataAccessException
	 * @Description : This method is used to get Post Application Feedback based
	 *              on Status for all Training Programs
	 */
	public List<TrainingManagerViewPostFeedbackBean> getPostApplicationFeedbackForAllTrainingProgramsOnStatus(
			List<TrainingDatProgrammeDetailBO> progDetails, String status,
			MasFeedbackQuestionInfoBO questionInfo) throws DataAccessException {
		TrainingManagerViewPostFeedbackBean feedBackBean;
		List<TrainingDatPostApplicationFeedbackBO> mgrFeedbackList = new ArrayList<TrainingDatPostApplicationFeedbackBO>();

		DatEmpPersonalDetailBO personalDetail = new DatEmpPersonalDetailBO();
		List<TrainingManagerViewPostFeedbackBean> feedBackList = new ArrayList<TrainingManagerViewPostFeedbackBean>();

		try {
			if (status.equalsIgnoreCase("COMPLETED")) {
				for (TrainingDatProgrammeDetailBO progDetail : progDetails) {
					int rating = 0;
					int noOfPersons = 0;
					float averageRating = 0.0f;
					if (progDetail.getTrainingDatRequestDetail()
							.getFkTrainingRequesterEmpId() != null) {
						feedBackBean = new TrainingManagerViewPostFeedbackBean();
						mgrFeedbackList = postFeedbackRepository
								.findByFkDatTrainingProgrammeIdAndFkManagerEmpId(
										progDetail.getPkTrainingProgrammeId(),
										progDetail
												.getTrainingDatRequestDetail()
												.getFkTrainingRequesterEmpId());
						if (mgrFeedbackList.size() > 0) {
							feedBackBean.setTitle(progDetail
									.getTrainingDatRequestDetail()
									.getMasTrainingTopicName()
									.getTrainingTopicName());
							feedBackBean.setDescription(progDetail
									.getTrainingDatRequestDetail()
									.getBriefTrainingContent());
							feedBackBean.setStartDate(progDetail
									.getTrainingStartDateTime());
							feedBackBean.setTrainerName(progDetail
									.getTrainerName());
							feedBackBean.setFeedbackStatus("VIEW FEEDBACK");

							feedBackBean.setProgId(progDetail
									.getPkTrainingProgrammeId());

							feedBackBean.setMgrId(progDetail
									.getTrainingDatRequestDetail()
									.getFkTrainingRequesterEmpId());

							personalDetail = empPersonalDetailRepository
									.findByFkEmpDetailId(progDetail
											.getTrainingDatRequestDetail()
											.getFkTrainingRequesterEmpId());

							feedBackBean.setMgrName(personalDetail
									.getEmpFirstName()
									+ " "
									+ personalDetail.getEmpLastName());

							for (TrainingDatPostApplicationFeedbackBO feedbackInfo : mgrFeedbackList) {
								System.out.println("Iterating mgrFeedbackList");
								if (feedbackInfo.getFkFeedbackQuestionInfoId() == questionInfo
										.getPkFeedbackQuestionInfoId()) {
									rating = rating
											+ Integer
													.parseInt(feedbackInfo
															.getPostApplicationMgrAnswer());
									noOfPersons = noOfPersons + 1;
									System.out.println("No of persons:"
											+ noOfPersons);
								}
							}
							averageRating = rating / noOfPersons;
							feedBackBean.setOverallRating(Math
									.round(averageRating) + "");
							feedBackList.add(feedBackBean);
						}
					}
				}
			}
			if (status.equalsIgnoreCase("PENDING FEEDBACK")) {
				for (TrainingDatProgrammeDetailBO progDetail : progDetails) {

					if (progDetail.getTrainingDatRequestDetail()
							.getFkTrainingRequesterEmpId() != null) {
						feedBackBean = new TrainingManagerViewPostFeedbackBean();
						mgrFeedbackList = postFeedbackRepository
								.findByFkDatTrainingProgrammeIdAndFkManagerEmpId(
										progDetail.getPkTrainingProgrammeId(),
										progDetail
												.getTrainingDatRequestDetail()
												.getFkTrainingRequesterEmpId());

						System.out.println("mgrFeedbackList size:"
								+ mgrFeedbackList.size() + " for prog id:"
								+ progDetail.getPkTrainingProgrammeId());
						if (mgrFeedbackList.size() == 0) {
							feedBackBean.setTitle(progDetail
									.getTrainingDatRequestDetail()
									.getMasTrainingTopicName()
									.getTrainingTopicName());
							feedBackBean.setDescription(progDetail
									.getTrainingDatRequestDetail()
									.getBriefTrainingContent());
							feedBackBean.setStartDate(progDetail
									.getTrainingStartDateTime());
							feedBackBean.setTrainerName(progDetail
									.getTrainerName());
							feedBackBean.setFeedbackStatus("PENDING FEEDBACK");

							feedBackBean.setProgId(progDetail
									.getPkTrainingProgrammeId());

							feedBackBean.setMgrId(progDetail
									.getTrainingDatRequestDetail()
									.getFkTrainingRequesterEmpId());

							personalDetail = empPersonalDetailRepository
									.findByFkEmpDetailId(progDetail
											.getTrainingDatRequestDetail()
											.getFkTrainingRequesterEmpId());

							feedBackBean.setMgrName(personalDetail
									.getEmpFirstName()
									+ " "
									+ personalDetail.getEmpLastName());

							feedBackBean.setOverallRating(0 + "");
							feedBackList.add(feedBackBean);
						}
					}
				}
			}
			if (status.equalsIgnoreCase("ALL")) {
				for (TrainingDatProgrammeDetailBO progDetail : progDetails) {
					int rating = 0;
					int noOfPersons = 0;
					float averageRating = 0.0f;

					if (progDetail.getTrainingDatRequestDetail()
							.getFkTrainingRequesterEmpId() != null) {
						feedBackBean = new TrainingManagerViewPostFeedbackBean();
						mgrFeedbackList = postFeedbackRepository
								.findByFkDatTrainingProgrammeIdAndFkManagerEmpId(
										progDetail.getPkTrainingProgrammeId(),
										progDetail
												.getTrainingDatRequestDetail()
												.getFkTrainingRequesterEmpId());

						feedBackBean.setTitle(progDetail
								.getTrainingDatRequestDetail()
								.getMasTrainingTopicName()
								.getTrainingTopicName());
						feedBackBean.setDescription(progDetail
								.getTrainingDatRequestDetail()
								.getBriefTrainingContent());
						feedBackBean.setStartDate(progDetail
								.getTrainingStartDateTime());
						feedBackBean
								.setTrainerName(progDetail.getTrainerName());
						feedBackBean.setFeedbackStatus("VIEW FEEDBACK");

						feedBackBean.setProgId(progDetail
								.getPkTrainingProgrammeId());

						feedBackBean.setMgrId(progDetail
								.getTrainingDatRequestDetail()
								.getFkTrainingRequesterEmpId());

						personalDetail = empPersonalDetailRepository
								.findByFkEmpDetailId(progDetail
										.getTrainingDatRequestDetail()
										.getFkTrainingRequesterEmpId());

						feedBackBean.setMgrName(personalDetail
								.getEmpFirstName()
								+ " "
								+ personalDetail.getEmpLastName());

						if (mgrFeedbackList.size() > 0) {
							for (TrainingDatPostApplicationFeedbackBO feedbackInfo : mgrFeedbackList) {
								if (feedbackInfo.getFkFeedbackQuestionInfoId() == questionInfo
										.getPkFeedbackQuestionInfoId()) {
									rating = rating
											+ Integer
													.parseInt(feedbackInfo
															.getPostApplicationMgrAnswer());
									noOfPersons = noOfPersons + 1;
								}
							}
							feedBackBean.setFeedbackStatus("VIEW FEEDBACK");
							averageRating = rating / noOfPersons;
							feedBackBean.setOverallRating(Math
									.round(averageRating) + "");
							feedBackList.add(feedBackBean);
						} else {
							feedBackBean.setFeedbackStatus("PENDING FEEDBACK");
							feedBackBean.setOverallRating(0 + "");
							feedBackList.add(feedBackBean);
						}
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured in Get All Programs and Attendee Feedback based on status",
					e);
		}
		return feedBackList;
	}

	/**
	 * 
	 * @param progDetails
	 * @param questionInfo
	 * @param feedbackInput
	 * @return List<TrainingManagerViewPostFeedbackBean>
	 * @throws DataAccessException
	 * @Description : This method is used to get Post Application Feedback based
	 *              on Status and date range for all Training Programs
	 */
	public List<TrainingManagerViewPostFeedbackBean> getPostApplicaitonFeedbackForAllTrainingProgramsOnStatusAndDateRange(
			List<TrainingDatProgrammeDetailBO> progDetails,
			MasFeedbackQuestionInfoBO questionInfo,
			TrainingPostFeedbackInputBean feedbackInput)
			throws DataAccessException {
		List<TrainingManagerViewPostFeedbackBean> viewFeedbackOnStatus = new ArrayList<TrainingManagerViewPostFeedbackBean>();
		List<TrainingManagerViewPostFeedbackBean> feedbackList = new ArrayList<TrainingManagerViewPostFeedbackBean>();
		TrainingDatProgrammeDetailBO progDetail;
		try {
			viewFeedbackOnStatus = getPostApplicationFeedbackForAllTrainingProgramsOnStatus(
					progDetails, feedbackInput.getStatus(), questionInfo);
			if (viewFeedbackOnStatus.size() > 0) {
				for (TrainingManagerViewPostFeedbackBean feedback : viewFeedbackOnStatus) {

					progDetail = trainingProgramRepository
							.findByPkTrainingProgrammeId(feedback.getProgId());

					if (progDetail != null) {
						if (progDetail.getProgrammeModifiedDate().getTime() >= feedbackInput
								.getStartDate().getTime()
								&& progDetail.getProgrammeModifiedDate()
										.getTime() <= feedbackInput
										.getEndDate().getTime()) {
							feedbackList.add(feedback);
						}
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured in Get All Programs and Attendee Feedback based on status and Date range",
					e);
		}
		return feedbackList;
	}

	/**
	 * 
	 * @param feedbackInput
	 * @return Page<AttendeeFeedbackOutputBean>
	 * @throws DataAccessException
	 * @Description : This service is used to get Post Application Feedback of a
	 *              Training Program based on status and Date Range
	 */
	public Page<AttendeeFeedbackOutputBean> getPostApplicationFeedbackForTrainingProgram(
			PostApplicationFeedbackInputBean feedbackInput)
			throws DataAccessException {
		LOG.startUsecase("getPostApplicationFeedbackForTrainingProgram");

		Page finalPage;
		Pageable pageable = new PageRequest(feedbackInput.getPageNumber(),
				feedbackInput.getPageSize());

		List<MasFeedbackQuestionInfoBO> questionType;
		List<Byte> feedbackQuestionType = new ArrayList<Byte>();
		feedbackQuestionType.add(overallRatingQuestionType);
		feedbackQuestionType.add(commentsQuestionType);
		List<Integer> questionInfoId = new ArrayList<Integer>();
		List<TrainingDatPostApplicationFeedbackBO> postFeedbackList = null;
		List<AttendeeFeedbackOutputBean> feedbackList = null;

		Date modifiedStartDate = null;
		Date modifiedEndDate = null;

		try {
			if (feedbackInput.getStartDate() != null
					&& feedbackInput.getEndDate() != null) {
				modifiedStartDate = DateTimeUtil
						.dateWithMinTimeofDay(feedbackInput.getStartDate());
				modifiedEndDate = DateTimeUtil
						.dateWithMaxTimeofDay(feedbackInput.getEndDate());
				feedbackInput.setStartDate(modifiedStartDate);
				feedbackInput.setEndDate(modifiedEndDate);
			}

			questionType = feedbackQuestionInfoRepository
					.findByFkMasWorkFlowIdAndFeedbackQuestionStatusAndFeedbackQuestionTypeIn(
							postApplicationWorkFlowId, activeQuestionStatus,
							feedbackQuestionType);

			if (questionType.size() > 0) {
				for (MasFeedbackQuestionInfoBO quesInfo : questionType) {
					questionInfoId.add(quesInfo.getPkFeedbackQuestionInfoId());
				}

				postFeedbackList = postFeedbackRepository
						.findByFkDatTrainingProgrammeIdAndFkManagerEmpIdAndFkFeedbackQuestionInfoIdIn(
								feedbackInput.getProgId(),
								feedbackInput.getMgrId(), questionInfoId);
			}

			if (feedbackInput.getStatus() != null) {
				if (feedbackInput.getStartDate() != null
						&& feedbackInput.getEndDate() != null) {
					feedbackList = getPostApplicationFeedbackByStatusAndDateRange(
							postFeedbackList, feedbackInput);
				} else if (feedbackInput.getStatus() != null) {
					feedbackList = getPostApplicationFeedbackByStatus(
							postFeedbackList, feedbackInput.getStatus());
				}
			} else {
				feedbackInput.setStatus("ALL");
				feedbackList = getPostApplicationFeedbackByStatusAndDateRange(
						postFeedbackList, feedbackInput);
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		finalPage = new PageImpl(feedbackList, pageable, feedbackList.size());
		return finalPage;
	}

	/**
	 * 
	 * @param feedbackList
	 * @param status
	 * @return List<AttendeeFeedbackOutputBean>
	 * @throws DataAccessException
	 * @Description : This method is used to view Post Application Feedback
	 *              Based on Status
	 */
	public List<AttendeeFeedbackOutputBean> getPostApplicationFeedbackByStatus(
			List<TrainingDatPostApplicationFeedbackBO> feedbackList,
			String status) throws DataAccessException {
		List<AttendeeFeedbackOutputBean> feedbackDetailsList = new ArrayList<AttendeeFeedbackOutputBean>();
		AttendeeFeedbackOutputBean feedbackBean;
		DatEmpPersonalDetailBO empPersonalDetail;
		DatEmpProfessionalDetailBO empProfessionalDetail;
		List<Byte> feedbackQuestionType = new ArrayList<Byte>();
		feedbackQuestionType.add(overallRatingQuestionType);
		feedbackQuestionType.add(commentsQuestionType);
		try {
			if (feedbackList != null) {
				for (TrainingDatPostApplicationFeedbackBO feeedback : feedbackList) {
					feedbackBean = new AttendeeFeedbackOutputBean();
					for (Byte question : feedbackQuestionType) {
						feedbackBean.setEmpId(feeedback.getFkNominatedEmpId());
						feedbackBean.setProgId(feeedback
								.getFkDatTrainingProgrammeId());
						feedbackBean.setProgTitle(feeedback
								.getTrainingDatProgrammeDetail()
								.getTrainingDatRequestDetail()
								.getMasTrainingTopicName()
								.getTrainingTopicName());
						feedbackBean.setStartDate(feeedback
								.getTrainingDatProgrammeDetail()
								.getTrainingStartDateTime());

						empPersonalDetail = empPersonalDetailRepository
								.findByFkEmpDetailId(feeedback
										.getFkNominatedEmpId());
						empProfessionalDetail = empProfessionalDetailRepository
								.findByFkMainEmpDetailId(feeedback
										.getFkNominatedEmpId());

						feedbackBean.setEmpName(empPersonalDetail
								.getEmpFirstName()
								+ " "
								+ empPersonalDetail.getEmpLastName());
						feedbackBean.setEmpMailId(empProfessionalDetail
								.getEmpProfessionalEmailId());
						feedbackBean.setFeedbackGivenDate(feeedback
								.getEvaluationCreatedDate());

						if (feeedback.getMasFeedbackQuestionInfo()
								.getFeedbackQuestionType() == overallRatingQuestionType)
							feedbackBean.setOverallRating(feeedback
									.getPostApplicationMgrAnswer());
						else if (feeedback.getMasFeedbackQuestionInfo()
								.getFeedbackQuestionType() == commentsQuestionType)
							feedbackBean.setComments(feeedback
									.getPostApplicationMgrAnswer());
					}
					feedbackDetailsList.add(feedbackBean);
				}
			} else {
				throw new DataAccessException("Feedback Details not found");
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		return populatePostApplicationFeedbackDetails(feedbackDetailsList,
				status);
	}

	/**
	 * 
	 * @param feedbackList
	 * @param feedbackInput
	 * @return List<AttendeeFeedbackOutputBean>
	 * @throws DataAccessException
	 * @Description : This method is used to get Post application feedback based
	 *              on Status and Date Range
	 */
	public List<AttendeeFeedbackOutputBean> getPostApplicationFeedbackByStatusAndDateRange(
			List<TrainingDatPostApplicationFeedbackBO> feedbackList,
			PostApplicationFeedbackInputBean feedbackInput)
			throws DataAccessException {
		List<AttendeeFeedbackOutputBean> feedbackListByStatus = null;
		List<AttendeeFeedbackOutputBean> finalFeedbackList = new ArrayList<AttendeeFeedbackOutputBean>();
		try {
			feedbackListByStatus = getPostApplicationFeedbackByStatus(
					feedbackList, feedbackInput.getStatus());

			if (feedbackListByStatus.size() > 0 || feedbackListByStatus != null) {
				for (AttendeeFeedbackOutputBean feedbackByStatus : feedbackListByStatus) {
					if (feedbackByStatus.getFeedbackGivenDate().getTime() >= feedbackInput
							.getStartDate().getTime()
							&& feedbackByStatus.getFeedbackGivenDate()
									.getTime() >= feedbackInput.getEndDate()
									.getTime())
						finalFeedbackList.add(feedbackByStatus);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		return finalFeedbackList;
	}

	/**
	 * 
	 * @param feedbackDetailsList
	 * @param status
	 * @return List<AttendeeFeedbackOutputBean>
	 * @Description : This method is used to populate Post Application Feedback
	 *              Details List
	 */
	public List<AttendeeFeedbackOutputBean> populatePostApplicationFeedbackDetails(
			List<AttendeeFeedbackOutputBean> feedbackDetailsList, String status) {
		List<AttendeeFeedbackOutputBean> feedbackList = new ArrayList<AttendeeFeedbackOutputBean>();
		List<AttendeeFeedbackOutputBean> finalFeedbackList = new ArrayList<AttendeeFeedbackOutputBean>();
		AttendeeFeedbackOutputBean feedbackBean = null;
		for (AttendeeFeedbackOutputBean feedback : feedbackDetailsList) {
			for (AttendeeFeedbackOutputBean feedbackUnique : feedbackDetailsList) {
				if (feedbackUnique.getEmpId() == feedback.getEmpId()) {
					feedbackBean = new AttendeeFeedbackOutputBean();
					feedbackBean = feedbackUnique;
					if (feedback.getOverallRating() != null) {
						feedbackBean.setOverallRating(feedback
								.getOverallRating());
					}
					if (feedback.getComments() != null) {
						feedbackBean.setComments(feedback.getComments());
					}
				}
			}
			feedbackList.add(feedbackBean);
		}
		for (AttendeeFeedbackOutputBean feedback : feedbackList) {
			if (status.equalsIgnoreCase(TrainingConstants.ONE_STAR))
				if (Integer.parseInt(feedback.getOverallRating()) == 1)
					finalFeedbackList.add(feedback);
			if (status.equalsIgnoreCase(TrainingConstants.TWO_STAR))
				if (Integer.parseInt(feedback.getOverallRating()) == 2)
					finalFeedbackList.add(feedback);
			if (status.equalsIgnoreCase(TrainingConstants.THREE_STAR))
				if (Integer.parseInt(feedback.getOverallRating()) == 3)
					finalFeedbackList.add(feedback);
			if (status.equalsIgnoreCase(TrainingConstants.FOUR_STAR))
				if (Integer.parseInt(feedback.getOverallRating()) == 4)
					finalFeedbackList.add(feedback);
			if (status.equalsIgnoreCase(TrainingConstants.FIVE_STAR))
				if (Integer.parseInt(feedback.getOverallRating()) == 5)
					finalFeedbackList.add(feedback);
			if (status.equalsIgnoreCase("ALL"))
				finalFeedbackList.add(feedback);
		}
		return removeDuplicatesFromFeedbackList(finalFeedbackList);
	}

	/**
	 * This method is used to remove duplicates from a feedback list
	 */
	private List<AttendeeFeedbackOutputBean> removeDuplicatesFromFeedbackList(
			List<AttendeeFeedbackOutputBean> feedbackList) {
		List<AttendeeFeedbackOutputBean> attendeefeedbackList = new ArrayList<AttendeeFeedbackOutputBean>();

		Map<Integer, AttendeeFeedbackOutputBean> attendeeFeedbackMap = new HashMap<Integer, AttendeeFeedbackOutputBean>();
		for (AttendeeFeedbackOutputBean u : feedbackList) {
			if (!attendeeFeedbackMap.containsKey(u.getEmpId())) {
				attendeeFeedbackMap.put(u.getEmpId(), u);
			}
		}

		attendeefeedbackList.addAll(attendeeFeedbackMap.values());
		return attendeefeedbackList;
	}

	/**
	 * 
	 * @param empId
	 * @param progId
	 * @return TrainingDetailedFeedbackOutputBean
	 * @throws DataAccessException
	 * @Description : This method is used to get Detailed Post Application
	 *              Feedback
	 */
	public TrainingDetailedFeedbackOutputBean getDetailedPostApplicationFeedback(
			int empId, int progId) throws DataAccessException {
		MasFeedbackQuestionInfoBO questionType;
		List<TrainingDatPostApplicationFeedbackBO> feedbackList;
		TrainingDetailedFeedbackOutputBean detailedFeedbackOutputBean = new TrainingDetailedFeedbackOutputBean();
		DatEmpPersonalDetailBO empPersonalDetail = new DatEmpPersonalDetailBO();
		DatEmpProfessionalDetailBO empProfessionalDetail = new DatEmpProfessionalDetailBO();
		TrainingDatProgrammeDetailBO progDetail = new TrainingDatProgrammeDetailBO();
		FeedbackResponsesBean feedbackBean = null;
		List<FeedbackResponsesBean> feedbackResponseList = new ArrayList<FeedbackResponsesBean>();
		try {
			questionType = feedbackQuestionInfoRepository
					.findByFkMasWorkFlowIdAndFeedbackQuestionStatusAndFeedbackQuestionType(
							postApplicationWorkFlowId, activeQuestionStatus,
							commentsQuestionType);

			if (questionType != null) {
				feedbackList = postFeedbackRepository
						.findByFkDatTrainingProgrammeIdAndFkNominatedEmpId(
								progId, empId);
				if (feedbackList.size() > 0) {
					empPersonalDetail = empPersonalDetailRepository
							.findByFkEmpDetailId(empId);
					empProfessionalDetail = empProfessionalDetailRepository
							.findByFkMainEmpDetailId(empId);

					progDetail = trainingProgramRepository
							.findByPkTrainingProgrammeId(progId);

					detailedFeedbackOutputBean = new TrainingDetailedFeedbackOutputBean();
					detailedFeedbackOutputBean.setEmpId(empId);
					detailedFeedbackOutputBean.setEmpName(empPersonalDetail
							.getEmpFirstName()
							+ " "
							+ empPersonalDetail.getEmpLastName());
					detailedFeedbackOutputBean
							.setEmpEmailId(empProfessionalDetail
									.getEmpProfessionalEmailId());
					detailedFeedbackOutputBean.setTrainingTitle(progDetail
							.getTrainingDatRequestDetail()
							.getMasTrainingTopicName().getTrainingTopicName());
					detailedFeedbackOutputBean.setTrainingStartDate(progDetail
							.getTrainingStartDateTime());
					detailedFeedbackOutputBean.setProgId(progId);

					for (TrainingDatPostApplicationFeedbackBO res : feedbackList) {
						if (res.getFkFeedbackQuestionInfoId() == questionType
								.getPkFeedbackQuestionInfoId()) {
							detailedFeedbackOutputBean.setComments(res
									.getPostApplicationMgrAnswer());
						} else {
							feedbackBean = new FeedbackResponsesBean();
							feedbackBean.setQuestionType(res
									.getMasFeedbackQuestionInfo()
									.getFeedbackQuestionType());
							feedbackBean.setQuestionName(res
									.getMasFeedbackQuestionInfo()
									.getFeedbackQuestionName());
							feedbackBean.setAnswer(res
									.getPostApplicationMgrAnswer());
							feedbackResponseList.add(feedbackBean);
						}
					}
					detailedFeedbackOutputBean
							.setFeedbackList(feedbackResponseList);
				} else {
					throw new DataAccessException("Feedback Details not found");
				}
			} else {
				throw new DataAccessException(
						"Some error happened while getting Questions");
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		return detailedFeedbackOutputBean;
	}

	/**
	 * 
	 * @param progId
	 * @param mgrId
	 * @return PostApplicationDetailedFeedbackBean
	 * @throws DataAccessException
	 * @Description : This method is used to view All Post Application Feedback
	 *              of Employee's for a particular Training Program
	 */
	public PostApplicationDetailedFeedbackBean getDetailedPostApplicationFeedbackForReportingManager(
			int progId, int mgrId) throws DataAccessException {
		List<TrainingDatPostApplicationFeedbackBO> feedbackLists = new ArrayList<TrainingDatPostApplicationFeedbackBO>();
		PostApplicationDetailedFeedbackBean detailedFeedback = null;
		TrainingDatProgrammeDetailBO progDetail = null;
		AttendedListBean attendedListBean = null;
		FeedbackListBean feedbackListBean = null;
		List<AttendedListBean> attendedList = new ArrayList<AttendedListBean>();
		List<FeedbackListBean> feedbackList;
		List<TrainingDatNominationDetailBO> nominationDetails = new ArrayList<TrainingDatNominationDetailBO>();

		try {
			progDetail = trainingProgramRepository
					.findByPkTrainingProgrammeId(progId);

			nominationDetails = trainingNominationRepository
					.findByFkDatTrainingProgrammeIdAndAttendedTrainingAndFkNominatedByEmpId(
							progId, "YES", mgrId);

			if (progDetail != null) {
				detailedFeedback = new PostApplicationDetailedFeedbackBean();
				detailedFeedback.setProgId(progId);
				detailedFeedback.setProgTitle(progDetail
						.getTrainingDatRequestDetail()
						.getMasTrainingTopicName().getTrainingTopicName());
				detailedFeedback.setProgStartDate(progDetail
						.getTrainingStartDateTime());
				detailedFeedback.setProgEndDate(progDetail
						.getTrainingEndDateTime());
				detailedFeedback.setVenue(progDetail.getTrainingMasRoomDetail()
						.getTrainingRoomName());
				detailedFeedback.setTrainerName(progDetail.getTrainerName());

				if (nominationDetails.size() > 0) {
					for (TrainingDatNominationDetailBO nominationDetail : nominationDetails) {
						feedbackList = new ArrayList<FeedbackListBean>();
						feedbackLists = postFeedbackRepository
								.findByFkDatTrainingProgrammeIdAndFkNominatedEmpId(
										progId, nominationDetail
												.getFkDatNominatedEmpId());

						if (feedbackLists.size() > 0) {
							attendedListBean = new AttendedListBean();
							for (TrainingDatPostApplicationFeedbackBO postFeedback : feedbackLists) {
								attendedListBean.setAttendeeEmpId(postFeedback
										.getFkNominatedEmpId());
								attendedListBean
										.setAttendeeEmpName(getEmpNameByEmpId(postFeedback
												.getFkNominatedEmpId()));

								feedbackListBean = new FeedbackListBean();

								feedbackListBean.setQuestionType(postFeedback
										.getMasFeedbackQuestionInfo()
										.getFeedbackQuestionType());
								feedbackListBean.setQuestionName(postFeedback
										.getMasFeedbackQuestionInfo()
										.getFeedbackQuestionName());
								feedbackListBean.setAnswer(postFeedback
										.getPostApplicationMgrAnswer());

								feedbackList.add(feedbackListBean);
							}
							attendedListBean.setFeedbackList(feedbackList);
							attendedList.add(attendedListBean);
						}
					}
				} else {
					throw new DataAccessException(
							"No attendees for this training program");
				}
				detailedFeedback.setAttendedList(attendedList);
			} else {
				throw new DataAccessException(
						"Some error happened while getting program details");
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		return detailedFeedback;
	}

	/**
	 * 
	 * @param empId
	 * @return personalDetail
	 * @Description This method is used to retrieve the employee name
	 */
	private String getEmpNameByEmpId(Integer empId) {
		LOG.startUsecase("getEmpNameByEmpId");
		DatEmpPersonalDetailBO personalDetail = new DatEmpPersonalDetailBO();
		try {
			personalDetail = empPersonalDetailRepository
					.findByFkEmpDetailId(empId);
			if (personalDetail != null) {
				return personalDetail.getEmpFirstName() + " "
						+ personalDetail.getEmpLastName();
			}
		} catch (Exception e) {
			LOG.info("Exception Occured: ", e);
		}
		LOG.endUsecase("getEmpNameByEmpId");
		return "";

	}
	// EOA by Kamal Anand
}
