/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingFeedBackService.java                      */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This service is used for the training feed back   */
/*                 related service calls.                            */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 23-Jun-2016    THBS     1.0       Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.training.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.MasFeedbackQuestionInfoBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.DateTimeUtil;
import com.thbs.mis.notificationframework.trainingnotification.service.TrainingNotificationService;
import com.thbs.mis.training.bean.AttendedTrainingOutputBean;
import com.thbs.mis.training.bean.AttendeeFeedbackInputBean;
import com.thbs.mis.training.bean.AttendeeFeedbackOutputBean;
import com.thbs.mis.training.bean.AttendeeFeedbackStatusBean;
import com.thbs.mis.training.bean.FeedbackResponsesBean;
import com.thbs.mis.training.bean.TrainingCoordinatorViewFeedbackBean;
import com.thbs.mis.training.bean.TrainingCreateAttendeesFeedbackBean;
import com.thbs.mis.training.bean.TrainingDetailedFeedbackOutputBean;
import com.thbs.mis.training.bean.TrainingFeedbackInputBean;
import com.thbs.mis.training.bean.TrainingFeedbackOutputBean;
import com.thbs.mis.training.bean.TrainingPostFeedbackInputBean;
import com.thbs.mis.training.bo.TrainingDatAttendeeFeedbackBO;
import com.thbs.mis.training.bo.TrainingDatNominationDetailBO;
import com.thbs.mis.training.bo.TrainingDatProgrammeDetailBO;
import com.thbs.mis.training.constants.TrainingConstants;
import com.thbs.mis.training.dao.FeedbackQuestionInfoRepository;
import com.thbs.mis.training.dao.TrainingFeedBackRepository;
import com.thbs.mis.training.dao.TrainingNominationRepository;
import com.thbs.mis.training.dao.TrainingPostFeedbackRepository;
import com.thbs.mis.training.dao.TrainingProgrammeRepository;

/**
 * <Description EmployeeSrvc:> This service is used for the training feed back
 * related service calls.
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
@Service
public class TrainingFeedBackService {
	// Added by Kamal Anand
	@Autowired
	private TrainingFeedBackRepository trainingFeedBackRepository;

	@Autowired
	private TrainingNominationRepository trainingNominationRepository;

	@Autowired
	private FeedbackQuestionInfoRepository feedbackQuestionInfoRepository;

	@Autowired
	private EmpDetailRepository empDetailRepository;

	@Autowired
	private EmployeePersonalDetailsRepository empPersonalDetailRepository;

	@Autowired
	private EmployeeProfessionalRepository empProfessionalDetailRepository;

	@Autowired
	private TrainingProgrammeRepository trainingProgramRepository;

	@Autowired
	private TrainingPostFeedbackRepository postFeedbackRepository;
	
	@Autowired
	private TrainingNotificationService notificationService;
	
	@Value("${training.notification.roleid}")
	private String trainingTeamRoleIds;

	@Value("${attendee.feedback.workflow}")
	private short workFlowId;

	@Value("${overall.question.type}")
	private byte overallRatingQuestionType;

	@Value("${comments.question.type}")
	private byte commentsQuestionType;

	@Value("${active.question.status}")
	private byte activeQuestionStatus;

	@Value("${post.application.feedback.workflow}")
	private short postApplicationWorkFlowId;

	@Value("${training.program.completed.status}")
	private byte trainingProgramCompletedStatus;

	private static final AppLog LOG = LogFactory
			.getLog(TrainingFeedBackService.class);

	/**
	 * 
	 * @param trainingFeedbackInputBean
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 *             Get Attended Training Programs List and its Feedback Status
	 *             based on Employee Id
	 * @Description : This method is used to get Attended Training Programs and
	 *              its feedback status based on Status or Date Range
	 */
	public List<TrainingFeedbackOutputBean> getAllAttendedTrainingProgramsAndFeedbackStatusByEmployeeId(
			TrainingFeedbackInputBean trainingFeedbackInputBean)
			throws DataAccessException, CommonCustomException {
		LOG.startUsecase("Get Attended Training Programs List and its Feedback Status based on Employee Id");
		List<TrainingFeedbackOutputBean> trainingFeedbackOutputList = new ArrayList<TrainingFeedbackOutputBean>();
		TrainingFeedbackOutputBean feedbackDetailBean;
		List<TrainingDatNominationDetailBO> attendedProgDetails;
		List<TrainingDatAttendeeFeedbackBO> attendeeFeedback;
		Date modifiedStartDate = null;
		Date modifiedEndDate = null;
		LOG.debug("Get Attended Training Programs List and its Feedback Status based on Employee Id");
		try {
			attendedProgDetails = trainingNominationRepository
					.findByFkDatNominatedEmpIdAndAttendedTraining(
							trainingFeedbackInputBean.getEmpId(), "YES");
			if (attendedProgDetails != null && attendedProgDetails.size() > 0) {

				if (trainingFeedbackInputBean.getStartDate() != null
						&& trainingFeedbackInputBean.getEndDate() != null) {
					modifiedStartDate = DateTimeUtil
							.dateWithMinTimeofDay(trainingFeedbackInputBean
									.getStartDate());
					modifiedEndDate = DateTimeUtil
							.dateWithMaxTimeofDay(trainingFeedbackInputBean
									.getEndDate());
					trainingFeedbackInputBean.setStartDate(modifiedStartDate);
					trainingFeedbackInputBean.setEndDate(modifiedEndDate);
				}

				if (trainingFeedbackInputBean.getStatus() != null) {
					if (trainingFeedbackInputBean.getStartDate() != null
							&& trainingFeedbackInputBean.getEndDate() != null) {

						trainingFeedbackOutputList = getAttendedTrainingProgramListBasedOnStatusAndDateRange(
								trainingFeedbackInputBean, attendedProgDetails);
					} else if (trainingFeedbackInputBean.getStatus() != null) {

						trainingFeedbackOutputList = getAttendedTrainingProgramListBasedOnStatus(
								trainingFeedbackInputBean.getStatus(),
								trainingFeedbackInputBean.getEmpId(),
								attendedProgDetails);
					}
				} else {

					for (TrainingDatNominationDetailBO attendedProg : attendedProgDetails) {

						if (attendedProg.getTrainingDatProgrammeDetail()
								.getFkProgrammeStatusId() == trainingProgramCompletedStatus) {
							feedbackDetailBean = new TrainingFeedbackOutputBean();
							if (attendedProg.getTrainingDatProgrammeDetail()
									.getTrainingStartDateTime().getTime() >= trainingFeedbackInputBean
									.getStartDate().getTime()
									&& attendedProg
											.getTrainingDatProgrammeDetail()
											.getTrainingStartDateTime()
											.getTime() <= trainingFeedbackInputBean
											.getEndDate().getTime()) {
								feedbackDetailBean
										.setBriefTrainingContent(attendedProg
												.getTrainingDatProgrammeDetail()
												.getTrainingDatRequestDetail()
												.getBriefTrainingContent());
								feedbackDetailBean.setTitle(attendedProg
										.getTrainingDatProgrammeDetail()
										.getTrainingDatRequestDetail()
										.getMasTrainingTopicName()
										.getTrainingTopicName());
								feedbackDetailBean
										.setProgramStartDate(attendedProg
												.getTrainingDatProgrammeDetail()
												.getTrainingStartDateTime());
								feedbackDetailBean.setProgId(attendedProg
										.getFkDatTrainingProgrammeId());
								feedbackDetailBean
										.setProgramModifiedDate(attendedProg
												.getTrainingDatProgrammeDetail()
												.getProgrammeModifiedDate());
								feedbackDetailBean
										.setDateDifferenceNow(dateDifferenceBwModifiedDateAndCurrentDate(attendedProg
												.getTrainingDatProgrammeDetail()
												.getProgrammeModifiedDate()));
								attendeeFeedback = trainingFeedBackRepository
										.findByFkTrainingDatProgrammeIdAndFkAttendeeEmpId(
												attendedProg
														.getFkDatTrainingProgrammeId(),
												trainingFeedbackInputBean
														.getEmpId());
								if (attendeeFeedback.size() > 0) {
									feedbackDetailBean.setFeedbackStatus(1);
								} else {
									feedbackDetailBean.setFeedbackStatus(0);
								}
								trainingFeedbackOutputList
										.add(feedbackDetailBean);
							}
						} else {
							throw new CommonCustomException(
									"No completed training programs");
						}
					}
				}
			} else {
				throw new DataAccessException(
						"Employee has not attended any Training Program.");
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("Get Attended Training Programs List and its Feedback Status based on Employee Id");
		return trainingFeedbackOutputList;
	}

	// Added by Balaji
	private Long dateDifferenceBwModifiedDateAndCurrentDate(
			Date programModifiedDate) {
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		String date = simpleDateFormat.format(programModifiedDate);

		LocalDate startDate = LocalDate.parse(date);
		LocalDate endtDate = LocalDate.now();
		Long range = ChronoUnit.DAYS.between(startDate, endtDate);
		return range;
	}

	// End of Addition by Balaji
	/**
	 * 
	 * @param status
	 * @param empId
	 * @param attendedProgDetails
	 * @return
	 * @throws CommonCustomException
	 * @Description : This method is used to get all attended training programs
	 *              and feedback status based on status.
	 */
	public List<TrainingFeedbackOutputBean> getAttendedTrainingProgramListBasedOnStatus(
			String status, int empId,
			List<TrainingDatNominationDetailBO> attendedProgDetails)
			throws CommonCustomException {
		LOG.startUsecase(" Attended Training Programs on Status");
		List<TrainingFeedbackOutputBean> trainingFeedbackOutputList = new ArrayList<TrainingFeedbackOutputBean>();
		TrainingFeedbackOutputBean feedbackDetailBean;
		List<TrainingDatAttendeeFeedbackBO> attendeeFeedback;

		try {
			if (attendedProgDetails.size() > 0) {
				for (TrainingDatNominationDetailBO attendedProg : attendedProgDetails) {
					if (attendedProg.getTrainingDatProgrammeDetail()
							.getFkProgrammeStatusId() == trainingProgramCompletedStatus) {
						feedbackDetailBean = new TrainingFeedbackOutputBean();
						feedbackDetailBean.setBriefTrainingContent(attendedProg
								.getTrainingDatProgrammeDetail()
								.getTrainingDatRequestDetail()
								.getBriefTrainingContent());
						feedbackDetailBean.setTitle(attendedProg
								.getTrainingDatProgrammeDetail()
								.getTrainingDatRequestDetail()
								.getMasTrainingTopicName()
								.getTrainingTopicName());
						feedbackDetailBean.setProgramStartDate(attendedProg
								.getTrainingDatProgrammeDetail()
								.getTrainingStartDateTime());
						feedbackDetailBean.setProgId(attendedProg
								.getFkDatTrainingProgrammeId());
						feedbackDetailBean.setProgramModifiedDate(attendedProg
								.getTrainingDatProgrammeDetail()
								.getProgrammeModifiedDate());
						feedbackDetailBean
								.setDateDifferenceNow(dateDifferenceBwModifiedDateAndCurrentDate(attendedProg
										.getTrainingDatProgrammeDetail()
										.getProgrammeModifiedDate()));
						attendeeFeedback = trainingFeedBackRepository
								.findByFkTrainingDatProgrammeIdAndFkAttendeeEmpId(
										attendedProg
												.getFkDatTrainingProgrammeId(),
										empId);
						if (status
								.equalsIgnoreCase(TrainingConstants.ALL_STATUS)) {
							if (attendeeFeedback.size() > 0) {
								feedbackDetailBean.setFeedbackStatus(1);
							} else {
								feedbackDetailBean.setFeedbackStatus(0);
							}
							trainingFeedbackOutputList.add(feedbackDetailBean);
						}
						if (status
								.equalsIgnoreCase(TrainingConstants.COMPLETED_STATUS)) {
							if (attendeeFeedback.size() > 0) {
								feedbackDetailBean.setFeedbackStatus(1);
								trainingFeedbackOutputList
										.add(feedbackDetailBean);
							}
						}
						if (status
								.equalsIgnoreCase(TrainingConstants.PENDING_FEEDBACK_STATUS)) {
							if (attendeeFeedback.size() == 0) {
								feedbackDetailBean.setFeedbackStatus(0);
								trainingFeedbackOutputList
										.add(feedbackDetailBean);
							}
						}
					}
				}
			} else {
				throw new CommonCustomException("No attended Training Programs");
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase(" Attended Training Programs on Status");
		return trainingFeedbackOutputList;
	}

	/**
	 * 
	 * @param trainingFeedbackInputBean
	 * @param attendedProgDetails
	 * @return
	 * @throws CommonCustomException
	 * @Description : This method is used to get attended training programs and
	 *              feedback status based on status and date range
	 */
	public List<TrainingFeedbackOutputBean> getAttendedTrainingProgramListBasedOnStatusAndDateRange(
			TrainingFeedbackInputBean trainingFeedbackInputBean,
			List<TrainingDatNominationDetailBO> attendedProgDetails)
			throws CommonCustomException {
		LOG.startUsecase("Attended Training Programs on Status and Date Range");
		List<TrainingFeedbackOutputBean> trainingFeedbackOutputListOnStatus = new ArrayList<TrainingFeedbackOutputBean>();
		List<TrainingFeedbackOutputBean> trainingFeedbackOutputList = new ArrayList<TrainingFeedbackOutputBean>();
		try {
			trainingFeedbackOutputListOnStatus = getAttendedTrainingProgramListBasedOnStatus(
					trainingFeedbackInputBean.getStatus(),
					trainingFeedbackInputBean.getEmpId(), attendedProgDetails);
			if (trainingFeedbackOutputListOnStatus.size() > 0) {
				for (TrainingFeedbackOutputBean feedback : trainingFeedbackOutputListOnStatus) {
					if (feedback.getProgramStartDate().getTime() >= trainingFeedbackInputBean
							.getStartDate().getTime()
							&& feedback.getProgramStartDate().getTime() <= trainingFeedbackInputBean
									.getEndDate().getTime()) {
						trainingFeedbackOutputList.add(feedback);
					}
				}
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("Attended Training Programs on Status and Date Range");
		return trainingFeedbackOutputList;
	}

	/**
	 * 
	 * @param progId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to Get number of Attendees for a
	 *              Training Program and out of that attendees how many has
	 *              provided the feedback
	 */
	public AttendedTrainingOutputBean getNumberofAttendeesGivenFeedback(
			int progId) throws BusinessException, CommonCustomException {
		LOG.startUsecase("Get Number of Attendees given feedback");
		AttendedTrainingOutputBean attendedTrainingBean = null;
		List<TrainingDatNominationDetailBO> attendedProgDetails;
		Integer feedbackGivenCount = 0;
		try {
			attendedProgDetails = trainingNominationRepository
					.findByFkDatTrainingProgrammeIdAndAttendedTraining(progId,
							"YES");
			if (attendedProgDetails != null && attendedProgDetails.size() > 0) {
				attendedTrainingBean = new AttendedTrainingOutputBean();
				attendedTrainingBean.setNoOfPaticipants(attendedProgDetails
						.size());
				for (TrainingDatNominationDetailBO attendedProgList : attendedProgDetails) {
					if (attendedProgList.getIsFeedbackProvided()
							.equalsIgnoreCase("YES"))
						feedbackGivenCount += 1;
				}
				attendedTrainingBean
						.setNoOfParticipantsGivenFeedback(feedbackGivenCount);
				attendedTrainingBean.setProgId(progId);
			}
		} catch (Exception e) {
			throw new CommonCustomException(
					"Exception Occured in Get Number of Attendees given feedback",
					e);
		}
		LOG.endUsecase("Get Number of Attendees given feedback");
		return attendedTrainingBean;
	}

	/**
	 * 
	 * @param feedbackInputBean
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to view Attendee Feedback
	 */
	public Page<AttendeeFeedbackOutputBean> getAttendeesFeedbackByTrainingProgramId(
			AttendeeFeedbackInputBean feedbackInputBean)
			throws BusinessException, DataAccessException {
		LOG.startUsecase("Attendees Feedback By Training Program Id");
		List<MasFeedbackQuestionInfoBO> questionType;
		List<Byte> feedbackQuestionType = new ArrayList<Byte>();
		feedbackQuestionType.add(overallRatingQuestionType);
		feedbackQuestionType.add(commentsQuestionType);
		List<TrainingDatNominationDetailBO> feedbackProvidedList;
		DatEmpPersonalDetailBO empPersonalDetail = new DatEmpPersonalDetailBO();
		DatEmpProfessionalDetailBO empProfessionalDetail = new DatEmpProfessionalDetailBO();
		TrainingDatProgrammeDetailBO programDetail;

		TrainingDatAttendeeFeedbackBO feedbackList;
		AttendeeFeedbackOutputBean output;
		List<AttendeeFeedbackOutputBean> outputBean = new ArrayList<AttendeeFeedbackOutputBean>();
		List<AttendeeFeedbackOutputBean> finalResultBean = new ArrayList<AttendeeFeedbackOutputBean>();

		Date modifiedStartDate = null;
		Date modifiedEndDate = null;

		Page finalPage;
		Pageable pageable = new PageRequest(feedbackInputBean.getPageNumber(),
				feedbackInputBean.getPageSize());

		LOG.debug("Get Attendees feedback details for a training program");
		try {
			programDetail = trainingProgramRepository
					.findByPkTrainingProgrammeId(feedbackInputBean.getProgId());

			questionType = feedbackQuestionInfoRepository
					.findByFkMasWorkFlowIdAndFeedbackQuestionStatusAndFeedbackQuestionTypeIn(
							workFlowId, activeQuestionStatus,
							feedbackQuestionType);
			if (questionType != null && questionType.size() > 0) {
				feedbackProvidedList = trainingNominationRepository
						.findByFkDatTrainingProgrammeIdAndIsFeedbackProvidedAndAttendedTrainingAndNominationStatus(
								feedbackInputBean.getProgId(), "YES", "YES",
								"NOMINATED");
				if (feedbackProvidedList != null
						&& feedbackProvidedList.size() > 0) {
					for (TrainingDatNominationDetailBO feedbackGiven : feedbackProvidedList) {
						output = new AttendeeFeedbackOutputBean();
						for (MasFeedbackQuestionInfoBO questionId : questionType) {
							feedbackList = trainingFeedBackRepository
									.findByFkTrainingDatProgrammeIdAndFkAttendeeEmpIdAndFkDatFeedbackQuestionInfoId(
											feedbackInputBean.getProgId(),
											feedbackGiven
													.getFkDatNominatedEmpId(),
											questionId
													.getPkFeedbackQuestionInfoId());
							empPersonalDetail = empPersonalDetailRepository
									.findByFkEmpDetailId(feedbackGiven
											.getFkDatNominatedEmpId());
							empProfessionalDetail = empProfessionalDetailRepository
									.findByFkMainEmpDetailId(feedbackGiven
											.getFkDatNominatedEmpId());
							if (feedbackList != null) {
								output.setProgId(feedbackInputBean.getProgId());
								output.setEmpId(feedbackGiven
										.getFkDatNominatedEmpId());
								output.setProgTitle(programDetail
										.getTrainingDatRequestDetail()
										.getMasTrainingTopicName()
										.getTrainingTopicName());
								output.setStartDate(programDetail
										.getTrainingStartDateTime());
								output.setEmpName(empPersonalDetail
										.getEmpFirstName()
										+ " "
										+ empPersonalDetail.getEmpLastName());
								output.setEmpMailId(empProfessionalDetail
										.getEmpProfessionalEmailId());
								output.setFeedbackGivenDate(feedbackList
										.getAttendeeFeedbackCreatedDate());
								if (feedbackList.getMasFeedbackQuestionInfo()
										.getFeedbackQuestionType() == 4) {
									output.setOverallRating(feedbackList
											.getAttendeeAnswer());
								} else if (feedbackList
										.getMasFeedbackQuestionInfo()
										.getFeedbackQuestionType() == 5) {
									output.setComments(feedbackList
											.getAttendeeAnswer());
								}
							} else {
								throw new DataAccessException(
										"Not able to get feedback details");
							}
						}
						outputBean.add(output);
					}
				}
			} else {
				throw new DataAccessException("Unable to get question types");
			}

			if (feedbackInputBean.getStartDate() != null
					&& feedbackInputBean.getEndDate() != null) {
				modifiedStartDate = DateTimeUtil
						.dateWithMinTimeofDay(feedbackInputBean.getStartDate());
				modifiedEndDate = DateTimeUtil
						.dateWithMaxTimeofDay(feedbackInputBean.getEndDate());
				feedbackInputBean.setStartDate(modifiedStartDate);
				feedbackInputBean.setEndDate(modifiedEndDate);
			}

			if (feedbackInputBean.getStatus() != null) {
				if (feedbackInputBean.getStartDate() != null
						&& feedbackInputBean.getEndDate() != null) {
					finalResultBean = getFeedbacksforProgramsBasedOnStatusAndDateRange(
							feedbackInputBean, outputBean);
				} else if (feedbackInputBean.getStatus() != null) {
					finalResultBean = getFeedbacksforProgramsBasedOnStatus(
							feedbackInputBean.getStatus(), outputBean);
				}
			}
			if (feedbackInputBean.getStatus() == null) {
				for (AttendeeFeedbackOutputBean feedbackOutput : outputBean) {
					if (feedbackOutput.getFeedbackGivenDate().getTime() >= feedbackInputBean
							.getStartDate().getTime()
							&& feedbackOutput.getFeedbackGivenDate().getTime() <= feedbackInputBean
									.getEndDate().getTime()) {
						finalResultBean.add(feedbackOutput);
					}
				}
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		finalPage = new PageImpl(finalResultBean, pageable,
				finalResultBean.size());
		LOG.endUsecase("Attendees Feedback By Training Program Id");
		return finalPage;
	}

	/**
	 * 
	 * @param status
	 * @param outputBean
	 * @return
	 * @Description : This method is used to get attendee's feedbacks for
	 *              Training Program based on status
	 */
	public List<AttendeeFeedbackOutputBean> getFeedbacksforProgramsBasedOnStatus(
			String status, List<AttendeeFeedbackOutputBean> outputBean) {
		LOG.startUsecase("Feedbacks for Programs Based On Status");
		List<AttendeeFeedbackOutputBean> finalResultBean = new ArrayList<AttendeeFeedbackOutputBean>();

		switch (status) {
		case TrainingConstants.ONE_STAR:

			for (AttendeeFeedbackOutputBean feedbackOutput : outputBean) {
				if (Integer.parseInt(feedbackOutput.getOverallRating()) == 1)
					finalResultBean.add(feedbackOutput);
			}
			break;

		case TrainingConstants.TWO_STAR:
			for (AttendeeFeedbackOutputBean feedbackOutput : outputBean) {
				if (Integer.parseInt(feedbackOutput.getOverallRating()) == 2)
					finalResultBean.add(feedbackOutput);
			}
			break;

		case TrainingConstants.THREE_STAR:
			for (AttendeeFeedbackOutputBean feedbackOutput : outputBean) {
				if (Integer.parseInt(feedbackOutput.getOverallRating()) == 3)
					finalResultBean.add(feedbackOutput);
			}
			break;

		case TrainingConstants.FOUR_STAR:
			for (AttendeeFeedbackOutputBean feedbackOutput : outputBean) {
				if (Integer.parseInt(feedbackOutput.getOverallRating()) == 4)
					finalResultBean.add(feedbackOutput);
			}
			break;

		case TrainingConstants.FIVE_STAR:
			for (AttendeeFeedbackOutputBean feedbackOutput : outputBean) {
				if (Integer.parseInt(feedbackOutput.getOverallRating()) == 5)
					finalResultBean.add(feedbackOutput);
			}
			break;

		case "ALL":

			finalResultBean.addAll(outputBean);

		}
		LOG.endUsecase("Feedbacks for Programs Based On Status");
		return finalResultBean;

	}

	/**
	 * 
	 * @param feedbackInputBean
	 * @param outputBean
	 * @return
	 * @Description : This method is used to get attendee's feedbacks for a
	 *              Training Program based on status and date range
	 */
	public List<AttendeeFeedbackOutputBean> getFeedbacksforProgramsBasedOnStatusAndDateRange(
			AttendeeFeedbackInputBean feedbackInputBean,
			List<AttendeeFeedbackOutputBean> outputBean) {
		LOG.startUsecase("Feedbacks for Programs Based On Status And DateRange");
		List<AttendeeFeedbackOutputBean> feedbackBasedOnStatusList = new ArrayList<AttendeeFeedbackOutputBean>();
		List<AttendeeFeedbackOutputBean> finalResultBean = new ArrayList<AttendeeFeedbackOutputBean>();

		feedbackBasedOnStatusList = getFeedbacksforProgramsBasedOnStatus(
				feedbackInputBean.getStatus(), outputBean);

		if (feedbackBasedOnStatusList.size() > 0) {
			for (AttendeeFeedbackOutputBean feedbackOutput : feedbackBasedOnStatusList) {

				if (feedbackOutput.getFeedbackGivenDate().getTime() >= feedbackInputBean
						.getStartDate().getTime()
						&& feedbackOutput.getFeedbackGivenDate().getTime() <= feedbackInputBean
								.getEndDate().getTime()) {
					finalResultBean.add(feedbackOutput);
				}
			}
		}
		LOG.endUsecase("Feedbacks for Programs Based On Status And DateRange");
		return finalResultBean;
	}

	/**
	 * 
	 * @param empId
	 * @param progId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to view the detailed feedback of a
	 *              employee.
	 */
	public TrainingDetailedFeedbackOutputBean getDetailedFeedback(int empId,
			int progId) throws DataAccessException {
		LOG.startUsecase("Get Detailed Feedback");
		MasFeedbackQuestionInfoBO questionType;
		List<TrainingDatAttendeeFeedbackBO> feedbackList;
		TrainingDetailedFeedbackOutputBean detailedFeedbackOutputBean = new TrainingDetailedFeedbackOutputBean();
		DatEmpPersonalDetailBO empPersonalDetail = new DatEmpPersonalDetailBO();
		DatEmpProfessionalDetailBO empProfessionalDetail = new DatEmpProfessionalDetailBO();
		TrainingDatProgrammeDetailBO progDetail = new TrainingDatProgrammeDetailBO();
		FeedbackResponsesBean feedbackBean = null;
		List<FeedbackResponsesBean> feedbackResponseList = new ArrayList<FeedbackResponsesBean>();

		LOG.debug("Detailed Feeback");
		try {
			questionType = feedbackQuestionInfoRepository
					.findByFkMasWorkFlowIdAndFeedbackQuestionStatusAndFeedbackQuestionType(
							workFlowId, activeQuestionStatus,
							commentsQuestionType);

			if (questionType != null) {
				feedbackList = trainingFeedBackRepository
						.findByFkTrainingDatProgrammeIdAndFkAttendeeEmpId(
								progId, empId);
				if (feedbackList.size() > 0 && feedbackList != null) {
					empPersonalDetail = empPersonalDetailRepository
							.findByFkEmpDetailId(empId);

					if (empPersonalDetail == null) {
						throw new DataAccessException(
								"Unable to get Employee Personal Details");
					}
					empProfessionalDetail = empProfessionalDetailRepository
							.findByFkMainEmpDetailId(empId);

					if (empProfessionalDetail == null) {
						throw new DataAccessException(
								"Unable to get Employee Professional Details");
					}
					progDetail = trainingProgramRepository
							.findByPkTrainingProgrammeId(progId);

					if (progDetail == null) {
						throw new DataAccessException(
								"Unable to get Training Program Details");
					}

					detailedFeedbackOutputBean = new TrainingDetailedFeedbackOutputBean();
					detailedFeedbackOutputBean.setEmpId(empId);
					detailedFeedbackOutputBean.setEmpName(empPersonalDetail
							.getEmpFirstName()
							+ " "
							+ empPersonalDetail.getEmpLastName());
					detailedFeedbackOutputBean
							.setEmpEmailId(empProfessionalDetail
									.getEmpProfessionalEmailId());
					detailedFeedbackOutputBean.setTrainingTitle(progDetail
							.getTrainingDatRequestDetail()
							.getMasTrainingTopicName().getTrainingTopicName());
					detailedFeedbackOutputBean.setTrainingStartDate(progDetail
							.getTrainingStartDateTime());
					detailedFeedbackOutputBean.setProgId(progId);

					for (TrainingDatAttendeeFeedbackBO res : feedbackList) {

						if (res.getFkDatFeedbackQuestionInfoId() == questionType
								.getPkFeedbackQuestionInfoId()) {
							detailedFeedbackOutputBean.setComments(res
									.getAttendeeAnswer());
						} else {
							feedbackBean = new FeedbackResponsesBean();
							feedbackBean.setQuestionType(res
									.getMasFeedbackQuestionInfo()
									.getFeedbackQuestionType());
							feedbackBean.setQuestionName(res
									.getMasFeedbackQuestionInfo()
									.getFeedbackQuestionName());
							feedbackBean.setAnswer(res.getAttendeeAnswer());
							feedbackResponseList.add(feedbackBean);
						}
					}
					detailedFeedbackOutputBean
							.setFeedbackList(feedbackResponseList);
				} else {
					throw new DataAccessException("Feedback details not found");
				}
			} else {
				throw new DataAccessException(
						"Unable to get Feedback Question Details");
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("Get Detailed Feedback");
		return detailedFeedbackOutputBean;
	}

	/**
	 * 
	 * @param feedbackInput
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to get all Training programs and
	 *              attendees feedback
	 */
	public Page<TrainingCoordinatorViewFeedbackBean> getAllAttendeeFeedback(
			TrainingPostFeedbackInputBean feedbackInput)
			throws DataAccessException {
		LOG.startUsecase("Get All Attendee Feedback");
		List<TrainingDatProgrammeDetailBO> progDetails = null;
		TrainingCoordinatorViewFeedbackBean feedBackBean;
		List<TrainingDatAttendeeFeedbackBO> attendeesFeedbackList;
		MasFeedbackQuestionInfoBO questionInfo;
		List<TrainingCoordinatorViewFeedbackBean> feedBackList = new ArrayList<TrainingCoordinatorViewFeedbackBean>();
		LOG.debug("View All Training Programs and feedback details");

		Date modifiedStartDate = null;
		Date modifiedEndDate = null;

		Page finalPage;
		Pageable pageable = new PageRequest(feedbackInput.getPageNumber(),
				feedbackInput.getPageSize());
		try {
			progDetails = trainingProgramRepository
					.findByFkProgrammeStatusId(trainingProgramCompletedStatus);

			questionInfo = feedbackQuestionInfoRepository
					.findByFkMasWorkFlowIdAndFeedbackQuestionStatusAndFeedbackQuestionType(
							workFlowId, activeQuestionStatus,
							overallRatingQuestionType);

			if (feedbackInput.getStartDate() != null
					&& feedbackInput.getEndDate() != null) {
				modifiedStartDate = DateTimeUtil
						.dateWithMinTimeofDay(feedbackInput.getStartDate());
				modifiedEndDate = DateTimeUtil
						.dateWithMaxTimeofDay(feedbackInput.getEndDate());
				feedbackInput.setStartDate(modifiedStartDate);
				feedbackInput.setEndDate(modifiedEndDate);
			}

			if (progDetails.size() > 0) {
				if (feedbackInput.getStatus() != null) {
					if (feedbackInput.getStartDate() != null
							&& feedbackInput.getEndDate() != null) {
						feedBackList = getAteendeesFeedbackForAllTrainingProgramsOnStatusAndDateRange(
								progDetails, questionInfo, feedbackInput);
					} else {
						feedBackList = getAteendeesFeedbackForAllTrainingProgramsOnStatus(
								progDetails, feedbackInput.getStatus(),
								questionInfo);
					}
				} else if (feedbackInput.getStatus() == null) {
					for (TrainingDatProgrammeDetailBO progDetail : progDetails) {
						int rating = 0;
						int noOfPersons = 0;
						float averageRating = 0.0f;
						if (progDetail.getTrainingEndDateTime().getTime() >= feedbackInput
								.getStartDate().getTime()
								&& progDetail.getTrainingEndDateTime()
										.getTime() <= feedbackInput
										.getEndDate().getTime()) {
							feedBackBean = new TrainingCoordinatorViewFeedbackBean();
							attendeesFeedbackList = trainingFeedBackRepository
									.findByFkTrainingDatProgrammeId(progDetail
											.getPkTrainingProgrammeId());
							feedBackBean.setTitle(progDetail
									.getTrainingDatRequestDetail()
									.getMasTrainingTopicName()
									.getTrainingTopicName());
							feedBackBean.setDescription(progDetail
									.getTrainingDatRequestDetail()
									.getBriefTrainingContent());
							feedBackBean.setStartDate(progDetail
									.getTrainingStartDateTime());
							feedBackBean.setTrainerName(progDetail
									.getTrainerName());
							feedBackBean.setProgId(progDetail
									.getPkTrainingProgrammeId());
							feedBackBean.setFeedbackStatus("VIEW FEEDBACK");

							if (attendeesFeedbackList.size() > 0) {
								for (TrainingDatAttendeeFeedbackBO feedbackInfo : attendeesFeedbackList) {
									if (feedbackInfo
											.getFkDatFeedbackQuestionInfoId() == questionInfo
											.getPkFeedbackQuestionInfoId()) {
										rating = rating
												+ Integer.parseInt(feedbackInfo
														.getAttendeeAnswer());
										noOfPersons = noOfPersons + 1;
									}
								}
								feedBackBean.setFeedbackStatus("VIEW FEEDBACK");
								averageRating = rating / noOfPersons;
								feedBackBean.setOverallRating(Math
										.round(averageRating) + "");
								feedBackBean.setProgId(progDetail
										.getPkTrainingProgrammeId());
								feedBackList.add(feedBackBean);
							} else {
								feedBackBean
										.setFeedbackStatus("PENDING FEEDBACK");
								feedBackBean.setOverallRating(0 + "");
								feedBackList.add(feedBackBean);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured in Get All Programs and Attendee Feedback",
					e);
		}
		finalPage = new PageImpl(feedBackList, pageable, feedBackList.size());
		LOG.endUsecase("Get All Attendee Feedback");
		return finalPage;
	}

	/**
	 * 
	 * @param progDetails
	 * @param status
	 * @param questionInfo
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to get attendees feedback for all
	 *              training programs based on status
	 */
	public List<TrainingCoordinatorViewFeedbackBean> getAteendeesFeedbackForAllTrainingProgramsOnStatus(
			List<TrainingDatProgrammeDetailBO> progDetails, String status,
			MasFeedbackQuestionInfoBO questionInfo) throws DataAccessException {
		LOG.startUsecase("Get Attendees Feedback For All Training Programs On Status");
		TrainingCoordinatorViewFeedbackBean feedBackBean;
		List<TrainingDatAttendeeFeedbackBO> attendeesFeedbackList;
		List<TrainingCoordinatorViewFeedbackBean> feedBackList = new ArrayList<TrainingCoordinatorViewFeedbackBean>();
		try {
			if (status.equalsIgnoreCase("COMPLETED")) {
				for (TrainingDatProgrammeDetailBO progDetail : progDetails) {
					int rating = 0;
					int noOfPersons = 0;
					float averageRating = 0.0f;
					feedBackBean = new TrainingCoordinatorViewFeedbackBean();
					attendeesFeedbackList = trainingFeedBackRepository
							.findByFkTrainingDatProgrammeId(progDetail
									.getPkTrainingProgrammeId());
					if (attendeesFeedbackList.size() > 0) {
						feedBackBean.setTitle(progDetail
								.getTrainingDatRequestDetail()
								.getMasTrainingTopicName()
								.getTrainingTopicName());
						feedBackBean.setDescription(progDetail
								.getTrainingDatRequestDetail()
								.getBriefTrainingContent());
						feedBackBean.setStartDate(progDetail
								.getTrainingStartDateTime());
						feedBackBean
								.setTrainerName(progDetail.getTrainerName());
						feedBackBean.setFeedbackStatus("VIEW FEEDBACK");
						feedBackBean.setProgId(progDetail
								.getPkTrainingProgrammeId());
						for (TrainingDatAttendeeFeedbackBO feedbackInfo : attendeesFeedbackList) {
							if (feedbackInfo.getFkDatFeedbackQuestionInfoId() == questionInfo
									.getPkFeedbackQuestionInfoId()) {
								rating = rating
										+ Integer.parseInt(feedbackInfo
												.getAttendeeAnswer());
								noOfPersons = noOfPersons + 1;
							}
						}
						averageRating = rating / noOfPersons;
						feedBackBean.setOverallRating(Math.round(averageRating)
								+ "");
						feedBackList.add(feedBackBean);
					}
				}
			}
			if (status.equalsIgnoreCase("PENDING FEEDBACK")) {
				for (TrainingDatProgrammeDetailBO progDetail : progDetails) {
					feedBackBean = new TrainingCoordinatorViewFeedbackBean();
					attendeesFeedbackList = trainingFeedBackRepository
							.findByFkTrainingDatProgrammeId(progDetail
									.getPkTrainingProgrammeId());
					if (attendeesFeedbackList.size() == 0) {
						feedBackBean.setTitle(progDetail
								.getTrainingDatRequestDetail()
								.getMasTrainingTopicName()
								.getTrainingTopicName());
						feedBackBean.setDescription(progDetail
								.getTrainingDatRequestDetail()
								.getBriefTrainingContent());
						feedBackBean.setStartDate(progDetail
								.getTrainingStartDateTime());
						feedBackBean
								.setTrainerName(progDetail.getTrainerName());
						feedBackBean.setFeedbackStatus("PENDING FEEDBACK");
						feedBackBean.setProgId(progDetail
								.getPkTrainingProgrammeId());
						feedBackBean.setOverallRating(0 + "");
						feedBackList.add(feedBackBean);
					}
				}
			}
			if (status.equalsIgnoreCase("ALL")) {
				for (TrainingDatProgrammeDetailBO progDetail : progDetails) {
					int rating = 0;
					int noOfPersons = 0;
					float averageRating = 0.0f;
					feedBackBean = new TrainingCoordinatorViewFeedbackBean();
					attendeesFeedbackList = trainingFeedBackRepository
							.findByFkTrainingDatProgrammeId(progDetail
									.getPkTrainingProgrammeId());
					feedBackBean.setTitle(progDetail
							.getTrainingDatRequestDetail()
							.getMasTrainingTopicName().getTrainingTopicName());
					feedBackBean.setDescription(progDetail
							.getTrainingDatRequestDetail()
							.getBriefTrainingContent());
					feedBackBean.setStartDate(progDetail
							.getTrainingStartDateTime());
					feedBackBean.setTrainerName(progDetail.getTrainerName());
					feedBackBean.setProgId(progDetail
							.getPkTrainingProgrammeId());

					if (attendeesFeedbackList.size() > 0) {
						for (TrainingDatAttendeeFeedbackBO feedbackInfo : attendeesFeedbackList) {
							if (feedbackInfo.getFkDatFeedbackQuestionInfoId() == questionInfo
									.getPkFeedbackQuestionInfoId()) {
								rating = rating
										+ Integer.parseInt(feedbackInfo
												.getAttendeeAnswer());
								noOfPersons = noOfPersons + 1;
							}
						}
						feedBackBean.setFeedbackStatus("VIEW FEEDBACK");
						averageRating = rating / noOfPersons;
						feedBackBean.setOverallRating(Math.round(averageRating)
								+ "");
						feedBackList.add(feedBackBean);
					} else {
						feedBackBean.setFeedbackStatus("PENDING FEEDBACK");
						feedBackBean.setOverallRating(0 + "");
						feedBackList.add(feedBackBean);
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured in Get All Programs and Attendee Feedback based on status",
					e);
		}
		LOG.endUsecase("Get Attendees Feedback For All Training Programs On Status");
		return feedBackList;
	}

	/**
	 * 
	 * @param progDetails
	 * @param questionInfo
	 * @param feedbackInput
	 * @return
	 * @throws DataAccessException
	 * @Description: This method is used to get all Training Programs based on
	 *               status and date range
	 */
	public List<TrainingCoordinatorViewFeedbackBean> getAteendeesFeedbackForAllTrainingProgramsOnStatusAndDateRange(
			List<TrainingDatProgrammeDetailBO> progDetails,
			MasFeedbackQuestionInfoBO questionInfo,
			TrainingPostFeedbackInputBean feedbackInput)
			throws DataAccessException {
		LOG.startUsecase("Get Attendees Feedback For All Training Programs On Status And Date Range");
		List<TrainingCoordinatorViewFeedbackBean> viewFeedbackOnStatus = new ArrayList<TrainingCoordinatorViewFeedbackBean>();
		List<TrainingCoordinatorViewFeedbackBean> feedbackList = new ArrayList<TrainingCoordinatorViewFeedbackBean>();
		TrainingDatProgrammeDetailBO progDetail;
		try {
			viewFeedbackOnStatus = getAteendeesFeedbackForAllTrainingProgramsOnStatus(
					progDetails, feedbackInput.getStatus(), questionInfo);
			if (viewFeedbackOnStatus.size() > 0) {
				for (TrainingCoordinatorViewFeedbackBean feedback : viewFeedbackOnStatus) {
					progDetail = trainingProgramRepository
							.findByPkTrainingProgrammeId(feedback.getProgId());
					if (progDetail != null) {
						if (progDetail.getTrainingEndDateTime().getTime() >= feedbackInput
								.getStartDate().getTime()
								&& progDetail.getTrainingEndDateTime()
										.getTime() <= feedbackInput
										.getEndDate().getTime()) {
							feedbackList.add(feedback);
						}
					}
				}
			}

		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured in Get All Programs and Attendee Feedback based on status and Date range",
					e);
		}
		LOG.endUsecase("Get Attendees Feedback For All Training Programs On Status And Date Range");
		return feedbackList;
	}

	/**
	 * 
	 * @param progId
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to get Attendees Feedback Status for a
	 *              Training Program.
	 */
	public List<AttendeeFeedbackStatusBean> getAttendeesFeedbackStatus(
			int progId) throws DataAccessException {
		LOG.startUsecase("Entering getAttendeesFeedbackStatus");
		TrainingDatProgrammeDetailBO progDetail = null;
		List<TrainingDatNominationDetailBO> nominationDetails = null;
		List<AttendeeFeedbackStatusBean> feedbackList = new ArrayList<AttendeeFeedbackStatusBean>();
		DatEmpPersonalDetailBO empPersonalDetail = null;
		AttendeeFeedbackStatusBean feedbackBean = null;
		try {
			progDetail = trainingProgramRepository
					.findByPkTrainingProgrammeId(progId);
			if (progDetail != null) {
				nominationDetails = trainingNominationRepository
						.findByFkDatTrainingProgrammeIdAndAttendedTraining(
								progDetail.getPkTrainingProgrammeId(), "YES");
				if (nominationDetails.size() > 0 || nominationDetails != null) {
					for (TrainingDatNominationDetailBO nominationDetail : nominationDetails) {
						feedbackBean = new AttendeeFeedbackStatusBean();
						feedbackBean.setEmpId(nominationDetail
								.getFkDatNominatedEmpId());
						feedbackBean.setFeedbackStatus(nominationDetail
								.getIsFeedbackProvided());

						empPersonalDetail = empPersonalDetailRepository
								.findByFkEmpDetailId(nominationDetail
										.getFkDatNominatedEmpId());

						if (empPersonalDetail != null) {
							feedbackBean.setEmpName(empPersonalDetail
									.getEmpFirstName()
									+ " "
									+ empPersonalDetail.getEmpLastName());
						} else {
							throw new DataAccessException(
									"Exception occured while getting employee personal details");
						}
						feedbackList.add(feedbackBean);
					}
				}
			} else {
				throw new DataAccessException(
						"Unable to find Program Details for the training program");
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("Exiting getAttendeesFeedbackStatus");
		return feedbackList;
	}

	// End of Addition by Kamal Anand

	// Added by Smrithi

	/**
	 * @author THBS
	 * @param attendeesFeedbackBean
	 * @return
	 * @throws DataAccessException
	 * @throws ParseException
	 * @throws MethodArgumentNotValidException
	 * @Description: This method is used to create attendees feedback.
	 */
	public List<TrainingDatAttendeeFeedbackBO> createAttendeesFeedback(
			List<TrainingCreateAttendeesFeedbackBean> attendeesFeedbackBean)
			throws DataAccessException, ParseException,
			MethodArgumentNotValidException, CommonCustomException {
		List<TrainingDatAttendeeFeedbackBO> trainingAttendeeFeedbackBO = new ArrayList<TrainingDatAttendeeFeedbackBO>();
		TrainingDatAttendeeFeedbackBO trainingAttendeeFeedbackBO1;
		List<TrainingDatAttendeeFeedbackBO> checkFeedbackDetails = null;
		TrainingDatNominationDetailBO nominationDetails = null;
		LOG.startUsecase("create Attendees Feedback");
		TrainingDatNominationDetailBO nominationDetail = null;
		DatEmpProfessionalDetailBO profDetails = null;
		List<Short> trainingTeamRoleIdList = Arrays
				.asList(trainingTeamRoleIds.split(",")).stream()
				.map(Short::valueOf).collect(Collectors.toList());
		try {

			checkFeedbackDetails = trainingFeedBackRepository
					.findByFkTrainingDatProgrammeIdAndFkAttendeeEmpId(
							attendeesFeedbackBean.get(0)
									.getTrainingDatProgrammeId(),
							attendeesFeedbackBean.get(0).getAttendeeEmpId());

			nominationDetails = trainingNominationRepository
					.findByFkDatTrainingProgrammeIdAndFkDatNominatedEmpIdAndAttendedTraining(
							attendeesFeedbackBean.get(0)
									.getTrainingDatProgrammeId(),
							attendeesFeedbackBean.get(0).getAttendeeEmpId(),
							"YES");

			if (nominationDetails != null) {
				if (checkFeedbackDetails.size() == 0
						|| checkFeedbackDetails == null) {
					for (int i = 0; i < attendeesFeedbackBean.size(); i++) {
						trainingAttendeeFeedbackBO1 = new TrainingDatAttendeeFeedbackBO();

						trainingAttendeeFeedbackBO1
								.setFkTrainingDatProgrammeId(attendeesFeedbackBean
										.get(i).getTrainingDatProgrammeId());
						trainingAttendeeFeedbackBO1
								.setAttendeeAnswer(attendeesFeedbackBean.get(i)
										.getAttendeeAnswer());
						trainingAttendeeFeedbackBO1
								.setAttendeeFeedbackCreatedDate(new Date());
						trainingAttendeeFeedbackBO1
								.setFkAttendeeEmpId(attendeesFeedbackBean
										.get(i).getAttendeeEmpId());
						trainingAttendeeFeedbackBO1
								.setFkDatFeedbackQuestionInfoId(attendeesFeedbackBean
										.get(i).getFeedbackQuestionInfoId());
						trainingFeedBackRepository
								.save(trainingAttendeeFeedbackBO1);
						trainingAttendeeFeedbackBO
								.add(trainingAttendeeFeedbackBO1);

						if (trainingAttendeeFeedbackBO.size() > 0) {
							nominationDetail = trainingNominationRepository
									.findByFkDatNominatedEmpIdAndFkDatTrainingProgrammeId(
											attendeesFeedbackBean.get(0)
													.getAttendeeEmpId(),
											attendeesFeedbackBean
													.get(0)
													.getTrainingDatProgrammeId());

							if (nominationDetail != null) {
								nominationDetail.setIsFeedbackProvided("YES");
								trainingNominationRepository
										.save(nominationDetail);
							} else {
								throw new DataAccessException(
										"Exception Occured while getting nomination details");
							}
						}
					}
				} else {
					throw new CommonCustomException(
							"Employee has already given the feedback for this training Program");
				}
			} else {
				throw new CommonCustomException(
						"Employee has not attended the training program");
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		//Added by Kamal Anand for Notifications
		try {
			profDetails = empProfessionalDetailRepository
					.findByFkMainEmpDetailId(attendeesFeedbackBean.get(0)
							.getAttendeeEmpId());
			if (nominationDetails.getTrainingDatProgrammeDetail()
					.getTrainingDatRequestDetail()
					.getFkTrainingRequesterEmpId() != null) {
				if (nominationDetails.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getFkTrainingRequesterEmpId().intValue() == profDetails
						.getFkEmpReportingMgrId().intValue()) {
					notificationService
							.setTrainingFeedbackProvidedNotificationToRmScreen(
									nominationDetails
											.getTrainingDatProgrammeDetail()
											.getTrainingDatRequestDetail()
											.getFkTrainingRequesterEmpId(),
									attendeesFeedbackBean.get(0)
											.getTrainingDatProgrammeId());
				}
			} else {
				List<Integer> empIdList = empDetailRepository
						.getActiveEmployeesForRoleId(trainingTeamRoleIdList);
				if (empIdList != null) {
					empIdList
							.forEach(empId -> {
								try {
									notificationService
											.setTrainingFeedbackProvidedNotificationToTCScreen(
													empId,
													attendeesFeedbackBean
															.get(0)
															.getTrainingDatProgrammeId());
								} catch (Exception e) {
									LOG.info("Some exception occurred while setting Attendee Feedback Notifications");
								}
							});
				}
			}
		} catch(Exception e) {
			LOG.info("Some Exception occurred while setting Attendee Feedback Notifications");
		}
		//End of Addition by Kamal Anand for Notifications
		LOG.endUsecase("create Attendees Feedback");
		return trainingAttendeeFeedbackBO;
	}
	// EOA by Smrithi
}
