/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingProgramService.java                                		 */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  22-Jun-2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 22-Jun-2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.training.service;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.MasTrainingTopicNameBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.mail.EmailServiceUtil;
import com.thbs.mis.framework.report.Util.DownloadFileUtil;
import com.thbs.mis.framework.util.DateTimeUtil;
import com.thbs.mis.notificationframework.trainingnotification.service.TrainingNotificationService;
import com.thbs.mis.training.bean.AddParticipantsOutputBean;
import com.thbs.mis.training.bean.AttendedEmployeeDetailsBean;
import com.thbs.mis.training.bean.CreateTrainingProgramBean;
import com.thbs.mis.training.bean.DetailedTrainingOutputBean;
import com.thbs.mis.training.bean.EmployeeAttendedDetailsBean;
import com.thbs.mis.training.bean.NominationDetailsUploadExcelBean;
import com.thbs.mis.training.bean.PagenationBean;
import com.thbs.mis.training.bean.TeamTrainingRequestBean;
import com.thbs.mis.training.bean.TeamTrainingRequestOutputBean;
import com.thbs.mis.training.bean.TrainingCalendarOutputBean;
import com.thbs.mis.training.bean.TrainingDatNominationDetailBean;
import com.thbs.mis.training.bean.TrainingDatNominationPoolDetailBean;
import com.thbs.mis.training.bean.TrainingDatProgrammeDetailBean;
import com.thbs.mis.training.bean.TrainingDatSubNominationPoolDetailBean;
import com.thbs.mis.training.bean.TrainingDetailsOutputBean;
import com.thbs.mis.training.bean.TrainingNominateBean;
import com.thbs.mis.training.bean.TrainingNominationBean;
import com.thbs.mis.training.bean.TrainingProgStatusBean;
import com.thbs.mis.training.bean.TrainingProgramBean;
import com.thbs.mis.training.bean.TrainingProgramDetailBean;
import com.thbs.mis.training.bean.TrainingProgramDetailOutputBean;
import com.thbs.mis.training.bean.TrainingProgramOutputBean;
import com.thbs.mis.training.bean.TrainingProgramViewBean;
import com.thbs.mis.training.bean.TrainingProgrammeDetailInputBean;
import com.thbs.mis.training.bean.TrainingRequestDetailBean;
import com.thbs.mis.training.bean.TrainingRequestDetailInputBean;
import com.thbs.mis.training.bean.TrainingRequestDetailSubInputBean;
import com.thbs.mis.training.bean.TrainingRequestandProgramdetailsBean;
import com.thbs.mis.training.bean.UpdateTrainingProgramBean;
import com.thbs.mis.training.bo.TrainingDatNominationDetailBO;
import com.thbs.mis.training.bo.TrainingDatNominationPoolDetailBO;
import com.thbs.mis.training.bo.TrainingDatPostApplicationFeedbackBO;
import com.thbs.mis.training.bo.TrainingDatProgrammeDetailBO;
import com.thbs.mis.training.bo.TrainingDatRequestDetailBO;
import com.thbs.mis.training.bo.TrainingMasProgrammeStatusBO;
import com.thbs.mis.training.constants.TrainingConstants;
import com.thbs.mis.training.dao.TrainingNominationPoolRepository;
import com.thbs.mis.training.dao.TrainingNominationRepository;
import com.thbs.mis.training.dao.TrainingPostFeedbackRepository;
import com.thbs.mis.training.dao.TrainingProgramSpecification;
import com.thbs.mis.training.dao.TrainingProgramStatusRepository;
import com.thbs.mis.training.dao.TrainingProgrammeRepository;
import com.thbs.mis.training.dao.TrainingRequestRepository;
import com.thbs.mis.training.dao.TrainingTopicsRepository;

/**
 * <Description TrainingProgramService:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
@Service
public class TrainingProgramService {

	// Added by Kamal Anand

	private static final AppLog LOG = LogFactory
			.getLog(TrainingProgramService.class);

	@Autowired
	private TrainingProgrammeRepository trainingProgramRepository;

	@Autowired
	private TrainingRequestRepository trainingRequestRepository;

	@Autowired
	private TrainingNominationRepository trainingNominationRepository;

	@Autowired
	private EmployeePersonalDetailsRepository empPersonalRepository;

	@Autowired
	private EmpDetailRepository empDetailRepository;

	@Autowired
	private EmployeeProfessionalRepository empProfessionalRepository;

	@Autowired
	private TrainingNominationPoolRepository nominationPoolRepository;

	@Autowired
	private TrainingTopicsRepository topicRepository;

	@Autowired
	private TrainingPostFeedbackRepository feedbackRepository;

	@Autowired
	private EmailServiceUtil emailService;

	@Value("${training.program.completed.status}")
	private byte programCompletedStatus;

	@Value("${training.program.cancelled.status}")
	private byte programCancelledStatus;

	@Value("${training.program.cancelled.template.path}")
	private File programCancelledTemplate;

	@Value("${training.nomination.pool.approved.status}")
	private byte nominationPoolApprovedStatus;

	@Value("${training.nomination.pool.rejected.status}")
	private byte nominationPoolRejectedStatus;

	@Value("${training.request.approved.status}")
	private byte requestApprovedStatus;

	@Value("${training.program.planned.status}")
	private byte programPlannedStatus;

	@Value("${training.program.completed.status}")
	private byte trainingProgramCompletedStatus;

	@Value("${training.program.agenda.file.basepath}")
	private String programAgendaDirPath;
	
	@Value("${training.coordinator.and.manager.role}")
	private String trainingCoordinatorAndManagerRole;
	
	@Value("${training.nomination.pool.interested.status}")
	private byte nominationPoolInterestedStatus;
	
	@Autowired
	private TrainingNotificationService notificationService;

	@Autowired
	private Validator validator;

	// Added by Pratibha
	@Autowired
	private TrainingProgramStatusRepository repository;

	// EOA by Pratibha

	// Added by Kamal Anand
	/**
	 * 
	 * @param trainingProgBean
	 * @return
	 * @throws CommonCustomException
	 * @Description : This method is used for creating a new Training Program
	 */
	public TrainingDatProgrammeDetailBO createTrainingProgram(
			CreateTrainingProgramBean trainingProgBean, MultipartFile file)
			throws CommonCustomException, ConstraintViolationException,
			IOException {

		LOG.startUsecase("Entering Create Training Program Service");
		TrainingDatProgrammeDetailBO programDetailResultBO = null;
		TrainingDatProgrammeDetailBO programDetail = new TrainingDatProgrammeDetailBO();
		TrainingDatRequestDetailBO requestDetail = new TrainingDatRequestDetailBO();
		TrainingDatProgrammeDetailBO progDetail = null;
		TrainingDatRequestDetailBO reqDetails = new TrainingDatRequestDetailBO();
		Set violations = validator.validate(trainingProgBean);
		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(violations);
		} else {
			programDetail.setFkTrainingRequestId(trainingProgBean
					.getTrainingRequestId());
			programDetail.setTrainingStartDateTime(trainingProgBean
					.getTrainingStartDate());
			programDetail.setTrainingEndDateTime(trainingProgBean
					.getTrainingEndDate());
			programDetail.setTrainingDurationInHours(trainingProgBean
					.getTrainingDurationInHrs());
			programDetail.setTrainingDurationInDays(trainingProgBean
					.getTrainingDurationInDays());
			programDetail.setTrainingNoOfHrsPerDay(trainingProgBean
					.getTrainingNoOfHrsPerDay());
			programDetail.setTrainerType(trainingProgBean.getTrainerType());
			programDetail.setTrainerName(trainingProgBean.getTrainerName());
			programDetail.setMaximumSeats(trainingProgBean.getMaxSeats());
			programDetail.setFkTrainingRoomId(trainingProgBean
					.getTrainingRoomId());
			programDetail.setExternalVenueName(trainingProgBean
					.getExternalVenueName());
			programDetail.setFkProgrammeStatusId(trainingProgBean
					.getProgramStatus());
			programDetail.setComments(trainingProgBean.getComments());
			programDetail.setTrainingIntimatedStatus(trainingProgBean
					.getTrainingIntimatedStatus());
			programDetail.setFkProgrammeCreatedBy(trainingProgBean
					.getProgramCreatedBy());
			programDetail.setProgrammeCreatedDate(new Date());
			if (file != null) {
				programDetail.setIsAgendaUploaded("YES");
				String mFileExtension = StringUtils.substringAfter(
						file.getOriginalFilename(), ".");
				if (!mFileExtension.equals("pdf")
						&& !mFileExtension.equals("doc")
						&& !mFileExtension.equals("jpg")
						&& !mFileExtension.equals("jpeg")
						&& !mFileExtension.equals("png")) {
					throw new CommonCustomException(
							"This file type is not allowed. Please choose different one.");
				}
			} else
				programDetail.setIsAgendaUploaded("NO");
			try {
				progDetail = trainingProgramRepository
						.findByFkTrainingRequestId(trainingProgBean
								.getTrainingRequestId());

				reqDetails = trainingRequestRepository
						.findByPkTrainingRequestId(trainingProgBean
								.getTrainingRequestId());

				if (progDetail == null) {
					if (reqDetails.getFkTrainingRequestStatusId() == requestApprovedStatus) {
						programDetailResultBO = trainingProgramRepository
								.save(programDetail);
						if (programDetailResultBO != null) {
							requestDetail = trainingRequestRepository
									.findByPkTrainingRequestId(programDetailResultBO
											.getFkTrainingRequestId());
							requestDetail.setIsTrainingProgramCreated("YES");
							trainingRequestRepository.save(requestDetail);
						}
						try {
							if(file != null)
							{
								String extension = StringUtils.substringAfter(
										file.getOriginalFilename(), ".");
								String newFileName = programDetailResultBO
										.getPkTrainingProgrammeId()
										+ "_"
										+ requestDetail.getMasTrainingTopicName()
												.getTrainingTopicName();
								File fileToBeStored = new File(programAgendaDirPath
										+ newFileName + "." + extension);
								FileCopyUtils.copy(file.getBytes(), fileToBeStored);
							}
						} catch (IOException e) {
							throw new IOException(e.getMessage());
						}
					} else {
						throw new CommonCustomException(
								"Training request is not yet approved");
					}
				} else {
					throw new CommonCustomException(
							"Training Program has already been created for the training request.");
				}
			} catch (CommonCustomException e) {
				throw new CommonCustomException(e.getMessage());
			}
		}
		LOG.info("Exiting Create Training Program Service");
		//Added by Kamal Anand for Notifications
		try {
			notificationService
					.setProgramCreatedForRequestNotificationToRequestorScreen(
							reqDetails.getFkTrainingRequesterEmpId(),
							programDetailResultBO.getPkTrainingProgrammeId());
		} catch(Exception e) {
			LOG.info("Some Exception occurred while setting Training Program created notification");
		}
		//End of Addition by Kamal Anand for Notifications
		return programDetailResultBO;
	}

	/**
	 * 
	 * @param trainingProgBean
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to update the existing Training
	 *              Program details
	 */
	public TrainingDatProgrammeDetailBO updateTrainingProgram(
			UpdateTrainingProgramBean trainingProgBean, MultipartFile file)
			throws DataAccessException, CommonCustomException, IOException,
			ConstraintViolationException {

		LOG.startUsecase("Entering Update Training Progam Service");

		TrainingDatProgrammeDetailBO programDetail = new TrainingDatProgrammeDetailBO();
		TrainingDatProgrammeDetailBO programDetailResultBO = null;
		boolean isFileFound = false;

		try {
			Set violations = validator.validate(trainingProgBean);
			if (!violations.isEmpty()) {
				throw new ConstraintViolationException(violations);
			} else {
				programDetail = trainingProgramRepository
						.findByPkTrainingProgrammeId(trainingProgBean
								.getPkTrainingProgramId());

				if (programDetail != null) {
					if (programDetail.getFkProgrammeStatusId() != programCompletedStatus) {
						programDetail.setPkTrainingProgrammeId(trainingProgBean
								.getPkTrainingProgramId());

						programDetail.setPkTrainingProgrammeId(trainingProgBean
								.getPkTrainingProgramId());

						if (trainingProgBean.getTrainingStartDate() != null)
							programDetail
									.setTrainingStartDateTime(trainingProgBean
											.getTrainingStartDate());

						if (trainingProgBean.getTrainingEndDate() != null)
							programDetail
									.setTrainingEndDateTime(trainingProgBean
											.getTrainingEndDate());

						if (trainingProgBean.getTrainingDurationInHrs() != null)
							programDetail
									.setTrainingDurationInHours(trainingProgBean
											.getTrainingDurationInHrs());
						if (trainingProgBean.getTrainingDurationInDays() != null)
							programDetail
									.setTrainingDurationInDays(trainingProgBean
											.getTrainingDurationInDays());

						if (trainingProgBean.getTrainingNoOfHrsPerDay() != null)
							programDetail
									.setTrainingNoOfHrsPerDay(trainingProgBean
											.getTrainingNoOfHrsPerDay());

						if (trainingProgBean.getTrainerType() != null)
							programDetail.setTrainerType(trainingProgBean
									.getTrainerType());

						if (trainingProgBean.getTrainerName() != null)
							programDetail.setTrainerName(trainingProgBean
									.getTrainerName());

						if (trainingProgBean.getMaxSeats() != null)
							programDetail.setMaximumSeats(trainingProgBean
									.getMaxSeats());

						if (trainingProgBean.getTrainingRoomId() > 0)
							programDetail.setFkTrainingRoomId(trainingProgBean
									.getTrainingRoomId());

						if (trainingProgBean.getExternalVenueName() != null)
							programDetail.setExternalVenueName(trainingProgBean
									.getExternalVenueName());
						if (trainingProgBean.getProgramStatus() > 0)
							programDetail
									.setFkProgrammeStatusId(trainingProgBean
											.getProgramStatus());

						if (trainingProgBean.getComments() != null)
							programDetail.setComments(trainingProgBean
									.getComments());

						if (trainingProgBean.getTrainingIntimatedStatus() != null)
							programDetail
									.setTrainingIntimatedStatus(trainingProgBean
											.getTrainingIntimatedStatus());

						if (trainingProgBean.getProgramModifiedBy() != null)
							programDetail
									.setFkProgrammeModifyBy(trainingProgBean
											.getProgramModifiedBy());

						programDetail.setProgrammeModifiedDate(new Date());

						if (file != null) {
							String mFileExtension = StringUtils.substringAfter(
									file.getOriginalFilename(), ".");
							if (!mFileExtension.equals("pdf")
									&& !mFileExtension.equals("doc")
									&& !mFileExtension.equals("jpg")
									&& !mFileExtension.equals("jpeg")
									&& !mFileExtension.equals("png")) {
								throw new CommonCustomException(
										"This file type is not allowed. Please choose different one.");
							} else {
								programDetail.setIsAgendaUploaded("YES");
								try {
									String fileName = trainingProgBean
											.getPkTrainingProgramId()
											.toString();
									File dir = new File(programAgendaDirPath);
									String[] listFiles = dir.list();
									for (String s : listFiles) {
										String start = StringUtils
												.substringBefore(s, "_");
										if (start.trim()
												.equals(fileName.trim())) {
											isFileFound = true;
											boolean res = DownloadFileUtil
													.deleteFileFromServer(
															programAgendaDirPath,
															s);
											if (res) {
												String extension = StringUtils
														.substringAfter(
																file.getOriginalFilename(),
																".");
												String newFileName = programDetail
														.getPkTrainingProgrammeId()
														+ "_"
														+ programDetail
																.getTrainingDatRequestDetail()
																.getMasTrainingTopicName()
																.getTrainingTopicName();
												File fileToBeStored = new File(
														programAgendaDirPath
																+ newFileName
																+ "."
																+ extension);
												FileCopyUtils.copy(
														file.getBytes(),
														fileToBeStored);
												break;
											} else {
												throw new CommonCustomException(
														"Some exception occured in file updation");
											}
										}
									}
									if (!isFileFound) {
										String extension = StringUtils
												.substringAfter(file
														.getOriginalFilename(),
														".");
										String newFileName = programDetail
												.getPkTrainingProgrammeId()
												+ "_"
												+ programDetail
														.getTrainingDatRequestDetail()
														.getMasTrainingTopicName()
														.getTrainingTopicName();
										File fileToBeStored = new File(
												programAgendaDirPath
														+ newFileName + "."
														+ extension);
										FileCopyUtils.copy(file.getBytes(),
												fileToBeStored);
									}
								} catch (IOException e) {
									throw new IOException(e.getMessage());
								}
							}
							programDetailResultBO = trainingProgramRepository
									.save(programDetail);

						} else {
							programDetailResultBO = trainingProgramRepository
									.save(programDetail);
						}
					} else {
						throw new CommonCustomException(
								"Training program details cannot be modified.");
					}
				} else {
					throw new DataAccessException(
							"Exception Occured in update training program details");
				}
				LOG.info("Exiting save()");
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception Occured in update training program details: ", e);
		}
		return programDetailResultBO;
	}

	/**
	 * 
	 * @return finalPage
	 * @throws DataAccessException
	 * @Description : This method is used to view all Training Programs
	 */
	public Page<TrainingProgramViewBean> viewAllTrainingPrograms(
			PagenationBean pageBean) throws DataAccessException {
		List<TrainingDatProgrammeDetailBO> trainingProgramDetailsList = null;
		List<TrainingProgramViewBean> progList = new ArrayList<TrainingProgramViewBean>();
		TrainingProgramViewBean progDetails = null;
		Page finalPage;
		Pageable pageable = new PageRequest(pageBean.getPageNumber(),
				pageBean.getPageSize());
		LOG.debug("View All Training Programs");
		try {
			trainingProgramDetailsList = trainingProgramRepository.findAll();

			if (trainingProgramDetailsList != null) {

				if (trainingProgramDetailsList.size() > 0) {
					for (TrainingDatProgrammeDetailBO programDetail : trainingProgramDetailsList) {
						progDetails = new TrainingProgramViewBean();
						progDetails.setProgStartDate(programDetail
								.getTrainingStartDateTime());
						progDetails.setProgramTitle(programDetail
								.getTrainingDatRequestDetail()
								.getMasTrainingTopicName()
								.getTrainingTopicName());
						progDetails.setProgDesc(programDetail
								.getTrainingDatRequestDetail()
								.getBriefTrainingContent());
						progDetails.setTrainerName(programDetail
								.getTrainerName());
						progDetails
								.setMaxSeats(programDetail.getMaximumSeats());
						progDetails.setProgId(programDetail
								.getPkTrainingProgrammeId());
						progDetails.setTrainingStatus(programDetail
								.getFkProgrammeStatusId());
						progDetails.setTrainingStatusName(programDetail
								.getTrainingMasProgrammeStatus()
								.getTrainingProgrammeStatusName());
						progList.add(progDetails);
					}
				}
			} else {
				throw new DataAccessException("Exception Occured");
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		finalPage = new PageImpl(progList, pageable,
				trainingProgramDetailsList.size());
		return finalPage;
	}

	/**
	 * 
	 * @param trainingProgramBean
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to view Training Programs based on
	 *              Status or Date Range
	 */
	public Page<TrainingProgramViewBean> viewAllTrainingProgramsByStatusDateRange(
			TrainingProgramBean trainingProgramBean) throws DataAccessException {
		List<TrainingDatProgrammeDetailBO> trainingProgramDetailsList = null;

		Byte progStatus = 0;
		if (trainingProgramBean.getStatus() != null)
			progStatus = Byte.parseByte(trainingProgramBean.getStatus() + "");
		Page finalPage;
		Pageable pageable = new PageRequest(
				trainingProgramBean.getPageNumber(),
				trainingProgramBean.getPageSize());
		TrainingProgramSpecification progSpec = new TrainingProgramSpecification();
		List<TrainingProgramViewBean> progList = new ArrayList<TrainingProgramViewBean>();
		TrainingProgramViewBean progDetails = null;
		Date modifiedStartDate = null;
		Date modifiedEndDate = null;

		LOG.debug("View All Training Programs by Status and Date Range");
		try {
			if (trainingProgramBean.getStartDate() != null
					&& trainingProgramBean.getEndDate() != null) {
				modifiedStartDate = DateTimeUtil
						.dateWithMinTimeofDay(trainingProgramBean
								.getStartDate());
				modifiedEndDate = DateTimeUtil
						.dateWithMaxTimeofDay(trainingProgramBean.getEndDate());
				trainingProgramBean.setStartDate(modifiedStartDate);
				trainingProgramBean.setEndDate(modifiedEndDate);
			}
			if (trainingProgramBean.getStatus() != null) {
				/*
				 * trainingProgramDetailsList = trainingProgramRepository
				 * .findByFkProgrammeStatusId((byte) trainingProgramBean
				 * .getStatus());
				 */
				if (trainingProgramBean.getStartDate() != null
						&& trainingProgramBean.getEndDate() != null) {

					if (trainingProgramBean.getStatus() == 0) {
						trainingProgramDetailsList = trainingProgramRepository
								.findByTrainingStartDateTimeBetween(
										trainingProgramBean.getStartDate(),
										trainingProgramBean.getEndDate());
					}
					if (trainingProgramBean.getStatus() > 0) {

						trainingProgramDetailsList = trainingProgramRepository
								.findByFkProgrammeStatusIdAndTrainingStartDateTimeBetween(
										progStatus,
										trainingProgramBean.getStartDate(),
										trainingProgramBean.getEndDate());
					}
				} else {
					if (trainingProgramBean.getStatus() == 0) {
						trainingProgramDetailsList = trainingProgramRepository
								.findAll();
					}
					if (trainingProgramBean.getStatus() > 0) {
						trainingProgramDetailsList = trainingProgramRepository
								.findByFkProgrammeStatusId(progStatus);
					}
				}
			}
			if (trainingProgramDetailsList.size() > 0) {
				for (TrainingDatProgrammeDetailBO programDetail : trainingProgramDetailsList) {
					progDetails = new TrainingProgramViewBean();
					progDetails.setProgStartDate(programDetail
							.getTrainingStartDateTime());
					progDetails.setProgramTitle(programDetail
							.getTrainingDatRequestDetail()
							.getMasTrainingTopicName().getTrainingTopicName());
					progDetails.setProgDesc(programDetail
							.getTrainingDatRequestDetail()
							.getBriefTrainingContent());
					progDetails.setTrainerName(programDetail.getTrainerName());
					progDetails.setMaxSeats(programDetail.getMaximumSeats());
					progDetails.setProgId(programDetail
							.getPkTrainingProgrammeId());
					progDetails.setTrainingStatus(programDetail
							.getFkProgrammeStatusId());
					progDetails.setTrainingStatusName(programDetail
							.getTrainingMasProgrammeStatus()
							.getTrainingProgrammeStatusName());

					progList.add(progDetails);
				}
			}

			/*
			 * Specification<TrainingDatProgrammeDetailBO>
			 * searchByStatusAndDateRangeSpec = progSpec
			 * .getTrainingProgramsByStatusAndDateRange(trainingProgramBean);
			 * trainingProgramDetailsList = trainingProgramRepository
			 * .findAll(searchByStatusAndDateRangeSpec);
			 */
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured in View All Training Programs by Status and Date Range: ",
					e);
		}
		finalPage = new PageImpl(progList, pageable,
				trainingProgramDetailsList.size());
		return finalPage;
	}

	/**
	 * 
	 * @param mgrId
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to view all the Requested Training
	 *              Programs of a Reporting Manager
	 */
	public List<TrainingProgramViewBean> viewAllRequestedTrainingProgram(
			int mgrId) throws DataAccessException {
		List<TrainingDatProgrammeDetailBO> trainingProgramDetailsList = new ArrayList<TrainingDatProgrammeDetailBO>();
		List<TrainingDatRequestDetailBO> trainingRequestDetailsList = null;
		TrainingDatProgrammeDetailBO trainingProgramDetail;
		List<TrainingProgramViewBean> progList = new ArrayList<TrainingProgramViewBean>();
		TrainingProgramViewBean progDetails = null;
		LOG.debug("View All Requested Training Programs");
		try {
			trainingRequestDetailsList = trainingRequestRepository
					.findByFkTrainingRequesterEmpIdAndIsTrainingProgramCreated(
							mgrId, "YES");
			if (trainingRequestDetailsList.size() > 0) {
				for (TrainingDatRequestDetailBO requestDetail : trainingRequestDetailsList) {
					trainingProgramDetail = new TrainingDatProgrammeDetailBO();
					trainingProgramDetail = trainingProgramRepository
							.findByFkTrainingRequestId(requestDetail
									.getPkTrainingRequestId());
					trainingProgramDetailsList.add(trainingProgramDetail);
				}
			}
			if (trainingProgramDetailsList.size() > 0) {
				for (TrainingDatProgrammeDetailBO programDetail : trainingProgramDetailsList) {
					progDetails = new TrainingProgramViewBean();
					progDetails.setProgStartDate(programDetail
							.getTrainingStartDateTime());
					progDetails.setProgramTitle(programDetail
							.getTrainingDatRequestDetail()
							.getMasTrainingTopicName().getTrainingTopicName());
					progDetails.setProgDesc(programDetail
							.getTrainingDatRequestDetail()
							.getBriefTrainingContent());
					progDetails.setTrainerName(programDetail.getTrainerName());
					progDetails.setMaxSeats(programDetail.getMaximumSeats());
					progDetails.setProgId(programDetail
							.getPkTrainingProgrammeId());
					progDetails.setTrainingStatus(programDetail
							.getFkProgrammeStatusId());
					progDetails.setTrainingStatusName(programDetail
							.getTrainingMasProgrammeStatus()
							.getTrainingProgrammeStatusName());
					if(programDetail.getTrainingDatRequestDetail().getProgramCategory() != null)
					{
						if(programDetail.getTrainingDatRequestDetail().getProgramCategory() == 1)	
							progDetails.setProgrammeCategory("SOFT SKILLS");
						if(programDetail.getTrainingDatRequestDetail().getProgramCategory() == 1)
							progDetails.setProgrammeCategory("TECHNICAL");
						if(programDetail.getTrainingDatRequestDetail().getProgramCategory() == 1)
							progDetails.setProgrammeCategory("PROCESS");				 
					}
					
					progList.add(progDetails);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured in View All Requested Training Programs: ",
					e);
		}
		return progList;
	}

	/**
	 * 
	 * @param nominationList
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to Nominate the participants to
	 *              Training Program
	 */
	public List<TrainingDatNominationDetailBO> nominateParticipants(
			List<TrainingNominateBean> nominationList)
			throws DataAccessException {
		TrainingDatProgrammeDetailBO trainingProg;
		List<TrainingDatNominationDetailBO> finalNominatedList = new ArrayList<TrainingDatNominationDetailBO>();
		TrainingDatNominationDetailBO nominate;
		TrainingDatNominationDetailBO finalNominate = new TrainingDatNominationDetailBO();
		TrainingDatNominationPoolDetailBO poolDetail;

		TrainingDatNominationPoolDetailBO saveNominationPoolDetail;

		TrainingDatNominationPoolDetailBO nominationPoolDetails;

		List<TrainingDatNominationDetailBO> nominatedList;

		LOG.debug("Nominate Participants");
		try {
			trainingProg = trainingProgramRepository
					.findByPkTrainingProgrammeId(nominationList.get(0)
							.getProgramId());

			if (trainingProg != null) {
				nominatedList = trainingNominationRepository
						.findByFkDatTrainingProgrammeIdAndNominationStatus(
								trainingProg.getPkTrainingProgrammeId(),
								"NOMINATED");
			} else {
				throw new DataAccessException(
						"Exception occured while retrieving training program details");
			}

			if (trainingProg != null
					&& trainingProg.getMaximumSeats() > (nominatedList.size() + nominationList
							.size())) {

				for (TrainingNominateBean nominationDetail : nominationList) {

					poolDetail = new TrainingDatNominationPoolDetailBO();

					poolDetail.setFkTrainingProgrammeId(nominationDetail
							.getProgramId());
					poolDetail.setNominationPoolCreatedDate(new Date());
					poolDetail.setNominationStatus((byte) 3);
					poolDetail.setApprovedOrRejectedDate(new Date());
					poolDetail.setFkApprovedOrRejectedBy(nominationList.get(0)
							.getNominatedByEmpId());
					poolDetail.setFkApproverEmpId(nominationList.get(0)
							.getNominatedByEmpId());
					poolDetail.setFkNominatedEmpId(nominationDetail
							.getNominatedEmpId());

					saveNominationPoolDetail = nominationPoolRepository
							.save(poolDetail);

					if (saveNominationPoolDetail != null) {

						nominationPoolDetails = nominationPoolRepository
								.findByFkNominatedEmpIdAndFkTrainingProgrammeId(
										nominationDetail.getNominatedEmpId(),
										nominationDetail.getProgramId());

						if (nominationPoolDetails != null) {
							nominate = new TrainingDatNominationDetailBO();
							nominate.setFkDatTrainingProgrammeId(nominationDetail
									.getProgramId());
							nominate.setFkNominationPoolId(nominationPoolDetails
									.getPkTrainingNominationPoolDetailId());
							nominate.setFkDatNominatedEmpId(nominationDetail
									.getNominatedEmpId());
							nominate.setNominationDate(new Date());
							nominate.setFkNominatedByEmpId(nominationDetail
									.getNominatedByEmpId());
							nominate.setNominationStatus("NOMINATED");
							nominate.setAttendedTraining("NO");
							nominate.setIsFeedbackProvided("NO");
							finalNominate = trainingNominationRepository
									.save(nominate);
							finalNominatedList.add(finalNominate);
						}
					}
				}
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		return finalNominatedList;
	}

	/**
	 * 
	 * @param progId
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to get all the reportee's who have
	 *              attended the Training Program along with Training Program
	 *              Details
	 */
	public TrainingNominationBean getTrainingProgramAndAttendees(int progId)
			throws DataAccessException {

		TrainingDatProgrammeDetailBO progDetail;
		TrainingNominationBean resultBean = new TrainingNominationBean();
		List<TrainingDatNominationDetailBO> nominationDetails;
		DatEmpPersonalDetailBO empDetail;
		DatEmpPersonalDetailBO intiatedbyDetail;
		AttendedEmployeeDetailsBean attendedEmpDetail = null;
		List<AttendedEmployeeDetailsBean> attendeeList = new ArrayList<AttendedEmployeeDetailsBean>();

		LOG.debug("Get Attendees list for a Training Program");
		try {
			progDetail = trainingProgramRepository
					.findByPkTrainingProgrammeId(progId);

			if (progDetail != null) {
				resultBean.setTrainingTitle(progDetail
						.getTrainingDatRequestDetail()
						.getMasTrainingTopicName().getTrainingTopicName());
				resultBean.setTrainingStartDate(progDetail
						.getTrainingStartDateTime());
				resultBean.setTrainerName(progDetail.getTrainerName());
				resultBean.setVenue(progDetail.getTrainingMasRoomDetail()
						.getTrainingRoomName());
				resultBean.setTime(progDetail.getTrainingStartDateTime());
				resultBean.setProgId(progId);
				if(progDetail.getTrainingDatRequestDetail().getProgramCategory() != null)
				{
					if(progDetail.getTrainingDatRequestDetail().getProgramCategory() == 1)	
						resultBean.setProgrammeCategory("SOFT SKILLS");
					if(progDetail.getTrainingDatRequestDetail().getProgramCategory() == 1)
						resultBean.setProgrammeCategory("TECHNICAL");
					if(progDetail.getTrainingDatRequestDetail().getProgramCategory() == 1)
						resultBean.setProgrammeCategory("PROCESS");				 
				}
				nominationDetails = trainingNominationRepository
						.findByFkDatTrainingProgrammeIdAndAttendedTrainingAndFkNominatedByEmpId(
								progId, "YES", progDetail
										.getTrainingDatRequestDetail()
										.getFkTrainingRequesterEmpId());

				if (nominationDetails != null && nominationDetails.size() > 0) {
					for (TrainingDatNominationDetailBO nominations : nominationDetails) {
						attendedEmpDetail = new AttendedEmployeeDetailsBean();
						empDetail = new DatEmpPersonalDetailBO();
						empDetail = empPersonalRepository
								.findByFkEmpDetailId(nominations
										.getFkDatNominatedEmpId());
						attendedEmpDetail.setEmpName(empDetail
								.getEmpFirstName()
								+ " "
								+ empDetail.getEmpLastName());
						attendedEmpDetail.setEmpId(nominations
								.getFkDatNominatedEmpId());
						attendeeList.add(attendedEmpDetail);
					}
					resultBean.setAttendedEmpList(attendeeList);
					intiatedbyDetail = empPersonalRepository
							.findByFkEmpDetailId(progDetail
									.getTrainingDatRequestDetail()
									.getFkTrainingRequesterEmpId());
					resultBean.setIntiatedBy(intiatedbyDetail.getEmpFirstName()
							+ " " + intiatedbyDetail.getEmpLastName());
				} else {
					throw new DataAccessException(
							"No attendees for the training program");
				}
			} else {
				throw new DataAccessException(
						"Exception occured while retrieving programme detail");
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		return resultBean;
	}

	/**
	 * 
	 * @param empId
	 * @param progId
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to get the nomination status of the
	 *              employee based on Training Program Id
	 */
	public AddParticipantsOutputBean getEmployeeTrainingNominationStatus(
			int empId, int progId) throws DataAccessException {
		DatEmpPersonalDetailBO empPersonalDetail;
		DatEmpProfessionalDetailBO empProfessionalDetail;
		TrainingDatNominationPoolDetailBO nominationPoolDetail;
		AddParticipantsOutputBean participantsBean = new AddParticipantsOutputBean();
		LOG.debug("Get Employee Nomination Status");
		try {

			nominationPoolDetail = nominationPoolRepository
					.findByFkNominatedEmpIdAndFkTrainingProgrammeId(empId,
							progId);

			empPersonalDetail = empPersonalRepository
					.findByFkEmpDetailId(empId);

			empProfessionalDetail = empProfessionalRepository
					.findByFkMainEmpDetailId(empId);

			participantsBean.setEmpName(empPersonalDetail.getEmpFirstName()
					+ " " + empPersonalDetail.getEmpLastName());
			participantsBean.setEmailId(empProfessionalDetail
					.getEmpProfessionalEmailId());
			participantsBean.setEmpId(empId);
			participantsBean.setProgId(progId);
			if (nominationPoolDetail != null) {
				byte nominationStatus = nominationPoolDetail
						.getNominationStatus();

				if (nominationStatus == 1)
					participantsBean.setNominationStatus("INTERESTED");
				if (nominationStatus == 2)
					participantsBean.setNominationStatus("NOT INTERESTED");
				if (nominationStatus == 3)
					participantsBean.setNominationStatus("APPROVED");
				if (nominationStatus == 4)
					participantsBean.setNominationStatus("REJECTED");
				if (nominationStatus == 5)
					participantsBean.setNominationStatus("WITHDRAWN");
				if (nominationStatus == 6)
					participantsBean.setNominationStatus("INVITED");
				if (nominationStatus == 7)
					participantsBean.setNominationStatus("DECISION NOT TAKEN");
				if (nominationStatus == 8)
					participantsBean.setNominationStatus("DECLINE");
			} else {
				participantsBean.setNominationStatus("SEND INVITE");
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured in Get Employee Nomination Status: ", e);
		}
		return participantsBean;
	}

	/**
	 * 
	 * @param mgrId
	 * @param progId
	 * @return
	 * @throws DataAccessException
	 * @Desciption : This method is used to get all Reportees and their
	 *             nomination status for a particular Training Program
	 */
	public List<AddParticipantsOutputBean> getAllReporteesAndTrainingProgramNominationStatus(
			Integer mgrId, Integer progId) throws DataAccessException,
			CommonCustomException {

		List<DatEmpProfessionalDetailBO> reporteesList;
		AddParticipantsOutputBean participantsBean = null;
	    List<AddParticipantsOutputBean> participantsList = new ArrayList<AddParticipantsOutputBean>();
		TrainingDatProgrammeDetailBO progDetail = null;
		LOG.debug("Get All Reportees Nomination Status");
		try {
			
			 
			/*reporteesList = empProfessionalRepository
					.findByFkEmpReportingMgrId(mgrId);	*/
			
			reporteesList = empProfessionalRepository
			.getActiveReportingMgr(mgrId);	
								
		if (reporteesList.size() > 0) {
			
			progDetail = trainingProgramRepository
					.findByPkTrainingProgrammeId(progId);
			
				if (progDetail != null) {
					
					if (progDetail.getTrainingDatRequestDetail()
							.getFkTrainingRequesterEmpId() == null
							|| mgrId.equals(progDetail
									.getTrainingDatRequestDetail()  
									.getFkTrainingRequesterEmpId()) ) {
						for (DatEmpProfessionalDetailBO reportee : reporteesList) {

							if (progDetail
									.getTrainingDatRequestDetail()
									.getLevelOfParticipants()
									.contains(
											reportee.getMasEmpLevelBO()
													.getEmpLevelName())) {
								participantsBean = new AddParticipantsOutputBean();
								participantsBean = getEmployeeTrainingNominationStatus(
										reportee.getFkMainEmpDetailId(), progId);
								participantsList.add(participantsBean);
							}
						}
					}				
					else {
						throw new CommonCustomException(
								"Manager has not requested for this training program");
					}
				}
			}
			} catch (CommonCustomException e) {
			throw new CommonCustomException(
					"Manager has not requested for this training program: ", e);
		}
		return participantsList;
	}

	/**
	 * 
	 * @param progId
	 * @return
	 * @throws DataAccessException
	 * @Description : This service is used to get all active employees and their
	 *              nomination status for a Training Program
	 */
	public List<AddParticipantsOutputBean> getAllActiveEmployeesAndTrainingProgramNominationStatus(
			int progId) throws DataAccessException {
		List<DatEmpDetailBO> activeEmpList = null;
		DatEmpProfessionalDetailBO empProfDetail = null;
		TrainingDatProgrammeDetailBO progDetail = null;
		AddParticipantsOutputBean participantsBean = null;
		List<AddParticipantsOutputBean> participantsList = new ArrayList<AddParticipantsOutputBean>();
		final byte activeEmployeeStatus = 1;
		try {
			activeEmpList = empDetailRepository
					.findByFkEmpMainStatus(activeEmployeeStatus);

			if (activeEmpList != null) {
				if (activeEmpList.size() > 0 || activeEmpList != null) {
					progDetail = trainingProgramRepository
							.findByPkTrainingProgrammeId(progId);

					if (progDetail != null) {
						for (DatEmpDetailBO empDetail : activeEmpList) {
							empProfDetail = empProfessionalRepository
									.findByFkMainEmpDetailId(empDetail
											.getPkEmpId());
							if (empProfDetail != null) {
								if (progDetail
										.getTrainingDatRequestDetail()
										.getLevelOfParticipants()
										.contains(
												empProfDetail
														.getMasEmpLevelBO()
														.getEmpLevelName())) {
									participantsBean = new AddParticipantsOutputBean();
									participantsBean = getEmployeeTrainingNominationStatus(
											empDetail.getPkEmpId(), progId);
									participantsList.add(participantsBean);
								}
							} else {
								throw new DataAccessException(
										"Erroe while getting Employee Professional Details");
							}
						}
					} else {
						throw new DataAccessException(
								"Erroe while getting Program Details");
					}
				}
			} else {
				throw new DataAccessException(
						"Error while getting active employee list");
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		return participantsList;
	}

	/**
	 * 
	 * @param progId
	 * @param empId
	 * @return progDetail
	 * @throws DataAccessException
	 * @Description : This method is used to retrieve Training Program Details
	 *              and also approval status of the employee for a training
	 *              program
	 */
	public TrainingProgramDetailBean viewProgramDetailsForTeamTrainingRequests(
			int progId, int empId) throws DataAccessException,
			CommonCustomException {
		TrainingDatNominationPoolDetailBO nominationPoolDetails = null;
		DatEmpPersonalDetailBO empPersonalDetails = null;
		TrainingProgramDetailBean progDetail = null;
		DatEmpPersonalDetailBO intiatedByDetails = null;
		LOG.debug("Get Employee Approval Status for a Training Program");
		try {
			nominationPoolDetails = nominationPoolRepository
					.findByFkNominatedEmpIdAndFkTrainingProgrammeId(empId,
							progId);
			
			if (nominationPoolDetails != null) {
				empPersonalDetails = empPersonalRepository
						.findByFkEmpDetailId(empId);
				if (empPersonalDetails != null) {
					progDetail = new TrainingProgramDetailBean();
					progDetail.setTrainingTitle(nominationPoolDetails
							.getTrainingDatProgrammeDetail()
							.getTrainingDatRequestDetail()
							.getMasTrainingTopicName().getTrainingTopicName());
					progDetail.setDate(nominationPoolDetails
							.getTrainingDatProgrammeDetail()
							.getTrainingStartDateTime());
					progDetail.setTrainerName(nominationPoolDetails
							.getTrainingDatProgrammeDetail().getTrainerName());
					progDetail.setVenue(nominationPoolDetails
							.getTrainingDatProgrammeDetail()
							.getTrainingMasRoomDetail().getTrainingRoomName());
					progDetail.setEmpName(empPersonalDetails.getEmpFirstName()
							+ " " + empPersonalDetails.getEmpLastName());
					progDetail.setNoOfParticipants(nominationPoolDetails
							.getTrainingDatProgrammeDetail()
							.getTrainingDatRequestDetail()
							.getNoOfParticipants());
					progDetail.setProgId(progId);

					intiatedByDetails = empPersonalRepository
							.findByFkEmpDetailId(nominationPoolDetails
									.getTrainingDatProgrammeDetail()
									.getTrainingDatRequestDetail()
									.getFkTrainingRequesterEmpId());

					progDetail.setStartTime(nominationPoolDetails
							.getTrainingDatProgrammeDetail()
							.getTrainingStartDateTime());
					progDetail.setIntiatedBy(intiatedByDetails
							.getEmpFirstName()
							+ " "
							+ intiatedByDetails.getEmpLastName());
					progDetail.setDescription(nominationPoolDetails
							.getTrainingDatProgrammeDetail()
							.getTrainingDatRequestDetail()
							.getBriefTrainingContent());

					if (nominationPoolDetails.getNominationStatus() == 1) {
						progDetail.setApprovalStatus("NOT APPROVED");
					}
					if (nominationPoolDetails.getNominationStatus() == 3) {
						progDetail.setApprovalStatus("APPROVED");
					}
					if (nominationPoolDetails.getNominationStatus() == 4) {
						progDetail.setApprovalStatus("REJECTED");
					}
					// added by pratibha
					if(nominationPoolDetails.getTrainingDatProgrammeDetail().getTrainingDatRequestDetail().getProgramCategory() != null)
					{
						if(nominationPoolDetails.getTrainingDatProgrammeDetail().getTrainingDatRequestDetail().getProgramCategory() == 1)	
							progDetail.setProgrammeCategory("SOFT SKILLS");
						if(nominationPoolDetails.getTrainingDatProgrammeDetail().getTrainingDatRequestDetail().getProgramCategory() == 2)
							progDetail.setProgrammeCategory("TECHNICAL");
						if(nominationPoolDetails.getTrainingDatProgrammeDetail().getTrainingDatRequestDetail().getProgramCategory() == 3)
							progDetail.setProgrammeCategory("PROCESS");				 
					}
					//EOA
				}
			} else {
				throw new DataAccessException(
						"Exception occured while retrieving Nomination pool details");
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured in Get Employee Approval Status for a Training Program: ",
					e);
		}
		return progDetail;
	}

	/**
	 * 
	 * @param statusBean
	 * @return programDetails
	 * @throws DataAccessException
	 * @Description : This method is used to update the Training Program Status.
	 */
	public TrainingDatProgrammeDetailBO updateTrainingProgramStatus(
			TrainingProgStatusBean statusBean) throws DataAccessException {

		TrainingDatProgrammeDetailBO programDetails = null;
		TrainingDatProgrammeDetailBO progDetail = new TrainingDatProgrammeDetailBO();
		List<TrainingDatNominationDetailBO> nominationDetailsList = null;
		DatEmpPersonalDetailBO empPersonalDetails = null;
		DatEmpProfessionalDetailBO empProfessionalDetails = null;
		List<String> to;
		List<String> cc;
		LOG.debug("Update Training Program Status");
		try {

			progDetail = trainingProgramRepository
					.findByPkTrainingProgrammeId(statusBean.getProgId());

			if (progDetail != null) {
				progDetail.setComments(statusBean.getComments());
				progDetail
						.setFkProgrammeStatusId((byte) statusBean.getStatus());
				progDetail.setPkTrainingProgrammeId(statusBean.getProgId());
				progDetail.setFkProgrammeModifyBy(statusBean.getEmpId());
				progDetail.setProgrammeModifiedDate(new Date());

				programDetails = trainingProgramRepository.save(progDetail);

				if (programDetails != null) {

					if (statusBean.getStatus() == programCancelledStatus) {

						nominationDetailsList = trainingNominationRepository
								.findByFkDatTrainingProgrammeIdAndNominationStatus(
										statusBean.getProgId(), "NOMINATED");

						if (nominationDetailsList.size() > 0) {
							for (TrainingDatNominationDetailBO nominationDetail : nominationDetailsList) {
								to = new ArrayList<String>();
								cc = new ArrayList<String>();

								empPersonalDetails = empPersonalRepository
										.findByFkEmpDetailId(nominationDetail
												.getFkDatNominatedEmpId());

								empProfessionalDetails = empProfessionalRepository
										.findByFkMainEmpDetailId(nominationDetail
												.getFkDatNominatedEmpId());

								if (empPersonalDetails == null) {
									throw new DataAccessException(
											"Some Error Occured while getting employee personal details");
								}
								if (empProfessionalDetails == null) {
									throw new DataAccessException(
											"Some Error Occured while getting employee professional details");
								}

								to.add(empProfessionalDetails
										.getEmpProfessionalEmailId());
								emailService.sendMail(
										to,
										cc,
										progDetail
												.getTrainingDatRequestDetail()
												.getMasTrainingTopicName()
												.getTrainingTopicName()
												+ " Program Cancelled",
										programCancelledTemplate.getPath(),
										empPersonalDetails.getEmpFirstName()
												+ " "
												+ empPersonalDetails
														.getEmpLastName(),
										progDetail
												.getTrainingDatRequestDetail()
												.getMasTrainingTopicName()
												.getTrainingTopicName(),
										progDetail.getTrainingStartDateTime(),
										progDetail.getTrainingEndDateTime(),
										progDetail.getTrainingMasRoomDetail()
												.getTrainingRoomName(),
										progDetail.getTrainerName());
							}
						}
					}
				} else {
					throw new DataAccessException(
							"Some Error Occured while updating Training Program Status");
				}
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		} catch (Exception e) {
			throw new DataAccessException(
					"Some Exception happened while sending mail");
		}
		return programDetails;
	}

	/**
	 * 
	 * @param progId
	 * @return progInfo
	 * @throws DataAccessException
	 * @Description : This method is used to get Program Details based on
	 *              Program Id
	 */
	public TrainingDatProgrammeDetailBO getProgramId(int progId)
			throws DataAccessException {
		LOG.startUsecase("Entered getRequestId");
		TrainingDatProgrammeDetailBO progInfo = new TrainingDatProgrammeDetailBO();
		try {
			progInfo = trainingProgramRepository
					.findByPkTrainingProgrammeId(progId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get the data from Database");
		}
		LOG.endUsecase("Exited getRequestId");
		return progInfo;

	}

	/**
	 * 
	 * @param statusId
	 * @return
	 * @throws DataAccessException
	 * @Description: This method is used to get Training Program Status based on
	 *               Program Status Id.
	 */
	public TrainingMasProgrammeStatusBO getTrainingProgramStatus(byte statusId)
			throws DataAccessException {
		LOG.startUsecase("Entered getTrainingProgramStatus");
		TrainingMasProgrammeStatusBO progStatusInfo = new TrainingMasProgrammeStatusBO();
		try {
			progStatusInfo = repository
					.findByPkTrainingProgrammeStatusId(statusId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get the data from Database");
		}
		LOG.endUsecase("Exited getTrainingProgramStatus");
		return progStatusInfo;

	}

	/**
	 * 
	 * @param progId
	 * @return
	 * @throws DataAccessException
	 * @Descrption : This method is used to get Nominated Employee's of a
	 *             Training Program
	 */
	public List<EmployeeAttendedDetailsBean> getAllNominatedEmployeesOfTrainingProgram(
			int progId) throws DataAccessException {
		TrainingDatProgrammeDetailBO progDetail = null;
		List<TrainingDatNominationDetailBO> nominatedList = null;
		EmployeeAttendedDetailsBean empDetail = null;
		List<EmployeeAttendedDetailsBean> empDetailsList = new ArrayList<EmployeeAttendedDetailsBean>();
		try {
			progDetail = trainingProgramRepository
					.findByPkTrainingProgrammeId(progId);

			if (progDetail != null) {
				nominatedList = trainingNominationRepository
						.findByFkDatTrainingProgrammeIdAndNominationStatus(
								progId, "NOMINATED");

				if (nominatedList != null) {
					for (TrainingDatNominationDetailBO nomDetail : nominatedList) {
						empDetail = new EmployeeAttendedDetailsBean();
						empDetail.setEmpId(nomDetail.getFkDatNominatedEmpId());
						empDetail.setEmpName(getEmpNameByEmpId(nomDetail
								.getFkDatNominatedEmpId()));
						empDetail.setAttendedTraining(nomDetail
								.getAttendedTraining());
						empDetailsList.add(empDetail);
					}
				} else {
					throw new DataAccessException(
							"No nominated employee's for this training program");
				}
			} else {
				throw new DataAccessException("Invalid Program Id");
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		return empDetailsList;
	}

	/**
	 * 
	 * @param empId
	 * @param progId
	 * @return boolean
	 * @throws CommonCustomException
	 * @Description : This service is used to remove Participants from a
	 *              Training Program.
	 */
	public boolean removeParticipants(int empId, int progId)
			throws CommonCustomException {
		LOG.startUsecase("Entering removeParticipants service");
		DatEmpDetailBO empDetails = null;
		TrainingDatProgrammeDetailBO progDetail = null;
		TrainingDatNominationDetailBO nominationDetails = null;

		boolean result = false;
		try {
			empDetails = empDetailRepository.findByPkEmpId(empId);
			if (empDetails != null) {
				progDetail = trainingProgramRepository
						.findByPkTrainingProgrammeId(progId);

				if (progDetail != null) {
					nominationDetails = trainingNominationRepository
							.findByFkDatTrainingProgrammeIdAndFkDatNominatedEmpIdAndNominationStatus(
									progId, empId, "NOMINATED");

					if (nominationDetails != null) {
						trainingNominationRepository.delete(nominationDetails);
						nominationPoolRepository.delete(nominationDetails
								.getTrainingDatNominationPoolDetail());
						result = true;
					} else {
						throw new CommonCustomException(
								"Employee has not been nominated for this Training Program.");
					}
				} else {
					throw new CommonCustomException("Invalid Program Id");
				}
			} else {
				throw new CommonCustomException("Invalid Employee Id");
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("Exiting removeParticipants service");
		return result;
	}

	/**
	 * 
	 * @param progId
	 * @param response
	 * @return
	 * @throws CommonCustomException
	 * @throws Exception
	 * @Description : This method is used to download Training Program Agenda
	 *              from server.
	 */
	public boolean downloadProgramAgenda(Integer progId,
			HttpServletResponse response) throws CommonCustomException,
			Exception {

		LOG.startUsecase("Entering Download Program Agenda Service");
		boolean isAgendaDownloaded = false;
		TrainingDatProgrammeDetailBO progDetail = null;
		List<String> downloadFileNames = new ArrayList<String>();

		try {
			progDetail = trainingProgramRepository.findByPkTrainingProgrammeId(progId);

			if (progDetail.getIsAgendaUploaded().equals("NO"))
				throw new CommonCustomException(
						"Agenda has not been uploaded for this Training Program");
			File dir = new File(programAgendaDirPath);
			File[] listOfFiles = dir.listFiles();
			
			
			for (int i = 0; i < listOfFiles.length; i++) 
			{
				if (listOfFiles[i].isFile()) 
				{
					if(listOfFiles[i].getName().startsWith(progId.toString() + "_"))
					{
						LOG.info("File :: " + listOfFiles[i].getName());
						downloadFileNames.add(listOfFiles[i].getName());
					}
			    } 
			}
			
			if(downloadFileNames.isEmpty())
				throw new CommonCustomException("No document found.");
			/*String[] listFiles = dir.list();
			for (String s : listFiles) {
				String start = StringUtils.substringBefore(s, "_");
				if (start.trim().equals(fileName.trim())) {*/
			DownloadFileUtil.downloadSelectedFile(response,programAgendaDirPath, downloadFileNames);
			isAgendaDownloaded = true;
				/*}
			}*/
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		LOG.endUsecase("Exiting Download Program Agenda Service");
		return isAgendaDownloaded;
	}

	// End of Addition by Kamal Anand

	// Added by Rajesh Kumar
	/**
	 * 
	 * <Description createTrainingNomiPoolDetail:> method is call the repository
	 * method to create Training Nomination Pool Detail data in the data base.
	 * It will take the BO and passes it to the Repository for update the
	 * Record.
	 * 
	 * @param TrainingDatNominationPoolDetailBO
	 *            is the argument for this method.
	 * 
	 * @return return the list of data in Training Dat Nomination Pool Detail BO
	 *         according to the input details.
	 * 
	 * @throws BusinessException
	 *             throws if any exception occurs during the data retrieval.
	 * 
	 */
	public TrainingDatNominationPoolDetailBO createTrainingNomiPoolDetail(
			TrainingDatNominationPoolDetailBean nominationPoolDetailBean)
			throws DataAccessException, CommonCustomException {

		TrainingDatNominationPoolDetailBO poolDetails = null;
		TrainingDatNominationPoolDetailBO createPoolDetails = null;
		List<TrainingDatNominationDetailBO> nominatedList = null;
		TrainingDatProgrammeDetailBO progDetails = null;
		List<TrainingDatProgrammeDetailBO> nominatedProgDetail = new ArrayList<TrainingDatProgrammeDetailBO>();
		DatEmpProfessionalDetailBO empProfDetail = null;
		Date programStartDate = null;
		Date todaysDate = null;
		LOG.startUsecase("Entering createTrainingNomiPoolDetail");
		try {
			poolDetails = nominationPoolRepository
					.findByFkNominatedEmpIdAndFkTrainingProgrammeId(
							nominationPoolDetailBean.getFkNominatedEmpId(),
							nominationPoolDetailBean.getFkTrainingProgrammeId());

			nominatedList = trainingNominationRepository
					.findByFkDatTrainingProgrammeIdAndNominationStatus(
							nominationPoolDetailBean.getFkTrainingProgrammeId(),
							"NOMINATED");

			progDetails = trainingProgramRepository
					.findByPkTrainingProgrammeId(nominationPoolDetailBean
							.getFkTrainingProgrammeId());

			nominatedProgDetail = trainingProgramRepository
					.getAllNominatedProgramsWithinDateRange(
							progDetails.getTrainingStartDateTime(),
							progDetails.getTrainingEndDateTime(),
							nominationPoolDetailBean.getFkNominatedEmpId());

			if (nominatedProgDetail.size() > 0) {
				throw new CommonCustomException(
						"Employee has already been nominated for another training program on this date");
			}

			empProfDetail = empProfessionalRepository
					.findByFkMainEmpDetailId(nominationPoolDetailBean
							.getFkNominatedEmpId());

			programStartDate = DateTimeUtil.dateWithMinTimeofDay(progDetails
					.getTrainingStartDateTime());
			todaysDate = DateTimeUtil.dateWithMinTimeofDay(new Date());

			if (!progDetails
					.getTrainingDatRequestDetail()
					.getLevelOfParticipants()
					.contains(
							empProfDetail.getMasEmpLevelBO().getEmpLevelName())) {
				throw new CommonCustomException(
						"Employee level does not match the Training Program level");
			}

			if (progDetails.getFkProgrammeStatusId() != programPlannedStatus) {
				throw new CommonCustomException(
						"Cannot Nominate Employee's for this Training Program Status");
			} else if (todaysDate.getTime() >= programStartDate.getTime()) {
				throw new CommonCustomException(
						"Cannot nominate employee on the day of training program or after training program");
			} else {

				if (nominatedList != null) {
					if (nominatedList.size() < progDetails.getMaximumSeats()) {
						if (poolDetails == null) {
							createPoolDetails = createNewNominationPoolDetails(nominationPoolDetailBean);
						} else {
							createPoolDetails = updateNominationPoolDetails(nominationPoolDetailBean);
						}
					} else {
						throw new CommonCustomException(
								"Seats are filled for this training program");
					}
				} else {
					if (poolDetails == null) {
						createPoolDetails = createNewNominationPoolDetails(nominationPoolDetailBean);
					} else {
						createPoolDetails = updateNominationPoolDetails(nominationPoolDetailBean);
					}
				}
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("Exiting createTrainingNomiPoolDetail");
		return createPoolDetails;
	}

	/**
	 * 
	 * @param nominationPoolDetailBean
	 * @return
	 * @throws CommonCustomException
	 * @Description : This method is used to create a new record in Nomination
	 *              Pool Details Table
	 */
	public TrainingDatNominationPoolDetailBO createNewNominationPoolDetails(
			TrainingDatNominationPoolDetailBean nominationPoolDetailBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering createNewNominationPoolDetails");

		TrainingDatNominationPoolDetailBO poolDetail = new TrainingDatNominationPoolDetailBO();
		TrainingDatNominationPoolDetailBO savePoolDetail = null;
		TrainingDatProgrammeDetailBO progDetails = null;
		TrainingDatNominationDetailBO saveNominationDetail = null;
		DatEmpDetailBO empDetails = new DatEmpDetailBO();
		try {
			
			
			
			if (nominationPoolDetailBean != null) {

				List<Short> tainingIdList = new ArrayList<Short>();
			  	String[] empIdArray = trainingCoordinatorAndManagerRole.split(",");
			  	for (String s : empIdArray)
			  		tainingIdList.add(Short.parseShort(s));
				
			  //	empDetails = empDetailRepository.getTrainingCoordinatorAndManagerList(nominationPoolDetailBean.getFkApproverEmpId(),tainingIdList);
				
			  	empDetails = empDetailRepository.findByPkEmpId(nominationPoolDetailBean.getFkApproverEmpId());
		
				progDetails = trainingProgramRepository
						.findByPkTrainingProgrammeId(nominationPoolDetailBean
								.getFkTrainingProgrammeId());

				if (progDetails != null) {
					if(empDetails != null)
					{
						if(tainingIdList.contains(empDetails.getFkEmpRoleId()))
								{
							if(progDetails.getTrainingDatRequestDetail().getFkTrainingRequesterEmpId() != null)
							{
							nominationPoolDetailBean.setFkApproverEmpId(progDetails.getTrainingDatRequestDetail().getFkTrainingRequesterEmpId());
							poolDetail.setFkApprovedOrRejectedBy(nominationPoolDetailBean
									.getFkApproverEmpId());
						
								}
							/*else
							{
								poolDetail.setFkApproverEmpId(nominationPoolDetailBean
										.getFkApproverEmpId());
								poolDetail.setFkApprovedOrRejectedBy(nominationPoolDetailBean
										.getFkApproverEmpId());
								
								
							}*/
					}
					
					if (progDetails.getTrainingDatRequestDetail()
							.getFkTrainingRequesterEmpId() != null) {
						poolDetail
								.setFkTrainingProgrammeId(nominationPoolDetailBean
										.getFkTrainingProgrammeId());
						poolDetail.setFkNominatedEmpId(nominationPoolDetailBean
								.getFkNominatedEmpId());
						poolDetail.setNominationPoolCreatedDate(new Date());
						poolDetail.setNominationStatus(nominationPoolDetailBean
								.getNominationStatus());
						poolDetail.setFkApproverEmpId(nominationPoolDetailBean
								.getFkApproverEmpId());

						if (nominationPoolDetailBean.getNominationStatus() == nominationPoolRejectedStatus
								|| nominationPoolDetailBean
										.getNominationStatus() == nominationPoolApprovedStatus) {
							poolDetail
									.setFkApprovedOrRejectedBy(nominationPoolDetailBean
											.getFkApproverEmpId());
							poolDetail.setApprovedOrRejectedDate(new Date());
						}

						savePoolDetail = nominationPoolRepository
								.save(poolDetail);
						
						//Added by Kamal Anand for Notifications
						if(nominationPoolDetailBean.getNominationStatus() == nominationPoolInterestedStatus) {
						try{
							notificationService.setEmpInterestedInProgNotificationToRmScreen(nominationPoolDetailBean
											.getFkApproverEmpId());
						} catch(Exception e) {
							LOG.info("Some exception occurred while setting employee interested in Training Program notification to RM Screen");
						}
						}
						//End of Addition by Kamal Anand for Notifications
						
						

						if (nominationPoolDetailBean.getNominationStatus() == nominationPoolApprovedStatus) {
							if (savePoolDetail != null) {
								saveNominationDetail = createNominationDetail(savePoolDetail);
								//Added by Kamal Anand for Notifications
									try {
										if (tainingIdList.contains(empDetails
												.getFkEmpRoleId()))
											notificationService
													.setTCInviteEmpForProgNotificationToEmpScreen(
															nominationPoolDetailBean
																	.getFkNominatedEmpId(),
															nominationPoolDetailBean
																	.getFkTrainingProgrammeId());
										else
											notificationService
													.setRmInviteEmpForProgNotificationToEmpScreen(
															nominationPoolDetailBean
																	.getFkNominatedEmpId(),
															nominationPoolDetailBean
																	.getFkTrainingProgrammeId());
									}catch(Exception e){
									LOG.info("Some exception occurred while setting emp invited to training program notification");
								}
								//End of Addition by Kamal Anand for Notifications
							}
						}
					} else {
						poolDetail
								.setFkTrainingProgrammeId(nominationPoolDetailBean
										.getFkTrainingProgrammeId());
						poolDetail.setFkNominatedEmpId(nominationPoolDetailBean
								.getFkNominatedEmpId());
						poolDetail.setNominationPoolCreatedDate(new Date());
						poolDetail.setNominationStatus(nominationPoolDetailBean
								.getNominationStatus());
						poolDetail.setFkApproverEmpId(nominationPoolDetailBean
								.getFkApproverEmpId());

						if (nominationPoolDetailBean.getNominationStatus() == nominationPoolRejectedStatus
								|| nominationPoolDetailBean
										.getNominationStatus() == nominationPoolApprovedStatus) {
							poolDetail
									.setFkApprovedOrRejectedBy(nominationPoolDetailBean
											.getFkApproverEmpId());
							poolDetail.setApprovedOrRejectedDate(new Date());
						}
						savePoolDetail = nominationPoolRepository
								.save(poolDetail);
						
						//Added by Kamal Anand for Notifications
						/*if(nominationPoolDetailBean.getNominationStatus() == nominationPoolInterestedStatus) {
						try{
							notificationService.setEmpInterestedInProgNotificationToRmScreen(nominationPoolDetailBean
											.getFkApproverEmpId());
						} catch(Exception e) {
							LOG.info("Some exception occurred while setting employee interested in Training Program notification to RM Screen");
						}
						}*/
						//End of Addition by Kamal Anand for Notifications
						
						if (nominationPoolDetailBean.getNominationStatus() == nominationPoolApprovedStatus) {
							if (savePoolDetail != null) {
								saveNominationDetail = createNominationDetail(savePoolDetail);
								//Added by Kamal Anand for Notifications
								try{
									notificationService.setRmInviteEmpForProgNotificationToEmpScreen(nominationPoolDetailBean.getFkNominatedEmpId(), nominationPoolDetailBean.getFkTrainingProgrammeId());
								}catch(Exception e) {
									LOG.info("Some exception occurred while setting RM invite employee to Training program notification");
								}
								//End of Addition by Kamal Anand for Notifications
							}
						}
					}
				} else {
					throw new DataAccessException(
							"Exception occured while trying to get training program details");
				}
			} else {
				throw new CommonCustomException(
						"Some error occured while getting input details");
			}
			}
		}catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("exiting createNewNominationPoolDetails");
		return savePoolDetail;
	}

	/**
	 * 
	 * @param nominationPoolDetailBean
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to update the Nomination Pool Details
	 *              and also create nomination details.
	 */
	public TrainingDatNominationPoolDetailBO updateNominationPoolDetails(
			TrainingDatNominationPoolDetailBean nominationPoolDetailBean)
			throws DataAccessException, CommonCustomException {
		LOG.startUsecase("entering updateNominationPoolDetails");

		TrainingDatNominationPoolDetailBO poolDetails = new TrainingDatNominationPoolDetailBO();
		TrainingDatNominationPoolDetailBO updatePoolDetails = null;
		TrainingDatNominationDetailBO nominationDetails = new TrainingDatNominationDetailBO();

		try {

			poolDetails = nominationPoolRepository
					.findByFkNominatedEmpIdAndFkTrainingProgrammeId(
							nominationPoolDetailBean.getFkNominatedEmpId(),
							nominationPoolDetailBean.getFkTrainingProgrammeId());

			if (poolDetails != null) {
				if (nominationPoolDetailBean.getNominationStatus() == nominationPoolApprovedStatus
						|| nominationPoolDetailBean.getNominationStatus() == nominationPoolRejectedStatus) {
					poolDetails.setNominationStatus(nominationPoolDetailBean
							.getNominationStatus());
					poolDetails
							.setFkApprovedOrRejectedBy(nominationPoolDetailBean
									.getFkApproverEmpId());
					poolDetails.setApprovedOrRejectedDate(new Date());
					poolDetails.setChoiceModifiedDate(new Date());

					updatePoolDetails = nominationPoolRepository
							.save(poolDetails);

					if (updatePoolDetails != null
							&& nominationPoolDetailBean.getNominationStatus() == nominationPoolApprovedStatus) {
						nominationDetails = createNominationDetail(updatePoolDetails);
						//Added by Kamal Anand for Notifications
						try {
							notificationService
									.setRmApprovedEmpRequestToEmpScreen(
											nominationPoolDetailBean
													.getFkNominatedEmpId(),
											nominationPoolDetailBean
													.getFkTrainingProgrammeId());
						}catch(Exception e){
							LOG.info("Some Exception occurred while setting RM approve employee request notification");
						}
						//End of Addition by Kamal Anand for Notifications
					}
				} else {
					poolDetails.setNominationStatus(nominationPoolDetailBean
							.getNominationStatus());
					poolDetails.setChoiceModifiedDate(new Date());

					updatePoolDetails = nominationPoolRepository
							.save(poolDetails);
				}
			} else {
				throw new DataAccessException(
						"Exception Occured while trying to get Nomination Pool Details");
			}

		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("exiting updateNominationPoolDetails");
		return updatePoolDetails;
	}

	/**
	 * 
	 * @param poolDetail
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to create a new record in Nomination
	 *              Details table.
	 */
	public TrainingDatNominationDetailBO createNominationDetail(
			TrainingDatNominationPoolDetailBO poolDetail)
			throws DataAccessException, CommonCustomException {
		LOG.startUsecase("entering createNominationDetail");

		TrainingDatNominationDetailBO nominationDetail = new TrainingDatNominationDetailBO();
		TrainingDatNominationDetailBO saveNominationDetail = null;
		TrainingDatNominationDetailBO checkNominationDetail = null;
		try {
			if (poolDetail != null) {

				checkNominationDetail = trainingNominationRepository
						.findByFkDatNominatedEmpIdAndFkDatTrainingProgrammeId(
								poolDetail.getFkNominatedEmpId(),
								poolDetail.getFkTrainingProgrammeId());

				if (checkNominationDetail == null) {
					LOG.info("inside checkNominationDetail null");
					nominationDetail.setFkNominationPoolId(poolDetail
							.getPkTrainingNominationPoolDetailId());
					nominationDetail.setFkDatTrainingProgrammeId(poolDetail
							.getFkTrainingProgrammeId());
					nominationDetail.setFkDatNominatedEmpId(poolDetail
							.getFkNominatedEmpId());
					nominationDetail.setNominationDate(new Date());
					nominationDetail.setFkNominatedByEmpId(poolDetail
							.getFkApproverEmpId());
					nominationDetail.setNominationStatus("NOMINATED");
					nominationDetail.setAttendedTraining("NO");
					nominationDetail.setIsFeedbackProvided("NO");

					saveNominationDetail = trainingNominationRepository
							.save(nominationDetail);
				} else {
					throw new CommonCustomException(
							"Employee has already been nominated for the training program");
				}
			} else {
				throw new DataAccessException(
						"Some error happened while getting nomination pool details.");
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("exiting createNominationDetail");
		return saveNominationDetail;
	}

	// EOA by Rajesh Kumar
	// Added by Prathibha
	/**
	 * 
	 * < createTrainingProgrambyTRining Coordinator:> method will call the
	 * repository method to create Training Propgrams in the data base. It will
	 * take the Bean and passes it to the Repository for create the Record.
	 * 
	 * @param TrainingRequestandProgramdetailsBean
	 *            is the parameter passed for this method.
	 * 
	 * @return TrainingDatProgrammeDetailBO
	 * 
	 * @throws DataAccessException
	 *             if any exception occurs during creation of program.
	 * @throws CommonCustomException 
	 * @throws IOException 
	 * 
	 */

	public TrainingDatProgrammeDetailBO createTrainingProgramByTrainingCoordinator(
			TrainingRequestandProgramdetailsBean trnRequPrgramBean,
			MultipartFile file) throws DataAccessException, CommonCustomException, IOException 
	{
		LOG.startUsecase("Create_Training_Program_by_TrainingCoordinator_Service");
		TrainingDatProgrammeDetailBO createdProgram = new TrainingDatProgrammeDetailBO();
		TrainingDatRequestDetailBO trainingDatRequestDetailBO = new TrainingDatRequestDetailBO();
		TrainingDatProgrammeDetailBO trainingProgramDetail = null;
		TrainingDatRequestDetailBO requestDetail;
		MasTrainingTopicNameBO trainingTopicName = null;

		if (file != null) 
		{
			String fileName = file.getOriginalFilename();
			LOG.info("fileName :: " + fileName);
			String mFileExtension = "";
			
			if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
				mFileExtension = fileName.substring(fileName.lastIndexOf(".")+1);
			
			LOG.info("mFileExtension :: " + mFileExtension);
			/*String[] fileSplit = fileName.split(".",2);
			
			LOG.info("fileSplit :: " + fileSplit.toString());
			LOG.info("File extension :: " + fileSplit[1].toString());*/
			/*String mFileExtension = StringUtils.substringAfter(
						file.getOriginalFilename(), ".");
			*/	
			if (!mFileExtension.equalsIgnoreCase("pdf")
						&& !mFileExtension.equalsIgnoreCase("doc")
						&& !mFileExtension.equalsIgnoreCase("jpg")
						&& !mFileExtension.equalsIgnoreCase("jpeg")
						&& !mFileExtension.equalsIgnoreCase("png")) 
			{
				throw new CommonCustomException(
							"This file type is not allowed. Please choose different one.");
			} else {
					createdProgram.setIsAgendaUploaded("YES");
			}
		} else 
			createdProgram.setIsAgendaUploaded("NO");
			
			trainingDatRequestDetailBO.setComments(trnRequPrgramBean.getComments());
			trainingDatRequestDetailBO.setExternalTrainerRequest(trnRequPrgramBean.getExternalTrainerRequest());
			trainingDatRequestDetailBO.setBriefTrainingContent(trnRequPrgramBean.getBriefTrainingContent());
			trainingDatRequestDetailBO.setExpectedTrainingStartDate(trnRequPrgramBean
							.getTrainingStartDateTime());
			trainingDatRequestDetailBO.setIsSoftwareLicenceAvailable(trnRequPrgramBean
							.getIsSoftwareLicenceAvailable());
			trainingDatRequestDetailBO.setIsTrainingProgramCreated(trnRequPrgramBean
							.getIsTrainingProgramCreated());
			trainingDatRequestDetailBO.setLevelOfParticipants(trnRequPrgramBean.getLevelOfParticipants());
			trainingDatRequestDetailBO.setLevelOfTrainingRequest(trnRequPrgramBean
							.getLevelOfTrainingRequest());
			trainingDatRequestDetailBO.setNoOfLicensesAvailbale(trnRequPrgramBean
							.getNoOfLicensesAvailbale());
			trainingDatRequestDetailBO.setNoOfParticipants(trnRequPrgramBean.getNoOfParticipants());
			trainingDatRequestDetailBO.setPreferredTrainingStartTime(trnRequPrgramBean
							.getPreferredTrainingStartTime());
			trainingDatRequestDetailBO.setPreferredTrainingEndTime(trnRequPrgramBean
							.getPreferredTrainingEndTime());
			if(trnRequPrgramBean.getProductVersionForTraining() != null)
			{
				trainingDatRequestDetailBO.setProductVersionForTraining(
						trnRequPrgramBean.getProductVersionForTraining());
			}else{
				trainingDatRequestDetailBO.setProductVersionForTraining(null);
			}
			
			if(trnRequPrgramBean.getSpecificInputComments() != null)
			{
				trainingDatRequestDetailBO.setSpecificInputComments(trnRequPrgramBean.getSpecificInputComments());
			}else {
				trainingDatRequestDetailBO.setSpecificInputComments(null);
		
			}
			trainingDatRequestDetailBO.setTrainingModifiedDate(trnRequPrgramBean
							.getTrainingModifiedDate());
			trainingDatRequestDetailBO.setTrainingRequestedDate(trnRequPrgramBean
							.getTrainingRequestedDate());
			
			if(trnRequPrgramBean.getUsageOfProduct() != null)
			{
				trainingDatRequestDetailBO.setUsageOfProduct(trnRequPrgramBean.getUsageOfProduct());
			}else{
				trainingDatRequestDetailBO.setUsageOfProduct(null);
			}
			
			trainingDatRequestDetailBO.setFkTrainingRequestStatusId(trnRequPrgramBean
							.getFkTrainingRequestStatusId());
			trainingDatRequestDetailBO.setFkTrainingRequesterEmpId(trnRequPrgramBean
							.getFkTrainingRequesterEmpId());
			trainingDatRequestDetailBO.setFkTrainingTopicNameId(trnRequPrgramBean
							.getFkTrainingTopicNameId());
			trainingDatRequestDetailBO.setExpectedTrainingStartDate(trnRequPrgramBean
							.getTrainingStartDateTime());
			trainingDatRequestDetailBO.setProgramCategory(trnRequPrgramBean
					.getProgramCategory());

			try{
				requestDetail = trainingRequestRepository.save(trainingDatRequestDetailBO);
			}catch(Exception ex){
				throw new CommonCustomException("Failed to save traing details into db.");
			}
			
			if (requestDetail != null) 
			{
				createdProgram.setFkTrainingRequestId(trainingDatRequestDetailBO.getPkTrainingRequestId());
				
				if(trnRequPrgramBean.getComments() != null)
				{
					createdProgram.setComments(trnRequPrgramBean.getComments());
				}else	{
					createdProgram.setComments(null);
				}
				
				createdProgram.setExternalVenueName(trnRequPrgramBean.getExternalVenueName());
				createdProgram.setMaximumSeats(trnRequPrgramBean.getMaximumSeats());
				createdProgram.setProgrammeCreatedDate(new Date());
				createdProgram.setProgrammeModifiedDate(trnRequPrgramBean.getProgrammeModifiedDate());
				createdProgram.setTrainerName(trnRequPrgramBean.getTrainerName());
				createdProgram.setTrainerType(trnRequPrgramBean.getTrainerType());
				createdProgram.setTrainingDurationInDays(trnRequPrgramBean.getTrainingDurationInDays());
				createdProgram.setTrainingDurationInHours(trnRequPrgramBean.getTrainingDurationInHours());
				createdProgram.setTrainingEndDateTime(trnRequPrgramBean.getTrainingEndDateTime());
				createdProgram.setTrainingIntimatedStatus(trnRequPrgramBean.getTrainingIntimatedStatus());
				createdProgram.setTrainingNoOfHrsPerDay(trnRequPrgramBean.getTrainingNoOfHrsPerDay());
				createdProgram.setTrainingStartDateTime(trnRequPrgramBean.getTrainingStartDateTime());
				createdProgram.setFkProgrammeCreatedBy(trnRequPrgramBean.getFkProgrammeCreatedBy());
				createdProgram.setFkProgrammeModifyBy(trnRequPrgramBean.getFkProgrammeModifyBy());
				createdProgram.setFkProgrammeStatusId(trnRequPrgramBean.getFkProgrammeStatusId());
				createdProgram.setFkTrainingRoomId(trnRequPrgramBean.getFkTrainingRoomId());
				//createdProgram.setPkTrainingProgrammeId(trnRequPrgramBean.getPkTrainingProgrammeId());
				
				if (file != null)
					createdProgram.setIsAgendaUploaded("YES");
				else
					createdProgram.setIsAgendaUploaded("NO");

				try{
					trainingProgramDetail = trainingProgramRepository.save(createdProgram);
					//Added by Kamal Anand for Notifications
					try {
						notificationService
								.setTcCreateProgramNotificationToEmpScreen(trainingProgramDetail.getPkTrainingProgrammeId());
						notificationService
								.setTcCreateProgramNotificationToRmScreen(trainingProgramDetail.getPkTrainingProgrammeId());
					} catch (Exception e) {
						LOG.info("Some exception ocurred while setting Create Training Program by Training Coordinator Notifications");
					}
					//End of Addition by Kamal Anand for Notifications
				}catch(Exception ex){
					throw new CommonCustomException("Failed to save a new program in db.");
				}
				
				if (file != null) 
				{
					try {
						trainingTopicName = topicRepository.findByPkTrainingTopicNameId(trnRequPrgramBean
										.getFkTrainingTopicNameId());

						String extension = StringUtils.substringAfter(file.getOriginalFilename(), ".");
						String newFileName = trainingProgramDetail.getPkTrainingProgrammeId() + "_"
								+ trainingTopicName.getTrainingTopicName();
						File fileToBeStored = new File(programAgendaDirPath + newFileName + "." + extension);
						FileCopyUtils.copy(file.getBytes(), fileToBeStored);
					} catch (IOException e) {
						throw new CommonCustomException("Failed to upload document into server.");
					}
				}
			}
		
	
		LOG.endUsecase("Create_Training_Program_by_TrainingCoordinator_Service");

		return trainingProgramDetail;
	}

	// EOA by Prathibha

	// Added by Balaji
	/**
	 * 
	 * @param requestInputBean
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 */

	public List<TeamTrainingRequestOutputBean> viewTeamTrainingRequest(
			TeamTrainingRequestBean requestInputBean) throws BusinessException,
			DataAccessException {
		LOG.startUsecase("viewTeamTrainingRequest");
		List<TeamTrainingRequestOutputBean> teamTrainingRequestOutput = new ArrayList<TeamTrainingRequestOutputBean>();
		try {
			List<TrainingDatSubNominationPoolDetailBean> getTrainingDatNominationPoolDetailList = getTrainingDatNominationPoolDetail(requestInputBean);
			if (getTrainingDatNominationPoolDetailList.size() > 0) {
				for (TrainingDatSubNominationPoolDetailBean nominationPoolDetailBean : getTrainingDatNominationPoolDetailList) {
					TeamTrainingRequestOutputBean teamTrainingRequestOutputBean = new TeamTrainingRequestOutputBean();
					teamTrainingRequestOutputBean
							.setNominatedEmpId(nominationPoolDetailBean
									.getFkNominatedEmpId());
					teamTrainingRequestOutputBean
							.setApproverEmpId(nominationPoolDetailBean
									.getFkApproverEmpId());
					teamTrainingRequestOutputBean
							.setEmpFullName(nominationPoolDetailBean
									.getNominatedEmpName());
					teamTrainingRequestOutputBean
							.setBriefTrainingContent(nominationPoolDetailBean
									.getTrainingProgrammeDetailInputBean()
									.getTrainingRequestDetailInputBean()
									.getBriefTrainingContent());
					teamTrainingRequestOutputBean
							.setMaximumSeats(nominationPoolDetailBean
									.getTrainingProgrammeDetailInputBean()
									.getMaximumSeats());
					teamTrainingRequestOutputBean
							.setNominationStatusId(nominationPoolDetailBean
									.getNominationStatus());
					teamTrainingRequestOutputBean
							.setNominationStatusName(getNominationStatusName(nominationPoolDetailBean
									.getNominationStatus()));
					teamTrainingRequestOutputBean
							.setTopicId(nominationPoolDetailBean
									.getTrainingProgrammeDetailInputBean()
									.getTrainingRequestDetailInputBean()
									.getFkTrainingTopicNameId());
					teamTrainingRequestOutputBean
							.setTrainingTopicName(nominationPoolDetailBean
									.getTrainingProgrammeDetailInputBean()
									.getTrainingRequestDetailInputBean()
									.getMasTrainingTopicNameBO()
									.getTrainingTopicName());
					teamTrainingRequestOutputBean
							.setTrainingStartDateTime(nominationPoolDetailBean
									.getTrainingProgrammeDetailInputBean()
									.getTrainingStartDateTime());
					teamTrainingRequestOutputBean
							.setPkTrainingNominationPoolDetailId(nominationPoolDetailBean
									.getPkTrainingNominationPoolDetailId());
					teamTrainingRequestOutputBean
							.setProgramId(nominationPoolDetailBean
									.getFkTrainingProgrammeId());
					teamTrainingRequestOutputBean
							.setTrainingReqId(nominationPoolDetailBean
									.getTrainingProgrammeDetailInputBean()
									.getFkTrainingRequestId());

					teamTrainingRequestOutput
							.add(teamTrainingRequestOutputBean);
				}
			}
		} catch (Exception e) {
			LOG.info("Exception Occured:", e);
		}

		LOG.endUsecase("viewTeamTrainingRequest");
		return teamTrainingRequestOutput;
	}

	/**
	 * 
	 * @param requestInputBean
	 * @return nominationList
	 * @throws CommonCustomException
	 */
	private List<TrainingDatNominationPoolDetailBO> getFilteredTrainingDatNominationPoolDetail(
			TeamTrainingRequestBean requestInputBean)
			throws CommonCustomException {
		LOG.startUsecase("getFilteredTrainingDatNominationPoolDetail");
		List<TrainingDatNominationPoolDetailBO> nominationList = new ArrayList<TrainingDatNominationPoolDetailBO>();
		if ((requestInputBean.getNominationStatusId() == 0 && requestInputBean
				.getNominatedEmpId() == 0)
				|| (requestInputBean.getNominationStatusId() == 0 && requestInputBean
						.getNominatedEmpId() == null)) {
			nominationList = nominationPoolRepository
					.findByFkApproverEmpId(requestInputBean
							.getFkApproverEmpId());
		} else if ((requestInputBean.getNominationStatusId() != 0 && requestInputBean
				.getNominatedEmpId() == 0)
				|| (requestInputBean.getNominationStatusId() != 0 && requestInputBean
						.getNominatedEmpId() == null)) {
			nominationList = nominationPoolRepository
					.findByFkApproverEmpIdAndNominationStatus(
							requestInputBean.getFkApproverEmpId(),
							requestInputBean.getNominationStatusId());
		} else if ((requestInputBean.getNominationStatusId() != 0 && requestInputBean
				.getNominatedEmpId() != 0)
				|| (requestInputBean.getNominationStatusId() != 0 && requestInputBean
						.getNominatedEmpId() != null)) {
			nominationList = nominationPoolRepository
					.findByFkApproverEmpIdAndFkNominatedEmpIdAndNominationStatus(
							requestInputBean.getFkApproverEmpId(),
							requestInputBean.getNominatedEmpId(),
							requestInputBean.getNominationStatusId());
		} else if ((requestInputBean.getNominationStatusId() == 0 && requestInputBean
				.getNominatedEmpId() != 0)
				|| (requestInputBean.getNominationStatusId() == 0 && requestInputBean
						.getNominatedEmpId() != null)) {
			nominationList = nominationPoolRepository
					.findByFkApproverEmpIdAndFkNominatedEmpId(
							requestInputBean.getFkApproverEmpId(),
							requestInputBean.getNominatedEmpId());
		}
		if (nominationList.isEmpty()) {
			throw new CommonCustomException("No data found in Nomination List");
		}
		LOG.endUsecase("getFilteredTrainingDatNominationPoolDetail");
		return nominationList;
	}

	/**
	 * 
	 * @param requestInputBean
	 * @return nominationPoolDetailOutput
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	private List<TrainingDatSubNominationPoolDetailBean> getTrainingDatNominationPoolDetail(
			TeamTrainingRequestBean requestInputBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("getTrainingDatNominationPoolDetail");
		List<TrainingDatSubNominationPoolDetailBean> nominationPoolDetailOutput = new ArrayList<TrainingDatSubNominationPoolDetailBean>();
		try {
			LOG.debug("Get the training dat Nomination Pool Detail");
			if (requestInputBean.getTrainingStartDate() != null
					&& requestInputBean.getTrainingEndDate() != null) {
				Date truncatedStartDate = DateUtils.truncate(
						requestInputBean.getTrainingStartDate(), Calendar.DATE);
				Date truncatedEndDate = DateUtils.truncate(
						requestInputBean.getTrainingEndDate(), Calendar.DATE);
				requestInputBean.setTrainingStartDate(truncatedStartDate);
				LocalTime midnight = LocalTime.MAX;
				LocalDate date = truncatedEndDate.toInstant()
						.atZone(ZoneId.systemDefault()).toLocalDate();
				LocalDateTime todayMidnight = LocalDateTime.of(date, midnight);
				Date endDate = Date.from(todayMidnight.atZone(
						ZoneId.systemDefault()).toInstant());
				requestInputBean.setTrainingEndDate(endDate);

				List<TrainingDatNominationPoolDetailBO> nominationList = getFilteredTrainingDatNominationPoolDetail(requestInputBean);

				if (nominationList != null) {
					for (TrainingDatNominationPoolDetailBO nominationPoolDetailBO : nominationList) {
						TrainingDatSubNominationPoolDetailBean nominationPoolDetailBean = new TrainingDatSubNominationPoolDetailBean();
						TrainingProgrammeDetailInputBean trainingProgrammeDetailInputBean = new TrainingProgrammeDetailInputBean();
						trainingProgrammeDetailInputBean = getTrainingProgrammeDetail(
								nominationPoolDetailBO
										.getFkTrainingProgrammeId(),
								requestInputBean.getTrainingStartDate(),
								requestInputBean.getTrainingEndDate());
						if (trainingProgrammeDetailInputBean != null) {
							nominationPoolDetailBean
									.setTrainingProgrammeDetailInputBean(trainingProgrammeDetailInputBean);
							nominationPoolDetailBean
									.setFkApproverEmpId(nominationPoolDetailBO
											.getFkApproverEmpId());
							nominationPoolDetailBean
									.setFkTrainingProgrammeId(nominationPoolDetailBO
											.getFkTrainingProgrammeId());
							nominationPoolDetailBean
									.setNominationStatus(nominationPoolDetailBO
											.getNominationStatus());
							nominationPoolDetailBean
									.setFkNominatedEmpId(nominationPoolDetailBO
											.getFkNominatedEmpId());
							nominationPoolDetailBean
									.setNominatedEmpName(getEmpNameByEmpId(nominationPoolDetailBO
											.getFkNominatedEmpId()));

							nominationPoolDetailBean
									.setPkTrainingNominationPoolDetailId(nominationPoolDetailBO
											.getPkTrainingNominationPoolDetailId());
							nominationPoolDetailOutput
									.add(nominationPoolDetailBean);

						}
					}
				}

			} else if (requestInputBean.getTrainingStartDate() == null
					&& requestInputBean.getTrainingEndDate() == null) {
				List<TrainingDatNominationPoolDetailBO> nominationList = getFilteredTrainingDatNominationPoolDetail(requestInputBean);

				if (nominationList != null) {
					for (TrainingDatNominationPoolDetailBO nominationPoolDetailBO : nominationList) {
						TrainingDatSubNominationPoolDetailBean nominationPoolDetailBean = new TrainingDatSubNominationPoolDetailBean();
						nominationPoolDetailBean
								.setFkApproverEmpId(nominationPoolDetailBO
										.getFkApproverEmpId());
						nominationPoolDetailBean
								.setFkTrainingProgrammeId(nominationPoolDetailBO
										.getFkTrainingProgrammeId());
						nominationPoolDetailBean
								.setNominationStatus(nominationPoolDetailBO
										.getNominationStatus());
						nominationPoolDetailBean
								.setFkNominatedEmpId(nominationPoolDetailBO
										.getFkNominatedEmpId());
						nominationPoolDetailBean
								.setNominatedEmpName(getEmpNameByEmpId(nominationPoolDetailBO
										.getFkNominatedEmpId()));
						nominationPoolDetailBean
								.setTrainingProgrammeDetailInputBean(getTrainingProgrammeDetail(nominationPoolDetailBO
										.getFkTrainingProgrammeId()));
						nominationPoolDetailBean
								.setPkTrainingNominationPoolDetailId(nominationPoolDetailBO
										.getPkTrainingNominationPoolDetailId());
						nominationPoolDetailOutput
								.add(nominationPoolDetailBean);
					}
				}

			}

		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("getTrainingDatNominationPoolDetail");
		return nominationPoolDetailOutput;
	}

	/**
	 * 
	 * @param pkTrainingProgrammeId
	 * @param trainingStartDateTime
	 * @param trainingEndDateTime
	 * @return trainingProgramDetailBean
	 * @throws DataAccessException
	 */
	private TrainingProgrammeDetailInputBean getTrainingProgrammeDetail(
			Integer pkTrainingProgrammeId, Date trainingStartDateTime,
			Date trainingEndDateTime) throws DataAccessException {
		LOG.startUsecase("getTrainingProgrammeDetail");
		TrainingProgrammeDetailInputBean trainingProgramDetailBean = new TrainingProgrammeDetailInputBean();
		TrainingDatProgrammeDetailBO trainingProgrammeDetail = trainingProgramRepository
				.findByPkTrainingProgrammeIdAndTrainingStartDateTimeGreaterThanEqualAndTrainingEndDateTimeLessThanEqual(
						pkTrainingProgrammeId, trainingStartDateTime,
						trainingEndDateTime);
		if (trainingProgrammeDetail != null) {
			trainingProgramDetailBean
					.setFkTrainingRequestId(trainingProgrammeDetail
							.getFkTrainingRequestId());
			trainingProgramDetailBean.setMaximumSeats(trainingProgrammeDetail
					.getMaximumSeats());
			trainingProgramDetailBean
					.setPkTrainingProgrammeId(trainingProgrammeDetail
							.getPkTrainingProgrammeId());
			trainingProgramDetailBean
					.setTrainingStartDateTime(trainingProgrammeDetail
							.getTrainingStartDateTime());
			trainingProgramDetailBean
					.setTrainingRequestDetailInputBean(getTopicIdByRequestId(trainingProgrammeDetail
							.getFkTrainingRequestId()));
		}
		LOG.endUsecase("getTrainingProgrammeDetail");
		return trainingProgramDetailBean;
	}

	/**
	 * 
	 * @param pkTrainingProgrammeId
	 * @return trainingProgramDetailBean
	 * @throws DataAccessException
	 * @throws Exception
	 */
	private TrainingProgrammeDetailInputBean getTrainingProgrammeDetail(
			Integer pkTrainingProgrammeId) throws DataAccessException,
			Exception {
		LOG.startUsecase("getTrainingProgrammeDetail with program id");
		TrainingProgrammeDetailInputBean trainingProgramDetailBean = new TrainingProgrammeDetailInputBean();
		try {
			TrainingDatProgrammeDetailBO trainingProgrammeDetail = trainingProgramRepository
					.findByPkTrainingProgrammeId(pkTrainingProgrammeId);
			if (trainingProgrammeDetail != null) {
				trainingProgramDetailBean
						.setFkTrainingRequestId(trainingProgrammeDetail
								.getFkTrainingRequestId());
				trainingProgramDetailBean
						.setMaximumSeats(trainingProgrammeDetail
								.getMaximumSeats());
				trainingProgramDetailBean
						.setPkTrainingProgrammeId(trainingProgrammeDetail
								.getPkTrainingProgrammeId());
				trainingProgramDetailBean
						.setTrainingStartDateTime(trainingProgrammeDetail
								.getTrainingStartDateTime());
				trainingProgramDetailBean
						.setTrainingRequestDetailInputBean(getTopicIdByRequestId(trainingProgrammeDetail
								.getFkTrainingRequestId()));
			} else {
				throw new DataAccessException(
						"Exception occured while retrieving Training program Details"); // TODO
			}
		} catch (Exception e) {
			LOG.info("Exception Occured: ", e);
		}
		LOG.endUsecase("getTrainingProgrammeDetail with program id");
		return trainingProgramDetailBean;

	}

	/**
	 * 
	 * @param fkTrainingRequestId
	 * @return requestInputDetail
	 */
	private TrainingRequestDetailInputBean getTopicIdByRequestId(
			Integer fkTrainingRequestId) {
		LOG.startUsecase("getTopicIdByRequestId");
		TrainingRequestDetailInputBean requestInputDetail = new TrainingRequestDetailInputBean();
		try {
			TrainingDatRequestDetailBO requestOutput = trainingRequestRepository
					.findByPkTrainingRequestId(fkTrainingRequestId);
			if (requestOutput != null) {
				requestInputDetail.setBriefTrainingContent(requestOutput
						.getBriefTrainingContent());
				requestInputDetail.setFkTrainingTopicNameId(requestOutput
						.getFkTrainingTopicNameId());
				requestInputDetail
						.setMasTrainingTopicNameBO(getTrainigTopicDetails(requestOutput
								.getFkTrainingTopicNameId()));
			} else {
				throw new DataAccessException(
						"Exception occured while retrieving Request details"); // TODO
			}
		} catch (Exception e) {
			LOG.info("Exception Occured: ", e);
		}
		LOG.endUsecase("getTopicIdByRequestId");
		return requestInputDetail;
	}

	/**
	 * 
	 * @param topicID
	 * @return trainingTopicOutput
	 * @Description This method is used to retrieve the topic name from DB
	 */
	private MasTrainingTopicNameBO getTrainigTopicDetails(int topicID) {
		LOG.startUsecase("getTrainigTopicDetails");
		MasTrainingTopicNameBO trainingTopicOutput = new MasTrainingTopicNameBO();
		try {
			trainingTopicOutput = topicRepository
					.findByPkTrainingTopicNameId(topicID);
		} catch (Exception e) {
			LOG.info("Exception Occured: ", e);
		}
		LOG.endUsecase("getTrainigTopicDetails");
		return trainingTopicOutput;

	}

	/**
	 * 
	 * @param empId
	 * @return personalDetail
	 * @Description This method is used to retrieve the employee name
	 */
	private String getEmpNameByEmpId(Integer empId) {
		LOG.startUsecase("getEmpNameByEmpId");
		DatEmpPersonalDetailBO personalDetail = new DatEmpPersonalDetailBO();
		try {
			personalDetail = empPersonalRepository.findByFkEmpDetailId(empId);
			if (personalDetail != null) {
				return personalDetail.getEmpFirstName() + " "
						+ personalDetail.getEmpLastName();
			}
		} catch (Exception e) {
			LOG.info("Exception Occured: ", e);
		}
		LOG.endUsecase("getEmpNameByEmpId");
		return "";

	}

	/**
	 * 
	 * @param trainingProgrammeInputBean
	 * @return finalListOutput
	 * @throws CommonCustomException
	 * @throws ParseException
	 * @Description To retrive the training details by passing RmId and
	 *              dateRange with in 90 days from current date
	 */
	public List<TrainingDetailsOutputBean> viewTrainingDetailsByDateAndRM(
			TrainingRequestDetailSubInputBean trainingProgrammeInputBean)
			throws CommonCustomException, ParseException {

		LOG.startUsecase("viewTrainingDetailsByDateAndRM");

		List<TrainingDetailsOutputBean> listOutput = new ArrayList<TrainingDetailsOutputBean>();
		List<TrainingDetailsOutputBean> finallistOutput = new ArrayList<TrainingDetailsOutputBean>();

		if (trainingProgrammeInputBean.getStartDate() != null
				&& trainingProgrammeInputBean.getEndDate() != null) {

			trainingProgrammeInputBean
					.setStartDate(DateTimeUtil
							.getStartDateTime(trainingProgrammeInputBean
									.getStartDate()));
			trainingProgrammeInputBean.setEndDate(DateTimeUtil
					.getEndDateTime(trainingProgrammeInputBean.getEndDate()));

			List<TrainingDatProgrammeDetailBO> trainingProgramDetails = trainingProgramRepository
					.getAllProgramDetailsByRmIdDateRange(
							trainingProgrammeInputBean.getRmMgrId(),
							trainingProgrammeInputBean.getStartDate(),
							trainingProgrammeInputBean.getEndDate());
			if (trainingProgramDetails.isEmpty()) {
				LOG.info("No records found");
			} else {
				listOutput = getTrainingDetailsByDateRangeAndRmId(trainingProgramDetails);
			}

		} else if (trainingProgrammeInputBean.getStartDate() == null
				&& trainingProgrammeInputBean.getEndDate() == null) {
			List<TrainingDatProgrammeDetailBO> trainingProgramDetailsForRmId = trainingProgramRepository
					.getAllProgramDetailsByRmId(trainingProgrammeInputBean
							.getRmMgrId());
			if (trainingProgramDetailsForRmId.isEmpty()) {
				LOG.info("No records found");
			} else {
				listOutput = getTrainingDetailsByDateRangeAndRmId(trainingProgramDetailsForRmId);
			}

		}

		for (TrainingDetailsOutputBean trainingDetailsOutputBean : listOutput) {
			if (trainingProgrammeInputBean.getFeedbackStatus()
					.equalsIgnoreCase("PENDING")) {
				if (trainingDetailsOutputBean.getFeedbackStatus()
						.equalsIgnoreCase("FILL FEEDBACK")) {
					finallistOutput.add(trainingDetailsOutputBean);
				}
			} else if (trainingProgrammeInputBean.getFeedbackStatus()
					.equalsIgnoreCase("COMPLETED")) {
				if (trainingDetailsOutputBean.getFeedbackStatus()
						.equalsIgnoreCase("VIEW FEEDBACK")) {
					finallistOutput.add(trainingDetailsOutputBean);
				}
			} else if (trainingProgrammeInputBean.getFeedbackStatus()
					.equalsIgnoreCase("ALL")) {
				finallistOutput.add(trainingDetailsOutputBean);
			}
		}
		LOG.endUsecase("viewTrainingDetailsByDateAndRM");
		return finallistOutput;

	}

	/**
	 * 
	 * @Description This method used to set the values in the output bean
	 * @param trainingRequestDetails
	 * @return outputBeanList
	 */
	public List<TrainingDetailsOutputBean> getTrainingDetailsByDateRangeAndRmId(
			List<TrainingDatProgrammeDetailBO> trainingRequestDetails) {
		LOG.startUsecase("getTrainingDetailsByDateRange");

		List<TrainingDetailsOutputBean> outputBeanList = new ArrayList<TrainingDetailsOutputBean>();

		for (TrainingDatProgrammeDetailBO trainingDatProgrammeDetailBO : trainingRequestDetails) {

			TrainingDetailsOutputBean trainingDetailOutputBean = new TrainingDetailsOutputBean();
			TrainingRequestDetailSubInputBean trainingInputBean = new TrainingRequestDetailSubInputBean();

			trainingDetailOutputBean
					.setReportingManager(trainingDatProgrammeDetailBO
							.getTrainingDatRequestDetail()
							.getFkTrainingRequesterEmpId());
			trainingDetailOutputBean
					.setPkTrainingRequestId(trainingDatProgrammeDetailBO
							.getFkTrainingRequestId());
			trainingDetailOutputBean
					.setMaximumSeats(trainingDatProgrammeDetailBO
							.getMaximumSeats());
			trainingDetailOutputBean.setProgramId(trainingDatProgrammeDetailBO
					.getPkTrainingProgrammeId());
			trainingDetailOutputBean
					.setTrainingStartDate(trainingDatProgrammeDetailBO
							.getTrainingStartDateTime());
			trainingDetailOutputBean.setTopicName(trainingDatProgrammeDetailBO
					.getTrainingDatRequestDetail().getMasTrainingTopicName()
					.getTrainingTopicName());
			trainingDetailOutputBean
					.setTrainerName(trainingDatProgrammeDetailBO
							.getTrainerName());
			trainingDetailOutputBean
					.setProgramModifiedDate(trainingDatProgrammeDetailBO
							.getProgrammeModifiedDate());
			trainingDetailOutputBean
					.setBreifDescription(trainingDatProgrammeDetailBO
							.getTrainingDatRequestDetail()
							.getBriefTrainingContent());
			if (trainingDetailOutputBean.getProgramModifiedDate() != null) {
				trainingDetailOutputBean
						.setDateDifferenceNow(dateDifferenceBwModifiedDateAndCurrentDate(trainingDetailOutputBean
								.getProgramModifiedDate()));
			}
			if (viewFeedbackStatus(
					trainingDatProgrammeDetailBO.getPkTrainingProgrammeId(),
					trainingDatProgrammeDetailBO.getTrainingDatRequestDetail()
							.getFkTrainingRequesterEmpId()) > 0) {
				trainingDetailOutputBean.setFeedbackStatus("VIEW FEEDBACK");
			} else {
				trainingDetailOutputBean.setFeedbackStatus("FILL FEEDBACK");

			}

			outputBeanList.add(trainingDetailOutputBean);
		}

		LOG.endUsecase("getTrainingDetailsByDateRange");

		return outputBeanList;
	}

	/**
	 * 
	 * @Description This method is used to view the feed back status
	 * @param fkDatTrainingProgrammeId
	 * @param managerId
	 * @return feedbackListSize
	 */
	private Integer viewFeedbackStatus(Integer fkDatTrainingProgrammeId,
			Integer managerId) {
		LOG.startUsecase("viewFeedbackStatus");
		Integer feedbackListSize = 0;
		try {
			List<TrainingDatPostApplicationFeedbackBO> trainingFeedbackDetails = feedbackRepository
					.findByFkDatTrainingProgrammeIdAndFkManagerEmpId(
							fkDatTrainingProgrammeId, managerId);
			feedbackListSize = trainingFeedbackDetails.size();
		} catch (Exception e) {
			LOG.info("Exception Occured: ", e);
		}
		LOG.endUsecase("viewFeedbackStatus");
		return feedbackListSize;
	}

	/**
	 * 
	 * @Description This method is used to set the difference between modified
	 *              date and current date
	 * @param modifiedDate
	 * @return range
	 */
	private Long dateDifferenceBwModifiedDateAndCurrentDate(Date modifiedDate) { // TODO
		LOG.startUsecase("dateDifferenceBwModifiedDateAndCurrentDate");
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(modifiedDate);
		LocalDate startDate = LocalDate.parse(date);
		LocalDate endtDate = LocalDate.now();
		Long range = ChronoUnit.DAYS.between(startDate, endtDate);
		LOG.endUsecase("dateDifferenceBwModifiedDateAndCurrentDate");
		return range;
	}

	// End of Addition by Balaji
	// Added by Pratibha

	/**
	 * 
	 * @param nominationDetailBean
	 * @return finalBean1
	 * @throws DataAccessException
	 * @Description : Service is used to get all Employee Nominated Training
	 *              Programs and also Training Coordinator created Training
	 *              Program by Date Range
	 */

	public List<Map> getAllTrainingsForEmployee(
			TrainingDatNominationDetailBean nominationDetailBean)
			throws DataAccessException {

		List<TrainingCalendarOutputBean> trainingCalendarOutputBeanList = new ArrayList<TrainingCalendarOutputBean>();
		List<TrainingCalendarOutputBean> trainingCalendarOutputBeanListfinal = new ArrayList<TrainingCalendarOutputBean>();
		TrainingCalendarOutputBean calDetails;
		Map<Date, List<TrainingCalendarOutputBean>> map = new HashMap<Date, List<TrainingCalendarOutputBean>>();
		TrainingDatProgrammeDetailBO progDetail1 = new TrainingDatProgrammeDetailBO();
		List<TrainingDatNominationDetailBO> nominationDetail = null;
		List<Map> finalBean1 = new ArrayList<Map>();
		DatEmpProfessionalDetailBO empProfDetail = null;

		LOG.startUsecase("Get All Trainings For Employee");
		try {
			Date truncatedStartDate = DateTimeUtil
					.dateWithMinTimeofDay(nominationDetailBean.getStartDate());
			Date truncatedEndDate = DateTimeUtil
					.dateWithMaxTimeofDay(nominationDetailBean.getEndDate());
			nominationDetailBean.setStartDate(truncatedStartDate);

			nominationDetailBean.setEndDate(truncatedEndDate);

			nominationDetail = trainingNominationRepository
					.findByFkDatNominatedEmpIdAndNominationStatus(
							nominationDetailBean.getFkDatNominatedEmpId(),
							"NOMINATED");

			List<TrainingDatRequestDetailBO> requestList = trainingRequestRepository
					.findByfkTrainingRequesterEmpIdIsNull();
			

			List<TrainingDatRequestDetailBO> requestedList = trainingRequestRepository
					.findByFkTrainingRequesterEmpId(nominationDetailBean
							.getFkDatNominatedEmpId());

			empProfDetail = empProfessionalRepository
					.findByFkMainEmpDetailId(nominationDetailBean
							.getFkDatNominatedEmpId());
          
			if (requestedList.size() > 0 || requestedList != null) {
				for (TrainingDatRequestDetailBO req : requestedList) {
					if (req.getIsTrainingProgramCreated().equalsIgnoreCase(
							"YES")) {
						progDetail1 = trainingProgramRepository
								.findByFkTrainingRequestId(req
										.getPkTrainingRequestId());
						if (progDetail1 != null) {
							if (empProfDetail != null) {
								if (progDetail1.getTrainingStartDateTime()
										.getTime() >= nominationDetailBean
										.getStartDate().getTime()
										&& progDetail1
												.getTrainingStartDateTime()
												.getTime() <= nominationDetailBean
												.getEndDate().getTime()) {
									calDetails = new TrainingCalendarOutputBean();
									calDetails.setTrainingTitle(progDetail1
											.getTrainingDatRequestDetail()
											.getMasTrainingTopicName()
											.getTrainingTopicName());
									calDetails.setStartDate(progDetail1
											.getTrainingStartDateTime());

									calDetails.setStartTime(progDetail1
											.getTrainingStartDateTime());
									calDetails.setProgId(progDetail1
											.getPkTrainingProgrammeId());
									calDetails.setEmpId(nominationDetailBean
											.getFkDatNominatedEmpId());
									trainingCalendarOutputBeanList
											.add(calDetails);
								}
							}
						}
					}
				}
			}
			if (requestList.size() > 0 || requestList != null) {
				for (TrainingDatRequestDetailBO req : requestList) {
					if (req.getIsTrainingProgramCreated().equalsIgnoreCase(
							"YES")) {
						progDetail1 = trainingProgramRepository
								.findByFkTrainingRequestId(req
										.getPkTrainingRequestId());

						if (progDetail1 != null) {
							if (empProfDetail != null) {

								if (progDetail1.getTrainingStartDateTime()
										.getTime() >= nominationDetailBean
										.getStartDate().getTime()
										&& progDetail1
												.getTrainingStartDateTime()
												.getTime() <= nominationDetailBean
												.getEndDate().getTime()) {
									calDetails = new TrainingCalendarOutputBean();
									calDetails.setTrainingTitle(progDetail1
											.getTrainingDatRequestDetail()
											.getMasTrainingTopicName()
											.getTrainingTopicName());
									calDetails.setStartDate(progDetail1
											.getTrainingStartDateTime());

									calDetails.setStartTime(progDetail1
											.getTrainingStartDateTime());
									calDetails.setProgId(progDetail1
											.getPkTrainingProgrammeId());
									calDetails.setEmpId(nominationDetailBean
											.getFkDatNominatedEmpId());

									if (empProfDetail
											.getHasReporteesFlag()
											.equalsIgnoreCase(
													TrainingConstants.HAS_REPORTEES)) {
										trainingCalendarOutputBeanList
												.add(calDetails);
									} else {
										if (progDetail1
												.getTrainingDatRequestDetail()
												.getLevelOfParticipants()
												.contains(
														empProfDetail
																.getMasEmpLevelBO()
																.getEmpLevelName())) {
											trainingCalendarOutputBeanList
													.add(calDetails);
										}
									}
								}
							}
						} else {
							throw new DataAccessException(
									"Exception occured while retrieving Programme details");
						}
					}
				}
			}

			if (nominationDetail.size() > 0 || nominationDetail != null) {
				for (TrainingDatNominationDetailBO nomDetail : nominationDetail) {
					if (nomDetail.getTrainingDatProgrammeDetail()
							.getTrainingStartDateTime().getTime() >= nominationDetailBean
							.getStartDate().getTime()
							&& nomDetail.getTrainingDatProgrammeDetail()
									.getTrainingStartDateTime().getTime() <= nominationDetailBean
									.getEndDate().getTime()) {
						if (nomDetail.getTrainingDatProgrammeDetail()
								.getTrainingDatRequestDetail()
								.getFkTrainingRequesterEmpId() != null) {
							calDetails = new TrainingCalendarOutputBean();
							calDetails.setTrainingTitle(nomDetail
									.getTrainingDatProgrammeDetail()
									.getTrainingDatRequestDetail()
									.getMasTrainingTopicName()
									.getTrainingTopicName());
							calDetails.setStartDate(nomDetail
									.getTrainingDatProgrammeDetail()
									.getTrainingStartDateTime());
							calDetails.setStartTime(nomDetail
									.getTrainingDatProgrammeDetail()
									.getTrainingStartDateTime());
							calDetails.setProgId(nomDetail
									.getFkDatTrainingProgrammeId());
							calDetails.setEmpId(nominationDetailBean
									.getFkDatNominatedEmpId());
							trainingCalendarOutputBeanList.add(calDetails);
						}
					}
				}
			}
			trainingCalendarOutputBeanListfinal = removeDuplicatesFromCalendarList(trainingCalendarOutputBeanList);

			if (trainingCalendarOutputBeanList.size() > 0) {
				for (TrainingCalendarOutputBean bean : trainingCalendarOutputBeanListfinal) {
					if (!map.containsKey(bean.getStartDate())) {
						List<TrainingCalendarOutputBean> finalBean = new ArrayList<TrainingCalendarOutputBean>();
						finalBean.add(bean);
						map.put(bean.getStartDate(), finalBean);
						continue;
					}
					if (map.containsKey(bean.getStartDate())) {
						List<TrainingCalendarOutputBean> finalBean = new ArrayList<TrainingCalendarOutputBean>();
						finalBean.add(bean);
						map.get(bean.getStartDate()).addAll(finalBean);
					}
				}
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("Get All Trainings For Employee");
		finalBean1.add(map);
		return finalBean1;
	}

	/**
	 * This method is used to remove duplicates from a Calendar list
	 */
	private List<TrainingCalendarOutputBean> removeDuplicatesFromCalendarList(
			List<TrainingCalendarOutputBean> calendarList)
			throws DataAccessException {
		List<TrainingCalendarOutputBean> calList = new ArrayList<TrainingCalendarOutputBean>();
		Map<Integer, TrainingCalendarOutputBean> calMap = new HashMap<Integer, TrainingCalendarOutputBean>();
		TrainingDatNominationDetailBO nominationDetail = null;
		for (TrainingCalendarOutputBean u : calendarList) {
			try {
				nominationDetail = trainingNominationRepository
						.findByFkDatTrainingProgrammeIdAndFkDatNominatedEmpIdAndNominationStatus(
								u.getProgId(), u.getEmpId(), "NOMINATED");

				if (nominationDetail != null) {
					u.setNominationStatus("NOMINATED");
				} else {
					u.setNominationStatus("");
				}

				if (!calMap.containsKey(u.getProgId())) {
					calMap.put(u.getProgId(), u);
				}
			} catch (Exception e) {
				throw new DataAccessException(e.getMessage());
			}

		}
		calList.addAll(calMap.values());
		return calList;
	}

	/**
	 * 
	 * @param pgmId
	 * @return trainingProgrammeDetailOutputBean
	 * @throws DataAccessException
	 * @Description : Service is used to view all training program details based
	 *              on programId
	 */
	public TrainingProgramDetailOutputBean viewTrainingProgramDetailsBasedOnProgramId(
			int pgmId) throws DataAccessException {
		TrainingDatProgrammeDetailBO programDetail;
		TrainingProgramDetailOutputBean trainingProgrammeDetailOutputBean = new TrainingProgramDetailOutputBean();
		TrainingRequestDetailBean requestDetail = new TrainingRequestDetailBean();
		DatEmpPersonalDetailBO personalDetail;
		int remainingSeats = 0;

		LOG.startUsecase("View Training ProgramDetails Based On ProgramId");
		try {
			programDetail = trainingProgramRepository
					.findByPkTrainingProgrammeId(pgmId);
			if (programDetail != null) {
				remainingSeats = trainingNominationRepository
						.getCountOfFilledSeats(pgmId);
				trainingProgrammeDetailOutputBean
						.setIsAgendaUploaded(programDetail
								.getIsAgendaUploaded());
				trainingProgrammeDetailOutputBean
						.setRemainingSeats(programDetail.getMaximumSeats()
								- remainingSeats);
				trainingProgrammeDetailOutputBean.setProgId(programDetail
						.getPkTrainingProgrammeId());
				trainingProgrammeDetailOutputBean
						.setTrainingTitle(programDetail
								.getTrainingDatRequestDetail()
								.getMasTrainingTopicName()
								.getTrainingTopicName());
				trainingProgrammeDetailOutputBean.setStartDate(programDetail
						.getTrainingStartDateTime());
				trainingProgrammeDetailOutputBean
						.setNoOfParticipants(programDetail
								.getTrainingDatRequestDetail()
								.getNoOfParticipants());
				trainingProgrammeDetailOutputBean.setTrainer(programDetail
						.getTrainerName());
				trainingProgrammeDetailOutputBean.setVenue(programDetail
						.getTrainingMasRoomDetail().getTrainingRoomName());
				trainingProgrammeDetailOutputBean.setDescription(programDetail
						.getTrainingDatRequestDetail()
						.getBriefTrainingContent());
				trainingProgrammeDetailOutputBean.setProgStatusId(programDetail
						.getFkProgrammeStatusId());
				trainingProgrammeDetailOutputBean.setProgStatus(programDetail
						.getTrainingMasProgrammeStatus()
						.getTrainingProgrammeStatusName());
				/*trainingProgrammeDetailOutputBean
						.setDurationInHours(programDetail
								.getTrainingDurationInHours());*/
				trainingProgrammeDetailOutputBean
						.setDurationInDays(programDetail
								.getTrainingDurationInDays());
				trainingProgrammeDetailOutputBean
						.setNoOfHrsPerDay(programDetail
								.getTrainingNoOfHrsPerDay());
				trainingProgrammeDetailOutputBean.setEndDate(programDetail
						.getTrainingEndDateTime());
				trainingProgrammeDetailOutputBean.setTrainerType(programDetail
						.getTrainerType());
				trainingProgrammeDetailOutputBean.setMaxSeats(programDetail
						.getMaximumSeats());
				trainingProgrammeDetailOutputBean
						.setExternalVenueName(programDetail
								.getExternalVenueName());
				trainingProgrammeDetailOutputBean
						.setProgramCategoryId(programDetail
								.getTrainingDatRequestDetail()
								.getProgramCategory());
				trainingProgrammeDetailOutputBean
						.setProgramCategoryName(TrainingRequestService
								.getProgramCategoryNameById(programDetail
										.getTrainingDatRequestDetail()
										.getProgramCategory()));
				requestDetail
						.setTrainingRequestId(programDetail
								.getTrainingDatRequestDetail()
								.getPkTrainingRequestId());

				if (programDetail.getTrainingDatRequestDetail()
						.getFkTrainingRequesterEmpId() != null) {
					requestDetail.setRequestorEmpId(programDetail
							.getTrainingDatRequestDetail()
							.getFkTrainingRequesterEmpId());
					requestDetail
							.setRequestorName(getEmpNameByEmpId(programDetail
									.getTrainingDatRequestDetail()
									.getFkTrainingRequesterEmpId()));
				} else {
					requestDetail.setRequestorEmpId(null);
					requestDetail.setRequestorName("");
				}

				requestDetail.setTopicId(programDetail
						.getTrainingDatRequestDetail()
						.getFkTrainingTopicNameId());
				requestDetail.setTopicName(programDetail
						.getTrainingDatRequestDetail()
						.getMasTrainingTopicName().getTrainingTopicName());

				if (programDetail.getTrainingDatRequestDetail()
						.getLevelOfTrainingRequest() == 1)
					requestDetail.setLevelOfTrainingRequest("BASIC");
				if (programDetail.getTrainingDatRequestDetail()
						.getLevelOfTrainingRequest() == 2)
					requestDetail.setLevelOfTrainingRequest("INTERMEDIATE");
				if (programDetail.getTrainingDatRequestDetail()
						.getLevelOfTrainingRequest() == 3)
					requestDetail.setLevelOfTrainingRequest("ADVANCED");
                if(programDetail
						.getTrainingDatRequestDetail()
						.getProductVersionForTraining() != null)
				requestDetail.setProductVersion(programDetail
						.getTrainingDatRequestDetail()
						.getProductVersionForTraining());
				requestDetail.setNoOfParticipants(programDetail
						.getTrainingDatRequestDetail().getNoOfParticipants());
				requestDetail
						.setLevelOfParticipants(programDetail
								.getTrainingDatRequestDetail()
								.getLevelOfParticipants());

				if(programDetail.getTrainingDatRequestDetail().getUsageOfProduct() != null)
				{
				if (programDetail.getTrainingDatRequestDetail()
						.getUsageOfProduct() == 1)
					requestDetail.setUsageOfProduct("DEVELOPMENT");
				if (programDetail.getTrainingDatRequestDetail()
						.getUsageOfProduct() == 2)
					requestDetail.setUsageOfProduct("ADMINISTRATIVE");
				if (programDetail.getTrainingDatRequestDetail()
						.getUsageOfProduct() == 3)
					requestDetail.setUsageOfProduct("OTHERS");
				}

				requestDetail.setBriefTrainingContent(programDetail
						.getTrainingDatRequestDetail()
						.getBriefTrainingContent());
				if(requestDetail.getIsSoftwareLicenseAvailable() != null)
				requestDetail.setIsSoftwareLicenseAvailable(programDetail
						.getTrainingDatRequestDetail()
						.getIsSoftwareLicenceAvailable());
				requestDetail.setExpectedTrainingStartDate(programDetail
						.getTrainingDatRequestDetail()
						.getExpectedTrainingStartDate());
				requestDetail.setPreferredTrainingStartTime(programDetail
						.getTrainingDatRequestDetail()
						.getPreferredTrainingStartTime());
				requestDetail.setPreferredTrainingEndTime(programDetail
						.getTrainingDatRequestDetail()
						.getPreferredTrainingEndTime());
				requestDetail.setExternalTrainerRequest(programDetail
						.getTrainingDatRequestDetail()
						.getExternalTrainerRequest());
				requestDetail.setSpecificInputComments(programDetail
						.getTrainingDatRequestDetail()
						.getSpecificInputComments());
				requestDetail.setTrainingRequestedDate(programDetail
						.getTrainingDatRequestDetail()
						.getTrainingRequestedDate());
				requestDetail.setTrainingModifiedDate(programDetail
						.getTrainingDatRequestDetail()
						.getTrainingModifiedDate());
				requestDetail.setRequestStatusId(programDetail
						.getTrainingDatRequestDetail()
						.getFkTrainingRequestStatusId());
				requestDetail.setRequestStatusName(programDetail
						.getTrainingDatRequestDetail()
						.getTrainingMasRequestStatus()
						.getTrainingRequestStatusName());
				requestDetail.setComments(programDetail
						.getTrainingDatRequestDetail().getComments());
				requestDetail.setIsProgramCreated(programDetail
						.getTrainingDatRequestDetail()
						.getIsTrainingProgramCreated());
				trainingProgrammeDetailOutputBean
						.setRequestDetails(requestDetail);
				if(programDetail.getTrainingDatRequestDetail().getProgramCategory() != null)
				{
					if(programDetail.getTrainingDatRequestDetail().getProgramCategory() == 1)	
					   trainingProgrammeDetailOutputBean.setProgrammeCategory("SOFT SKILLS");
					if(programDetail.getTrainingDatRequestDetail().getProgramCategory() == 2)
						trainingProgrammeDetailOutputBean.setProgrammeCategory("TECHNICAL");
					if(programDetail.getTrainingDatRequestDetail().getProgramCategory() == 3)
						trainingProgrammeDetailOutputBean.setProgrammeCategory("PROCESS");				 
				}
			} else {
				throw new DataAccessException(
						"Program details does not found");
			}
			if (programDetail.getTrainingDatRequestDetail()
					.getFkTrainingRequesterEmpId() != null) {
				personalDetail = empPersonalRepository
						.findByFkEmpDetailId(programDetail
								.getTrainingDatRequestDetail()
								.getFkTrainingRequesterEmpId());
				trainingProgrammeDetailOutputBean.setInitiatedBy(personalDetail
						.getEmpFirstName()
						+ " "
						+ personalDetail.getEmpLastName());
			} else {
				trainingProgrammeDetailOutputBean.setInitiatedBy("");
			}

		} catch (DataAccessException e) {

			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("View Training ProgramDetails Based On ProgramId");
		return trainingProgrammeDetailOutputBean;
	}

	/**
	 * 
	 * @return massProgramResult
	 * @throws DataAccessException
	 * @Description :Service is used to get all Training Program Status
	 */
	public List<TrainingMasProgrammeStatusBO> getTrainingProgramStatus()
			throws DataAccessException {
		List<TrainingMasProgrammeStatusBO> massProgramResult;
		LOG.startUsecase("Get Training Program Status");
		try {

			massProgramResult = repository.findAll();

		} catch (Exception e) {

			throw new DataAccessException("Exception occured", e);
		}
		LOG.endUsecase("Get Training Program Status");
		return massProgramResult;
	}

	/**
	 * 
	 * @param programId
	 * @param empId
	 * @return outputBean
	 * @throws DataAccessException
	 * @Description :Service is used to get Training Programs based on Training
	 *              program id and nomination status
	 */
	public DetailedTrainingOutputBean getTrainingProgramBasedOnTrainingProgramIdAndSetNominationStatus(
			int programId, int empId) throws DataAccessException {

		DetailedTrainingOutputBean outputBean;
		DatEmpPersonalDetailBO personalDetail = null;
		DatEmpProfessionalDetailBO professionalDetail = null;
		TrainingDatNominationPoolDetailBO nominationPoolBO = null;
		TrainingDatProgrammeDetailBO progDetail = null;
		TrainingRequestDetailBean requestDetail = new TrainingRequestDetailBean();
		List<TrainingDatNominationDetailBO> nominatedList = null;

		LOG.startUsecase("Get Training Program Based On Training ProgramId And Set Nomination Status");
		try {
			outputBean = new DetailedTrainingOutputBean();
			nominationPoolBO = nominationPoolRepository
					.findByFkTrainingProgrammeIdAndFkNominatedEmpId(programId,
							empId);

			nominatedList = trainingNominationRepository
					.findByFkDatTrainingProgrammeIdAndNominationStatus(
							programId, "NOMINATED");

			progDetail = trainingProgramRepository
					.findByPkTrainingProgrammeId(programId);

			outputBean.setRemainingSeats(progDetail.getMaximumSeats()
					- nominatedList.size());

			professionalDetail = empProfessionalRepository
					.findByFkMainEmpDetailId(empId);

			if (professionalDetail != null) {
				outputBean
						.setReportingMgrName(getEmpNameByEmpId(professionalDetail
								.getFkEmpReportingMgrId()));
			} else {
				throw new DataAccessException(
						"Some error happened while getting Employee Professional Details");
			}
			if (nominatedList != null) {
				if (nominatedList.size() >= 0) {
					if (nominatedList.size() < progDetail.getMaximumSeats())
						outputBean.setSeatsFilled(false);
					else
						outputBean.setSeatsFilled(true);
				}

			} else {
				outputBean.setSeatsFilled(false);
			}

			if (nominationPoolBO != null) {
				outputBean
						.setNominatedbyEmpName(getEmpNameByEmpId(nominationPoolBO
								.getFkApproverEmpId()));
				if (nominationPoolBO.getNominationStatus() == 1)
					outputBean.setNominationStatus("INTERESTED");
				else if (nominationPoolBO.getNominationStatus() == 2)
					outputBean.setNominationStatus("NOT INTERESTED");
				else if (nominationPoolBO.getNominationStatus() == 3)
					outputBean.setNominationStatus("APPROVED");
				else if (nominationPoolBO.getNominationStatus() == 4)
					outputBean.setNominationStatus("REJECTED");
				else if (nominationPoolBO.getNominationStatus() == 5)
					outputBean.setNominationStatus("WITHDRAWN");
				else if (nominationPoolBO.getNominationStatus() == 6)
					outputBean.setNominationStatus("INVITED");
				else if (nominationPoolBO.getNominationStatus() == 7) {
					outputBean.setNominationStatus("DECISION NOT TAKEN");
				} else if (nominationPoolBO.getNominationStatus() == 8) {
					outputBean.setNominationStatus("DECLINE");
				} else {
					outputBean.setNominationStatus("SEND INVITE");
				}
				
				if (nominationPoolBO.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getFkTrainingRequesterEmpId() != null) {
					personalDetail = empPersonalRepository
							.findByFkEmpDetailId(nominationPoolBO
									.getTrainingDatProgrammeDetail()
									.getTrainingDatRequestDetail()
									.getFkTrainingRequesterEmpId());
				} else {
					personalDetail = empPersonalRepository
							.findByFkEmpDetailId(nominationPoolBO
									.getTrainingDatProgrammeDetail()
									.getFkProgrammeCreatedBy());
				}

				outputBean.setInitiatedBy(personalDetail.getEmpFirstName()
						+ " " + personalDetail.getEmpLastName());
				
				outputBean.setNoOfParticipants(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail().getNoOfParticipants());
				outputBean.setStartDate(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingStartDateTime());

				outputBean.setTime(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingStartDateTime());
				outputBean.setTrainer(nominationPoolBO
						.getTrainingDatProgrammeDetail().getTrainerName());
				outputBean.setTrainingTitle(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getMasTrainingTopicName().getTrainingTopicName());
				outputBean.setVenue(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingMasRoomDetail().getTrainingRoomName());
				outputBean.setDescription(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getBriefTrainingContent());
				outputBean.setProgId(nominationPoolBO
						.getFkTrainingProgrammeId());
			
				outputBean.setRequesterEmpId(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getFkTrainingRequesterEmpId());
				outputBean.setProgStatus(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingMasProgrammeStatus()
						.getTrainingProgrammeStatusName());
				outputBean.setProgStatusId(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getFkProgrammeStatusId());
				outputBean.setEndDate(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingEndDateTime());
				outputBean.setDurationInDays(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDurationInDays()
						+ "");
				/*outputBean.setDurationInHours(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDurationInHours()
						+ "");*/
				outputBean
						.setLevelOfParticipants(nominationPoolBO
								.getTrainingDatProgrammeDetail()
								.getTrainingDatRequestDetail()
								.getLevelOfParticipants());
				/*outputBean.setNoOfHoursPerDay(progDetail
						.getTrainingDatProgrammeDetail()
						.getTrainingNoOfHrsPerDay()
						+ "");*/
		        outputBean.setNoOfHoursPerDay(progDetail.getTrainingNoOfHrsPerDay()+ "");
				outputBean.setMaxSeats(nominationPoolBO
						.getTrainingDatProgrammeDetail().getMaximumSeats());
				outputBean.setApprovedOrRejectedBy(nominationPoolBO
						.getFkApprovedOrRejectedBy());

				requestDetail
						.setTrainingRequestId(nominationPoolBO
								.getTrainingDatProgrammeDetail()
							.getTrainingDatRequestDetail()
								.getPkTrainingRequestId());
				
				if (nominationPoolBO.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getFkTrainingRequesterEmpId() != null) {
					requestDetail.setRequestorEmpId(nominationPoolBO
							.getTrainingDatProgrammeDetail()
							.getTrainingDatRequestDetail()
							.getFkTrainingRequesterEmpId());
					requestDetail
							.setRequestorName(getEmpNameByEmpId(nominationPoolBO
									.getTrainingDatProgrammeDetail()
									.getTrainingDatRequestDetail()
									.getFkTrainingRequesterEmpId()));
				} else {
					requestDetail.setRequestorEmpId(null);
					requestDetail.setRequestorName("");
				}

				requestDetail.setTopicId(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getFkTrainingTopicNameId());
				requestDetail.setTopicName(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getMasTrainingTopicName().getTrainingTopicName());

				if (nominationPoolBO.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getLevelOfTrainingRequest() == 1)
					requestDetail.setLevelOfTrainingRequest("BASIC");
				if (nominationPoolBO.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getLevelOfTrainingRequest() == 2)
					requestDetail.setLevelOfTrainingRequest("INTERMEDIATE");
				if (nominationPoolBO.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getLevelOfTrainingRequest() == 3)
					requestDetail.setLevelOfTrainingRequest("ADVANCED");

				requestDetail.setProductVersion(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getProductVersionForTraining());
				requestDetail.setNoOfParticipants(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail().getNoOfParticipants());
				requestDetail
						.setLevelOfParticipants(nominationPoolBO
								.getTrainingDatProgrammeDetail()
								.getTrainingDatRequestDetail()
								.getLevelOfParticipants());

				if(nominationPoolBO.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail().getUsageOfProduct() != null)
				{	
				if (nominationPoolBO.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail().getUsageOfProduct() == 1)
					requestDetail.setUsageOfProduct("DEVELOPMENT");
				if (nominationPoolBO.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail().getUsageOfProduct() == 2)
					requestDetail.setUsageOfProduct("ADMINISTRATIVE");
				if (nominationPoolBO.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail().getUsageOfProduct() == 3)
					requestDetail.setUsageOfProduct("OTHERS");
				}

				requestDetail.setBriefTrainingContent(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getBriefTrainingContent());
				requestDetail.setIsSoftwareLicenseAvailable(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getIsSoftwareLicenceAvailable());
				requestDetail.setExpectedTrainingStartDate(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getExpectedTrainingStartDate());
				requestDetail.setPreferredTrainingStartTime(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getPreferredTrainingStartTime());
				requestDetail.setPreferredTrainingEndTime(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getPreferredTrainingEndTime());
				requestDetail.setExternalTrainerRequest(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getExternalTrainerRequest());
				requestDetail.setSpecificInputComments(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getSpecificInputComments());
				requestDetail.setTrainingRequestedDate(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getTrainingRequestedDate());
				requestDetail.setTrainingModifiedDate(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getTrainingModifiedDate());
				requestDetail.setRequestStatusId(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getFkTrainingRequestStatusId());
				requestDetail.setRequestStatusName(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getTrainingMasRequestStatus()
						.getTrainingRequestStatusName());
				requestDetail.setComments(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail().getComments());
				requestDetail.setIsProgramCreated(nominationPoolBO
						.getTrainingDatProgrammeDetail()
						.getTrainingDatRequestDetail()
						.getIsTrainingProgramCreated());

				outputBean.setRequestDetails(requestDetail);

			} else {
				progDetail = trainingProgramRepository
						.findByPkTrainingProgrammeId(programId);
				if (progDetail != null) {
					if (progDetail.getTrainingDatRequestDetail()
							.getFkTrainingRequesterEmpId() != null) {
						personalDetail = empPersonalRepository
								.findByFkEmpDetailId(progDetail
										.getTrainingDatRequestDetail()
										.getFkTrainingRequesterEmpId());
					} else {
						personalDetail = empPersonalRepository
								.findByFkEmpDetailId(progDetail
										.getFkProgrammeCreatedBy());
					}
					outputBean.setInitiatedBy(personalDetail.getEmpFirstName()
							+ " " + personalDetail.getEmpLastName());
					outputBean.setNominatedbyEmpName("");
					outputBean.setNoOfParticipants(progDetail
							.getTrainingDatRequestDetail()
							.getNoOfParticipants());
					outputBean.setStartDate(progDetail
							.getTrainingStartDateTime());
                         
					if(progDetail.getTrainingDatRequestDetail().getProgramCategory() != null)
					{
						if(progDetail.getTrainingDatRequestDetail().getProgramCategory() == 1)	
							outputBean.setProgrammeCategory("SOFT SKILLS");
		  			    if(progDetail.getTrainingDatRequestDetail().getProgramCategory() == 2)
							outputBean.setProgrammeCategory("TECHNICAL");
						if(progDetail.getTrainingDatRequestDetail().getProgramCategory() == 3)
							outputBean.setProgrammeCategory("PROCESS");				 
					}
					
					outputBean.setTime(progDetail.getTrainingStartDateTime());
					outputBean.setTrainer(progDetail.getTrainerName());
					outputBean.setTrainingTitle(progDetail
							.getTrainingDatRequestDetail()
							.getMasTrainingTopicName().getTrainingTopicName());
					outputBean.setVenue(progDetail.getTrainingMasRoomDetail()
							.getTrainingRoomName());
					outputBean.setDescription(progDetail
							.getTrainingDatRequestDetail()
							.getBriefTrainingContent());
					outputBean.setProgId(progDetail.getPkTrainingProgrammeId());
					outputBean.setNominationStatus("DECISION NOT TAKEN");
					outputBean.setRequesterEmpId(progDetail
							.getTrainingDatRequestDetail()
							.getFkTrainingRequesterEmpId());
					outputBean.setProgStatus(progDetail
							.getTrainingMasProgrammeStatus()
							.getTrainingProgrammeStatusName());
					outputBean.setProgStatusId(progDetail
							.getFkProgrammeStatusId());
					outputBean.setEndDate(progDetail.getTrainingEndDateTime());
					outputBean.setDurationInDays(progDetail
							.getTrainingDurationInDays() + "");
					/*outputBean.setDurationInHours(progDetail
							.getTrainingDurationInHours() + "");*/
					outputBean.setLevelOfParticipants(progDetail
							.getTrainingDatRequestDetail()
							.getLevelOfParticipants());
					outputBean.setNoOfHoursPerDay(progDetail
							.getTrainingNoOfHrsPerDay() + "");
					outputBean.setMaxSeats(progDetail.getMaximumSeats());
					outputBean.setApprovedOrRejectedBy(null);

					if (progDetail.getTrainingDatRequestDetail()
							.getPkTrainingRequestId() != null) {
						requestDetail.setRequestorEmpId(progDetail
								.getTrainingDatRequestDetail()
								.getPkTrainingRequestId());
						requestDetail
								.setRequestorName(getEmpNameByEmpId(progDetail
										.getTrainingDatRequestDetail()
										.getFkTrainingRequesterEmpId()));
					} else {
						requestDetail.setRequestorEmpId(null);
						requestDetail.setRequestorName("");
					}
					requestDetail.setTrainingRequestId(progDetail
							.getTrainingDatRequestDetail()
							.getPkTrainingRequestId());

					requestDetail.setTopicId(progDetail
							.getTrainingDatRequestDetail()
							.getFkTrainingTopicNameId());
					requestDetail.setTopicName(progDetail
							.getTrainingDatRequestDetail()
							.getMasTrainingTopicName().getTrainingTopicName());

					if (progDetail.getTrainingDatRequestDetail()
							.getLevelOfTrainingRequest() == 1)
						requestDetail.setLevelOfTrainingRequest("BASIC");
					if (progDetail.getTrainingDatRequestDetail()
							.getLevelOfTrainingRequest() == 2)
						requestDetail.setLevelOfTrainingRequest("INTERMEDIATE");
					if (progDetail.getTrainingDatRequestDetail()
							.getLevelOfTrainingRequest() == 3)
						requestDetail.setLevelOfTrainingRequest("ADVANCED");

					requestDetail.setProductVersion(progDetail
							.getTrainingDatRequestDetail()
							.getProductVersionForTraining());
					requestDetail.setNoOfParticipants(progDetail
							.getTrainingDatRequestDetail()
							.getNoOfParticipants());
					requestDetail.setLevelOfParticipants(progDetail
							.getTrainingDatRequestDetail()
							.getLevelOfParticipants());
                  
					if(progDetail.getTrainingDatRequestDetail()
							.getUsageOfProduct() != null)
					{
					if (progDetail.getTrainingDatRequestDetail()
							.getUsageOfProduct() == 1)
						requestDetail.setUsageOfProduct("DEVELOPMENT");
					if (progDetail.getTrainingDatRequestDetail()
							.getUsageOfProduct() == 2)
						requestDetail.setUsageOfProduct("ADMINISTRATIVE");
					if (progDetail.getTrainingDatRequestDetail()
							.getUsageOfProduct() == 3)
						requestDetail.setUsageOfProduct("OTHERS");
					}
					requestDetail.setBriefTrainingContent(progDetail
							.getTrainingDatRequestDetail()
							.getBriefTrainingContent());
					requestDetail.setIsSoftwareLicenseAvailable(progDetail
							.getTrainingDatRequestDetail()
							.getIsSoftwareLicenceAvailable());
					requestDetail.setExpectedTrainingStartDate(progDetail
							.getTrainingDatRequestDetail()
							.getExpectedTrainingStartDate());
					requestDetail.setPreferredTrainingStartTime(progDetail
							.getTrainingDatRequestDetail()
							.getPreferredTrainingStartTime());
					requestDetail.setPreferredTrainingEndTime(progDetail
							.getTrainingDatRequestDetail()
							.getPreferredTrainingEndTime());
					requestDetail.setExternalTrainerRequest(progDetail
							.getTrainingDatRequestDetail()
							.getExternalTrainerRequest());
					requestDetail.setSpecificInputComments(progDetail
							.getTrainingDatRequestDetail()
							.getSpecificInputComments());
					requestDetail.setTrainingRequestedDate(progDetail
							.getTrainingDatRequestDetail()
							.getTrainingRequestedDate());
					requestDetail.setTrainingModifiedDate(progDetail
							.getTrainingDatRequestDetail()
							.getTrainingModifiedDate());
					requestDetail.setRequestStatusId(progDetail
							.getTrainingDatRequestDetail()
							.getFkTrainingRequestStatusId());
					requestDetail.setRequestStatusName(progDetail
							.getTrainingDatRequestDetail()
							.getTrainingMasRequestStatus()
							.getTrainingRequestStatusName());
					requestDetail.setComments(progDetail
							.getTrainingDatRequestDetail().getComments());
					requestDetail.setIsProgramCreated(progDetail
							.getTrainingDatRequestDetail()
							.getIsTrainingProgramCreated());
					outputBean.setRequestDetails(requestDetail);
					
					
				}
			}
		} catch (Exception e) {

			throw new DataAccessException("Exception occured", e);
		}
		LOG.endUsecase("Get Training Program Based On Training ProgramId And Set Nomination Status");
		return outputBean;
	}

	/**
	 * 
	 * @param bean
	 * @return outputBean2
	 * @throws DataAccessException
	 * @Description :Service is used to get all Employee Nominated Training
	 *              Programs and also Training Coordinator created Training
	 *              Program by Date Both RM created and Training Coordinator
	 *              Created
	 */
	public List<TrainingProgramOutputBean> retrieveProgramDetails(
			TrainingDatProgrammeDetailBean bean) throws DataAccessException {
		TrainingProgramOutputBean trainingProgrammeOutputBean;
		List<TrainingProgramOutputBean> outputBean1 = new ArrayList<TrainingProgramOutputBean>();
		List<TrainingProgramOutputBean> outputBean2 = new ArrayList<TrainingProgramOutputBean>();
		List<TrainingProgramOutputBean> outputList = new ArrayList<TrainingProgramOutputBean>();
		List<TrainingProgramOutputBean> tempList = new ArrayList<TrainingProgramOutputBean>();
		TrainingDatProgrammeDetailBO progDetail1 = new TrainingDatProgrammeDetailBO();
		DatEmpProfessionalDetailBO empProfDetail = null;
		List<TrainingDatNominationDetailBO> nominationDetail = null;
		/*
		 * List<TrainingDatRequestDetailBO> requestedList = null;
		 * List<TrainingDatRequestDetailBO> requestList = null;
		 */
		LOG.startUsecase("Retrieve Program Details");
		try {
			Date truncatedStartDate = DateTimeUtil.dateWithMinTimeofDay(bean
					.getTrainingDate());
			Date endDate = DateTimeUtil.dateWithMaxTimeofDay(bean
					.getTrainingDate());
			bean.setTrainingDate(truncatedStartDate);

			List<TrainingDatRequestDetailBO> requestList = trainingRequestRepository
					.findByfkTrainingRequesterEmpIdIsNull();

			nominationDetail = trainingNominationRepository
					.findByFkDatNominatedEmpIdAndNominationStatus(
							bean.getEmpId(), "NOMINATED");

			List<TrainingDatRequestDetailBO> requestedList = trainingRequestRepository
					.findByFkTrainingRequesterEmpId(bean.getEmpId());

			empProfDetail = empProfessionalRepository
					.findByFkMainEmpDetailId(bean.getEmpId());

			if (nominationDetail.size() > 0 || nominationDetail != null) {
				for (TrainingDatNominationDetailBO nomDetail : nominationDetail) {
					if (nomDetail.getTrainingDatProgrammeDetail()
							.getTrainingStartDateTime().getTime() >= bean
							.getTrainingDate().getTime()
							&& nomDetail.getTrainingDatProgrammeDetail()
									.getTrainingStartDateTime().getTime() <= endDate
									.getTime()) {
						if (nomDetail.getTrainingDatProgrammeDetail()
								.getTrainingDatRequestDetail()
								.getFkTrainingRequesterEmpId() != null) {
							trainingProgrammeOutputBean = new TrainingProgramOutputBean();
							trainingProgrammeOutputBean.setProgId(nomDetail
									.getTrainingDatProgrammeDetail()
									.getPkTrainingProgrammeId());
							trainingProgrammeOutputBean
									.setBriefContent(nomDetail
											.getTrainingDatProgrammeDetail()
											.getTrainingDatRequestDetail()
											.getBriefTrainingContent());
							trainingProgrammeOutputBean.setTrainer(nomDetail
									.getTrainingDatProgrammeDetail()
									.getTrainerName());
							trainingProgrammeOutputBean
									.setTrainingTitle(nomDetail
											.getTrainingDatProgrammeDetail()
											.getTrainingDatRequestDetail()
											.getMasTrainingTopicName()
											.getTrainingTopicName());
							trainingProgrammeOutputBean.setVenue(nomDetail
									.getTrainingDatProgrammeDetail()
									.getTrainingMasRoomDetail()
									.getTrainingRoomName());

							trainingProgrammeOutputBean.setEmpId(bean
									.getEmpId());

							outputBean2.add(trainingProgrammeOutputBean);
						}
					}
				}
			}
			if (requestedList.size() > 0 || requestedList != null) {
				for (TrainingDatRequestDetailBO req : requestedList) {
					if (req.getIsTrainingProgramCreated().equalsIgnoreCase(
							"YES")) {
						progDetail1 = trainingProgramRepository
								.findByFkTrainingRequestId(req
										.getPkTrainingRequestId());
						if (progDetail1 != null) {
							if (empProfDetail != null) {
								if (progDetail1.getTrainingStartDateTime()
										.getTime() >= bean.getTrainingDate()
										.getTime()
										&& progDetail1
												.getTrainingStartDateTime()
												.getTime() <= endDate.getTime()) {
									trainingProgrammeOutputBean = new TrainingProgramOutputBean();
									trainingProgrammeOutputBean
											.setProgId(progDetail1
													.getPkTrainingProgrammeId());
									trainingProgrammeOutputBean
											.setBriefContent(progDetail1
													.getTrainingDatRequestDetail()
													.getBriefTrainingContent());
									trainingProgrammeOutputBean
											.setTrainer(progDetail1
													.getTrainerName());
									trainingProgrammeOutputBean
											.setTrainingTitle(progDetail1
													.getTrainingDatRequestDetail()
													.getMasTrainingTopicName()
													.getTrainingTopicName());
									trainingProgrammeOutputBean
											.setVenue(progDetail1
													.getTrainingMasRoomDetail()
													.getTrainingRoomName());

									trainingProgrammeOutputBean.setEmpId(bean
											.getEmpId());

									outputBean2
											.add(trainingProgrammeOutputBean);
								}
							}
						}
					}
				}
			}
			if (requestList.size() > 0 || requestList != null) {
				for (TrainingDatRequestDetailBO req : requestList) {
					if (req.getIsTrainingProgramCreated().equalsIgnoreCase(
							"YES")) {
						progDetail1 = trainingProgramRepository
								.findByFkTrainingRequestId(req
										.getPkTrainingRequestId());
						if (progDetail1.getTrainingStartDateTime().getTime() >= bean
								.getTrainingDate().getTime()
								&& progDetail1.getTrainingStartDateTime()
										.getTime() <= endDate.getTime()) {
							trainingProgrammeOutputBean = new TrainingProgramOutputBean();
							trainingProgrammeOutputBean.setProgId(progDetail1
									.getPkTrainingProgrammeId());
							trainingProgrammeOutputBean
									.setBriefContent(progDetail1
											.getTrainingDatRequestDetail()
											.getBriefTrainingContent());
							trainingProgrammeOutputBean.setTrainer(progDetail1
									.getTrainerName());
							trainingProgrammeOutputBean
									.setTrainingTitle(progDetail1
											.getTrainingDatRequestDetail()
											.getMasTrainingTopicName()
											.getTrainingTopicName());
							trainingProgrammeOutputBean.setVenue(progDetail1
									.getTrainingMasRoomDetail()
									.getTrainingRoomName());

							trainingProgrammeOutputBean.setEmpId(bean
									.getEmpId());

							if (empProfDetail.getHasReporteesFlag()
									.equalsIgnoreCase(
											TrainingConstants.HAS_REPORTEES)) {
								outputBean2.add(trainingProgrammeOutputBean);
							} else {
								if (progDetail1
										.getTrainingDatRequestDetail()
										.getLevelOfParticipants()
										.contains(
												empProfDetail
														.getMasEmpLevelBO()
														.getEmpLevelName())) {
									outputBean2
											.add(trainingProgrammeOutputBean);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Exception occured", e);
		}
		LOG.endUsecase("Retrieve Program Details");
		return removeDuplicatesFromProgramList(outputBean2);
	}

	/**
	 * This method is used to remove duplicates from a Program list
	 */
	private List<TrainingProgramOutputBean> removeDuplicatesFromProgramList(
			List<TrainingProgramOutputBean> progsList) {
		List<TrainingProgramOutputBean> progList = new ArrayList<TrainingProgramOutputBean>();

		Map<Integer, TrainingProgramOutputBean> progMap = new HashMap<Integer, TrainingProgramOutputBean>();
		for (TrainingProgramOutputBean u : progsList) {
			if (!progMap.containsKey(u.getProgId())) {
				progMap.put(u.getProgId(), u);
			}
		}

		progList.addAll(progMap.values());
		return progList;
	}

	// EOA by Pratibha
	// Added by Mani
	/**
	 * 
	 * @return nominationDetailsUploadExcelBeanList
	 * @throws IOException
	 * @throws CommonCustomException
	 * @throws ParseException
	 */
	public List<NominationDetailsUploadExcelBean> uploadNominationDetails(
			List<NominationDetailsUploadExcelBean> nominationDetailsUploadList,
			Integer updatedBy) throws IOException, CommonCustomException,
			ParseException {
		LOG.startUsecase("uploadNominationDetails");
		List<NominationDetailsUploadExcelBean> nominationDetailsUploadExcelBeanList = new ArrayList<NominationDetailsUploadExcelBean>();
		boolean result = false;

		for (NominationDetailsUploadExcelBean bean : nominationDetailsUploadList) {
			int empId = bean.getEmployeeId().intValue();
			int programeId = bean.getProgramId().intValue();
			TrainingDatNominationDetailBO nominationDetails = getNominationDetailsByEmpIdAndProgramId(
					empId, programeId);
			if (nominationDetails != null) {
				result = true;
				nominationDetails.setAttendenceUpdateDate(new Date());
				nominationDetails.setAttendedTraining(bean
						.getAteendanceIsYesOrNo());// From Bean
				nominationDetails.setFkAttendenceUpdateBy(updatedBy);
				nominationDetails = trainingNominationRepository
						.save(nominationDetails);
				bean.setAteendanceIsYesOrNo(nominationDetails
						.getAttendedTraining());
				bean.setAttendanceUpdatedBy(nominationDetails
						.getFkAttendenceUpdateBy());
				bean.setUpdatedOn(nominationDetails.getAttendenceUpdateDate());
				nominationDetailsUploadExcelBeanList.add(bean);
			} else {
				result = false;
				throw new CommonCustomException(
						"unable to find the details from training nomination details "
								+ "from the input of '" + empId
								+ "' and ' programeId of '" + programeId + "'");
			}
		}

		LOG.endUsecase("uploadNominationDetails");
		return nominationDetailsUploadExcelBeanList;
	}

	/**
	 * 
	 * @param excelSheetDataList
	 * @return nominationDetailsUploadList
	 * @throws CommonCustomException
	 */
	/*
	 * List<NominationDetailsUploadExcelBean> convertToNominationDetailsBean(
	 * List<ExcelSheetData> excelSheetDataList) throws CommonCustomException {
	 * LOG.startUsecase("convertToNominationDetailsBean"); int ROW_HEADER = 0;
	 * 
	 * int PROGRAM_ID_INDEX = 0, PROGRAM_NAME_INDEX = 1, EMPLOYEE_ID_INDEX = 2,
	 * EMPLOYEE_NAME_INDEX = 3, IS_NOMINATED_STATUS_INDEX = 4;
	 * 
	 * List<NominationDetailsUploadExcelBean> nominationDetailsUploadList = new
	 * ArrayList<NominationDetailsUploadExcelBean>();
	 * 
	 * for (ExcelSheetData excelData : excelSheetDataList) {
	 * 
	 * NominationDetailsUploadExcelBean nominationDetails = new
	 * NominationDetailsUploadExcelBean();
	 * 
	 * for (ExcelData cellData : excelData.getRowData()) {
	 * 
	 * if (cellData.getRowIndex() != ROW_HEADER) { try { if
	 * (cellData.getCellIndex() == PROGRAM_ID_INDEX) { try { if
	 * (cellData.getCellData().isEmpty() || cellData.getCellData() == null ||
	 * cellData.getCellData().trim() .equalsIgnoreCase("")) { throw new
	 * CommonCustomException( "Program id should not to be empty or null"); }
	 * else { Float programId = Float.parseFloat(cellData .getCellData());
	 * nominationDetails.setProgramId(programId .intValue()); } } catch
	 * (NumberFormatException e) { throw new CommonCustomException(
	 * "Invalid Input.(Unable to parse the program id from  " +
	 * cellData.getCellData() + " value)"); } } else if (cellData.getCellIndex()
	 * == PROGRAM_NAME_INDEX) { if (cellData.getCellData().isEmpty() ||
	 * cellData.getCellData() == null || cellData.getCellData().trim()
	 * .equalsIgnoreCase("")) { throw new CommonCustomException(
	 * "Program Name should not to be empty or null"); } else {
	 * nominationDetails.setProgramName(cellData .getCellData()); } } else if
	 * (cellData.getCellIndex() == EMPLOYEE_ID_INDEX) { try { if
	 * (cellData.getCellData().isEmpty() || cellData.getCellData() == null ||
	 * cellData.getCellData().trim() .equalsIgnoreCase("")) { throw new
	 * CommonCustomException( "Employee Id should not to be empty or null"); }
	 * else { Float empId = Float.parseFloat(cellData .getCellData());
	 * nominationDetails.setEmployeeId(empId .intValue()); } } catch
	 * (NumberFormatException e) { throw new CommonCustomException(
	 * "Invalid Input.(Unable to parse the Employee id from  " +
	 * cellData.getCellData() + " value)"); } } else if (cellData.getCellIndex()
	 * == EMPLOYEE_NAME_INDEX) { if (cellData.getCellData().isEmpty() ||
	 * cellData.getCellData() == null || cellData.getCellData().trim()
	 * .equalsIgnoreCase("")) { throw new CommonCustomException(
	 * "Employee Name should not to be empty or null"); } else {
	 * nominationDetails.setEmployeeName(cellData .getCellData()); } } else if
	 * (cellData.getCellIndex() == IS_NOMINATED_STATUS_INDEX) { if
	 * (cellData.getCellData().isEmpty() || cellData.getCellData() == null ||
	 * cellData.getCellData().trim() .equalsIgnoreCase("")) { throw new
	 * CommonCustomException( "Status  should not to be empty or null"); } else
	 * { if (cellData.getCellData().trim() .equalsIgnoreCase("YES") ||
	 * cellData.getCellData().trim() .equalsIgnoreCase("NO")) {
	 * nominationDetails .setAteendanceIsYesOrNo(cellData .getCellData()); }
	 * else { throw new CommonCustomException(
	 * "Attendance Status Should be YES/NO"); } } } else { throw new
	 * CommonCustomException(
	 * "As per Template row data size should be maximum 5 data per row.Please check with MIS team If any concerns."
	 * ); }
	 * 
	 * } catch (NumberFormatException e) { throw new CommonCustomException(
	 * "Unable to parse the cell value of '" + cellData.getCellData() +
	 * "' to Number Format. Please contact to administrator"); } } }// Row
	 * iterate loop end if (nominationDetails.getEmployeeId() != null &&
	 * nominationDetails.getProgramId() != null) {
	 * nominationDetailsUploadList.add(nominationDetails); }
	 * 
	 * }// Excel sheet data iterate loop end
	 * LOG.endUsecase("convertToNominationDetailsBean"); return
	 * nominationDetailsUploadList; }
	 */

	/**
	 * 
	 * @param empId
	 * @param programId
	 * @return nominationDetails
	 */
	TrainingDatNominationDetailBO getNominationDetailsByEmpIdAndProgramId(
			int empId, int programId) {
		LOG.startUsecase("getNominationDetailsByEmpIdAndProgramId");
		TrainingDatNominationDetailBO nominationDetails = trainingNominationRepository
				.findByFkDatNominatedEmpIdAndFkDatTrainingProgrammeId(empId,
						programId);
		LOG.endUsecase("getNominationDetailsByEmpIdAndProgramId");
		return nominationDetails;
	}

	/**
	 * 
	 * @param nominationDetailsUploadList
	 * @return result
	 * @throws CommonCustomException
	 */
	boolean isValidExcelData(
			List<NominationDetailsUploadExcelBean> nominationDetailsUploadList)
			throws CommonCustomException {
		LOG.startUsecase("nominationDetailsUploadList");
		boolean result = true;
		for (NominationDetailsUploadExcelBean bean : nominationDetailsUploadList) {
			TrainingDatNominationDetailBO nominationDetails = getNominationDetailsByEmpIdAndProgramId(
					bean.getEmployeeId().intValue(), bean.getProgramId()
							.intValue());
			if (nominationDetails == null) {
				result = false;
				throw new CommonCustomException(
						"Wrong input data contains the row of "
								+ bean.toString());
			}
		}
		LOG.endUsecase("nominationDetailsUploadList");
		return result;
	}

	private String getNominationStatusName(byte nominationStatus) {
		String nominationStatusName = "";

		switch (nominationStatus) {
		case 1:
			nominationStatusName = "INTERESTED";
			break;
		case 2:
			nominationStatusName = "NOT INTERESTED";
			break;
		case 3:
			nominationStatusName = "APPROVED";
			break;
		case 4:
			nominationStatusName = "REJECTED";
			break;
		case 5:
			nominationStatusName = "WITHDRAWN";
			break;
		case 6:
			nominationStatusName = "INVITED";
			break;
		default:
			nominationStatusName = "";
			break;
		}
		return nominationStatusName;
	}
	// End of addition by Mani
}