/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingRequestService.java                       */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contain all the methods related to     */
/*                  the TrainingRequestService 		                 */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.DateTimeUtil;
import com.thbs.mis.notificationframework.trainingnotification.service.TrainingNotificationService;
import com.thbs.mis.training.bean.CreateTrainingRequestBean;
import com.thbs.mis.training.bean.PagenationBean;
import com.thbs.mis.training.bean.TrainingRequestBean;
import com.thbs.mis.training.bean.TrainingRequestBeanTrainingCoordinator;
import com.thbs.mis.training.bean.TrainingRequestDetailViewBean;
import com.thbs.mis.training.bean.TrainingRequestsCoordinatorViewBean;
import com.thbs.mis.training.bean.TrainingRequestsViewBean;
import com.thbs.mis.training.bean.UpdateTrainingRequestBean;
import com.thbs.mis.training.bean.UpdateTrainingRequestStatusBean;
import com.thbs.mis.training.bo.TrainingDatProgrammeDetailBO;
import com.thbs.mis.training.bo.TrainingDatRequestDetailBO;
import com.thbs.mis.training.bo.TrainingMasRequestStatusBO;
import com.thbs.mis.training.dao.TrainingProgrammeRepository;
import com.thbs.mis.training.dao.TrainingRequestRepository;
import com.thbs.mis.training.dao.TrainingRequestStatusRepository;

/**
 * <Description EmployeeSrvc:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
@Service
public class TrainingRequestService {

	// Added by Kamal Anand

	@Autowired
	private TrainingRequestRepository trainingRequestRepository;

	@Autowired
	private EmployeePersonalDetailsRepository empPersonalDetailRepository;

	@Autowired
	private TrainingProgrammeRepository programRepository;

	@Autowired
	private EmpDetailRepository empDetailRepository;

	@Autowired
	private TrainingRequestStatusRepository requestStatusRepository;
	
	@Autowired
	private TrainingNotificationService notificationService;

	@Value("${training.request.approved.status}")
	private byte requestApprovedStatus;
	
	@Value("${training.request.pending.status}")
	private byte requestPendingStatus;
	
	@Value("${training.request.rejected.status}")
	private byte requestRejectedStatus;

	@Value("${training.notification.roleid}")
	private String trainingTeamRoleIds;
	
	private static final AppLog LOG = LogFactory
			.getLog(TrainingRequestService.class);

	/**
	 * 
	 * @param trainingRequestBean
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to create a new training request
	 */
	public TrainingDatRequestDetailBO createTrainingRequest(
			CreateTrainingRequestBean trainingRequestBean)
			throws DataAccessException {
		LOG.startUsecase("Create Training Request");
		TrainingDatRequestDetailBO trainingRequestDetail = new TrainingDatRequestDetailBO();
		TrainingDatRequestDetailBO requestDetailBO;
		List<Short> trainingTeamRoleIdList = Arrays
				.asList(trainingTeamRoleIds.split(",")).stream()
				.map(Short::valueOf).collect(Collectors.toList());
		
		trainingRequestDetail.setFkTrainingRequesterEmpId(trainingRequestBean
				.getRequestorEmpId());
		trainingRequestDetail.setFkTrainingTopicNameId(trainingRequestBean
				.getTrainingTopicId());
		trainingRequestDetail.setLevelOfTrainingRequest(trainingRequestBean
				.getLevelTrainingRequest());
		trainingRequestDetail.setProductVersionForTraining(trainingRequestBean
				.getProductVersion());
		trainingRequestDetail.setNoOfParticipants(trainingRequestBean
				.getNoOfParticipants());
		trainingRequestDetail.setLevelOfParticipants(trainingRequestBean
				.getLevelOfParticipants());
		trainingRequestDetail.setUsageOfProduct(trainingRequestBean
				.getUsageOfProduct());
		trainingRequestDetail.setBriefTrainingContent(trainingRequestBean
				.getBriefTrainingContent());
		trainingRequestDetail.setIsSoftwareLicenceAvailable(trainingRequestBean
				.getIsSoftwareLicenseAvailable());
		trainingRequestDetail.setNoOfLicensesAvailbale(trainingRequestBean
				.getNoOfLicensesAvailable());
		trainingRequestDetail.setExpectedTrainingStartDate(trainingRequestBean
				.getExpectedTrainingStartDate());
		trainingRequestDetail.setPreferredTrainingStartTime(trainingRequestBean
				.getPreferredTrainingStartTime());
		trainingRequestDetail.setPreferredTrainingEndTime(trainingRequestBean
				.getPreferredTrainingEndTime());
		trainingRequestDetail.setExternalTrainerRequest(trainingRequestBean
				.getExternalTrainerRequest());
		trainingRequestDetail.setSpecificInputComments(trainingRequestBean
				.getSpecificInputComments());
		trainingRequestDetail.setTrainingRequestedDate(new Date());
		trainingRequestDetail.setFkTrainingRequestStatusId((byte) 1);
		trainingRequestDetail.setComments(trainingRequestBean.getComments());
		trainingRequestDetail.setIsTrainingProgramCreated("NO");
		trainingRequestDetail.setProgramCategory(trainingRequestBean
				.getProgramCategory());

		try {
			requestDetailBO = trainingRequestRepository
					.save(trainingRequestDetail);
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured in Create Training Request: ", e);
		}
		//Added by Kamal Anand for Notifications
		try {
			List<Integer> empIdList = empDetailRepository.getActiveEmployeesForRoleId(trainingTeamRoleIdList);
			if(empIdList != null)
			{
				empIdList.forEach(empId -> {
					try {
						notificationService.setTrainingRequestNotificationToTCScreen(empId);
					} catch (Exception e) {
						LOG.info("Some exception occurred while setting Create Training Request Notification");
					}
				});
			}
		} catch(Exception e){
			LOG.info("Exception occurred while getting empId for Training coordinator roles");
		}
		//End of Addition by Kamal Anand for Notifications
		LOG.endUsecase("Create Training Request");
		return requestDetailBO;
	}

	/**
	 * 
	 * @param trainingRequestBean
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to update a training request
	 */
	public TrainingDatRequestDetailBO updateTrainingRequest(
			UpdateTrainingRequestBean trainingRequestBean)
			throws DataAccessException, CommonCustomException {

		TrainingDatRequestDetailBO trainingRequestDetail = new TrainingDatRequestDetailBO();
		TrainingDatRequestDetailBO resultBo = null;
		LOG.startUsecase("Update Training Request");
		try {

			trainingRequestDetail = trainingRequestRepository
					.findByPkTrainingRequestId(trainingRequestBean
							.getTrainingRequestId());

			if (trainingRequestDetail != null) {

				if (trainingRequestDetail.getIsTrainingProgramCreated()
						.equalsIgnoreCase("NO")) {
					trainingRequestDetail
							.setPkTrainingRequestId(trainingRequestBean
									.getTrainingRequestId());
					if (trainingRequestBean.getRequestorEmpId() != null)
						trainingRequestDetail
								.setFkTrainingRequesterEmpId(trainingRequestBean
										.getRequestorEmpId());
					if (trainingRequestBean.getTrainingTopicId() > 0)
						trainingRequestDetail
								.setFkTrainingTopicNameId(trainingRequestBean
										.getTrainingTopicId());
					if (trainingRequestBean.getLevelTrainingRequest() > 0)
						trainingRequestDetail
								.setLevelOfTrainingRequest(trainingRequestBean
										.getLevelTrainingRequest());
					if (trainingRequestBean.getProductVersion() != null)
						trainingRequestDetail
								.setProductVersionForTraining(trainingRequestBean
										.getProductVersion());
					if (trainingRequestBean.getNoOfParticipants() != null)
						trainingRequestDetail
								.setNoOfParticipants(trainingRequestBean
										.getNoOfParticipants());
					if (trainingRequestBean.getLevelOfParticipants() != null)
						trainingRequestDetail
								.setLevelOfParticipants(trainingRequestBean
										.getLevelOfParticipants());
					if (trainingRequestBean.getUsageOfProduct() > 0)
						trainingRequestDetail
								.setUsageOfProduct(trainingRequestBean
										.getUsageOfProduct());
					if (trainingRequestBean.getBriefTrainingContent() != null)
						trainingRequestDetail
								.setBriefTrainingContent(trainingRequestBean
										.getBriefTrainingContent());
					if (trainingRequestBean.getIsSoftwareLicenseAvailable() != null)
						trainingRequestDetail
								.setIsSoftwareLicenceAvailable(trainingRequestBean
										.getIsSoftwareLicenseAvailable());
					if (trainingRequestBean.getNoOfLicensesAvailable() != null)
						trainingRequestDetail
								.setNoOfLicensesAvailbale(trainingRequestBean
										.getNoOfLicensesAvailable());
					if (trainingRequestBean.getExpectedTrainingStartDate() != null)
						trainingRequestDetail
								.setExpectedTrainingStartDate(trainingRequestBean
										.getExpectedTrainingStartDate());
					if (trainingRequestBean.getPreferredTrainingStartTime() != null)
						trainingRequestDetail
								.setPreferredTrainingStartTime(trainingRequestBean
										.getPreferredTrainingStartTime());
					if (trainingRequestBean.getPreferredTrainingEndTime() != null)
						trainingRequestDetail
								.setPreferredTrainingEndTime(trainingRequestBean
										.getPreferredTrainingEndTime());
					if (trainingRequestBean.getExternalTrainerRequest() != null)
						trainingRequestDetail
								.setExternalTrainerRequest(trainingRequestBean
										.getExternalTrainerRequest());
					if (trainingRequestBean.getSpecificInputComments() != null)
						trainingRequestDetail
								.setSpecificInputComments(trainingRequestBean
										.getSpecificInputComments());
					trainingRequestDetail.setTrainingModifiedDate(new Date());
					if (trainingRequestBean.getRequestStatus() > 0)
						trainingRequestDetail
								.setFkTrainingRequestStatusId(trainingRequestBean
										.getRequestStatus());
					if (trainingRequestBean.getComments() != null)
						trainingRequestDetail.setComments(trainingRequestBean
								.getComments());

					if (trainingRequestBean.getProgramCategory() != null)
						trainingRequestDetail
								.setProgramCategory(trainingRequestBean
										.getProgramCategory());

					resultBo = trainingRequestRepository
							.save(trainingRequestDetail);
				} else {
					throw new CommonCustomException(
							"Training Request Detail cannot be modified as Training Program has already been created");
				}
			} else {
				throw new DataAccessException(
						"Training Request Details not found");
			}

		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception Occured in Update Training Request: ", e);
		}
		LOG.endUsecase("Update Training Request");
		return resultBo;
	}

	/**
	 * 
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to View All Training Requests by
	 *              Training Coordinator
	 */
	public Page<TrainingRequestsCoordinatorViewBean> viewAllTrainingRequestsByTrainingManager(
			PagenationBean pageDetailBean) throws DataAccessException {
		LOG.startUsecase("View All Training Requests By Training Manager");
		List<TrainingDatRequestDetailBO> trainingRequestsList = null;
		TrainingRequestsCoordinatorViewBean viewRequestsBean;
		List<TrainingRequestsCoordinatorViewBean> trainingRequestsViewList = new ArrayList<TrainingRequestsCoordinatorViewBean>();
		DatEmpPersonalDetailBO empPersonalDetail;
		Page finalPage;
		Pageable pageable = new PageRequest(pageDetailBean.getPageNumber(),
				pageDetailBean.getPageSize());
		try {
			trainingRequestsList = trainingRequestRepository
					.findByfkTrainingRequesterEmpIdNotNull();
			if (trainingRequestsList.size() > 0) {
				for (TrainingDatRequestDetailBO requestDetail : trainingRequestsList) {
					empPersonalDetail = new DatEmpPersonalDetailBO();
					viewRequestsBean = new TrainingRequestsCoordinatorViewBean();
					viewRequestsBean.setTrainingDate(requestDetail
							.getTrainingRequestedDate());
					viewRequestsBean.setTrainingTitle(requestDetail
							.getMasTrainingTopicName().getTrainingTopicName());
					viewRequestsBean.setTrainingDescription(requestDetail
							.getSpecificInputComments());
					viewRequestsBean.setNoOfParticipants(requestDetail
							.getNoOfParticipants());
					viewRequestsBean.setStatus(requestDetail
							.getTrainingMasRequestStatus()
							.getTrainingRequestStatusName());
					viewRequestsBean.setMgrId(requestDetail
							.getFkTrainingRequesterEmpId());
					viewRequestsBean.setRequestId(requestDetail
							.getPkTrainingRequestId());

					empPersonalDetail = empPersonalDetailRepository
							.findByFkEmpDetailId(requestDetail
									.getFkTrainingRequesterEmpId());

					viewRequestsBean.setMgrName(empPersonalDetail
							.getEmpFirstName()
							+ " "
							+ empPersonalDetail.getEmpLastName());
					trainingRequestsViewList.add(viewRequestsBean);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured in View All Training Requests by Training Coordinator: ",
					e);
		}
		finalPage = new PageImpl(trainingRequestsViewList, pageable,
				trainingRequestsViewList.size());
		LOG.endUsecase("View All Training Requests By Training Manager");
		return finalPage;
	}

	/**
	 * 
	 * @param mgrId
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to View All Training Requests By
	 *              Reporting Manager
	 */
	public List<TrainingRequestsViewBean> viewAllTrainingRequestsByReportingMgrId(
			int mgrId) throws DataAccessException {
		LOG.startUsecase("view All Training Requests By Reporting MgrId");
		List<TrainingDatRequestDetailBO> trainingRequestsList = null;
		TrainingRequestsViewBean viewRequestsBean;
		List<TrainingRequestsViewBean> trainingRequestsViewList = new ArrayList<TrainingRequestsViewBean>();
		DatEmpPersonalDetailBO empPersonalDetail;
		TrainingDatProgrammeDetailBO progDetail = null;
		try {

			trainingRequestsList = trainingRequestRepository
					.findByFkTrainingRequesterEmpId(mgrId);
			if (trainingRequestsList.size() > 0) {
				for (TrainingDatRequestDetailBO requestDetail : trainingRequestsList) {
					empPersonalDetail = new DatEmpPersonalDetailBO();
					viewRequestsBean = new TrainingRequestsViewBean();
					viewRequestsBean.setTrainingDate(requestDetail
							.getTrainingRequestedDate());
					viewRequestsBean.setTrainingTitle(requestDetail
							.getMasTrainingTopicName().getTrainingTopicName());
					viewRequestsBean.setTrainingDescription(requestDetail
							.getSpecificInputComments());
					viewRequestsBean.setNoOfParticipants(requestDetail
							.getNoOfParticipants());
					viewRequestsBean.setStatus(requestDetail
							.getTrainingMasRequestStatus()
							.getTrainingRequestStatusName());
					viewRequestsBean.setIsProgramCreated(requestDetail
							.getIsTrainingProgramCreated());
					viewRequestsBean.setRequestId(requestDetail
							.getPkTrainingRequestId());
					if (requestDetail.getIsTrainingProgramCreated()
							.equalsIgnoreCase("YES")) {
						progDetail = programRepository
								.findByFkTrainingRequestId(requestDetail
										.getPkTrainingRequestId());
						viewRequestsBean.setProgId(progDetail
								.getPkTrainingProgrammeId());
						viewRequestsBean.setProgramStatus(progDetail
								.getFkProgrammeStatusId());
					} else {
						viewRequestsBean.setProgId(0);
						viewRequestsBean.setProgramStatus(null);
					}

					empPersonalDetail = empPersonalDetailRepository
							.findByFkEmpDetailId(requestDetail
									.getFkTrainingRequesterEmpId());

					viewRequestsBean.setMgrName(empPersonalDetail
							.getEmpFirstName()
							+ " "
							+ empPersonalDetail.getEmpLastName());
					trainingRequestsViewList.add(viewRequestsBean);
				}
			} else {
				throw new DataAccessException(
						"No Training Request Details Found");
			}
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("view All Training Requests By Reporting MgrId");
		return trainingRequestsViewList;

	}

	/**
	 * 
	 * @param trainingRequestBean
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to View All Training Requests By
	 *              Status or by Date Range for a Training Coordinator
	 */
	public Page<TrainingRequestsCoordinatorViewBean> viewTrainingRequestDetailsByTrainingMgrStatusAndDateRange(
			TrainingRequestBeanTrainingCoordinator trainingRequestBean)
			throws DataAccessException {
		LOG.startUsecase("view Training Request Details By TrainingMgr Status And Date Range");
		List<TrainingDatRequestDetailBO> trainingRequestsList = null;
		TrainingRequestsCoordinatorViewBean viewRequestsBean;
		List<TrainingRequestsCoordinatorViewBean> trainingRequestsViewList = new ArrayList<TrainingRequestsCoordinatorViewBean>();
		DatEmpPersonalDetailBO empPersonalDetail;
		Date modifiedStartDate = null;
		Date modifiedEndDate = null;
		Byte requestStatus = 0;
		if (trainingRequestBean.getStatus() != null)
			requestStatus = Byte
					.parseByte(trainingRequestBean.getStatus() + "");

		Page finalPage;
		Pageable pageable = new PageRequest(
				trainingRequestBean.getPageNumber(),
				trainingRequestBean.getPageSize());
		try {
			if (trainingRequestBean.getStartDate() != null
					&& trainingRequestBean.getEndDate() != null) {
				modifiedStartDate = DateTimeUtil
						.dateWithMinTimeofDay(trainingRequestBean
								.getStartDate());
				modifiedEndDate = DateTimeUtil
						.dateWithMaxTimeofDay(trainingRequestBean.getEndDate());
				trainingRequestBean.setStartDate(modifiedStartDate);
				trainingRequestBean.setEndDate(modifiedEndDate);
			}
			if (trainingRequestBean.getStatus() != null) {
				if (trainingRequestBean.getStartDate() != null
						&& trainingRequestBean.getEndDate() != null) {

					if (trainingRequestBean.getStatus() == 0) {
						trainingRequestsList = trainingRequestRepository
								.findByTrainingRequestedDateBetweenAndFkTrainingRequesterEmpIdNotNull(
										trainingRequestBean.getStartDate(),
										trainingRequestBean.getEndDate());
					}
					if (trainingRequestBean.getStatus() > 0) {
						trainingRequestsList = trainingRequestRepository
								.findByFkTrainingRequestStatusIdAndTrainingRequestedDateBetweenAndFkTrainingRequesterEmpIdNotNull(
										requestStatus,
										trainingRequestBean.getStartDate(),
										trainingRequestBean.getEndDate());
					}
				} else {
					if (trainingRequestBean.getStatus() == 0) {
						trainingRequestsList = trainingRequestRepository
								.findByfkTrainingRequesterEmpIdNotNull();
					}
					if (trainingRequestBean.getStatus() > 0) {
						trainingRequestsList = trainingRequestRepository
								.findByFkTrainingRequestStatusIdAndFkTrainingRequesterEmpIdNotNull(requestStatus);
					}
				}
			}
			if (trainingRequestBean.getStatus() == null) {

				trainingRequestsList = trainingRequestRepository
						.findByTrainingRequestedDateBetweenAndFkTrainingRequesterEmpIdNotNull(
								trainingRequestBean.getStartDate(),
								trainingRequestBean.getEndDate());
			}
			if (trainingRequestsList.size() > 0) {
				for (TrainingDatRequestDetailBO requestDetail : trainingRequestsList) {
					if (trainingRequestBean.getMgrId() != null) {
						if (requestDetail.getFkTrainingRequesterEmpId().equals(
								trainingRequestBean.getMgrId())) {
							empPersonalDetail = new DatEmpPersonalDetailBO();
							viewRequestsBean = new TrainingRequestsCoordinatorViewBean();
							viewRequestsBean.setTrainingDate(requestDetail
									.getTrainingRequestedDate());
							viewRequestsBean.setTrainingTitle(requestDetail
									.getMasTrainingTopicName()
									.getTrainingTopicName());
							viewRequestsBean
									.setTrainingDescription(requestDetail
											.getSpecificInputComments());
							viewRequestsBean.setNoOfParticipants(requestDetail
									.getNoOfParticipants());
							viewRequestsBean.setStatus(requestDetail
									.getTrainingMasRequestStatus()
									.getTrainingRequestStatusName());

							empPersonalDetail = empPersonalDetailRepository
									.findByFkEmpDetailId(requestDetail
											.getFkTrainingRequesterEmpId());

							viewRequestsBean.setMgrName(empPersonalDetail
									.getEmpFirstName()
									+ " "
									+ empPersonalDetail.getEmpLastName());
							viewRequestsBean.setMgrId(requestDetail
									.getFkTrainingRequesterEmpId());
							viewRequestsBean.setRequestId(requestDetail
									.getPkTrainingRequestId());
							viewRequestsBean.setIsProgramCreated(requestDetail
									.getIsTrainingProgramCreated());
							trainingRequestsViewList.add(viewRequestsBean);
						}
					} else {
						empPersonalDetail = new DatEmpPersonalDetailBO();
						viewRequestsBean = new TrainingRequestsCoordinatorViewBean();
						viewRequestsBean.setTrainingDate(requestDetail
								.getTrainingRequestedDate());
						viewRequestsBean.setTrainingTitle(requestDetail
								.getMasTrainingTopicName()
								.getTrainingTopicName());
						viewRequestsBean.setTrainingDescription(requestDetail
								.getSpecificInputComments());
						viewRequestsBean.setNoOfParticipants(requestDetail
								.getNoOfParticipants());
						viewRequestsBean.setStatus(requestDetail
								.getTrainingMasRequestStatus()
								.getTrainingRequestStatusName());

						empPersonalDetail = empPersonalDetailRepository
								.findByFkEmpDetailId(requestDetail
										.getFkTrainingRequesterEmpId());

						viewRequestsBean.setMgrName(empPersonalDetail
								.getEmpFirstName()
								+ " "
								+ empPersonalDetail.getEmpLastName());
						viewRequestsBean.setMgrId(requestDetail
								.getFkTrainingRequesterEmpId());
						viewRequestsBean.setRequestId(requestDetail
								.getPkTrainingRequestId());
						viewRequestsBean.setIsProgramCreated(requestDetail
								.getIsTrainingProgramCreated());
						trainingRequestsViewList.add(viewRequestsBean);
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured in View All Training Requests by Status and Date Range: ",
					e);
		}
		finalPage = new PageImpl(trainingRequestsViewList, pageable,
				trainingRequestsViewList.size());
		LOG.endUsecase("view Training Request Details By TrainingMgr Status And Date Range");
		return finalPage;
	}

	/**
	 * 
	 * @param trainingRequestBean
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to View All Training Request by Status
	 *              or by Date Range for a Reporting Manager
	 */
	public List<TrainingRequestsViewBean> viewTrainingRequestByReportingManagerStatusAndDateRange(
			TrainingRequestBean trainingRequestBean) throws DataAccessException {
		LOG.startUsecase("view Training Request By Reporting Manager Status And Date Range");
		List<TrainingDatRequestDetailBO> trainingRequestsList = null;
		TrainingRequestsViewBean viewRequestsBean;
		List<TrainingRequestsViewBean> trainingRequestsViewList = new ArrayList<TrainingRequestsViewBean>();
		DatEmpPersonalDetailBO empPersonalDetail;
		Byte requestStatus = 0;
		TrainingDatProgrammeDetailBO progDetail = null;
		Date modifiedStartDate = null;
		Date modifiedEndDate = null;

		if (trainingRequestBean.getStatus() != null)
			requestStatus = Byte
					.parseByte(trainingRequestBean.getStatus() + "");
		try {
			if (trainingRequestBean.getStartDate() != null
					&& trainingRequestBean.getEndDate() != null) {
				modifiedStartDate = DateTimeUtil
						.dateWithMinTimeofDay(trainingRequestBean
								.getStartDate());
				modifiedEndDate = DateTimeUtil
						.dateWithMaxTimeofDay(trainingRequestBean.getEndDate());
				trainingRequestBean.setStartDate(modifiedStartDate);
				trainingRequestBean.setEndDate(modifiedEndDate);
			}

			if (trainingRequestBean.getStatus() != null) {
				if (trainingRequestBean.getStartDate() != null
						&& trainingRequestBean.getEndDate() != null) {

					if (trainingRequestBean.getStatus() == 0) {
						trainingRequestsList = trainingRequestRepository
								.findByFkTrainingRequesterEmpIdAndTrainingRequestedDateBetween(
										trainingRequestBean.getMgrId(),
										trainingRequestBean.getStartDate(),
										trainingRequestBean.getEndDate());
					}
					if (trainingRequestBean.getStatus() > 0) {
						trainingRequestsList = trainingRequestRepository
								.findByFkTrainingRequesterEmpIdAndFkTrainingRequestStatusIdAndTrainingRequestedDateBetween(
										trainingRequestBean.getMgrId(),
										requestStatus,
										trainingRequestBean.getStartDate(),
										trainingRequestBean.getEndDate());
					}
				} else {
					if (trainingRequestBean.getStatus() == 0) {
						trainingRequestsList = trainingRequestRepository
								.findByFkTrainingRequesterEmpId(trainingRequestBean
										.getMgrId());
					}
					if (trainingRequestBean.getStatus() > 0) {
						trainingRequestsList = trainingRequestRepository
								.findByFkTrainingRequestStatusIdAndFkTrainingRequesterEmpId(
										requestStatus,
										trainingRequestBean.getMgrId());
					}
				}
			}
			if (trainingRequestBean.getStatus() == null) {
				trainingRequestsList = trainingRequestRepository
						.findByFkTrainingRequesterEmpIdAndTrainingRequestedDateBetween(
								trainingRequestBean.getMgrId(),
								trainingRequestBean.getStartDate(),
								trainingRequestBean.getEndDate());
			}

			if (trainingRequestsList.size() > 0) {
				for (TrainingDatRequestDetailBO requestDetail : trainingRequestsList) {
					empPersonalDetail = new DatEmpPersonalDetailBO();
					viewRequestsBean = new TrainingRequestsViewBean();
					viewRequestsBean.setTrainingDate(requestDetail
							.getTrainingRequestedDate());
					viewRequestsBean.setTrainingTitle(requestDetail
							.getMasTrainingTopicName().getTrainingTopicName());
					viewRequestsBean.setTrainingDescription(requestDetail
							.getSpecificInputComments());
					viewRequestsBean.setNoOfParticipants(requestDetail
							.getNoOfParticipants());
					viewRequestsBean.setStatus(requestDetail
							.getTrainingMasRequestStatus()
							.getTrainingRequestStatusName());
					viewRequestsBean.setIsProgramCreated(requestDetail
							.getIsTrainingProgramCreated());

					if (requestDetail.getIsTrainingProgramCreated()
							.equalsIgnoreCase("YES")) {
						progDetail = programRepository
								.findByFkTrainingRequestId(requestDetail
										.getPkTrainingRequestId());
						viewRequestsBean.setProgId(progDetail
								.getPkTrainingProgrammeId());
						viewRequestsBean.setProgramStatus(progDetail
								.getFkProgrammeStatusId());
					} else {
						viewRequestsBean.setProgId(0);
						viewRequestsBean.setProgramStatus(null);
					}
					empPersonalDetail = empPersonalDetailRepository
							.findByFkEmpDetailId(requestDetail
									.getFkTrainingRequesterEmpId());

					viewRequestsBean.setMgrName(empPersonalDetail
							.getEmpFirstName()
							+ " "
							+ empPersonalDetail.getEmpLastName());

					viewRequestsBean.setRequestId(requestDetail
							.getPkTrainingRequestId());
					trainingRequestsViewList.add(viewRequestsBean);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured in View Training Requests by status or Date Range for Reporting Manager: ",
					e);
		}
		LOG.endUsecase("view Training Request By Reporting Manager Status And Date Range");
		return trainingRequestsViewList;
	}

	/**
	 * 
	 * @param reqId
	 * @return
	 * @throws DataAccessException
	 * @Description : This service is used to get Training Request Details based
	 *              on Training Request Id
	 */
	public TrainingRequestDetailViewBean viewATrainingRequestByRequestId(
			int reqId) throws DataAccessException, CommonCustomException {
		LOG.startUsecase("View A Training Request By RequestId");
		TrainingRequestDetailViewBean requestDetailView = new TrainingRequestDetailViewBean();
		DatEmpPersonalDetailBO empPersonalDetail = null;
		TrainingDatRequestDetailBO requestDetail = null;
		try {
			requestDetail = trainingRequestRepository
					.findByPkTrainingRequestId(reqId);
			if (requestDetail != null) {
				requestDetailView.setTrainingTopicName(requestDetail
						.getMasTrainingTopicName().getTrainingTopicName());
				if(requestDetail
						.getUsageOfProduct() != null)
				requestDetailView.setUsageOfProductId(requestDetail
						.getUsageOfProduct());
				requestDetailView.setLevelOfTrainingId(requestDetail
						.getLevelOfTrainingRequest());
				if (requestDetail.getLevelOfTrainingRequest() == (byte) 1)
					requestDetailView.setLevelOfTraining("Basic");
				else if (requestDetail.getLevelOfTrainingRequest() == (byte) 2)
					requestDetailView.setLevelOfTraining("Intermediate");
				else if (requestDetail.getLevelOfTrainingRequest() == (byte) 3)
					requestDetailView.setLevelOfTraining("Advanced");
				requestDetailView.setVersionNumber(requestDetail
						.getProductVersionForTraining());
				requestDetailView.setNoOfParticipants(requestDetail
						.getNoOfParticipants());
				
				if(requestDetail.getUsageOfProduct() != null)
				{		
				if (requestDetail.getUsageOfProduct() == (byte) 1)
					requestDetailView.setUsageOfProduct("Development");
				else if (requestDetail.getUsageOfProduct() == (byte) 2)
					requestDetailView.setUsageOfProduct("Administrative");
				else if (requestDetail.getUsageOfProduct() == (byte) 3)
					requestDetailView.setUsageOfProduct("Others");
				}
				requestDetailView.setBriefCourseContent(requestDetail
						.getBriefTrainingContent());
				requestDetailView.setLevelOfParticipants(requestDetail
						.getLevelOfParticipants());
				requestDetailView.setIsSoftwareLicensesAvailable(requestDetail
						.getIsSoftwareLicenceAvailable());
				requestDetailView.setPreferredStartDate(requestDetail
						.getExpectedTrainingStartDate());
				requestDetailView.setExternalTrainerRequest(requestDetail
						.getExternalTrainerRequest());
				requestDetailView.setPreferredStartTime(requestDetail
						.getPreferredTrainingStartTime());
				requestDetailView.setPreferredEndTime(requestDetail
						.getPreferredTrainingEndTime());
				if(requestDetail
						.getSpecificInputComments() != null)
				requestDetailView.setSpecificInputs(requestDetail
						.getSpecificInputComments());
				requestDetailView.setRequestId(requestDetail
						.getPkTrainingRequestId());
				requestDetailView.setTrainingTopicId(requestDetail
						.getFkTrainingTopicNameId());
				requestDetailView.setNoOfLicenseAvailable(requestDetail
						.getNoOfLicensesAvailbale());
				requestDetailView.setIsProgramCreated(requestDetail
						.getIsTrainingProgramCreated());
				requestDetailView.setProgramCategoryId(requestDetail
						.getProgramCategory());
				requestDetailView
						.setProgramCategoryName(getProgramCategoryNameById(requestDetail
								.getProgramCategory()));
				if (requestDetail.getFkTrainingRequesterEmpId() != null) {
					empPersonalDetail = empPersonalDetailRepository
							.findByFkEmpDetailId(requestDetail
									.getFkTrainingRequesterEmpId());
					requestDetailView.setRequestorName(empPersonalDetail
							.getEmpFirstName()
							+ " "
							+ empPersonalDetail.getEmpLastName());
				} else {
					requestDetailView.setRequestorName(" ");
				}
				requestDetailView.setStatusId(requestDetail
						.getFkTrainingRequestStatusId());
				requestDetailView.setStatusName(requestDetail
						.getTrainingMasRequestStatus()
						.getTrainingRequestStatusName());
				requestDetailView.setRequestorId(requestDetail
						.getFkTrainingRequesterEmpId());

			} else {
				throw new DataAccessException(
						"No training requests for the requested ID");
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("View A Training Request By RequestId");
		return requestDetailView;
	}

	/**
	 * 
	 * @param statusId
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to View All Training Request Details
	 *              By Status
	 */
	public List<TrainingRequestsCoordinatorViewBean> viewAllTrainingRequestDetailsByStatusTrainingMgr(
			int statusId) throws DataAccessException {
		LOG.startUsecase("view All Training Request Details By Status TrainingMgr");
		List<TrainingDatRequestDetailBO> trainingRequestsList = null;
		TrainingRequestsCoordinatorViewBean viewRequestsBean;
		List<TrainingRequestsCoordinatorViewBean> trainingRequestsViewList = new ArrayList<TrainingRequestsCoordinatorViewBean>();
		DatEmpPersonalDetailBO empPersonalDetail;
		try {
			trainingRequestsList = trainingRequestRepository
					.findByfkTrainingRequesterEmpIdNotNullAndFkTrainingRequestStatusId((byte) statusId);
			if (trainingRequestsList.size() > 0) {
				for (TrainingDatRequestDetailBO requestDetail : trainingRequestsList) {
					empPersonalDetail = new DatEmpPersonalDetailBO();
					viewRequestsBean = new TrainingRequestsCoordinatorViewBean();
					viewRequestsBean.setTrainingDate(requestDetail
							.getTrainingRequestedDate());
					viewRequestsBean.setTrainingTitle(requestDetail
							.getMasTrainingTopicName().getTrainingTopicName());
					if(requestDetail
					.getSpecificInputComments() != null)
					viewRequestsBean.setTrainingDescription(requestDetail
							.getSpecificInputComments());
					viewRequestsBean.setNoOfParticipants(requestDetail
							.getNoOfParticipants());
					viewRequestsBean.setStatus(requestDetail
							.getTrainingMasRequestStatus()
							.getTrainingRequestStatusName());
					viewRequestsBean.setMgrId(requestDetail
							.getFkTrainingRequesterEmpId());
					viewRequestsBean.setRequestId(requestDetail
							.getPkTrainingRequestId());
					viewRequestsBean.setIsProgramCreated(requestDetail
							.getIsTrainingProgramCreated());

					empPersonalDetail = empPersonalDetailRepository
							.findByFkEmpDetailId(requestDetail
									.getFkTrainingRequesterEmpId());

					viewRequestsBean.setMgrName(empPersonalDetail
							.getEmpFirstName()
							+ " "
							+ empPersonalDetail.getEmpLastName());
					trainingRequestsViewList.add(viewRequestsBean);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured in View All Training Requests by Training Coordinator: ",
					e);
		}
		LOG.endUsecase("view All Training Request Details By Status TrainingMgr");
		return trainingRequestsViewList;

	}

	/**
	 * 
	 * @param reqId
	 * @return
	 * @throws DataAccessException
	 * @Description: This method is used to Training Request Details based on
	 *               Training Request Id
	 */
	public TrainingDatRequestDetailBO getRequestId(int reqId)
			throws DataAccessException {
		LOG.startUsecase("Entered getRequestId");
		TrainingDatRequestDetailBO reqInfo = new TrainingDatRequestDetailBO();
		try {
			reqInfo = trainingRequestRepository
					.findByPkTrainingRequestId(reqId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get the data from Database");
		}
		LOG.endUsecase("Exited getRequestId");
		return reqInfo;
	}

	/**
	 * 
	 * @param empId
	 * @return
	 * @throws DataAccessException
	 * @Description: This method is used to Employee Details based on Employee
	 *               Id
	 */
	public DatEmpDetailBO getEmployeeId(int empId) throws DataAccessException {
		LOG.startUsecase("Entered getEmployeeId");
		DatEmpDetailBO empInfo = new DatEmpDetailBO();
		try {
			empInfo = empDetailRepository.findByPkEmpId(empId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get the data from Database");
		}
		LOG.endUsecase("Exited getEmployeeId");
		return empInfo;
	}

	/**
	 * 
	 * @param reqId
	 * @return
	 * @throws DataAccessException
	 * @Description: This method is used to get Request Status details based on
	 *               status id
	 */
	public TrainingMasRequestStatusBO getRequestStatus(byte reqId)
			throws DataAccessException {
		LOG.startUsecase("Entered getRequestStatus");
		TrainingMasRequestStatusBO reqStatus = new TrainingMasRequestStatusBO();
		try {
			reqStatus = requestStatusRepository
					.findByPkTrainingRequestStatusId(reqId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get the data from Database");
		}
		LOG.endUsecase("Exited getRequestStatus");
		return reqStatus;
	}

	public List<TrainingRequestsCoordinatorViewBean> getApprovedTrainingRequests()
			throws DataAccessException {
		List<TrainingRequestsCoordinatorViewBean> trainingRequestList = new ArrayList<TrainingRequestsCoordinatorViewBean>();
		TrainingRequestsCoordinatorViewBean trainingRequest = null;
		List<TrainingDatRequestDetailBO> requestDetails = null;
		DatEmpPersonalDetailBO empPersonalDetail = null;
		try {
			requestDetails = trainingRequestRepository
					.findByFkTrainingRequestStatusIdAndIsTrainingProgramCreated(
							requestApprovedStatus, "NO");

			if (requestDetails != null) {
				for (TrainingDatRequestDetailBO requestDetail : requestDetails) {
					empPersonalDetail = new DatEmpPersonalDetailBO();
					trainingRequest = new TrainingRequestsCoordinatorViewBean();
					trainingRequest.setTrainingDate(requestDetail
							.getTrainingRequestedDate());
					trainingRequest.setTrainingTitle(requestDetail
							.getMasTrainingTopicName().getTrainingTopicName());
					if(requestDetail
							.getSpecificInputComments() != null)
					trainingRequest.setTrainingDescription(requestDetail
							.getSpecificInputComments());
					trainingRequest.setNoOfParticipants(requestDetail
							.getNoOfParticipants());
					trainingRequest.setStatus(requestDetail
							.getTrainingMasRequestStatus()
							.getTrainingRequestStatusName());
					trainingRequest.setMgrId(requestDetail
							.getFkTrainingRequesterEmpId());
					trainingRequest.setRequestId(requestDetail
							.getPkTrainingRequestId());
					trainingRequest.setIsProgramCreated(requestDetail
							.getIsTrainingProgramCreated());

					empPersonalDetail = empPersonalDetailRepository
							.findByFkEmpDetailId(requestDetail
									.getFkTrainingRequesterEmpId());

					if (empPersonalDetail != null) {
						trainingRequest.setMgrName(empPersonalDetail
								.getEmpFirstName()
								+ " "
								+ empPersonalDetail.getEmpLastName());
					} else {
						throw new DataAccessException(
								"Some error happeneed while getting Employee Personal Details");
					}

					trainingRequestList.add(trainingRequest);
				}
			} else {
				throw new DataAccessException(
						"No Training Request Details found");
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		return trainingRequestList;
	}

	/**
	 * 
	 * @param id
	 * @return programCategoryName
	 * @Description : This method is used to get category name by category id
	 */
	public static String getProgramCategoryNameById(Byte id) {
		LOG.startUsecase("Entering getProgramCategoryNameById method");
		String catName = "";
		if (id == 1)
			catName = "Soft Skills";
		if (id == 2)
			catName = "Technical";
		if (id == 3)
			catName = "Process";
		LOG.endUsecase("Exiting getProgramCategoryNameById method");
		return catName;
	}

	// End of Addition by Kamal Anand

	// Added by Rajesh

	/**
	 * 
	 * <Description updateTrainingRequestStatus:> method is call the repository
	 * method to update the Request Status data in the data base. It will take
	 * the BO and passes it to the Repository for update the Record.
	 * 
	 * @param TrainingDatRequestDetailBO
	 *            is the argument for this method.
	 * 
	 * @return return the list of data in Training Dat Request Detail BO
	 *         according to the input details.
	 * 
	 * @throws BusinessException
	 *             throws if any exception occurs during the data retrieval.
	 * @throws CommonCustomException
	 * 
	 */
	public TrainingDatRequestDetailBO updateTrainingRequestStatus(
			UpdateTrainingRequestStatusBean bean) throws BusinessException,
			CommonCustomException {
		LOG.startUsecase("Update Training Request Status");
		TrainingDatRequestDetailBO trainingRequestDetail = null;
		try {
			trainingRequestDetail = trainingRequestRepository
					.findByPkTrainingRequestId(bean.getPkTrainingRequestId());
			if (trainingRequestDetail == null) {
				throw new CommonCustomException("ID does not exist");
			}
			trainingRequestDetail.setFkTrainingRequestStatusId(bean
					.getFk_training_request_status_id());
			trainingRequestDetail.setComments(bean.getComments());
			trainingRequestDetail.setTrainingModifiedDate(new Date());
			trainingRequestDetail = trainingRequestRepository
					.save(trainingRequestDetail);
			//Added by Kamal Anand for Notifications
			try {
				if (bean.getFk_training_request_status_id().equals(
						requestApprovedStatus)) {
					notificationService
							.setApproveTrainingRequestNotificationToRmScreen(trainingRequestDetail
									.getFkTrainingRequesterEmpId());
					notificationService
							.setOnHoldTrainingRequestNotificationToRmScreen(trainingRequestDetail
									.getFkTrainingRequesterEmpId());
				}
				if (bean.getFk_training_request_status_id().equals(
						requestPendingStatus))
					notificationService
							.setOnHoldTrainingRequestNotificationToRmScreen(trainingRequestDetail
									.getFkTrainingRequesterEmpId());
				if (bean.getFk_training_request_status_id().equals(
						requestRejectedStatus)) {
					notificationService
							.setRejectTrainingRequestNotificationToRmScreen(trainingRequestDetail
									.getFkTrainingRequesterEmpId());
					notificationService
							.setOnHoldTrainingRequestNotificationToRmScreen(trainingRequestDetail
									.getFkTrainingRequesterEmpId());
				}
			} catch(Exception e){
				LOG.info("Some exception occurred while setting update training request status notiication");
			}
			//End of Addition by Kamal Anand for Notifications
		} catch (CommonCustomException e) {
			throw new BusinessException(e.getMessage());
		}
		LOG.endUsecase("Update Training Request Status");
		return trainingRequestDetail;
	}
	// EOA by Rajesh

}
