/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingCommonService.java                        */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contain all the methods related to     */
/*                  the TrainingCommonService 		                 */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.MasFeedbackQuestionInfoBO;
import com.thbs.mis.common.bo.MasTrainingTopicNameBO;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.training.bean.TrainingFeedbackQuestionBean;
import com.thbs.mis.training.bo.TrainingMasRequestStatusBO;
import com.thbs.mis.training.bo.TrainingMasRoomDetailBO;
import com.thbs.mis.training.dao.FeedbackQuestionInfoRepository;
import com.thbs.mis.training.dao.TrainingRequestStatusRepository;
import com.thbs.mis.training.dao.TrainingRoomDetailRepository;
import com.thbs.mis.training.dao.TrainingTopicsRepository;

@Service
public class TrainingCommonService {

	private static final AppLog LOG = LogFactory
			.getLog(TrainingCommonService.class);

	@Autowired
	private TrainingTopicsRepository trainingTopicsRepository;

	@Autowired
	private TrainingRoomDetailRepository trainingRoomDetailRepos;

	@Autowired
	private TrainingRequestStatusRepository trainingRequestStatusRepos;

	@Autowired
	private FeedbackQuestionInfoRepository feedbackRepository;

	// Added by Smrithi

	/**
	 * @author THBS
	 * @return List<MasTrainingTopicNameBO> List of Training topic names
	 * @throws DataAccessException
	 * @Description: This method is used to get the training topics.
	 */
	public List<MasTrainingTopicNameBO> getMasTrainingTopics()
			throws DataAccessException {
		List<MasTrainingTopicNameBO> trainingTopicBO;

		LOG.startUsecase("getTrainingTopics");
		try {

			trainingTopicBO = trainingTopicsRepository.findAll();

		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new DataAccessException("Exception occured", e);
		}
		LOG.endUsecase("getTrainingTopics");
		return trainingTopicBO;
	}

	// EOA by Smrithi

	// Added by Rajesh

	/**
	 * 
	 * <Description getTrainingRoomDetail:> method is call the repository method
	 * to view the Training Room Detail data in the data base. It will take the
	 * BO and passes it to the Repository for update the Record.
	 * 
	 * @param TrainingMasRoomDetailBO
	 *            is the argument for this method.
	 * 
	 * @return return the list of data in Training Mas Room Detail BO according
	 *         to the input details.
	 * 
	 * @throws BusinessException
	 *             throws if any exception occurs during the data retrieval.
	 * 
	 */
	public List<TrainingMasRoomDetailBO> getTrainingRoomDetail()
			throws BusinessException {
		List<TrainingMasRoomDetailBO> trainingRoomDetailsList = null;

		LOG.debug("About to retrieve data");
		try {
			LOG.info("Entering save()");

			trainingRoomDetailsList = trainingRoomDetailRepos.findAll();

			LOG.info("Exiting save()");

		} catch (Exception e) {
			throw new BusinessException("Exception Occured : ", e);
		}
		return trainingRoomDetailsList;

	}

	/**
	 * 
	 * <Description getTrainingRequestStatus:> method is call the repository
	 * method to view the Training Request Status data in the data base. It will
	 * take the BO and passes it to the Repository for view the Record.
	 * 
	 * @param TrainingMasRequestStatusBO
	 *            is the argument for this method.
	 * 
	 * @return return the list of data in Training Mas Request Status BO
	 *         according to the input details.
	 * 
	 * @throws BusinessException
	 *             throws if any exception occurs during the data retrieval.
	 * 
	 */
	public List<TrainingMasRequestStatusBO> getTrainingRequestStatus()
			throws BusinessException { // Rajesh Kumar
		List<TrainingMasRequestStatusBO> trainingReqStatus = null;

		LOG.debug("About to retrieve data");
		try {
			LOG.info("Entering save()");

			trainingReqStatus = trainingRequestStatusRepos.findAll();

			LOG.info("Exiting save()");

		} catch (Exception e) {
			throw new BusinessException("Exception Occured : ", e);
		}
		return trainingReqStatus;

	}

	// EOA by Rajesh

	// Added by Pratibha

	/**
	 * 
	 * @param trainingFeedbackQuestionBean
	 * @return List<MasFeedbackQuestionInfoBO> returns list of feedBackQusetions
	 * @throws DataAccessException
	 * @Description :This service is used to get the feedback question based on
	 *              feedback status
	 */
	public List<MasFeedbackQuestionInfoBO> getFeedbackQuestion(
			TrainingFeedbackQuestionBean trainingFeedbackQuestionBean)
			throws DataAccessException {

		List<MasFeedbackQuestionInfoBO> questionInfo;
		LOG.startUsecase("getFeedbackQuestion");
		try {

			questionInfo = feedbackRepository
					.findByFeedbackQuestionStatusAndFkMasWorkFlowIdOrderByFeedbackQuestionOrderAsc(
							trainingFeedbackQuestionBean
									.getFeedbackQuestionStatus(),
							trainingFeedbackQuestionBean.getFkMasWorkFlowId());

		} catch (Exception e) {

			throw new DataAccessException("Exception occured", e);
		}
		LOG.endUsecase("getFeedbackQuestion");
		return questionInfo;
	}

	// EOA by Pratibha

	/**
	 * 
	 * @param roomId
	 * @return TrainingMasRoomDetailBO returns room details
	 * @throws DataAccessException
	 * @Description :This service is used to get the training room details based
	 *              on room id
	 */
	public TrainingMasRoomDetailBO getRoomDetails(Short roomId)
			throws DataAccessException {
		LOG.startUsecase("Entered getTrainingProgramStatus");
		TrainingMasRoomDetailBO progStatusInfo = new TrainingMasRoomDetailBO();
		try {
			progStatusInfo = trainingRoomDetailRepos
					.findByPkTrainingRoomId(roomId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get the data from Database");
		}
		LOG.endUsecase("Exited getTrainingProgramStatus");
		return progStatusInfo;
	}
}
