package com.thbs.mis.training.Validator;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.training.bean.TrainingRequestBean;


public class StartDateEndDateValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return TrainingRequestBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		TrainingRequestBean trainingRequestBean = (TrainingRequestBean) obj;
		Date startDate = trainingRequestBean.getStartDate();
		Date endDate = trainingRequestBean.getEndDate();
		if (startDate != null) {
			if (endDate == null) {
				e.rejectValue("endDate", "End date should not be null");
			} else {
				if (endDate.before(startDate)) {
					e.rejectValue("endDate",
							"End date is lesser than Start date");

				} else {
					System.out.println("Condition Success");
				}
			}

		}
	}
}
