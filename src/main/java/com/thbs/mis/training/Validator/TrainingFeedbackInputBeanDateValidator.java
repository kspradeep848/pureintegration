/*********************************************************************/
/*                  FILE HEADER                                                                                                              */
/*********************************************************************/
/*                                                                                                                                                               */
/*  FileName    :  TrainingFeedbackInputBeanDateValidator.java                                */
/*                                                                                                                                                               */
/*  Author      :  THBS		                                                                                                                   */
/*                                                                                                                                                               */
/*  Date        :  23-Jun-2016                                                                                                             */
/*                                                                                                                                                               */
/*  Description :  This class is used to validate                                                                       */
/*                the date range.			                                                                                                       */
/*                                                                                                                                                              */
/*                                                                                                                                                             */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                          */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                          */
/*********************************************************************/

package com.thbs.mis.training.Validator;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.training.bean.TrainingFeedbackInputBean;

public class TrainingFeedbackInputBeanDateValidator implements Validator {

	private final String feedbackStatus = "ALL,COMPLETED,PENDING FEEDBACK";

	@Override
	public boolean supports(Class<?> clazz) {
		return TrainingFeedbackInputBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		TrainingFeedbackInputBean trainingFeedbackBean = (TrainingFeedbackInputBean) obj;
		Date startDate = trainingFeedbackBean.getStartDate();
		Date endDate = trainingFeedbackBean.getEndDate();
		if (startDate != null) {
			if (endDate == null) {
				e.rejectValue("endDate", "End date should not be null");
			} else {
				if (endDate.before(startDate)) {
					e.rejectValue("endDate",
							"End date is lesser than Start date");

				} else {

				}
			}
		}
		if (trainingFeedbackBean.getStatus() != null) {
			if (!feedbackStatus.contains(trainingFeedbackBean.getStatus())) {
				e.rejectValue("status", "Status must be any one of "
						+ feedbackStatus);
			}
		}
		if (trainingFeedbackBean.getStatus() == null
				&& trainingFeedbackBean.getStartDate() == null
				&& trainingFeedbackBean.getEndDate() == null) {
			e.rejectValue("status", "Status or date range is mandatory field");
		}
		if (trainingFeedbackBean.getEmpId() == null) {
			e.rejectValue("empId", "Employee Id is mandatory field");
		}
		if (trainingFeedbackBean.getPageNumber() == null) {
			e.rejectValue("pageNumber", "Page Number Id is mandatory field");
		}
		if (trainingFeedbackBean.getPageSize() == null) {
			e.rejectValue("pageSize", "Page Size Id is mandatory field");
		}
	}
}
