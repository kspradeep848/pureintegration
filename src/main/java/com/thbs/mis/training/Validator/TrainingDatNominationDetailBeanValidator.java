package com.thbs.mis.training.Validator;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.training.bean.TrainingDatNominationDetailBean;

public class TrainingDatNominationDetailBeanValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return TrainingDatNominationDetailBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		TrainingDatNominationDetailBean trainingDatNominationDetailBean = (TrainingDatNominationDetailBean) obj;
		Date startDate = trainingDatNominationDetailBean.getStartDate();
		Date endDate = trainingDatNominationDetailBean.getEndDate();
		if (startDate != null) {
			if (endDate == null) {
				e.rejectValue("endDate", "End date should not be null");
			} else {
				if (endDate.before(startDate)) {
					e.rejectValue("endDate",
							"End date is lesser than Start date");

				} else {
					System.out.println("Condition Success");
				}
			}

		}
	}
}
