package com.thbs.mis.training.Validator;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.training.bean.TeamTrainingRequestBean;

public class TeamTrainingRequestValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return TeamTrainingRequestBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		TeamTrainingRequestBean trainingRequestBean = (TeamTrainingRequestBean) obj;
		Date startDate = trainingRequestBean.getTrainingStartDate();
		Date endDate = trainingRequestBean.getTrainingEndDate();

		if (startDate != null && endDate != null) {
						if (endDate.before(startDate)) {
							e.rejectValue("trainingEndDate",
									"End date is lesser than Start date");
						}

					} else if (startDate != null && endDate == null) {
						e.rejectValue("trainingEndDate", "end date should not be null");
					} else if (startDate == null && endDate != null) {
						e.rejectValue("trainingStartDate", "start date should not be null");
					}
	}

}
