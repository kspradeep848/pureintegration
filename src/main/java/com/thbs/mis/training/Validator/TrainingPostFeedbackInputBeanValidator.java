package com.thbs.mis.training.Validator;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.training.bean.TrainingPostFeedbackInputBean;

public class TrainingPostFeedbackInputBeanValidator implements Validator {

	private final String feedbackStatus = "ALL,COMPLETED,PENDING FEEDBACK";

	@Override
	public boolean supports(Class<?> clazz) {
		return TrainingPostFeedbackInputBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		TrainingPostFeedbackInputBean feedbackBean = (TrainingPostFeedbackInputBean) obj;
		Date startDate = feedbackBean.getStartDate();
		Date endDate = feedbackBean.getEndDate();
		if (startDate != null) {
			if (endDate == null) {
				e.rejectValue("endDate", "End date should not be null");
			} else {
				if (endDate.before(startDate)) {
					e.rejectValue("endDate",
							"End date is lesser than Start date");

				} else {
				}
			}

		}
		if (feedbackBean.getStatus() != null) {
			if (!feedbackStatus.contains(feedbackBean.getStatus())) {
				e.rejectValue("status", "Status must be any one of "
						+ feedbackStatus);
			}
		}
		if (feedbackBean.getStatus() == null
				&& feedbackBean.getStartDate() == null
				&& feedbackBean.getEndDate() == null) {
			e.rejectValue("status", "Status or date range is mandatory field");
		}
		if (feedbackBean.getPageNumber() == null) {
			e.rejectValue("pageNumber", "page number is a mandatory field");
		}
		if (feedbackBean.getPageSize() == null) {
			e.rejectValue("pageSize", " Page Size is a mandatory field");
		}
	}

}
