package com.thbs.mis.training.Validator;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.training.bean.TeamTrainingRequestBean;
import com.thbs.mis.training.bean.TrainingRequestDetailSubInputBean;

public class TrainingRequestDetailValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return TrainingRequestDetailSubInputBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		try {
			TrainingRequestDetailSubInputBean trainingRequestBean = (TrainingRequestDetailSubInputBean) obj;
			Date startDate = trainingRequestBean.getStartDate();
			Date endDate = trainingRequestBean.getEndDate();
			String nominationStatus = trainingRequestBean.getFeedbackStatus();
			if (startDate != null && endDate != null) {

				if (endDate.before(startDate)) {
					e.rejectValue("endDate",
							"End date is lesser than Start date");
				}

			} else if (startDate != null && endDate == null) {
				e.rejectValue("endDate", "end date should not be null");
			} else if (startDate == null && endDate != null) {
				e.rejectValue("startDate", "start date should not be null");
			}
			if (nominationStatus != null) {
				if (!nominationStatus.equals("ALL")
						&& !nominationStatus.equals("COMPLETED")
						&& !nominationStatus.equals("PENDING")) {
					e.rejectValue("feedbackStatus",
							"Please enter valid feedback status");

				}
			}
		} catch (Exception e1) {

			e1.printStackTrace();
		}
	}

}
