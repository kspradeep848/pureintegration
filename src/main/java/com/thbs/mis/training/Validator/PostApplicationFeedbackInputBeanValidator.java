package com.thbs.mis.training.Validator;

import java.util.Arrays;
import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.training.bean.PostApplicationFeedbackInputBean;

public class PostApplicationFeedbackInputBeanValidator implements Validator {

	private final String feedbackStatus = "ALL,1STAR,2STAR,3STAR,4STAR,5STAR";
	String[] statusArray = feedbackStatus.split(",");

	@Override
	public boolean supports(Class<?> clazz) {
		return PostApplicationFeedbackInputBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		PostApplicationFeedbackInputBean postApplicationFeedbackInputBean = (PostApplicationFeedbackInputBean) obj;
		Date startDate = postApplicationFeedbackInputBean.getStartDate();
		Date endDate = postApplicationFeedbackInputBean.getEndDate();
		if (startDate != null) {
			if (endDate == null) {
				e.rejectValue("endDate", "End date should not be null");
			} else {
				if (endDate.before(startDate)) {
					e.rejectValue("endDate",
							"End date is lesser than Start date");

				} else {
					System.out.println("Condition Success");
				}
			}

		}
		if (postApplicationFeedbackInputBean.getStatus() != null) {
			if ((!Arrays.asList(statusArray).contains(postApplicationFeedbackInputBean.getStatus()))) {
				e.rejectValue("status", "Status must be any one of the "
						+ feedbackStatus);
			}
		}
		if (postApplicationFeedbackInputBean.getStatus() == null
				&& postApplicationFeedbackInputBean.getStartDate() == null
				&& postApplicationFeedbackInputBean.getEndDate() == null) {
			e.rejectValue("status", "Either status or date range is mandatory");
		}
		if (postApplicationFeedbackInputBean.getPageNumber() == null) {
			e.rejectValue("pageNumber", "Page Number is a mandatory field");
		}
		if (postApplicationFeedbackInputBean.getPageSize() == null) {
			e.rejectValue("pageSize", "Page Size is a mandatory field");
		}
		if(postApplicationFeedbackInputBean.getMgrId() == null){
			e.rejectValue("mgrId", "Manager Id is a mandatory field");
		}
		if(postApplicationFeedbackInputBean.getProgId() == null){
			e.rejectValue("progId", "Program Id is a mandatory field");
		}
	}

}
