/*********************************************************************/
/*                  FILE HEADER                                                                                                              */
/*********************************************************************/
/*                                                                                                                                                               */
/*  FileName    :  AttendeeFeedbackInputBeanValidator.java                                         */
/*                                                                                                                                                               */
/*  Author      :  THBS		                                                                                                                   */
/*                                                                                                                                                               */
/*  Date        :  23-Jun-2016                                                                                                             */
/*                                                                                                                                                               */
/*  Description :  This class is used to validate                                                                       */
/*                the date range.			                                                                                                       */
/*                                                                                                                                                              */
/*                                                                                                                                                             */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                          */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                          */
/*********************************************************************/

package com.thbs.mis.training.Validator;

import java.util.Arrays;
import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.training.bean.AttendeeFeedbackInputBean;

public class AttendeeFeedbackInputBeanValidator implements Validator {

	private final String feedbackStatus = "ALL,1STAR,2STAR,3STAR,4STAR,5STAR";
	String[] statusArray = feedbackStatus.split(",");

	@Override
	public boolean supports(Class<?> clazz) {
		return AttendeeFeedbackInputBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		AttendeeFeedbackInputBean attendeeFeedbackInputBean = (AttendeeFeedbackInputBean) obj;
		Date startDate = attendeeFeedbackInputBean.getStartDate();
		Date endDate = attendeeFeedbackInputBean.getEndDate();
		if (startDate != null) {
			if (endDate == null) {
				e.rejectValue("endDate", "End date should not be null");
			} else {
				if (endDate.before(startDate)) {
					e.rejectValue("endDate",
							"End date is lesser than Start date");

				} else {

				}
			}
		}
		if (attendeeFeedbackInputBean.getStatus() != null) {
			if (!Arrays.asList(statusArray).contains(attendeeFeedbackInputBean.getStatus())) {
				e.rejectValue("status", "Status must be any one of "
						+ feedbackStatus);
			}
		}
		if (attendeeFeedbackInputBean.getStatus() == null
				&& attendeeFeedbackInputBean.getStartDate() == null
				&& attendeeFeedbackInputBean.getEndDate() == null) {
			e.rejectValue("status", "Status or date range is mandatory field");
		}
		if (attendeeFeedbackInputBean.getProgId() == null) {
			e.rejectValue("progId", "Program Id is mandatory field");
		}
		if (attendeeFeedbackInputBean.getPageNumber() == null) {
			e.rejectValue("pageNumber", "Page Number is a mandatory field");
		}
		if (attendeeFeedbackInputBean.getPageSize() == null) {
			e.rejectValue("pageSize", "Page Size is a mandatory field");
		}
	}
}
