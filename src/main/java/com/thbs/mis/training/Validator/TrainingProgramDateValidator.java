package com.thbs.mis.training.Validator;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.training.bean.TrainingProgramBean;

public class TrainingProgramDateValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return TrainingProgramBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		TrainingProgramBean trainingProgramBean = (TrainingProgramBean) obj;
		Date startDate = trainingProgramBean.getStartDate();
		Date endDate = trainingProgramBean.getEndDate();
		if (startDate != null) {
			if (endDate == null) {
				e.rejectValue("endDate", "End date should not be null");
			} else {
				if (endDate.before(startDate)) {
					e.rejectValue("endDate",
							"End date is lesser than Start date");

				} else {
					System.out.println("Condition Success");
				}
			}

		}
	}
}
