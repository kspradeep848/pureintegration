/*********************************************************************/
/*                  FILE HEADER                                      */

/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingRequestURIConstants.java                  */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  November 02, 2016                                 */
/*                                                                   */
/*  Description :  This class contains  all the Training Request     */
/*                    	 URIConstants                                */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 02, 2016     THBS     1.0        Initial version created */
/*********************************************************************/
package com.thbs.mis.training.constants;

public class TrainingRequestURIConstants {

	// Added by Kamal Anand
	public static final String CREATE_TRAINING_REQUEST = "/training/trainingrequest/create";
	public static final String UPDATE_TRAINING_REQUEST = "/training/trainingrequest/update";
	public static final String VIEW_ALL_TRAINING_REQUESTS_BY_TRAINING_MANAGER = "/training/trainingrequest/trainingmgr/view";
	public static final String VIEW_ALL_TRAINING_REQUEST_BY_REPORTING_MANAGER = "/training/trainingrequest/reportingmgr/{mgrId}";
	public static final String VIEW_TRAINING_REQUEST_BY_STATUS_DATERANGE_TRAINING_COORDINATOR = "/training/trainingrequest/trainingmgr";
	public static final String VIEW_TRAINING_REQUEST_BY_STATUS_DATERANGE_REPORTING_MANAGER = "/training/trainingrequest/reportingmgr";
	public static final String VIEW_TRAINING_REQUEST_BY_REQUEST_ID = "/training/trainingrequest/{reqId}";
	public static final String VIEW_TRAINING_REQUEST_BY_STATUS = "/training/trainingrequest/status/{status}";
	public static final String GET_APPROVED_TRAINING_REQUESTS = "/training/trainingrequest/approved";
	// End of Addition by Kamal Anand
	
	// Added by Rajesh 
	public static final String UPDATE_TRAINING_REQUEST_STATUS = "/training/updateTrainingRequestStatus";  
	// EOA by Rajesh
}
