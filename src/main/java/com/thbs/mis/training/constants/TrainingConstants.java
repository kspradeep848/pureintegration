package com.thbs.mis.training.constants;

public interface TrainingConstants {
	

	public static final String ALL_STATUS = "ALL";
	public static final String COMPLETED_STATUS = "COMPLETED";
	public static final String PENDING_FEEDBACK_STATUS = "PENDING FEEDBACK";

	public static final String ONE_STAR = "1STAR";
	public static final String TWO_STAR = "2STAR";
	public static final String THREE_STAR = "3STAR";
	public static final String FOUR_STAR = "4STAR";
	public static final String FIVE_STAR = "5STAR";
	public static final String ALL_STAUS = "ALL";

	public static final String HAS_REPORTEES = "YES_HAS_REPORTEES";
}
