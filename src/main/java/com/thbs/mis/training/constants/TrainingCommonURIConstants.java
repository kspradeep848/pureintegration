/*********************************************************************/
/*                  FILE HEADER                                      */

/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingCommonURIConstants.java                   */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  November 02, 2016                                 */
/*                                                                   */
/*  Description :  This class contains  all the training  common     */
/*                   URI constants 	                                 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 02, 2016     THBS     1.0        Initial version created */
/*********************************************************************/

package com.thbs.mis.training.constants;

/**
 * 
 * @author THBS
 * @Description: This is used to create URI for all the common services.
 */
public class TrainingCommonURIConstants {
	//Added by Smrithi
	public static final String GET_TRAINING_TOPICS_NAMES = "/training/topic/names";
	//EOA by Smrithi
	
	//Added by Rajesh
	public static final String GET_TRAINING_ROOM_DETAILS = "/training/roomdetails";
	
	public static final String GET_TRAINING_REQUEST_STATUS = "/training/trainingRequestStatus";
	//EOA by Rajesh
	
	//Added by Pratibha
	public static final String GET_FEEDBACK_QUESTION = "/training/feedback/question";
	//EOA by Pratibha
}

