package com.thbs.mis.training.constants;

public class TrainingPostFeedBackRestURIConstants {
	// Added by Smrithi
	public static final String CREATE_POST_ATTENDEES_FEEDBACK = "/training/post/attendees/feedback/create";
	// EOA by Smrithi

	// Added by Kamal Anand
	public static final String TRAINING_COORDINATOR_VIEW_POST_APPLICATION_FEEDBACK_ALL_PROGRAMS = "/training/mgrfeedback"; 
	public static final String TRAINING_COORDINATOR_VIEW_POST_APPLICATION_FEEDBACK_OF_TRAINING_PROGRAM = "/training/mgrfeedback/program"; 
	public static final String TRAINING_COORDINATOR_VIEW_DETAILED_POST_APPLICATION_FEEDBACK = "/training/mgrfeedback/employee/{empId}/program/{progId}"; 
	public static final String REPORTING_MANAGER_VIEW_DETAILED_POST_APPLICATION_FEEDBACK = "/training/mgrfeedback/{progId}/manager/{mgrId}"; 
	// EOA by Kamal Anand
}
