/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingRestURIConstants.java                                		 */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  22-Jun-2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 22-Jun-2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.training.constants;

/**
 * <Description TrainingRestURIConstants:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
public class TrainingRestURIConstants {

	// Added by Kamal Anand
	public static final String CREATE_TRAINING_PROGRAM = "/training/trainingprogram/create";
	public static final String UPDATE_TRAINING_PROGRAM = "/training/trainingprogram/update";
	public static final String VIEW_ALL_TRAINING_PROGRAMS = "/training/trainingprogram/view";
	public static final String VIEW_ALL_TRAINING_PROGRAMS_BY_STATUS_DATERANGE = "/training/trainingprogram";
	public static final String VIEW_ALL_REQUESTED_TRAINING_PROGRAMS = "/training/trainingprogram/reportingmanager/{mgrId}";
	public static final String NOMINATE_PARTICIPANTS = "/training/trainingprogram/nominate";
	public static final String GET_TRAINING_PROGRAM_ATTENDEES_BY_TRAININGPROGRAMID = "/training/trainingprogram/attendees/{progId}"; // For Team Member's Feedback - Details screen of Reporting Manager
	//public static final String GET_EMPLOYEE_NOMINATION_STATUS = "/training/trainingprogram/employee/{empId}/program/{progId}";
	public static final String GET_REPORTEES_TRAINING_NOMINATION_STATUS = "/training/trainingprogram/manager/{mgrId}/program/{progId}";
	public static final String VIEW_PROGRAM_DETAILS_TEAM_TRAINING_REQUESTS = "/training/trainingprogram/{progId}/employee/{empId}";
	public static final String UPDATE_TRAINING_PROGRAM_STATUS = "/training/trainingprogram/status";
	public static final String GET_ALL_ACTIVE_EMPLOYEE_TRAINING_NOMINATION_STATUS = "/training/trainingprogram/employee/nomination/{progId}";
	public static final String GET_NOMINATED_EMPLOYEES_BY_TRAININGPROGRAMID = "/training/trainingprogram/nominated/{progId}";
	public static final String REMOVE_PARTICIPANTS = "/training/trainingprogram/remove/{empId}/program/{progId}";
	public static final String DOWNLOAD_PROGRAM_AGENDA = "/training/trainingprogram/agenda/{progId}";
	// End of Addition by Kamal Anand
	
	// Added by Rajesh
	public static final String CREATE_NOMINATION_POOL_DETAILS = "/training/createNominationPoolDetails"; 
	//EOA by Rajesh
	
	//Added by Prathibha
	public static final String CREATE_TRAINING_PROGRAM_BY_TRAINING_COORDINATOR="/training/trainingprogram/trainingmanger"; 
	//EOA by Prathibha

	
	//Added by Balaji

	public static final String TEAM_TRAINING_REQUEST = "/training/team/training/requests";
	
	public static final String TRAINING_DETAILS_BY_DATE_RMID = "/training/viewTrainingByRM";
	
	//End of Addition by Balaji
	
	//Added by Pratibha
	public static final String GET_ALL_TRAININGS_FOR_EMPLOYEE = "/training/calendar/trainings"; 
	public static final String VIEW_TRAINING_PROGRAM_DETAILS_BY_PROGRAM_ID = "/training/trainings/{pgmId}";
	public static final String GET_TRAINING_PROGRAM_STATUS = "/training/program/status";
	public static final String GET_TRAINING_PROGRAM_BY_PROGRAM_ID_AND_SET_NOMINATION_STATUS = "/training/trainings/{programId}/{empId}";
	public static final String RETRIEVE_PROGRAM_DETAILS_BASED_ON_DATE = "/training/program/details";
	//EOA by Pratibha

	//Added by Mani
	public static final String UPLOAD_TRAINING_NOMINATION_STATUS ="/training/nomination/attendance/{updatedBy}";
	//End of addition by Mani
}
