/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingFeedBackRestURIConstants.java             */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the URI constants required*/
/*                  for the training attendees feedback              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.constants;

public class TrainingFeedBackRestURIConstants {
	
	//Added by Kamal Anand
	public static final String GET_ATTENDED_TRAINING_PROGRAMS_AND_FEEDBACKSTATUS_BY_EMPLOYEEID = "/training/feedback/attended"; //Feedback Employee
	public static final String GET_NUMBER_OF_ATTENDEES_GIVEN_FEEDBACK = "/training/feedback/count/{progId}";
	public static final String TRAINING_COORDINATOR_VIEW_ATEENDEE_FEEDBACK = "/training/feedback/ateendees";  //View Feedback screen Training Manager
	public static final String VIEW_DETAILED_FEEDBACK = "/training/feedback/employee/{empId}/program/{progId}";
	public static final String TRAINING_COORDINATOR_VIEW_ATTENDEE_FEEDBACK_ALL_PROGRAMS = "/training/attendeefeedback";
	public static final String GET_ATTENDEES_FEEDBACK_STATUS_FOR_TRAINING_PROGRAM = "/training/feedback/status/{progId}";
	//End of Addition by Kamal Anand

	//Added by Smrithi
	public static final String CREATE_ATTENDEES_FEEDBACK = "/training/attendees/feedback/create";
	//EOA by Smrithi
}
