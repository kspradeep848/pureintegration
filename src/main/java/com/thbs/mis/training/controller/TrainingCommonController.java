/*********************************************************************/
/*                  FILE HEADER                                      */

/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingCommonController.java                     */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  22-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains  all the methods related to   */
/*                   training common controller	                     */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 22-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.common.bo.MasFeedbackQuestionInfoBO;
import com.thbs.mis.common.bo.MasTrainingTopicNameBO;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.training.bean.TrainingFeedbackQuestionBean;
import com.thbs.mis.training.bo.TrainingMasRequestStatusBO;
import com.thbs.mis.training.bo.TrainingMasRoomDetailBO;
import com.thbs.mis.training.constants.TrainingCommonURIConstants;
import com.thbs.mis.training.service.TrainingCommonService;

@Api(value = "Common Services of Training Module", description = "Common Operations related to the Training Module", basePath = "http://localhost:8080/", consumes = "Rest Service", position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "Common services of Training")
@Controller
public class TrainingCommonController {

	private static final AppLog LOG = LogFactory
			.getLog(TrainingCommonController.class);

	@Autowired
	private TrainingCommonService trainingCommonService;

	// Added by Smrithi

	/**
	 * @Description: This method is used to view the training topics.
	 * @return List<MasTrainingTopicNameBO>
	 * @throws CommonCustomException 
	 * @throwsCommonCustomException 
	 * @throws MethodArgumentNotValidException
	 * 
	 */
	@ApiOperation(tags="Training",value = " To view training topics"
			+ "This service will be called from the front-end when the user wants to view the training topics", httpMethod = "GET", notes = "This Service has been implemented to view the training topics."
			+ "<br>The Details will be stored in the database.", nickname = "Get Training Topics", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of  Training topic names  resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingCommonURIConstants.GET_TRAINING_TOPICS_NAMES, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getMasTrainingTopics()
			throws BusinessException, CommonCustomException {
		LOG.startUsecase("getTrainingTopics Controller");
		
		List<MasTrainingTopicNameBO> trainingTopicsNameList = null;
		try {
			trainingTopicsNameList = trainingCommonService.getMasTrainingTopics();
			if (trainingTopicsNameList.size() > 0) {
				LOG.endUsecase("getTrainingTopics Controller");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Training Topics  are displayed.",
								trainingTopicsNameList));
			} else {
				LOG.endUsecase("getTrainingTopics Controller");
				return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.FAILURE,
								"Training Topics  are not displayed."));
			}

			/*
			 * return ResponseEntity.status(HttpStatus.OK.value()).body( new
			 * MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
			 * "Training Topics  are displayed.",trainingTopics));
			 */

		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new CommonCustomException(e.getMessage());
		}

	}

	// EOA by Smrithi

	// Added by Rajesh
	/**
	 * <Description getTrainingRoomDetail:> This contains the methods used to get the training room details
	 * @author THBS
	 * @version 1.0
	 * @throws BusinessException
	 * @return TrainingMasRoomDetailBO (List)
	 */
	@ApiOperation(tags="Training",value = "Get Training Room Details."
			+ "To Get the Training Room details.", httpMethod = "GET", notes = "This Service has been implemented to Get Training Room details."
			+ "<br>The Response will be the details of the Training Room."
			+ "<br><b>Inputs :</b><br> Get Training Room Details. <br><br>", nickname = "Get Training Room Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response =MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Training Room Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingCommonURIConstants.GET_TRAINING_ROOM_DETAILS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getTrainingRoomDetail()
			throws BusinessException {
		LOG.startUsecase("Get the Training Room Detail Controller");
		
		List<TrainingMasRoomDetailBO> trainingRoomDetailsList;
		
		try {
			trainingRoomDetailsList = trainingCommonService.getTrainingRoomDetail();
			
			if (trainingRoomDetailsList.size() > 0) {
				LOG.endUsecase("Get the Training Room Detail Controller");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Training Room Details are displayed.",
								trainingRoomDetailsList));
			} else {
				LOG.endUsecase("Get the Training Room Detail Controller");
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.ACCEPTED.value(),
								MISConstants.SUCCESS,
								"Training Room Details are not available in DB."));
			}
		}
		
		catch (Exception e) {
			LOG.info("Get the Training Room Detail Catch Block");
			throw new BusinessException("Exception Occured", e);
		}

	}

	/**
	 * <Description getTrainingRequestStatus:> This contains the methods used
	 * for the GET_TRAINING_REQUEST_STATUS for the Controller operation.
	 * @author THBS
	 * @version 1.0
	 * @throws BusinessException
	 * @return List <TrainingMasRequestStatusBO> 
	 */
	@ApiOperation(tags="Training",value = "Get Training Request Status ."
			+ "To get the status of Training request.", httpMethod = "GET", notes = "This Service has been implemented to get the Status details of a training request ."
			+ "<br>The Response will be the details of the Training Request ."
			+ "<br><b>Inputs :</b><br> Get Training Request Status. <br><br>",

	nickname = "Get Training Request Status", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Training Request Status resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingCommonURIConstants.GET_TRAINING_REQUEST_STATUS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getTrainingRequestStatus()
			throws BusinessException , Exception  {
		LOG.startUsecase("Get the Training Request Status Controller");
		
		List<TrainingMasRequestStatusBO> trainingReqStatusList = null;
		
		try {
			trainingReqStatusList = trainingCommonService
					.getTrainingRequestStatus();
			if (trainingReqStatusList.size() > 0) {
				LOG.endUsecase ("getTrainingRequestStatus Contoller");
				
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Training Request Status are displayed.",
								trainingReqStatusList));
			} else {
				LOG.endUsecase ("getTrainingRequestStatus Contoller");
				
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.ACCEPTED.value(),
								MISConstants.SUCCESS,
								"Training Request Status are not available in DB."));
			}
		}
		catch (BusinessException e) {
			LOG.info("getTrainingRequestStatus Catch block");
			throw new BusinessException("Exception Occured", e);
		}catch (Exception e) {
			LOG.info("getTrainingRequestStatus Catch block");
			throw new Exception("Exception Occured", e);
		}

	}

	// EOA by Rajesh

	// Added by Pratibha

	/**
	 * 
	 * @param goalBo
	 * @return List<MasFeedbackQuestionInfoBO>
	 * @throws BusinessException
	 * @Description :This service is used to get the feedback question based on feedback status
	 *              
	 */

	@ApiOperation(tags="Training",value = "Get feedback question."
			+ "This service is used to get the feedback question based on feedback status.", httpMethod = "POST", notes = "This Service has been implemented to get feedback question.", nickname = "Get Feedback question based on status", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of  feedback question  resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingCommonURIConstants.GET_FEEDBACK_QUESTION, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> getFeedbackQuestion(
			@Valid @RequestBody TrainingFeedbackQuestionBean TrningFbQst)
			throws BusinessException {
		LOG.startUsecase("getFeedbackQuestion Controller");
		List<MasFeedbackQuestionInfoBO> questionInfoList = null;

		try {
			questionInfoList = trainingCommonService.getFeedbackQuestion(TrningFbQst);
		} catch (Exception e) {
			throw new BusinessException("exception ", e);
		}

		if (questionInfoList.size() > 0) {
			LOG.endUsecase("getFeedbackQuestion Controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Feedback question details.",
							questionInfoList));
		} else {
			LOG.endUsecase("getFeedbackQuestion Controller");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Feedback question details are not found"));
		}
	}
	// EOA by Pratibha
}
