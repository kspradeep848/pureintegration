/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingRequestController.java                    */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        : 09-Nov-2016                                        */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Training Request                                 */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016      THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.ValidationUtils;
import com.thbs.mis.training.Validator.StartDateEndDateValidator;
import com.thbs.mis.training.Validator.TrainingRequestBeanTrainingCoordinatorValidator;
import com.thbs.mis.training.bean.CreateTrainingRequestBean;
import com.thbs.mis.training.bean.PagenationBean;
import com.thbs.mis.training.bean.TrainingRequestBean;
import com.thbs.mis.training.bean.TrainingRequestBeanTrainingCoordinator;
import com.thbs.mis.training.bean.TrainingRequestDetailViewBean;
import com.thbs.mis.training.bean.TrainingRequestsCoordinatorViewBean;
import com.thbs.mis.training.bean.TrainingRequestsViewBean;
import com.thbs.mis.training.bean.UpdateTrainingRequestBean;
import com.thbs.mis.training.bean.UpdateTrainingRequestStatusBean;
import com.thbs.mis.training.bo.TrainingDatRequestDetailBO;
import com.thbs.mis.training.constants.TrainingRequestURIConstants;
import com.thbs.mis.training.service.TrainingRequestService;

/**
 * Handles requests for the Training service.
 */
@Api(value = "Training Request Related Operations", description = "Operations Related to the Training Request", basePath = "http://localhost:8080/", consumes = "Rest Service", position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "Training Request Related Services")
@Controller
public class TrainingRequestController {

	// Added by Kamal Anand
	private static final AppLog LOG = LogFactory
			.getLog(TrainingRequestController.class);

	@Autowired
	private TrainingRequestService trainingRequestService;

	@InitBinder
	protected void initBinder(WebDataBinder binder) throws BindException {
		if (binder.getObjectName().equalsIgnoreCase("TrainingRequestBean")) {
			binder.addValidators(new StartDateEndDateValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"TrainingRequestBeanTrainingCoordinator")) {
			binder.addValidators(new TrainingRequestBeanTrainingCoordinatorValidator());
		} else {
			binder.close();
		}
	}

	/**
	 * 
	 * @param trainingRequestBean
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to Create the new Training Request
	 */
	@ApiOperation(tags="Voucher",value = " To create new Training Request."
			+ "This service will be called from the front-end when the user wants to create new training request", httpMethod = "POST", notes = "This Service has been implemented to create new training request."
			+ "<br>The Details will be stored in database.", nickname = "Create Training Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Request Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRequestURIConstants.CREATE_TRAINING_REQUEST, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> createTrainingRequest(
			@ApiParam(name = "CreateTrainingRequestBean", example = "CreateTrainingRequestBean", value = "Training Request Bean with all tha data to create the training Request.") @Valid @RequestBody CreateTrainingRequestBean trainingRequestBean)
			throws BusinessException, DataAccessException {
		LOG.startUsecase("Create Training Request");
		TrainingDatRequestDetailBO trainingRequestDetail;
		try {
			trainingRequestDetail = trainingRequestService
					.createTrainingRequest(trainingRequestBean);

			LOG.debug("Training Request Success :" + trainingRequestDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(
					"Exception occured in Create Training Request", e);
		}
		if (trainingRequestDetail != null) {
			LOG.endUsecase("Create Training Request");
			return ResponseEntity.status(HttpStatus.CREATED.value()).body(
					new MISResponse(HttpStatus.CREATED.value(),
							MISConstants.SUCCESS,
							"Training Request Created Successfully."));
		} else {
			LOG.endUsecase("Create Training Request");
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
							.value(), MISConstants.FAILURE,
							"Some Error happend while creating Training Request."));
		}
	}

	/**
	 * 
	 * @param trainingRequestBean
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to update the existing Training
	 *              Request
	 */
	@ApiOperation(tags="Voucher",value = " To update Training Request."
			+ "This service will be called from the front-end when the user wants to update a training request", httpMethod = "PUT", notes = "This Service has been implemented to update training request."
			+ "<br>The Details will be stored in database.", nickname = "Update Training Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The updated Request Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRequestURIConstants.UPDATE_TRAINING_REQUEST, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> updateTrainingRequest(
			@ApiParam(name = "UpdateTrainingRequestBean", example = "CreateTrainingRequestBean", value = "Training Request Bean with all tha data to update the training Request.") @Valid @RequestBody UpdateTrainingRequestBean trainingRequestBean)
			throws BusinessException, DataAccessException,
			CommonCustomException {
		LOG.startUsecase("Update Training Request");
		TrainingDatRequestDetailBO trainingRequestDetail;
		try {
			trainingRequestDetail = trainingRequestService
					.updateTrainingRequest(trainingRequestBean);

		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (trainingRequestDetail != null) {
			LOG.endUsecase("Update Training Request");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Training Request Updated Successfully."));
		} else {
			LOG.endUsecase("Update Training Request");
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
							.value(), MISConstants.FAILURE,
							"Some Error happend while updating Training Request."));
		}
	}

	/**
	 * 
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to View All Training Requests by
	 *              Training Coordinator
	 */
	@ApiOperation(tags="Voucher",value = " To view all Training Requests by Training Manager."
			+ "This service will be called from the front-end when the Training Manager wants to view all Training Requests", httpMethod = "POST", notes = "This Service has been implemented to view all training requests by the training manager."
			+ "<br>The Details will be retrieved from database.", nickname = "View Training Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Training Request Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRequestURIConstants.VIEW_ALL_TRAINING_REQUESTS_BY_TRAINING_MANAGER, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> viewTrainingRequestDetailsByTrainingMgr(
			@Valid @RequestBody PagenationBean pageDetailBean)
			throws BusinessException, DataAccessException {
		LOG.startUsecase("View All Training Request Details by Training Coordinator");
		Page<TrainingRequestsCoordinatorViewBean> trainingRequestsList;
		try {
			trainingRequestsList = trainingRequestService
					.viewAllTrainingRequestsByTrainingManager(pageDetailBean);
		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception Occured in View All Training Request Details by Training Coordinator",
					e);
		}
		if (trainingRequestsList.getContent().size() > 0) {
			LOG.endUsecase("View All Training Request Details by Training Coordinator");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Training Request Details.",
							trainingRequestsList));
		} else {
			LOG.endUsecase("View All Training Request Details by Training Coordinator");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"No Training Request Details Found.",
							trainingRequestsList));
		}
	}

	/**
	 * 
	 * @param mgrId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to View All Training Requests By
	 *              Reporting Manager
	 */
	@ApiOperation(tags="Voucher",value = " To view all Training Requests by Reporting manager."
			+ "This service will be called from the front-end when the Reporting manager wants to view all Training Request", httpMethod = "GET", notes = "This Service has been implemented to view all training request by the Reporting manager."
			+ "<br>The Details will be retrieved from database.", nickname = "View Training Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Training Request Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRequestURIConstants.VIEW_ALL_TRAINING_REQUEST_BY_REPORTING_MANAGER, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> viewTrainingRequestDetailsByReportingMgr(
			@PathVariable int mgrId) throws BusinessException,
			DataAccessException {
		LOG.startUsecase("View All Training Request by Reporting Manager");
		List<TrainingRequestsViewBean> trainingRequestsList;
		try {
			trainingRequestsList = trainingRequestService
					.viewAllTrainingRequestsByReportingMgrId(mgrId);
		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception Occured in View All Training Request by Reporting Manager",
					e);
		}
		if (trainingRequestsList.size() > 0) {
			LOG.endUsecase("View All Training Request by Reporting Manager");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Training Request Details.",
							trainingRequestsList));
		} else {
			LOG.endUsecase("View All Training Request by Reporting Manager");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS,
							"No Training Request Details Found.",
							trainingRequestsList));
		}
	}

	/**
	 * 
	 * @param trainingRequestBean
	 * @param binding
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws MethodArgumentNotValidException
	 * @Description : This method is used to View All Training Requests By
	 *              Status or by Date Range for a Training Coordinator
	 */
	@ApiOperation(tags="Voucher",value = " To view Training Requests by Status or Date range Training Coordinator."
			+ "This service will be called from the front-end when the Training Coordinator wants to see Training Request by Status or Date Range", httpMethod = "POST", notes = "This Service has been implemented to view all training request by status or date range by the training coordinator."
			+ "<br>The Details will be retrieved from database.", nickname = "View Training Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Training Request Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRequestURIConstants.VIEW_TRAINING_REQUEST_BY_STATUS_DATERANGE_TRAINING_COORDINATOR, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> viewTrainingRequestDetailsByTrainingMgrStatusAndDateRange(
			@ApiParam(name = "TrainingRequestBean", example = "TrainingRequestBean", value = "Training Request Bean with all tha data to view training Requests.") @Valid @RequestBody TrainingRequestBeanTrainingCoordinator trainingRequestBean,
			BindingResult binding) throws BusinessException,
			DataAccessException, MethodArgumentNotValidException {
		LOG.startUsecase("View Training Request Details by Status or Date Range for Training Coordinator");
		Page<TrainingRequestsCoordinatorViewBean> trainingRequestsList;
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				trainingRequestsList = trainingRequestService
						.viewTrainingRequestDetailsByTrainingMgrStatusAndDateRange(trainingRequestBean);
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception Occured in View Training Request Details by Status or Date Range for Training Coordinator",
					e);
		}
		if (trainingRequestsList.getContent().size() > 0) {
			LOG.endUsecase("View Training Request Details by Status or Date Range for Training Coordinator");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Training Request Details.",
							trainingRequestsList));
		} else {
			LOG.endUsecase("View Training Request Details by Status or Date Range for Training Coordinator");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"No Training Request Details Found.",
							trainingRequestsList));
		}
	}

	/**
	 * 
	 * @param reqId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This service is used to view Training Request Details
	 *              based on Training Request Id
	 */
	@ApiOperation(tags="Voucher",value = " To view Training Requests by Training Request Id."
			+ "This service will be called from the front-end when the user wants to see particular Training Request using the training request id", httpMethod = "GET", notes = "This Service has been implemented to view particular training request using the  training request id."
			+ "<br>The Details will be retrieved from database.", nickname = "View Training Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Training Request Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRequestURIConstants.VIEW_TRAINING_REQUEST_BY_REQUEST_ID, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> viewTrainingRequestDetailsByRequestId(
			@PathVariable int reqId) throws BusinessException,
			DataAccessException, CommonCustomException {
		LOG.startUsecase("View All Training Request by Reporting Manager");
		TrainingRequestDetailViewBean requestDetailView = null;
		List<TrainingRequestDetailViewBean> trainingRequestsList = new ArrayList<TrainingRequestDetailViewBean>();
		try {
			requestDetailView = trainingRequestService
					.viewATrainingRequestByRequestId(reqId);
			trainingRequestsList.add(requestDetailView);
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		if (requestDetailView != null) {
			LOG.endUsecase("View All Training Request by Reporting Manager");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Training Request Details.",
							trainingRequestsList));
		} else {
			LOG.endUsecase("View All Training Request by Reporting Manager");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS,
							"No Training Request Details Found.",
							trainingRequestsList));
		}
	}

	/**
	 * 
	 * @param trainingRequestBean
	 * @param binding
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws MethodArgumentNotValidException
	 * @Description : This method is used to View All Training Request by Status
	 *              or by Date Range for a Reporting Manager
	 */
	@ApiOperation(tags="Voucher",value = " To view Training Requests by Status and Date range by the Reporting Manager."
			+ "This service will be called from the front-end when the Reporting Manager wants to see Training Request by Status or Date Range", httpMethod = "POST", notes = "This Service has been implemented to view all training request by status or date range  by the Reporting Manager."
			+ "<br>The Details will be retrieved from database.", nickname = "View Training Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Training Request Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRequestURIConstants.VIEW_TRAINING_REQUEST_BY_STATUS_DATERANGE_REPORTING_MANAGER, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> viewTrainingRequestDetailsByReportingMgrStatusAndDateRange(
			@ApiParam(name = "TrainingRequestBean", example = "TrainingRequestBean", value = "Training Request Bean with all tha data to view training Requests.") @Valid @RequestBody TrainingRequestBean trainingRequestBean,
			BindingResult binding) throws BusinessException,
			DataAccessException, MethodArgumentNotValidException {
		LOG.startUsecase("View Training Request Details by Status or Date Range for Reporting Manager");
		List<TrainingRequestsViewBean> trainingRequestsList;
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				trainingRequestsList = trainingRequestService
						.viewTrainingRequestByReportingManagerStatusAndDateRange(trainingRequestBean);
			}

		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception Occured in View Training Request Details by Status or Date Range for Reporting Manager",
					e);
		}
		if (trainingRequestsList.size() > 0) {
			LOG.endUsecase("View Training Request Details by Status or Date Range for Reporting Manager");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Training Request Details.",
							trainingRequestsList));
		} else {
			LOG.endUsecase("View Training Request Details by Status or Date Range for Reporting Manager");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"No Training Request Details Found.",
							trainingRequestsList));
		}
	}

	/**
	 * 
	 * @param status
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to view all Training Requests by
	 *              Training Coordinator By status
	 */
	@ApiOperation(tags="Voucher",value = " To view all Training Requests by Training Coordinator By status."
			+ "This service will be called from the front-end when the Training Coordinator wants to see all Training Request", httpMethod = "GET", notes = "This Service has been implemented to view all training requests by the training coordinator."
			+ "<br>The Details will be retrieved from database.", nickname = "View Training Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Training Request Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRequestURIConstants.VIEW_TRAINING_REQUEST_BY_STATUS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> viewAllTrainingRequestDetailsByStatusTrainingMgr(
			@PathVariable int status) throws BusinessException,
			DataAccessException {
		LOG.startUsecase("View All Training Request Details by Training Coordinator by status");
		List<TrainingRequestsCoordinatorViewBean> trainingRequestsList;
		try {
			trainingRequestsList = trainingRequestService
					.viewAllTrainingRequestDetailsByStatusTrainingMgr(status);
		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception Occured in View All Training Request Details by Training Coordinator by status",
					e);
		}
		if (trainingRequestsList.size() > 0) {
			LOG.endUsecase("View All Training Request Details by Training Coordinator by status");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Training Request Details.",
							trainingRequestsList));
		} else {
			LOG.endUsecase("View All Training Request Details by Training Coordinator by status");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"No Training Request Details Found.",
							trainingRequestsList));
		}
	}
	
	@ApiOperation(tags="Voucher",value = " To view all Approved Training Requests for which Program is not created."
			+ "This service will be called from the front-end when the user wants to see all Approved Training Request for which Program is not created", httpMethod = "GET", notes = "This Service has been implemented to view all approved training request."
			+ "<br>The Details will be retrieved from database.", nickname = "View Approved Training Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Approved Training Request Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRequestURIConstants.GET_APPROVED_TRAINING_REQUESTS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getApprovedTrainingRequests() throws BusinessException,
			DataAccessException {
		LOG.startUsecase("View All Training Request by Reporting Manager");
		List<TrainingRequestsCoordinatorViewBean> trainingRequestsList;
		try {
			trainingRequestsList = trainingRequestService
					.getApprovedTrainingRequests();
		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception Occured in View All Training Request by Reporting Manager",
					e);
		}
		if (trainingRequestsList.size() > 0) {
			LOG.endUsecase("View All Training Request by Reporting Manager");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Training Request Details.",
							trainingRequestsList));
		} else {
			LOG.endUsecase("View All Training Request by Reporting Manager");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS,
							"No Training Request Details Found.",
							trainingRequestsList));
		}
	}

	// End of Addition by Kamal Anand

	// Added by Rajesh

	/**
	 * <Description updateTrainingRequestStatus:> This contains the methods used
	 * for the UPDATE_TRAINING_REQUEST_STATUS for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws BusinessException
	 * @see
	 */
	@ApiOperation(tags="Voucher",value = " To  Update Training Request Status  details."
			+ "To update Training Request Status details.", httpMethod = "PUT", notes = "This Service has been implemented to update Training Request Status details."
			+ "<br>The Response will be the detailed of the Update Training Request Status."
			+ "<br><b>Inputs :</b><br> Update Training Request Status. <br><br>",

	nickname = "Update Training Request Status", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = TrainingDatRequestDetailBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of updated Training request status resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRequestURIConstants.UPDATE_TRAINING_REQUEST_STATUS, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> updateTrainingRequestStatus(
			@ApiParam(name = "UpdateTrainingRequestStatus", example = "1", value = "Update Training Request Status.") @Valid @RequestBody UpdateTrainingRequestStatusBean bean)
			throws BusinessException, CommonCustomException {
		LOG.startUsecase("Get the Training Request Status");
		TrainingDatRequestDetailBO trainingRequest = null;
		List<TrainingDatRequestDetailBO> trainingRequestDetail = new ArrayList<TrainingDatRequestDetailBO>();
		try {
			trainingRequest = trainingRequestService
					.updateTrainingRequestStatus(bean);
			trainingRequestDetail.add(trainingRequest);

			if (trainingRequest != null) {
				LOG.endUsecase("Get the Training Request Status");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Training Request Updated successfully."));
			} else {
				LOG.endUsecase("Get the Training Request Status");
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
								.value(), MISConstants.FAILURE,
								"Training request not updated."));
			}

		} catch (BusinessException e) {
			throw new BusinessException(e.getMessage());
		}
	}
	// EOA by Rajesh
}
