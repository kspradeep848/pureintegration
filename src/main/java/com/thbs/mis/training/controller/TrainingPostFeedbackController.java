/*********************************************************************/
/*                  FILE HEADER                                      */

/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingPostFeedbackController.java               */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  24-10-2016                                        */
/*                                                                   */
/*  Description :  This class contains all common controller methods  */
/*                   of TrainingPostFeedBack                         */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 24-10-2016, 2016     THBS     1.0        Initial version created */
/*********************************************************************/
package com.thbs.mis.training.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.ValidationUtils;
import com.thbs.mis.training.Validator.PostApplicationFeedbackInputBeanValidator;
import com.thbs.mis.training.Validator.TrainingFeedbackInputBeanDateValidator;
import com.thbs.mis.training.Validator.TrainingPostFeedbackInputBeanValidator;
import com.thbs.mis.training.bean.AttendeeFeedbackOutputBean;
import com.thbs.mis.training.bean.PostApplicationDetailedFeedbackBean;
import com.thbs.mis.training.bean.PostApplicationFeedbackInputBean;
import com.thbs.mis.training.bean.TrainingCoordinatorViewFeedbackBean;
import com.thbs.mis.training.bean.TrainingDetailedFeedbackOutputBean;
import com.thbs.mis.training.bean.TrainingPostApplicationFeedbackBean;
import com.thbs.mis.training.bean.TrainingPostFeedbackInputBean;
import com.thbs.mis.training.bo.TrainingDatPostApplicationFeedbackBO;
import com.thbs.mis.training.constants.TrainingPostFeedBackRestURIConstants;
import com.thbs.mis.training.service.TrainingPostFeedBackService;

@Api(value = "Post Feed Back Related Operations", description = "Operations Related to the Training Post Feed Back", basePath = "http://localhost:8080/", consumes = "Rest Service", position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "Training Post Feed Back Related Services")
@Controller
public class TrainingPostFeedbackController {

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(TrainingPostFeedbackController.class);

	@Autowired
	private TrainingPostFeedBackService trainingPostFeedBackService;

	@InitBinder
	protected void initBinder(WebDataBinder binder) throws BindException {
		if (binder.getObjectName()
				.equalsIgnoreCase("TrainingFeedbackInputBean")) {
			binder.addValidators(new TrainingFeedbackInputBeanDateValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"PostApplicationFeedbackInputBean")) {
			binder.addValidators(new PostApplicationFeedbackInputBeanValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"TrainingPostFeedbackInputBean")) {
			binder.addValidators(new TrainingPostFeedbackInputBeanValidator());
		} else {
			binder.close();
		}
	}

	// Added by Smrithi
	/**
	 * 
	 * @param postApplicationFeedbackBean
	 * @return
	 * @throws BusinessException
	 * @throws MethodArgumentNotValidException
	 * @Description: This method is used to create training post attendees
	 *               feedBack.
	 */
	@ApiOperation(tags="Training",value = " To create training post attendees feedBack"
			+ "This service will be called from the front-end when the user wants to create training post attendees feedBack", httpMethod = "POST", notes = "This Service has been implemented to create training post attendees feedBack."
			+ "<br>The Details will be stored in database.", nickname = "Create Post Attendees Feed Back", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Post Attendee Feedback created resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingPostFeedBackRestURIConstants.CREATE_POST_ATTENDEES_FEEDBACK, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> createTrainingPostFeedBack(
			@ApiParam(name = "TrainingDatPostApplicationFeedbackBO", example = "TrainingPostApplicationFeedbackBean", value = "Training post feedback Bo with all tha data to create the training post feedback.") @Valid @RequestBody List<TrainingPostApplicationFeedbackBean> postApplicationFeedbackBean)
			throws BusinessException, DataAccessException, ParseException,
			MethodArgumentNotValidException {
		LOG.startUsecase("Create Post Attendees Feed Back.");

		List<TrainingDatPostApplicationFeedbackBO> trainingPostApplicationFeedbackBO;
		try {

			trainingPostApplicationFeedbackBO = trainingPostFeedBackService
					.createPostAttendeesFeedback(postApplicationFeedbackBean);

			if (trainingPostApplicationFeedbackBO.size() > 0) {
				System.out.println("coming inside if");
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Post Attendees feedback are created successfully."));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
								.value(), MISConstants.FAILURE,
								"Post Attendees feedback not created."));
			}

		} catch (DataAccessException e) {
			e.printStackTrace();
			throw new BusinessException("Exception occured", e);
		}

	}

	// EOA by Smrithi

	// Added by Kamal Anand
	/**
	 * 
	 * @param feedbackInput
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description:This method is used to view Post Application Feedback for
	 *                   all Training Programs
	 */
	@ApiOperation(tags="Training",value = " To view Post Application Feedback for All Training Programs by the training coordinator."
			+ "This service will be called from the front-end when the training coordinator  wants to view Post Application Feedback for All Training Programs", httpMethod = "POST", notes = "This Service has been implemented to view Post Application Feedback for All Training Programs."
			+ "<br>The Details will be retrieved from database.", nickname = "View Post application feedback for a Training program ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Post Application Feedback details for all programs  Resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingPostFeedBackRestURIConstants.TRAINING_COORDINATOR_VIEW_POST_APPLICATION_FEEDBACK_ALL_PROGRAMS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> getAllManagerFeedback(
			@ApiParam(name = "TrainingPostFeedbackInputBean", example = "TrainingPostFeedbackInputBean", value = "Training Feedback Input Bean with all tha data to view the program details and Post Application Feedback.") @Valid @RequestBody TrainingPostFeedbackInputBean feedbackInput,
			BindingResult binding) throws BusinessException,
			DataAccessException {
		LOG.startUsecase("View Post Application Feedback for all Training Programs");

		Page<TrainingCoordinatorViewFeedbackBean> feedbackList;
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				feedbackList = trainingPostFeedBackService
						.getAllManagerFeedback(feedbackInput);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
		if (feedbackList.getContent().size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Feedback Details.",
							feedbackList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No Feedback Details."));
		}
	}

	/**
	 * 
	 * @param feedbackInput
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description:This method is used to view Post Application Feedback for
	 *                   all Training Programs
	 */
	@ApiOperation(tags="Training",value = " To view Post Application Feedback for All Employee's for a particular Training Programs."
			+ "This service will be called from the front-end when the training coordinator wants to see Post Application Feedback for all Employee's for a Training Programs", httpMethod = "POST", notes = "This Service has been implemented to view  Post Application Feedback for all Employees for a particular Training Program."
			+ "<br>The Details will be retrieved from database.", nickname = "View Post application feedback for a Training program ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Post Application Feedback Details Resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingPostFeedBackRestURIConstants.TRAINING_COORDINATOR_VIEW_POST_APPLICATION_FEEDBACK_OF_TRAINING_PROGRAM, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> getPostApplicationFeedbackForTrainingProgram(
			@ApiParam(name = "PostApplicationFeedbackInputBean", example = "PostApplicationFeedbackInputBean", value = "Training Feedback Input Bean with all tha data to view the program details and Post Application Feedback.") @Valid @RequestBody PostApplicationFeedbackInputBean feedbackInput,
			BindingResult binding) throws BusinessException,
			DataAccessException {
		LOG.startUsecase("View Post Application Feedback for all Employee's of a Training Programs");

		Page<AttendeeFeedbackOutputBean> feedbackList;
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				feedbackList = trainingPostFeedBackService
						.getPostApplicationFeedbackForTrainingProgram(feedbackInput);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
		if (feedbackList.getContent().size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Feedback Details.",
							feedbackList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No Feedback Details."));
		}
	}

	/**
	 * 
	 * @param empId
	 * @param progId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 *             Description : This method is used to view Detailed Post
	 *             Application Feedback of the Employee for a particular
	 *             Training Program
	 */
	@ApiOperation(tags="Training",value = " To view the  detailed Post Application Feedback given to employee for a Training Program."
			+ "This service will be called from the front-end when the user wants to view the  Detailed Post Application Feedback of a employee", httpMethod = "GET", notes = "This Service has been implemented to view  Detailed Post Application Feedback of a employee for a particular Training Program."
			+ "<br>The Details will be retrieved from database.", nickname = "View Post application feedback for a Training program ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Detailed Post Application Feedback", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingPostFeedBackRestURIConstants.TRAINING_COORDINATOR_VIEW_DETAILED_POST_APPLICATION_FEEDBACK, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getDetailedPostApplicationFeedback(
			@PathVariable int empId, @PathVariable int progId)
			throws BusinessException, DataAccessException,CommonCustomException {
		LOG.startUsecase("View Detailed Post Application Feedback for a Employee");

		TrainingDetailedFeedbackOutputBean feedback;
		List<TrainingDetailedFeedbackOutputBean> feedbackList = new ArrayList<TrainingDetailedFeedbackOutputBean>();
		try {
			feedback = trainingPostFeedBackService
					.getDetailedPostApplicationFeedback(empId, progId);
			feedbackList.add(feedback);
		} catch (DataAccessException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (feedbackList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Detailed Feedback.",
							feedbackList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No Feedback Details."));
		}
	}

	/**
	 * 
	 * @param progId
	 * @param mgrId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This service is used to view all Employee's Post
	 *              Application Feedback for a Particular Training Program
	 */
	@ApiOperation(tags="Training",value = " To view  a detailed Post Application Feedback given to all employee's for a Training Program by the reporting manager."
			+ "This service will be called from the front-end when the reporting manager  wants to view  Detailed Post Application Feedback for all employee's for a Training Program", httpMethod = "GET", notes = "This Service has been implemented to view  Detailed Post Application Feedback of all employee's for a particular Training Program."
			+ "<br>The Details will be retrieved from database.", nickname = "View Post application feedback for a Training program ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Detailed Post Application Feedback", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingPostFeedBackRestURIConstants.REPORTING_MANAGER_VIEW_DETAILED_POST_APPLICATION_FEEDBACK, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getDetailedPostApplicationFeedbackForReportingManager(
			@PathVariable int progId, @PathVariable int mgrId)
			throws BusinessException, DataAccessException,CommonCustomException {
		LOG.startUsecase("View Detailed Post Application Feedback for a Employee");

		PostApplicationDetailedFeedbackBean feedback;
		List<PostApplicationDetailedFeedbackBean> feedbackList = new ArrayList<PostApplicationDetailedFeedbackBean>();
		try {
			feedback = trainingPostFeedBackService
					.getDetailedPostApplicationFeedbackForReportingManager(
							progId, mgrId);
			feedbackList.add(feedback);
		} catch (DataAccessException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (feedbackList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Detailed Feedback.",
							feedbackList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No Feedback Details."));
		}
	}
	// End of Addition by Kamal Anand

}
