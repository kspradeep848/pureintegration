/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingFeedBackController.java                   */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Training feed Back.                              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.ValidationUtils;
import com.thbs.mis.training.Validator.AttendeeFeedbackInputBeanValidator;
import com.thbs.mis.training.Validator.TrainingFeedbackInputBeanDateValidator;
import com.thbs.mis.training.Validator.TrainingPostFeedbackInputBeanValidator;
import com.thbs.mis.training.bean.AttendedTrainingOutputBean;
import com.thbs.mis.training.bean.AttendeeFeedbackInputBean;
import com.thbs.mis.training.bean.AttendeeFeedbackOutputBean;
import com.thbs.mis.training.bean.AttendeeFeedbackStatusBean;
import com.thbs.mis.training.bean.TrainingCoordinatorViewFeedbackBean;
import com.thbs.mis.training.bean.TrainingCreateAttendeesFeedbackBean;
import com.thbs.mis.training.bean.TrainingDetailedFeedbackOutputBean;
import com.thbs.mis.training.bean.TrainingFeedbackInputBean;
import com.thbs.mis.training.bean.TrainingFeedbackOutputBean;
import com.thbs.mis.training.bean.TrainingPostFeedbackInputBean;
import com.thbs.mis.training.bo.TrainingDatAttendeeFeedbackBO;
import com.thbs.mis.training.constants.TrainingFeedBackRestURIConstants;
import com.thbs.mis.training.service.TrainingFeedBackService;

/**
 * This is the controller class for the training FeedBack details,
 * It will handles all the request related for the training feedback details.
 */

/**
 * This is the controller class for the training FeedBack details, It will
 * handles all the request related for the training feedback details.
 */
@Api(value = "Feed Back Related Operations", description = "Operations Related to the Training Feed Back", basePath = "http://localhost:8080/", consumes = "Rest Service", position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "Training Feed Back Related Services")
@Controller
public class TrainingFeedBackController {

	// Added by Kamal Anand
	private static final AppLog LOG = LogFactory
			.getLog(TrainingFeedBackController.class);

	@Autowired
	private TrainingFeedBackService trainingFeedBackService;

	@InitBinder
	protected void initBinder(WebDataBinder binder) throws BindException {
		if (binder.getObjectName()
				.equalsIgnoreCase("TrainingFeedbackInputBean")) {
			binder.addValidators(new TrainingFeedbackInputBeanDateValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"AttendeeFeedbackInputBean")) {
			binder.addValidators(new AttendeeFeedbackInputBeanValidator());
		}else if (binder.getObjectName().equalsIgnoreCase(
				"TrainingPostFeedbackInputBean")) {
			binder.addValidators(new TrainingPostFeedbackInputBeanValidator());
		} else {
			binder.close();
		}
	}

	/**
	 * 
	 * @param trainingFeedbackBean
	 * @param binding
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to get Attended Training Programs and
	 *              its feedback status based on Status or Date Range
	 */
	@ApiOperation(tags="Training",value = " To get attended training programs and feedback status of the training programs."
			+ "This service will be called from the front-end when the user wants to see attended training programs and feedback status of the training programs based on status or date range", httpMethod = "POST", notes = "This Service has been implemented to view attended training programs."
			+ "<br>The Details will be retrieved from the  database.", nickname = "View Training Programs and Feedback Status", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of  Training Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingFeedBackRestURIConstants.GET_ATTENDED_TRAINING_PROGRAMS_AND_FEEDBACKSTATUS_BY_EMPLOYEEID, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> getAllAttendedTrainingProgramsAndFeedbackStatusByEmployeeId(
			@ApiParam(name = "TrainingFeedbackInputBean", example = "TrainingFeedbackInputBean", value = "Training Feedback Input Bean with all tha data to view the training programs and feedback status.") @Valid @RequestBody TrainingFeedbackInputBean trainingFeedbackBean,
			BindingResult binding) throws BusinessException,
			DataAccessException,CommonCustomException {

		LOG.startUsecase("Get Attended Training Programs List and its Feedback Status based on Employee Id");
		List<TrainingFeedbackOutputBean> trainingFeedbackList = new ArrayList<TrainingFeedbackOutputBean>();
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				trainingFeedbackList = trainingFeedBackService
						.getAllAttendedTrainingProgramsAndFeedbackStatusByEmployeeId(trainingFeedbackBean);
			}
		} catch (DataAccessException e) {
			throw new CommonCustomException(e.getMessage());
		}catch(CommonCustomException e){
			throw new CommonCustomException(e.getMessage());
		}
		if (trainingFeedbackList.size() > 0) {
			LOG.endUsecase("Get Attended Training Programs List and its Feedback Status based on Employee Id");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Attended Training Program Details List.",
							trainingFeedbackList));
		} else {
			LOG.endUsecase("Get Attended Training Programs List and its Feedback Status based on Employee Id");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS,
							"No Attended Training Programs."));
		}
	}

	/**
	 * 
	 * @param progId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to Get number of Attendees for a
	 *              Training Program and out of that attendees how many has
	 *              provided the feedback
	 */
	@ApiOperation(tags="Training",value = " To get number of Attendees given feedback out of number of attendees for a training program."
			+ "This service will be called from the front-end when the user wants to see number of Attendees given feedback out of number of attendees for a training program", httpMethod = "GET", notes = "This Service has been implemented to get number of attendees given feedback."
			+ "<br>The Details will be retrieved from database.", nickname = "View Training Programs and Feedback Status", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Number of attendees given feedback resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingFeedBackRestURIConstants.GET_NUMBER_OF_ATTENDEES_GIVEN_FEEDBACK, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getNumberofAttendeesGivenFeedback(
			@PathVariable int progId) throws BusinessException,
			CommonCustomException {
		AttendedTrainingOutputBean attendedDetails = new AttendedTrainingOutputBean();
		List<AttendedTrainingOutputBean> attendedDetailsList = new ArrayList<AttendedTrainingOutputBean>();
		LOG.startUsecase("Get number of Attendees given feedback");
		try {
			attendedDetails = trainingFeedBackService
					.getNumberofAttendeesGivenFeedback(progId);
			attendedDetailsList.add(attendedDetails);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (attendedDetails != null) {
			LOG.endUsecase("Get number of Attendees given feedback");
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Number of Persons Attended.", attendedDetailsList));
		} else {
			LOG.endUsecase("Get number of Attendees given feedback");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE,
							"No participants for this training program."));
		}
	}

	/**
	 * 
	 * @param bean
	 * @param binding
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws MethodArgumentNotValidException
	 * @Description : This method is used to view Attendee Feedback
	 */
	@ApiOperation(tags="Training",value = " To view Attendees Feedback for a Training Program by the training coordinator."
			+ "This service will be called from the front-end when the training coordinator wants to see attendees feedback for a training program based on status or date range", httpMethod = "POST", notes = "This Service has been implemented to get attendees feedback for a training program."
			+ "<br>The Details will be retrieved from database.", nickname = "View attendees feedback for a Training program ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Attendee Feedback resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingFeedBackRestURIConstants.TRAINING_COORDINATOR_VIEW_ATEENDEE_FEEDBACK, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> getAttendeesFeedbackByTrainingProgramId(
			@ApiParam(name = "AttendeeFeedbackInputBean", example = "AttendeeFeedbackInputBean", value = "Training Feedback Input Bean with all tha data to view the attendees feedback.") @Valid @RequestBody AttendeeFeedbackInputBean feedbackInputbean,
			BindingResult binding) throws BusinessException,
			DataAccessException, MethodArgumentNotValidException {

		Page<AttendeeFeedbackOutputBean> feedbackOutputList;
		LOG.startUsecase("Get Attendees feedback details for a training program");
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				feedbackOutputList = trainingFeedBackService
						.getAttendeesFeedbackByTrainingProgramId(feedbackInputbean);
			}

		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
		if (feedbackOutputList.getContent().size() > 0) {
			LOG.endUsecase("Get Attendees feedback details for a training program");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Attendees Training Feedback List.",
							feedbackOutputList));
		} else {
			LOG.endUsecase("getDetailedAttendeesFeedbackByTrainingProgramId");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"No Training Feedback provided for this status or date range.",feedbackOutputList));
		}
	}

	/**
	 * 
	 * @param empId
	 * @param progId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to view the detailed feedback of a
	 *              employee.
	 */
	@ApiOperation(tags="Training",value = " To get Attendee detailed Feedback for a Training Program."
			+ "This service will be called from the front-end when the user wants to see attendee detailed feedback for a training program", httpMethod = "GET", notes = "This Service has been implemented to get attendee detailed feedback for a training program."
			+ "<br>The Details will be retrieved from database.", nickname = "View attendee detailed feedback for a Training program ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Detailed Feedback resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingFeedBackRestURIConstants.VIEW_DETAILED_FEEDBACK, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getDetailedFeedback(
			@PathVariable int empId, @PathVariable int progId)
			throws DataAccessException,CommonCustomException {
		TrainingDetailedFeedbackOutputBean detailedFeedback = new TrainingDetailedFeedbackOutputBean();
		List<TrainingDetailedFeedbackOutputBean> detailedFeedbackList = new ArrayList<TrainingDetailedFeedbackOutputBean>();
		LOG.startUsecase("View Detailed Feedback");
		try {
			detailedFeedback = trainingFeedBackService.getDetailedFeedback(
					empId, progId);
			detailedFeedbackList.add(detailedFeedback);

		} catch (DataAccessException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (detailedFeedback != null) {
			LOG.endUsecase("View Detailed Feedback");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Detailed Feedback.",
							detailedFeedbackList));
		} else {
			LOG.endUsecase("View Detailed Feedback");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Detailed feedback not provided."));
		}
	}

	/**
	 * 
	 * @param feedbackInput
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to get all Training programs and
	 *              attendees feedback
	 */
	@ApiOperation(tags="Training",value = " To get Attendees Feedback for a All Training Programs."
			+ "This service will be called from the front-end when the user wants to see Attendees Feedback for  All Training Programs", httpMethod = "POST", notes = "This Service has been implemented to get Attendees Feedback for  All Training Programs."
			+ "<br>The Details will be retrieved from database.", nickname = "View attendee feedback for a Training program ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Attendee Feedback Programs Resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingFeedBackRestURIConstants.TRAINING_COORDINATOR_VIEW_ATTENDEE_FEEDBACK_ALL_PROGRAMS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> getAllAttendeeFeedback(
			@ApiParam(name = "TrainingPostFeedbackInputBean", example = "TrainingPostFeedbackInputBean", value = "Training Feedback Input Bean with all tha data to view the program details and feedback of Attendees.") @Valid @RequestBody TrainingPostFeedbackInputBean feedbackInput)
			throws BusinessException, DataAccessException {
		LOG.startUsecase("View All Training Programs and feedback details");

		Page<TrainingCoordinatorViewFeedbackBean> feedbackList;
		try {
			feedbackList = trainingFeedBackService
					.getAllAttendeeFeedback(feedbackInput);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		if (feedbackList.getContent().size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Feedback Details.",
							feedbackList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No Feedback Details.",feedbackList));
		}
	}

	/**
	 * 
	 * @param progId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This service is used to get Attendees feedback status for
	 *              a Training Program.
	 */
	@ApiOperation(tags="Training",value = " To get Attendees feedback status for a Training Program."
			+ "This service will be called from the front-end when the user wants to see Attendees feedback status for a Training Program", httpMethod = "GET", notes = "This Service has been implemented to get Attendees feedback status for a Training Program."
			+ "<br>The Details will be retrieved from database.", nickname = " Attendees feedback status for a Training Program", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Attendee Feedback Status Resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingFeedBackRestURIConstants.GET_ATTENDEES_FEEDBACK_STATUS_FOR_TRAINING_PROGRAM, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAttendeesFeedbackStatus(
			@PathVariable int progId) throws BusinessException,
			DataAccessException {
		List<AttendeeFeedbackStatusBean> attendedDetailsList = new ArrayList<AttendeeFeedbackStatusBean>();
		LOG.startUsecase("Get Attendees Feedback Status for a Training Program");
		try {
			attendedDetailsList = trainingFeedBackService
					.getAttendeesFeedbackStatus(progId);
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		if (attendedDetailsList.size() > 0) {
			LOG.endUsecase("Attendees Feedback Status of a Training Program");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Attendees Feedback Status of a Training Program.",
							attendedDetailsList));
		} else {
			LOG.endUsecase("Attendees Feedback Status of a Training Program");
			return ResponseEntity
					.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS,
							"No Attendees Feedback Status of a Training Program."));
		}
	}

	// End Of Addition by Kamal Anand

	// Added by Smrithi

	/**
	 * 
	 * @param attendeesFeedbackBean
	 * @return
	 * @throws BusinessException
	 * @throws MethodArgumentNotValidException
	 * @Description: This method is used to create training attendees feedBack.
	 */
	@ApiOperation(tags="Training",value = " To create training attendees feedBack"
			+ "This service will be called from the front-end when the user wants to create training attendees feedBack", httpMethod = "POST", notes = "This Service has been implemented to create training attendees feedBack."
			+ "<br>The Details will be stored in database.", nickname = "Create Attendees Feed Back", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = TrainingDatAttendeeFeedbackBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = TrainingDatAttendeeFeedbackBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Created Attendee Feedback  resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingFeedBackRestURIConstants.CREATE_ATTENDEES_FEEDBACK, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> createTrainingAttendeesFeedBack(
			@ApiParam(name = "TrainingAttendeesFeedbackBO", example = "TrainingCreateAttendeesFeedbackBean", value = "Training feed Back Bo with all tha data to create the training feed back.") @RequestBody List<TrainingCreateAttendeesFeedbackBean> attendeesFeedbackBean)
			throws BusinessException, DataAccessException, ParseException,
			MethodArgumentNotValidException, CommonCustomException {
		LOG.startUsecase("Create Attendees Feed Back.");

		List<TrainingDatAttendeeFeedbackBO> trainingFeedbackBO;
		try {
			trainingFeedbackBO = trainingFeedBackService
					.createAttendeesFeedback(attendeesFeedbackBean);
			if (trainingFeedbackBO.size() > 0) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Attendees feedback are created."));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
								.value(), MISConstants.FAILURE,
								"Attendees feedback not created."));
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
	}

	// EOA by Smrithi
}
