/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingProgramController.java                    */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  22-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains  all the methods related to   */
/*                   training program controller				 	 */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 22-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.ValidationUtils;
import com.thbs.mis.training.Validator.TeamTrainingRequestValidator;
import com.thbs.mis.training.Validator.TrainingDatNominationDetailBeanValidator;
import com.thbs.mis.training.Validator.TrainingProgramDateValidator;
import com.thbs.mis.training.Validator.TrainingRequestDetailValidator;
import com.thbs.mis.training.bean.AddParticipantsOutputBean;
import com.thbs.mis.training.bean.CreateTrainingProgramBean;
import com.thbs.mis.training.bean.DetailedTrainingOutputBean;
import com.thbs.mis.training.bean.EmployeeAttendedDetailsBean;
import com.thbs.mis.training.bean.NominationDetailsUploadExcelBean;
import com.thbs.mis.training.bean.PagenationBean;
import com.thbs.mis.training.bean.TeamTrainingRequestBean;
import com.thbs.mis.training.bean.TeamTrainingRequestOutputBean;
import com.thbs.mis.training.bean.TrainingDatNominationDetailBean;
import com.thbs.mis.training.bean.TrainingDatNominationPoolDetailBean;
import com.thbs.mis.training.bean.TrainingDatProgrammeDetailBean;
import com.thbs.mis.training.bean.TrainingDetailsOutputBean;
import com.thbs.mis.training.bean.TrainingNominateBean;
import com.thbs.mis.training.bean.TrainingNominationBean;
import com.thbs.mis.training.bean.TrainingProgStatusBean;
import com.thbs.mis.training.bean.TrainingProgramBean;
import com.thbs.mis.training.bean.TrainingProgramDetailBean;
import com.thbs.mis.training.bean.TrainingProgramDetailOutputBean;
import com.thbs.mis.training.bean.TrainingProgramOutputBean;
import com.thbs.mis.training.bean.TrainingProgramViewBean;
import com.thbs.mis.training.bean.TrainingRequestDetailSubInputBean;
import com.thbs.mis.training.bean.TrainingRequestandProgramdetailsBean;
import com.thbs.mis.training.bean.UpdateTrainingProgramBean;
import com.thbs.mis.training.bo.TrainingDatNominationDetailBO;
import com.thbs.mis.training.bo.TrainingDatNominationPoolDetailBO;
import com.thbs.mis.training.bo.TrainingDatProgrammeDetailBO;
import com.thbs.mis.training.bo.TrainingMasProgrammeStatusBO;
import com.thbs.mis.training.constants.TrainingRestURIConstants;
import com.thbs.mis.training.service.TrainingProgramService;

/**
 * <Description TrainingProgramController:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
@Api(value = "Training Program Related Operations", description = "Operations Related to the Training Program", basePath = "http://localhost:8080/", consumes = "Rest Service", position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "Training Program Related Services")
@Controller
public class TrainingProgramController {

	// Added by Kamal Anand
	private static final AppLog LOG = LogFactory
			.getLog(TrainingProgramController.class);

	@Autowired
	TrainingProgramService trainingProgramService;

	@Autowired
	ServletContext servletContext;

	@InitBinder
	protected void initBinder(WebDataBinder binder) throws BindException {
		if (binder.getObjectName().equalsIgnoreCase("TrainingProgramBean")) {
			binder.addValidators(new TrainingProgramDateValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"TeamTrainingRequestBean")) {
			binder.addValidators(new TeamTrainingRequestValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"TrainingRequestDetailSubInputBean")) {
			binder.addValidators(new TrainingRequestDetailValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"TrainingDatNominationDetailBean")) {
			binder.addValidators(new TrainingDatNominationDetailBeanValidator());
		} else {
			binder.close();
		}
	}

	/**
	 * 
	 * @param trainingProgBean
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used for creating a new Training Program
	 */
	@ApiOperation(tags = "Training", value = " To create new Training Program."
			+ "This service will be called from the front-end when the user wants to create new training program", httpMethod = "POST", notes = "This Service has been implemented to create new training program."
			+ "<br>The Details will be stored in database.", nickname = "Create Training Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.CREATE_TRAINING_PROGRAM, method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> createTrainingProgram(
			@RequestParam(value = "file", name = "file", required = false) MultipartFile file,
			@RequestParam(value = "json", name = "json") String trainingProgBean)
			throws BusinessException, CommonCustomException,
			ConstraintViolationException, Exception {
		LOG.startUsecase("Create Training Program");
		TrainingDatProgrammeDetailBO trainingProgramDetail = null;
		try {
			ObjectMapper obj = new ObjectMapper();
			CreateTrainingProgramBean inputBean = null;
			try {
				inputBean = obj.readValue(trainingProgBean,
						CreateTrainingProgramBean.class);
			} catch (Exception e1) {
				throw new Exception(e1.getMessage());
			}
			trainingProgramDetail = trainingProgramService
					.createTrainingProgram(inputBean, file);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (ConstraintViolationException e) {
			throw new ConstraintViolationException(e.getConstraintViolations());
		}

		if (trainingProgramDetail != null) {
			LOG.endUsecase("Create Training Program");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Training Program Created Successfully."));
		} else {
			LOG.endUsecase("Create Training Program");
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
							.value(), MISConstants.FAILURE,
							"Some Error happend while creating Training Program."));
		}
	}

	/**
	 * 
	 * @param response
	 * @param progId
	 * @return
	 * @throws CommonCustomException
	 * @throws Exception
	 * @Description : This service is used to download Training Program Agenda
	 *              from server.
	 */
	@ApiOperation(tags = "Training", value = " To download Program Agenda."
			+ "This service will be called from the front-end when the user wants to download Program Agenda", httpMethod = "GET", notes = "This Service has been implemented to download Program Agenda."
			+ "<br>The Details will be downloaded from server.", nickname = "Download Training Program Agenda", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Downloaded Program Agenda file", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.DOWNLOAD_PROGRAM_AGENDA, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> downloadProgramAgenda(
			HttpServletResponse response, @PathVariable("progId") Integer progId)
			throws CommonCustomException, Exception {
		boolean isFileDownloaded = false;
		try {
			isFileDownloaded = trainingProgramService.downloadProgramAgenda(
					progId, response);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		if (isFileDownloaded)
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Training Program Agenda downloaded successfully"));
		else
			return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
					new MISResponse(HttpStatus.BAD_REQUEST.value(),
							MISConstants.FAILURE,
							"Training Program Agenda Not Found."));
	}

	/**
	 * 
	 * @param trainingProgBean
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to update the existing Training
	 *              Program details
	 */
	@ApiOperation(tags = "Training", value = " To update a Training Program."
			+ "This service will be called from the front-end when the user wants to update a training program", httpMethod = "PUT", notes = "This Service has been implemented to update a training program."
			+ "<br>The Details will be stored in database.", nickname = "Update Training Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The updated program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.UPDATE_TRAINING_PROGRAM, method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> updateTrainingProgram(
			@RequestParam(value = "file", name = "file", required = false) MultipartFile file,
			@RequestParam(value = "json", name = "json") String trainingProgBean)
			throws BusinessException, DataAccessException,
			CommonCustomException, Exception {
		LOG.startUsecase("Update Training Program");
		TrainingDatProgrammeDetailBO trainingProgramDetail;
		try {
			ObjectMapper obj = new ObjectMapper();
			UpdateTrainingProgramBean inputBean = null;
			try {
				inputBean = obj.readValue(trainingProgBean,
						UpdateTrainingProgramBean.class);
			} catch (Exception e1) {
				throw new Exception(e1.getMessage());
			}
			trainingProgramDetail = trainingProgramService
					.updateTrainingProgram(inputBean, file);
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (trainingProgramDetail != null) {
			LOG.endUsecase("Update Training Program");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Training Program Updated Successfully."));
		} else {
			LOG.endUsecase("Update Training Program");
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
							.value(), MISConstants.FAILURE,
							"Some Error happend while Updating Training Program."));
		}
	}

	/**
	 * @param PagenationBean
	 *            This is input parameter for this controller
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to view all Training Programs
	 */
	@ApiOperation(tags = "Training", value = " To view all Training Programs."
			+ "This service will be called from the front-end when the user wants to view all training program", httpMethod = "POST", notes = "This Service has been implemented to view all training programs."
			+ "<br>The Details will be retrieved in database.", nickname = "View All Training Programs", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.VIEW_ALL_TRAINING_PROGRAMS, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> viewAllTrainingPrograms(
			@Valid @RequestBody PagenationBean pageBean)
			throws BusinessException, DataAccessException {
		LOG.startUsecase("View All Training Program");
		Page<TrainingProgramViewBean> trainingProgramDetailsList;
		try {
			trainingProgramDetailsList = trainingProgramService
					.viewAllTrainingPrograms(pageBean);
		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception occured in View all Training Programs", e);
		}
		if (trainingProgramDetailsList.getContent().size() > 0) {
			LOG.endUsecase("Create Training Request");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Training Program Details List.",
							trainingProgramDetailsList));
		} else {
			LOG.endUsecase("View All Training Program");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"No Training Program Details.",
							trainingProgramDetailsList));
		}
	}

	/**
	 * 
	 * @param progBean
	 * @param binding
	 *            This is input parameter for this controller
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws MethodArgumentNotValidException
	 * @Description : This method is used to view Training Programs based on
	 *              Status or Date Range
	 */
	@ApiOperation(tags = "Training", value = " To view all Training Programs based on status or date range."
			+ "This service will be called from the front-end when the user wants to view all training programs by status or date range", httpMethod = "POST", notes = "This Service has been implemented to view all training programs by status or date range."
			+ "<br>The Details will be retrieved in database.", nickname = "View All Training Programs", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.VIEW_ALL_TRAINING_PROGRAMS_BY_STATUS_DATERANGE, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> viewAllTrainingProgramsByStatusDateRange(
			@ApiParam(name = "TrainingProgramBean", example = "TrainingProgramBean", value = "Training Program Bean with all tha data to view training Programs.") @Valid @RequestBody TrainingProgramBean progBean,
			BindingResult binding) throws BusinessException,
			DataAccessException, MethodArgumentNotValidException {
		LOG.startUsecase("View All Training Programs by Status or Date Range");
		Page<TrainingProgramViewBean> trainingProgramDetailsList;
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				trainingProgramDetailsList = trainingProgramService
						.viewAllTrainingProgramsByStatusDateRange(progBean);
			}

		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception occured in View All Training Programs by Status or Date Range",
					e);
		}
		if (trainingProgramDetailsList.getContent().size() > 0) {
			LOG.endUsecase("View All Training Programs by Status or Date Range");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Training Program Details List.",
							trainingProgramDetailsList));
		} else {
			LOG.endUsecase("View All Training Programs by Status or Date Range");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"No Training Program Details.",
							trainingProgramDetailsList));
		}
	}

	/**
	 * 
	 * @param mgrId
	 *            This is input parameter for this controller
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to view all the Requested Training
	 *              Programs of a Reporting Manager
	 */
	@ApiOperation(tags = "Training", value = " To view all Requested Training Programs."
			+ "This service will be called from the front-end when the user wants to view all requested training programs", httpMethod = "GET", notes = "This Service has been implemented to view requested training programs."
			+ "<br>The Details will be retrieved in database.", nickname = "View Requested Training Programs", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.VIEW_ALL_REQUESTED_TRAINING_PROGRAMS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> viewAllRequestedTrainingProgram(
			@PathVariable int mgrId) throws BusinessException,
			DataAccessException {
		LOG.startUsecase("View All Requested Training Programs");
		List<TrainingProgramViewBean> trainingProgramDetailsList;
		try {
			trainingProgramDetailsList = trainingProgramService
					.viewAllRequestedTrainingProgram(mgrId);
		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception occured in View All Requested Training Programs:",
					e);
		}
		if (trainingProgramDetailsList.size() > 0) {
			LOG.endUsecase("View All Requested Training Programs");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Training Program Details List.",
							trainingProgramDetailsList));
		} else {
			LOG.endUsecase("View All Requested Training Programs");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS,
							"No Training Program Details Found."));
		}
	}

	/**
	 * 
	 * @param
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to Nominate the participants to
	 *              Training Program
	 */
	@ApiOperation(tags = "Training", value = " To nominate the participants for a training program."
			+ "This service will be called from the front-end when the user wants to nominate participants for a training program", httpMethod = "POST", notes = "This Service has been implemented to nominate participants to training program."
			+ "<br>The Details will be stored in database.", nickname = "Nominate Participants", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Nominate participants detailed resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.NOMINATE_PARTICIPANTS, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> nominateParticipants(
			@ApiParam(name = "TrainingNominateBean", example = "TrainingNominateBean", value = "List of Training Nomination Beans with all tha data to nominate participants.") @RequestBody List<TrainingNominateBean> nominationList)
			throws BusinessException, DataAccessException {
		LOG.startUsecase("Nominate Participants");
		List<TrainingDatNominationDetailBO> nominationDetailsList;
		try {
			nominationDetailsList = trainingProgramService
					.nominateParticipants(nominationList);
		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception occured in nominate participants:", e);
		}
		if (nominationDetailsList.size() > 0) {
			LOG.endUsecase("Nominate Participants");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Participants Nominated Successfully."));
		} else {
			LOG.endUsecase("Nominate Participants");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE, "Seats are filled."));
		}
	}

	/**
	 * 
	 * @param progId
	 *            This is input parameter for this controller
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to get all the reportees who have
	 *              attended the Training Program along with Training Program
	 *              Details
	 */
	@ApiOperation(tags = "Training", value = " To get Attendees list who has attended the training program."
			+ "This service will be called from the front-end when the user wants to see attendees who has attended the training program before giving post application feedback", httpMethod = "GET", notes = "This Service has been implemented to get attendees of a training program."
			+ "<br>The Details will be stored in database.", nickname = "Get Training Program Attendees", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Training Program detailed resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.GET_TRAINING_PROGRAM_ATTENDEES_BY_TRAININGPROGRAMID, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getTrainingProgramAndAttendees(
			@PathVariable int progId) throws BusinessException,
			DataAccessException {
		LOG.startUsecase("Get All Attendees of a Training Program with program details for providing post application feedback");
		TrainingNominationBean attendees = new TrainingNominationBean();
		List<TrainingNominationBean> attendeesList = new ArrayList<TrainingNominationBean>();
		try {
			attendees = trainingProgramService
					.getTrainingProgramAndAttendees(progId);
			attendeesList.add(attendees);
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		if (attendeesList.size() > 0) {
			LOG.endUsecase("Get All Attendees of a Training Program with program details for providing post application feedback");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Attendees List.",
							attendeesList));
		} else {
			LOG.endUsecase("Get All Attendees of a Training Program with program details for providing post application feedback");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE, "Attendees List.",
							attendeesList));
		}
	}

	/**
	 * 
	 * @param empId
	 * @param progId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to get the nomination status of the
	 *              employee based on Training Program Id
	 */
	/*
	 * @ApiOperation(tags="Training",value =
	 * "To get Employee Nomination status for a training program." +
	 * "This service will be called from the front-end when the user wants to  get Employee Nomination status for a training program"
	 * , httpMethod = "GET", notes =
	 * "This Service has been implemented to get Employee Nomination status for a training program."
	 * + "<br>The Details will be stored in database.", nickname =
	 * "Get Employee Nomination status for a Training Program", protocols =
	 * "HTTP", responseReference = "application/json", produces =
	 * "application/json", response = MISResponse.class)
	 * 
	 * @ApiResponses(value = {
	 * 
	 * @ApiResponse(code = 200, message = "Request Executed Successfully",
	 * response = MISResponse.class),
	 * 
	 * @ApiResponse(code = 201, message = "Created Successfully",
	 * responseContainer = "Http Response", responseHeaders =
	 * 
	 * @ResponseHeader(name = "Location", description =
	 * "Nominate status of employee detailed resource", response = URI.class)),
	 * 
	 * @ApiResponse(code = 404, message = "Resource NOT FOUND",
	 * responseContainer = "Http Response"),
	 * 
	 * @ApiResponse(code = 500, message = "Internal Server Error",
	 * responseContainer = "Http Response") })
	 * 
	 * @RequestMapping(value =
	 * TrainingRestURIConstants.GET_EMPLOYEE_NOMINATION_STATUS, method =
	 * RequestMethod.GET) public ResponseEntity<MISResponse>
	 * getEmployeeTrainingNominationStatus(
	 * 
	 * @PathVariable int empId, @PathVariable int progId) throws
	 * BusinessException, DataAccessException {
	 * LOG.startUsecase("Get Employee Nomination Status");
	 * AddParticipantsOutputBean attendees = new AddParticipantsOutputBean();
	 * List<AddParticipantsOutputBean> attendeesList = new
	 * ArrayList<AddParticipantsOutputBean>(); try { attendees =
	 * trainingProgramService .getEmployeeTrainingNominationStatus(empId,
	 * progId); attendeesList.add(attendees); } catch (Exception e) { throw new
	 * DataAccessException(
	 * "Exception occured in Get Employee Nomination Status:", e); } if
	 * (attendees != null) { LOG.endUsecase("Get Employee Nomination Status");
	 * return ResponseEntity.status(HttpStatus.OK.value()).body( new
	 * MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
	 * "Employee Nomination Details.", attendeesList)); } else {
	 * LOG.endUsecase("Get Employee Nomination Status"); return ResponseEntity
	 * .status(HttpStatus.INTERNAL_SERVER_ERROR.value()) .body(new
	 * MISResponse(HttpStatus.INTERNAL_SERVER_ERROR .value(),
	 * MISConstants.FAILURE,
	 * "Some Error occured while getting nomination details.")); } }
	 */

	/**
	 * 
	 * @param mgrId
	 *            , progId This is input parameter for this controller
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Desciption : This method is used to get all Reportees and their
	 *             nomination status for a particular Training Program
	 */
	@ApiOperation(tags = "Training", value = "To get All Reportees Nomination status for a training program."
			+ "This service will be called from the front-end when the user wants to  get all Reportees Nomination status for a training program", httpMethod = "GET", notes = "This Service has been implemented to get all Reportees Nomination status for a training program."
			+ "<br>The Details will be stored in database.", nickname = "Get all reporteees Nomination status for a Training Program", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Nominate status of all reportees detailed resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.GET_REPORTEES_TRAINING_NOMINATION_STATUS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAllReporteesAndTrainingProgramNominationStatus(
			@PathVariable int mgrId, @PathVariable int progId)
			throws BusinessException, DataAccessException,
			CommonCustomException {
		LOG.startUsecase("Get All Reportees Nomination Status");
		List<AddParticipantsOutputBean> attendeesList = new ArrayList<AddParticipantsOutputBean>();
		try {
			attendeesList = trainingProgramService
					.getAllReporteesAndTrainingProgramNominationStatus(mgrId,
							progId);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		if (attendeesList.size() > 0) {
			LOG.endUsecase("Get All Reportees Nomination Status");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Employee Nomination Details.", attendeesList));
		} else {
			LOG.endUsecase("Get All Reportees Nomination Status");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS,
							"No employee nomination details found."));
		}
	}

	/**
	 * 
	 * @param mgrId
	 *            , progId This is input parameter for this controller
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to retrieve Training Program Details
	 *              and also approval status of the employee for a training
	 *              program
	 */
	@ApiOperation(tags = "Training", value = "To view Program Details and approval status of employee."
			+ "This service will be called from the front-end when the user wants to view Program Details and approval status of an  employee", httpMethod = "GET", notes = "This Service has been implemented to view Program Details and approval status of an employee."
			+ "<br>The Details will be stored in database.", nickname = "Program Details and approval status of employee", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Training program detailed resource with approval status", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.VIEW_PROGRAM_DETAILS_TEAM_TRAINING_REQUESTS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> viewProgramDetailsForTeamTrainingRequests(
			@PathVariable int progId, @PathVariable int empId)
			throws BusinessException, CommonCustomException {
		LOG.startUsecase("Get Employee Approval Status for a Training Program");
		TrainingProgramDetailBean progDetail = new TrainingProgramDetailBean();
		List<TrainingProgramDetailBean> progList = new ArrayList<TrainingProgramDetailBean>();
		try {
			progDetail = trainingProgramService
					.viewProgramDetailsForTeamTrainingRequests(progId, empId);
			progList.add(progDetail);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (progDetail != null) {
			LOG.endUsecase("Get Program Details and Employee Approval Status for Training Program");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Program Details and approval status of employee.",
							progList));
		} else {
			LOG.endUsecase("Get Program Details and Employee Approval Status for Training Program");
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
							.value(), MISConstants.FAILURE,
							"Some Error occured while getting program details and approval status."));
		}
	}

	/**
	 * 
	 * @param TrainingProgStatusBean
	 *            This is input parameter for this controller
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This method is used to update the Training Program Status.
	 */
	@ApiOperation(tags = "Training", value = "To update the Training Program status."
			+ "This service will be called from the front-end when the user wants to update training program status", httpMethod = "PUT", notes = "This Service has been implemented to update training program status."
			+ "<br>The Details will be stored in database.", nickname = "Update Training Program status", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Training program detailed resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.UPDATE_TRAINING_PROGRAM_STATUS, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> updateTrainingProgramStatus(
			@ApiParam(name = "TrainingProgStatusBean", example = "TrainingProgStatusBean", value = "Training Program status Bean with all tha data to update Training Program Status.") @RequestBody TrainingProgStatusBean trainingProgBean)
			throws BusinessException, DataAccessException {
		LOG.startUsecase("Update Training Program Status");
		TrainingDatProgrammeDetailBO progDetail;
		try {
			progDetail = trainingProgramService
					.updateTrainingProgramStatus(trainingProgBean);
		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception occured in Update Training Program Status:", e);
		}
		if (progDetail != null) {
			LOG.endUsecase("Update Training Program Status");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Training Program Status Updated Successfully."));
		} else {
			LOG.endUsecase("Update Training Program Status");
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
							.value(), MISConstants.FAILURE,
							"Some Error happend while Updating Training Program Status."));
		}
	}

	/**
	 * 
	 * @param progId
	 *            This is input parameter for this controller
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @Description : This service is used to get All Active Employee's
	 *              Nomination status for a Training Program
	 */
	@ApiOperation(tags = "Training", value = "To get All Active Employee's Nomination status for a training program."
			+ "This service will be called from the front-end when the user wants to  get all Active Employee's Nomination status for a training program", httpMethod = "GET", notes = "This Service has been implemented to get all Active Employee's Nomination status for a training program."
			+ "<br>The Details will be stored in database.", nickname = "Get all active employee's Nomination status for a Training Program", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Nomination status of all active employee's for a training program detailed resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.GET_ALL_ACTIVE_EMPLOYEE_TRAINING_NOMINATION_STATUS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAllReporteesAndTrainingProgramNominationStatus(
			@PathVariable int progId) throws BusinessException,
			DataAccessException {
		LOG.startUsecase("Get All Active Employee's Nomination Status");
		List<AddParticipantsOutputBean> attendeesList = new ArrayList<AddParticipantsOutputBean>();
		try {
			attendeesList = trainingProgramService
					.getAllActiveEmployeesAndTrainingProgramNominationStatus(progId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception occured in Get All Active Employee's Nomination Status:",
					e);
		}
		if (attendeesList.size() > 0) {
			LOG.endUsecase("Get All Active Employee's Nomination Status");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Employee Nomination Details.", attendeesList));
		} else {
			LOG.endUsecase("Get All Active Employee's Nomination Status");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Employee Nomination Details."));
		}
	}

	/**
	 * 
	 * @param progId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(tags = "Training", value = "To get All Nominated Employee's of a Training Program."
			+ "This service will be called from the front-end when the user wants to  get all Nominated Employee's for a training program", httpMethod = "GET", notes = "This Service has been implemented to get all Nominated Employee's for a training program."
			+ "<br>The Details will be retrieved database.", nickname = "Get all Nominated Employee's of a Training Program", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Attended Employee Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.GET_NOMINATED_EMPLOYEES_BY_TRAININGPROGRAMID, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAllNominatedEmployeesOfTrainingProgram(
			@PathVariable int progId) throws BusinessException,
			DataAccessException {
		LOG.startUsecase("Get All Nominated Employee's of a Training Program");
		List<EmployeeAttendedDetailsBean> attendeesList = new ArrayList<EmployeeAttendedDetailsBean>();
		try {
			attendeesList = trainingProgramService
					.getAllNominatedEmployeesOfTrainingProgram(progId);
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		if (attendeesList.size() > 0) {
			LOG.endUsecase("Get All Nominated Employee's of a Training Program");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Attended Employee Details.",
							attendeesList));
		} else {
			LOG.endUsecase("Get All Nominated Employee's of a Training Program");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"No Attended Employee Details."));
		}
	}

	/**
	 * 
	 * @param empId
	 * @param progId
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @Description : This service is used to remove participants from a
	 *              Training Program
	 */
	@ApiOperation(tags = "Training", value = "To remove Participants from a Training Program."
			+ "This service will be called from the front-end when the user wants to  remove Participants from a training program", httpMethod = "DELETE", notes = "This Service has been implemented to remove participants from a training program."
			+ "<br>The Details will be deleted from database.", nickname = "Delete Participants of a Training Program", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Remove Participants success or failure message", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.REMOVE_PARTICIPANTS, method = RequestMethod.DELETE)
	public ResponseEntity<MISResponse> removeParticipants(
			@PathVariable int empId, @PathVariable int progId)
			throws CommonCustomException {
		LOG.startUsecase("Entering removeParticipants");
		boolean removePaticipants;
		try {
			removePaticipants = trainingProgramService.removeParticipants(
					empId, progId);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (removePaticipants) {
			LOG.endUsecase("Exiting removeParticipants");
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Participant Removed from the Training Program successfully"));
		} else {
			LOG.endUsecase("Exiting removeParticipants");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Some error occured while removing participant"));
		}
	}

	// End of Addition by Kamal Anand

	// Added by Rajesh Kumar
	/**
	 * <Description createTrainingNomiPoolDetail:> This contains the methods
	 * used for the CREATE_NOMINATION_POOL_DETAILS for the Controller operation.
	 * 
	 * @param TrainingDatNominationPoolDetailBean
	 *            This is input parameter for this controller
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @author THBS
	 * @version 1.0
	 * @throws BusinessException
	 * @see
	 */
	@ApiOperation(tags = "Training", value = "Create Nomination Pool Details."
			+ "To Create Nomination Pool details.", httpMethod = "POST", notes = "This Service has been implemented to Create Nomination Pool details."
			+ "<br>The Response will be the detailed of the Create Nomination Pool."
			+ "<br><b>Inputs :</b><br> Create Nomination Pool Details. <br><br>",

	nickname = "Create Nomination Pool Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = TrainingDatNominationPoolDetailBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = TrainingDatNominationPoolDetailBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Created nomination pool detailsresource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.CREATE_NOMINATION_POOL_DETAILS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> createTrainingNomiPoolDetail(
			@Valid @ApiParam(name = "createNomintionPoolDetail", example = "TrainingDatNominationPoolDetailBean", value = "Create Nomination Pool Details.") @RequestBody TrainingDatNominationPoolDetailBean bean)
			throws BusinessException, CommonCustomException,
			DataAccessException {
		LOG.startUsecase("Create Nomination Pool Details");
		TrainingDatNominationPoolDetailBO nominationDetail;
		try {
			nominationDetail = trainingProgramService
					.createTrainingNomiPoolDetail(bean);
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (nominationDetail != null) {
			LOG.endUsecase("Create Nomination Pool Details");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Nomination Pool Details created successfully."));
		} else {
			LOG.endUsecase("Create Nomination Pool Details");
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
							.value(), MISConstants.FAILURE,
							"Some Error happened while creating nomination pool details."));
		}

	}

	// EOA by Rajesh Kumar

	// Added by Prathibha

	/**
	 * @Description:Method is invoked to create Training Program by Training
	 *                     Coordinator>
	 * @param trainingRequestandProgramdetailsBean
	 * @param binding
	 * @return TrainingDatProgrammeDetailBO created.
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(tags = "Training", value = "Create a new Training Program by Training co-ordiantor."
			+ "This service will be called from the front-end when the Training coordinator want to create training program details.", httpMethod = "POST", notes = "This Service has been implemented to create training program details by the Training co-ordinator."
			+ "<br>The Details will be stored in database."
			+ "<br>The Response will be TrainingDatProgrammeDetailBO (will be SUCCESS  when row created snd FAILURE when Program not created)"
			+ "<br><b>Inputs :</b><br>createTrainingProgrambyTrainingCoordinator <br><br>",

	nickname = "Training Program Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = TrainingDatProgrammeDetailBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = TrainingDatProgrammeDetailBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program by Training Coordinator", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.CREATE_TRAINING_PROGRAM_BY_TRAINING_COORDINATOR, method = RequestMethod.POST, consumes = "multipart/form-data")
	public @ResponseBody ResponseEntity<MISResponse> createTrainingProgrambyTC(
			@RequestParam(value = "file", name = "file", required = false) MultipartFile file,
			@RequestParam(value = "json", name = "json") String trainingRequestandProgramdetailsBean)
			throws DataAccessException, Exception {
		LOG.startUsecase("Create Training Program");
		TrainingDatProgrammeDetailBO progDetail;

		try {
			ObjectMapper obj = new ObjectMapper();
			TrainingRequestandProgramdetailsBean inputBean = null;
			try {
				inputBean = obj.readValue(trainingRequestandProgramdetailsBean,
						TrainingRequestandProgramdetailsBean.class);
			} catch (Exception e1) {
				throw new Exception(e1.getMessage());
			}
			progDetail = trainingProgramService
					.createTrainingProgramByTrainingCoordinator(inputBean, file);
			if (progDetail != null) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Training Program Created Successfully"));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
								.value(), MISConstants.FAILURE,
								"Unable to create Training Program"));
			}
		} catch (DataAccessException e) {
			LOG.endUsecase("Training_Program_Controller _Catch");
			throw new DataAccessException(
					"Exception occured in create training program by training coordinator:",
					e);
		}
	}

	// EOA by Prathibha

	// Added by Balaji

	/**
	 * 
	 * @param requestInputBean
	 *            This is input parameter for this controller
	 * @param binding
	 *            This is input parameter for this controller
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @Description This service is used to retrieve the Team training requests
	 */
	@ApiOperation(tags = "Training", value = "view The list Of Team training requests"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to see the Team training request details."

			+ "<br>The Details will be retrieved as per request."

			+ "<br>The Response will be the detailed of the Team training request Details."

			+ "<br><b>Inputs are Approver Employee ID, Nomination Status</b><br>", nickname = "Team Training Program Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Data Retrieved  Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of All training request details resource", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of All training request details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.TEAM_TRAINING_REQUEST, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> teamTrainingRequests(
			@RequestBody @Valid TeamTrainingRequestBean requestInputBean,
			BindingResult binding) throws BusinessException {
		LOG.startUsecase("teamTrainingRequests");
		List<TeamTrainingRequestOutputBean> teamTrainingRequestOutput = new ArrayList<TeamTrainingRequestOutputBean>();
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				teamTrainingRequestOutput = trainingProgramService
						.viewTeamTrainingRequest(requestInputBean);
			}

		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		LOG.endUsecase("teamTrainingRequests");
		if (teamTrainingRequestOutput.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Requests are Displayed",
							teamTrainingRequestOutput));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Records not found",
							teamTrainingRequestOutput));
		}
	}

	@ApiOperation(tags = "Training", value = "view The list Of training Programs which created between last 90 days from current date"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to see the training program details which created between last 90 days from current date."

			+ "<br>The Details will be retrieved as per request."

			+ "<br>The Response will be the detailed of the training program Details."

			+ "<br><b>Inputs are Reporting manager Id, feedback status or date range </b><br>", nickname = "Training Program Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Data Retrieved  Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of All training  details resource", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of All training  details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	/**
	 * 
	 * @param trainingProgrammeInputBean, binding This is input parameter for this controller
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws CommonCustomException
	 * @throws ParseException
	 * @Description This service is used to view Training requests details by passing RmId, feedback status and by date range 
	 */
	@RequestMapping(value = TrainingRestURIConstants.TRAINING_DETAILS_BY_DATE_RMID, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> trainingDetailsByDateAndRM(
			@RequestBody @Valid TrainingRequestDetailSubInputBean trainingProgrammeInputBean,
			BindingResult binding) throws CommonCustomException,
			ParseException, BusinessException {
		LOG.startUsecase("trainingDetailsByDateAndRM");

		List<TrainingDetailsOutputBean> trainingDetailsOutputBean = new ArrayList<TrainingDetailsOutputBean>();

		try {

			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				trainingDetailsOutputBean = trainingProgramService
						.viewTrainingDetailsByDateAndRM(trainingProgrammeInputBean);
			}

		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		LOG.endUsecase("trainingDetailsByDateAndRM");

		if (trainingDetailsOutputBean.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Requests are Displayed",
							trainingDetailsOutputBean));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE, "Request not found",
							trainingDetailsOutputBean));
		}
	}

	// End of Addition by Balaji

	// Added by Pratibha
	/**
	 * 
	 * @param nominationDetailBean
	 *            This is input parameter for this controller
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @Description : Service is used to get all Employee Nominated Training
	 *              Programs and also Training Coordinator created Training
	 *              Program by Date Range
	 */

	@ApiOperation(tags = "Training", value = "To Get All Trainings for Employee."
			+ "This service is used to get all trainings for employee.", httpMethod = "POST", notes = "This Service has been implemented to get all trainings for employee.", nickname = "Get All Trainings for Employee", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of trainings of all employees  resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.GET_ALL_TRAININGS_FOR_EMPLOYEE, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> getAllTrainingsForEmployee(
			@Valid @RequestBody TrainingDatNominationDetailBean nominationDetailBean)
			throws BusinessException {
		LOG.startUsecase("get All Trainings For Employee");
		List<Map> nominationDetail;
		try {
			nominationDetail = trainingProgramService
					.getAllTrainingsForEmployee(nominationDetailBean);
			if (nominationDetail.size() > 0) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Training details for employee.",
								nominationDetail));
			} else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.FAILURE,
								"Training details for employee are not found.",
								nominationDetail));
			}

		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}

	/**
	 * 
	 * @param pgmId
	 *            This is input parameter for this controller
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @Description : Service is used to view all training program details based
	 *              on programId
	 */
	@ApiOperation(tags = "Training", value = "To view training program details based on programId."
			+ "This service is used to to view  training program details  based on programId.", httpMethod = "GET", notes = "This Service has been implemented to view  training program details  based on programId.", nickname = "View nominated training programs based On programId", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of  training program details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.VIEW_TRAINING_PROGRAM_DETAILS_BY_PROGRAM_ID, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> toViewTrainingProgramDetailsBasedOnProgramId(
			@PathVariable int pgmId) throws BusinessException {
		LOG.startUsecase("toViewTrainingProgramDetailsBasedOnProgramId");
		TrainingProgramDetailOutputBean progDetail;
		List<TrainingProgramDetailOutputBean> progDetailsList = new ArrayList<TrainingProgramDetailOutputBean>();
		try {
			progDetail = trainingProgramService
					.viewTrainingProgramDetailsBasedOnProgramId(pgmId);
			progDetailsList.add(progDetail);

		} catch (Exception e) {

			throw new BusinessException(e.getMessage());
		}
		if (progDetail != null) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Training Program Details.",
							progDetailsList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Training Program Details not found"));
		}

	}

	/**
	 * @param
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @Description :Service is used to get all Training Program Status
	 */

	@ApiOperation(tags = "Training", value = " To Get training program status."
			+ "This service is used to get the training program status.", httpMethod = "GET", notes = "This Service has been implemented to get the training program status.", nickname = "Get training program status", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of training program status resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.GET_TRAINING_PROGRAM_STATUS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getTrainingProgramStatus()
			throws BusinessException {
		LOG.startUsecase("getTrainingProgramStatus");
		List<TrainingMasProgrammeStatusBO> programStatus = null;

		try {
			programStatus = trainingProgramService.getTrainingProgramStatus();

		} catch (Exception e) {

			throw new BusinessException(e.getMessage());
		}
		if (programStatus.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							" program details based on date.", programStatus));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							" program details based on date are not found"));
		}
	}

	/**
	 * 
	 * @param programId
	 * @param empId
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @Description :Service is used to get Training Programs based on Training
	 *              program id and nomination status
	 */
	@ApiOperation(tags = "Training", value = "To view Training Program based on training programId and set nomination status."
			+ "This service is used to to view training program based on training programId and nomination status.", httpMethod = "GET", notes = "This Service has been implemented to view training program based on training programId and nomination status.", nickname = "View Training Program based on training programId and set nomination status", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of training programs  resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.GET_TRAINING_PROGRAM_BY_PROGRAM_ID_AND_SET_NOMINATION_STATUS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getTrainingProgramBasedOnTrainingProgramIdAndSetNominationStatus(
			@PathVariable int programId, @PathVariable int empId)
			throws BusinessException {
		LOG.startUsecase("viewTrainingProgramBasedOnTrainingProgramIdAndSetNominationSatus");
		List<DetailedTrainingOutputBean> programDetailList = new ArrayList<DetailedTrainingOutputBean>();
		DetailedTrainingOutputBean programDetail;
		try {
			programDetail = trainingProgramService
					.getTrainingProgramBasedOnTrainingProgramIdAndSetNominationStatus(
							programId, empId);
			programDetailList.add(programDetail);

		} catch (Exception e) {

			throw new BusinessException("exception ", e);
		}
		if (programDetailList.size() > 0) {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(
							HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Training Program based on training programId and nomination status.",
							programDetailList));
		} else {
			return ResponseEntity
					.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Training Program based on training programId are not found"));
		}
	}

	/**
	 * 
	 * @param proBean
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @Description :Service is used to get all Employee Nominated Training
	 *              Programs and also Training Coordinator created Training
	 *              Program by Date Both RM created and Training Coordinator
	 *              Created
	 */
	@ApiOperation(tags = "Training", value = "To retrieve Training program details  for Employee Based on Date."
			+ "This service is used to retrieve  training program details  for employee based on date.", httpMethod = "POST", notes = "This Service has been implemented to  retrieve  training program details for employee based on date.", nickname = "Get Trainings for Employee Based on Date", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of  Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = TrainingRestURIConstants.RETRIEVE_PROGRAM_DETAILS_BASED_ON_DATE, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> retrieveProgramDetails(
			@Valid @RequestBody TrainingDatProgrammeDetailBean proBean)
			throws BusinessException {
		LOG.startUsecase("retrieveProgramDetails");
		List<TrainingProgramOutputBean> programDetail = null;
		try {
			programDetail = trainingProgramService
					.retrieveProgramDetails(proBean);

		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		if (programDetail.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							" program details based on date.", programDetail));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							" program details based on date are not found"));
		}
	}

	// EOA by Pratibha
	// Added by Mani
	/**
	 * 
	 * @param : updatedBy
	 * @return ResponseEntity<MISResponse> The Entity contains MIS Response
	 * @throws BusinessException
	 * @throws ParseException
	 * @Description :Service is used to parse the excel sheet data and converted
	 *              to nomination details bean to update the details to
	 *              training_dat_nomination_detail for the columns for the
	 *              fk_attendence_update_by
	 *              ,attendence_update_date,attended_training.
	 * @exception :throws CommonCustomException, IOException {@code} :
	 *            ExcelData.java,ExcelSheetData.java,ReadExcelTeampleUtil.java
	 *            these are all the dependency file to parse the excel sheet
	 *            data.
	 */
	@ApiOperation(tags = "Training", value = "Parse the Training Nomination Attendance Deatails and Update to Database"
			+ "Parse the Training Nomination Attendance Deatails from the excel sheet and Upadte to Database", httpMethod = "GET", notes = "This Service has been implemented to parse the excel sheet data and update to attendance details", nickname = "Update Training Nomination Attendance Deatails.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Updated Successfully", response = MISResponse.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	// @RequestMapping(value =
	// TrainingRestURIConstants.UPLOAD_TRAINING_NOMINATION_STATUS, method =
	// RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@RequestMapping(value = TrainingRestURIConstants.UPLOAD_TRAINING_NOMINATION_STATUS, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> upload(
			@RequestBody List<NominationDetailsUploadExcelBean> nominationDetailsUploadList,
			@PathVariable int updatedBy) throws Exception {
		System.out.println("size " + nominationDetailsUploadList.size());

		List<NominationDetailsUploadExcelBean> outputList = new ArrayList<NominationDetailsUploadExcelBean>();
		outputList = trainingProgramService.uploadNominationDetails(
				nominationDetailsUploadList, updatedBy);

		LOG.endUsecase("uploadNominationDetails");

		if (!outputList.isEmpty()) {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(
							HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Training Nomination Attendance Details has been updated successfully.",
							outputList));
		} else {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(
							HttpStatus.OK.value(),
							MISConstants.FAILURE,
							"Some Error Occured while saving the data from excel sheet.Please contact to administrator"));
		}

	}

}
