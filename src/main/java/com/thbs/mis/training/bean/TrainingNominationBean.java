/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingNominationBean.java                   	  */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Training feed Back.                              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class TrainingNominationBean {

	private String trainingTitle;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date trainingStartDate;
	private String trainerName;
	private String venue;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date time;
	private String initiatedBy;
	private List<AttendedEmployeeDetailsBean> attendedEmpList;
	private int progId;
	private String programmeCategory;

	public String getInitiatedBy() {
		return initiatedBy;
	}

	public void setInitiatedBy(String initiatedBy) {
		this.initiatedBy = initiatedBy;
	}

	public int getProgId() {
		return progId;
	}

	public void setProgId(int progId) {
		this.progId = progId;
	}

	public String getTrainingTitle() {
		return trainingTitle;
	}

	public void setTrainingTitle(String trainingTitle) {
		this.trainingTitle = trainingTitle;
	}

	public Date getTrainingStartDate() {
		return trainingStartDate;
	}

	public void setTrainingStartDate(Date trainingStartDate) {
		this.trainingStartDate = trainingStartDate;
	}

	public String getTrainerName() {
		return trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getIntiatedBy() {
		return initiatedBy;
	}

	public void setIntiatedBy(String initiatedBy) {
		this.initiatedBy = initiatedBy;
	}

	public List<AttendedEmployeeDetailsBean> getAttendedEmpList() {
		return attendedEmpList;
	}

	public void setAttendedEmpList(List<AttendedEmployeeDetailsBean> attendedEmpList) {
		this.attendedEmpList = attendedEmpList;
	}

	public String getProgrammeCategory() {
		return programmeCategory;
	}

	public void setProgrammeCategory(String programmeCategory) {
		this.programmeCategory = programmeCategory;
	}

	@Override
	public String toString() {
		return "TrainingNominationBean [trainingTitle=" + trainingTitle
				+ ", trainingStartDate=" + trainingStartDate + ", trainerName="
				+ trainerName + ", venue=" + venue + ", time=" + time
				+ ", initiatedBy=" + initiatedBy + ", attendedEmpList="
				+ attendedEmpList + ", progId=" + progId
				+ ", programmeCategory=" + programmeCategory + "]";
	}

}
