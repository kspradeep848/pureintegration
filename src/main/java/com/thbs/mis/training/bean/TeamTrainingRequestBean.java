/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TeamTrainingRequestBean.java                   	 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the fields                */
/*                 used for the required BO                          */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016     THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;

import org.springframework.format.annotation.DateTimeFormat;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class TeamTrainingRequestBean {

	byte nominationStatusId;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd")
	Date trainingStartDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd")
	Date trainingEndDate;
	
	//@Employee_Id_Not_Found
	Integer fkApproverEmpId;
	 
	//@Employee_Id_Not_Found
	Integer nominatedEmpId;

	public byte getNominationStatusId() {
		return nominationStatusId;
	}

	public void setNominationStatusId(byte nominationStatusId) {
		this.nominationStatusId = nominationStatusId;
	}

	public Date getTrainingStartDate() {
		return trainingStartDate;
	}

	public void setTrainingStartDate(Date trainingStartDate) {
		this.trainingStartDate = trainingStartDate;
	}

	public Date getTrainingEndDate() {
		return trainingEndDate;
	}

	public void setTrainingEndDate(Date trainingEndDate) {
		this.trainingEndDate = trainingEndDate;
	}

	public Integer getFkApproverEmpId() {
		return fkApproverEmpId;
	}

	public void setFkApproverEmpId(Integer fkApproverEmpId) {
		this.fkApproverEmpId = fkApproverEmpId;
	}

	public Integer getNominatedEmpId() {
		return nominatedEmpId;
	}

	public void setNominatedEmpId(Integer nominatedEmpId) {
		this.nominatedEmpId = nominatedEmpId;
	}

	@Override
	public String toString() {
		return "TeamTrainingRequestBean [nominationStatusId="
				+ nominationStatusId + ", trainingStartDate="
				+ trainingStartDate + ", trainingEndDate=" + trainingEndDate
				+ ", fkApproverEmpId=" + fkApproverEmpId + ", nominatedEmpId="
				+ nominatedEmpId + "]";
	}

}
