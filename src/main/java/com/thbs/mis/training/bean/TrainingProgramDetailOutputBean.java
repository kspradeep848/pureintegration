/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingProgramDetailOutputBean.java               */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Training feed Back.                              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class TrainingProgramDetailOutputBean {

	private String trainingTitle;

	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date startDate;
	private Integer noOfParticipants;
	private String trainer;
	private String venue;
	private String initiatedBy;
	private String description;
	private Integer progId;
	private int progStatusId;
	private String progStatus;
	//private int durationInHours;
	private int durationInDays;
	private int noOfHrsPerDay;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date endDate;
	private int maxSeats;
	private String trainerType;
	private TrainingRequestDetailBean requestDetails;
	private String externalVenueName;
	private Integer remainingSeats;
	private Byte programCategoryId;
	private String programCategoryName;
	private String isAgendaUploaded;
	private String programmeCategory;
	
	public String getIsAgendaUploaded() {
		return isAgendaUploaded;
	}

	public void setIsAgendaUploaded(String isAgendaUploaded) {
		this.isAgendaUploaded = isAgendaUploaded;
	}

	public Byte getProgramCategoryId() {
		return programCategoryId;
	}

	public void setProgramCategoryId(Byte programCategoryId) {
		this.programCategoryId = programCategoryId;
	}

	public String getProgramCategoryName() {
		return programCategoryName;
	}

	public void setProgramCategoryName(String programCategoryName) {
		this.programCategoryName = programCategoryName;
	}

	public Integer getRemainingSeats() {
		return remainingSeats;
	}

	public void setRemainingSeats(Integer remainingSeats) {
		this.remainingSeats = remainingSeats;
	}

	public String getExternalVenueName() {
		return externalVenueName;
	}

	public void setExternalVenueName(String externalVenueName) {
		this.externalVenueName = externalVenueName;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getMaxSeats() {
		return maxSeats;
	}

	public void setMaxSeats(int maxSeats) {
		this.maxSeats = maxSeats;
	}

	public String getTrainerType() {
		return trainerType;
	}

	public void setTrainerType(String trainerType) {
		this.trainerType = trainerType;
	}

	public String getTrainingTitle() {
		return trainingTitle;
	}

	public void setTrainingTitle(String trainingTitle) {
		this.trainingTitle = trainingTitle;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getNoOfParticipants() {
		return noOfParticipants;
	}

	public void setNoOfParticipants(Integer noOfParticipants) {
		this.noOfParticipants = noOfParticipants;
	}

	public String getTrainer() {
		return trainer;
	}

	public void setTrainer(String trainer) {
		this.trainer = trainer;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getInitiatedBy() {
		return initiatedBy;
	}

	public void setInitiatedBy(String initiatedBy) {
		this.initiatedBy = initiatedBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getProgId() {
		return progId;
	}

	public void setProgId(Integer progId) {
		this.progId = progId;
	}

	public int getProgStatusId() {
		return progStatusId;
	}

	public void setProgStatusId(int progStatusId) {
		this.progStatusId = progStatusId;
	}

	public String getProgStatus() {
		return progStatus;
	}

	public void setProgStatus(String progStatus) {
		this.progStatus = progStatus;
	}

	public int getDurationInDays() {
		return durationInDays;
	}

	public void setDurationInDays(int durationInDays) {
		this.durationInDays = durationInDays;
	}

	public int getNoOfHrsPerDay() {
		return noOfHrsPerDay;
	}

	public void setNoOfHrsPerDay(int noOfHrsPerDay) {
		this.noOfHrsPerDay = noOfHrsPerDay;
	}

	public TrainingRequestDetailBean getRequestDetails() {
		return requestDetails;
	}

	public void setRequestDetails(TrainingRequestDetailBean requestDetails) {
		this.requestDetails = requestDetails;
	}

	public String getProgrammeCategory() {
		return programmeCategory;
	}

	public void setProgrammeCategory(String programmeCategory) {
		this.programmeCategory = programmeCategory;
	}

	@Override
	public String toString() {
		return "TrainingProgramDetailOutputBean [trainingTitle="
				+ trainingTitle + ", startDate=" + startDate
				+ ", noOfParticipants=" + noOfParticipants + ", trainer="
				+ trainer + ", venue=" + venue + ", initiatedBy=" + initiatedBy
				+ ", description=" + description + ", progId=" + progId
				+ ", progStatusId=" + progStatusId + ", progStatus="
				+ progStatus + ", durationInDays=" + durationInDays
				+ ", noOfHrsPerDay=" + noOfHrsPerDay + ", endDate=" + endDate
				+ ", maxSeats=" + maxSeats + ", trainerType=" + trainerType
				+ ", requestDetails=" + requestDetails + ", externalVenueName="
				+ externalVenueName + ", remainingSeats=" + remainingSeats
				+ ", programCategoryId=" + programCategoryId
				+ ", programCategoryName=" + programCategoryName
				+ ", isAgendaUploaded=" + isAgendaUploaded
				+ ", programmeCategory=" + programmeCategory + "]";
	}
}
