/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  AttendedTrainingOutputBean.java                                                        */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/

package com.thbs.mis.training.bean;

public class AttendedTrainingOutputBean {

	private int noOfPaticipants;
	private int noOfParticipantsGivenFeedback;
	private int progId;
	
	
	public int getProgId() {
		return progId;
	}

	public void setProgId(int progId) {
		this.progId = progId;
	}

	public int getNoOfPaticipants() {
		return noOfPaticipants;
	}

	public void setNoOfPaticipants(int noOfPaticipants) {
		this.noOfPaticipants = noOfPaticipants;
	}

	public int getNoOfParticipantsGivenFeedback() {
		return noOfParticipantsGivenFeedback;
	}

	public void setNoOfParticipantsGivenFeedback(
			int noOfParticipantsGivenFeedback) {
		this.noOfParticipantsGivenFeedback = noOfParticipantsGivenFeedback;
	}

	@Override
	public String toString() {
		return "AttendedTrainingOutputBean [noOfPaticipants=" + noOfPaticipants
				+ ", noOfParticipantsGivenFeedback="
				+ noOfParticipantsGivenFeedback + ", progId=" + progId + "]";
	}

}
