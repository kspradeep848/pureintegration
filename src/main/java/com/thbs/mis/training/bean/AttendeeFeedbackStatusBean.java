package com.thbs.mis.training.bean;

public class AttendeeFeedbackStatusBean {

	private int empId;
	private String empName;
	private String feedbackStatus;

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getFeedbackStatus() {
		return feedbackStatus;
	}

	public void setFeedbackStatus(String feedbackStatus) {
		this.feedbackStatus = feedbackStatus;
	}

	@Override
	public String toString() {
		return "AttendeeFeedbackStatusBean [empId=" + empId + ", empName="
				+ empName + ", feedbackStatus=" + feedbackStatus + "]";
	}

}
