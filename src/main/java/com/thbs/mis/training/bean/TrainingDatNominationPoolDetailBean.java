/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingDatNominationPoolDetailBean.java    		 */
/*                                                                   */
/*  Author      :  Rajesh Kumar S		                             */
/*                                                                   */
/*  Date        :  15-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Training Dat Nomination Pool Detail.             */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date              Who           Version         Comments     	 */
/*-------------------------------------------------------------------*/
/* 14-Nov-2016    Rajesh Kumar S    1.0      Initial version created */
/*********************************************************************/
package com.thbs.mis.training.bean;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;
import com.thbs.mis.framework.customValidation.annotation.training.Training_Program_Id_Not_Found;

public class TrainingDatNominationPoolDetailBean {

	private String rejectedComments;

    @Min(1)
    @Max(8)
	@NotNull(message = "Nomination Status field can not be null")
	@NumberFormat(style = Style.NUMBER)
	private Byte nominationStatus;

	@Min(1)
	@NotNull(message = "Training Program ID field can not be null")
	@NumberFormat(style = Style.NUMBER)
	@Training_Program_Id_Not_Found
	private Integer fkTrainingProgrammeId;

	@Min(1)
	@NotNull(message = "Nominated Emp ID field can not be null")
	@NumberFormat(style = Style.NUMBER)
	@Employee_Id_Not_Found
	private Integer fkNominatedEmpId;

	@Min(1)
	@NotNull(message = "ApproverEmpId field can not be null")
	@NumberFormat(style = Style.NUMBER)
	@Employee_Id_Not_Found
	private Integer fkApproverEmpId;

	public Byte getNominationStatus() {
		return nominationStatus;
	}

	public void setNominationStatus(Byte nominationStatus) {
		this.nominationStatus = nominationStatus;
	}

	public Integer getFkTrainingProgrammeId() {
		return fkTrainingProgrammeId;
	}

	public void setFkTrainingProgrammeId(Integer fkTrainingProgrammeId) {
		this.fkTrainingProgrammeId = fkTrainingProgrammeId;
	}

	public Integer getFkNominatedEmpId() {
		return fkNominatedEmpId;
	}

	public void setFkNominatedEmpId(Integer fkNominatedEmpId) {
		this.fkNominatedEmpId = fkNominatedEmpId;
	}

	public Integer getFkApproverEmpId() {
		return fkApproverEmpId;
	}

	public void setFkApproverEmpId(Integer fkApproverEmpId) {
		this.fkApproverEmpId = fkApproverEmpId;
	}

	public String getRejectedComments() {
		return rejectedComments;
	}

	public void setRejectedComments(String rejectedComments) {
		this.rejectedComments = rejectedComments;
	}

	@Override
	public String toString() {
		return "TrainingDatNominationPoolDetailBean [rejectedComments="
				+ rejectedComments + ", nominationStatus=" + nominationStatus
				+ ", fkTrainingProgrammeId=" + fkTrainingProgrammeId
				+ ", fkNominatedEmpId=" + fkNominatedEmpId
				+ ", fkApproverEmpId=" + fkApproverEmpId + "]";
	}

}
