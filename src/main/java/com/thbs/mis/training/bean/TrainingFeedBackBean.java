/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingFeedBackBean.java                   		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Training feed Back.                              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.bean;

import java.util.Date;

public class TrainingFeedBackBean
{
	private Date fromDate;
	
	private Date toDate;
	
	private int attendeesEmpID;
	
	private int trainingProgrammeId;
	
	private int attendedTraining;

	public Date getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(Date fromDate)
	{
		this.fromDate = fromDate;
	}

	public Date getToDate()
	{
		return toDate;
	}

	public void setToDate(Date toDate)
	{
		this.toDate = toDate;
	}

	public int getAttendeesEmpID()
	{
		return attendeesEmpID;
	}

	public void setAttendeesEmpID(int attendeesEmpID)
	{
		this.attendeesEmpID = attendeesEmpID;
	}

	public int getTrainingProgrammeId()
	{
		return trainingProgrammeId;
	}

	public void setTrainingProgrammeId(int trainingProgrammeId)
	{
		this.trainingProgrammeId = trainingProgrammeId;
	}

	public int getAttendedTraining()
	{
		return attendedTraining;
	}

	public void setAttendedTraining(int attendedTraining)
	{
		this.attendedTraining = attendedTraining;
	}

	@Override
	public String toString()
	{
		return "TrainingFeedBackBean [fromDate=" + fromDate
				+ ", toDate=" + toDate + ", attendeesEmpID="
				+ attendeesEmpID + ", trainingProgrammeId="
				+ trainingProgrammeId + ", attendedTraining="
				+ attendedTraining + "]";
	}
	
	
	
}
