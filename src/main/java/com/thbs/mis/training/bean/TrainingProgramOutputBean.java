/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  TrainingProgramOutputBean.java                                                        */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/


package com.thbs.mis.training.bean;

public class TrainingProgramOutputBean {
	private String trainingTitle;
	private String briefContent;
	private String venue;
	private String trainer;
    private Integer progId;
	private Integer empId;
	
	
	
	public String getTrainingTitle() {
		return trainingTitle;
	}
	public void setTrainingTitle(String trainingTitle) {
		this.trainingTitle = trainingTitle;
	}
	public String getBriefContent() {
		return briefContent;
	}
	public void setBriefContent(String briefContent) {
		this.briefContent = briefContent;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getTrainer() {
		return trainer;
	}
	
	public Integer getProgId() {
		return progId;
	}
	public void setProgId(Integer progId) {
		this.progId = progId;
	}
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public void setTrainer(String trainer) {
		this.trainer = trainer;
	}
	@Override
	public String toString() {
		return "TrainingProgramOutputBean [trainingTitle=" + trainingTitle
				+ ", briefContent=" + briefContent + ", venue=" + venue
				+ ", trainer=" + trainer + ", progId=" + progId + ", empId="
				+ empId + "]";
	}
	
 
}
