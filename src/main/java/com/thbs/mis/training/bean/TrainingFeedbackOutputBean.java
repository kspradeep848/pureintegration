/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  TrainingFeedbackOutputBean.java                                                        */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/
package com.thbs.mis.training.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class TrainingFeedbackOutputBean {

	private String briefTrainingContent;
	private String title;
	private int feedbackStatus;
	private int progId;

	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date programStartDate;

	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date programModifiedDate;
	
	private Long dateDifferenceNow;
	
	public Long getDateDifferenceNow() {
		return dateDifferenceNow;
	}

	public void setDateDifferenceNow(Long dateDifferenceNow) {
		this.dateDifferenceNow = dateDifferenceNow;
	}

	public Date getProgramModifiedDate() {
		return programModifiedDate;
	}

	public void setProgramModifiedDate(Date programModifiedDate) {
		this.programModifiedDate = programModifiedDate;
	}

	public int getProgId() {
		return progId;
	}

	public void setProgId(int progId) {
		this.progId = progId;
	}

	public Date getProgramStartDate() {
		return programStartDate;
	}

	public void setProgramStartDate(Date programStartDate) {
		this.programStartDate = programStartDate;
	}

	public String getBriefTrainingContent() {
		return briefTrainingContent;
	}

	public void setBriefTrainingContent(String briefTrainingContent) {
		this.briefTrainingContent = briefTrainingContent;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getFeedbackStatus() {
		return feedbackStatus;
	}

	public void setFeedbackStatus(int feedbackStatus) {
		this.feedbackStatus = feedbackStatus;
	}

	@Override
	public String toString() {
		return "TrainingFeedbackOutputBean [briefTrainingContent="
				+ briefTrainingContent + ", title=" + title
				+ ", feedbackStatus=" + feedbackStatus + ", progId=" + progId
				+ ", programStartDate=" + programStartDate
				+ ", programModifiedDate=" + programModifiedDate
				+ ", dateDifferenceNow=" + dateDifferenceNow + "]";
	}


}
