/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  FeedbackAnswerBean.java                                                                      */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/


package com.thbs.mis.training.bean;

public class FeedbackAnswerBean {

	private int empId;
	private String empName;
	private String empEmailId;
	private String feedbackAnswer;

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpEmailId() {
		return empEmailId;
	}

	public void setEmpEmailId(String empEmailId) {
		this.empEmailId = empEmailId;
	}

	public String getFeedbackAnswer() {
		return feedbackAnswer;
	}

	public void setFeedbackAnswer(String feedbackAnswer) {
		this.feedbackAnswer = feedbackAnswer;
	}

	@Override
	public String toString() {
		return "FeedbackAnswerBean [empId=" + empId + ", empName=" + empName
				+ ", empEmailId=" + empEmailId + ", feedbackAnswer="
				+ feedbackAnswer + "]";
	}

}
