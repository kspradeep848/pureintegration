/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingManagerViewPostFeedbackBean.java          */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  17-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details for the           */
/*                 	TrainingManagerViewPostFeedbackBean.             */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 17-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/


package com.thbs.mis.training.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class TrainingManagerViewPostFeedbackBean {
	
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date startDate;
	private String title;
	private String description;
	private String trainerName;
	private String overallRating;
	private String feedbackStatus;
	private int progId;
	private int mgrId;
	private String mgrName;
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTrainerName() {
		return trainerName;
	}
	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}
	public String getOverallRating() {
		return overallRating;
	}
	public void setOverallRating(String overallRating) {
		this.overallRating = overallRating;
	}
	public String getFeedbackStatus() {
		return feedbackStatus;
	}
	public void setFeedbackStatus(String feedbackStatus) {
		this.feedbackStatus = feedbackStatus;
	}
	public int getProgId() {
		return progId;
	}
	public void setProgId(int progId) {
		this.progId = progId;
	}
	public int getMgrId() {
		return mgrId;
	}
	public void setMgrId(int mgrId) {
		this.mgrId = mgrId;
	}
	public String getMgrName() {
		return mgrName;
	}
	public void setMgrName(String mgrName) {
		this.mgrName = mgrName;
	}
	@Override
	public String toString() {
		return "TrainingManagerViewPostFeedbackBean [startDate=" + startDate
				+ ", title=" + title + ", description=" + description
				+ ", trainerName=" + trainerName + ", overallRating="
				+ overallRating + ", feedbackStatus=" + feedbackStatus
				+ ", progId=" + progId + ", mgrId=" + mgrId + ", mgrName="
				+ mgrName + "]";
	}
}
