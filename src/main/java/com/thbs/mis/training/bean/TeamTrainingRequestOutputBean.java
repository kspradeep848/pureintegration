/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TeamTrainingRequestOutputBean.java                */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the fields                */
/*                 used for the required BO                          */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016     THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomDateSerializer;

public class TeamTrainingRequestOutputBean {
	
	private Date trainingStartDateTime; //TrainingDatProgrammeDetailBO
	
	private Integer maximumSeats;		//TrainingDatProgrammeDetailBO
	
	private String trainingTopicName;	//mas_training_topic_name
	
	private String briefTrainingContent; //TrainingDatRequestDetailBO
	
	private String empFullName;   		//DatEmpPersonalDetailBO
	
	private byte nominationStatusId;		//TrainingDatNominationPoolDetailBO
	
	private int pkTrainingNominationPoolDetailId;
	
	private int programId;
	
	private Integer nominatedEmpId;
	
	private Integer approverEmpId;
	
	private String nominationStatusName;
	
	private Integer trainingReqId;
 
	public Integer getTrainingReqId() {
		return trainingReqId;
	}

	public void setTrainingReqId(Integer trainingReqId) {
		this.trainingReqId = trainingReqId;
	}

	public Integer getApproverEmpId() {
		return approverEmpId;
	}

	public void setApproverEmpId(Integer approverEmpId) {
		this.approverEmpId = approverEmpId;
	}

	public Integer getNominatedEmpId() {
		return nominatedEmpId;
	}

	public void setNominatedEmpId(Integer nominatedEmpId) {
		this.nominatedEmpId = nominatedEmpId;
	}

	private int topicId;

	public int getTopicId() {
		return topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public int getProgramId() {
		return programId;
	}

	public void setProgramId(int programId) {
		this.programId = programId;
	}

	public int getPkTrainingNominationPoolDetailId() {
		return pkTrainingNominationPoolDetailId;
	}

	public void setPkTrainingNominationPoolDetailId(
			int pkTrainingNominationPoolDetailId) {
		this.pkTrainingNominationPoolDetailId = pkTrainingNominationPoolDetailId;
	}

	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getTrainingStartDateTime() {
		return trainingStartDateTime;
	}

	public void setTrainingStartDateTime(Date trainingStartDateTime) {
		this.trainingStartDateTime = trainingStartDateTime;
	}

	public Integer getMaximumSeats() {
		return maximumSeats;
	}

	public void setMaximumSeats(Integer maximumSeats) {
		this.maximumSeats = maximumSeats;
	}

	public String getTrainingTopicName() {
		return trainingTopicName;
	}

	public void setTrainingTopicName(String trainingTopicName) {
		this.trainingTopicName = trainingTopicName;
	}

	public String getBriefTrainingContent() {
		return briefTrainingContent;
	}

	public void setBriefTrainingContent(String briefTrainingContent) {
		this.briefTrainingContent = briefTrainingContent;
	}

	public String getEmpFullName() {
		return empFullName;
	}

	public void setEmpFullName(String empFullName) {
		this.empFullName = empFullName;
	}

	public byte getNominationStatusId() {
		return nominationStatusId;
	}

	public void setNominationStatusId(byte nominationStatusId) {
		this.nominationStatusId = nominationStatusId;
	}

	public String getNominationStatusName() {
		return nominationStatusName;
	}

	public void setNominationStatusName(String nominationStatusName) {
		this.nominationStatusName = nominationStatusName;
	}

	@Override
	public String toString() {
		return "TeamTrainingRequestOutputBean [trainingStartDateTime="
				+ trainingStartDateTime + ", maximumSeats=" + maximumSeats
				+ ", trainingTopicName=" + trainingTopicName
				+ ", briefTrainingContent=" + briefTrainingContent
				+ ", empFullName=" + empFullName + ", nominationStatusId="
				+ nominationStatusId + ", pkTrainingNominationPoolDetailId="
				+ pkTrainingNominationPoolDetailId + ", programId=" + programId
				+ ", nominatedEmpId=" + nominatedEmpId + ", approverEmpId="
				+ approverEmpId + ", nominationStatusName="
				+ nominationStatusName + ", topicId=" + topicId + "]";
	}

	 

	 
	 

}
