package com.thbs.mis.training.bean;

import java.sql.Time;
import java.util.Date;

public class TrainingRequestDetailBean {

	private int trainingRequestId;
	private Integer requestorEmpId;
	private String requestorName;
	private int topicId;
	private String topicName;
	private String levelOfTrainingRequest;
	private String productVersion;
	private int noOfParticipants;
	private String levelOfParticipants;
	private String usageOfProduct;
	private String briefTrainingContent;
	private String isSoftwareLicenseAvailable;
	private Date expectedTrainingStartDate;
	private Time preferredTrainingStartTime;
	private Time preferredTrainingEndTime;
	private String externalTrainerRequest;
	private String specificInputComments;
	private Date trainingRequestedDate;
	private Date trainingModifiedDate;
	private int requestStatusId;
	private String requestStatusName;
	private String comments;
	private String isProgramCreated;

	public int getTrainingRequestId() {
		return trainingRequestId;
	}

	public void setTrainingRequestId(int trainingRequestId) {
		this.trainingRequestId = trainingRequestId;
	}

	public Integer getRequestorEmpId() {
		return requestorEmpId;
	}

	public void setRequestorEmpId(Integer requestorEmpId) {
		this.requestorEmpId = requestorEmpId;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public int getTopicId() {
		return topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getLevelOfTrainingRequest() {
		return levelOfTrainingRequest;
	}

	public void setLevelOfTrainingRequest(String levelOfTrainingRequest) {
		this.levelOfTrainingRequest = levelOfTrainingRequest;
	}

	public String getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	public int getNoOfParticipants() {
		return noOfParticipants;
	}

	public void setNoOfParticipants(int noOfParticipants) {
		this.noOfParticipants = noOfParticipants;
	}

	public String getLevelOfParticipants() {
		return levelOfParticipants;
	}

	public void setLevelOfParticipants(String levelOfParticipants) {
		this.levelOfParticipants = levelOfParticipants;
	}

	public String getUsageOfProduct() {
		return usageOfProduct;
	}

	public void setUsageOfProduct(String usageOfProduct) {
		this.usageOfProduct = usageOfProduct;
	}

	public String getBriefTrainingContent() {
		return briefTrainingContent;
	}

	public void setBriefTrainingContent(String briefTrainingContent) {
		this.briefTrainingContent = briefTrainingContent;
	}

	public String getIsSoftwareLicenseAvailable() {
		return isSoftwareLicenseAvailable;
	}

	public void setIsSoftwareLicenseAvailable(String isSoftwareLicenseAvailable) {
		this.isSoftwareLicenseAvailable = isSoftwareLicenseAvailable;
	}

	public Date getExpectedTrainingStartDate() {
		return expectedTrainingStartDate;
	}

	public void setExpectedTrainingStartDate(Date expectedTrainingStartDate) {
		this.expectedTrainingStartDate = expectedTrainingStartDate;
	}

	public Time getPreferredTrainingStartTime() {
		return preferredTrainingStartTime;
	}

	public void setPreferredTrainingStartTime(Time preferredTrainingStartTime) {
		this.preferredTrainingStartTime = preferredTrainingStartTime;
	}

	public Time getPreferredTrainingEndTime() {
		return preferredTrainingEndTime;
	}

	public void setPreferredTrainingEndTime(Time preferredTrainingEndTime) {
		this.preferredTrainingEndTime = preferredTrainingEndTime;
	}

	public String getExternalTrainerRequest() {
		return externalTrainerRequest;
	}

	public void setExternalTrainerRequest(String externalTrainerRequest) {
		this.externalTrainerRequest = externalTrainerRequest;
	}

	public String getSpecificInputComments() {
		return specificInputComments;
	}

	public void setSpecificInputComments(String specificInputComments) {
		this.specificInputComments = specificInputComments;
	}

	public Date getTrainingRequestedDate() {
		return trainingRequestedDate;
	}

	public void setTrainingRequestedDate(Date trainingRequestedDate) {
		this.trainingRequestedDate = trainingRequestedDate;
	}

	public Date getTrainingModifiedDate() {
		return trainingModifiedDate;
	}

	public void setTrainingModifiedDate(Date trainingModifiedDate) {
		this.trainingModifiedDate = trainingModifiedDate;
	}

	public int getRequestStatusId() {
		return requestStatusId;
	}

	public void setRequestStatusId(int requestStatusId) {
		this.requestStatusId = requestStatusId;
	}

	public String getRequestStatusName() {
		return requestStatusName;
	}

	public void setRequestStatusName(String requestStatusName) {
		this.requestStatusName = requestStatusName;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getIsProgramCreated() {
		return isProgramCreated;
	}

	public void setIsProgramCreated(String isProgramCreated) {
		this.isProgramCreated = isProgramCreated;
	}

	@Override
	public String toString() {
		return "TrainingRequestDetailBean [trainingRequestId="
				+ trainingRequestId + ", requestorEmpId=" + requestorEmpId
				+ ", requestorName=" + requestorName + ", topicId=" + topicId
				+ ", topicName=" + topicName + ", levelOfTrainingRequest="
				+ levelOfTrainingRequest + ", productVersion=" + productVersion
				+ ", noOfParticipants=" + noOfParticipants
				+ ", levelOfParticipants=" + levelOfParticipants
				+ ", usageOfProduct=" + usageOfProduct
				+ ", briefTrainingContent=" + briefTrainingContent
				+ ", isSoftwareLicenseAvailable=" + isSoftwareLicenseAvailable
				+ ", expectedTrainingStartDate=" + expectedTrainingStartDate
				+ ", preferredTrainingStartTime=" + preferredTrainingStartTime
				+ ", preferredTrainingEndTime=" + preferredTrainingEndTime
				+ ", externalTrainerRequest=" + externalTrainerRequest
				+ ", specificInputComments=" + specificInputComments
				+ ", trainingRequestedDate=" + trainingRequestedDate
				+ ", trainingModifiedDate=" + trainingModifiedDate
				+ ", requestStatusId=" + requestStatusId
				+ ", requestStatusName=" + requestStatusName + ", comments="
				+ comments + ", isProgramCreated=" + isProgramCreated + "]";
	}
}
