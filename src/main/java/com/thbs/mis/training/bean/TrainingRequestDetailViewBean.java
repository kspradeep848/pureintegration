/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingRequestDetailViewBean.java                */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                Training Request Detail View                       */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016     THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.bean;

import java.sql.Time;
import java.util.Date;

public class TrainingRequestDetailViewBean {

	private String trainingTopicName;
	private String levelOfTraining;
	private String versionNumber;
	private int noOfParticipants;
	private String usageOfProduct;
	private String briefCourseContent;
	private String levelOfParticipants;
	private String isSoftwareLicensesAvailable;
	private String externalTrainerRequest;
	private Date preferredStartDate;
	private Time preferredStartTime;
	private Time preferredEndTime;
	private String specificInputs;
	private Integer requestId;
	private String requestorName;
	private int trainingTopicId;
	private int statusId;
	private String statusName;
	private Integer requestorId;
	private Integer noOfLicenseAvailable;
	private int usageOfProductId;
	private int levelOfTrainingId;
	private String isProgramCreated;
	private Byte programCategoryId;
	private String programCategoryName;

	public Byte getProgramCategoryId() {
		return programCategoryId;
	}

	public void setProgramCategoryId(Byte programCategoryId) {
		this.programCategoryId = programCategoryId;
	}

	public String getProgramCategoryName() {
		return programCategoryName;
	}

	public void setProgramCategoryName(String programCategoryName) {
		this.programCategoryName = programCategoryName;
	}

	public String getIsProgramCreated() {
		return isProgramCreated;
	}

	public void setIsProgramCreated(String isProgramCreated) {
		this.isProgramCreated = isProgramCreated;
	}

	public int getUsageOfProductId() {
		return usageOfProductId;
	}

	public void setUsageOfProductId(int usageOfProductId) {
		this.usageOfProductId = usageOfProductId;
	}

	public int getLevelOfTrainingId() {
		return levelOfTrainingId;
	}

	public void setLevelOfTrainingId(int levelOfTrainingId) {
		this.levelOfTrainingId = levelOfTrainingId;
	}

	public Integer getNoOfLicenseAvailable() {
		return noOfLicenseAvailable;
	}

	public void setNoOfLicenseAvailable(Integer noOfLicenseAvailable) {
		this.noOfLicenseAvailable = noOfLicenseAvailable;
	}

	public Integer getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(Integer requestorId) {
		this.requestorId = requestorId;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public int getTrainingTopicId() {
		return trainingTopicId;
	}

	public void setTrainingTopicId(int trainingTopicId) {
		this.trainingTopicId = trainingTopicId;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getTrainingTopicName() {
		return trainingTopicName;
	}

	public void setTrainingTopicName(String trainingTopicName) {
		this.trainingTopicName = trainingTopicName;
	}

	public String getLevelOfTraining() {
		return levelOfTraining;
	}

	public void setLevelOfTraining(String levelOfTraining) {
		this.levelOfTraining = levelOfTraining;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public int getNoOfParticipants() {
		return noOfParticipants;
	}

	public void setNoOfParticipants(int noOfParticipants) {
		this.noOfParticipants = noOfParticipants;
	}

	public String getUsageOfProduct() {
		return usageOfProduct;
	}

	public void setUsageOfProduct(String usageOfProduct) {
		this.usageOfProduct = usageOfProduct;
	}

	public String getBriefCourseContent() {
		return briefCourseContent;
	}

	public void setBriefCourseContent(String briefCourseContent) {
		this.briefCourseContent = briefCourseContent;
	}

	public String getLevelOfParticipants() {
		return levelOfParticipants;
	}

	public void setLevelOfParticipants(String levelOfParticipants) {
		this.levelOfParticipants = levelOfParticipants;
	}

	public String getIsSoftwareLicensesAvailable() {
		return isSoftwareLicensesAvailable;
	}

	public void setIsSoftwareLicensesAvailable(
			String isSoftwareLicensesAvailable) {
		this.isSoftwareLicensesAvailable = isSoftwareLicensesAvailable;
	}

	public String getExternalTrainerRequest() {
		return externalTrainerRequest;
	}

	public void setExternalTrainerRequest(String externalTrainerRequest) {
		this.externalTrainerRequest = externalTrainerRequest;
	}

	public Date getPreferredStartDate() {
		return preferredStartDate;
	}

	public void setPreferredStartDate(Date preferredStartDate) {
		this.preferredStartDate = preferredStartDate;
	}

	public Time getPreferredStartTime() {
		return preferredStartTime;
	}

	public void setPreferredStartTime(Time preferredStartTime) {
		this.preferredStartTime = preferredStartTime;
	}

	public Time getPreferredEndTime() {
		return preferredEndTime;
	}

	public void setPreferredEndTime(Time preferredEndTime) {
		this.preferredEndTime = preferredEndTime;
	}

	public String getSpecificInputs() {
		return specificInputs;
	}

	public void setSpecificInputs(String specificInputs) {
		this.specificInputs = specificInputs;
	}

	@Override
	public String toString() {
		return "TrainingRequestDetailViewBean [trainingTopicName="
				+ trainingTopicName + ", levelOfTraining=" + levelOfTraining
				+ ", versionNumber=" + versionNumber + ", noOfParticipants="
				+ noOfParticipants + ", usageOfProduct=" + usageOfProduct
				+ ", briefCourseContent=" + briefCourseContent
				+ ", levelOfParticipants=" + levelOfParticipants
				+ ", isSoftwareLicensesAvailable="
				+ isSoftwareLicensesAvailable + ", externalTrainerRequest="
				+ externalTrainerRequest + ", preferredStartDate="
				+ preferredStartDate + ", preferredStartTime="
				+ preferredStartTime + ", preferredEndTime=" + preferredEndTime
				+ ", specificInputs=" + specificInputs + ", requestId="
				+ requestId + ", requestorName=" + requestorName
				+ ", trainingTopicId=" + trainingTopicId + ", statusId="
				+ statusId + ", statusName=" + statusName + ", requestorId="
				+ requestorId + ", noOfLicenseAvailable="
				+ noOfLicenseAvailable + ", usageOfProductId="
				+ usageOfProductId + ", levelOfTrainingId=" + levelOfTrainingId
				+ ", isProgramCreated=" + isProgramCreated
				+ ", programCategoryId=" + programCategoryId
				+ ", programCategoryName=" + programCategoryName + "]";
	}

}
