/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingProgrammeDetailInputBean.java              */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Training feed Back.                              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

public class TrainingProgrammeDetailInputBean {

	private Integer pkTrainingProgrammeId;

	private Integer maximumSeats;

	private Date trainingStartDateTime;

	private Integer fkTrainingRequestId;
	
	private String trainerName;
	
	private Date programModifiedDate;
	
	public Date getProgramModifiedDate() {
		return programModifiedDate;
	}

	public void setProgramModifiedDate(Date programModifiedDate) {
		this.programModifiedDate = programModifiedDate;
	}

	public String getTrainerName() {
		return trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	TrainingRequestDetailInputBean trainingRequestDetailInputBean;

	public TrainingRequestDetailInputBean getTrainingRequestDetailInputBean() {
		return trainingRequestDetailInputBean;
	}

	public void setTrainingRequestDetailInputBean(
			TrainingRequestDetailInputBean trainingRequestDetailInputBean) {
		this.trainingRequestDetailInputBean = trainingRequestDetailInputBean;
	}

	public Integer getPkTrainingProgrammeId() {
		return pkTrainingProgrammeId;
	}

	public void setPkTrainingProgrammeId(Integer pkTrainingProgrammeId) {
		this.pkTrainingProgrammeId = pkTrainingProgrammeId;
	}

	public Integer getMaximumSeats() {
		return maximumSeats;
	}

	public void setMaximumSeats(Integer maximumSeats) {
		this.maximumSeats = maximumSeats;
	}

	public Date getTrainingStartDateTime() {
		return trainingStartDateTime;
	}

	public void setTrainingStartDateTime(Date trainingStartDateTime) {
		this.trainingStartDateTime = trainingStartDateTime;
	}

	public Integer getFkTrainingRequestId() {
		return fkTrainingRequestId;
	}

	public void setFkTrainingRequestId(Integer fkTrainingRequestId) {
		this.fkTrainingRequestId = fkTrainingRequestId;
	}

	@Override
	public String toString() {
		return "TrainingProgrammeDetailInputBean [pkTrainingProgrammeId="
				+ pkTrainingProgrammeId + ", maximumSeats=" + maximumSeats
				+ ", trainingStartDateTime=" + trainingStartDateTime
				+ ", fkTrainingRequestId=" + fkTrainingRequestId
				+ ", trainerName=" + trainerName + ", programModifiedDate="
				+ programModifiedDate + ", trainingRequestDetailInputBean="
				+ trainingRequestDetailInputBean + "]";
	}


	
	

}
