/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  TrainingCreateAttendeesFeedbackBean.java                                 */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/



package com.thbs.mis.training.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;
import com.thbs.mis.framework.customValidation.annotation.training.Training_Program_Id_Not_Found;


public class TrainingCreateAttendeesFeedbackBean {

	@Training_Program_Id_Not_Found
    @Min(1)
	@NotNull(message="Programme Id cannot be null")
	private int trainingDatProgrammeId;
    
    @Employee_Id_Not_Found
    @Min(1)
	@NotNull(message="Attendee Employee Id cannot be null")
	private int attendeeEmpId;
    
    @Min(1)
	@NotNull(message=" Feedback Question Id cannot be null")
	private int feedbackQuestionInfoId;
    
    @NotEmpty(message="Attendee answer cannot be empty")
    @Length(max=250 ,message="Length should be within 250 characters")
    @NotNull(message="Attendee answer should not be null")
	private String attendeeAnswer;
    
	
	public int getTrainingDatProgrammeId() {
		return trainingDatProgrammeId;
	}
	public void setTrainingDatProgrammeId(int trainingDatProgrammeId) {
		this.trainingDatProgrammeId = trainingDatProgrammeId;
	}
	public int getAttendeeEmpId() {
		return attendeeEmpId;
	}
	public void setAttendeeEmpId(int attendeeEmpId) {
		this.attendeeEmpId = attendeeEmpId;
	}
	public int getFeedbackQuestionInfoId() {
		return feedbackQuestionInfoId;
	}
	public void setFeedbackQuestionInfoId(int feedbackQuestionInfoId) {
		this.feedbackQuestionInfoId = feedbackQuestionInfoId;
	}
	public String getAttendeeAnswer() {
		return attendeeAnswer;
	}
	public void setAttendeeAnswer(String attendeeAnswer) {
		this.attendeeAnswer = attendeeAnswer;
	}
	
	
	@Override
	public String toString() {
		return "TrainingCreateAttendeesFeedbackBean [trainingDatProgrammeId="
				+ trainingDatProgrammeId + ", attendeeEmpId=" + attendeeEmpId
				+ ", feedbackQuestionInfoId=" + feedbackQuestionInfoId
				+ ", attendeeAnswer=" + attendeeAnswer + "]";
	}
	
}
