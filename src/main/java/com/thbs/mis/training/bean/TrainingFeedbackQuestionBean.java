/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingFeedbackQuestionBean.java                 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  17-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	TrainingFeedbackQuestionBean.                    */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 17-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class TrainingFeedbackQuestionBean {
    @Min(1)
	@NotNull(message = "feedbackQuestionStatus should not be null")
	@NumberFormat(style = Style.NUMBER)
	private Byte feedbackQuestionStatus;

	@Min(1)
	@NotNull(message = "fkMasWorkFlowId should not be null")
	@NumberFormat(style = Style.NUMBER)
	private Short fkMasWorkFlowId;

	public Byte getFeedbackQuestionStatus() {
		return feedbackQuestionStatus;
	}

	public void setFeedbackQuestionStatus(Byte feedbackQuestionStatus) {
		this.feedbackQuestionStatus = feedbackQuestionStatus;
	}

	public Short getFkMasWorkFlowId() {
		return fkMasWorkFlowId;
	}

	public void setFkMasWorkFlowId(Short fkMasWorkFlowId) {
		this.fkMasWorkFlowId = fkMasWorkFlowId;
	}

	@Override
	public String toString() {
		return "TrainingFeedbackQuestionBean [feedbackQuestionStatus="
				+ feedbackQuestionStatus + ", fkMasWorkFlowId="
				+ fkMasWorkFlowId + "]";
	}

}
