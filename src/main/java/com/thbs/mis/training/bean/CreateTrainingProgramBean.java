/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  CreateTrainingProgramBean.java                   		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO	                              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;
import com.thbs.mis.framework.customValidation.annotation.training.Training_Program_Status_Not_Found;
import com.thbs.mis.framework.customValidation.annotation.training.Training_Request_Id_Not_Found;
import com.thbs.mis.framework.customValidation.annotation.training.Training_Room_Id_Not_Found;
import com.thbs.mis.framework.util.TimeDeserializer;

/**
 * @author kamal_anand
 *
 */
public class CreateTrainingProgramBean {

	private int pkTrainingProgramId;

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Training Request Id should be a numeric value")
	@Training_Request_Id_Not_Found
	private int trainingRequestId;

	@NotNull(message = "Training Start Date field can not be null")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd HH:mm:SSSS")
	@JsonDeserialize(using = TimeDeserializer.class)
	private Date trainingStartDate;

	@NotNull(message = "Training End Date field can not be null")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd HH:mm:SSSS")
	@JsonDeserialize(using = TimeDeserializer.class)
	private Date trainingEndDate;

	/*@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Training Duration in Hours should be a numeric value")*/
	private int trainingDurationInHrs;

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Training Duration in Days should be a numeric value")
	private int trainingDurationInDays;

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Training Number of Hours Per Day should be a numeric value")
	private int trainingNoOfHrsPerDay;

	@NotEmpty(message = "Trainer type field cannot be empty")
	@NotNull(message = "Trainer type field cannot be null")
	private String trainerType;

	@NotEmpty(message = "Trainer Name cannot be empty")
	@NotNull(message = "Trainer Name cannot be null")
	@Length(max = 250, message = "Trainer Name can't exceed 250 chars")
	private String trainerName;

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Max seats should be a numeric value")
	private int maxSeats;

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Training room Id should be a numeric value")
	@Training_Room_Id_Not_Found
	private short trainingRoomId;

	@Length(max = 250, message = "External Venue Name can't exceed 250 chars")
	private String externalVenueName;

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Training Program Status should be a numeric value")
	@Training_Program_Status_Not_Found
	private byte programStatus;

	@Min(0)
	@Max(2)
	@NumberFormat(style = Style.NUMBER, pattern = "Training Intimated Status should be a numeric value")
	@NotNull(message = "Trainer Intimation Status cannot be null")
	private Byte trainingIntimatedStatus;

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Training Program Created By should be a numeric value")
	@Employee_Id_Not_Found
	private int programCreatedBy;

	@Length(max = 300, message = "Comments can't exceed 300 chars")
	private String comments;

	public int getPkTrainingProgramId() {
		return pkTrainingProgramId;
	}

	public void setPkTrainingProgramId(int pkTrainingProgramId) {
		this.pkTrainingProgramId = pkTrainingProgramId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getTrainingRequestId() {
		return trainingRequestId;
	}

	public void setTrainingRequestId(int trainingRequestId) {
		this.trainingRequestId = trainingRequestId;
	}

	public Date getTrainingStartDate() {
		return trainingStartDate;
	}

	public void setTrainingStartDate(Date trainingStartDate) {
		this.trainingStartDate = trainingStartDate;
	}

	public Date getTrainingEndDate() {
		return trainingEndDate;
	}

	public void setTrainingEndDate(Date trainingEndDate) {
		this.trainingEndDate = trainingEndDate;
	}

	public int getTrainingDurationInHrs() {
		return trainingDurationInHrs;
	}

	public void setTrainingDurationInHrs(int trainingDurationInHrs) {
		this.trainingDurationInHrs = trainingDurationInHrs;
	}

	public int getTrainingDurationInDays() {
		return trainingDurationInDays;
	}

	public void setTrainingDurationInDays(int trainingDurationInDays) {
		this.trainingDurationInDays = trainingDurationInDays;
	}

	public int getTrainingNoOfHrsPerDay() {
		return trainingNoOfHrsPerDay;
	}

	public void setTrainingNoOfHrsPerDay(int trainingNoOfHrsPerDay) {
		this.trainingNoOfHrsPerDay = trainingNoOfHrsPerDay;
	}

	public String getTrainerType() {
		return trainerType;
	}

	public void setTrainerType(String trainerType) {
		this.trainerType = trainerType;
	}

	public String getTrainerName() {
		return trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	public int getMaxSeats() {
		return maxSeats;
	}

	public void setMaxSeats(int maxSeats) {
		this.maxSeats = maxSeats;
	}

	public short getTrainingRoomId() {
		return trainingRoomId;
	}

	public void setTrainingRoomId(short trainingRoomId) {
		this.trainingRoomId = trainingRoomId;
	}

	public String getExternalVenueName() {
		return externalVenueName;
	}

	public void setExternalVenueName(String externalVenueName) {
		this.externalVenueName = externalVenueName;
	}

	public byte getProgramStatus() {
		return programStatus;
	}

	public void setProgramStatus(byte programStatus) {
		this.programStatus = programStatus;
	}

	public int getProgramCreatedBy() {
		return programCreatedBy;
	}

	public void setProgramCreatedBy(int programCreatedBy) {
		this.programCreatedBy = programCreatedBy;
	}

	public Byte getTrainingIntimatedStatus() {
		return trainingIntimatedStatus;
	}

	public void setTrainingIntimatedStatus(Byte trainingIntimatedStatus) {
		this.trainingIntimatedStatus = trainingIntimatedStatus;
	}

	@Override
	public String toString() {
		return "CreateTrainingProgramBean [pkTrainingProgramId="
				+ pkTrainingProgramId + ", trainingRequestId="
				+ trainingRequestId + ", trainingStartDate="
				+ trainingStartDate + ", trainingEndDate=" + trainingEndDate
				+ ", trainingDurationInHrs=" + trainingDurationInHrs
				+ ", trainingDurationInDays=" + trainingDurationInDays
				+ ", trainingNoOfHrsPerDay=" + trainingNoOfHrsPerDay
				+ ", trainerType=" + trainerType + ", trainerName="
				+ trainerName + ", maxSeats=" + maxSeats + ", trainingRoomId="
				+ trainingRoomId + ", externalVenueName=" + externalVenueName
				+ ", programStatus=" + programStatus
				+ ", trainingIntimatedStatus=" + trainingIntimatedStatus
				+ ", programCreatedBy=" + programCreatedBy + ", comments="
				+ comments + "]";
	}

}
