/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  AddParticipantsOutputBean.java                   		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO	                              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.bean;

public class AddParticipantsOutputBean {
	
	private int empId;
	private String empName;
	private String emailId;
	private String nominationStatus;
	private int progId;
	
	public int getProgId() {
		return progId;
	}
	public void setProgId(int progId) {
		this.progId = progId;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getNominationStatus() {
		return nominationStatus;
	}
	public void setNominationStatus(String nominationStatus) {
		this.nominationStatus = nominationStatus;
	}
	@Override
	public String toString() {
		return "AddParticipantsOutputBean [empId=" + empId + ", empName="
				+ empName + ", emailId=" + emailId + ", nominationStatus="
				+ nominationStatus + ", progId=" + progId + "]";
	}
}
