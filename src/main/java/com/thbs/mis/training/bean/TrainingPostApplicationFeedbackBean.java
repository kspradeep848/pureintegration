/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingPostApplicationFeedbackBean.java          */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  November 02, 2016                                 */
/*                                                                   */
/*  Description :  This class contains for the inputs required for   */
/*                   Training Post Application Feedback details	 	 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 02, 2016     THBS     1.0        Initial version created */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class TrainingPostApplicationFeedbackBean {

	@Min(1)
	@NotNull(message = "Post Application Feedback Id cannot be null")
	private int postApplicationFeedbackId;

	@Min(1)
	@NotNull(message = "Training Programme Id cannot be null")
	private int datTrainingProgrammeId;

	@Min(1)
	@NotNull(message = "Manager Employee Id cannot be null")
	private int managerEmpId;

	@Min(1)
	@NotNull(message = " Feedback Question Id cannot be null")
	private int feedbackQuestionInfoId;

	@NotEmpty(message = "Post Application Manager Answer cannot be empty")
	@Length(max = 250, message = "Length should be within 250 characters")
	@NotNull(message = "Post Application Manager answer should not be null")
	private String postApplicationMgrAnswer;

	private List<EmployeePostApplicationFeedbackInputBean> employeeAnswerList;

	public List<EmployeePostApplicationFeedbackInputBean> getEmployeeAnswerList() {
		return employeeAnswerList;
	}

	public void setEmployeeAnswerList(
			List<EmployeePostApplicationFeedbackInputBean> employeeAnswerList) {
		this.employeeAnswerList = employeeAnswerList;
	}

	public int getPostApplicationFeedbackId() {
		return postApplicationFeedbackId;
	}

	public void setPostApplicationFeedbackId(int postApplicationFeedbackId) {
		this.postApplicationFeedbackId = postApplicationFeedbackId;
	}

	public int getDatTrainingProgrammeId() {
		return datTrainingProgrammeId;
	}

	public void setDatTrainingProgrammeId(int datTrainingProgrammeId) {
		this.datTrainingProgrammeId = datTrainingProgrammeId;
	}

	public int getManagerEmpId() {
		return managerEmpId;
	}

	public void setManagerEmpId(int managerEmpId) {
		this.managerEmpId = managerEmpId;
	}

	public int getFeedbackQuestionInfoId() {
		return feedbackQuestionInfoId;
	}

	public void setFeedbackQuestionInfoId(int feedbackQuestionInfoId) {
		this.feedbackQuestionInfoId = feedbackQuestionInfoId;
	}

	public String getPostApplicationMgrAnswer() {
		return postApplicationMgrAnswer;
	}

	public void setPostApplicationMgrAnswer(String postApplicationMgrAnswer) {
		this.postApplicationMgrAnswer = postApplicationMgrAnswer;
	}

	@Override
	public String toString() {
		return "TrainingPostApplicationFeedbackBean [postApplicationFeedbackId="
				+ postApplicationFeedbackId
				+ ", datTrainingProgrammeId="
				+ datTrainingProgrammeId
				+ ", managerEmpId="
				+ managerEmpId
				+ ", feedbackQuestionInfoId="
				+ feedbackQuestionInfoId
				+ ", postApplicationMgrAnswer="
				+ postApplicationMgrAnswer
				+ ", employeeAnswerList=" + employeeAnswerList + "]";
	}

}
