/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingRequestsViewBean.java                     */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                Training Requests  View                            */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016     THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class TrainingRequestsViewBean {

	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date trainingDate;
	private String trainingTitle;
	private String trainingDescription;
	private int noOfParticipants;
	private String status;
	private String mgrName;
	private String isProgramCreated;
	private int requestId;
	private int progId;
	private Byte programStatus;

	public Byte getProgramStatus() {
		return programStatus;
	}

	public void setProgramStatus(Byte programStatus) {
		this.programStatus = programStatus;
	}

	public int getProgId() {
		return progId;
	}

	public void setProgId(int progId) {
		this.progId = progId;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public String getIsProgramCreated() {
		return isProgramCreated;
	}

	public void setIsProgramCreated(String isProgramCreated) {
		this.isProgramCreated = isProgramCreated;
	}

	public String getMgrName() {
		return mgrName;
	}

	public void setMgrName(String mgrName) {
		this.mgrName = mgrName;
	}

	public Date getTrainingDate() {
		return trainingDate;
	}

	public void setTrainingDate(Date trainingDate) {
		this.trainingDate = trainingDate;
	}

	public String getTrainingTitle() {
		return trainingTitle;
	}

	public void setTrainingTitle(String trainingTitle) {
		this.trainingTitle = trainingTitle;
	}

	public String getTrainingDescription() {
		return trainingDescription;
	}

	public void setTrainingDescription(String trainingDescription) {
		this.trainingDescription = trainingDescription;
	}

	public int getNoOfParticipants() {
		return noOfParticipants;
	}

	public void setNoOfParticipants(int noOfParticipants) {
		this.noOfParticipants = noOfParticipants;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "TrainingRequestsViewBean [trainingDate=" + trainingDate
				+ ", trainingTitle=" + trainingTitle + ", trainingDescription="
				+ trainingDescription + ", noOfParticipants="
				+ noOfParticipants + ", status=" + status + ", mgrName="
				+ mgrName + ", isProgramCreated=" + isProgramCreated
				+ ", requestId=" + requestId + ", progId=" + progId
				+ ", programStatus=" + programStatus + "]";
	}
}
