
/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  TrainingRequestDetailInputBean.java                                                 */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/
package com.thbs.mis.training.bean;

import com.thbs.mis.common.bo.MasTrainingTopicNameBO;

public class TrainingRequestDetailInputBean {

	private int fkTrainingTopicNameId;

	private String briefTrainingContent;

	
	MasTrainingTopicNameBO masTrainingTopicNameBO;
	 

	public MasTrainingTopicNameBO getMasTrainingTopicNameBO() {
		return masTrainingTopicNameBO;
	}

	public void setMasTrainingTopicNameBO(
			MasTrainingTopicNameBO masTrainingTopicNameBO) {
		this.masTrainingTopicNameBO = masTrainingTopicNameBO;
	}

	public int getFkTrainingTopicNameId() {
		return fkTrainingTopicNameId;
	}

	public void setFkTrainingTopicNameId(int fkTrainingTopicNameId) {
		this.fkTrainingTopicNameId = fkTrainingTopicNameId;
	}

	public String getBriefTrainingContent() {
		return briefTrainingContent;
	}

	public void setBriefTrainingContent(String briefTrainingContent) {
		this.briefTrainingContent = briefTrainingContent;
	}

	
}
