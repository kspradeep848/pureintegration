/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  TrainingRequestOutputDetailBean.java                                             */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/
package com.thbs.mis.training.bean;

import java.util.Date;

public class TrainingRequestOutputDetailBean {
	private String trainingTitle;
	private Date startDate;
	private Integer noOfParticipants;
	private String trainer;
	private String venue;
	private String initiatedBy;
	private Date time;
	private String description;
	private Integer progId;
	private int progStatusId;
	private String progStatus;
	
	
	public int getProgStatusId() {
		return progStatusId;
	}
	public void setProgStatusId(int progStatusId) {
		this.progStatusId = progStatusId;
	}
	public String getProgStatus() {
		return progStatus;
	}
	public void setProgStatus(String progStatus) {
		this.progStatus = progStatus;
	}
	public Integer getProgId() {
		return progId;
	}
	public void setProgId(Integer progId) {
		this.progId = progId;
	}
	public String getTrainingTitle() {
		return trainingTitle;
	}
	public void setTrainingTitle(String trainingTitle) {
		this.trainingTitle = trainingTitle;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Integer getNoOfParticipants() {
		return noOfParticipants;
	}
	public void setNoOfParticipants(Integer noOfParticipants) {
		this.noOfParticipants = noOfParticipants;
	}
	public String getTrainer() {
		return trainer;
	}
	public void setTrainer(String trainer) {
		this.trainer = trainer;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getInitiatedBy() {
		return initiatedBy;
	}
	public void setInitiatedBy(String initiatedBy) {
		this.initiatedBy = initiatedBy;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return "TrainingRequestOutputDetailBean [trainingTitle="
				+ trainingTitle + ", startDate=" + startDate
				+ ", noOfParticipants=" + noOfParticipants + ", trainer="
				+ trainer + ", venue=" + venue + ", initiatedBy=" + initiatedBy
				+ ", time=" + time + ", description=" + description
				+ ", progId=" + progId + ", progStatusId=" + progStatusId
				+ ", progStatus=" + progStatus + "]";
	}

}
