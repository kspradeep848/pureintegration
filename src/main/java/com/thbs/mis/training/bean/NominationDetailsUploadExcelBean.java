/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  NominationDetailsUploadExcelBean.java                                         */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomDateSerializer;

public class NominationDetailsUploadExcelBean {

	Integer programId;
	Integer employeeId;
	String ateendanceIsYesOrNo;
	Integer attendanceUpdatedBy;
	Date updatedOn;

	public Integer getProgramId() {
		return programId;
	}

	public void setProgramId(Integer programId) {
		this.programId = programId;
	}

 

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

 
	public String getAteendanceIsYesOrNo() {
		return ateendanceIsYesOrNo;
	}

	public void setAteendanceIsYesOrNo(String ateendanceIsYesOrNo) {
		this.ateendanceIsYesOrNo = ateendanceIsYesOrNo;
	}

	public Integer getAttendanceUpdatedBy() {
		return attendanceUpdatedBy;
	}

	public void setAttendanceUpdatedBy(Integer attendanceUpdatedBy) {
		this.attendanceUpdatedBy = attendanceUpdatedBy;
	}

	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}


}
