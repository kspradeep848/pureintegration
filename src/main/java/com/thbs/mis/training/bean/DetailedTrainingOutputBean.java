/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  DetailedTrainingOutputBean.java                    */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the fields                */
/*                 used for the required BO                          */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016     THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class DetailedTrainingOutputBean {
	private String trainingTitle;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date startDate;
	private Integer noOfParticipants;
	private String trainer;
	private String venue;
	private String initiatedBy;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date time;
	private String nominationStatus;
	private String description;
	private Integer progId;
	private Integer requesterEmpId;
	private Byte progStatusId;
	private String progStatus;
	private TrainingRequestDetailBean requestDetails;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date endDate;
	//private String durationInHours;
	private String durationInDays;
	private String levelOfParticipants;
	private String noOfHoursPerDay;
	private Integer maxSeats;
	private Integer approvedOrRejectedBy;
	private boolean isSeatsFilled;
	private String reportingMgrName;
	private String nominatedbyEmpName;
	private Integer remainingSeats;
	private String programmeCategory;
	
	public Integer getRemainingSeats() {
		return remainingSeats;
	}

	public void setRemainingSeats(Integer remainingSeats) {
		this.remainingSeats = remainingSeats;
	}

	public String getNominatedbyEmpName() {
		return nominatedbyEmpName;
	}

	public void setNominatedbyEmpName(String nominatedbyEmpName) {
		this.nominatedbyEmpName = nominatedbyEmpName;
	}
	public String getReportingMgrName() {
		return reportingMgrName;
	}

	public void setReportingMgrName(String reportingMgrName) {
		this.reportingMgrName = reportingMgrName;
	}

	public boolean isSeatsFilled() {
		return isSeatsFilled;
	}

	public void setSeatsFilled(boolean isSeatsFilled) {
		this.isSeatsFilled = isSeatsFilled;
	}

	public Integer getApprovedOrRejectedBy() {
		return approvedOrRejectedBy;
	}

	public void setApprovedOrRejectedBy(Integer approvedOrRejectedBy) {
		this.approvedOrRejectedBy = approvedOrRejectedBy;
	}

	public Integer getMaxSeats() {
		return maxSeats;
	}

	public void setMaxSeats(Integer maxSeats) {
		this.maxSeats = maxSeats;
	}

	public String getNoOfHoursPerDay() {
		return noOfHoursPerDay;
	}

	public void setNoOfHoursPerDay(String noOfHoursPerDay) {
		this.noOfHoursPerDay = noOfHoursPerDay;
	}

	public TrainingRequestDetailBean getRequestDetails() {
		return requestDetails;
	}

	public void setRequestDetails(TrainingRequestDetailBean requestDetails) {
		this.requestDetails = requestDetails;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDurationInDays() {
		return durationInDays;
	}

	public void setDurationInDays(String durationInDays) {
		this.durationInDays = durationInDays;
	}

	public String getLevelOfParticipants() {
		return levelOfParticipants;
	}

	public void setLevelOfParticipants(String levelOfParticipants) {
		this.levelOfParticipants = levelOfParticipants;
	}

	public Integer getRequesterEmpId() {
		return requesterEmpId;
	}

	public void setRequesterEmpId(Integer requesterEmpId) {
		this.requesterEmpId = requesterEmpId;
	}

	public Byte getProgStatusId() {
		return progStatusId;
	}

	public void setProgStatusId(Byte progStatusId) {
		this.progStatusId = progStatusId;
	}

	public String getProgStatus() {
		return progStatus;
	}

	public void setProgStatus(String progStatus) {
		this.progStatus = progStatus;
	}

	public Integer getProgId() {
		return progId;
	}

	public void setProgId(Integer progId) {
		this.progId = progId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTrainingTitle() {
		return trainingTitle;
	}

	public String getNominationStatus() {
		return nominationStatus;
	}

	public void setNominationStatus(String nominationStatus) {
		this.nominationStatus = nominationStatus;
	}

	public void setTrainingTitle(String trainingTitle) {
		this.trainingTitle = trainingTitle;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getNoOfParticipants() {
		return noOfParticipants;
	}

	public void setNoOfParticipants(Integer noOfParticipants) {
		this.noOfParticipants = noOfParticipants;
	}

	public String getTrainer() {
		return trainer;
	}

	public void setTrainer(String trainer) {
		this.trainer = trainer;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getInitiatedBy() {
		return initiatedBy;
	}

	public void setInitiatedBy(String initiatedBy) {
		this.initiatedBy = initiatedBy;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getProgrammeCategory() {
		return programmeCategory;
	}

	public void setProgrammeCategory(String programmeCategory) {
		this.programmeCategory = programmeCategory;
	}

	@Override
	public String toString() {
		return "DetailedTrainingOutputBean [trainingTitle=" + trainingTitle
				+ ", startDate=" + startDate + ", noOfParticipants="
				+ noOfParticipants + ", trainer=" + trainer + ", venue="
				+ venue + ", initiatedBy=" + initiatedBy + ", time=" + time
				+ ", nominationStatus=" + nominationStatus + ", description="
				+ description + ", progId=" + progId + ", requesterEmpId="
				+ requesterEmpId + ", progStatusId=" + progStatusId
				+ ", progStatus=" + progStatus + ", requestDetails="
				+ requestDetails + ", endDate=" + endDate + ", durationInDays="
				+ durationInDays + ", levelOfParticipants="
				+ levelOfParticipants + ", noOfHoursPerDay=" + noOfHoursPerDay
				+ ", maxSeats=" + maxSeats + ", approvedOrRejectedBy="
				+ approvedOrRejectedBy + ", isSeatsFilled=" + isSeatsFilled
				+ ", reportingMgrName=" + reportingMgrName
				+ ", nominatedbyEmpName=" + nominatedbyEmpName
				+ ", remainingSeats=" + remainingSeats + ", programmeCategory="
				+ programmeCategory + "]";
	}

}
