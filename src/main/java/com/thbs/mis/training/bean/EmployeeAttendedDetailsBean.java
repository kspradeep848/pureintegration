package com.thbs.mis.training.bean;

public class EmployeeAttendedDetailsBean {

	private int empId;
	private String empName;
	private String attendedTraining;

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getAttendedTraining() {
		return attendedTraining;
	}

	public void setAttendedTraining(String attendedTraining) {
		this.attendedTraining = attendedTraining;
	}

	@Override
	public String toString() {
		return "EmployeeAttendedDetailsBean [empId=" + empId + ", empName="
				+ empName + ", attendedTraining=" + attendedTraining + "]";
	}

}
