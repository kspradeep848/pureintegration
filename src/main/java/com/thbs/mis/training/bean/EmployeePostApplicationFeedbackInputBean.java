
/**********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmployeePostApplicationFeedbackInputBean.java     */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  17-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details  for the      	 */
/*                 	EmployeePostApplicationFeedbackInputBean.        */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 17-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.bean;

public class EmployeePostApplicationFeedbackInputBean {

	private int empId;
	private String answer;

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Override
	public String toString() {
		return "EmployeePostApplicationFeedbackInputBean [empId=" + empId
				+ ", answer=" + answer + "]";
	}

}
