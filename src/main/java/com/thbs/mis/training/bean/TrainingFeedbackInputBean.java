/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  TrainingFeedbackInputBean.java                                                        */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/


package com.thbs.mis.training.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class TrainingFeedbackInputBean {

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Employee Id should be a numeric value")
	@Employee_Id_Not_Found
	private Integer empId;
	
	private String status;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	@Min(0)
	@NumberFormat(style = Style.NUMBER, pattern = "Page Number should be a numeric value")
	private Integer pageNumber;
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Page Size should be a numeric value")
	private Integer pageSize;

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "TrainingFeedbackInputBean [empId=" + empId + ", status="
				+ status + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", pageNumber=" + pageNumber + ", pageSize=" + pageSize + "]";
	}

}
