/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  QuestionAndAnswerListBean.java                                                       */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.List;

public class QuestionAndAnswerListBean {

	private int questionType;
	private String questionName;
	private List<FeedbackAnswerBean> answerList;

	public int getQuestionType() {
		return questionType;
	}

	public void setQuestionType(int questionType) {
		this.questionType = questionType;
	}

	public String getQuestionName() {
		return questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public List<FeedbackAnswerBean> getAnswerList() {
		return answerList;
	}

	public void setAnswerList(List<FeedbackAnswerBean> answerList) {
		this.answerList = answerList;
	}

	@Override
	public String toString() {
		return "QuestionAndAnswerListBean [questionType=" + questionType
				+ ", questionName=" + questionName + ", answerList="
				+ answerList + "]";
	}

}
