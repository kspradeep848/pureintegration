/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingDatSubNominationPoolDetailBean.java        */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the fields                */
/*                 used for the required BO                          */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016     THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

public class TrainingDatSubNominationPoolDetailBean {

	private Integer fkTrainingProgrammeId;
	private Integer fkApproverEmpId;
	private Integer fkNominatedEmpId;
	private String nominatedEmpName;
	private int pkTrainingNominationPoolDetailId;



	public int getPkTrainingNominationPoolDetailId() {
		return pkTrainingNominationPoolDetailId;
	}

	public void setPkTrainingNominationPoolDetailId(
			int pkTrainingNominationPoolDetailId) {
		this.pkTrainingNominationPoolDetailId = pkTrainingNominationPoolDetailId;
	}

	public String getNominatedEmpName() {
		return nominatedEmpName;
	}

	public void setNominatedEmpName(String nominatedEmpName) {
		this.nominatedEmpName = nominatedEmpName;
	}

	public Integer getFkNominatedEmpId() {
		return fkNominatedEmpId;
	}

	public void setFkNominatedEmpId(Integer fkNominatedEmpId) {
		this.fkNominatedEmpId = fkNominatedEmpId;
	}

	private byte nominationStatus;

	TrainingProgrammeDetailInputBean trainingProgrammeDetailInputBean;

	public TrainingProgrammeDetailInputBean getTrainingProgrammeDetailInputBean() {
		return trainingProgrammeDetailInputBean;
	}

	public void setTrainingProgrammeDetailInputBean(
			TrainingProgrammeDetailInputBean trainingProgrammeDetailInputBean) {
		this.trainingProgrammeDetailInputBean = trainingProgrammeDetailInputBean;
	}

	public Integer getFkTrainingProgrammeId() {
		return fkTrainingProgrammeId;
	}

	public void setFkTrainingProgrammeId(Integer fkTrainingProgrammeId) {
		this.fkTrainingProgrammeId = fkTrainingProgrammeId;
	}

	public Integer getFkApproverEmpId() {
		return fkApproverEmpId;
	}

	public void setFkApproverEmpId(Integer fkApproverEmpId) {
		this.fkApproverEmpId = fkApproverEmpId;
	}

	public byte getNominationStatus() {
		return nominationStatus;
	}

	public void setNominationStatus(byte nominationStatus) {
		this.nominationStatus = nominationStatus;
	}

	@Override
	public String toString() {
		return "TrainingDatSubNominationPoolDetailBean [fkTrainingProgrammeId="
				+ fkTrainingProgrammeId + ", fkApproverEmpId="
				+ fkApproverEmpId + ", fkNominatedEmpId=" + fkNominatedEmpId
				+ ", nominatedEmpName=" + nominatedEmpName
				+ ", pkTrainingNominationPoolDetailId="
				+ pkTrainingNominationPoolDetailId + ", nominationStatus="
				+ nominationStatus + ", trainingProgrammeDetailInputBean="
				+ trainingProgrammeDetailInputBean + "]";
	}
	
}
