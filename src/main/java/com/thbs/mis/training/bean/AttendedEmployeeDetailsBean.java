/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  AttendedEmployeeDetailsBean.java                   		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO                             */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.bean;

public class AttendedEmployeeDetailsBean {
	
	private int empId;
	private String empName;
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	@Override
	public String toString() {
		return "AttendedEmployeeDetailsBean [empId=" + empId + ", empName="
				+ empName + "]";
	}
}
