/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingNominateBean.java            		    */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the fields                */
/*                 used for the required BO                          */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016     THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.io.Serializable;

public class TrainingNominateBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private int programId;
	private int nominatedEmpId;
	private int nominatedByEmpId;

	public int getProgramId() {
		return programId;
	}

	public void setProgramId(int programId) {
		this.programId = programId;
	}

	public int getNominatedEmpId() {
		return nominatedEmpId;
	}

	public void setNominatedEmpId(int nominatedEmpId) {
		this.nominatedEmpId = nominatedEmpId;
	}

	public int getNominatedByEmpId() {
		return nominatedByEmpId;
	}

	public void setNominatedByEmpId(int nominatedByEmpId) {
		this.nominatedByEmpId = nominatedByEmpId;
	}

	@Override
	public String toString() {
		return "TrainingNominateBean [programId=" + programId
				+ ", nominatedEmpId=" + nominatedEmpId + ", nominatedByEmpId="
				+ nominatedByEmpId + "]";
	}
}
