/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  UpdateTrainingRequestStatusBean.java              */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                Update Training Request Status                     */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016     THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.training.Training_Request_Id_Not_Found;
import com.thbs.mis.framework.customValidation.annotation.training.Training_Request_Status_Not_Found;

public class UpdateTrainingRequestStatusBean {
	@Min(1)
	@NotNull(message = "Training Request Status ID field can not be null")
	@NumberFormat(style = Style.NUMBER)
	@Training_Request_Status_Not_Found
	private Byte fk_training_request_status_id;
	
	@Min(1)
	@NotNull(message = "Training Request ID field can not be null")
	@NumberFormat(style = Style.NUMBER)
	@Training_Request_Id_Not_Found
	private Integer pkTrainingRequestId;

	@NotNull(message = "comments field can not be null")
	@NotBlank(message = "comments field can not be blank")
	@Size(max = 250)
	private String comments;
	
	public Byte getFk_training_request_status_id() {
		return fk_training_request_status_id;
	}

	public void setFk_training_request_status_id(Byte fk_training_request_status_id) {
		this.fk_training_request_status_id = fk_training_request_status_id;
	}

	public Integer getPkTrainingRequestId() {
		return pkTrainingRequestId;
	}

	public void setPkTrainingRequestId(Integer pkTrainingRequestId) {
		this.pkTrainingRequestId = pkTrainingRequestId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "UpdateTrainingRequestStatusBean [fk_training_request_status_id="
				+ fk_training_request_status_id
				+ ", pkTrainingRequestId="
				+ pkTrainingRequestId + ", comments=" + comments + "]";
	}
	
	
	
}
