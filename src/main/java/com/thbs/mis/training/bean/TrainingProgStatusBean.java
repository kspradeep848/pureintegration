/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingProgStatusBean.java                   	 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Training feed Back.                              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;
import com.thbs.mis.framework.customValidation.annotation.training.Training_Program_Id_Not_Found;
import com.thbs.mis.framework.customValidation.annotation.training.Training_Program_Status_Not_Found;

public class TrainingProgStatusBean {

	@Training_Program_Status_Not_Found
	private byte status;
	private String comments;

	@Training_Program_Id_Not_Found
	private int progId;

	@Employee_Id_Not_Found
	private int empId;

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getProgId() {
		return progId;
	}

	public void setProgId(int progId) {
		this.progId = progId;
	}

	@Override
	public String toString() {
		return "TrainingProgStatusBean [status=" + status + ", comments="
				+ comments + ", progId=" + progId + ", empId=" + empId + "]";
	}

}
