/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingDetailsOutputBean.java    		          */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the fields                */
/*                 used for the required BO                          */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016     THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomDateSerializer;

public class TrainingDetailsOutputBean {

	private Integer reportingManagerId;

	private String topicName;

	private Date trainingStartDate;

	private String breifDescription;

	private Integer maximumSeats;
	
	private String feedbackStatus;
	
	private Integer pkTrainingRequestId;
	
	private Integer programId;
	
	private String trainerName;
	
	private Date programModifiedDate;
	
	private Long dateDifferenceNow;
	
	public Long getDateDifferenceNow() {
		return dateDifferenceNow;
	}

	public void setDateDifferenceNow(Long dateDifferenceNow) {
		this.dateDifferenceNow = dateDifferenceNow;
	}

	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getProgramModifiedDate() {
		return programModifiedDate;
	}

	public void setProgramModifiedDate(Date programModifiedDate) {
		this.programModifiedDate = programModifiedDate;
	}

	public String getTrainerName() {
		return trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	public Integer getProgramId() {
		return programId;
	}

	public void setProgramId(Integer programId) {
		this.programId = programId;
	}

	public Integer getPkTrainingRequestId() {
		return pkTrainingRequestId;
	}

	public void setPkTrainingRequestId(Integer pkTrainingRequestId) {
		this.pkTrainingRequestId = pkTrainingRequestId;
	}

	public String getFeedbackStatus() {
		return feedbackStatus;
	}

	public void setFeedbackStatus(String feedbackStatus) {
		this.feedbackStatus = feedbackStatus;
	}

	public String getBreifDescription() {
		return breifDescription;
	}

	public void setBreifDescription(String breifDescription) {
		this.breifDescription = breifDescription;
	}

	public void setReportingManagerId(Integer reportingManagerId) {
		this.reportingManagerId = reportingManagerId;
	}

	public Integer getReportingManagerId() {
		return reportingManagerId;
	}

	public void setReportingManager(Integer reportingManagerId) {
		this.reportingManagerId = reportingManagerId;
	}

	public String getTopicName() {
		return topicName;
	}

	public Integer getMaximumSeats() {
		return maximumSeats;
	}

	public void setMaximumSeats(Integer maximumSeats) {
		this.maximumSeats = maximumSeats;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}
	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getTrainingStartDate() {
		return trainingStartDate;
	}

	public void setTrainingStartDate(Date trainingStartDate) {
		this.trainingStartDate = trainingStartDate;
	}

	@Override
	public String toString() {
		return "TrainingDetailsOutputBean [reportingManagerId="
				+ reportingManagerId + ", topicName=" + topicName
				+ ", trainingStartDate=" + trainingStartDate
				+ ", breifDescription=" + breifDescription + ", maximumSeats="
				+ maximumSeats + ", feedbackStatus=" + feedbackStatus
				+ ", pkTrainingRequestId=" + pkTrainingRequestId
				+ ", programId=" + programId + ", trainerName=" + trainerName
				+ ", programModifiedDate=" + programModifiedDate
				+ ", dateDifferenceNow=" + dateDifferenceNow + "]";
	}



}
