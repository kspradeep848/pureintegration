/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  UpdateTrainingProgramBean.java                   */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Training feed Back.                              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.thbs.mis.framework.customValidation.annotation.training.Training_Program_Id_Not_Found;
import com.thbs.mis.framework.customValidation.annotation.training.Training_Program_Status_Not_Found;
import com.thbs.mis.framework.customValidation.annotation.training.Training_Room_Id_Not_Found;
import com.thbs.mis.framework.util.TimeDeserializer;

public class UpdateTrainingProgramBean {

	@Training_Program_Id_Not_Found
	private Integer pkTrainingProgramId;

	private Integer trainingRequestId;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd HH:mm:SSSS")
	@JsonDeserialize(using = TimeDeserializer.class)
	private Date trainingStartDate;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd HH:mm:SSSS")
	@JsonDeserialize(using = TimeDeserializer.class)
	private Date trainingEndDate;

	private Integer trainingDurationInHrs;

	private Integer trainingDurationInDays;

	private Integer trainingNoOfHrsPerDay;

	private String trainerType;

	private String trainerName;

	private Integer maxSeats;

	@Training_Room_Id_Not_Found
	private short trainingRoomId;

	private String externalVenueName;

	@Training_Program_Status_Not_Found
	private byte programStatus;

	private Byte trainingIntimatedStatus;

	private Integer programCreatedBy;

	private Date programCreatedDate;

	private Integer programModifiedBy;

	private String comments;

	public Integer getPkTrainingProgramId() {
		return pkTrainingProgramId;
	}

	public void setPkTrainingProgramId(Integer pkTrainingProgramId) {
		this.pkTrainingProgramId = pkTrainingProgramId;
	}

	public Integer getTrainingRequestId() {
		return trainingRequestId;
	}

	public void setTrainingRequestId(Integer trainingRequestId) {
		this.trainingRequestId = trainingRequestId;
	}

	public Date getTrainingStartDate() {
		return trainingStartDate;
	}

	public void setTrainingStartDate(Date trainingStartDate) {
		this.trainingStartDate = trainingStartDate;
	}

	public Date getTrainingEndDate() {
		return trainingEndDate;
	}

	public void setTrainingEndDate(Date trainingEndDate) {
		this.trainingEndDate = trainingEndDate;
	}

	public Integer getTrainingDurationInHrs() {
		return trainingDurationInHrs;
	}

	public void setTrainingDurationInHrs(Integer trainingDurationInHrs) {
		this.trainingDurationInHrs = trainingDurationInHrs;
	}

	public Integer getTrainingDurationInDays() {
		return trainingDurationInDays;
	}

	public void setTrainingDurationInDays(Integer trainingDurationInDays) {
		this.trainingDurationInDays = trainingDurationInDays;
	}

	public Integer getTrainingNoOfHrsPerDay() {
		return trainingNoOfHrsPerDay;
	}

	public void setTrainingNoOfHrsPerDay(Integer trainingNoOfHrsPerDay) {
		this.trainingNoOfHrsPerDay = trainingNoOfHrsPerDay;
	}

	public String getTrainerType() {
		return trainerType;
	}

	public void setTrainerType(String trainerType) {
		this.trainerType = trainerType;
	}

	public String getTrainerName() {
		return trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	public Integer getMaxSeats() {
		return maxSeats;
	}

	public void setMaxSeats(Integer maxSeats) {
		this.maxSeats = maxSeats;
	}

	public short getTrainingRoomId() {
		return trainingRoomId;
	}

	public void setTrainingRoomId(short trainingRoomId) {
		this.trainingRoomId = trainingRoomId;
	}

	public String getExternalVenueName() {
		return externalVenueName;
	}

	public void setExternalVenueName(String externalVenueName) {
		this.externalVenueName = externalVenueName;
	}

	public byte getProgramStatus() {
		return programStatus;
	}

	public void setProgramStatus(byte programStatus) {
		this.programStatus = programStatus;
	}

	public Byte getTrainingIntimatedStatus() {
		return trainingIntimatedStatus;
	}

	public void setTrainingIntegerimatedStatus(Byte trainingIntimatedStatus) {
		this.trainingIntimatedStatus = trainingIntimatedStatus;
	}

	public Integer getProgramCreatedBy() {
		return programCreatedBy;
	}

	public void setProgramCreatedBy(Integer programCreatedBy) {
		this.programCreatedBy = programCreatedBy;
	}

	public Date getProgramCreatedDate() {
		return programCreatedDate;
	}

	public void setProgramCreatedDate(Date programCreatedDate) {
		this.programCreatedDate = programCreatedDate;
	}

	public Integer getProgramModifiedBy() {
		return programModifiedBy;
	}

	public void setProgramModifiedBy(Integer programModifiedBy) {
		this.programModifiedBy = programModifiedBy;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "UpdateTrainingProgramBean [pkTrainingProgramId="
				+ pkTrainingProgramId + ", trainingRequestId="
				+ trainingRequestId + ", trainingStartDate="
				+ trainingStartDate + ", trainingEndDate=" + trainingEndDate
				+ ", trainingDurationInHrs=" + trainingDurationInHrs
				+ ", trainingDurationInDays=" + trainingDurationInDays
				+ ", trainingNoOfHrsPerDay=" + trainingNoOfHrsPerDay
				+ ", trainerType=" + trainerType + ", trainerName="
				+ trainerName + ", maxSeats=" + maxSeats + ", trainingRoomId="
				+ trainingRoomId + ", externalVenueName=" + externalVenueName
				+ ", programStatus=" + programStatus
				+ ", trainingIntimatedStatus=" + trainingIntimatedStatus
				+ ", programCreatedBy=" + programCreatedBy
				+ ", programCreatedDate=" + programCreatedDate
				+ ", programModifiedBy=" + programModifiedBy + ", comments="
				+ comments + "]";
	}

}
