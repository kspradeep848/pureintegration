/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingProgrammeDetailSubInputBean.java           */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Training feed Back.                              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

public class TrainingProgrammeDetailSubInputBean {
	
	Date programStartDate;
	
	Integer maximumSeats;
	
	Integer programmeId;
	
	String trainerName;
	
	Date programModifiedDate;
	
	public Date getProgramModifiedDate() {
		return programModifiedDate;
	}

	public void setProgramModifiedDate(Date programModifiedDate) {
		this.programModifiedDate = programModifiedDate;
	}

	public String getTrainerName() {
		return trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	public Integer getProgrammeId() {
		return programmeId;
	}

	public void setProgrammeId(Integer programmeId) {
		this.programmeId = programmeId;
	}

	public Date getProgramStartDate() {
		return programStartDate;
	}
	
	public void setProgramStartDate(Date programStartDate) {
		this.programStartDate = programStartDate;
	}
	
	public Integer getMaximumSeats() {
		return maximumSeats;
	}
	
	public void setMaximumSeats(Integer maximumSeats) {
		this.maximumSeats = maximumSeats;
	}

	@Override
	public String toString() {
		return "TrainingProgrammeDetailSubInputBean [programStartDate="
				+ programStartDate + ", maximumSeats=" + maximumSeats
				+ ", programmeId=" + programmeId + ", trainerName="
				+ trainerName + ", programModifiedDate=" + programModifiedDate
				+ "]";
	}


	
	

}
