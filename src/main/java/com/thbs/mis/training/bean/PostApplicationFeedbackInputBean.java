package com.thbs.mis.training.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;
import com.thbs.mis.framework.customValidation.annotation.training.Training_Program_Id_Not_Found;

public class PostApplicationFeedbackInputBean {

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Employee Id should be a numeric value")
	@Employee_Id_Not_Found
	private Integer mgrId;

	@Min(1)
	@Training_Program_Id_Not_Found
	private Integer progId;

	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;

	@Min(0)
	@NumberFormat(style = Style.NUMBER, pattern = "Page Number should be a numeric value")
	private Integer pageNumber;
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Page Size should be a numeric value")
	private Integer pageSize;

	public Integer getMgrId() {
		return mgrId;
	}

	public void setMgrId(Integer mgrId) {
		this.mgrId = mgrId;
	}

	public Integer getProgId() {
		return progId;
	}

	public void setProgId(Integer progId) {
		this.progId = progId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	@Override
	public String toString() {
		return "PostApplicationFeedbackInputBean [mgrId=" + mgrId + ", progId="
				+ progId + ", status=" + status + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", pageNumber=" + pageNumber
				+ ", pageSize=" + pageSize + "]";
	}

}
