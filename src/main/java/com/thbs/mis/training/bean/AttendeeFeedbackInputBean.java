/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  AttendeeFeedbackInputBean.java                                                        */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

import javax.validation.constraints.Min;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.training.Training_Program_Id_Not_Found;

public class AttendeeFeedbackInputBean {

	@Training_Program_Id_Not_Found
	private Integer progId;

	private String status;
	private Date startDate;
	private Date endDate;

	@Min(0)
	@NumberFormat(style = Style.NUMBER, pattern = "Page Number should be a numeric value")
	private Integer pageNumber;

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Page Size should be a numeric value")
	private Integer pageSize;

	public Integer getProgId() {
		return progId;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public void setProgId(Integer progId) {
		this.progId = progId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "AttendeeFeedbackInputBean [progId=" + progId + ", status="
				+ status + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", pageNumber=" + pageNumber + ", pageSize=" + pageSize + "]";
	}

}
