package com.thbs.mis.training.bean;

public class FeedbackListBean {
	
	private int questionType;
	private String questionName;
	private String answer;
	public int getQuestionType() {
		return questionType;
	}
	public void setQuestionType(int questionType) {
		this.questionType = questionType;
	}
	public String getQuestionName() {
		return questionName;
	}
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	@Override
	public String toString() {
		return "FeedbackListBean [questionType=" + questionType
				+ ", questionName=" + questionName + ", answer=" + answer + "]";
	}
	
	

}
