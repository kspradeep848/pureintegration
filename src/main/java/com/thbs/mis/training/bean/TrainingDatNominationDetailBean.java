/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingDatNominationDetailBean.java                */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the fields                */
/*                 used for the required BO                          */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016     THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

public class TrainingDatNominationDetailBean {
	@NotNull(message =  "Please Add fkDatNominatedEmpId")
	@Min(1)
	private Integer fkDatNominatedEmpId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd ")
	private Date startDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd ")
	private Date endDate;
	
	
	public Integer getFkDatNominatedEmpId() {
		return fkDatNominatedEmpId;
	}
	public void setFkDatNominatedEmpId(Integer fkDatNominatedEmpId) {
		this.fkDatNominatedEmpId = fkDatNominatedEmpId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	@Override
	public String toString() {
		return "TrainingDatNominationDetailBean [fkDatNominatedEmpId="
				+ fkDatNominatedEmpId + ", startDate=" + startDate
				+ ", endDate=" + endDate + "]";
	}
	
}
