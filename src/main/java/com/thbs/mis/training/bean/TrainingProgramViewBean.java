/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingProgramViewBean.java                   	*/
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Training feed Back.                              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class TrainingProgramViewBean {

	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date progStartDate;
	private String programTitle;
	private String progDesc;
	private String trainerName;
	private int maxSeats;
	private int progId;
	private int trainingStatus;
	private String trainingStatusName;
	private String programmeCategory;

	public String getTrainingStatusName() {
		return trainingStatusName;
	}

	public void setTrainingStatusName(String trainingStatusName) {
		this.trainingStatusName = trainingStatusName;
	}

	public int getProgId() {
		return progId;
	}

	public void setProgId(int progId) {
		this.progId = progId;
	}

	public Date getProgStartDate() {
		return progStartDate;
	}

	public void setProgStartDate(Date progStartDate) {
		this.progStartDate = progStartDate;
	}

	public String getProgramTitle() {
		return programTitle;
	}

	public void setProgramTitle(String programTitle) {
		this.programTitle = programTitle;
	}

	public String getProgDesc() {
		return progDesc;
	}

	public void setProgDesc(String progDesc) {
		this.progDesc = progDesc;
	}

	public String getTrainerName() {
		return trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	public int getMaxSeats() {
		return maxSeats;
	}

	public void setMaxSeats(int maxSeats) {
		this.maxSeats = maxSeats;
	}

	public int getTrainingStatus() {
		return trainingStatus;
	}

	public void setTrainingStatus(int trainingStatus) {
		this.trainingStatus = trainingStatus;
	}

	public String getProgrammeCategory() {
		return programmeCategory;
	}

	public void setProgrammeCategory(String programmeCategory) {
		this.programmeCategory = programmeCategory;
	}

	@Override
	public String toString() {
		return "TrainingProgramViewBean [progStartDate=" + progStartDate
				+ ", programTitle=" + programTitle + ", progDesc=" + progDesc
				+ ", trainerName=" + trainerName + ", maxSeats=" + maxSeats
				+ ", progId=" + progId + ", trainingStatus=" + trainingStatus
				+ ", trainingStatusName=" + trainingStatusName
				+ ", programmeCategory=" + programmeCategory + "]";
	}

}
