/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingRequestsCoordinatorViewBean.java          */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                Training Requests Coordinator View                 */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016     THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class TrainingRequestsCoordinatorViewBean {

	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date trainingDate;
	private String trainingTitle;
	private String trainingDescription;
	private int noOfParticipants;
	private String status;
	private String mgrName;
	private int requestId;
	private int mgrId;
	private String isProgramCreated;

	public String getIsProgramCreated() {
		return isProgramCreated;
	}

	public void setIsProgramCreated(String isProgramCreated) {
		this.isProgramCreated = isProgramCreated;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public int getMgrId() {
		return mgrId;
	}

	public void setMgrId(int mgrId) {
		this.mgrId = mgrId;
	}

	public String getMgrName() {
		return mgrName;
	}

	public void setMgrName(String mgrName) {
		this.mgrName = mgrName;
	}

	public Date getTrainingDate() {
		return trainingDate;
	}

	public void setTrainingDate(Date trainingDate) {
		this.trainingDate = trainingDate;
	}

	public String getTrainingTitle() {
		return trainingTitle;
	}

	public void setTrainingTitle(String trainingTitle) {
		this.trainingTitle = trainingTitle;
	}

	public String getTrainingDescription() {
		return trainingDescription;
	}

	public void setTrainingDescription(String trainingDescription) {
		this.trainingDescription = trainingDescription;
	}

	public int getNoOfParticipants() {
		return noOfParticipants;
	}

	public void setNoOfParticipants(int noOfParticipants) {
		this.noOfParticipants = noOfParticipants;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "TrainingRequestsCoordinatorViewBean [trainingDate="
				+ trainingDate + ", trainingTitle=" + trainingTitle
				+ ", trainingDescription=" + trainingDescription
				+ ", noOfParticipants=" + noOfParticipants + ", status="
				+ status + ", mgrName=" + mgrName + ", requestId=" + requestId
				+ ", mgrId=" + mgrId + ", isProgramCreated=" + isProgramCreated
				+ "]";
	}

}
