/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  AttendeeFeedbackOutputBean.java                                                      */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class AttendeeFeedbackOutputBean {

	private int empId;
	private int progId;
	private String empName;
	private String empMailId;
	private String comments;
	private String overallRating;
	private String progTitle;
	
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date startDate;
	
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date feedbackGivenDate;

	public Date getFeedbackGivenDate() {
		return feedbackGivenDate;
	}

	public void setFeedbackGivenDate(Date feedbackGivenDate) {
		this.feedbackGivenDate = feedbackGivenDate;
	}

	public String getProgTitle() {
		return progTitle;
	}

	public void setProgTitle(String progTitle) {
		this.progTitle = progTitle;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpMailId() {
		return empMailId;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public int getProgId() {
		return progId;
	}

	public void setProgId(int progId) {
		this.progId = progId;
	}

	public void setEmpMailId(String empMailId) {
		this.empMailId = empMailId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getOverallRating() {
		return overallRating;
	}

	public void setOverallRating(String overallRating) {
		this.overallRating = overallRating;
	}

	@Override
	public String toString() {
		return "AttendeeFeedbackOutputBean [empId=" + empId + ", progId="
				+ progId + ", empName=" + empName + ", empMailId=" + empMailId
				+ ", comments=" + comments + ", overallRating=" + overallRating
				+ ", progTitle=" + progTitle + ", startDate=" + startDate + "]";
	}

}
