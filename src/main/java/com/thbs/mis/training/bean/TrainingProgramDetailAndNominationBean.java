package com.thbs.mis.training.bean;

import java.io.Serializable;
import java.util.List;

import com.thbs.mis.training.bo.TrainingDatNominationDetailBO;
import com.thbs.mis.training.bo.TrainingDatProgrammeDetailBO;

public class TrainingProgramDetailAndNominationBean implements Serializable
{

	/**
	 * Variable <code>serialVersionUID</code> holds <code>long</code> value.
	 */
	//Kamal
	private static final long serialVersionUID = 1L;
	
	private TrainingDatProgrammeDetailBO trainingProgram;
	
	private List<TrainingDatNominationDetailBO> nominateEmpids;

	public TrainingDatProgrammeDetailBO getTrainingProgram()
	{
		return trainingProgram;
	}

	public void setTrainingProgram(TrainingDatProgrammeDetailBO trainingProgram)
	{
		this.trainingProgram = trainingProgram;
	}

	public List<TrainingDatNominationDetailBO> getNominateEmpids()
	{
		return nominateEmpids;
	}

	public void setNominateEmpids(
			List<TrainingDatNominationDetailBO> nominateEmpids)
	{
		this.nominateEmpids = nominateEmpids;
	}

	@Override
	public String toString()
	{
		return "TrainingProgramDetailAndNominationBean [trainingProgram="
				+ trainingProgram
				+ ", nominateEmpids="
				+ nominateEmpids + "]";
	}
	

}
