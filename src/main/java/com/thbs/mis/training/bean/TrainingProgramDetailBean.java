/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingProgramDetailBean.java                    */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  23-Jun-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Training feed Back.                              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class TrainingProgramDetailBean {
	
	private String trainingTitle;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date date;
	private int noOfParticipants;
	private String trainerName;
	private String venue;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date startTime;
	private String intiatedBy;
	private String description;
	private String empName;
	private String approvalStatus;
	private int progId;
	private String programmeCategory;
	
	public int getProgId() {
		return progId;
	}
	public void setProgId(int progId) {
		this.progId = progId;
	}
	public String getApprovalStatus() {
		return approvalStatus;
	}
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	public String getTrainingTitle() {
		return trainingTitle;
	}
	public void setTrainingTitle(String trainingTitle) {
		this.trainingTitle = trainingTitle;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getNoOfParticipants() {
		return noOfParticipants;
	}
	public void setNoOfParticipants(int noOfParticipants) {
		this.noOfParticipants = noOfParticipants;
	}
	public String getTrainerName() {
		return trainerName;
	}
	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public String getIntiatedBy() {
		return intiatedBy;
	}
	public void setIntiatedBy(String intiatedBy) {
		this.intiatedBy = intiatedBy;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getProgrammeCategory() {
		return programmeCategory;
	}
	public void setProgrammeCategory(String programmeCategory) {
		this.programmeCategory = programmeCategory;
	}
	@Override
	public String toString() {
		return "TrainingProgramDetailBean [trainingTitle=" + trainingTitle
				+ ", date=" + date + ", noOfParticipants=" + noOfParticipants
				+ ", trainerName=" + trainerName + ", venue=" + venue
				+ ", startTime=" + startTime + ", intiatedBy=" + intiatedBy
				+ ", description=" + description + ", empName=" + empName
				+ ", approvalStatus=" + approvalStatus + ", progId=" + progId
				+ ", programmeCategory=" + programmeCategory + "]";
	}
	
}
