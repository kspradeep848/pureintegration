/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  UpdateTrainingRequestBean.java                    */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 Update Training Request.                          */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016     THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.bean;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.thbs.mis.framework.customValidation.annotation.training.Training_Request_Id_Not_Found;

public class UpdateTrainingRequestBean {
	@Training_Request_Id_Not_Found
	private Integer trainingRequestId;

	private Integer requestorEmpId;

	private byte trainingTopicId;

	private byte levelTrainingRequest;

	private String productVersion;

	private Integer noOfParticipants;

	private String levelOfParticipants;

	private byte usageOfProduct;

	private String briefTrainingContent;

	private String isSoftwareLicenseAvailable;

	private Integer noOfLicensesAvailable;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date expectedTrainingStartDate;

	private Time preferredTrainingStartTime;

	private Time preferredTrainingEndTime;

	private String externalTrainerRequest;
	
	private String specificInputComments;

	private byte requestStatus;

	private String comments;
	
	private Byte programCategory;

	public Byte getProgramCategory() {
		return programCategory;
	}

	public void setProgramCategory(Byte programCategory) {
		this.programCategory = programCategory;
	}

	public Integer getTrainingRequestId() {
		return trainingRequestId;
	}

	public void setTrainingRequestId(Integer trainingRequestId) {
		this.trainingRequestId = trainingRequestId;
	}

	public Integer getRequestorEmpId() {
		return requestorEmpId;
	}

	public void setRequestorEmpId(Integer requestorEmpId) {
		this.requestorEmpId = requestorEmpId;
	}

	public byte getTrainingTopicId() {
		return trainingTopicId;
	}

	public void setTrainingTopicId(byte trainingTopicId) {
		this.trainingTopicId = trainingTopicId;
	}

	public byte getLevelTrainingRequest() {
		return levelTrainingRequest;
	}

	public void setLevelTrainingRequest(byte levelTrainingRequest) {
		this.levelTrainingRequest = levelTrainingRequest;
	}

	public String getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	public Integer getNoOfParticipants() {
		return noOfParticipants;
	}

	public void setNoOfParticipants(Integer noOfParticipants) {
		this.noOfParticipants = noOfParticipants;
	}

	public String getLevelOfParticipants() {
		return levelOfParticipants;
	}

	public void setLevelOfParticipants(String levelOfParticipants) {
		this.levelOfParticipants = levelOfParticipants;
	}

	public byte getUsageOfProduct() {
		return usageOfProduct;
	}

	public void setUsageOfProduct(byte usageOfProduct) {
		this.usageOfProduct = usageOfProduct;
	}

	public String getBriefTrainingContent() {
		return briefTrainingContent;
	}

	public void setBriefTrainingContent(String briefTrainingContent) {
		this.briefTrainingContent = briefTrainingContent;
	}

	public String getIsSoftwareLicenseAvailable() {
		return isSoftwareLicenseAvailable;
	}

	public void setIsSoftwareLicenseAvailable(String isSoftwareLicenseAvailable) {
		this.isSoftwareLicenseAvailable = isSoftwareLicenseAvailable;
	}

	public Integer getNoOfLicensesAvailable() {
		return noOfLicensesAvailable;
	}

	public void setNoOfLicensesAvailable(Integer noOfLicensesAvailable) {
		this.noOfLicensesAvailable = noOfLicensesAvailable;
	}

	public Date getExpectedTrainingStartDate() {
		return expectedTrainingStartDate;
	}

	public void setExpectedTrainingStartDate(Date expectedTrainingStartDate) {
		this.expectedTrainingStartDate = expectedTrainingStartDate;
	}

	public Time getPreferredTrainingStartTime() {
		return preferredTrainingStartTime;
	}

	public void setPreferredTrainingStartTime(Time preferredTrainingStartTime) {
		this.preferredTrainingStartTime = preferredTrainingStartTime;
	}

	public Time getPreferredTrainingEndTime() {
		return preferredTrainingEndTime;
	}

	public void setPreferredTrainingEndTime(Time preferredTrainingEndTime) {
		this.preferredTrainingEndTime = preferredTrainingEndTime;
	}

	public String getExternalTrainerRequest() {
		return externalTrainerRequest;
	}

	public void setExternalTrainerRequest(String externalTrainerRequest) {
		this.externalTrainerRequest = externalTrainerRequest;
	}

	public String getSpecificInputComments() {
		return specificInputComments;
	}

	public void setSpecificInputComments(String specificInputComments) {
		this.specificInputComments = specificInputComments;
	}

	public byte getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(byte requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "UpdateTrainingRequestBean [trainingRequestId="
				+ trainingRequestId + ", requestorEmpId=" + requestorEmpId
				+ ", trainingTopicId=" + trainingTopicId
				+ ", levelTrainingRequest=" + levelTrainingRequest
				+ ", productVersion=" + productVersion + ", noOfParticipants="
				+ noOfParticipants + ", levelOfParticipants="
				+ levelOfParticipants + ", usageOfProduct=" + usageOfProduct
				+ ", briefTrainingContent=" + briefTrainingContent
				+ ", isSoftwareLicenseAvailable=" + isSoftwareLicenseAvailable
				+ ", noOfLicensesAvailable=" + noOfLicensesAvailable
				+ ", expectedTrainingStartDate=" + expectedTrainingStartDate
				+ ", preferredTrainingStartTime=" + preferredTrainingStartTime
				+ ", preferredTrainingEndTime=" + preferredTrainingEndTime
				+ ", externalTrainerRequest=" + externalTrainerRequest
				+ ", specificInputComments=" + specificInputComments
				+ ", requestStatus=" + requestStatus + ", comments=" + comments
				+ ", programCategory=" + programCategory + "]";
	}
	
}
