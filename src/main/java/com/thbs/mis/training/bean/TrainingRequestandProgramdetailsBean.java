/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  TrainingRequestandProgramdetailsBean.java                               */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/

package com.thbs.mis.training.bean;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;
import com.thbs.mis.framework.customValidation.annotation.training.Training_Program_Status_Not_Found;
import com.thbs.mis.framework.customValidation.annotation.training.Training_Room_Id_Not_Found;

public class TrainingRequestandProgramdetailsBean {

	private Integer pkTrainingRequestId;

	// @NotEmpty(message = "Training content cannot be empty")
	@Length(max = 250, message = "Length should be within 250 characters")
	@NotNull(message = "Breif training content cannot be null")
	@NotBlank(message = "Training content cannot be blank")
	private String briefTrainingContent;

	private String comments;

	@NotNull(message = "Expected training start date cannot be null")
	@DateTimeFormat(style = "yyyy-MM-dd")
	// @NotBlank(message="Expected training start date cannot be blank")
	private Date expectedTrainingStartDate;

	private String externalTrainerRequest;

	// @NotEmpty(message = " Software Licence cannot be empty mention YES/NO")
	/*@NotNull(message = "Licence avaialable cannot be null")
	@NotBlank(message = "Licence available cannot be blank provide yes/no")*/
	private String isSoftwareLicenceAvailable;

	@NotNull(message = "Istraining program created cannot be null")
	@NotBlank(message = "Please mention wether program created or not YES/NO")
	private String isTrainingProgramCreated;

	@Length(max = 250, message = "Length should be within 250 characters")
	@NotNull(message = "Level of participants cannot be null")
	@NotBlank(message = "level of participants cannot be blank")
	private String levelOfParticipants;

	@NotNull(message = "Level of training request cannot be null")
	@Min(1)
	private Byte levelOfTrainingRequest;

	private Integer noOfLicensesAvailbale;

	@NotNull(message = "number of participants cannot be null")
	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Training requestemployee Id should be a numeric value")
	// @NotBlank(message="participants cannot be blank")
	private Integer noOfParticipants;

	@NotNull(message = "Preferred Training End time cannot be null")
	@Temporal(TemporalType.TIMESTAMP)
	// @NotBlank(message="Preferred training end time cannot be blank" )
	private Time preferredTrainingEndTime;

	@NotNull(message = "preferred start time cannot be null")
	@Temporal(TemporalType.TIMESTAMP)
	// @NotBlank(message="Preferred training Strat time cannot be blank")
	private Time preferredTrainingStartTime;

	private String productVersionForTraining;

	private String specificInputComments;

	private Date trainingModifiedDate;

	@NotNull(message = "Training requeted date cannot be null")
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date trainingRequestedDate;

	/*
	 * @NotNull(message="Use of product cannot be null")
	 * 
	 * @Min(1)
	 * 
	 * @Max(3)
	 */
	private Byte usageOfProduct;

	@NotNull(message = "TrainingRequest status id cannot be null")
	@Min(1)
	@Max(5)
	private Byte fkTrainingRequestStatusId;

	/*
	 * //@NotNull (message="Training requester id cannot be null")
	 * 
	 * @Min(1) //@NotBlank(message="Training requester empid cannot be blank")
	 */private Integer fkTrainingRequesterEmpId;

	@NotNull(message = "Training topic name id cannot be null")
	@Min(1)
	// @NotBlank(message="Training topic name id cannot be blank")
	private Short fkTrainingTopicNameId;

	@Min(1)
	@Max(3)
	@NumberFormat(style = Style.NUMBER, pattern = "Training Program Category should be a numeric value")
	private Byte programCategory;

	public Integer getPkTrainingRequestId() {
		return pkTrainingRequestId;
	}

	public void setPkTrainingRequestId(Integer pkTrainingRequestId) {
		this.pkTrainingRequestId = pkTrainingRequestId;
	}

	public String getBriefTrainingContent() {
		return briefTrainingContent;
	}

	public void setBriefTrainingContent(String briefTrainingContent) {
		this.briefTrainingContent = briefTrainingContent;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getExpectedTrainingStartDate() {
		return expectedTrainingStartDate;
	}

	public void setExpectedTrainingStartDate(Date expectedTrainingStartDate) {
		this.expectedTrainingStartDate = expectedTrainingStartDate;
	}

	public String getExternalTrainerRequest() {
		return externalTrainerRequest;
	}

	public void setExternalTrainerRequest(String externalTrainerRequest) {
		this.externalTrainerRequest = externalTrainerRequest;
	}

	public String getIsSoftwareLicenceAvailable() {
		return isSoftwareLicenceAvailable;
	}

	public void setIsSoftwareLicenceAvailable(String isSoftwareLicenceAvailable) {
		this.isSoftwareLicenceAvailable = isSoftwareLicenceAvailable;
	}

	public String getIsTrainingProgramCreated() {
		return isTrainingProgramCreated;
	}

	public void setIsTrainingProgramCreated(String isTrainingProgramCreated) {
		this.isTrainingProgramCreated = isTrainingProgramCreated;
	}

	public String getLevelOfParticipants() {
		return levelOfParticipants;
	}

	public void setLevelOfParticipants(String levelOfParticipants) {
		this.levelOfParticipants = levelOfParticipants;
	}

	public Byte getLevelOfTrainingRequest() {
		return levelOfTrainingRequest;
	}

	public void setLevelOfTrainingRequest(Byte levelOfTrainingRequest) {
		this.levelOfTrainingRequest = levelOfTrainingRequest;
	}

	public Integer getNoOfLicensesAvailbale() {
		return noOfLicensesAvailbale;
	}

	public void setNoOfLicensesAvailbale(Integer noOfLicensesAvailbale) {
		this.noOfLicensesAvailbale = noOfLicensesAvailbale;
	}

	public Integer getNoOfParticipants() {
		return noOfParticipants;
	}

	public void setNoOfParticipants(Integer noOfParticipants) {
		this.noOfParticipants = noOfParticipants;
	}

	public Time getPreferredTrainingEndTime() {
		return preferredTrainingEndTime;
	}

	public void setPreferredTrainingEndTime(Time preferredTrainingEndTime) {
		this.preferredTrainingEndTime = preferredTrainingEndTime;
	}

	public Time getPreferredTrainingStartTime() {
		return preferredTrainingStartTime;
	}

	public void setPreferredTrainingStartTime(Time preferredTrainingStartTime) {
		this.preferredTrainingStartTime = preferredTrainingStartTime;
	}

	public String getProductVersionForTraining() {
		return productVersionForTraining;
	}

	public void setProductVersionForTraining(String productVersionForTraining) {
		this.productVersionForTraining = productVersionForTraining;
	}

	public String getSpecificInputComments() {
		return specificInputComments;
	}

	public void setSpecificInputComments(String specificInputComments) {
		this.specificInputComments = specificInputComments;
	}

	public Date getTrainingModifiedDate() {
		return trainingModifiedDate;
	}

	public void setTrainingModifiedDate(Date trainingModifiedDate) {
		this.trainingModifiedDate = trainingModifiedDate;
	}

	public Date getTrainingRequestedDate() {
		return trainingRequestedDate;
	}

	public void setTrainingRequestedDate(Date trainingRequestedDate) {
		this.trainingRequestedDate = trainingRequestedDate;
	}

	public Byte getUsageOfProduct() {
		return usageOfProduct;
	}

	public void setUsageOfProduct(Byte usageOfProduct) {
		this.usageOfProduct = usageOfProduct;
	}

	public Byte getFkTrainingRequestStatusId() {
		return fkTrainingRequestStatusId;
	}

	public void setFkTrainingRequestStatusId(Byte fkTrainingRequestStatusId) {
		this.fkTrainingRequestStatusId = fkTrainingRequestStatusId;
	}

	public Integer getFkTrainingRequesterEmpId() {
		return fkTrainingRequesterEmpId;
	}

	public void setFkTrainingRequesterEmpId(Integer fkTrainingRequesterEmpId) {
		this.fkTrainingRequesterEmpId = fkTrainingRequesterEmpId;
	}

	public Short getFkTrainingTopicNameId() {
		return fkTrainingTopicNameId;
	}

	public void setFkTrainingTopicNameId(Short fkTrainingTopicNameId) {
		this.fkTrainingTopicNameId = fkTrainingTopicNameId;
	}

	private Integer pkTrainingProgrammeId;
	private String externalVenueName;

	@NotNull(message = "Maxium seats cannot be null")
	@Min(1)
	// @NotBlank(message="Maximum seats cannot be blank")
	private Integer maximumSeats;

	/*
	 * @NotNull
	 * 
	 * @DateTimeFormat(style = "yyyy-MM-dd")
	 */
	private Date programmeCreatedDate;

	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date programmeModifiedDate;

	@Length(max = 250, message = "Length should be within 250 characters")
	@NotNull(message = "Trainer name should not be null")
	@NotBlank(message = "Trainer name should not be blank")
	private String trainerName;

	@Length(max = 250, message = "Length should be within 250 characters")
	@NotNull(message = "Trainer Type cannot be null")
	@NotBlank(message = "mention trainter type Exernal/internal")
	private String trainerType;

	@NotNull(message = "Training duration in days cannot be null")
	@Min(1)
	private Integer trainingDurationInDays;

	/*@NotNull(message = "Training duration in hors cannot be null")
	@Min(1)*/
	private Integer trainingDurationInHours;

	@NotNull(message = "Training End DateTime cannot be null")
	@DateTimeFormat(style = "yyyy-MM-dd")
	@Temporal(TemporalType.TIMESTAMP)
	private Date trainingEndDateTime;

	@NotNull(message = "Traininging intimated status cannot be null")
	@Max(1)
	private Byte trainingIntimatedStatus;

	@NotNull(message = "Training hours per day cannot be null")
	@Min(1)
	private Integer trainingNoOfHrsPerDay;

	@NotNull(message = "Training strat date cannot be null")
	@DateTimeFormat(style = "yyyy-MM-dd")
	@Temporal(TemporalType.TIMESTAMP)
	private Date trainingStartDateTime;

	@NotNull(message = "Programm created by cannot be null")
	@Min(1)
	@Employee_Id_Not_Found
	private Integer fkProgrammeCreatedBy;

	private Integer fkProgrammeModifyBy;

	@NotNull(message = "program status id cannot be null")
	@Min(1)
	@Training_Program_Status_Not_Found
	private Byte fkProgrammeStatusId;

	/*
	 * @Min(1)
	 * 
	 * @Max(5)
	 * 
	 * @NotNull (message="Training request id cannot be null")
	 */
	private Integer fkTrainingRequestId;

	@NotNull(message = "Training room id cannot ne null")
	@Min(1)
	@Training_Room_Id_Not_Found
	private Short fkTrainingRoomId;

	private String pgmDetcomments;

	public String getPgmDetcomments() {
		return pgmDetcomments;
	}

	public void setPgmDetcomments(String pgmDetcomments) {
		this.pgmDetcomments = pgmDetcomments;
	}

	public Integer getPkTrainingProgrammeId() {
		return pkTrainingProgrammeId;
	}

	public void setPkTrainingProgrammeId(Integer pkTrainingProgrammeId) {
		this.pkTrainingProgrammeId = pkTrainingProgrammeId;
	}

	public String getExternalVenueName() {
		return externalVenueName;
	}

	public void setExternalVenueName(String externalVenueName) {
		this.externalVenueName = externalVenueName;
	}

	public Integer getMaximumSeats() {
		return maximumSeats;
	}

	public void setMaximumSeats(Integer maximumSeats) {
		this.maximumSeats = maximumSeats;
	}

	public Date getProgrammeCreatedDate() {
		return programmeCreatedDate;
	}

	public void setProgrammeCreatedDate(Date programmeCreatedDate) {
		this.programmeCreatedDate = programmeCreatedDate;
	}

	public Date getProgrammeModifiedDate() {
		return programmeModifiedDate;
	}

	public void setProgrammeModifiedDate(Date programmeModifiedDate) {
		this.programmeModifiedDate = programmeModifiedDate;
	}

	public String getTrainerName() {
		return trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	public String getTrainerType() {
		return trainerType;
	}

	public void setTrainerType(String trainerType) {
		this.trainerType = trainerType;
	}

	public Integer getTrainingDurationInDays() {
		return trainingDurationInDays;
	}

	public void setTrainingDurationInDays(Integer trainingDurationInDays) {
		this.trainingDurationInDays = trainingDurationInDays;
	}

	public Integer getTrainingDurationInHours() {
		return trainingDurationInHours;
	}

	public void setTrainingDurationInHours(Integer trainingDurationInHours) {
		this.trainingDurationInHours = trainingDurationInHours;
	}

	public Date getTrainingEndDateTime() {
		return trainingEndDateTime;
	}

	public void setTrainingEndDateTime(Date trainingEndDateTime) {
		this.trainingEndDateTime = trainingEndDateTime;
	}

	public Byte getTrainingIntimatedStatus() {
		return trainingIntimatedStatus;
	}

	public void setTrainingIntimatedStatus(Byte trainingIntimatedStatus) {
		this.trainingIntimatedStatus = trainingIntimatedStatus;
	}

	public Integer getTrainingNoOfHrsPerDay() {
		return trainingNoOfHrsPerDay;
	}

	public void setTrainingNoOfHrsPerDay(Integer trainingNoOfHrsPerDay) {
		this.trainingNoOfHrsPerDay = trainingNoOfHrsPerDay;
	}

	public Date getTrainingStartDateTime() {
		return trainingStartDateTime;
	}

	public void setTrainingStartDateTime(Date trainingStartDateTime) {
		this.trainingStartDateTime = trainingStartDateTime;
	}

	public Integer getFkProgrammeCreatedBy() {
		return fkProgrammeCreatedBy;
	}

	public void setFkProgrammeCreatedBy(Integer fkProgrammeCreatedBy) {
		this.fkProgrammeCreatedBy = fkProgrammeCreatedBy;
	}

	public Integer getFkProgrammeModifyBy() {
		return fkProgrammeModifyBy;
	}

	public void setFkProgrammeModifyBy(Integer fkProgrammeModifyBy) {
		this.fkProgrammeModifyBy = fkProgrammeModifyBy;
	}

	public Byte getFkProgrammeStatusId() {
		return fkProgrammeStatusId;
	}

	public void setFkProgrammeStatusId(Byte fkProgrammeStatusId) {
		this.fkProgrammeStatusId = fkProgrammeStatusId;
	}

	public Integer getFkTrainingRequestId() {
		return fkTrainingRequestId;
	}

	public void setFkTrainingRequestId(Integer fkTrainingRequestId) {
		this.fkTrainingRequestId = fkTrainingRequestId;
	}

	public Short getFkTrainingRoomId() {
		return fkTrainingRoomId;
	}

	public void setFkTrainingRoomId(Short fkTrainingRoomId) {
		this.fkTrainingRoomId = fkTrainingRoomId;
	}

	public Byte getProgramCategory() {
		return programCategory;
	}

	public void setProgramCategory(Byte programCategory) {
		this.programCategory = programCategory;
	}

	@Override
	public String toString() {
		return "TrainingRequestandProgramdetailsBean [pkTrainingRequestId="
				+ pkTrainingRequestId + ", briefTrainingContent="
				+ briefTrainingContent + ", comments=" + comments
				+ ", expectedTrainingStartDate=" + expectedTrainingStartDate
				+ ", externalTrainerRequest=" + externalTrainerRequest
				+ ", isSoftwareLicenceAvailable=" + isSoftwareLicenceAvailable
				+ ", isTrainingProgramCreated=" + isTrainingProgramCreated
				+ ", levelOfParticipants=" + levelOfParticipants
				+ ", levelOfTrainingRequest=" + levelOfTrainingRequest
				+ ", noOfLicensesAvailbale=" + noOfLicensesAvailbale
				+ ", noOfParticipants=" + noOfParticipants
				+ ", preferredTrainingEndTime=" + preferredTrainingEndTime
				+ ", preferredTrainingStartTime=" + preferredTrainingStartTime
				+ ", productVersionForTraining=" + productVersionForTraining
				+ ", specificInputComments=" + specificInputComments
				+ ", trainingModifiedDate=" + trainingModifiedDate
				+ ", trainingRequestedDate=" + trainingRequestedDate
				+ ", usageOfProduct=" + usageOfProduct
				+ ", fkTrainingRequestStatusId=" + fkTrainingRequestStatusId
				+ ", fkTrainingRequesterEmpId=" + fkTrainingRequesterEmpId
				+ ", fkTrainingTopicNameId=" + fkTrainingTopicNameId
				+ ", programCategory=" + programCategory
				+ ", pkTrainingProgrammeId=" + pkTrainingProgrammeId
				+ ", externalVenueName=" + externalVenueName
				+ ", maximumSeats=" + maximumSeats + ", programmeCreatedDate="
				+ programmeCreatedDate + ", programmeModifiedDate="
				+ programmeModifiedDate + ", trainerName=" + trainerName
				+ ", trainerType=" + trainerType + ", trainingDurationInDays="
				+ trainingDurationInDays + ", trainingDurationInHours="
				+ trainingDurationInHours + ", trainingEndDateTime="
				+ trainingEndDateTime + ", trainingIntimatedStatus="
				+ trainingIntimatedStatus + ", trainingNoOfHrsPerDay="
				+ trainingNoOfHrsPerDay + ", trainingStartDateTime="
				+ trainingStartDateTime + ", fkProgrammeCreatedBy="
				+ fkProgrammeCreatedBy + ", fkProgrammeModifyBy="
				+ fkProgrammeModifyBy + ", fkProgrammeStatusId="
				+ fkProgrammeStatusId + ", fkTrainingRequestId="
				+ fkTrainingRequestId + ", fkTrainingRoomId="
				+ fkTrainingRoomId + ", pgmDetcomments=" + pgmDetcomments + "]";
	}

}
