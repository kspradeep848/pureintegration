/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingPostApplicationFeedbackInputBean.java     */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  November 02, 2016                                 */
/*                                                                   */
/*  Description :  This class contains for the inputs required for   */
/*                   Training Post Application Feedback Input details*/
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 02, 2016     THBS     1.0        Initial version created */
/*********************************************************************/

package com.thbs.mis.training.bean;

public class TrainingPostApplicationFeedbackInputBean {
	
	String feedbackStatus;

	public String getFeedbackStatus() {
		return feedbackStatus;
	}

	public void setFeedbackStatus(String feedbackStatus) {
		this.feedbackStatus = feedbackStatus;
	}
	
	

}
