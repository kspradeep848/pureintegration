package com.thbs.mis.training.bean;

import java.util.List;

public class AttendedListBean {
	
	private int attendeeEmpId;
	private String attendeeEmpName;
	private List<FeedbackListBean> feedbackList;
	
	public int getAttendeeEmpId() {
		return attendeeEmpId;
	}


	public void setAttendeeEmpId(int attendeeEmpId) {
		this.attendeeEmpId = attendeeEmpId;
	}


	public String getAttendeeEmpName() {
		return attendeeEmpName;
	}


	public void setAttendeeEmpName(String attendeeEmpName) {
		this.attendeeEmpName = attendeeEmpName;
	}


	public List<FeedbackListBean> getFeedbackList() {
		return feedbackList;
	}


	public void setFeedbackList(List<FeedbackListBean> feedbackList) {
		this.feedbackList = feedbackList;
	}


	@Override
	public String toString() {
		return "AttendedListBean [attendeeEmpId=" + attendeeEmpId
				+ ", attendeeEmpName=" + attendeeEmpName + ", feedbackList="
				+ feedbackList + "]";
	}
	

}
