/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingUpdateNominationPoolDetailsBean.java      */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  17-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	TrainingUpdateNominationPoolDetailsBean.         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 17-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class TrainingUpdateNominationPoolDetailsBean {

	private byte nominationStatus;
	
	@Min(1)
	@NotNull(message = "trainingProgId should not be null")
	@NumberFormat(style = Style.NUMBER)
	private Integer trainingProgId;
	
	@Min(1)
	@NotNull(message = "nominatedEmpId should not be null")
	@NumberFormat(style = Style.NUMBER)
	private Integer nominatedEmpId;
	
	@Min(1)
	@NotNull(message = "rmId should not be null")
	@NumberFormat(style = Style.NUMBER)
	private Integer rmId;

	public byte getNominationStatus() {
		return nominationStatus;
	}

	public void setNominationStatus(byte nominationStatus) {
		this.nominationStatus = nominationStatus;
	}

	public Integer getTrainingProgId() {
		return trainingProgId;
	}

	public void setTrainingProgId(Integer trainingProgId) {
		this.trainingProgId = trainingProgId;
	}

	public Integer getNominatedEmpId() {
		return nominatedEmpId;
	}

	public void setNominatedEmpId(Integer nominatedEmpId) {
		this.nominatedEmpId = nominatedEmpId;
	}

	public Integer getRmId() {
		return rmId;
	}

	public void setRmId(Integer rmId) {
		this.rmId = rmId;
	}

	@Override
	public String toString() {
		return "TrainingUpdateNominationPoolDetailsBean [nominationStatus="
				+ nominationStatus + ", trainingProgId=" + trainingProgId
				+ ", nominatedEmpId=" + nominatedEmpId + "]";
	}

}
