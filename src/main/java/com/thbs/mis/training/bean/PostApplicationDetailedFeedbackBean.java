/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  PostApplicationDetailedFeedbackBean.java          */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  17-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	PostApplicationDetailedFeedbackBean.             */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 17-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PostApplicationDetailedFeedbackBean {

	private int progId;
	private String progTitle;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date progStartDate;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date progEndDate;
	private String trainerName;
	private String venue;
	// private List<QuestionAndAnswerListBean> questionAnswerList;
	private List<AttendedListBean> attendedList;

	public Date getProgEndDate() {
		return progEndDate;
	}

	public void setProgEndDate(Date progEndDate) {
		this.progEndDate = progEndDate;
	}

	public String getTrainerName() {
		return trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public List<AttendedListBean> getAttendedList() {
		return attendedList;
	}

	public void setAttendedList(List<AttendedListBean> attendedList) {
		this.attendedList = attendedList;
	}

	public int getProgId() {
		return progId;
	}

	public void setProgId(int progId) {
		this.progId = progId;
	}

	public String getProgTitle() {
		return progTitle;
	}

	public void setProgTitle(String progTitle) {
		this.progTitle = progTitle;
	}

	public Date getProgStartDate() {
		return progStartDate;
	}

	public void setProgStartDate(Date progStartDate) {
		this.progStartDate = progStartDate;
	}

	@Override
	public String toString() {
		return "PostApplicationDetailedFeedbackBean [progId=" + progId
				+ ", progTitle=" + progTitle + ", progStartDate="
				+ progStartDate + ", progEndDate=" + progEndDate
				+ ", trainerName=" + trainerName + ", venue=" + venue
				+ ", attendedList=" + attendedList + "]";
	}

}
