/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  TrainingDetailedFeedbackOutputBean.java                                      */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/


package com.thbs.mis.training.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class TrainingDetailedFeedbackOutputBean {

	private String trainingTitle;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date trainingStartDate;
	private int empId;
	private String empName;
	private String empEmailId;
	private String comments;
	private List<FeedbackResponsesBean> feedbackList;
	private int progId;
	
	
	public int getProgId() {
		return progId;
	}
	public void setProgId(int progId) {
		this.progId = progId;
	}
	public String getTrainingTitle() {
		return trainingTitle;
	}
	public void setTrainingTitle(String trainingTitle) {
		this.trainingTitle = trainingTitle;
	}
	public Date getTrainingStartDate() {
		return trainingStartDate;
	}
	public void setTrainingStartDate(Date trainingStartDate) {
		this.trainingStartDate = trainingStartDate;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpEmailId() {
		return empEmailId;
	}
	public void setEmpEmailId(String empEmailId) {
		this.empEmailId = empEmailId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public List<FeedbackResponsesBean> getFeedbackList() {
		return feedbackList;
	}
	public void setFeedbackList(List<FeedbackResponsesBean> feedbackList) {
		this.feedbackList = feedbackList;
	}
	@Override
	public String toString() {
		return "TrainingDetailedFeedbackOutputBean [trainingTitle="
				+ trainingTitle + ", trainingStartDate=" + trainingStartDate
				+ ", empId=" + empId + ", empName=" + empName + ", empEmailId="
				+ empEmailId + ", comments=" + comments + ", feedbackList="
				+ feedbackList + ", progId=" + progId + "]";
	}


}
