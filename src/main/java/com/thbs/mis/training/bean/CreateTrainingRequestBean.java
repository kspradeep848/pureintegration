/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  CreateTrainingRequestBean.java                    */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 Create Training Request.                          */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 09-Nov-2016     THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.training.bean;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class CreateTrainingRequestBean {

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Requestor Employee Id should be a numeric value")
	private int requestorEmpId;

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Training topic Id should be a numeric value")
	private int trainingTopicId;

	@Min(1)
	@Max(3)
	@NumberFormat(style = Style.NUMBER, pattern = "Level of Training request should be a numeric value")
	private byte levelTrainingRequest;

	//@Length(max = 50, message = "Product Version can't exceed 50 chars")
	private String productVersion;

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Number of Participants must be a numeric value")
	private int noOfParticipants;

	@NotEmpty(message = "Level of Participants cannot be empty")
	@NotNull(message = "Level of Participants cannot be null")
	@Length(max = 150, message = "Level of Participants can't exceed 150 chars")
	private String levelOfParticipants;

	/*@Min(1)
	@Max(3)
	@NumberFormat(style = Style.NUMBER, pattern = "Usage of Product must be a numeric value")*/
	private Byte usageOfProduct;

	@NotEmpty(message = "Brief Training Content cannot be empty")
	@NotNull(message = "Brief Training Content cannot be null")
	@Length(max = 300, message = "Level of Participants can't exceed 300 chars")
	private String briefTrainingContent;

	/*@NotEmpty(message = "Is Software Licenses Available cannot be empty")
	@NotNull(message = "Is Software Licenses Available cannot be null")*/
	private String isSoftwareLicenseAvailable;

	//@NumberFormat(style = Style.NUMBER)
	private Integer noOfLicensesAvailable;

	@NotNull(message = "Expected Training start date field can not be null")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date expectedTrainingStartDate;

	@NotNull(message = "preferredTrainingStartTime Description should not be null")
	private Time preferredTrainingStartTime;

	@NotNull(message = "preferredTrainingEndTime Description should not be null")
	private Time preferredTrainingEndTime;

	private String externalTrainerRequest;

	//@Length(max = 250, message = "Specific Input Comments can't exceed 300 chars")
	private String specificInputComments;

	@Length(max = 250, message = "Comments can't exceed 250 chars")
	private String comments;
	
	@Min(1)
	@Max(3)
	@NumberFormat(style = Style.NUMBER, pattern = "Training Program Category should be a numeric value")
	private Byte programCategory;

	public Byte getProgramCategory() {
		return programCategory;
	}

	public void setProgramCategory(Byte programCategory) {
		this.programCategory = programCategory;
	}

	public int getRequestorEmpId() {
		return requestorEmpId;
	}

	public void setRequestorEmpId(int requestorEmpId) {
		this.requestorEmpId = requestorEmpId;
	}

	public int getTrainingTopicId() {
		return trainingTopicId;
	}

	public void setTrainingTopicId(int trainingTopicId) {
		this.trainingTopicId = trainingTopicId;
	}

	public byte getLevelTrainingRequest() {
		return levelTrainingRequest;
	}

	public void setLevelTrainingRequest(byte levelTrainingRequest) {
		this.levelTrainingRequest = levelTrainingRequest;
	}

	public String getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	public int getNoOfParticipants() {
		return noOfParticipants;
	}

	public void setNoOfParticipants(int noOfParticipants) {
		this.noOfParticipants = noOfParticipants;
	}

	public String getLevelOfParticipants() {
		return levelOfParticipants;
	}

	public void setLevelOfParticipants(String levelOfParticipants) {
		this.levelOfParticipants = levelOfParticipants;
	}

	public Byte getUsageOfProduct() {
		return usageOfProduct;
	}

	public void setUsageOfProduct(Byte usageOfProduct) {
		this.usageOfProduct = usageOfProduct;
	}

	public String getBriefTrainingContent() {
		return briefTrainingContent;
	}

	public void setBriefTrainingContent(String briefTrainingContent) {
		this.briefTrainingContent = briefTrainingContent;
	}

	public String getIsSoftwareLicenseAvailable() {
		return isSoftwareLicenseAvailable;
	}

	public void setIsSoftwareLicenseAvailable(String isSoftwareLicenseAvailable) {
		this.isSoftwareLicenseAvailable = isSoftwareLicenseAvailable;
	}

	public Integer getNoOfLicensesAvailable() {
		return noOfLicensesAvailable;
	}

	public void setNoOfLicensesAvailable(Integer noOfLicensesAvailable) {
		this.noOfLicensesAvailable = noOfLicensesAvailable;
	}

	public Date getExpectedTrainingStartDate() {
		return expectedTrainingStartDate;
	}

	public void setExpectedTrainingStartDate(Date expectedTrainingStartDate) {
		this.expectedTrainingStartDate = expectedTrainingStartDate;
	}

	public Time getPreferredTrainingStartTime() {
		return preferredTrainingStartTime;
	}

	public void setPreferredTrainingStartTime(Time preferredTrainingStartTime) {
		this.preferredTrainingStartTime = preferredTrainingStartTime;
	}

	public Time getPreferredTrainingEndTime() {
		return preferredTrainingEndTime;
	}

	public void setPreferredTrainingEndTime(Time preferredTrainingEndTime) {
		this.preferredTrainingEndTime = preferredTrainingEndTime;
	}

	public String getExternalTrainerRequest() {
		return externalTrainerRequest;
	}

	public void setExternalTrainerRequest(String externalTrainerRequest) {
		this.externalTrainerRequest = externalTrainerRequest;
	}

	public String getSpecificInputComments() {
		return specificInputComments;
	}

	public void setSpecificInputComments(String specificInputComments) {
		this.specificInputComments = specificInputComments;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "CreateTrainingRequestBean [requestorEmpId=" + requestorEmpId
				+ ", trainingTopicId=" + trainingTopicId
				+ ", levelTrainingRequest=" + levelTrainingRequest
				+ ", productVersion=" + productVersion + ", noOfParticipants="
				+ noOfParticipants + ", levelOfParticipants="
				+ levelOfParticipants + ", usageOfProduct=" + usageOfProduct
				+ ", briefTrainingContent=" + briefTrainingContent
				+ ", isSoftwareLicenseAvailable=" + isSoftwareLicenseAvailable
				+ ", noOfLicensesAvailable=" + noOfLicensesAvailable
				+ ", expectedTrainingStartDate=" + expectedTrainingStartDate
				+ ", preferredTrainingStartTime=" + preferredTrainingStartTime
				+ ", preferredTrainingEndTime=" + preferredTrainingEndTime
				+ ", externalTrainerRequest=" + externalTrainerRequest
				+ ", specificInputComments=" + specificInputComments
				+ ", comments=" + comments + ", programCategory="
				+ programCategory + "]";
	}

}
