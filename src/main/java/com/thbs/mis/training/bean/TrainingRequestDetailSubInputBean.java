/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  TrainingRequestDetailSubInputBean.java                                        */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                           */
/*  Description :  This class contains all the fields                                                           */
/*                 used for the required BO			                                                                              */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/
package com.thbs.mis.training.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;

import org.springframework.format.annotation.DateTimeFormat;

public class TrainingRequestDetailSubInputBean {

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date endDate;

	@Min(1)
	private int rmMgrId;
	private int status;
	private int trainingRequestId;
	private String isTrainingCreated;
	private String comments;
	private int trainingTopicId;
	private String briefDescription; //
	private int progId; //
	private String feedbackStatus; //
	private int pkTrainingRequestId;
	
	public int getProgramId() {
		return programId;
	}

	public void setProgramId(int programId) {
		this.programId = programId;
	}

	private int programId;

	public int getPkTrainingRequestId() {
		return pkTrainingRequestId;
	}

	public void setPkTrainingRequestId(int pkTrainingRequestId) {
		this.pkTrainingRequestId = pkTrainingRequestId;
	}

	public String getFeedbackStatus() {
		return feedbackStatus;
	}

	public void setFeedbackStatus(String feedbackStatus) {
		this.feedbackStatus = feedbackStatus;
	}

	public int getProgId() {
		return progId;
	}

	public void setProgId(int progId) {
		this.progId = progId;
	}

	public String getBriefDescription() {
		return briefDescription;
	}

	public void setBriefDescription(String briefDescription) {
		this.briefDescription = briefDescription;
	}

	public int getTrainingTopicId() {
		return trainingTopicId;
	}

	public void setTrainingTopicId(int trainingTopicId) {
		this.trainingTopicId = trainingTopicId;
	}

	public String getIsTrainingCreated() {
		return isTrainingCreated;
	}

	public void setIsTrainingCreated(String isTrainingCreated) {
		this.isTrainingCreated = isTrainingCreated;
	}

	public int getTrainingRequestId() {
		return trainingRequestId;
	}

	public void setTrainingRequestId(int trainingRequestId) {
		this.trainingRequestId = trainingRequestId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getRmMgrId() {
		return rmMgrId;
	}

	public void setRmMgrId(int rmMgrId) {
		this.rmMgrId = rmMgrId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "TrainingRequestDetailSubInputBean [startDate=" + startDate
				+ ", endDate=" + endDate + ", rmMgrId=" + rmMgrId + ", status="
				+ status + ", trainingRequestId=" + trainingRequestId
				+ ", isTrainingCreated=" + isTrainingCreated + ", comments="
				+ comments + ", trainingTopicId=" + trainingTopicId
				+ ", briefDescription=" + briefDescription + ", progId="
				+ progId + ", feedbackStatus=" + feedbackStatus
				+ ", pkTrainingRequestId=" + pkTrainingRequestId
				+ ", programId=" + programId + "]";
	}



	
}
