package com.thbs.mis.training.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;

/**
 * The persistent class for the training_dat_nomination_pool_detail
 * database table.
 * 
 */
@Entity
@Table(name = "training_dat_nomination_pool_detail")
@NamedQuery(name = "TrainingDatNominationPoolDetailBO.findAll", query = "SELECT t FROM TrainingDatNominationPoolDetailBO t")
public class TrainingDatNominationPoolDetailBO implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_training_nomination_pool_detail_id")
	private Integer pkTrainingNominationPoolDetailId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved_or_rejected_date")
	private Date approvedOrRejectedDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "choice_modified_date")
	private Date choiceModifiedDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "nomination_pool_created_date")
	private Date nominationPoolCreatedDate;

	@Column(name = "nomination_status")
	private byte nominationStatus;

	@Column(name = "rejected_comments")
	private String rejectedComments;

	@Column(name = "fk_approved_or_rejected_by")
	private Integer fkApprovedOrRejectedBy;

	@Column(name = "fk_nominated_emp_id")
	private Integer fkNominatedEmpId;

	@Column(name = "fk_training_programme_id")
	private Integer fkTrainingProgrammeId;

	@OneToOne
	@JoinColumn(name = "fk_approved_or_rejected_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOApprovedOrRejectedBy;

	@OneToOne
	@JoinColumn(name = "fk_nominated_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBONominatedEmpId;

	@OneToOne
	@JoinColumn(name = "fk_training_programme_id", unique = true, nullable = true, insertable = false, updatable = false)
	private TrainingDatProgrammeDetailBO trainingDatProgrammeDetail;
	
	@Column(name = "fk_approver_emp_id")
	private Integer fkApproverEmpId;

	public Integer getFkApproverEmpId() {
		return fkApproverEmpId;
	}

	public void setFkApproverEmpId(Integer fkApproverEmpId) {
		this.fkApproverEmpId = fkApproverEmpId;
	}

	public TrainingDatNominationPoolDetailBO()
	{
	}

	public Integer getPkTrainingNominationPoolDetailId()
	{
		return this.pkTrainingNominationPoolDetailId;
	}

	public void setPkTrainingNominationPoolDetailId(
			Integer pkTrainingNominationPoolDetailId)
	{
		this.pkTrainingNominationPoolDetailId = pkTrainingNominationPoolDetailId;
	}

	public Date getApprovedOrRejectedDate()
	{
		return this.approvedOrRejectedDate;
	}

	public void setApprovedOrRejectedDate(Date approvedOrRejectedDate)
	{
		this.approvedOrRejectedDate = approvedOrRejectedDate;
	}

	public Date getChoiceModifiedDate()
	{
		return this.choiceModifiedDate;
	}

	public void setChoiceModifiedDate(Date choiceModifiedDate)
	{
		this.choiceModifiedDate = choiceModifiedDate;
	}

	public Date getNominationPoolCreatedDate()
	{
		return this.nominationPoolCreatedDate;
	}

	public void setNominationPoolCreatedDate(
			Date nominationPoolCreatedDate)
	{
		this.nominationPoolCreatedDate = nominationPoolCreatedDate;
	}

	public byte getNominationStatus()
	{
		return this.nominationStatus;
	}

	public void setNominationStatus(byte nominationStatus)
	{
		this.nominationStatus = nominationStatus;
	}

	public String getRejectedComments()
	{
		return this.rejectedComments;
	}

	public void setRejectedComments(String rejectedComments)
	{
		this.rejectedComments = rejectedComments;
	}

	public Integer getFkApprovedOrRejectedBy() {
		return fkApprovedOrRejectedBy;
	}

	public void setFkApprovedOrRejectedBy(Integer fkApprovedOrRejectedBy) {
		this.fkApprovedOrRejectedBy = fkApprovedOrRejectedBy;
	}

	public Integer getFkNominatedEmpId() {
		return fkNominatedEmpId;
	}

	public void setFkNominatedEmpId(Integer fkNominatedEmpId) {
		this.fkNominatedEmpId = fkNominatedEmpId;
	}

	public Integer getFkTrainingProgrammeId() {
		return fkTrainingProgrammeId;
	}

	public void setFkTrainingProgrammeId(Integer fkTrainingProgrammeId) {
		this.fkTrainingProgrammeId = fkTrainingProgrammeId;
	}

	public DatEmpDetailBO getDatEmpDetailBOApprovedOrRejectedBy() {
		return datEmpDetailBOApprovedOrRejectedBy;
	}

	public void setDatEmpDetailBOApprovedOrRejectedBy(
			DatEmpDetailBO datEmpDetailBOApprovedOrRejectedBy) {
		this.datEmpDetailBOApprovedOrRejectedBy = datEmpDetailBOApprovedOrRejectedBy;
	}

	public DatEmpDetailBO getDatEmpDetailBONominatedEmpId() {
		return datEmpDetailBONominatedEmpId;
	}

	public void setDatEmpDetailBONominatedEmpId(
			DatEmpDetailBO datEmpDetailBONominatedEmpId) {
		this.datEmpDetailBONominatedEmpId = datEmpDetailBONominatedEmpId;
	}

	public TrainingDatProgrammeDetailBO getTrainingDatProgrammeDetail() {
		return trainingDatProgrammeDetail;
	}

	public void setTrainingDatProgrammeDetail(
			TrainingDatProgrammeDetailBO trainingDatProgrammeDetail) {
		this.trainingDatProgrammeDetail = trainingDatProgrammeDetail;
	}

	@Override
	public String toString() {
		return "TrainingDatNominationPoolDetailBO [pkTrainingNominationPoolDetailId="
				+ pkTrainingNominationPoolDetailId
				+ ", approvedOrRejectedDate="
				+ approvedOrRejectedDate
				+ ", choiceModifiedDate="
				+ choiceModifiedDate
				+ ", nominationPoolCreatedDate="
				+ nominationPoolCreatedDate
				+ ", nominationStatus="
				+ nominationStatus
				+ ", rejectedComments="
				+ rejectedComments
				+ ", fkApprovedOrRejectedBy="
				+ fkApprovedOrRejectedBy
				+ ", fkNominatedEmpId="
				+ fkNominatedEmpId
				+ ", fkTrainingProgrammeId="
				+ fkTrainingProgrammeId
				+ ", datEmpDetailBOApprovedOrRejectedBy="
				+ datEmpDetailBOApprovedOrRejectedBy
				+ ", datEmpDetailBONominatedEmpId="
				+ datEmpDetailBONominatedEmpId
				+ ", trainingDatProgrammeDetail="
				+ trainingDatProgrammeDetail
				+ ", fkApproverEmpId=" + fkApproverEmpId + "]";
	}

}