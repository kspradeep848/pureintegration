/*********************************************************************/
/*                  FILE HEADER                                                                                                            */
/*********************************************************************/
/*                                                                                                                                                           */
/*  FileName    :  TrainingDatAttendeeFeedbackBO.java                                               */
/*                                                                                                                                                          */
/*  Author      :  THBS		                                                                                                               */
/*                                                                                                                                                          */
/*  Date        :  23-Jun-2016                                                                                                        */
/*                                                                                                                                                          */
/*  Description :  This class contains all the fields used                                                  */
/*                for the TrainingDatAttendeeFeedbackBO 		                                                */
/*                                                                                                                                                           */
/*                                                                                                                                                            */
/*********************************************************************/
/* Date        Who     Version      Comments             			                                                         */
/*----------------------------------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0      Initial version created   	 	                                         */
/*********************************************************************/

package com.thbs.mis.training.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasFeedbackQuestionInfoBO;

/**
 * The persistent class for the training_dat_attendee_feedback database
 * table.
 * 
 */
@Entity
@Table(name = "training_dat_attendee_feedback")
@NamedQuery(name = "TrainingDatAttendeeFeedbackBO.findAll", query = "SELECT t FROM TrainingDatAttendeeFeedbackBO t")
public class TrainingDatAttendeeFeedbackBO implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_training_attendee_feedback_id")
	private Integer pkTrainingAttendeeFeedbackId;

	@Column(name = "attendee_answer")
	private String attendeeAnswer;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "attendee_feedback_created_date")
	private Date attendeeFeedbackCreatedDate;

	@Column(name = "fk_attendee_emp_id")
	private Integer fkAttendeeEmpId;

	@Column(name = "fk_dat_feedback_question_info_id")
	private Integer fkDatFeedbackQuestionInfoId;

	@Column(name = "fk_training_dat_programme_id")
	private Integer fkTrainingDatProgrammeId;

	@OneToOne
	@JoinColumn(name = "fk_attendee_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailAttendeeEmpId;

	@OneToOne
	@JoinColumn(name = "fk_dat_feedback_question_info_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasFeedbackQuestionInfoBO masFeedbackQuestionInfo;

	@OneToOne
	@JoinColumn(name = "fk_training_dat_programme_id", unique = true, nullable = true, insertable = false, updatable = false)
	private TrainingDatProgrammeDetailBO trainingDatProgrammeDetail;

	public TrainingDatAttendeeFeedbackBO()
	{
	}

	public Integer getPkTrainingAttendeeFeedbackId()
	{
		return this.pkTrainingAttendeeFeedbackId;
	}

	public void setPkTrainingAttendeeFeedbackId(
			Integer pkTrainingAttendeeFeedbackId)
	{
		this.pkTrainingAttendeeFeedbackId = pkTrainingAttendeeFeedbackId;
	}

	public String getAttendeeAnswer()
	{
		return this.attendeeAnswer;
	}

	public void setAttendeeAnswer(String attendeeAnswer)
	{
		this.attendeeAnswer = attendeeAnswer;
	}

	public Date getAttendeeFeedbackCreatedDate()
	{
		return this.attendeeFeedbackCreatedDate;
	}

	public void setAttendeeFeedbackCreatedDate(
			Date attendeeFeedbackCreatedDate)
	{
		this.attendeeFeedbackCreatedDate = attendeeFeedbackCreatedDate;
	}

	public Integer getFkAttendeeEmpId()
	{
		return fkAttendeeEmpId;
	}

	public void setFkAttendeeEmpId(Integer fkAttendeeEmpId)
	{
		this.fkAttendeeEmpId = fkAttendeeEmpId;
	}

	public Integer getFkDatFeedbackQuestionInfoId()
	{
		return fkDatFeedbackQuestionInfoId;
	}

	public void setFkDatFeedbackQuestionInfoId(
			Integer fkDatFeedbackQuestionInfoId)
	{
		this.fkDatFeedbackQuestionInfoId = fkDatFeedbackQuestionInfoId;
	}

	public Integer getFkTrainingDatProgrammeId()
	{
		return fkTrainingDatProgrammeId;
	}

	public void setFkTrainingDatProgrammeId(Integer fkTrainingDatProgrammeId)
	{
		this.fkTrainingDatProgrammeId = fkTrainingDatProgrammeId;
	}

	public DatEmpDetailBO getDatEmpDetailAttendeeEmpId()
	{
		return datEmpDetailAttendeeEmpId;
	}

	public void setDatEmpDetailAttendeeEmpId(
			DatEmpDetailBO datEmpDetailAttendeeEmpId)
	{
		this.datEmpDetailAttendeeEmpId = datEmpDetailAttendeeEmpId;
	}

	public MasFeedbackQuestionInfoBO getMasFeedbackQuestionInfo()
	{
		return masFeedbackQuestionInfo;
	}

	public void setMasFeedbackQuestionInfo(
			MasFeedbackQuestionInfoBO masFeedbackQuestionInfo)
	{
		this.masFeedbackQuestionInfo = masFeedbackQuestionInfo;
	}

	public TrainingDatProgrammeDetailBO getTrainingDatProgrammeDetail()
	{
		return trainingDatProgrammeDetail;
	}

	public void setTrainingDatProgrammeDetail(
			TrainingDatProgrammeDetailBO trainingDatProgrammeDetail)
	{
		this.trainingDatProgrammeDetail = trainingDatProgrammeDetail;
	}

	@Override
	public String toString()
	{
		return "TrainingDatAttendeeFeedbackBO [pkTrainingAttendeeFeedbackId="
				+ pkTrainingAttendeeFeedbackId
				+ ", attendeeAnswer="
				+ attendeeAnswer
				+ ", attendeeFeedbackCreatedDate="
				+ attendeeFeedbackCreatedDate
				+ ", fkAttendeeEmpId="
				+ fkAttendeeEmpId
				+ ", fkDatFeedbackQuestionInfoId="
				+ fkDatFeedbackQuestionInfoId
				+ ", fkTrainingDatProgrammeId="
				+ fkTrainingDatProgrammeId
				+ ", datEmpDetailAttendeeEmpId="
				+ datEmpDetailAttendeeEmpId
				+ ", masFeedbackQuestionInfo="
				+ masFeedbackQuestionInfo
				+ ", trainingDatProgrammeDetail="
				+ trainingDatProgrammeDetail + "]";
	}

}