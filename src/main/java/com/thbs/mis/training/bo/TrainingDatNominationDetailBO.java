package com.thbs.mis.training.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;

import java.util.Date;

/**
 * The persistent class for the training_dat_nomination_detail database
 * table.
 * 
 */
@Entity
@Table(name = "training_dat_nomination_detail")
@NamedQuery(name = "TrainingDatNominationDetailBO.findAll", query = "SELECT t FROM TrainingDatNominationDetailBO t")
public class TrainingDatNominationDetailBO implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_training_nomination_detail_id")
	private Integer pkTrainingNominationDetailId;

	@Column(name = "attended_training")
	private String attendedTraining;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "attendence_update_date")
	private Date attendenceUpdateDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "nomination_date")
	private Date nominationDate;

	@Column(name = "nomination_status")
	private String nominationStatus;

	@Column(name = "fk_attendence_update_by")
	private Integer fkAttendenceUpdateBy;

	@Column(name = "fk_nominated_by_emp_id")
	private Integer fkNominatedByEmpId;

	@Column(name = "fk_dat_nominated_emp_id")
	private Integer fkDatNominatedEmpId;

	@Column(name = "fk_nomination_pool_id")
	private Integer fkNominationPoolId;

	@Column(name = "fk_dat_training_programme_id")
	private Integer fkDatTrainingProgrammeId;
	
	// Kamal
	@Column(name = "is_feedback_provided")
	private String isFeedbackProvided;
	// Kamal
	
	@OneToOne
	@JoinColumn(name = "fk_attendence_update_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailAttendenceUpdatedBy;

	@OneToOne
	@JoinColumn(name = "fk_dat_nominated_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailNominatedEmpId;

	@OneToOne
	@JoinColumn(name = "fk_nominated_by_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailNominatedByEmpid;

	@OneToOne
	@JoinColumn(name = "fk_nomination_pool_id", unique = true, nullable = true, insertable = false, updatable = false)
	private TrainingDatNominationPoolDetailBO trainingDatNominationPoolDetail;

	@OneToOne
	@JoinColumn(name = "fk_dat_training_programme_id", unique = true, nullable = true, insertable = false, updatable = false)
	private TrainingDatProgrammeDetailBO trainingDatProgrammeDetail;

	public TrainingDatNominationDetailBO()
	{
	}
	public String getIsFeedbackProvided() {
		return isFeedbackProvided;
	}

	public void setIsFeedbackProvided(String isFeedbackProvided) {
		this.isFeedbackProvided = isFeedbackProvided;
	}
	public Integer getPkTrainingNominationDetailId()
	{
		return this.pkTrainingNominationDetailId;
	}

	public void setPkTrainingNominationDetailId(
			Integer pkTrainingNominationDetailId)
	{
		this.pkTrainingNominationDetailId = pkTrainingNominationDetailId;
	}

	public String getAttendedTraining()
	{
		return this.attendedTraining;
	}

	public void setAttendedTraining(String attendedTraining)
	{
		this.attendedTraining = attendedTraining;
	}

	public Date getAttendenceUpdateDate()
	{
		return this.attendenceUpdateDate;
	}

	public void setAttendenceUpdateDate(Date attendenceUpdateDate)
	{
		this.attendenceUpdateDate = attendenceUpdateDate;
	}

	public Date getNominationDate()
	{
		return this.nominationDate;
	}

	public void setNominationDate(Date nominationDate)
	{
		this.nominationDate = nominationDate;
	}

	public String getNominationStatus()
	{
		return this.nominationStatus;
	}

	public void setNominationStatus(String nominationStatus)
	{
		this.nominationStatus = nominationStatus;
	}

	public Integer getFkAttendenceUpdateBy()
	{
		return fkAttendenceUpdateBy;
	}

	public void setFkAttendenceUpdateBy(Integer fkAttendenceUpdateBy)
	{
		this.fkAttendenceUpdateBy = fkAttendenceUpdateBy;
	}

	public Integer getFkNominatedByEmpId()
	{
		return fkNominatedByEmpId;
	}

	public void setFkNominatedByEmpId(Integer fkNominatedByEmpId)
	{
		this.fkNominatedByEmpId = fkNominatedByEmpId;
	}

	public Integer getFkDatNominatedEmpId()
	{
		return fkDatNominatedEmpId;
	}

	public void setFkDatNominatedEmpId(Integer fkDatNominatedEmpId)
	{
		this.fkDatNominatedEmpId = fkDatNominatedEmpId;
	}

	public Integer getFkNominationPoolId()
	{
		return fkNominationPoolId;
	}

	public void setFkNominationPoolId(Integer fkNominationPoolId)
	{
		this.fkNominationPoolId = fkNominationPoolId;
	}

	public Integer getFkDatTrainingProgrammeId()
	{
		return fkDatTrainingProgrammeId;
	}

	public void setFkDatTrainingProgrammeId(Integer fkDatTrainingProgrammeId)
	{
		this.fkDatTrainingProgrammeId = fkDatTrainingProgrammeId;
	}

	public DatEmpDetailBO getDatEmpDetailAttendenceUpdatedBy()
	{
		return datEmpDetailAttendenceUpdatedBy;
	}

	public void setDatEmpDetailAttendenceUpdatedBy(
			DatEmpDetailBO datEmpDetailAttendenceUpdatedBy)
	{
		this.datEmpDetailAttendenceUpdatedBy = datEmpDetailAttendenceUpdatedBy;
	}

	public DatEmpDetailBO getDatEmpDetailNominatedEmpId()
	{
		return datEmpDetailNominatedEmpId;
	}

	public void setDatEmpDetailNominatedEmpId(
			DatEmpDetailBO datEmpDetailNominatedEmpId)
	{
		this.datEmpDetailNominatedEmpId = datEmpDetailNominatedEmpId;
	}

	public DatEmpDetailBO getDatEmpDetailNominatedByEmpid()
	{
		return datEmpDetailNominatedByEmpid;
	}

	public void setDatEmpDetailNominatedByEmpid(
			DatEmpDetailBO datEmpDetailNominatedByEmpid)
	{
		this.datEmpDetailNominatedByEmpid = datEmpDetailNominatedByEmpid;
	}

	public TrainingDatNominationPoolDetailBO getTrainingDatNominationPoolDetail()
	{
		return trainingDatNominationPoolDetail;
	}

	public void setTrainingDatNominationPoolDetail(
			TrainingDatNominationPoolDetailBO trainingDatNominationPoolDetail)
	{
		this.trainingDatNominationPoolDetail = trainingDatNominationPoolDetail;
	}

	public TrainingDatProgrammeDetailBO getTrainingDatProgrammeDetail()
	{
		return trainingDatProgrammeDetail;
	}

	public void setTrainingDatProgrammeDetail(
			TrainingDatProgrammeDetailBO trainingDatProgrammeDetail)
	{
		this.trainingDatProgrammeDetail = trainingDatProgrammeDetail;
	}

	@Override
	public String toString() {
		return "TrainingDatNominationDetailBO [pkTrainingNominationDetailId="
				+ pkTrainingNominationDetailId + ", attendedTraining="
				+ attendedTraining + ", attendenceUpdateDate="
				+ attendenceUpdateDate + ", nominationDate=" + nominationDate
				+ ", nominationStatus=" + nominationStatus
				+ ", fkAttendenceUpdateBy=" + fkAttendenceUpdateBy
				+ ", fkNominatedByEmpId=" + fkNominatedByEmpId
				+ ", fkDatNominatedEmpId=" + fkDatNominatedEmpId
				+ ", fkNominationPoolId=" + fkNominationPoolId
				+ ", fkDatTrainingProgrammeId=" + fkDatTrainingProgrammeId
				+ ", isFeedbackProvided=" + isFeedbackProvided
				+ ", datEmpDetailAttendenceUpdatedBy="
				+ datEmpDetailAttendenceUpdatedBy
				+ ", datEmpDetailNominatedEmpId=" + datEmpDetailNominatedEmpId
				+ ", datEmpDetailNominatedByEmpid="
				+ datEmpDetailNominatedByEmpid
				+ ", trainingDatNominationPoolDetail="
				+ trainingDatNominationPoolDetail
				+ ", trainingDatProgrammeDetail=" + trainingDatProgrammeDetail
				+ "]";
	}

}