package com.thbs.mis.training.bo;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasTrainingTopicNameBO;

/**
 * The persistent class for the training_dat_request_detail database table.
 * 
 */
@Entity
@Table(name = "training_dat_request_detail")
@NamedQuery(name = "TrainingDatRequestDetailBO.findAll", query = "SELECT t FROM TrainingDatRequestDetailBO t")
public class TrainingDatRequestDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_training_request_id")
	private Integer pkTrainingRequestId;

	@Column(name = "brief_training_content")
	private String briefTrainingContent;

	private String comments;

	@Temporal(TemporalType.DATE)
	@Column(name = "expected_training_start_date")
	private Date expectedTrainingStartDate;

	@Column(name = "external_trainer_request")
	private String externalTrainerRequest;

	@Column(name = "is_software_licence_available")
	private String isSoftwareLicenceAvailable;

	@Column(name = "is_training_program_created")
	private String isTrainingProgramCreated;

	@Column(name = "level_of_participants")
	private String levelOfParticipants;

	@Column(name = "level_of_training_request")
	private byte levelOfTrainingRequest;

	@Column(name = "no_of_licenses_availbale")
	private Integer noOfLicensesAvailbale;

	@Column(name = "no_of_participants")
	private Integer noOfParticipants;

	@Column(name = "preferred_training_end_time")
	private Time preferredTrainingEndTime;

	@Column(name = "preferred_training_start_time")
	private Time preferredTrainingStartTime;

	@Column(name = "product_version_for_training")
	private String productVersionForTraining;

	@Column(name = "specific_input_comments")
	private String specificInputComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "training_modified_date")
	private Date trainingModifiedDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "training_requested_date")
	private Date trainingRequestedDate;

	@Column(name = "usage_of_product")
	private Byte usageOfProduct;

	@Column(name = "fk_training_request_status_id")
	private byte fkTrainingRequestStatusId;

	@Column(name = "fk_training_requester_emp_id")
	private Integer fkTrainingRequesterEmpId;

	@Column(name = "fk_training_topic_name_id")
	private int fkTrainingTopicNameId;
	
	@Column(name = "programme_category")
	private Byte programCategory;

	@OneToOne
	@JoinColumn(name = "fk_training_request_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private TrainingMasRequestStatusBO trainingMasRequestStatus;

	@OneToOne
	@JoinColumn(name = "fk_training_requester_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail;

	@OneToOne
	@JoinColumn(name = "fk_training_topic_name_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasTrainingTopicNameBO masTrainingTopicName;

	public TrainingDatRequestDetailBO() {
	}

	public Integer getPkTrainingRequestId() {
		return pkTrainingRequestId;
	}

	public void setPkTrainingRequestId(Integer pkTrainingRequestId) {
		this.pkTrainingRequestId = pkTrainingRequestId;
	}

	public String getBriefTrainingContent() {
		return briefTrainingContent;
	}

	public void setBriefTrainingContent(String briefTrainingContent) {
		this.briefTrainingContent = briefTrainingContent;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getExpectedTrainingStartDate() {
		return expectedTrainingStartDate;
	}

	public void setExpectedTrainingStartDate(Date expectedTrainingStartDate) {
		this.expectedTrainingStartDate = expectedTrainingStartDate;
	}

	public String getExternalTrainerRequest() {
		return externalTrainerRequest;
	}

	public void setExternalTrainerRequest(String externalTrainerRequest) {
		this.externalTrainerRequest = externalTrainerRequest;
	}

	public String getIsSoftwareLicenceAvailable() {
		return isSoftwareLicenceAvailable;
	}

	public void setIsSoftwareLicenceAvailable(String isSoftwareLicenceAvailable) {
		this.isSoftwareLicenceAvailable = isSoftwareLicenceAvailable;
	}

	public String getIsTrainingProgramCreated() {
		return isTrainingProgramCreated;
	}

	public void setIsTrainingProgramCreated(String isTrainingProgramCreated) {
		this.isTrainingProgramCreated = isTrainingProgramCreated;
	}

	public String getLevelOfParticipants() {
		return levelOfParticipants;
	}

	public void setLevelOfParticipants(String levelOfParticipants) {
		this.levelOfParticipants = levelOfParticipants;
	}

	public byte getLevelOfTrainingRequest() {
		return levelOfTrainingRequest;
	}

	public void setLevelOfTrainingRequest(byte levelOfTrainingRequest) {
		this.levelOfTrainingRequest = levelOfTrainingRequest;
	}

	public Integer getNoOfLicensesAvailbale() {
		return noOfLicensesAvailbale;
	}

	public void setNoOfLicensesAvailbale(Integer noOfLicensesAvailbale) {
		this.noOfLicensesAvailbale = noOfLicensesAvailbale;
	}

	public Integer getNoOfParticipants() {
		return noOfParticipants;
	}

	public void setNoOfParticipants(Integer noOfParticipants) {
		this.noOfParticipants = noOfParticipants;
	}

	public Time getPreferredTrainingEndTime() {
		return preferredTrainingEndTime;
	}

	public void setPreferredTrainingEndTime(Time preferredTrainingEndTime) {
		this.preferredTrainingEndTime = preferredTrainingEndTime;
	}

	public Time getPreferredTrainingStartTime() {
		return preferredTrainingStartTime;
	}

	public void setPreferredTrainingStartTime(Time preferredTrainingStartTime) {
		this.preferredTrainingStartTime = preferredTrainingStartTime;
	}

	public String getProductVersionForTraining() {
		return productVersionForTraining;
	}

	public void setProductVersionForTraining(String productVersionForTraining) {
		this.productVersionForTraining = productVersionForTraining;
	}

	public String getSpecificInputComments() {
		return specificInputComments;
	}

	public void setSpecificInputComments(String specificInputComments) {
		this.specificInputComments = specificInputComments;
	}

	public Date getTrainingModifiedDate() {
		return trainingModifiedDate;
	}

	public void setTrainingModifiedDate(Date trainingModifiedDate) {
		this.trainingModifiedDate = trainingModifiedDate;
	}

	public Date getTrainingRequestedDate() {
		return trainingRequestedDate;
	}

	public void setTrainingRequestedDate(Date trainingRequestedDate) {
		this.trainingRequestedDate = trainingRequestedDate;
	}

	public Byte getUsageOfProduct() {
		return usageOfProduct;
	}

	public void setUsageOfProduct(Byte usageOfProduct) {
		this.usageOfProduct = usageOfProduct;
	}

	public byte getFkTrainingRequestStatusId() {
		return fkTrainingRequestStatusId;
	}

	public void setFkTrainingRequestStatusId(byte fkTrainingRequestStatusId) {
		this.fkTrainingRequestStatusId = fkTrainingRequestStatusId;
	}

	public Integer getFkTrainingRequesterEmpId() {
		return fkTrainingRequesterEmpId;
	}

	public void setFkTrainingRequesterEmpId(Integer fkTrainingRequesterEmpId) {
		this.fkTrainingRequesterEmpId = fkTrainingRequesterEmpId;
	}

	public int getFkTrainingTopicNameId() {
		return fkTrainingTopicNameId;
	}

	public void setFkTrainingTopicNameId(int fkTrainingTopicNameId) {
		this.fkTrainingTopicNameId = fkTrainingTopicNameId;
	}

	public Byte getProgramCategory() {
		return programCategory;
	}

	public void setProgramCategory(Byte programCategory) {
		this.programCategory = programCategory;
	}

	public TrainingMasRequestStatusBO getTrainingMasRequestStatus() {
		return trainingMasRequestStatus;
	}

	public void setTrainingMasRequestStatus(
			TrainingMasRequestStatusBO trainingMasRequestStatus) {
		this.trainingMasRequestStatus = trainingMasRequestStatus;
	}

	public DatEmpDetailBO getDatEmpDetail() {
		return datEmpDetail;
	}

	public void setDatEmpDetail(DatEmpDetailBO datEmpDetail) {
		this.datEmpDetail = datEmpDetail;
	}

	public MasTrainingTopicNameBO getMasTrainingTopicName() {
		return masTrainingTopicName;
	}

	public void setMasTrainingTopicName(MasTrainingTopicNameBO masTrainingTopicName) {
		this.masTrainingTopicName = masTrainingTopicName;
	}

	@Override
	public String toString() {
		return "TrainingDatRequestDetailBO [pkTrainingRequestId="
				+ pkTrainingRequestId + ", briefTrainingContent="
				+ briefTrainingContent + ", comments=" + comments
				+ ", expectedTrainingStartDate=" + expectedTrainingStartDate
				+ ", externalTrainerRequest=" + externalTrainerRequest
				+ ", isSoftwareLicenceAvailable=" + isSoftwareLicenceAvailable
				+ ", isTrainingProgramCreated=" + isTrainingProgramCreated
				+ ", levelOfParticipants=" + levelOfParticipants
				+ ", levelOfTrainingRequest=" + levelOfTrainingRequest
				+ ", noOfLicensesAvailbale=" + noOfLicensesAvailbale
				+ ", noOfParticipants=" + noOfParticipants
				+ ", preferredTrainingEndTime=" + preferredTrainingEndTime
				+ ", preferredTrainingStartTime=" + preferredTrainingStartTime
				+ ", productVersionForTraining=" + productVersionForTraining
				+ ", specificInputComments=" + specificInputComments
				+ ", trainingModifiedDate=" + trainingModifiedDate
				+ ", trainingRequestedDate=" + trainingRequestedDate
				+ ", usageOfProduct=" + usageOfProduct
				+ ", fkTrainingRequestStatusId=" + fkTrainingRequestStatusId
				+ ", fkTrainingRequesterEmpId=" + fkTrainingRequesterEmpId
				+ ", fkTrainingTopicNameId=" + fkTrainingTopicNameId
				+ ", programCategory=" + programCategory
				+ ", trainingMasRequestStatus=" + trainingMasRequestStatus
				+ ", datEmpDetail=" + datEmpDetail + ", masTrainingTopicName="
				+ masTrainingTopicName + "]";
	}

		}