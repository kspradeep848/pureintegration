/*********************************************************************/
/*                  FILE HEADER                                      */


/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingMasRoomDetailBO.java                      */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  11-11-2016                                        */
/*                                                                   */
/*  Description :  This BO class related to TrainingRoom details      */      
/*                                                                    */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/*11-11-2016    THBS     1.0        Initial version created           */
/*********************************************************************/

package com.thbs.mis.training.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the training_mas_room_detail database table.
 * 
 */
@Entity
@Table(name = "training_mas_room_detail")
@NamedQuery(name = "TrainingMasRoomDetailBO.findAll", query = "SELECT t FROM TrainingMasRoomDetailBO t")
public class TrainingMasRoomDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_training_room_id")
	private short pkTrainingRoomId;

	@Column(name = "training_room_name")
	private String trainingRoomName;

	public TrainingMasRoomDetailBO() {
	}

	public short getPkTrainingRoomId() {
		return this.pkTrainingRoomId;
	}

	public void setPkTrainingRoomId(short pkTrainingRoomId) {
		this.pkTrainingRoomId = pkTrainingRoomId;
	}

	public String getTrainingRoomName() {
		return this.trainingRoomName;
	}

	public void setTrainingRoomName(String trainingRoomName) {
		this.trainingRoomName = trainingRoomName;
	}

	@Override
	public String toString() {
		return "TrainingMasRoomDetailBO [pkTrainingRoomId=" + pkTrainingRoomId
				+ ", trainingRoomName=" + trainingRoomName + "]";
	}


}