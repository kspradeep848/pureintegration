/*********************************************************************/
/*                  FILE HEADER                                      */

/*********************************************************************/
/*                                                                   */
/*  FileName    : TrainingDatPostApplicationFeedbackBO.java         */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  November 02, 2016                                 */
/*                                                                   */
/*  Description :  This class implemented by                         */
/*               Training Post Application Feedback 	        	 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 02, 2016     THBS     1.0        Initial version created */
/*********************************************************************/

package com.thbs.mis.training.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasFeedbackQuestionInfoBO;

/**
 * The persistent class for the training_dat_post_application_feedback database
 * table.
 * 
 */
@Entity
@Table(name = "training_dat_post_application_feedback")
@NamedQuery(name = "TrainingDatPostApplicationFeedbackBO.findAll", query = "SELECT t FROM TrainingDatPostApplicationFeedbackBO t")
public class TrainingDatPostApplicationFeedbackBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_post_application_feedback_id")
	private Integer pkPostApplicationFeedbackId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "evaluation_created_date")
	private Date evaluationCreatedDate;

	@Column(name = "post_application_mgr_answer")
	private String postApplicationMgrAnswer;

	@Column(name = "fk_dat_training_programme_id")
	private Integer fkDatTrainingProgrammeId;

	@Column(name = "fk_feedback_question_info_id")
	private Integer fkFeedbackQuestionInfoId;

	@Column(name = "fk_manager_emp_id")
	private Integer fkManagerEmpId;

	@Column(name = "fk_nominated_emp_id")
	private Integer fkNominatedEmpId;

	public Integer getFkNominatedEmpId() {
		return fkNominatedEmpId;
	}

	public void setFkNominatedEmpId(Integer fkNominatedEmpId) {
		this.fkNominatedEmpId = fkNominatedEmpId;
	}

	@OneToOne
	@JoinColumn(name = "fk_dat_training_programme_id", unique = true, nullable = true, insertable = false, updatable = false)
	private TrainingDatProgrammeDetailBO trainingDatProgrammeDetail;

	@OneToOne
	@JoinColumn(name = "fk_feedback_question_info_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasFeedbackQuestionInfoBO masFeedbackQuestionInfo;

	@OneToOne
	@JoinColumn(name = "fk_manager_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail;

	public TrainingDatPostApplicationFeedbackBO() {
	}

	public Integer getPkPostApplicationFeedbackId() {
		return this.pkPostApplicationFeedbackId;
	}

	public void setPkPostApplicationFeedbackId(
			Integer pkPostApplicationFeedbackId) {
		this.pkPostApplicationFeedbackId = pkPostApplicationFeedbackId;
	}

	public Date getEvaluationCreatedDate() {
		return this.evaluationCreatedDate;
	}

	public void setEvaluationCreatedDate(Date evaluationCreatedDate) {
		this.evaluationCreatedDate = evaluationCreatedDate;
	}

	public String getPostApplicationMgrAnswer() {
		return this.postApplicationMgrAnswer;
	}

	public void setPostApplicationMgrAnswer(String postApplicationMgrAnswer) {
		this.postApplicationMgrAnswer = postApplicationMgrAnswer;
	}

	public Integer getFkDatTrainingProgrammeId() {
		return fkDatTrainingProgrammeId;
	}

	public void setFkDatTrainingProgrammeId(Integer fkDatTrainingProgrammeId) {
		this.fkDatTrainingProgrammeId = fkDatTrainingProgrammeId;
	}

	public Integer getFkFeedbackQuestionInfoId() {
		return fkFeedbackQuestionInfoId;
	}

	public void setFkFeedbackQuestionInfoId(Integer fkFeedbackQuestionInfoId) {
		this.fkFeedbackQuestionInfoId = fkFeedbackQuestionInfoId;
	}

	public Integer getFkManagerEmpId() {
		return fkManagerEmpId;
	}

	public void setFkManagerEmpId(Integer fkManagerEmpId) {
		this.fkManagerEmpId = fkManagerEmpId;
	}

	public TrainingDatProgrammeDetailBO getTrainingDatProgrammeDetail() {
		return trainingDatProgrammeDetail;
	}

	public void setTrainingDatProgrammeDetail(
			TrainingDatProgrammeDetailBO trainingDatProgrammeDetail) {
		this.trainingDatProgrammeDetail = trainingDatProgrammeDetail;
	}

	public MasFeedbackQuestionInfoBO getMasFeedbackQuestionInfo() {
		return masFeedbackQuestionInfo;
	}

	public void setMasFeedbackQuestionInfo(
			MasFeedbackQuestionInfoBO masFeedbackQuestionInfo) {
		this.masFeedbackQuestionInfo = masFeedbackQuestionInfo;
	}

	public DatEmpDetailBO getDatEmpDetail() {
		return datEmpDetail;
	}

	public void setDatEmpDetail(DatEmpDetailBO datEmpDetail) {
		this.datEmpDetail = datEmpDetail;
	}

	@Override
	public String toString() {
		return "TrainingDatPostApplicationFeedbackBO [pkPostApplicationFeedbackId="
				+ pkPostApplicationFeedbackId
				+ ", evaluationCreatedDate="
				+ evaluationCreatedDate
				+ ", postApplicationMgrAnswer="
				+ postApplicationMgrAnswer
				+ ", fkDatTrainingProgrammeId="
				+ fkDatTrainingProgrammeId
				+ ", fkFeedbackQuestionInfoId="
				+ fkFeedbackQuestionInfoId
				+ ", fkManagerEmpId="
				+ fkManagerEmpId
				+ ", fkNominatedEmpId="
				+ fkNominatedEmpId
				+ ", trainingDatProgrammeDetail="
				+ trainingDatProgrammeDetail
				+ ", masFeedbackQuestionInfo="
				+ masFeedbackQuestionInfo
				+ ", datEmpDetail=" + datEmpDetail + "]";
	}

}