/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingMasRequestStatusBO.java               */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  11-11-2016                                        */
/*                                                                   */
/*  Description :  This BO class consists of                         */      
/*                 training_mas_request_status constraints           */
/*                 and related to Training request application       */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 11-11-2016, 2016     THBS     1.0        Initial version created */
/*********************************************************************/


package com.thbs.mis.training.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the training_mas_request_status database table.
 * 
 */
@Entity
@Table(name="training_mas_request_status")
@NamedQuery(name="TrainingMasRequestStatusBO.findAll", query="SELECT t FROM TrainingMasRequestStatusBO t")
public class TrainingMasRequestStatusBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_training_request_status_id")
	private byte pkTrainingRequestStatusId;

	@Column(name="training_request_status_name")
	private String trainingRequestStatusName;

	

	public TrainingMasRequestStatusBO() {
	}

	public byte getPkTrainingRequestStatusId() {
		return this.pkTrainingRequestStatusId;
	}

	public void setPkTrainingRequestStatusId(byte pkTrainingRequestStatusId) {
		this.pkTrainingRequestStatusId = pkTrainingRequestStatusId;
	}

	public String getTrainingRequestStatusName() {
		return this.trainingRequestStatusName;
	}

	public void setTrainingRequestStatusName(String trainingRequestStatusName) {
		this.trainingRequestStatusName = trainingRequestStatusName;
	}

	@Override
	public String toString() {
		return "TrainingMasRequestStatusBO [pkTrainingRequestStatusId="
				+ pkTrainingRequestStatusId + ", trainingRequestStatusName="
				+ trainingRequestStatusName + "]";
	}


}