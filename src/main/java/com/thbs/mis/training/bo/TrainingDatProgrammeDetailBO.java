package com.thbs.mis.training.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the training_dat_programme_detail database table.
 * 
 */
@Entity
@Table(name = "training_dat_programme_detail")
@NamedQuery(name = "TrainingDatProgrammeDetailBO.findAll", query = "SELECT t FROM TrainingDatProgrammeDetailBO t")
public class TrainingDatProgrammeDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_training_programme_id")
	private Integer pkTrainingProgrammeId;

	@Column(name = "comments")
	private String comments;

	@Column(name = "external_venue_name")
	private String externalVenueName;

	@Column(name = "maximum_seats")
	private Integer maximumSeats;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "programme_created_date")
	private Date programmeCreatedDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "programme_modified_date")
	private Date programmeModifiedDate;

	@Column(name = "trainer_name")
	private String trainerName;

	@Column(name = "trainer_type")
	private String trainerType;

	@Column(name = "training_duration_in_days")
	private Integer trainingDurationInDays;

	@Column(name = "training_duration_in_hours")
	private Integer trainingDurationInHours;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "training_end_date_time")
	private Date trainingEndDateTime;

	@Column(name = "training_intimated_status")
	private Byte trainingIntimatedStatus;

	@Column(name = "training_no_of_hrs_per_day")
	private Integer trainingNoOfHrsPerDay;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "training_start_date_time")
	private Date trainingStartDateTime;

	@Column(name = "fk_programme_created_by")
	private Integer fkProgrammeCreatedBy;

	@Column(name = "fk_programme_modified_by")
	private Integer fkProgrammeModifyBy;

	@Column(name = "fk_programme_status_id")
	private byte fkProgrammeStatusId;

	@Column(name = "fk_training_request_id")
	private Integer fkTrainingRequestId;

	@Column(name = "fk_training_room_id")
	private short fkTrainingRoomId;

	@Column(name = "is_agenda_uploaded")
	private String isAgendaUploaded;

	@OneToOne
	@JoinColumn(name = "fk_programme_created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOProgramCreatedBy;

	@OneToOne
	@JoinColumn(name = "fk_programme_modified_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOProgramModifyBy;

	@OneToOne
	@JoinColumn(name = "fk_programme_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private TrainingMasProgrammeStatusBO trainingMasProgrammeStatus;

	@OneToOne
	@JoinColumn(name = "fk_training_request_id", unique = true, nullable = true, insertable = false, updatable = false)
	private TrainingDatRequestDetailBO trainingDatRequestDetail;

	@OneToOne
	@JoinColumn(name = "fk_training_room_id", unique = true, nullable = true, insertable = false, updatable = false)
	private TrainingMasRoomDetailBO trainingMasRoomDetail;

	public TrainingDatProgrammeDetailBO() {
	}

	public String getIsAgendaUploaded() {
		return isAgendaUploaded;
	}

	public void setIsAgendaUploaded(String isAgendaUploaded) {
		this.isAgendaUploaded = isAgendaUploaded;
	}

	public Integer getPkTrainingProgrammeId() {
		return this.pkTrainingProgrammeId;
	}

	public void setPkTrainingProgrammeId(Integer pkTrainingProgrammeId) {
		this.pkTrainingProgrammeId = pkTrainingProgrammeId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getExternalVenueName() {
		return this.externalVenueName;
	}

	public void setExternalVenueName(String externalVenueName) {
		this.externalVenueName = externalVenueName;
	}

	public Integer getMaximumSeats() {
		return this.maximumSeats;
	}

	public void setMaximumSeats(Integer maximumSeats) {
		this.maximumSeats = maximumSeats;
	}

	public Date getProgrammeCreatedDate() {
		return this.programmeCreatedDate;
	}

	public void setProgrammeCreatedDate(Date programmeCreatedDate) {
		this.programmeCreatedDate = programmeCreatedDate;
	}

	public Date getProgrammeModifiedDate() {
		return this.programmeModifiedDate;
	}

	public void setProgrammeModifiedDate(Date programmeModifiedDate) {
		this.programmeModifiedDate = programmeModifiedDate;
	}

	public String getTrainerName() {
		return this.trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	public String getTrainerType() {
		return this.trainerType;
	}

	public void setTrainerType(String trainerType) {
		this.trainerType = trainerType;
	}

	public Integer getTrainingDurationInDays() {
		return this.trainingDurationInDays;
	}

	public void setTrainingDurationInDays(Integer trainingDurationInDays) {
		this.trainingDurationInDays = trainingDurationInDays;
	}

	public Integer getTrainingDurationInHours() {
		return this.trainingDurationInHours;
	}

	public void setTrainingDurationInHours(Integer trainingDurationInHours) {
		this.trainingDurationInHours = trainingDurationInHours;
	}

	public Date getTrainingEndDateTime() {
		return this.trainingEndDateTime;
	}

	public void setTrainingEndDateTime(Date trainingEndDateTime) {
		this.trainingEndDateTime = trainingEndDateTime;
	}

	public Byte getTrainingIntimatedStatus() {
		return this.trainingIntimatedStatus;
	}

	public void setTrainingIntimatedStatus(Byte trainingIntimatedStatus) {
		this.trainingIntimatedStatus = trainingIntimatedStatus;
	}

	public Integer getTrainingNoOfHrsPerDay() {
		return this.trainingNoOfHrsPerDay;
	}

	public void setTrainingNoOfHrsPerDay(Integer trainingNoOfHrsPerDay) {
		this.trainingNoOfHrsPerDay = trainingNoOfHrsPerDay;
	}

	public Date getTrainingStartDateTime() {
		return this.trainingStartDateTime;
	}

	public void setTrainingStartDateTime(Date trainingStartDateTime) {
		this.trainingStartDateTime = trainingStartDateTime;
	}

	public Integer getFkProgrammeCreatedBy() {
		return fkProgrammeCreatedBy;
	}

	public void setFkProgrammeCreatedBy(Integer fkProgrammeCreatedBy) {
		this.fkProgrammeCreatedBy = fkProgrammeCreatedBy;
	}

	public Integer getFkProgrammeModifyBy() {
		return fkProgrammeModifyBy;
	}

	public void setFkProgrammeModifyBy(Integer fkProgrammeModifyBy) {
		this.fkProgrammeModifyBy = fkProgrammeModifyBy;
	}

	public byte getFkProgrammeStatusId() {
		return fkProgrammeStatusId;
	}

	public void setFkProgrammeStatusId(byte fkProgrammeStatusId) {
		this.fkProgrammeStatusId = fkProgrammeStatusId;
	}

	public Integer getFkTrainingRequestId() {
		return fkTrainingRequestId;
	}

	public void setFkTrainingRequestId(Integer fkTrainingRequestId) {
		this.fkTrainingRequestId = fkTrainingRequestId;
	}

	public short getFkTrainingRoomId() {
		return fkTrainingRoomId;
	}

	public void setFkTrainingRoomId(short fkTrainingRoomId) {
		this.fkTrainingRoomId = fkTrainingRoomId;
	}

	public DatEmpDetailBO getDatEmpDetailBOProgramCreatedBy() {
		return datEmpDetailBOProgramCreatedBy;
	}

	public void setDatEmpDetailBOProgramCreatedBy(
			DatEmpDetailBO datEmpDetailBOProgramCreatedBy) {
		this.datEmpDetailBOProgramCreatedBy = datEmpDetailBOProgramCreatedBy;
	}

	public DatEmpDetailBO getDatEmpDetailBOProgramModifyBy() {
		return datEmpDetailBOProgramModifyBy;
	}

	public void setDatEmpDetailBOProgramModifyBy(
			DatEmpDetailBO datEmpDetailBOProgramModifyBy) {
		this.datEmpDetailBOProgramModifyBy = datEmpDetailBOProgramModifyBy;
	}

	public TrainingMasProgrammeStatusBO getTrainingMasProgrammeStatus() {
		return trainingMasProgrammeStatus;
	}

	public void setTrainingMasProgrammeStatus(
			TrainingMasProgrammeStatusBO trainingMasProgrammeStatus) {
		this.trainingMasProgrammeStatus = trainingMasProgrammeStatus;
	}

	public TrainingDatRequestDetailBO getTrainingDatRequestDetail() {
		return trainingDatRequestDetail;
	}

	public void setTrainingDatRequestDetail(
			TrainingDatRequestDetailBO trainingDatRequestDetail) {
		this.trainingDatRequestDetail = trainingDatRequestDetail;
	}

	public TrainingMasRoomDetailBO getTrainingMasRoomDetail() {
		return trainingMasRoomDetail;
	}

	public void setTrainingMasRoomDetail(
			TrainingMasRoomDetailBO trainingMasRoomDetail) {
		this.trainingMasRoomDetail = trainingMasRoomDetail;
	}

	@Override
	public String toString() {
		return "TrainingDatProgrammeDetailBO [pkTrainingProgrammeId="
				+ pkTrainingProgrammeId + ", comments=" + comments
				+ ", externalVenueName=" + externalVenueName
				+ ", maximumSeats=" + maximumSeats + ", programmeCreatedDate="
				+ programmeCreatedDate + ", programmeModifiedDate="
				+ programmeModifiedDate + ", trainerName=" + trainerName
				+ ", trainerType=" + trainerType + ", trainingDurationInDays="
				+ trainingDurationInDays + ", trainingDurationInHours="
				+ trainingDurationInHours + ", trainingEndDateTime="
				+ trainingEndDateTime + ", trainingIntimatedStatus="
				+ trainingIntimatedStatus + ", trainingNoOfHrsPerDay="
				+ trainingNoOfHrsPerDay + ", trainingStartDateTime="
				+ trainingStartDateTime + ", fkProgrammeCreatedBy="
				+ fkProgrammeCreatedBy + ", fkProgrammeModifyBy="
				+ fkProgrammeModifyBy + ", fkProgrammeStatusId="
				+ fkProgrammeStatusId + ", fkTrainingRequestId="
				+ fkTrainingRequestId + ", fkTrainingRoomId="
				+ fkTrainingRoomId + ", isAgendaUploaded=" + isAgendaUploaded
				+ ", datEmpDetailBOProgramCreatedBy="
				+ datEmpDetailBOProgramCreatedBy
				+ ", datEmpDetailBOProgramModifyBy="
				+ datEmpDetailBOProgramModifyBy
				+ ", trainingMasProgrammeStatus=" + trainingMasProgrammeStatus
				+ ", trainingDatRequestDetail=" + trainingDatRequestDetail
				+ ", trainingMasRoomDetail=" + trainingMasRoomDetail + "]";
	}

}