package com.thbs.mis.training.bo;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the training_mas_programme_status database table.
 * 
 */
@Entity
@Table(name="training_mas_programme_status")
@NamedQuery(name="TrainingMasProgrammeStatusBO.findAll", query="SELECT t FROM TrainingMasProgrammeStatusBO t")
public class TrainingMasProgrammeStatusBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_training_programme_status_id")
	private byte pkTrainingProgrammeStatusId;

	@Column(name="training_programme_status_name")
	private String trainingProgrammeStatusName;



	public TrainingMasProgrammeStatusBO() {
	}

	public byte getPkTrainingProgrammeStatusId() {
		return this.pkTrainingProgrammeStatusId;
	}

	public void setPkTrainingProgrammeStatusId(byte pkTrainingProgrammeStatusId) {
		this.pkTrainingProgrammeStatusId = pkTrainingProgrammeStatusId;
	}

	public String getTrainingProgrammeStatusName() {
		return this.trainingProgrammeStatusName;
	}

	public void setTrainingProgrammeStatusName(String trainingProgrammeStatusName) {
		this.trainingProgrammeStatusName = trainingProgrammeStatusName;
	}

	

}