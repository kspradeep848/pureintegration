/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  ProjectURIConstants.java                          */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  05-March-2018                                     */
/*                                                                   */
/*  Description :  Project uri used to fetch the service			 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 05-Mar-2018    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.project.constants;

public class ProjectURIConstants {

	// for client Page
	public final static String CREATE_CLIENT = "/project/client";

	public final static String UPDATE_CLIENT = "/project/client/update";
	
	public final static String GET_CLIENT_DETAILS_BASED_ON_CLIENT_ID = "/project/clientdetail/{clientId}/{empId}";

	public final static String GET_CLIENT_BASED_ON_INPUT_SEARCH = "/project/client/search";

	public final static String DOWNLOAD_SUPPORTING_DOC_CLIENT = "/project/client/doc/{clientId}";

	public static final String GET_CLIENT_BASED_ON_STATUS_ID = "/project/clients/{statusId}";

	public static final String GET_CLIENT_BASED_ON_BU_ID = "/project/client/bu/{buId}";

	public static final String GET_CLIENT_BASED_ON_REGION_ID = "/project/client/region/{regionId}";

	public static final String GET_COUNTRY_BASED_ON_REGION_ID = "/project/country/region/{regionId}";


	// For Project Page

	public static final String CREATE_PROJECT = "/project";

	public static final String UPDATE_PROJECT = "/project";

	public static final String VIEW_PROJECTS = "/project/search";

	public static final String GET_PROJECTS = "/project/client/{clientId}";

	public static final String VIEW_PERTICULAR_PROJECT = "/project/search/{projectId}";

	public static final String GET_PROJECT_HISTORY = "/project/history/{clientId}";
	
	public static final String GET_ALL_PROJECT_CLASSIFICATION = "/project/projectclassification";
	
	public static final String GET_ALL_PROJECTSTATUS = "/project/projectstatus";
	
	public static final String GET_CLIENT_ADDRESS = "/project/client/address/{clientId}";

	public static final String GET_PROJECT_USER_ACCESS = "/project/projectleveluseraccess";

	public static final String VIEW_PROJECT_DETAILS = "/project/{projectId}";
	
	public static final String GET_ALL_PROJECTS_BASED_ON_LOGGED_IN = "/project/names/{empId}";
	
	public static final String GET_ALL_PROJECTS_BASED_ON_BUID = "/project/names/buid/{buId}";

	// For work order Page

	public final static String WORK_ORDER = "/project/workorder";

	public final static String GET_WORK_ORDER = "/project/workorder/{workorderId}/{empId}";

	public final static String VIEW_WORK_ORDER = "/project/workorder/search";

	public final static String FULL_VIEW_WORK_ORDER = "/project/workorder/fullview/{workorderId}/{empId}";

	public final static String DOC_DETAILS = "/project/workorder/docdetails/{workorderId}/{empId}";

	public final static String SOW_UPLOAD = "/project/workorder/sow/upload";

	public final static String SOW_DOWNLOAD = "/project/workorder/sow/download";

	public static final String SOW_SUPPORT_UPLOAD = "/project/workorder/sowsupportdoc/upload";

	public static final String SOW_SUPPORT_DOWNLOAD = "/project/workorder/sowsupportdoc/download";

	public static final String GET_ALL_WO_BY_PROJECTID = "/project/workorder/{projectId}";

	public static final String GET_ALL_CLIENT_MANAGERS = "/project/workorder/clientmanagers";

	public static final String GET_ALL_WORK_ORDER_STATUS = "/project/workorderstatus";
	
	public static final String GET_WORK_ORDER_HISTORY = "/project/workorder/history/{projectId}";
	
	public static final String GET_ALL_WO_NUM_BASED_ON_LOGGEDIN = "/project/workorder/wonum/{empId}";

	public static final String GET_ALL_WORK_ORDER_INITIALS = "/project/workorder/initials";
	
	public static final String GET_ALL_WO_NUM_BASED_ON_BUID = "/project/workorder/wonum/buid/{buId}";
	
	//For budget page

	public static final String GET_CLIENT_BUDGET_SUMMARY = "/project/clientbudgetsummary/{workorderId}/{empId}";

	public static final String GET_COMPANY_BUDGET_SUMMARY = "/project/companybudgetsummary/{workorderId}/{empId}";
	
	public static final String GET_CLIENT_BUDGET = "/project/clientbudgetdetails/{workOrderId}/{empId}";

	public static final String GET_COMPANY_BUDGET = "/project/companybudgetdetails/{workOrderId}/{empId}";
	
	public static final String CREATE_CLIENT_BUDGET = "/project/clientbudget";

	public static final String GET_ALL_CLIENT_BUDGET_RESOURCE_DESIGNATION = "/project/clientbudget/designationlist/{woId}";

	public static final String THBS_BUDGET = "/project/thbsbudget";

	public static final String GET_ALL_THBS_LOCATION_BY_CLIENT_BUDGET_ID = "/project/thbsbudget/location/{clientBudgetId}"; 
	
	public static final String GET_THBS_BUDGET_DETAILS = "/project/thbsbudget/{budgetId}/{empId}";
	
	public static final String THBS_BUDGET_ADDITIONAL_AMT = "/project/thbsbudget/additionalamt";

	public static final String GET_ALL_THBS_BUDGET_BY_CLIENT_BUDGET_ID = "/project/thbsbudgets/{clientBudgetId}/{empId}";
	

	public static final String UPDATE_CLIENT_BUDGET = "/project/clientbudget/update";

	public static final String GET_ALL_CLIENT_BUDGET_DETAILS ="/project/clientbudget/{clientBudgetId}/{empId}";
	
	public static final String GET_ALL_CLIENT_BUDGET_DETAILS_BY_WORKORDER_ID = "/project/clientbudgets/{woId}/{empId}";
	
	public static final String GET_STANDARD_RATE = "/project/thbsbudget/standardrate/{empId}";
	
	public static final String GET_ALL_THBS_BUDGET_BY_WO_ID = "/project/thbsbudget/summary/{woId}/{empId}";
	
	public static final String SUBMIT_BUDGET_FOR_APPROVE = "/project/budget/submit";
	
	public static final String BUDGET_ACTION = "/project/budgetaction";


	// For Component Page
	public static final String CREATE_UPDATE_COMPONENT = "/project/component";

	public static final String SEARCH_COMPONENTS = "/project/component/search";

	public static final String SEARCH_COMPONENTS_ON_COMPONENT_FULLVIEW_ID = "/project/component/fullview/{componentId}";

	public static final String GET_COMPONENT = "/project/component/{componentId}";

	public static final String GET_ALL_COMPONENT_STATUS = "/project/componentstatus";

	public static final String GET_ALL_COMPONENT_TYPE = "/project/componenttype";
	
	public static final String GET_ALL_COMPONENT_OWNERS = "/project/componentowner/{buId}";

	// For Milestone Page
	public static final String CREATE_MILESTONE = "/project/milestone";

	public static final String SEARCH_MILESTONE = "/project/milestone/search";

	public static final String MILESTONE_DETAILS = "/project/milestone/{milestoneId}";

	public static final String OB10_VALUE = "/project/milestone/ob10value/{milestoneId}";

	public static final String TRIGGER_MILESTONE = "/project/milestone/trigger";
	
	public static final String GET_MILESTONE_STATUS = "/project/milestonestatus/{type}";

	public static final String GET_ALL_MILESTONE_COMPONENT_ID = "/project/milestone/names/{componentId}";
	
	// For Dashboard Page
	
	public static final String GET_STEPPER_STATUS = "/project/stepper";

	public static final String GET_ALL_PROJECTS_BASED_ON_COUNTRIES = "/project/globalregions/{empId}";

	public static final String GET_PROJECT_OVERVIEW = "/project/overview/{empId}";

	public static final String GET_PROJECT_STATUS = "/project/names/status/{empId}";
	
	
	// For history
	
	public static final String VIEW_HISTORY = "/project/history";


}
