package com.thbs.mis.project.constants;

public class ProjectConstants {
	
	public final static byte Active = 1;
	
	public final static byte WO_ACTIVE = 1;
	public final static byte WO_ON_HOLD = 3;
	public final static byte WO_CLOSED = 4;
	public final static byte WO_DISCARDED = 5;
	public final static byte WO_FUTURE = 2;
	
	public final static byte YES_FLAG=1;
	public final static byte NO_FLAG=0;
	
	public final static byte MILESTONE_WORK_IN_PROGRESS = 1;
	public final static byte MILESTONE_WORK_IN_COMPLETED=5;
	public final static byte MILESTONE_NOT_STARTED = 3;

	public final static byte COMPONENT_TYPE_ID_OTHERS = 3;
	public final static byte COMPONENT_TYPE_ID_FIXED = 2;
	public final static byte COMPONENT_TYPE_ID_TNM = 1;
	public final static byte COMPONENT_INFORCE = 1;
	public final static byte COMPONENT_FUTURE = 2;
	public final static byte COMPONENT_ONHOLD = 3;
	public final static byte COMPONENT_WORK_COMPLETED = 4;
	public final static byte COMPONENT_DISCARDED = 5;

	public final static byte PROJECT_ACTIVE = 1;
	public final static byte PROJECT_FUTURE = 2;
	public final static byte PROJECT_ONHOLD = 3;
	public final static byte PROJECT_CLOSED = 4;
	
	public final static Integer BUDGET_STATUS_REJECT = 4;
	public final static Integer BUDGET_STATUS_DRAFT = 1;
	public final static Integer BUDGET_STATUS_WAITING_FOR_APPROVAL = 2;
	
	public final static String CREATE="create";
	
	public final static String YES="YES";
	public final static String NO="NO";
	public final static String ACTIVE="ACTIVE";
	public final static String INACTIVE="INACTIVE";
	public final static String PO_TYPE="NORMAL,AGGREGATE,BOTH";
	public final static String UPLOADED="UPLOADED";
	public final static String NOT_UPLOADED="NOT UPLOADED";
	public final static String LOW="LOW";
	public final static String HIGH="HIGH";
	public final static Integer RISK_DAYS = 45;

}
