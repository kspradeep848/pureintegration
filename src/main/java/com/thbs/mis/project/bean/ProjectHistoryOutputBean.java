package com.thbs.mis.project.bean;

import java.util.Date;

public class ProjectHistoryOutputBean {
	
	private Date historyCreatedDate;
	private String historyMessage;
	private Date historyCreatedDateString;
	
	public Date getHistoryCreatedDate() {
		return historyCreatedDate;
	}
	public void setHistoryCreatedDate(Date historyCreatedDate) {
		this.historyCreatedDate = historyCreatedDate;
	}
	public String getHistoryMessage() {
		return historyMessage;
	}
	public void setHistoryMessage(String historyMessage) {
		this.historyMessage = historyMessage;
	}
	public Date getHistoryCreatedDateString() {
		return historyCreatedDateString;
	}
	public void setHistoryCreatedDateString(Date historyCreatedDateString) {
		this.historyCreatedDateString = historyCreatedDateString;
	}
	@Override
	public String toString() {
		return "projectHistoryInputBean [historyCreatedDate="
				+ historyCreatedDate + ", historyMessage=" + historyMessage
				+ ", historyCreatedDateString=" + historyCreatedDateString
				+ "]";
	}
}
