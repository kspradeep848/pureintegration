package com.thbs.mis.project.bean;

public class ProjectStatusBean {

	private Integer projectId;

	private String projectName;

	private Byte projectStatus;

	private String projectStatusName;

	private String link;

	public ProjectStatusBean() {
		super();
	}

	public ProjectStatusBean(Integer projectId, String projectName,
			Byte projectStatus, String projectStatusName) {
		super();
		this.projectId = projectId;
		this.projectName = projectName;
		this.projectStatus = projectStatus;
		this.projectStatusName = projectStatusName;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Byte getProjectStatus() {
		return projectStatus;
	}

	public void setProjectStatus(Byte projectStatus) {
		this.projectStatus = projectStatus;
	}

	public String getProjectStatusName() {
		return projectStatusName;
	}

	public void setProjectStatusName(String projectStatusName) {
		this.projectStatusName = projectStatusName;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public String toString() {
		return "ProjectStatusBean [projectId=" + projectId + ", projectName="
				+ projectName + ", projectStatus=" + projectStatus
				+ ", projectStatusName=" + projectStatusName + ", link=" + link
				+ "]";
	}
}
