package com.thbs.mis.project.bean;

import java.util.Arrays;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;

public class CreateProjectBean {
  
	private Integer projectId;
	
	@NotBlank(message = "projectName should not be blank.")
	@NotEmpty(message = "projectName Description should not be empty.")
	@NotNull(message = "projectName Description should not be null.")
	private String projectName;

	@JsonSerialize(using = DateSerializer.class)
	@NotNull(message = "projectStartDate field can not be null.")
	private Date projectStartDate;

	@JsonSerialize(using = DateSerializer.class)
	@NotNull(message = "projectEndDate field can not be null.")
	private Date projectEndDate;

	@NotBlank(message = "projectDescription should not be blank.")
	@NotEmpty(message = "projectDescription Description should not be empty.")
	@NotNull(message = "projectDescription Description should not be null.")
	private String projectDescription;

	@Min(1)
	@NotNull(message = "clientAddressId field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "clientAddressId must be an Short.")
	private Short clientId;

	@Min(1)
	@NotNull(message = "verticalId field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "verticalId must be an Short.")
	private Short verticalId;

	@Min(1)
	@NotNull(message = "regionId field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "regionId must be an Short.")
	private Short regionId;

	private Integer deliveryManagerId;

	@Min(1)
	@NotNull(message = "buUnitId field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "buUnitId must be an Short.")
	private Short buUnitId;

	@Min(1)
	@NotNull(message = "domainId field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "domainId must be an Short.")
	private Short domainId;

	@Min(1)
	@NotNull(message = "domainMgrId field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "domainMgrId must be an Integer.")
	private Integer domainMgrId;

	@NotBlank(message = "projectType should not be blank.")
	@NotEmpty(message = "projectType Description should not be empty.")
	@NotNull(message = "projectType Description should not be null.")
	private String projectType;

	@Min(1)
	@NotNull(message = "accountId field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "accountId must be an Short.")
	private Short accountId;

	@Min(1)
	@NotNull(message = "accountMgrId field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "accountMgrId must be an Integer.")
	private Integer accountMgrId;

	@NotBlank(message = "projectBillableFlag should not be blank.")
	@NotEmpty(message = "projectBillableFlag Description should not be empty.")
	@NotNull(message = "projectBillableFlag Description should not be null.")
	private String projectBillableFlag;

	@Min(1)
	@NotNull(message = "createdBy field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "createdBy must be an Integer.")
	private Integer createdBy;

	@NotBlank(message = "isHavingAggregatePo should not be blank.")
	@NotEmpty(message = "isHavingAggregatePo Description should not be empty.")
	@NotNull(message = "isHavingAggregatePo Description should not be null.")
	private String isHavingAggregatePo;

	@NotEmpty(message = "projectCoordinator details should not be empty.")
	@NotNull(message = "projectCoordinator should not be null.")
	private int[] projectCoordinator;

	private int[] projectLevelUserAccess;

	@NotNull(message = "projectClassificationId should not be null.")
	private Byte projectClassificationId;
	
	private Integer updatedBy;

	private String commentsForUpdation;
	
	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Date getProjectStartDate() {
		return projectStartDate;
	}

	public void setProjectStartDate(Date projectStartDate) {
		this.projectStartDate = projectStartDate;
	}

	public Date getProjectEndDate() {
		return projectEndDate;
	}

	public void setProjectEndDate(Date projectEndDate) {
		this.projectEndDate = projectEndDate;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public Short getClientId() {
		return clientId;
	}

	public void setClientId(Short clientId) {
		this.clientId = clientId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Short getVerticalId() {
		return verticalId;
	}

	public void setVerticalId(Short verticalId) {
		this.verticalId = verticalId;
	}

	public Short getRegionId() {
		return regionId;
	}

	public void setRegionId(Short regionId) {
		this.regionId = regionId;
	}

	public Integer getDeliveryManagerId() {
		return deliveryManagerId;
	}

	public void setDeliveryManagerId(Integer deliveryManagerId) {
		this.deliveryManagerId = deliveryManagerId;
	}

	public Short getBuUnitId() {
		return buUnitId;
	}

	public void setBuUnitId(Short buUnitId) {
		this.buUnitId = buUnitId;
	}

	public Short getDomainId() {
		return domainId;
	}

	public void setDomainId(Short domainId) {
		this.domainId = domainId;
	}

	public Integer getDomainMgrId() {
		return domainMgrId;
	}

	public void setDomainMgrId(Integer domainMgrId) {
		this.domainMgrId = domainMgrId;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public Short getAccountId() {
		return accountId;
	}

	public void setAccountId(Short accountId) {
		this.accountId = accountId;
	}

	public Integer getAccountMgrId() {
		return accountMgrId;
	}

	public void setAccountMgrId(Integer accountMgrId) {
		this.accountMgrId = accountMgrId;
	}

	public String getProjectBillableFlag() {
		return projectBillableFlag;
	}

	public void setProjectBillableFlag(String projectBillableFlag) {
		this.projectBillableFlag = projectBillableFlag;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public int[] getProjectCoordinator() {
		return projectCoordinator;
	}

	public void setProjectCoordinator(int[] projectCoordinator) {
		this.projectCoordinator = projectCoordinator;
	}

	public int[] getProjectLevelUserAccess() {
		return projectLevelUserAccess;
	}

	public void setProjectLevelUserAccess(int[] projectLevelUserAccess) {
		this.projectLevelUserAccess = projectLevelUserAccess;
	}

	public Byte getProjectClassificationId() {
		return projectClassificationId;
	}

	public void setProjectClassificationId(Byte projectClassificationId) {
		this.projectClassificationId = projectClassificationId;
	}

	public String getIsHavingAggregatePo() {
		return isHavingAggregatePo;
	}

	public void setIsHavingAggregatePo(String isHavingAggregatePo) {
		this.isHavingAggregatePo = isHavingAggregatePo;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCommentsForUpdation() {
		return commentsForUpdation;
	}

	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}

	@Override
	public String toString() {
		return "CreateProjectBean [projectName=" + projectName
				+ ", projectStartDate=" + projectStartDate
				+ ", projectEndDate=" + projectEndDate
				+ ", projectDescription=" + projectDescription + ", clientId="
				+ clientId + ", verticalId=" + verticalId + ", regionId="
				+ regionId + ", deliveryManagerId=" + deliveryManagerId
				+ ", buUnitId=" + buUnitId + ", domainId=" + domainId
				+ ", domainMgrId=" + domainMgrId + ", projectType="
				+ projectType + ", accountId=" + accountId + ", accountMgrId="
				+ accountMgrId + ", projectBillableFlag=" + projectBillableFlag
				+ ", createdBy=" + createdBy + ", isHavingAggregatePo="
				+ isHavingAggregatePo + ", projectCoordinator="
				+ Arrays.toString(projectCoordinator)
				+ ", projectLevelUserAccess="
				+ Arrays.toString(projectLevelUserAccess)
				+ ", projectClassificationId=" + projectClassificationId + "]";
	}
}