package com.thbs.mis.project.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class MasClientAddressOutputBean {

	private Short clientAddressId;

	private String addressLineOne;

	private String addressLineTwo;

	private String cityName;

	private String stateName;

	private Short countryId;

	private String countryName;

	private String isBillToAddress;

	private String isShipToAddress;

	private String zipCode;

	private Integer createdBy;

	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date createdOn;

	private Integer updatedBy;

	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date updatedOn;

	private String updatedComments;

	public Short getClientAddressId() {
		return clientAddressId;
	}

	public void setClientAddressId(Short clientAddressId) {
		this.clientAddressId = clientAddressId;
	}

	public String getAddressLineOne() {
		return addressLineOne;
	}

	public void setAddressLineOne(String addressLineOne) {
		this.addressLineOne = addressLineOne;
	}

	public String getAddressLineTwo() {
		return addressLineTwo;
	}

	public void setAddressLineTwo(String addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Short getCountryId() {
		return countryId;
	}

	public void setCountryId(Short countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getIsBillToAddress() {
		return isBillToAddress;
	}

	public void setIsBillToAddress(String isBillToAddress) {
		this.isBillToAddress = isBillToAddress;
	}

	public String getIsShipToAddress() {
		return isShipToAddress;
	}

	public void setIsShipToAddress(String isShipToAddress) {
		this.isShipToAddress = isShipToAddress;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedComments() {
		return updatedComments;
	}

	public void setUpdatedComments(String updatedComments) {
		this.updatedComments = updatedComments;
	}

	@Override
	public String toString() {
		return "MasClientAddressOutputBean [clientAddressId=" + clientAddressId
				+ ", addressLineOne=" + addressLineOne + ", addressLineTwo="
				+ addressLineTwo + ", cityName=" + cityName + ", stateName="
				+ stateName + ", countryId=" + countryId + ", countryName="
				+ countryName + ", isBillToAddress=" + isBillToAddress
				+ ", isShipToAddress=" + isShipToAddress + ", zipCode="
				+ zipCode + ", createdBy=" + createdBy + ", createdOn="
				+ createdOn + ", updatedBy=" + updatedBy + ", updatedOn="
				+ updatedOn + ", updatedComments=" + updatedComments + "]";
	}
}
