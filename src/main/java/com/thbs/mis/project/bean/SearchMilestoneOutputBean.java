package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.Date;

public class SearchMilestoneOutputBean {
	private int milestoneId;
	private String milestoneName;
	private BigDecimal milestoneValue;
	private String milestoneStatus;
	private Date milestoneEndDate;

	public int getMilestoneId() {
		return milestoneId;
	}

	public void setMilestoneId(int milestoneId) {
		this.milestoneId = milestoneId;
	}

	public String getMilestoneName() {
		return milestoneName;
	}

	public void setMilestoneName(String milestoneName) {
		this.milestoneName = milestoneName;
	}

	public BigDecimal getMilestoneValue() {
		return milestoneValue;
	}

	public void setMilestoneValue(BigDecimal milestoneValue) {
		this.milestoneValue = milestoneValue;
	}

	public String getMilestoneStatus() {
		return milestoneStatus;
	}

	public void setMilestoneStatus(String milestoneStatus) {
		this.milestoneStatus = milestoneStatus;
	}

	public Date getMilestoneEndDate() {
		return milestoneEndDate;
	}

	public void setMilestoneEndDate(Date milestoneEndDate) {
		this.milestoneEndDate = milestoneEndDate;
	}

	@Override
	public String toString() {
		return "SearchMilestoneOutputBean [milestoneId=" + milestoneId
				+ ", milestoneName=" + milestoneName + ", milestoneValue="
				+ milestoneValue + ", milestoneStatus=" + milestoneStatus + "]";
	}

}
