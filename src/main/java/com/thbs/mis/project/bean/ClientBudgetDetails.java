package com.thbs.mis.project.bean;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;



public class ClientBudgetDetails {
	@NotNull(message="Budget days cannot be null")
	private Integer budgetDays;
	@NotNull(message="Billling rate cannot be null")
	private BigDecimal billingRate;
	@NotNull(message="Cost to client  cannot be null")
	private BigDecimal costToClient;
	@NotEmpty(message="Location cannot be empty please enter OFFSHORE/ONSITE ")
	private String location;
	@NotNull(message="Resource Role-Id rate cannot be null")
	private Short resourceRoleId;
	@NotNull(message="noOfResource cannot be null")
	private Integer noOfResource;
	private Integer clientBudgetResourceId;
	private String commentsForUpdation;
	
	public BigDecimal getBillingRate() {
		return billingRate;
	}

	public void setBillingRate(BigDecimal billingRate) {
		this.billingRate = billingRate;
	}

	public BigDecimal getCostToClient() {
		return costToClient;
	}

	public void setCostToClient(BigDecimal costToClient) {
		this.costToClient = costToClient;
	}

	public Short getResourceRoleId() {
		return resourceRoleId;
	}

	public void setResourceRoleId(Short resourceRoleId) {
		this.resourceRoleId = resourceRoleId;
	}

	public String getCommentsForUpdation() {
		return commentsForUpdation;
	}

	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}

	public Integer getClientBudgetResourceId() {
		return clientBudgetResourceId;
	}

	public void setClientBudgetResourceId(Integer clientBudgetResourceId) {
		this.clientBudgetResourceId = clientBudgetResourceId;
	}

	public Integer getBudgetDays() {
		return budgetDays;
	}

	public void setBudgetDays(Integer budgetDays) {
		this.budgetDays = budgetDays;
	}

	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}


	public Integer getNoOfResource() {
		return noOfResource;
	}

	public void setNoOfResource(Integer noOfResource) {
		this.noOfResource = noOfResource;
	}

	@Override
	public String toString() {
		return "ManDaysDetails [budgetDays=" + budgetDays + ", billingRate="
				+ billingRate + ", costToClient=" + costToClient
				+ ", location=" + location + ", resourceRoleId="
				+ resourceRoleId + ", noOfResource=" + noOfResource
				+ ", clientBudgetResourceId=" + clientBudgetResourceId
				+ ", commentsForUpdation=" + commentsForUpdation + "]";
	}

}
