package com.thbs.mis.project.bean;

public class MilestoneBean {
	private Integer milestoneId;
	private String milestoneName;
	public Integer getMilestoneId() {
		return milestoneId;
	}
	public void setMilestoneId(Integer milestoneId) {
		this.milestoneId = milestoneId;
	}
	public String getMilestoneName() {
		return milestoneName;
	}
	public void setMilestoneName(String milestoneName) {
		this.milestoneName = milestoneName;
	}
	@Override
	public String toString() {
		return "ComponentBean [milestoneId=" + milestoneId + ", milestoneName="
				+ milestoneName + "]";
	}

}
