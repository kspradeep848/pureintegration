package com.thbs.mis.project.bean;

import javax.validation.constraints.NotNull;

public class BudgetSubmitInputBean {

	@NotNull(message="Workorder id cannot be null")
	private Integer woId;
	@NotNull(message="Requested id cannot be null")
	private Integer requestedBy;
	public Integer getWoId() {
		return woId;
	}
	public void setWoId(Integer woId) {
		this.woId = woId;
	}
	public Integer getRequestedBy() {
		return requestedBy;
	}
	public void setRequestedBy(Integer requestedBy) {
		this.requestedBy = requestedBy;
	}
	@Override
	public String toString() {
		return "BudgetSubmitInputBean [woId=" + woId + ", requestedBy="
				+ requestedBy + "]";
	}
	
	
}
