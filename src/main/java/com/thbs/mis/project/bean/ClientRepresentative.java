package com.thbs.mis.project.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class ClientRepresentative {
	
	@Min(1)
	private Short clientRepresentativeId;

	@NotEmpty(message = "clientRepresentativeName should not be empty.")
	@NotNull(message = "clientRepresentativeName should not be null.")
	private String clientRepresentativeName;
	
	@NotEmpty(message = "clientRepresentativeEmailId should not be empty.")
	@NotNull(message = "clientRepresentativeEmailId should not be null.")
	@Email
	private String clientRepresentativeEmailId;

	public Short getClientRepresentativeId() {
		return clientRepresentativeId;
	}

	public void setClientRepresentativeId(Short clientRepresentativeId) {
		this.clientRepresentativeId = clientRepresentativeId;
	}

	public String getClientRepresentativeName() {
		return clientRepresentativeName;
	}

	public void setClientRepresentativeName(String clientRepresentativeName) {
		this.clientRepresentativeName = clientRepresentativeName;
	}

	public String getClientRepresentativeEmailId() {
		return clientRepresentativeEmailId;
	}

	public void setClientRepresentativeEmailId(String clientRepresentativeEmailId) {
		this.clientRepresentativeEmailId = clientRepresentativeEmailId;
	}

	@Override
	public String toString() {
		return "ClientRepresentative [clientRepresentativeId="
				+ clientRepresentativeId + ", clientRepresentativeName="
				+ clientRepresentativeName + ", clientRepresentativeEmailId="
				+ clientRepresentativeEmailId + "]";
	}
}
