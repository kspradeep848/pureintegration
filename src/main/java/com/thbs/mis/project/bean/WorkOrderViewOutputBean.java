package com.thbs.mis.project.bean;

import java.util.Date;

public class WorkOrderViewOutputBean {

	private Integer woId;
	private String woNumber;
	private String projectName;
	private int noOfPo;
	private int noOfComponent;
	private String woDescription;
	private String woStatus;
	private Date woCreatedDate;

	public Integer getWoId() {
		return woId;
	}

	public void setWoId(Integer woId) {
		this.woId = woId;
	}

	public String getWoNumber() {
		return woNumber;
	}

	public void setWoNumber(String woNumber) {
		this.woNumber = woNumber;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public int getNoOfPo() {
		return noOfPo;
	}

	public void setNoOfPo(int noOfPo) {
		this.noOfPo = noOfPo;
	}

	public int getNoOfComponent() {
		return noOfComponent;
	}

	public void setNoOfComponent(int noOfComponent) {
		this.noOfComponent = noOfComponent;
	}

	public String getWoDescription() {
		return woDescription;
	}

	public void setWoDescription(String woDescription) {
		this.woDescription = woDescription;
	}

	public String getWoStatus() {
		return woStatus;
	}

	public void setWoStatus(String woStatus) {
		this.woStatus = woStatus;
	}

	public Date getWoCreatedDate() {
		return woCreatedDate;
	}

	public void setWoCreatedDate(Date woCreatedDate) {
		this.woCreatedDate = woCreatedDate;
	}

	@Override
	public String toString() {
		return "WorkOrderViewOutputBean [woId=" + woId + ", woNumber=" + woNumber + ", projectName=" + projectName
				+ ", noOfPo=" + noOfPo + ", noOfComponent=" + noOfComponent + ", woDescription=" + woDescription
				+ ", woStatus=" + woStatus + "]";
	}

}
