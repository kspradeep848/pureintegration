package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class BudgetDetailsOutputBean {

	private Integer pkClientBudgetId;
	private Short fkResourceDesignationId;
	private String fkResourceDesignationName;
	private String resourceLocation;
	private Short fkParentLocationId;
	private String fkParentLocationName;
	private Integer noOfResource;
	private Integer fkWorkorderId;
	private Integer budgetedDays;
	private String isBillingRateAvailable;
	private BigDecimal costToClient;
	private Integer createdBy;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date createdOn;
	private String weeklyOffFlag;
	List<InvoiceRateTypeBean> invoiceRateTypeDetails;
	List<WeeklyOffBean> weeklyOffDetails;
	List<HolidayDetailsBean> holidayDetails;
	private BigDecimal rateAmount;
	private Byte rateTypeId;
	private String rateTypeName;
	private BigDecimal additionalAmt;
	private BigDecimal sowAmount;
	private Short versionId;
	private Short currencyTypeId;
	private String currencyTypeName;
	private Integer status;

	public Integer getPkClientBudgetId() {
		return pkClientBudgetId;
	}

	public void setPkClientBudgetId(Integer clientBudgetId) {
		this.pkClientBudgetId = clientBudgetId;
	}

	public Short getFkResourceDesignationId() {
		return fkResourceDesignationId;
	}

	public void setFkResourceDesignationId(Short fkResourceDesignationId) {
		this.fkResourceDesignationId = fkResourceDesignationId;
	}

	public String getFkResourceDesignationName() {
		return fkResourceDesignationName;
	}

	public void setFkResourceDesignationName(String fkResourceDesignationName) {
		this.fkResourceDesignationName = fkResourceDesignationName;
	}

	public String getResourceLocation() {
		return resourceLocation;
	}

	public void setResourceLocation(String resourceLocation) {
		this.resourceLocation = resourceLocation;
	}

	public Short getFkParentLocationId() {
		return fkParentLocationId;
	}

	public void setFkParentLocationId(Short fkParentLocationId) {
		this.fkParentLocationId = fkParentLocationId;
	}

	public String getFkParentLocationName() {
		return fkParentLocationName;
	}

	public void setFkParentLocationName(String fkParentLocationName) {
		this.fkParentLocationName = fkParentLocationName;
	}

	public Integer getNoOfResource() {
		return noOfResource;
	}

	public void setNoOfResource(Integer noOfResource) {
		this.noOfResource = noOfResource;
	}

	public Integer getFkWorkorderId() {
		return fkWorkorderId;
	}

	public void setFkWorkorderId(Integer fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}

	public Integer getBudgetedDays() {
		return budgetedDays;
	}

	public void setBudgetedDays(Integer budgetedDays) {
		this.budgetedDays = budgetedDays;
	}

	public String getIsBillingRateAvailable() {
		return isBillingRateAvailable;
	}

	public void setIsBillingRateAvailable(String isBillingRateAvailable) {
		this.isBillingRateAvailable = isBillingRateAvailable;
	}

	public BigDecimal getCostToClient() {
		return costToClient;
	}

	public void setCostToClient(BigDecimal costToClient) {
		this.costToClient = costToClient;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getWeeklyOffFlag() {
		return weeklyOffFlag;
	}

	public void setWeeklyOffFlag(String weeklyOffFlag) {
		this.weeklyOffFlag = weeklyOffFlag;
	}

	public List<InvoiceRateTypeBean> getInvoiceRateTypeDetails() {
		return invoiceRateTypeDetails;
	}

	public void setInvoiceRateTypeDetails(
			List<InvoiceRateTypeBean> invoiceRateTypeDetails) {
		this.invoiceRateTypeDetails = invoiceRateTypeDetails;
	}

	public List<WeeklyOffBean> getWeeklyOffDetails() {
		return weeklyOffDetails;
	}

	public void setWeeklyOffDetails(List<WeeklyOffBean> weeklyOffDetails) {
		this.weeklyOffDetails = weeklyOffDetails;
	}

	public List<HolidayDetailsBean> getHolidayDetails() {
		return holidayDetails;
	}

	public void setHolidayDetails(List<HolidayDetailsBean> holidayDetails) {
		this.holidayDetails = holidayDetails;
	}

	public BigDecimal getRateAmount() {
		return rateAmount;
	}

	public void setRateAmount(BigDecimal rateAmount) {
		this.rateAmount = rateAmount;
	}
	
	public BigDecimal getAdditionalAmt() {
		return additionalAmt;
	}

	public void setAdditionalAmt(BigDecimal additionalAmt) {
		this.additionalAmt = additionalAmt;
	}
    
	public BigDecimal getSowAmount() {
		return sowAmount;
	}

	public void setSowAmount(BigDecimal sowAmount) {
		this.sowAmount = sowAmount;
	}
	
	public Short getVersionId() {
		return versionId;
	}

	public void setVersionId(Short versionId) {
		this.versionId = versionId;
	}

	public Short getCurrencyTypeId() {
		return currencyTypeId;
	}

	public void setCurrencyTypeId(Short currencyTypeId) {
		this.currencyTypeId = currencyTypeId;
	}

	public String getCurrencyTypeName() {
		return currencyTypeName;
	}

	public void setCurrencyTypeName(String currencyTypeName) {
		this.currencyTypeName = currencyTypeName;
	}

	public String getRateTypeName() {
		return rateTypeName;
	}

	public void setRateTypeName(String rateTypeName) {
		this.rateTypeName = rateTypeName;
	}

	public Byte getRateTypeId() {
		return rateTypeId;
	}

	public void setRateTypeId(Byte rateTypeId) {
		this.rateTypeId = rateTypeId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "BudgetDetailsOutputBean [pkClientBudgetId=" + pkClientBudgetId
				+ ", fkResourceDesignationId=" + fkResourceDesignationId
				+ ", fkResourceDesignationName=" + fkResourceDesignationName
				+ ", resourceLocation=" + resourceLocation
				+ ", fkParentLocationId=" + fkParentLocationId
				+ ", fkParentLocationName=" + fkParentLocationName
				+ ", noOfResource=" + noOfResource + ", fkWorkorderId="
				+ fkWorkorderId + ", budgetedDays=" + budgetedDays
				+ ", isBillingRateAvailable=" + isBillingRateAvailable
				+ ", costToClient=" + costToClient + ", createdBy=" + createdBy
				+ ", createdOn=" + createdOn + ", weeklyOffFlag="
				+ weeklyOffFlag + ", invoiceRateTypeDetails="
				+ invoiceRateTypeDetails + ", weeklyOffDetails="
				+ weeklyOffDetails + ", holidayDetails=" + holidayDetails
				+ ", rateAmount=" + rateAmount + ", rateTypeId=" + rateTypeId
				+ ", rateTypeName=" + rateTypeName + ", additionalAmt="
				+ additionalAmt + ", sowAmount=" + sowAmount + ", versionId="
				+ versionId + ", currencyTypeId=" + currencyTypeId
				+ ", currencyTypeName=" + currencyTypeName + ", status="
				+ status + "]";
	}
}     
