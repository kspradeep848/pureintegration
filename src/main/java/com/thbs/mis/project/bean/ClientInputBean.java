package com.thbs.mis.project.bean;

import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class ClientInputBean {

	@Min(1)
	private Short clientId;

	@NotBlank(message = "clientName should not be blank.")
	@NotEmpty(message = "clientName should not be empty.")
	@NotNull(message = "clientName should not be null.")
	private String clientName;

	@Valid
	@Size(max = 4)
	List<ClientAddressBean> clientAddresses;

	@NotBlank(message = "contactNumberOne should not be blank.")
	@NotEmpty(message = "contactNumberOne should not be empty.")
	@NotNull(message = "contactNumberOne should not be null.")
	private String contactNumberOne;

	private String contactNumberTwo;

	@Email
	private String emailId;

	@Valid
	private List<ClientRepresentative> clientRepresentativeList;

	@Min(1)
	@NotNull(message = "regionId field cannot be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "regionId must be short.")
	private Short regionId;

	private String panNumber;

	private String gstNumber;

	private String vatNumber;

	private String otherReferenceNumber;

	@Min(0)
	@NotNull(message = "creditPeriod field cannot be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "creditPeriod must be Short.")
	private Short creditPeriod;

	@NotBlank(message = "riskAssessment should not be blank.")
	@NotEmpty(message = "riskAssessment should not be empty.")
	@NotNull(message = "riskAssessment should not be null.")
	private String riskAssessment;

	@Min(0)
	@NotNull(message = "noOfDays should not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "noOfDays must be an Integer.")
	private Integer noOfDays;

	private Byte[] masBusinessEntity;

	@NotBlank(message = "poType cannot be blank.")
	@NotEmpty(message = "poType cannot be empty.")
	@NotNull(message = "poType cannot be null.")
	private String poType;
	
	private String vendorCode;

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "createdBy must be an Integer.")
	private Integer createdBy;

	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "createdBy must be an Integer.")
	private Integer updatedBy;

	private String upadtedComments;

	public Short getClientId() {
		return clientId;
	}

	public void setClientId(Short clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public List<ClientAddressBean> getClientAddresses() {
		return clientAddresses;
	}

	public void setClientAddresses(List<ClientAddressBean> clientAddresses) {
		this.clientAddresses = clientAddresses;
	}

	public String getContactNumberOne() {
		return contactNumberOne;
	}

	public void setContactNumberOne(String contactNumberOne) {
		this.contactNumberOne = contactNumberOne;
	}

	public String getContactNumberTwo() {
		return contactNumberTwo;
	}

	public void setContactNumberTwo(String contactNumberTwo) {
		this.contactNumberTwo = contactNumberTwo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public List<ClientRepresentative> getClientRepresentativeList() {
		return clientRepresentativeList;
	}

	public void setClientRepresentativeList(
			List<ClientRepresentative> clientRepresentativeList) {
		this.clientRepresentativeList = clientRepresentativeList;
	}

	public Short getRegionId() {
		return regionId;
	}

	public void setRegionId(Short regionId) {
		this.regionId = regionId;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public String getOtherReferenceNumber() {
		return otherReferenceNumber;
	}

	public void setOtherReferenceNumber(String otherReferenceNumber) {
		this.otherReferenceNumber = otherReferenceNumber;
	}

	public Short getCreditPeriod() {
		return creditPeriod;
	}

	public void setCreditPeriod(Short creditPeriod) {
		this.creditPeriod = creditPeriod;
	}

	public String getRiskAssessment() {
		return riskAssessment;
	}

	public void setRiskAssessment(String riskAssessment) {
		this.riskAssessment = riskAssessment;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Byte[] getMasBusinessEntity() {
		return masBusinessEntity;
	}

	public void setMasBusinessEntity(Byte[] masBusinessEntity) {
		this.masBusinessEntity = masBusinessEntity;
	}

	public String getPoType() {
		return poType;
	}

	public void setPoType(String poType) {
		this.poType = poType;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpadtedComments() {
		return upadtedComments;
	}

	public void setUpadtedComments(String upadtedComments) {
		this.upadtedComments = upadtedComments;
	}

	@Override
	public String toString() {
		return "ClientInputBean [clientId=" + clientId + ", clientName="
				+ clientName + ", clientAddresses=" + clientAddresses
				+ ", contactNumberOne=" + contactNumberOne
				+ ", contactNumberTwo=" + contactNumberTwo + ", emailId="
				+ emailId + ", clientRepresentativeList="
				+ clientRepresentativeList + ", regionId=" + regionId
				+ ", panNumber=" + panNumber + ", gstNumber=" + gstNumber
				+ ", vatNumber=" + vatNumber + ", otherReferenceNumber="
				+ otherReferenceNumber + ", creditPeriod=" + creditPeriod
				+ ", riskAssessment=" + riskAssessment + ", noOfDays="
				+ noOfDays + ", masBusinessEntity="
				+ Arrays.toString(masBusinessEntity) + ", poType=" + poType
				+ ", vendorCode=" + vendorCode + ", createdBy=" + createdBy
				+ ", updatedBy=" + updatedBy + ", upadtedComments="
				+ upadtedComments + "]";
	}
}
