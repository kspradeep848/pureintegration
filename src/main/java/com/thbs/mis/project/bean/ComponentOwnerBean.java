package com.thbs.mis.project.bean;

import java.util.Comparator;

public class ComponentOwnerBean implements Comparator<ComponentOwnerBean> {
	private Integer compOwnerId;
	private String compOwnerName;
	public Integer getCompOwnerId() {
		return compOwnerId;
	}
	public void setCompOwnerId(Integer compOwnerId) {
		this.compOwnerId = compOwnerId;
	}
	public String getCompOwnerName() {
		return compOwnerName;
	}
	public void setCompOwnerName(String compOwnerName) {
		this.compOwnerName = compOwnerName;
	}
	
	@Override
	public String toString() {
		return "ComponentOwner [compOwnerId=" + compOwnerId + ", compOwnerName="
				+ compOwnerName + "]";
	}
	
	@Override
	public int compare(ComponentOwnerBean bean, ComponentOwnerBean bean1) {
		// TODO Auto-generated method stub
		return bean.getCompOwnerName().compareTo(bean1.getCompOwnerName());
	}
}