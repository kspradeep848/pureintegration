package com.thbs.mis.project.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class ProjMasClientManagerBean 
{
	@Min(1)
	@NotNull(message = "clientManagerId field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "clientManagerId must be an Short.")
	private Short clientManagerId;
	
	@NotBlank(message = "clientManagerName should not be blank.")
	@NotEmpty(message = "clientManagerName should not be empty.")
	@NotNull(message = "clientManagerName should not be null.")
	private String clientManagerName;
	
	@NotBlank(message = "clientManagerEmailId should not be blank.")
	@NotEmpty(message = "clientManagerEmailId should not be empty.")
	@NotNull(message = "clientManagerEmailId should not be null.")
	private String clientManagerEmailId;
	
	public Short getClientManagerId() {
		return clientManagerId;
	}
	public void setClientManagerId(Short clientManagerId) {
		this.clientManagerId = clientManagerId;
	}
	public String getClientManagerName() {
		return clientManagerName;
	}
	public void setClientManagerName(String clientManagerName) {
		this.clientManagerName = clientManagerName;
	}
	public String getClientManagerEmailId() {
		return clientManagerEmailId;
	}
	public void setClientManagerEmailId(String clientManagerEmailId) {
		this.clientManagerEmailId = clientManagerEmailId;
	}
	@Override
	public String toString() {
		return "ProjMasClientManagerBean [clientManagerId=" + clientManagerId
				+ ", clientManagerName=" + clientManagerName
				+ ", clientManagerEmailId=" + clientManagerEmailId + "]";
	}
	
	
}
