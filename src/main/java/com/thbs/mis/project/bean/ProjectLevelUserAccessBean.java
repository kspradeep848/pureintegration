package com.thbs.mis.project.bean;

public class ProjectLevelUserAccessBean {

	private Integer projectLevelUserAccessId;
	private String projectLevelUserAccessName;
	private String projectLevelUserAccess;
	
	public Integer getProjectLevelUserAccessId() {
		return projectLevelUserAccessId;
	}
	public void setProjectLevelUserAccessId(Integer projectLevelUserAccessId) {
		this.projectLevelUserAccessId = projectLevelUserAccessId;
	}
	public String getProjectLevelUserAccessName() {
		return projectLevelUserAccessName;
	}
	public void setProjectLevelUserAccessName(String projectLevelUserAccessName) {
		this.projectLevelUserAccessName = projectLevelUserAccessName;
	}
	public String getProjectLevelUserAccess() {
		return projectLevelUserAccess;
	}
	public void setProjectLevelUserAccess(String projectLevelUserAccess) {
		this.projectLevelUserAccess = projectLevelUserAccess;
	}
	@Override
	public String toString() {
		return "ProjectLevelUserAccessBean [projectLevelUserAccessId="
				+ projectLevelUserAccessId + ", projectLevelUserAccessName="
				+ projectLevelUserAccessName + ", projectLevelUserAccess="
				+ projectLevelUserAccess + "]";
	}
}
