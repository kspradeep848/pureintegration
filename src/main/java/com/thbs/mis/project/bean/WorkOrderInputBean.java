package com.thbs.mis.project.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomDateSerializer;

public class WorkOrderInputBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NumberFormat(style = Style.NUMBER, pattern = "projectId should be numeric value")
	@NotNull(message = "projectId can not be null")
	@Min(1)
	private Integer projectId;

	private Integer woId;

	
	private BigDecimal additionalAmount;

	@NotNull(message = "sowAmount field can not be null.")
	private BigDecimal sowAmount;

	@JsonSerialize(using = CustomDateSerializer.class)
	@NotNull(message = "woEndDate field can not be null.")
	private Date woEndDate;

	@JsonSerialize(using = CustomDateSerializer.class)
	@NotNull(message = "woStartDate field can not be null.")
	private Date woStartDate;

	@NotBlank(message = "woDescription should not be blank.")
	@NotEmpty(message = "woDescription should not be empty.")
	@NotNull(message = "woDescription should not be null.")
	private String woDescription;

	@NotBlank(message = "woInitial should not be blank.")
	@NotEmpty(message = "woInitial should not be empty.")
	@NotNull(message = "woInitial should not be null.")
	private String woInitial;

	@NumberFormat(style = Style.NUMBER, pattern = "empId should be numeric value")
	@NotNull(message = "empId can not be null")
	@Min(1)
	private Integer deliveryManagerId;

	@NotNull(message = "sowCurrencyId can not be null")
	private short sowCurrencyId;

	@NumberFormat(style = Style.NUMBER, pattern = "woCreatedBy should be numeric value")
	@NotNull(message = "woCreatedBy can not be null")
	@Min(1)
	private Integer woCreatedBy;

	@NotNull(message = "fkWoStatusId can not be null")
	private byte fkWoStatusId;

	private BigDecimal discountValue;

	private Short paymentInstructionId;

	private Short clientManagerId;

	@NotBlank(message = "clientManagerName should not be blank.")
	@NotEmpty(message = "clientManagerName should not be empty.")
	@NotNull(message = "clientManagerName should not be null.")
	private String clientManagerName;

	private String clientManagerEmailId;

	private Integer woUpdatedBy;

	private String commentForUpdate;
	
	private String woNumber;

	@NotBlank(message = "isInternal should not be blank.")
	@NotEmpty(message = "isInternal should not be empty.")
	@NotNull(message = "isInternal can not be null")
	private String isInternal;
	
	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public BigDecimal getAdditionalAmount() {
		return additionalAmount;
	}

	public void setAdditionalAmount(BigDecimal additionalAmount) {
		this.additionalAmount = additionalAmount;
	}

	public BigDecimal getSowAmount() {
		return sowAmount;
	}

	public void setSowAmount(BigDecimal sowAmount) {
		this.sowAmount = sowAmount;
	}

	public Date getWoEndDate() {
		return woEndDate;
	}

	public void setWoEndDate(Date woEndDate) {
		this.woEndDate = woEndDate;
	}

	public Date getWoStartDate() {
		return woStartDate;
	}

	public void setWoStartDate(Date woStartDate) {
		this.woStartDate = woStartDate;
	}

	public String getWoDescription() {
		return woDescription;
	}

	public void setWoDescription(String woDescription) {
		this.woDescription = woDescription;
	}

	public String getWoInitial() {
		return woInitial;
	}

	public void setWoInitial(String woInitial) {
		this.woInitial = woInitial;
	}

	public Integer getDeliveryManagerId() {
		return deliveryManagerId;
	}

	public void setDeliveryManagerId(Integer deliveryManagerId) {
		this.deliveryManagerId = deliveryManagerId;
	}

	public short getSowCurrencyId() {
		return sowCurrencyId;
	}

	public void setSowCurrencyId(short sowCurrencyId) {
		this.sowCurrencyId = sowCurrencyId;
	}

	public Integer getWoCreatedBy() {
		return woCreatedBy;
	}

	public void setWoCreatedBy(Integer woCreatedBy) {
		this.woCreatedBy = woCreatedBy;
	}

	public byte getFkWoStatusId() {
		return fkWoStatusId;
	}

	public void setFkWoStatusId(byte fkWoStatusId) {
		this.fkWoStatusId = fkWoStatusId;
	}

	public BigDecimal getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(BigDecimal discountValue) {
		this.discountValue = discountValue;
	}

	public Short getPaymentInstructionId() {
		return paymentInstructionId;
	}

	public void setPaymentInstructionId(Short paymentInstructionId) {
		this.paymentInstructionId = paymentInstructionId;
	}


	public Integer getWoId() {
		return woId;
	}

	public void setWoId(Integer woId) {
		this.woId = woId;
	}

	public Integer getWoUpdatedBy() {
		return woUpdatedBy;
	}

	public void setWoUpdatedBy(Integer woUpdatedBy) {
		this.woUpdatedBy = woUpdatedBy;
	}

	public String getCommentForUpdate() {
		return commentForUpdate;
	}

	public void setCommentForUpdate(String commentForUpdate) {
		this.commentForUpdate = commentForUpdate;
	}
	

	public String getIsInternal() {
		return isInternal;
	}

	public void setIsInternal(String isInternal) {
		this.isInternal = isInternal;
	}

	public Short getClientManagerId() {
		return clientManagerId;
	}

	public void setClientManagerId(Short clientManagerId) {
		this.clientManagerId = clientManagerId;
	}

	public String getClientManagerName() {
		return clientManagerName;
	}

	public void setClientManagerName(String clientManagerName) {
		this.clientManagerName = clientManagerName;
	}

	public String getClientManagerEmailId() {
		return clientManagerEmailId;
	}

	public void setClientManagerEmailId(String clientManagerEmailId) {
		this.clientManagerEmailId = clientManagerEmailId;
	}

	public String getWoNumber() {
		return woNumber;
	}

	public void setWoNumber(String woNumber) {
		this.woNumber = woNumber;
	}

	@Override
	public String toString() {
		return "WorkOrderInputBean [projectId=" + projectId + ", woId=" + woId + ", additionalAmount="
				+ additionalAmount + ", sowAmount=" + sowAmount + ", woEndDate=" + woEndDate + ", woStartDate="
				+ woStartDate + ", woDescription=" + woDescription + ", woInitial=" + woInitial + ", deliveryManagerId="
				+ deliveryManagerId + ", sowCurrencyId=" + sowCurrencyId + ", woCreatedBy=" + woCreatedBy
				+ ", fkWoStatusId=" + fkWoStatusId + ", discountValue=" + discountValue
				+ ", paymentInstructionId=" + paymentInstructionId + ", clientManagerId=" + clientManagerId
				+ ", clientManagerName=" + clientManagerName + ", clientManagerEmailId=" + clientManagerEmailId
				+ ", woUpdatedBy=" + woUpdatedBy + ", commentForUpdate=" + commentForUpdate + ", isInternal="
				+ isInternal + "]";
	}
	
}
