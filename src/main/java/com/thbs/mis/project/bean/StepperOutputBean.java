package com.thbs.mis.project.bean;

public class StepperOutputBean {

	private String clientCreated;
	private String projectCreated;
	private String woCreated;
	private String sowUploaded;
	private String clientBudgetCreated;
	private String thbsBudgetCreated;
	private String budgetApproval;
	private String componentCreated;
	private String milestoneCreated;
	private byte isInternalWo;

	public String getClientCreated() {
		return clientCreated;
	}

	public void setClientCreated(String clientCreated) {
		this.clientCreated = clientCreated;
	}

	public String getProjectCreated() {
		return projectCreated;
	}

	public void setProjectCreated(String projectCreated) {
		this.projectCreated = projectCreated;
	}

	public String getWoCreated() {
		return woCreated;
	}

	public void setWoCreated(String woCreated) {
		this.woCreated = woCreated;
	}

	public String getSowUploaded() {
		return sowUploaded;
	}

	public void setSowUploaded(String sowUploaded) {
		this.sowUploaded = sowUploaded;
	}

	public String getClientBudgetCreated() {
		return clientBudgetCreated;
	}

	public void setClientBudgetCreated(String clientBudgetCreated) {
		this.clientBudgetCreated = clientBudgetCreated;
	}

	public String getThbsBudgetCreated() {
		return thbsBudgetCreated;
	}

	public void setThbsBudgetCreated(String thbsBudgetCreated) {
		this.thbsBudgetCreated = thbsBudgetCreated;
	}

	public String getBudgetApproval() {
		return budgetApproval;
	}

	public void setBudgetApproval(String budgetApproval) {
		this.budgetApproval = budgetApproval;
	}

	public String getComponentCreated() {
		return componentCreated;
	}

	public void setComponentCreated(String componentCreated) {
		this.componentCreated = componentCreated;
	}

	public String getMilestoneCreated() {
		return milestoneCreated;
	}

	public void setMilestoneCreated(String milestoneCreated) {
		this.milestoneCreated = milestoneCreated;
	}

	public byte getIsInternalWo() {
		return isInternalWo;
	}

	public void setIsInternalWo(byte isInternalWo) {
		this.isInternalWo = isInternalWo;
	}

	@Override
	public String toString() {
		return "StepperOutputBean [clientCreated=" + clientCreated
				+ ", projectCreated=" + projectCreated + ", woCreated="
				+ woCreated + ", sowUploaded=" + sowUploaded
				+ ", clientBudgetCreated=" + clientBudgetCreated
				+ ", thbsBudgetCreated=" + thbsBudgetCreated
				+ ", budgetApproval=" + budgetApproval + ", componentCreated="
				+ componentCreated + ", milestoneCreated=" + milestoneCreated
				+ ", isInternalWo=" + isInternalWo + "]";
	}
	
}
