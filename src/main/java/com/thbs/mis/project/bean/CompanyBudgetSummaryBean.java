package com.thbs.mis.project.bean;

import java.math.BigDecimal;

public class CompanyBudgetSummaryBean {

	private String role;
	private String level;
	private String location;
	private Integer budgetDays;
	private BigDecimal standardRate;
	private BigDecimal costToCompany;
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Integer getBudgetDays() {
		return budgetDays;
	}
	public void setBudgetDays(Integer budgetDays) {
		this.budgetDays = budgetDays;
	}
	public BigDecimal getStandardRate() {
		return standardRate;
	}
	public void setStandardRate(BigDecimal standardRate) {
		this.standardRate = standardRate;
	}
	public BigDecimal getCostToCompany() {
		return costToCompany;
	}
	public void setCostToCompany(BigDecimal costToCompany) {
		this.costToCompany = costToCompany;
	}
	
	@Override
	public String toString() {
		return "CompanyBudgetSummaryBean [role=" + role + ", level=" + level
				+ ", location=" + location + ", budgetDays=" + budgetDays
				+ ", standardRate=" + standardRate + ", costToCompany="
				+ costToCompany + "]";
	}
}
