package com.thbs.mis.project.bean;

import java.util.List;

public class TriggerInput {

	private List<TriggerInputDetails> lstTriggerInputDetails;
	private Integer triggerBy;

	public List<TriggerInputDetails> getLstTriggerInputDetails() {
		return lstTriggerInputDetails;
	}

	public void setLstTriggerInputDetails(List<TriggerInputDetails> lstTriggerInputDetails) {
		this.lstTriggerInputDetails = lstTriggerInputDetails;
	}

	public Integer getTriggerBy() {
		return triggerBy;
	}

	public void setTriggerBy(Integer triggerBy) {
		this.triggerBy = triggerBy;
	}

	@Override
	public String toString() {
		return "TriggerInput [lstTriggerInputDetails=" + lstTriggerInputDetails + ", triggerBy=" + triggerBy + "]";
	}

}
