package com.thbs.mis.project.bean;


public class DatMappingClientWithTheirMgrBean {
	private int pkMappingClientMgrsId;
	
	private short fkClientId;
	
	private short fkClientManagerId;

	public int getPkMappingClientMgrsId() {
		return pkMappingClientMgrsId;
	}


	public void setPkMappingClientMgrsId(int pkMappingClientMgrsId) {
		this.pkMappingClientMgrsId = pkMappingClientMgrsId;
	}


	public short getFkClientId() {
		return fkClientId;
	}


	public void setFkClientId(short fkClientId) {
		this.fkClientId = fkClientId;
	}


	public short getFkClientManagerId() {
		return fkClientManagerId;
	}


	public void setFkClientManagerId(short fkClientManagerId) {
		this.fkClientManagerId = fkClientManagerId;
	}


	@Override
	public String toString() {
		return "DatMappingClientWithTheirMgrBean [pkMappingClientMgrsId="
				+ pkMappingClientMgrsId + ", fkClientId=" + fkClientId
				+ ", fkClientManagerId=" + fkClientManagerId + "]";
	}
	
	
}
