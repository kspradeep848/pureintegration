package com.thbs.mis.project.bean;

import java.math.BigDecimal;

public class ComponentDetailsOutputBean2 {
	
	private int componentId;
	
	private String componentName;
	
	private int noOfResources;
	
	private int noOfMileStones;
	
	private BigDecimal componentValue;
	
	private String componentStatus;
	
	private String componentType;
	
	private BigDecimal componentBalance;

	public int getComponentId() {
		return componentId;
	}

	public void setComponentId(int componentId) {
		this.componentId = componentId;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public int getNoOfResources() {
		return noOfResources;
	}

	public void setNoOfResources(int noOfResources) {
		this.noOfResources = noOfResources;
	}

	public int getNoOfMileStones() {
		return noOfMileStones;
	}

	public void setNoOfMileStones(int noOfMileStones) {
		this.noOfMileStones = noOfMileStones;
	}

	public BigDecimal getComponentValue() {
		return componentValue;
	}

	public void setComponentValue(BigDecimal componentValue) {
		this.componentValue = componentValue;
	}

	public String getComponentStatus() {
		return componentStatus;
	}

	public void setComponentStatus(String componentStatus) {
		this.componentStatus = componentStatus;
	}

	public String getComponentType() {
		return componentType;
	}

	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}

	public BigDecimal getComponentBalance() {
		return componentBalance;
	}

	public void setComponentBalance(BigDecimal componentBalance) {
		this.componentBalance = componentBalance;
	}

	@Override
	public String toString() {
		return "ComponentDetailsOutputBean2 [componentId=" + componentId
				+ ", componentName=" + componentName + ", noOfResources="
				+ noOfResources + ", noOfMileStones=" + noOfMileStones
				+ ", componentValue=" + componentValue + ", componentStatus="
				+ componentStatus + ", componentType=" + componentType
				+ ", componentBalance=" + componentBalance + "]";
	}
	
	
}
