package com.thbs.mis.project.bean;

import java.math.BigDecimal;

public class ProjectPercentCalculationOutputBean 
{
private BigDecimal sumOfTriggeredValue;
private BigDecimal projectValue;
//private Integer projectId;

public ProjectPercentCalculationOutputBean(BigDecimal sumOfTriggeredValue,
		BigDecimal projectValue) {
	super();
	this.sumOfTriggeredValue = sumOfTriggeredValue;
	this.projectValue = projectValue;
	
}

public BigDecimal getSumOfTriggeredValue() {
	return sumOfTriggeredValue;
}

public void setSumOfTriggeredValue(BigDecimal sumOfTriggeredValue) {
	this.sumOfTriggeredValue = sumOfTriggeredValue;
}

public BigDecimal getProjectValue() {
	return projectValue;
}

public void setProjectValue(BigDecimal projectValue) {
	this.projectValue = projectValue;
}


@Override
public String toString() {
	return "ProjectPercentCalculationOutputBean [sumOfTriggeredValue="
			+ sumOfTriggeredValue + ", projectValue=" + projectValue
			 + "]";
}


}
