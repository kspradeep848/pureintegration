package com.thbs.mis.project.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class ViewHistoryInputBean {

	@NumberFormat(style = Style.NUMBER, pattern = "type should be numeric value")
	@NotNull(message = "type can not be null")
	@Min(1)
	private Integer type;

	@NumberFormat(style = Style.NUMBER, pattern = "id should be numeric value")
	@NotNull(message = "id can not be null")
	@Min(1)
	private Integer id;

	@NumberFormat(style = Style.NUMBER, pattern = "viewedBy should be numeric value")
	@NotNull(message = "viewedBy can not be null")
	@Min(1)
	private Integer viewedBy;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getViewedBy() {
		return viewedBy;
	}

	public void setViewedBy(Integer viewedBy) {
		this.viewedBy = viewedBy;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "ViewHistoryInputBean [type=" + type + ", id=" + id + ", viewedBy=" + viewedBy + "]";
	}

}
