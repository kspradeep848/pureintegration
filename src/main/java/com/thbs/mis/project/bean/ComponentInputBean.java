package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;


public class ComponentInputBean {
	
	private int compId;
	
	private String compComments;
	
	@NotNull(message="Component description cannot be null")
	@NotBlank(message="Component description cannot be blank")
	private String compDescription;
	
	@NotNull(message="Component start date cannot be null")
	private Date compStartDate;
	
	@NotNull(message="Component end date cannot be null")
	private Date compEndDate;
	
	
	private BigDecimal compAmount;
	
	private BigDecimal additionalCompAmt;
	
	@NotNull(message = "Component name cannot be null")
	@NotBlank(message="Component name cannot be blank")
	private String compName;
	
	private Short compStatusId;
	
	@Min(value=1,message="Enter valid ComponentType")
	@Max(value=3,message="Enter valid ComponentTypeId")
	private byte compTypeId;
	
	private int woId;
	
	private Integer compCreatedBy;
	
	private Integer compOwner;
	
	private Integer compUpdatedBy;
	
	private String isComponentOrSubProject;
	
	private String commentsForUpdate;

	public int getCompId() {
		return compId;
	}

	public void setCompId(int compId) {
		this.compId = compId;
	}

	public String getCompComments() {
		return compComments;
	}

	public void setCompComments(String compComments) {
		this.compComments = compComments;
	}

	public String getCompDescription() {
		return compDescription;
	}

	public void setCompDescription(String compDescription) {
		this.compDescription = compDescription;
	}

	public Date getCompStartDate() {
		return compStartDate;
	}

	public void setCompStartDate(Date compStartDate) {
		this.compStartDate = compStartDate;
	}

	public Date getCompEndDate() {
		return compEndDate;
	}

	public void setCompEndDate(Date compEndDate) {
		this.compEndDate = compEndDate;
	}

	public BigDecimal getCompAmount() {
		return compAmount;
	}

	public void setCompAmount(BigDecimal compAmount) {
		this.compAmount = compAmount;
	}

	public BigDecimal getAdditionalCompAmt() {
		return additionalCompAmt;
	}

	public void setAdditionalCompAmt(BigDecimal additionalCompAmt) {
		this.additionalCompAmt = additionalCompAmt;
	}

	public String getCompName() {
		return compName;
	}

	public void setCompName(String compName) {
		this.compName = compName;
	}

	
	public Short getCompStatusId() {
		return compStatusId;
	}

	public void setCompStatusId(Short compStatusId) {
		this.compStatusId = compStatusId;
	}

	public byte getCompTypeId() {
		return compTypeId;
	}

	public void setCompTypeId(byte compTypeId) {
		this.compTypeId = compTypeId;
	}

	public int getWoId() {
		return woId;
	}

	public void setWoId(int woId) {
		this.woId = woId;
	}

	public Integer getCompCreatedBy() {
		return compCreatedBy;
	}

	public void setCompCreatedBy(Integer compCreatedBy) {
		this.compCreatedBy = compCreatedBy;
	}

	public Integer getCompOwner() {
		return compOwner;
	}

	public void setCompOwner(Integer compOwner) {
		this.compOwner = compOwner;
	}

	public Integer getCompUpdatedBy() {
		return compUpdatedBy;
	}

	public void setCompUpdatedBy(Integer compUpdatedBy) {
		this.compUpdatedBy = compUpdatedBy;
	}

	public String getIsComponentOrSubProject() {
		return isComponentOrSubProject;
	}

	public void setIsComponentOrSubProject(String isComponentOrSubProject) {
		this.isComponentOrSubProject = isComponentOrSubProject;
	}

	public String getCommentsForUpdate() {
		return commentsForUpdate;
	}

	public void setCommentsForUpdate(String commentsForUpdate) {
		this.commentsForUpdate = commentsForUpdate;
	}

	@Override
	public String toString() {
		return "ComponentInputBean [compId=" + compId + ", compComments="
				+ compComments + ", compDescription=" + compDescription
				+ ", compStartDate=" + compStartDate + ", compEndDate="
				+ compEndDate + ", compAmount=" + compAmount
				+ ", additionalCompAmt=" + additionalCompAmt + ", compName="
				+ compName + ", compStatusId=" + compStatusId + ", compTypeId="
				+ compTypeId + ", woId=" + woId + ", compCreatedBy="
				+ compCreatedBy + ", compOwner=" + compOwner
				+ ", compUpdatedBy=" + compUpdatedBy
				+ ", isComponentOrSubProject=" + isComponentOrSubProject
				+ ", commentsForUpdate=" + commentsForUpdate + "]";
	}

	
}
