package com.thbs.mis.project.bean;

public class GetProjectsOutputBean {

	private Integer projectId;
	private String projectName;
	private String deliveryMangerId;

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getDeliveryMangerId() {
		return deliveryMangerId;
	}

	public void setDeliveryMangerId(String deliveryMangerId) {
		this.deliveryMangerId = deliveryMangerId;
	}

	@Override
	public String toString() {
		return "GetProjectsOutputBean [projectId=" + projectId
				+ ", projectName=" + projectName + ", deliveryMangerId="
				+ deliveryMangerId + "]";
	}
}
