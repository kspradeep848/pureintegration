package com.thbs.mis.project.bean;

import java.math.BigDecimal;

public class ProjMasClientBudgetDetailsBean {

	private int pkclientBudgetResourceId;
	private short  resourceRoleId;
	private String resourceRoleName;
	private String location;
	private Integer budgetedDays;
	private BigDecimal billingRate;
	private int noOfResource;
	private BigDecimal costToClient;
	
	public BigDecimal getBillingRate() {
		return billingRate;
	}
	public int getPkclientBudgetResourceId() {
		return pkclientBudgetResourceId;
	}
	public void setPkclientBudgetResourceId(int pkclientBudgetResourceId) {
		this.pkclientBudgetResourceId = pkclientBudgetResourceId;
	}
	public void setBillingRate(BigDecimal billingRate) {
		this.billingRate = billingRate;
	}
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Integer getBudgetedDays() {
		return budgetedDays;
	}
	public void setBudgetedDays(Integer budgetedDays) {
		this.budgetedDays = budgetedDays;
	}
	public BigDecimal getCostToClient() {
		return costToClient;
	}
	public void setCostToClient(BigDecimal costToClient) {
		this.costToClient = costToClient;
	}
	public short getResourceRoleId() {
		return resourceRoleId;
	}
	public void setResourceRoleId(short resourceRoleId) {
		this.resourceRoleId = resourceRoleId;
	}
	public String getResourceRoleName() {
		return resourceRoleName;
	}
	public void setResourceRoleName(String resourceRoleName) {
		this.resourceRoleName = resourceRoleName;
	}
	public int getNoOfResource() {
		return noOfResource;
	}
	public void setNoOfResource(int noOfResource) {
		this.noOfResource = noOfResource;
	}
	@Override
	public String toString() {
		return "ProjMasClientBudgetDetailsBean [pkclientBudgetResourceId="
				+ pkclientBudgetResourceId + ", resourceRoleId="
				+ resourceRoleId + ", resourceRoleName=" + resourceRoleName
				+ ", location=" + location + ", budgetedDays=" + budgetedDays
				+ ", billingRate=" + billingRate + ", noOfResource="
				+ noOfResource + ", costToClient=" + costToClient + "]";
	}

}
