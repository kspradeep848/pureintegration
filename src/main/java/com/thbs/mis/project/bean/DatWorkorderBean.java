package com.thbs.mis.project.bean;

public class DatWorkorderBean 
{
	private Integer woId;
	
	private String woNumber;
	
	

	public DatWorkorderBean(Integer woId, String woNumber) {
		super();
		this.woId = woId;
		this.woNumber = woNumber;
	}

	public Integer getWoId() {
		return woId;
	}

	public void setWoId(Integer woId) {
		this.woId = woId;
	}

	public String getWoNumber() {
		return woNumber;
	}

	public void setWoNumber(String woNumber) {
		this.woNumber = woNumber;
	}

	@Override
	public String toString() {
		return "DatWorkorderBean [woId=" + woId + ", woNumber=" + woNumber
				+ "]";
	}
	


}
