package com.thbs.mis.project.bean;

import java.util.Arrays;
import java.util.Date;

public class UpdateProjectBean {

	private Integer projectId;
	private String projectName;
	private Date projectStartDate;
	private Date projectEndDate;
	private String projectDescription;
	private Short clientId;
	private Short verticalId;
	private Short regionId;
	private Integer deliveryManagerId;
	private Short buUnitId;
	private Short domainId;
	private Integer domainMgrId;
	private String projectType;
	private Short accountId;
	private Integer accountMgrId;
	private String projectBillableFlag;
	private Integer updatedBy;
	private int[] projectCoordinator;
	private int[] projectLevelUserAccess;
	private Byte projectClassificationId;
	private String commentsForUpdation;
	private String isHavingAggregatePo;

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Date getProjectStartDate() {
		return projectStartDate;
	}

	public void setProjectStartDate(Date projectStartDate) {
		this.projectStartDate = projectStartDate;
	}

	public Date getProjectEndDate() {
		return projectEndDate;
	}

	public void setProjectEndDate(Date projectEndDate) {
		this.projectEndDate = projectEndDate;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public Short getClientId() {
		return clientId;
	}

	public void setClientId(Short clientId) {
		this.clientId = clientId;
	}

	public Short getVerticalId() {
		return verticalId;
	}

	public void setVerticalId(Short verticalId) {
		this.verticalId = verticalId;
	}

	public Short getRegionId() {
		return regionId;
	}

	public void setRegionId(Short regionId) {
		this.regionId = regionId;
	}

	public Integer getDeliveryManagerId() {
		return deliveryManagerId;
	}

	public void setDeliveryManagerId(Integer deliveryManagerId) {
		this.deliveryManagerId = deliveryManagerId;
	}

	public Short getBuUnitId() {
		return buUnitId;
	}

	public void setBuUnitId(Short buUnitId) {
		this.buUnitId = buUnitId;
	}

	public Short getDomainId() {
		return domainId;
	}

	public void setDomainId(Short domainId) {
		this.domainId = domainId;
	}

	public Integer getDomainMgrId() {
		return domainMgrId;
	}

	public void setDomainMgrId(Integer domainMgrId) {
		this.domainMgrId = domainMgrId;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public Short getAccountId() {
		return accountId;
	}

	public void setAccountId(Short accountId) {
		this.accountId = accountId;
	}

	public Integer getAccountMgrId() {
		return accountMgrId;
	}

	public void setAccountMgrId(Integer accountMgrId) {
		this.accountMgrId = accountMgrId;
	}

	public String getProjectBillableFlag() {
		return projectBillableFlag;
	}

	public void setProjectBillableFlag(String projectBillableFlag) {
		this.projectBillableFlag = projectBillableFlag;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public int[] getProjectCoordinator() {
		return projectCoordinator;
	}

	public void setProjectCoordinator(int[] projectCoordinator) {
		this.projectCoordinator = projectCoordinator;
	}

	public int[] getProjectLevelUserAccess() {
		return projectLevelUserAccess;
	}

	public void setProjectLevelUserAccess(int[] projectLevelUserAccess) {
		this.projectLevelUserAccess = projectLevelUserAccess;
	}

	public Byte getProjectClassificationId() {
		return projectClassificationId;
	}

	public void setProjectClassificationId(Byte projectClassificationId) {
		this.projectClassificationId = projectClassificationId;
	}

	public String getCommentsForUpdation() {
		return commentsForUpdation;
	}

	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}

	public String getIsHavingAggregatePo() {
		return isHavingAggregatePo;
	}

	public void setIsHavingAggregatePo(String isHavingAggregatePo) {
		this.isHavingAggregatePo = isHavingAggregatePo;
	}

	@Override
	public String toString() {
		return "UpdateProjectBean [projectId=" + projectId + ", projectName="
				+ projectName + ", projectStartDate=" + projectStartDate
				+ ", projectEndDate=" + projectEndDate
				+ ", projectDescription=" + projectDescription + ", clientId="
				+ clientId + ", verticalId=" + verticalId + ", regionId="
				+ regionId + ", deliveryManagerId=" + deliveryManagerId
				+ ", buUnitId=" + buUnitId + ", domainId=" + domainId
				+ ", domainMgrId=" + domainMgrId + ", projectType="
				+ projectType + ", accountId=" + accountId + ", accountMgrId="
				+ accountMgrId + ", projectBillableFlag=" + projectBillableFlag
				+ ", updatedBy=" + updatedBy + ", projectCoordinator="
				+ Arrays.toString(projectCoordinator)
				+ ", projectLevelUserAccess="
				+ Arrays.toString(projectLevelUserAccess)
				+ ", projectClassificationId=" + projectClassificationId
				+ ", commentsForUpdation=" + commentsForUpdation
				+ ", isHavingAggregatePo=" + isHavingAggregatePo + "]";
	}
}