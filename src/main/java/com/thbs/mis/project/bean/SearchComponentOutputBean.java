package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.Date;

public class SearchComponentOutputBean {
	
	private int componentId;
	
	private String componentName;
	
	private int noOfResources;
	
	private int noOfMileStones;
	
	private BigDecimal componentValue;
	
	private String componentStatus;
	
	private String componentType;
	
	private double componentBalance;
	
	private Date componentStartDate;
	
	private Date componentEndDate;
	
	private String componentOwner;

	public int getComponentId() {
		return componentId;
	}

	public void setComponentId(int componentId) {
		this.componentId = componentId;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public int getNoOfResources() {
		return noOfResources;
	}

	public void setNoOfResources(int noOfResources) {
		this.noOfResources = noOfResources;
	}

	public int getNoOfMileStones() {
		return noOfMileStones;
	}

	public void setNoOfMileStones(int noOfMileStones) {
		this.noOfMileStones = noOfMileStones;
	}

	public BigDecimal getComponentValue() {
		return componentValue;
	}

	public void setComponentValue(BigDecimal componentValue) {
		this.componentValue = componentValue;
	}

	public String getComponentStatus() {
		return componentStatus;
	}

	public void setComponentStatus(String componentStatus) {
		this.componentStatus = componentStatus;
	}

	public String getComponentType() {
		return componentType;
	}

	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}

	public double getComponentBalance() {
		return componentBalance;
	}

	public void setComponentBalance(double componentBalance) {
		this.componentBalance = componentBalance;
	}

	public Date getComponentStartDate() {
		return componentStartDate;
	}

	public void setComponentStartDate(Date componentStartDate) {
		this.componentStartDate = componentStartDate;
	}

	public Date getComponentEndDate() {
		return componentEndDate;
	}

	public void setComponentEndDate(Date componentEndDate) {
		this.componentEndDate = componentEndDate;
	}


	public String getComponentOwner() {
		return componentOwner;
	}

	public void setComponentOwner(String componentOwner) {
		this.componentOwner = componentOwner;
	}

	@Override
	public String toString() {
		return "SearchComponentOutputBean [componentId=" + componentId
				+ ", componentName=" + componentName + ", noOfResources="
				+ noOfResources + ", noOfMileStones=" + noOfMileStones
				+ ", componentValue=" + componentValue + ", componentStatus="
				+ componentStatus + ", componentType=" + componentType
				+ ", componentBalance=" + componentBalance
				+ ", componentStartDate=" + componentStartDate
				+ ", componentEndDate=" + componentEndDate
				+ ", componentOwner=" + componentOwner + "]";
	}
}
