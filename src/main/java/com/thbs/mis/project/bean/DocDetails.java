package com.thbs.mis.project.bean;

import java.util.Date;
import java.util.List;

public class DocDetails {

	private String sowFileName;

	private String sowRefFileName;

	private short version;

	private Date sowRevisionDate;

	private List<SupportDocDetails> supportDocDetails;

	public String getSowFileName() {
		return sowFileName;
	}

	public void setSowFileName(String sowFileName) {
		this.sowFileName = sowFileName;
	}

	public String getSowRefFileName() {
		return sowRefFileName;
	}

	public void setSowRefFileName(String sowRefFileName) {
		this.sowRefFileName = sowRefFileName;
	}

	public short getVersion() {
		return version;
	}

	public void setVersion(short version) {
		this.version = version;
	}

	public Date getSowRevisionDate() {
		return sowRevisionDate;
	}

	public void setSowRevisionDate(Date sowRevisionDate) {
		this.sowRevisionDate = sowRevisionDate;
	}

	public List<SupportDocDetails> getSupportDocDetails() {
		return supportDocDetails;
	}

	public void setSupportDocDetails(List<SupportDocDetails> supportDocDetails) {
		this.supportDocDetails = supportDocDetails;
	}

	@Override
	public String toString() {
		return "DocDetails [sowFileName=" + sowFileName + ", sowRefFileName=" + sowRefFileName + ", version=" + version
				+ ", sowRevisionDate=" + sowRevisionDate + ", supportDocDetails=" + supportDocDetails + "]";
	}

}
