package com.thbs.mis.project.bean;

public class SearchComponentInputBean {

	private Integer componentId;

	private Integer componentStatusId;

	private Integer componentTypeId;

	private Integer viewedBy;
	
	private Integer woId;

	private Integer pageNumber;

	private Integer pageSize;

	
	public Integer getComponentId() {
		return componentId;
	}


	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}


	public Integer getComponentStatusId() {
		return componentStatusId;
	}


	public void setComponentStatusId(Integer componentStatusId) {
		this.componentStatusId = componentStatusId;
	}


	public Integer getComponentTypeId() {
		return componentTypeId;
	}


	public void setComponentTypeId(Integer componentTypeId) {
		this.componentTypeId = componentTypeId;
	}


	public Integer getViewedBy() {
		return viewedBy;
	}


	public void setViewedBy(Integer viewedBy) {
		this.viewedBy = viewedBy;
	}


	public Integer getWoId() {
		return woId;
	}


	public void setWoId(Integer woId) {
		this.woId = woId;
	}


	public Integer getPageNumber() {
		return pageNumber;
	}


	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}


	public Integer getPageSize() {
		return pageSize;
	}


	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}


	@Override
	public String toString() {
		return "SearchComponentInputBean [componentId=" + componentId + ", componentStatusId=" + componentStatusId
				+ ", componentTypeId=" + componentTypeId + ", viewedBy=" + viewedBy + ", pageNumber=" + pageNumber
				+ ", pageSize=" + pageSize + "]";
	}

}
