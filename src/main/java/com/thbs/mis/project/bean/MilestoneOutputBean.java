package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.Date;

public class MilestoneOutputBean {

	private int milestoneId;
	private String milestoneName;
	private BigDecimal milestoneValue;
	private String milestoneType;
	private int componentId;
	private String componentName;
	private Date mileStoneEndDate;
	private Integer mileStonecreatedBy;
	private String enterLineItem;
	private BigDecimal quantity;
	private BigDecimal unitPrice;
	private byte milestoneStatusId;
	private Integer mileStoneUpdatedBy;
	private Date mileStoneUpdatedOn;
	private String commentsForUpdate;
	public int getMilestoneId() {
		return milestoneId;
	}

	public void setMilestoneId(int milestoneId) {
		this.milestoneId = milestoneId;
	}

	public String getMilestoneName() {
		return milestoneName;
	}

	public void setMilestoneName(String milestoneName) {
		this.milestoneName = milestoneName;
	}

	public BigDecimal getMilestoneValue() {
		return milestoneValue;
	}

	public void setMilestoneValue(BigDecimal milestoneValue) {
		this.milestoneValue = milestoneValue;
	}

	public String getMilestoneType() {
		return milestoneType;
	}

	public void setMilestoneType(String milestoneType) {
		this.milestoneType = milestoneType;
	}

	public int getComponentId() {
		return componentId;
	}

	public void setComponentId(int componentId) {
		this.componentId = componentId;
	}

	

	public Date getMileStoneEndDate() {
		return mileStoneEndDate;
	}

	public void setMileStoneEndDate(Date mileStoneEndDate) {
		this.mileStoneEndDate = mileStoneEndDate;
	}

	public Integer getMileStonecreatedBy() {
		return mileStonecreatedBy;
	}

	public void setMileStonecreatedBy(Integer mileStonecreatedBy) {
		this.mileStonecreatedBy = mileStonecreatedBy;
	}

	public String getEnterLineItem() {
		return enterLineItem;
	}

	public void setEnterLineItem(String enterLineItem) {
		this.enterLineItem = enterLineItem;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public byte getMilestoneStatusId() {
		return milestoneStatusId;
	}

	public void setMilestoneStatusId(byte milestoneStatusId) {
		this.milestoneStatusId = milestoneStatusId;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public Integer getMileStoneUpdatedBy() {
		return mileStoneUpdatedBy;
	}

	public void setMileStoneUpdatedBy(Integer mileStoneUpdatedBy) {
		this.mileStoneUpdatedBy = mileStoneUpdatedBy;
	}

	public Date getMileStoneUpdatedOn() {
		return mileStoneUpdatedOn;
	}

	public void setMileStoneUpdatedOn(Date mileStoneUpdatedOn) {
		this.mileStoneUpdatedOn = mileStoneUpdatedOn;
	}

	public String getCommentsForUpdate() {
		return commentsForUpdate;
	}

	public void setCommentsForUpdate(String commentsForUpdate) {
		this.commentsForUpdate = commentsForUpdate;
	}

}
