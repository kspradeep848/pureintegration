package com.thbs.mis.project.bean;

import java.math.BigDecimal;

public class TriggerInputDetails {

	private BigDecimal triggerValue;
	private Integer milestoneId;
	private BigDecimal oB10Value;
	private BigDecimal additionalValue;
	private Integer purchaseOrderId;
	private String triggerComment;
    private Boolean flag;
    
	public BigDecimal getTriggerValue() {
		return triggerValue;
	}

	public void setTriggerValue(BigDecimal triggerValue) {
		this.triggerValue = triggerValue;
	}

	public BigDecimal getoB10Value() {
		return oB10Value;
	}

	public void setoB10Value(BigDecimal oB10Value) {
		this.oB10Value = oB10Value;
	}

	public BigDecimal getAdditionalValue() {
		return additionalValue;
	}

	public void setAdditionalValue(BigDecimal additionalValue) {
		this.additionalValue = additionalValue;
	}

	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}

	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}

	public Integer getMilestoneId() {
		return milestoneId;
	}

	public void setMilestoneId(Integer milestoneId) {
		this.milestoneId = milestoneId;
	}

	public String getTriggerComment() {
		return triggerComment;
	}

	public void setTriggerComment(String triggerComment) {
		this.triggerComment = triggerComment;
	}

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "TriggerInput [triggerValue=" + triggerValue + ", milestoneId=" + milestoneId + ", oB10Value="
				+ oB10Value + ", additionalValue=" + additionalValue + ", purchaseOrderId=" + purchaseOrderId + "]";
	}

}
