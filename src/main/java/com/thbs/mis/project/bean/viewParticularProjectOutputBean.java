package com.thbs.mis.project.bean;

import java.util.Date;

public class viewParticularProjectOutputBean {

	private Integer projectId;
	private String projectName;
	//private Short region;
	// private String completionOfProj
	private Integer NoOfWorkOrder;
	private Integer NoOfComponent;
	private String projectStatus;
	private String domainOrDeliveryManger;
	private String clientName;
	private Date projectFromDate;
	private Date projectToDate;
	private Integer daysUntilDue;
	private String location;
	private float completionOfProj;

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Integer getNoOfWorkOrder() {
		return NoOfWorkOrder;
	}

	public void setNoOfWorkOrder(Integer noOfWorkOrder) {
		NoOfWorkOrder = noOfWorkOrder;
	}

	public Integer getNoOfComponent() {
		return NoOfComponent;
	}

	public void setNoOfComponent(Integer noOfComponent) {
		NoOfComponent = noOfComponent;
	}

	public String getProjectStatus() {
		return projectStatus;
	}

	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}

	public String getDomainOrDeliveryManger() {
		return domainOrDeliveryManger;
	}

	public void setDomainOrDeliveryManger(String domainOrDeliveryManger) {
		this.domainOrDeliveryManger = domainOrDeliveryManger;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public Date getProjectFromDate() {
		return projectFromDate;
	}

	public void setProjectFromDate(Date projectFromDate) {
		this.projectFromDate = projectFromDate;
	}

	public Date getProjectToDate() {
		return projectToDate;
	}

	public void setProjectToDate(Date projectToDate) {
		this.projectToDate = projectToDate;
	}

	public Integer getDaysUntilDue() {
		return daysUntilDue;
	}

	public void setDaysUntilDue(Integer daysUntilDue) {
		this.daysUntilDue = daysUntilDue;
	}
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	

	public float getCompletionOfProj() {
		return completionOfProj;
	}

	public void setCompletionOfProj(float completionOfProj) {
		this.completionOfProj = completionOfProj;
	}

	@Override
	public String toString() {
		return "viewParticularProjectOutputBean [projectId=" + projectId
				+ ", projectName=" + projectName + ", NoOfWorkOrder="
				+ NoOfWorkOrder + ", NoOfComponent=" + NoOfComponent
				+ ", projectStatus=" + projectStatus
				+ ", domainOrDeliveryManger=" + domainOrDeliveryManger
				+ ", clientName=" + clientName + ", projectFromDate="
				+ projectFromDate + ", projectToDate=" + projectToDate
				+ ", daysUntilDue=" + daysUntilDue + ", location=" + location
				+ ", completionOfProj=" + completionOfProj + "]";
	}

	
}