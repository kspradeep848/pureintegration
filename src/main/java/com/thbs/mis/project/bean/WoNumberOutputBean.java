package com.thbs.mis.project.bean;

public class WoNumberOutputBean
{
	private Integer woId;
	
	private String woNumber;

	public WoNumberOutputBean(Integer woId, String woNumber) {
		super();
		this.woId = woId;
		this.woNumber = woNumber;
	}

	public Integer getWoId() {
		return woId;
	}

	public void setWoId(Integer woId) {
		this.woId = woId;
	}

	public String getWoNumber() {
		return woNumber;
	}

	public void setWoNumber(String woNumber) {
		this.woNumber = woNumber;
	}

	@Override
	public String toString() {
		return "WoNumberOutputBean [woId=" + woId + ", woNumber=" + woNumber
				+ "]";
	}
	

}
