package com.thbs.mis.project.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class DatClientEntityMappingBean {

	@Min(1)
	@NotNull(message = "pkClientEntityMappingId field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "pkClientEntityMappingId must be an Short.")
	private Integer pkClientEntityMappingId;
	
	@Min(1)
	@NotNull(message = "fkClientId field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "fkClientId must be an Short.")
	private Short fkClientId;
	
	@Min(1)
	@NotNull(message = "fkEntityId field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "fkEntityId must be an Short.")
	private Byte fkEntityId;


	public Integer getPkClientEntityMappingId() {
		return pkClientEntityMappingId;
	}


	public void setPkClientEntityMappingId(Integer pkClientEntityMappingId) {
		this.pkClientEntityMappingId = pkClientEntityMappingId;
	}


	public Short getFkClientId() {
		return fkClientId;
	}


	public void setFkClientId(Short fkClientId) {
		this.fkClientId = fkClientId;
	}


	public Byte getFkEntityId() {
		return fkEntityId;
	}


	public void setFkEntityId(Byte fkEntityId) {
		this.fkEntityId = fkEntityId;
	}


	@Override
	public String toString() {
		return "DatClientEntityMappingBean [pkClientEntityMappingId="
				+ pkClientEntityMappingId + ", fkClientId=" + fkClientId
				+ ", fkEntityId=" + fkEntityId + "]";
	}
	
	
	
}
