package com.thbs.mis.project.bean;

public class ProjMasWoStatusBean 
{
	private byte woId;
	private String woStatus;
	
	public byte getWoId() {
		return woId;
	}
	public void setWoId(byte woId) {
		this.woId = woId;
	}
	public String getWoStatus() {
		return woStatus;
	}
	public void setWoStatus(String woStatus) {
		this.woStatus = woStatus;
	}
	@Override
	public String toString() {
		return "ProjMasWoStatusBean [woId=" + woId + ", woStatus=" + woStatus
				+ "]";
	}	
	
}
