package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.List;

public class ClientBudgetBean{
 private List<ProjMasClientBudgetDetailsBean> projMasClientBudgetDetailsBeanList;
 private BigDecimal additionalAmt;

 private String commentForAdditionalAmt;
 private Integer woId;
 
public BigDecimal getAdditionalAmt() {
	return additionalAmt;
}
public void setAdditionalAmt(BigDecimal additionalAmt) {
	this.additionalAmt = additionalAmt;
}
public String getCommentForAdditionalAmt() {
	return commentForAdditionalAmt;
}
public void setCommentForAdditionalAmt(String commentForAdditionalAmt) {
	this.commentForAdditionalAmt = commentForAdditionalAmt;
}

public Integer getWoId() {
	return woId;
}
public void setWoId(Integer woId) {
	this.woId = woId;
}
public List<ProjMasClientBudgetDetailsBean> getProjMasClientBudgetDetailsBeanList() {
	return projMasClientBudgetDetailsBeanList;
}
public void setProjMasClientBudgetDetailsBeanList(
		List<ProjMasClientBudgetDetailsBean> projMasClientBudgetDetailsBeanList) {
	this.projMasClientBudgetDetailsBeanList = projMasClientBudgetDetailsBeanList;
}
@Override
public String toString() {
	return "manDaysDetails [projMasClientBudgetDetailsBeanList="
			+ projMasClientBudgetDetailsBeanList + ", additionalAmt="
			+ additionalAmt + ", commentForAdditionalAmt="
			+ commentForAdditionalAmt + ", woId=" + woId + "]";
}


}
