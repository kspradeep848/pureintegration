package com.thbs.mis.project.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.common.bean.MasBusinessEntityBean;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class ClientOutputBean {
	
	private Short clientId;
	
	private String clientName;
	
	private String clientStatus;
	
	List<MasClientAddressOutputBean> clientAddresses;
	
	private String contactNumberOne;

	private String contactNumberTwo;

	private String emailId;
	
	private List<ClientRepresentative> clientRepresentative;
	
	private Short regionId;
	
	private String panNumber;

	private String gstNumber;

	private String vatNumber;
	
	private String otherReferenceNumber;
	
	private Short creditPeriod;
	
	private String paymentContractDocuments;
	
	private String riskAssessment;
	
	private Integer noOfDays;
	
	private List<MasBusinessEntityBean> masBusinessEntityBean;
	
	private String poType;
	
	private String vendorCode;
	
	private String clientDocsPath;
	
	private Integer createdBy;
	
	private Integer updatedBy;
	
	private String upadtedComments;	
	
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date createdOn;

	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date updatedOn;

	public Short getClientId() {
		return clientId;
	}

	public void setClientId(Short clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientStatus() {
		return clientStatus;
	}

	public void setClientStatus(String clientStatus) {
		this.clientStatus = clientStatus;
	}

	public List<MasClientAddressOutputBean> getClientAddresses() {
		return clientAddresses;
	}

	public void setClientAddresses(List<MasClientAddressOutputBean> clientAddresses) {
		this.clientAddresses = clientAddresses;
	}

	public String getContactNumberOne() {
		return contactNumberOne;
	}

	public void setContactNumberOne(String contactNumberOne) {
		this.contactNumberOne = contactNumberOne;
	}

	public String getContactNumberTwo() {
		return contactNumberTwo;
	}

	public void setContactNumberTwo(String contactNumberTwo) {
		this.contactNumberTwo = contactNumberTwo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public List<ClientRepresentative> getClientRepresentative() {
		return clientRepresentative;
	}

	public void setClientRepresentative(
			List<ClientRepresentative> clientRepresentative) {
		this.clientRepresentative = clientRepresentative;
	}

	public Short getRegionId() {
		return regionId;
	}

	public void setRegionId(Short regionId) {
		this.regionId = regionId;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public String getOtherReferenceNumber() {
		return otherReferenceNumber;
	}

	public void setOtherReferenceNumber(String otherReferenceNumber) {
		this.otherReferenceNumber = otherReferenceNumber;
	}

	public Short getCreditPeriod() {
		return creditPeriod;
	}

	public void setCreditPeriod(Short creditPeriod) {
		this.creditPeriod = creditPeriod;
	}

	public String getPaymentContractDocuments() {
		return paymentContractDocuments;
	}

	public void setPaymentContractDocuments(String paymentContractDocuments) {
		this.paymentContractDocuments = paymentContractDocuments;
	}

	public String getRiskAssessment() {
		return riskAssessment;
	}

	public void setRiskAssessment(String riskAssessment) {
		this.riskAssessment = riskAssessment;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public List<MasBusinessEntityBean> getMasBusinessEntityBean() {
		return masBusinessEntityBean;
	}

	public void setMasBusinessEntityBean(
			List<MasBusinessEntityBean> masBusinessEntityBean) {
		this.masBusinessEntityBean = masBusinessEntityBean;
	}

	public String getPoType() {
		return poType;
	}

	public void setPoType(String poType) {
		this.poType = poType;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getClientDocsPath() {
		return clientDocsPath;
	}

	public void setClientDocsPath(String clientDocsPath) {
		this.clientDocsPath = clientDocsPath;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpadtedComments() {
		return upadtedComments;
	}

	public void setUpadtedComments(String upadtedComments) {
		this.upadtedComments = upadtedComments;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Override
	public String toString() {
		return "ClientOutputBean [clientId=" + clientId + ", clientName="
				+ clientName + ", clientStatus=" + clientStatus
				+ ", clientAddresses=" + clientAddresses
				+ ", contactNumberOne=" + contactNumberOne
				+ ", contactNumberTwo=" + contactNumberTwo + ", emailId="
				+ emailId + ", clientRepresentative=" + clientRepresentative
				+ ", regionId=" + regionId + ", panNumber=" + panNumber
				+ ", gstNumber=" + gstNumber + ", vatNumber=" + vatNumber
				+ ", otherReferenceNumber=" + otherReferenceNumber
				+ ", creditPeriod=" + creditPeriod
				+ ", paymentContractDocuments=" + paymentContractDocuments
				+ ", riskAssessment=" + riskAssessment + ", noOfDays="
				+ noOfDays + ", masBusinessEntityBean=" + masBusinessEntityBean
				+ ", poType=" + poType + ", vendorCode=" + vendorCode
				+ ", clientDocsPath=" + clientDocsPath + ", createdBy="
				+ createdBy + ", updatedBy=" + updatedBy + ", upadtedComments="
				+ upadtedComments + ", createdOn=" + createdOn + ", updatedOn="
				+ updatedOn + "]";
	}

}
