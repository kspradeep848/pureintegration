package com.thbs.mis.project.bean;

public class MasMilestoneStatusBean {
	
	private byte milestoneStatusId;

	private String milestoneStatusName;

	public byte getMilestoneStatusId() {
		return milestoneStatusId;
	}

	public void setMilestoneStatusId(byte milestoneStatusId) {
		this.milestoneStatusId = milestoneStatusId;
	}

	public String getMilestoneStatusName() {
		return milestoneStatusName;
	}

	public void setMilestoneStatusName(String milestoneStatusName) {
		this.milestoneStatusName = milestoneStatusName;
	}

	@Override
	public String toString() {
		return "MasMilestoneStatusBean [milestoneStatusId=" + milestoneStatusId
				+ ", milestoneStatusName=" + milestoneStatusName + "]";
	}
}
