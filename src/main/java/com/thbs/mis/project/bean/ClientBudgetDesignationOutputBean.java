package com.thbs.mis.project.bean;

public class ClientBudgetDesignationOutputBean {

	private Integer clientBudgetId;
	private short resourceDesignationId;
	private String resourceDesignationName;
	
	
	
	
	public ClientBudgetDesignationOutputBean(Integer clientBudgetId,
			short resourceDesignationId, String resourceDesignationName) {
		super();
		this.clientBudgetId = clientBudgetId;
		this.resourceDesignationId = resourceDesignationId;
		this.resourceDesignationName = resourceDesignationName;
	}
	public Integer getClientBudgetId() {
		return clientBudgetId;
	}
	public void setClientBudgetId(Integer clientBudgetId) {
		this.clientBudgetId = clientBudgetId;
	}
	public short getResourceDesignationId() {
		return resourceDesignationId;
	}
	public void setResourceDesignationId(short resourceDesignationId) {
		this.resourceDesignationId = resourceDesignationId;
	}
	public String getResourceDesignationName() {
		return resourceDesignationName;
	}
	public void setResourceDesignationName(String resourceDesignationName) {
		this.resourceDesignationName = resourceDesignationName;
	}
	
	@Override
	public String toString() {
		return "ClientBudgetDesignationOutputBean [clientBudgetId="
				+ clientBudgetId + ", resourceDesignationId="
				+ resourceDesignationId + ", resourceDesignationName="
				+ resourceDesignationName + "]";
	}
	
	
	
}
