package com.thbs.mis.project.bean;

public class SearchMilestoneBean {

	private Byte milestoneStatus;
	
	private String milestoneEndDate;
	
	private Integer componentId;
	
	private int viewedBy;
	
	private Integer pageNumber;
	
	private Integer pageSize; 

	public Byte getMilestoneStatus() {
		return milestoneStatus;
	}

	public void setMilestoneStatus(Byte milestoneStatus) {
		this.milestoneStatus = milestoneStatus;
	}

	public int getViewedBy() {
		return viewedBy;
	}

	public void setViewedBy(int viewedBy) {
		this.viewedBy = viewedBy;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getMilestoneEndDate() {
		return milestoneEndDate;
	}

	public void setMilestoneEndDate(String milestoneEndDate) {
		this.milestoneEndDate = milestoneEndDate;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	@Override
	public String toString() {
		return "SearchMilestoneBean [milestoneStatus=" + milestoneStatus + ", milestoneEndDate=" + milestoneEndDate
				+ ", componentId=" + componentId + ", viewedBy=" + viewedBy + ", pageNumber=" + pageNumber
				+ ", pageSize=" + pageSize + "]";
	}

	
}
