package com.thbs.mis.project.bean;

import java.util.Date;

public class SupportDocDetails {

	private String fileName;

	private String refFileName;

	private Date uploadedOn;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getRefFileName() {
		return refFileName;
	}

	public void setRefFileName(String refFileName) {
		this.refFileName = refFileName;
	}

	public Date getUploadedOn() {
		return uploadedOn;
	}

	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	@Override
	public String toString() {
		return "SupportDocDetails [fileName=" + fileName + ", refFileName=" + refFileName + ", uploadedOn=" + uploadedOn
				+ "]";
	}

}
