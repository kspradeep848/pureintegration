package com.thbs.mis.project.bean;

public class ViewProjectOutputBean {
	
	private Integer	projectId;
	private String projectName;
	private	Short regionId;
	private	String regionName;
	private	float completionOfProj;
	private	Integer noOfWorkOrder;
	private	Integer noOfComponent;
	private	Short clientId;
	private	String clientName;
	private Byte projectStatusId;
	private	String projectStatusName;
	private	Integer domainMangerId;
	private	String domainMangerName;
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
	public float getCompletionOfProj() {
		return completionOfProj;
	}
	public void setCompletionOfProj(float completionOfProj) {
		this.completionOfProj = completionOfProj;
	}
	public Integer getNoOfWorkOrder() {
		return noOfWorkOrder;
	}
	public void setNoOfWorkOrder(Integer noOfWorkOrder) {
		this.noOfWorkOrder = noOfWorkOrder;
	}
	public Integer getNoOfComponent() {
		return noOfComponent;
	}
	public void setNoOfComponent(Integer noOfComponent) {
		this.noOfComponent = noOfComponent;
	}
	
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public Short getRegionId() {
		return regionId;
	}
	public void setRegionId(Short regionId) {
		this.regionId = regionId;
	}
	public Short getClientId() {
		return clientId;
	}
	public void setClientId(Short clientId) {
		this.clientId = clientId;
	}
	public Integer getDomainMangerId() {
		return domainMangerId;
	}
	public void setDomainMangerId(Integer domainMangerId) {
		this.domainMangerId = domainMangerId;
	}
	public String getDomainMangerName() {
		return domainMangerName;
	}
	public void setDomainMangerName(String domainMangerName) {
		this.domainMangerName = domainMangerName;
	}
	public Byte getProjectStatusId() {
		return projectStatusId;
	}
	public void setProjectStatusId(Byte projectStatusId) {
		this.projectStatusId = projectStatusId;
	}
	public String getProjectStatusName() {
		return projectStatusName;
	}
	public void setProjectStatusName(String projectStatusName) {
		this.projectStatusName = projectStatusName;
	}
	@Override
	public String toString() {
		return "ViewProjectOutputBean [projectId=" + projectId
				+ ", projectName=" + projectName + ", regionId=" + regionId
				+ ", regionName=" + regionName + ", completionOfProj="
				+ completionOfProj + ", noOfWorkOrder=" + noOfWorkOrder
				+ ", noOfComponent=" + noOfComponent + ", clientId=" + clientId
				+ ", clientName=" + clientName + ", projectStatusId="
				+ projectStatusId + ", projectStatusName=" + projectStatusName
				+ ", domainMangerId=" + domainMangerId + ", domainMangerName="
				+ domainMangerName + "]";
	}
}