package com.thbs.mis.project.bean;

import java.io.Serializable;

public class SowFileDownloadInput implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer woId;
	private Integer downloadedBy;
	private Short version;
	private String fileName;

	public Integer getWoId() {
		return woId;
	}

	public void setWoId(Integer woId) {
		this.woId = woId;
	}

	public Integer getDownloadedBy() {
		return downloadedBy;
	}

	public void setDownloadedBy(Integer downloadedBy) {
		this.downloadedBy = downloadedBy;
	}

	public Short getVersion() {
		return version;
	}

	public void setVersion(Short version) {
		this.version = version;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
