package com.thbs.mis.project.bean;

public class ProjMasProjectClassificationBean {
	
	private byte pkClassificationId;
	private String classificationName;
	
	public byte getPkClassificationId() {
		return pkClassificationId;
	}
	public void setPkClassificationId(byte pkClassificationId) {
		this.pkClassificationId = pkClassificationId;
	}
	public String getClassificationName() {
		return classificationName;
	}
	public void setClassificationName(String classificationName) {
		this.classificationName = classificationName;
	}
	@Override
	public String toString() {
		return "ProjMasProjectClassificationBO [pkClassificationId="
				+ pkClassificationId + ", classificationName="
				+ classificationName + "]";
	}
}
