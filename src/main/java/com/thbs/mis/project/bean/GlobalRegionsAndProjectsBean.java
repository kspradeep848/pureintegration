package com.thbs.mis.project.bean;

public class GlobalRegionsAndProjectsBean {

	private Short countryId;

	private String countryName;

	private Long totalProject;
	
	public GlobalRegionsAndProjectsBean() {
		super();
	}

	public GlobalRegionsAndProjectsBean(Short countryId, String countryName,
			Long totalProject) {
		super();
		this.countryId = countryId;
		this.countryName = countryName;
		this.totalProject = totalProject;
	}

	public Long getTotalProject() {
		return totalProject;
	}

	public void setTotalProject(Long totalProject) {
		this.totalProject = totalProject;
	}

	public Short getCountryId() {
		return countryId;
	}

	public void setCountryId(Short countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	@Override
	public String toString() {
		return "GlobalRegionsAndProjectsBean [countryId=" + countryId
				+ ", countryName=" + countryName + ", totalProject="
				+ totalProject + "]";
	}
}
