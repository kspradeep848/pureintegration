package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.List;

import org.apache.poi.hwpf.usermodel.DateAndTime;

public class ClientBudgetUpdateBean {

	
	
	private short fkResourceDesignationId;
	
	private String resourceLocation;
	
	
	private Short fkParentLocationId;
	
	
	private Integer noOfResource;
	
	
	private Integer budgetedDays;
	
	
	private Integer fkWorkorderId;
	
	
	private String isBillingRateAvailable;
	
	private BigDecimal costToClient;
	
	private Integer createdBy;

	private DateAndTime createOn;
	
	private String weeklyOffFlag;

	
	private String holidayFlag;
	
	/*@NotNull
	private Byte fkRateTypeId;*/
	
	
	private BigDecimal rateAmount;
	
	private List<RateDetailsBean> normalrateDetails;
	
	private List<RateDetailsBean> weeklyOffRateDetails;
	
	private List<RateDetailsBean> holidayRateDetails;
	
	
	//for update 
	private Integer pkClientBudgetId;
	
	private Integer updatedBy;
	
	private String commentsForupdate;
	
	

	public short getFkResourceDesignationId() {
		return fkResourceDesignationId;
	}

	public void setFkResourceDesignationId(short fkResourceDesignationId) {
		this.fkResourceDesignationId = fkResourceDesignationId;
	}

	public String getResourceLocation() {
		return resourceLocation;
	}

	public void setResourceLocation(String resourceLocation) {
		this.resourceLocation = resourceLocation;
	}

	public Short getFkParentLocationId() {
		return fkParentLocationId;
	}

	public void setFkParentLocationId(Short fkParentLocationId) {
		this.fkParentLocationId = fkParentLocationId;
	}

	public Integer getNoOfResource() {
		return noOfResource;
	}

	public void setNoOfResource(Integer noOfResource) {
		this.noOfResource = noOfResource;
	}

	public Integer getBudgetedDays() {
		return budgetedDays;
	}

	public void setBudgetedDays(Integer budgetedDays) {
		this.budgetedDays = budgetedDays;
	}

	public Integer getFkWorkorderId() {
		return fkWorkorderId;
	}

	public void setFkWorkorderId(Integer fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}

	public String getIsBillingRateAvailable() {
		return isBillingRateAvailable;
	}

	public void setIsBillingRateAvailable(String isBillingRateAvailable) {
		this.isBillingRateAvailable = isBillingRateAvailable;
	}

	public BigDecimal getCostToClient() {
		return costToClient;
	}

	public void setCostToClient(BigDecimal costToClient) {
		this.costToClient = costToClient;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public DateAndTime getCreateOn() {
		return createOn;
	}

	public void setCreateOn(DateAndTime createOn) {
		this.createOn = createOn;
	}

	public String getWeeklyOffFlag() {
		return weeklyOffFlag;
	}

	public void setWeeklyOffFlag(String weeklyOffFlag) {
		this.weeklyOffFlag = weeklyOffFlag;
	}

	public String getHolidayFlag() {
		return holidayFlag;
	}

	public void setHolidayFlag(String holidayFlag) {
		this.holidayFlag = holidayFlag;
	}

	public BigDecimal getRateAmount() {
		return rateAmount;
	}

	public void setRateAmount(BigDecimal rateAmount) {
		this.rateAmount = rateAmount;
	}
	public List<RateDetailsBean> getNormalrateDetails() {
		return normalrateDetails;
	}

	public void setNormalrateDetails(List<RateDetailsBean> normalrateDetails) {
		this.normalrateDetails = normalrateDetails;
	}

	public List<RateDetailsBean> getWeeklyOffRateDetails() {
		return weeklyOffRateDetails;
	}

	public void setWeeklyOffRateDetails(List<RateDetailsBean> weeklyOffRateDetails) {
		this.weeklyOffRateDetails = weeklyOffRateDetails;
	}

	public List<RateDetailsBean> getHolidayRateDetails() {
		return holidayRateDetails;
	}

	public void setHolidayRateDetails(List<RateDetailsBean> holidayRateDetails) {
		this.holidayRateDetails = holidayRateDetails;
	}

	public Integer getPkClientBudgetId() {
		return pkClientBudgetId;
	}

	public void setPkClientBudgetId(Integer pkClientBudgetId) {
		this.pkClientBudgetId = pkClientBudgetId;
	}


	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCommentsForupdate() {
		return commentsForupdate;
	}

	public void setCommentsForupdate(String commentsForupdate) {
		this.commentsForupdate = commentsForupdate;
	}

	@Override
	public String toString() {
		return "ClientBudgetUpdateBean [fkResourceDesignationId="
				+ fkResourceDesignationId + ", resourceLocation="
				+ resourceLocation + ", fkParentLocationId="
				+ fkParentLocationId + ", noOfResource=" + noOfResource
				+ ", budgetedDays=" + budgetedDays + ", fkWorkorderId="
				+ fkWorkorderId + ", isBillingRateAvailable="
				+ isBillingRateAvailable + ", costToClient=" + costToClient
				+ ", createdBy=" + createdBy + ", createOn=" + createOn
				+ ", weeklyOffFlag=" + weeklyOffFlag + ", holidayFlag="
				+ holidayFlag + ", rateAmount=" + rateAmount
				+ ", normalrateDetails=" + normalrateDetails
				+ ", weeklyOffRateDetails=" + weeklyOffRateDetails
				+ ", holidayRateDetails=" + holidayRateDetails
				+ ", pkClientBudgetId=" + pkClientBudgetId + ", updatedBy="
				+ updatedBy + ", commentsForupdate=" + commentsForupdate + "]";
	}
}
