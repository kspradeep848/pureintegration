package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomDateSerializer;

public class MilestoneInputBean {

	private int milestoneId;

	@NotBlank(message = "milestoneName should not be blank.")
	@NotEmpty(message = "milestoneName should not be empty.")
	@NotNull(message = "milestoneName should not be null.")
	private String milestoneName;

	@NotNull(message = "milestoneValue field can not be null.")
	private BigDecimal milestoneValue;

	@NotBlank(message = "milestoneType should not be blank.")
	@NotEmpty(message = "milestoneType should not be empty.")
	@NotNull(message = "milestoneType should not be null.")
	private String milestoneType;

	@NumberFormat(style = Style.NUMBER, pattern = "componentId should be numeric value")
	@NotNull(message = "componentId can not be null")
	private Integer componentId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomDateSerializer.class)
	@NotNull(message = "mileStoneEndDate field can not be null.")
	private Date mileStoneEndDate;

	@NumberFormat(style = Style.NUMBER, pattern = "mileStonecreatedBy should be numeric value")
	@NotNull(message = "mileStonecreatedBy can not be null")
	private Integer mileStonecreatedBy;

	private int milestoneUpdatedBy;

	private String enterLineItem;

	private BigDecimal quantity;

	private BigDecimal unitPrice;

	@NotNull(message = "milestoneStatusId should not be null.")
	private Byte milestoneStatusId;

	private String commentsForUpdate;

	public int getMilestoneId() {
		return milestoneId;
	}

	public void setMilestoneId(int milestoneId) {
		this.milestoneId = milestoneId;
	}

	public String getMilestoneName() {
		return milestoneName;
	}

	public void setMilestoneName(String milestoneName) {
		this.milestoneName = milestoneName;
	}

	public BigDecimal getMilestoneValue() {
		return milestoneValue;
	}

	public void setMilestoneValue(BigDecimal milestoneValue) {
		this.milestoneValue = milestoneValue;
	}

	public String getMilestoneType() {
		return milestoneType;
	}

	public void setMilestoneType(String milestoneType) {
		this.milestoneType = milestoneType;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Date getMileStoneEndDate() {
		return mileStoneEndDate;
	}

	public void setMileStoneEndDate(Date mileStoneEndDate) {
		this.mileStoneEndDate = mileStoneEndDate;
	}

	public Integer getMileStonecreatedBy() {
		return mileStonecreatedBy;
	}

	public void setMileStonecreatedBy(Integer mileStonecreatedBy) {
		this.mileStonecreatedBy = mileStonecreatedBy;
	}

	public int getMilestoneUpdatedBy() {
		return milestoneUpdatedBy;
	}

	public void setMilestoneUpdatedBy(int milestoneUpdatedBy) {
		this.milestoneUpdatedBy = milestoneUpdatedBy;
	}

	public String getEnterLineItem() {
		return enterLineItem;
	}

	public void setEnterLineItem(String enterLineItem) {
		this.enterLineItem = enterLineItem;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Byte getMilestoneStatusId() {
		return milestoneStatusId;
	}

	public void setMilestoneStatusId(Byte milestoneStatusId) {
		this.milestoneStatusId = milestoneStatusId;
	}

	public String getCommentsForUpdate() {
		return commentsForUpdate;
	}

	public void setCommentsForUpdate(String commentsForUpdate) {
		this.commentsForUpdate = commentsForUpdate;
	}

}
