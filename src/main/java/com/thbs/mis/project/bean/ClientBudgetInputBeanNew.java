package com.thbs.mis.project.bean;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class ClientBudgetInputBeanNew {
	
	@NotNull
	private Byte fkResourceDesignationId;
	private String resourceLocation;
	@NotNull
	private Short fkParentLocationId;
	@NotNull
	private Integer noOfResource;
	@NotNull
	private Integer fkWorkorderId;
	@NotNull
	private Integer budgetedDays;
	@NotBlank
	private String isBillingRateAvailable;
	
	private BigDecimal costToClient;
	@NotNull
	private Integer createdBy;
	@NotEmpty
	private String weeklyOffFlag;
	@NotEmpty
	private String holidayFlag;
	private BigDecimal holidayRate;
	private BigDecimal rateAmount;
	private BigDecimal additionalAmt; //to be saved in Workorder table
	public Byte getFkResourceDesignationId() {
		return fkResourceDesignationId;
	}
	public void setFkResourceDesignationId(Byte fkResourceDesignationId) {
		this.fkResourceDesignationId = fkResourceDesignationId;
	}
	public String getResourceLocation() {
		return resourceLocation;
	}
	public void setResourceLocation(String resourceLocation) {
		this.resourceLocation = resourceLocation;
	}
	public Short getFkParentLocationId() {
		return fkParentLocationId;
	}
	public void setFkParentLocationId(Short fkParentLocationId) {
		this.fkParentLocationId = fkParentLocationId;
	}
	public Integer getNoOfResource() {
		return noOfResource;
	}
	public void setNoOfResource(Integer noOfResource) {
		this.noOfResource = noOfResource;
	}
	public Integer getFkWorkorderId() {
		return fkWorkorderId;
	}
	public void setFkWorkorderId(Integer fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}
	public Integer getBudgetedDays() {
		return budgetedDays;
	}
	public void setBudgetedDays(Integer budgetedDays) {
		this.budgetedDays = budgetedDays;
	}
	public String getIsBillingRateAvailable() {
		return isBillingRateAvailable;
	}
	public void setIsBillingRateAvailable(String isBillingRateAvailable) {
		this.isBillingRateAvailable = isBillingRateAvailable;
	}
	public BigDecimal getCostToClient() {
		return costToClient;
	}
	public void setCostToClient(BigDecimal costToClient) {
		this.costToClient = costToClient;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public String getWeeklyOffFlag() {
		return weeklyOffFlag;
	}
	public void setWeeklyOffFlag(String weeklyOffFlag) {
		this.weeklyOffFlag = weeklyOffFlag;
	}
	public String getHolidayFlag() {
		return holidayFlag;
	}
	public void setHolidayFlag(String holidayFlag) {
		this.holidayFlag = holidayFlag;
	}
	public BigDecimal getHolidayRate() {
		return holidayRate;
	}
	public void setHolidayRate(BigDecimal holidayRate) {
		this.holidayRate = holidayRate;
	}
	public BigDecimal getRateAmount() {
		return rateAmount;
	}
	public void setRateAmount(BigDecimal rateAmount) {
		this.rateAmount = rateAmount;
	}
	public BigDecimal getAdditionalAmt() {
		return additionalAmt;
	}
	public void setAdditionalAmt(BigDecimal additionalAmt) {
		this.additionalAmt = additionalAmt;
	}
	
	@Override
	public String toString() {
		return "ClientBudgetInputBeanNew [fkResourceDesignationId="
				+ fkResourceDesignationId + ", resourceLocation="
				+ resourceLocation + ", fkParentLocationId="
				+ fkParentLocationId + ", noOfResource=" + noOfResource
				+ ", fkWorkorderId=" + fkWorkorderId + ", budgetedDays="
				+ budgetedDays + ", isBillingRateAvailable="
				+ isBillingRateAvailable + ", costToClient=" + costToClient
				+ ", createdBy=" + createdBy + ", weeklyOffFlag="
				+ weeklyOffFlag + ", holidayFlag=" + holidayFlag
				+ ", holidayRate=" + holidayRate + ", rateAmount=" + rateAmount
				+ ", additionalAmt=" + additionalAmt + "]";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
