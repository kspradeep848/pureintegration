package com.thbs.mis.project.bean;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class ViewProjectInputBean {

	@Min(1)
	@NotNull(message = "loggedInUser field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "loggedInUser must be an Integer.")
	private Integer loggedInUser;
	private Integer projectId;
	private Short clientId;
	private Date fromDate;
	private Date toDate;
	private Integer projectStatusId;
	private String projectType;
	private Integer domainManagerId;
	private Short regionId;
	private Integer pageNumber;
	private Integer pageSize;

	public Integer getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Integer loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Short getClientId() {
		return clientId;
	}

	public void setClientId(Short clientId) {
		this.clientId = clientId;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Integer getProjectStatusId() {
		return projectStatusId;
	}

	public void setProjectStatusId(Integer projectStatusId) {
		this.projectStatusId = projectStatusId;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public Integer getDomainManagerId() {
		return domainManagerId;
	}

	public void setDomainManagerId(Integer domainManagerId) {
		this.domainManagerId = domainManagerId;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Short getRegionId() {
		return regionId;
	}

	public void setRegionId(Short regionId) {
		this.regionId = regionId;
	}

	@Override
	public String toString() {
		return "ViewProjectInputBean [loggedInUser=" + loggedInUser
				+ ", projectId=" + projectId + ", clientId=" + clientId
				+ ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", projectStatusId=" + projectStatusId + ", projectType="
				+ projectType + ", domainManagerId=" + domainManagerId
				+ ", regionId=" + regionId + ", pageNumber=" + pageNumber
				+ ", pageSize=" + pageSize + "]";
	}
}