package com.thbs.mis.project.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class ClientAddressBean {

	@Min(1)
	private Short clientAddressId;

	@NotBlank(message = "addressLineOne should not be blank.")
	@NotEmpty(message = "addressLineOne should not be empty.")
	@NotNull(message = "addressLineOne should not be null.")
	private String addressLineOne;

	@NotBlank(message = "addressLineTwo should not be blank.")
	@NotEmpty(message = "addressLineTwo should not be empty.")
	@NotNull(message = "addressLineTwo should not be null.")
	private String addressLineTwo;

	@NotBlank(message = "cityName should not be blank.")
	@NotEmpty(message = "cityName should not be empty.")
	@NotNull(message = "cityName should not be null.")
	private String cityName;

	@NotBlank(message = "stateName should not be blank.")
	@NotEmpty(message = "stateName should not be empty.")
	@NotNull(message = "stateName should not be null.")
	private String stateName;

	@NotBlank(message = "countryName should not be blank.")
	@NotEmpty(message = "countryName should not be empty.")
	@NotNull(message = "countryName field can not be null.")
	private String countryName;

	private Short countryId;

	@NotBlank(message = "isBillToAddress should not be blank.")
	@NotEmpty(message = "isBillToAddress should not be empty.")
	@NotNull(message = "isBillToAddress should not be null.")
	private String isBillToAddress;

	@NotBlank(message = "isShipToAddress should not be blank.")
	@NotEmpty(message = "isShipToAddress should not be empty.")
	@NotNull(message = "isShipToAddress should not be null.")
	private String isShipToAddress;

	private String zipCode;

	public Short getClientAddressId() {
		return clientAddressId;
	}

	public void setClientAddressId(Short clientAddressId) {
		this.clientAddressId = clientAddressId;
	}

	public String getAddressLineOne() {
		return addressLineOne;
	}

	public void setAddressLineOne(String addressLineOne) {
		this.addressLineOne = addressLineOne;
	}

	public String getAddressLineTwo() {
		return addressLineTwo;
	}

	public void setAddressLineTwo(String addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Short getCountryId() {
		return countryId;
	}

	public void setCountryId(Short countryId) {
		this.countryId = countryId;
	}

	public String getIsBillToAddress() {
		return isBillToAddress;
	}

	public void setIsBillToAddress(String isBillToAddress) {
		this.isBillToAddress = isBillToAddress;
	}

	public String getIsShipToAddress() {
		return isShipToAddress;
	}

	public void setIsShipToAddress(String isShipToAddress) {
		this.isShipToAddress = isShipToAddress;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Override
	public String toString() {
		return "ClientAddressBean [clientAddressId=" + clientAddressId
				+ ", addressLineOne=" + addressLineOne + ", addressLineTwo="
				+ addressLineTwo + ", cityName=" + cityName + ", stateName="
				+ stateName + ", countryName=" + countryName + ", countryId="
				+ countryId + ", isBillToAddress=" + isBillToAddress
				+ ", isShipToAddress=" + isShipToAddress + ", zipCode="
				+ zipCode + "]";
	}
}
