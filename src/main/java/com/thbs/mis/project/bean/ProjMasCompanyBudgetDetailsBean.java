package com.thbs.mis.project.bean;

import java.math.BigDecimal;

public class ProjMasCompanyBudgetDetailsBean {
	private int pkcompanyBudgetResourceId;
	private short  resourceRoleId;
	private String resourceRoleName;
	private String location;
	private short  resourceLevel;
	private String resourceLevelName;
	private Integer budgetedDays;
	private int noOfResource;
	private BigDecimal billingRate;
	private BigDecimal amount;
	
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public int getPkcompanyBudgetResourceId() {
		return pkcompanyBudgetResourceId;
	}
	public void setPkcompanyBudgetResourceId(int pkcompanyBudgetResourceId) {
		this.pkcompanyBudgetResourceId = pkcompanyBudgetResourceId;
	}
	
	
	public short getResourceRoleId() {
		return resourceRoleId;
	}
	public void setResourceRoleId(short resourceRoleId) {
		this.resourceRoleId = resourceRoleId;
	}
	public String getResourceRoleName() {
		return resourceRoleName;
	}
	public void setResourceRoleName(String resourceRoleName) {
		this.resourceRoleName = resourceRoleName;
	}
	public short getResourceLevel() {
		return resourceLevel;
	}
	public void setResourceLevel(short resourceLevel) {
		this.resourceLevel = resourceLevel;
	}
	public String getResourceLevelName() {
		return resourceLevelName;
	}
	public void setResourceLevelName(String resourceLevelName) {
		this.resourceLevelName = resourceLevelName;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Integer getBudgetedDays() {
		return budgetedDays;
	}
	public void setBudgetedDays(Integer budgetedDays) {
		this.budgetedDays = budgetedDays;
	}
	public int getNoOfResource() {
		return noOfResource;
	}
	public void setNoOfResource(int noOfResource) {
		this.noOfResource = noOfResource;
	}
	public BigDecimal getBillingRate() {
		return billingRate;
	}
	public void setBillingRate(BigDecimal billingRate) {
		this.billingRate = billingRate;
	}
	@Override
	public String toString() {
		return "ProjMasCompanyBudgetDetailsBean [pkcompanyBudgetResourceId="
				+ pkcompanyBudgetResourceId + ", resourceRoleId="
				+ resourceRoleId + ", resourceRoleName=" + resourceRoleName
				+ ", location=" + location + ", resourceLevel=" + resourceLevel
				+ ", resourceLevelName=" + resourceLevelName
				+ ", budgetedDays=" + budgetedDays + ", noOfResource="
				+ noOfResource + ", billingRate=" + billingRate + ", amount="
				+ amount + "]";
	}
	
}
