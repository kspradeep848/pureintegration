package com.thbs.mis.project.bean;

public class SowFileUploadInput {

	private Integer woId;
	private Integer uploadedBy;

	public Integer getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(Integer uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public SowFileUploadInput() {
	}

	public SowFileUploadInput(Integer woId, Integer uploadedBy) {
		super();
		this.woId = woId;
		this.uploadedBy = uploadedBy;
	}

	public Integer getWoId() {
		return woId;
	}

	public void setWoId(Integer woId) {
		this.woId = woId;
	}

}
