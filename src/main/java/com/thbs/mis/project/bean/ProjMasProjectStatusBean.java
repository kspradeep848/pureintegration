package com.thbs.mis.project.bean;

public class ProjMasProjectStatusBean {
	private byte pkProjectStatusId;
	private String projectStatusName;
	
	public byte getPkProjectStatusId() {
		return pkProjectStatusId;
	}

	public void setPkProjectStatusId(byte pkProjectStatusId) {
		this.pkProjectStatusId = pkProjectStatusId;
	}

	public String getProjectStatusName() {
		return projectStatusName;
	}

	public void setProjectStatusName(String projectStatusName) {
		this.projectStatusName = projectStatusName;
	}

	@Override
	public String toString() {
		return "ProjMasProjectStatusBO [pkProjectStatusId=" + pkProjectStatusId
				+ ", projectStatusName=" + projectStatusName + "]";
	}

}
