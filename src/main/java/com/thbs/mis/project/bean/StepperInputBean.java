package com.thbs.mis.project.bean;

public class StepperInputBean {
	
	private int type;
	private int id;
	
	public int getType() {
		return type;
	}
	
	public void setType(int type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "StepperInputBean [type=" + type + ", id=" + id + "]";
	}

}
