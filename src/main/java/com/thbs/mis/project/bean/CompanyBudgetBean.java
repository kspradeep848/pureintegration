package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.List;

public class CompanyBudgetBean {
	 private List<ProjMasCompanyBudgetDetailsBean> projMasCompanyBudgetDetailsBeanList;
	 private Integer woId;
	 private BigDecimal addtionalPurchaseAmt;
	 private BigDecimal additionalExpenseAmt;
	 private BigDecimal additionalTravelAmt;
	 
	 
	public BigDecimal getAddtionalPurchaseAmt() {
		return addtionalPurchaseAmt;
	}
	public void setAddtionalPurchaseAmt(BigDecimal addtionalPurchaseAmt) {
		this.addtionalPurchaseAmt = addtionalPurchaseAmt;
	}
	public BigDecimal getAdditionalExpenseAmt() {
		return additionalExpenseAmt;
	}
	public void setAdditionalExpenseAmt(BigDecimal additionalExpenseAmt) {
		this.additionalExpenseAmt = additionalExpenseAmt;
	}
	public BigDecimal getAdditionalTravelAmt() {
		return additionalTravelAmt;
	}
	public void setAdditionalTravelAmt(BigDecimal additionalTravelAmt) {
		this.additionalTravelAmt = additionalTravelAmt;
	}
	
	public Integer getWoId() {
		return woId;
	}
	public void setWoId(Integer woId) {
		this.woId = woId;
	}
	public List<ProjMasCompanyBudgetDetailsBean> getProjMasCompanyBudgetDetailsBeanList() {
		return projMasCompanyBudgetDetailsBeanList;
	}
	public void setProjMasCompanyBudgetDetailsBeanList(
			List<ProjMasCompanyBudgetDetailsBean> projMasCompanyBudgetDetailsBeanList) {
		this.projMasCompanyBudgetDetailsBeanList = projMasCompanyBudgetDetailsBeanList;
	}
	@Override
	public String toString() {
		return "manDaysDetails [projMasCompanyBudgetDetailsBeanList="
				+ projMasCompanyBudgetDetailsBeanList + ", woId=" + woId
				+ ", addtionalPurchaseAmt=" + addtionalPurchaseAmt
				+ ", additionalExpenseAmt=" + additionalExpenseAmt
				+ ", additionalTravelAmt=" + additionalTravelAmt + "]";
	}
	 
	
}
