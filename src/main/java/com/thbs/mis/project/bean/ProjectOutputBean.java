package com.thbs.mis.project.bean;
public class ProjectOutputBean {

	private Integer projectId;
	private String projectName;	
	private String deliveryManagerId;
	
	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getDeliveryManagerId() {
		return deliveryManagerId;
	}

	public void setDeliveryManagerId(String deliveryManagerId) {
		this.deliveryManagerId = deliveryManagerId;
	}

	@Override
	public String toString() {
		return "CreateProjectOutputBean [projectId=" + projectId
				+ ", projectName=" + projectName + ", deliveryManagerId="
				+ deliveryManagerId + "]";
	}
}