package com.thbs.mis.project.bean;

public class StandardRateInputBean {
	
	private Short employeeLevelId;
	private Short cureencyId;
	private Integer thbsLocationId;
	private Integer woId;
	
	public Short getEmployeeLevelId() {
		return employeeLevelId;
	}
	public void setEmployeeLevelId(Short employeeLevelId) {
		this.employeeLevelId = employeeLevelId;
	}
	public Short getCureencyId() {
		return cureencyId;
	}
	public void setCureencyId(Short cureencyId) {
		this.cureencyId = cureencyId;
	}
	public Integer getThbsLocationId() {
		return thbsLocationId;
	}
	public void setThbsLocationId(Integer thbsLocationId) {
		this.thbsLocationId = thbsLocationId;
	}
	public Integer getWoId() {
		return woId;
	}
	public void setWoId(Integer woId) {
		this.woId = woId;
	}
	@Override
	public String toString() {
		return "StandardRateInputBean [employeeLevelId=" + employeeLevelId
				+ ", cureencyId=" + cureencyId + ", thbsLocationId="
				+ thbsLocationId + ", woId=" + woId + "]";
	}
	
}
