package com.thbs.mis.project.bean;

public class ThbsBudgetLocationOutputBean {


private Integer pkThbsBudgetLocationId;
private String budgetLocationName;
private String onsiteOffshoreFlag;

public ThbsBudgetLocationOutputBean(Integer pkThbsBudgetLocationId,
		 String onsiteOffshoreFlag,String budgetLocationName) {
	super();
	this.pkThbsBudgetLocationId = pkThbsBudgetLocationId;
	this.budgetLocationName = budgetLocationName;
	this.onsiteOffshoreFlag = onsiteOffshoreFlag;
}

public Integer getPkThbsBudgetLocationId() {
	return pkThbsBudgetLocationId;
}
public void setPkThbsBudgetLocationId(Integer pkThbsBudgetLocationId) {
	this.pkThbsBudgetLocationId = pkThbsBudgetLocationId;
}
public String getBudgetLocationName() {
	return budgetLocationName;
}
public void setBudgetLocationName(String budgetLocationName) {
	this.budgetLocationName = budgetLocationName;
}
public String getOnsiteOffshoreFlag() {
	return onsiteOffshoreFlag;
}
public void setOnsiteOffshoreFlag(String onsiteOffshoreFlag) {
	this.onsiteOffshoreFlag = onsiteOffshoreFlag;
}


@Override
public String toString() {
	return "ThbsBudgetLocationOutputBean [pkThbsBudgetLocationId="
			+ pkThbsBudgetLocationId + ", budgetLocationName="
			+ budgetLocationName + ", onsiteOffshoreFlag=" + onsiteOffshoreFlag
			+ "]";
}


}
