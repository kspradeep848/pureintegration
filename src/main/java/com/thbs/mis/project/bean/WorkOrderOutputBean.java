package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomDateSerializer;

public class WorkOrderOutputBean {

	private Integer projectId;

	private Integer woId;

	private String projectName;

	private BigDecimal additionalAmount;

	private BigDecimal sowAmount;

	@JsonSerialize(using = CustomDateSerializer.class)
	private Date woEndDate;

	@JsonSerialize(using = CustomDateSerializer.class)
	private Date woStartDate;

	private String woDescription;

	private Short clientManagerId;

	private String clientManagerName;

	private String clientManagerEmailId;

	private Integer deliveryManagerId;

	private String deliveryManagerName;

	private short sowCurrencyId;

	private String sowCurrencyName;

	private Integer woCreatedBy;

	private byte woStatusId;

	private String woStatusName;

	private BigDecimal discountValue;

	private String commentForUpdate;

	private int noOfPo;

	private int noOfcomponents;

	private String woType;

	private BigDecimal woBal;

	private String woNumber;
	
	private String woInitial;

	private Short buId;
	
	public String getWoNumber() {
		return woNumber;
	}

	public void setWoNumber(String woNumber) {
		this.woNumber = woNumber;
	}

	public String getWoType() {
		return woType;
	}

	public void setWoType(String woType) {
		this.woType = woType;
	}

	public BigDecimal getWoBal() {
		return woBal;
	}

	public void setWoBal(BigDecimal woBal) {
		this.woBal = woBal;
	}

	public int getNoOfPo() {
		return noOfPo;
	}

	public void setNoOfPo(int noOfPo) {
		this.noOfPo = noOfPo;
	}

	public int getNoOfcomponents() {
		return noOfcomponents;
	}

	public void setNoOfcomponents(int noOfcomponents) {
		this.noOfcomponents = noOfcomponents;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getWoId() {
		return woId;
	}

	public void setWoId(Integer woId) {
		this.woId = woId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public BigDecimal getAdditionalAmount() {
		return additionalAmount;
	}

	public void setAdditionalAmount(BigDecimal additionalAmount) {
		this.additionalAmount = additionalAmount;
	}

	public BigDecimal getSowAmount() {
		return sowAmount;
	}

	public void setSowAmount(BigDecimal sowAmount) {
		this.sowAmount = sowAmount;
	}

	public Date getWoEndDate() {
		return woEndDate;
	}

	public void setWoEndDate(Date woEndDate) {
		this.woEndDate = woEndDate;
	}

	public Date getWoStartDate() {
		return woStartDate;
	}

	public void setWoStartDate(Date woStartDate) {
		this.woStartDate = woStartDate;
	}

	public String getWoDescription() {
		return woDescription;
	}

	public void setWoDescription(String woDescription) {
		this.woDescription = woDescription;
	}

	public Short getClientManagerId() {
		return clientManagerId;
	}

	public void setClientManagerId(Short clientManagerId) {
		this.clientManagerId = clientManagerId;
	}

	public String getClientManagerName() {
		return clientManagerName;
	}

	public void setClientManagerName(String clientManagerName) {
		this.clientManagerName = clientManagerName;
	}

	public String getClientManagerEmailId() {
		return clientManagerEmailId;
	}

	public void setClientManagerEmailId(String clientManagerEmailId) {
		this.clientManagerEmailId = clientManagerEmailId;
	}

	public Integer getDeliveryManagerId() {
		return deliveryManagerId;
	}

	public void setDeliveryManagerId(Integer deliveryManagerId) {
		this.deliveryManagerId = deliveryManagerId;
	}

	public String getDeliveryManagerName() {
		return deliveryManagerName;
	}

	public void setDeliveryManagerName(String deliveryManagerName) {
		this.deliveryManagerName = deliveryManagerName;
	}

	public short getSowCurrencyId() {
		return sowCurrencyId;
	}

	public void setSowCurrencyId(short sowCurrencyId) {
		this.sowCurrencyId = sowCurrencyId;
	}

	public String getSowCurrencyName() {
		return sowCurrencyName;
	}

	public void setSowCurrencyName(String sowCurrencyName) {
		this.sowCurrencyName = sowCurrencyName;
	}

	public Integer getWoCreatedBy() {
		return woCreatedBy;
	}

	public void setWoCreatedBy(Integer woCreatedBy) {
		this.woCreatedBy = woCreatedBy;
	}

	public byte getWoStatusId() {
		return woStatusId;
	}

	public void setWoStatusId(byte woStatusId) {
		this.woStatusId = woStatusId;
	}

	public String getWoStatusName() {
		return woStatusName;
	}

	public void setWoStatusName(String woStatusName) {
		this.woStatusName = woStatusName;
	}

	public BigDecimal getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(BigDecimal discountValue) {
		this.discountValue = discountValue;
	}

	public String getCommentForUpdate() {
		return commentForUpdate;
	}

	public void setCommentForUpdate(String commentForUpdate) {
		this.commentForUpdate = commentForUpdate;
	}

	public String getWoInitial() {
		return woInitial;
	}

	public void setWoInitial(String woInitial) {
		this.woInitial = woInitial;
	}
	
	public Short getBuId() {
		return buId;
	}

	public void setBuId(Short buId) {
		this.buId = buId;
	}

	@Override
	public String toString() {
		return "WorkOrderOutputBean [projectId=" + projectId + ", woId=" + woId + ", projectName=" + projectName
				+ ", additionalAmount=" + additionalAmount + ", sowAmount=" + sowAmount + ", woEndDate=" + woEndDate
				+ ", woStartDate=" + woStartDate + ", woDescription=" + woDescription + ", clientManagerId="
				+ clientManagerId + ", clientManagerName=" + clientManagerName + ", clientManagerEmailId="
				+ clientManagerEmailId + ", deliveryManagerId=" + deliveryManagerId + ", deliveryManagerName="
				+ deliveryManagerName + ", sowCurrencyId=" + sowCurrencyId + ", sowCurrencyName=" + sowCurrencyName
				+ ", woCreatedBy=" + woCreatedBy + ", woStatusId=" + woStatusId + ", woStatusName=" + woStatusName
				+ ", discountValue=" + discountValue + ", commentForUpdate=" + commentForUpdate + ", noOfPo=" + noOfPo
				+ ", noOfcomponents=" + noOfcomponents + ", woType=" + woType + ", woBal=" + woBal + ", woNumber="
				+ woNumber + "]";
	}

}
