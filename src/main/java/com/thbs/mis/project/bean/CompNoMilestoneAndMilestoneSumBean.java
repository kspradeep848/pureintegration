package com.thbs.mis.project.bean;

import java.math.BigDecimal;

public class CompNoMilestoneAndMilestoneSumBean 
{
private Long noOfMilesdtones;
private BigDecimal sunOfMilestones;


public Long getNoOfMilesdtones() {
	return noOfMilesdtones;
}
public void setNoOfMilesdtones(Long noOfMilesdtones) {
	this.noOfMilesdtones = noOfMilesdtones;
}
public BigDecimal getSunOfMilestones() {
	return sunOfMilestones;
}
public void setSunOfMilestones(BigDecimal sunOfMilestones) {
	this.sunOfMilestones = sunOfMilestones;
}

public CompNoMilestoneAndMilestoneSumBean(Long noOfMilesdtones,
		BigDecimal sunOfMilestones) {
	super();
	this.noOfMilesdtones = noOfMilesdtones;
	this.sunOfMilestones = sunOfMilestones;
}


}
