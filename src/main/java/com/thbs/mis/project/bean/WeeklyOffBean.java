package com.thbs.mis.project.bean;

import java.math.BigDecimal;

public class WeeklyOffBean {

	private Integer clientBudgetRateDetailsId;
	private Byte rateTypeId;
	private Byte invoiceRateTypeId;
	private Integer weeklyOffId;
	private BigDecimal value;

	public Integer getClientBudgetRateDetailsId() {
		return clientBudgetRateDetailsId;
	}

	public void setClientBudgetRateDetailsId(Integer clientBudgetRateDetailsId) {
		this.clientBudgetRateDetailsId = clientBudgetRateDetailsId;
	}

	public Byte getRateTypeId() {
		return rateTypeId;
	}

	public void setRateTypeId(Byte rateTypeId) {
		this.rateTypeId = rateTypeId;
	}

	public Byte getInvoiceRateTypeId() {
		return invoiceRateTypeId;
	}

	public void setInvoiceRateTypeId(Byte invoiceRateTypeId) {
		this.invoiceRateTypeId = invoiceRateTypeId;
	}

	public Integer getWeeklyOffId() {
		return weeklyOffId;
	}

	public void setWeeklyOffId(Integer weeklyOffId) {
		this.weeklyOffId = weeklyOffId;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "WeeklyOffBean [clientBudgetRateDetailsId="
				+ clientBudgetRateDetailsId + ", rateTypeId=" + rateTypeId
				+ ", invoiceRateTypeId=" + invoiceRateTypeId + ", weeklyOffId="
				+ weeklyOffId + ", value=" + value + "]";
	}

}