package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.List;

public class ProjMasCompanyBudgetSummaryBean {
	
	List<CompanyBudgetSummaryBean> companyBudgetSummaryBean;
	private BigDecimal costToCompanyFinal;
	private BigDecimal costToClientLevel;
	private BigDecimal contribution;
	private Integer percentageContribution;
	private String budgetStatus;
	
	public List<CompanyBudgetSummaryBean> getCompanyBudgetSummaryBean() {
		return companyBudgetSummaryBean;
	}
	public void setCompanyBudgetBean(List<CompanyBudgetSummaryBean> companyBudgetSummaryBean) {
		this.companyBudgetSummaryBean = companyBudgetSummaryBean;
	}
	public BigDecimal getCostToCompanyFinal() {
		return costToCompanyFinal;
	}
	public void setCostToCompanyFinal(BigDecimal costToCompanyFinal) {
		this.costToCompanyFinal = costToCompanyFinal;
	}
	public BigDecimal getCostToClientLevel() {
		return costToClientLevel;
	}
	public void setCostToClientLevel(BigDecimal costToClientLevel) {
		this.costToClientLevel = costToClientLevel;
	}
	public BigDecimal getContribution() {
		return contribution;
	}
	public void setContribution(BigDecimal contribution) {
		this.contribution = contribution;
	}
	public Integer getPercentageContribution() {
		return percentageContribution;
	}
	public void setPercentageContribution(Integer percentageContribution) {
		this.percentageContribution = percentageContribution;
	}
	public String getBudgetStatus() {
		return budgetStatus;
	}
	public void setBudgetStatus(String budgetStatus) {
		this.budgetStatus = budgetStatus;
	}
	
	@Override
	public String toString() {
		return "ProjMasCompanyBudgetSummaryBean [companyBudgetSummaryBean="
				+ companyBudgetSummaryBean + ", costToCompanyFinal="
				+ costToCompanyFinal + ", costToClientLevel="
				+ costToClientLevel + ", contribution=" + contribution
				+ ", percentageContribution=" + percentageContribution
				+ ", budgetStatus=" + budgetStatus + "]";
	}
}
