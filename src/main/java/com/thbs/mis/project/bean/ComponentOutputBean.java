package com.thbs.mis.project.bean;

public class ComponentOutputBean {
	
	private int compId;
	
	private String compName;
	
	private Integer woId;

	public Integer getWoId() {
		return woId;
	}

	public void setWoId(Integer woId) {
		this.woId = woId;
	}

	public int getCompId() {
		return compId;
	}

	public void setCompId(int compId) {
		this.compId = compId;
	}

	public String getCompName() {
		return compName;
	}

	public void setCompName(String compName) {
		this.compName = compName;
	}

	@Override
	public String toString() {
		return "ComponentOutputBean [compId=" + compId + ", compName="
				+ compName + ", woId=" + woId + "]";
	}
		
}
