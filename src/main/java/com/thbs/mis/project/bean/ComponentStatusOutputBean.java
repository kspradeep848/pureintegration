package com.thbs.mis.project.bean;

public class ComponentStatusOutputBean {
	
	private int pkComponentId;
	
	private String componentName;
	
	public ComponentStatusOutputBean(int pkComponentId, String componentName){
		this.pkComponentId = pkComponentId;
		this.componentName = componentName;
	}

	public int getPkComponentId() {
		return pkComponentId;
	}

	public void setPkComponentId(int pkComponentId) {
		this.pkComponentId = pkComponentId;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	@Override
	public String toString() {
		return "ComponentStatusOutputBean [pkComponentId=" + pkComponentId
				+ ", componentName=" + componentName + "]";
	}
}
