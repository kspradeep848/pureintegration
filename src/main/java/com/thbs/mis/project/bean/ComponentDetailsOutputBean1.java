package com.thbs.mis.project.bean;

import java.math.BigDecimal;

public class ComponentDetailsOutputBean1 {

	private int compId;

	private String compDescription;

	private String compStartDate;

	private String compEndDate;

	private BigDecimal compAmount;

	private String compName;

	private int compStatusId;

	private String compStatusName;

	private int compTypeId;

	private String compTypeName;

	private int woId;

	private int compOwner;

	private String compOwnerName;

	private Integer noOfMilestone;

	private Integer noOfResource;

	private BigDecimal compBalance;

	private Integer noOfMilestoneTriggered;
	
	private String commentsForUpdate;
	
	private Integer createdBy;
	

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public int getCompId() {
		return compId;
	}

	public void setCompId(int compId) {
		this.compId = compId;
	}

	public String getCompDescription() {
		return compDescription;
	}

	public void setCompDescription(String compDescription) {
		this.compDescription = compDescription;
	}

	public String getCompStartDate() {
		return compStartDate;
	}

	public void setCompStartDate(String compStartDate) {
		this.compStartDate = compStartDate;
	}

	public String getCompEndDate() {
		return compEndDate;
	}

	public void setCompEndDate(String compEndDate) {
		this.compEndDate = compEndDate;
	}

	public BigDecimal getCompAmount() {
		return compAmount;
	}

	public void setCompAmount(BigDecimal compAmount) {
		this.compAmount = compAmount;
	}

	public String getCompName() {
		return compName;
	}

	public void setCompName(String compName) {
		this.compName = compName;
	}

	public int getCompStatusId() {
		return compStatusId;
	}

	public void setCompStatusId(int compStatusId) {
		this.compStatusId = compStatusId;
	}

	public String getCompStatusName() {
		return compStatusName;
	}

	public void setCompStatusName(String compStatusName) {
		this.compStatusName = compStatusName;
	}

	public int getCompTypeId() {
		return compTypeId;
	}

	public void setCompTypeId(int compTypeId) {
		this.compTypeId = compTypeId;
	}

	public String getCompTypeName() {
		return compTypeName;
	}

	public void setCompTypeName(String compTypeName) {
		this.compTypeName = compTypeName;
	}

	public int getWoId() {
		return woId;
	}

	public void setWoId(int woId) {
		this.woId = woId;
	}

	public int getCompOwner() {
		return compOwner;
	}

	public void setCompOwner(int compOwner) {
		this.compOwner = compOwner;
	}

	public String getCompOwnerName() {
		return compOwnerName;
	}

	public void setCompOwnerName(String compOwnerName) {
		this.compOwnerName = compOwnerName;
	}

	public Integer getNoOfMilestone() {
		return noOfMilestone;
	}

	public void setNoOfMilestone(Integer noOfMilestone) {
		this.noOfMilestone = noOfMilestone;
	}

	public Integer getNoOfResource() {
		return noOfResource;
	}

	public void setNoOfResource(Integer noOfResource) {
		this.noOfResource = noOfResource;
	}

	public BigDecimal getCompBalance() {
		return compBalance;
	}

	public void setCompBalance(BigDecimal compBalance) {
		this.compBalance = compBalance;
	}

	public Integer getNoOfMilestoneTriggered() {
		return noOfMilestoneTriggered;
	}

	public void setNoOfMilestoneTriggered(Integer noOfMilestoneTriggered) {
		this.noOfMilestoneTriggered = noOfMilestoneTriggered;
	}

	public String getCommentsForUpdate() {
		return commentsForUpdate;
	}

	public void setCommentsForUpdate(String commentsForUpdate) {
		this.commentsForUpdate = commentsForUpdate;
	}

	@Override
	public String toString() {
		return "ComponentDetailsOutputBean1 [compId=" + compId
				+ ", compDescription=" + compDescription + ", compStartDate="
				+ compStartDate + ", compEndDate=" + compEndDate
				+ ", compAmount=" + compAmount + ", compName=" + compName
				+ ", compStatusId=" + compStatusId + ", compStatusName="
				+ compStatusName + ", compTypeId=" + compTypeId
				+ ", compTypeName=" + compTypeName + ", woId=" + woId
				+ ", compOwner=" + compOwner + ", compOwnerName="
				+ compOwnerName + ", noOfMilestone=" + noOfMilestone
				+ ", noOfResource=" + noOfResource + ", compBalance="
				+ compBalance + ", noOfMilestoneTriggered="
				+ noOfMilestoneTriggered + ", commentsForUpdate="
				+ commentsForUpdate + "]";
	}

}
