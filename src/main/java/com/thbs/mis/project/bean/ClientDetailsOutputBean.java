package com.thbs.mis.project.bean;

public class ClientDetailsOutputBean {

	private Short clientId;

	private String clientName;

	public ClientDetailsOutputBean(Short clientId, String clientName) {
		super();
		this.clientId = clientId;
		this.clientName = clientName;
	}

	public Short getClientId() {
		return clientId;
	}

	public void setClientId(Short clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	@Override
	public String toString() {
		return "ClientDetailsOutputBean [clientId=" + clientId
				+ ", clientName=" + clientName + "]";
	}
	
	@Override
	public boolean equals(Object o) {
		 if (o == this) {
	            return true;
	        }
		 if (!(o instanceof ClientDetailsOutputBean)) {
	            return false;
	        }
		 ClientDetailsOutputBean client =(ClientDetailsOutputBean)o;
		
		 return (client.clientId.equals(this.clientId) && client.clientName.equals(this.clientName))?true:false;
	}
	
	 @Override
	    public int hashCode() {
	        final int prime = 13;
	        int result = 1;
	        result = prime * result
	                + (this.clientName.hashCode()+this.clientId.hashCode());
	        return result;
	    }
}
