package com.thbs.mis.project.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ClientSearchInputBean {

	@Min(1)
	private Short clientId;
	
	private String clientStatus;

	@Min(1)
	@NotNull(message = "regionId shouldn't be null")
	private Short regionId;

	@Min(0)
	@NotNull(message = "pageNumber shouldn't be null")
	private Integer pageNumber;
	
	@Min(1)
	@NotNull(message = "pageSize shouldn't be null")
	private Integer pageSize;

	@Min(1)
	@NotNull(message = "loggedInUser shouldn't be null")
	private Integer loggedInUser;

	public Short getClientId() {
		return clientId;
	}

	public void setClientId(Short clientId) {
		this.clientId = clientId;
	}

	public String getClientStatus() {
		return clientStatus;
	}

	public void setClientStatus(String clientStatus) {
		this.clientStatus = clientStatus;
	}

	public Short getRegionId() {
		return regionId;
	}

	public void setRegionId(Short regionId) {
		this.regionId = regionId;
	}
	
	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Integer loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	@Override
	public String toString() {
		return "ClientSearchInputBean [clientId=" + clientId
				+ ", clientStatus=" + clientStatus + ", regionId=" + regionId
				+ ", pageNumber=" + pageNumber + ", pageSize=" + pageSize
				+ ", loggedInUser=" + loggedInUser + "]";
	}
	
}
