package com.thbs.mis.project.bean;

public class ProjMasCompTypeBean {
	
	private byte componentTypeId;

	private String componentTypeName;
	
	
	public byte getComponentTypeId() {
		return componentTypeId;
	}

	public void setComponentTypeId(byte componentTypeId) {
		this.componentTypeId = componentTypeId;
	}

	public String getComponentTypeName() {
		return componentTypeName;
	}

	public void setComponentTypeName(String componentTypeName) {
		this.componentTypeName = componentTypeName;
	}

	@Override
	public String toString() {
		return "ProjMasCompTypeBO [pkComponentTypeId=" + componentTypeId
				+ ", componentTypeName=" + componentTypeName + "]";
	}

}
