package com.thbs.mis.project.bean;

import java.math.BigDecimal;

public class ClientBudgetSummaryBean {
	
	private String role;
	private String location;
	private Integer budgetDays;
	private BigDecimal billingRate;
	private BigDecimal costToClient;
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Integer getBudgetDays() {
		return budgetDays;
	}
	public void setBudgetDays(Integer budgetDays) {
		this.budgetDays = budgetDays;
	}
	public BigDecimal getBillingRate() {
		return billingRate;
	}
	public void setBillingRate(BigDecimal billingRate) {
		this.billingRate = billingRate;
	}
	public BigDecimal getCostToClient() {
		return costToClient;
	}
	public void setCostToClient(BigDecimal costToClient) {
		this.costToClient = costToClient;
	}
	
	@Override
	public String toString() {
		return "ClientBudgetSummaryBean [role=" + role + ", location=" + location
				+ ", budgetDays=" + budgetDays + ", billingRate=" + billingRate
				+ ", costToClient="	+ costToClient + "]";
	}
}
