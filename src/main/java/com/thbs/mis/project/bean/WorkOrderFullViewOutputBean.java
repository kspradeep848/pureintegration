package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.Date;

public class WorkOrderFullViewOutputBean {
	private Integer woId;
	private int noOfPo;
	private int noOfComponent;
	private String woDescription;
	private String woStatus;
	private String woNumber;
	private Date fromDate;
	private Date toDate;
	private String woType;
	private Integer projectOwnerId;
	private String projectOwnerName;
	private BigDecimal sowAmount;
	private short sowCurrencyId;
	private String sowCurrencyName;

	public Integer getWoId() {
		return woId;
	}

	public void setWoId(Integer woId) {
		this.woId = woId;
	}

	public int getNoOfPo() {
		return noOfPo;
	}

	public void setNoOfPo(int noOfPo) {
		this.noOfPo = noOfPo;
	}

	public int getNoOfComponent() {
		return noOfComponent;
	}

	public void setNoOfComponent(int noOfComponent) {
		this.noOfComponent = noOfComponent;
	}

	public String getWoDescription() {
		return woDescription;
	}

	public void setWoDescription(String woDescription) {
		this.woDescription = woDescription;
	}

	public String getWoStatus() {
		return woStatus;
	}

	public void setWoStatus(String woStatus) {
		this.woStatus = woStatus;
	}

	public String getWoNumber() {
		return woNumber;
	}

	public void setWoNumber(String woNumber) {
		this.woNumber = woNumber;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getWoType() {
		return woType;
	}

	public void setWoType(String woType) {
		this.woType = woType;
	}

	public Integer getProjectOwnerId() {
		return projectOwnerId;
	}

	public void setProjectOwnerId(Integer projectOwnerId) {
		this.projectOwnerId = projectOwnerId;
	}

	public String getProjectOwnerName() {
		return projectOwnerName;
	}

	public void setProjectOwnerName(String projectOwnerName) {
		this.projectOwnerName = projectOwnerName;
	}

	public BigDecimal getSowAmount() {
		return sowAmount;
	}

	public void setSowAmount(BigDecimal sowAmount) {
		this.sowAmount = sowAmount;
	}

	public short getSowCurrencyId() {
		return sowCurrencyId;
	}

	public void setSowCurrencyId(short sowCurrencyId) {
		this.sowCurrencyId = sowCurrencyId;
	}

	public String getSowCurrencyName() {
		return sowCurrencyName;
	}

	public void setSowCurrencyName(String sowCurrencyName) {
		this.sowCurrencyName = sowCurrencyName;
	}

	@Override
	public String toString() {
		return "WorkOrderFullViewOutputBean [woId=" + woId + ", noOfPo=" + noOfPo + ", noOfComponent=" + noOfComponent
				+ ", woDescription=" + woDescription + ", woStatus=" + woStatus + ", woNumber=" + woNumber
				+ ", fromDate=" + fromDate + ", toDate=" + toDate + ", woType=" + woType + ", projectOwnerId="
				+ projectOwnerId + ", projectOwnerName=" + projectOwnerName + ", sowAmount=" + sowAmount
				+ ", sowCurrencyId=" + sowCurrencyId + ", sowCurrencyName=" + sowCurrencyName + "]";
	}

}
