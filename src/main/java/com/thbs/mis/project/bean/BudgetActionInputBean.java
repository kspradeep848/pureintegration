package com.thbs.mis.project.bean;

import java.util.List;

public class BudgetActionInputBean {
	private Integer woId;
	private Integer statusId;
	private List<Integer> clientBudgetDetails;
	private List<Integer> thbsBudgetDetails;
	private String reasonForReject;
	private String reasonForNotRequired;
	private Integer approvedBy;
	public Integer getWoId() {
		return woId;
	}
	public void setWoId(Integer woId) {
		this.woId = woId;
	}
	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public List<Integer> getClientBudgetDetails() {
		return clientBudgetDetails;
	}
	public void setClientBudgetDetails(List<Integer> clientBudgetDetails) {
		this.clientBudgetDetails = clientBudgetDetails;
	}
	public List<Integer> getThbsBudgetDetails() {
		return thbsBudgetDetails;
	}
	public void setThbsBudgetDetails(List<Integer> thbsBudgetDetails) {
		this.thbsBudgetDetails = thbsBudgetDetails;
	}
	public String getReasonForReject() {
		return reasonForReject;
	}
	public void setReasonForReject(String reasonForReject) {
		this.reasonForReject = reasonForReject;
	}
	public String getReasonForNotRequired() {
		return reasonForNotRequired;
	}
	public void setReasonForNotRequired(String reasonForNotRequired) {
		this.reasonForNotRequired = reasonForNotRequired;
	}
	public Integer getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(Integer approvedBy) {
		this.approvedBy = approvedBy;
	}
	@Override
	public String toString() {
		return "BudgetActionInputBean [woId=" + woId + ", statusId=" + statusId
				+ ", clientBudgetDetails=" + clientBudgetDetails
				+ ", thbsBudgetDetails=" + thbsBudgetDetails
				+ ", reasonForReject=" + reasonForReject
				+ ", reasonForNotRequired=" + reasonForNotRequired
				+ ", approvedBy=" + approvedBy + "]";
	}
	
	}
