package com.thbs.mis.project.bean;

public class ClientSearchOutputBean {
	
	private String clientName;

	private Short clientId;
	
	private String clientStatus;

	private Short countryId;
	
	private Short regionId;
	
	private String countryName;
	
	private Short creditPeriod;
	
	private String riskAssessment;

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	
	public Short getClientId() {
		return clientId;
	}

	public void setClientId(Short clientId) {
		this.clientId = clientId;
	}

	public String getClientStatus() {
		return clientStatus;
	}

	public void setClientStatus(String clientStatus) {
		this.clientStatus = clientStatus;
	}

	public Short getCountryId() {
		return countryId;
	}

	public void setCountryId(Short countryId) {
		this.countryId = countryId;
	}

	public Short getRegionId() {
		return regionId;
	}

	public void setRegionId(Short regionId) {
		this.regionId = regionId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Short getCreditPeriod() {
		return creditPeriod;
	}

	public void setCreditPeriod(Short creditPeriod) {
		this.creditPeriod = creditPeriod;
	}

	public String getRiskAssessment() {
		return riskAssessment;
	}

	public void setRiskAssessment(String riskAssessment) {
		this.riskAssessment = riskAssessment;
	}

	@Override
	public String toString() {
		return "ClientSearchOutputBean [clientName=" + clientName
				+ ", clientId=" + clientId + ", clientStatus=" + clientStatus
				+ ", countryId=" + countryId + ", regionId=" + regionId
				+ ", countryName=" + countryName + ", creditPeriod="
				+ creditPeriod + ", riskAssessment=" + riskAssessment + "]";
	}
}
