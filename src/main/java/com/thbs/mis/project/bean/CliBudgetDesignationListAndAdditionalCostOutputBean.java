package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.List;

public class CliBudgetDesignationListAndAdditionalCostOutputBean
{
List<ClientBudgetDesignationOutputBean> ClientBudgetDesignationOutputBeanList ;

private BigDecimal purchaseAmt;

private BigDecimal expenseAmt;

private BigDecimal travelAmt;


public List<ClientBudgetDesignationOutputBean> getClientBudgetDesignationOutputBeanList() {
	return ClientBudgetDesignationOutputBeanList;
}

public void setClientBudgetDesignationOutputBeanList(
		List<ClientBudgetDesignationOutputBean> clientBudgetDesignationOutputBeanList) {
	ClientBudgetDesignationOutputBeanList = clientBudgetDesignationOutputBeanList;
}

public BigDecimal getPurchaseAmt() {
	return purchaseAmt;
}

public void setPurchaseAmt(BigDecimal purchaseAmt) {
	this.purchaseAmt = purchaseAmt;
}

public BigDecimal getExpenseAmt() {
	return expenseAmt;
}

public void setExpenseAmt(BigDecimal expenseAmt) {
	this.expenseAmt = expenseAmt;
}

public BigDecimal getTravelAmt() {
	return travelAmt;
}

public void setTravelAmt(BigDecimal travelAmt) {
	this.travelAmt = travelAmt;
}




}
