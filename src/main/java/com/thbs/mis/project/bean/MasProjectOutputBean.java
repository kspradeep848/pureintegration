package com.thbs.mis.project.bean;

import java.util.Date;
import java.util.List;

import com.thbs.mis.common.bean.ProjectCoordinatorBean;

public class MasProjectOutputBean {

	private Integer projectId;
	private String projectName;
	private Date projectStartDate;
	private Date projectEndDate;
	private String projectDescription;
	private List<MasClientAddressOutputBean> clientAddresses;
	private short verticalId;
	private String verticalName;
	private short regionId;
	private String regionName;
	private Integer deliveryManagerId;
	private String deliveryManagerName;
	private short buUnitId;
	private String buUnitName;
	private Short domainId;
	private String domainName;
	private Integer domainMgrId;
	private String domainMgrName;
	private byte projectStatusId;
	private String projectStatusName;
	private String projectType;
	private short accountId;
	private String accountName;
	private Integer accountMgrId;
	private String accountMgrName;
	private String projectBillableFlag;
	private Integer createdBy;
	private Date createdOn;
	private Integer updatedBy;
	private String commentsForUpdation;
	private List<ProjectCoordinatorBean> projectCoordinatorDetails;
	private List<ProjectLevelUserAccessBean> projectLevelUserAccessDetails;
	private byte projectClassificationId;
	private String projectClassificationName;
	private String isHavingAggregatePo;
	private short clientId;
	private float completionOfProj;
	
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Date getProjectStartDate() {
		return projectStartDate;
	}
	public void setProjectStartDate(Date projectStartDate) {
		this.projectStartDate = projectStartDate;
	}
	public Date getProjectEndDate() {
		return projectEndDate;
	}
	public void setProjectEndDate(Date projectEndDate) {
		this.projectEndDate = projectEndDate;
	}
	public String getProjectDescription() {
		return projectDescription;
	}
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}
	public List<MasClientAddressOutputBean> getClientAddresses() {
		return clientAddresses;
	}
	public void setClientAddresses(List<MasClientAddressOutputBean> clientAddresses) {
		this.clientAddresses = clientAddresses;
	}
	public short getVerticalId() {
		return verticalId;
	}
	public void setVerticalId(short verticalId) {
		this.verticalId = verticalId;
	}
	public String getVerticalName() {
		return verticalName;
	}
	public void setVerticalName(String verticalName) {
		this.verticalName = verticalName;
	}
	public short getRegionId() {
		return regionId;
	}
	public void setRegionId(short regionId) {
		this.regionId = regionId;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public Integer getDeliveryManagerId() {
		return deliveryManagerId;
	}
	public void setDeliveryManagerId(Integer deliveryManagerId) {
		this.deliveryManagerId = deliveryManagerId;
	}
	public String getDeliveryManagerName() {
		return deliveryManagerName;
	}
	public void setDeliveryManagerName(String deliveryManagerName) {
		this.deliveryManagerName = deliveryManagerName;
	}
	public short getBuUnitId() {
		return buUnitId;
	}
	public void setBuUnitId(short buUnitId) {
		this.buUnitId = buUnitId;
	}
	public String getBuUnitName() {
		return buUnitName;
	}
	public void setBuUnitName(String buUnitName) {
		this.buUnitName = buUnitName;
	}
	public Short getDomainId() {
		return domainId;
	}
	public void setDomainId(Short domainId) {
		this.domainId = domainId;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public Integer getDomainMgrId() {
		return domainMgrId;
	}
	public void setDomainMgrId(Integer domainMgrId) {
		this.domainMgrId = domainMgrId;
	}
	public String getDomainMgrName() {
		return domainMgrName;
	}
	public void setDomainMgrName(String domainMgrName) {
		this.domainMgrName = domainMgrName;
	}
	public byte getProjectStatusId() {
		return projectStatusId;
	}
	public void setProjectStatusId(byte projectStatusId) {
		this.projectStatusId = projectStatusId;
	}
	public String getProjectStatusName() {
		return projectStatusName;
	}
	public void setProjectStatusName(String projectStatusName) {
		this.projectStatusName = projectStatusName;
	}
	public String getProjectType() {
		return projectType;
	}
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}
	public short getAccountId() {
		return accountId;
	}
	public void setAccountId(short accountId) {
		this.accountId = accountId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public Integer getAccountMgrId() {
		return accountMgrId;
	}
	public void setAccountMgrId(Integer accountMgrId) {
		this.accountMgrId = accountMgrId;
	}
	public String getAccountMgrName() {
		return accountMgrName;
	}
	public void setAccountMgrName(String accountMgrName) {
		this.accountMgrName = accountMgrName;
	}
	public String getProjectBillableFlag() {
		return projectBillableFlag;
	}
	public void setProjectBillableFlag(String projectBillableFlag) {
		this.projectBillableFlag = projectBillableFlag;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getCommentsForUpdation() {
		return commentsForUpdation;
	}
	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}
	public List<ProjectCoordinatorBean> getProjectCoordinatorDetails() {
		return projectCoordinatorDetails;
	}
	public void setProjectCoordinatorDetails(
			List<ProjectCoordinatorBean> projectCoordinatorDetails) {
		this.projectCoordinatorDetails = projectCoordinatorDetails;
	}
	public List<ProjectLevelUserAccessBean> getProjectLevelUserAccessDetails() {
		return projectLevelUserAccessDetails;
	}
	public void setProjectLevelUserAccessDetails(
			List<ProjectLevelUserAccessBean> projectLevelUserAccessDetails) {
		this.projectLevelUserAccessDetails = projectLevelUserAccessDetails;
	}

	public byte getProjectClassificationId() {
		return projectClassificationId;
	}
	public void setProjectClassificationId(byte projectClassificationId) {
		this.projectClassificationId = projectClassificationId;
	}
	public String getProjectClassificationName() {
		return projectClassificationName;
	}
	public void setProjectClassificationName(String projectClassificationName) {
		this.projectClassificationName = projectClassificationName;
	}
	public String getIsHavingAggregatePo() {
		return isHavingAggregatePo;
	}
	public void setIsHavingAggregatePo(String isHavingAggregatePo) {
		this.isHavingAggregatePo = isHavingAggregatePo;
	}
	
	public short getClientId() {
		return clientId;
	}
	public void setClientId(short clientId) {
		this.clientId = clientId;
	}
	
	
	public float getCompletionOfProj() {
		return completionOfProj;
	}
	public void setCompletionOfProj(float completionOfProj) {
		this.completionOfProj = completionOfProj;
	}
	@Override
	public String toString() {
		return "MasProjectOutputBean [projectId=" + projectId
				+ ", projectName=" + projectName + ", projectStartDate="
				+ projectStartDate + ", projectEndDate=" + projectEndDate
				+ ", projectDescription=" + projectDescription
				+ ", clientAddresses=" + clientAddresses + ", verticalId="
				+ verticalId + ", verticalName=" + verticalName + ", regionId="
				+ regionId + ", regionName=" + regionName
				+ ", deliveryManagerId=" + deliveryManagerId
				+ ", deliveryManagerName=" + deliveryManagerName
				+ ", buUnitId=" + buUnitId + ", buUnitName=" + buUnitName
				+ ", domainId=" + domainId + ", domainName=" + domainName
				+ ", domainMgrId=" + domainMgrId + ", domainMgrName="
				+ domainMgrName + ", projectStatusId=" + projectStatusId
				+ ", projectStatusName=" + projectStatusName + ", projectType="
				+ projectType + ", accountId=" + accountId + ", accountName="
				+ accountName + ", accountMgrId=" + accountMgrId
				+ ", accountMgrName=" + accountMgrName
				+ ", projectBillableFlag=" + projectBillableFlag
				+ ", createdBy=" + createdBy + ", createdOn=" + createdOn
				+ ", updatedBy=" + updatedBy + ", commentsForUpdation="
				+ commentsForUpdation + ", projectCoordinatorDetails="
				+ projectCoordinatorDetails
				+ ", projectLevelUserAccessDetails="
				+ projectLevelUserAccessDetails + ", projectClassificationId="
				+ projectClassificationId + ", projectClassificationName="
				+ projectClassificationName + ", isHavingAggregatePo="
				+ isHavingAggregatePo + ", clientId=" + clientId
				+ ", completionOfProj=" + completionOfProj + "]";
	}
	
	
}
