package com.thbs.mis.project.bean;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class WorkOrderViewInputBean {
	private String woNumber;
	private Integer woId;
	@NumberFormat(style = Style.NUMBER, pattern = "projectId should be numeric value")
	@NotNull(message = "projectId can not be null")
	@Min(1)
	private Integer projectId;
	private Date fromDate;
	private Date toDate;
	private String woType;
	private String poMapped;
	private byte woStatus;
	private Integer pageNumber;
	private Integer pageSize;
	private Integer viewedBy;
	
	public String getWoNumber() {
		return woNumber;
	}
	public void setWoNumber(String woNumber) {
		this.woNumber = woNumber;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getWoType() {
		return woType;
	}
	public void setWoType(String woType) {
		this.woType = woType;
	}
	public String getPoMapped() {
		return poMapped;
	}
	public void setPoMapped(String poMapped) {
		this.poMapped = poMapped;
	}
	public byte getWoStatus() {
		return woStatus;
	}
	public void setWoStatus(byte woStatus) {
		this.woStatus = woStatus;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getViewedBy() {
		return viewedBy;
	}
	public void setViewedBy(Integer viewedBy) {
		this.viewedBy = viewedBy;
	}
	public Integer getWoId() {
		return woId;
	}
	public void setWoId(Integer woId) {
		this.woId = woId;
	}
	
}
