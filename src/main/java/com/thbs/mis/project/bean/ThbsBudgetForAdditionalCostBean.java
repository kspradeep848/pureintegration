package com.thbs.mis.project.bean;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ThbsBudgetForAdditionalCostBean {

	@Min(1)
	private Integer budgetAdditionalCostId;
	
	@NotNull(message = "workorderId should not be null.")
	@Min(1)
	private Integer workorderId;

	@Min(0)
	private BigDecimal purchaseAmt;
	
	@Min(0)
	private BigDecimal expenseAmt;

	@Min(0)
	private BigDecimal travelAmt;

	@Min(1)
	private Integer createdBy;

	private String commentsForUpdation;

	private Integer updatedBy;

	public Integer getBudgetAdditionalCostId() {
		return budgetAdditionalCostId;
	}

	public void setBudgetAdditionalCostId(Integer budgetAdditionalCostId) {
		this.budgetAdditionalCostId = budgetAdditionalCostId;
	}

	public Integer getWorkorderId() {
		return workorderId;
	}

	public void setWorkorderId(Integer workorderId) {
		this.workorderId = workorderId;
	}

	public BigDecimal getPurchaseAmt() {
		return purchaseAmt;
	}

	public void setPurchaseAmt(BigDecimal purchaseAmt) {
		this.purchaseAmt = purchaseAmt;
	}

	public BigDecimal getExpenseAmt() {
		return expenseAmt;
	}

	public void setExpenseAmt(BigDecimal expenseAmt) {
		this.expenseAmt = expenseAmt;
	}

	public BigDecimal getTravelAmt() {
		return travelAmt;
	}

	public void setTravelAmt(BigDecimal travelAmt) {
		this.travelAmt = travelAmt;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getCommentsForUpdation() {
		return commentsForUpdation;
	}

	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString() {
		return "ThbsBudgetForAdditionalCostBean [budgetAdditionalCostId="
				+ budgetAdditionalCostId + ", workorderId=" + workorderId
				+ ", purchaseAmt=" + purchaseAmt + ", expenseAmt=" + expenseAmt
				+ ", travelAmt=" + travelAmt + ", createdBy=" + createdBy
				+ ", commentsForUpdation=" + commentsForUpdation
				+ ", updatedBy=" + updatedBy + "]";
	}

}
