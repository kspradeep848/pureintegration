package com.thbs.mis.project.bean;

public class ProjMasCompStatusBean {
	
	private short componentStatusId;
	
	private String ComponentStatus;

	public short getComponentStatusId() {
		return componentStatusId;
	}

	public void setComponentStatusId(short componentStatusId) {
		this.componentStatusId = componentStatusId;
	}

	public String getComponentStatus() {
		return ComponentStatus;
	}

	public void setComponentStatus(String componentStatus) {
		ComponentStatus = componentStatus;
	}

	@Override
	public String toString() {
		return "BootcampBean [componentStatusId=" + componentStatusId + ", ComponentStatus="
				+ ComponentStatus +  "]";
	}
	
}
