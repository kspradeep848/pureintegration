package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


public class ProjDatThbsBudgetAtResourceLevelBean {
	@Min(value=1,message = "BudgetDays must be greater than zero")
	@NotNull(message="BudgetDays should not be null")
	private Integer budgetedDays;
	private String commentsForUpdation;
	
	private Integer thbsBudgetResourceId;
	
	private BigDecimal costToCompany;
	
	private Date createdOn;
	@NotNull(message="resourceLocation should not be null")
	private String resourceLocation;
	private BigDecimal standardRate;
	private Date updatedOn;
	@NotNull(message="noOfResource should not be null")
	@Min(value=1)
	private Integer noOfResource;
	
	private Integer createdBy;
	@NotNull(message="ResourceLevelId should not be null")
	private short resourceLevelId;
	@NotNull(message="ResourceDesignationId should not be null")
	private short resourceRoleId;
	private Integer updatedBy;
	private Integer workorderId;
	
	public Integer getThbsBudgetResourceId() {
		return thbsBudgetResourceId;
	}
	public void setThbsBudgetResourceId(Integer thbsBudgetResourceId) {
		this.thbsBudgetResourceId = thbsBudgetResourceId;
	}
	public int getBudgetedDays() {
		return budgetedDays;
	}
	public short getResourceLevelId() {
		return resourceLevelId;
	}
	public void setResourceLevelId(short resourceLevelId) {
		this.resourceLevelId = resourceLevelId;
	}
	
	public void setBudgetedDays(int budgetedDays) {
		this.budgetedDays = budgetedDays;
	}
	
	public String getCommentsForUpdation() {
		return commentsForUpdation;
	}
	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}
	public BigDecimal getCostToCompany() {
		return costToCompany;
	}
	public void setCostToCompany(BigDecimal costToCompany) {
		this.costToCompany = costToCompany;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getResourceLocation() {
		return resourceLocation;
	}
	public void setResourceLocation(String resourceLocation) {
		this.resourceLocation = resourceLocation;
	}
	public BigDecimal getStandardRate() {
		return standardRate;
	}
	public void setStandardRate(BigDecimal standardRate) {
		this.standardRate = standardRate;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public int getNoOfResource() {
		return noOfResource;
	}
	public void setNoOfResource(int noOfResource) {
		this.noOfResource = noOfResource;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	
	public short getResourceRoleId() {
		return resourceRoleId;
	}
	public void setResourceRoleId(short resourceRoleId) {
		this.resourceRoleId = resourceRoleId;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public Integer getWorkorderId() {
		return workorderId;
	}
	public void setWorkorderId(Integer workorderId) {
		this.workorderId = workorderId;
	}
	@Override
	public String toString() {
		return "ProjDatThbsBudgetAtResourceLevelBean [budgetedDays="
				+ budgetedDays + ", commentsForUpdation=" + commentsForUpdation
				+ ", thbsBudgetResourceId=" + thbsBudgetResourceId
				+ ", costToCompany=" + costToCompany + ", createdOn="
				+ createdOn + ", resourceLocation=" + resourceLocation
				+ ", standardRate=" + standardRate + ", updatedOn=" + updatedOn
				+ ", noOfResource=" + noOfResource + ", createdBy=" + createdBy
				+ ", resourceLevelId=" + resourceLevelId + ", resourceRoleId="
				+ resourceRoleId + ", updatedBy=" + updatedBy
				+ ", workorderId=" + workorderId + "]";
	}
	
	

}
