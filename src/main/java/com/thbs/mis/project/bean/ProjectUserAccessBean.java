package com.thbs.mis.project.bean;


public class ProjectUserAccessBean implements Comparable<ProjectUserAccessBean>
{	
	private Integer empId;
	private String empName;
	private String emp;
	
	public Integer getEmpId() {
		return empId;
	}
	
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	
	public String getEmpName() {
		return empName;
	}
	
	public void setEmpName(String empName) {
		this.empName = empName;
	}


	public String getEmp() {
		return emp;
	}

	public void setEmp(String emp) {
		this.emp = emp;
	}

	@Override
	public int compareTo(ProjectUserAccessBean arg0) 
	{
		return this.getEmpName().toUpperCase().compareTo(arg0.getEmpName().toUpperCase());
	}

	@Override
	public String toString() {
		return "ProjectUserAccessBean [empId=" + empId + ", empName=" + empName
				+ ", emp=" + emp + "]";
	}

	
}
