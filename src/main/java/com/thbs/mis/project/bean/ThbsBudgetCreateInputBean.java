package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class ThbsBudgetCreateInputBean {
	
	private Integer pkThbsBudgetId;

	@NotNull(message="Client budget id cannot be null")
	private Integer fkClientBudgetId;
	
	@NotNull(message="Resource designation id cannot be null")
	private Integer fkResourceDesignationId;
	
	@NotNull(message="Resource level id cannot be null")
	private Integer fkResourceLevelId;
	
	private Integer standardRate;
	
	@NotNull(message="Budget location  cannot be null")
//	@NotBlank(message="Client budget id cannot be blank")
//	@NotEmpty(message="Client budget id cannot be empty")
	private String budgetLocation;
	
	@NotNull(message="Number of Resources cannot be null")
	private Integer noOfResource;
	
	private BigDecimal costToCompany;
	
	@NotNull(message="Thbs budget location id cannot be null")
	private Integer fkThbsBudgetLocationId;
	
	@NotNull(message="Budgeted days cannot be null")
	private Integer budgetedDays;
	
	private Integer createdBy;
	private String CommentsForUpdate;
	private Integer updatedBy;
	private Date createdOn;
	
	
	
	public Integer getFkClientBudgetId() {
		return fkClientBudgetId;
	}
	public void setFkClientBudgetId(Integer fkClientBudgetId) {
		this.fkClientBudgetId = fkClientBudgetId;
	}
	public Integer getFkResourceDesignationId() {
		return fkResourceDesignationId;
	}
	public void setFkResourceDesignationId(Integer fkResourceDesignationId) {
		this.fkResourceDesignationId = fkResourceDesignationId;
	}
	public Integer getFkResourceLevelId() {
		return fkResourceLevelId;
	}
	public void setFkResourceLevelId(Integer fkResourceLevelId) {
		this.fkResourceLevelId = fkResourceLevelId;
	}
	public Integer getStandardRate() {
		return standardRate;
	}
	public void setStandardRate(Integer standardRate) {
		this.standardRate = standardRate;
	}
	public String getBudgetLocation() {
		return budgetLocation;
	}
	public void setBudgetLocation(String budgetLocation) {
		this.budgetLocation = budgetLocation;
	}
	public Integer getNoOfResource() {
		return noOfResource;
	}
	public void setNoOfResource(Integer noOfResource) {
		this.noOfResource = noOfResource;
	}
	public BigDecimal getCostToCompany() {
		return costToCompany;
	}
	public void setCostToCompany(BigDecimal costToCompany) {
		this.costToCompany = costToCompany;
	}
	public Integer getFkThbsBudgetLocationId() {
		return fkThbsBudgetLocationId;
	}
	public void setFkThbsBudgetLocationId(Integer fkThbsBudgetLocationId) {
		this.fkThbsBudgetLocationId = fkThbsBudgetLocationId;
	}
	public Integer getBudgetedDays() {
		return budgetedDays;
	}
	public void setBudgetedDays(Integer budgetedDays) {
		this.budgetedDays = budgetedDays;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public String getCommentsForUpdate() {
		return CommentsForUpdate;
	}
	public void setCommentsForUpdate(String commentsForUpdate) {
		CommentsForUpdate = commentsForUpdate;
	}
	
	public Integer getPkThbsBudgetId() {
		return pkThbsBudgetId;
	}
	public void setPkThbsBudgetId(Integer pkThbsBudgetId) {
		this.pkThbsBudgetId = pkThbsBudgetId;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	
	@Override
	public String toString() {
		return "ThbsBudgetCreateInputBean [fkClientBudgetId="
				+ fkClientBudgetId 
				+ ", fkResourceDesignationId=" + fkResourceDesignationId
				+ ", fkResourceLevelId=" + fkResourceLevelId
				+ ", standardRate=" + standardRate + ", budgetLocation="
				+ budgetLocation + ", noOfResource=" + noOfResource
				+ ", costToCompany=" + costToCompany
				+ ", fkThbsBudgetLocationId=" + fkThbsBudgetLocationId
				+ ", budgetedDays=" + budgetedDays + ", createdBy=" + createdBy
				+ ", CommentsForUpdate=" + CommentsForUpdate + "]";
	}
	
	
	
}
