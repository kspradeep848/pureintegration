package com.thbs.mis.project.bean;

import java.util.List;

public class WorkOrderDocOutputBean {

	private List<DocDetails> docDetails;

	public List<DocDetails> getDocDetails() {
		return docDetails;
	}

	public void setDocDetails(List<DocDetails> docDetails) {
		this.docDetails = docDetails;
	}

	@Override
	public String toString() {
		return "WorkOrderDocOutputBean [docDetails=" + docDetails + "]";
	}

}
