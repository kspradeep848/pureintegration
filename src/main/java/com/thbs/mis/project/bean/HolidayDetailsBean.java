package com.thbs.mis.project.bean;

import java.math.BigDecimal;

public class HolidayDetailsBean {
	private Integer clientBudgetRateDetailsId;
	private Byte rateTypeId;
	private Byte invoiceRateTypeId;
	private BigDecimal value;

	public Integer getClientBudgetRateDetailsId() {
		return clientBudgetRateDetailsId;
	}

	public void setClientBudgetRateDetailsId(Integer clientBudgetRateDetailsId) {
		this.clientBudgetRateDetailsId = clientBudgetRateDetailsId;
	}

	public Byte getRateTypeId() {
		return rateTypeId;
	}

	public void setRateTypeId(Byte rateTypeId) {
		this.rateTypeId = rateTypeId;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public Byte getInvoiceRateTypeId() {
		return invoiceRateTypeId;
	}

	public void setInvoiceRateTypeId(Byte invoiceRateTypeId) {
		this.invoiceRateTypeId = invoiceRateTypeId;
	}

	@Override
	public String toString() {
		return "HolidayDetailsBean [clientBudgetRateDetailsId="
				+ clientBudgetRateDetailsId + ", rateTypeId=" + rateTypeId
				+ ", invoiceRateTypeId=" + invoiceRateTypeId + ", value="
				+ value + "]";
	}

}
