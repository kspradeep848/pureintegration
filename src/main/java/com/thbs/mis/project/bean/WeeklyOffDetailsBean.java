package com.thbs.mis.project.bean;

import java.math.BigDecimal;

public class WeeklyOffDetailsBean {
	
	private Integer weeklyOffid;
	private BigDecimal weeklyOffRate;
	public Integer getWeeklyOffid() {
		return weeklyOffid;
	}
	public void setWeeklyOffid(Integer weeklyOffid) {
		this.weeklyOffid = weeklyOffid;
	}
	public BigDecimal getWeeklyOffRate() {
		return weeklyOffRate;
	}
	public void setWeeklyOffRate(BigDecimal weeklyOffRate) {
		this.weeklyOffRate = weeklyOffRate;
	}
	@Override
	public String toString() {
		return "WeeklyOffDetailsBean [weeklyOffid=" + weeklyOffid
				+ ", weeklyOffRate=" + weeklyOffRate + "]";
	}
	
}
