package com.thbs.mis.project.bean;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

public class RateDetailsBean {
	
	@NotNull
	private Byte fkRateTypeId;
	private Byte invoiceRateTypeid;
	private BigDecimal value;
	private Integer pkClientBudgetRateDetId;
	private Integer weeklyOffId;
	
	public Byte getFkRateTypeId() {
		return fkRateTypeId;
	}
	public void setFkRateTypeId(Byte fkRateTypeId) {
		this.fkRateTypeId = fkRateTypeId;
	}
	public Byte getInvoiceRateTypeid() {
		return invoiceRateTypeid;
	}
	public void setInvoiceRateTypeid(Byte invoiceRateTypeid) {
		this.invoiceRateTypeid = invoiceRateTypeid;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public Integer getPkClientBudgetRateDetId() {
		return pkClientBudgetRateDetId;
	}
	public void setPkClientBudgetRateDetId(Integer pkClientBudgetRateDetId) {
		this.pkClientBudgetRateDetId = pkClientBudgetRateDetId;
	}

	public Integer getWeeklyOffId() {
		return weeklyOffId;
	}
	public void setWeeklyOffId(Integer weeklyOffId) {
		this.weeklyOffId = weeklyOffId;
	}
	@Override
	public String toString() {
		return "RateDetailsBean [fkRateTypeId=" + fkRateTypeId
				+ ", invoiceRateTypeid=" + invoiceRateTypeid + ", value="
				+ value + ", pkClientBudgetRateDetId="
				+ pkClientBudgetRateDetId + ", weeklyOffId=" + weeklyOffId
				+ "]";
	}
	

}
