package com.thbs.mis.project.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class ViewHistoryOutputBean {

	private String message;

	private Date createdOn;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@JsonSerialize(using = CustomTimeSerializer.class)  
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public String toString() {
		return "ViewHistoryOutputBean [message=" + message + ", createdOn=" + createdOn + "]";
	}

}
