package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.Date;

public class ComponentDetailsOutputBean {

	private int componentId;
	
	private String componentName;
	
	private int noOfResources;
	
	private Long noOfMileStones;
	
	private BigDecimal componentValue;
	
	private String componentStatus;
	
	private String componentType;
	
	private BigDecimal componentBalance;
	
	private double componentOwnerId;
	
	private double componentOwnerName;
	
	private String mileStoneTriggered;
	
	private Date componentStartDate;
	
	private Date componentEndDate;

	public int getComponentId() {
		return componentId;
	}

	public void setComponentId(int componentId) {
		this.componentId = componentId;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public int getNoOfResources() {
		return noOfResources;
	}

	public void setNoOfResources(int noOfResources) {
		this.noOfResources = noOfResources;
	}

	public Long getNoOfMileStones() {
		return noOfMileStones;
	}

	public void setNoOfMileStones(Long noOfMileStones) {
		this.noOfMileStones = noOfMileStones;
	}

	public BigDecimal getComponentValue() {
		return componentValue;
	}

	public void setComponentValue(BigDecimal componentValue) {
		this.componentValue = componentValue;
	}

	public String getComponentStatus() {
		return componentStatus;
	}

	public void setComponentStatus(String componentStatus) {
		this.componentStatus = componentStatus;
	}

	public String getComponentType() {
		return componentType;
	}

	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}

	

	public BigDecimal getComponentBalance() {
		return componentBalance;
	}

	public void setComponentBalance(BigDecimal componentBalance) {
		this.componentBalance = componentBalance;
	}

	public double getComponentOwnerId() {
		return componentOwnerId;
	}

	public void setComponentOwnerId(double componentOwnerId) {
		this.componentOwnerId = componentOwnerId;
	}

	public double getComponentOwnerName() {
		return componentOwnerName;
	}

	public void setComponentOwnerName(double componentOwnerName) {
		this.componentOwnerName = componentOwnerName;
	}

	public String getMileStoneTriggered() {
		return mileStoneTriggered;
	}

	public void setMileStoneTriggered(String mileStoneTriggered) {
		this.mileStoneTriggered = mileStoneTriggered;
	}

	public Date getComponentStartDate() {
		return componentStartDate;
	}

	public void setComponentStartDate(Date componentStartDate) {
		this.componentStartDate = componentStartDate;
	}

	public Date getComponentEndDate() {
		return componentEndDate;
	}

	public void setComponentEndDate(Date componentEndDate) {
		this.componentEndDate = componentEndDate;
	}

	@Override
	public String toString() {
		return "ComponentDetailsOutputBean [componentId=" + componentId
				+ ", componentName=" + componentName + ", noOfResources="
				+ noOfResources + ", noOfMileStones=" + noOfMileStones
				+ ", componentValue=" + componentValue + ", componentStatus="
				+ componentStatus + ", componentType=" + componentType
				+ ", componentBalance=" + componentBalance
				+ ", componentOwnerId=" + componentOwnerId
				+ ", componentOwnerName=" + componentOwnerName
				+ ", mileStoneTriggered=" + mileStoneTriggered
				+ ", componentStartDate=" + componentStartDate
				+ ", componentEndDate=" + componentEndDate + "]";
	}
	
	
	
	
	
}
