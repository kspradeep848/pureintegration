package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.Date;


public class ThbsBudgetOutputBean {
	

private Integer fkClientBudgetId;
private Integer pkThbsBudgetId;
private Integer fkWorkorderId;
private Integer fkResourceDesignationId;
private String ResourceDesignationName;
private Integer fkResourceLevelId;
private String ResourceLevelName;
private BigDecimal standardRate;
private String budgetLocation;
private Integer noOfResource;
private BigDecimal costToCompany;
private Integer fkThbsBudgetLocationId;
private String fkThbsBudgetLocationName;
private Integer budgetedDays;
private Integer createdBy;
private Date createdOn;
private String status;
private Short currencyId;
private String currencyName;


public Short getCurrencyId() {
	return currencyId;
}
public void setCurrencyId(Short currencyId) {
	this.currencyId = currencyId;
}
public String getCurrencyName() {
	return currencyName;
}
public void setCurrencyName(String currencyName) {
	this.currencyName = currencyName;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public Integer getFkClientBudgetId() {
	return fkClientBudgetId;
}
public void setFkClientBudgetId(Integer fkClientBudgetId) {
	this.fkClientBudgetId = fkClientBudgetId;
}
public Integer getPkThbsBudgetId() {
	return pkThbsBudgetId;
}
public void setPkThbsBudgetId(Integer pkThbsBudgetId) {
	this.pkThbsBudgetId = pkThbsBudgetId;
}
public Integer getFkWorkorderId() {
	return fkWorkorderId;
}
public void setFkWorkorderId(Integer fkWorkorderId) {
	this.fkWorkorderId = fkWorkorderId;
}
public Integer getFkResourceDesignationId() {
	return fkResourceDesignationId;
}
public void setFkResourceDesignationId(Integer fkResourceDesignationId) {
	this.fkResourceDesignationId = fkResourceDesignationId;
}
public String getResourceDesignationName() {
	return ResourceDesignationName;
}
public void setResourceDesignationName(String resourceDesignationName) {
	ResourceDesignationName = resourceDesignationName;
}
public Integer getFkResourceLevelId() {
	return fkResourceLevelId;
}
public void setFkResourceLevelId(Integer fkResourceLevelId) {
	this.fkResourceLevelId = fkResourceLevelId;
}
public String getResourceLevelName() {
	return ResourceLevelName;
}
public void setResourceLevelName(String resourceLevelName) {
	ResourceLevelName = resourceLevelName;
}
public BigDecimal getStandardRate() {
	return standardRate;
}
public void setStandardRate(BigDecimal standardRate) {
	this.standardRate = standardRate;
}
public String getBudgetLocation() {
	return budgetLocation;
}
public void setBudgetLocation(String budgetLocation) {
	this.budgetLocation = budgetLocation;
}
public Integer getNoOfResource() {
	return noOfResource;
}
public void setNoOfResource(Integer noOfResource) {
	this.noOfResource = noOfResource;
}
public BigDecimal getCostToCompany() {
	return costToCompany;
}
public void setCostToCompany(BigDecimal costToCompany) {
	this.costToCompany = costToCompany;
}
public Integer getFkThbsBudgetLocationId() {
	return fkThbsBudgetLocationId;
}
public void setFkThbsBudgetLocationId(Integer fkThbsBudgetLocationId) {
	this.fkThbsBudgetLocationId = fkThbsBudgetLocationId;
}
public String getFkThbsBudgetLocationName() {
	return fkThbsBudgetLocationName;
}
public void setFkThbsBudgetLocationName(String fkThbsBudgetLocationName) {
	this.fkThbsBudgetLocationName = fkThbsBudgetLocationName;
}
public Integer getBudgetedDays() {
	return budgetedDays;
}
public void setBudgetedDays(Integer budgetedDays) {
	this.budgetedDays = budgetedDays;
}
public Integer getCreatedBy() {
	return createdBy;
}
public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
}
public Date getCreatedOn() {
	return createdOn;
}
public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
}
@Override
public String toString() {
	return "ThbsBudgetOutputBean [fkClientBudgetId=" + fkClientBudgetId
			+ ", pkThbsBudgetId=" + pkThbsBudgetId + ", fkWorkorderId="
			+ fkWorkorderId + ", fkResourceDesignationId="
			+ fkResourceDesignationId + ", ResourceDesignationName="
			+ ResourceDesignationName + ", fkResourceLevelId="
			+ fkResourceLevelId + ", ResourceLevelName=" + ResourceLevelName
			+ ", standardRate=" + standardRate + ", budgetLocation="
			+ budgetLocation + ", noOfResource=" + noOfResource
			+ ", costToCompany=" + costToCompany + ", fkThbsBudgetLocationId="
			+ fkThbsBudgetLocationId + ", fkThbsBudgetLocationName="
			+ fkThbsBudgetLocationName + ", budgetedDays=" + budgetedDays
			+ ", createdBy=" + createdBy + ", createdOn=" + createdOn
			+ ", status=" + status + ", currencyId=" + currencyId
			+ ", currencyName=" + currencyName + "]";
}

}
