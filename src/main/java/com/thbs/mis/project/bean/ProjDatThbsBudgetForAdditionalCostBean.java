package com.thbs.mis.project.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

public class ProjDatThbsBudgetForAdditionalCostBean {
	
	
	private List<ProjDatThbsBudgetAtResourceLevelBean> thbsRoleBudgetList;
	
	
	private String commentsForUpdation;
	
	private Date createdOn;
	@NotNull(message="Expense Amount should not be Null")
	private BigDecimal expenseAmt;
	@NotNull(message="Purchase Amount should not be Null")
	private BigDecimal purchaseAmt;
	@NotNull(message="Travel Amount should not be Null")
	private BigDecimal travelAmt;
	private Date updatedOn;
	
	@NotNull(message="LoggedIn user field cannot be Null")
	private Integer loggedInId;
	
	@NotNull(message="WorkOrderId should not be Null")
	private Integer workorderId;
	
	
	public Integer getWorkorderId() {
		return workorderId;
	}
	public void setWorkorderId(Integer workorderId) {
		this.workorderId = workorderId;
	}
	public String getCommentsForUpdation() {
		return commentsForUpdation;
	}
	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public BigDecimal getExpenseAmt() {
		return expenseAmt;
	}
	public void setExpenseAmt(BigDecimal expenseAmt) {
		this.expenseAmt = expenseAmt;
	}
	public BigDecimal getPurchaseAmt() {
		return purchaseAmt;
	}
	public void setPurchaseAmt(BigDecimal purchaseAmt) {
		this.purchaseAmt = purchaseAmt;
	}
	public BigDecimal getTravelAmt() {
		return travelAmt;
	}
	public void setTravelAmt(BigDecimal travelAmt) {
		this.travelAmt = travelAmt;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
		
	public Integer getLoggedInId() {
		return loggedInId;
	}
	public void setLoggedInId(Integer loggedInId) {
		this.loggedInId = loggedInId;
	}
	public List<ProjDatThbsBudgetAtResourceLevelBean> getThbsRoleBudgetList() {
		return thbsRoleBudgetList;
	}
	public void setThbsRoleBudgetList(
			List<ProjDatThbsBudgetAtResourceLevelBean> thbsRoleBudgetList) {
		this.thbsRoleBudgetList = thbsRoleBudgetList;
	}
	
	@Override
	public String toString() {
		return "ProjDatThbsBudgetForAdditionalCostBean [thbsRoleBudgetList="
				+ thbsRoleBudgetList + ", commentsForUpdation="
				+ commentsForUpdation + ", createdOn=" + createdOn
				+ ", expenseAmt=" + expenseAmt + ", purchaseAmt=" + purchaseAmt
				+ ", travelAmt=" + travelAmt + ", updatedOn=" + updatedOn
				+ ", loggedInId=" + loggedInId + ", workorderId=" + workorderId
				+ "]";
	}
	
	
}
