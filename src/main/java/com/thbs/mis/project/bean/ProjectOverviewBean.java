package com.thbs.mis.project.bean;

public class ProjectOverviewBean {

	public Long totalProject;

	public String projectLink;

	public Long totalWorkOrder;

	public String workOrderLink;

	public Long totalComponent;

	public String componentLink;

	public Long totalMilestone;

	public String milestoneLink;

	public Long totalEmployee;

	public String employeeLink;

	public ProjectOverviewBean() {
		super();
	}

	public ProjectOverviewBean(Long totalProject, Long totalWorkOrder,
			Long totalComponent, Long totalMilestone) {
		super();
		this.totalProject = totalProject;
		this.totalWorkOrder = totalWorkOrder;
		this.totalComponent = totalComponent;
		this.totalMilestone = totalMilestone;
	}

	public Long getTotalProject() {
		return totalProject;
	}

	public void setTotalProject(Long totalProject) {
		this.totalProject = totalProject;
	}

	public String getProjectLink() {
		return projectLink;
	}

	public void setProjectLink(String projectLink) {
		this.projectLink = projectLink;
	}

	public Long getTotalWorkOrder() {
		return totalWorkOrder;
	}

	public void setTotalWorkOrder(Long totalWorkOrder) {
		this.totalWorkOrder = totalWorkOrder;
	}

	public String getWorkOrderLink() {
		return workOrderLink;
	}

	public void setWorkOrderLink(String workOrderLink) {
		this.workOrderLink = workOrderLink;
	}

	public Long getTotalComponent() {
		return totalComponent;
	}

	public void setTotalComponent(Long totalComponent) {
		this.totalComponent = totalComponent;
	}

	public String getComponentLink() {
		return componentLink;
	}

	public void setComponentLink(String componentLink) {
		this.componentLink = componentLink;
	}

	public Long getTotalMilestone() {
		return totalMilestone;
	}

	public void setTotalMilestone(Long totalMilestone) {
		this.totalMilestone = totalMilestone;
	}

	public String getMilestoneLink() {
		return milestoneLink;
	}

	public void setMilestoneLink(String milestoneLink) {
		this.milestoneLink = milestoneLink;
	}

	public Long getTotalEmployee() {
		return totalEmployee;
	}

	public void setTotalEmployee(Long totalEmployee) {
		this.totalEmployee = totalEmployee;
	}

	public String getEmployeeLink() {
		return employeeLink;
	}

	public void setEmployeeLink(String employeeLink) {
		this.employeeLink = employeeLink;
	}

	@Override
	public String toString() {
		return "ProjectOverviewBean [totalProject=" + totalProject
				+ ", projectLink=" + projectLink + ", totalWorkOrder="
				+ totalWorkOrder + ", workOrderLink=" + workOrderLink
				+ ", totalComponent=" + totalComponent + ", componentLink="
				+ componentLink + ", totalMilestone=" + totalMilestone
				+ ", milestoneLink=" + milestoneLink + ", totalEmployee="
				+ totalEmployee + ", employeeLink=" + employeeLink + "]";
	}
}
