package com.thbs.mis.project.bean;

public class ProjectNamesOutputBean {
	
	private Integer projectId;
	
	private String projectName;

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public ProjectNamesOutputBean(Integer projectId, String projectName) {
		super();
		this.projectId = projectId;
		this.projectName = projectName;
	}

	@Override
	public String toString() {
		return "ProjectNamesOutputBean [projectId=" + projectId
				+ ", projectName=" + projectName + "]";
	}

	@Override
	public boolean equals(Object o) {
		 if (o == this) {
	            return true;
	        }
		 if (!(o instanceof ProjectNamesOutputBean)) {
	            return false;
	        }
		 ProjectNamesOutputBean p =(ProjectNamesOutputBean)o;
		
		 return (p.projectId.equals(this.projectId) && p.projectName.equals(this.projectName))?true:false;
	}
	
	 @Override
	    public int hashCode() {
	        final int prime = 13;
	        int result = 1;
	        result = prime * result
	                + (this.projectName.hashCode()+this.projectId.hashCode());
	        return result;
	    }

}
