package com.thbs.mis.project.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjDatProjectMappedToProjectCoordinatorBO;

@Repository
public interface ProjDatProjectMappedToProjectCoordinatorRepository 
extends GenericRepository<ProjDatProjectMappedToProjectCoordinatorBO, Integer>
{

	@Query("SELECT prj.fkProjectCoordinatorId from ProjDatProjectMappedToProjectCoordinatorBO prj where prj.fkProjectId = :projectId")
	public Set<Integer> getByProjectId(@Param("projectId") Integer integer);

    public ProjDatProjectMappedToProjectCoordinatorBO 
    findByFkProjectIdAndFkProjectCoordinatorId (Integer projectid , Integer pcId );
//Added by prathibha for create client budget
    
    @Query("SELECT pc FROM ProjDatProjectMappedToProjectCoordinatorBO pc JOIN "
    		+ "MasProjectBO masprj ON pc.fkProjectId = masprj.pkProjectId WHERE "
    		+ "pc.fkProjectId =:projId AND (pc.fkProjectCoordinatorId =:empId OR "
    		+ "masprj.fkDomainMgrId =:empId OR masprj.fkDeliveryMgrId =:empId) ")
    public ProjDatProjectMappedToProjectCoordinatorBO finddByProjectandPCorDMorDL(@Param("projId")Integer projId , @Param("empId") Integer empId);
    
    
    
/*@Query("SELECT pc FROM ProjDatProjectMappedToProjectCoordinatorBO pc JOIN "
	+ "ProjectLevelUserAccessGroupBO ug ON pc.fkProjectId = ug.fkProjectId "
	+ "WHERE pc.fkProjectId =:projId AND (pc.fkProjectCoordinatorId =:empId OR "
	+ "ug.fkUserId =:empId)GROUP BY pc.fkProjectId ")
public ProjDatProjectMappedToProjectCoordinatorBO pcTableUserAccessTableDetails
       (@Param("projId")Integer projId , @Param("empId") Integer empId );*/
//EOA by prathibha  


public  List<ProjDatProjectMappedToProjectCoordinatorBO> findByFkProjectId(int fkProjectId);


//Added by Lalith kumar for getting all Pc's related to project
@Query("SELECT p.fkProjectCoordinatorId FROM "
		+ "ProjDatProjectMappedToProjectCoordinatorBO p "
		+ "inner join DatWorkorderBO d on p.fkProjectId= d.fkProjectId"
		+ " where d.pkWorkorderId = :woId")
public List<Integer> getAllPcRelatedToProject(@Param("woId") Integer woId);
//EOA by Lalith kumar

	public List<ProjDatProjectMappedToProjectCoordinatorBO> findAll();

}
