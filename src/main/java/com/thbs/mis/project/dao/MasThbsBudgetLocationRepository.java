package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bean.ThbsBudgetLocationOutputBean;
import com.thbs.mis.project.bo.MasThbsBudgetLocationBO;

public interface MasThbsBudgetLocationRepository
extends
GenericRepository<MasThbsBudgetLocationBO, Integer>{

	@Query("SELECT new com.thbs.mis.project.bean.ThbsBudgetLocationOutputBean(thbsLoc.pkThbsBudgetLocationId,"
			+ " thbsLoc.onsiteOffshoreFlag, thbsLoc.budgetLocationName)"
			+ " FROM  MasThbsBudgetLocationBO thbsLoc "
			+ " JOIN "
			+ " ProjDatBudgetLocationMappedParentLocationBO loc"
			+ " ON thbsLoc.pkThbsBudgetLocationId = loc.fkFinanceBudgetLocationId"
			+ " JOIN "
			+ " ProjDatClientBudgetBO bdg "
			+ " ON bdg.fkParentLocationId = loc.fkParentLocationId"
			+ " WHERE"
			+ " bdg.pkClientBudgetId =:clientBudgetId")
	public List<ThbsBudgetLocationOutputBean> getAllThbsBudgetLocationBasedOnClientBudgetId(@Param("clientBudgetId") Integer clientBudgetId);
	
	
}
