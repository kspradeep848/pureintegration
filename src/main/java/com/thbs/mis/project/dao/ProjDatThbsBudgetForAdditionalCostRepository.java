package com.thbs.mis.project.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjDatThbsBudgetForAdditionalCostBO;


@Repository
public interface ProjDatThbsBudgetForAdditionalCostRepository extends 
			GenericRepository<ProjDatThbsBudgetForAdditionalCostBO, Integer>
{
	public ProjDatThbsBudgetForAdditionalCostBO findByFkWorkorderId(Integer workorderId);
	
	public ProjDatThbsBudgetForAdditionalCostBO findByPkThbsBudgetAdditionalCostId(Integer budgetAdditionalCostId);

}
