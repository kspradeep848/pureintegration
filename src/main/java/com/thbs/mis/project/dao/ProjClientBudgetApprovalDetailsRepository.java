package com.thbs.mis.project.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjClientBudgetApprovalDetailBO;


@Repository
public interface ProjClientBudgetApprovalDetailsRepository extends
		GenericRepository<ProjClientBudgetApprovalDetailBO, Integer> {

	@Query("select p.fkBudgetStatusId from ProjClientBudgetApprovalDetailBO p where p.fkClientBudgetId =:clientBudgetId")
	public Integer findByFkClientBudgetId(
			@Param("clientBudgetId") Integer clientBudgetId);
	
}