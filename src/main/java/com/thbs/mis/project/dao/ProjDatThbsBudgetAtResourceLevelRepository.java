
package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjDatThbsBudgetAtResourceLevelBO;

@Repository
public interface ProjDatThbsBudgetAtResourceLevelRepository extends 
		GenericRepository<ProjDatThbsBudgetAtResourceLevelBO, Integer>
{
	public List<ProjDatThbsBudgetAtResourceLevelBO> findByFkWorkorderId(Integer fkWorkorderId);
	
	public List<ProjDatThbsBudgetAtResourceLevelBO> findByFkWorkorderIdAndFkResourceDesignationId(Integer fkWorkorderId,short fkResourceDesignationId);
	
	public ProjDatThbsBudgetAtResourceLevelBO findByPkThbsBudgetResourceId(Integer pkThbsBudgetResourceId);
	
	@Query("select bo from ProjDatThbsBudgetAtResourceLevelBO bo where bo.fkWorkorderId =:fkWorkorderId")
	public ProjDatThbsBudgetAtResourceLevelBO getThbsBudgetByWoId(@Param("fkWorkorderId") Integer fkWorkorderId);
}
