package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.DatComponentAuditBO;

public interface DatComponentAuditRepository extends
		GenericRepository<DatComponentAuditBO, Integer> {

	@Query("SELECT rep FROM DatComponentAuditBO rep Where rep.fkComponentId=:fkComponentId"
			+ " AND rep.action = :action")
	public DatComponentAuditBO findByFkComponentId(
			@Param("fkComponentId") Integer fkComponentId,
			@Param("action") String action);

	@Query("SELECT rep FROM DatComponentAuditBO rep Where rep.fkComponentId=:fkComponentId"
			+ " AND rep.commentsForUpdation NOT LIKE :pattern%")
	public List<DatComponentAuditBO> findByFkComponentIdAndPattern(
			@Param("fkComponentId") Integer fkComponentId,
			@Param("pattern") String pattern, Pageable topThreeComp);

	@Query("select bo from DatComponentAuditBO bo where bo.fkComponentId=:fkComponentId "
			+ "and bo.compHistory!=null")
	public List<DatComponentAuditBO> getByComponentId(
			@Param("fkComponentId") Integer fkComponentId, Pageable topThreeComp);

}
