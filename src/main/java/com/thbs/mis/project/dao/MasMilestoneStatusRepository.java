package com.thbs.mis.project.dao;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.MasMilestoneStatusBO;

@Repository
public interface MasMilestoneStatusRepository extends GenericRepository<MasMilestoneStatusBO, Integer> {


	@Query("SELECT d FROM MasMilestoneStatusBO d WHERE d.pkMilestoneStatusId= :milestoneStatusId")
	List<MasMilestoneStatusBO> listOfMilestoneStatusIdDetails(@Param("milestoneStatusId") byte milestoneStatusId);

	MasMilestoneStatusBO findByPkMilestoneStatusId(byte fkMilestoneStatusId);
	
	@Query("SELECT status from MasMilestoneStatusBO status where status.milestoneStatusName in (:mileStoneStatusNameList)")
	public List<MasMilestoneStatusBO> findByMilestoneStatusName(@Param("mileStoneStatusNameList") List<String> mileStoneStatusNameList);


}
