package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.DatMilestoneAuditBO;


public interface DatMilestoneAuditRepository extends GenericRepository<DatMilestoneAuditBO, Integer> {
	
	public DatMilestoneAuditBO findByFkMilestoneIdAndAction(Integer fkMilestoneId, String action);
	
	@Query("SELECT rep FROM DatMilestoneAuditBO rep Where rep.fkMilestoneId=:fkMilestoneId"
			+ " AND rep.milestoneHistory != null")
	public List<DatMilestoneAuditBO> findAllUpdatedByFkMilestoneId(@Param("fkMilestoneId") Integer fkMilestoneId, Pageable topThreeMile);
	
}
