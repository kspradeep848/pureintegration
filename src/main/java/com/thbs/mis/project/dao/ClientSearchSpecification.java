package com.thbs.mis.project.dao;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.criteria.Root;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;

import com.thbs.mis.project.bo.MasClientBO;
import com.thbs.mis.project.bean.ClientSearchInputBean;

import org.springframework.data.jpa.domain.Specification;

public class ClientSearchSpecification {
	
	public static Specification<MasClientBO> getClientStatus(ClientSearchInputBean searchBean)
	{
		return new Specification<MasClientBO>() {
			@Override
		
			public Predicate toPredicate(Root<MasClientBO> root,
					CriteriaQuery<?> query, CriteriaBuilder cb)
					{
				final Collection<Predicate> predicates = new ArrayList<Predicate>();
				if(searchBean.getClientStatus()!=null)
				{
					final Predicate statusPredicate = cb.equal(
							root.get("clientStatus"), searchBean.getClientStatus());
					predicates.add(statusPredicate);
				}
				if(searchBean.getClientId() != null)
				{
					final Predicate clientIdPredicate = cb.equal(
							root.get("pkClientId"), searchBean.getClientId());
					predicates.add(clientIdPredicate);
				}
				
				if(searchBean.getRegionId() != null)
				{
					final Predicate fkRegionIdPredicate = cb.equal(
							root.get("fkRegionId"), searchBean.getRegionId());
					predicates.add(fkRegionIdPredicate);
				}
				
				return cb.and(predicates.toArray(new Predicate[predicates
				                       						.size()]));
					}
			
			
		};
	}

}
