package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.thbs.mis.common.dao.GenericRepository;

import com.thbs.mis.project.bo.ProjDatMilestoneTriggerDetailBO;

public interface ProjDatMilestoneTriggerDetailRepository
		extends GenericRepository<ProjDatMilestoneTriggerDetailBO, Integer> {
	
	//Added by Smrithi
		
		@Query( " SELECT milestoneTrig  from ProjDatMilestoneTriggerDetailBO  milestoneTrig "
				+ " JOIN DatMilestoneBO milestone  on milestone.pkMilestoneId = milestoneTrig.fkMilestoneId "
				+ " JOIN DatComponentBO comp on comp.pkComponentId = milestone.fkComponentId "
				+ " where comp.fkWorkorderId = ? ")
		public List<ProjDatMilestoneTriggerDetailBO> undoPoMapping (Integer woId);
		
		//EOA by Smrithi
	
	/*public List<ProjDatMilestoneTriggerDetailBO> findByFkMilestoneId(Integer milestoneId);

	
	@Query("select new com.thbs.mis.project.bean.ProjectPercentCalculationOutputBean"
			+ "(sum(trigger.triggerValue),sum(woBO.sowAmount),prjBo.pkProjectId) from "
			+ "ProjDatMilestoneTriggerDetailBO trigger "
			+ "inner join DatMilestoneBO milestoneBO on "
			+ "trigger.fkMilestoneId = milestoneBO.pkMilestoneId "
			+ "inner join DatComponentBO compoBo on milestoneBO.fkComponentId = compoBo.pkComponentId "
			+ "inner join DatWorkorderBO woBO on compoBo.fkWorkorderId = woBO.pkWorkorderId "
			+ "inner join MasProjectBO prjBo on woBO.fkProjectId = prjBo.pkProjectId")
	public List<ProjectPercentCalculationOutputBean> getTotalTriggeredValue();
	
	@Query(value = "SELECT SUM(proj.triggerValue) FROM ProjDatMilestoneTriggerDetailBO proj WHERE proj.fkMilestoneId = :milestoneId")
	public BigDecimal getSumOfTriggerValueByMilestone(@Param("milestoneId") int milestoneId);*/
	
	
	/*@Query("select new com.thbs.mis.project.bean.ProjectPercentCalculationOutputBean"
			+ "(sumOfTriggeredValue,sowAmount,fkProjectId) from "
			+ "(SELECT fkProjectId , sum(sowAmount) as sowAmount"
			+ "DatWorkorderBO woBO where woBO.pkWorkorderId in "
			+ "(select distinct(pkWorkorderId) from DatWorkorderBO "
			+ "left outer join DatComponentBO compoBo on pkWorkorderId = compoBo.fkWorkorderId "
			+ "left outer join DatMilestoneBO mileStoneBO on compoBo.pkComponentId = mileStoneBO.fkComponentId "
			+ "left outer join ProjDatMilestoneTriggerDetailBO trigger on milestoneBO.pkMilestoneId = trigger.fkMilestoneId"
			+ ") group by fkProjectId) as a "
			+ "join "
			+ "(select prj.pkProjectId, sum(mt.triggerValue) as sumOfTriggeredValue "
			+ "from ProjDatMilestoneTriggerDetailBO mt "
			+ "inner join DatMilestoneBO m "
			+ "on mt.fkMilestoneId=m.pkMilestoneId "
			+ "inner join DatComponentBO c on c.pkComponentId = m.fkComponentId "
			+ "inner join DatWorkorderBO wo on wo.pkWorkorderId = c.fkWorkorderId "
			+ "inner join MasProjectBO prj on prj.pkProjectId = wo.fkProjectId "
			+ "group by prj.pkProjectId) as b ON a.fkProjectId = b.pkProjectId")
	public List<ProjectPercentCalculationOutputBean> getTotalTriggeredValue();*/
	
}
