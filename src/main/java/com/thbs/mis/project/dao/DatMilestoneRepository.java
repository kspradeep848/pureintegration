/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  DatMilestoneRepository.java           			 */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  06-March-2018                                     */
/*                                                                   */
/*  Description :  About milestone details						     */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*    Date          Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 06-March-2018    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.project.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bean.CompNoMilestoneAndMilestoneSumBean;
import com.thbs.mis.project.bo.DatMilestoneBO;

@Repository
public interface DatMilestoneRepository extends GenericRepository<DatMilestoneBO, Integer> {

	public DatMilestoneBO findByPkMilestoneId(Integer milestoneId);

	public DatMilestoneBO findByMilestoneName(String milestoneName);

	public ArrayList<DatMilestoneBO> findAll(Specification<DatMilestoneBO> boSpec);

	@Query(value = "SELECT m FROM DatMilestoneBO m WHERE m.pkMilestoneId = :milestoneId")
	public List<DatMilestoneBO> getAllMilestoneDetailsById(@Param("milestoneId") int milestoneId);

	@Query(value = "select a from DatMilestoneBO a where a.fkComponentId = :fkComponentId")
	public List<DatMilestoneBO> findByFkComponentId(@Param("fkComponentId") int fkComponentId);

	public Page<DatMilestoneBO> findAll(Specification<DatMilestoneBO> boSpec, Pageable pageable);

	
	@Query(value = "select new com.thbs.mis.project.bean.CompNoMilestoneAndMilestoneSumBean("
			+ "count(m.pkMilestoneId),sum(m.milestoneValue)) from DatMilestoneBO"
			+ " m where m.fkComponentId =:compId")
	public CompNoMilestoneAndMilestoneSumBean getSumOfMilesStonesOnCompId(@Param("compId") int compId);
	
	
	@Query(value = "SELECT  sum(m.milestoneValue) FROM DatMilestoneBO m WHERE m.fkComponentId = :compId")
	public BigDecimal getSumOfMilestoneValue(@Param("compId") Integer compId);
	
	@Query(value = "select MIN(datMilestoneBO.fkMilestoneStatusId) from DatMilestoneBO datMilestoneBO where datMilestoneBO.fkComponentId = :fkComponentId")
	public Byte getStatusByComponentId(@Param("fkComponentId") int fkComponentId);
	
	@Query("SELECT rep FROM DatMilestoneBO rep Where rep.pkMilestoneId=:pkMilestoneId"+
			" AND rep.updatedComments Not LIKE :pattern% AND rep.updatedComments!=null")
	public DatMilestoneBO getByPkMilestoneIdAndPattern(@Param("pkMilestoneId") Integer pkMilestoneId,@Param("pattern") String  pattern);

	
}
