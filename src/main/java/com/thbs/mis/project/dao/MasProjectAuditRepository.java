package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.MasProjectAuditBO;

@Repository
public interface MasProjectAuditRepository extends
		GenericRepository<MasProjectAuditBO, Integer> {

	// public MasProjectAuditBO findBy

	@Query(nativeQuery = true, value = "SELECT m FROM mas_project_audit m where m.fk_client_id =:clientId "
			+ "order by m.entered_on desc limit 5")
	public List<MasProjectAuditBO> historyDetails(
			@Param("clientId") Short clientId);
	
	
	public List<MasProjectAuditBO> findByFkProjectId(Integer projectId);
	
	@Query("SELECT rep FROM MasProjectAuditBO rep Where rep.fkProjectId=:fkProjectId" +
			" AND rep.action = :action")
			public MasProjectAuditBO findByFkProjectId(@Param("fkProjectId") Integer fkProjectId,@Param("action") String action);
			
			@Query("SELECT rep FROM MasProjectAuditBO rep Where rep.fkProjectId=:fkProjectId"
					+ " AND rep.commentsForUpdation NOT LIKE :pattern%")
			public List<MasProjectAuditBO> findByFkProjectIdAndPattern(@Param("fkProjectId") Integer fkProjectId,
					@Param("pattern") String pattern, Pageable topThree);
			
			
			@Query("SELECT rep FROM MasProjectAuditBO rep Where rep.fkProjectId=:fkProjectId"
					+ " AND rep.projHistory!=null")
			public List<MasProjectAuditBO> getByFkProjectId(@Param("fkProjectId") Integer fkProjectId,
					 Pageable topThree);
			
			
			
}
