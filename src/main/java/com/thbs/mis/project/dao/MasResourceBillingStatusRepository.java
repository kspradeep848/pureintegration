package com.thbs.mis.project.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.MasResourceBillingStatusBO;


@Repository
public interface MasResourceBillingStatusRepository extends GenericRepository<MasResourceBillingStatusBO, Integer>
{

}
