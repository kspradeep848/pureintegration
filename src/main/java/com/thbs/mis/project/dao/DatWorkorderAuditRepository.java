package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.DatWorkorderAuditBO;

public interface DatWorkorderAuditRepository extends GenericRepository<DatWorkorderAuditBO, Integer> {
	
	@Query("SELECT rep FROM DatWorkorderAuditBO rep Where rep.fkWorkorderId=:fkWorkorderId" +
	" AND rep.action = :action")
	public DatWorkorderAuditBO findByFkWorkorderId(@Param("fkWorkorderId") Integer fkWorkorderId,@Param("action") String action);
	
	@Query("SELECT rep FROM DatWorkorderAuditBO rep Where rep.fkWorkorderId=:fkWorkorderId"
			+ " AND rep.commentForUpdate NOT LIKE :pattern%")
	public List<DatWorkorderAuditBO> findByFkWorkorderIdAndPattern(@Param("fkWorkorderId") Integer fkWorkorderId,
			@Param("pattern") String pattern, Pageable topThree);
	
	@Query("select bo from DatWorkorderAuditBO bo where bo.fkWorkorderId=:fkWorkorderId"
			+ " AND bo.woHistory!=null")
	public List<DatWorkorderAuditBO> getByWoId(@Param("fkWorkorderId") Integer fkWorkorderId, Pageable topThree);
}
