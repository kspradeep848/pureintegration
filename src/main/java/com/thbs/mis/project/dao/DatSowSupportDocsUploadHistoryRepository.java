package com.thbs.mis.project.dao;

import java.util.List;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.DatSowSupportDocsUploadHistoryBO;

public interface DatSowSupportDocsUploadHistoryRepository  extends GenericRepository<DatSowSupportDocsUploadHistoryBO, Integer>
{

	public List<DatSowSupportDocsUploadHistoryBO> findByFkWorkorderIdAndVersionId(Integer workorderId,Short versionId);
	
	public DatSowSupportDocsUploadHistoryBO findBySowSupportFileRefName(String sowSupportFileRefName);
}
