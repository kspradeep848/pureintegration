package com.thbs.mis.project.dao;

import java.math.BigDecimal;
import java.util.List;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjDatClientBudgetAtResourceLevelBO;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjDatClientBudgetAtResourceLevelRepository extends 
		GenericRepository<ProjDatClientBudgetAtResourceLevelBO,Long>
{
	
	public List<ProjDatClientBudgetAtResourceLevelBO> findByFkResourceDesignationId(Short designationId);


	public List<ProjDatClientBudgetAtResourceLevelBO> findByFkWorkorderId(Integer fkWorkorderId);

	//public List<ProjDatClientBudgetAtResourceLevelBO> findByFkResourceRoleId(Short resourceRoleId);
	

	@Query("select rec from ProjDatClientBudgetAtResourceLevelBO rec WHERE rec.fkResourceDesignationId =:designationId AND "
			+ "rec.fkWorkorderId =:fkWorkorderId")
	public ProjDatClientBudgetAtResourceLevelBO findByFkResourceDesignationIdandFkWorkorderId(@Param("designationId")Short designationId ,@Param("fkWorkorderId")Integer fkWorkorderId);
	
	//Added by prathibha for Update Client Budget
	ProjDatClientBudgetAtResourceLevelBO findByPkClientBudgetResourceId(Integer clientBudgetId);
	
	//Added by prathibha for Create/Update client budget
	@Query("Select SUM(rec.costToClient) from ProjDatClientBudgetAtResourceLevelBO rec WHERE rec.fkWorkorderId =:woId ")
	public BigDecimal getSumofCosttoClientbyWorkorder(@Param("woId") Integer woId);

	@Query("Select rec from ProjDatClientBudgetAtResourceLevelBO rec WHERE rec.fkWorkorderId =:woId ")
	public ProjDatClientBudgetAtResourceLevelBO getClientBudgetByWoId(@Param("woId") Integer woId);

	
}
