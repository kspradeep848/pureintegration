package com.thbs.mis.project.dao;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjBudgetStatusBO;

public interface ProjBudgetStatusRepository  extends
GenericRepository<ProjBudgetStatusBO, Integer> 
{

}
