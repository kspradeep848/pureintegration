package com.thbs.mis.project.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.MasClientAddressBO;


@Repository
public interface ProjClientAddressRepository extends GenericRepository<MasClientAddressBO,Integer> {
	
	//Added by Balaji 
	public MasClientAddressBO findByFkClientId(Short fkClientId);
	//EOA by Balaji
}
