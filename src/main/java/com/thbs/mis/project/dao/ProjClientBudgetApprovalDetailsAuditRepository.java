package com.thbs.mis.project.dao;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjClientBudgetApprovalDetailsAuditBO;

public interface ProjClientBudgetApprovalDetailsAuditRepository extends
GenericRepository<ProjClientBudgetApprovalDetailsAuditBO, Integer>  
{

}
