package com.thbs.mis.project.dao;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjDatClientBudgetForAdditionalCostBO;

public interface ProjDatClientBudgetForAdditionalCostRepository extends 
		GenericRepository<ProjDatClientBudgetForAdditionalCostBO, Integer>
{
	public ProjDatClientBudgetForAdditionalCostBO findByFkWorkorderId(Integer fkWorkorderId);
	
	
}
