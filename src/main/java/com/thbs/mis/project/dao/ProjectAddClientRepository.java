package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.MasClientBO;


@Repository
public interface ProjectAddClientRepository extends GenericRepository<MasClientBO, Integer>, JpaSpecificationExecutor<MasClientBO>{

	@Query("select e from MasClientBO e where e.clientStatus=:statusName")
	public List<MasClientBO> clientStatus(@org.springframework.data.repository.query.Param("statusName") String statusName);

	public MasClientBO findByPkClientId(Short pkClientId);
}
