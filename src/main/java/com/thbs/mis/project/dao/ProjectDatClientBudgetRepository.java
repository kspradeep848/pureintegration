package com.thbs.mis.project.dao;


import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bean.ClientBudgetDesignationOutputBean;
import com.thbs.mis.project.bo.ProjDatClientBudgetBO;

@Repository
public interface ProjectDatClientBudgetRepository extends GenericRepository<ProjDatClientBudgetBO, Long> {
	
	public ProjDatClientBudgetBO findByFkWorkOrderId(Integer woId);
	
	@Query("select p from ProjDatClientBudgetBO p where p.fkWorkOrderId =:woId")
	public List<ProjDatClientBudgetBO> getByFkWorkOrderId(@Param("woId") Integer woId);
	
	public ProjDatClientBudgetBO findByPkClientBudgetId(Integer pkClientBudgetId);
	
	public ProjDatClientBudgetBO findByFkWorkOrderIdAndFkResourceDesignationIdAndResourceLocation(Integer woId , short fkResourceDesignationId , String resourceLocation);
	
	public ProjDatClientBudgetBO findByPkClientBudgetIdAndFkWorkOrderIdAndFkResourceDesignationId
			(Integer pkClientBudgetId,Integer woId ,Integer fkResourceDesignationId);
	
	@Query("select new com.thbs.mis.project.bean.ClientBudgetDesignationOutputBean("
			+ "cliBudgetBo.pkClientBudgetId,desgBo.pkEmpDesignationId,"
			+ "desgBo.empDesignationName) "
			+ " from ProjDatClientBudgetBO cliBudgetBo "
			+ " inner join "
			+ " MasEmpDesignationBO desgBo on"
			+ " cliBudgetBo.fkResourceDesignationId = desgBo.pkEmpDesignationId "
			+ " where cliBudgetBo.fkWorkOrderId =:woId")
	public List<ClientBudgetDesignationOutputBean> getAllClientBugetDesignationListByWoId(@Param("woId") Integer woId);
	
	/*@Query("SELECT thbsLoc"
			+ " FROM  MasThbsBudgetLocationBO thbsLoc "
			+ " JOIN "
			+ " ProjDatBudgetLocationMappedParentLocationBO loc"
			+ " ON thbsLoc.pkThbsBudgetLocationId = loc.fkFinanceBudgetLocationId"
			+ " JOIN "
			+ " ProjDatClientBudgetBO bdg "
			+ " ON bdg.fkParentLocationId = loc.fkParentLocationId"
			+ " WHERE"
			+ " bdg.pkClientBudgetId =:clientBudgetId")
	public List<MasThbsBudgetLocationBO> getAllThbsBudgetLocationBasedOnClientBudgetId(@Param("clientBudgetId") Integer clientBudgetId);
		*/
	//Added by prathibha for update client budget
	
	@Query("Select p from ProjDatClientBudgetBO p where p.fkWorkOrderId=:woId AND p.fkResourceDesignationId=:desigId AND "
			+ "p.resourceLocation=:resourceLocation  " )
	public ProjDatClientBudgetBO getBudgetDetailsRecordByWorkOrderId
		(@Param("woId")Integer woId ,@Param("desigId")short desigId ,@Param("resourceLocation") String resourceLocation );
	
	//Added by prathibha for client budget
	@Query("SELECT SUM(cltBudDet.costToClient) FROM ProjDatClientBudgetBO cltBudDet WHERE cltBudDet.fkWorkOrderId =:workOrderId ")
	public BigDecimal getCostToClientTotal(@Param("workOrderId")Integer workOrderId);
	
	}
