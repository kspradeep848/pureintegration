package com.thbs.mis.project.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.DatResourceAllocationToComponentBO;

@Repository
public interface DatResourceAllocationToComponentRepository extends GenericRepository<DatResourceAllocationToComponentBO, Integer>
{

}
