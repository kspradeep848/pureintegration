package com.thbs.mis.project.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.project.bean.ViewProjectInputBean;
import com.thbs.mis.project.bo.MasProjectBO;

public class ProjectAdvanceSearchSpecification {
	public static Specification<MasProjectBO> viewProjectDetails(
			ViewProjectInputBean viewProjectInputBean) {
		return new Specification<MasProjectBO>() {
			@Override
			public Predicate toPredicate(Root<MasProjectBO> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				final Collection<Predicate> predicates = new ArrayList<>();
				Date fromDate = new Date();
				Date toDate = new Date();
				if (viewProjectInputBean.getFromDate() != null
						&& viewProjectInputBean.getToDate() != null) {
					SimpleDateFormat sdfWithSeconds = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					String stringFromDate = sdf.format(viewProjectInputBean
							.getFromDate());
					String stringToDate = sdf.format(viewProjectInputBean
							.getToDate());
					stringFromDate = stringFromDate + " 00:00:00";
					stringToDate = stringToDate + " 23:59:59";
					try {
						fromDate = sdfWithSeconds.parse(stringFromDate);
						toDate = sdfWithSeconds.parse(stringToDate);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				if (viewProjectInputBean.getClientId() != null) {
					final Predicate clientId = cb.equal(root.get("fkClientId"),
							viewProjectInputBean.getClientId());
					predicates.add(clientId);
				}
				if (viewProjectInputBean.getFromDate() != null
						&& viewProjectInputBean.getToDate() != null) {
					final Predicate dateRangePredicate = cb.or(cb.and(cb
							.greaterThanOrEqualTo(root.get("projectStartDate"),
									fromDate), cb.lessThanOrEqualTo(
							root.get("projectStartDate"), toDate)), cb.and(cb
							.greaterThanOrEqualTo(root.get("projectEndDate"),
									fromDate), cb.lessThanOrEqualTo(
							root.get("projectEndDate"), toDate)));
					predicates.add(dateRangePredicate);
				}
				if (viewProjectInputBean.getProjectId() != null) {
					final Predicate pkProjectId = cb.equal(
							root.get("pkProjectId"),
							viewProjectInputBean.getProjectId());
					predicates.add(pkProjectId);
				}
				if (viewProjectInputBean.getProjectStatusId() != null) {
					final Predicate statusId = cb.equal(
							root.get("projectStatusId"),
							viewProjectInputBean.getProjectStatusId());
					predicates.add(statusId);
				}
				if (viewProjectInputBean.getProjectType() != null) {
					final Predicate projectType = cb.equal(
							root.get("projectType"),
							viewProjectInputBean.getProjectType());
					predicates.add(projectType);
				}
				if (viewProjectInputBean.getRegionId() != null) {
					final Predicate region = cb.equal(root.get("fkRegionId"),
							viewProjectInputBean.getRegionId());
					predicates.add(region);
				}
				if (viewProjectInputBean.getDomainManagerId() != null) {
					final Predicate domainMgrId = cb.equal(
							root.get("fkDomainMgrId"),
							viewProjectInputBean.getDomainManagerId());
					predicates.add(domainMgrId);
				}
				return cb.and(predicates.toArray(new Predicate[predicates
						.size()]));
			}
		};
	}
}
