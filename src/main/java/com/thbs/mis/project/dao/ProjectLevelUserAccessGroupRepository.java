package com.thbs.mis.project.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.DatWorkorderBO;
import com.thbs.mis.project.bo.ProjectLevelUserAccessGroupBO;

@Repository
public interface ProjectLevelUserAccessGroupRepository extends
		GenericRepository<ProjectLevelUserAccessGroupBO, Integer> {

	// Added by Lalith kumar
	@Query(nativeQuery = true, value = "SELECT p.fk_project_coordinator_id FROM "
			+ "proj_dat_project_mapped_to_project_coordinator p "
			+ "inner join dat_workorder d on p.fk_project_id= d.fk_project_id"
			+ " where d.pk_workorder_id = :id "
			+ "union SELECT q.fk_user_id FROM project_level_user_access_group q"
			+ " inner join dat_workorder d on q.fk_project_id= d.fk_project_id"
			+ " where d.pk_workorder_id = :id ;")
	public List<Integer> getUserAccessByWorkOrderId(@Param("id") Integer id);

	// EOA by Lalith kumar

	@Query("SELECT prj.fkUserId from ProjectLevelUserAccessGroupBO prj where prj.fkProjectId = :projectId")
	public Set<Integer> getAllUserAccessByProjectId(@Param("projectId") Integer integer);

	public List<ProjectLevelUserAccessGroupBO> findAll();
	
	List<ProjectLevelUserAccessGroupBO> findByFkProjectId(int fkProjectId);
	
	@Query(nativeQuery = true, value = "SELECT p.fk_project_id FROM "
			+ " proj_dat_project_mapped_to_project_coordinator p "
			+ " where p.fk_project_coordinator_id =:empId "
			+ " union SELECT q.fk_project_id FROM project_level_user_access_group q"
			+ " where q.fk_user_id =:empId "
			+ " union "
			+ " select pk_project_id from mas_projects where fk_bu_unit_id =:buId")
	public Set<Integer> getProjectIdByEmpId(@Param("empId") Integer empId,@Param("buId") Short buId);
	
	@Query("select bo.fkUserId from ProjectLevelUserAccessGroupBO bo "
			+ " inner join DatWorkorderBO wo on wo.fkProjectId = bo.fkProjectId "
			+ " where wo.pkWorkorderId =:woId")
	public Set<Integer> getOnlyUserAccessListByWoId(@Param("woId") Integer woId);

}
