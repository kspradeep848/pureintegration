package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjThbsBudgetApprovalDetailBO;

public interface ProjThbsBudgetApprovalDetailRepository  extends
GenericRepository<ProjThbsBudgetApprovalDetailBO, Integer> 
{

	@Query("select count(bo) from ProjThbsBudgetApprovalDetailBO bo "
			+ " inner join "
			+ " ProjDatThbsBudgetBO thbs on thbs.pkThbsBudgetId = bo.fkThbsBudgetId"
			+ " where thbs.fkWorkorderId =:woId and bo.fkBudgetStatusId =:fkBudgetStatusId")
	public long getCountOfRejectThbsBudgetsByWoId(@Param("woId") Integer woId,@Param("fkBudgetStatusId") Integer fkBudgetStatusId);
	

	@Query("select bo from ProjThbsBudgetApprovalDetailBO bo "
			+ " inner join "
			+ " ProjDatThbsBudgetBO thbs on thbs.pkThbsBudgetId = bo.fkThbsBudgetId"
			+ " where thbs.fkWorkorderId =:woId and bo.fkBudgetStatusId =:fkBudgetStatusId")
	public List<ProjThbsBudgetApprovalDetailBO> getThbsBudgetsByWoId(@Param("woId") Integer woId,@Param("fkBudgetStatusId") Integer fkBudgetStatusId);
	
	@Query("select bo from ProjThbsBudgetApprovalDetailBO bo "
			+ " where bo.fkThbsBudgetId in (:thbsBudgetIdList)")
	public List<ProjThbsBudgetApprovalDetailBO> getAllThbsBudgetsByIdList(@Param("thbsBudgetIdList") List<Integer> thbsBudgetIdList);

	public ProjThbsBudgetApprovalDetailBO findByFkThbsBudgetId(Integer fkThbsBudgetId);
	
}
