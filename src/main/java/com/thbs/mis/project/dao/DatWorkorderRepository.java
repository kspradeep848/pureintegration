package com.thbs.mis.project.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bean.DatWorkorderBean;
import com.thbs.mis.project.bean.WoNumberOutputBean;
import com.thbs.mis.project.bo.DatWorkorderBO;

@Transactional
@Repository
public interface DatWorkorderRepository extends GenericRepository<DatWorkorderBO, Integer>
{
	public DatWorkorderBO findByPkWorkorderId(Integer woId);

	public List<DatWorkorderBO> findAll(Specification<DatWorkorderBO> datWorkorderBOObj);

	public List<DatWorkorderBO> findByFkProjectId(Integer projectId);

	public Page<DatWorkorderBO> findAll(Specification<DatWorkorderBO> datWorkorderBOObj, Pageable pageable);

	//Added By Rithesh
	@Modifying
	@Query(value="update DatWorkorderBO set fkWorkorderStatusId = :fkWorkorderStatusId where pkWorkorderId = :pkWorkorderId")
	public void updateWorkOrderStatus(@Param("pkWorkorderId") int pkWorkorderId, @Param("fkWorkorderStatusId") byte fkWorkorderStatusId);
	
	//EOA by Rithesh
	
	@Query("SELECT MAX(rep.pkWorkorderId) " + " FROM DatWorkorderBO  rep ")
	public int getMaxWorkorderId();

	@Query("Select new com.thbs.mis.project.bean.DatWorkorderBean(dat.pkWorkorderId,dat.workorderNumber) from DatWorkorderBO dat "
			+ "where dat.fkProjectId =:projectId")
	public List<DatWorkorderBean> getWoByProjectId(@Param("projectId") Integer projectId);

	@Query(value = "select MIN(datWorkorderBO.fkWorkorderStatusId) from DatWorkorderBO datWorkorderBO where datWorkorderBO.fkProjectId =:projectId")
	public Byte getStatusByProjectId(@Param("projectId") Integer projectId);
	
	//Added by Lalith kumar for getting all Wo numbers based on loggedIn
	@Query(name = "DatComponentBO.getWorkorderNumbers")
	public Set<WoNumberOutputBean> getAllWoNumbersBasedOnEmpId(@Param("empId") Integer empId,@Param("buId") Short buId);
	//EOA by Lalith kumar
	
	
	@Query("select new com.thbs.mis.project.bean.WoNumberOutputBean(woBo.pkWorkorderId,woBo.workorderNumber) from DatWorkorderBO woBo"
			+ " join MasProjectBO prj on woBo.fkProjectId=prj.pkProjectId where prj.fkBuUnitId =:buId")
	public Set<WoNumberOutputBean> getAllWoNumbersBasedOnBuId(@Param("buId") Short buId);

	/*@Query("SELECT SUM(proj.sowAmount) from DatWorkorderBO proj " + "+proj.pkWorkorderId in (:workOrderList)")
	public BigDecimal getSumOfSowAmount(@Param("workOrderList") List<Integer> workOrderList);*/
	
	@Query("SELECT rep FROM DatWorkorderBO rep Where rep.pkWorkorderId=:pkWorkorderId"+
			" AND rep.commentForUpdate Not LIKE :pattern% AND rep.commentForUpdate!=null")
	public DatWorkorderBO findByPkWorkorderIdAndPattern(@Param("pkWorkorderId") Integer pkWorkorderId,@Param("pattern") String  pattern);
}
