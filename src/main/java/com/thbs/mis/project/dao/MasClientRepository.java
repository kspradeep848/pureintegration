package com.thbs.mis.project.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bean.MasClientOutputBean;
import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bean.ClientDetailsOutputBean;
import com.thbs.mis.project.bo.MasClientBO;

@Repository
public interface MasClientRepository extends
		GenericRepository<MasClientBO, Integer>,
		JpaSpecificationExecutor<MasClientBO> {

	public MasClientBO findByPkClientId(Short clientId);

	@Query("SELECT c from MasClientBO c WHERE c.clientStatus = :statusName")
	public List<MasClientBO> getClient(@Param("statusName") String statusName);

	//Added by Balaji 
	public MasClientBO findByClientName(String clientName);
	//EOA by Balaji

	@Query("SELECT c from MasClientBO c order by c.clientStatus")
	public List<MasClientBO> getAllClientsByStatusId();
	
	@Query("Select client from MasClientBO client where lower(client.clientName) like lower(:clientName)")
	public MasClientBO getByClientName(@Param("clientName") String clientName);
	
	@Query("SELECT client FROM MasClientBO client Where client.pkClientId=:clientId"+
			" AND client.updatedComments Not LIKE :pattern% AND client.updatedComments!=null")
	public MasClientBO findByPkClientIdAndPattern(@Param("clientId") Short clientId, @Param("pattern") String pattern);

	@Query("SELECT new com.thbs.mis.project.bean.ClientDetailsOutputBean(client.pkClientId, client.clientName) "
			+ "FROM MasClientBO client inner join MasProjectBO project "
			+ "on client.pkClientId =  project.fkClientId "
			+ "where project.fkBuUnitId =:buId")
	public Set<ClientDetailsOutputBean> getClientDetailsBasedOnBuId(@Param("buId") short buId);
	
	@Query("SELECT new com.thbs.mis.common.bean.MasClientOutputBean(client.pkClientId, client.clientName, client.clientStatus) "
			+ "FROM MasClientBO client where client.fkRegionId =:regionId")
	public Set<MasClientOutputBean> getByFkRegionId(@Param("regionId") Short regionId);
	
}
