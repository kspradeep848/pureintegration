package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjDatThbsBudgetBO;

@Repository
public interface ProjDatThbsBudgetRepository extends GenericRepository<ProjDatThbsBudgetBO, Integer>
{

	public ProjDatThbsBudgetBO findByPkThbsBudgetId(Integer budgetId);
	
	public ProjDatThbsBudgetBO findByBudgetLocationAndFkClientBudgetIdAndFkWorkorderIdAndFkResourceLevelIdAndFkThbsBudgetDesignationIdAndFkThbsBudgetLocationId
	(String budgetLocation,Integer cliBudgetId,Integer fkWoId,Integer fkResourceLevelId,
			Integer fkThbsBudgetDesignationId,Integer fkThbsBudgetLocationId);
	
	@Query("select bo from ProjDatThbsBudgetBO bo where bo.fkClientBudgetId=:clientBudgetId")
	public List<ProjDatThbsBudgetBO> getAllThbsBudgetByClientBudgetId(@Param("clientBudgetId") Integer clientBudgetId);
	 
}
