package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjClientBudgetApprovalDetailBO;

@Repository
public interface ProjClientBudgetApprovalDetailRepository extends
GenericRepository<ProjClientBudgetApprovalDetailBO, Integer>  
{

	@Query("select count(bo) from ProjClientBudgetApprovalDetailBO bo "
			+ " inner join "
			+ " ProjDatClientBudgetBO cli on cli.pkClientBudgetId = bo.fkClientBudgetId"
			+ " where cli.fkWorkOrderId =:woId and bo.fkBudgetStatusId =:fkBudgetStatusId")
	public long getCountOfRejectBudgetsByWoId(@Param("woId") Integer woId,@Param("fkBudgetStatusId") Integer fkBudgetStatusId);
	
	@Query("select bo from ProjClientBudgetApprovalDetailBO bo "
			+ " inner join "
			+ " ProjDatClientBudgetBO cli on cli.pkClientBudgetId = bo.fkClientBudgetId"
			+ " where cli.fkWorkOrderId =:woId and bo.fkBudgetStatusId =:fkBudgetStatusId")
	public List<ProjClientBudgetApprovalDetailBO> getBudgetsByWoId(@Param("woId") Integer woId,@Param("fkBudgetStatusId") Integer fkBudgetStatusId);
	
	@Query("select bo from ProjClientBudgetApprovalDetailBO bo "
			+ " where bo.fkClientBudgetId in (:clientBudgetIdList)")
	public List<ProjClientBudgetApprovalDetailBO> getAllClientBudgetsByIdList(@Param("clientBudgetIdList") List<Integer> clientBudgetIdList);
	
}
