package com.thbs.mis.project.dao;


import java.math.BigDecimal;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjMasResourceStandardRateDetailBO;

@Repository
public interface StandardRateRepository  extends
GenericRepository<ProjMasResourceStandardRateDetailBO, Integer> 
{
	
	@Query(nativeQuery= true, value= "select employee_cost from proj_mas_resource_standard_rate_details"
			+ " where fk_employee_level_id =:fkEmployeeLevelId "
			+ " and fk_currency_id =:fkCurrencyId and "
			+ " fk_thbs_budget_location_id =:fkThbsBudgetLocationId "
			+ " and curdate() between valid_from and valid_to")
	public BigDecimal getStandardRate(@Param("fkEmployeeLevelId") Short fkEmployeeLevelId,@Param("fkCurrencyId") Short fkCurrencyId,@Param("fkThbsBudgetLocationId") Integer fkThbsBudgetLocationId);
}
