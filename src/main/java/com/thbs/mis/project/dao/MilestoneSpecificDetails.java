package com.thbs.mis.project.dao;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.project.bean.SearchMilestoneBean;
import com.thbs.mis.project.bo.DatMilestoneBO;

public class MilestoneSpecificDetails {
	public static Specification<DatMilestoneBO> getAllMilestoneDetails(SearchMilestoneBean inputBean) {
		return new Specification<DatMilestoneBO>() {
			public Predicate toPredicate(Root<DatMilestoneBO> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				final Collection<Predicate> predicates = new ArrayList<>();
				if (inputBean.getMilestoneStatus() != null) {
					final Predicate milestoneStatus = cb.equal(root.get("fkMilestoneStatusId"),
							inputBean.getMilestoneStatus().intValue());
					predicates.add(milestoneStatus);
				}
				if (inputBean.getComponentId() != null) {
					final Predicate componentId = cb.equal(root.get("fkComponentId"),
							inputBean.getComponentId().intValue());
					predicates.add(componentId);
				}
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			}
			
		};
	}
}