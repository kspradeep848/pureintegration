package com.thbs.mis.project.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjMasCompStatusBO;

@Repository
public interface ProjMasCompStatusRepository extends GenericRepository<ProjMasCompStatusBO, Integer>
{

	ProjMasCompStatusBO findByPkComponentStatusId(short fkComponentStatusId);

	ProjMasCompStatusBO findByComponentStatus(String componentStatus);

}

