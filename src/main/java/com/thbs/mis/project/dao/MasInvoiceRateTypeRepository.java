package com.thbs.mis.project.dao;

import java.util.List;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.MasInvoiceRateTypeBO;

public interface MasInvoiceRateTypeRepository extends GenericRepository<MasInvoiceRateTypeBO, Long> 
{
	List<MasInvoiceRateTypeBO> findAll();
	public MasInvoiceRateTypeBO findByPkInvoiceRateTypeId(byte rateId);
	
}
