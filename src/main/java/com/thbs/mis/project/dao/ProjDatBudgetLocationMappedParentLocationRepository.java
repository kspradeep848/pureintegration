package com.thbs.mis.project.dao;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjDatBudgetLocationMappedParentLocationBO;

public interface ProjDatBudgetLocationMappedParentLocationRepository 
extends
GenericRepository<ProjDatBudgetLocationMappedParentLocationBO, Integer>{

}
