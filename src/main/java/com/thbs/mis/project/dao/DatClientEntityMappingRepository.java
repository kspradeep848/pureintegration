
/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  DatClientEntityMappingRepository.java             */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  06-March-2018                                     */
/*                                                                   */
/*  Description :  Relationship between business unit and domains    */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*    Date          Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 06-March-2018    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.DatClientEntityMappingBO;

@Repository

public interface DatClientEntityMappingRepository extends GenericRepository<DatClientEntityMappingBO,Integer> {

	public List<DatClientEntityMappingBO> findByFkClientId(Short clientId);
	
	public DatClientEntityMappingBO findByFkClientIdAndFkEntityId(Short clientId, Byte fkEntityId);
	
	@Query("Select fkEntityId from DatClientEntityMappingBO where fkClientId =:clientId")
	public List<Byte> getByFkClientId(Short clientId);

}