package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.MasClientAuditBO;

public interface MasClientAuditRepository extends
		GenericRepository<MasClientAuditBO, Integer>,
		JpaSpecificationExecutor<MasClientAuditBO> {

	@Query("SELECT aud FROM MasClientAuditBO aud where aud.fkClientId=:clientId"
			+ " AND aud.clientHistory!=null")
	public List<MasClientAuditBO> getAllUpdatedByFkClientId(@Param("clientId") Short clientId, Pageable topThree);
	
	public MasClientAuditBO findByFkClientIdAndAction(Short clientId, String action);
}
