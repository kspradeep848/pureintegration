package com.thbs.mis.project.dao;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;

import com.thbs.mis.project.bo.ProjMasClientManagerBO;

@Repository
public interface ProjMasClientManagerRepository extends GenericRepository<ProjMasClientManagerBO, Integer>
{
	public ProjMasClientManagerBO findByPkClientManagerId(Short clientManagerId);
	
	@Query("SELECT rep" + " FROM ProjMasClientManagerBO  rep "
			+ "WHERE rep.emailAddress = :emailAddress")
	public ProjMasClientManagerBO getByEmailId(@Param("emailAddress") String emailAddress);
	
}
