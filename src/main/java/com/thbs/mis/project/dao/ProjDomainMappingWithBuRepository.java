package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjDomainMappingWithBuBO;

public interface ProjDomainMappingWithBuRepository extends GenericRepository<ProjDomainMappingWithBuBO, Integer> {

	@Query("SELECT new com.thbs.mis.project.bo.ProjDomainMappingWithBuBO(domainMap.fkDomainId,masDomain.domainName) "
			+ " FROM ProjDomainMappingWithBuBO domainMap "
			+ " JOIN MasDomainBO masDomain ON domainMap.fkDomainId = masDomain.pkDomainId "
			+ " WHERE domainMap.fkBuId = :buId ")
	List<ProjDomainMappingWithBuBO> getAllDomainDetailByBuId(@Param("buId") Short buId);

}
