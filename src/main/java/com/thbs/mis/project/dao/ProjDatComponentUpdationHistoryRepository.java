package com.thbs.mis.project.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjDatComponentUpdationHistoryBO;


@Repository
public interface ProjDatComponentUpdationHistoryRepository extends GenericRepository<ProjDatComponentUpdationHistoryBO, Integer>
{

}
