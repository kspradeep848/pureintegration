package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.MasClientAddressBO;


@Repository
public interface MasClientAddressRepository extends GenericRepository<MasClientAddressBO, Integer>, JpaSpecificationExecutor<MasClientAddressBO>
{
	public List<MasClientAddressBO> findByFkClientId(short fkClientId);
	
	// Added By Shashi for invoice PO
	public MasClientAddressBO findByPkClientAddressId(short pkClientAddressId);
	// EOA By Shashi for invoice PO
	
	public MasClientAddressBO findByFkClientIdAndIsBillToAddress(short clientId, String status);

}
