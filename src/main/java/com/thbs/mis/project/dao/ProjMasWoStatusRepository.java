package com.thbs.mis.project.dao;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjMasWoStatusBO;



@Repository
public interface ProjMasWoStatusRepository extends GenericRepository<ProjMasWoStatusBO, Integer>

{
	public ProjMasWoStatusBO findBypkWorkorderStatusId(byte workorderStatusId);
	
	@Query("SELECT d FROM ProjMasWoStatusBO d WHERE d.pkWorkorderStatusId= :workOrderStatusId")
	public List<ProjMasWoStatusBO> listOfWOStatusIds(@Param("workOrderStatusId") byte workOrderStatusId);

}
