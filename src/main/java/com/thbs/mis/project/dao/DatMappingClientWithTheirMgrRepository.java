package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.DatMappingClientWithTheirMgrBO;
@Repository
public interface DatMappingClientWithTheirMgrRepository  extends GenericRepository<DatMappingClientWithTheirMgrBO, Integer>
{
	public DatMappingClientWithTheirMgrBO findByFkClientManagerIdAndFkClientId(Short clientManagerId, Short fkClientId);
	
	public List<DatMappingClientWithTheirMgrBO> findByFkClientId(Short clientId);
}
