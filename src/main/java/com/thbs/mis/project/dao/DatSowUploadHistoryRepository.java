package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.DatSowUploadHistoryBO;

@Repository
public interface DatSowUploadHistoryRepository extends GenericRepository<DatSowUploadHistoryBO, Integer>
{
	@Query("SELECT DISTINCT rep.fkForwardRmId " + " FROM ExpDatReportBO rep "
			+ " WHERE rep.fkEmpIdInt =:fkEmpIdInt AND rep.fkStatusId IN :fkStatusId ")
	public List<Integer> groupBySkipLevelManager(@Param("fkEmpIdInt") Integer fkEmpIdInt,@Param("fkStatusId") short fkStatusId);
	
	public DatSowUploadHistoryBO findByfkWorkorderId(Integer workorderId);
	
	public List<DatSowUploadHistoryBO> findByFkWorkorderId(Integer workorderId);
	
	@Query("SELECT MAX(rep.versionId) " + " FROM DatSowUploadHistoryBO  rep "
			+ "WHERE rep.fkWorkorderId = :fkWorkorderId")
	public short getbyWorkorderId(@Param("fkWorkorderId") Integer fkWorkorderId);
	
	/*@Query(nativeQuery=true,value="SELECT r FROM dat_sow_upload_history r "
			+ "WHERE r.fk_workorder_id =:fkWorkorderId ORDER BY r.created_on desc")*/
	/*@Query(Select )
	public List<DatSowUploadHistoryBO> sowDetails(@Param("fkWorkorderId") Integer fkWorkorderId);*/

	public List<DatSowUploadHistoryBO> findByFkWorkorderIdOrderByCreatedOnDesc(Integer workorderId);
	
	public DatSowUploadHistoryBO findByFkWorkorderIdAndVersionId(Integer woId, Short version);
}
