/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  DatComponentRepository.java 			             */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  06-March-2018                                     */
/*                                                                   */
/*  Description :  About component details						     */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*    Date          Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 06-March-2018    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.project.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.DatComponentBO;

@Repository
public interface DatComponentRepository extends GenericRepository<DatComponentBO, Integer> {

	public DatComponentBO findByPkComponentId(int compId);
	
	@Query(value = "select a from DatComponentBO a where a.pkComponentId = :compId")
	public DatComponentBO getcompIdDetails(@Param("compId") int compId);

	@Query("SELECT d FROM DatComponentBO d where fkWorkorderId in "
			+ "(SELECT pkWorkorderId FROM DatWorkorderBO where fkProjectId = :projectId)")
	public List<DatComponentBO> numberOfComponents(@Param("projectId") Integer projectId);

	public List<DatComponentBO> findByFkWorkorderId(Integer projectId);

	@Query(value = "select a from DatComponentBO a where a.fkWorkorderId = :fkWorkorderId")
	List<DatComponentBO> getWorkOrderDetails(@Param("fkWorkorderId") int fkWorkorderId);

	//Added by Lalith kumar for getting sum of component amount
	@Query(value = "select sum(c.componentAmount) from DatComponentBO c where c.fkWorkorderId = :fkWorkorderId")
	public BigDecimal getSumOfComponentAmount(@Param("fkWorkorderId") Integer fkWorkorderId);
	//EOA by Lalith kumar
	
	public DatComponentBO findByFkWorkorderIdAndComponentName(Integer fkWorkorderId,String componentName);

	@Query(value = "select MIN(datComponentBO.fkComponentStatusId) from DatComponentBO datComponentBO where datComponentBO.fkWorkorderId = :fkWorkorderId")
	public Byte getStatusByWorkOrderId(@Param("fkWorkorderId") int fkWorkorderId);

	public Page<DatComponentBO> findAll(Specification<DatComponentBO> datComp, Pageable pageable);
	
	@Query("SELECT rep FROM DatComponentBO rep Where rep.pkComponentId=:pkComponentId"+
			" AND rep.commentsForUpdation Not LIKE :pattern% AND rep.commentsForUpdation!=null")
	public DatComponentBO findByPkComponentIdAndPattern(@Param("pkComponentId") Integer pkComponentId,@Param("pattern") String  pattern);


}
