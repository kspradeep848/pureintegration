package com.thbs.mis.project.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bean.GlobalRegionsAndProjectsBean;
import com.thbs.mis.project.bean.ProjectNamesOutputBean;
import com.thbs.mis.project.bean.ProjectPercentCalculationOutputBean;
import com.thbs.mis.project.bean.ProjectOverviewBean;
import com.thbs.mis.project.bean.ProjectStatusBean;
import com.thbs.mis.project.bo.MasProjectBO;

@Repository
public interface MasProjectRepository extends
		GenericRepository<MasProjectBO, Integer>,
		JpaSpecificationExecutor<MasProjectBO> {

	public List<MasProjectBO> findAll();

	public List<MasProjectBO> findByFkClientId(Short clientId);
	
	public MasProjectBO findByPkProjectId(Integer projectId);	
	
	//Added by Lalith kumar for getting all project name based on loggedIn user
	@Query(name = "MasProjectBO.getProjectNames")
	public Set<ProjectNamesOutputBean> getAllProjectBasedOnEmpId(@Param("empId") Integer empId);
	
	@Query("select new com.thbs.mis.project.bean.ProjectNamesOutputBean(prj.pkProjectId,"
			+ "prj.projectName) from MasProjectBO prj where prj.fkBuUnitId =:fkBuUnitId")
	public Set<ProjectNamesOutputBean> getAllProjectBasedOnBuId(@Param("fkBuUnitId") Short fkBuUnitId);
	//EOA by Lalith kumar
	
	// Added By Rinta
	@Query("select new com.thbs.mis.project.bean.GlobalRegionsAndProjectsBean("
			+ " cli.fkCountryId,country.countryName,count(prj.pkProjectId))"
			+ " from MasProjectBO prj inner join MasClientAddressBO cli"
			+ " on prj.fkClientId = cli.fkClientId"
			+ " inner join MasCountryBO country  on"
			+ " country.pkCountryId = cli.fkCountryId"
			+ " where prj.pkProjectId in (:prjList)"
			+ " group by cli.fkCountryId")
	public List<GlobalRegionsAndProjectsBean> getNoOfProjectOnCountry(
			@Param("prjList") Set<Integer> prjList);

	// EOA By Rinta

	// Added By Rinta
	@Query("select new com.thbs.mis.project.bean.ProjectOverviewBean("
			+ "count(distinct prj.pkProjectId),count(distinct wrk.pkWorkorderId),"
			+ "count(distinct cmp.pkComponentId),count(mile.pkMilestoneId)) "
			+ "FROM MasProjectBO prj left join DatWorkorderBO wrk "
			+ "on prj.pkProjectId = wrk.fkProjectId "
			+ "left join DatComponentBO cmp on wrk.pkWorkorderId = cmp.fkWorkorderId "
			+ "left join DatMilestoneBO mile on cmp.pkComponentId = mile.fkComponentId "
			+ "where prj.pkProjectId in (:prjList)")
	public ProjectOverviewBean getProjectOverview(
			@Param("prjList") Set<Integer> prjList);
	

	@Query("select new com.thbs.mis.project.bean.ProjectStatusBean("
			+ "prj.pkProjectId,prj.projectName,prj.projectStatusId,status.projectStatusName) "
			+ "from MasProjectBO prj join ProjMasProjectStatusBO status "
			+ "on prj.projectStatusId = status.pkProjectStatusId "
			+ "where prj.pkProjectId in (:prjList)")
	public List<ProjectStatusBean> getProjectStatus(
			@Param("prjList") Set<Integer> prjList);
	// EOA By Rinta

	//Added by Lalith kumar
	/*@Query("select new com.thbs.mis.project.bean.ProjectPercentCalculationOutputBean(c.sumOfTriggeredValue,c.sowamount,c.fkProjectId) from"
			+ " (select "
			+ " b.sumOfTriggeredValue,a.sowamount,a.fkProjectId"
			+ " from (SELECT fkProjectId , sum(sowAmount) as sowamount FROM DatWorkorderBO"
			+ " where pkWorkorderId in"
			+ " (select distinct pkWorkorderId from DatWorkorderBO datwo"
			+ " left outer join DatComponentBO on datwo.pkWorkorderId = fkWorkorderId"
			+ " left outer join DatMilestoneBO on pkComponentId = fkComponentId"
			+ " left outer join ProjDatMilestoneTriggerDetailBO on "
			+ " pkMilestoneId = fkMilestoneId"
			+ " where datwo.fkProjectId =:prjId) group by fkProjectId)"
			+ "  as a"
			+ " join "
			+ " (select prj.pkProjectId as pkProjectId, sum(mt.triggerValue) as sumOfTriggeredValue"
			+ " from ProjDatMilestoneTriggerDetailBO mt"
			+ " inner join DatMilestoneBO m"
			+ " on mt.fkMilestoneId=m.pkMilestoneId"
			+ " inner join DatComponentBO c on c.pkComponentId = m.fkComponentId"
			+ " inner join DatWorkorderBO wo on wo.pkWorkorderId = c.fkWorkorderId"
			+ " inner join MasProjectBO prj on prj.pkProjectId = wo.fkProjectId"
			+ " where prj.pkProjectId =:prjId) as b "
			+ "ON a.fkProjectId = b.pkProjectId) as c")
	public ProjectPercentCalculationOutputBean getProjectTriggeredAndProjectValue(@Param("prjId") Integer prjId);
	*/
	

	@Query(name = "MasProjectBO.getProjectPercentage")
	public ProjectPercentCalculationOutputBean getProjectTriggeredAndProjectValue(@Param("prjId") Integer prjId);
	
	@Query("SELECT rep FROM MasProjectBO rep Where rep.pkProjectId=:pkProjectId"+
			" AND rep.commentsForUpdation Not LIKE :pattern% AND rep.commentsForUpdation!=null")
	public MasProjectBO findByPkProjectIdAndPattern(@Param("pkProjectId") Integer pkProjectId,@Param("pattern") String  pattern);

	//EOA by Lalith kumar
	
	//Added by Lalith kumar
	
	@Query("select bo from MasProjectBO bo inner join DatWorkorderBO wo on"
			+ " wo.fkProjectId = bo.pkProjectId where wo.pkWorkorderId =:woId")
	public MasProjectBO getProjectByWoId(@Param("woId") Integer woId);
	
	//EOA by Lalith kumar
	
	//Added by Balaji
	public List<MasProjectBO> findByFkBuUnitId(Short fkBuUnitId);
	//EOA by Balaji
	
}
