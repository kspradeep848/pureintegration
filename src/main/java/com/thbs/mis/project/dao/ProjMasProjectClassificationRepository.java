package com.thbs.mis.project.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjMasProjectClassificationBO;

@Repository
public interface ProjMasProjectClassificationRepository extends
		GenericRepository<ProjMasProjectClassificationBO, Integer> {
	
	public ProjMasProjectClassificationBO findByPkClassificationId(
			Byte classificationId);
}
