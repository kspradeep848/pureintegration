package com.thbs.mis.project.dao;



import java.util.List;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.MasRateTypeBO;


public interface MasRateTypeRepository extends 
GenericRepository<MasRateTypeBO,Long>
{
	List<MasRateTypeBO> findAll();
	
	public MasRateTypeBO findByPkRateTypeId(byte rateId);
	
}
