package com.thbs.mis.project.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjBudgetRejectNotRequiredCommentsDetailBO;

@Repository
public interface ProjBudgetRejectNotRequiredCommentsDetailRepository
extends
GenericRepository<ProjBudgetRejectNotRequiredCommentsDetailBO, Integer>
{

}
