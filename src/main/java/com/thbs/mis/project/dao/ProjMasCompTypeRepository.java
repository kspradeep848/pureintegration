package com.thbs.mis.project.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjMasCompTypeBO;

@Repository
public interface ProjMasCompTypeRepository extends GenericRepository<ProjMasCompTypeBO, Integer>
{

	ProjMasCompTypeBO findByPkComponentTypeId(byte fkComponentTypeId);

	ProjMasCompTypeBO findByComponentTypeName(String componentType);

}
