package com.thbs.mis.project.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.ProjDatClientBudgetRateDetailBO;

@Repository
public interface ProjDatClientBudgetRateDetailsRepository extends
		GenericRepository<ProjDatClientBudgetRateDetailBO, Integer> {

	public List<ProjDatClientBudgetRateDetailBO> findByFkClientBudgetId(
			Integer fkClientBudgetId);
	
	public ProjDatClientBudgetRateDetailBO findByPkClientBudgetRateDetailsId(Integer rateDetId  );
	
	@Query("SELECT cbrdet from ProjDatClientBudgetRateDetailBO cbrdet WHERE cbrdet.fkClientBudgetId=:fkClientBudgetId")
	public ProjDatClientBudgetRateDetailBO getRecordByFkClientId(@Param("fkClientBudgetId")Integer fkClientBudgetId);
	
	
	@Query("Select cbrDet from ProjDatClientBudgetRateDetailBO cbrDet where  "
			+ "cbrDet.fkClientBudgetId=:budgetId AND cbrDet.fkInvoiceRateTypeId=:invoiceRateId AND "
			+ "cbrDet.fkRateTypeId=:rateType AND cbrDet.rateAmount=:amount AND "
			+ "cbrDet.holidayFlag=:holidayFlag AND cbrDet.weeklyOffFlag=:weeklyOffFlag  " )
	public ProjDatClientBudgetRateDetailBO getCleintBudgetRateDetailByDetails 
	  (@Param("budgetId")Integer budgetId , @Param("invoiceRateId") byte invoiceRateId ,
			  @Param("rateType")byte rateType , @Param("amount")BigDecimal amount,
			  @Param("holidayFlag")String holidayFlag , @Param("weeklyOffFlag") String weeklyOffFlag );
	
	public ProjDatClientBudgetRateDetailBO findByFkClientBudgetIdAndWeeklyOffFlagAndHolidayFlag
					(Integer fkClientBudgetId , String weeklyOffFlag , String HolidayFlag );
}