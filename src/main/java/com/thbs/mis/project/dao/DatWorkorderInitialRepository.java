package com.thbs.mis.project.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.project.bo.DatWorkorderInitialBO;

@Repository
public interface DatWorkorderInitialRepository extends GenericRepository<DatWorkorderInitialBO, Integer> 
{

	public DatWorkorderInitialBO findByWoInitialAndFkProjectId(String woInitial,Integer fkProjectId);

	// Added By Rinta
	@Query(" select distinct woInitial from DatWorkorderInitialBO ")
	public List<String> getAllWoIntials();
	// EOA By Rinta
}
