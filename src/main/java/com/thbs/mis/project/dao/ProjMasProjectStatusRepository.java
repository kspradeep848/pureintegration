package com.thbs.mis.project.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.thbs.mis.project.bo.ProjMasProjectStatusBO;

@Repository
public interface ProjMasProjectStatusRepository extends CrudRepository<ProjMasProjectStatusBO, Long>
{

	ProjMasProjectStatusBO findByPkProjectStatusId(Byte status);
}

