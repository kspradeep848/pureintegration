package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.MasBusinessEntityBO;


/**
 * The persistent class for the dat_client_entity_mapping database table.
 * 
 */
@Entity
@Table(name="dat_client_entity_mapping")
@NamedQuery(name="DatClientEntityMappingBO.findAll", query="SELECT d FROM DatClientEntityMappingBO d")
public class DatClientEntityMappingBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_client_entity_mapping_id")
	private Integer pkClientEntityMappingId;
	
	@Column(name="fk_client_id")
	private Short fkClientId;
	
	@Column(name="fk_entity_id")
	private Byte fkEntityId;

	//bi-directional many-to-one association to MasClient
	@OneToOne
	@JoinColumn(name="fk_client_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasClientBO masClient;

	//bi-directional many-to-one association to MasBusinessEntity
	@OneToOne
	@JoinColumn(name="fk_entity_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasBusinessEntityBO masBusinessEntity;

	public DatClientEntityMappingBO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getPkClientEntityMappingId() {
		return this.pkClientEntityMappingId;
	}

	public void setPkClientEntityMappingId(Integer pkClientEntityMappingId) {
		this.pkClientEntityMappingId = pkClientEntityMappingId;
	}

	public MasClientBO getMasClient() {
		return this.masClient;
	}

	public void setMasClient(MasClientBO masClient) {
		this.masClient = masClient;
	}

	public MasBusinessEntityBO getMasBusinessEntity() {
		return this.masBusinessEntity;
	}

	public void setMasBusinessEntity(MasBusinessEntityBO masBusinessEntity) {
		this.masBusinessEntity = masBusinessEntity;
	}

	public Short getFkClientId() {
		return fkClientId;
	}

	public void setFkClientId(Short byte1) {
		this.fkClientId = byte1;
	}

	public Byte getFkEntityId() {
		return fkEntityId;
	}

	public void setFkEntityId(Byte fkEntityId) {
		this.fkEntityId = fkEntityId;
	}

	@Override
	public String toString() {
		return "DatClientEntityMappingBO [pkClientEntityMappingId="
				+ pkClientEntityMappingId + ", fkClientId=" + fkClientId
				+ ", fkEntityId=" + fkEntityId + ", masClient=" + masClient
				+ ", masBusinessEntity=" + masBusinessEntity + "]";
	}

	

	
}