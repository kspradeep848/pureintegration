package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the proj_mas_comp_status database table.
 * 
 */
@Entity
@Table(name="proj_mas_comp_status")
@NamedQuery(name="ProjMasCompStatusBO.findAll", query="SELECT p FROM ProjMasCompStatusBO p")
public class ProjMasCompStatusBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_component_status_id")
	private short pkComponentStatusId;

	@Column(name="component_status")
	private String componentStatus;
	
	//bi-directional many-to-one association to DatComponentAudit
	/*@OneToMany(mappedBy="datComponentAuditComponentStatusId")
	private List<DatComponentAuditBO> datComponentAuditComponentStatusIdList;*/

	public ProjMasCompStatusBO() {
	}

	public short getPkComponentStatusId() {
		return this.pkComponentStatusId;
	}

	public void setPkComponentStatusId(short pkComponentStatusId) {
		this.pkComponentStatusId = pkComponentStatusId;
	}

	public String getComponentStatus() {
		return this.componentStatus;
	}

	public void setComponentStatus(String componentStatus) {
		this.componentStatus = componentStatus;
	}

	@Override
	public String toString() {
		return "ProjMasCompStatusBO [pkComponentStatusId="
				+ pkComponentStatusId + ", componentStatus=" + componentStatus
				+ "]";
	}

	

}