package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.bo.MasDomainBO;

/**
 * The persistent class for the proj_domain_mapping_with_bu database table.
 * 
 */
@Entity
@Table(name = "proj_domain_mapping_with_bu")
@NamedQuery(name = "ProjDomainMappingWithBuBO.findAll", query = "SELECT p FROM ProjDomainMappingWithBuBO p")
public class ProjDomainMappingWithBuBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_mapping_id")
	private int pkMappingId;

	@Column(name = "fk_bu_id")
	private short fkBuId;

	@Column(name = "fk_domain_id")
	private short fkDomainId;

	// bi-directional many-to-one association to MasBuUnit
	@OneToOne
	@JoinColumn(name = "fk_bu_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasBuUnitBO masBuUnit;

	// bi-directional many-to-one association to MasDomain
	@OneToOne
	@JoinColumn(name = "fk_domain_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasDomainBO masDomain;

	@Transient
	private String domainName;

	@Transient
	private Short domainId;

	public ProjDomainMappingWithBuBO() {
	}

	public ProjDomainMappingWithBuBO(short fkDomainId, String domainName) {
		super();
		this.domainName = domainName;
		this.fkDomainId = fkDomainId;
	}

	public String getDomainName() {
		return domainName;
	}

	public Short getDomainId() {
		return domainId;
	}

	public void setDomainId(Short domainId) {
		this.domainId = domainId;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public int getPkMappingId() {
		return this.pkMappingId;
	}

	public void setPkMappingId(int pkMappingId) {
		this.pkMappingId = pkMappingId;
	}

	public short getFkBuId() {
		return fkBuId;
	}

	public void setFkBuId(short fkBuId) {
		this.fkBuId = fkBuId;
	}

	public short getFkDomainId() {
		return fkDomainId;
	}

	public void setFkDomainId(short fkDomainId) {
		this.fkDomainId = fkDomainId;
	}

	public MasBuUnitBO getMasBuUnit() {
		return masBuUnit;
	}

	public void setMasBuUnit(MasBuUnitBO masBuUnit) {
		this.masBuUnit = masBuUnit;
	}

	public MasDomainBO getMasDomain() {
		return masDomain;
	}

	public void setMasDomain(MasDomainBO masDomain) {
		this.masDomain = masDomain;
	}

	@Override
	public String toString() {
		return "ProjDomainMappingWithBuBO [pkMappingId=" + pkMappingId + ", fkBuId=" + fkBuId + ", fkDomainId="
				+ fkDomainId + ", masBuUnit=" + masBuUnit + ", masDomain=" + masDomain + ", domainName=" + domainName
				+ ", domainId=" + domainId + "]";
	}

}