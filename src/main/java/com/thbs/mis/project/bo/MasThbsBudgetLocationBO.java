package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the mas_thbs_budget_location database table.
 * 
 */
@Entity
@Table(name="mas_thbs_budget_location")
@NamedQuery(name="MasThbsBudgetLocationBO.findAll", query="SELECT m FROM MasThbsBudgetLocationBO m")
public class MasThbsBudgetLocationBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="pk_thbs_budget_location_id")
	private Integer pkThbsBudgetLocationId;

	@Column(name="budget_location_name")
	private String budgetLocationName;

	@Column(name="onsite_offshore_flag")
	private String onsiteOffshoreFlag;

	public MasThbsBudgetLocationBO() {
	}

	public Integer getPkThbsBudgetLocationId() {
		return this.pkThbsBudgetLocationId;
	}

	public void setPkThbsBudgetLocationId(Integer pkThbsBudgetLocationId) {
		this.pkThbsBudgetLocationId = pkThbsBudgetLocationId;
	}

	public String getBudgetLocationName() {
		return this.budgetLocationName;
	}

	public void setBudgetLocationName(String budgetLocationName) {
		this.budgetLocationName = budgetLocationName;
	}

	public String getOnsiteOffshoreFlag() {
		return this.onsiteOffshoreFlag;
	}

	public void setOnsiteOffshoreFlag(String onsiteOffshoreFlag) {
		this.onsiteOffshoreFlag = onsiteOffshoreFlag;
	}

}