package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the proj_dat_thbs_budget_for_additional_cost database table.
 * 
 */
@Entity
@Table(name="proj_dat_thbs_budget_for_additional_cost")
@NamedQuery(name="ProjDatThbsBudgetForAdditionalCostBO.findAll", query="SELECT p FROM ProjDatThbsBudgetForAdditionalCostBO p")
public class ProjDatThbsBudgetForAdditionalCostBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_thbs_budget_additional_cost_id")
	private Integer pkThbsBudgetAdditionalCostId;

	@Column(name="comments_for_updation")
	private String commentsForUpdation;

	@Column(name="created_by")
	private Integer createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="expense_amt")
	private BigDecimal expenseAmt;

	@Column(name="fk_workorder_id")
	private Integer fkWorkorderId;

	@Column(name="purchase_amt")
	private BigDecimal purchaseAmt;

	@Column(name="travel_amt")
	private BigDecimal travelAmt;

	@Column(name="updated_by")
	private Integer updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;

	public ProjDatThbsBudgetForAdditionalCostBO() {
	}

	@OneToOne
	@JoinColumn(name="fk_workorder_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datWorkorder;

	public Integer getPkThbsBudgetAdditionalCostId() {
		return pkThbsBudgetAdditionalCostId;
	}

	public void setPkThbsBudgetAdditionalCostId(Integer pkThbsBudgetAdditionalCostId) {
		this.pkThbsBudgetAdditionalCostId = pkThbsBudgetAdditionalCostId;
	}

	public String getCommentsForUpdation() {
		return commentsForUpdation;
	}

	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public BigDecimal getExpenseAmt() {
		return expenseAmt;
	}

	public void setExpenseAmt(BigDecimal expenseAmt) {
		this.expenseAmt = expenseAmt;
	}

	public Integer getFkWorkorderId() {
		return fkWorkorderId;
	}

	public void setFkWorkorderId(Integer fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}

	public BigDecimal getPurchaseAmt() {
		return purchaseAmt;
	}

	public void setPurchaseAmt(BigDecimal purchaseAmt) {
		this.purchaseAmt = purchaseAmt;
	}

	public BigDecimal getTravelAmt() {
		return travelAmt;
	}

	public void setTravelAmt(BigDecimal travelAmt) {
		this.travelAmt = travelAmt;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public DatWorkorderBO getDatWorkorder() {
		return datWorkorder;
	}

	public void setDatWorkorder(DatWorkorderBO datWorkorder) {
		this.datWorkorder = datWorkorder;
	}

	@Override
	public String toString() {
		return "ProjDatThbsBudgetForAdditionalCostBO [pkThbsBudgetAdditionalCostId="
				+ pkThbsBudgetAdditionalCostId
				+ ", commentsForUpdation="
				+ commentsForUpdation
				+ ", createdBy="
				+ createdBy
				+ ", createdOn="
				+ createdOn
				+ ", expenseAmt="
				+ expenseAmt
				+ ", fkWorkorderId="
				+ fkWorkorderId
				+ ", purchaseAmt="
				+ purchaseAmt
				+ ", travelAmt="
				+ travelAmt
				+ ", updatedBy="
				+ updatedBy
				+ ", updatedOn="
				+ updatedOn
				+ ", datWorkorder="
				+ datWorkorder + "]";
	}

	

}