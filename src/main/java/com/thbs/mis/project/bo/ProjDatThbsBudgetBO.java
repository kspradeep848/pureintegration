package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.MasEmpDesignationBO;
import com.thbs.mis.common.bo.MasEmpLevelBO;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the proj_dat_thbs_budget database table.
 * 
 */
@Entity
@Table(name="proj_dat_thbs_budget")
@NamedQuery(name="ProjDatThbsBudgetBO.findAll", query="SELECT p FROM ProjDatThbsBudgetBO p")
public class ProjDatThbsBudgetBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="pk_thbs_budget_id")
	private Integer pkThbsBudgetId;

	@Column(name="budget_location")
	private String budgetLocation;

	@Column(name="budgeted_day_remaining")
	private Integer budgetedDayRemaining;

	@Column(name="budgeted_days")
	private Integer budgetedDays;

	@Column(name="comments_for_updation")
	private String commentsForUpdation;

	@Column(name="cost_to_company")
	private BigDecimal costToCompany;

	@Column(name="created_by")
	private Integer createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="fk_client_budget_id")
	private Integer fkClientBudgetId;

	@Column(name="fk_thbs_budget_location_id")
	private Integer fkThbsBudgetLocationId;

	@Column(name="fk_workorder_id")
	private Integer fkWorkorderId;

	@Column(name="no_of_resource")
	private Integer noOfResource;

	@Column(name="no_of_resource_remaining")
	private Integer noOfResourceRemaining;

	@Column(name="updated_by")
	private Integer updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;
	
	@Column(name="fk_thbs_budget_designation_id")
	private Integer fkThbsBudgetDesignationId;
	
	@Column(name="fk_resource_level_id")
	private Integer fkResourceLevelId;
	
	
	@Column(name="fk_resource_standard_rate_id")
	private Integer fkResourceStandardRateId;

	

	//bi-directional one-to-one association to MasEmpDesignation
	@OneToOne
	@JoinColumn(name="fk_thbs_budget_designation_id" ,unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpDesignationBO masEmpDesignationBO;

	//bi-directional one-to-one association to MasEmpLevel
	@OneToOne
	@JoinColumn(name="fk_resource_level_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpLevelBO masEmpLevelBO;
	
	//bi-directional one-to-one association to MasEmpLevel
	@OneToOne
	@JoinColumn(name="fk_client_budget_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjDatClientBudgetBO projDatClientBudgetBO;
	
	//bi-directional one-to-one association to MasEmpLevel
	@OneToOne
	@JoinColumn(name="fk_workorder_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datWorkorderBO;
	
	//bi-directional one-to-one association to MasEmpLevel
	@OneToOne
	@JoinColumn(name="fk_thbs_budget_location_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasThbsBudgetLocationBO masThbsBudgetLocationBO;		
	
	
	public ProjDatThbsBudgetBO() {
	}
	

	public Integer getPkThbsBudgetId() {
		return this.pkThbsBudgetId;
	}

	public void setPkThbsBudgetId(Integer pkThbsBudgetId) {
		this.pkThbsBudgetId = pkThbsBudgetId;
	}

	public String getBudgetLocation() {
		return this.budgetLocation;
	}

	public void setBudgetLocation(String budgetLocation) {
		this.budgetLocation = budgetLocation;
	}

	public Integer getBudgetedDayRemaining() {
		return this.budgetedDayRemaining;
	}

	public void setBudgetedDayRemaining(Integer budgetedDayRemaining) {
		this.budgetedDayRemaining = budgetedDayRemaining;
	}

	public Integer getBudgetedDays() {
		return this.budgetedDays;
	}

	public void setBudgetedDays(Integer budgetedDays) {
		this.budgetedDays = budgetedDays;
	}

	public String getCommentsForUpdation() {
		return this.commentsForUpdation;
	}

	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}

	public BigDecimal getCostToCompany() {
		return this.costToCompany;
	}

	public void setCostToCompany(BigDecimal costToCompany) {
		this.costToCompany = costToCompany;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getFkResourceStandardRateId() {
		return fkResourceStandardRateId;
	}

	public void setFkResourceStandardRateId(Integer fkResourceStandardRateId) {
		this.fkResourceStandardRateId = fkResourceStandardRateId;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getFkClientBudgetId() {
		return this.fkClientBudgetId;
	}

	public void setFkClientBudgetId(Integer fkClientBudgetId) {
		this.fkClientBudgetId = fkClientBudgetId;
	}

	public int getFkThbsBudgetLocationId() {
		return this.fkThbsBudgetLocationId;
	}

	public void setFkThbsBudgetLocationId(Integer fkThbsBudgetLocationId) {
		this.fkThbsBudgetLocationId = fkThbsBudgetLocationId;
	}

	public Integer getFkWorkorderId() {
		return this.fkWorkorderId;
	}

	public void setFkWorkorderId(Integer fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}

	public Integer getNoOfResource() {
		return this.noOfResource;
	}

	public void setNoOfResource(Integer noOfResource) {
		this.noOfResource = noOfResource;
	}

	public Integer getNoOfResourceRemaining() {
		return this.noOfResourceRemaining;
	}

	public void setNoOfResourceRemaining(Integer noOfResourceRemaining) {
		this.noOfResourceRemaining = noOfResourceRemaining;
	}

	public Integer getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Integer getFkThbsBudgetDesignationId() {
		return fkThbsBudgetDesignationId;
	}

	public void setFkThbsBudgetDesignationId(Integer fkThbsBudgetDesignationId) {
		this.fkThbsBudgetDesignationId = fkThbsBudgetDesignationId;
	}

	public Integer getFkResourceLevelId() {
		return fkResourceLevelId;
	}

	public void setFkResourceLevelId(Integer fkResourceLevelId) {
		this.fkResourceLevelId = fkResourceLevelId;
	}

	public MasEmpDesignationBO getMasEmpDesignationBO() {
		return masEmpDesignationBO;
	}

	public void setMasEmpDesignationBO(MasEmpDesignationBO masEmpDesignationBO) {
		this.masEmpDesignationBO = masEmpDesignationBO;
	}

	public MasEmpLevelBO getMasEmpLevelBO() {
		return masEmpLevelBO;
	}

	public void setMasEmpLevelBO(MasEmpLevelBO masEmpLevelBO) {
		this.masEmpLevelBO = masEmpLevelBO;
	}

	public ProjDatClientBudgetBO getProjDatClientBudgetBO() {
		return projDatClientBudgetBO;
	}

	public void setProjDatClientBudgetBO(ProjDatClientBudgetBO projDatClientBudgetBO) {
		this.projDatClientBudgetBO = projDatClientBudgetBO;
	}

	public DatWorkorderBO getDatWorkorderBO() {
		return datWorkorderBO;
	}

	public void setDatWorkorderBO(DatWorkorderBO datWorkorderBO) {
		this.datWorkorderBO = datWorkorderBO;
	}

	public MasThbsBudgetLocationBO getMasThbsBudgetLocationBO() {
		return masThbsBudgetLocationBO;
	}

	public void setMasThbsBudgetLocationBO(
			MasThbsBudgetLocationBO masThbsBudgetLocationBO) {
		this.masThbsBudgetLocationBO = masThbsBudgetLocationBO;
	}

	

}