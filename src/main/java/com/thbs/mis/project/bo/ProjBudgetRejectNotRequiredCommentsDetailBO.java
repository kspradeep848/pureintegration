package com.thbs.mis.project.bo;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the proj_budget_reject_not_required_comments_details database table.
 * 
 */
@Entity
@Table(name="proj_budget_reject_not_required_comments_details")
@NamedQuery(name="ProjBudgetRejectNotRequiredCommentsDetailBO.findAll", query="SELECT p FROM ProjBudgetRejectNotRequiredCommentsDetailBO p")
public class ProjBudgetRejectNotRequiredCommentsDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_budget_reject_comments_details_id")
	private int pkBudgetRejectCommentsDetailsId;

	@Column(name="entered_by")
	private int enteredBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="entered_on")
	private Date enteredOn;

	@Column(name="not_required_comment")
	private String notRequiredComment;

	@Column(name="rejected_comments")
	private String rejectedComments;

	@Column(name="fk_workorder_id")
	private Integer fkWorkorderId;

	//bi-directional many-to-one association to DatWorkorder
	@OneToOne
	@JoinColumn(name="fk_workorder_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datWorkorder;

	//bi-directional many-to-one association to ProjBudgetRejectNotRequiredCommentsDetailsAudit
	@OneToMany(mappedBy="projBudgetRejectNotRequiredCommentsDetail")
	private List<ProjBudgetRejectNotRequiredCommentsDetailsAuditBO> projBudgetRejectNotRequiredCommentsDetailsAudits;

	public ProjBudgetRejectNotRequiredCommentsDetailBO() {
	}

	public int getPkBudgetRejectCommentsDetailsId() {
		return this.pkBudgetRejectCommentsDetailsId;
	}

	public void setPkBudgetRejectCommentsDetailsId(int pkBudgetRejectCommentsDetailsId) {
		this.pkBudgetRejectCommentsDetailsId = pkBudgetRejectCommentsDetailsId;
	}

	public int getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(int enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredOn() {
		return this.enteredOn;
	}

	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}

	public String getNotRequiredComment() {
		return this.notRequiredComment;
	}

	public void setNotRequiredComment(String notRequiredComment) {
		this.notRequiredComment = notRequiredComment;
	}

	public String getRejectedComments() {
		return this.rejectedComments;
	}

	public void setRejectedComments(String rejectedComments) {
		this.rejectedComments = rejectedComments;
	}

	public Integer getFkWorkorderId() {
		return fkWorkorderId;
	}

	public void setFkWorkorderId(Integer fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}

	public DatWorkorderBO getDatWorkorder() {
		return datWorkorder;
	}

	public void setDatWorkorder(DatWorkorderBO datWorkorder) {
		this.datWorkorder = datWorkorder;
	}

	public List<ProjBudgetRejectNotRequiredCommentsDetailsAuditBO> getProjBudgetRejectNotRequiredCommentsDetailsAudits() {
		return projBudgetRejectNotRequiredCommentsDetailsAudits;
	}

	public void setProjBudgetRejectNotRequiredCommentsDetailsAudits(
			List<ProjBudgetRejectNotRequiredCommentsDetailsAuditBO> projBudgetRejectNotRequiredCommentsDetailsAudits) {
		this.projBudgetRejectNotRequiredCommentsDetailsAudits = projBudgetRejectNotRequiredCommentsDetailsAudits;
	}

	@Override
	public String toString() {
		return "ProjBudgetRejectNotRequiredCommentsDetailBO [pkBudgetRejectCommentsDetailsId="
				+ pkBudgetRejectCommentsDetailsId
				+ ", enteredBy="
				+ enteredBy
				+ ", enteredOn="
				+ enteredOn
				+ ", notRequiredComment="
				+ notRequiredComment
				+ ", rejectedComments="
				+ rejectedComments
				+ ", fkWorkorderId="
				+ fkWorkorderId
				+ ", datWorkorder="
				+ datWorkorder
				+ ", projBudgetRejectNotRequiredCommentsDetailsAudits="
				+ projBudgetRejectNotRequiredCommentsDetailsAudits + "]";
	}



}