		
package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasRegionBO;


/**
 * The persistent class for the mas_client database table.
 * 
 */
@Entity
@Table(name="mas_client")
@NamedQuery(name="MasClientBO.findAll", query="SELECT m FROM MasClientBO m")
public class MasClientBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_client_id")
	private Short pkClientId;

	@Column(name="client_name")
	private String clientName;

	@Column(name="client_status")
	private String clientStatus;
	
	@Column(name="vendor_code")
	private String vendorCode;
	
	@Column(name="payment_contract_documents")
	private String paymentContractDocuments;

	@Column(name="risk_assessment")
	private String riskAssessment;

	@Column(name="credit_period")
	private Short creditPeriod;

	@Column(name="created_by")
	private Integer createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="updated_by")
	private Integer updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;
	
	@Column(name="updated_comments")
	private String updatedComments;

	@Column(name="po_type")
	private String poType;
	
	@Column(name="company_email_address")
	private String companyEmailAddress;
	
	@Column(name="fk_region_id")
	private Short fkRegionId;
	
	@Column(name="no_of_days_po_mapping")
	private Integer noOfDaysPoMapping;
	
	@Column(name="contact_number_one")
	private String contactNumberOne;
	
	@Column(name="contact_number_two")
	private String contactNumberTwo;
	
	@Column(name="pan_number")
	private String panNumber;
	
	@Column(name="gst_number")
	private String gstNumber;
	
	@Column(name="vat_number")
	private String vatNumber;
	
	@Column(name="other_reference_number")
	private String otherReferenceNumber;
	
	@Column(name="client_history")
	private String clientHistory;
	
	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail1;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="updated_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail2;
	
	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="fk_region_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasRegionBO masRegionBO;
		
	public MasClientBO() {
	}

	public Short getPkClientId() {
		return pkClientId;
	}

	public void setPkClientId(Short pkClientId) {
		this.pkClientId = pkClientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientStatus() {
		return clientStatus;
	}

	public void setClientStatus(String clientStatus) {
		this.clientStatus = clientStatus;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getPaymentContractDocuments() {
		return paymentContractDocuments;
	}

	public void setPaymentContractDocuments(String paymentContractDocuments) {
		this.paymentContractDocuments = paymentContractDocuments;
	}

	public String getRiskAssessment() {
		return riskAssessment;
	}

	public void setRiskAssessment(String riskAssessment) {
		this.riskAssessment = riskAssessment;
	}

	public Short getCreditPeriod() {
		return creditPeriod;
	}

	public void setCreditPeriod(Short creditPeriod) {
		this.creditPeriod = creditPeriod;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedComments() {
		return updatedComments;
	}

	public void setUpdatedComments(String updatedComments) {
		this.updatedComments = updatedComments;
	}

	public String getPoType() {
		return poType;
	}

	public void setPoType(String poType) {
		this.poType = poType;
	}

	public String getCompanyEmailAddress() {
		return companyEmailAddress;
	}

	public void setCompanyEmailAddress(String companyEmailAddress) {
		this.companyEmailAddress = companyEmailAddress;
	}

	public Short getFkRegionId() {
		return fkRegionId;
	}

	public void setFkRegionId(Short fkRegionId) {
		this.fkRegionId = fkRegionId;
	}

	public Integer getNoOfDaysPoMapping() {
		return noOfDaysPoMapping;
	}

	public void setNoOfDaysPoMapping(Integer noOfDaysPoMapping) {
		this.noOfDaysPoMapping = noOfDaysPoMapping;
	}

	public String getContactNumberOne() {
		return contactNumberOne;
	}

	public void setContactNumberOne(String contactNumberOne) {
		this.contactNumberOne = contactNumberOne;
	}

	public String getContactNumberTwo() {
		return contactNumberTwo;
	}

	public void setContactNumberTwo(String contactNumberTwo) {
		this.contactNumberTwo = contactNumberTwo;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public String getOtherReferenceNumber() {
		return otherReferenceNumber;
	}

	public void setOtherReferenceNumber(String otherReferenceNumber) {
		this.otherReferenceNumber = otherReferenceNumber;
	}

	public String getClientHistory() {
		return clientHistory;
	}

	public void setClientHistory(String clientHistory) {
		this.clientHistory = clientHistory;
	}

	public DatEmpDetailBO getDatEmpDetail1() {
		return datEmpDetail1;
	}

	public void setDatEmpDetail1(DatEmpDetailBO datEmpDetail1) {
		this.datEmpDetail1 = datEmpDetail1;
	}

	public DatEmpDetailBO getDatEmpDetail2() {
		return datEmpDetail2;
	}

	public void setDatEmpDetail2(DatEmpDetailBO datEmpDetail2) {
		this.datEmpDetail2 = datEmpDetail2;
	}

	public MasRegionBO getMasRegionBO() {
		return masRegionBO;
	}

	public void setMasRegionBO(MasRegionBO masRegionBO) {
		this.masRegionBO = masRegionBO;
	}

	@Override
	public String toString() {
		return "MasClientBO [pkClientId=" + pkClientId + ", clientName="
				+ clientName + ", clientStatus=" + clientStatus
				+ ", vendorCode=" + vendorCode + ", paymentContractDocuments="
				+ paymentContractDocuments + ", riskAssessment="
				+ riskAssessment + ", creditPeriod=" + creditPeriod
				+ ", createdBy=" + createdBy + ", createdOn=" + createdOn
				+ ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn
				+ ", updatedComments=" + updatedComments + ", poType=" + poType
				+ ", companyEmailAddress=" + companyEmailAddress
				+ ", fkRegionId=" + fkRegionId + ", noOfDaysPoMapping="
				+ noOfDaysPoMapping + ", contactNumberOne=" + contactNumberOne
				+ ", contactNumberTwo=" + contactNumberTwo + ", panNumber="
				+ panNumber + ", gstNumber=" + gstNumber + ", vatNumber="
				+ vatNumber + ", otherReferenceNumber=" + otherReferenceNumber
				+ ", clientHistory=" + clientHistory + ", datEmpDetail1="
				+ datEmpDetail1 + ", datEmpDetail2=" + datEmpDetail2
				+ ", masRegionBO=" + masRegionBO + "]";
	}
}
