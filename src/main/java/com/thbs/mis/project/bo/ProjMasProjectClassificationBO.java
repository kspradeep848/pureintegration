package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the proj_mas_project_classification database table.
 * 
 */
@Entity
@Table(name="proj_mas_project_classification")
@NamedQuery(name="ProjMasProjectClassificationBO.findAll", query="SELECT p FROM ProjMasProjectClassificationBO p")
public class ProjMasProjectClassificationBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_classification_id")
	private byte pkClassificationId;

	@Column(name="classification_name")
	private String classificationName;

	//bi-directional many-to-one association to MasProjectAudit
	/*@OneToMany(mappedBy="masProjectAuditClassificationId")
	private List<MasProjectAuditBO> masProjectAuditClassificationIdList;*/
	
	public ProjMasProjectClassificationBO() {
	}

	public byte getPkClassificationId() {
		return this.pkClassificationId;
	}

	public void setPkClassificationId(byte pkClassificationId) {
		this.pkClassificationId = pkClassificationId;
	}

	public String getClassificationName() {
		return this.classificationName;
	}

	public void setClassificationName(String classificationName) {
		this.classificationName = classificationName;
	}

	@Override
	public String toString() {
		return "ProjMasProjectClassificationBO [pkClassificationId="
				+ pkClassificationId + ", classificationName="
				+ classificationName + "]";
	}

	
	
}