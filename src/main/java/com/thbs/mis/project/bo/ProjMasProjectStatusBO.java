package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the proj_mas_project_status database table.
 * 
 */
@Entity
@Table(name="proj_mas_project_status")
@NamedQuery(name="ProjMasProjectStatusBO.findAll", query="SELECT p FROM ProjMasProjectStatusBO p")
public class ProjMasProjectStatusBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_project_status_id")
	private byte pkProjectStatusId;

	@Column(name="project_status_name")
	private String projectStatusName;
	
	public ProjMasProjectStatusBO() {
	}

	public byte getPkProjectStatusId() {
		return this.pkProjectStatusId;
	}

	public void setPkProjectStatusId(byte pkProjectStatusId) {
		this.pkProjectStatusId = pkProjectStatusId;
	}

	public String getProjectStatusName() {
		return this.projectStatusName;
	}

	public void setProjectStatusName(String projectStatusName) {
		this.projectStatusName = projectStatusName;
	}

	@Override
	public String toString() {
		return "ProjMasProjectStatusBO [pkProjectStatusId=" + pkProjectStatusId
				+ ", projectStatusName=" + projectStatusName + "]";
	}

	
}