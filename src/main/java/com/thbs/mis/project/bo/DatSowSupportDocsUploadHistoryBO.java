package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the dat_sow_support_docs_upload_history database table.
 * 
 */
@Entity
@Table(name="dat_sow_support_docs_upload_history")
@NamedQuery(name="DatSowSupportDocsUploadHistoryBO.findAll", query="SELECT d FROM DatSowSupportDocsUploadHistoryBO d")
public class DatSowSupportDocsUploadHistoryBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_sow_support_docs_upload_id")
	private int pkSowSupportDocsUploadId;

	@Column(name="created_by")
	private int createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="sow_support_file_name")
	private String sowSupportFileName;
	
	@Column(name="sow_support_file_ref_name")
	private String sowSupportFileRefName;

	@Column(name="fk_workorder_id")
	private int fkWorkorderId;
	
	@Column(name="version_id")
	private short versionId;
	
	//bi-directional many-to-one association to DatWorkorder
	@OneToOne
	@JoinColumn(name="fk_workorder_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datWorkorder;

	public DatSowSupportDocsUploadHistoryBO() {
	}

	public int getPkSowSupportDocsUploadId() {
		return this.pkSowSupportDocsUploadId;
	}

	public void setPkSowSupportDocsUploadId(int pkSowSupportDocsUploadId) {
		this.pkSowSupportDocsUploadId = pkSowSupportDocsUploadId;
	}

	public int getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getSowSupportFileName() {
		return this.sowSupportFileName;
	}

	public void setSowSupportFileName(String sowSupportFileName) {
		this.sowSupportFileName = sowSupportFileName;
	}

	public DatWorkorderBO getDatWorkorder() {
		return this.datWorkorder;
	}

	public void setDatWorkorder(DatWorkorderBO datWorkorder) {
		this.datWorkorder = datWorkorder;
	}

	public String getSowSupportFileRefName() {
		return sowSupportFileRefName;
	}

	public void setSowSupportFileRefName(String sowSupportFileRefName) {
		this.sowSupportFileRefName = sowSupportFileRefName;
	}

	public int getFkWorkorderId() {
		return fkWorkorderId;
	}

	public void setFkWorkorderId(int fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}

	public short getVersionId() {
		return versionId;
	}

	public void setVersionId(short versionId) {
		this.versionId = versionId;
	}

	@Override
	public String toString() {
		return "DatSowSupportDocsUploadHistoryBO [pkSowSupportDocsUploadId="
				+ pkSowSupportDocsUploadId + ", createdBy=" + createdBy
				+ ", createdOn=" + createdOn + ", sowSupportFileName="
				+ sowSupportFileName + ", sowSupportFileRefName="
				+ sowSupportFileRefName + ", fkWorkorderId=" + fkWorkorderId
				+ ", versionId=" + versionId + ", datWorkorder=" + datWorkorder
				+ "]";
	}

	
	

	
	
}