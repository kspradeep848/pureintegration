package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the proj_mas_client_manager database table.
 * 
 */
@Entity
@Table(name="proj_mas_client_manager")
@NamedQuery(name="ProjMasClientManagerBO.findAll", query="SELECT p FROM ProjMasClientManagerBO p")
public class ProjMasClientManagerBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_client_manager_id")
	private Short pkClientManagerId;

	@Column(name="email_address")
	private String emailAddress;

	@Column(name="manager_name")
	private String managerName;
	
	//bi-directional many-to-one association to MasClientAudit
	/*@OneToMany(mappedBy="masClientAuditClientRepresentativeId")
	private List<MasClientAuditBO> masClientAuditClientRepresentativeIdList;
	
	//bi-directional many-to-one association to DatWorkorderAudit
	@OneToMany(mappedBy="datWorkorderAuditClientManagerId")
	private List<DatWorkorderAuditBO> datWorkorderAuditClientManagerIdList;*/

	public ProjMasClientManagerBO() {
	}

	public Short getPkClientManagerId() {
		return this.pkClientManagerId;
	}

	public void setPkClientManagerId(Short pkClientManagerId) {
		this.pkClientManagerId = pkClientManagerId;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getManagerName() {
		return this.managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	@Override
	public String toString() {
		return "ProjMasClientManagerBO [pkClientManagerId=" + pkClientManagerId
				+ ", emailAddress=" + emailAddress + ", managerName="
				+ managerName + "]";
	}

	
}