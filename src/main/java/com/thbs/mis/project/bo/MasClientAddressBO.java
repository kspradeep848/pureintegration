package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasCountryBO;


/**
 * The persistent class for the mas_client_address database table.
 * 
 */
@Entity
@Table(name="mas_client_address")
@NamedQuery(name="MasClientAddressBO.findAll", query="SELECT m FROM MasClientAddressBO m")
public class MasClientAddressBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_client_address_id")
	private Short pkClientAddressId;
	
	@Column(name="fk_client_id")
	private Short fkClientId;

	@Column(name="address_line_one")
	private String addressLineOne;

	@Column(name="address_line_two")
	private String addressLineTwo;
	
	@Column(name="fk_country_id")
	private Short fkCountryId;

	@Column(name="city_name")
	private String cityName;
	
	@Column(name="state_name")
	private String stateName;
	
	@Column(name="zip_code")
	private String zipCode;

	@Column(name="is_bill_to_address")
	private String isBillToAddress;

	@Column(name="is_ship_to_address")
	private String isShipToAddress;

	@Column(name="created_by")
	private Integer createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="updated_by")
	private Integer updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;
	
	@Column(name="updated_comments")
	private String updatedComments;

	//bi-directional many-to-one association to MasClient
	@OneToOne
	@JoinColumn(name="fk_client_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasClientBO masClient;
	
	//bi-directional many-to-one association to MasCountry
	@ManyToOne
	@JoinColumn(name="fk_country_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasCountryBO masCountry;
	
	@OneToOne
	@JoinColumn(name="created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail;
	
	@OneToOne
	@JoinColumn(name="updated_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail1;
	
	public MasClientAddressBO() {
	}

	public Short getPkClientAddressId() {
		return pkClientAddressId;
	}

	public void setPkClientAddressId(Short pkClientAddressId) {
		this.pkClientAddressId = pkClientAddressId;
	}

	public Short getFkClientId() {
		return fkClientId;
	}

	public void setFkClientId(Short fkClientId) {
		this.fkClientId = fkClientId;
	}

	public String getAddressLineOne() {
		return addressLineOne;
	}

	public void setAddressLineOne(String addressLineOne) {
		this.addressLineOne = addressLineOne;
	}

	public String getAddressLineTwo() {
		return addressLineTwo;
	}

	public void setAddressLineTwo(String addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}

	public Short getFkCountryId() {
		return fkCountryId;
	}

	public void setFkCountryId(Short fkCountryId) {
		this.fkCountryId = fkCountryId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getIsBillToAddress() {
		return isBillToAddress;
	}

	public void setIsBillToAddress(String isBillToAddress) {
		this.isBillToAddress = isBillToAddress;
	}

	public String getIsShipToAddress() {
		return isShipToAddress;
	}

	public void setIsShipToAddress(String isShipToAddress) {
		this.isShipToAddress = isShipToAddress;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedComments() {
		return updatedComments;
	}

	public void setUpdatedComments(String updatedComments) {
		this.updatedComments = updatedComments;
	}

	public MasClientBO getMasClient() {
		return masClient;
	}

	public void setMasClient(MasClientBO masClient) {
		this.masClient = masClient;
	}

	public MasCountryBO getMasCountry() {
		return masCountry;
	}

	public void setMasCountry(MasCountryBO masCountry) {
		this.masCountry = masCountry;
	}

	public DatEmpDetailBO getDatEmpDetail() {
		return datEmpDetail;
	}

	public void setDatEmpDetail(DatEmpDetailBO datEmpDetail) {
		this.datEmpDetail = datEmpDetail;
	}

	public DatEmpDetailBO getDatEmpDetail1() {
		return datEmpDetail1;
	}

	public void setDatEmpDetail1(DatEmpDetailBO datEmpDetail1) {
		this.datEmpDetail1 = datEmpDetail1;
	}

	@Override
	public String toString() {
		return "MasClientAddressBO [pkClientAddressId=" + pkClientAddressId
				+ ", fkClientId=" + fkClientId + ", addressLineOne="
				+ addressLineOne + ", addressLineTwo=" + addressLineTwo
				+ ", fkCountryId=" + fkCountryId + ", cityName=" + cityName
				+ ", stateName=" + stateName + ", zipCode=" + zipCode
				+ ", isBillToAddress=" + isBillToAddress + ", isShipToAddress="
				+ isShipToAddress + ", createdBy=" + createdBy + ", createdOn="
				+ createdOn + ", updatedBy=" + updatedBy + ", updatedOn="
				+ updatedOn + ", updatedComments=" + updatedComments
				+ ", masClient=" + masClient + ", masCountry=" + masCountry
				+ ", datEmpDetail=" + datEmpDetail + ", datEmpDetail1="
				+ datEmpDetail1 + "]";
	}
}