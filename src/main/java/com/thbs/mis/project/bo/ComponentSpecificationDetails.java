package com.thbs.mis.project.bo;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.project.bean.SearchComponentInputBean;

public class ComponentSpecificationDetails {
	
	public static Specification<DatComponentBO> searchComponents(SearchComponentInputBean InputDetails) {
		return new Specification<DatComponentBO>() {

		@Override
		public Predicate toPredicate(Root<DatComponentBO> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		final Collection<Predicate> predicates = new ArrayList<>();
		
		
		if (InputDetails.getComponentId() !=null) {
			final Predicate project = cb.equal(root.get("pkComponentId"), InputDetails.getComponentId());
			predicates.add(project);
		}
		
		if (InputDetails.getComponentStatusId() != null) {
			final Predicate compStatus = cb.equal(root.get("fkComponentStatusId"), InputDetails.getComponentStatusId());
			predicates.add(compStatus);
		}
		
		if (InputDetails.getComponentTypeId() != null) {
			final Predicate compType = cb.equal(root.get("fkComponentTypeId"), InputDetails.getComponentTypeId());
			predicates.add(compType);
		}
		
		if (InputDetails.getWoId() != null) {
			final Predicate woId = cb.equal(root.get("fkWorkorderId"), InputDetails.getWoId());
			predicates.add(woId);
		}
		return cb.and(predicates.toArray(new Predicate[predicates.size()]));
		}
		};
	}
}
