package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasCurrencytypeBO;
import com.thbs.mis.framework.util.CustomDateSerializer;
import com.thbs.mis.project.bean.WoNumberOutputBean;


/**
 * The persistent class for the dat_workorder database table.
 * 
 */
@Entity
@Table(name="dat_workorder")
@NamedQuery(name="DatWorkorderBO.findAll", query="SELECT d FROM DatWorkorderBO d")


@SqlResultSetMapping(name = "woNumDetailsMapping", classes = { @ConstructorResult(targetClass = WoNumberOutputBean.class, 
columns = {
	@ColumnResult(name = "pk_workorder_id"),
	@ColumnResult(name = "workorder_number")})})


@NamedNativeQueries({
	@NamedNativeQuery(name = "DatComponentBO.getWorkorderNumbers",
			query = "select d.pk_workorder_id,d.workorder_number from "
					+ " dat_workorder d inner join mas_projects on "
					+ " pk_project_id=d.fk_project_id "
					+ " where pk_project_id in"
					+ " (select m.fk_project_id from project_level_user_access_group m"
					+ " where m.fk_user_id =:empId"
					+ " union"
					+ " select s.fk_project_id from "
					+ " proj_dat_project_mapped_to_project_coordinator s"
					+ " where s.fk_project_coordinator_id =:empId"
					+ " union "
					+ " select prj.pk_project_id from mas_projects prj"
					+ " where prj.fk_bu_unit_id =:buId)"
					+ "",resultSetMapping = "woNumDetailsMapping")})
public class DatWorkorderBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_workorder_id")
	private int pkWorkorderId;

	@Column(name="additional_amount")
	private BigDecimal additionalAmount;

	@Column(name="comment_for_update")
	private String commentForUpdate;

	@Column(name="sow_amount")
	private BigDecimal sowAmount;

	@Column(name="sow_uploaded_flag")
	private String sowUploadedFlag;

	@Temporal(TemporalType.DATE)
	@Column(name="wo_end_date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date woEndDate;

	@Temporal(TemporalType.DATE)
	@Column(name="wo_start_date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date woStartDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="workorder_created_on")
	private Date workorderCreatedOn;

	@Column(name="workorder_description")
	private String workorderDescription;

	@Column(name="workorder_number")
	private String workorderNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="workorder_updated_on")
	private Date workorderUpdatedOn;
	
	@Column(name="is_internal")
	private String isInternal;
	
	@Column(name="workorder_discount")
	private BigDecimal workorderDiscount;
	
	@Column(name="client_manager_id")
	private short clientManagerId;
	
	@Column(name="sow_currency_id")
	private short sowCurrencyId;
	
	@Column(name="fk_project_id")
	private int fkProjectId;
	
	@Column(name="project_owner")
	private Integer projectOwner;
	
	@Column(name="fk_workorder_status_id")
	private byte fkWorkorderStatusId;
	
	@Column(name="workorder_created_by")
	private Integer workorderCreatedBy;
	
	@Column(name="workorder_updated_by")
	private Integer workorderUpdatedBy;

	@Column(name="wo_history")
	private String woHistory;
	
	//bi-directional many-to-one association to ProjMasClientManager
	@OneToOne
	@JoinColumn(name="client_manager_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasClientManagerBO projMasClientManager;

	//bi-directional many-to-one association to MasCurrencytype
	@OneToOne
	@JoinColumn(name="sow_currency_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasCurrencytypeBO masCurrencytype;

	//bi-directional many-to-one association to MasProject
	@OneToOne
	@JoinColumn(name="fk_project_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasProjectBO masProject;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="project_owner", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail1;

	//bi-directional many-to-one association to ProjMasWoStatus
	@OneToOne
	@JoinColumn(name="fk_workorder_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasWoStatusBO projMasWoStatus;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="workorder_created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail2;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="workorder_updated_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail3;
	
	public DatWorkorderBO() {
		// TODO Auto-generated constructor stub
	}
	
	public int getPkWorkorderId() {
		return this.pkWorkorderId;
	}

	public void setPkWorkorderId(int pkWorkorderId) {
		this.pkWorkorderId = pkWorkorderId;
	}

	public BigDecimal getAdditionalAmount() {
		return this.additionalAmount;
	}

	public void setAdditionalAmount(BigDecimal additionalAmount) {
		this.additionalAmount = additionalAmount;
	}

	public String getCommentForUpdate() {
		return this.commentForUpdate;
	}

	public void setCommentForUpdate(String commentForUpdate) {
		this.commentForUpdate = commentForUpdate;
	}

	public BigDecimal getSowAmount() {
		return this.sowAmount;
	}

	public void setSowAmount(BigDecimal sowAmount) {
		this.sowAmount = sowAmount;
	}

	public String getSowUploadedFlag() {
		return this.sowUploadedFlag;
	}

	public void setSowUploadedFlag(String sowUploadedFlag) {
		this.sowUploadedFlag = sowUploadedFlag;
	}

	public Date getWoEndDate() {
		return this.woEndDate;
	}

	public void setWoEndDate(Date woEndDate) {
		this.woEndDate = woEndDate;
	}

	public Date getWoStartDate() {
		return this.woStartDate;
	}

	public void setWoStartDate(Date woStartDate) {
		this.woStartDate = woStartDate;
	}

	public Date getWorkorderCreatedOn() {
		return this.workorderCreatedOn;
	}

	public void setWorkorderCreatedOn(Date workorderCreatedOn) {
		this.workorderCreatedOn = workorderCreatedOn;
	}

	public String getWorkorderDescription() {
		return this.workorderDescription;
	}

	public void setWorkorderDescription(String workorderDescription) {
		this.workorderDescription = workorderDescription;
	}

	public String getWoHistory() {
		return woHistory;
	}

	public void setWoHistory(String woHistory) {
		this.woHistory = woHistory;
	}

	public String getWorkorderNumber() {
		return this.workorderNumber;
	}

	public void setWorkorderNumber(String workorderNumber) {
		this.workorderNumber = workorderNumber;
	}

	public Date getWorkorderUpdatedOn() {
		return this.workorderUpdatedOn;
	}

	public void setWorkorderUpdatedOn(Date workorderUpdatedOn) {
		this.workorderUpdatedOn = workorderUpdatedOn;
	}

	public ProjMasClientManagerBO getProjMasClientManager() {
		return projMasClientManager;
	}

	public void setProjMasClientManager(ProjMasClientManagerBO projMasClientManager) {
		this.projMasClientManager = projMasClientManager;
	}

	public MasCurrencytypeBO getMasCurrencytype() {
		return masCurrencytype;
	}

	public void setMasCurrencytype(MasCurrencytypeBO masCurrencytype) {
		this.masCurrencytype = masCurrencytype;
	}

	public MasProjectBO getMasProject() {
		return masProject;
	}

	public void setMasProject(MasProjectBO masProject) {
		this.masProject = masProject;
	}

	public DatEmpDetailBO getDatEmpDetail1() {
		return datEmpDetail1;
	}

	public void setDatEmpDetail1(DatEmpDetailBO datEmpDetail1) {
		this.datEmpDetail1 = datEmpDetail1;
	}

	public ProjMasWoStatusBO getProjMasWoStatus() {
		return projMasWoStatus;
	}

	public void setProjMasWoStatus(ProjMasWoStatusBO projMasWoStatus) {
		this.projMasWoStatus = projMasWoStatus;
	}

	public DatEmpDetailBO getDatEmpDetail2() {
		return datEmpDetail2;
	}

	public void setDatEmpDetail2(DatEmpDetailBO datEmpDetail2) {
		this.datEmpDetail2 = datEmpDetail2;
	}

	public DatEmpDetailBO getDatEmpDetail3() {
		return datEmpDetail3;
	}

	public void setDatEmpDetail3(DatEmpDetailBO datEmpDetail3) {
		this.datEmpDetail3 = datEmpDetail3;
	}

	public short getClientManagerId() {
		return clientManagerId;
	}

	public void setClientManagerId(short clientManagerId) {
		this.clientManagerId = clientManagerId;
	}

	public short getSowCurrencyId() {
		return sowCurrencyId;
	}

	public void setSowCurrencyId(short sowCurrencyId) {
		this.sowCurrencyId = sowCurrencyId;
	}

	public int getFkProjectId() {
		return fkProjectId;
	}

	public void setFkProjectId(int fkProjectId) {
		this.fkProjectId = fkProjectId;
	}

	public Integer getProjectOwner() {
		return projectOwner;
	}

	public void setProjectOwner(Integer projectOwner) {
		this.projectOwner = projectOwner;
	}

	public byte getFkWorkorderStatusId() {
		return fkWorkorderStatusId;
	}

	public void setFkWorkorderStatusId(byte fkWorkorderStatusId) {
		this.fkWorkorderStatusId = fkWorkorderStatusId;
	}

	public Integer getWorkorderCreatedBy() {
		return workorderCreatedBy;
	}

	public void setWorkorderCreatedBy(Integer workorderCreatedBy) {
		this.workorderCreatedBy = workorderCreatedBy;
	}

	public Integer getWorkorderUpdatedBy() {
		return workorderUpdatedBy;
	}

	public void setWorkorderUpdatedBy(Integer workorderUpdatedBy) {
		this.workorderUpdatedBy = workorderUpdatedBy;
	}

	public String getIsInternal() {
		return isInternal;
	}

	public void setIsInternal(String isInternal) {
		this.isInternal = isInternal;
	}

	public BigDecimal getWorkorderDiscount() {
		return workorderDiscount;
	}

	public void setWorkorderDiscount(BigDecimal workorderDiscount) {
		this.workorderDiscount = workorderDiscount;
	}

	@Override
	public String toString() {
		return "DatWorkorderBO [pkWorkorderId=" + pkWorkorderId
				+ ", additionalAmount=" + additionalAmount
				+ ", commentForUpdate=" + commentForUpdate + ", sowAmount="
				+ sowAmount + ", sowUploadedFlag=" + sowUploadedFlag
				+ ", woEndDate=" + woEndDate + ", woStartDate=" + woStartDate
				+ ", workorderCreatedOn=" + workorderCreatedOn
				+ ", workorderDescription=" + workorderDescription
				+ ", workorderNumber=" + workorderNumber
				+ ", workorderUpdatedOn=" + workorderUpdatedOn
				+ ", isInternal=" + isInternal + ", workorderDiscount="
				+ workorderDiscount + ", clientManagerId=" + clientManagerId
				+ ", sowCurrencyId=" + sowCurrencyId + ", fkProjectId="
				+ fkProjectId + ", projectOwner=" + projectOwner
				+ ", fkWorkorderStatusId=" + fkWorkorderStatusId
				+ ", workorderCreatedBy=" + workorderCreatedBy
				+ ", workorderUpdatedBy=" + workorderUpdatedBy
				+ ", projMasClientManager=" + projMasClientManager
				+ ", masCurrencytype=" + masCurrencytype + ", masProject="
				+ masProject + ", datEmpDetail1=" + datEmpDetail1
				+ ", projMasWoStatus=" + projMasWoStatus + ", datEmpDetail2="
				+ datEmpDetail2 + ", datEmpDetail3=" + datEmpDetail3 + "]";
	}

	
}