package com.thbs.mis.project.bo;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the proj_budget_reject_not_required_comments_details_audit database table.
 * 
 */
@Entity
@Table(name="proj_budget_reject_not_required_comments_details_audit")
@NamedQuery(name="ProjBudgetRejectNotRequiredCommentsDetailsAuditBO.findAll", query="SELECT p FROM ProjBudgetRejectNotRequiredCommentsDetailsAuditBO p")
public class ProjBudgetRejectNotRequiredCommentsDetailsAuditBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_budget_reject_comments_details_audit_id")
	private int pkBudgetRejectCommentsDetailsAuditId;

	private String action;

	@Column(name="entered_by")
	private int enteredBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="entered_on")
	private Date enteredOn;

	@Column(name="not_required_comment")
	private String notRequiredComment;

	@Column(name="rejected_comments")
	private String rejectedComments;


	@Column(name="fk_budget_reject_comments_details_id")
	private String fkBudgetRejectCommentsDetailsId;

	//bi-directional many-to-one association to ProjBudgetRejectNotRequiredCommentsDetail
	@OneToOne
	@JoinColumn(name="fk_budget_reject_comments_details_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjBudgetRejectNotRequiredCommentsDetailBO projBudgetRejectNotRequiredCommentsDetail;

	//bi-directional many-to-one association to DatWorkorder
	@OneToOne
	@JoinColumn(name="fk_workorder_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datWorkorder;

	public ProjBudgetRejectNotRequiredCommentsDetailsAuditBO() {
	}

	public int getPkBudgetRejectCommentsDetailsAuditId() {
		return this.pkBudgetRejectCommentsDetailsAuditId;
	}

	public void setPkBudgetRejectCommentsDetailsAuditId(int pkBudgetRejectCommentsDetailsAuditId) {
		this.pkBudgetRejectCommentsDetailsAuditId = pkBudgetRejectCommentsDetailsAuditId;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(int enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredOn() {
		return this.enteredOn;
	}

	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}

	public String getNotRequiredComment() {
		return this.notRequiredComment;
	}

	public void setNotRequiredComment(String notRequiredComment) {
		this.notRequiredComment = notRequiredComment;
	}

	public String getRejectedComments() {
		return this.rejectedComments;
	}

	public void setRejectedComments(String rejectedComments) {
		this.rejectedComments = rejectedComments;
	}

	public String getFkBudgetRejectCommentsDetailsId() {
		return fkBudgetRejectCommentsDetailsId;
	}

	public void setFkBudgetRejectCommentsDetailsId(
			String fkBudgetRejectCommentsDetailsId) {
		this.fkBudgetRejectCommentsDetailsId = fkBudgetRejectCommentsDetailsId;
	}

	public ProjBudgetRejectNotRequiredCommentsDetailBO getProjBudgetRejectNotRequiredCommentsDetail() {
		return projBudgetRejectNotRequiredCommentsDetail;
	}

	public void setProjBudgetRejectNotRequiredCommentsDetail(
			ProjBudgetRejectNotRequiredCommentsDetailBO projBudgetRejectNotRequiredCommentsDetail) {
		this.projBudgetRejectNotRequiredCommentsDetail = projBudgetRejectNotRequiredCommentsDetail;
	}

	public DatWorkorderBO getDatWorkorder() {
		return datWorkorder;
	}

	public void setDatWorkorder(DatWorkorderBO datWorkorder) {
		this.datWorkorder = datWorkorder;
	}

	@Override
	public String toString() {
		return "ProjBudgetRejectNotRequiredCommentsDetailsAuditBO [pkBudgetRejectCommentsDetailsAuditId="
				+ pkBudgetRejectCommentsDetailsAuditId
				+ ", action="
				+ action
				+ ", enteredBy="
				+ enteredBy
				+ ", enteredOn="
				+ enteredOn
				+ ", notRequiredComment="
				+ notRequiredComment
				+ ", rejectedComments="
				+ rejectedComments
				+ ", fkBudgetRejectCommentsDetailsId="
				+ fkBudgetRejectCommentsDetailsId
				+ ", projBudgetRejectNotRequiredCommentsDetail="
				+ projBudgetRejectNotRequiredCommentsDetail
				+ ", datWorkorder="
				+ datWorkorder + "]";
	}

	
}