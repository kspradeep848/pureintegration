package com.thbs.mis.project.bo;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the proj_client_budget_approval_details_audit database table.
 * 
 */
@Entity
@Table(name="proj_client_budget_approval_details_audit")
@NamedQuery(name="ProjClientBudgetApprovalDetailsAuditBO.findAll", query="SELECT p FROM ProjClientBudgetApprovalDetailsAuditBO p")
public class ProjClientBudgetApprovalDetailsAuditBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_client_budget_approval_details_audit_id")
	private int pkClientBudgetApprovalDetailsAuditId;

	private String action;

	@Column(name="approved_or_rejected_by")
	private int approvedOrRejectedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="approved_or_rejected_on")
	private Date approvedOrRejectedOn;

	@Column(name="fk_budget_status_id")
	private int fkBudgetStatusId;

	@Column(name="requested_by")
	private int requestedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="requested_on")
	private Date requestedOn;

	//bi-directional many-to-one association to ProjClientBudgetApprovalDetail
	@OneToOne
	@JoinColumn(name="fk_client_budget_approval_details_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjClientBudgetApprovalDetailBO projClientBudgetApprovalDetailBO;

	//bi-directional many-to-one association to ProjDatClientBudget
	@OneToOne
	@JoinColumn(name="fk_client_budget_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjDatClientBudgetBO projDatClientBudget;

	public int getPkClientBudgetApprovalDetailsAuditId() {
		return pkClientBudgetApprovalDetailsAuditId;
	}

	public void setPkClientBudgetApprovalDetailsAuditId(
			int pkClientBudgetApprovalDetailsAuditId) {
		this.pkClientBudgetApprovalDetailsAuditId = pkClientBudgetApprovalDetailsAuditId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getApprovedOrRejectedBy() {
		return approvedOrRejectedBy;
	}

	public void setApprovedOrRejectedBy(int approvedOrRejectedBy) {
		this.approvedOrRejectedBy = approvedOrRejectedBy;
	}

	public Date getApprovedOrRejectedOn() {
		return approvedOrRejectedOn;
	}

	public void setApprovedOrRejectedOn(Date approvedOrRejectedOn) {
		this.approvedOrRejectedOn = approvedOrRejectedOn;
	}

	public int getFkBudgetStatusId() {
		return fkBudgetStatusId;
	}

	public void setFkBudgetStatusId(int fkBudgetStatusId) {
		this.fkBudgetStatusId = fkBudgetStatusId;
	}

	public int getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(int requestedBy) {
		this.requestedBy = requestedBy;
	}

	public Date getRequestedOn() {
		return requestedOn;
	}

	public void setRequestedOn(Date requestedOn) {
		this.requestedOn = requestedOn;
	}

	public ProjClientBudgetApprovalDetailBO getProjClientBudgetApprovalDetailBO() {
		return projClientBudgetApprovalDetailBO;
	}

	public void setProjClientBudgetApprovalDetailBO(
			ProjClientBudgetApprovalDetailBO projClientBudgetApprovalDetailBO) {
		this.projClientBudgetApprovalDetailBO = projClientBudgetApprovalDetailBO;
	}

	public ProjDatClientBudgetBO getProjDatClientBudget() {
		return projDatClientBudget;
	}

	public void setProjDatClientBudget(ProjDatClientBudgetBO projDatClientBudget) {
		this.projDatClientBudget = projDatClientBudget;
	}

	

}