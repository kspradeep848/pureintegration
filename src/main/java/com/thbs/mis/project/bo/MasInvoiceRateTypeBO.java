package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="mas_invoice_rate_type")
//@NamedQuery(name="MasInvoiceRateTypeBO.findAll", query="SELECT m FROM MasInvoiceRateTypeBO m")
public class MasInvoiceRateTypeBO implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MAS_INVOICE_RATE_TYPE_PKINVOICERATETYPEID_GENERATOR", sequenceName="BO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MAS_INVOICE_RATE_TYPE_PKINVOICERATETYPEID_GENERATOR")
	@Column(name="pk_invoice_rate_type_id")
	private byte pkInvoiceRateTypeId;

	@Column(name="invoice_rate_type_name")
	private String invoiceRateTypeName;

	/*@OneToOne(mappedBy="masInvoiceRateType")
	private List<ProjDatClientBudgetRateDetailBO> projDatClientBudgetRateDetails;*/

	public byte getPkInvoiceRateTypeId() {
		return pkInvoiceRateTypeId;
	}

	public void setPkInvoiceRateTypeId(byte pkInvoiceRateTypeId) {
		this.pkInvoiceRateTypeId = pkInvoiceRateTypeId;
	}

	public String getInvoiceRateTypeName() {
		return invoiceRateTypeName;
	}

	public void setInvoiceRateTypeName(String invoiceRateTypeName) {
		this.invoiceRateTypeName = invoiceRateTypeName;
	}

	/*public List<ProjDatClientBudgetRateDetailBO> getProjDatClientBudgetRateDetails() {
		return projDatClientBudgetRateDetails;
	}

	public void setProjDatClientBudgetRateDetails(
			List<ProjDatClientBudgetRateDetailBO> projDatClientBudgetRateDetails) {
		this.projDatClientBudgetRateDetails = projDatClientBudgetRateDetails;
	}*/

	@Override
	public String toString() {
		return "MasInvoiceRateTypeBO [pkInvoiceRateTypeId="
				+ pkInvoiceRateTypeId + ", invoiceRateTypeName="
				+ invoiceRateTypeName + "]";
	}

	
}
