package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.MasLocationParentBO;


/**
 * The persistent class for the proj_dat_budget_location_mapped_parent_location database table.
 * 
 */
@Entity
@Table(name="proj_dat_budget_location_mapped_parent_location")
@NamedQuery(name="ProjDatBudgetLocationMappedParentLocationBO.findAll", query="SELECT p FROM ProjDatBudgetLocationMappedParentLocationBO p")
public class ProjDatBudgetLocationMappedParentLocationBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="pk_budget_location_mapped_physical_location_id")
	private int pkBudgetLocationMappedPhysicalLocationId;

	@Column(name="fk_finance_budget_location_id")
	private int fkFinanceBudgetLocationId;

	@Column(name="fk_parent_location_id")
	private int fkParentLocationId;

	//bi-directional many-to-one association to MasThbsBudgetLocation
	@OneToOne
	@JoinColumn(name="fk_finance_budget_location_id",unique = true, nullable = true, insertable = false, updatable = false)
	private MasThbsBudgetLocationBO masThbsBudgetLocationBO;

	//bi-directional many-to-one association to MasLocationParent
	@OneToOne
	@JoinColumn(name="fk_parent_location_id",unique = true, nullable = true, insertable = false, updatable = false)
	private MasLocationParentBO masLocationParent;

	public int getFkFinanceBudgetLocationId() {
		return fkFinanceBudgetLocationId;
	}

	public void setFkFinanceBudgetLocationId(int fkFinanceBudgetLocationId) {
		this.fkFinanceBudgetLocationId = fkFinanceBudgetLocationId;
	}

	public int getFkParentLocationId() {
		return fkParentLocationId;
	}

	public void setFkParentLocationId(int fkParentLocationId) {
		this.fkParentLocationId = fkParentLocationId;
	}

	public MasThbsBudgetLocationBO getMasThbsBudgetLocationBO() {
		return masThbsBudgetLocationBO;
	}

	public ProjDatBudgetLocationMappedParentLocationBO() {
	}

	public int getPkBudgetLocationMappedPhysicalLocationId() {
		return this.pkBudgetLocationMappedPhysicalLocationId;
	}

	public void setPkBudgetLocationMappedPhysicalLocationId(int pkBudgetLocationMappedPhysicalLocationId) {
		this.pkBudgetLocationMappedPhysicalLocationId = pkBudgetLocationMappedPhysicalLocationId;
	}

	public MasThbsBudgetLocationBO getMasThbsBudgetLocation() {
		return this.masThbsBudgetLocationBO;
	}

	public void setMasThbsBudgetLocationBO(MasThbsBudgetLocationBO masThbsBudgetLocationBO) {
		this.masThbsBudgetLocationBO = masThbsBudgetLocationBO;
	}

	public MasLocationParentBO getMasLocationParent() {
		return this.masLocationParent;
	}

	public void setMasLocationParent(MasLocationParentBO masLocationParent) {
		this.masLocationParent = masLocationParent;
	}

}