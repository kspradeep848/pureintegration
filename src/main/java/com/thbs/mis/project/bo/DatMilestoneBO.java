package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;


/**
 * The persistent class for the dat_milestone database table.
 * 
 */
@Entity
@Table(name="dat_milestone")
@NamedQuery(name="DatMilestoneBO.findAll", query="SELECT d FROM DatMilestoneBO d")
public class DatMilestoneBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_milestone_id")
	private int pkMilestoneId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="milestone_type")
	private String mielstoneType;

	@Column(name="milestone_name")
	private String milestoneName;

	@Column(name="milestone_value")
	private BigDecimal milestoneValue;

	@Column(name="updation_comments")
	private String updatedComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;
	
	@Column(name="line_item")
	private String lineItem;
	
	@Column(name="quantity")
	private BigDecimal quantity;

	@Column(name="unit_price")
	private BigDecimal unitPrice;
	
	@Column(name="milestone_remaining_value")
	private BigDecimal milestoneRemainingValue;

	@Column(name="fk_component_id")
	private Integer fkComponentId;
	
	@Column(name="created_by")
	private Integer createdBy;
	
	@Column(name="fk_milestone_status_id")
	private byte fkMilestoneStatusId;
	
	@Column(name="updated_by")
	private Integer updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="milestone_end_date")
	private Date milestoneEndDate;
	
	@Column(name="milestone_history")
	private String milestoneHistory;
	
	//bi-directional many-to-one association to DatComponent
	@OneToOne
	@JoinColumn(name="fk_component_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatComponentBO datComponent;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail1;

	//bi-directional many-to-one association to MasMilestoneStatus
	@OneToOne
	@JoinColumn(name="fk_milestone_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasMilestoneStatusBO masMilestoneStatus;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="updated_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail2;

	public DatMilestoneBO() {
		// TODO Auto-generated constructor stub
	}

	public int getPkMilestoneId() {
		return this.pkMilestoneId;
	}

	public void setPkMilestoneId(int pkMilestoneId) {
		this.pkMilestoneId = pkMilestoneId;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public BigDecimal getMilestoneRemainingValue() {
		return milestoneRemainingValue;
	}

	public void setMilestoneRemainingValue(BigDecimal milestoneRemainingValue) {
		this.milestoneRemainingValue = milestoneRemainingValue;
	}

	public String getMielstoneType() {
		return this.mielstoneType;
	}

	public void setMielstoneType(String mielstoneType) {
		this.mielstoneType = mielstoneType;
	}

	public String getMilestoneName() {
		return this.milestoneName;
	}

	public void setMilestoneName(String milestoneName) {
		this.milestoneName = milestoneName;
	}

	public BigDecimal getMilestoneValue() {
		return this.milestoneValue;
	}

	public void setMilestoneValue(BigDecimal milestoneValue) {
		this.milestoneValue = milestoneValue;
	}

	public String getUpdatedComments() {
		return this.updatedComments;
	}

	public void setUpdatedComments(String updatedComments) {
		this.updatedComments = updatedComments;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getMilestoneHistory() {
		return milestoneHistory;
	}

	public void setMilestoneHistory(String milestoneHistory) {
		this.milestoneHistory = milestoneHistory;
	}

	public DatComponentBO getDatComponent() {
		return datComponent;
	}

	public void setDatComponent(DatComponentBO datComponent) {
		this.datComponent = datComponent;
	}

	public DatEmpDetailBO getDatEmpDetail1() {
		return datEmpDetail1;
	}

	public void setDatEmpDetail1(DatEmpDetailBO datEmpDetail1) {
		this.datEmpDetail1 = datEmpDetail1;
	}

	public MasMilestoneStatusBO getMasMilestoneStatus() {
		return masMilestoneStatus;
	}

	public void setMasMilestoneStatus(MasMilestoneStatusBO masMilestoneStatus) {
		this.masMilestoneStatus = masMilestoneStatus;
	}

	public DatEmpDetailBO getDatEmpDetail2() {
		return datEmpDetail2;
	}

	public void setDatEmpDetail2(DatEmpDetailBO datEmpDetail2) {
		this.datEmpDetail2 = datEmpDetail2;
	}

	public int getFkComponentId() {
		return fkComponentId;
	}

	public void setFkComponentId(int fkComponentId) {
		this.fkComponentId = fkComponentId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public byte getFkMilestoneStatusId() {
		return fkMilestoneStatusId;
	}

	public void setFkMilestoneStatusId(byte fkMilestoneStatusId) {
		this.fkMilestoneStatusId = fkMilestoneStatusId;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getLineItem() {
		return lineItem;
	}

	public void setLineItem(String lineItem) {
		this.lineItem = lineItem;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Date getMilestoneEndDate() {
		return milestoneEndDate;
	}

	public void setMilestoneEndDate(Date milestoneEndDate) {
		this.milestoneEndDate = milestoneEndDate;
	}

	public void setFkComponentId(Integer fkComponentId) {
		this.fkComponentId = fkComponentId;
	}

	@Override
	public String toString() {
		return "DatMilestoneBO [pkMilestoneId=" + pkMilestoneId
				+ ", createdOn=" + createdOn + ", mielstoneType="
				+ mielstoneType + ", milestoneName=" + milestoneName
				+ ", milestoneValue=" + milestoneValue + ", updatedComments="
				+ updatedComments + ", updatedOn=" + updatedOn + ", lineItem="
				+ lineItem + ", quantity=" + quantity + ", unitPrice="
				+ unitPrice + ", milestoneRemainingValue="
				+ milestoneRemainingValue + ", fkComponentId=" + fkComponentId
				+ ", createdBy=" + createdBy + ", fkMilestoneStatusId="
				+ fkMilestoneStatusId + ", updatedBy=" + updatedBy
				+ ", milestoneEndDate=" + milestoneEndDate
				+ ", milestoneHistory=" + milestoneHistory + ", datComponent="
				+ datComponent + ", datEmpDetail1=" + datEmpDetail1
				+ ", masMilestoneStatus=" + masMilestoneStatus
				+ ", datEmpDetail2=" + datEmpDetail2 + "]";
	}

}