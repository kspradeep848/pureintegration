package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;

import java.util.Date;


/**
 * The persistent class for the dat_sow_upload_history database table.
 * 
 */
@Entity
@Table(name="dat_sow_upload_history")
@NamedQuery(name="DatSowUploadHistoryBO.findAll", query="SELECT d FROM DatSowUploadHistoryBO d")
public class DatSowUploadHistoryBO implements Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_sow_upload_id")
	private int pkSowUploadId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="sow_file_name")
	private String sowFileName;

	@Column(name="sow_file_ref_name")
	private String sowRefFileName;
	
/*	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;*/

	@Column(name="version_id")
	private short versionId;

	@Column(name="created_by")
	private Integer createdBy;
	
/*	@Column(name="updated_by")
	private Integer updatedBy;*/
	
	@Column(name="fk_workorder_id")
	private int fkWorkorderId;
	
	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail1;

	//bi-directional many-to-one association to DatEmpDetail
	/*@OneToOne
	@JoinColumn(name="updated_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail2;*/

	//bi-directional many-to-one association to DatWorkorder
	@OneToOne
	@JoinColumn(name="fk_workorder_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datWorkorder;

	public DatSowUploadHistoryBO() {
	}

	public int getPkSowUploadId() {
		return this.pkSowUploadId;
	}

	public void setPkSowUploadId(int pkSowUploadId) {
		this.pkSowUploadId = pkSowUploadId;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getSowFileName() {
		return this.sowFileName;
	}

	public void setSowFileName(String sowFileName) {
		this.sowFileName = sowFileName;
	}

	/*public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}*/

	public short getVersionId() {
		return this.versionId;
	}

	public void setVersionId(short versionId) {
		this.versionId = versionId;
	}

	public DatEmpDetailBO getDatEmpDetail1() {
		return datEmpDetail1;
	}

	public void setDatEmpDetail1(DatEmpDetailBO datEmpDetail1) {
		this.datEmpDetail1 = datEmpDetail1;
	}

	/*public DatEmpDetailBO getDatEmpDetail2() {
		return datEmpDetail2;
	}

	public void setDatEmpDetail2(DatEmpDetailBO datEmpDetail2) {
		this.datEmpDetail2 = datEmpDetail2;
	}*/

	public DatWorkorderBO getDatWorkorder() {
		return datWorkorder;
	}

	public void setDatWorkorder(DatWorkorderBO datWorkorder) {
		this.datWorkorder = datWorkorder;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public int getFkWorkorderId() {
		return fkWorkorderId;
	}

	public void setFkWorkorderId(int fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}
	
	public String getSowRefFileName() {
		return sowRefFileName;
	}

	public void setSowRefFileName(String sowRefFileName) {
		this.sowRefFileName = sowRefFileName;
	}

	@Override
	public String toString() {
		return "DatSowUploadHistoryBO [pkSowUploadId=" + pkSowUploadId
				+ ", createdOn=" + createdOn + ", sowFileName=" + sowFileName
				+ ", versionId=" + versionId + ", createdBy=" + createdBy
				+ ", fkWorkorderId=" + fkWorkorderId + ", datEmpDetail1="
				+ datEmpDetail1 + ", datWorkorder=" + datWorkorder + "]";
	}

	
}