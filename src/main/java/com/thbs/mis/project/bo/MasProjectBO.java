package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasAccountBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.bo.MasDomainBO;
import com.thbs.mis.common.bo.MasRegionBO;
import com.thbs.mis.common.bo.MasVerticalBO;
import com.thbs.mis.framework.util.CustomDateSerializer;
import com.thbs.mis.project.bean.ProjectNamesOutputBean;
import com.thbs.mis.project.bean.ProjectPercentCalculationOutputBean;


/**
 * The persistent class for the mas_projects database table.
 * 
 */
@Entity
@Table(name="mas_projects")
@NamedQuery(name="MasProjectBO.findAll", query="SELECT m FROM MasProjectBO m")
@SqlResultSetMappings({
@SqlResultSetMapping(name = "projectDetailsMapping", classes = { @ConstructorResult(targetClass = ProjectNamesOutputBean.class, 
columns = {
	@ColumnResult(name = "pk_project_id"),
	@ColumnResult(name = "project_name")})}),
	
@SqlResultSetMapping(name = "projectPercentageMapping", classes = { @ConstructorResult(targetClass = ProjectPercentCalculationOutputBean.class, 
columns = {
	@ColumnResult(name = "sumOfTriggeredValue",type=BigDecimal.class),
	@ColumnResult(name = "sowamount",type=BigDecimal.class)})})
})


@NamedNativeQueries({
	@NamedNativeQuery(name = "MasProjectBO.getProjectNames", 
			query = "select  m.project_name,m.pk_project_id from mas_projects m "
					+ " inner join project_level_user_access_group p on m.pk_project_id = p.fk_project_id"
					+ " where  p.fk_user_id =:empId"
					+ " union "
					+ " select  m.project_name,m.pk_project_id from mas_projects m"
					+ " inner join proj_dat_project_mapped_to_project_coordinator map "
					+ " on map.fk_project_id = m.pk_project_id "
					+ " where  map.fk_project_coordinator_id =:empId",resultSetMapping = "projectDetailsMapping"),
	
	@NamedNativeQuery(name = "MasProjectBO.getProjectPercentage",
			query ="select a.sowamount,b.sumOfTriggeredValue"
					+ " from (SELECT fk_project_id , sum(sow_amount) as sowamount "
					+ " FROM DevProjectschema.dat_workorder where pk_workorder_id in"
					+ " (select distinct(pk_workorder_id) from DevProjectschema.dat_workorder d"
					+ " left outer join dat_component on pk_workorder_id = fk_workorder_id"
					+ "	left outer join dat_milestone on pk_component_id = fk_component_id"
					+ "	left outer join proj_dat_milestone_trigger_details "
					+ " on pk_milestone_id = fk_milestone_id "
					+ " where d.fk_project_id =:prjId"
					+ " )) as a"
					+ "	join (select prj.pk_project_id, sum(mt.trigger_value) as sumOfTriggeredValue"
					+ "	from DevProjectschema.proj_dat_milestone_trigger_details mt"
					+ "	inner join dat_milestone m"
					+ "	on mt.fk_milestone_id=m.pk_milestone_id"
					+ "	inner join dat_component c on c.pk_component_id = m.fk_component_id"
					+ "	inner join dat_workorder wo on wo.pk_workorder_id = c.fk_workorder_id"
					+ "	inner join mas_projects prj on prj.pk_project_id = wo.fk_project_id"
					+ "	where prj.pk_project_id =:prjId) as b ON a.fk_project_id = b.pk_project_id",
					resultSetMapping = "projectPercentageMapping")          
})

public class MasProjectBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_project_id")
	private int pkProjectId;

	@Column(name="comments_for_updation")
	private String commentsForUpdation;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="project_billable_flag")
	private String projectBillableFlag;

	@Column(name="project_description")
	private String projectDescription;

	@Temporal(TemporalType.DATE)
	@Column(name="project_end_date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date projectEndDate;

	@Column(name="project_name")
	private String projectName;

	@Temporal(TemporalType.DATE)
	@Column(name="project_start_date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date projectStartDate;

	@Column(name="project_type")
	private String projectType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;

	@Column(name="fk_account_id")
	private short fkAccountId;
	
	@Column(name="fk_account_mgr_id")
	private Integer fkAccountMgrId;
	
	@Column(name="fk_bu_unit_id")
	private short fkBuUnitId;
	
	@Column(name="fk_classification_id")
	private byte fkClassificationId;
	
	@Column(name="fk_client_id")
	private short fkClientId;
	
	@Column(name="created_by")
	private Integer createdBy;
	
	@Column(name="fk_delivery_mgr_id")
	private Integer fkDeliveryMgrId;
	
	@Column(name="fk_domain_id")
	private short fkDomainId;
	
	@Column(name="fk_domain_mgr_id")
	private Integer fkDomainMgrId;
	
	@Column(name="project_status_id")
	private byte projectStatusId;
	
	@Column(name="updated_by")
	private Integer updatedBy;
	
	@Column(name="fk_vertical_id")
	private short fkVerticalId;
	
	@Column(name="fk_region_id")
	private short fkRegionId;
	
	@Column(name="is_having_aggregate_po")
	private String isHavingAggregatePo;
	
	@Column(name="proj_history")
	private String projHistory;
	
	
	//bi-directional many-to-one association to MasAccount
	@OneToOne
	@JoinColumn(name="fk_account_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasAccountBO masAccount;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="fk_account_mgr_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail1;

	//bi-directional many-to-one association to MasBuUnit
	@OneToOne
	@JoinColumn(name="fk_bu_unit_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasBuUnitBO masBuUnit;

	//bi-directional many-to-one association to ProjMasProjectClassification
	@OneToOne
	@JoinColumn(name="fk_classification_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasProjectClassificationBO projMasProjectClassification;

	//bi-directional many-to-one association to MasClient
	@OneToOne
	@JoinColumn(name="fk_client_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasClientBO masClient;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail2;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="fk_delivery_mgr_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail3;

	//bi-directional many-to-one association to MasDomain
	@OneToOne
	@JoinColumn(name="fk_domain_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasDomainBO masDomain;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="fk_domain_mgr_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail4;

	//bi-directional many-to-one association to ProjMasProjectStatus
	@OneToOne
	@JoinColumn(name="project_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasProjectStatusBO projMasProjectStatus;
	
	//bi-directional many-to-one association to MasRegion
	@OneToOne
	@JoinColumn(name="fk_region_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasRegionBO masRegion;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="updated_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail5;

	//bi-directional many-to-one association to MasVertical
	@OneToOne
	@JoinColumn(name="fk_vertical_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasVerticalBO masVertical;	

	public MasProjectBO(){
		
	}

	public int getPkProjectId() {
		return this.pkProjectId;
	}

	public void setPkProjectId(int pkProjectId) {
		this.pkProjectId = pkProjectId;
	}

	public String getCommentsForUpdation() {
		return this.commentsForUpdation;
	}

	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getProjectBillableFlag() {
		return this.projectBillableFlag;
	}

	public void setProjectBillableFlag(String projectBillableFlag) {
		this.projectBillableFlag = projectBillableFlag;
	}

	public String getProjectDescription() {
		return this.projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public Date getProjectEndDate() {
		return this.projectEndDate;
	}

	public void setProjectEndDate(Date projectEndDate) {
		this.projectEndDate = projectEndDate;
	}

	public String getProjectName() {
		return this.projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Date getProjectStartDate() {
		return this.projectStartDate;
	}

	public void setProjectStartDate(Date projectStartDate) {
		this.projectStartDate = projectStartDate;
	}

	public String getProjectType() {
		return this.projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public MasAccountBO getMasAccount() {
		return masAccount;
	}

	public void setMasAccount(MasAccountBO masAccount) {
		this.masAccount = masAccount;
	}

	public DatEmpDetailBO getDatEmpDetail1() {
		return datEmpDetail1;
	}

	public void setDatEmpDetail1(DatEmpDetailBO datEmpDetail1) {
		this.datEmpDetail1 = datEmpDetail1;
	}

	public MasBuUnitBO getMasBuUnit() {
		return masBuUnit;
	}

	public void setMasBuUnit(MasBuUnitBO masBuUnit) {
		this.masBuUnit = masBuUnit;
	}

	public ProjMasProjectClassificationBO getProjMasProjectClassification() {
		return projMasProjectClassification;
	}

	public void setProjMasProjectClassification(
			ProjMasProjectClassificationBO projMasProjectClassification) {
		this.projMasProjectClassification = projMasProjectClassification;
	}

	public MasClientBO getMasClient() {
		return masClient;
	}

	public void setMasClient(MasClientBO masClient) {
		this.masClient = masClient;
	}

	public DatEmpDetailBO getDatEmpDetail2() {
		return datEmpDetail2;
	}

	public void setDatEmpDetail2(DatEmpDetailBO datEmpDetail2) {
		this.datEmpDetail2 = datEmpDetail2;
	}

	public DatEmpDetailBO getDatEmpDetail3() {
		return datEmpDetail3;
	}

	public void setDatEmpDetail3(DatEmpDetailBO datEmpDetail3) {
		this.datEmpDetail3 = datEmpDetail3;
	}

	public MasDomainBO getMasDomain() {
		return masDomain;
	}

	public void setMasDomain(MasDomainBO masDomain) {
		this.masDomain = masDomain;
	}

	public DatEmpDetailBO getDatEmpDetail4() {
		return datEmpDetail4;
	}

	public void setDatEmpDetail4(DatEmpDetailBO datEmpDetail4) {
		this.datEmpDetail4 = datEmpDetail4;
	}

	public ProjMasProjectStatusBO getProjMasProjectStatus() {
		return projMasProjectStatus;
	}

	public void setProjMasProjectStatus(ProjMasProjectStatusBO projMasProjectStatus) {
		this.projMasProjectStatus = projMasProjectStatus;
	}

	public DatEmpDetailBO getDatEmpDetail5() {
		return datEmpDetail5;
	}

	public void setDatEmpDetail5(DatEmpDetailBO datEmpDetail5) {
		this.datEmpDetail5 = datEmpDetail5;
	}

	public MasVerticalBO getMasVertical() {
		return masVertical;
	}

	public void setMasVertical(MasVerticalBO masVertical) {
		this.masVertical = masVertical;
	}

	public short getFkAccountId() {
		return fkAccountId;
	}

	public void setFkAccountId(short fkAccountId) {
		this.fkAccountId = fkAccountId;
	}

	public Integer getFkAccountMgrId() {
		return fkAccountMgrId;
	}

	public void setFkAccountMgrId(Integer fkAccountMgrId) {
		this.fkAccountMgrId = fkAccountMgrId;
	}

	public short getFkBuUnitId() {
		return fkBuUnitId;
	}

	public void setFkBuUnitId(short fkBuUnitId) {
		this.fkBuUnitId = fkBuUnitId;
	}

	public byte getFkClassificationId() {
		return fkClassificationId;
	}

	public void setFkClassificationId(byte fkClassificationId) {
		this.fkClassificationId = fkClassificationId;
	}

	public short getFkClientId() {
		return fkClientId;
	}

	public void setFkClientId(short fkClientId) {
		this.fkClientId = fkClientId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getFkDeliveryMgrId() {
		return fkDeliveryMgrId;
	}

	public void setFkDeliveryMgrId(Integer fkDeliveryMgrId) {
		this.fkDeliveryMgrId = fkDeliveryMgrId;
	}

	public short getFkDomainId() {
		return fkDomainId;
	}

	public void setFkDomainId(short fkDomainId) {
		this.fkDomainId = fkDomainId;
	}

	public Integer getFkDomainMgrId() {
		return fkDomainMgrId;
	}

	public void setFkDomainMgrId(Integer fkDomainMgrId) {
		this.fkDomainMgrId = fkDomainMgrId;
	}

	public byte getProjectStatusId() {
		return projectStatusId;
	}

	public void setProjectStatusId(byte projectStatusId) {
		this.projectStatusId = projectStatusId;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getFkVerticalId() {
		return fkVerticalId;
	}

	public void setFkVerticalId(short fkVerticalId) {
		this.fkVerticalId = fkVerticalId;
	}

	public short getFkRegionId() {
		return fkRegionId;
	}

	public void setFkRegionId(short fkRegionId) {
		this.fkRegionId = fkRegionId;
	}

	public MasRegionBO getMasRegion() {
		return masRegion;
	}

	public void setMasRegion(MasRegionBO masRegion) {
		this.masRegion = masRegion;
	}

	public String getIsHavingAggregatePo() {
		return isHavingAggregatePo;
	}

	public void setIsHavingAggregatePo(String isHavingAggregatePo) {
		this.isHavingAggregatePo = isHavingAggregatePo;
	}

	public String getProjHistory() {
		return projHistory;
	}

	public void setProjHistory(String projHistory) {
		this.projHistory = projHistory;
	}

	@Override
	public String toString() {
		return "MasProjectBO [pkProjectId=" + pkProjectId
				+ ", commentsForUpdation=" + commentsForUpdation
				+ ", createdOn=" + createdOn + ", projectBillableFlag="
				+ projectBillableFlag + ", projectDescription="
				+ projectDescription + ", projectEndDate=" + projectEndDate
				+ ", projectName=" + projectName + ", projectStartDate="
				+ projectStartDate + ", projectType=" + projectType
				+ ", updatedOn=" + updatedOn + ", fkAccountId=" + fkAccountId
				+ ", fkAccountMgrId=" + fkAccountMgrId + ", fkBuUnitId="
				+ fkBuUnitId + ", fkClassificationId=" + fkClassificationId
				+ ", fkClientId=" + fkClientId + ", createdBy=" + createdBy
				+ ", fkDeliveryMgrId=" + fkDeliveryMgrId + ", fkDomainId="
				+ fkDomainId + ", fkDomainMgrId=" + fkDomainMgrId
				+ ", projectStatusId=" + projectStatusId + ", updatedBy="
				+ updatedBy + ", fkVerticalId=" + fkVerticalId
				+ ", fkRegionId=" + fkRegionId + ", isHavingAggregatePo="
				+ isHavingAggregatePo + ", projHistory=" + projHistory
				+ ", masAccount=" + masAccount + ", datEmpDetail1="
				+ datEmpDetail1 + ", masBuUnit=" + masBuUnit
				+ ", projMasProjectClassification="
				+ projMasProjectClassification + ", masClient=" + masClient
				+ ", datEmpDetail2=" + datEmpDetail2 + ", datEmpDetail3="
				+ datEmpDetail3 + ", masDomain=" + masDomain
				+ ", datEmpDetail4=" + datEmpDetail4
				+ ", projMasProjectStatus=" + projMasProjectStatus
				+ ", masRegion=" + masRegion + ", datEmpDetail5="
				+ datEmpDetail5 + ", masVertical=" + masVertical + "]";
	}

	
	
}