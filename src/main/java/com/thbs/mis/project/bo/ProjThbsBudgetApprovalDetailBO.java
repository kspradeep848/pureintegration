package com.thbs.mis.project.bo;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the proj_thbs_budget_approval_details database table.
 * 
 */
@Entity
@Table(name="proj_thbs_budget_approval_details")
@NamedQuery(name="ProjThbsBudgetApprovalDetailBO.findAll", query="SELECT p FROM ProjThbsBudgetApprovalDetailBO p")
public class ProjThbsBudgetApprovalDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_thbs_budget_approval_details_id")
	private Integer pkThbsBudgetApprovalDetailsId;

	@Column(name="approved_or_rejected_by")
	private Integer approvedOrRejectedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="approved_or_rejected_on")
	private Date approvedOrRejectedOn;

	@Column(name="requested_by")
	private Integer requestedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="requested_on")
	private Date requestedOn;
	
	@Column(name="fk_budget_status_id")
	private Integer fkBudgetStatusId;

	@Column(name="fk_thbs_budget_id")
	private Integer fkThbsBudgetId;

	

	//bi-directional many-to-one association to ProjBudgetStatus
	@OneToOne
	@JoinColumn(name="fk_budget_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjBudgetStatusBO projBudgetStatusBO;

	//bi-directional many-to-one association to ProjDatThbsBudget
	@OneToOne
	@JoinColumn(name="fk_thbs_budget_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjDatThbsBudgetBO projDatThbsBudgetBO;

	public ProjThbsBudgetApprovalDetailBO() {
	}

	public int getPkThbsBudgetApprovalDetailsId() {
		return this.pkThbsBudgetApprovalDetailsId;
	}

	public void setPkThbsBudgetApprovalDetailsId(int pkThbsBudgetApprovalDetailsId) {
		this.pkThbsBudgetApprovalDetailsId = pkThbsBudgetApprovalDetailsId;
	}

	public Date getApprovedOrRejectedOn() {
		return this.approvedOrRejectedOn;
	}

	public void setApprovedOrRejectedOn(Date approvedOrRejectedOn) {
		this.approvedOrRejectedOn = approvedOrRejectedOn;
	}

	public int getRequestedBy() {
		return this.requestedBy;
	}

	public void setRequestedBy(int requestedBy) {
		this.requestedBy = requestedBy;
	}

	public Date getRequestedOn() {
		return this.requestedOn;
	}

	public void setRequestedOn(Date requestedOn) {
		this.requestedOn = requestedOn;
	}

	public ProjBudgetStatusBO getProjBudgetStatusBO() {
		return projBudgetStatusBO;
	}

	public void setProjBudgetStatusBO(ProjBudgetStatusBO projBudgetStatusBO) {
		this.projBudgetStatusBO = projBudgetStatusBO;
	}

	public ProjDatThbsBudgetBO getProjDatThbsBudgetBO() {
		return projDatThbsBudgetBO;
	}

	public void setProjDatThbsBudgetBO(ProjDatThbsBudgetBO projDatThbsBudgetBO) {
		this.projDatThbsBudgetBO = projDatThbsBudgetBO;
	}
	
	public Integer getFkBudgetStatusId() {
		return fkBudgetStatusId;
	}

	public void setFkBudgetStatusId(Integer fkBudgetStatusId) {
		this.fkBudgetStatusId = fkBudgetStatusId;
	}

	public Integer getFkThbsBudgetId() {
		return fkThbsBudgetId;
	}

	public void setFkThbsBudgetId(Integer fkThbsBudgetId) {
		this.fkThbsBudgetId = fkThbsBudgetId;
	}

	public void setPkThbsBudgetApprovalDetailsId(
			Integer pkThbsBudgetApprovalDetailsId) {
		this.pkThbsBudgetApprovalDetailsId = pkThbsBudgetApprovalDetailsId;
	}

	public Integer getApprovedOrRejectedBy() {
		return approvedOrRejectedBy;
	}

	public void setApprovedOrRejectedBy(Integer approvedOrRejectedBy) {
		this.approvedOrRejectedBy = approvedOrRejectedBy;
	}

	public void setRequestedBy(Integer requestedBy) {
		this.requestedBy = requestedBy;
	}

	@Override
	public String toString() {
		return "ProjThbsBudgetApprovalDetailBO [pkThbsBudgetApprovalDetailsId="
				+ pkThbsBudgetApprovalDetailsId + ", approvedOrRejectedBy="
				+ approvedOrRejectedBy + ", approvedOrRejectedOn="
				+ approvedOrRejectedOn + ", requestedBy=" + requestedBy
				+ ", requestedOn=" + requestedOn + ", fkBudgetStatusId="
				+ fkBudgetStatusId + ", fkThbsBudgetId=" + fkThbsBudgetId
				+ ", projBudgetStatusBO=" + projBudgetStatusBO
				+ ", projDatThbsBudgetBO=" + projDatThbsBudgetBO + "]";
	}

}