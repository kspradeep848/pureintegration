package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
@Entity
@Table(name="mas_rate_type")
@NamedQuery(name="MasRateTypeBO.findAll", query="SELECT m FROM MasRateTypeBO m")
public class MasRateTypeBO implements Serializable{

	private static final long serialVersionUID = 1L;

	@javax.persistence.Id
	@javax.persistence.GeneratedValue(strategy=GenerationType.IDENTITY)
	@javax.persistence.Column(name="pk_rate_type_id")
	private Byte pkRateTypeId;

	@javax.persistence.Column(name="rate_type_name")
	private String rateTypeName;
	
	public Byte getPkRateTypeId() {
		return pkRateTypeId;
	}

	public void setPkRateTypeId(Byte pkRateTypeId) {
		this.pkRateTypeId = pkRateTypeId;
	}

	public String getRateTypeName() {
		return rateTypeName;
	}

	public void setRateTypeName(String rateTypeName) {
		this.rateTypeName = rateTypeName;
	}

	@Override
	public String toString() {
		return "MasRateTypeBO [pkRateTypeId=" + pkRateTypeId
				+ ", rateTypeName=" + rateTypeName + "]";
	}

}
