package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the proj_mas_wo_status database table.
 * 
 */
@Entity
@Table(name="proj_mas_wo_status")
@NamedQuery(name="ProjMasWoStatusBO.findAll", query="SELECT p FROM ProjMasWoStatusBO p")
public class ProjMasWoStatusBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_workorder_status_id")
	private byte pkWorkorderStatusId;

	@Column(name="workorder_status")
	private String workorderStatus;
	
	public ProjMasWoStatusBO() {
	}

	public byte getPkWorkorderStatusId() {
		return this.pkWorkorderStatusId;
	}

	public void setPkWorkorderStatusId(byte pkWorkorderStatusId) {
		this.pkWorkorderStatusId = pkWorkorderStatusId;
	}

	public String getWorkorderStatus() {
		return this.workorderStatus;
	}

	public void setWorkorderStatus(String workorderStatus) {
		this.workorderStatus = workorderStatus;
	}

	@Override
	public String toString() {
		return "ProjMasWoStatusBO [pkWorkorderStatusId=" + pkWorkorderStatusId
				+ ", workorderStatus=" + workorderStatus + "]";
	}

	
}