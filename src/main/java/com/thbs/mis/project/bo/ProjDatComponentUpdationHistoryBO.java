package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;

import java.util.Date;


/**
 * The persistent class for the proj_dat_component_updation_history database table.
 * 
 */
@Entity
@Table(name="proj_dat_component_updation_history")
@NamedQuery(name="ProjDatComponentUpdationHistoryBO.findAll", query="SELECT p FROM ProjDatComponentUpdationHistoryBO p")
public class ProjDatComponentUpdationHistoryBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_comp_updation_id")
	private int pkCompUpdationId;

	@Column(name="created_or_updated_comments")
	private String createdOrUpdatedComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="fk_created_or_updated_on")
	private Date fkCreatedOrUpdatedOn;
	
	@Column(name="fk_component_id")
	private int fkComponentId;
	
	@Column(name="fk_created_or_updated_by")
	private Integer fkCreatedOrUpdatedBy;

	//bi-directional many-to-one association to DatComponent
	@OneToOne
	@JoinColumn(name="fk_component_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatComponentBO datComponent;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="fk_created_or_updated_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail;

	public ProjDatComponentUpdationHistoryBO() {
	}

	public int getPkCompUpdationId() {
		return this.pkCompUpdationId;
	}

	public void setPkCompUpdationId(int pkCompUpdationId) {
		this.pkCompUpdationId = pkCompUpdationId;
	}

	public String getCreatedOrUpdatedComments() {
		return this.createdOrUpdatedComments;
	}

	public void setCreatedOrUpdatedComments(String createdOrUpdatedComments) {
		this.createdOrUpdatedComments = createdOrUpdatedComments;
	}

	public Date getFkCreatedOrUpdatedOn() {
		return this.fkCreatedOrUpdatedOn;
	}

	public void setFkCreatedOrUpdatedOn(Date fkCreatedOrUpdatedOn) {
		this.fkCreatedOrUpdatedOn = fkCreatedOrUpdatedOn;
	}

	public DatComponentBO getDatComponent() {
		return this.datComponent;
	}

	public void setDatComponent(DatComponentBO datComponent) {
		this.datComponent = datComponent;
	}

	public DatEmpDetailBO getDatEmpDetail() {
		return this.datEmpDetail;
	}

	public void setDatEmpDetail(DatEmpDetailBO datEmpDetail) {
		this.datEmpDetail = datEmpDetail;
	}

	public int getFkComponentId() {
		return fkComponentId;
	}

	public void setFkComponentId(int fkComponentId) {
		this.fkComponentId = fkComponentId;
	}

	public Integer getFkCreatedOrUpdatedBy() {
		return fkCreatedOrUpdatedBy;
	}

	public void setFkCreatedOrUpdatedBy(Integer fkCreatedOrUpdatedBy) {
		this.fkCreatedOrUpdatedBy = fkCreatedOrUpdatedBy;
	}

	@Override
	public String toString() {
		return "ProjDatComponentUpdationHistoryBO [pkCompUpdationId="
				+ pkCompUpdationId + ", createdOrUpdatedComments="
				+ createdOrUpdatedComments + ", fkCreatedOrUpdatedOn="
				+ fkCreatedOrUpdatedOn + ", fkComponentId=" + fkComponentId
				+ ", fkCreatedOrUpdatedBy=" + fkCreatedOrUpdatedBy
				+ ", datComponent=" + datComponent + ", datEmpDetail="
				+ datEmpDetail + "]";
	}

	

	
}