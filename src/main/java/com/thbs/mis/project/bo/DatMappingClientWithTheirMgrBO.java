package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the dat_mapping_client_with_their_mgrs database table.
 * 
 */
@Entity
@Table(name="dat_mapping_client_with_their_mgrs")
@NamedQuery(name="DatMappingClientWithTheirMgrBO.findAll", query="SELECT d FROM DatMappingClientWithTheirMgrBO d")
public class DatMappingClientWithTheirMgrBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_mapping_client_mgrs_id")
	private Integer pkMappingClientMgrsId;
	
	@Column(name="fk_client_id")
	private Short fkClientId;

	@Column(name="fk_client_manager_id")
	private Short fkClientManagerId;
	
	//bi-directional many-to-one association to MasClient
	@OneToOne
	@JoinColumn(name="fk_client_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasClientBO masClient;

	//bi-directional many-to-one association to ProjMasClientManager
	@OneToOne
	@JoinColumn(name="fk_client_manager_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasClientManagerBO projMasClientManager;

	public DatMappingClientWithTheirMgrBO() {
	}


	public Integer getPkMappingClientMgrsId() {
		return pkMappingClientMgrsId;
	}


	public void setPkMappingClientMgrsId(Integer pkMappingClientMgrsId) {
		this.pkMappingClientMgrsId = pkMappingClientMgrsId;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public Short getFkClientId() {
		return fkClientId;
	}

	public void setFkClientId(Short fkClientId) {
		this.fkClientId = fkClientId;
	}

	public Short getFkClientManagerId() {
		return fkClientManagerId;
	}

	public void setFkClientManagerId(Short fkClientManagerId) {
		this.fkClientManagerId = fkClientManagerId;
	}

	public MasClientBO getMasClient() {
		return masClient;
	}

	public void setMasClient(MasClientBO masClient) {
		this.masClient = masClient;
	}

	public ProjMasClientManagerBO getProjMasClientManager() {
		return projMasClientManager;
	}

	public void setProjMasClientManager(ProjMasClientManagerBO projMasClientManager) {
		this.projMasClientManager = projMasClientManager;
	}

	@Override
	public String toString() {
		return "DatMappingClientWithTheirMgrBO [pkMappingClientMgrsId="
				+ pkMappingClientMgrsId + ", fkClientId=" + fkClientId
				+ ", fkClientManagerId=" + fkClientManagerId + ", masClient="
				+ masClient + ", projMasClientManager=" + projMasClientManager
				+ "]";
	}

}