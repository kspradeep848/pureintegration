package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasAccountBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.bo.MasDomainBO;
import com.thbs.mis.common.bo.MasRegionBO;
import com.thbs.mis.common.bo.MasVerticalBO;


/**
 * The persistent class for the mas_project_audit database table.
 * 
 */
@Entity
@Table(name="mas_project_audit")
@NamedQuery(name="MasProjectAuditBO.findAll", query="SELECT m FROM MasProjectAuditBO m")
public class MasProjectAuditBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_project_audit_id")
	private int pkProjectAuditId;

	@Column(name="action")
	private String action;

	@Column(name="comments_for_updation")
	private String commentsForUpdation;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="entered_on")
	private Date enteredOn;

	@Column(name="project_billable_flag")
	private String projectBillableFlag;

	@Column(name="project_description")
	private String projectDescription;

	@Temporal(TemporalType.DATE)
	@Column(name="project_end_date")
	private Date projectEndDate;

	@Column(name="project_name")
	private String projectName;

	@Temporal(TemporalType.DATE)
	@Column(name="project_start_date")
	private Date projectStartDate;

	@Column(name="project_type")
	private String projectType;
	
	@Column(name="is_having_aggregate_po")
	private String isHavingAggregatePo;
	
	@Column(name="fk_project_id")
	private Integer fkProjectId;
	
	@Column(name="entered_by")
	private Integer enteredBy;
	
	@Column(name="proj_history")
	private String projHistory;
	
	//bi-directional many-to-one association to MasAccount
	@OneToOne
	@JoinColumn(name="fk_account_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasAccountBO masProjectAuditAccountId;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="fk_account_mgr_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO masProjectAuditAccountMgrId;

	//bi-directional many-to-one association to MasBuUnit
	@OneToOne
	@JoinColumn(name="fk_bu_unit_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasBuUnitBO masProjectAuditBuId;

	//bi-directional many-to-one association to ProjMasProjectClassification
	@OneToOne
	@JoinColumn(name="fk_classification_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasProjectClassificationBO masProjectAuditClassificationId;

	//bi-directional many-to-one association to MasClient
	@OneToOne
	@JoinColumn(name="fk_client_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasClientBO masProjectAuditClientId;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="fk_delivery_mgr_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO masProjectAuditDeliveryMgrId;

	//bi-directional many-to-one association to MasDomain
	@OneToOne
	@JoinColumn(name="fk_domain_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasDomainBO masProjectAuditDomainId;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="fk_domain_mgr_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO masProjectAuditDomainMgrId;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="entered_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO masProjectAuditEnteredBy;

	//bi-directional many-to-one association to MasProject
	@OneToOne
	@JoinColumn(name="fk_project_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasProjectBO masProjectAuditProjectId;

	//bi-directional many-to-one association to ProjMasProjectStatus
	@OneToOne
	@JoinColumn(name="project_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasProjectStatusBO masProjectAuditProjectStatusId;

	//bi-directional many-to-one association to MasRegion
	@OneToOne
	@JoinColumn(name="fk_region_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasRegionBO masProjectAuditRegionId;

	//bi-directional many-to-one association to MasVertical
	@OneToOne
	@JoinColumn(name="fk_vertical_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasVerticalBO masProjectAuditVerticalId;

	public MasProjectAuditBO() {
	}

	public int getPkProjectAuditId() {
		return this.pkProjectAuditId;
	}

	public void setPkProjectAuditId(int pkProjectAuditId) {
		this.pkProjectAuditId = pkProjectAuditId;
	}

	public String getAction() {
		return this.action;
	}
	

	public Integer getFkProjectId() {
		return fkProjectId;
	}

	public void setFkProjectId(Integer fkProjectId) {
		this.fkProjectId = fkProjectId;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCommentsForUpdation() {
		return this.commentsForUpdation;
	}

	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}

	public Date getEnteredOn() {
		return this.enteredOn;
	}

	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}

	public String getProjectBillableFlag() {
		return this.projectBillableFlag;
	}

	public void setProjectBillableFlag(String projectBillableFlag) {
		this.projectBillableFlag = projectBillableFlag;
	}

	public String getProjectDescription() {
		return this.projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public Date getProjectEndDate() {
		return this.projectEndDate;
	}

	public void setProjectEndDate(Date projectEndDate) {
		this.projectEndDate = projectEndDate;
	}

	public String getProjectName() {
		return this.projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjHistory() {
		return projHistory;
	}

	public void setProjHistory(String projHistory) {
		this.projHistory = projHistory;
	}

	public Date getProjectStartDate() {
		return this.projectStartDate;
	}

	public void setProjectStartDate(Date projectStartDate) {
		this.projectStartDate = projectStartDate;
	}

	public String getProjectType() {
		return this.projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public MasAccountBO getMasProjectAuditAccountId() {
		return masProjectAuditAccountId;
	}

	public void setMasProjectAuditAccountId(MasAccountBO masProjectAuditAccountId) {
		this.masProjectAuditAccountId = masProjectAuditAccountId;
	}

	public DatEmpDetailBO getMasProjectAuditAccountMgrId() {
		return masProjectAuditAccountMgrId;
	}

	public void setMasProjectAuditAccountMgrId(
			DatEmpDetailBO masProjectAuditAccountMgrId) {
		this.masProjectAuditAccountMgrId = masProjectAuditAccountMgrId;
	}

	public MasBuUnitBO getMasProjectAuditBuId() {
		return masProjectAuditBuId;
	}

	public void setMasProjectAuditBuId(MasBuUnitBO masProjectAuditBuId) {
		this.masProjectAuditBuId = masProjectAuditBuId;
	}

	public ProjMasProjectClassificationBO getMasProjectAuditClassificationId() {
		return masProjectAuditClassificationId;
	}

	public void setMasProjectAuditClassificationId(
			ProjMasProjectClassificationBO masProjectAuditClassificationId) {
		this.masProjectAuditClassificationId = masProjectAuditClassificationId;
	}

	public MasClientBO getMasProjectAuditClientId() {
		return masProjectAuditClientId;
	}

	public void setMasProjectAuditClientId(MasClientBO masProjectAuditClientId) {
		this.masProjectAuditClientId = masProjectAuditClientId;
	}

	public DatEmpDetailBO getMasProjectAuditDeliveryMgrId() {
		return masProjectAuditDeliveryMgrId;
	}

	public void setMasProjectAuditDeliveryMgrId(
			DatEmpDetailBO masProjectAuditDeliveryMgrId) {
		this.masProjectAuditDeliveryMgrId = masProjectAuditDeliveryMgrId;
	}

	public MasDomainBO getMasProjectAuditDomainId() {
		return masProjectAuditDomainId;
	}

	public void setMasProjectAuditDomainId(MasDomainBO masProjectAuditDomainId) {
		this.masProjectAuditDomainId = masProjectAuditDomainId;
	}

	public DatEmpDetailBO getMasProjectAuditDomainMgrId() {
		return masProjectAuditDomainMgrId;
	}

	public void setMasProjectAuditDomainMgrId(
			DatEmpDetailBO masProjectAuditDomainMgrId) {
		this.masProjectAuditDomainMgrId = masProjectAuditDomainMgrId;
	}

	public DatEmpDetailBO getMasProjectAuditEnteredBy() {
		return masProjectAuditEnteredBy;
	}

	public void setMasProjectAuditEnteredBy(DatEmpDetailBO masProjectAuditEnteredBy) {
		this.masProjectAuditEnteredBy = masProjectAuditEnteredBy;
	}

	public MasProjectBO getMasProjectAuditProjectId() {
		return masProjectAuditProjectId;
	}

	public void setMasProjectAuditProjectId(MasProjectBO masProjectAuditProjectId) {
		this.masProjectAuditProjectId = masProjectAuditProjectId;
	}

	public ProjMasProjectStatusBO getMasProjectAuditProjectStatusId() {
		return masProjectAuditProjectStatusId;
	}

	public void setMasProjectAuditProjectStatusId(
			ProjMasProjectStatusBO masProjectAuditProjectStatusId) {
		this.masProjectAuditProjectStatusId = masProjectAuditProjectStatusId;
	}

	public MasRegionBO getMasProjectAuditRegionId() {
		return masProjectAuditRegionId;
	}

	public void setMasProjectAuditRegionId(MasRegionBO masProjectAuditRegionId) {
		this.masProjectAuditRegionId = masProjectAuditRegionId;
	}

	public MasVerticalBO getMasProjectAuditVerticalId() {
		return masProjectAuditVerticalId;
	}

	public void setMasProjectAuditVerticalId(MasVerticalBO masProjectAuditVerticalId) {
		this.masProjectAuditVerticalId = masProjectAuditVerticalId;
	}

	public String getIsHavingAggregatePo() {
		return isHavingAggregatePo;
	}

	public void setIsHavingAggregatePo(String isHavingAggregatePo) {
		this.isHavingAggregatePo = isHavingAggregatePo;
	}

	public Integer getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(Integer enteredBy) {
		this.enteredBy = enteredBy;
	}

	@Override
	public String toString() {
		return "MasProjectAuditBO [pkProjectAuditId=" + pkProjectAuditId
				+ ", action=" + action + ", commentsForUpdation="
				+ commentsForUpdation + ", enteredOn=" + enteredOn
				+ ", projectBillableFlag=" + projectBillableFlag
				+ ", projectDescription=" + projectDescription
				+ ", projectEndDate=" + projectEndDate + ", projectName="
				+ projectName + ", projectStartDate=" + projectStartDate
				+ ", projectType=" + projectType + ", isHavingAggregatePo="
				+ isHavingAggregatePo + ", fkProjectId=" + fkProjectId
				+ ", enteredBy=" + enteredBy + ", masProjectAuditAccountId="
				+ masProjectAuditAccountId + ", masProjectAuditAccountMgrId="
				+ masProjectAuditAccountMgrId + ", masProjectAuditBuId="
				+ masProjectAuditBuId + ", masProjectAuditClassificationId="
				+ masProjectAuditClassificationId
				+ ", masProjectAuditClientId=" + masProjectAuditClientId
				+ ", masProjectAuditDeliveryMgrId="
				+ masProjectAuditDeliveryMgrId + ", masProjectAuditDomainId="
				+ masProjectAuditDomainId + ", masProjectAuditDomainMgrId="
				+ masProjectAuditDomainMgrId + ", masProjectAuditEnteredBy="
				+ masProjectAuditEnteredBy + ", masProjectAuditProjectId="
				+ masProjectAuditProjectId
				+ ", masProjectAuditProjectStatusId="
				+ masProjectAuditProjectStatusId + ", masProjectAuditRegionId="
				+ masProjectAuditRegionId + ", masProjectAuditVerticalId="
				+ masProjectAuditVerticalId + "]";
	}

	
}