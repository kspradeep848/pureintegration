package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the proj_dat_milestone_trigger_details database table.
 * 
 */
@Entity
@Table(name="proj_dat_milestone_trigger_details")
@NamedQuery(name="ProjDatMilestoneTriggerDetailBO.findAll", query="SELECT p FROM ProjDatMilestoneTriggerDetailBO p")
public class ProjDatMilestoneTriggerDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_milestone_trigger_id")
	private Integer pkMilestoneTriggerId;

	@Column(name="fk_invoice_id")
	private Integer fkInvoiceId;

	@Column(name="is_credit_note_raised")
	private String isCreditNoteRaised;

	@Column(name="trigger_comments")
	private String triggerComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="trigger_date")
	private Date triggerDate;

	@Column(name="trigger_value")
	private BigDecimal triggerValue;
	
	@Column(name="trigger_by")
	private Integer triggerBy;
	
	@Column(name="fk_milestone_id")
	private int fkMilestoneId;
	
	/*@Column(name="fk_purchase_order_id")
	private int fkPurchaseOrderId;*/

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="trigger_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail;

	//bi-directional many-to-one association to DatMilestone
	@OneToOne
	@JoinColumn(name="fk_milestone_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatMilestoneBO datMilestone;

	public ProjDatMilestoneTriggerDetailBO() {
	}

	public Integer getPkMilestoneTriggerId() {
		return this.pkMilestoneTriggerId;
	}

	public void setPkMilestoneTriggerId(Integer pkMilestoneTriggerId) {
		this.pkMilestoneTriggerId = pkMilestoneTriggerId;
	}

	public Integer getFkInvoiceId() {
		return this.fkInvoiceId;
	}

	public void setFkInvoiceId(Integer fkInvoiceId) {
		this.fkInvoiceId = fkInvoiceId;
	}

	public String getIsCreditNoteRaised() {
		return this.isCreditNoteRaised;
	}

	public void setIsCreditNoteRaised(String isCreditNoteRaised) {
		this.isCreditNoteRaised = isCreditNoteRaised;
	}

	public String getTriggerComments() {
		return this.triggerComments;
	}

	public void setTriggerComments(String triggerComments) {
		this.triggerComments = triggerComments;
	}

	public Date getTriggerDate() {
		return this.triggerDate;
	}

	public void setTriggerDate(Date triggerDate) {
		this.triggerDate = triggerDate;
	}

	public BigDecimal getTriggerValue() {
		return this.triggerValue;
	}

	public void setTriggerValue(BigDecimal triggerValue) {
		this.triggerValue = triggerValue;
	}

	public Integer getTriggerBy() {
		return triggerBy;
	}

	public void setTriggerBy(Integer triggerBy) {
		this.triggerBy = triggerBy;
	}

	public Integer getFkMilestoneId() {
		return fkMilestoneId;
	}

	public void setFkMilestoneId(Integer fkMilestoneId) {
		this.fkMilestoneId = fkMilestoneId;
	}

	public DatEmpDetailBO getDatEmpDetail() {
		return datEmpDetail;
	}

	public void setDatEmpDetail(DatEmpDetailBO datEmpDetail) {
		this.datEmpDetail = datEmpDetail;
	}

	public DatMilestoneBO getDatMilestone() {
		return datMilestone;
	}

	public void setDatMilestone(DatMilestoneBO datMilestone) {
		this.datMilestone = datMilestone;
	}

	@Override
	public String toString() {
		return "ProjDatMilestoneTriggerDetailBO [pkMilestoneTriggerId="
				+ pkMilestoneTriggerId + ", fkInvoiceId=" + fkInvoiceId
				+ ", isCreditNoteRaised=" + isCreditNoteRaised
				+ ", triggerComments=" + triggerComments + ", triggerDate="
				+ triggerDate + ", triggerValue=" + triggerValue
				+ ", triggerBy=" + triggerBy + ", fkMilestoneId="
				+ fkMilestoneId + ", datEmpDetail=" + datEmpDetail
				+ ", datMilestone=" + datMilestone + "]";
	}

	
	

}