package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;

/**
 * The persistent class for the mas_client_audit database table.
 * 
 */
@Entity
@Table(name = "mas_client_audit")
@NamedQuery(name = "MasClientAuditBO.findAll", query = "SELECT m FROM MasClientAuditBO m")
public class MasClientAuditBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_client_audit_id")
	private int pkClientAuditId;

	@Column(name = "fk_client_id")
	private Short fkClientId;

	@Column(name = "action")
	private String action;

	@Column(name = "client_name")
	private String clientName;

	@Column(name = "client_status")
	private String clientStatus;

	@Column(name = "credit_period")
	private Short creditPeriod;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "entered_on")
	private Date enteredOn;

	@Column(name = "payment_contract_documents")
	private String paymentContractDocuments;

	@Column(name = "risk_assessment")
	private String riskAssessment;

	@Column(name = "client_representative")
	private Short clientRepresentative;

	@Column(name = "updated_comments")
	private String updatedComments;

	@Column(name = "vendor_code")
	private String vendorCode;

	@Column(name = "entered_by")
	private Integer enteredBy;
	
	@Column(name="po_type")
	private String poType;
	
	@Column(name="company_email_address")
	private String companyEmailAddress;
	
	@Column(name="fk_region_id")
	private Short fkRegionId;
	
	@Column(name="no_of_days_po_mapping")
	private Integer noOfDaysPoMapping;
	
	@Column(name="contact_number_one")
	private String contactNumberOne;
	
	@Column(name="contact_number_two")
	private String contactNumberTwo;
	
	@Column(name="pan_number")
	private String panNumber;
	
	@Column(name="gst_number")
	private String gstNumber;
	
	@Column(name="vat_number")
	private String vatNumber;
	
	@Column(name="other_reference_number")
	private String otherReferenceNumber;
	
	@Column(name="client_history")
	private String clientHistory;

	// bi-directional many-to-one association to MasClient
	@OneToOne
	@JoinColumn(name = "fk_client_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasClientBO masClientAuditClientId;

	// bi-directional many-to-one association to ProjMasClientManager
	@OneToOne
	@JoinColumn(name = "client_representative", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasClientManagerBO masClientAuditClientRepresentativeId;

	// bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name = "entered_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO masClientAuditEnteredBy;

	public MasClientAuditBO() {
	}

	public int getPkClientAuditId() {
		return this.pkClientAuditId;
	}

	public void setPkClientAuditId(int pkClientAuditId) {
		this.pkClientAuditId = pkClientAuditId;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getClientName() {
		return this.clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientStatus() {
		return this.clientStatus;
	}

	public void setClientStatus(String clientStatus) {
		this.clientStatus = clientStatus;
	}

	public Short getCreditPeriod() {
		return this.creditPeriod;
	}

	public void setCreditPeriod(Short creditPeriod) {
		this.creditPeriod = creditPeriod;
	}

	public Date getEnteredOn() {
		return this.enteredOn;
	}

	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}

	public String getPaymentContractDocuments() {
		return this.paymentContractDocuments;
	}

	public void setPaymentContractDocuments(String paymentContractDocuments) {
		this.paymentContractDocuments = paymentContractDocuments;
	}

	public String getRiskAssessment() {
		return this.riskAssessment;
	}

	public void setRiskAssessment(String riskAssessment) {
		this.riskAssessment = riskAssessment;
	}

	public String getUpdatedComments() {
		return this.updatedComments;
	}

	public void setUpdatedComments(String updatedComments) {
		this.updatedComments = updatedComments;
	}

	public String getVendorCode() {
		return this.vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public Short getFkClientId() {
		return fkClientId;
	}

	public void setFkClientId(Short fkClientId) {
		this.fkClientId = fkClientId;
	}

	public Short getClientRepresentative() {
		return clientRepresentative;
	}

	public void setClientRepresentative(Short clientRepresentative) {
		this.clientRepresentative = clientRepresentative;
	}

	public Integer getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(Integer enteredBy) {
		this.enteredBy = enteredBy;
	}

	public String getCompanyEmailAddress() {
		return companyEmailAddress;
	}

	public void setCompanyEmailAddress(String companyEmailAddress) {
		this.companyEmailAddress = companyEmailAddress;
	}

	public Short getFkRegionId() {
		return fkRegionId;
	}

	public void setFkRegionId(Short fkRegionId) {
		this.fkRegionId = fkRegionId;
	}

	public Integer getNoOfDaysPoMapping() {
		return noOfDaysPoMapping;
	}

	public void setNoOfDaysPoMapping(Integer noOfDaysPoMapping) {
		this.noOfDaysPoMapping = noOfDaysPoMapping;
	}

	public String getContactNumberOne() {
		return contactNumberOne;
	}

	public void setContactNumberOne(String contactNumberOne) {
		this.contactNumberOne = contactNumberOne;
	}

	public String getContactNumberTwo() {
		return contactNumberTwo;
	}

	public void setContactNumberTwo(String contactNumberTwo) {
		this.contactNumberTwo = contactNumberTwo;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public String getOtherReferenceNumber() {
		return otherReferenceNumber;
	}

	public void setOtherReferenceNumber(String otherReferenceNumber) {
		this.otherReferenceNumber = otherReferenceNumber;
	}

	public String getClientHistory() {
		return clientHistory;
	}

	public void setClientHistory(String clientHistory) {
		this.clientHistory = clientHistory;
	}

	public DatEmpDetailBO getMasClientAuditEnteredBy() {
		return masClientAuditEnteredBy;
	}

	public void setMasClientAuditEnteredBy(
			DatEmpDetailBO masClientAuditEnteredBy) {
		this.masClientAuditEnteredBy = masClientAuditEnteredBy;
	}

	public MasClientBO getMasClientAuditClientId() {
		return masClientAuditClientId;
	}

	public void setMasClientAuditClientId(MasClientBO masClientAuditClientId) {
		this.masClientAuditClientId = masClientAuditClientId;
	}

	public ProjMasClientManagerBO getMasClientAuditClientRepresentativeId() {
		return masClientAuditClientRepresentativeId;
	}

	public void setMasClientAuditClientRepresentativeId(
			ProjMasClientManagerBO masClientAuditClientRepresentativeId) {
		this.masClientAuditClientRepresentativeId = masClientAuditClientRepresentativeId;
	}

	public String getPoType() {
		return poType;
	}

	public void setPoType(String poType) {
		this.poType = poType;
	}

	@Override
	public String toString() {
		return "MasClientAuditBO [pkClientAuditId=" + pkClientAuditId
				+ ", fkClientId=" + fkClientId + ", action=" + action
				+ ", clientName=" + clientName + ", clientStatus="
				+ clientStatus + ", creditPeriod=" + creditPeriod
				+ ", enteredOn=" + enteredOn + ", paymentContractDocuments="
				+ paymentContractDocuments + ", riskAssessment="
				+ riskAssessment + ", clientRepresentative="
				+ clientRepresentative + ", updatedComments=" + updatedComments
				+ ", vendorCode=" + vendorCode + ", enteredBy=" + enteredBy
				+ ", poType=" + poType + ", companyEmailAddress="
				+ companyEmailAddress + ", fkRegionId=" + fkRegionId
				+ ", noOfDaysPoMapping=" + noOfDaysPoMapping
				+ ", contactNumberOne=" + contactNumberOne
				+ ", contactNumberTwo=" + contactNumberTwo + ", panNumber="
				+ panNumber + ", gstNumber=" + gstNumber + ", vatNumber="
				+ vatNumber + ", otherReferenceNumber=" + otherReferenceNumber
				+ ", clientHistory=" + clientHistory
				+ ", masClientAuditClientId=" + masClientAuditClientId
				+ ", masClientAuditClientRepresentativeId="
				+ masClientAuditClientRepresentativeId
				+ ", masClientAuditEnteredBy=" + masClientAuditEnteredBy + "]";
	}

}