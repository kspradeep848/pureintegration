package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;


/**
 * The persistent class for the project_level_user_access_group database table.
 * 
 */
@Entity
@Table(name="project_level_user_access_group")
@NamedQuery(name="ProjectLevelUserAccessGroupBO.findAll", query="SELECT p FROM ProjectLevelUserAccessGroupBO p")
public class ProjectLevelUserAccessGroupBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_access_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int pkAccessId;
	
	@Column(name="fk_project_id")
	private int fkProjectId;

	@Column(name="fk_user_id")
	private Integer fkUserId;
	
	//bi-directional many-to-one association to MasProject
	@OneToOne
	@JoinColumn(name="fk_project_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasProjectBO masProject;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="fk_user_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail;

	public ProjectLevelUserAccessGroupBO() {
	}

	public int getPkAccessId() {
		return this.pkAccessId;
	}

	public void setPkAccessId(int pkAccessId) {
		this.pkAccessId = pkAccessId;
	}

	public MasProjectBO getMasProject() {
		return this.masProject;
	}

	public void setMasProject(MasProjectBO masProject) {
		this.masProject = masProject;
	}

	public DatEmpDetailBO getDatEmpDetail() {
		return this.datEmpDetail;
	}

	public void setDatEmpDetail(DatEmpDetailBO datEmpDetail) {
		this.datEmpDetail = datEmpDetail;
	}

	public int getFkProjectId() {
		return fkProjectId;
	}

	public void setFkProjectId(int fkProjectId) {
		this.fkProjectId = fkProjectId;
	}

	public Integer getFkUserId() {
		return fkUserId;
	}

	public void setFkUserId(Integer fkUserId) {
		this.fkUserId = fkUserId;
	}

	@Override
	public String toString() {
		return "ProjectLevelUserAccessGroupBO [pkAccessId=" + pkAccessId
				+ ", fkProjectId=" + fkProjectId + ", fkUserId=" + fkUserId
				+ ", masProject=" + masProject + ", datEmpDetail="
				+ datEmpDetail + "]";
	}

	
}