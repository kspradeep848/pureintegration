package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the dat_workorder_initials database table.
 * 
 */
@Entity
@Table(name="dat_workorder_initials")
@NamedQuery(name="DatWorkorderInitialBO.findAll", query="SELECT d FROM DatWorkorderInitialBO d")
public class DatWorkorderInitialBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_workorder_initials_id")
	private int pkWorkorderInitialsId;

	@Column(name="wo_initial")
	private String woInitial;

	@Column(name="fk_project_id")
	private int fkProjectId;
			
	//bi-directional one-to-one association to MasProject
	@OneToOne
	@JoinColumn(name="fk_project_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasProjectBO masProject;

	public DatWorkorderInitialBO() {
	}

	public int getPkWorkorderInitialsId() {
		return this.pkWorkorderInitialsId;
	}

	public void setPkWorkorderInitialsId(int pkWorkorderInitialsId) {
		this.pkWorkorderInitialsId = pkWorkorderInitialsId;
	}

	public String getWoInitial() {
		return this.woInitial;
	}

	public void setWoInitial(String woInitial) {
		this.woInitial = woInitial;
	}

	public int getFkProjectId() {
		return fkProjectId;
	}

	public void setFkProjectId(int fkProjectId) {
		this.fkProjectId = fkProjectId;
	}

	public MasProjectBO getMasProject() {
		return masProject;
	}

	public void setMasProject(MasProjectBO masProject) {
		this.masProject = masProject;
	}

	@Override
	public String toString() {
		return "DatWorkorderInitialBO [pkWorkorderInitialsId="
				+ pkWorkorderInitialsId + ", woInitial=" + woInitial
				+ ", fkProjectId=" + fkProjectId + ", masProject=" + masProject
				+ "]";
	}

}