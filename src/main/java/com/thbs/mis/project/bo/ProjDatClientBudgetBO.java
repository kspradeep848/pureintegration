package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.MasEmpDesignationBO;

/**
 * The persistent class for the proj_dat_client_budget database table.
 * 
 */
@Entity
@Table(name = "proj_dat_client_budget")
@NamedNativeQuery(name = "ProjDatClientBudgetBO.findAll", query = "SELECT p FROM ProjDatClientBudgetBO p")
public class ProjDatClientBudgetBO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id

	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_client_budget_id")
	private Integer pkClientBudgetId;

	@Column(name = "budgeted_days")
	private Integer budgetedDays;

	@Column(name = "comnents_for_updation")
	private String comnentsForUpdation;

	@Column(name = "cost_to_client")
	private BigDecimal costToClient;

	@Column(name = "created_by")
	private Integer createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "deliverable_name")
	private String deliverableName;


	@Column(name="fk_parent_location_id")
	private Short fkParentLocationId;

	@Column(name="fk_resource_designation_id")
	private short fkResourceDesignationId;

	@Column(name = "is_billing_rate_available")
	private String isBillingRateAvailable;


	
	@Column(name = "no_of_resource")
	private Integer noOfResource;

	@Column(name = "resource_location")
	private String resourceLocation;


	@Column(name="updated_by")
	private Integer updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_on")
	private Date updatedOn;

	@Column(name = "fk_workorder_id")
	private Integer fkWorkOrderId;

	@OneToOne
	@JoinColumn(name = "fk_workorder_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datWorkorder;


	@OneToOne
	@JoinColumn(name = "fk_resource_designation_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpDesignationBO masEmpDesignationBO;

	
	public short getFkResourceDesignationId() {
		return fkResourceDesignationId;
	}

	public void setFkResourceDesignationId(short fkResourceDesignationId) {
		this.fkResourceDesignationId = fkResourceDesignationId;
	}

	public void setFkParentLocationId(Short fkParentLocationId) {
		this.fkParentLocationId = fkParentLocationId;
	}

	public Integer getPkClientBudgetId() {
		return pkClientBudgetId;
	}

	public void setPkClientBudgetId(Integer pkClientBudgetId) {
		this.pkClientBudgetId = pkClientBudgetId;
	}

	public Integer getBudgetedDays() {
		return budgetedDays;
	}

	public void setBudgetedDays(Integer budgetedDays) {
		this.budgetedDays = budgetedDays;
	}

	public String getComnentsForUpdation() {
		return comnentsForUpdation;
	}

	public void setComnentsForUpdation(String comnentsForUpdation) {
		this.comnentsForUpdation = comnentsForUpdation;
	}

	public BigDecimal getCostToClient() {
		return costToClient;
	}

	public void setCostToClient(BigDecimal costToClient) {
		this.costToClient = costToClient;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getDeliverableName() {
		return deliverableName;
	}

	public void setDeliverableName(String deliverableName) {
		this.deliverableName = deliverableName;
	}

	public short getFkParentLocationId() {
		return fkParentLocationId;
	}

	public void setFkParentLocationId(short fkParentLocationId) {
		this.fkParentLocationId = fkParentLocationId;
	}


	public String getIsBillingRateAvailable() {
		return isBillingRateAvailable;
	}

	public void setIsBillingRateAvailable(String isBillingRateAvailable) {
		this.isBillingRateAvailable = isBillingRateAvailable;
	}

	public Integer getNoOfResource() {
		return noOfResource;
	}

	public void setNoOfResource(Integer noOfResource) {
		this.noOfResource = noOfResource;
	}

	public String getResourceLocation() {
		return resourceLocation;
	}

	public void setResourceLocation(String resourceLocation) {
		this.resourceLocation = resourceLocation;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Integer getFkWorkOrderId() {
		return fkWorkOrderId;
	}

	public void setFkWorkOrderId(Integer fkWorkOrderId) {
		this.fkWorkOrderId = fkWorkOrderId;
	}

	public DatWorkorderBO getDatWorkorder() {
		return datWorkorder;
	}

	public void setDatWorkorder(DatWorkorderBO datWorkorder) {
		this.datWorkorder = datWorkorder;
	}


	public MasEmpDesignationBO getMasEmpDesignationBO() {
		return masEmpDesignationBO;
	}

	public void setMasEmpDesignationBO(MasEmpDesignationBO masEmpDesignationBO) {
		this.masEmpDesignationBO = masEmpDesignationBO;
	}


	@Override
	public String toString() {
		return "ProjDatClientBudgetBO [pkClientBudgetId=" + pkClientBudgetId
				+ ", budgetedDays=" + budgetedDays + ", comnentsForUpdation="
				+ comnentsForUpdation + ", costToClient=" + costToClient
				+ ", createdBy=" + createdBy + ", createdOn=" + createdOn
				+ ", deliverableName=" + deliverableName
				+ ", fkParentLocationId=" + fkParentLocationId
				+ ", fkResourceDesignationId=" + fkResourceDesignationId
				+ ", isBillingRateAvailable=" + isBillingRateAvailable
				+ ", noOfResource=" + noOfResource + ", resourceLocation="
				+ resourceLocation + ", updatedBy=" + updatedBy
				+ ", updatedOn=" + updatedOn + ", fkWorkOrderId="
				+ fkWorkOrderId + ", datWorkorder=" + datWorkorder
				+ ", masEmpDesignationBO=" + masEmpDesignationBO + "]";
	}

	
	
}
