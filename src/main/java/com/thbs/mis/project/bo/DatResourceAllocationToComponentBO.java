package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasLocationPhysicalBO;

import java.util.Date;


/**
 * The persistent class for the dat_resource_allocation_to_component database table.
 * 
 */
@Entity
@Table(name="dat_resource_allocation_to_component")
@NamedQuery(name="DatResourceAllocationToComponentBO.findAll", query="SELECT d FROM DatResourceAllocationToComponentBO d")
public class DatResourceAllocationToComponentBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_resource_allocation_id")
	private int pkResourceAllocationId;

	@Column(name="onsite_flag")
	private String onsiteFlag;

	@Column(name="resource_allocation_in_percentage")
	private byte resourceAllocationInPercentage;

	@Temporal(TemporalType.DATE)
	@Column(name="resource_end_date")
	private Date resourceEndDate;

	@Temporal(TemporalType.DATE)
	@Column(name="resource_start_date")
	private Date resourceStartDate;

	@Column(name="fk_component_id")
	private int fkComponentId;
	
	@Column(name="fk_emp_id")
	private Integer fkEmployeeId;
	
	@Column(name="fk_employee_location_id")
	private short fkEmployeeLocationId;
	
	@Column(name="fk_resource_billing_status_id")
	private byte fkResourceBillingStatusId;
	
	//bi-directional many-to-one association to DatComponent
	@OneToOne
	@JoinColumn(name="fk_component_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatComponentBO datComponent;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="fk_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail;

	//bi-directional many-to-one association to MasLocationPhysical
	@OneToOne
	@JoinColumn(name="fk_employee_location_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasLocationPhysicalBO masLocationPhysical;

	//bi-directional many-to-one association to MasResourceBillingStatus
	@OneToOne
	@JoinColumn(name="fk_resource_billing_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasResourceBillingStatusBO masResourceBillingStatus;

	public DatResourceAllocationToComponentBO() {
		// TODO Auto-generated constructor stub
	}

	public int getPkResourceAllocationId() {
		return this.pkResourceAllocationId;
	}

	public void setPkResourceAllocationId(int pkResourceAllocationId) {
		this.pkResourceAllocationId = pkResourceAllocationId;
	}

	public String getOnsiteFlag() {
		return this.onsiteFlag;
	}

	public void setOnsiteFlag(String onsiteFlag) {
		this.onsiteFlag = onsiteFlag;
	}

	public byte getResourceAllocationInPercentage() {
		return this.resourceAllocationInPercentage;
	}

	public void setResourceAllocationInPercentage(byte resourceAllocationInPercentage) {
		this.resourceAllocationInPercentage = resourceAllocationInPercentage;
	}

	public Date getResourceEndDate() {
		return this.resourceEndDate;
	}

	public void setResourceEndDate(Date resourceEndDate) {
		this.resourceEndDate = resourceEndDate;
	}

	public Date getResourceStartDate() {
		return this.resourceStartDate;
	}

	public void setResourceStartDate(Date resourceStartDate) {
		this.resourceStartDate = resourceStartDate;
	}

	public int getFkComponentId() {
		return fkComponentId;
	}

	public void setFkComponentId(int fkComponentId) {
		this.fkComponentId = fkComponentId;
	}

	public Integer getFkEmployeeId() {
		return fkEmployeeId;
	}

	public void setFkEmployeeId(Integer fkEmployeeId) {
		this.fkEmployeeId = fkEmployeeId;
	}

	public short getFkEmployeeLocationId() {
		return fkEmployeeLocationId;
	}

	public void setFkEmployeeLocationId(short fkEmployeeLocationId) {
		this.fkEmployeeLocationId = fkEmployeeLocationId;
	}

	public byte getFkResourceBillingStatusId() {
		return fkResourceBillingStatusId;
	}

	public void setFkResourceBillingStatusId(byte fkResourceBillingStatusId) {
		this.fkResourceBillingStatusId = fkResourceBillingStatusId;
	}

	public DatComponentBO getDatComponent() {
		return datComponent;
	}

	public void setDatComponent(DatComponentBO datComponent) {
		this.datComponent = datComponent;
	}

	public DatEmpDetailBO getDatEmpDetail() {
		return datEmpDetail;
	}

	public void setDatEmpDetail(DatEmpDetailBO datEmpDetail) {
		this.datEmpDetail = datEmpDetail;
	}

	public MasLocationPhysicalBO getMasLocationPhysical() {
		return masLocationPhysical;
	}

	public void setMasLocationPhysical(MasLocationPhysicalBO masLocationPhysical) {
		this.masLocationPhysical = masLocationPhysical;
	}

	public MasResourceBillingStatusBO getMasResourceBillingStatus() {
		return masResourceBillingStatus;
	}

	public void setMasResourceBillingStatus(
			MasResourceBillingStatusBO masResourceBillingStatus) {
		this.masResourceBillingStatus = masResourceBillingStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "DatResourceAllocationToComponentBO [pkResourceAllocationId="
				+ pkResourceAllocationId + ", onsiteFlag=" + onsiteFlag
				+ ", resourceAllocationInPercentage="
				+ resourceAllocationInPercentage + ", resourceEndDate="
				+ resourceEndDate + ", resourceStartDate=" + resourceStartDate
				+ ", fkComponentId=" + fkComponentId + ", fkEmployeeId="
				+ fkEmployeeId + ", fkEmployeeLocationId="
				+ fkEmployeeLocationId + ", fkResourceBillingStatusId="
				+ fkResourceBillingStatusId + ", datComponent=" + datComponent
				+ ", datEmpDetail=" + datEmpDetail + ", masLocationPhysical="
				+ masLocationPhysical + ", masResourceBillingStatus="
				+ masResourceBillingStatus + "]";
	}

	
	
}