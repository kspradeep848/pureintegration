package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the dat_milestone_audit database table.
 * 
 */
@Entity
@Table(name="dat_milestone_audit")
@NamedQuery(name="DatMilestoneAuditBO.findAll", query="SELECT d FROM DatMilestoneAuditBO d")
public class DatMilestoneAuditBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="pk_milestone_audit_id")
	private int pkMilestoneAuditId;

	private String action;

	@Column(name="fk_milestone_id")
	private int fkMilestoneId;

	@Column(name="fk_milestone_status_id")
	private Byte fkMilestoneStatusId;

	@Column(name="line_item")
	private String lineItem;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="milestone_end_date")
	private Date milestoneEndDate;

	@Column(name="milestone_name")
	private String milestoneName;

	@Column(name="milestone_remaining_value")
	private BigDecimal milestoneRemainingValue;

	@Column(name="milestone_type")
	private String milestoneType;

	@Column(name="milestone_value")
	private BigDecimal milestoneValue;

	@Column(name="net_price")
	private BigDecimal netPrice;

	private BigDecimal quantity;

	@Column(name="unit_price")
	private BigDecimal unitPrice;

	@Column(name="updated_by")
	private int updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;

	@Column(name="updation_comments")
	private String updationComments;
	
	@Column(name="milestone_history")
	private String milestoneHistory;

	public DatMilestoneAuditBO() {
	}

	
	
	//bi-directional one-to-one association to DatMilestone
		@OneToOne
		@JoinColumn(name="fk_milestone_id", unique = true, nullable = true, insertable = false, updatable = false)
		private DatMilestoneBO datMilestoneBO;

		//bi-directional one-to-one association to MasMilestoneStatus
		@OneToOne
		@JoinColumn(name="fk_milestone_status_id", unique = true, nullable = true, insertable = false, updatable = false)
		private MasMilestoneStatusBO masMilestoneStatusBO;

		//bi-directional one-to-one association to DatEmpDetail
		@OneToOne
		@JoinColumn(name="updated_by", unique = true, nullable = true, insertable = false, updatable = false)
		private DatEmpDetailBO datEmpDetailBO;

	
	

	
	
	public int getPkMilestoneAuditId() {
		return this.pkMilestoneAuditId;
	}

	public void setPkMilestoneAuditId(int pkMilestoneAuditId) {
		this.pkMilestoneAuditId = pkMilestoneAuditId;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getFkMilestoneId() {
		return this.fkMilestoneId;
	}

	public void setFkMilestoneId(int fkMilestoneId) {
		this.fkMilestoneId = fkMilestoneId;
	}

	public Byte getFkMilestoneStatusId() {
		return fkMilestoneStatusId;
	}

	public void setFkMilestoneStatusId(Byte fkMilestoneStatusId) {
		this.fkMilestoneStatusId = fkMilestoneStatusId;
	}

	public String getMilestoneHistory() {
		return milestoneHistory;
	}

	public void setMilestoneHistory(String milestoneHistory) {
		this.milestoneHistory = milestoneHistory;
	}

	public DatMilestoneBO getDatMilestoneBO() {
		return datMilestoneBO;
	}

	public void setDatMilestoneBO(DatMilestoneBO datMilestoneBO) {
		this.datMilestoneBO = datMilestoneBO;
	}

	public MasMilestoneStatusBO getMasMilestoneStatusBO() {
		return masMilestoneStatusBO;
	}

	public void setMasMilestoneStatusBO(MasMilestoneStatusBO masMilestoneStatusBO) {
		this.masMilestoneStatusBO = masMilestoneStatusBO;
	}

	public DatEmpDetailBO getDatEmpDetailBO() {
		return datEmpDetailBO;
	}

	public void setDatEmpDetailBO(DatEmpDetailBO datEmpDetailBO) {
		this.datEmpDetailBO = datEmpDetailBO;
	}

	public String getLineItem() {
		return this.lineItem;
	}

	public void setLineItem(String lineItem) {
		this.lineItem = lineItem;
	}

	public Date getMilestoneEndDate() {
		return this.milestoneEndDate;
	}

	public void setMilestoneEndDate(Date milestoneEndDate) {
		this.milestoneEndDate = milestoneEndDate;
	}

	public String getMilestoneName() {
		return this.milestoneName;
	}

	public void setMilestoneName(String milestoneName) {
		this.milestoneName = milestoneName;
	}

	public BigDecimal getMilestoneRemainingValue() {
		return this.milestoneRemainingValue;
	}

	public void setMilestoneRemainingValue(BigDecimal milestoneRemainingValue) {
		this.milestoneRemainingValue = milestoneRemainingValue;
	}

	public String getMilestoneType() {
		return this.milestoneType;
	}

	public void setMilestoneType(String milestoneType) {
		this.milestoneType = milestoneType;
	}

	public BigDecimal getMilestoneValue() {
		return this.milestoneValue;
	}

	public void setMilestoneValue(BigDecimal milestoneValue) {
		this.milestoneValue = milestoneValue;
	}

	public BigDecimal getNetPrice() {
		return this.netPrice;
	}

	public void setNetPrice(BigDecimal netPrice) {
		this.netPrice = netPrice;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public int getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdationComments() {
		return this.updationComments;
	}

	public void setUpdationComments(String updationComments) {
		this.updationComments = updationComments;
	}

	@Override
	public String toString() {
		return "DatMilestoneAuditBO [pkMilestoneAuditId=" + pkMilestoneAuditId
				+ ", action=" + action + ", fkMilestoneId=" + fkMilestoneId
				+ ", fkMilestoneStatusId=" + fkMilestoneStatusId
				+ ", lineItem=" + lineItem + ", milestoneEndDate="
				+ milestoneEndDate + ", milestoneName=" + milestoneName
				+ ", milestoneRemainingValue=" + milestoneRemainingValue
				+ ", milestoneType=" + milestoneType + ", milestoneValue="
				+ milestoneValue + ", netPrice=" + netPrice + ", quantity="
				+ quantity + ", unitPrice=" + unitPrice + ", updatedBy="
				+ updatedBy + ", updatedOn=" + updatedOn
				+ ", updationComments=" + updationComments
				+ ", milestoneHistory=" + milestoneHistory
				+ ", datMilestoneBO=" + datMilestoneBO
				+ ", masMilestoneStatusBO=" + masMilestoneStatusBO
				+ ", datEmpDetailBO=" + datEmpDetailBO + "]";
	}

}