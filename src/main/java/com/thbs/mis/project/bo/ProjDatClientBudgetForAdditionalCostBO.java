package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the proj_dat_client_budget_for_additional_cost database table.
 * 
 */
@Entity
@Table(name="proj_dat_client_budget_for_additional_cost")
@NamedQuery(name="ProjDatClientBudgetForAdditionalCostBO.findAll", query="SELECT p FROM ProjDatClientBudgetForAdditionalCostBO p")
public class ProjDatClientBudgetForAdditionalCostBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_client_budget_additional_cost_id")
	private Integer pkClientBudgetAdditionalCostId;

	@Column(name="client_budget_additional_cost")
	private BigDecimal clientBudgetAdditionalCost;

	@Column(name="client_budget_additional_cost_comment")
	private String clientBudgetAdditionalCostComment;

	@Column(name="created_by")
	private Integer createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="fk_workorder_id")
	private Integer fkWorkorderId;

	@Column(name="updated_by")
	private Integer updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;

	public ProjDatClientBudgetForAdditionalCostBO() {
	}

	@OneToOne
	@JoinColumn(name="fk_workorder_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datWorkorder;

	public Integer getPkClientBudgetAdditionalCostId() {
		return pkClientBudgetAdditionalCostId;
	}

	public void setPkClientBudgetAdditionalCostId(
			Integer pkClientBudgetAdditionalCostId) {
		this.pkClientBudgetAdditionalCostId = pkClientBudgetAdditionalCostId;
	}

	public BigDecimal getClientBudgetAdditionalCost() {
		return clientBudgetAdditionalCost;
	}

	public void setClientBudgetAdditionalCost(BigDecimal clientBudgetAdditionalCost) {
		this.clientBudgetAdditionalCost = clientBudgetAdditionalCost;
	}

	public String getClientBudgetAdditionalCostComment() {
		return clientBudgetAdditionalCostComment;
	}

	public void setClientBudgetAdditionalCostComment(
			String clientBudgetAdditionalCostComment) {
		this.clientBudgetAdditionalCostComment = clientBudgetAdditionalCostComment;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getFkWorkorderId() {
		return fkWorkorderId;
	}

	public void setFkWorkorderId(Integer fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public DatWorkorderBO getDatWorkorder() {
		return datWorkorder;
	}

	public void setDatWorkorder(DatWorkorderBO datWorkorder) {
		this.datWorkorder = datWorkorder;
	}

	@Override
	public String toString() {
		return "ProjDatClientBudgetForAdditionalCostBO [pkClientBudgetAdditionalCostId="
				+ pkClientBudgetAdditionalCostId
				+ ", clientBudgetAdditionalCost="
				+ clientBudgetAdditionalCost
				+ ", clientBudgetAdditionalCostComment="
				+ clientBudgetAdditionalCostComment
				+ ", createdBy="
				+ createdBy
				+ ", createdOn="
				+ createdOn
				+ ", fkWorkorderId="
				+ fkWorkorderId
				+ ", updatedBy="
				+ updatedBy
				+ ", updatedOn="
				+ updatedOn + ", datWorkorder=" + datWorkorder + "]";
	}
	
}