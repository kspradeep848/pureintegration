package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the proj_dat_thbs_budget_for_additional_cost_audit database table.
 * 
 */
@Entity
@Table(name="proj_dat_thbs_budget_for_additional_cost_audit")
@NamedQuery(name="ProjDatThbsBudgetForAdditionalCostAuditBO.findAll", query="SELECT p FROM ProjDatThbsBudgetForAdditionalCostAuditBO p")
public class ProjDatThbsBudgetForAdditionalCostAuditBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_thbs_budget_for_additional_cost_audit_id")
	private Integer pkThbsBudgetForAdditionalCostAuditId;

	private String action;

	@Column(name="comments_for_updation")
	private String commentsForUpdation;

	@Column(name="entered_by")
	private int enteredBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="entered_on")
	private Date enteredOn;

	@Column(name="expense_amt")
	private BigDecimal expenseAmt;

	@Column(name="fk_thbs_budget_additional_cost_id")
	private Integer fkThbsBudgetAdditionalCostId;

	@Column(name="fk_workorder_id")
	private Integer fkWorkorderId;

	@Column(name="purchase_amt")
	private BigDecimal purchaseAmt;

	@Column(name="travel_amt")
	private BigDecimal travelAmt;

	public ProjDatThbsBudgetForAdditionalCostAuditBO() {
	}

	@OneToOne
	@JoinColumn(name="fk_workorder_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datWorkorder;
	
	@OneToOne
	@JoinColumn(name="fk_thbs_budget_additional_cost_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjDatThbsBudgetForAdditionalCostBO projDatThbsBudgetForAdditionalCostBO;

	public Integer getPkThbsBudgetForAdditionalCostAuditId() {
		return pkThbsBudgetForAdditionalCostAuditId;
	}

	public void setPkThbsBudgetForAdditionalCostAuditId(
			Integer pkThbsBudgetForAdditionalCostAuditId) {
		this.pkThbsBudgetForAdditionalCostAuditId = pkThbsBudgetForAdditionalCostAuditId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCommentsForUpdation() {
		return commentsForUpdation;
	}

	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}

	public int getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(int enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredOn() {
		return enteredOn;
	}

	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}

	public BigDecimal getExpenseAmt() {
		return expenseAmt;
	}

	public void setExpenseAmt(BigDecimal expenseAmt) {
		this.expenseAmt = expenseAmt;
	}

	public Integer getFkThbsBudgetAdditionalCostId() {
		return fkThbsBudgetAdditionalCostId;
	}

	public void setFkThbsBudgetAdditionalCostId(Integer fkThbsBudgetAdditionalCostId) {
		this.fkThbsBudgetAdditionalCostId = fkThbsBudgetAdditionalCostId;
	}

	public Integer getFkWorkorderId() {
		return fkWorkorderId;
	}

	public void setFkWorkorderId(Integer fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}

	public BigDecimal getPurchaseAmt() {
		return purchaseAmt;
	}

	public void setPurchaseAmt(BigDecimal purchaseAmt) {
		this.purchaseAmt = purchaseAmt;
	}

	public BigDecimal getTravelAmt() {
		return travelAmt;
	}

	public void setTravelAmt(BigDecimal travelAmt) {
		this.travelAmt = travelAmt;
	}

	public DatWorkorderBO getDatWorkorder() {
		return datWorkorder;
	}

	public void setDatWorkorder(DatWorkorderBO datWorkorder) {
		this.datWorkorder = datWorkorder;
	}

	public ProjDatThbsBudgetForAdditionalCostBO getProjDatThbsBudgetForAdditionalCostBO() {
		return projDatThbsBudgetForAdditionalCostBO;
	}

	public void setProjDatThbsBudgetForAdditionalCostBO(
			ProjDatThbsBudgetForAdditionalCostBO projDatThbsBudgetForAdditionalCostBO) {
		this.projDatThbsBudgetForAdditionalCostBO = projDatThbsBudgetForAdditionalCostBO;
	}

	@Override
	public String toString() {
		return "ProjDatThbsBudgetForAdditionalCostAuditBO [pkThbsBudgetForAdditionalCostAuditId="
				+ pkThbsBudgetForAdditionalCostAuditId
				+ ", action="
				+ action
				+ ", commentsForUpdation="
				+ commentsForUpdation
				+ ", enteredBy="
				+ enteredBy
				+ ", enteredOn="
				+ enteredOn
				+ ", expenseAmt="
				+ expenseAmt
				+ ", fkThbsBudgetAdditionalCostId="
				+ fkThbsBudgetAdditionalCostId
				+ ", fkWorkorderId="
				+ fkWorkorderId
				+ ", purchaseAmt="
				+ purchaseAmt
				+ ", travelAmt="
				+ travelAmt
				+ ", datWorkorder="
				+ datWorkorder
				+ ", projDatThbsBudgetForAdditionalCostBO="
				+ projDatThbsBudgetForAdditionalCostBO + "]";
	}
	
	

}