package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasCurrencytypeBO;


/**
 * The persistent class for the dat_workorder_audit database table.
 * 
 */
@Entity
@Table(name="dat_workorder_audit")
@NamedQuery(name="DatWorkorderAuditBO.findAll", query="SELECT d FROM DatWorkorderAuditBO d")
public class DatWorkorderAuditBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_workorder_audit_id")
	private int pkWorkorderAuditId;

	@Column(name="action")
	private String action;

	@Column(name="additional_amount")
	private BigDecimal additionalAmount;

	@Column(name="comment_for_update")
	private String commentForUpdate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="entered_on")
	private Date enteredOn;
	
	@Column(name="entered_by")
	private Integer enteredBy;
	
	@Column(name="is_internal")
	private String isInternal;

	@Column(name="sow_amount")
	private BigDecimal sowAmount;

	@Column(name="sow_uploaded_flag")
	private String sowUploadedFlag;

	@Temporal(TemporalType.DATE)
	@Column(name="wo_end_date")
	private Date woEndDate;

	@Temporal(TemporalType.DATE)
	@Column(name="wo_start_date")
	private Date woStartDate;

	@Column(name="workorder_description")
	private String workorderDescription;

	@Column(name="workorder_initial")
	private String workorderInitial;
	
	@Column(name="fk_workorder_id")
	private Integer fkWorkorderId;

	@Column(name="workorder_number")
	private String workorderNumber;
	
	@Column(name="workorder_discount")
	private BigDecimal workorderDiscount;
	
	@Column(name="wo_history")
	private String woHistory;
	

	//bi-directional many-to-one association to ProjMasClientManager
	@OneToOne
	@JoinColumn(name="client_manager_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasClientManagerBO datWorkorderAuditClientManagerId;

	//bi-directional many-to-one association to MasCurrencytype
	@OneToOne
	@JoinColumn(name="sow_currency_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasCurrencytypeBO datWorkorderAuditCurrencyId;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="entered_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datWorkorderAuditEnteredBy;

	//bi-directional many-to-one association to MasProject
	@OneToOne
	@JoinColumn(name="fk_project_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasProjectBO datWorkorderAuditProjectId;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="project_owner", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datWorkorderProjectOwnerId;

	//bi-directional many-to-one association to DatWorkorder
	@OneToOne
	@JoinColumn(name="fk_workorder_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datWorkorderAuditWorkorderId;

	//bi-directional many-to-one association to ProjMasWoStatus
	@OneToOne
	@JoinColumn(name="fk_workorder_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasWoStatusBO datWorkorderWoStatusId;

	public DatWorkorderAuditBO() {
	}

	public int getPkWorkorderAuditId() {
		return this.pkWorkorderAuditId;
	}

	public void setPkWorkorderAuditId(int pkWorkorderAuditId) {
		this.pkWorkorderAuditId = pkWorkorderAuditId;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public BigDecimal getAdditionalAmount() {
		return this.additionalAmount;
	}

	public void setAdditionalAmount(BigDecimal additionalAmount) {
		this.additionalAmount = additionalAmount;
	}

	public String getCommentForUpdate() {
		return this.commentForUpdate;
	}

	public void setCommentForUpdate(String commentForUpdate) {
		this.commentForUpdate = commentForUpdate;
	}

	public Date getEnteredOn() {
		return this.enteredOn;
	}

	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}

	public Integer getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(Integer enteredBy) {
		this.enteredBy = enteredBy;
	}

	public String getIsInternal() {
		return this.isInternal;
	}

	public void setIsInternal(String isInternal) {
		this.isInternal = isInternal;
	}

	public BigDecimal getSowAmount() {
		return this.sowAmount;
	}

	public void setSowAmount(BigDecimal sowAmount) {
		this.sowAmount = sowAmount;
	}

	public String getSowUploadedFlag() {
		return this.sowUploadedFlag;
	}

	public void setSowUploadedFlag(String sowUploadedFlag) {
		this.sowUploadedFlag = sowUploadedFlag;
	}

	public Date getWoEndDate() {
		return this.woEndDate;
	}

	public void setWoEndDate(Date woEndDate) {
		this.woEndDate = woEndDate;
	}

	public Date getWoStartDate() {
		return this.woStartDate;
	}

	public void setWoStartDate(Date woStartDate) {
		this.woStartDate = woStartDate;
	}

	public String getWorkorderDescription() {
		return this.workorderDescription;
	}

	public void setWorkorderDescription(String workorderDescription) {
		this.workorderDescription = workorderDescription;
	}

	public String getWorkorderInitial() {
		return this.workorderInitial;
	}

	public void setWorkorderInitial(String workorderInitial) {
		this.workorderInitial = workorderInitial;
	}

	public String getWorkorderNumber() {
		return this.workorderNumber;
	}

	public void setWorkorderNumber(String workorderNumber) {
		this.workorderNumber = workorderNumber;
	}

	public String getWoHistory() {
		return woHistory;
	}

	public void setWoHistory(String woHistory) {
		this.woHistory = woHistory;
	}

	public ProjMasClientManagerBO getDatWorkorderAuditClientManagerId() {
		return datWorkorderAuditClientManagerId;
	}

	public void setDatWorkorderAuditClientManagerId(
			ProjMasClientManagerBO datWorkorderAuditClientManagerId) {
		this.datWorkorderAuditClientManagerId = datWorkorderAuditClientManagerId;
	}

	public MasCurrencytypeBO getDatWorkorderAuditCurrencyId() {
		return datWorkorderAuditCurrencyId;
	}

	public void setDatWorkorderAuditCurrencyId(
			MasCurrencytypeBO datWorkorderAuditCurrencyId) {
		this.datWorkorderAuditCurrencyId = datWorkorderAuditCurrencyId;
	}

	public DatEmpDetailBO getDatWorkorderAuditEnteredBy() {
		return datWorkorderAuditEnteredBy;
	}

	public void setDatWorkorderAuditEnteredBy(
			DatEmpDetailBO datWorkorderAuditEnteredBy) {
		this.datWorkorderAuditEnteredBy = datWorkorderAuditEnteredBy;
	}


	public MasProjectBO getDatWorkorderAuditProjectId() {
		return datWorkorderAuditProjectId;
	}

	public void setDatWorkorderAuditProjectId(
			MasProjectBO datWorkorderAuditProjectId) {
		this.datWorkorderAuditProjectId = datWorkorderAuditProjectId;
	}

	public DatEmpDetailBO getDatWorkorderProjectOwnerId() {
		return datWorkorderProjectOwnerId;
	}

	public void setDatWorkorderProjectOwnerId(
			DatEmpDetailBO datWorkorderProjectOwnerId) {
		this.datWorkorderProjectOwnerId = datWorkorderProjectOwnerId;
	}

	public DatWorkorderBO getDatWorkorderAuditWorkorderId() {
		return datWorkorderAuditWorkorderId;
	}

	public void setDatWorkorderAuditWorkorderId(
			DatWorkorderBO datWorkorderAuditWorkorderId) {
		this.datWorkorderAuditWorkorderId = datWorkorderAuditWorkorderId;
	}

	public ProjMasWoStatusBO getDatWorkorderWoStatusId() {
		return datWorkorderWoStatusId;
	}

	public void setDatWorkorderWoStatusId(ProjMasWoStatusBO datWorkorderWoStatusId) {
		this.datWorkorderWoStatusId = datWorkorderWoStatusId;
	}

	public BigDecimal getWorkorderDiscount() {
		return workorderDiscount;
	}

	public void setWorkorderDiscount(BigDecimal workorderDiscount) {
		this.workorderDiscount = workorderDiscount;
	}
	
	

	public Integer getFkWorkorderId() {
		return fkWorkorderId;
	}

	public void setFkWorkorderId(Integer fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}

	@Override
	public String toString() {
		return "DatWorkorderAuditBO [pkWorkorderAuditId=" + pkWorkorderAuditId
				+ ", action=" + action + ", additionalAmount="
				+ additionalAmount + ", commentForUpdate=" + commentForUpdate
				+ ", enteredOn=" + enteredOn + ", isInternal=" + isInternal
				+ ", sowAmount=" + sowAmount + ", sowUploadedFlag="
				+ sowUploadedFlag + ", woEndDate=" + woEndDate
				+ ", woStartDate=" + woStartDate + ", workorderDescription="
				+ workorderDescription + ", workorderInitial="
				+ workorderInitial + ", workorderNumber=" + workorderNumber
				+ ", workorderDiscount=" + workorderDiscount
				+ ", datWorkorderAuditClientManagerId="
				+ datWorkorderAuditClientManagerId
				+ ", datWorkorderAuditCurrencyId="
				+ datWorkorderAuditCurrencyId + ", datWorkorderAuditEnteredBy="
				+ datWorkorderAuditEnteredBy + ", datWorkorderAuditProjectId="
				+ datWorkorderAuditProjectId + ", datWorkorderProjectOwnerId="
				+ datWorkorderProjectOwnerId
				+ ", datWorkorderAuditWorkorderId="
				+ datWorkorderAuditWorkorderId + ", datWorkorderWoStatusId="
				+ datWorkorderWoStatusId + "]";
	}

	
}