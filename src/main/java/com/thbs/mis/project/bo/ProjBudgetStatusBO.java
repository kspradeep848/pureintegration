package com.thbs.mis.project.bo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the proj_budget_status database table.
 * 
 */
@Entity
@Table(name = "proj_budget_status")
@NamedQuery(name = "ProjBudgetStatusBO.findAll", query = "SELECT p FROM ProjBudgetStatusBO p")
public class ProjBudgetStatusBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "pk_budget_status_id")
	private int pkBudgetStatusId;

	@Column(name = "budget_status_name")
	private String budgetStatusName;

	
	public ProjBudgetStatusBO() {
	}

	public int getPkBudgetStatusId() {
		return this.pkBudgetStatusId;
	}

	public void setPkBudgetStatusId(int pkBudgetStatusId) {

		this.pkBudgetStatusId = pkBudgetStatusId;
	}

	public String getBudgetStatusName() {
		return budgetStatusName;

	}

	public void setBudgetStatusName(String budgetStatusName) {
		this.budgetStatusName = budgetStatusName;
	}

	

	@Override
	public String toString() {
		return "ProjBudgetStatusBO [pkBudgetStatusId=" + pkBudgetStatusId
				+ ", budgetStatusName=" + budgetStatusName;
	}

}
