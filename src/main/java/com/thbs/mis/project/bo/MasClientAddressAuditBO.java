package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasCountryBO;


/**
 * The persistent class for the mas_client_address_audit database table.
 * 
 */
@Entity
@Table(name="mas_client_address_audit")
@NamedQuery(name="MasClientAddressAuditBO.findAll", query="SELECT m FROM MasClientAddressAuditBO m")
public class MasClientAddressAuditBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_client_address_audit_id")
	private int pkClientAddressAuditId;

	@Column(name="action")
	private String action;
	
	@Column(name="fk_client_address_id")
	private Short fkClientAddressId;
	
	@Column(name="fk_client_id")
	private Short fkClientId;

	@Column(name="address_line_one")
	private String addressLineOne;

	@Column(name="address_line_two")
	private String addressLineTwo;

	@Column(name="city_name")
	private String cityName;
	
	@Column(name="fk_country_id")
	private Short fkCountryId;

	@Column(name="entered_by")
	private Integer enteredBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="entered_on")
	private Date enteredOn;

	@Column(name="is_bill_to_address")
	private String isBillToAddress;

	@Column(name="is_ship_to_address")
	private String isShipToAddress;

	@Column(name="state_name")
	private String stateName;

	@Column(name="updated_comments")
	private String updatedComments;

	@Column(name="zip_code")
	private String zipCode;

	//bi-directional many-to-one association to MasClientAddress
	@OneToOne
	@JoinColumn(name="fk_client_address_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasClientAddressBO masClientAddressAuditClientAddressId;

	//bi-directional many-to-one association to MasClient
	@OneToOne
	@JoinColumn(name="fk_client_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasClientBO masClientAddressAuditClientId;

	//bi-directional many-to-one association to MasCountry
	@OneToOne
	@JoinColumn(name="fk_country_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasCountryBO masClientAddressAuditCountryId;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="entered_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO masClientAddressAuditEnteredBy;

	public MasClientAddressAuditBO() {
	}

	public int getPkClientAddressAuditId() {
		return this.pkClientAddressAuditId;
	}

	public void setPkClientAddressAuditId(int pkClientAddressAuditId) {
		this.pkClientAddressAuditId = pkClientAddressAuditId;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAddressLineOne() {
		return this.addressLineOne;
	}

	public void setAddressLineOne(String addressLineOne) {
		this.addressLineOne = addressLineOne;
	}

	public String getAddressLineTwo() {
		return this.addressLineTwo;
	}

	public void setAddressLineTwo(String addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Date getEnteredOn() {
		return this.enteredOn;
	}

	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}

	public String getIsBillToAddress() {
		return this.isBillToAddress;
	}

	public void setIsBillToAddress(String isBillToAddress) {
		this.isBillToAddress = isBillToAddress;
	}

	public String getIsShipToAddress() {
		return this.isShipToAddress;
	}

	public void setIsShipToAddress(String isShipToAddress) {
		this.isShipToAddress = isShipToAddress;
	}

	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getUpdatedComments() {
		return this.updatedComments;
	}

	public void setUpdatedComments(String updatedComments) {
		this.updatedComments = updatedComments;
	}

	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Short getFkClientAddressId() {
		return fkClientAddressId;
	}

	public void setFkClientAddressId(Short fkClientAddressId) {
		this.fkClientAddressId = fkClientAddressId;
	}

	public Short getFkClientId() {
		return fkClientId;
	}

	public void setFkClientId(Short fkClientId) {
		this.fkClientId = fkClientId;
	}

	public Short getFkCountryId() {
		return fkCountryId;
	}

	public void setFkCountryId(Short fkCountryId) {
		this.fkCountryId = fkCountryId;
	}

	public Integer getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(Integer enteredBy) {
		this.enteredBy = enteredBy;
	}

	public MasClientAddressBO getMasClientAddressAuditClientAddressId() {
		return masClientAddressAuditClientAddressId;
	}

	public void setMasClientAddressAuditClientAddressId(
			MasClientAddressBO masClientAddressAuditClientAddressId) {
		this.masClientAddressAuditClientAddressId = masClientAddressAuditClientAddressId;
	}

	public MasClientBO getMasClientAddressAuditClientId() {
		return masClientAddressAuditClientId;
	}

	public void setMasClientAddressAuditClientId(
			MasClientBO masClientAddressAuditClientId) {
		this.masClientAddressAuditClientId = masClientAddressAuditClientId;
	}

	public MasCountryBO getMasClientAddressAuditCountryId() {
		return masClientAddressAuditCountryId;
	}

	public void setMasClientAddressAuditCountryId(
			MasCountryBO masClientAddressAuditCountryId) {
		this.masClientAddressAuditCountryId = masClientAddressAuditCountryId;
	}

	public DatEmpDetailBO getMasClientAddressAuditEnteredBy() {
		return masClientAddressAuditEnteredBy;
	}

	public void setMasClientAddressAuditEnteredBy(
			DatEmpDetailBO masClientAddressAuditEnteredBy) {
		this.masClientAddressAuditEnteredBy = masClientAddressAuditEnteredBy;
	}

	@Override
	public String toString() {
		return "MasClientAddressAuditBO [pkClientAddressAuditId="
				+ pkClientAddressAuditId + ", action=" + action
				+ ", fkClientAddressId=" + fkClientAddressId + ", fkClientId="
				+ fkClientId + ", addressLineOne=" + addressLineOne
				+ ", addressLineTwo=" + addressLineTwo + ", cityName="
				+ cityName + ", fkCountryId=" + fkCountryId + ", enteredBy="
				+ enteredBy + ", enteredOn=" + enteredOn + ", isBillToAddress="
				+ isBillToAddress + ", isShipToAddress=" + isShipToAddress
				+ ", stateName=" + stateName + ", updatedComments="
				+ updatedComments + ", zipCode=" + zipCode
				+ ", masClientAddressAuditClientAddressId="
				+ masClientAddressAuditClientAddressId
				+ ", masClientAddressAuditClientId="
				+ masClientAddressAuditClientId
				+ ", masClientAddressAuditCountryId="
				+ masClientAddressAuditCountryId
				+ ", masClientAddressAuditEnteredBy="
				+ masClientAddressAuditEnteredBy + "]";
	}

}