package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToOne;

@javax.persistence.Entity
@javax.persistence.Table(name = "proj_dat_client_budget_rate_details")
@NamedNativeQuery(name = "ProjDatClientBudgetRateDetailBO.findAll", query = "SELECT p FROM ProjDatClientBudgetRateDetailBO p")
public class ProjDatClientBudgetRateDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_client_budget_rate_details_id")
	private Integer pkClientBudgetRateDetailsId;


	@Column(name = "fk_weekly_off_id")
	private Integer fkWeeklyOffId;

	@Column(name = "holiday_flag")
	private String holidayFlag;
	
	@Column(name = "fk_client_budget_id")
	private Integer fkClientBudgetId;
	
	@Column(name = "rate_amount")
	private BigDecimal rateAmount;

	@Column(name = "weekly_off_flag")
	private String weeklyOffFlag;

	@Column(name = "fk_invoice_rate_type_id")
	private Byte fkInvoiceRateTypeId;

	@Column(name = "fk_rate_type_id")
	private Byte fkRateTypeId;

	@OneToOne
	@JoinColumn(name = "fk_client_budget_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjDatClientBudgetBO projDatClientBudget;

	@OneToOne
	@JoinColumn(name = "fk_invoice_rate_type_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasInvoiceRateTypeBO masInvoiceRateType;

	@OneToOne
	@JoinColumn(name = "fk_rate_type_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasRateTypeBO masRateType;


	public void setPkClientBudgetRateDetailsId(Integer pkClientBudgetRateDetailsId) {
		this.pkClientBudgetRateDetailsId = pkClientBudgetRateDetailsId;
	}

	public Integer getPkClientBudgetRateDetailsId() {
		return pkClientBudgetRateDetailsId;
	}

	public Integer getFkWeeklyOffId() {
		return fkWeeklyOffId;
	}

	public void setFkWeeklyOffId(Integer fkWeeklyOffId) {
		this.fkWeeklyOffId = fkWeeklyOffId;
	}

	public String getHolidayFlag() {
		return holidayFlag;
	}

	public void setHolidayFlag(String holidayFlag) {
		this.holidayFlag = holidayFlag;
	}

	public Integer getFkClientBudgetId() {
		return fkClientBudgetId;
	}

	public void setFkClientBudgetId(Integer fkClientBudgetId) {
		this.fkClientBudgetId = fkClientBudgetId;
	}

	public BigDecimal getRateAmount() {
		return rateAmount;
	}

	public void setRateAmount(BigDecimal rateAmount) {
		this.rateAmount = rateAmount;
	}

	public String getWeeklyOffFlag() {
		return weeklyOffFlag;
	}

	public void setWeeklyOffFlag(String weeklyOffFlag) {
		this.weeklyOffFlag = weeklyOffFlag;
	}

	public Byte getFkInvoiceRateTypeId() {
		return fkInvoiceRateTypeId;
	}

	public void setFkInvoiceRateTypeId(Byte fkInvoiceRateTypeId) {
		this.fkInvoiceRateTypeId = fkInvoiceRateTypeId;
	}

	public Byte getFkRateTypeId() {
		return fkRateTypeId;
	}

	public void setFkRateTypeId(Byte fkRateTypeId) {
		this.fkRateTypeId = fkRateTypeId;
	}

	public ProjDatClientBudgetBO getProjDatClientBudget() {
		return projDatClientBudget;
	}

	public void setProjDatClientBudget(ProjDatClientBudgetBO projDatClientBudget) {
		this.projDatClientBudget = projDatClientBudget;
	}

	public MasInvoiceRateTypeBO getMasInvoiceRateType() {
		return masInvoiceRateType;
	}

	public void setMasInvoiceRateType(MasInvoiceRateTypeBO masInvoiceRateType) {
		this.masInvoiceRateType = masInvoiceRateType;
	}

	public MasRateTypeBO getMasRateType() {
		return masRateType;
	}

	public void setMasRateType(MasRateTypeBO masRateType) {
		this.masRateType = masRateType;
	}

	@Override
	public String toString() {
		return "ProjDatClientBudgetRateDetailBO [pkClientBudgetRateDetailsId="
				+ pkClientBudgetRateDetailsId + ", fkWeeklyOffId="
				+ fkWeeklyOffId + ", holidayFlag=" + holidayFlag
				+ ", fkClientBudgetId=" + fkClientBudgetId + ", rateAmount="
				+ rateAmount + ", weeklyOffFlag=" + weeklyOffFlag
				+ ", fkInvoiceRateTypeId=" + fkInvoiceRateTypeId
				+ ", fkRateTypeId=" + fkRateTypeId + ", projDatClientBudget="
				+ projDatClientBudget + ", masInvoiceRateType="
				+ masInvoiceRateType + ", masRateType=" + masRateType + "]";
	}

}
