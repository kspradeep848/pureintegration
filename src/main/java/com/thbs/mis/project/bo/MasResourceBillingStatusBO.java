package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the mas_resource_billing_status database table.
 * 
 */
@Entity
@Table(name="mas_resource_billing_status")
@NamedQuery(name="MasResourceBillingStatusBO.findAll", query="SELECT m FROM MasResourceBillingStatusBO m")
public class MasResourceBillingStatusBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_resource_billing_status_id")
	private byte pkResourceBillingStatusId;

	@Column(name="billing_status_name")
	private String billingStatusName;

	
	public MasResourceBillingStatusBO() {
	}

	public byte getPkResourceBillingStatusId() {
		return this.pkResourceBillingStatusId;
	}

	public void setPkResourceBillingStatusId(byte pkResourceBillingStatusId) {
		this.pkResourceBillingStatusId = pkResourceBillingStatusId;
	}

	public String getBillingStatusName() {
		return this.billingStatusName;
	}

	public void setBillingStatusName(String billingStatusName) {
		this.billingStatusName = billingStatusName;
	}

	@Override
	public String toString() {
		return "MasResourceBillingStatusBO [pkResourceBillingStatusId="
				+ pkResourceBillingStatusId + ", billingStatusName="
				+ billingStatusName + "]";
	}

	

}