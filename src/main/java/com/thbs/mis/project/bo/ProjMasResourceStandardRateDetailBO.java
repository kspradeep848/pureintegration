package com.thbs.mis.project.bo;


import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.MasCurrencytypeBO;
import com.thbs.mis.common.bo.MasEmpLevelBO;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the proj_mas_resource_standard_rate_details database table.
 * 
 */
@Entity
@Table(name="proj_mas_resource_standard_rate_details")
@NamedQuery(name="ProjMasResourceStandardRateDetailBO.findAll", query="SELECT p FROM ProjMasResourceStandardRateDetailBO p")
public class ProjMasResourceStandardRateDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_resource_standard_rate_details")
	private int pkResourceStandardRateDetails;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="employee_cost")
	private BigDecimal employeeCost;

	@Column(name="fk_created_by")
	private int fkCreatedBy;

	@Column(name="fk_employee_level_id")
	private short fkEmployeeLevelId;

	@Column(name="fk_updated_by")
	private int fkUpdatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;

	@Column(name="updation_comment")
	private String updationComment;

	@Temporal(TemporalType.DATE)
	@Column(name="valid_from")
	private Date validFrom;

	@Temporal(TemporalType.DATE)
	@Column(name="valid_to")
	private Date validTo;

	@Column(name="fk_currency_id")
	private Date fkCurrencyId;

	@Column(name="fk_thbs_budget_location_id")
	private Date fkThbsBudgetLocationId;

	//bi-directional many-to-one association to MasCurrencytype
	@OneToOne
	@JoinColumn(name="fk_currency_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasCurrencytypeBO masCurrencytype;

	//bi-directional many-to-one association to MasThbsBudgetLocation
	@OneToOne
	@JoinColumn(name="fk_thbs_budget_location_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasThbsBudgetLocationBO masThbsBudgetLocation;
	
	@OneToOne
	@JoinColumn(name="fk_employee_level_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpLevelBO masEmpLevelBO;
	
	

	public ProjMasResourceStandardRateDetailBO() {
	}

	public int getPkResourceStandardRateDetails() {
		return this.pkResourceStandardRateDetails;
	}

	public void setPkResourceStandardRateDetails(int pkResourceStandardRateDetails) {
		this.pkResourceStandardRateDetails = pkResourceStandardRateDetails;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public BigDecimal getEmployeeCost() {
		return this.employeeCost;
	}

	public void setEmployeeCost(BigDecimal employeeCost) {
		this.employeeCost = employeeCost;
	}

	public int getFkCreatedBy() {
		return this.fkCreatedBy;
	}

	public void setFkCreatedBy(int fkCreatedBy) {
		this.fkCreatedBy = fkCreatedBy;
	}

	public short getFkEmployeeLevelId() {
		return this.fkEmployeeLevelId;
	}

	public void setFkEmployeeLevelId(short fkEmployeeLevelId) {
		this.fkEmployeeLevelId = fkEmployeeLevelId;
	}

	public int getFkUpdatedBy() {
		return this.fkUpdatedBy;
	}

	public void setFkUpdatedBy(int fkUpdatedBy) {
		this.fkUpdatedBy = fkUpdatedBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdationComment() {
		return this.updationComment;
	}

	public void setUpdationComment(String updationComment) {
		this.updationComment = updationComment;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public MasCurrencytypeBO getMasCurrencytype() {
		return masCurrencytype;
	}

	public void setMasCurrencytype(MasCurrencytypeBO masCurrencytype) {
		this.masCurrencytype = masCurrencytype;
	}

	public MasThbsBudgetLocationBO getMasThbsBudgetLocation() {
		return masThbsBudgetLocation;
	}

	public void setMasThbsBudgetLocation(
			MasThbsBudgetLocationBO masThbsBudgetLocation) {
		this.masThbsBudgetLocation = masThbsBudgetLocation;
	}

	public Date getFkCurrencyId() {
		return fkCurrencyId;
	}

	public void setFkCurrencyId(Date fkCurrencyId) {
		this.fkCurrencyId = fkCurrencyId;
	}

	public Date getFkThbsBudgetLocationId() {
		return fkThbsBudgetLocationId;
	}

	public void setFkThbsBudgetLocationId(Date fkThbsBudgetLocationId) {
		this.fkThbsBudgetLocationId = fkThbsBudgetLocationId;
	}

	public MasEmpLevelBO getMasEmpLevelBO() {
		return masEmpLevelBO;
	}

	public void setMasEmpLevelBO(MasEmpLevelBO masEmpLevelBO) {
		this.masEmpLevelBO = masEmpLevelBO;
	}

	@Override
	public String toString() {
		return "ProjMasResourceStandardRateDetailBO [pkResourceStandardRateDetails="
				+ pkResourceStandardRateDetails
				+ ", createdOn="
				+ createdOn
				+ ", employeeCost="
				+ employeeCost
				+ ", fkCreatedBy="
				+ fkCreatedBy
				+ ", fkEmployeeLevelId="
				+ fkEmployeeLevelId
				+ ", fkUpdatedBy="
				+ fkUpdatedBy
				+ ", updatedOn="
				+ updatedOn
				+ ", updationComment="
				+ updationComment
				+ ", validFrom="
				+ validFrom
				+ ", validTo="
				+ validTo
				+ ", fkCurrencyId="
				+ fkCurrencyId
				+ ", fkThbsBudgetLocationId="
				+ fkThbsBudgetLocationId
				+ ", masCurrencytype="
				+ masCurrencytype
				+ ", masThbsBudgetLocation="
				+ masThbsBudgetLocation
				+ ", masEmpLevelBO="
				+ masEmpLevelBO
				+ "]";
	}

	
}