package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the proj_mas_comp_type database table.
 * 
 */
@Entity
@Table(name="proj_mas_comp_type")
@NamedQuery(name="ProjMasCompTypeBO.findAll", query="SELECT p FROM ProjMasCompTypeBO p")
public class ProjMasCompTypeBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_component_type_id")
	private byte pkComponentTypeId;

	@Column(name="component_type_name")
	private String componentTypeName;

	//bi-directional many-to-one association to DatComponentAudit
	/*@OneToMany(mappedBy="datComponentAuditComponentTypeId")
	private List<DatComponentAuditBO> datComponentAuditComponentTypeIdList;*/

	public ProjMasCompTypeBO() {
	}

	public byte getPkComponentTypeId() {
		return this.pkComponentTypeId;
	}

	public void setPkComponentTypeId(byte pkComponentTypeId) {
		this.pkComponentTypeId = pkComponentTypeId;
	}

	public String getComponentTypeName() {
		return this.componentTypeName;
	}

	public void setComponentTypeName(String componentTypeName) {
		this.componentTypeName = componentTypeName;
	}

	@Override
	public String toString() {
		return "ProjMasCompTypeBO [pkComponentTypeId=" + pkComponentTypeId
				+ ", componentTypeName=" + componentTypeName + "]";
	}

			

}