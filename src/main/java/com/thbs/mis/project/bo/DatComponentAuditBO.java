package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;


/**
 * The persistent class for the dat_component_audit database table.
 * 
 */
@Entity
@Table(name="dat_component_audit")
@NamedQuery(name="DatComponentAuditBO.findAll", query="SELECT d FROM DatComponentAuditBO d")
public class DatComponentAuditBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_component_audit_id")
	private int pkComponentAuditId;

	@Column(name="action")
	private String action;

	@Column(name="comments_for_updation")
	private String commentsForUpdation;

	@Column(name="comp_description")
	private String compDescription;

	@Temporal(TemporalType.DATE)
	@Column(name="comp_end_date")
	private Date compEndDate;

	@Temporal(TemporalType.DATE)
	@Column(name="comp_start_date")
	private Date compStartDate;

	@Column(name="component_amount")
	private BigDecimal componentAmount;

	@Column(name="component_name")
	private String componentName;

	@Column(name="fk_component_id")
	private Integer fkComponentId;


	@Column(name="entered_by")
	private Integer enteredBy;

	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="entered_on")
	private Date enteredOn;
	
	@Column(name="comp_history")
	private String compHistory;

	//bi-directional many-to-one association to DatComponent
	@OneToOne
	@JoinColumn(name="fk_component_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatComponentBO datComponentAuditComponentId;

	//bi-directional many-to-one association to ProjMasCompStatus
	@OneToOne
	@JoinColumn(name="fk_component_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasCompStatusBO datComponentAuditComponentStatusId;

	//bi-directional many-to-one association to ProjMasCompType
	@OneToOne
	@JoinColumn(name="fk_component_type_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasCompTypeBO datComponentAuditComponentTypeId;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="entered_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datComponentAuditEnteredBy;

	//bi-directional many-to-one association to DatWorkorder
	@OneToOne
	@JoinColumn(name="fk_workorder_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datComponentAuditWorkorderId;

	public DatComponentAuditBO() {
	}

	public int getPkComponentAuditId() {
		return this.pkComponentAuditId;
	}

	public void setPkComponentAuditId(int pkComponentAuditId) {
		this.pkComponentAuditId = pkComponentAuditId;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCommentsForUpdation() {
		return this.commentsForUpdation;
	}

	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}

	public String getCompDescription() {
		return this.compDescription;
	}

	public void setCompDescription(String compDescription) {
		this.compDescription = compDescription;
	}

	public Date getCompEndDate() {
		return this.compEndDate;
	}

	public void setCompEndDate(Date compEndDate) {
		this.compEndDate = compEndDate;
	}

	public Date getCompStartDate() {
		return this.compStartDate;
	}

	public void setCompStartDate(Date compStartDate) {
		this.compStartDate = compStartDate;
	}

	public BigDecimal getComponentAmount() {
		return this.componentAmount;
	}

	public void setComponentAmount(BigDecimal componentAmount) {
		this.componentAmount = componentAmount;
	}

	public String getComponentName() {
		return this.componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public Date getEnteredOn() {
		return this.enteredOn;
	}

	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}

	public DatComponentBO getDatComponentAuditComponentId() {
		return datComponentAuditComponentId;
	}

	public String getCompHistory() {
		return compHistory;
	}

	public void setCompHistory(String compHistory) {
		this.compHistory = compHistory;
	}

	public void setDatComponentAuditComponentId(
			DatComponentBO datComponentAuditComponentId) {
		this.datComponentAuditComponentId = datComponentAuditComponentId;
	}

	public ProjMasCompStatusBO getDatComponentAuditComponentStatusId() {
		return datComponentAuditComponentStatusId;
	}

	public void setDatComponentAuditComponentStatusId(
			ProjMasCompStatusBO datComponentAuditComponentStatusId) {
		this.datComponentAuditComponentStatusId = datComponentAuditComponentStatusId;
	}

	public ProjMasCompTypeBO getDatComponentAuditComponentTypeId() {
		return datComponentAuditComponentTypeId;
	}

	public void setDatComponentAuditComponentTypeId(
			ProjMasCompTypeBO datComponentAuditComponentTypeId) {
		this.datComponentAuditComponentTypeId = datComponentAuditComponentTypeId;
	}

	public DatEmpDetailBO getDatComponentAuditEnteredBy() {
		return datComponentAuditEnteredBy;
	}

	public void setDatComponentAuditEnteredBy(
			DatEmpDetailBO datComponentAuditEnteredBy) {
		this.datComponentAuditEnteredBy = datComponentAuditEnteredBy;
	}

	public DatWorkorderBO getDatComponentAuditWorkorderId() {
		return datComponentAuditWorkorderId;
	}

	public void setDatComponentAuditWorkorderId(
			DatWorkorderBO datComponentAuditWorkorderId) {
		this.datComponentAuditWorkorderId = datComponentAuditWorkorderId;
	}

	public Integer getFkComponentId() {
		return fkComponentId;
	}

	public void setFkComponentId(Integer fkComponentId) {
		this.fkComponentId = fkComponentId;
	}

	public Integer getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(Integer enteredBy) {
		this.enteredBy = enteredBy;
	}

	@Override
	public String toString() {
		return "DatComponentAuditBO [pkComponentAuditId=" + pkComponentAuditId
				+ ", action=" + action + ", commentsForUpdation="
				+ commentsForUpdation + ", compDescription=" + compDescription
				+ ", compEndDate=" + compEndDate + ", compStartDate="
				+ compStartDate + ", componentAmount=" + componentAmount
				+ ", componentName=" + componentName + ", fkComponentId="
				+ fkComponentId + ", enteredBy=" + enteredBy + ", enteredOn="
				+ enteredOn + ", datComponentAuditComponentId="
				+ datComponentAuditComponentId
				+ ", datComponentAuditComponentStatusId="
				+ datComponentAuditComponentStatusId
				+ ", datComponentAuditComponentTypeId="
				+ datComponentAuditComponentTypeId
				+ ", datComponentAuditEnteredBy=" + datComponentAuditEnteredBy
				+ ", datComponentAuditWorkorderId="
				+ datComponentAuditWorkorderId + "]";
	}

	
}