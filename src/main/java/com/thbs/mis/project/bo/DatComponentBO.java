package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.framework.util.CustomDateSerializer;

/**
 * The persistent class for the dat_component database table.
 * 
 */
@Entity
@Table(name = "dat_component")
@NamedQuery(name = "DatComponentBO.findAll", query = "SELECT d FROM DatComponentBO d")
public class DatComponentBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_component_id")
	private int pkComponentId;

	@Column(name = "comments_for_updation")
	private String commentsForUpdation;

	@Column(name="comp_description")
	private String compDescription;

	/*
	 * @Column(name="comp_discount_type") private String compDiscountType;
	 */

	@Temporal(TemporalType.DATE)
	@Column(name = "comp_end_date")
	private Date compEndDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "comp_start_date")
	private Date compStartDate;

	@Column(name = "component_amount")
	private BigDecimal componentAmount;

	/*
	 * @Column(name="component_discount") private BigDecimal componentDiscount;
	 */

	@Column(name = "component_name")
	private String componentName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_on")
	private Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_on")
	private Date updatedOn;

	@Column(name = "fk_component_status_id")
	private short fkComponentStatusId;

	@Column(name = "fk_component_type_id")
	private byte fkComponentTypeId;

	@Column(name = "created_by")
	private Integer createdBy;

	@Column(name = "updated_by")
	private Integer updatedBy;

	@Column(name = "fk_workorder_id")
	private int fkWorkorderId;
	
	@Column(name = "is_subproject_or_component")
	private String isSubprojectOrComponent;

	@Column(name = "comp_history")
	private String compHistory;

	// bi-directional many-to-one association to ProjMasCompStatus
	@OneToOne
	@JoinColumn(name = "fk_component_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasCompStatusBO projMasCompStatus;

	// bi-directional many-to-one association to ProjMasCompType
	@OneToOne
	@JoinColumn(name = "fk_component_type_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasCompTypeBO projMasCompType;

	// bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name = "created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail1;

	// bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name = "updated_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail2;

	// bi-directional many-to-one association to DatWorkorder
	@OneToOne
	@JoinColumn(name = "fk_workorder_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datWorkorder;

	public DatComponentBO() {
	}

	public int getPkComponentId() {
		return this.pkComponentId;
	}

	public void setPkComponentId(int pkComponentId) {
		this.pkComponentId = pkComponentId;
	}

	public String getCommentsForUpdation() {
		return this.commentsForUpdation;
	}

	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}

	public String getCompDescription() {
		return this.compDescription;
	}

	public void setCompDescription(String compDescription) {
		this.compDescription = compDescription;
	}
	
	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getCompEndDate() {
		return this.compEndDate;
	}

	public void setCompEndDate(Date compEndDate) {
		this.compEndDate = compEndDate;
	}

	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getCompStartDate() {
		return this.compStartDate;
	}

	public void setCompStartDate(Date compStartDate) {
		this.compStartDate = compStartDate;
	}

	public BigDecimal getComponentAmount() {
		return this.componentAmount;
	}

	public void setComponentAmount(BigDecimal componentAmount) {
		this.componentAmount = componentAmount;
	}

	public String getComponentName() {
		return this.componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public ProjMasCompStatusBO getProjMasCompStatus() {
		return projMasCompStatus;
	}

	public void setProjMasCompStatus(ProjMasCompStatusBO projMasCompStatus) {
		this.projMasCompStatus = projMasCompStatus;
	}

	public ProjMasCompTypeBO getProjMasCompType() {
		return projMasCompType;
	}

	public void setProjMasCompType(ProjMasCompTypeBO projMasCompType) {
		this.projMasCompType = projMasCompType;
	}

	public DatEmpDetailBO getDatEmpDetail1() {
		return datEmpDetail1;
	}

	public void setDatEmpDetail1(DatEmpDetailBO datEmpDetail1) {
		this.datEmpDetail1 = datEmpDetail1;
	}

	public DatEmpDetailBO getDatEmpDetail2() {
		return datEmpDetail2;
	}

	public void setDatEmpDetail2(DatEmpDetailBO datEmpDetail2) {
		this.datEmpDetail2 = datEmpDetail2;
	}

	public DatWorkorderBO getDatWorkorder() {
		return datWorkorder;
	}

	public void setDatWorkorder(DatWorkorderBO datWorkorder) {
		this.datWorkorder = datWorkorder;
	}

	public short getFkComponentStatusId() {
		return fkComponentStatusId;
	}

	public void setFkComponentStatusId(short fkComponentStatusId) {
		this.fkComponentStatusId = fkComponentStatusId;
	}

	public byte getFkComponentTypeId() {
		return fkComponentTypeId;
	}

	public void setFkComponentTypeId(byte fkComponentTypeId) {
		this.fkComponentTypeId = fkComponentTypeId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public int getFkWorkorderId() {
		return fkWorkorderId;
	}

	public void setFkWorkorderId(int fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}
	
	public String getIsSubprojectOrComponent() {
		return isSubprojectOrComponent;
	}

	public void setIsSubprojectOrComponent(String isSubprojectOrComponent) {
		this.isSubprojectOrComponent = isSubprojectOrComponent;
	}

	public String getCompHistory() {
		return compHistory;
	}

	public void setCompHistory(String compHistory) {
		this.compHistory = compHistory;
	}

	@Override
	public String toString() {
		return "DatComponentBO [pkComponentId=" + pkComponentId
				+ ", commentsForUpdation=" + commentsForUpdation
				+ ", compDescription=" + compDescription + ", compEndDate="
				+ compEndDate + ", compStartDate=" + compStartDate
				+ ", componentAmount=" + componentAmount + ", componentName="
				+ componentName + ", createdOn=" + createdOn + ", updatedOn="
				+ updatedOn + ", fkComponentStatusId=" + fkComponentStatusId
				+ ", fkComponentTypeId=" + fkComponentTypeId + ", createdBy="
				+ createdBy + ", updatedBy=" + updatedBy + ", fkWorkorderId="
				+ fkWorkorderId + ", isSubprojectOrComponent="
				+ isSubprojectOrComponent + ", projMasCompStatus="
				+ projMasCompStatus + ", projMasCompType=" + projMasCompType
				+ ", datEmpDetail1=" + datEmpDetail1 + ", datEmpDetail2="
				+ datEmpDetail2 + ", datWorkorder=" + datWorkorder + "]";
	}

	
}