package com.thbs.mis.project.bo;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the proj_client_budget_approval_details database table.
 * 
 */
@Entity
@Table(name="proj_client_budget_approval_details")
@NamedQuery(name="ProjClientBudgetApprovalDetailBO.findAll", query="SELECT p FROM ProjClientBudgetApprovalDetailBO p")
public class ProjClientBudgetApprovalDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_client_budget_approval_details_id")
	private int pkClientBudgetApprovalDetailsId;

	@Column(name="approved_or_rejected_by")
	private Integer approvedOrRejectedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="approved_or_rejected_on")
	private Date approvedOrRejectedOn;

	@Column(name="requested_by")
	private Integer requestedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="requested_on")
	private Date requestedOn;

	@Column(name="fk_budget_status_id")
	private Integer fkBudgetStatusId;

	@Column(name="fk_client_budget_id")
	private Integer fkClientBudgetId;

	//bi-directional many-to-one association to ProjBudgetStatus
	@OneToOne
	@JoinColumn(name="fk_budget_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjBudgetStatusBO projBudgetStatus;

	//bi-directional many-to-one association to ProjDatClientBudget
	@OneToOne
	@JoinColumn(name="fk_client_budget_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ProjDatClientBudgetBO projDatClientBudget;

/*	//bi-directional many-to-one association to ProjClientBudgetApprovalDetailsAudit
	@OneToMany(mappedBy="projClientBudgetApprovalDetail")
	private List<ProjClientBudgetApprovalDetailsAuditBO> projClientBudgetApprovalDetailsAudits;
*/
	public ProjClientBudgetApprovalDetailBO() {
	}

	public int getPkClientBudgetApprovalDetailsId() {
		return pkClientBudgetApprovalDetailsId;
	}

	public void setPkClientBudgetApprovalDetailsId(
			int pkClientBudgetApprovalDetailsId) {
		this.pkClientBudgetApprovalDetailsId = pkClientBudgetApprovalDetailsId;
	}

	public Integer getApprovedOrRejectedBy() {
		return approvedOrRejectedBy;
	}

	public void setApprovedOrRejectedBy(int approvedOrRejectedBy) {
		this.approvedOrRejectedBy = approvedOrRejectedBy;
	}

	public Date getApprovedOrRejectedOn() {
		return approvedOrRejectedOn;
	}

	public void setApprovedOrRejectedOn(Date approvedOrRejectedOn) {
		this.approvedOrRejectedOn = approvedOrRejectedOn;
	}

	public Integer getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(int requestedBy) {
		this.requestedBy = requestedBy;
	}

	public Date getRequestedOn() {
		return requestedOn;
	}

	public void setRequestedOn(Date requestedOn) {
		this.requestedOn = requestedOn;
	}

	public ProjDatClientBudgetBO getProjDatClientBudget() {
		return projDatClientBudget;
	}

	public void setProjDatClientBudget(ProjDatClientBudgetBO projDatClientBudget) {
		this.projDatClientBudget = projDatClientBudget;
	}

	public ProjBudgetStatusBO getProjBudgetStatus() {
		return projBudgetStatus;
	}

	public void setProjBudgetStatus(ProjBudgetStatusBO projBudgetStatus) {
		this.projBudgetStatus = projBudgetStatus;
	}


	public Integer getFkBudgetStatusId() {
		return fkBudgetStatusId;
	}

	public void setFkBudgetStatusId(Integer fkBudgetStatusId) {
		this.fkBudgetStatusId = fkBudgetStatusId;
	}

	public Integer getFkClientBudgetId() {
		return fkClientBudgetId;
	}

	public void setFkClientBudgetId(Integer fkClientBudgetId) {
		this.fkClientBudgetId = fkClientBudgetId;
	}

	

}