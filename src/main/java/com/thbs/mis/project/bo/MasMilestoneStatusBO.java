package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the mas_milestone_status database table.
 * 
 */
@Entity
@Table(name="mas_milestone_status")
@NamedQuery(name="MasMilestoneStatusBO.findAll", query="SELECT m FROM MasMilestoneStatusBO m")
public class MasMilestoneStatusBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_milestone_status_id")
	private byte pkMilestoneStatusId;

	@Column(name="milestone_status_name")
	private String milestoneStatusName;

	//bi-directional many-to-one association to DatMilestone
	/*@OneToMany(mappedBy="masMilestoneStatus")
	private List<DatMilestone> datMilestones;
*/
	public MasMilestoneStatusBO() {
	}

	public byte getPkMilestoneStatusId() {
		return this.pkMilestoneStatusId;
	}

	public void setPkMilestoneStatusId(byte pkMilestoneStatusId) {
		this.pkMilestoneStatusId = pkMilestoneStatusId;
	}

	public String getMilestoneStatusName() {
		return this.milestoneStatusName;
	}

	public void setMilestoneStatusName(String milestoneStatusName) {
		this.milestoneStatusName = milestoneStatusName;
	}

	@Override
	public String toString() {
		return "MasMilestoneStatusBO [pkMilestoneStatusId="
				+ pkMilestoneStatusId + ", milestoneStatusName="
				+ milestoneStatusName + "]";
	}

	

}