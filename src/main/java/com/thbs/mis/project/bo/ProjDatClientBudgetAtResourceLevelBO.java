package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasEmpDesignationBO;


/**
 * The persistent class for the proj_dat_client_budget_at_resource_level database table.
 * 
 */
@Entity
@Table(name="proj_dat_client_budget_at_resource_level")
@NamedQuery(name="ProjDatClientBudgetAtResourceLevelBO.findAll", query="SELECT p FROM ProjDatClientBudgetAtResourceLevelBO p")
public class ProjDatClientBudgetAtResourceLevelBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_client_budget_resource_id")
	private int pkClientBudgetResourceId;

	@Column(name="budgeted_days")
	private int budgetedDays;

	@Column(name="comnents_for_updation")
	private String comnentsForUpdation;

	@Column(name="cost_to_client")
	private BigDecimal costToClient;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	/*@Temporal(TemporalType.DATE)
	@Column(name="end_date")
	private Date endDate;*/

	@Column(name="invoice_rate")
	private BigDecimal invoiceRate;

	@Column(name="resource_location")
	private String resourceLocation;
	
	@Column(name="no_of_resource")
	private int noOfResource;

	/*@Temporal(TemporalType.DATE)
	@Column(name="start_date")
	private Date startDate;*/

	@Column(name="updated_by")
	private Integer updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;
	
	@Column(name="created_by")
	private Integer createdBy;
	
	@Column(name="fk_resource_designation_id")
	private short fkResourceDesignationId;
	
	@Column(name="fk_workorder_id")
	private Integer fkWorkorderId;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail;

	//bi-directional many-to-one association to MasEmpDesignation
	@OneToOne
	@JoinColumn(name="fk_resource_designation_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpDesignationBO masEmpDesignation;

	//bi-directional many-to-one association to DatWorkorder
	@OneToOne
	@JoinColumn(name="fk_workorder_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datWorkorder;
	
	@OneToOne
	@JoinColumn(name="updated_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail1;

	public ProjDatClientBudgetAtResourceLevelBO() {
	}

	public Integer getPkClientBudgetResourceId() {
		return this.pkClientBudgetResourceId;
	}

	public void setPkClientBudgetResourceId(int pkClientBudgetResourceId) {
		this.pkClientBudgetResourceId = pkClientBudgetResourceId;
	}

	public int getBudgetedDays() {
		return this.budgetedDays;
	}

	public void setBudgetedDays(int budgetedDays) {
		this.budgetedDays = budgetedDays;
	}

	public String getComnentsForUpdation() {
		return this.comnentsForUpdation;
	}

	public void setComnentsForUpdation(String comnentsForUpdation) {
		this.comnentsForUpdation = comnentsForUpdation;
	}

	public BigDecimal getCostToClient() {
		return this.costToClient;
	}

	public void setCostToClient(BigDecimal costToClient) {
		this.costToClient = costToClient;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

/*	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}*/

	public BigDecimal getInvoiceRate() {
		return this.invoiceRate;
	}

	public void setInvoiceRate(BigDecimal invoiceRate) {
		this.invoiceRate = invoiceRate;
	}

	public String getResourceLocation() {
		return this.resourceLocation;
	}

	public void setResourceLocation(String resourceLocation) {
		this.resourceLocation = resourceLocation;
	}

	/*public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}*/

	public Integer getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public DatEmpDetailBO getDatEmpDetail() {
		return datEmpDetail;
	}

	public void setDatEmpDetail(DatEmpDetailBO datEmpDetail) {
		this.datEmpDetail = datEmpDetail;
	}

	public DatWorkorderBO getDatWorkorder() {
		return datWorkorder;
	}

	public void setDatWorkorder(DatWorkorderBO datWorkorder) {
		this.datWorkorder = datWorkorder;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	public Integer getFkWorkorderId() {
		return fkWorkorderId;
	}

	public void setFkWorkorderId(Integer fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}

	public int getNoOfResource() {
		return noOfResource;
	}

	public void setNoOfResource(int noOfResource) {
		this.noOfResource = noOfResource;
	}

	public DatEmpDetailBO getDatEmpDetail1() {
		return datEmpDetail1;
	}

	public void setDatEmpDetail1(DatEmpDetailBO datEmpDetail1) {
		this.datEmpDetail1 = datEmpDetail1;
	}

	public short getFkResourceDesignationId() {
		return fkResourceDesignationId;
	}

	public void setFkResourceDesignationId(short fkResourceDesignationId) {
		this.fkResourceDesignationId = fkResourceDesignationId;
	}

	public MasEmpDesignationBO getMasEmpDesignation() {
		return masEmpDesignation;
	}

	public void setMasEmpDesignation(MasEmpDesignationBO masEmpDesignation) {
		this.masEmpDesignation = masEmpDesignation;
	}

	@Override
	public String toString() {
		return "ProjDatClientBudgetAtResourceLevelBO [pkClientBudgetResourceId="
				+ pkClientBudgetResourceId
				+ ", budgetedDays="
				+ budgetedDays
				+ ", comnentsForUpdation="
				+ comnentsForUpdation
				+ ", costToClient="
				+ costToClient
				+ ", createdOn="
				+ createdOn
				+ ", invoiceRate="
				+ invoiceRate
				+ ", resourceLocation="
				+ resourceLocation
				+ ", noOfResource="
				+ noOfResource
				+ ", updatedBy="
				+ updatedBy
				+ ", updatedOn="
				+ updatedOn
				+ ", createdBy="
				+ createdBy
				+ ", fkResourceDesignationId="
				+ fkResourceDesignationId
				+ ", fkWorkorderId="
				+ fkWorkorderId
				+ ", datEmpDetail="
				+ datEmpDetail
				+ ", masEmpDesignation="
				+ masEmpDesignation
				+ ", datWorkorder="
				+ datWorkorder
				+ ", datEmpDetail1=" + datEmpDetail1 + "]";
	}

	

}