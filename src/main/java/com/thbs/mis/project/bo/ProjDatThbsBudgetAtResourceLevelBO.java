package com.thbs.mis.project.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasEmpDesignationBO;
import com.thbs.mis.common.bo.MasEmpLevelBO;


/**
 * The persistent class for the proj_dat_thbs_budget_at_resource_level database table.
 * 
 */
@Entity
@Table(name="proj_dat_thbs_budget_at_resource_level")
@NamedQuery(name="ProjDatThbsBudgetAtResourceLevelBO.findAll", query="SELECT p FROM ProjDatThbsBudgetAtResourceLevelBO p")
public class ProjDatThbsBudgetAtResourceLevelBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_thbs_budget_resource_id")
	private Integer pkThbsBudgetResourceId;

	@Column(name="budgeted_days")
	private Integer budgetedDays;

	@Column(name="comments_for_updation")
	private String commentsForUpdation;

	@Column(name="cost_to_company")
	private BigDecimal costToCompany;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	/*@Temporal(TemporalType.DATE)
	@Column(name="end_date")
	private Date endDate;*/

	@Column(name="resource_location")
	private String resourceLocation;

	@Column(name="standard_rate")
	private BigDecimal standardRate;

/*	@Temporal(TemporalType.DATE)
	@Column(name="start_date")
	private Date startDate;*/

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;
	
	@Column(name="no_of_resource")
	private Integer noOfResource;

	@Column(name="created_by")
	private Integer createdBy;
	
	@Column(name="fk_resource_level_id")
	private short fkResourceLevelId;
	
	@Column(name="fk_resource_designation_id")
	private short fkResourceDesignationId;
	
	@Column(name="updated_by")
	private Integer updatedBy;
	
	@Column(name="fk_workorder_id")
	private Integer fkWorkorderId;
	
	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail1;

	//bi-directional many-to-one association to MasEmpLevel
	@OneToOne
	@JoinColumn(name="fk_resource_level_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpLevelBO masEmpLevel;

	//bi-directional many-to-one association to MasEmpDesignation
	@OneToOne
	@JoinColumn(name="fk_resource_designation_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpDesignationBO masEmpDesignation;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="updated_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail2;

	//bi-directional many-to-one association to DatWorkorder
	@OneToOne
	@JoinColumn(name="fk_workorder_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datWorkorder;

	public ProjDatThbsBudgetAtResourceLevelBO() {
	}

	public Integer getPkThbsBudgetResourceId() {
		return this.pkThbsBudgetResourceId;
	}

	public void setPkThbsBudgetResourceId(Integer pkThbsBudgetResourceId) {
		this.pkThbsBudgetResourceId = pkThbsBudgetResourceId;
	}

	public Integer getBudgetedDays() {
		return this.budgetedDays;
	}

	public void setBudgetedDays(Integer budgetedDays) {
		this.budgetedDays = budgetedDays;
	}

	public String getCommentsForUpdation() {
		return this.commentsForUpdation;
	}

	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}

	public BigDecimal getCostToCompany() {
		return this.costToCompany;
	}

	public void setCostToCompany(BigDecimal costToCompany) {
		this.costToCompany = costToCompany;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/*public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}*/

	public String getResourceLocation() {
		return this.resourceLocation;
	}

	public void setResourceLocation(String resourceLocation) {
		this.resourceLocation = resourceLocation;
	}

	public BigDecimal getStandardRate() {
		return this.standardRate;
	}

	public void setStandardRate(BigDecimal standardRate) {
		this.standardRate = standardRate;
	}

	/*public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}*/

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public short getFkResourceLevelId() {
		return fkResourceLevelId;
	}

	public void setFkResourceLevelId(short fkResourceLevelId) {
		this.fkResourceLevelId = fkResourceLevelId;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getFkWorkorderId() {
		return fkWorkorderId;
	}

	public void setFkWorkorderId(Integer fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}

	public DatEmpDetailBO getDatEmpDetail1() {
		return datEmpDetail1;
	}

	public void setDatEmpDetail1(DatEmpDetailBO datEmpDetail1) {
		this.datEmpDetail1 = datEmpDetail1;
	}

	public MasEmpLevelBO getMasEmpLevel() {
		return masEmpLevel;
	}

	public void setMasEmpLevel(MasEmpLevelBO masEmpLevel) {
		this.masEmpLevel = masEmpLevel;
	}

	public DatEmpDetailBO getDatEmpDetail2() {
		return datEmpDetail2;
	}

	public void setDatEmpDetail2(DatEmpDetailBO datEmpDetail2) {
		this.datEmpDetail2 = datEmpDetail2;
	}

	public DatWorkorderBO getDatWorkorder() {
		return datWorkorder;
	}

	public void setDatWorkorder(DatWorkorderBO datWorkorder) {
		this.datWorkorder = datWorkorder;
	}

	public Integer getNoOfResource() {
		return noOfResource;
	}

	public void setNoOfResource(Integer noOfResource) {
		this.noOfResource = noOfResource;
	}

	public short getFkResourceDesignationId() {
		return fkResourceDesignationId;
	}

	public void setFkResourceDesignationId(short fkResourceDesignationId) {
		this.fkResourceDesignationId = fkResourceDesignationId;
	}

	public MasEmpDesignationBO getMasEmpDesignation() {
		return masEmpDesignation;
	}

	public void setMasEmpDesignation(MasEmpDesignationBO masEmpDesignation) {
		this.masEmpDesignation = masEmpDesignation;
	}

	@Override
	public String toString() {
		return "ProjDatThbsBudgetAtResourceLevelBO [pkThbsBudgetResourceId="
				+ pkThbsBudgetResourceId + ", budgetedDays=" + budgetedDays
				+ ", commentsForUpdation=" + commentsForUpdation
				+ ", costToCompany=" + costToCompany + ", createdOn="
				+ createdOn + ", resourceLocation=" + resourceLocation
				+ ", standardRate=" + standardRate + ", updatedOn=" + updatedOn
				+ ", noOfResource=" + noOfResource + ", createdBy=" + createdBy
				+ ", fkResourceLevelId=" + fkResourceLevelId
				+ ", fkResourceDesignationId=" + fkResourceDesignationId
				+ ", updatedBy=" + updatedBy + ", fkWorkorderId="
				+ fkWorkorderId + ", datEmpDetail1=" + datEmpDetail1
				+ ", masEmpLevel=" + masEmpLevel + ", masEmpDesignation="
				+ masEmpDesignation + ", datEmpDetail2=" + datEmpDetail2
				+ ", datWorkorder=" + datWorkorder + "]";
	}

	
	
}