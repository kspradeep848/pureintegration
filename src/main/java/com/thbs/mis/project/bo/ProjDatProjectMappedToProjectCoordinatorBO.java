package com.thbs.mis.project.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;


/**
 * The persistent class for the proj_dat_project_mapped_to_project_coordinator database table.
 * 
 */
@Entity
@Table(name="proj_dat_project_mapped_to_project_coordinator")
@NamedQuery(name="ProjDatProjectMappedToProjectCoordinatorBO.findAll", query="SELECT p FROM ProjDatProjectMappedToProjectCoordinatorBO p")
public class ProjDatProjectMappedToProjectCoordinatorBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_project_mappedto_pc_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pkProjectMappedtoPcId;
	
	@Column(name="fk_project_id")
	private Integer fkProjectId;
	
	@Column(name="fk_project_coordinator_id")
	private Integer fkProjectCoordinatorId;
	
	//bi-directional many-to-one association to MasProject
	@OneToOne
	@JoinColumn(name="fk_project_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasProjectBO masProject;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="fk_project_coordinator_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail;

	public ProjDatProjectMappedToProjectCoordinatorBO() {
	}

	public Integer getPkProjectMappedtoPcId() {
		return this.pkProjectMappedtoPcId;
	}

	public void setPkProjectMappedtoPcId(Integer pkProjectMappedtoPcId) {
		this.pkProjectMappedtoPcId = pkProjectMappedtoPcId;
	}

	public MasProjectBO getMasProject() {
		return this.masProject;
	}

	public void setMasProject(MasProjectBO masProject) {
		this.masProject = masProject;
	}

	public DatEmpDetailBO getDatEmpDetail() {
		return this.datEmpDetail;
	}

	public void setDatEmpDetail(DatEmpDetailBO datEmpDetail) {
		this.datEmpDetail = datEmpDetail;
	}

	public int getFkProjectId() {
		return fkProjectId;
	}

	public void setFkProjectId(int fkProjectId) {
		this.fkProjectId = fkProjectId;
	}

	public Integer getFkProjectCoordinatorId() {
		return fkProjectCoordinatorId;
	}

	public void setFkProjectCoordinatorId(Integer fkProjectCoordinatorId) {
		this.fkProjectCoordinatorId = fkProjectCoordinatorId;
	}

	@Override
	public String toString() {
		return "ProjDatProjectMappedToProjectCoordinatorBO [pkProjectMappedtoPcId="
				+ pkProjectMappedtoPcId
				+ ", fkProjectId="
				+ fkProjectId
				+ ", fkProjectCoordinatorId="
				+ fkProjectCoordinatorId
				+ ", masProject="
				+ masProject
				+ ", datEmpDetail="
				+ datEmpDetail + "]";
	}

	
	
}