package com.thbs.mis.project.service;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.projectnotification.service.ProjectNotificationService;
import com.thbs.mis.project.bean.MasMilestoneStatusBean;
import com.thbs.mis.project.bean.MilestoneBean;
import com.thbs.mis.project.bean.MilestoneInputBean;
import com.thbs.mis.project.bean.MilestoneOutputBean;
import com.thbs.mis.project.bean.SearchMilestoneBean;
import com.thbs.mis.project.bean.SearchMilestoneOutputBean;
import com.thbs.mis.project.bean.TriggerInput;
import com.thbs.mis.project.bean.TriggerInputDetails;
import com.thbs.mis.project.bo.DatComponentBO;
import com.thbs.mis.project.bo.DatMilestoneBO;
import com.thbs.mis.project.bo.DatWorkorderBO;
import com.thbs.mis.project.bo.MasMilestoneStatusBO;
import com.thbs.mis.project.bo.MasProjectBO;
import com.thbs.mis.project.bo.ProjDatProjectMappedToProjectCoordinatorBO;
import com.thbs.mis.project.constants.ProjectConstants;
import com.thbs.mis.project.dao.DatComponentRepository;
import com.thbs.mis.project.dao.DatMilestoneRepository;
import com.thbs.mis.project.dao.DatWorkorderRepository;
import com.thbs.mis.project.dao.MasMilestoneStatusRepository;
import com.thbs.mis.project.dao.MasProjectRepository;
import com.thbs.mis.project.dao.MilestoneSpecificDetails;
import com.thbs.mis.project.dao.ProjDatProjectMappedToProjectCoordinatorRepository;

@Service
public class MilestoneService {
	@Autowired
	private DatMilestoneRepository milestoneRepository;

	@Autowired
	private MasMilestoneStatusRepository masMilestonerepository;
	@Autowired
	private DatComponentRepository  datComponentRepository;
	@Autowired
	private DatComponentRepository comphonentRepository;

	@Autowired
	private ProjDatProjectMappedToProjectCoordinatorRepository projDatProjectMappedToProjectCoordinatorRepository;

	@Autowired
	private DatWorkorderRepository datWorkorderRepository;
	
	@Autowired
	private MasProjectRepository masProjectRepository;
	
	@Autowired
	private MasMilestoneStatusRepository masMilestoneStatusRepository;
	
	@Autowired
	private ProjectNotificationService projectNotificationService;
	
	/*@Autowired
	private InvoiceDatPurchaseOrderRepository invoiceDatPurchaseOrderRepository;
	
	@Autowired
	private DatMilestoneRepository datMilestoneRepository;*/
	
	@Value("${mileStone.status.type1}")
	private String mileStoneStatusType1;
	
	@Value("${mileStone.status.type2}")
	private String mileStoneStatusType2;
	
	@Value("${project.active.emp.status}")
	private byte activeEmpStatus;
	
	@Value("${project.message.specification.from.to}")
	private String specificationFromTo;

	@Value("${project.history.pattern}")
	private String projectHistoryCommentsPattern;

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory.getLog(MilestoneService.class);

	public MilestoneOutputBean createMilestoneService(MilestoneInputBean bean) throws CommonCustomException,DataAccessException {
		BigDecimal sumOfMilestones;
		DatMilestoneBO milestoneBO = new DatMilestoneBO();
		MilestoneOutputBean milestoneOutputBean = new MilestoneOutputBean();
		DatMilestoneBO milestone = null;
		DatComponentBO compId = null;
		List<ProjDatProjectMappedToProjectCoordinatorBO> coordinatorDetails = null;
		DatWorkorderBO projId = null;
		try {
			compId = comphonentRepository.getcompIdDetails(bean.getComponentId());
		} catch (Exception e) {
		//	e.printStackTrace();
			throw new DataAccessException("Something went wrong while creating Milestone");
		}
		if(compId==null)
			throw new CommonCustomException("Component details doesn't exist");
		if(compId.getFkComponentTypeId()==ProjectConstants.COMPONENT_TYPE_ID_TNM)
			throw new CommonCustomException("Cannot create milestone for time and material component.");
		
		try {
			projId = datWorkorderRepository.findByPkWorkorderId(compId.getFkWorkorderId());
		} catch (Exception e) {
			throw new DataAccessException("Something went wrong while creating Milestone");
		}
		try {
			coordinatorDetails = projDatProjectMappedToProjectCoordinatorRepository
					.findByFkProjectId(projId.getFkProjectId());
		} catch (Exception e) {
			//e.printStackTrace();
			throw new DataAccessException("Something went wrong while creating Milestone");
		}
		List<Integer> empIds = new ArrayList<Integer>();
		for (ProjDatProjectMappedToProjectCoordinatorBO bo : coordinatorDetails) {
			empIds.add(bo.getFkProjectCoordinatorId());
		}
		Boolean b = empIds.contains(bean.getMileStonecreatedBy());
		if (!b)
			throw new CommonCustomException("You are not the authorized person to create the milestone");
		try {
			milestone = milestoneRepository.findByMilestoneName(bean.getMilestoneName());
		} catch (Exception e) {
		//	e.printStackTrace();
			throw new DataAccessException("unable to create mileStone. ");
		}
		try {
			sumOfMilestones = milestoneRepository.getSumOfMilestoneValue(bean.getComponentId());
		} catch (Exception e) {
		//	e.printStackTrace();
			throw new DataAccessException("unable to create mileStone. ");
		}
		if(bean.getMilestoneStatusId().intValue() !=1 && bean.getMilestoneStatusId().intValue() !=2)
			throw new CommonCustomException("Please select work in progress or not started status for milestone");

		if (milestone == null) {
			milestoneBO.setMilestoneName(bean.getMilestoneName());
		} else {
			throw new CommonCustomException("Milestone Name is allready existed");
		}
		milestoneBO.setFkMilestoneStatusId(bean.getMilestoneStatusId());
		if(sumOfMilestones == null)
			sumOfMilestones = new BigDecimal(0);
		BigDecimal sumOfMilstoneValue = bean.getMilestoneValue()
				.add(sumOfMilestones);
		if (sumOfMilstoneValue.compareTo(compId.getComponentAmount()) > 0)
			throw new CommonCustomException("Milestone value exceeds component value");
		milestoneBO.setMilestoneValue(bean.getMilestoneValue());
		try {
			milestoneBO.setMielstoneType(bean.getMilestoneType());
			milestoneBO.setFkComponentId(bean.getComponentId());
			milestoneBO.setCreatedBy(bean.getMileStonecreatedBy());
			if(bean.getEnterLineItem()!=null)
			milestoneBO.setLineItem(bean.getEnterLineItem());
			if(bean.getQuantity()!=null)
			milestoneBO.setQuantity(bean.getQuantity());
			else
				milestoneBO.setQuantity(new BigDecimal(0));		
			if(bean.getUnitPrice()!=null)
			milestoneBO.setUnitPrice(bean.getUnitPrice());
			else
				milestoneBO.setUnitPrice(new BigDecimal(0));
			milestoneBO.setCreatedOn(new Date());
			milestoneBO.setMilestoneRemainingValue(bean.getMilestoneValue());
			milestoneBO.setMilestoneEndDate(bean.getMileStoneEndDate());
			try {
					milestoneBO = milestoneRepository.save(milestoneBO);
			} catch (Exception e) {
				//		e.printStackTrace();
				throw new DataAccessException("Something went wrong while creating Milestone.");
			}
			if(milestoneBO != null) {
				projectNotificationService.sendMileStoneCreatedNotificationToPC(milestoneBO.getPkMilestoneId());
			}
			updateStatus(bean.getComponentId());
			milestoneOutputBean.setMilestoneId(milestoneBO.getPkMilestoneId());
			milestoneOutputBean.setMilestoneName(milestoneBO.getMilestoneName());
			milestoneOutputBean.setMilestoneValue(milestoneBO.getMilestoneValue());
			milestoneOutputBean.setMilestoneType(milestoneBO.getMielstoneType());
			milestoneOutputBean.setComponentId(milestoneBO.getFkComponentId());
			milestoneOutputBean.setComponentName(compId.getComponentName());
			milestoneOutputBean.setMileStoneEndDate(milestoneBO.getMilestoneEndDate());
			milestoneOutputBean.setMileStonecreatedBy(milestoneBO.getCreatedBy());
			milestoneOutputBean.setEnterLineItem(milestoneBO.getLineItem());
			milestoneOutputBean.setQuantity(milestoneBO.getQuantity());
			milestoneOutputBean.setUnitPrice(milestoneBO.getUnitPrice());
			milestoneOutputBean.setMilestoneStatusId(milestoneBO.getFkMilestoneStatusId());
		} catch (Exception e) {
		//	e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		return milestoneOutputBean;
	}

	public MilestoneOutputBean updateMilestoneService(MilestoneInputBean bean) throws CommonCustomException, DataAccessException {
		MilestoneOutputBean milestoneOutputBean = new MilestoneOutputBean();
		DatMilestoneBO boBean = null;
		DatComponentBO datComponentBO = null;
		DatWorkorderBO datWorkorderBO=null;
		MasMilestoneStatusBO masMilestoneStatusBO;
		StringBuilder  updateComments = new StringBuilder();
		String oldName = "";
		
		try {
			boBean = milestoneRepository.findByPkMilestoneId(bean.getMilestoneId());
		} catch (Exception e) {
			throw new CommonCustomException("milestone details are not found");
		}
		if(boBean==null)
			throw new CommonCustomException("Milestone details doesn't exist");
		oldName = boBean.getMilestoneName();
		 try {
			 datComponentBO = comphonentRepository.getcompIdDetails(bean.getComponentId());
		} catch (Exception e1) {
			throw new DataAccessException("Something went wrong while updating Milestone");
		}
		 if(bean.getCommentsForUpdate()==null)
				throw new CommonCustomException("Please add the comments");
		if(datComponentBO==null)
			throw new CommonCustomException("component details doesn't exist");
		try {
			datWorkorderBO = datComponentBO.getDatWorkorder();
		} catch (Exception e1) {
			throw new DataAccessException("Something went wrong while updating Milestone");
		}
		if(datWorkorderBO==null)
			throw new CommonCustomException("work order details doesn't exist");
		List<ProjDatProjectMappedToProjectCoordinatorBO> coordinatorDetails = null;
		try {
			coordinatorDetails = projDatProjectMappedToProjectCoordinatorRepository
					.findByFkProjectId(datWorkorderBO.getFkProjectId());
		} catch (Exception e) {
			throw new DataAccessException("Something went wrong while creating Milestone");
		}
		List<Integer> empIds = new ArrayList<Integer>();
		for (ProjDatProjectMappedToProjectCoordinatorBO bo : coordinatorDetails) {
			empIds.add(bo.getFkProjectCoordinatorId());
		}
		Boolean b = empIds.contains(bean.getMilestoneUpdatedBy());
		if (!b)
			throw new CommonCustomException("You are not the authorized person to update milestone");
		
		if(!bean.getMilestoneName().equals(boBean.getMilestoneName()))
			updateComments.append(MessageFormat.format(specificationFromTo,"Milestone name"
					,boBean.getMilestoneName(),bean.getMilestoneName()));
		
		if(!(bean.getMilestoneValue().compareTo(boBean.getMilestoneValue())==0))
			updateComments.append(MessageFormat.format(specificationFromTo,"Milestone value"
					,boBean.getMilestoneValue(),bean.getMilestoneValue()));
		
		if(!bean.getMilestoneType().equalsIgnoreCase(boBean.getMielstoneType()))
			updateComments.append(MessageFormat.format(specificationFromTo,"Milestone type"
					,boBean.getMielstoneType(),bean.getMilestoneType()));
		
		
		if(bean.getQuantity()==null)
			bean.setQuantity(new BigDecimal(0));
		
		if(bean.getUnitPrice()==null)
			bean.setUnitPrice(new BigDecimal(0));
		
		//Checking Line item is modified or not
		if(bean.getEnterLineItem()!=null && boBean.getLineItem()!=null)
			{
			if(!bean.getEnterLineItem().equals(boBean.getLineItem()))
				updateComments.append(MessageFormat.format(specificationFromTo,"Line item"
						,boBean.getLineItem(),bean.getEnterLineItem()));
			}
			else if(bean.getEnterLineItem()!=null && boBean.getLineItem()==null){
					updateComments.append(MessageFormat.format(specificationFromTo,"Line item"
							,boBean.getLineItem(),bean.getEnterLineItem()));
			}
			else if(bean.getEnterLineItem()==null && boBean.getLineItem()!=null){
				updateComments.append(MessageFormat.format(specificationFromTo,"Line item"
						,boBean.getLineItem(),bean.getEnterLineItem()));
			}
		
			
			if(!(bean.getQuantity().compareTo(boBean.getQuantity())==0))
				updateComments.append(MessageFormat.format(specificationFromTo,"Quantity"
						,boBean.getQuantity(),bean.getQuantity()));
			
			
			
			if(!(bean.getUnitPrice().compareTo(boBean.getUnitPrice())==0))
				updateComments.append(MessageFormat.format(specificationFromTo,"Unit price"
						,boBean.getUnitPrice(),bean.getUnitPrice()));
		
	
		
		//Getting only Date excluding TimeStamp for checking date
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    	String formattedNewEndDate = formatter.format(bean.getMileStoneEndDate());
    	String formattedOldEndDate = formatter.format(boBean.getMilestoneEndDate());		
		
		
		if(!formattedNewEndDate.equals(formattedOldEndDate))
			updateComments.append(MessageFormat.format(specificationFromTo,"EndDate"
					,formattedOldEndDate,formattedNewEndDate));
		
		try{
			masMilestoneStatusBO = masMilestoneStatusRepository.findByPkMilestoneStatusId(bean.getMilestoneStatusId());
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to update milestone");
		}
		
		if(masMilestoneStatusBO==null)
		{
			throw new CommonCustomException("Enter valid milestone status");
		}
		
		if(!bean.getMilestoneStatusId().equals(boBean.getFkMilestoneStatusId()))
			updateComments.append(MessageFormat.format(specificationFromTo,"Milestone Status"
					,boBean.getMasMilestoneStatus().getMilestoneStatusName(),
					masMilestoneStatusBO.getMilestoneStatusName()));
		
		boBean.setMilestoneName(bean.getMilestoneName());
		if (bean.getMilestoneStatusId() < 1 && bean.getMilestoneStatusId() > 5)
			throw new CommonCustomException("Please select valid status for milestone");
		boBean.setMielstoneType(bean.getMilestoneType());
		boBean.setFkComponentId(bean.getComponentId());
		boBean.setUpdatedBy(bean.getMilestoneUpdatedBy());
	//	if(bean.getEnterLineItem()!=null)
			boBean.setLineItem(bean.getEnterLineItem());
			if(bean.getQuantity()!=null)
				boBean.setQuantity(bean.getQuantity());
			else
				boBean.setQuantity(new BigDecimal(0));		
			if(bean.getUnitPrice()!=null)
				boBean.setUnitPrice(bean.getUnitPrice());
			else
				boBean.setUnitPrice(new BigDecimal(0));
		boBean.setMilestoneEndDate(bean.getMileStoneEndDate());
		boBean.setUpdatedOn(new Date());
		//boBean.setUpdatedComments(updateComments+projectHistoryCommentsPattern+bean.getCommentsForUpdate());
		boBean.setUpdatedComments(bean.getCommentsForUpdate());
		boBean.setMilestoneHistory(updateComments.toString());
		boBean.setFkMilestoneStatusId(bean.getMilestoneStatusId());
		
			
		
		if (bean.getMilestoneStatusId().intValue() != 5) {
			boBean.setMilestoneValue(bean.getMilestoneValue());
			try {
				boBean = milestoneRepository.save(boBean);
			} catch (Exception e) {
				throw new DataAccessException("Something went wrong while creating Milestone.");
			}
		} else {
			boBean.setMilestoneValue(new BigDecimal(0));
			try {
				boBean = milestoneRepository.save(boBean);
			} catch (Exception e) {
				throw new DataAccessException("Something went wrong while creating Milestone.");
			}
			datComponentBO.setComponentAmount(datComponentBO.getComponentAmount().add(bean.getMilestoneValue()));
			try {
				comphonentRepository.save(datComponentBO);
			} catch (Exception e) {
				throw new DataAccessException("Something went wrong while creating Milestone.");
			}
		}
		if(boBean != null) {
			projectNotificationService.sendMilestoneUpdatedNotificationToPC(boBean.getPkMilestoneId(), oldName);
		}
		updateStatus(bean.getComponentId());
		milestoneOutputBean.setMilestoneId(boBean.getPkMilestoneId());
		milestoneOutputBean.setMilestoneName(boBean.getMilestoneName());
		milestoneOutputBean.setMilestoneValue(boBean.getMilestoneValue());
		milestoneOutputBean.setMilestoneType(boBean.getMielstoneType());
		milestoneOutputBean.setComponentId(boBean.getFkComponentId());
		milestoneOutputBean.setComponentName(datComponentBO.getComponentName());
		milestoneOutputBean.setMileStoneEndDate(boBean.getMilestoneEndDate());
		milestoneOutputBean.setMileStonecreatedBy(boBean.getCreatedBy());
		milestoneOutputBean.setEnterLineItem(boBean.getLineItem());
		milestoneOutputBean.setQuantity(boBean.getQuantity());
		milestoneOutputBean.setUnitPrice(boBean.getUnitPrice());
		milestoneOutputBean.setMilestoneStatusId(boBean.getFkMilestoneStatusId());
		milestoneOutputBean.setCommentsForUpdate(boBean.getUpdatedComments());
		milestoneOutputBean.setMileStoneUpdatedBy(boBean.getUpdatedBy());
		milestoneOutputBean.setMileStoneUpdatedOn(boBean.getUpdatedOn());
		return milestoneOutputBean;
	}

	public Page<SearchMilestoneOutputBean> searchMilestoneService(SearchMilestoneBean bean)
			throws CommonCustomException, DataAccessException {
		Page<SearchMilestoneOutputBean> useViewMilestoneDetails=null;
		DatComponentBO compId = comphonentRepository.getcompIdDetails(bean.getComponentId());
		DatWorkorderBO projId = datWorkorderRepository.findByPkWorkorderId(compId.getFkWorkorderId());
		List<ProjDatProjectMappedToProjectCoordinatorBO> coordinatorDetails = projDatProjectMappedToProjectCoordinatorRepository
				.findByFkProjectId(projId.getFkProjectId());

		List<Integer> empIds = new ArrayList<Integer>();

		for (ProjDatProjectMappedToProjectCoordinatorBO bo : coordinatorDetails) {
			empIds.add(bo.getFkProjectCoordinatorId());
		}
		Boolean b = empIds.contains(bean.getViewedBy());
		if (b) 
			useViewMilestoneDetails = useViewMilestoneDetails(bean);
		else
			throw new CommonCustomException("You are not authorized to view Milestone Details");

		return useViewMilestoneDetails;
		}

	public Page<SearchMilestoneOutputBean> useViewMilestoneDetails(SearchMilestoneBean bean)
			throws CommonCustomException, DataAccessException {
		Page<DatMilestoneBO> datMilestoneBOLst = null;
		Page<SearchMilestoneOutputBean> finalPage = null;
		PageRequest pageable = new PageRequest(bean.getPageNumber(), bean.getPageSize(),
				Sort.Direction.DESC, "createdOn");
		List<SearchMilestoneOutputBean> milestoneBean = new ArrayList<SearchMilestoneOutputBean>();
		Specification<DatMilestoneBO> boSpec = null;
		boSpec = MilestoneSpecificDetails.getAllMilestoneDetails(bean);
		if (boSpec == null)
			throw new CommonCustomException("Failed to view milestone details.");
		try {
			datMilestoneBOLst = milestoneRepository.findAll(boSpec, pageable);
		} catch (Exception e) {
			throw new DataAccessException("Failed to view milestone details.");
		}
		for (DatMilestoneBO bo : datMilestoneBOLst) {
			MasMilestoneStatusBO milestoneBO = masMilestonerepository
					.findByPkMilestoneStatusId(bo.getFkMilestoneStatusId());
			SearchMilestoneOutputBean inputBean = new SearchMilestoneOutputBean();
			inputBean.setMilestoneId(bo.getPkMilestoneId());
			inputBean.setMilestoneName(bo.getMilestoneName());
			inputBean.setMilestoneValue(bo.getMilestoneValue());
			inputBean.setMilestoneEndDate(bo.getMilestoneEndDate());
			inputBean.setMilestoneStatus(milestoneBO.getMilestoneStatusName());
			milestoneBean.add(inputBean);
		}
		finalPage = new PageImpl<SearchMilestoneOutputBean>(milestoneBean, pageable, milestoneBean.size());

		return finalPage;
	}

	public List<MilestoneOutputBean> getAllMilestoneDetailsByBuId(int milestoneId) throws DataAccessException {

		List<MilestoneOutputBean> outputBeanList = new ArrayList<MilestoneOutputBean>();
		List<DatMilestoneBO> milestoneBOList;
		DatComponentBO datComponentBO=null;
		try {
			milestoneBOList = milestoneRepository.getAllMilestoneDetailsById(milestoneId);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of milestone Details :  ", e);
		}
		try {
			datComponentBO = comphonentRepository.getcompIdDetails(milestoneBOList.get(0).getFkComponentId());
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of milestone Details :  ", e);
		}
		if (!milestoneBOList.isEmpty()) {
			for (DatMilestoneBO milestoneBO : milestoneBOList) {
				MilestoneOutputBean bean = new MilestoneOutputBean();
				bean.setMilestoneId(milestoneBO.getPkMilestoneId());
				bean.setMilestoneName(milestoneBO.getMilestoneName());
				bean.setMilestoneValue(milestoneBO.getMilestoneValue());
				bean.setMilestoneType(milestoneBO.getMielstoneType());
				bean.setComponentId(milestoneBO.getFkComponentId());
				bean.setComponentName(datComponentBO.getComponentName());
				bean.setMileStoneEndDate(milestoneBO.getMilestoneEndDate());
				bean.setMileStonecreatedBy(milestoneBO.getCreatedBy());
				bean.setEnterLineItem(milestoneBO.getLineItem());
				bean.setQuantity(milestoneBO.getQuantity());
				bean.setUnitPrice(milestoneBO.getUnitPrice());
				bean.setMilestoneStatusId(milestoneBO.getFkMilestoneStatusId());
				bean.setMileStoneUpdatedBy(milestoneBO.getUpdatedBy());
				bean.setMileStoneUpdatedOn(milestoneBO.getUpdatedOn());
				bean.setCommentsForUpdate(milestoneBO.getUpdatedComments());
				bean.setMileStonecreatedBy(milestoneBO.getCreatedBy());
				outputBeanList.add(bean);
			}
		}
		return outputBeanList;
	}

	// Added by Rinta Mariam Jose
	
	public List<MasMilestoneStatusBean> getMilestoneStaus(int type) throws DataAccessException, CommonCustomException {
		
		List<MasMilestoneStatusBean> masMilestoneStatusBeanList = new ArrayList<MasMilestoneStatusBean>();
		List<MasMilestoneStatusBO> masMilestoneStatusBOList;
		List<String> mileStoneStatusNameList;
		
		try
		{
			if(type == 1)
			{
				mileStoneStatusNameList = Arrays.asList(mileStoneStatusType1.split(","));
				masMilestoneStatusBOList = (List<MasMilestoneStatusBO>) masMilestonerepository.findByMilestoneStatusName(mileStoneStatusNameList);
			}
			else if(type == 2)
			{
				mileStoneStatusNameList = Arrays.asList(mileStoneStatusType2.split(","));
				masMilestoneStatusBOList = (List<MasMilestoneStatusBO>) masMilestonerepository.findByMilestoneStatusName(mileStoneStatusNameList);
			}
			else if(type == 3)
			{
				masMilestoneStatusBOList = (List<MasMilestoneStatusBO>) masMilestonerepository.findAll();
			}
			else
				throw new CommonCustomException("Invalid value for type");
		
		}catch(Exception e)
		{
			throw new DataAccessException("Failed to retrieve list of milestone status Details :  "+ e.getMessage());
		}
		if (!masMilestoneStatusBOList.isEmpty()) {
			for (MasMilestoneStatusBO masMilestoneStatusBO : masMilestoneStatusBOList) {
				MasMilestoneStatusBean masMilestoneStatusBean = new MasMilestoneStatusBean();
				masMilestoneStatusBean.setMilestoneStatusId(masMilestoneStatusBO.getPkMilestoneStatusId());
				masMilestoneStatusBean.setMilestoneStatusName(masMilestoneStatusBO.getMilestoneStatusName());
				masMilestoneStatusBeanList.add(masMilestoneStatusBean);
			}
		}
		else
			throw new CommonCustomException("Milestone status Details not found");
		
		return masMilestoneStatusBeanList;
	}

	
	public BigDecimal getOB10value(Integer milestoneId) throws DataAccessException, CommonCustomException {
		DatMilestoneBO datMilestoneBO = null;
		DatComponentBO datComponentBO = null;
		BigDecimal netValue;
		BigDecimal invoiceValue;
		BigDecimal ob10quantity;
		BigDecimal ob10value;
		try {
			datMilestoneBO = milestoneRepository.findByPkMilestoneId(milestoneId);
		} catch (Exception e) {
			throw new DataAccessException("Unable to get the milestone details");
		}
		if (datMilestoneBO == null)
			throw new CommonCustomException("Milestone doesn't exist");
		netValue = datMilestoneBO.getQuantity().multiply(datMilestoneBO.getUnitPrice());
		try {
			datComponentBO = comphonentRepository.findByPkComponentId(datMilestoneBO.getFkComponentId());
		} catch (Exception e) {
			throw new DataAccessException("Unable to get the milestone details");
		}
		if(datComponentBO==null)
			throw new CommonCustomException("Unable to get the milestone details");
		invoiceValue = datMilestoneBO.getMilestoneValue().multiply(datComponentBO.getComponentAmount());
		ob10quantity = (datMilestoneBO.getQuantity().multiply(invoiceValue)).divide(netValue);
		ob10value = ob10quantity.multiply(netValue);
		return ob10value;
	}

	public BigDecimal triggermilestone(TriggerInput triggerInput) throws CommonCustomException, DataAccessException {
		if (triggerInput.getLstTriggerInputDetails().size() > 1) {
			for (TriggerInputDetails triggerInputDetails : triggerInput.getLstTriggerInputDetails()) {
				if (triggerInputDetails.getPurchaseOrderId() == null)
					throw new CommonCustomException("Please select the purchase order");
				if (triggerInputDetails.getMilestoneId() == null)
					throw new CommonCustomException("Please select the milestone");
				if (triggerInputDetails.getTriggerValue().intValue() > 0) {
					saveTriggerDetails(1,triggerInput);
				}
				if (triggerInputDetails.getAdditionalValue().intValue() > 0) {
					saveTriggerDetails(2,triggerInput);
				}
				if (triggerInputDetails.getoB10Value().intValue() > 0) {
					saveTriggerDetails(3,triggerInput);
				}
			}
		} else {
			if (triggerInput.getLstTriggerInputDetails().get(0).getPurchaseOrderId() == null)
				throw new CommonCustomException("Please select the purchase order");
			if (triggerInput.getLstTriggerInputDetails().get(0).getMilestoneId() == null)
				throw new CommonCustomException("Please select the milestone");
			if (triggerInput.getLstTriggerInputDetails().get(0).getTriggerValue().intValue() > 0) 
				saveTriggerDetails(1,triggerInput);
			else
				throw new CommonCustomException("Please add valid amount");
			if (triggerInput.getLstTriggerInputDetails().get(0).getAdditionalValue().intValue() > 0) 
				saveTriggerDetails(2,triggerInput);
			else
				throw new CommonCustomException("Please add valid amount");
			if (triggerInput.getLstTriggerInputDetails().get(0).getoB10Value().intValue() > 0) 
				saveTriggerDetails(3,triggerInput);
			else
				throw new CommonCustomException("Please add valid amount");
		}
		return null;
	}
	
	private void saveTriggerDetails(int triggerType, TriggerInput triggerInput) throws DataAccessException, CommonCustomException {
		switch (triggerType) {/*
		case 1:
			//ProjDatMilestoneTriggerDetailRepository.
			break;
		case 2:
			
			break;
		case 3:
			for (TriggerInputDetails triggerInputDetails : triggerInput.getLstTriggerInputDetails()) {
				DatMilestoneBO datMilestoneBO = null;
				int inputCompareWithMilestoneValue;
				int twentyPercResult;
				int actualValueResultComparison;
				int nintyninepercComparision;
				BigDecimal sumOfTriggeredValue;
				List<Integer> lstOfWoFromDatMapingPOWithWoBo = new ArrayList<Integer>();
				InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrderBO = null;
				BigDecimal sumOfSow = new BigDecimal(0);
				List<ProjDatMilestoneTriggerDetailBO> lstProjDatMilestoneTriggerDetailBO = null;
				try {
					datMilestoneBO = datMilestoneRepository.findByPkMilestoneId(triggerInputDetails.getMilestoneId());
				} catch (Exception e1) {
					throw new DataAccessException("Unable to get milestone details");
				}
				if (datMilestoneBO == null)
					throw new CommonCustomException("milestone doesn't exist");
				actualValueResultComparison = triggerInputDetails.getTriggerValue()
						.compareTo(datMilestoneBO.getMilestoneValue());
				try {
					sumOfTriggeredValue = projDatMilestoneTriggerDetailRepository
							.getSumOfTriggerValueByMilestone(triggerInputDetails.getMilestoneId());
				} catch (Exception e1) {
					throw new DataAccessException("Unable to get milestone details");
				}
				try {
					lstProjDatMilestoneTriggerDetailBO = projDatMilestoneTriggerDetailRepository
							.findByFkMilestoneId(triggerInputDetails.getMilestoneId());
				} catch (Exception e1) {
					throw new DataAccessException("Unable to get milestone details");
				}
				
				if (datMilestoneBO.getFkMilestoneStatusId() == ProjectConstants.MILESTONE_NOT_STARTED) {
					inputCompareWithMilestoneValue = triggerInputDetails.getTriggerValue().compareTo(datMilestoneBO
							.getMilestoneValue().multiply(new BigDecimal(20)).divide(new BigDecimal(100)));
					nintyninepercComparision = triggerInputDetails.getTriggerValue().compareTo(datMilestoneBO
							.getMilestoneValue().multiply(new BigDecimal(99)).divide(new BigDecimal(100)));
					if (actualValueResultComparison > 0)
						throw new CommonCustomException("Trigger value cann't be greater than milestone value");
					if (inputCompareWithMilestoneValue > 0
							&& triggerInputDetails.getFlag().equals(ProjectConstants.YES_FLAG)
							&& actualValueResultComparison < 0) {
						datMilestoneBO.setFkMilestoneStatusId(ProjectConstants.MILESTONE_WORK_IN_PROGRESS);
						if(nintyninepercComparision>0)
							datMilestoneBO.setFkMilestoneStatusId(ProjectConstants.MILESTONE_WORK_IN_COMPLETED);

					}
					twentyPercResult = sumOfTriggeredValue.compareTo(datMilestoneBO.getMilestoneValue()
							.multiply(new BigDecimal(20)).divide(new BigDecimal(100)));
					if (inputCompareWithMilestoneValue <= 0
							&& triggerInputDetails.getFlag().equals(ProjectConstants.NO_FLAG)) {
						if (twentyPercResult >= 0)
							throw new CommonCustomException(
									"Oops! You can trigger only 20% of the milestone value, when your work has not started");
						if(nintyninepercComparision>0)
							throw new CommonCustomException(
									"Oops! You cann't trigger more than 99%");
					}
					if (actualValueResultComparison == 0
							&& triggerInputDetails.getFlag().equals(ProjectConstants.YES_FLAG))
						datMilestoneBO.setFkMilestoneStatusId(ProjectConstants.MILESTONE_WORK_IN_PROGRESS);
				}
				if (datMilestoneBO.getFkMilestoneStatusId() == ProjectConstants.MILESTONE_WORK_IN_PROGRESS) {
					if (actualValueResultComparison == 0
							&& triggerInputDetails.getFlag().equals(ProjectConstants.YES_FLAG))
						datMilestoneBO.setFkMilestoneStatusId(ProjectConstants.MILESTONE_WORK_IN_COMPLETED);
					if (lstProjDatMilestoneTriggerDetailBO != null)
						invoiceDatPurchaseOrderBO = invoiceDatPurchaseOrderRepository
								.findByPkPurchaseOrderId(triggerInputDetails.getPurchaseOrderId());
					try {
						lstOfWoFromDatMapingPOWithWoBo = invoiceDatMappingPoWithWoRepository
								.getByPurchaseOrderId(triggerInputDetails.getPurchaseOrderId());
					} catch (Exception e1) {
						throw new DataAccessException("Unable to get work order details from po and wo map table");
					}
					if (!lstOfWoFromDatMapingPOWithWoBo.isEmpty())
						try {
							sumOfSow = datWorkorderRepository.getSumOfSowAmount(lstOfWoFromDatMapingPOWithWoBo);
						} catch (Exception e1) {
							throw new DataAccessException("Unable to get sum of sow amount from work order");
						}
					BigDecimal checkingPoValue = new BigDecimal(invoiceDatPurchaseOrderBO.getPoValue())
							.subtract(sumOfSow);
					int poValueLeft = triggerInputDetails.getTriggerValue().compareTo(checkingPoValue);
					if (poValueLeft > 0)
						throw new CommonCustomException("PO Balance is not sufficient for triggering");
				}
			}
				ProjDatMilestoneTriggerDetailBO projDatMilestoneTriggerDetailBO=null;
				projDatMilestoneTriggerDetailBO.setTriggerDate(new Date());
				//projDatMilestoneTriggerDetailBO.setTriggerValue(triggerInputDetails.getoB10Value());
				projDatMilestoneTriggerDetailBO.setTriggerBy(triggerInput.getTriggerBy());
				//projDatMilestoneTriggerDetailBO.setFkMilestoneId(triggerInputDetails.getMilestoneId());
				//projDatMilestoneTriggerDetailBO.setTriggerComments(triggerInputDetails.getTriggerComment());
				try {
					projDatMilestoneTriggerDetailRepository.save(projDatMilestoneTriggerDetailBO);
				} catch (Exception e) {
					throw new DataAccessException("Unable to trigger");
				}
			
			break;
		default:
			break;
		*/}
	}

	public List<MilestoneBean> getAllMilestoneByComponentId(Integer compId) throws DataAccessException, CommonCustomException
	{
		List<MilestoneBean> milestoneBeanList = new ArrayList<MilestoneBean>();
		DatComponentBO datComponentBO = null;
		List<DatMilestoneBO> datMilestoneBOList = null;
		try {
			datComponentBO = datComponentRepository.findByPkComponentId(compId);
		} catch (Exception e) {
			throw new DataAccessException("Failed to fetch the details" + e.getMessage());
		}
		if (datComponentBO != null) {
			try {
				datMilestoneBOList = milestoneRepository.findByFkComponentId(compId);
				if (!datMilestoneBOList.isEmpty()) {
					for (DatMilestoneBO datMilestoneBO : datMilestoneBOList) {
						MilestoneBean milestoneBean = new MilestoneBean();
						milestoneBean.setMilestoneId(datMilestoneBO.getPkMilestoneId());
						milestoneBean.setMilestoneName(datMilestoneBO.getMilestoneName());
						milestoneBeanList.add(milestoneBean);
					}
				} else {
					throw new CommonCustomException("No MileStone found for that ComponentId");
				}
			} catch (Exception e) {
				//e.printStackTrace();
				throw new DataAccessException("Failed to fetch the details" + e.getMessage());
			}
		} else {
			throw new CommonCustomException("No such Component Id Exists");
		}
		return milestoneBeanList;

	}
	
	
	private void updateStatus(Integer componentId) throws DataAccessException, CommonCustomException {
		updateComponentStatus(componentId);
	}

	public void updateComponentStatus(Integer componentId) throws DataAccessException, CommonCustomException {
		Byte minMilestoneStatus;
		DatComponentBO datComponentBO=null;
		try {
			datComponentBO = datComponentRepository.findByPkComponentId(componentId);
		} catch (Exception e) {
			throw new DataAccessException("Something went wrong while creating milestone");
		}
		if(datComponentBO == null)
			throw new CommonCustomException("Unable to update component status.");
		try {
			minMilestoneStatus = milestoneRepository.getStatusByComponentId(componentId);
		} catch (Exception e) {
			throw new DataAccessException("Something went wrong while creating milestone");
		}	
		short status = datComponentBO.getFkComponentStatusId();
		System.out.println("......................"+minMilestoneStatus);
		switch (minMilestoneStatus) {
		case 1:
			datComponentBO.setFkComponentStatusId(ProjectConstants.COMPONENT_INFORCE);
			break;
		case 3:
			datComponentBO.setFkComponentStatusId(ProjectConstants.COMPONENT_FUTURE);
			break;
		case 4:
			datComponentBO.setFkComponentStatusId(ProjectConstants.COMPONENT_ONHOLD);
			break;
		case 5:
			BigDecimal sumOfMilestones;
			try {
				sumOfMilestones = milestoneRepository.getSumOfMilestoneValue(componentId);
			} catch (Exception e) {
			//	e.printStackTrace();
				throw new DataAccessException("unable to create mileStone. ");
			}
			if(sumOfMilestones == null)
				sumOfMilestones = new BigDecimal(0);
			
			if (sumOfMilestones.compareTo(datComponentBO.getComponentAmount()) == 0)
				datComponentBO.setFkComponentStatusId(ProjectConstants.COMPONENT_WORK_COMPLETED);
			break;
		case 7:
			datComponentBO.setFkComponentStatusId(ProjectConstants.COMPONENT_DISCARDED);
			break;
		}
		System.out.println(datComponentBO+toString());
		if(status != datComponentBO.getFkComponentStatusId()) {
			try {
				datComponentRepository.save(datComponentBO);
			} catch (Exception e1) {
				throw new DataAccessException("Something went wrong while creating milestone");
			}
		}
		if(datComponentBO.getFkComponentStatusId()==ProjectConstants.COMPONENT_WORK_COMPLETED)
		{
			try{
			projectNotificationService.sendComponentClosureNotificationToAllPC(datComponentBO);
			}catch(Exception e)
			{
				throw new CommonCustomException(e.getMessage());
			}
		}
		
		updateWorkOrderStatus(datComponentBO.getFkWorkorderId());
	}

	public void updateWorkOrderStatus(Integer workOrderId) throws DataAccessException, CommonCustomException {
		Byte minComponentStatus;
		DatWorkorderBO datWorkorderBO=null;
		byte status ;
		String oldStatusName;
		try {
			minComponentStatus = datComponentRepository.getStatusByWorkOrderId(workOrderId);
		} catch (Exception e) {
			throw new DataAccessException("Something went wrong while creating milestone");
		}
		try {
			datWorkorderBO=datWorkorderRepository.findByPkWorkorderId(workOrderId);
		} catch (Exception e) {
			throw new DataAccessException("Something went wrong while creating milestone");
		}
		System.out.println(".......minComponentStatus..............."+minComponentStatus);
		status = datWorkorderBO.getFkWorkorderStatusId();
		oldStatusName = datWorkorderBO.getProjMasWoStatus().getWorkorderStatus();

		switch (minComponentStatus) {
		case 1:
			datWorkorderBO.setFkWorkorderStatusId(ProjectConstants.WO_ACTIVE);
			break;
		case 2:
			datWorkorderBO.setFkWorkorderStatusId(ProjectConstants.WO_FUTURE);
			break;
		case 3:
			datWorkorderBO.setFkWorkorderStatusId(ProjectConstants.WO_ON_HOLD);
			break;
		case 4:
			// Health WOC
			BigDecimal sumWo = new BigDecimal(0);
			try{
				sumWo =	datComponentRepository.getSumOfComponentAmount(workOrderId);
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to create component");
			}
			LOG.info("sumWo"+sumWo);
			LOG.info("datWorkorderBO.getSowAmount()"+datWorkorderBO.getSowAmount());
			
			if(sumWo!=null){
				if((datWorkorderBO.getSowAmount().compareTo(sumWo)) == 0)
					datWorkorderBO.setFkWorkorderStatusId(ProjectConstants.WO_CLOSED);
			}
			
			break;
		case 5:
			// Health WOD
			datWorkorderBO.setFkWorkorderStatusId(ProjectConstants.WO_CLOSED);
			break;
		}
		//saving woStatus in wo table	
		try{
			LOG.info("datWorkorderBO"+datWorkorderBO);
			if(status!=datWorkorderBO.getFkWorkorderStatusId())
			{
				try {
					datWorkorderRepository.save(datWorkorderBO);
				} catch (Exception e) {
					throw new DataAccessException("Something went wrong while creating milestone");
				}
				projectNotificationService.sendWoStatusUpdatedNotificationToAllPC(oldStatusName,status,datWorkorderBO);
				updateProjectStatus(datWorkorderBO.getFkProjectId());		
			}
		}catch(Exception e)
		{
			throw new CommonCustomException(e.getMessage());
		}
	}

	private void updateProjectStatus(int fkProjectId) throws DataAccessException {
		Byte minworkOrderStatus;
		MasProjectBO masProjectBO = null;
		try {
			minworkOrderStatus = datWorkorderRepository.getStatusByProjectId(fkProjectId);
		} catch (Exception e) {
			throw new DataAccessException("Something went wrong while creating milestone");
		}
		try {
			masProjectBO = masProjectRepository.findByPkProjectId(fkProjectId);
		} catch (Exception e) {
			throw new DataAccessException("Something went wrong while creating milestone");
		}
		System.out.println(".......minworkOrderStatus..............."+minworkOrderStatus);
		switch (minworkOrderStatus) {
		case 1:
			masProjectBO.setProjectStatusId(ProjectConstants.PROJECT_ACTIVE);
			break;
		case 2:
			masProjectBO.setProjectStatusId(ProjectConstants.PROJECT_FUTURE);
			break;
		case 3:
			masProjectBO.setProjectStatusId(ProjectConstants.PROJECT_ONHOLD);
			break;
		case 4:
			masProjectBO.setProjectStatusId(ProjectConstants.PROJECT_CLOSED);
			break;
		}
		try {
			masProjectRepository.save(masProjectBO);
		} catch (Exception e) {
			throw new DataAccessException("Something went wrong while creating milestone");
		}
	}
}
