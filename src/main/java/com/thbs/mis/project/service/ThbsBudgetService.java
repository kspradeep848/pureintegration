package com.thbs.mis.project.service;

import io.swagger.models.auth.In;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasEmpDesignationBO;
import com.thbs.mis.common.bo.MasEmpLevelBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.project.bean.BudgetActionInputBean;
import com.thbs.mis.project.bean.BudgetSubmitInputBean;
import com.thbs.mis.project.bean.StandardRateInputBean;
import com.thbs.mis.project.bean.ThbsBudgetCreateInputBean;
import com.thbs.mis.project.bean.ThbsBudgetCreationOutputBean;
import com.thbs.mis.project.bean.ThbsBudgetForAdditionalCostBean;
import com.thbs.mis.project.bean.ThbsBudgetLocationOutputBean;
import com.thbs.mis.project.bean.ThbsBudgetOutputBean;
import com.thbs.mis.project.bo.DatWorkorderBO;
import com.thbs.mis.project.bo.MasProjectBO;
import com.thbs.mis.project.bo.ProjBudgetRejectNotRequiredCommentsDetailBO;
import com.thbs.mis.project.bo.ProjClientBudgetApprovalDetailBO;
import com.thbs.mis.project.bo.ProjDatClientBudgetBO;
import com.thbs.mis.project.bo.ProjDatThbsBudgetBO;
import com.thbs.mis.project.bo.ProjDatThbsBudgetForAdditionalCostBO;
import com.thbs.mis.project.bo.ProjThbsBudgetApprovalDetailBO;
import com.thbs.mis.project.constants.ProjectConstants;
import com.thbs.mis.project.dao.DatWorkorderRepository;
import com.thbs.mis.project.dao.MasProjectRepository;
import com.thbs.mis.project.dao.MasThbsBudgetLocationRepository;
import com.thbs.mis.project.dao.ProjBudgetRejectNotRequiredCommentsDetailRepository;
import com.thbs.mis.project.dao.ProjClientBudgetApprovalDetailRepository;
import com.thbs.mis.project.dao.ProjDatProjectMappedToProjectCoordinatorRepository;
import com.thbs.mis.project.dao.ProjDatThbsBudgetForAdditionalCostRepository;
import com.thbs.mis.project.dao.ProjDatThbsBudgetRepository;
import com.thbs.mis.project.dao.ProjThbsBudgetApprovalDetailRepository;
import com.thbs.mis.project.dao.ProjectDatClientBudgetRepository;
import com.thbs.mis.project.dao.ProjectLevelUserAccessGroupRepository;
import com.thbs.mis.project.dao.StandardRateRepository;

@Service
public class ThbsBudgetService {
	
	@Autowired
	private ProjDatThbsBudgetRepository projDatThbsBudgetRepository;
	
	@Autowired
	private DatWorkorderRepository datWorkorderRepository;
	
	@Autowired
	private ProjectDatClientBudgetRepository projectDatClientBudgetRepository;
	
	@Autowired
	private EmpDetailRepository empDetailRepository;
	
	@Autowired
	private MasThbsBudgetLocationRepository masThbsBudgetLocationRepository;
	
	@Autowired
	private ProjDatProjectMappedToProjectCoordinatorRepository projectMappedToCoordinatorRepos;
	
	@Autowired
	private ProjDatThbsBudgetForAdditionalCostRepository projDatThbsBudgetForAdditionalCostRepository;
	
	@Autowired
	private ProjDatProjectMappedToProjectCoordinatorRepository projDatProjectMappedToProjectCoordinatorRepository;
	
	@Autowired
	private ProjectLevelUserAccessGroupRepository projectLevelUserAccessGroupRepository;
	
	@Autowired
	private  MasProjectRepository  masProjectRepository;
	
	@Autowired
	private StandardRateRepository standardRateRepository;
	
	@Autowired
	private ProjClientBudgetApprovalDetailRepository projClientBudgetApprovalDetailRepository;
	
	@Autowired
	private ProjThbsBudgetApprovalDetailRepository projThbsBudgetApprovalDetailRepository;
	
	@Autowired
	private ProjBudgetRejectNotRequiredCommentsDetailRepository projBudgetRejectNotRequiredCommentsDetailRepository;
	
	@Value("${project.active.emp.status}")
	private byte activeEmpStatus;
	
	@Value("${project.history.pattern}")
	private String pattern;
	
	private static final AppLog LOG = LogFactory
			.getLog(ThbsBudgetService.class);

	//Added by Lalith Kumar
	public ThbsBudgetCreationOutputBean createThbsBudget(ThbsBudgetCreateInputBean inputBean) throws DataAccessException, CommonCustomException
	{
		ProjDatThbsBudgetBO projDatThbsBudgetBO = new ProjDatThbsBudgetBO();
		ProjDatThbsBudgetBO projDatThbsBudgetBOExisted = null;
		ProjThbsBudgetApprovalDetailBO projThbsBudgetApprovalDetailBO;

		ProjDatClientBudgetBO projDatClientBudgetBO;
		DatEmpDetailBO datEmpDetailBO;
		List<Integer> accesslist = new ArrayList<Integer>();
		ThbsBudgetCreationOutputBean thbsBudgetCreationOutputBean = new ThbsBudgetCreationOutputBean();
		MasProjectBO masProjectBO;
		Date today = new Date();
		
		try{
			datEmpDetailBO = empDetailRepository.findByPkEmpId(inputBean.getCreatedBy());
		}catch(Exception e)
		{
			//e.printStackTrace();
			throw new DataAccessException("Unable to create Thbs budget");
		}
		
		if(datEmpDetailBO==null)
			throw new CommonCustomException("Logged In user does not exists...!");
		
		
		if(!datEmpDetailBO.getMasEmpMainStatusBO().getEmpMainStatusName().equalsIgnoreCase(ProjectConstants.ACTIVE))
			throw new CommonCustomException("Logged In Employee is not an Active Employee.");
		
		try{
			projDatClientBudgetBO =	projectDatClientBudgetRepository.findByPkClientBudgetId(inputBean.getFkClientBudgetId());
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to create Thbs budget");
		}
		
		
		if(projDatClientBudgetBO==null)
			throw new CommonCustomException("Enter valid Client budget.");
		
		LOG.info("projDatClientBudgetBO===========>>>"+projDatClientBudgetBO);
		
		try{
			accesslist = projDatProjectMappedToProjectCoordinatorRepository.getAllPcRelatedToProject(projDatClientBudgetBO.getFkWorkOrderId());
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to create Thbs budget");
		}
		
		try{
			masProjectBO = masProjectRepository.getProjectByWoId(projDatClientBudgetBO.getFkWorkOrderId());
		}catch(Exception e){
			throw new DataAccessException("Unable to create Thbs budget");
		}
		
		if(masProjectBO!=null)
		{
			accesslist.add(masProjectBO.getFkDeliveryMgrId());
			accesslist.add(masProjectBO.getFkDomainMgrId());
		}
		
		if(!accesslist.contains(inputBean.getCreatedBy()))
		throw new CommonCustomException("You are not an authorized person to create budget.");
			
		try{
			
			projDatThbsBudgetBOExisted = projDatThbsBudgetRepository.findByBudgetLocationAndFkClientBudgetIdAndFkWorkorderIdAndFkResourceLevelIdAndFkThbsBudgetDesignationIdAndFkThbsBudgetLocationId
					(inputBean.getBudgetLocation(),inputBean.getFkClientBudgetId(),projDatClientBudgetBO.getFkWorkOrderId(),inputBean.getFkResourceLevelId(),inputBean.getFkResourceDesignationId(),inputBean.getFkThbsBudgetLocationId());
			
		}catch(Exception e)
		{
			//e.printStackTrace();
			throw new DataAccessException("Unable to create Thbs budget"); 
		}
		
		if(projDatThbsBudgetBOExisted!=null)
			throw new CommonCustomException("Duplicate record...!");
			
		projDatThbsBudgetBO.setFkClientBudgetId(inputBean.getFkClientBudgetId());

		if(projDatClientBudgetBO.getDatWorkorder()==null)
			throw new CommonCustomException("Enter valid workorder.");
		projDatThbsBudgetBO.setFkWorkorderId(projDatClientBudgetBO.getFkWorkOrderId());

		if(inputBean.getCostToCompany()!=null)
		{
			projDatThbsBudgetBO.setCostToCompany(inputBean.getCostToCompany());
		}
		else
		{
			projDatThbsBudgetBO.setCostToCompany(inputBean.getCostToCompany());
		}
		LOG.startUsecase("Entering ThbsBudgetservice and setting values....!");
		projDatThbsBudgetBO.setBudgetedDayRemaining(inputBean.getBudgetedDays());
		projDatThbsBudgetBO.setBudgetedDays(inputBean.getBudgetedDays());
		projDatThbsBudgetBO.setBudgetLocation(inputBean.getBudgetLocation());
		projDatThbsBudgetBO.setCreatedBy(inputBean.getCreatedBy());
		projDatThbsBudgetBO.setFkThbsBudgetLocationId(inputBean.getFkThbsBudgetLocationId());
		projDatThbsBudgetBO.setNoOfResource(inputBean.getNoOfResource());
		projDatThbsBudgetBO.setNoOfResourceRemaining(inputBean.getNoOfResource());
		projDatThbsBudgetBO.setCreatedOn(today);
		projDatThbsBudgetBO.setFkThbsBudgetDesignationId(inputBean.getFkResourceDesignationId());
		projDatThbsBudgetBO.setFkResourceLevelId(inputBean.getFkResourceLevelId());
		projDatThbsBudgetBO.setFkResourceStandardRateId(inputBean.getStandardRate());

		
		
		LOG.info("All values has been set...!---->>projDatThbsBudgetBO===>"+projDatThbsBudgetBO);
		try{
			projDatThbsBudgetBO=projDatThbsBudgetRepository.save(projDatThbsBudgetBO);
		}catch(Exception e)
		{
			//e.printStackTrace();
			throw new DataAccessException("unable to create Thbs budget");
		}
		
		if(projDatThbsBudgetBO!=null)
		{
			projThbsBudgetApprovalDetailBO = new ProjThbsBudgetApprovalDetailBO();
			projThbsBudgetApprovalDetailBO.setFkBudgetStatusId(ProjectConstants.BUDGET_STATUS_DRAFT);
			projThbsBudgetApprovalDetailBO.setFkThbsBudgetId(projDatThbsBudgetBO.getPkThbsBudgetId());
			projThbsBudgetApprovalDetailBO.setRequestedBy(inputBean.getCreatedBy());
			projThbsBudgetApprovalDetailBO.setRequestedOn(today);
			
			try{
				projThbsBudgetApprovalDetailRepository.save(projThbsBudgetApprovalDetailBO);
			}catch(Exception e)
			{
				// e.printStackTrace();
				throw new DataAccessException("unable to create Thbs budget");
			}
			
		}
		
		thbsBudgetCreationOutputBean.setFkWorkorderId(projDatThbsBudgetBO.getFkWorkorderId());
		return thbsBudgetCreationOutputBean;
	}
	//EOA by Lalith kumar
	
	//Added by Lalith Kumar
	public ThbsBudgetCreationOutputBean updateThbsBudget(ThbsBudgetCreateInputBean inputBean) throws DataAccessException, CommonCustomException
	{
		ProjDatThbsBudgetBO projDatThbsBudgetBO = new ProjDatThbsBudgetBO();
		ProjDatClientBudgetBO projDatClientBudgetBO;
		DatEmpDetailBO datEmpDetailBO;
		List<Integer> accessList; 
		MasProjectBO masProjectBO;
		
		ThbsBudgetCreationOutputBean thbsBudgetCreationOutputBean = new ThbsBudgetCreationOutputBean();
		Date today = new Date();

		if(inputBean.getUpdatedBy()==null)
			throw new CommonCustomException("UpdatedBy field cannot be null");
		
		if(inputBean.getCommentsForUpdate()==null)
			throw new CommonCustomException("Comments for update cannot be null");
		
		
		try{
			datEmpDetailBO = empDetailRepository.findByPkEmpId(inputBean.getCreatedBy());
		}catch(Exception e)
		{
			//e.printStackTrace();
			throw new DataAccessException("Unable to update Thbs budget");
		}
		
		if(datEmpDetailBO==null)
			throw new CommonCustomException("Logged In user does not exists...!");
		
		if(!datEmpDetailBO.getMasEmpMainStatusBO().getEmpMainStatusName().equalsIgnoreCase(ProjectConstants.ACTIVE))
			throw new CommonCustomException("Logged In Employee is not an Active Employee.");
		
		
		LOG.startUsecase("Getting thbs budget details from DB");

		try{
			
			projDatThbsBudgetBO = projDatThbsBudgetRepository.findByPkThbsBudgetId(inputBean.getPkThbsBudgetId());
			
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to update Thbs budget");
		}
		
		if(projDatThbsBudgetBO==null)
		throw new CommonCustomException("No records found with that budget id.");
		
		try{
			projDatClientBudgetBO =	projectDatClientBudgetRepository.findByPkClientBudgetId(inputBean.getFkClientBudgetId());
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to update Thbs budget");
		}
		
		if(projDatClientBudgetBO==null)
			throw new CommonCustomException("Enter valid Client budget.");		
		
		try{
			accessList = projDatProjectMappedToProjectCoordinatorRepository.getAllPcRelatedToProject(projDatClientBudgetBO.getFkWorkOrderId());
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to create Thbs budget");
		}
		
		try{
			masProjectBO = masProjectRepository.getProjectByWoId(projDatClientBudgetBO.getFkWorkOrderId());
		}catch(Exception e){
			throw new DataAccessException("Unable to create Thbs budget");
		}
		
		if(masProjectBO!=null)
		{
			accessList.add(masProjectBO.getFkDeliveryMgrId());
			accessList.add(masProjectBO.getFkDomainMgrId());
		}
		
		if(!accessList.contains(inputBean.getCreatedBy()))
		throw new CommonCustomException("You are not an authorized person to create budget.");
			
		
		LOG.startUsecase("Entering ThbsBudgetservice and setting values....!");

		projDatThbsBudgetBO.setBudgetedDayRemaining(inputBean.getBudgetedDays());
		projDatThbsBudgetBO.setBudgetedDays(inputBean.getBudgetedDays());
		projDatThbsBudgetBO.setBudgetLocation(inputBean.getBudgetLocation());
	//	projDatThbsBudgetBO.setCreatedBy(inputBean.getCreatedBy());
		projDatThbsBudgetBO.setFkThbsBudgetLocationId(inputBean.getFkThbsBudgetLocationId());
		projDatThbsBudgetBO.setNoOfResource(inputBean.getNoOfResource());
		projDatThbsBudgetBO.setNoOfResourceRemaining(inputBean.getNoOfResource());
	//	projDatThbsBudgetBO.setCreatedOn(today);
		projDatThbsBudgetBO.setFkThbsBudgetDesignationId(inputBean.getFkResourceDesignationId());
		projDatThbsBudgetBO.setFkResourceLevelId(inputBean.getFkResourceLevelId());
		projDatThbsBudgetBO.setUpdatedBy(inputBean.getUpdatedBy());
		projDatThbsBudgetBO.setCommentsForUpdation(inputBean.getCommentsForUpdate());
		projDatThbsBudgetBO.setUpdatedOn(today);
		projDatThbsBudgetBO.setFkResourceStandardRateId(inputBean.getStandardRate());
//Standard Rate field should be added
	
		
		projDatThbsBudgetBO.setFkClientBudgetId(inputBean.getFkClientBudgetId());

		if(projDatClientBudgetBO.getDatWorkorder()==null)
			throw new CommonCustomException("Enter valid workorder.");
		projDatThbsBudgetBO.setFkWorkorderId(projDatClientBudgetBO.getFkWorkOrderId());

		if(inputBean.getCostToCompany()!=null)
		{
			projDatThbsBudgetBO.setCostToCompany(inputBean.getCostToCompany());
		}
		else
		{
			projDatThbsBudgetBO.setCostToCompany(inputBean.getCostToCompany());
		}
		
		
		
		LOG.info("All values has been set...!---->>projDatThbsBudgetBO===>"+projDatThbsBudgetBO);
		try{
			projDatThbsBudgetBO=projDatThbsBudgetRepository.save(projDatThbsBudgetBO);
		}catch(Exception e)
		{
			//e.printStackTrace();
			throw new DataAccessException("unable to create Thbs budget");
		}
		
		
		if(projDatThbsBudgetBO!=null)
		{
			ProjThbsBudgetApprovalDetailBO projThbsBudgetApprovalDetailBO = new ProjThbsBudgetApprovalDetailBO();
			
			try{
				projThbsBudgetApprovalDetailBO=	projThbsBudgetApprovalDetailRepository.findByFkThbsBudgetId(inputBean.getPkThbsBudgetId());
			}catch(Exception e)
			{
				// e.printStackTrace();
				throw new DataAccessException("unable to create Thbs budget");
			}
			if(projThbsBudgetApprovalDetailBO!=null)
			{
			projThbsBudgetApprovalDetailBO.setFkBudgetStatusId(ProjectConstants.BUDGET_STATUS_DRAFT);
			projThbsBudgetApprovalDetailBO.setFkThbsBudgetId(projDatThbsBudgetBO.getPkThbsBudgetId());
			projThbsBudgetApprovalDetailBO.setRequestedBy(inputBean.getCreatedBy());
			projThbsBudgetApprovalDetailBO.setRequestedOn(today);
			
			try{
				projThbsBudgetApprovalDetailRepository.save(projThbsBudgetApprovalDetailBO);
			}catch(Exception e)
			{
				// e.printStackTrace();
				throw new DataAccessException("unable to create Thbs budget");
			}
			}
			
		}
		
		thbsBudgetCreationOutputBean.setFkWorkorderId(projDatThbsBudgetBO.getFkWorkorderId());
		return thbsBudgetCreationOutputBean;
	}
	//EOA by Lalith kumar
	
	//Added by Lalith Kumar
	public List<ThbsBudgetLocationOutputBean> getThbsBudgetLocationBasedOnClientBudgetId(Integer clientBudgetId) throws DataAccessException, CommonCustomException
	{
		List<ThbsBudgetLocationOutputBean>  thbsBudgetLocationOutputBeanList = new ArrayList<ThbsBudgetLocationOutputBean>();
		//List<MasThbsBudgetLocationBO>  thbsBudgetLocationOutputBeanList = new ArrayList<MasThbsBudgetLocationBO>();

		ProjDatClientBudgetBO projDatClientBudgetBO;
		
		try{
			projDatClientBudgetBO = projectDatClientBudgetRepository.findByPkClientBudgetId(clientBudgetId);
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to get budget Locations.");
		}
		
	
		if(projDatClientBudgetBO==null)
			throw new CommonCustomException("No records found. Enter valid client budget id.");
		
		try{
			thbsBudgetLocationOutputBeanList = masThbsBudgetLocationRepository.getAllThbsBudgetLocationBasedOnClientBudgetId(clientBudgetId);
		}catch(Exception e)
		{
			//e.printStackTrace();
			throw new DataAccessException("Unable to get budget Locations.");
		}
		
		return thbsBudgetLocationOutputBeanList;
	}
	//EOA by Lalith kumar
	
	//Added by Lalith Kumar
	public ThbsBudgetOutputBean getThbsBudget(Integer budgetId,Integer empId) throws DataAccessException, CommonCustomException
	{
		ThbsBudgetOutputBean thbsBudgetOutputBean = new ThbsBudgetOutputBean();
		ProjDatThbsBudgetBO projDatThbsBudgetBO;
		DatEmpDetailBO datEmpDetailBO;
		MasEmpDesignationBO masEmpDesignationBO;
		DatWorkorderBO datWorkorderBO;
		MasEmpLevelBO masEmpLevelBO;
		List<Integer> accesslist;
		MasProjectBO masProjectBO;
		
		try{
			datEmpDetailBO = empDetailRepository.findByPkEmpId(empId);
		}catch(Exception e)
		{
			throw new DataAccessException("unable to get Thbs budget details");
		}
		
		if(datEmpDetailBO==null)
			throw new CommonCustomException("Logged In user does not exists.");
		

		if(!datEmpDetailBO.getMasEmpMainStatusBO().getEmpMainStatusName().equalsIgnoreCase(ProjectConstants.ACTIVE))
			throw new CommonCustomException("Logged In Employee is not an Active Employee.");
		
		
		try{
			projDatThbsBudgetBO = projDatThbsBudgetRepository.findByPkThbsBudgetId(budgetId);
			
		}catch(Exception e)
		{
			throw new DataAccessException("unable to get Thbs budget details");
		}
		
		if(projDatThbsBudgetBO==null)
			throw new CommonCustomException("No records found with that budget id.");
		
	//Checking whether Logged In user is Authorized person to view budget	
		try{
			accesslist = projDatProjectMappedToProjectCoordinatorRepository.getAllPcRelatedToProject(projDatThbsBudgetBO.getFkWorkorderId());
			
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to create Thbs budget");
		}
		try{
			datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(projDatThbsBudgetBO.getFkWorkorderId());
		}catch(Exception e){
			throw new DataAccessException("Unable to create Thbs budget");
		}
		if(datWorkorderBO==null)
			throw new CommonCustomException("Workorder does not exists.");
		
		masProjectBO = datWorkorderBO.getMasProject();
	
		if(masProjectBO!=null)
		{
			accesslist.add(masProjectBO.getFkDeliveryMgrId());
			accesslist.add(masProjectBO.getFkDomainMgrId());
		}
		
		if(!accesslist.contains(empId))
		throw new CommonCustomException("You are not an authorized person to view budget details.");
	//End Of checking Authorization
		
		//Setting values to output bean
		masEmpDesignationBO = projDatThbsBudgetBO.getMasEmpDesignationBO();
		masEmpLevelBO = projDatThbsBudgetBO.getMasEmpLevelBO();
		
		thbsBudgetOutputBean.setBudgetedDays(projDatThbsBudgetBO.getBudgetedDays());
		thbsBudgetOutputBean.setBudgetLocation(projDatThbsBudgetBO.getBudgetLocation());
		thbsBudgetOutputBean.setCostToCompany(projDatThbsBudgetBO.getCostToCompany());
		thbsBudgetOutputBean.setCreatedBy(projDatThbsBudgetBO.getCreatedBy());
		thbsBudgetOutputBean.setCreatedOn(projDatThbsBudgetBO.getCreatedOn());
		thbsBudgetOutputBean.setFkClientBudgetId(projDatThbsBudgetBO.getFkClientBudgetId());
		thbsBudgetOutputBean.setFkResourceDesignationId(projDatThbsBudgetBO.getFkThbsBudgetDesignationId());
		thbsBudgetOutputBean.setFkResourceLevelId(projDatThbsBudgetBO.getFkResourceLevelId());
		thbsBudgetOutputBean.setFkThbsBudgetLocationId(projDatThbsBudgetBO.getMasThbsBudgetLocationBO().getPkThbsBudgetLocationId());
		thbsBudgetOutputBean.setFkThbsBudgetLocationName(projDatThbsBudgetBO.getMasThbsBudgetLocationBO().getBudgetLocationName());
		thbsBudgetOutputBean.setFkWorkorderId(projDatThbsBudgetBO.getFkWorkorderId());
		thbsBudgetOutputBean.setNoOfResource(projDatThbsBudgetBO.getNoOfResource());
		thbsBudgetOutputBean.setResourceLevelName(masEmpLevelBO.getEmpLevelName());
		thbsBudgetOutputBean.setResourceDesignationName(masEmpDesignationBO.getEmpDesignationName());
		thbsBudgetOutputBean.setPkThbsBudgetId(projDatThbsBudgetBO.getPkThbsBudgetId());
	//Should be set by calling standard rate service			
		thbsBudgetOutputBean.setStandardRate(new BigDecimal(100));
		thbsBudgetOutputBean.setCurrencyId(datWorkorderBO.getMasCurrencytype().getPk_CurrencyType_ID());
		thbsBudgetOutputBean.setCurrencyName(datWorkorderBO.getMasCurrencytype().getCurrencyTypeCode());
		thbsBudgetOutputBean.setStatus("Waiting for Approval");
		

		return thbsBudgetOutputBean;
		
	}
	//EOA by Lalith kumar
	
	//Added by Lalith Kumar
	public List<ThbsBudgetOutputBean> getAllThbsBudgetDetailsByClientBudgetId(Integer clientBudgetId,Integer empId)
			throws DataAccessException,CommonCustomException
	{
		List<ThbsBudgetOutputBean> thbsBudgetOutputBeanList=null;
		List<ProjDatThbsBudgetBO> ProjDatThbsBudgetBOList;
		DatEmpDetailBO datEmpDetailBO;
		ProjDatClientBudgetBO projDatClientBudgetBO;
		DatWorkorderBO datWorkorderBO;
		MasProjectBO masProjectBO;
		List<Integer> accesslist;
		
		
		try{
			datEmpDetailBO = empDetailRepository.findByPkEmpId(empId);
		}catch(Exception e)
		{
			throw new DataAccessException("unable to get Thbs budget details");
		}
		
		if(datEmpDetailBO==null)
			throw new CommonCustomException("Logged In user does not exists.");
		
		if(!datEmpDetailBO.getMasEmpMainStatusBO().getEmpMainStatusName().equalsIgnoreCase(ProjectConstants.ACTIVE))
			throw new CommonCustomException("Logged In Employee is not an Active Employee.");
		
		try{
			projDatClientBudgetBO = projectDatClientBudgetRepository.findByPkClientBudgetId(clientBudgetId);
		}catch(Exception e)
		{
			throw new DataAccessException("unable to get Thbs budget details");
		}
		
		if(projDatClientBudgetBO==null)
			throw new CommonCustomException("Client budget does not exists...!");
		
		try{
			ProjDatThbsBudgetBOList=projDatThbsBudgetRepository.getAllThbsBudgetByClientBudgetId(clientBudgetId);
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to get Thbs budget details");
		}
		
			//Checking whether Logged In user is Authorized person to view budget	
				try{
					accesslist = projectLevelUserAccessGroupRepository.getUserAccessByWorkOrderId(projDatClientBudgetBO.getFkWorkOrderId());
					
				}catch(Exception e)
				{
					throw new DataAccessException("Unable to create Thbs budget");
				}

				
				try{
					datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(projDatClientBudgetBO.getFkWorkOrderId());
				}catch(Exception e){
					throw new DataAccessException("Unable to create Thbs budget");
				}
				if(datWorkorderBO==null)
					throw new CommonCustomException("Workorder does not exists.");
				
				masProjectBO = datWorkorderBO.getMasProject();
				if(masProjectBO!=null)
				{
					accesslist.add(masProjectBO.getFkDeliveryMgrId());
					accesslist.add(masProjectBO.getFkDomainMgrId());
				}
				
				if(!accesslist.contains(empId))
				throw new CommonCustomException("You are not an authorized person to view budget details.");
			//End Of checking Authorization
				
		thbsBudgetOutputBeanList = new ArrayList<ThbsBudgetOutputBean>();

		for(ProjDatThbsBudgetBO projDatThbsBudgetBO:ProjDatThbsBudgetBOList)
		{
			
		ThbsBudgetOutputBean thbsBudgetOutputBean = new ThbsBudgetOutputBean();
		MasEmpDesignationBO masEmpDesignationBO;
		MasEmpLevelBO masEmpLevelBO;
		masEmpDesignationBO = projDatThbsBudgetBO.getMasEmpDesignationBO();
			masEmpLevelBO = projDatThbsBudgetBO.getMasEmpLevelBO();
			
			thbsBudgetOutputBean.setBudgetedDays(projDatThbsBudgetBO.getBudgetedDays());
			thbsBudgetOutputBean.setBudgetLocation(projDatThbsBudgetBO.getBudgetLocation());
			thbsBudgetOutputBean.setCostToCompany(projDatThbsBudgetBO.getCostToCompany());
			thbsBudgetOutputBean.setCreatedBy(projDatThbsBudgetBO.getCreatedBy());
			thbsBudgetOutputBean.setCreatedOn(projDatThbsBudgetBO.getCreatedOn());
			thbsBudgetOutputBean.setFkClientBudgetId(projDatThbsBudgetBO.getFkClientBudgetId());
			thbsBudgetOutputBean.setFkResourceDesignationId(projDatThbsBudgetBO.getFkThbsBudgetDesignationId());
			thbsBudgetOutputBean.setFkResourceLevelId(projDatThbsBudgetBO.getFkResourceLevelId());
			thbsBudgetOutputBean.setFkThbsBudgetLocationId(projDatThbsBudgetBO.getMasThbsBudgetLocationBO().getPkThbsBudgetLocationId());
			thbsBudgetOutputBean.setFkThbsBudgetLocationName(projDatThbsBudgetBO.getMasThbsBudgetLocationBO().getBudgetLocationName());
			thbsBudgetOutputBean.setFkWorkorderId(projDatThbsBudgetBO.getFkWorkorderId());
			thbsBudgetOutputBean.setNoOfResource(projDatThbsBudgetBO.getNoOfResource());
			thbsBudgetOutputBean.setStandardRate(new BigDecimal(100));
			thbsBudgetOutputBean.setResourceLevelName(masEmpLevelBO.getEmpLevelName());
			thbsBudgetOutputBean.setResourceDesignationName(masEmpDesignationBO.getEmpDesignationName());
			thbsBudgetOutputBean.setPkThbsBudgetId(projDatThbsBudgetBO.getPkThbsBudgetId());
			thbsBudgetOutputBean.setCurrencyId(datWorkorderBO.getMasCurrencytype().getPk_CurrencyType_ID());
			thbsBudgetOutputBean.setCurrencyName(datWorkorderBO.getMasCurrencytype().getCurrencyTypeCode());
			thbsBudgetOutputBean.setStatus("Waiting for Approval");
			
			thbsBudgetOutputBeanList.add(thbsBudgetOutputBean);
					
		}
		
		return thbsBudgetOutputBeanList;
	}
	//EOA by Lalith kumar
		
	// Added By Rinta
	/**
	 * <Description Create Thbs Budget For Additional Cost> : Create Thbs Budget
	 * For Additional Cost.
	 * 
	 * @param thbsBudgetForAdditionalCostBean
	 * @return ThbsBudgetCreationOutputBean
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	public List<ThbsBudgetCreationOutputBean> createThbsBudgetForAdditionalCost(
			ThbsBudgetForAdditionalCostBean thbsBudgetForAdditionalCostBean)
			throws DataAccessException, CommonCustomException {
		ProjDatThbsBudgetForAdditionalCostBO projDatThbsBudgetForAdditionalCostBO = null;
		DatEmpDetailBO datEmpDetailBO;
		List<Integer> accessList;
		DatWorkorderBO datWorkorderBO;
		List<ThbsBudgetCreationOutputBean> thbsBudgetCreationOutputBeanList = new ArrayList<ThbsBudgetCreationOutputBean>();

		ThbsBudgetCreationOutputBean thbsBudgetCreationOutputBean = new ThbsBudgetCreationOutputBean();
		LOG.startUsecase("Entering createThbsBudgetForAdditionalCost and setting values....!");

		Integer empId = thbsBudgetForAdditionalCostBean.getCreatedBy();
		Integer woId = thbsBudgetForAdditionalCostBean.getWorkorderId();

		if (empId == null)
			throw new CommonCustomException(
					"Created By is a mandatory field and can't be negative.");
		try {
			datEmpDetailBO = empDetailRepository.findByPkEmpId(empId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException("Unable to create Thbs budget");
		}

		if (datEmpDetailBO == null)
			throw new CommonCustomException(
					"Logged In user does not exists...!");

		if (!datEmpDetailBO.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException(
					"Logged In Employee is not a Active Employee.");

		try {
			datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(woId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Unable to create thbs budget for additional cost.");
		}

		if (datWorkorderBO == null)
			throw new CommonCustomException("Enter valid workorderId.");

		try {
			accessList = projectMappedToCoordinatorRepos
					.getAllPcRelatedToProject(woId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to create thbs budget for additional cost.");
		}

		accessList.add(datWorkorderBO.getMasProject().getFkDeliveryMgrId());
		accessList.add(datWorkorderBO.getMasProject().getFkDomainMgrId());

		if (!accessList.contains(empId))
			throw new CommonCustomException(
					"You are not authorized person to create component");

		try {
			projDatThbsBudgetForAdditionalCostBO = projDatThbsBudgetForAdditionalCostRepository
					.findByFkWorkorderId(woId);
		} catch (Exception e) {
			 // e.printStackTrace();
			throw new DataAccessException(
					"Unable to create thbs budget for additional cost.");
		}

		if (projDatThbsBudgetForAdditionalCostBO != null)
			throw new CommonCustomException(
					"Thbs budget for additional cost is already created!!!");

		projDatThbsBudgetForAdditionalCostBO = new ProjDatThbsBudgetForAdditionalCostBO();

		projDatThbsBudgetForAdditionalCostBO.setFkWorkorderId(woId);
		projDatThbsBudgetForAdditionalCostBO
				.setPurchaseAmt(thbsBudgetForAdditionalCostBean
						.getPurchaseAmt());
		projDatThbsBudgetForAdditionalCostBO
				.setExpenseAmt(thbsBudgetForAdditionalCostBean.getExpenseAmt());
		projDatThbsBudgetForAdditionalCostBO
				.setTravelAmt(thbsBudgetForAdditionalCostBean.getTravelAmt());
		projDatThbsBudgetForAdditionalCostBO.setCreatedBy(empId);
		projDatThbsBudgetForAdditionalCostBO.setCreatedOn(new Date());

		try {
			projDatThbsBudgetForAdditionalCostRepository
					.save(projDatThbsBudgetForAdditionalCostBO);
		} catch (Exception e) {
			 // e.printStackTrace();
			throw new DataAccessException(
					"Unable to create thbs budget for additional cost.");
		}
		thbsBudgetCreationOutputBean.setFkWorkorderId(woId);
		thbsBudgetCreationOutputBeanList.add(thbsBudgetCreationOutputBean);

		LOG.endUsecase("Exiting createThbsBudgetForAdditionalCost....!");
		return thbsBudgetCreationOutputBeanList;

	}

	// EOA By Rinta

	// Added By Rinta
	/**
	 * <Description Update Thbs Budget For Additional Cost> : Update Thbs Budget
	 * For Additional Cost.
	 * 
	 * @param thbsBudgetForAdditionalCostBean
	 * @return ThbsBudgetCreationOutputBean
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */

	public List<ThbsBudgetCreationOutputBean> updateThbsBudgetForAdditionalCost(
			ThbsBudgetForAdditionalCostBean thbsBudgetForAdditionalCostBean)
			throws DataAccessException, CommonCustomException {
		ProjDatThbsBudgetForAdditionalCostBO projDatThbsBudgetForAdditionalCostBO = null;
		DatEmpDetailBO datEmpDetailBO;
		List<Integer> accessList;
		DatWorkorderBO datWorkorderBO;
		List<ThbsBudgetCreationOutputBean> thbsBudgetCreationOutputBeanList = new ArrayList<ThbsBudgetCreationOutputBean>();
		BigDecimal newValue;
		BigDecimal oldValue;
		String comments = new String("");

		ThbsBudgetCreationOutputBean thbsBudgetCreationOutputBean = new ThbsBudgetCreationOutputBean();
		LOG.startUsecase("Entering createThbsBudgetForAdditionalCost and setting values....!");

		Integer empId = thbsBudgetForAdditionalCostBean.getUpdatedBy();
		Integer woId = thbsBudgetForAdditionalCostBean.getWorkorderId();

		if (empId == null)
			throw new CommonCustomException(
					"Updated By is a mandatory field and can't be negative.");

		if (thbsBudgetForAdditionalCostBean.getCommentsForUpdation() == null
				&& thbsBudgetForAdditionalCostBean.getCommentsForUpdation()
						.isEmpty())
			throw new CommonCustomException(
					"Comments for Updation is mandatory.");

		try {
			datEmpDetailBO = empDetailRepository.findByPkEmpId(empId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException("Unable to create Thbs budget");
		}

		if (datEmpDetailBO == null)
			throw new CommonCustomException(
					"Logged In user does not exists...!");

		if (!datEmpDetailBO.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException(
					"Logged In Employee Id is not a Active Employee.");

		try {
			projDatThbsBudgetForAdditionalCostBO = projDatThbsBudgetForAdditionalCostRepository
					.findByPkThbsBudgetAdditionalCostId(thbsBudgetForAdditionalCostBean
							.getBudgetAdditionalCostId());
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Unable to update thbs budget for additional cost.");
		}

		if (projDatThbsBudgetForAdditionalCostBO == null)
			throw new CommonCustomException(
					"Enter valid budgetAdditionalCostId.");

		try {
			datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(woId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to create thbs budget for additional cost.");
		}

		if (datWorkorderBO == null)
			throw new CommonCustomException("Enter valid workorderId.");

		if (!projDatThbsBudgetForAdditionalCostBO.getFkWorkorderId().equals(
				woId))
			throw new CommonCustomException("workorderId can't be changed.");

		try {
			accessList = projectMappedToCoordinatorRepos
					.getAllPcRelatedToProject(woId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to create thbs budget for additional cost.");
		}

		accessList.add(datWorkorderBO.getMasProject().getFkDeliveryMgrId());
		accessList.add(datWorkorderBO.getMasProject().getFkDomainMgrId());

		if (!accessList.contains(empId))
			throw new CommonCustomException(
					"You are not authorized person to update ");

		try {
			projDatThbsBudgetForAdditionalCostBO = projDatThbsBudgetForAdditionalCostRepository
					.findByFkWorkorderId(woId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Unable to create thbs budget for additional cost.");
		}

		if (projDatThbsBudgetForAdditionalCostBO == null)
			throw new CommonCustomException(
					"Thbs budget for additional cost is not created yet!!!");

		newValue = thbsBudgetForAdditionalCostBean.getExpenseAmt();
		oldValue = projDatThbsBudgetForAdditionalCostBO.getExpenseAmt();
		if (oldValue != null && newValue != null) {
			if (oldValue.compareTo(newValue) != 0) {
				System.out.println("E1");
				projDatThbsBudgetForAdditionalCostBO.setExpenseAmt(newValue);
				comments += "Expense amount has been changed from " + oldValue
						+ " to " + newValue + ".";
			}
		} else if (oldValue != null && !oldValue.equals(newValue)) {
			System.out.println("E2");
			projDatThbsBudgetForAdditionalCostBO.setExpenseAmt(newValue);
			comments += "Expense amount has been changed from " + oldValue
					+ " to " + newValue + ". ";
		} else if (newValue != null && !newValue.equals(oldValue)) {
			System.out.println("E3");
			projDatThbsBudgetForAdditionalCostBO.setExpenseAmt(newValue);
			comments += "Expense amount has been changed from " + oldValue
					+ " to " + newValue + ". ";
		}

		newValue = thbsBudgetForAdditionalCostBean.getPurchaseAmt();
		oldValue = projDatThbsBudgetForAdditionalCostBO.getPurchaseAmt();
		if (oldValue != null && newValue != null) {
			if (oldValue.compareTo(newValue) != 0) {
				System.out.println("P1");
				projDatThbsBudgetForAdditionalCostBO.setPurchaseAmt(newValue);
				comments += "Purchase amount has been changed from " + oldValue
						+ " to " + newValue + ". ";
			}
		} else if (oldValue != null && !oldValue.equals(newValue)) {
			System.out.println("P2");
			projDatThbsBudgetForAdditionalCostBO.setPurchaseAmt(newValue);
			comments += "Purchase amount has been changed from " + oldValue
					+ " to " + newValue + ". ";
		} else if (newValue != null && !newValue.equals(oldValue)) {
			System.out.println("P3");
			projDatThbsBudgetForAdditionalCostBO.setPurchaseAmt(newValue);
			comments += "Purchase amount has been changed from " + oldValue
					+ " to " + newValue + ". ";
		}

		newValue = thbsBudgetForAdditionalCostBean.getTravelAmt();
		oldValue = projDatThbsBudgetForAdditionalCostBO.getTravelAmt();
		if (oldValue != null && newValue != null) {
			if (oldValue.compareTo(newValue) != 0) {
				System.out.println("T1");
				projDatThbsBudgetForAdditionalCostBO.setTravelAmt(newValue);
				comments += "Travel amount has been changed from " + oldValue
						+ " to " + newValue + ". ";
			}
		} else if (oldValue != null && !oldValue.equals(newValue)) {
			System.out.println("T2");
			projDatThbsBudgetForAdditionalCostBO.setTravelAmt(newValue);
			comments += "Travel amount has been changed from " + oldValue
					+ " to " + newValue + ". ";
		} else if (newValue != null && !newValue.equals(oldValue)) {
			System.out.println("T3");
			projDatThbsBudgetForAdditionalCostBO.setTravelAmt(newValue);
			comments += "Travel amount has been changed from " + oldValue
					+ " to " + newValue + ". ";
		}

		if (comments.equals(""))
			throw new CommonCustomException("Nothing to update!!!");

		projDatThbsBudgetForAdditionalCostBO.setUpdatedBy(empId);
		projDatThbsBudgetForAdditionalCostBO.setUpdatedOn(new Date());
		projDatThbsBudgetForAdditionalCostBO.setCommentsForUpdation(comments
				+ pattern + " "
				+ thbsBudgetForAdditionalCostBean.getCommentsForUpdation());

		try {
			projDatThbsBudgetForAdditionalCostRepository
					.save(projDatThbsBudgetForAdditionalCostBO);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Unable to create thbs budget for additional cost.");
		}
		thbsBudgetCreationOutputBean.setFkWorkorderId(woId);
		thbsBudgetCreationOutputBeanList.add(thbsBudgetCreationOutputBean);

		LOG.endUsecase("Exiting updateThbsBudgetForAdditionalCost....!");
		return thbsBudgetCreationOutputBeanList;
	}
	// EOA By Rinta


	//Added by Lalith Kumar
	public List<ThbsBudgetOutputBean> getAllThbsBudgetDetailsByWoId(Integer woId,Integer empId)
			throws DataAccessException,CommonCustomException
	{
		List<ThbsBudgetOutputBean> thbsBudgetOutputBeanList=null;
		List<ProjDatThbsBudgetBO> ProjDatThbsBudgetBOList;
		DatEmpDetailBO datEmpDetailBO;
		DatWorkorderBO datWorkorderBO;
		MasProjectBO masProjectBO;
		List<Integer> accesslist;
		
		
		try{
			datEmpDetailBO = empDetailRepository.findByPkEmpId(empId);
		}catch(Exception e)
		{
			throw new DataAccessException("unable to get Thbs budget details");
		}
		
		if(datEmpDetailBO==null)
			throw new CommonCustomException("Logged In user does not exists.");
		
		if(!datEmpDetailBO.getMasEmpMainStatusBO().getEmpMainStatusName().equalsIgnoreCase(ProjectConstants.ACTIVE))
			throw new CommonCustomException("Logged In Employee is not an Active Employee.");
		

			//Checking whether Logged In user is Authorized person to view budget	
				try{
					accesslist = projectLevelUserAccessGroupRepository.getUserAccessByWorkOrderId(woId);
					
				}catch(Exception e)
				{
					throw new DataAccessException("unable to get Thbs budget details");
				}

				try{
					datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(woId);
				}catch(Exception e){
					throw new DataAccessException("unable to get Thbs budget details");
				}
				if(datWorkorderBO==null)
					throw new CommonCustomException("Workorder does not exists.");
				
				masProjectBO = datWorkorderBO.getMasProject();
				if(masProjectBO!=null)
				{
					accesslist.add(masProjectBO.getFkDeliveryMgrId());
					accesslist.add(masProjectBO.getFkDomainMgrId());
				}
				
				if(!accesslist.contains(empId))
				throw new CommonCustomException("You are not an authorized person to view budget details.");
			//End Of checking Authorization
				
				try{
					ProjDatThbsBudgetBOList = (List<ProjDatThbsBudgetBO>) projDatThbsBudgetRepository.findAll();

				}catch(Exception e)
				{
					throw new DataAccessException("unable to get Thbs budget details");
				}
				
		thbsBudgetOutputBeanList = new ArrayList<ThbsBudgetOutputBean>();

		for(ProjDatThbsBudgetBO projDatThbsBudgetBO:ProjDatThbsBudgetBOList)
		{
			
		ThbsBudgetOutputBean thbsBudgetOutputBean = new ThbsBudgetOutputBean();
		MasEmpDesignationBO masEmpDesignationBO;
		MasEmpLevelBO masEmpLevelBO;
		masEmpDesignationBO = projDatThbsBudgetBO.getMasEmpDesignationBO();
			masEmpLevelBO = projDatThbsBudgetBO.getMasEmpLevelBO();
			
			thbsBudgetOutputBean.setBudgetedDays(projDatThbsBudgetBO.getBudgetedDays());
			thbsBudgetOutputBean.setBudgetLocation(projDatThbsBudgetBO.getBudgetLocation());
			thbsBudgetOutputBean.setCostToCompany(projDatThbsBudgetBO.getCostToCompany());
			thbsBudgetOutputBean.setCreatedBy(projDatThbsBudgetBO.getCreatedBy());
			thbsBudgetOutputBean.setCreatedOn(projDatThbsBudgetBO.getCreatedOn());
			thbsBudgetOutputBean.setFkClientBudgetId(projDatThbsBudgetBO.getFkClientBudgetId());
			thbsBudgetOutputBean.setFkResourceDesignationId(projDatThbsBudgetBO.getFkThbsBudgetDesignationId());
			thbsBudgetOutputBean.setFkResourceLevelId(projDatThbsBudgetBO.getFkResourceLevelId());
			thbsBudgetOutputBean.setFkThbsBudgetLocationId(projDatThbsBudgetBO.getMasThbsBudgetLocationBO().getPkThbsBudgetLocationId());
			thbsBudgetOutputBean.setFkThbsBudgetLocationName(projDatThbsBudgetBO.getMasThbsBudgetLocationBO().getBudgetLocationName());
			thbsBudgetOutputBean.setFkWorkorderId(projDatThbsBudgetBO.getFkWorkorderId());
			thbsBudgetOutputBean.setNoOfResource(projDatThbsBudgetBO.getNoOfResource());
			thbsBudgetOutputBean.setStandardRate(new BigDecimal(100));
			thbsBudgetOutputBean.setResourceLevelName(masEmpLevelBO.getEmpLevelName());
			thbsBudgetOutputBean.setResourceDesignationName(masEmpDesignationBO.getEmpDesignationName());
			thbsBudgetOutputBean.setPkThbsBudgetId(projDatThbsBudgetBO.getPkThbsBudgetId());
			thbsBudgetOutputBean.setCurrencyId(datWorkorderBO.getMasCurrencytype().getPk_CurrencyType_ID());
			thbsBudgetOutputBean.setCurrencyName(datWorkorderBO.getMasCurrencytype().getCurrencyTypeCode());
			thbsBudgetOutputBean.setStatus("Waiting for Approval");
			
			thbsBudgetOutputBeanList.add(thbsBudgetOutputBean);
					
		}
		
		return thbsBudgetOutputBeanList;
	}
	//EOA by Lalith kumar
	
	
	public List<BigDecimal> getStandardRate(StandardRateInputBean inputBean,Integer empId) throws DataAccessException, CommonCustomException
	{
		List<BigDecimal> standardRateList =null;
		BigDecimal standardRate = null;
		Set<Integer> accessList;
		
		try{
			accessList = projectLevelUserAccessGroupRepository.getOnlyUserAccessListByWoId(inputBean.getWoId());
		}catch(Exception e)
		{
			throw new DataAccessException("unable to get Standard rate.");
		}
		LOG.info("accessList==========>>"+accessList);
		LOG.info("empId==========>>"+empId);
		LOG.info("accessList.contains(empId)========>>>"+accessList.contains(empId));
		if(accessList.contains(empId))
		{		
			LOG.info("Setting standard rate.");
		try{
			standardRate = standardRateRepository.getStandardRate(inputBean.getEmployeeLevelId(),inputBean.getCureencyId(), inputBean.getThbsLocationId());
		}catch(Exception e)
		{
			// e.printStackTrace();
			throw new DataAccessException("unable to get Standard rate.");
		}
	LOG.info("standardRate---------->>>>>>>"+standardRate);
		
		standardRateList = new ArrayList<BigDecimal>();
		standardRateList.add(standardRate);
		}else
		{
			LOG.info("Employ does not have access to view standard rate");
			standardRateList = new ArrayList<BigDecimal>();
			standardRateList.add(standardRate);
		}
		
		return standardRateList;
	}
		
	@Transactional
	public boolean budgetSubmitForApproval(BudgetSubmitInputBean inputBean) throws DataAccessException, CommonCustomException
	{
		DatEmpDetailBO datEmpDetailBO;
		List<ProjClientBudgetApprovalDetailBO> projClientBudgetApprovalDetailBOList;
		List<ProjThbsBudgetApprovalDetailBO> projThbsBudgetApprovalDetailBOList;
		Date today = new Date();
		long clientBudgetRejectCount = 0;
		long thbsBudgetRejectCount = 0;

		boolean submitted=false;
		
		try{
			datEmpDetailBO = empDetailRepository.findByPkEmpId(inputBean.getRequestedBy());
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to submit budget.");
		}
		
		if(datEmpDetailBO==null)
			throw new CommonCustomException("Logged in user does not exists.");
		
		
		try{
			clientBudgetRejectCount = projClientBudgetApprovalDetailRepository.getCountOfRejectBudgetsByWoId(inputBean.getWoId(),ProjectConstants.BUDGET_STATUS_REJECT);
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to submit budget.");
		}
		
		try{
			thbsBudgetRejectCount = projThbsBudgetApprovalDetailRepository.getCountOfRejectThbsBudgetsByWoId(inputBean.getWoId(),ProjectConstants.BUDGET_STATUS_REJECT);
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to submit budget.");
		}
		
		if(clientBudgetRejectCount==0 && thbsBudgetRejectCount==0)
		{
			try{
				projClientBudgetApprovalDetailBOList = projClientBudgetApprovalDetailRepository.getBudgetsByWoId(inputBean.getWoId(),ProjectConstants.BUDGET_STATUS_DRAFT);
				
			}catch(Exception e)
			{
				// e.printStackTrace();
				throw new DataAccessException("Unable to submit budget.");
			}
			
			for(ProjClientBudgetApprovalDetailBO projClientBudgetApprovalDetailBO:projClientBudgetApprovalDetailBOList)
			{
				projClientBudgetApprovalDetailBO.setRequestedBy(inputBean.getRequestedBy());
				projClientBudgetApprovalDetailBO.setRequestedOn(today);
				projClientBudgetApprovalDetailBO.setFkBudgetStatusId(ProjectConstants.BUDGET_STATUS_WAITING_FOR_APPROVAL);
			}
			try{
				projClientBudgetApprovalDetailRepository.save(projClientBudgetApprovalDetailBOList);
			}catch(Exception e)
			{
				// e.printStackTrace();
				throw new DataAccessException("Unable to submit budget.");
			}
			
			try {
				projThbsBudgetApprovalDetailBOList = projThbsBudgetApprovalDetailRepository.getThbsBudgetsByWoId(inputBean.getWoId(),ProjectConstants.BUDGET_STATUS_DRAFT);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException("Unable to submit budget.");
			}
			
			for(ProjThbsBudgetApprovalDetailBO projThbsBudgetApprovalDetailBO : projThbsBudgetApprovalDetailBOList)
			{
				projThbsBudgetApprovalDetailBO.setRequestedBy(inputBean.getRequestedBy());
				projThbsBudgetApprovalDetailBO.setRequestedOn(today);
				projThbsBudgetApprovalDetailBO.setFkBudgetStatusId(ProjectConstants.BUDGET_STATUS_WAITING_FOR_APPROVAL);
		
			}
			try{
				projThbsBudgetApprovalDetailRepository.save(projThbsBudgetApprovalDetailBOList);
			}catch(Exception e)
			{
				// e.printStackTrace();
				throw new DataAccessException("Unable to submit budget.");
			}
			submitted = true;
		}else
		{
		throw new CommonCustomException("Please verify. Some roles are in rejected state.");
		}
			
		return submitted;
	}

	@Transactional
	public boolean budgetAction(BudgetActionInputBean inputBean) throws DataAccessException, CommonCustomException
	{
		DatWorkorderBO datWorkorderBO;
		List<ProjClientBudgetApprovalDetailBO> projClientBudgetApprovalDetailBOList ;
		List<ProjThbsBudgetApprovalDetailBO> projThbsBudgetApprovalDetailBOList ;
		ProjBudgetRejectNotRequiredCommentsDetailBO commentsBO;
		Date today = new Date();
		boolean approved=false;
		
		try{
			datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(inputBean.getWoId());
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to approve budget.");
		}
		if(datWorkorderBO==null)
			throw new CommonCustomException("Workorder does not exists.");

		if(inputBean.getClientBudgetDetails().isEmpty())
		{
			projClientBudgetApprovalDetailBOList = new ArrayList<ProjClientBudgetApprovalDetailBO>();
			try{
			 projClientBudgetApprovalDetailBOList = projClientBudgetApprovalDetailRepository.getBudgetsByWoId(inputBean.getWoId(),ProjectConstants.BUDGET_STATUS_WAITING_FOR_APPROVAL);
			}catch(Exception e)
			{
				// e.printStackTrace();
				throw new DataAccessException("Unable to approve budget.");
			}
			 for(ProjClientBudgetApprovalDetailBO projClientBudgetApprovalDetailBO : projClientBudgetApprovalDetailBOList )
			 {
				 projClientBudgetApprovalDetailBO.setApprovedOrRejectedBy(inputBean.getApprovedBy());
				 projClientBudgetApprovalDetailBO.setApprovedOrRejectedOn(today);
				 projClientBudgetApprovalDetailBO.setFkBudgetStatusId(inputBean.getStatusId());
				
			 }
			
			 try{
				 projClientBudgetApprovalDetailRepository.save(projClientBudgetApprovalDetailBOList);
			 }catch(Exception e)
			 {
				 // e.printStackTrace();
					throw new DataAccessException("Unable to approve budget.");
			 }
			
		
		}else
		{
			try{
			 projClientBudgetApprovalDetailBOList = projClientBudgetApprovalDetailRepository.getAllClientBudgetsByIdList(inputBean.getClientBudgetDetails());
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to approve budget.");
			}
			 for(ProjClientBudgetApprovalDetailBO projClientBudgetApprovalDetailBO : projClientBudgetApprovalDetailBOList )
			 {
				 projClientBudgetApprovalDetailBO.setApprovedOrRejectedBy(inputBean.getApprovedBy());
				 projClientBudgetApprovalDetailBO.setApprovedOrRejectedOn(today);
				 projClientBudgetApprovalDetailBO.setFkBudgetStatusId(inputBean.getStatusId());
			 }
			
			 try{
				 projClientBudgetApprovalDetailRepository.save(projClientBudgetApprovalDetailBOList);
			 }catch(Exception e)
			 {
				 // e.printStackTrace();
					throw new DataAccessException("Unable to approve budget.");
			 }
			
			
		}
		
		//=========================THBS BUDGET ACTION============================================
		
		if(inputBean.getThbsBudgetDetails().isEmpty())
		{
			projThbsBudgetApprovalDetailBOList = new ArrayList<ProjThbsBudgetApprovalDetailBO>();
			try{
				projThbsBudgetApprovalDetailBOList = projThbsBudgetApprovalDetailRepository.getThbsBudgetsByWoId(inputBean.getWoId(), ProjectConstants.BUDGET_STATUS_WAITING_FOR_APPROVAL);
			}catch(Exception e)
			{
				// e.printStackTrace();
				throw new DataAccessException("Unable to approve budget.");
			}
			 for(ProjThbsBudgetApprovalDetailBO projThbsBudgetApprovalDetailBO : projThbsBudgetApprovalDetailBOList )
			 {
				 projThbsBudgetApprovalDetailBO.setApprovedOrRejectedBy(inputBean.getApprovedBy());
				 projThbsBudgetApprovalDetailBO.setApprovedOrRejectedOn(today);
				 projThbsBudgetApprovalDetailBO.setFkBudgetStatusId(inputBean.getStatusId());
				 
			 }
			
			 try{
				 projThbsBudgetApprovalDetailRepository.save(projThbsBudgetApprovalDetailBOList);
			 }catch(Exception e)
			 {
				 // e.printStackTrace();
					throw new DataAccessException("Unable to approve budget.");
			 }
			
			
		}else
		{
			projThbsBudgetApprovalDetailBOList = new ArrayList<ProjThbsBudgetApprovalDetailBO>();
			try{
				projThbsBudgetApprovalDetailBOList = projThbsBudgetApprovalDetailRepository.getAllThbsBudgetsByIdList(inputBean.getThbsBudgetDetails());
			}catch(Exception e)
			{
				// e.printStackTrace();
				throw new DataAccessException("Unable to approve budget.");
			}
			 for(ProjThbsBudgetApprovalDetailBO projThbsBudgetApprovalDetailBO : projThbsBudgetApprovalDetailBOList )
			 {
				 projThbsBudgetApprovalDetailBO.setApprovedOrRejectedBy(inputBean.getApprovedBy());
				 projThbsBudgetApprovalDetailBO.setApprovedOrRejectedOn(today);
				 projThbsBudgetApprovalDetailBO.setFkBudgetStatusId(inputBean.getStatusId());
			 }
			 
			 try{
				 projThbsBudgetApprovalDetailRepository.save(projThbsBudgetApprovalDetailBOList);
			 }catch(Exception e)
			 {
				 // e.printStackTrace();
					throw new DataAccessException("Unable to approve budget.");
			 }
			
		}
		
		if(inputBean.getReasonForNotRequired()!=null && inputBean.getReasonForReject()!=null)
		{
		 commentsBO = new ProjBudgetRejectNotRequiredCommentsDetailBO();
		 commentsBO.setNotRequiredComment(inputBean.getReasonForNotRequired());
		 commentsBO.setRejectedComments(inputBean.getReasonForReject());
		 commentsBO.setFkWorkorderId(inputBean.getWoId());
		 commentsBO.setEnteredOn(today);
		 commentsBO.setEnteredBy(inputBean.getApprovedBy());
		 
		 try{
			 projBudgetRejectNotRequiredCommentsDetailRepository.save(commentsBO);
		 }catch(Exception e)
		 {
			 // e.printStackTrace();
			 throw new DataAccessException("Unable to approve budget.");
		 }
		}
		 approved = true;
		
		return approved;
	}
	
}
