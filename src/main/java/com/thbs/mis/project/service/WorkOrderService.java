package com.thbs.mis.project.service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.MasCurrencytypeBO;
import com.thbs.mis.common.bo.MasEmpRoleBO;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.MasCurrencyTypeRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.report.Util.DownloadFileUtil;
import com.thbs.mis.notificationframework.gsrnotification.service.GSRNotificationService;
import com.thbs.mis.notificationframework.projectnotification.service.ProjectNotificationService;
import com.thbs.mis.project.bean.DatWorkorderBean;
import com.thbs.mis.project.bean.DocDetails;
import com.thbs.mis.project.bean.ProjMasWoStatusBean;
import com.thbs.mis.project.bean.ProjectHistoryOutputBean;
import com.thbs.mis.project.bean.ProjectNamesOutputBean;
import com.thbs.mis.project.bean.SowFileDownloadInput;
import com.thbs.mis.project.bean.SowFileUploadInput;
import com.thbs.mis.project.bean.SupportDocDetails;
import com.thbs.mis.project.bean.WoNumberOutputBean;
import com.thbs.mis.project.bean.WorkOrderDocOutputBean;
import com.thbs.mis.project.bean.WorkOrderFullViewOutputBean;
import com.thbs.mis.project.bean.WorkOrderInputBean;
import com.thbs.mis.project.bean.WorkOrderOutputBean;
import com.thbs.mis.project.bean.WorkOrderViewInputBean;
import com.thbs.mis.project.bean.WorkOrderViewOutputBean;
import com.thbs.mis.project.bo.DatComponentBO;
import com.thbs.mis.project.bo.DatSowSupportDocsUploadHistoryBO;
import com.thbs.mis.project.bo.DatSowUploadHistoryBO;
import com.thbs.mis.project.bo.DatWorkorderBO;
import com.thbs.mis.project.bo.DatWorkorderInitialBO;
import com.thbs.mis.project.bo.MasProjectBO;
import com.thbs.mis.project.bo.ProjMasClientManagerBO;
import com.thbs.mis.project.bo.ProjMasWoStatusBO;
import com.thbs.mis.project.constants.ProjectConstants;
import com.thbs.mis.project.dao.DatComponentRepository;
import com.thbs.mis.project.dao.DatMappingClientWithTheirMgrRepository;
import com.thbs.mis.project.dao.DatSowSupportDocsUploadHistoryRepository;
import com.thbs.mis.project.dao.DatSowUploadHistoryRepository;
import com.thbs.mis.project.dao.DatWorkorderAuditRepository;
import com.thbs.mis.project.dao.DatWorkorderInitialRepository;
import com.thbs.mis.project.dao.DatWorkorderRepository;
import com.thbs.mis.project.dao.MasProjectRepository;
import com.thbs.mis.project.dao.ProjDatProjectMappedToProjectCoordinatorRepository;
import com.thbs.mis.project.dao.ProjMasClientManagerRepository;
import com.thbs.mis.project.dao.ProjMasWoStatusRepository;
import com.thbs.mis.project.dao.ProjectLevelUserAccessGroupRepository;


@Service
public class WorkOrderService {
	private static final String EMAIL_PATTERN  = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private Pattern pattern;
	private Matcher matcher;
	private static final AppLog LOG = LogFactory.getLog(WorkOrderService.class);
	
	@Value("${sow.upload.directory}")
	String WORKORDER_SOW_UPLOAD_DIRECTORY;
	
	@Value("${workorder.number.digit.length}")
	String WORKORDER_NUMBER_DIGIT_LENGTH;

	@Value("${sow.supportdocs.upload.directory}")
	String WORKORDER_SOW_SUPPORT_UPLOAD_DIRECTORY;
	
	@Value("${project.history.pattern}")
	private String projectHistoryCommentsPattern;
	
	@Autowired
	DatWorkorderRepository datWorkorderRepository;

	@Autowired
	DatWorkorderInitialRepository datWorkorderInitialRepository;
	
	@Autowired
	MasProjectRepository masProjectRepository;

	@Autowired
	private EmpDetailRepository empDetailRepos;

	@Autowired
	ProjMasClientManagerRepository projMasClientManagerRepository;

	@Autowired
	DatMappingClientWithTheirMgrRepository datMappingClientWithTheirMgrRepository;
	
	@Autowired
	ProjMasWoStatusRepository projMasWoStatusRepository;

	@Autowired
	DatComponentRepository datComponentRepository;
	
	@Autowired
	private EmployeePersonalDetailsRepository employeePersonalDetailsRepository;

	@Autowired
	GSRNotificationService gsrNotificationService;

	@Autowired
	DatEmployeeProfessionalRepository datEmployeeProfessionalRepository;

	@Autowired
	DatSowUploadHistoryRepository datSowUploadHistoryRepository;
	
	@Autowired
	ProjectLevelUserAccessGroupRepository projectLevelUserAccessGroupRepository;
	
	@Autowired
	MasCurrencyTypeRepository masCurrencyTypeRepository;

	@Autowired
	DatWorkorderAuditRepository datWorkorderAuditRepository;

	@Autowired
	ProjDatProjectMappedToProjectCoordinatorRepository projDatProjectMappedToProjectCoordinatorRepository;
	
	@Autowired
	private DatSowSupportDocsUploadHistoryRepository datSowSupportDocsUploadHistoryRepository;
	
	@Autowired
	private ProjDatProjectMappedToProjectCoordinatorRepository projectMappedToCoordinatorRepos;
	
	@Autowired
	private ProjectNotificationService projectNotificationService;
	
	List<DatWorkorderBO> datWorkorderBOList;

	@Value("${project.active.emp.status}")
	private byte activeEmpStatus;

	@Value("${delivery.manager.key.roles.names}")
	private String deliverymanagerRolesList;
	
	@Value("${project.coordinator.role.names}")
	private String workorderUserAccesRoles;
	
	@Value("${project.finance.access.roles.names}")
	private String financeTeamAccessRoleList;
	
	@Transactional
	public WorkOrderOutputBean saveWorkOrder(WorkOrderInputBean workOrderInputBean)
			throws   CommonCustomException, DataAccessException {
		Calendar cal = Calendar.getInstance();
		WorkOrderOutputBean workOrderOutputBean = new WorkOrderOutputBean();
		MasProjectBO masProjectBO = null;
		DatEmpDetailBO empDetail;
		MasCurrencytypeBO masCurrencytypeBO=null;
		DatEmpDetailBO projectOwnerDetails;
		DatWorkorderInitialBO datWorkorderInitialBO = new DatWorkorderInitialBO();
		DatWorkorderInitialBO datWorkorderInitialBOExisted = new DatWorkorderInitialBO();
		DatEmpPersonalDetailBO projectOwnerPersonalDetails;
		DatWorkorderBO datWorkorderBO = new DatWorkorderBO();
		ProjMasClientManagerBO projMasClientManagerBO = new ProjMasClientManagerBO();
		ProjMasClientManagerBO isEmailExistInMasClientManagerBO = null;
		Set<Integer> projDatProjectMappedToProjectCoordinatorBO = new HashSet<Integer>();
		try {
			masProjectBO = masProjectRepository.findByPkProjectId(workOrderInputBean.getProjectId());
			LOG.info("masProjectBO....!!!!!"+masProjectBO);
		} catch (Exception e) {
			throw new DataAccessException("Fail to create work order");
		}
		if(masProjectBO==null)
			throw new CommonCustomException("project doesn't exist");
			try {
				projDatProjectMappedToProjectCoordinatorBO = projDatProjectMappedToProjectCoordinatorRepository
						.getByProjectId(workOrderInputBean.getProjectId());
			} catch (Exception e) {
				throw new DataAccessException("Unable to create work order");
			}
			if (projDatProjectMappedToProjectCoordinatorBO.size() > 0) {
				if (!projDatProjectMappedToProjectCoordinatorBO.contains(workOrderInputBean.getWoCreatedBy())) {
					throw new CommonCustomException("You are not the authorized person to create work order");
				}
			} else {
				throw new CommonCustomException("You are not the authorized person to create work order");
			}
		try {
			empDetail = empDetailRepos.findByPkEmpId(workOrderInputBean.getWoCreatedBy());
		} catch (Exception e) {
			throw new DataAccessException("Unable to get Logged in User Id Details");
		}
		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		if (!(empDetail.getFkEmpMainStatus() == activeEmpStatus))
			throw new CommonCustomException("Logged In Employee Id is not a Active Employee.");
		else {
			if (workOrderInputBean.getWoEndDate().before(workOrderInputBean.getWoStartDate())) {
				throw new CommonCustomException("woStartDate should be lesser than woEndDate");
			}
			if (workOrderInputBean.getWoStartDate().before(masProjectBO.getProjectStartDate())) {
				throw new CommonCustomException("woStartDate should be lesser than project start date");
			}
			boolean lengthOfIntial=workOrderInputBean.getWoInitial().length()==3;
			if(!lengthOfIntial){
				throw new CommonCustomException("Wo Initial needs to be 3 character");
			}
			boolean existAlphats=Pattern.matches("[A-Z]+", workOrderInputBean.getWoInitial());
			if(!existAlphats)
				throw new CommonCustomException("Initial needs to be only alphabetics in capital");
			datWorkorderBO.setFkProjectId(workOrderInputBean.getProjectId());
			datWorkorderBO.setIsInternal(workOrderInputBean.getIsInternal());
			datWorkorderBO.setWoEndDate(workOrderInputBean.getWoEndDate());
			datWorkorderBO.setWoStartDate(workOrderInputBean.getWoStartDate());
			int res = workOrderInputBean.getWoEndDate().compareTo(masProjectBO.getProjectEndDate());
			if (!workOrderInputBean.getIsInternal().equals("YES")) {
				if (workOrderInputBean.getSowAmount().intValue() <= 0)
					throw new CommonCustomException("Enter sow value.");
				datWorkorderBO.setSowAmount(workOrderInputBean.getSowAmount());
			} else {
				if (workOrderInputBean.getSowAmount().intValue() > 0
						|| workOrderInputBean.getSowAmount().intValue() < 0)
					throw new CommonCustomException("By default sow value will be zero for internal project.");
				datWorkorderBO.setSowAmount(new BigDecimal("0.00"));
			}
			if (!workOrderInputBean.getIsInternal().equals("YES")) {
				if (workOrderInputBean.getAdditionalAmount().intValue() < 0)
					throw new CommonCustomException("Enter valid additional amount.");
				datWorkorderBO.setAdditionalAmount(workOrderInputBean.getAdditionalAmount());
			} else {
				if (workOrderInputBean.getAdditionalAmount().intValue() > 0
						|| workOrderInputBean.getAdditionalAmount().intValue() < 0)
					throw new CommonCustomException("By default additional value will be zero for internal project.");
				datWorkorderBO.setAdditionalAmount(new BigDecimal("0.00"));
			}
			if (!workOrderInputBean.getIsInternal().equals("YES")) {
				if (workOrderInputBean.getDiscountValue().intValue() < 0)
					throw new CommonCustomException("Enter valid discount amount.");
				if (workOrderInputBean.getDiscountValue().intValue() >= workOrderInputBean.getSowAmount().intValue())
					throw new CommonCustomException("Discount value cann't be more than or equal to sow amount.");
				datWorkorderBO.setWorkorderDiscount(workOrderInputBean.getDiscountValue());
			} else {
				if (workOrderInputBean.getDiscountValue().intValue() > 0
						|| workOrderInputBean.getDiscountValue().intValue() < 0)
					throw new CommonCustomException("By default discount value will be zero for internal project.");
				datWorkorderBO.setWorkorderDiscount(new BigDecimal("0.00"));
			}
			int decideWoStatus = workOrderInputBean.getWoStartDate().compareTo(new Date());
			if (decideWoStatus > 0)
				datWorkorderBO.setFkWorkorderStatusId(ProjectConstants.WO_FUTURE);
			else
				datWorkorderBO.setFkWorkorderStatusId(ProjectConstants.WO_ACTIVE);
			try {
				masCurrencytypeBO=masCurrencyTypeRepository.getByCurrencyId(workOrderInputBean.getSowCurrencyId());
			} catch (Exception e2) {
				throw new DataAccessException("Invalid currency type.");
			}
			if(masCurrencytypeBO==null)
				throw new CommonCustomException("Invalid currency type.");
			datWorkorderBO.setSowCurrencyId(workOrderInputBean.getSowCurrencyId());
			int maxWoId;
			try {
				maxWoId = datWorkorderRepository.getMaxWorkorderId();
			} catch (Exception e1) {
				throw new DataAccessException("Unable to create the work order");
			}
			String workordernumber = workOrderInputBean.getWoInitial()
					+ new SimpleDateFormat("MM").format(cal.getTime())
					+ new SimpleDateFormat("YYYY").format(cal.getTime()) + String.format(WORKORDER_NUMBER_DIGIT_LENGTH, maxWoId+1);
			datWorkorderBO.setWorkorderNumber(workordernumber);
			datWorkorderBO.setSowUploadedFlag("NOT UPLOADED");
			datWorkorderBO.setWorkorderDescription(workOrderInputBean.getWoDescription());
			datWorkorderBO.setWorkorderCreatedOn(new Date());
			datWorkorderBO.setWorkorderCreatedBy(workOrderInputBean.getWoCreatedBy());
			
			try {
			//	projectOwnerDetails = empDetailRepos.findByPkEmpId(workOrderInputBean.getDeliveryManagerId());
				 projectOwnerPersonalDetails = employeePersonalDetailsRepository.findByFkEmpDetailId(workOrderInputBean.getDeliveryManagerId());
				 LOG.info("projectOwnerPersonalDetails..!!!!!!!!!!"+projectOwnerPersonalDetails);
			} catch (Exception e) {
				throw new DataAccessException("Unable to get project owner in Emp Details");
			}
			if(projectOwnerPersonalDetails!=null)
			{
			projectOwnerDetails = projectOwnerPersonalDetails.getDatEmpDetailBO();
			}
			else{
				throw new CommonCustomException("No records found for that Delivery managerId ");
			}
				
			if (projectOwnerDetails == null)
				throw new CommonCustomException("Invalid project owner In Emp Id");
			if (!projectOwnerDetails.getFkEmpMainStatus().equals(activeEmpStatus))
				throw new CommonCustomException("project owner is not a Active Employee.");
			boolean doesRoleExists = Arrays
					.asList(deliverymanagerRolesList.split(","))
					.stream()
					.anyMatch(
							status -> status.equals(projectOwnerDetails.getMasEmpRoleBO()
									.getEmpRoleKeyName()));
			if (!doesRoleExists)
				throw new CommonCustomException(
						"You are not the delivery manager");
			datWorkorderBO.setProjectOwner(workOrderInputBean.getDeliveryManagerId());
			if (workOrderInputBean.getClientManagerId() != null) {
				try {
					projMasClientManagerBO = projMasClientManagerRepository
							.findByPkClientManagerId(workOrderInputBean.getClientManagerId());
				} catch (Exception e1) {
					throw new DataAccessException("Fail to create work order");
				}
				if (projMasClientManagerBO != null) {
					if (projMasClientManagerBO.getEmailAddress() != null) {
						if (!projMasClientManagerBO.getEmailAddress()
								.equals(workOrderInputBean.getClientManagerEmailId())) {
							if (workOrderInputBean.getClientManagerEmailId() != null) {
								try {
									isEmailExistInMasClientManagerBO = projMasClientManagerRepository
											.getByEmailId(workOrderInputBean.getClientManagerEmailId());
								} catch (Exception e) {
									//();
									throw new DataAccessException("Fail to create work order");
								}
								if (isEmailExistInMasClientManagerBO != null)
									throw new CommonCustomException("client manager email already exist");
								pattern = Pattern.compile(EMAIL_PATTERN);
								matcher = pattern.matcher(workOrderInputBean.getClientManagerEmailId());
								if (!matcher.matches())
									throw new CommonCustomException("Invalid email id");
								projMasClientManagerBO.setEmailAddress(workOrderInputBean.getClientManagerEmailId());
							}
							projMasClientManagerBO.setManagerName(workOrderInputBean.getClientManagerName());
							projMasClientManagerBO.setPkClientManagerId(workOrderInputBean.getClientManagerId());
							try {
								projMasClientManagerBO = projMasClientManagerRepository.save(projMasClientManagerBO);
							} catch (Exception e) {
								throw new DataAccessException("Fail to create work order");
							}
						}
						datWorkorderBO.setClientManagerId(workOrderInputBean.getClientManagerId());

					} else {
						if (workOrderInputBean.getClientManagerEmailId() != null) {
							try {
								isEmailExistInMasClientManagerBO = projMasClientManagerRepository
										.getByEmailId(workOrderInputBean.getClientManagerEmailId());
							} catch (Exception e) {
								//();
								throw new DataAccessException("Fail to create work order");
							}
							if (isEmailExistInMasClientManagerBO != null)
								throw new CommonCustomException("client manager email already exist");
							pattern = Pattern.compile(EMAIL_PATTERN);
							matcher = pattern.matcher(workOrderInputBean.getClientManagerEmailId());
							if (!matcher.matches())
								throw new CommonCustomException("Invalid email id");
							projMasClientManagerBO.setEmailAddress(workOrderInputBean.getClientManagerEmailId());
						}
						projMasClientManagerBO.setManagerName(workOrderInputBean.getClientManagerName());
						projMasClientManagerBO.setPkClientManagerId(workOrderInputBean.getClientManagerId());
						try {
							projMasClientManagerBO = projMasClientManagerRepository.save(projMasClientManagerBO);
						} catch (Exception e) {
							throw new DataAccessException("Fail to create work order");
						}
					}
					datWorkorderBO.setClientManagerId(workOrderInputBean.getClientManagerId());
				}
				else{
					throw new CommonCustomException("Client Manager doesn't exist");
				}
			} else {
				if (workOrderInputBean.getClientManagerEmailId() != null) {
					pattern = Pattern.compile(EMAIL_PATTERN);
					matcher = pattern.matcher(workOrderInputBean.getClientManagerEmailId());
					if (!matcher.matches())
						throw new CommonCustomException("Invalid email id");
					projMasClientManagerBO.setEmailAddress(workOrderInputBean.getClientManagerEmailId());
				}
				projMasClientManagerBO.setManagerName(workOrderInputBean.getClientManagerName());
				try {
					isEmailExistInMasClientManagerBO = projMasClientManagerRepository
							.getByEmailId(workOrderInputBean.getClientManagerEmailId());
				} catch (Exception e) {
					//();
					throw new DataAccessException("Fail to create work order");
				}
				if (isEmailExistInMasClientManagerBO == null)
					projMasClientManagerBO = projMasClientManagerRepository.save(projMasClientManagerBO);
				else
					throw new CommonCustomException("client manager email already exist");
				datWorkorderBO.setClientManagerId(projMasClientManagerBO.getPkClientManagerId());
			}
			
			try {
				datWorkorderBO = datWorkorderRepository.save(datWorkorderBO);
			} catch (Exception e) {
				//e.printStackTrace();
				throw new DataAccessException("Unable to create the work order");
			}
			if (res > 0) {
				masProjectBO.setProjectEndDate(workOrderInputBean.getWoEndDate());
				try {
					masProjectRepository.save(masProjectBO);
				} catch (Exception e1) {
					throw new DataAccessException("Fail to create work order");
				}
			}
			if(datWorkorderBO!=null)
			{
				try{
					datWorkorderInitialBOExisted = datWorkorderInitialRepository
							.findByWoInitialAndFkProjectId(workOrderInputBean.getWoInitial(),workOrderInputBean.getProjectId());
				}catch(Exception e)
				{
					throw new DataAccessException("Something went wrong while creating workOrder Initial");
				}
			if(datWorkorderInitialBOExisted == null)
				{
			datWorkorderInitialBO.setWoInitial(workOrderInputBean.getWoInitial());
			datWorkorderInitialBO.setFkProjectId(workOrderInputBean.getProjectId());
			workOrderOutputBean.setWoInitial(workOrderInputBean.getWoInitial());
			try{
				datWorkorderInitialBO = datWorkorderInitialRepository.save(datWorkorderInitialBO);
			}catch(Exception e)
			{
				throw new DataAccessException("Something went wrong while creating workOrder Initial. ");
			}
			}
			else
				workOrderOutputBean.setWoInitial(datWorkorderInitialBOExisted.getWoInitial());
			}
			LOG.info("datWorkorderBO.....!!!!!!"+datWorkorderBO);
			ProjMasWoStatusBO projMasWoStatusBO = projMasWoStatusRepository.findBypkWorkorderStatusId(datWorkorderBO.getFkWorkorderStatusId());
				LOG.info("projMasWoStatusBO.....!!!!!!"+projMasWoStatusBO);
				
				try{
					projectNotificationService.sendWorkorderCreatedNotificationToAllPC(datWorkorderBO);
				}catch(Exception e)
				{
					throw new CommonCustomException(e.getMessage());
				}
				try {
					if(datWorkorderBO.getCommentForUpdate()!=null){
						List<String> comments = Arrays.asList(datWorkorderBO.getCommentForUpdate().split(projectHistoryCommentsPattern));
						workOrderOutputBean.setCommentForUpdate(comments.get(comments.size()-1));
						}
					workOrderOutputBean.setWoId(datWorkorderBO.getPkWorkorderId());
					workOrderOutputBean.setAdditionalAmount(datWorkorderBO.getAdditionalAmount());
					workOrderOutputBean.setClientManagerEmailId(workOrderInputBean.getClientManagerEmailId());
					workOrderOutputBean.setClientManagerId(datWorkorderBO.getClientManagerId());
					workOrderOutputBean.setClientManagerName(workOrderInputBean.getClientManagerName());
					workOrderOutputBean.setDeliveryManagerId(workOrderInputBean.getDeliveryManagerId());
					workOrderOutputBean.setDeliveryManagerName(gsrNotificationService.getEmployeeNameByEmpId(workOrderInputBean.getDeliveryManagerId()));
					workOrderOutputBean.setDiscountValue(datWorkorderBO.getWorkorderDiscount());
					workOrderOutputBean.setProjectId(datWorkorderBO.getFkProjectId());
					workOrderOutputBean.setProjectName(masProjectBO.getProjectName());
					workOrderOutputBean.setSowAmount(datWorkorderBO.getSowAmount());
					workOrderOutputBean.setSowCurrencyId(datWorkorderBO.getSowCurrencyId());
					workOrderOutputBean.setSowCurrencyName(masCurrencytypeBO.getCurrencyTypeCode());
					workOrderOutputBean.setWoCreatedBy(datWorkorderBO.getWorkorderCreatedBy());
					workOrderOutputBean.setWoDescription(datWorkorderBO.getWorkorderDescription());
					workOrderOutputBean.setWoEndDate(datWorkorderBO.getWoEndDate());
				//	workOrderOutputBean.setWoInitial(datWorkorderInitialBOExisted.getWoInitial());
					workOrderOutputBean.setWoStartDate(datWorkorderBO.getWoStartDate());
					workOrderOutputBean.setWoStatusId(datWorkorderBO.getFkWorkorderStatusId());
					workOrderOutputBean.setWoStatusName(projMasWoStatusBO.getWorkorderStatus());
					workOrderOutputBean.setWoType(datWorkorderBO.getIsInternal());
					workOrderOutputBean.setWoNumber(datWorkorderBO.getWorkorderNumber());
					workOrderOutputBean.setWoBal(datWorkorderBO.getSowAmount());
				} catch (Exception e) {
					LOG.info("final output........!!!!"+workOrderOutputBean.toString());
				}
			
			return workOrderOutputBean;
		}
	}

	@Transactional
	public WorkOrderOutputBean updateWorkOrder(WorkOrderInputBean workOrderInputBean)
			throws CommonCustomException, DataAccessException {
		WorkOrderOutputBean workOrderOutputBean = new WorkOrderOutputBean();
		MasProjectBO masProjectBO = null;
		MasCurrencytypeBO masCurrencytypeBO=null;
		MasCurrencytypeBO masCurrencytypeBO1=null;
		DatEmpDetailBO empDetail;
		DatEmpDetailBO projectOwnerDetails;
		DatWorkorderBO datWorkorderBO = new DatWorkorderBO();
		ProjMasClientManagerBO projMasClientManagerBO = new ProjMasClientManagerBO();
		ProjMasClientManagerBO isEmailExistInMasClientManagerBO = new ProjMasClientManagerBO();
		Set<Integer> projDatProjectMappedToProjectCoordinatorBO = new HashSet<Integer>();
		try {
			masProjectBO = masProjectRepository.findByPkProjectId(workOrderInputBean.getProjectId());
		} catch (Exception e) {
			throw new DataAccessException("Fail to update work order");
		}
		if (masProjectBO == null)
			throw new CommonCustomException("project doesn't exist");
		if (workOrderInputBean.getWoId() == null)
			throw new CommonCustomException("work order doesn't exist");
		try {
			projDatProjectMappedToProjectCoordinatorBO = projDatProjectMappedToProjectCoordinatorRepository
					.getByProjectId(workOrderInputBean.getProjectId());
		} catch (Exception e) {
			throw new DataAccessException("Unable to update work order");
		}
		if (projDatProjectMappedToProjectCoordinatorBO.size() > 0) {
			if (!projDatProjectMappedToProjectCoordinatorBO.contains(workOrderInputBean.getWoUpdatedBy())) {
				throw new CommonCustomException("You are not the authorized person to update project");
			}
		} else {
			throw new CommonCustomException("You are not the authorized person to update project");
		}
		try {
			empDetail = empDetailRepos.findByPkEmpId(workOrderInputBean.getWoUpdatedBy());
			datWorkorderBO.setWorkorderUpdatedBy(workOrderInputBean.getWoUpdatedBy());
		} catch (Exception e) {
			throw new DataAccessException("Unable to get Logged in User Id Details");
		}
		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException("Logged In Employee Id is not a Active Employee.");
		if (workOrderInputBean.getCommentForUpdate() == null)
			throw new CommonCustomException("Please enter the comment");
		else {
			if (workOrderInputBean.getWoEndDate().before(workOrderInputBean.getWoStartDate())) {
				throw new CommonCustomException("woEndDate should be greater than woStartDate");
			}
			if ((workOrderInputBean.getWoId() != null) && (workOrderInputBean.getProjectId() != null)) {
				try {
					datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(workOrderInputBean.getWoId());
				} catch (Exception e2) {
					throw new DataAccessException("work order is not found");
				}
				StringBuffer updatedComment=updateComment(datWorkorderBO,workOrderInputBean);
				if (datWorkorderBO == null)
					throw new CommonCustomException("work order is not found");
				int res = workOrderInputBean.getWoEndDate().compareTo(masProjectBO.getProjectEndDate());
				if (!datWorkorderBO.getWorkorderNumber().equals(workOrderInputBean.getWoNumber()))
					throw new CommonCustomException("Can't change work order number");
				if(!datWorkorderBO.getWorkorderNumber().substring(0,3).equals(workOrderInputBean.getWoInitial()))
					throw new CommonCustomException("Can't change work order initial");
				
				//Getting only Date for checking start date of wo
	        	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	        	String formattedNewStartDate = formatter.format(workOrderInputBean.getWoStartDate());
	        	String formattedOldStartDate = formatter.format(datWorkorderBO.getWoStartDate());
	       			
				if((workOrderInputBean.getWoStartDate().compareTo(new Date())<=0) && 
						!(formattedNewStartDate.equals(formattedOldStartDate)))
				throw new CommonCustomException("Can't change work start date");
				datWorkorderBO.setFkProjectId(workOrderInputBean.getProjectId());
				datWorkorderBO.setWoEndDate(workOrderInputBean.getWoEndDate());
				datWorkorderBO.setWoStartDate(workOrderInputBean.getWoStartDate());
				if (!workOrderInputBean.getIsInternal().equals("YES")) {
					if (workOrderInputBean.getSowAmount().intValue() <= 0)
						throw new CommonCustomException("Enter sow value.");
					datWorkorderBO.setSowAmount(workOrderInputBean.getSowAmount());
				} else {
					if (workOrderInputBean.getSowAmount().intValue() > 0
							|| workOrderInputBean.getSowAmount().intValue() < 0)
						throw new CommonCustomException("By default sow value will be zero for internal project.");
					datWorkorderBO.setSowAmount(new BigDecimal("0.00"));
				}
				if (!workOrderInputBean.getIsInternal().equals("YES")) {
					if (workOrderInputBean.getAdditionalAmount().intValue() < 0)
						throw new CommonCustomException("Enter valid additional amount.");
					datWorkorderBO.setAdditionalAmount(workOrderInputBean.getAdditionalAmount());
				} else {
					if (workOrderInputBean.getAdditionalAmount().intValue() > 0
							|| workOrderInputBean.getAdditionalAmount().intValue() < 0)
						throw new CommonCustomException(
								"By default additional value will be zero for internal project.");
					datWorkorderBO.setAdditionalAmount(new BigDecimal("0.00"));
				}
				if (!workOrderInputBean.getIsInternal().equals("YES")) {
					if (workOrderInputBean.getDiscountValue().intValue() < 0)
						throw new CommonCustomException("Enter valid discount amount.");
					if (workOrderInputBean.getDiscountValue().intValue() >= workOrderInputBean.getSowAmount().intValue())
						throw new CommonCustomException("Discount value cann't be more than or equal to sow amount.");
					datWorkorderBO.setWorkorderDiscount(workOrderInputBean.getDiscountValue());
				} else {
					if (workOrderInputBean.getDiscountValue().intValue() > 0
							|| workOrderInputBean.getDiscountValue().intValue() < 0)
						throw new CommonCustomException("By default discount value will be zero for internal project.");
					datWorkorderBO.setWorkorderDiscount(new BigDecimal("0.00"));
				}
				int decideWoStatus = workOrderInputBean.getWoStartDate().compareTo(new Date());
				if (decideWoStatus > 0)
					datWorkorderBO.setFkWorkorderStatusId(ProjectConstants.WO_FUTURE);
				else
					datWorkorderBO.setFkWorkorderStatusId(ProjectConstants.WO_ACTIVE);
				try {
					masCurrencytypeBO = masCurrencyTypeRepository
							.getByCurrencyId(workOrderInputBean.getSowCurrencyId());
				} catch (Exception e2) {
					throw new DataAccessException("Invalid currency type.");
				}
				if (masCurrencytypeBO == null)
					throw new CommonCustomException("Invalid currency type.");
				try {
					masCurrencytypeBO1 = masCurrencyTypeRepository
							.getByCurrencyId(datWorkorderBO.getSowCurrencyId());
				} catch (Exception e2) {
					throw new DataAccessException("Invalid currency type.");
				}
				if (datWorkorderBO.getSowCurrencyId() != workOrderInputBean.getSowCurrencyId())
					updatedComment.append("currency type" + " from " + masCurrencytypeBO1.getCurrencyTypeCode() + " to "
							+ masCurrencytypeBO.getCurrencyTypeCode()+",");
				
				datWorkorderBO.setSowCurrencyId(workOrderInputBean.getSowCurrencyId());
				datWorkorderBO.setWorkorderUpdatedOn(new Date());
				try {
					projectOwnerDetails = empDetailRepos.findByPkEmpId(workOrderInputBean.getDeliveryManagerId());
				} catch (Exception e) {
					throw new DataAccessException("Unable to get project owner in Emp Details");
				}
				if (projectOwnerDetails == null)
					throw new CommonCustomException("Invalid project owner In Emp Id");
				if (!projectOwnerDetails.getFkEmpMainStatus().equals(activeEmpStatus))
					throw new CommonCustomException("project owner is not a Active Employee.");
				boolean doesRoleExists = Arrays.asList(deliverymanagerRolesList.split(",")).stream()
						.anyMatch(status -> status.equals(projectOwnerDetails.getMasEmpRoleBO().getEmpRoleKeyName()));
				if (!doesRoleExists)
					throw new CommonCustomException("You are not the delivery manager");
				datWorkorderBO.setProjectOwner(workOrderInputBean.getDeliveryManagerId());
				datWorkorderBO.setWorkorderUpdatedBy(workOrderInputBean.getWoUpdatedBy());
				datWorkorderBO.setIsInternal(workOrderInputBean.getIsInternal());
				if (workOrderInputBean.getClientManagerId() != null) {
					try {
						projMasClientManagerBO = projMasClientManagerRepository
								.findByPkClientManagerId(workOrderInputBean.getClientManagerId());
					} catch (Exception e1) {
						throw new DataAccessException("Fail to update work order");
					}
					
					if (projMasClientManagerBO != null) {
						if(!workOrderInputBean.getClientManagerName().equals(projMasClientManagerBO.getManagerName())){
							updatedComment.append(
									"client manager name " + "from " + projMasClientManagerBO.getManagerName() + " to "
											+ workOrderInputBean.getClientManagerName());
						}
						if (projMasClientManagerBO.getEmailAddress() != null) {
							if (!projMasClientManagerBO.getEmailAddress()
									.equals(workOrderInputBean.getClientManagerEmailId())) {
								if (workOrderInputBean.getClientManagerEmailId() != null) {
									updatedComment.append(
											" client email id " + "from " + projMasClientManagerBO.getEmailAddress() + " to "
													+ workOrderInputBean.getClientManagerEmailId() +" ,");									
									try {
										isEmailExistInMasClientManagerBO = projMasClientManagerRepository
												.getByEmailId(workOrderInputBean.getClientManagerEmailId());
									} catch (Exception e) {
										throw new DataAccessException("Fail to update work order");
									}
//									if (isEmailExistInMasClientManagerBO != null)
//										throw new CommonCustomException("client manager email already exist");
									pattern = Pattern.compile(EMAIL_PATTERN);
									matcher = pattern.matcher(workOrderInputBean.getClientManagerEmailId());
									if (!matcher.matches())
										throw new CommonCustomException("Invalid email id");
									projMasClientManagerBO
											.setEmailAddress(workOrderInputBean.getClientManagerEmailId());
								}
								projMasClientManagerBO.setManagerName(workOrderInputBean.getClientManagerName());
								projMasClientManagerBO.setPkClientManagerId(workOrderInputBean.getClientManagerId());
								try {
									projMasClientManagerBO = projMasClientManagerRepository
											.save(projMasClientManagerBO);
								} catch (Exception e) {
									throw new DataAccessException("Fail to update work order");
								}
							}
						} else {
							if (workOrderInputBean.getClientManagerEmailId() != null) {
								updatedComment.append(" client email id " + workOrderInputBean.getClientManagerEmailId() + ",");
								try {
									isEmailExistInMasClientManagerBO = projMasClientManagerRepository
											.getByEmailId(workOrderInputBean.getClientManagerEmailId());
								} catch (Exception e) {
									throw new DataAccessException("Fail to update work order");
								}
								if (isEmailExistInMasClientManagerBO != null)
									throw new CommonCustomException("client manager email already exist");
								pattern = Pattern.compile(EMAIL_PATTERN);
								matcher = pattern.matcher(workOrderInputBean.getClientManagerEmailId());
								if (!matcher.matches())
									throw new CommonCustomException("Invalid email id");
								projMasClientManagerBO.setEmailAddress(workOrderInputBean.getClientManagerEmailId());
							}
							projMasClientManagerBO.setManagerName(workOrderInputBean.getClientManagerName());
							projMasClientManagerBO.setPkClientManagerId(workOrderInputBean.getClientManagerId());
							try {
								projMasClientManagerBO = projMasClientManagerRepository.save(projMasClientManagerBO);
							} catch (Exception e) {
								throw new DataAccessException("Fail to update work order");
							}
						}
						datWorkorderBO.setClientManagerId(workOrderInputBean.getClientManagerId());

					} else {
						throw new CommonCustomException("Client doesn't exist");
					}
				} else {
					projMasClientManagerBO.setManagerName(workOrderInputBean.getClientManagerName());
					updatedComment.append(" client manager " + workOrderInputBean.getClientManagerName()+",");
					try {
						isEmailExistInMasClientManagerBO = projMasClientManagerRepository
								.getByEmailId(workOrderInputBean.getClientManagerEmailId());
					} catch (Exception e) {
						throw new DataAccessException("Fail to update work order");
					}
					if (workOrderInputBean.getClientManagerEmailId() != null) {
						pattern = Pattern.compile(EMAIL_PATTERN);
						matcher = pattern.matcher(workOrderInputBean.getClientManagerEmailId());
						if (!matcher.matches())
							throw new CommonCustomException("Invalid email id");
						projMasClientManagerBO.setEmailAddress(workOrderInputBean.getClientManagerEmailId());
					}
					if (isEmailExistInMasClientManagerBO == null)
						projMasClientManagerBO = projMasClientManagerRepository.save(projMasClientManagerBO);
				else
						throw new CommonCustomException("client manager email already exist");
					datWorkorderBO.setClientManagerId(projMasClientManagerBO.getPkClientManagerId());
				}
				datWorkorderBO.setWorkorderDescription(workOrderInputBean.getWoDescription());

				datWorkorderBO.setCommentForUpdate(workOrderInputBean.getCommentForUpdate());
				if(updatedComment!=null)
					datWorkorderBO.setWoHistory(new String(updatedComment));
				else
					throw new CommonCustomException("Did not modify any field......!");
				try {
					datWorkorderBO = datWorkorderRepository.save(datWorkorderBO);
				} catch (Exception e) {
					throw new DataAccessException("Failed to update work order");
				}
				if (res > 0) {
					masProjectBO.setProjectEndDate(workOrderInputBean.getWoEndDate());
					try {
						masProjectRepository.save(masProjectBO);
					} catch (Exception e1) {
						throw new DataAccessException("Fail to update work order");
					}
					
				}
				try{
				projectNotificationService.sendWorkorderUpdatedNotificationToAllPC(datWorkorderBO);
				}catch(Exception e)
				{
					throw new CommonCustomException("Unable to send notification");
				}
				
				ProjMasWoStatusBO projMasWoStatusBO = datWorkorderBO.getProjMasWoStatus();

				workOrderOutputBean.setWoId(datWorkorderBO.getPkWorkorderId());
				workOrderOutputBean.setAdditionalAmount(datWorkorderBO.getAdditionalAmount());
				workOrderOutputBean.setClientManagerEmailId(workOrderInputBean.getClientManagerEmailId());
				workOrderOutputBean.setClientManagerId(datWorkorderBO.getClientManagerId());
				workOrderOutputBean.setClientManagerName(workOrderInputBean.getClientManagerName());
				if(datWorkorderBO.getCommentForUpdate()!=null){
					List<String> comments = Arrays.asList(datWorkorderBO.getCommentForUpdate().split(projectHistoryCommentsPattern));
					workOrderOutputBean.setCommentForUpdate(comments.get(comments.size()-1));
					}workOrderOutputBean.setDeliveryManagerId(workOrderInputBean.getDeliveryManagerId());
				workOrderOutputBean.setDeliveryManagerName(
						gsrNotificationService.getEmployeeNameByEmpId(workOrderInputBean.getDeliveryManagerId()));
				workOrderOutputBean.setDiscountValue(datWorkorderBO.getWorkorderDiscount());
				workOrderOutputBean.setProjectId(datWorkorderBO.getFkProjectId());
				workOrderOutputBean.setProjectName(masProjectBO.getProjectName());
				workOrderOutputBean.setSowAmount(datWorkorderBO.getSowAmount());
				workOrderOutputBean.setSowCurrencyId(datWorkorderBO.getSowCurrencyId());
				workOrderOutputBean.setSowCurrencyName(masCurrencytypeBO.getCurrencyTypeCode());
				workOrderOutputBean.setWoCreatedBy(datWorkorderBO.getWorkorderCreatedBy());
				workOrderOutputBean.setWoDescription(datWorkorderBO.getWorkorderDescription());
				workOrderOutputBean.setWoEndDate(datWorkorderBO.getWoEndDate());
				workOrderOutputBean.setWoInitial(datWorkorderBO.getWorkorderNumber().substring(0, 3));
				workOrderOutputBean.setWoNumber(datWorkorderBO.getWorkorderNumber());
				workOrderOutputBean.setWoStartDate(datWorkorderBO.getWoStartDate());
				workOrderOutputBean.setWoStatusId(datWorkorderBO.getFkWorkorderStatusId());
				workOrderOutputBean.setWoStatusName(projMasWoStatusBO.getWorkorderStatus());
			} else {
				throw new CommonCustomException("Failed to update work order");
			}
		}
		return workOrderOutputBean;
	}

	private StringBuffer updateComment(DatWorkorderBO datWorkorderBO, WorkOrderInputBean workOrderInputBean) {
		StringBuffer comment =new StringBuffer();
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String startDate=simpleDateFormat.format(workOrderInputBean.getWoStartDate());
		String endDate=simpleDateFormat.format(workOrderInputBean.getWoEndDate());
		String dbStartDate=simpleDateFormat.format(datWorkorderBO.getWoStartDate());
		String dbEndDate=simpleDateFormat.format(datWorkorderBO.getWoEndDate());
		if (!datWorkorderBO.getIsInternal().equals(workOrderInputBean.getIsInternal()))
			comment = comment.append("internal type" + " from " + datWorkorderBO.getIsInternal() + " to "
					+ workOrderInputBean.getIsInternal() +",");
		if (!dbStartDate.equals(startDate)){
			System.out.println(datWorkorderBO.getWoStartDate().compareTo(workOrderInputBean.getWoStartDate()));
			comment = comment.append("start date" + " from " + datWorkorderBO.getWoStartDate() + " to "
					+ startDate+",");
		}
		if (!dbEndDate.equals(endDate)){
			System.out.println(datWorkorderBO.getWoEndDate().compareTo(workOrderInputBean.getWoEndDate()));
			comment = comment.append("end date" + " from " + datWorkorderBO.getWoEndDate() + " to "
					+ endDate+",");
		}
		if (!datWorkorderBO.getProjectOwner().equals(workOrderInputBean.getDeliveryManagerId()))
			comment = comment.append("delivery manager" + " from " + datWorkorderBO.getProjectOwner() + " to "
					+ workOrderInputBean.getDeliveryManagerId()+",");
		if (!(datWorkorderBO.getSowAmount().compareTo(workOrderInputBean.getSowAmount())==0)){
			int res=datWorkorderBO.getSowAmount().compareTo(new BigDecimal(0.0));
			int res1=workOrderInputBean.getSowAmount().compareTo(new BigDecimal(0.0));
			if(res!=0 || res1!=0)
			comment = comment.append("sow amount" + " from " + datWorkorderBO.getSowAmount() + " to "
					+ workOrderInputBean.getSowAmount()+",");
		}
		if (!(datWorkorderBO.getWorkorderDiscount().compareTo(workOrderInputBean.getDiscountValue())==0)){
			int res=datWorkorderBO.getWorkorderDiscount().compareTo(new BigDecimal(0.0));
			int res1=workOrderInputBean.getDiscountValue().compareTo(new BigDecimal(0.0));
			if(res!=0 || res1!=0)
			comment = comment.append("discount value" + " from " + datWorkorderBO.getWorkorderDiscount() + " to "
					+ workOrderInputBean.getDiscountValue()+",");
		}
		if (!datWorkorderBO.getWorkorderDescription().equals(workOrderInputBean.getWoDescription()))
			comment = comment.append("description " + " from " + datWorkorderBO.getWorkorderDescription() + " to "
					+ workOrderInputBean.getWoDescription()+",");
		if (!(datWorkorderBO.getAdditionalAmount().compareTo(workOrderInputBean.getAdditionalAmount())==0)){
			int res=datWorkorderBO.getAdditionalAmount().compareTo(new BigDecimal(0.0));
			int res1=workOrderInputBean.getAdditionalAmount().compareTo(new BigDecimal(0.0));
			if(res!=0 || res1!=0)
			comment = comment.append("additional amount" + " from " + datWorkorderBO.getAdditionalAmount() + " to "
					+ workOrderInputBean.getAdditionalAmount()+",");
		}
		return comment;
	}

	public Page<WorkOrderViewOutputBean> viewWorkOrder(WorkOrderViewInputBean workOrderViewInputBean)
			throws CommonCustomException, DataAccessException {
		this.datWorkorderBOList = new ArrayList<DatWorkorderBO>();
		Page<DatWorkorderBO> datWorkorderBOList = null;
		Page<WorkOrderViewOutputBean> finalPage;
		List<WorkOrderViewOutputBean> workOrderViewOutputBeanList = new ArrayList<WorkOrderViewOutputBean>();
		DatEmpDetailBO empDetail = null;
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBO = null;
		boolean existedInProjectLevelUserAccessTable = false;
		boolean existedInProjectCordinatorTable = false;
		Set<Integer> projectLevelUserAccessGroupBO = new HashSet<Integer>();
		Set<Integer> projDatProjectMappedToProjectCoordinatorBO = new HashSet<Integer>();
		List<String> workorderUserAccesRolesList = Arrays.asList(workorderUserAccesRoles.split(","));
		if(workOrderViewInputBean.getProjectId()==null)
			throw new CommonCustomException("Please select project");

		try {
			projectLevelUserAccessGroupBO = projectLevelUserAccessGroupRepository
					.getAllUserAccessByProjectId(workOrderViewInputBean.getProjectId());
		} catch (Exception e) {
		//	e.printStackTrace();
			throw new DataAccessException("Unable to view work order");
		}
		if (projectLevelUserAccessGroupBO.size() > 0) {
			if (projectLevelUserAccessGroupBO.contains(workOrderViewInputBean.getViewedBy())) {
				existedInProjectLevelUserAccessTable = true;
			}
		}
		if (!existedInProjectLevelUserAccessTable) {
			try {
				projDatProjectMappedToProjectCoordinatorBO = projDatProjectMappedToProjectCoordinatorRepository
						.getByProjectId(workOrderViewInputBean.getProjectId());
			} catch (Exception e) {
				//e.printStackTrace();
				throw new DataAccessException("Unable to view work order");
			}
			if (projDatProjectMappedToProjectCoordinatorBO.size() > 0) {
				if (projDatProjectMappedToProjectCoordinatorBO.contains(workOrderViewInputBean.getViewedBy())) {
					existedInProjectCordinatorTable = true;
				}
			}
		}
		try {
			empDetail = empDetailRepos.findByPkEmpId(workOrderViewInputBean.getViewedBy());
		} catch (Exception e) {
			//e.printStackTrace();
			throw new DataAccessException("Unable to get Logged in User Id Details");
		}
		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException("Logged In Employee Id is not a Active Employee.");
		else {
			Specification<DatWorkorderBO> datWorkorderBOObj = null;
			datWorkorderBOObj = WorkOrderAdvanceSearchSpecification.getAllWorkOrderDetails(workOrderViewInputBean);
			if(workOrderViewInputBean.getPageSize()<=0)
				throw new CommonCustomException("Page size should be greater than zero");
			
			PageRequest pageable = new PageRequest(workOrderViewInputBean.getPageNumber(),
					workOrderViewInputBean.getPageSize(), Sort.Direction.DESC, "workorderCreatedOn");
			try {
				datWorkorderBOList = datWorkorderRepository.findAll(datWorkorderBOObj, pageable);
			} catch (Exception e) {
				//e.printStackTrace();
				throw new DataAccessException("Failed to view work orders..........!!!!");
			}
			MasProjectBO masProjectBO = null;
			try {
				masProjectBO = masProjectRepository.findByPkProjectId(workOrderViewInputBean.getProjectId());
			} catch (Exception e1) {
				//e1.printStackTrace();
				throw new DataAccessException("Failed to view work orders.....!!!!@@@@");
			}
			if (masProjectBO != null) {
				if (!existedInProjectCordinatorTable && !existedInProjectLevelUserAccessTable) {
					try {
						datEmpProfessionalDetailBO = datEmployeeProfessionalRepository
								.findByFkMainEmpDetailId(workOrderViewInputBean.getViewedBy());
					} catch (Exception e) {
						//e.printStackTrace();
						throw new DataAccessException("Unable to get Logged in User Id Details");
					}
					if (datEmpProfessionalDetailBO == null)
						throw new CommonCustomException("Invalid Logged In Emp Id");
					if ((datEmpProfessionalDetailBO.getFkEmpBuUnit() != masProjectBO.getFkBuUnitId()) || !(workorderUserAccesRolesList.contains(empDetail.getMasEmpRoleBO().getEmpRoleKeyName())))
					{
						throw new CommonCustomException("You are not the authorized person to view work order");
					}
				}
			} 
			else{
				throw new CommonCustomException("Invalid project");
			}
			if (datWorkorderBOList != null) {
				for (DatWorkorderBO datWorkorderBO : datWorkorderBOList) {
					WorkOrderViewOutputBean workOrderViewOutputBean = new WorkOrderViewOutputBean();
					List<DatComponentBO> datComponentBO;
					try {
						datComponentBO = datComponentRepository.findByFkWorkorderId(datWorkorderBO.getPkWorkorderId());
					} catch (Exception e2) {
						//e2.printStackTrace();
						throw new DataAccessException("Failed to view work orders...@@@@");
					}
					if (datComponentBO != null) {
						workOrderViewOutputBean.setNoOfComponent(datComponentBO.size());
						workOrderViewOutputBean.setNoOfPo(10);
					}
					workOrderViewOutputBean.setProjectName(masProjectBO.getProjectName());
					workOrderViewOutputBean.setWoDescription(datWorkorderBO.getWorkorderDescription());
					workOrderViewOutputBean.setWoNumber(datWorkorderBO.getWorkorderNumber());
					workOrderViewOutputBean.setWoCreatedDate(datWorkorderBO.getWorkorderCreatedOn());
					ProjMasWoStatusBO projMasWoStatusBO;
					try {
						projMasWoStatusBO = projMasWoStatusRepository
								.findBypkWorkorderStatusId(datWorkorderBO.getFkWorkorderStatusId());
					} catch (Exception e) {
						//e.printStackTrace();
						throw new DataAccessException("Failed to view work orders....****");
					}
					if (workOrderViewOutputBean != null) {
						workOrderViewOutputBean.setWoStatus(projMasWoStatusBO.getWorkorderStatus());
						workOrderViewOutputBean.setWoId(datWorkorderBO.getPkWorkorderId());
					}
					workOrderViewOutputBeanList.add(workOrderViewOutputBean);
				}
			}
			finalPage = new PageImpl<WorkOrderViewOutputBean>(workOrderViewOutputBeanList, pageable,
					workOrderViewOutputBeanList.size());
			// }	
			return finalPage;
		}
	}

	public WorkOrderFullViewOutputBean fullViewWorkOrder(Integer workorderId, Integer empId)
			throws DataAccessException, CommonCustomException {
		WorkOrderFullViewOutputBean workOrderFullViewOutputBean = new WorkOrderFullViewOutputBean();
		DatEmpDetailBO empDetail=null;
		Set<Integer> projectLevelUserAccessGroupBO = new HashSet<Integer>();
		boolean existedInProjectLevelUserAccessTable = false;
		DatWorkorderBO datWorkorderBO=null;
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBO=null;
		boolean existedInProjectCordinatorTable = false;
		Set<Integer> projDatProjectMappedToProjectCoordinatorBO = new HashSet<Integer>();
		List<String> workorderUserAccesRolesList = Arrays.asList(workorderUserAccesRoles.split(","));
		try {
			empDetail = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			throw new DataAccessException("Unable to get Logged in User Id Details");
		}
		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException("Logged In Employee Id is not a Active Employee.");
		else {
			try {
				datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(workorderId);
			} catch (Exception e) {
				throw new DataAccessException("Unable to get work order");
			}
			if(datWorkorderBO==null)
				throw new CommonCustomException("work order doesn't exist");
			if (datWorkorderBO != null) {
				try {
					projectLevelUserAccessGroupBO = projectLevelUserAccessGroupRepository
							.getAllUserAccessByProjectId(datWorkorderBO.getFkProjectId());
				} catch (Exception e) {
					throw new CommonCustomException("Unable to create work order");
				}
				if (projectLevelUserAccessGroupBO.size() > 0) {
					if (projectLevelUserAccessGroupBO.contains(empId)) {
						existedInProjectLevelUserAccessTable = true;
					}
				}
				if (!existedInProjectLevelUserAccessTable) {
					try {
						projDatProjectMappedToProjectCoordinatorBO = projDatProjectMappedToProjectCoordinatorRepository
								.getByProjectId(datWorkorderBO.getFkProjectId());
					} catch (Exception e) {
						throw new DataAccessException("Unable to create work order");
					}
					if (projDatProjectMappedToProjectCoordinatorBO.size() > 0) {
						if (projDatProjectMappedToProjectCoordinatorBO.contains(empId)) {
							existedInProjectCordinatorTable = true;
						}
					}
				}
				MasProjectBO masProjectBO = datWorkorderBO.getMasProject();
				/*try {
					masProjectBO = masProjectRepository.findByPkProjectId(datWorkorderBO.getFkProjectId());
				} catch (Exception e1) {
					throw new DataAccessException("Failed to view work orders");
				}*/
				if (masProjectBO != null) {
					if (!existedInProjectCordinatorTable && !existedInProjectLevelUserAccessTable) {
						try {
							datEmpProfessionalDetailBO = datEmployeeProfessionalRepository
									.findByFkMainEmpDetailId(empId);
						} catch (Exception e) {
							throw new DataAccessException("Unable to get Logged in User Id Details");
						}
						if (datEmpProfessionalDetailBO == null)
							throw new CommonCustomException("Invalid Logged In Emp Id");
						if (datEmpProfessionalDetailBO.getFkEmpBuUnit() != masProjectBO.getFkBuUnitId() ||
								!(workorderUserAccesRolesList.contains(empDetail.getMasEmpRoleBO().getEmpRoleKeyName())))
							throw new CommonCustomException(
									"You are not the authorized person to view work order");
					}
				}
				workOrderFullViewOutputBean.setWoType(datWorkorderBO.getIsInternal());
				workOrderFullViewOutputBean.setFromDate(datWorkorderBO.getWoStartDate());
				workOrderFullViewOutputBean.setToDate(datWorkorderBO.getWoEndDate());
				List<DatComponentBO> datComponentBO;
				try {
					datComponentBO = datComponentRepository.findByFkWorkorderId(datWorkorderBO.getPkWorkorderId());
				} catch (Exception e1) {
					throw new DataAccessException("Unable to get work order");
				}
				if (datComponentBO != null) {
					workOrderFullViewOutputBean.setNoOfComponent(datComponentBO.size());
				}
				workOrderFullViewOutputBean.setNoOfPo(10);
				workOrderFullViewOutputBean.setProjectOwnerId(datWorkorderBO.getProjectOwner());
				workOrderFullViewOutputBean.setProjectOwnerName(
						gsrNotificationService.getEmployeeNameByEmpId(datWorkorderBO.getProjectOwner()));
				workOrderFullViewOutputBean.setWoDescription(datWorkorderBO.getWorkorderDescription());
				workOrderFullViewOutputBean.setWoId(datWorkorderBO.getPkWorkorderId());
				workOrderFullViewOutputBean.setWoNumber(datWorkorderBO.getWorkorderNumber());
				workOrderFullViewOutputBean.setSowAmount(datWorkorderBO.getSowAmount());
				workOrderFullViewOutputBean.setSowCurrencyId(datWorkorderBO.getSowCurrencyId());
				MasCurrencytypeBO masCurrencytypeBO=null;
				try {
					 masCurrencytypeBO=masCurrencyTypeRepository.getByCurrencyId(datWorkorderBO.getSowCurrencyId());
				} catch (Exception e1) {
					throw new DataAccessException("Unable to get work order");
				}
				workOrderFullViewOutputBean.setSowCurrencyName(masCurrencytypeBO.getCurrencyTypeCode());
				ProjMasWoStatusBO projMasWoStatusBO;
				try {
					projMasWoStatusBO = projMasWoStatusRepository
							.findBypkWorkorderStatusId(datWorkorderBO.getFkWorkorderStatusId());
				} catch (Exception e) {
					throw new DataAccessException("Unable to get work order");
				}
				if (projMasWoStatusBO != null)
					workOrderFullViewOutputBean.setWoStatus(projMasWoStatusBO.getWorkorderStatus());
			}
		}
		return workOrderFullViewOutputBean;
	}
	
	public boolean uploadSowFile(MultipartFile sowFile, SowFileUploadInput sowFileUploadInput,List<MultipartFile> SowFileUploadInputList)
		throws CommonCustomException, IOException, JSONException, DataAccessException {
	boolean result = false;
	DatEmpDetailBO empDetail;
	short defaultversionId = 1;
	short versionId;
	DatWorkorderBO datWorkorderBO = new DatWorkorderBO();
	DatSowUploadHistoryBO datSowUploadHistoryBO = new DatSowUploadHistoryBO();
	List<DatSowUploadHistoryBO> workorderExisted=null;
	Set<Integer> projDatProjectMappedToProjectCoordinatorBO = new HashSet<Integer>();
	if (sowFile == null)
		throw new CommonCustomException("Please select the file");
	if(sowFileUploadInput.getUploadedBy()==null)
	{
		throw new CommonCustomException("Enter valid Logged-in Employee id");
	}
	if(sowFileUploadInput.getWoId()==null)
	{
		throw new CommonCustomException("Enter valid Workorder id.");
	}
	try {
		empDetail = empDetailRepos.findByPkEmpId(sowFileUploadInput.getUploadedBy());
	} catch (Exception e) {
		throw new DataAccessException("Unable to get Logged in User Id Details");
	}
	if (empDetail == null)
		throw new CommonCustomException("Invalid Logged In Emp Id");
	if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
		throw new CommonCustomException("Logged In Employee Id is not an Active Employee.");
	else {
		try {
			datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(sowFileUploadInput.getWoId());
		} catch (Exception e) {
			throw new DataAccessException("Unable to upload sow");
		}
		if (datWorkorderBO != null) {
			try {
				projDatProjectMappedToProjectCoordinatorBO = projDatProjectMappedToProjectCoordinatorRepository
						.getByProjectId(datWorkorderBO.getFkProjectId());
			} catch (Exception e) {
				throw new DataAccessException("Unable to upload sow");
			}
			if (projDatProjectMappedToProjectCoordinatorBO.size() > 0) {
				if (!projDatProjectMappedToProjectCoordinatorBO.contains(sowFileUploadInput.getUploadedBy())) {
					throw new CommonCustomException(
							"You are not the authorized person to upload sow");
				}
			}
			else {
				throw new CommonCustomException("You are not the authorized person to upload sow");
			}
		}
		try {
			workorderExisted = datSowUploadHistoryRepository.findByFkWorkorderId(sowFileUploadInput.getWoId());
		} catch (Exception e) {
			throw new DataAccessException("Unable to get latest version order");
		}
		if(workorderExisted.isEmpty())
			versionId=defaultversionId;
		else {
			try {
				versionId = datSowUploadHistoryRepository.getbyWorkorderId(sowFileUploadInput.getWoId());
				versionId = (short) (versionId + defaultversionId);
			} catch (Exception e) {
				throw new DataAccessException("Unable to get latest version order");
			}
		}
		String localFileName = sowFile.getOriginalFilename();
		String fileName = sowFileUploadInput.getWoId() + "@" + versionId + "^" + localFileName ;
		datSowUploadHistoryBO.setCreatedOn(new Date());
		datSowUploadHistoryBO.setSowFileName(localFileName);
		datSowUploadHistoryBO.setCreatedBy(sowFileUploadInput.getUploadedBy());
		datSowUploadHistoryBO.setFkWorkorderId(sowFileUploadInput.getWoId());
		datSowUploadHistoryBO.setSowRefFileName(fileName);
		datSowUploadHistoryBO.setVersionId(versionId);
		try {
			datSowUploadHistoryBO = datSowUploadHistoryRepository.save(datSowUploadHistoryBO);
		} catch (Exception e) {
			throw new DataAccessException("Fail to upload file.Please contact administrator.");
		}
		//datWorkorderBO.setSowUploadedFlag("UPLOADED");
		datWorkorderBO.setWorkorderUpdatedBy(sowFileUploadInput.getUploadedBy());
		datWorkorderBO.setWorkorderUpdatedOn(new Date());
		if (!SowFileUploadInputList.isEmpty() && sowFile!=null)
			datWorkorderBO.setCommentForUpdate("Uploaded sow and supporting document file");
		else if(SowFileUploadInputList.isEmpty() && sowFile!=null)
			datWorkorderBO.setCommentForUpdate("Uploaded sow document");
		else if(!SowFileUploadInputList.isEmpty() && sowFile==null)
			datWorkorderBO.setCommentForUpdate("Uploaded supporting document");
		datWorkorderBO.setSowUploadedFlag("UPLOADED");
		result = this.upload(datWorkorderBO.getPkWorkorderId(), sowFile, versionId);

		DatWorkorderBO saved;

		try {
			saved = datWorkorderRepository.save(datWorkorderBO);
		} catch (Exception e) {
			throw new DataAccessException("Fail to upload file.Please contact administrator.");
		}

		if (result == false)
			throw new CommonCustomException("Fail to upload file.Please contact administrator.");

		if(result && saved != null && datSowUploadHistoryBO != null) {
			projectNotificationService.sendSowUploadNotificationToDMDL(datSowUploadHistoryBO.getFkWorkorderId(), datSowUploadHistoryBO.getVersionId());
		}
		/*} else {
throw new CommonCustomException("Fail to upload file.Please contact administrator.");

}*/
	}
	if (SowFileUploadInputList.size() > 0) {
		for (MultipartFile supportFileUploadInput : SowFileUploadInputList) {
			uploadSowSupportFile(supportFileUploadInput, sowFileUploadInput);
		}
	}
	return result;
}
	
	public boolean upload(Integer workorderNumber, MultipartFile sowFile, short version)
			throws IOException, CommonCustomException {
		boolean result = false;
		try {
			String localFileName = sowFile.getOriginalFilename().replaceAll("\\s+", "");
			String fileName =  workorderNumber + "@" + version + "^" + localFileName ;
			File saveFile = new File(WORKORDER_SOW_UPLOAD_DIRECTORY + workorderNumber + "/" + version, fileName);
			FileUtils.writeByteArrayToFile(saveFile, sowFile.getBytes());
			result = true;
		} catch (Exception e) {
			result = false;
			throw new CommonCustomException("" + sowFile.getOriginalFilename() + " saved failed");
		}
		return result;
	}

	public boolean downloadSowFile(SowFileDownloadInput sowFileDownloadInput, HttpServletResponse response)
			throws CommonCustomException, DataAccessException, IOException {
		boolean result = false;
		DatEmpDetailBO empDetail;
		DatWorkorderBO datWorkorderBO = new DatWorkorderBO();
		boolean existedInProjectLevelUserAccessTable=false;
		DatSowUploadHistoryBO datSowUploadHistoryBO = null;
		Set<Integer> projectLevelUserAccessGroupBO = new HashSet<Integer>();
		Set<Integer> projDatProjectMappedToProjectCoordinatorBO = new HashSet<Integer>();
		try {
			empDetail = empDetailRepos.findByPkEmpId(sowFileDownloadInput.getDownloadedBy());
		} catch (Exception e) {
			throw new DataAccessException("Unable to get Logged in User Id Details");
		}
		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException("Logged In Employee Id is not a Active Employee.");
		else {
			try {
				datSowUploadHistoryBO = datSowUploadHistoryRepository.findByFkWorkorderIdAndVersionId(
						sowFileDownloadInput.getWoId(), sowFileDownloadInput.getVersion());
			} catch (Exception e) {
				throw new DataAccessException("Unable to download sow file");
			}
			if (datSowUploadHistoryBO != null) {
				try {
					datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(sowFileDownloadInput.getWoId());
				} catch (Exception e) {
					throw new DataAccessException("Unable to download sow file");
				}
				if (datWorkorderBO != null) {
					try {
						projectLevelUserAccessGroupBO = projectLevelUserAccessGroupRepository
								.getAllUserAccessByProjectId(datWorkorderBO.getFkProjectId());
					} catch (Exception e) {
						throw new DataAccessException("Unable to view work order");
					}
					if (projectLevelUserAccessGroupBO.size() > 0) {
						if (projectLevelUserAccessGroupBO.contains(sowFileDownloadInput.getDownloadedBy())) {
							existedInProjectLevelUserAccessTable = true;
						}
					}
					if (!existedInProjectLevelUserAccessTable) {
						try {
							projDatProjectMappedToProjectCoordinatorBO = projDatProjectMappedToProjectCoordinatorRepository
									.getByProjectId(datWorkorderBO.getFkProjectId());
						} catch (Exception e) {
							throw new DataAccessException("Unable to create work order");
						}
						if (projDatProjectMappedToProjectCoordinatorBO.size() > 0) {
							if (!projDatProjectMappedToProjectCoordinatorBO.contains(sowFileDownloadInput.getDownloadedBy())) {
								throw new CommonCustomException(
										"You are not the authorized person to download sow");
							}
						}
						else {
							throw new CommonCustomException("You are not the authorized person to download sow");
						}
					}
					String basePath = WORKORDER_SOW_UPLOAD_DIRECTORY + sowFileDownloadInput.getWoId() + "/"
							+ sowFileDownloadInput.getVersion() + "/";
					// String basePath =
					// "D:/opt/Documents/Project_Management_Docs/Sow_Docs/30/13/";
					DownloadFileUtil.downloadFile(response, basePath, sowFileDownloadInput.getFileName());
					result = true;
				} else {
					throw new CommonCustomException("Work order does not exists...!");
				}
			}else{
				throw new CommonCustomException("File does not exists...!");
			}
		}
		return result;
	}

	public WorkOrderOutputBean getWorkOrder(Integer workorderId, Integer empId)
			throws DataAccessException, CommonCustomException {
		WorkOrderOutputBean workOrderOutputBean = new WorkOrderOutputBean();
		ProjMasClientManagerBO projMasClientManagerBO = null;
		DatEmpDetailBO empDetail;
		MasEmpRoleBO masEmpRoleBO;
		DatWorkorderBO datWorkorderBO = null;
		boolean existedInProjectLevelUserAccessTable = false;
		boolean existedInProjectCordinatorTable = false;
		boolean existedInFinanceAccessList =false;
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBO = null;
		Set<Integer> projectLevelUserAccessGroupBO = new HashSet<Integer>();
		Set<Integer> projDatProjectMappedToProjectCoordinatorBO = new HashSet<Integer>();
		List<DatComponentBO> listDatComponentBo = new ArrayList<DatComponentBO>();
		List<String> financeList = new ArrayList<String>();
		int componentCount;
		int poCount;
		BigDecimal sumOfComp,sowAmount;
		
		financeList = Arrays.asList(financeTeamAccessRoleList.split(","));
	
		try {
			empDetail = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			throw new DataAccessException("Unable to get Logged in User Id Details");
		}
		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException("Logged In Employee Id is not a Active Employee.");
		else {
			try {
				datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(workorderId);
			} catch (Exception e) {
			//	e.printStackTrace();

				throw new DataAccessException("Unable to get work order");
			}
			if (datWorkorderBO != null) {
				try {
					projectLevelUserAccessGroupBO = projectLevelUserAccessGroupRepository
							.getAllUserAccessByProjectId(datWorkorderBO.getFkProjectId());
				} catch (Exception e) {
					//e.printStackTrace();
					throw new CommonCustomException("Unable to get work order");
				}
				if (projectLevelUserAccessGroupBO.size() > 0) {
					if (projectLevelUserAccessGroupBO.contains(empId))
						existedInProjectLevelUserAccessTable = true;
				}
				if (!existedInProjectLevelUserAccessTable) {
					try {
						projDatProjectMappedToProjectCoordinatorBO = projDatProjectMappedToProjectCoordinatorRepository
								.getByProjectId(datWorkorderBO.getFkProjectId());
					} catch (Exception e) {
					//	e.printStackTrace();
						throw new DataAccessException("Unable to get work order");
					}
					if (projDatProjectMappedToProjectCoordinatorBO.size() > 0) {
						if (projDatProjectMappedToProjectCoordinatorBO.contains(empId))
							existedInProjectCordinatorTable = true;
					}
				}
				masEmpRoleBO = empDetail.getMasEmpRoleBO();
				if(!existedInProjectCordinatorTable && !existedInProjectLevelUserAccessTable)
				{
					if(financeList.contains(masEmpRoleBO.getEmpRoleKeyName()))
					existedInFinanceAccessList = true;
				}
				workOrderOutputBean.setAdditionalAmount(datWorkorderBO.getAdditionalAmount());
				try {
					projMasClientManagerBO = projMasClientManagerRepository
							.findByPkClientManagerId(datWorkorderBO.getClientManagerId());
				} catch (Exception e2) {
					throw new DataAccessException("Failed to get work orders");
				}
				if (projMasClientManagerBO != null) {
					workOrderOutputBean.setClientManagerId(projMasClientManagerBO.getPkClientManagerId());
					workOrderOutputBean.setClientManagerName(projMasClientManagerBO.getManagerName());
					workOrderOutputBean.setClientManagerEmailId(projMasClientManagerBO.getEmailAddress());
				}
				if(datWorkorderBO.getCommentForUpdate()!=null){
				List<String> comments = Arrays.asList(datWorkorderBO.getCommentForUpdate().split(projectHistoryCommentsPattern));
				workOrderOutputBean.setCommentForUpdate(comments.get(comments.size()-1));
				}
				workOrderOutputBean.setDeliveryManagerId(datWorkorderBO.getProjectOwner());
				workOrderOutputBean.setDeliveryManagerName(
						gsrNotificationService.getEmployeeNameByEmpId(datWorkorderBO.getProjectOwner()));
				workOrderOutputBean.setProjectId(datWorkorderBO.getFkProjectId());
				
				 MasProjectBO masProjectBO = datWorkorderBO.getMasProject();
				
				if (masProjectBO != null) {
					workOrderOutputBean.setProjectName(masProjectBO.getProjectName());
					if (!existedInProjectCordinatorTable && !existedInProjectLevelUserAccessTable && !existedInFinanceAccessList) {
						try {
							datEmpProfessionalDetailBO = datEmployeeProfessionalRepository
									.findByFkMainEmpDetailId(empId);
						} catch (Exception e) {
							throw new DataAccessException("Unable to get Logged in User Id Details");
						}
						if (datEmpProfessionalDetailBO == null)
							throw new CommonCustomException("Invalid Logged In Emp Id");
						if (datEmpProfessionalDetailBO.getFkEmpBuUnit() != masProjectBO.getFkBuUnitId())
							throw new CommonCustomException("You are not the authorized person to get work order details");
					}
				}
				
				try{
				listDatComponentBo = datComponentRepository.getWorkOrderDetails(workorderId);
				}catch(Exception e)
				{
				//	e.printStackTrace();
					throw new CommonCustomException("Something went wrong while getting component details");
				}
				
				componentCount = listDatComponentBo.size();
				poCount = 2;
				
				ProjMasWoStatusBO projMasWoStatusBO = datWorkorderBO.getProjMasWoStatus();
				MasCurrencytypeBO masCurrencyBo = datWorkorderBO.getMasCurrencytype();
				workOrderOutputBean.setSowAmount(datWorkorderBO.getSowAmount());
				workOrderOutputBean.setSowCurrencyId(datWorkorderBO.getSowCurrencyId());
				workOrderOutputBean.setWoCreatedBy(datWorkorderBO.getWorkorderCreatedBy());
				workOrderOutputBean.setWoDescription(datWorkorderBO.getWorkorderDescription());
				workOrderOutputBean.setWoEndDate(datWorkorderBO.getWoEndDate());
				workOrderOutputBean.setWoInitial(datWorkorderBO.getWorkorderNumber().substring(0, 3));
				workOrderOutputBean.setWoId(datWorkorderBO.getPkWorkorderId());
				workOrderOutputBean.setWoStartDate(datWorkorderBO.getWoStartDate());
				workOrderOutputBean.setSowCurrencyName(masCurrencyBo.getCurrencyTypeCode());
				workOrderOutputBean.setNoOfcomponents(componentCount);
				workOrderOutputBean.setNoOfPo(poCount);
				workOrderOutputBean.setWoType(datWorkorderBO.getIsInternal());
				workOrderOutputBean.setWoNumber(datWorkorderBO.getWorkorderNumber());
				workOrderOutputBean.setDiscountValue(datWorkorderBO.getWorkorderDiscount());
				workOrderOutputBean.setBuId(masProjectBO.getFkBuUnitId());
				try{
				sowAmount = datWorkorderBO.getSowAmount();
				sumOfComp = datComponentRepository.getSumOfComponentAmount(workorderId);
				}catch(Exception e)
				{
					throw new DataAccessException("Something went wrong while getting sum of component value");
				}
				System.out.println(sumOfComp);
				if(sumOfComp!=null)
				{
				BigDecimal woBal = sowAmount.subtract(sumOfComp);
				
				workOrderOutputBean.setWoBal(woBal);
				}else
				{	
					workOrderOutputBean.setWoBal(datWorkorderBO.getSowAmount());
				}
				
				if (projMasWoStatusBO != null) {
					workOrderOutputBean.setWoStatusName(projMasWoStatusBO.getWorkorderStatus());
					workOrderOutputBean.setWoStatusId(projMasWoStatusBO.getPkWorkorderStatusId());
				}
			}
			else
			{
				throw new CommonCustomException("No records found with that workorderId");
			}
		}
		
		return workOrderOutputBean;
	}

	// Added by Rinta Mariam Jose

	public List<ProjMasWoStatusBean> getAllProjMasWorkOrderStatus() throws DataAccessException, CommonCustomException {
		List<ProjMasWoStatusBean> projMasWoStatusBeanList = new ArrayList<ProjMasWoStatusBean>();
		List<ProjMasWoStatusBO> projMasWoStatusBOList = null;

		try {
			projMasWoStatusBOList = (List<ProjMasWoStatusBO>) projMasWoStatusRepository.findAll();
		}

		catch (Exception e) {
			throw new DataAccessException("Failed to fetch the work order status : ", e);
		}
		
		if (!projMasWoStatusBOList.isEmpty()) {
			for (ProjMasWoStatusBO projMasWoStatusBO : projMasWoStatusBOList) {
				ProjMasWoStatusBean projMasWoStatusBean = new ProjMasWoStatusBean();
				projMasWoStatusBean.setWoId(projMasWoStatusBO.getPkWorkorderStatusId());
				projMasWoStatusBean.setWoStatus(projMasWoStatusBO.getWorkorderStatus());
				projMasWoStatusBeanList.add(projMasWoStatusBean);
			}
		} else
			throw new CommonCustomException("No work order status is available");
		
		return projMasWoStatusBeanList;
	}

	// EOA by Rinta Mariam Jose
	
	public WorkOrderDocOutputBean getDocDetails(Integer workorderId, Integer empId)
			throws CommonCustomException, DataAccessException {
		WorkOrderDocOutputBean workOrderDocOutputBean = new WorkOrderDocOutputBean();
		List<DocDetails> lstDocDetails = new ArrayList<DocDetails>();
		DatWorkorderBO datWorkorderBO = null;
		List<DatSowUploadHistoryBO> listDatSowUploadHistoryBO = new ArrayList<DatSowUploadHistoryBO>();
		List<DatSowSupportDocsUploadHistoryBO> lstDatSowSupportDocsUploadHistoryBO = new ArrayList<DatSowSupportDocsUploadHistoryBO>();
		boolean existedInProjectLevelUserAccessTable = false;
		Set<Integer> projectLevelUserAccessGroupBO = new HashSet<Integer>();
		DatEmpDetailBO empDetail=null;
		Set<Integer> projDatProjectMappedToProjectCoordinatorBO = new HashSet<Integer>();
		try {
			empDetail = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			throw new DataAccessException("Unable to get Logged in User Id Details");
		}
		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException("Logged In Employee Id is not a Active Employee.");
		else {
			try {
				datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(workorderId);
			} catch (Exception e) {
				throw new DataAccessException("Unable to get work order");
			}
			if (datWorkorderBO != null) {
				try {
					projectLevelUserAccessGroupBO = projectLevelUserAccessGroupRepository
							.getAllUserAccessByProjectId(datWorkorderBO.getFkProjectId());
				} catch (Exception e) {
					throw new CommonCustomException("Unable to view work order");
				}
				if (projectLevelUserAccessGroupBO.size() > 0) {
					if (projectLevelUserAccessGroupBO.contains(empId)) {
						existedInProjectLevelUserAccessTable = true;
					}
				}
				if (!existedInProjectLevelUserAccessTable) {
					try {
						projDatProjectMappedToProjectCoordinatorBO = projDatProjectMappedToProjectCoordinatorRepository
								.getByProjectId(datWorkorderBO.getFkProjectId());
					} catch (Exception e) {
						throw new DataAccessException("Unable to view work order");
					}
					if (projDatProjectMappedToProjectCoordinatorBO.size() > 0) {
						if (!projDatProjectMappedToProjectCoordinatorBO.contains(empId)) {
							throw new CommonCustomException("You are not the authorized person to get doc details");
						}
					} else
						throw new CommonCustomException("You are not the authorized person to get doc details");
				}
				try {
					listDatSowUploadHistoryBO = datSowUploadHistoryRepository.findByFkWorkorderId(workorderId);
				} catch (Exception e) {
					throw new DataAccessException("Unable to get work order");
				}
				if (listDatSowUploadHistoryBO.size() > 0) {
					for (DatSowUploadHistoryBO DatSowUploadHistoryBO : listDatSowUploadHistoryBO) {
						DocDetails docDetails = new DocDetails();
						List<SupportDocDetails> listSupportDocDetails = new ArrayList<SupportDocDetails>();
						docDetails.setSowRefFileName(DatSowUploadHistoryBO.getSowRefFileName());
						docDetails.setSowFileName(DatSowUploadHistoryBO.getSowFileName());
						docDetails.setSowRevisionDate(DatSowUploadHistoryBO.getCreatedOn());
						docDetails.setVersion(DatSowUploadHistoryBO.getVersionId());
						lstDatSowSupportDocsUploadHistoryBO = datSowSupportDocsUploadHistoryRepository
								.findByFkWorkorderIdAndVersionId(workorderId,DatSowUploadHistoryBO.getVersionId());
						for (DatSowSupportDocsUploadHistoryBO datSowSupportDocsUploadHistoryBO : lstDatSowSupportDocsUploadHistoryBO) {
							SupportDocDetails supportDocDetails = new SupportDocDetails();
							supportDocDetails.setFileName(datSowSupportDocsUploadHistoryBO.getSowSupportFileName());
							supportDocDetails
									.setRefFileName(datSowSupportDocsUploadHistoryBO.getSowSupportFileRefName());
							supportDocDetails.setUploadedOn(datSowSupportDocsUploadHistoryBO.getCreatedOn());
							listSupportDocDetails.add(supportDocDetails);
						}
						docDetails.setSupportDocDetails(listSupportDocDetails);
						lstDocDetails.add(docDetails);
					}
					workOrderDocOutputBean.setDocDetails(lstDocDetails);
				}
			} else {
				throw new CommonCustomException("work order doesn't exist");
			}
		}
		return workOrderDocOutputBean;
	}
	
		//Added by Lalith kumar
	
	public List<DatWorkorderBean> getAllWorkOrderByProjectId(Integer projectId) throws DataAccessException, CommonCustomException {
		List<DatWorkorderBean> datWorkorderBeanList = new ArrayList<DatWorkorderBean>();
		MasProjectBO masProjectBO;
			
		try{
			masProjectBO = masProjectRepository.findByPkProjectId(projectId);
			//LOG.info("masProjectBO...!!!!!!"+masProjectBO);
		}catch(Exception e)
		{
			throw new DataAccessException("Something went wrong while fetching project details");
		}
		if(masProjectBO!=null)
		{
		try {
			datWorkorderBeanList = datWorkorderRepository.getWoByProjectId(projectId);
		}
		catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		if(datWorkorderBeanList.isEmpty())
		{
			throw new CommonCustomException("No workorders found for that project");
		}
		
		}else
		{
			throw new CommonCustomException("No project found for that projectId");
		}

		return datWorkorderBeanList;
	}
	
	//EOA by Lalith kumar
	
	//Added by Lalith kumar
	
	public boolean uploadSowSupportFile(MultipartFile sowSupportFile, SowFileUploadInput sowFileUploadInput)
			throws CommonCustomException, IOException, JSONException, DataAccessException {
		boolean result = false;
		DatEmpDetailBO empDetail;
		short versionId;
		String fileName;
		String localFileName;
		DatWorkorderBO datWorkorderBO = new DatWorkorderBO();
		DatSowSupportDocsUploadHistoryBO datSowSupportDocsUploadHistoryBO = new DatSowSupportDocsUploadHistoryBO();
		List<Integer> accessList = new ArrayList<Integer>();
//		if(sowSupportFile==null)
//			throw new CommonCustomException("Please select the file");
		try {
			LOG.info("Entering Access try block....");
			accessList = projectMappedToCoordinatorRepos.getAllPcRelatedToProject(sowFileUploadInput.getWoId());
			LOG.info("Retrived AccessList...."+accessList);
		} catch (Exception e) {
			//e.printStackTrace();
			throw new CommonCustomException("Unable to view work order");
		}
		if (accessList.size() > 0) {
			
		if (accessList.contains(sowFileUploadInput.getUploadedBy())) {
			
		
		try {
			empDetail = empDetailRepos.findByPkEmpId(sowFileUploadInput.getUploadedBy());
		} catch (Exception e) {
			throw new DataAccessException("Unable to get Logged in User Id Details");
		}
		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		if (!(empDetail.getFkEmpMainStatus()== activeEmpStatus))
			throw new CommonCustomException("Logged In Employee Id is not a Active Employee.");
		else {
			try {
				datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(sowFileUploadInput.getWoId());
			} catch (Exception e) {
				throw new DataAccessException("Unable to get work order");
			}
			if (datWorkorderBO != null) {
				
				try {
					versionId = datSowUploadHistoryRepository.getbyWorkorderId(sowFileUploadInput.getWoId());
				} catch (Exception e) {
					throw new DataAccessException("Unable to get latest version order");
				}
				
				try {
					long epochTimestamp = System.currentTimeMillis() / 1000L;
					localFileName = sowSupportFile.getOriginalFilename().replaceAll("\\s+", "");
					String file=FilenameUtils.removeExtension(localFileName);
					String extension=FilenameUtils.getExtension(localFileName);
					fileName = file + epochTimestamp + "." + extension;
					File saveFile = new File(WORKORDER_SOW_SUPPORT_UPLOAD_DIRECTORY + sowFileUploadInput.getWoId() + "/" + versionId+"/"+"SupportingDocs", fileName);
					FileUtils.writeByteArrayToFile(saveFile, sowSupportFile.getBytes());
					result = true;
				} catch (Exception e) {
					result = false;
					throw new CommonCustomException("" + sowSupportFile.getOriginalFilename() + " saved failed");
				}
				
				
				datSowSupportDocsUploadHistoryBO.setCreatedOn(new Date());
				datSowSupportDocsUploadHistoryBO.setSowSupportFileName(localFileName);
				datSowSupportDocsUploadHistoryBO.setCreatedBy(sowFileUploadInput.getUploadedBy());
				datSowSupportDocsUploadHistoryBO.setFkWorkorderId(sowFileUploadInput.getWoId());
				datSowSupportDocsUploadHistoryBO.setSowSupportFileRefName(fileName);
				datSowSupportDocsUploadHistoryBO.setVersionId(versionId);
				try {
					datSowSupportDocsUploadHistoryBO = datSowSupportDocsUploadHistoryRepository.save(datSowSupportDocsUploadHistoryBO);
					result = true;
				} catch (Exception e) {
					throw new CommonCustomException("Fail to upload file.Please contact administrator.");
				}
			}
		}
				
				if (result == false)
					throw new CommonCustomException("Fail to upload file.Please contact administrator.");
			} else {
				throw new CommonCustomException("LoggedIn employ does not have access to upload this file..");
			}
		}else
		{
			throw new CommonCustomException("no records found for that workorder id..");
		}
		return result;
	}
	
	//EOA by Lalith kumar
	
	//Added by Lalith kumar
	
	public boolean downloadSowSupportFile(SowFileDownloadInput sowFileDownloadInput, HttpServletResponse response)
			throws CommonCustomException, DataAccessException, IOException {
		boolean result = false;
		DatEmpDetailBO empDetail;
		DatSowSupportDocsUploadHistoryBO datSowSupportDocsUploadHistoryBO ;
		try {
			empDetail = empDetailRepos.findByPkEmpId(sowFileDownloadInput.getDownloadedBy());
		} catch (Exception e) {
			throw new DataAccessException("Unable to get Logged in User Id Details");
		}
		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		LOG.info("empDetail.getFkEmpMainStatus()......."+empDetail.getFkEmpMainStatus());
		if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException("Logged In Employee Id is not a Active Employee.");
		else {
			DatWorkorderBO datWorkorderBO;
			try{
				datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(sowFileDownloadInput.getWoId());
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to download sow support file");
			}
			
			if(datWorkorderBO != null)
			{
				LOG.info("datWorkoderBo is not null:"+datWorkorderBO);
				
				try {
					 datSowSupportDocsUploadHistoryBO = datSowSupportDocsUploadHistoryRepository.findBySowSupportFileRefName(sowFileDownloadInput.getFileName());
				} catch (Exception e) {
					throw new DataAccessException("Unable to download sow support file");
				}
				if (datSowSupportDocsUploadHistoryBO != null) {
				
					String basePath = WORKORDER_SOW_SUPPORT_UPLOAD_DIRECTORY + sowFileDownloadInput.getWoId()
							+ "/" + sowFileDownloadInput.getVersion() + "/" + "SupportingDocs" + "/";
					

					try{
					DownloadFileUtil.downloadFile(response, basePath, sowFileDownloadInput.getFileName());
					}catch(Exception e)
					{
						//();
						throw new CommonCustomException("File does not exists...!");
					}
				}else
				{
					throw new CommonCustomException("Unable to download : File does not exists."); 
				}
					result = true;
			
			}else{
				throw new CommonCustomException("WorkOrder does not exists...!");
			}
		}
		return result;
	}
	
	//EOA by Lalith kumar
	
	
	//Added by Lalith kumar for getting all project names based on loggedIn
	public List<ProjectNamesOutputBean> getAllProjectNamesBasedOnLoggedIn(Integer empId) throws DataAccessException, CommonCustomException
	{
		Set<ProjectNamesOutputBean> projectNames = new HashSet<ProjectNamesOutputBean>(); 
		Set<ProjectNamesOutputBean> projectNamesOnBu = new HashSet<ProjectNamesOutputBean>();
		DatEmpDetailBO empDetail;
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBO;
		List<String> workorderUserAccesRolesList = Arrays.asList(workorderUserAccesRoles.split(","));
	
		try {
			empDetail = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			throw new DataAccessException("Unable to get Logged in User Id Details");
		}
		//checking employ is exists or not
		if(empDetail != null)
		{
			if (!(empDetail.getFkEmpMainStatus() == activeEmpStatus))
				throw new CommonCustomException("Logged In Employee Id is not a Active Employee.");
			
			//Getting all the project names from pc and useraccess tables
			try{
				projectNames = masProjectRepository.getAllProjectBasedOnEmpId(empId);
				
			}catch(Exception e)
			{
			//	e.printStackTrace();
				throw new DataAccessException("Unable to get project names...!");
			}
			try{
				datEmpProfessionalDetailBO = datEmployeeProfessionalRepository.findByFkMainEmpDetailId(empId);
			}catch(Exception e)
			{
			//	e.printStackTrace();
				throw new DataAccessException("Unable to get Logged in user details...!");
			}
			
			//Getting all the project names based on loggedIn Bu details
			if(workorderUserAccesRolesList.contains(empDetail.getMasEmpRoleBO().getEmpRoleKeyName()))
			{
				try{
				
						projectNamesOnBu = masProjectRepository.getAllProjectBasedOnBuId(datEmpProfessionalDetailBO.getFkEmpBuUnit());
				}catch(Exception e)
				{
				//	e.printStackTrace();
					throw new DataAccessException("Unable to get project names...!");
				}
				
				//Combining about two outputs into one list
				projectNames.addAll(projectNamesOnBu);
				
			}else
			{
				throw new CommonCustomException("Your are not Authorized person to view projects");
			}
			
		}else
		{
			throw new CommonCustomException("LoggedIn user does not exists...!");
		}
		
		List<ProjectNamesOutputBean> p = new ArrayList<ProjectNamesOutputBean>(projectNames);
		
		 return p;
	}
	//EOA by Lalith kumar
	
	//Added by Lalith kumar for getting all WoNumbers based on loggedIn
	
	public List<WoNumberOutputBean> getAllWoNumbersBasedOnLoggedIn(Integer empId) throws DataAccessException, CommonCustomException
	{
		Set<WoNumberOutputBean> woNumbersList = new HashSet<WoNumberOutputBean>(); 
		DatEmpDetailBO empDetail;
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBO;
		List<String> workorderUserAccesRolesList = Arrays.asList(workorderUserAccesRoles.split(","));
	
		try {
			empDetail = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			throw new DataAccessException("Unable to get Logged in User Id Details");
		}
		
		//checking whether loggedIn user exists or not
		if(empDetail != null)
		{
			if (!(empDetail.getFkEmpMainStatus() == activeEmpStatus))
				throw new CommonCustomException("Logged In Employee Id is not a Active Employee.");
			try{
				datEmpProfessionalDetailBO = datEmployeeProfessionalRepository.findByFkMainEmpDetailId(empId);
			}catch(Exception e)
			{
			//	e.printStackTrace();
				throw new DataAccessException("Unable to get Logged in user details...!");
			}
			
			//checking loggedIn keyrole name whether it is above pc or not
			if(workorderUserAccesRolesList.contains(empDetail.getMasEmpRoleBO().getEmpRoleKeyName()))
			{
				//Getting all wo numbers based on LoggedIn
				try{
					woNumbersList = datWorkorderRepository.getAllWoNumbersBasedOnEmpId(empId, datEmpProfessionalDetailBO.getFkEmpBuUnit());
				}catch(Exception e)
				{
					throw new DataAccessException("Unable to fetch Workorder numbers");
				}
			}else
			{
				throw new CommonCustomException("You are not authorized person to show workorder numbers");
			}
		
		}else
			throw new CommonCustomException("LoggedIn user does not exists.");

		
		List<WoNumberOutputBean> woNumList = new ArrayList<WoNumberOutputBean>(woNumbersList);
		return woNumList;
	}
	//EOA by Lalith kumar
	
	
	/*public static void main(String args[]) {
		final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		String email = "deepa@com";
		
		
	}*/

	public ProjectHistoryOutputBean getWorkOrderHistory(Short projectId) {
	//	datWorkorderAuditRepository.findBy
		return null;
	}  
	
	// Added By Rinta
	/**
	 * Description: This service is used to get all workorder initials.
	 * 
	 * @return List<String>
	 * @throws DataAccessException
	 */
	public List<String> getAllWorkOrderInitials()
			throws DataAccessException {

		List<String> workOrderInitialsList;
		try {
			workOrderInitialsList = datWorkorderInitialRepository
					.getAllWoIntials();
		} catch (Exception e) {
			throw new DataAccessException(
					"Some error occured during fetching workorder initials.");
		}
		return workOrderInitialsList;
	}

	// EOA By Rinta
	
	
	//Added by Lalith kumar for getting all project names based on BuId
		public List<WoNumberOutputBean> getAllWoBasedOnBuId(Short buId) throws DataAccessException, CommonCustomException
		{
			Set<WoNumberOutputBean> woNumOutputSet = new HashSet<WoNumberOutputBean>(); 
			List<WoNumberOutputBean> woNumOutputList ;

				
				//Getting all the project names from pc and useraccess tables
				try{
					woNumOutputSet = datWorkorderRepository.getAllWoNumbersBasedOnBuId(buId);
					
				}catch(Exception e)
				{
				//	e.printStackTrace();
					throw new DataAccessException("Unable to get project names...!");
				}
			
				woNumOutputList=new ArrayList<WoNumberOutputBean>(woNumOutputSet);
				
			return woNumOutputList;
		
		}
		//EOA by Lalith kumar
		
	
	public static void main(String[] args) {
		int maxWoNumber=99999;
		String woNumber=null;
		woNumber = String.format("%05d", maxWoNumber);  
		System.out.println("/...."+woNumber);
	}
}