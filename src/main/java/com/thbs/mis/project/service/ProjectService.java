/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  ProjectService.java                               */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  05-March-2018                                     */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*      Date        Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 05-March-2018    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.project.service;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.bean.ProjectCoordinatorBean;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.MasAccountBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.bo.MasDomainBO;
import com.thbs.mis.common.bo.MasRegionBO;
import com.thbs.mis.common.bo.MasVerticalBO;
import com.thbs.mis.common.dao.BusinessUnitRepository;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.MasAccountRepository;
import com.thbs.mis.common.dao.MasDomainRepository;
import com.thbs.mis.common.dao.MasRegionRepository;
import com.thbs.mis.common.dao.MasVerticalRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.service.GSRGoalService;
import com.thbs.mis.notificationframework.gsrnotification.service.GSRNotificationService;
import com.thbs.mis.notificationframework.projectnotification.service.ProjectNotificationService;
import com.thbs.mis.project.bean.CreateProjectBean;
import com.thbs.mis.project.bean.GetProjectsOutputBean;
import com.thbs.mis.project.bean.MasClientAddressOutputBean;
import com.thbs.mis.project.bean.MasProjectOutputBean;
import com.thbs.mis.project.bean.ProjMasCompStatusBean;
import com.thbs.mis.project.bean.ProjMasCompTypeBean;
import com.thbs.mis.project.bean.ProjMasProjectClassificationBean;
import com.thbs.mis.project.bean.ProjMasProjectStatusBean;
import com.thbs.mis.project.bean.ProjectHistoryOutputBean;
import com.thbs.mis.project.bean.ProjectLevelUserAccessBean;
import com.thbs.mis.project.bean.ProjectNamesOutputBean;
import com.thbs.mis.project.bean.ProjectOutputBean;
import com.thbs.mis.project.bean.ProjectPercentCalculationOutputBean;
import com.thbs.mis.project.bean.ProjectUserAccessBean;
import com.thbs.mis.project.bean.ViewProjectInputBean;
import com.thbs.mis.project.bean.ViewProjectOutputBean;
import com.thbs.mis.project.bean.viewParticularProjectOutputBean;
import com.thbs.mis.project.bo.DatComponentBO;
import com.thbs.mis.project.bo.DatWorkorderBO;
import com.thbs.mis.project.bo.MasClientBO;
import com.thbs.mis.project.bo.MasProjectAuditBO;
import com.thbs.mis.project.bo.MasProjectBO;
import com.thbs.mis.project.bo.ProjDatProjectMappedToProjectCoordinatorBO;
import com.thbs.mis.project.bo.ProjMasCompStatusBO;
import com.thbs.mis.project.bo.ProjMasCompTypeBO;
import com.thbs.mis.project.bo.ProjMasProjectClassificationBO;
import com.thbs.mis.project.bo.ProjMasProjectStatusBO;
import com.thbs.mis.project.bo.ProjectLevelUserAccessGroupBO;
import com.thbs.mis.project.constants.ProjectConstants;
import com.thbs.mis.project.dao.DatComponentRepository;
import com.thbs.mis.project.dao.DatWorkorderRepository;
import com.thbs.mis.project.dao.MasClientRepository;
import com.thbs.mis.project.dao.MasProjectAuditRepository;
import com.thbs.mis.project.dao.MasProjectRepository;
import com.thbs.mis.project.dao.ProjDatProjectMappedToProjectCoordinatorRepository;
import com.thbs.mis.project.dao.ProjMasCompStatusRepository;
import com.thbs.mis.project.dao.ProjMasCompTypeRepository;
import com.thbs.mis.project.dao.ProjMasProjectClassificationRepository;
import com.thbs.mis.project.dao.ProjMasProjectStatusRepository;
import com.thbs.mis.project.dao.ProjectAdvanceSearchSpecification;
import com.thbs.mis.project.dao.ProjectLevelUserAccessGroupRepository;

@Service
public class ProjectService {

	private static final AppLog LOG = LogFactory.getLog(ProjectService.class);

	@Autowired
	private ProjMasProjectStatusRepository projMasProjectStatusRepository;

	// Added by Lalith kumar
	@Autowired
	private ProjMasCompTypeRepository projMasCompTypeRepository;

	@Autowired
	private ProjMasCompStatusRepository projMasCompStatusRepository;
	// Eoa by Lalith kumar

	@Autowired
	private EmployeePersonalDetailsRepository empPersonalRepository;

	@Autowired
	private DatWorkorderRepository datWorkorderRepos;

	@Autowired
	private ProjMasProjectStatusRepository projMasProjectStatusRepos;

	@Autowired
	private ProjMasProjectClassificationRepository projMasProjectClassificationRepos;

	@Autowired
	private GSRGoalService gsrGoalService;
	
	@Autowired
	private ProjectNotificationService projectNotificationService;
	
	@Autowired
	private GSRNotificationService gsrNotificationService;


	@Autowired
	private DatComponentRepository datComponentRepos;

	@Autowired
	private BusinessUnitRepository businessUnitRepos;

	List<MasProjectBO> masProjectBOList;

	@Autowired
	private MasProjectRepository masProjectRepos;

	@Autowired
	private ProjDatProjectMappedToProjectCoordinatorRepository projectMappedToCoordinatorRepos;

	@Autowired
	private ProjectLevelUserAccessGroupRepository projectLevelUserAccessGroupRepos;

	@Autowired
	private EmpDetailRepository empDetailRepos;

	@Autowired
	private MasDomainRepository masDomainRepos;

	@Autowired
	private MasVerticalRepository masVerticalRepos;

	@Autowired
	private MasAccountRepository masAccountRepos;

	@Autowired
	private MasClientRepository masClientRepos;

	@Autowired
	private MasRegionRepository masRegionRepos;

	@Autowired
	private MasProjectAuditRepository masProjectAuditRepos;

	@Autowired
	private DatEmployeeProfessionalRepository datEmployeeProfessionalRepos;

	@Autowired
	private ClientService clientService;
	
	@Value("${project.file.basepath}")
	private String projectDocsBasePath;

	@Value("${project.active.emp.status}")
	private byte activeEmpStatus;

	@Value("${project.create.update.key.roles.names}")
	private String projectCreateUpdateKeyRolesNames;

	@Value("${role.projectFinanceTeam}")
	private String financeRoleList;

	@Value("${project.user.acces.role.names}")
	private String projectUserAccessRoleNames;

	@Value("${project.status.active}")
	private Byte projectStatusActive;

	@Value("${project.ststus.future}")
	private Byte projectSattusFuture;

	@Value("${project.status.onHold}")
	private Byte projectStstusOnHold;

	@Value("${project.status.closed}")
	private Byte projectStatusclosed;

	@Value("${project.budget.user.role.access.allowed}")
	private String projectBudgetAllowedUserRoleNames;

	@Value("${project.budget.user.role.access.notAllowed}")
	private String projectBudgetNotAllowedUserRoleNames;

	@Value("${roleName.deliveryManager}")
	private String roleNameDeliveryManager;

	@Value("${roleName.domainManager}")
	private String roleNameDomainManager;

	@Value("${roleName.accountManager}")
	private String roleNameAccountManager;

	@Value("${project.coordinator.role.names}")
	private String projectCoordinatorRoleNames;

	@Value("${project.levelUserAcess.role.names}")
	private String projectLevelUserAcessRoleNames;
	
	
	@Value("${common.account.manager.level.names}")
	private String projectAccountManagerLevelNames;
	
	@Value("${common.domain.manager.level.names}")
	private String projectDomainManagerLevelNames;
	
	@Value("${common.delivery.manager.level.names}")
	private String projectDeliveryManagerLevelNames;
	
	@Value("${project.levelUserAcess.role.names}")
	private String projectUserAccessRoleKeyNames;
	
	@Value("${project.message.specification.from.to}")
	private String specificationFromTo;
	
	@Value("${project.history.pattern}")
	private String projectHistoryCommentsPattern;

	// Added by pratibha
	@Transactional
	public ProjectOutputBean createProject(CreateProjectBean createProjectBean)
			throws CommonCustomException, DataAccessException {
		ProjectOutputBean createProjectOutputBean = new ProjectOutputBean();

		try {
			LOG.startUsecase("Create project");
			MasProjectBO masProjectBO = new MasProjectBO();
			DatEmpDetailBO empDetail;
			MasDomainBO masDomainBO = new MasDomainBO();
			MasClientBO masClientBO = new MasClientBO();
			MasVerticalBO masVerticalBO = new MasVerticalBO();
			MasRegionBO masRegionBO = new MasRegionBO();
			DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
			MasBuUnitBO masBuUnitBO = new MasBuUnitBO();
			MasAccountBO masAccountBO = new MasAccountBO();
			ProjMasProjectClassificationBO projMasProjectClassificationBO = new ProjMasProjectClassificationBO();
			List<MasProjectBO> masProjectBOList = new ArrayList<MasProjectBO>();
			DatEmpProfessionalDetailBO datEmpProfessionalDetailBO = new DatEmpProfessionalDetailBO();
			List<String> deliveryManagerLevelList = Arrays.asList(projectDeliveryManagerLevelNames.split(","));
			List<String> domainManagerLevelList  = Arrays.asList(projectDomainManagerLevelNames.split(","));
			List<String> AccountManagerLevelList = Arrays.asList(projectAccountManagerLevelNames.split(","));
			
			try {
				masProjectBOList = masProjectRepos.findAll();
			} catch (Exception e1) {
				e1.printStackTrace();
				throw new DataAccessException(
						"Failed to fetch all the details of projects");
			}
			if (!masProjectBOList.isEmpty()) {
				for (MasProjectBO masProjectBO2 : masProjectBOList) {
					if (masProjectBO2.getProjectName().equalsIgnoreCase(
							createProjectBean.getProjectName())) {
						throw new CommonCustomException("Duplicate project name");
					 }
				}
			}
			try {
				masDomainBO = masDomainRepos.findByPkDomainId(createProjectBean
						.getDomainId());
			} catch (Exception e1) {
				throw new DataAccessException("Unable to get domain details");
			}
			if (masDomainBO == null) {
				throw new CommonCustomException("Invalid domainId "
						+ createProjectBean.getDomainId());
			}
			try {
				projMasProjectClassificationBO = projMasProjectClassificationRepos
						.findByPkClassificationId(createProjectBean
								.getProjectClassificationId());
			} catch (Exception e1) {
				throw new DataAccessException(
						"Unable to get projectClassification details");
			}
			if (projMasProjectClassificationBO == null) {
				throw new CommonCustomException("Invalid projectClassificationId "
						+ createProjectBean.getProjectClassificationId());
			}
			try {
				masAccountBO = masAccountRepos.findByPkAccountId(createProjectBean
						.getAccountId());
			} catch (Exception e1) {
				throw new DataAccessException("Unable to get account details");
			}
			if (masAccountBO == null) {
				throw new CommonCustomException("Invalid accountId "
						+ createProjectBean.getAccountId());
			}
			try {
				masClientBO = masClientRepos.findByPkClientId(createProjectBean
						.getClientId());
			} catch (Exception e1) {
				throw new DataAccessException("Unable to get client details");
			}
			if (masClientBO == null) {
				throw new CommonCustomException("Invalid clientId "
						+ createProjectBean.getClientId());
			}
			try {
				masVerticalBO = masVerticalRepos
						.findByPkVerticalId(createProjectBean.getVerticalId());
			} catch (Exception e1) {
				throw new DataAccessException(
						"Unable to get details from masVertical repository");
			}
			if (masVerticalBO == null) {
				throw new CommonCustomException("Invalid verticalId "
						+ createProjectBean.getVerticalId());
			}
			try {
				masRegionBO = masRegionRepos.findByPkRegionId(createProjectBean
						.getRegionId());
			} catch (Exception e1) {
				throw new DataAccessException("Unable to get region details");
			}
			if (masRegionBO == null) {
				throw new CommonCustomException("Invalid regionId "
						+ createProjectBean.getRegionId());
			}

			try {
				masBuUnitBO = businessUnitRepos.findByPkBuUnitId(createProjectBean
						.getBuUnitId());
			} catch (Exception e1) {
				throw new DataAccessException("Unable to get businessUnit details");
			}
			if (masBuUnitBO == null) {
				throw new CommonCustomException("Invalid businessUnitId "
						+ createProjectBean.getBuUnitId());
			}
			try {
				datEmpDetailBO = empDetailRepos.findByPkEmpId(createProjectBean
						.getDeliveryManagerId());
			} catch (Exception e) {
				throw new DataAccessException(
						"Unable to get deliveryManagerId Details");
			}
			
			try {
				datEmpProfessionalDetailBO = datEmployeeProfessionalRepos
						.findByFkMainEmpDetailId(createProjectBean
								.getDeliveryManagerId());
			} catch (Exception e1) {
				throw new DataAccessException(
						"Failed to fetch deliveryManager details");
			}	
			
			
			
			if (datEmpDetailBO == null)
				throw new CommonCustomException("Invalid deliveryManagerId");
			if (!datEmpDetailBO.getFkEmpMainStatus().equals(activeEmpStatus))
				throw new CommonCustomException("InActive deliveryManagerId.");
			if (!deliveryManagerLevelList.contains(datEmpProfessionalDetailBO.getMasEmpLevelBO().getEmpLevelName()))
				throw new CommonCustomException("Invalid deliveryManagerId");
			
			
			/*if (!datEmpDetailBO.getMasEmpRoleBO().getEmpRoleKeyName()
					.equalsIgnoreCase(roleNameDeliveryManager))
				throw new CommonCustomException("Invalid deliveryManagerId");
			*/
			if (datEmpProfessionalDetailBO.getFkEmpBuUnit().intValue() != createProjectBean
					.getBuUnitId().intValue()) {
				throw new CommonCustomException(
						"DeliveryManager does not belongs to the BU-"
								+ createProjectBean.getBuUnitId());
			}
			try {
				datEmpDetailBO = empDetailRepos.findByPkEmpId(createProjectBean
						.getDomainMgrId());
			} catch (Exception e) {
				throw new DataAccessException("Unable to get domainMgrId Details");
			}
			try {
				datEmpProfessionalDetailBO = datEmployeeProfessionalRepos
						.findByFkMainEmpDetailId(createProjectBean.getDomainMgrId());
			} catch (Exception e1) {
				throw new DataAccessException(
						"Failed to fetch domainManager details");
			}
			
			
			if (datEmpDetailBO == null)
				throw new CommonCustomException("Invalid domainMgrId");
			if (!datEmpDetailBO.getFkEmpMainStatus().equals(activeEmpStatus))
				throw new CommonCustomException("InActive domainMgrId.");
			if(!domainManagerLevelList.contains(datEmpProfessionalDetailBO.getMasEmpLevelBO().getEmpLevelName()))
			{
				throw new CommonCustomException("Invalid domainMgrId");
			}
			
		/*	if (!datEmpDetailBO.getMasEmpRoleBO().getEmpRoleKeyName()
					.equalsIgnoreCase(roleNameDomainManager))
				throw new CommonCustomException("Invalid domainMgrId");
		*/	
			if (datEmpProfessionalDetailBO.getFkEmpBuUnit().intValue() != createProjectBean
					.getBuUnitId().intValue()) {
				throw new CommonCustomException(
						"DomainManager does not belongs to the BU-"
								+ createProjectBean.getBuUnitId());
			}

			try {
				datEmpDetailBO = empDetailRepos.findByPkEmpId(createProjectBean
						.getAccountMgrId());
			} catch (Exception e) {
				throw new DataAccessException("Unable to get accountMgrId Details");
			}
			try {
				datEmpProfessionalDetailBO = datEmployeeProfessionalRepos
						.findByFkMainEmpDetailId(createProjectBean
								.getAccountMgrId());
			} catch (Exception e1) {
				throw new DataAccessException(
						"Failed to fetch accountManager details");
			}			
			
			if (datEmpDetailBO == null)
				throw new CommonCustomException("Invalid accountMgrId");
			if (!datEmpDetailBO.getFkEmpMainStatus().equals(activeEmpStatus))
				throw new CommonCustomException("InActive accountMgrId.");
			if(!AccountManagerLevelList.contains(datEmpProfessionalDetailBO.getMasEmpLevelBO().getEmpLevelName()))
				throw new CommonCustomException("Invalid accountMgrId");
			
			/*if (!datEmpDetailBO.getMasEmpRoleBO().getEmpRoleKeyName()
					.equalsIgnoreCase(roleNameAccountManager))
				throw new CommonCustomException("Invalid accountMgrId");
			*/
			if (datEmpProfessionalDetailBO.getFkEmpBuUnit().intValue() != createProjectBean
					.getBuUnitId().intValue()) {
				throw new CommonCustomException(
						"AccountManager does not belongs to the BU-"
								+ createProjectBean.getBuUnitId());
			}

			try {
				empDetail = empDetailRepos.findByPkEmpId(createProjectBean
						.getCreatedBy());
			} catch (Exception e) {
				throw new DataAccessException(
						"Unable to get Logged in User Id Details");
			}
			if (empDetail == null)
				throw new CommonCustomException("Invalid Logged In Emp Id");
			if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
				throw new CommonCustomException(
						"Logged In Employee Id is not a Active Employee.");
			else {
				boolean doesRoleExists = Arrays
						.asList(projectCreateUpdateKeyRolesNames.split(","))
						.stream()
						.anyMatch(
								status -> status.equals(empDetail.getMasEmpRoleBO()
										.getEmpRoleKeyName()));
				if (!doesRoleExists)
					throw new CommonCustomException(
							"You are not the authorized person to create project");
				else {
					if (createProjectBean.getProjectEndDate().before(
							createProjectBean.getProjectStartDate())) {
						throw new CommonCustomException(
								"projectStartDate should be lesser than projectEndDate");
					}
					masProjectBO.setProjectName(createProjectBean.getProjectName());
					masProjectBO.setProjectStartDate(createProjectBean
							.getProjectStartDate());
					masProjectBO.setProjectEndDate(createProjectBean
							.getProjectEndDate());
					masProjectBO.setProjectDescription(createProjectBean
							.getProjectDescription());
					masProjectBO.setFkDeliveryMgrId(createProjectBean
							.getDeliveryManagerId());
					masProjectBO.setFkBuUnitId(createProjectBean.getBuUnitId());
					masProjectBO.setFkDomainId(createProjectBean.getDomainId());
					masProjectBO.setFkDomainMgrId(createProjectBean
							.getDomainMgrId());
					if (new Date().after(createProjectBean.getProjectStartDate())
							&& new Date().before(createProjectBean
									.getProjectEndDate()))
						masProjectBO.setProjectStatusId(projectStatusActive);

					else if (new Date().before(createProjectBean
							.getProjectStartDate()))
						masProjectBO.setProjectStatusId(projectSattusFuture);
					else
						throw new CommonCustomException(
								"Invalid project startDate and endDate");

					masProjectBO.setProjectType(createProjectBean.getProjectType());
					masProjectBO.setFkAccountId(createProjectBean.getAccountId());
					masProjectBO.setFkAccountMgrId(createProjectBean
							.getAccountMgrId());
					masProjectBO.setProjectBillableFlag(createProjectBean
							.getProjectBillableFlag());
					masProjectBO.setCreatedBy(createProjectBean.getCreatedBy());
					masProjectBO.setFkVerticalId(createProjectBean.getVerticalId());
					masProjectBO.setFkClientId(createProjectBean.getClientId());
					masProjectBO.setCreatedOn(new Date());
					masProjectBO.setIsHavingAggregatePo(createProjectBean
							.getIsHavingAggregatePo());
					masProjectBO.setFkClassificationId(createProjectBean
							.getProjectClassificationId());
					masProjectBO.setIsHavingAggregatePo("YES");
					masProjectBO.setFkRegionId(createProjectBean.getRegionId());
					if (createProjectBean.getProjectCoordinator().length > 0
						//	&& createProjectBean.getProjectLevelUserAccess().length > 0
						) {
						int userLength = createProjectBean.getProjectCoordinator().length;
						Set<Integer> userSet = new HashSet<Integer>();
						for (int index = 0; index < createProjectBean
								.getProjectCoordinator().length; index++) {
							userSet.add(createProjectBean.getProjectCoordinator()[index]);
						}
						if (userLength != userSet.size()) {
							throw new CommonCustomException(
									"Duplicate project co-ordinator");
						}
						int userLength1 = createProjectBean
								.getProjectLevelUserAccess().length;
						Set<Integer> userSet1 = new HashSet<Integer>();
						for (int index = 0; index < createProjectBean
								.getProjectLevelUserAccess().length; index++) {
							userSet1.add(createProjectBean
									.getProjectLevelUserAccess()[index]);
						}
						if (userLength1 != userSet1.size()) {
							throw new CommonCustomException(
									"Duplicate projectLevelUserAccess");
						}

						for (int index = 0; index < createProjectBean
								.getProjectCoordinator().length; index++) {
							DatEmpDetailBO detailBo;
							try {
								detailBo = empDetailRepos
										.findByPkEmpId(createProjectBean
												.getProjectCoordinator()[index]);
							} catch (Exception e) {
								throw new DataAccessException(
										"Unable to get projectCoordinator Details");
							}
							if (detailBo == null)
								throw new CommonCustomException(
										"Invalid projectCoordinator");

							else if (!detailBo.getFkEmpMainStatus().equals(
									activeEmpStatus))
								throw new CommonCustomException(
										"InActive projectCoordinator");
							else {
								boolean doesRoleExis = Arrays
										.asList(projectCoordinatorRoleNames
												.split(","))
										.stream()
										.anyMatch(
												status -> status.equals(detailBo
														.getMasEmpRoleBO()
														.getEmpRoleKeyName()));
								if (!doesRoleExis)
									throw new CommonCustomException(
											"Invalid projectCoordinator "
													+ createProjectBean
															.getProjectCoordinator()[index]);
							}
						}

						for (int index = 0; index < createProjectBean
								.getProjectLevelUserAccess().length; index++) {
							DatEmpDetailBO detailBo;
							try {
								detailBo = empDetailRepos
										.findByPkEmpId(createProjectBean
												.getProjectLevelUserAccess()[index]);
							} catch (Exception e) {
								throw new DataAccessException(
										"Unable to get projectLevelUserAccess Details");
							}
							if (detailBo == null)
								throw new CommonCustomException(
										"Invalid projectLevelUserAccess");

							else if (!detailBo.getFkEmpMainStatus().equals(
									activeEmpStatus))
								throw new CommonCustomException(
										"InActive projectLevelUserAccess");
							else {
								boolean doesRoleExis = Arrays
										.asList(projectLevelUserAcessRoleNames
												.split(","))
										.stream()
										.anyMatch(
												status -> status.equals(detailBo
														.getMasEmpRoleBO()
														.getEmpRoleKeyName()));
								if (!doesRoleExis)
									throw new CommonCustomException(
											"Invalid projectLevelUserAccess "
													+ createProjectBean
															.getProjectLevelUserAccess()[index]);
							}
						}
						System.out.println("................."+masProjectBO.toString());
						try {
							masProjectBO = masProjectRepos.save(masProjectBO);
						} catch (Exception e) {
							//e.printStackTrace();
							throw new DataAccessException(
									"Error occured while saving project");
						}
						for (int index = 0; index < createProjectBean
								.getProjectCoordinator().length; index++) {
							ProjDatProjectMappedToProjectCoordinatorBO mappedObject = new ProjDatProjectMappedToProjectCoordinatorBO();
							mappedObject.setFkProjectId(masProjectBO
									.getPkProjectId());
							mappedObject
									.setFkProjectCoordinatorId(createProjectBean
											.getProjectCoordinator()[index]);
							try {
								projectMappedToCoordinatorRepos.save(mappedObject);
							} catch (Exception e) {
								throw new DataAccessException(
										"Error occured while saving data to projectMappedToCoordinatorRepository");
							}
						}
						for (int index = 0; index < createProjectBean
								.getProjectLevelUserAccess().length; index++) {
							ProjectLevelUserAccessGroupBO userAccessGroup = new ProjectLevelUserAccessGroupBO();
							userAccessGroup.setFkProjectId(masProjectBO
									.getPkProjectId());
							userAccessGroup.setFkUserId(createProjectBean
									.getProjectLevelUserAccess()[index]);
							try {
								projectLevelUserAccessGroupRepos
										.save(userAccessGroup);
							} catch (Exception e) {
								throw new DataAccessException(
										"Error occured while saving data to projectLevelUserAccessGroupRepository");
							}
						}
						createProjectOutputBean.setProjectId(masProjectBO
								.getPkProjectId());
						createProjectOutputBean.setProjectName(masProjectBO
								.getProjectName());
						createProjectOutputBean.setDeliveryManagerId(gsrGoalService
								.getEmpNameByEmpId(masProjectBO.getFkDeliveryMgrId()
										.intValue())
								+ '-' + masProjectBO.getFkDeliveryMgrId());
						try{
							projectNotificationService.sendProjectCreatedNotificationToDMDLPC(masProjectBO);
						}catch(Exception e) {
							throw new CommonCustomException(e.getMessage());
						}
					} else
						throw new CommonCustomException("Project Coordinator Details shouldn't be null.");
				}
			}
			LOG.endUsecase("Create project");
		} catch (Exception e) {
			//e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		return createProjectOutputBean;
	}

	public ProjectOutputBean updateProject(CreateProjectBean updateProjectBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Create project");
		MasProjectBO masProjectBO = new MasProjectBO();
		DatEmpDetailBO empDetail;
		ProjectOutputBean updateProjectOutputBean = new ProjectOutputBean();
		MasDomainBO masDomainBO = new MasDomainBO();
		MasClientBO masClientBO = new MasClientBO();
		MasVerticalBO masVerticalBO = new MasVerticalBO();
		MasRegionBO masRegionBO = new MasRegionBO();
		DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
		MasBuUnitBO masBuUnitBO = new MasBuUnitBO();
		MasAccountBO masAccountBO = new MasAccountBO();
		ProjMasProjectClassificationBO projMasProjectClassificationBO = new ProjMasProjectClassificationBO();
		List<ProjDatProjectMappedToProjectCoordinatorBO> projDatProjectMappedToProjectCoordinatorBOList = new ArrayList<ProjDatProjectMappedToProjectCoordinatorBO>();
		List<ProjectLevelUserAccessGroupBO> projectLevelUserAccessGroupBOList = new ArrayList<ProjectLevelUserAccessGroupBO>();
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBO = new DatEmpProfessionalDetailBO();
		List<String> deliveryManagerLevelList = Arrays.asList(projectDeliveryManagerLevelNames.split(","));
		List<String> domainManagerLevelList  = Arrays.asList(projectDomainManagerLevelNames.split(","));
		List<String> AccountManagerLevelList = Arrays.asList(projectAccountManagerLevelNames.split(","));
		List<String> PcList = Arrays.asList(projectCreateUpdateKeyRolesNames.split(","));
		Set<Integer> userSet1 = null;
		Set<Integer> userSet = null;
		StringBuffer updateComment = new StringBuffer();

		Iterable<ProjDatProjectMappedToProjectCoordinatorBO> projDatProjectMappedToProjectCoordinatorUpdated;
		Iterable<ProjectLevelUserAccessGroupBO> projectlevelUserAccessUpdate;
		try {
			masProjectBO = masProjectRepos.findByPkProjectId(updateProjectBean
					.getProjectId());
		} catch (Exception e1) {
			throw new DataAccessException("Failed to fetch details of project");
		}
		if (masProjectBO == null) {
			throw new CommonCustomException("Invalid project");
		}

		try {
			empDetail = empDetailRepos.findByPkEmpId(updateProjectBean
					.getUpdatedBy());
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get Logged in User Id Details");
		}
		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException(
					"Logged In Employee Id is not a Active Employee.");
		else {
			boolean doesRoleExists = Arrays
					.asList(projectCreateUpdateKeyRolesNames.split(","))
					.stream()
					.anyMatch(
							status -> status.equals(empDetail.getMasEmpRoleBO()
									.getEmpRoleKeyName()));
			if (!doesRoleExists)
				throw new CommonCustomException(
						"You are not the authorized person to update project");
			else {
				if (updateProjectBean.getProjectEndDate() != null
						&& updateProjectBean.getProjectStartDate() != null) {
					if (updateProjectBean.getProjectEndDate().before(
							updateProjectBean.getProjectStartDate())) {
						throw new CommonCustomException(
								"projectStartDate should be lesser than projectEndDate");
					}
					if (updateProjectBean.getProjectEndDate().before(
									new Date())) {
						throw new CommonCustomException(
								"Project endDate can be only updated to future dates");
					}
					/*if (updateProjectBean.getProjectStartDate().equals(
							masProjectBO.getProjectStartDate())) {
				throw new CommonCustomException(
						"You can't update project start date");
					}*/
					
					if(!masProjectBO.getProjectName().equals(updateProjectBean.getProjectName()))
						updateComment.append(MessageFormat.format(specificationFromTo,"Project name"
							,masProjectBO.getProjectName(),updateProjectBean.getProjectName()));
					

					if(!updateProjectBean.getAccountMgrId().equals(masProjectBO.getFkAccountMgrId()))
						updateComment.append(MessageFormat.format(specificationFromTo,"Account Manager"
								,gsrNotificationService.getEmployeeNameByEmpId(masProjectBO.getFkAccountMgrId())
								,gsrNotificationService.getEmployeeNameByEmpId(updateProjectBean.getAccountMgrId())));
					

					if(!updateProjectBean.getDeliveryManagerId().equals(masProjectBO.getFkDeliveryMgrId()))
						updateComment.append(MessageFormat.format(specificationFromTo,"Delivery Manager"
								,gsrNotificationService.getEmployeeNameByEmpId(masProjectBO.getFkDeliveryMgrId())
								,gsrNotificationService.getEmployeeNameByEmpId(updateProjectBean.getDeliveryManagerId())));
					
					if(!updateProjectBean.getDomainMgrId().equals(masProjectBO.getFkDomainMgrId()))
						updateComment.append(MessageFormat.format(specificationFromTo,"Domain Manager"
								,gsrNotificationService.getEmployeeNameByEmpId(masProjectBO.getFkDomainMgrId())
								,gsrNotificationService.getEmployeeNameByEmpId(updateProjectBean.getDomainMgrId())));
					
					if(!updateProjectBean.getIsHavingAggregatePo().equals(masProjectBO.getIsHavingAggregatePo()))
						updateComment.append(MessageFormat.format(specificationFromTo,"AggregatePo"
								,masProjectBO.getIsHavingAggregatePo(),updateProjectBean.getIsHavingAggregatePo()));
					
					if(!updateProjectBean.getProjectDescription().equals(masProjectBO.getProjectDescription()))
						updateComment.append(MessageFormat.format(specificationFromTo,"Project description"
								,masProjectBO.getProjectDescription(),updateProjectBean.getProjectDescription()));
				
					//Getting only Date excluding TimeStamp for checking date
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		        	String formattedNewStartDate = formatter.format(updateProjectBean.getProjectStartDate());
		        	String formattedOldStartDate = formatter.format(masProjectBO.getProjectStartDate());
		        	String formattedNewEndDate = formatter.format(updateProjectBean.getProjectEndDate());
		        	String formattedOldEndDate = formatter.format(masProjectBO.getProjectEndDate());

									
					if(!formattedNewEndDate.equals(formattedOldEndDate))
						updateComment.append(MessageFormat.format(specificationFromTo,"Project EndDate"
								,masProjectBO.getProjectEndDate(),updateProjectBean.getProjectEndDate()));
					
					if(!formattedNewStartDate.equals(formattedOldStartDate))
						updateComment.append(MessageFormat.format(specificationFromTo,"Project StartDate"
								,masProjectBO.getProjectStartDate(),updateProjectBean.getProjectStartDate()));
					
					
					masProjectBO.setProjectStartDate(updateProjectBean
							.getProjectStartDate());
					masProjectBO.setProjectEndDate(updateProjectBean
							.getProjectEndDate());
					if (new Date().after(updateProjectBean
							.getProjectStartDate())
							&& new Date().before(updateProjectBean
									.getProjectEndDate()))
						masProjectBO.setProjectStatusId(projectStatusActive);
					else if (new Date().before(updateProjectBean
							.getProjectStartDate()))
						masProjectBO.setProjectStatusId(projectSattusFuture);
					else
						throw new CommonCustomException(
								"Invalid project startDate and endDate");

				}
				masProjectBO.setPkProjectId(updateProjectBean.getProjectId());
				if (updateProjectBean.getProjectDescription() != null)
					masProjectBO.setProjectDescription(updateProjectBean
							.getProjectDescription());
				if (updateProjectBean.getProjectType() != null)
					masProjectBO.setProjectType(updateProjectBean
							.getProjectType());
				if (updateProjectBean.getProjectBillableFlag() != null)
				{
					if((updateProjectBean.getProjectBillableFlag().equals("BILLABLE")&&masProjectBO.getProjectBillableFlag().equals("INTERNAL")))
					{
					masProjectBO.setProjectBillableFlag(updateProjectBean
							.getProjectBillableFlag());
					updateComment.append(MessageFormat.format(specificationFromTo,"ProjectBillable flag"
							,masProjectBO.getProjectBillableFlag(),updateProjectBean.getProjectBillableFlag()));
				
					}
					/*else
						throw new CommonCustomException("Cannot change Billable to Internal");*/
				}
				masProjectBO.setUpdatedBy(updateProjectBean.getUpdatedBy());
				masProjectBO.setUpdatedOn(new Date());
				masProjectBO.setCreatedBy(masProjectBO.getCreatedBy());
				masProjectBO.setCreatedOn(masProjectBO.getCreatedOn());
				
				if(updateProjectBean.getProjectName()!=null)
				{
					masProjectBO.setProjectName(updateProjectBean.getProjectName());
				}

				if (updateProjectBean.getDomainId() != null) {
					try {
						masDomainBO = masDomainRepos
								.findByPkDomainId(updateProjectBean
										.getDomainId());
					} catch (Exception e1) {
						throw new DataAccessException(
								"Unable to get domain details");
					}
					if (masDomainBO == null) {
						throw new CommonCustomException("Invalid domainId "
								+ updateProjectBean.getDomainId());
					}
					

					if(!updateProjectBean.getDomainId().equals(masProjectBO.getFkDomainId()))
						updateComment.append(MessageFormat.format(specificationFromTo,"Domain"
								,masProjectBO.getMasDomain().getDomainName(),masDomainBO.getDomainName()));
					
					masProjectBO.setFkDomainId(updateProjectBean.getDomainId());
				}
				if (updateProjectBean.getIsHavingAggregatePo() != null) {
					masProjectBO.setIsHavingAggregatePo(updateProjectBean
							.getIsHavingAggregatePo());
				}
				if (updateProjectBean.getProjectClassificationId() != null) {
					try {
						projMasProjectClassificationBO = projMasProjectClassificationRepos
								.findByPkClassificationId(updateProjectBean
										.getProjectClassificationId());
					} catch (Exception e1) {
						throw new DataAccessException(
								"Unable to get projectClassification details");
					}
					if (projMasProjectClassificationBO == null) {
						throw new CommonCustomException(
								"Invalid projectClassificationId "
										+ updateProjectBean
												.getProjectClassificationId());
					}
					
					if(!updateProjectBean.getProjectClassificationId().equals(masProjectBO.getFkClassificationId()))
						updateComment.append(MessageFormat.format(specificationFromTo,"Classification Id"
								,masProjectBO.getProjMasProjectClassification().getClassificationName(),
								projMasProjectClassificationBO.getClassificationName()));
	
					masProjectBO.setFkClassificationId(updateProjectBean
							.getProjectClassificationId());
				}

				if (updateProjectBean.getAccountId() != null) {
					try {
						masAccountBO = masAccountRepos
								.findByPkAccountId(updateProjectBean
										.getAccountId());
					} catch (Exception e1) {
						throw new DataAccessException(
								"Unable to get account details");
					}
					if (masAccountBO == null) {
						throw new CommonCustomException("Invalid accountId "
								+ updateProjectBean.getAccountId());
					}
					
					//Adding message to update comment string for changing Account
					if(!updateProjectBean.getAccountId().equals(masProjectBO.getFkAccountId()))
						updateComment.append(MessageFormat.format(specificationFromTo,"Account Id"
								,masProjectBO.getMasAccount().getAccountName()
								,masAccountBO.getAccountName()));
				
					masProjectBO.setFkAccountId(updateProjectBean
							.getAccountId());
				}

				if (updateProjectBean.getClientId() != null) {
					try {
						masClientBO = masClientRepos
								.findByPkClientId(updateProjectBean
										.getClientId());
					} catch (Exception e1) {
						throw new DataAccessException(
								"Unable to get client details");
					}
					if (masClientBO == null) {
						throw new CommonCustomException("Invalid clientId "
								+ updateProjectBean.getClientId());
					}
					masProjectBO.setFkClientId(updateProjectBean.getClientId());
				}

				if (updateProjectBean.getVerticalId() != null) {
					try {
						masVerticalBO = masVerticalRepos
								.findByPkVerticalId(updateProjectBean
										.getVerticalId());
					} catch (Exception e1) {
						throw new DataAccessException(
								"Unable to get details from masVertical repository");
					}
					if (masVerticalBO == null) {
						throw new CommonCustomException("Invalid verticalId "
								+ updateProjectBean.getVerticalId());
					}
					

					if(!updateProjectBean.getVerticalId().equals(masProjectBO.getFkVerticalId()))
						updateComment.append(MessageFormat.format(specificationFromTo,"Vertical Id"
								,masProjectBO.getMasVertical().getVerticalName(),masVerticalBO.getVerticalName()));
				
					masProjectBO.setFkVerticalId(updateProjectBean
							.getVerticalId());
				}

				if (updateProjectBean.getRegionId() != null) {
					try {
						masRegionBO = masRegionRepos
								.findByPkRegionId(updateProjectBean
										.getRegionId());
					} catch (Exception e1) {
						throw new DataAccessException(
								"Unable to get region details");
					}
					if (masRegionBO == null) {
						throw new CommonCustomException("Invalid regionId "
								+ updateProjectBean.getRegionId());
					}
					

					if(!updateProjectBean.getRegionId().equals(masProjectBO.getFkRegionId()))
						updateComment.append(MessageFormat.format(specificationFromTo,"Region Id"
								,masProjectBO.getMasRegion().getRegionName(),masRegionBO.getRegionName()));
					
					
					masProjectBO.setFkRegionId(updateProjectBean.getRegionId());
				}

				if (updateProjectBean.getBuUnitId() != null) {
					try {
						masBuUnitBO = businessUnitRepos
								.findByPkBuUnitId(updateProjectBean
										.getBuUnitId());
					} catch (Exception e1) {
						throw new DataAccessException(
								"Unable to get businessUnit details");
					}
					if (masBuUnitBO == null) {
						throw new CommonCustomException(
								"Invalid businessUnitId "
										+ updateProjectBean.getBuUnitId());
					}
					
					//setting comments for Bu unit update
					if(!updateProjectBean.getBuUnitId().equals(masProjectBO.getFkBuUnitId()))
						updateComment.append(MessageFormat.format(specificationFromTo,"BuUnit"
								,masProjectBO.getMasBuUnit().getBuUnitName(),masBuUnitBO.getBuUnitName()));
					
					
					masProjectBO.setFkBuUnitId(updateProjectBean.getBuUnitId());
				}

				if (updateProjectBean.getDeliveryManagerId() != null) {
					try {
						datEmpDetailBO = empDetailRepos
								.findByPkEmpId(updateProjectBean
										.getDeliveryManagerId());
					} catch (Exception e) {
						throw new DataAccessException(
								"Unable to get deliveryManagerId Details");
					}
					try {
						datEmpProfessionalDetailBO = datEmployeeProfessionalRepos
								.findByFkMainEmpDetailId(updateProjectBean
										.getDeliveryManagerId());
					} catch (Exception e1) {
						throw new DataAccessException(
								"Failed to fetch deliveryManager details");
					}
					
					if (datEmpDetailBO == null)
						throw new CommonCustomException(
								"Invalid deliveryManagerId");
					if (!datEmpDetailBO.getFkEmpMainStatus().equals(
							activeEmpStatus))
						throw new CommonCustomException(
								"InActive deliveryManagerId.");
					if (!deliveryManagerLevelList.contains(datEmpProfessionalDetailBO.getMasEmpLevelBO().getEmpLevelName()))
						throw new CommonCustomException("Invalid deliveryManagerId");	
					if (datEmpProfessionalDetailBO.getFkEmpBuUnit().intValue() != updateProjectBean
							.getBuUnitId().intValue()) {
						throw new CommonCustomException(
								"DeliveryManager does not belongs to the BU-"
										+ updateProjectBean.getBuUnitId());
					}
					masProjectBO.setFkDeliveryMgrId(updateProjectBean
							.getDeliveryManagerId());
				}

				if (updateProjectBean.getDomainMgrId() != null) {
					try {
						datEmpDetailBO = empDetailRepos
								.findByPkEmpId(updateProjectBean
										.getDomainMgrId());
					} catch (Exception e) {
						throw new DataAccessException(
								"Unable to get domainMgrId Details");
					}
					
					try {
						datEmpProfessionalDetailBO = datEmployeeProfessionalRepos
								.findByFkMainEmpDetailId(updateProjectBean
										.getDomainMgrId());
					} catch (Exception e1) {
						throw new DataAccessException(
								"Failed to fetch domainManager details");
					}
					if (datEmpDetailBO == null)
						throw new CommonCustomException("Invalid domainMgrId");
					if (!datEmpDetailBO.getFkEmpMainStatus().equals(
							activeEmpStatus))
						throw new CommonCustomException("InActive domainMgrId.");
					if(!domainManagerLevelList.contains(datEmpProfessionalDetailBO.getMasEmpLevelBO().getEmpLevelName()))
					{
						throw new CommonCustomException("Invalid domainMgrId");
					}					
					if (datEmpProfessionalDetailBO.getFkEmpBuUnit().intValue() != updateProjectBean
							.getBuUnitId().intValue()) {
						throw new CommonCustomException(
								"DomainManager does not belongs to the BU-"
										+ updateProjectBean.getBuUnitId());
					}
					masProjectBO.setFkDomainMgrId(updateProjectBean
							.getDomainMgrId());
				}

				if (updateProjectBean.getAccountMgrId() != null) {
					try {
						datEmpDetailBO = empDetailRepos
								.findByPkEmpId(updateProjectBean
										.getAccountMgrId());
					} catch (Exception e) {
						throw new DataAccessException(
								"Unable to get accountMgrId Details");
					}
					
					try {
						datEmpProfessionalDetailBO = datEmployeeProfessionalRepos
								.findByFkMainEmpDetailId(updateProjectBean
										.getAccountMgrId());
					} catch (Exception e1) {
						throw new DataAccessException(
								"Failed to fetch accountManager details");
					}
					
					if (datEmpDetailBO == null)
						throw new CommonCustomException("Invalid accountMgrId");
					if (!datEmpDetailBO.getFkEmpMainStatus().equals(
							activeEmpStatus))
						throw new CommonCustomException(
								"InActive accountMgrId.");
					if(!AccountManagerLevelList.contains(datEmpProfessionalDetailBO.getMasEmpLevelBO().getEmpLevelName()))
						throw new CommonCustomException("Invalid accountMgrId");
					if (datEmpProfessionalDetailBO.getFkEmpBuUnit().intValue() != updateProjectBean
							.getBuUnitId().intValue()) {
						throw new CommonCustomException(
								"AccountManager does not belongs to the BU-"
										+ updateProjectBean.getBuUnitId());
					}
					masProjectBO.setFkAccountMgrId(updateProjectBean
							.getAccountMgrId());
				}

				if (updateProjectBean.getProjectCoordinator() != null) {
					int userLength = updateProjectBean.getProjectCoordinator().length;
					 userSet = new HashSet<Integer>();
					for (int index = 0; index < updateProjectBean
							.getProjectCoordinator().length; index++) {
						userSet.add(updateProjectBean.getProjectCoordinator()[index]);
					}
					if (userLength != userSet.size()) {
						throw new CommonCustomException(
								"Duplicate project co-ordinator.");
					}
				}

				if (updateProjectBean.getProjectLevelUserAccess() != null) {
					int userLength1 = updateProjectBean
							.getProjectLevelUserAccess().length;
					 userSet1 = new HashSet<Integer>();
					for (int index = 0; index < updateProjectBean
							.getProjectLevelUserAccess().length; index++) {
						userSet1.add(updateProjectBean
								.getProjectLevelUserAccess()[index]);
					}
					if (userLength1 != userSet1.size()) {
						throw new CommonCustomException(
								"Duplicate projectLevelUserAccess");
					}
				}
				Set<Integer> userAccessList;
				Set<Integer> updatedUserAccessList = new HashSet<Integer>();
				List<DatEmpDetailBO> datempDetailBoListUser = null;
				List<DatEmpDetailBO> datempDetailBoListPc = null;
				
				try{
					
					userAccessList = projectLevelUserAccessGroupRepos.getAllUserAccessByProjectId(updateProjectBean.getProjectId());
					
				}catch(Exception e)
				{
					throw new DataAccessException("unable to update project..");
				}
				//updateComment.append("Added ");
				for(Integer i : userSet1)
				{
					if(!userAccessList.contains(i))
					{
						updatedUserAccessList.add(i);
						updateComment.append(gsrNotificationService.getEmployeeNameByEmpId(i)+"-"+i+",");

					}
				}
				if(!updatedUserAccessList.isEmpty())
					updateComment.append("were added to UserAccessList,");
			
				Set<Integer> pcMappedList;
				Set<Integer> updatedPcMappedList = new HashSet<Integer>();
				
				try{
					pcMappedList = projectMappedToCoordinatorRepos.getByProjectId(updateProjectBean.getProjectId());	
				}catch(Exception e)
				{
					throw new DataAccessException("unable to update project");
				}
				//updateComment.append("Added ");
				for(Integer i : userSet)
				{
					if(!pcMappedList.contains(i))
					{
						updatedPcMappedList.add(i);	
						updateComment.append(gsrNotificationService.getEmployeeNameByEmpId(i)+"-"+i+",");
					}
				}
				if(!updatedPcMappedList.isEmpty())
				updateComment.append("were added to ProjectCoordinatorsList,");
				
				LOG.info("updatedPcMappedList..."+updatedPcMappedList);
				LOG.info("updatedUserAccessList..."+updatedUserAccessList);
				
				if(!updatedUserAccessList.isEmpty())
				{
				
				try{
					datempDetailBoListUser= empDetailRepos.getEmpList(updatedUserAccessList);
				}catch(Exception e)
				{
				//	e.printStackTrace();
					throw new DataAccessException("unable to update project");
				}
				
				if(datempDetailBoListUser!=null && !(datempDetailBoListUser.isEmpty()))
				{
				for(DatEmpDetailBO emp : datempDetailBoListUser)
				{
					if(emp.getFkEmpMainStatus() == ProjectConstants.Active)
					{
						if(PcList.contains(emp.getMasEmpRoleBO().getEmpRoleKeyName()))
						{
							ProjectLevelUserAccessGroupBO user = new ProjectLevelUserAccessGroupBO();
							user.setFkProjectId(updateProjectBean.getProjectId());
							user.setFkUserId(emp.getPkEmpId());						
							
							projectLevelUserAccessGroupBOList.add(user);
							
						}else
							throw new CommonCustomException("EmpId : "+emp.getPkEmpId()+" cannot be added to project coordinator list");
						
					}else
					{
						throw new CommonCustomException("EmpId : "+emp.getPkEmpId()+" is Inactive");
					}					
				}
				try{
					projectlevelUserAccessUpdate = projectLevelUserAccessGroupRepos.save(projectLevelUserAccessGroupBOList);
				}catch(Exception e)
				{
					throw new CommonCustomException("unable to update project");
				}
				}
				}
				LOG.info("datempDetailBoListUser...."+datempDetailBoListUser);
				if(!updatedPcMappedList.isEmpty())
				{
				try{
					datempDetailBoListPc= empDetailRepos.getEmpList(updatedPcMappedList);
				}catch(Exception e)
				{
					//e.printStackTrace();
					throw new DataAccessException("unable to update project");
				}
				LOG.info("datempDetailBoListPc...."+datempDetailBoListPc);
				
				if(datempDetailBoListPc!=null && !(datempDetailBoListPc.isEmpty()))
				{
				for(DatEmpDetailBO emp : datempDetailBoListPc)
				{
					if(emp.getFkEmpMainStatus() == ProjectConstants.Active)
					{
						if(PcList.contains(emp.getMasEmpRoleBO().getEmpRoleKeyName()))
						{
							ProjDatProjectMappedToProjectCoordinatorBO pc = new ProjDatProjectMappedToProjectCoordinatorBO();
							pc.setFkProjectId(updateProjectBean.getProjectId());
							pc.setFkProjectCoordinatorId(emp.getPkEmpId());						
							
							projDatProjectMappedToProjectCoordinatorBOList.add(pc);
							
						}else
							throw new CommonCustomException("EmpId : "+emp.getPkEmpId()+" cannot be added to project coordinator list");
						
					}else
					{
						throw new CommonCustomException("EmpId : "+emp.getPkEmpId()+" is Inactive");
					}					
				}
				try{
					projDatProjectMappedToProjectCoordinatorUpdated = projectMappedToCoordinatorRepos.save(projDatProjectMappedToProjectCoordinatorBOList);
				}catch(Exception e)
				{	
					throw new CommonCustomException("unable to update project");
				}}
				}
				}
			masProjectBO.setCommentsForUpdation(updateProjectBean.getCommentsForUpdation());
			int len = updateComment.length();
			masProjectBO.setProjHistory(new String(updateComment.deleteCharAt(len-1)));
				try {
					masProjectBO = masProjectRepos.save(masProjectBO);
				} catch (Exception e) {
					//e.printStackTrace();
					throw new DataAccessException("Error occured while saving project");
				}
				
				try{
				projectNotificationService.sendProjectUpdatedNotificationToDMDLPC(masProjectBO);
				}catch(Exception e)
				{
					throw new CommonCustomException(e.getMessage());
				}
				updateProjectOutputBean.setProjectId(masProjectBO
						.getPkProjectId());
				updateProjectOutputBean.setProjectName(masProjectBO
						.getProjectName());
				updateProjectOutputBean.setDeliveryManagerId(gsrGoalService
						.getEmpNameByEmpId(masProjectBO.getFkDeliveryMgrId()
								.intValue())
						+ '-' + masProjectBO.getFkDeliveryMgrId());
			}
		
		return updateProjectOutputBean;
	}

	public Page<ViewProjectOutputBean> searchProject(
			ViewProjectInputBean viewProjectInputBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Search project");
		Page<MasProjectBO> masProjectBOList = null;
		Page<ViewProjectOutputBean> finalPage;
		Specification<MasProjectBO> projectDetailsObj = null;
		List<DatWorkorderBO> datWorkorderBOList = new ArrayList<DatWorkorderBO>();
		List<ViewProjectOutputBean> viewProjectOutputBeanList = new ArrayList<ViewProjectOutputBean>();
		List<DatComponentBO> datComponentBOList = new ArrayList<DatComponentBO>();
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBO = new DatEmpProfessionalDetailBO();
		DatEmpDetailBO detailBo;
		ProjectPercentCalculationOutputBean projectPercentCalculationOutputBean;

		if (viewProjectInputBean.getFromDate() != null
				&& viewProjectInputBean.getToDate() != null) {
			if (viewProjectInputBean.getFromDate().after(
					viewProjectInputBean.getToDate())) {
				throw new CommonCustomException(
						"projectStartDate should be lesser than projectEndDate");
			}
		}
		try {
			detailBo = empDetailRepos.findByPkEmpId(viewProjectInputBean
					.getLoggedInUser());
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get projectCoordinator Details");
		}
		if (detailBo == null)
			throw new CommonCustomException("Invalid loggedInUser");

		else if (!detailBo.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException("InActive loggedInUser");
		else {
			boolean doesRoleExis = Arrays
					.asList(projectCoordinatorRoleNames.split(","))
					.stream()
					.anyMatch(
							status -> status.equals(detailBo.getMasEmpRoleBO()
									.getEmpRoleKeyName()));
			if (!doesRoleExis)
				throw new CommonCustomException(
						"You are not an authorizes person to view the project");
		}
		try {
			datEmpProfessionalDetailBO = datEmployeeProfessionalRepos
					.findByFkMainEmpDetailId(viewProjectInputBean
							.getLoggedInUser());
		} catch (Exception e2) {
			throw new DataAccessException(
					"Failed to fetch details of loggedInUser");
		}


		try {
			projectDetailsObj = ProjectAdvanceSearchSpecification
					.viewProjectDetails(viewProjectInputBean);
		} catch (Exception e1) {
			throw new DataAccessException(
					"Failed to get details of project details");
		}
		Pageable pageable = new PageRequest(
				viewProjectInputBean.getPageNumber(),
				viewProjectInputBean.getPageSize(),Sort.Direction.DESC, "createdOn");

		try {
			masProjectBOList = masProjectRepos.findAll(projectDetailsObj,
					pageable);
		} catch (Exception e) {
			//e.printStackTrace();
			
			  throw new DataAccessException(
			  "Failed to get all the details of project");
			 
		}
		if (masProjectBOList == null) {
			throw new CommonCustomException(
					"No data found for the requested input");
		}
		for (MasProjectBO masProjectBO : masProjectBOList) {

			// if (masProjectBO.getFkBuUnitId() == buId) {
			ViewProjectOutputBean viewProjectOutputBean = new ViewProjectOutputBean();
			viewProjectOutputBean.setProjectId(masProjectBO.getPkProjectId());
			viewProjectOutputBean.setProjectName(masProjectBO.getProjectName());
			viewProjectOutputBean.setRegionName(masProjectBO.getMasRegion()
					.getRegionName());
			viewProjectOutputBean.setRegionId(masProjectBO.getFkRegionId());
			viewProjectOutputBean.setDomainMangerName(gsrGoalService
					.getEmpNameByEmpId(masProjectBO.getFkDomainMgrId()
							.intValue()));
			viewProjectOutputBean.setDomainMangerId(masProjectBO
					.getFkDomainMgrId());
			viewProjectOutputBean.setProjectStatusName(masProjectBO
					.getProjMasProjectStatus().getProjectStatusName());
			viewProjectOutputBean.setProjectStatusId(masProjectBO
					.getProjectStatusId());
			viewProjectOutputBean.setClientName(masProjectBO.getMasClient()
					.getClientName());
			viewProjectOutputBean.setClientId(masProjectBO.getFkClientId());
			// viewProjectOutputBean.setCompletionOfProj("50%");
			try {
				datComponentBOList = datComponentRepos
						.numberOfComponents(masProjectBO.getPkProjectId());
			} catch (Exception e1) {
				throw new DataAccessException(
						"Failed to get details of componenets");
			}
			try {
				datWorkorderBOList = datWorkorderRepos
						.findByFkProjectId(masProjectBO.getPkProjectId());
			} catch (Exception e) {
				//e.printStackTrace();
			}
			viewProjectOutputBean.setNoOfWorkOrder(datWorkorderBOList.size());
			viewProjectOutputBean.setNoOfComponent(datComponentBOList.size());
try{
			try{
				projectPercentCalculationOutputBean = masProjectRepos.getProjectTriggeredAndProjectValue(masProjectBO.getPkProjectId());
				
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to get project details...!");
			}
			
			
			if(projectPercentCalculationOutputBean.getProjectValue()==null)
				projectPercentCalculationOutputBean.setProjectValue(new BigDecimal(0));
			else if( projectPercentCalculationOutputBean.getSumOfTriggeredValue()==null)
				projectPercentCalculationOutputBean.setSumOfTriggeredValue(new BigDecimal(0));
			else{
				LOG.info("ProjectCalculationbean..........!!!!!!"+projectPercentCalculationOutputBean);
			float completedPercent = (projectPercentCalculationOutputBean.getSumOfTriggeredValue()
					.floatValue()/(projectPercentCalculationOutputBean.getProjectValue().floatValue()))*100;
			LOG.info("completedPercent..............!!!!!" + completedPercent);
			LOG.info("percentage ..............@@@@@@@" + completedPercent);
			viewProjectOutputBean.setCompletionOfProj(completedPercent);
			}
}catch(Exception e)
{
	
}
			viewProjectOutputBeanList.add(viewProjectOutputBean);
		}
		// }
		finalPage = new PageImpl<ViewProjectOutputBean>(
				viewProjectOutputBeanList, pageable,
				viewProjectOutputBeanList.size());

		return finalPage;

	}

	public List<GetProjectsOutputBean> getProjects(Short clientId)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("get project details");
		List<GetProjectsOutputBean> getProjectsOutputBeanList = new ArrayList<GetProjectsOutputBean>();
		this.masProjectBOList = new ArrayList<MasProjectBO>();

		try {
			this.masProjectBOList = masProjectRepos.findByFkClientId(clientId);
		} catch (Exception e) {
			throw new DataAccessException("Failed to fetch details of project");
		}

		if (this.masProjectBOList.isEmpty()) {
			throw new CommonCustomException("No Data found");
		}

		for (MasProjectBO masProjectBO : this.masProjectBOList) {
			GetProjectsOutputBean getProjectsOutputBean = new GetProjectsOutputBean();
			getProjectsOutputBean.setProjectId(masProjectBO.getPkProjectId());
			getProjectsOutputBean.setProjectName(masProjectBO.getProjectName());
			getProjectsOutputBean.setDeliveryMangerId(gsrGoalService
					.getEmpNameByEmpId(masProjectBO.getFkDeliveryMgrId()
							.intValue())
					+ '-' + masProjectBO.getFkDeliveryMgrId());
			getProjectsOutputBeanList.add(getProjectsOutputBean);
		}
		LOG.endUsecase("get project details");
		return getProjectsOutputBeanList;
	}

	public viewParticularProjectOutputBean viewParticularProject(
			Integer projectId) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("view perticular project details");
		MasProjectBO masProjectBO = new MasProjectBO();
		viewParticularProjectOutputBean viewParticularProjectOutputBean = new viewParticularProjectOutputBean();
		List<DatComponentBO> datComponentBOList = new ArrayList<DatComponentBO>();
		List<DatWorkorderBO> datWorkorderBOList = new ArrayList<DatWorkorderBO>();
		ProjMasProjectStatusBO projMasProjectStatusBO = new ProjMasProjectStatusBO();
		ProjectPercentCalculationOutputBean projectPercentCalculationOutputBean;

		try {
			datWorkorderBOList = datWorkorderRepos.findByFkProjectId(projectId);
		} catch (Exception e1) {
			throw new DataAccessException("Failed to get details of workOrder");
		}

		try {
			datComponentBOList = datComponentRepos
					.numberOfComponents(projectId);
		} catch (Exception e1) {
			throw new DataAccessException(
					"Failed to get details of componenets");
		}

		try {
			masProjectBO = masProjectRepos.findByPkProjectId(projectId);
		} catch (Exception e) {
			throw new DataAccessException("Failed to fetch details of project");
		}

		if (masProjectBO == null) {
			throw new CommonCustomException("Invalid projectId");
		}

		try {
			projMasProjectStatusBO = projMasProjectStatusRepos
					.findByPkProjectStatusId(masProjectBO.getProjectStatusId());
		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to fetch details of project status");
		}
		try{
			 projectPercentCalculationOutputBean = masProjectRepos.getProjectTriggeredAndProjectValue(masProjectBO.getPkProjectId());
			
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to get project details...!");
		}
		viewParticularProjectOutputBean.setProjectId(masProjectBO
				.getPkProjectId());
		viewParticularProjectOutputBean.setProjectName(masProjectBO
				.getProjectName());
		viewParticularProjectOutputBean.setNoOfWorkOrder(datWorkorderBOList
				.size());
		viewParticularProjectOutputBean.setNoOfComponent(datComponentBOList
				.size());
		viewParticularProjectOutputBean.setProjectStatus(projMasProjectStatusBO
				.getProjectStatusName());
		viewParticularProjectOutputBean.setProjectFromDate(masProjectBO
				.getProjectStartDate());
		viewParticularProjectOutputBean.setProjectToDate(masProjectBO
				.getProjectEndDate());
		viewParticularProjectOutputBean.setClientName(masProjectBO
				.getMasClient().getClientName());
		viewParticularProjectOutputBean
				.setDomainOrDeliveryManger(gsrGoalService
						.getEmpNameByEmpId(masProjectBO.getFkDomainMgrId()
								.intValue())
						+ '-' + masProjectBO.getFkDomainMgrId());
		viewParticularProjectOutputBean.setLocation(masProjectBO.getMasRegion().getRegionName());
		
		LOG.endUsecase("view perticular project details");
		viewParticularProjectOutputBean.setDaysUntilDue(10);
		
		try{
			
		if(projectPercentCalculationOutputBean.getProjectValue()==null)
			projectPercentCalculationOutputBean.setProjectValue(new BigDecimal(0));
		else if( projectPercentCalculationOutputBean.getSumOfTriggeredValue()==null)
			projectPercentCalculationOutputBean.setSumOfTriggeredValue(new BigDecimal(0));
		else{
			LOG.info("ProjectCalculationbean..........!!!!!!"+projectPercentCalculationOutputBean);
		float completedPercent = (projectPercentCalculationOutputBean.getSumOfTriggeredValue()
				.floatValue()/(projectPercentCalculationOutputBean.getProjectValue().floatValue()))*100;
		LOG.info("completedPercent..............!!!!!" + completedPercent);
		LOG.info("percentage ..............@@@@@@@" + completedPercent);
		viewParticularProjectOutputBean.setCompletionOfProj(completedPercent);
		}
		}catch(Exception e)
		{
			LOG.info("completedPercent ::::"+viewParticularProjectOutputBean.getCompletionOfProj());
		}
		
		return viewParticularProjectOutputBean;
	}

	String message = "";

	public ProjectHistoryOutputBean getProjectHistory(Short clientId)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("get project history");
		ProjectHistoryOutputBean projectHistoryOutputBean = new ProjectHistoryOutputBean();
		List<MasProjectAuditBO> masProjectAuditBOList = new ArrayList<MasProjectAuditBO>();
		List<String> arrayOfList = new ArrayList<String>();
		// masProjectAuditBO = masProjectAuditRepos.
		masProjectAuditBOList = masProjectAuditRepos.historyDetails(clientId);
		if (masProjectAuditBOList.isEmpty())
			this.message = "project created";
		else {
			masProjectAuditBOList.forEach(obj -> {
				this.message = "";
				this.message = this.history(obj);
				arrayOfList.add(this.message);
			});
		}

		return projectHistoryOutputBean;
	}

	public String history(MasProjectAuditBO obj) {
		String historyValue = "";
		/*
		 * switch (obj.getProjectStatusId()) { case 1: break; case 2:
		 * 
		 * 
		 * }
		 */

		return historyValue;
	}

	// EOA by pratibha
	public List<ProjMasProjectStatusBean> getAllProjectStatus()
			throws DataAccessException {

		List<ProjMasProjectStatusBean> listProjectStatusbean = new ArrayList<ProjMasProjectStatusBean>();
		try {
			List<ProjMasProjectStatusBO> listProjectStatus = new ArrayList<ProjMasProjectStatusBO>();

			try {
				listProjectStatus = (List<ProjMasProjectStatusBO>) projMasProjectStatusRepository
						.findAll();
			} catch (Exception e) {
				throw new DataAccessException("Failed to fetch the details.");
			}
			if (!listProjectStatus.isEmpty()) {
				for (ProjMasProjectStatusBO projectStatusBo : listProjectStatus) {
					ProjMasProjectStatusBean projectStatusbean = new ProjMasProjectStatusBean();
					projectStatusbean.setPkProjectStatusId(projectStatusBo
							.getPkProjectStatusId());
					projectStatusbean.setProjectStatusName(projectStatusBo
							.getProjectStatusName());
					listProjectStatusbean.add(projectStatusbean);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		return listProjectStatusbean;
	}

	// Added by Lalith kumar
	public List<ProjMasCompTypeBean> getAllCompType()
			throws DataAccessException {

		List<ProjMasCompTypeBean> listProjMasCompTypeBean = new ArrayList<ProjMasCompTypeBean>();
		try {
			List<ProjMasCompTypeBO> listProjMasCompTypeBO = new ArrayList<ProjMasCompTypeBO>();
			try {

				// get all component types into list
				listProjMasCompTypeBO = (List<ProjMasCompTypeBO>) projMasCompTypeRepository
						.findAll();

			} catch (Exception e) {
				throw new DataAccessException(
						"Failed to retrive ComponentTypes from Database");
			}

			if (!listProjMasCompTypeBO.isEmpty()) {
				for (ProjMasCompTypeBO projMasCompTypeBO : listProjMasCompTypeBO) {
					ProjMasCompTypeBean projMasCompTypeBean = new ProjMasCompTypeBean();

					projMasCompTypeBean.setComponentTypeId(projMasCompTypeBO
							.getPkComponentTypeId());
					projMasCompTypeBean.setComponentTypeName(projMasCompTypeBO
							.getComponentTypeName());

					listProjMasCompTypeBean.add(projMasCompTypeBean);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		return listProjMasCompTypeBean;

	}

	// Eoa by Lalith kumar

	// Added by Lalith kumar
	public List<ProjMasCompStatusBean> getAllCompStatus()
			throws DataAccessException {

		List<ProjMasCompStatusBean> listProjMasCompStatusBean = new ArrayList<ProjMasCompStatusBean>();

		try {
			List<ProjMasCompStatusBO> listProjMasCompStatusBO = new ArrayList<ProjMasCompStatusBO>();

			try {
				// get status of all components
				listProjMasCompStatusBO = (List<ProjMasCompStatusBO>) projMasCompStatusRepository
						.findAll();

			} catch (Exception e) {
				throw new DataAccessException(
						"Failed to retrive list of ComponentStatus from Database");
			}

			if (!listProjMasCompStatusBO.isEmpty()) {
				for (ProjMasCompStatusBO projMasCompStatusBO : listProjMasCompStatusBO) {
					ProjMasCompStatusBean projMasCompStatusBean = new ProjMasCompStatusBean();

					projMasCompStatusBean
							.setComponentStatusId(projMasCompStatusBO
									.getPkComponentStatusId());
					projMasCompStatusBean
							.setComponentStatus(projMasCompStatusBO
									.getComponentStatus());

					listProjMasCompStatusBean.add(projMasCompStatusBean);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		return listProjMasCompStatusBean;

	}

	// EOA by Lalith kumar

	// Added by Rinta Mariam Jose

	public List<ProjectUserAccessBean> getProjectUserAccessList()
			throws CommonCustomException, DataAccessException {
		List<DatEmpPersonalDetailBO> datEmpDetailBOList =  new ArrayList<DatEmpPersonalDetailBO>();
		List<ProjectUserAccessBean> projectUserAccessBeanList = new ArrayList<ProjectUserAccessBean>();
		List<String> projectUserAccesslst = Stream.of(projectUserAccessRoleKeyNames.split(","))
		           .collect(Collectors.toList());
		try {
			datEmpDetailBOList = empPersonalRepository.getAllProjectUserAccess(projectUserAccesslst);
		}catch (Exception e) {
			throw new DataAccessException("Failed to fetch details");
		}
		if(datEmpDetailBOList.isEmpty()){
			throw new CommonCustomException("Records not found");
		}
		else{
			for(DatEmpPersonalDetailBO datEmpPersonalDetailBO : datEmpDetailBOList)
			{
				System.out.println(datEmpPersonalDetailBO.toString());
				ProjectUserAccessBean projectUserAccessBean = new ProjectUserAccessBean();
				projectUserAccessBean.setEmpId(datEmpPersonalDetailBO.getFkEmpDetailId());
				projectUserAccessBean.setEmpName(datEmpPersonalDetailBO.getEmpFirstName()+" "+datEmpPersonalDetailBO.getEmpLastName());
				projectUserAccessBean.setEmp(projectUserAccessBean.getEmpName()+"-"+projectUserAccessBean.getEmpId());
				projectUserAccessBeanList.add(projectUserAccessBean);
			}
			Collections.sort(projectUserAccessBeanList);
		}
		return projectUserAccessBeanList;
		
	}

	// EOA by Rinta Mariam Jose

	public List<ProjMasProjectClassificationBean> getAllProjectclasications()
			throws DataAccessException {

		List<ProjMasProjectClassificationBean> listprojectclabean = new ArrayList<ProjMasProjectClassificationBean>();
		try {
			List<ProjMasProjectClassificationBO> listprojectclabo = null;

			try {
				listprojectclabo = (List<ProjMasProjectClassificationBO>) projMasProjectClassificationRepos
						.findAll();
			} catch (Exception e) {
				//e.printStackTrace();
				throw new DataAccessException(
						"Failed to fetch the details from database.");
			}
			if (!listprojectclabo.isEmpty()) {
				for (ProjMasProjectClassificationBO projectclBo : listprojectclabo) {
					ProjMasProjectClassificationBean projectclabean = new ProjMasProjectClassificationBean();
					projectclabean.setPkClassificationId(projectclBo
							.getPkClassificationId());
					projectclabean.setClassificationName(projectclBo
							.getClassificationName());
					listprojectclabean.add(projectclabean);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		return listprojectclabean;

	}

	
	// Added By Rinta Mariam Jose
	public List<MasProjectOutputBean> viewProjectDetails(Integer projectId)
			throws CommonCustomException, DataAccessException {

		LOG.startUsecase("view project details");
		MasProjectBO masProjectBO = null;
		MasProjectOutputBean masProjectOutputBean = new MasProjectOutputBean();
		List<MasProjectOutputBean> masProjectOutputBeanList = new ArrayList<MasProjectOutputBean>();
		DatEmpPersonalDetailBO datEmpPersonalDetailBO = null;
		List<DatEmpPersonalDetailBO> datEmpPersonalDetailBOList = null;
		List<ProjectLevelUserAccessBean> projectLevelUserAccessBeanList = new ArrayList<ProjectLevelUserAccessBean>();
		Set<Integer> projectLevelUserAccessGroupList;
		Set<Integer> projectCoordinatorGroupList;
		List<ProjectCoordinatorBean> projectCoordinatorBeanList = new ArrayList<ProjectCoordinatorBean>();
		List<MasClientAddressOutputBean> masClientAddressOutputBeanList = null;
		ProjectPercentCalculationOutputBean projectPercentCalculationOutputBean;
		try {
			try {
				masProjectBO = masProjectRepos.findByPkProjectId(projectId);
			} catch (Exception e) {
				//e.printStackTrace();
				throw new DataAccessException(
						"Failed to fetch the details from mas_project table : "
								+ e.getMessage());
			}

			if (masProjectBO != null) {
				masProjectOutputBean
						.setProjectId(masProjectBO.getPkProjectId());
				masProjectOutputBean.setProjectName(masProjectBO
						.getProjectName());
				masProjectOutputBean.setProjectStartDate(masProjectBO
						.getProjectStartDate());
				masProjectOutputBean.setProjectEndDate(masProjectBO
						.getProjectEndDate());
				masProjectOutputBean.setProjectDescription(masProjectBO
						.getProjectDescription());
				masProjectOutputBean.setClientId(masProjectBO.getFkClientId());
				try {
					masClientAddressOutputBeanList = clientService
							.getClientAddress(masProjectBO.getFkClientId());
					if (!masClientAddressOutputBeanList.isEmpty())
						masProjectOutputBean
								.setClientAddresses(masClientAddressOutputBeanList);
				} catch (Exception e) {
				//	e.printStackTrace();
					throw new DataAccessException(
							"Failed to fetch the details from mas_client_address table : "
									+ e.getMessage());
				}

				masProjectOutputBean.setVerticalId(masProjectBO
						.getFkVerticalId());
				masProjectOutputBean.setVerticalName(masProjectBO
						.getMasVertical().getVerticalName());
				masProjectOutputBean.setRegionId(masProjectBO.getFkRegionId());
				masProjectOutputBean.setRegionName(masProjectBO.getMasRegion()
						.getRegionName());
				masProjectOutputBean.setDeliveryManagerId(masProjectBO
						.getFkDeliveryMgrId());

				try {
					datEmpPersonalDetailBO = empPersonalRepository
							.getEmpNameByFkEmpDetailId(masProjectBO
									.getFkDeliveryMgrId());
				} catch (Exception e) {
					//e.printStackTrace();
					throw new DataAccessException(
							"Failed to fetch the details from dat_emp_personal table.");
				}

				if (datEmpPersonalDetailBO != null)
					masProjectOutputBean
							.setDeliveryManagerName(datEmpPersonalDetailBO
									.getEmpFirstName()
									+ " "
									+ datEmpPersonalDetailBO.getEmpLastName());

				masProjectOutputBean.setBuUnitId(masProjectBO.getFkBuUnitId());
				masProjectOutputBean.setBuUnitName(masProjectBO.getMasBuUnit()
						.getBuUnitName());
				masProjectOutputBean.setDomainId(masProjectBO.getFkDomainId());
				masProjectOutputBean.setDomainName(masProjectBO.getMasDomain()
						.getDomainName());
				masProjectOutputBean.setDomainMgrId(masProjectBO
						.getFkDomainMgrId());

				try {
					datEmpPersonalDetailBO = empPersonalRepository
							.getEmpNameByFkEmpDetailId(masProjectBO
									.getFkDomainMgrId());
				} catch (Exception e) {
				//	e.printStackTrace();
					throw new DataAccessException(
							"Failed to fetch the details from dat_emp_personal table.");
				}

				if (datEmpPersonalDetailBO != null)
					masProjectOutputBean
							.setDomainMgrName(datEmpPersonalDetailBO
									.getEmpFirstName()
									+ " "
									+ datEmpPersonalDetailBO.getEmpLastName());

				masProjectOutputBean.setProjectStatusId(masProjectBO
						.getProjectStatusId());
				masProjectOutputBean.setProjectStatusName(masProjectBO
						.getProjMasProjectStatus().getProjectStatusName());
				masProjectOutputBean.setProjectType(masProjectBO
						.getProjectType());
				masProjectOutputBean
						.setAccountId(masProjectBO.getFkAccountId());
				masProjectOutputBean.setAccountName(masProjectBO
						.getMasAccount().getAccountName());
				masProjectOutputBean.setAccountMgrId(masProjectBO
						.getFkAccountMgrId());
				masProjectOutputBean.setIsHavingAggregatePo(masProjectBO
						.getIsHavingAggregatePo());
				
				
				try{
					projectPercentCalculationOutputBean = masProjectRepos.getProjectTriggeredAndProjectValue(masProjectBO.getPkProjectId());
					
				}catch(Exception e)
				{
					throw new DataAccessException("Unable to get project details...!");
				}
				try{	
					if(projectPercentCalculationOutputBean.getProjectValue()==null)
						projectPercentCalculationOutputBean.setProjectValue(new BigDecimal(0));
					else if( projectPercentCalculationOutputBean.getSumOfTriggeredValue()==null)
						projectPercentCalculationOutputBean.setSumOfTriggeredValue(new BigDecimal(0));
					else{
						LOG.info("ProjectCalculationbean..........!!!!!!"+projectPercentCalculationOutputBean);
					float completedPercent = (projectPercentCalculationOutputBean.getSumOfTriggeredValue()
							.floatValue()/(projectPercentCalculationOutputBean.getProjectValue().floatValue()))*100;
					LOG.info("completedPercent..............!!!!!" + completedPercent);
					LOG.info("percentage ..............@@@@@@@" + completedPercent);
					masProjectOutputBean.setCompletionOfProj(completedPercent);
					}
					}catch(Exception e)
					{
						LOG.info("completedPercent ::::"+masProjectOutputBean.getCompletionOfProj());
					}

				try {
					datEmpPersonalDetailBO = empPersonalRepository
							.getEmpNameByFkEmpDetailId(masProjectBO
									.getFkAccountMgrId());
				} catch (Exception e) {
				//	e.printStackTrace();
					throw new DataAccessException(
							"Failed to fetch the details from dat_emp_personal table.");
				}

				if (datEmpPersonalDetailBO != null)
					masProjectOutputBean
							.setAccountMgrName(datEmpPersonalDetailBO
									.getEmpFirstName()
									+ " "
									+ datEmpPersonalDetailBO.getEmpLastName());

				masProjectOutputBean.setProjectBillableFlag(masProjectBO
						.getProjectBillableFlag());
				masProjectOutputBean.setCreatedBy(masProjectBO.getCreatedBy());
				masProjectOutputBean.setCreatedOn(masProjectBO.getCreatedOn());
				masProjectOutputBean.setUpdatedBy(masProjectBO.getUpdatedBy());
			if(masProjectBO.getCommentsForUpdation()!=null)
			{
			List<String> comments = Arrays.asList(masProjectBO.getCommentsForUpdation().split(projectHistoryCommentsPattern));
			masProjectOutputBean.setCommentsForUpdation(comments.get(comments.size()-1));
			}
				try {
					projectCoordinatorGroupList = (Set<Integer>) projectMappedToCoordinatorRepos
							.getByProjectId(projectId);
				} catch (Exception e) {
					//e.printStackTrace();
					throw new DataAccessException(
							"Failed to fetch the details from proj_dat_project_mapped_to_project_coordinator table.");
				}

				if (!projectCoordinatorGroupList.isEmpty()) {
					try {
						datEmpPersonalDetailBOList = empPersonalRepository
								.getEmpNamesByFkEmpDetailIdIn(projectCoordinatorGroupList);
					} catch (Exception e) {
					//	e.printStackTrace();
						throw new DataAccessException(
								"Failed to fetch the details from dat_emp_personal(info regarding proj_dat_project_mapped_to_project_coordinator) table.");
					}

					if (!datEmpPersonalDetailBOList.isEmpty()) {
						for (DatEmpPersonalDetailBO projectCoordinator : datEmpPersonalDetailBOList) {
							ProjectCoordinatorBean projectCoordinatorBean = new ProjectCoordinatorBean();
							projectCoordinatorBean
									.setProjectCoordinatorId(projectCoordinator
											.getFkEmpDetailId());
							projectCoordinatorBean
									.setProjectCoordinatorName(projectCoordinator
											.getEmpFirstName()
											+ " "
											+ projectCoordinator
													.getEmpLastName());
							projectCoordinatorBean
									.setProjectCoordinator(projectCoordinatorBean
											.getProjectCoordinatorName()
											+ "-"
											+ projectCoordinatorBean
													.getProjectCoordinatorId());
							projectCoordinatorBeanList
									.add(projectCoordinatorBean);
						}
						masProjectOutputBean
								.setProjectCoordinatorDetails(projectCoordinatorBeanList);
					}
				}

				try {
					projectLevelUserAccessGroupList = (Set<Integer>) projectLevelUserAccessGroupRepos
							.getAllUserAccessByProjectId(projectId);
				} catch (Exception e) {
					//e.printStackTrace();
					throw new DataAccessException(
							"Failed to fetch the details from project_level_user_access_group table.");
				}

				if (!projectLevelUserAccessGroupList.isEmpty()) {
					try {
						datEmpPersonalDetailBOList = empPersonalRepository
								.getEmpNamesByFkEmpDetailIdIn(projectLevelUserAccessGroupList);
					} catch (Exception e) {
					//	e.printStackTrace();
						throw new DataAccessException(
								"Failed to fetch the details from dat_emp_personal(info regarding project_level_user_access_group) table.");
					}

					if (!datEmpPersonalDetailBOList.isEmpty()) {
						for (DatEmpPersonalDetailBO projectLevelUserAccess : datEmpPersonalDetailBOList) {
							ProjectLevelUserAccessBean projectLevelUserAccessBean = new ProjectLevelUserAccessBean();
							projectLevelUserAccessBean
									.setProjectLevelUserAccessId(projectLevelUserAccess
											.getFkEmpDetailId());
							projectLevelUserAccessBean
									.setProjectLevelUserAccessName(projectLevelUserAccess
											.getEmpFirstName()
											+ " "
											+ projectLevelUserAccess
													.getEmpLastName());
							projectLevelUserAccessBean
									.setProjectLevelUserAccess(projectLevelUserAccessBean
											.getProjectLevelUserAccessName()
											+ "-"
											+ projectLevelUserAccessBean
													.getProjectLevelUserAccessId());
							projectLevelUserAccessBeanList
									.add(projectLevelUserAccessBean);
						}
						masProjectOutputBean
								.setProjectLevelUserAccessDetails(projectLevelUserAccessBeanList);
						;
					}
				}

				masProjectOutputBean.setProjectClassificationId(masProjectBO
						.getFkClassificationId());
				masProjectOutputBean.setProjectClassificationName(masProjectBO
						.getProjMasProjectClassification()
						.getClassificationName());
				
				masProjectOutputBeanList.add(masProjectOutputBean);
			} else
				throw new CommonCustomException("Invalid projectId.");
		} catch (Exception e) {
		//	e.printStackTrace();
			throw new CommonCustomException(
					"Some error occured during viewing project details : "
							+ e.getMessage());
		}
		return masProjectOutputBeanList;
	}
	// EOA by Rinta Mariam Jose
	
	

	//Added by Lalith kumar for getting all project names based on BuId
	public List<ProjectNamesOutputBean> getAllProjectNamesBasedOnBuId(Short buId) throws DataAccessException, CommonCustomException
	{
		Set<ProjectNamesOutputBean> projectNames = new HashSet<ProjectNamesOutputBean>(); 
		List<ProjectNamesOutputBean> projectNamesList ;

			
			//Getting all the project names from pc and useraccess tables
			try{
				projectNames = masProjectRepos.getAllProjectBasedOnBuId(buId);
				
			}catch(Exception e)
			{
			//	e.printStackTrace();
				throw new DataAccessException("Unable to get project names...!");
			}
		
			projectNamesList=new ArrayList<ProjectNamesOutputBean>(projectNames);
			
		return projectNamesList;
	
	}
	//EOA by Lalith kumar
	
	
}
