package com.thbs.mis.project.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.thbs.mis.common.bean.MasBusinessEntityBean;
import com.thbs.mis.common.bean.MasClientOutputBean;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasBusinessEntityBO;
import com.thbs.mis.common.bo.MasCountryBO;
import com.thbs.mis.common.bo.MasRegionBO;
import com.thbs.mis.common.dao.CountryRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.MasBusinessEntityRepository;
import com.thbs.mis.common.dao.MasCountryRepository;
import com.thbs.mis.common.dao.MasRegionRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.report.Util.DownloadFileUtil;
import com.thbs.mis.framework.util.CollectionsUtil;
import com.thbs.mis.notificationframework.projectnotification.service.ProjectNotificationService;
import com.thbs.mis.project.bean.ClientAddressBean;
import com.thbs.mis.project.bean.ClientDetailsOutputBean;
import com.thbs.mis.project.bean.ClientInputBean;
import com.thbs.mis.project.bean.ClientOutputBean;
import com.thbs.mis.project.bean.ClientRepresentative;
import com.thbs.mis.project.bean.ClientSearchInputBean;
import com.thbs.mis.project.bean.ClientSearchOutputBean;
import com.thbs.mis.project.bean.MasClientAddressOutputBean;
import com.thbs.mis.project.bean.ProjMasClientManagerBean;
import com.thbs.mis.project.bo.DatClientEntityMappingBO;
import com.thbs.mis.project.bo.DatMappingClientWithTheirMgrBO;
import com.thbs.mis.project.bo.MasClientAddressBO;
import com.thbs.mis.project.bo.MasClientBO;
import com.thbs.mis.project.bo.ProjMasClientManagerBO;
import com.thbs.mis.project.constants.ProjectConstants;
import com.thbs.mis.project.dao.ClientSearchSpecification;
import com.thbs.mis.project.dao.DatClientEntityMappingRepository;
import com.thbs.mis.project.dao.DatMappingClientWithTheirMgrRepository;
import com.thbs.mis.project.dao.MasClientAddressRepository;
import com.thbs.mis.project.dao.MasClientRepository;
import com.thbs.mis.project.dao.ProjClientAddressRepository;
import com.thbs.mis.project.dao.ProjMasClientManagerRepository;
import com.thbs.mis.project.dao.ProjectAddClientRepository;

@Service
public class ClientService {

	private static final AppLog LOG = LogFactory.getLog(ClientService.class);

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	private Pattern pattern;

	private Matcher matcher;

	@Autowired
	private ProjMasClientManagerRepository projMasClientManagerRepository;

	@Autowired
	private MasClientAddressRepository masClientAddressRepository;

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private MasClientRepository masClientRepository;

	@Autowired
	private MasBusinessEntityRepository masBusinessEntityRepository;

	@Autowired
	private EmpDetailRepository empDetailRepos;

	@Autowired
	ProjectAddClientRepository projectAddClientRepository;

	@Autowired
	DatMappingClientWithTheirMgrRepository datMappingClientWithTheirMgrRepository;

	@Autowired
	DatClientEntityMappingRepository datClientEntityMappingRepository;

	@Autowired
	ProjClientAddressRepository projClientAddressRepository;

	@Autowired
	private ProjectNotificationService projectNotificationService;

	@Autowired
	private Validator validator;
	
	@Autowired
	private MasRegionRepository masRegionRepository;
	
	@Autowired
	private MasCountryRepository masCountryRepository;

	@Value("${project.file.basepath}")
	private String projectDocsBasePath;

	@Value("${project.active.emp.status}")
	private byte activeEmpStatus;

	@Value("${project.manager.key.roles.names}")
	private String managerRolesList;

	@Value("${role.projectFinanceTeam}")
	private String financeRoleList;

	@Value("${project.status.active}")
	private Byte projectStatusActive;

	@Value("${project.history.pattern}")
	private String projectHistoryPattern;
	
	// project.coordinator.role.names
	@Value("${project.coordinator.role.names}")
	private String pcRolesList;
	
	// Added by Rinta Mariam Jose

	/**
	 * @category This service is used for UI page to add a new client.
	 *           <Description addClient> : Add a Client
	 * @param clientDetails
	 * @param uploadSupportDocs
	 * @return flag
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */

	@Transactional
	public boolean addClient(ClientInputBean clientDetails,
			List<MultipartFile> uploadSupportDocs)
			throws CommonCustomException, DataAccessException,
			ConstraintViolationException {

		LOG.startUsecase("Entered adding Client service.");
		boolean flag = false;
		boolean uploadFlagDocs = false;
		MasClientBO masClientBO = null;
		DatEmpDetailBO empDetail;
		ProjMasClientManagerBO isEmailExistInMasClientManagerBO = null;
		List<ProjMasClientManagerBO> clientManagerBOList = null;
		List<Short> clientManagerIdList = null;
		pattern = Pattern.compile(EMAIL_PATTERN);
		Date today = new Date();

		Set<ConstraintViolation<ClientInputBean>> violations = validator
				.validate(clientDetails);
		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(violations);
		}

		if (uploadSupportDocs == null || uploadSupportDocs.isEmpty())
			throw new CommonCustomException("Select a file");

		Integer empId = clientDetails.getCreatedBy();

		if (empId == null)
			throw new CommonCustomException(
					"Created By is a mandatory field and can't be negative.");
		
		if (clientDetails.getClientAddresses() == null
				&& clientDetails.getClientAddresses().isEmpty())
			throw new CommonCustomException(
					"clientAddresses should not be null.");

		if (clientDetails.getMasBusinessEntity() == null
				|| clientDetails.getMasBusinessEntity().length <= 0)
			throw new CommonCustomException(
					"BusinessEntity Details are mandatory.");

		try {
			empDetail = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get Logged in User Id Details");
		}
		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException(
					"Logged In Employee Id is not a Active Employee.");

		boolean doesRoleExists = Arrays
				.asList(financeRoleList.split(","))
				.stream()
				.anyMatch(
						status -> status.equalsIgnoreCase(empDetail
								.getMasEmpRoleBO().getEmpRoleKeyName()));
		if (!doesRoleExists)
			throw new CommonCustomException(
					"You are not the authorized person to Add client detail");

		try {
			masClientBO = masClientRepository.getByClientName(clientDetails
					.getClientName());
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Error occured while adding new client.");
		}

		if (masClientBO != null)
			throw new CommonCustomException(
					"Client cannot be added since client name already exists.");

		boolean billToFlag = false;
		boolean shipToFlag = false;

		for (ClientAddressBean clientAddressBean : clientDetails
				.getClientAddresses()) {

			MasCountryBO masCountryBO;
			try {
				masCountryBO = countryRepository
						.getByCountryName(clientAddressBean.getCountryName());
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException(
						"Some error occured while adding client details. Kindly get in touch with Nucleus Support team.");
			}

			if (masCountryBO == null)
				throw new CommonCustomException(
						clientAddressBean.getCountryName()
								+ " doesn't exist in database.");

			clientAddressBean.setCountryId(masCountryBO.getPkCountryId());

			if (!(clientAddressBean.getIsBillToAddress().equalsIgnoreCase(
					ProjectConstants.YES) || clientAddressBean
					.getIsBillToAddress().equalsIgnoreCase(ProjectConstants.NO)))
				throw new CommonCustomException(
						"Invalid data for IsBillToAddress");

			if (!(clientAddressBean.getIsShipToAddress().equalsIgnoreCase(
					ProjectConstants.YES) || clientAddressBean
					.getIsShipToAddress().equalsIgnoreCase(ProjectConstants.NO)))
				throw new CommonCustomException(
						"Invalid data for IsShipToAddress");

			if (clientAddressBean.getIsBillToAddress().equalsIgnoreCase(
					ProjectConstants.YES)
					&& !billToFlag)
				billToFlag = true;

			else if (clientAddressBean.getIsBillToAddress().equalsIgnoreCase(
					ProjectConstants.YES)
					&& billToFlag)
				throw new CommonCustomException(
						"Only one billToAddress can be entered");

			if (clientAddressBean.getIsShipToAddress().equalsIgnoreCase(
					ProjectConstants.YES)
					&& !shipToFlag)
				shipToFlag = true;

			else if (clientAddressBean.getIsShipToAddress().equalsIgnoreCase(
					ProjectConstants.YES)
					&& shipToFlag)
				throw new CommonCustomException(
						"Only one shipToAddress can be entered");
		}

		if (!billToFlag || !shipToFlag)
			throw new CommonCustomException(
					"BillToAddress and ShipToAddress are mandatory");

		String phoneRegex = "^[- +()]*[0-9]+[- +()0-9]*";
		
		// String regExp = "(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\\s*[)]?[-\\s\\.]?[(]?[0-9]{1,3}[)]?([-\\s\\.]?[0-9]{3})([-\\s\\.]?[0-9]{3,4})";

		if (!clientDetails.getContactNumberOne().matches(phoneRegex)) {
			throw new CommonCustomException("Enter valid contact number (#1).");
		}

		if (clientDetails.getContactNumberTwo() != null
				&& !clientDetails.getContactNumberTwo().matches(phoneRegex)) {
			throw new CommonCustomException("Enter valid contact number (#2).");
		}

		if (clientDetails.getEmailId() != null) {
			matcher = pattern.matcher(clientDetails.getEmailId());
			if (!matcher.matches())
				throw new CommonCustomException("Invalid client email id");

		}

		String temp = clientDetails.getRiskAssessment();
		if (!(temp.equalsIgnoreCase(ProjectConstants.LOW) || temp
				.equalsIgnoreCase(ProjectConstants.HIGH)))
			throw new CommonCustomException("Invalid RiskAssessment data.");

		Integer days = clientDetails.getNoOfDays();
		if (temp.equalsIgnoreCase(ProjectConstants.LOW)) {
			if (!(days == 0 || days > ProjectConstants.RISK_DAYS)) {
				throw new CommonCustomException(
						"No of days should be greater than "
								+ ProjectConstants.RISK_DAYS
								+ " for LOW risk assessment.");
			}
		}

		if (temp.equalsIgnoreCase(ProjectConstants.HIGH)) {
			if (!(days >= ProjectConstants.RISK_DAYS)) {
				throw new CommonCustomException(
						"No of days should be greater than or equal to "
								+ ProjectConstants.RISK_DAYS
								+ " for HIGH risk assessment.");
			}
		}

		if (clientDetails.getClientRepresentativeList() != null) {

			clientManagerBOList = new ArrayList<ProjMasClientManagerBO>();
			clientManagerIdList = new ArrayList<Short>();
			for (ClientRepresentative clientRepresentative : clientDetails
					.getClientRepresentativeList()) {

				String repEmailId = clientRepresentative
						.getClientRepresentativeEmailId();
				matcher = pattern.matcher(repEmailId);
				if (!matcher.matches())
					throw new CommonCustomException(
							"Invalid client representative email id");

				try {
					isEmailExistInMasClientManagerBO = projMasClientManagerRepository
							.getByEmailId(repEmailId);
				} catch (Exception e) {
					throw new DataAccessException(
							"Some error occured while fetching client representative details. Kindly get in touch with Nucleus Support team.");
				}

				if (isEmailExistInMasClientManagerBO == null) {
					isEmailExistInMasClientManagerBO = new ProjMasClientManagerBO();
					isEmailExistInMasClientManagerBO
							.setManagerName(clientRepresentative
									.getClientRepresentativeName());
					isEmailExistInMasClientManagerBO
							.setEmailAddress(repEmailId);
					clientManagerBOList.add(isEmailExistInMasClientManagerBO);
				} else {
					clientManagerIdList.add(isEmailExistInMasClientManagerBO.getPkClientManagerId());
				}
			}

			try {
				clientManagerBOList = CollectionsUtil
						.getIterableAsList(projMasClientManagerRepository
								.save(clientManagerBOList));
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException(
						"Some error occured while adding client representative details. Kindly get in touch with Nucleus Support team.");
			}
			
			if(!clientManagerBOList.isEmpty()) {
				for(ProjMasClientManagerBO projMasClientManagerBO : clientManagerBOList) {
					clientManagerIdList.add(projMasClientManagerBO.getPkClientManagerId());
				}
			}
		}

		MasRegionBO masRegionBO = null;
		try {
			masRegionBO = masRegionRepository.findByPkRegionId(clientDetails
					.getRegionId());
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Some error occured while fetching details about region. Kindly get in touch with Nucleus Support team.");
		}

		if (masRegionBO == null)
			throw new CommonCustomException(
					"Region doesn't exist in database. Kindly get in touch with Nucleus Support team.");
		
		Byte[] entity = clientDetails.getMasBusinessEntity();
		if (clientDetails.getMasBusinessEntity() != null
				|| clientDetails.getMasBusinessEntity().length > 0) {
			for (int i = 0; i < entity.length; i++) {
	
				MasBusinessEntityBO masBusinessEntityBO;
				try {
					masBusinessEntityBO = masBusinessEntityRepository
							.findByPkEntityId(entity[i]);
				} catch (Exception e) {
					// e.printStackTrace();
					throw new DataAccessException(
							"Some error occured while adding client details. Kindly get in touch with Nucleus Support team.");
				}
	
				if (masBusinessEntityBO == null) {
					throw new CommonCustomException("Invalid business entity Id : "
							+ entity[i]);
				}
			}
		}

		boolean poTypeFlag = Arrays
				.asList(ProjectConstants.PO_TYPE.split(","))
				.stream()
				.anyMatch(
						status -> status.equalsIgnoreCase(clientDetails
								.getPoType()));
		if (!poTypeFlag)
			throw new CommonCustomException("Invalid poType data.");

		masClientBO = new MasClientBO();
		masClientBO.setClientName(clientDetails.getClientName());
		masClientBO.setClientStatus(ProjectConstants.ACTIVE);
		masClientBO.setVendorCode(clientDetails.getVendorCode());
		masClientBO.setContactNumberOne(clientDetails.getContactNumberOne());
		masClientBO.setContactNumberTwo(clientDetails.getContactNumberTwo());
		masClientBO.setCompanyEmailAddress(clientDetails.getEmailId());
		masClientBO.setFkRegionId(clientDetails.getRegionId());
		masClientBO.setPanNumber(clientDetails.getPanNumber());
		masClientBO.setGstNumber(clientDetails.getGstNumber());
		masClientBO.setVatNumber(clientDetails.getVatNumber());
		masClientBO.setOtherReferenceNumber(clientDetails
				.getOtherReferenceNumber());
		masClientBO.setCreditPeriod(clientDetails.getCreditPeriod());

		if (!uploadSupportDocs.isEmpty()) {
			masClientBO.setPaymentContractDocuments(ProjectConstants.UPLOADED);
		} else {
			masClientBO
					.setPaymentContractDocuments(ProjectConstants.NOT_UPLOADED);
		}

		masClientBO.setRiskAssessment(temp);
		masClientBO.setNoOfDaysPoMapping(days);
		masClientBO.setPoType(clientDetails.getPoType());
		masClientBO.setCreatedBy(empId);
		masClientBO.setCreatedOn(today);

		try {
			masClientBO = masClientRepository.save(masClientBO);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Some error occured while adding client details. Kindly get in touch with Nucleus Support team.");
		}

		if (masClientBO != null) {
			Short clientId = masClientBO.getPkClientId();

			List<MasClientAddressBO> masClientAddressBOList = new ArrayList<MasClientAddressBO>();
			for (ClientAddressBean clientAddressBean : clientDetails
					.getClientAddresses()) {
				MasClientAddressBO masClientAddressBO = new MasClientAddressBO();
				masClientAddressBO.setFkClientId(clientId);
				masClientAddressBO.setAddressLineOne(clientAddressBean
						.getAddressLineOne());
				masClientAddressBO.setAddressLineTwo(clientAddressBean
						.getAddressLineTwo());
				masClientAddressBO.setCityName(clientAddressBean.getCityName());
				masClientAddressBO.setStateName(clientAddressBean
						.getStateName());
				masClientAddressBO.setFkCountryId(clientAddressBean
						.getCountryId());
				masClientAddressBO.setIsBillToAddress(clientAddressBean
						.getIsBillToAddress());
				masClientAddressBO.setIsShipToAddress(clientAddressBean
						.getIsShipToAddress());
				masClientAddressBO.setZipCode(clientAddressBean.getZipCode());
				masClientAddressBO.setCreatedBy(empId);
				masClientAddressBO.setCreatedOn(today);
				masClientAddressBOList.add(masClientAddressBO);
			}
			if (!masClientAddressBOList.isEmpty()) {
				try {
					masClientAddressRepository.save(masClientAddressBOList);
				} catch (Exception e) {
					// e.printStackTrace();
					throw new DataAccessException(
							"Some error ocurred while saving client address details. Kindly get in touch with Nucleus Support team.");
				}
			}

			if(clientManagerIdList != null && !clientManagerIdList.isEmpty()) {
				List<DatMappingClientWithTheirMgrBO> datMappingClientWithTheirMgrBOList = new ArrayList<DatMappingClientWithTheirMgrBO>();
				for (Short clientManagerId : clientManagerIdList) {
					DatMappingClientWithTheirMgrBO datMappingClientWithTheirMgrBO = new DatMappingClientWithTheirMgrBO();
					datMappingClientWithTheirMgrBO.setFkClientId(clientId);
					datMappingClientWithTheirMgrBO
							.setFkClientManagerId(clientManagerId);
					datMappingClientWithTheirMgrBOList
							.add(datMappingClientWithTheirMgrBO);
				}
				if (!datMappingClientWithTheirMgrBOList.isEmpty()) {
					try {
						datMappingClientWithTheirMgrRepository
								.save(datMappingClientWithTheirMgrBOList);
					} catch (Exception e) {
						// e.printStackTrace();
						throw new DataAccessException(
								"Some error occured while adding client representative details. Kindly get in touch with Nucleus Support team.");
					}
				}
			}

			List<DatClientEntityMappingBO> datClientEntityMappingBOList = new ArrayList<DatClientEntityMappingBO>();
			if (clientDetails.getMasBusinessEntity() != null
					|| clientDetails.getMasBusinessEntity().length > 0) {
				for (int i = 0; i < entity.length; i++) {
					DatClientEntityMappingBO datClientEntityMappingBO = new DatClientEntityMappingBO();
					datClientEntityMappingBO.setFkClientId(clientId);
					datClientEntityMappingBO.setFkEntityId(entity[i]);
					datClientEntityMappingBOList.add(datClientEntityMappingBO);
				}
				if (!datClientEntityMappingBOList.isEmpty()) {
					try {
						datClientEntityMappingRepository
								.save(datClientEntityMappingBOList);
					} catch (Exception e) {
						// e.printStackTrace();
						throw new DataAccessException(
								"Some error occured while adding business entity details. Kindly get in touch with Nucleus Support team.");
					}
				}
			}

			if (uploadSupportDocs != null) {

				uploadFlagDocs = uploadSupportDocuments(masClientBO,
						uploadSupportDocs);

				if (!uploadFlagDocs) {
					throw new CommonCustomException(
							"Your request has been saved, but failed to store your docs in server. Kindly get in touch with Nucleus Support team.");
				}
				flag = true;
			}

			if(flag)
				projectNotificationService
						.sendClientCreatedNotificationToFinance(clientId);
		}

		return flag;
	}

	/**
	 * @category This service is used for UI page to update client details.
	 *           <Description updateClient> : Update client details.
	 * @param clientDetails
	 * @param uploadSupportDocs
	 * @return flag
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */

	@Transactional
	public boolean updateClient(ClientInputBean clientDetails,
			List<MultipartFile> uploadSupportDocs)
			throws CommonCustomException, DataAccessException,
			ConstraintViolationException {

		LOG.startUsecase("Entered updateClient() service.");
		System.out.print("uploadSupportDocs" + uploadSupportDocs.toString());
		boolean flag = false;
		boolean uploadFlagDocs = false;
		MasClientBO masClientBO = null;
		DatEmpDetailBO empDetail;
		List<ProjMasClientManagerBO> newClientManagerBOList = new ArrayList<ProjMasClientManagerBO>();
		List<ProjMasClientManagerBO> savedClientManagerBOList = null;
		StringBuffer comments = new StringBuffer();
		Date today = new Date();
		String oldClientName;
		List<DatClientEntityMappingBO> datClientEntityMappingBOListSave = new ArrayList<DatClientEntityMappingBO>();
		List<DatMappingClientWithTheirMgrBO> datMappingClientWithTheirMgrBOSave = new ArrayList<DatMappingClientWithTheirMgrBO>();
		DatMappingClientWithTheirMgrBO datMappingClientWithTheirMgrBO = null;
		String added = "";
		pattern = Pattern.compile(EMAIL_PATTERN);
		String phoneRegex = "/^[- +()]*[0-9][- +()0-9]*$/";
		String zipRegex = "[0-9]+";
		
		Set<ConstraintViolation<ClientInputBean>> violations = validator
				.validate(clientDetails);
		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(violations);
		}

		Integer empId = clientDetails.getUpdatedBy();
		if (empId == null)
			throw new CommonCustomException("UpdatedBy is a mandatory field.");
		
		Short clientId = clientDetails.getClientId();
		if (clientId == null)
			throw new CommonCustomException("clientId is a mandatory field.");

		if (clientDetails.getUpadtedComments() == null)
			throw new CommonCustomException(
					"updatedComments is a mandatory field.");

		try {
			empDetail = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get Logged in User Id Details");
		}

		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException(
					"Logged In Employee Id is not a Active Employee.");

		boolean doesRoleExists = Arrays
				.asList(financeRoleList.split(","))
				.stream()
				.anyMatch(
						status -> status.equalsIgnoreCase(empDetail
								.getMasEmpRoleBO().getEmpRoleKeyName()));
		if (!doesRoleExists)
			throw new CommonCustomException(
					"You are not the authorized person to update client detail.");

		try {
			masClientBO = masClientRepository.findByPkClientId(clientId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Some error ocurred while fetching client details");
		}

		if (masClientBO == null)
			throw new CommonCustomException("Invalid client");

		String newTemp = clientDetails.getClientName();
		String oldTemp = masClientBO.getClientName();
		oldClientName = oldTemp;

		if (!newTemp.equalsIgnoreCase(oldTemp)) {
			try {
				if (masClientRepository.getByClientName(newTemp) != null) {
					throw new CommonCustomException(
							"Client cannot be modified since entered client name already exists.");
				} else {
					comments.append("Client Name has been changed from "
							+ oldTemp + " to " + newTemp + ". ");
					masClientBO.setClientName(newTemp);
				}
			} catch (CommonCustomException e) {
				// e.printStackTrace();
				throw new CommonCustomException(e.getMessage());
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException(
						"Some error ocurred while modifying client details.");
			}
		}
		
		newTemp = clientDetails.getContactNumberOne();
		oldTemp = masClientBO.getContactNumberOne();
		if (!newTemp.equals(oldTemp)) {
			if (!clientDetails.getContactNumberOne().matches(phoneRegex)) {
				throw new CommonCustomException("Enter valid contact number (#1).");
			}
			comments.append("ContactNumberOne has been changed from " + oldTemp
					+ " to " + newTemp + ". ");
			masClientBO.setContactNumberOne(newTemp);
		}

		newTemp = clientDetails.getContactNumberTwo();
		oldTemp = masClientBO.getContactNumberTwo();
		if ((newTemp != null && !newTemp.equals(oldTemp)) || (oldTemp != null && !oldTemp.equals(newTemp))) {
			if (clientDetails.getContactNumberTwo() != null
					&& !clientDetails.getContactNumberTwo().matches(phoneRegex)) {
				throw new CommonCustomException("Enter valid contact number (#2).");
			}
			comments.append("ContactNumberTwo has been changed from " + oldTemp
					+ " to " + newTemp + ". ");
			masClientBO.setContactNumberTwo(newTemp);
		}

		newTemp = clientDetails.getEmailId();
		oldTemp = masClientBO.getCompanyEmailAddress();
		if ((newTemp != null && !newTemp.equals(oldTemp)) || (oldTemp != null && !oldTemp.equals(newTemp))) {
			if(clientDetails.getEmailId() != null ) {
				matcher = pattern.matcher(clientDetails.getEmailId());
				if (!matcher.matches())
					throw new CommonCustomException("Invalid client email id");
			}
			comments.append("ClientEmailId has been changed from "
					+ oldTemp + " to "
					+ newTemp + ". ");
			masClientBO.setCompanyEmailAddress(newTemp);
		}

		if (!clientDetails.getRegionId().equals(masClientBO.getFkRegionId())) {
			MasRegionBO masRegionBO = null;
			try {
				masRegionBO = masRegionRepository
						.findByPkRegionId(clientDetails.getRegionId());
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException(
						"Some error occured while fetching details about region. Kindly get in touch with Nucleus Support team.");
			}
			if (masRegionBO == null)
				throw new CommonCustomException(
						"Region doesn't exist in database. Kindly get in touch with Nucleus Support team.");

			comments.append("Region has been changed from "
					+ masClientBO.getMasRegionBO().getRegionName() + " to "
					+ masRegionBO.getRegionName() + ". ");
			masClientBO.setFkRegionId(clientDetails.getRegionId());
		}

		newTemp = clientDetails.getPanNumber();
		oldTemp = masClientBO.getPanNumber();
		if ((newTemp != null && !newTemp.equals(oldTemp)) || (oldTemp != null && !oldTemp.equals(newTemp))) {
			comments.append("Pan number has been changed from " + oldTemp
					+ " to " + newTemp + ". ");
			masClientBO.setPanNumber(newTemp);
		}

		newTemp = clientDetails.getGstNumber();
		oldTemp = masClientBO.getGstNumber();
		if ((newTemp != null && !newTemp.equals(oldTemp)) || (oldTemp != null && !oldTemp.equals(newTemp))) {
			comments.append("Gst number has been changed from " + oldTemp
					+ " to " + newTemp + ". ");
			masClientBO.setGstNumber(newTemp);
		}
		
		newTemp = clientDetails.getVatNumber();
		oldTemp = masClientBO.getVatNumber();
		if ((newTemp != null && !newTemp.equals(oldTemp)) || (oldTemp != null && !oldTemp.equals(newTemp))) {
			comments.append("Vat number has been changed from " + oldTemp
					+ " to " + newTemp + ". ");
			masClientBO.setVatNumber(newTemp);
		}
		
		newTemp = clientDetails.getOtherReferenceNumber();
		oldTemp = masClientBO.getOtherReferenceNumber();
		if ((newTemp != null && !newTemp.equals(oldTemp)) || (oldTemp != null && !oldTemp.equals(newTemp))) {
			comments.append("OtherReferenceNumber has been changed from "
					+ oldTemp + " to " + newTemp + ". ");
			masClientBO.setOtherReferenceNumber(newTemp);
		}
		
		if (!clientDetails.getCreditPeriod().equals(
				masClientBO.getCreditPeriod())) {
			comments.append("CreditPeriod has been changed from "
					+ masClientBO.getCreditPeriod() + " to "
					+ clientDetails.getCreditPeriod() + ". ");
			masClientBO.setCreditPeriod(clientDetails.getCreditPeriod());
		}

		newTemp = clientDetails.getRiskAssessment();
		oldTemp = masClientBO.getRiskAssessment();
		Integer newDays = clientDetails.getNoOfDays();
		Integer oldDays = masClientBO.getNoOfDaysPoMapping();
		if(newTemp.equals(ProjectConstants.LOW) || newTemp.equals(ProjectConstants.HIGH)) {
			if (!newTemp.equalsIgnoreCase(oldTemp)) {
				comments.append("Risk assessment has been changed from "
						+ oldTemp + " to " + newTemp + ". ");
				masClientBO.setRiskAssessment(newTemp);
				if (newTemp.equalsIgnoreCase(ProjectConstants.LOW)) {
					if (!(newDays == 0 || newDays > ProjectConstants.RISK_DAYS)) {
						throw new CommonCustomException(
								"No of days should be greater than "
										+ ProjectConstants.RISK_DAYS
										+ " for LOW risk assessment.");
					}
				}

				else if (newTemp.equalsIgnoreCase(ProjectConstants.HIGH)) {
					if (!(newDays >= ProjectConstants.RISK_DAYS)) {
						throw new CommonCustomException(
								"No of days should be greater than or equal to "
										+ ProjectConstants.RISK_DAYS
										+ " for HIGH risk assessment.");
					}
				}
			}
		} else {
			throw new CommonCustomException(
					"Invalid RiskAssessment data.");
		}
		if (!newDays.equals(oldDays)) {
			if (newTemp.equalsIgnoreCase(ProjectConstants.LOW)) {
				if (!(newDays == 0 || newDays > ProjectConstants.RISK_DAYS)) {
					throw new CommonCustomException(
							"No of days should be greater than "
									+ ProjectConstants.RISK_DAYS
									+ " for LOW risk assessment.");
				}
			}

			else if (newTemp.equalsIgnoreCase(ProjectConstants.HIGH)) {
				if (!(newDays >= ProjectConstants.RISK_DAYS)) {
					throw new CommonCustomException(
							"No of days should be greater than or equal to "
									+ ProjectConstants.RISK_DAYS
									+ " for HIGH risk assessment.");
				}
			}
			comments.append("No of days has been changed from "
					+ oldDays + " to " + newDays + ". ");
			masClientBO.setNoOfDaysPoMapping(newDays);
		}
		
		newTemp = clientDetails.getPoType();
		oldTemp = masClientBO.getPoType();
		if (!newTemp.equalsIgnoreCase(oldTemp)) {
			boolean poTypeFlag = Arrays
					.asList(ProjectConstants.PO_TYPE.split(","))
					.stream()
					.anyMatch(
							status -> status.equalsIgnoreCase(clientDetails
									.getPoType()));
			if (!poTypeFlag)
				throw new CommonCustomException("Invalid poType data.");

			comments.append("PoType has been changed from " + oldTemp + " to "
					+ newTemp + ". ");
			masClientBO.setPoType(newTemp);
		}

		newTemp = clientDetails.getVendorCode();
		oldTemp = masClientBO.getVendorCode();
		if ((newTemp != null && !newTemp.equals(oldTemp))
				|| (oldTemp != null && !oldTemp.equals(newTemp))) {
			comments.append("VendorCode has been changed from " + oldTemp
					+ " to " + newTemp + ". ");
			masClientBO.setVendorCode(clientDetails.getVendorCode());
		}
		
		if (clientDetails.getClientRepresentativeList() != null
				&& !clientDetails.getClientRepresentativeList().isEmpty()) {
			
			for (ClientRepresentative clientRepresentative : clientDetails
					.getClientRepresentativeList()) {
				ProjMasClientManagerBO isEmailExistInMasClientManagerBO = null;
				Short repId = clientRepresentative.getClientRepresentativeId();
				datMappingClientWithTheirMgrBO = null;

				if (repId != null ) {
						try {
							datMappingClientWithTheirMgrBO = datMappingClientWithTheirMgrRepository
									.findByFkClientManagerIdAndFkClientId(repId,
											clientId);
						} catch (Exception e) {
							throw new DataAccessException(
									"Some error occured while fetching client representative details. Kindly get in touch with Nucleus Support team.");
						}
						System.out.println("\n Existing Mgr: "+datMappingClientWithTheirMgrBO);
				}

				if (datMappingClientWithTheirMgrBO == null) {
					newTemp = clientRepresentative
							.getClientRepresentativeEmailId();
					if(repId == null) {
						matcher = pattern.matcher(newTemp);
						if (!matcher.matches())
							throw new CommonCustomException(
									"Invalid client representative email id");
					}
						try {
							isEmailExistInMasClientManagerBO = projMasClientManagerRepository
									.getByEmailId(newTemp);
						} catch (Exception e) {
							throw new DataAccessException(
									"Some error occured while fetching client representative details. Kindly get in touch with Nucleus Support team.");
						}
						if (isEmailExistInMasClientManagerBO == null) {
							isEmailExistInMasClientManagerBO = new ProjMasClientManagerBO();
							isEmailExistInMasClientManagerBO
									.setManagerName(clientRepresentative
											.getClientRepresentativeName());
							isEmailExistInMasClientManagerBO
									.setEmailAddress(newTemp);
							newClientManagerBOList
									.add(isEmailExistInMasClientManagerBO);
						} else {

						try {
							datMappingClientWithTheirMgrBO = datMappingClientWithTheirMgrRepository
									.findByFkClientManagerIdAndFkClientId(isEmailExistInMasClientManagerBO.getPkClientManagerId(),
											clientId);
						} catch (Exception e) {
							throw new DataAccessException(
									"Some error occured while fetching client representative details. Kindly get in touch with Nucleus Support team.");
						}

						if (datMappingClientWithTheirMgrBO == null) {
							datMappingClientWithTheirMgrBO = new DatMappingClientWithTheirMgrBO();
							datMappingClientWithTheirMgrBO
									.setFkClientId(clientId);
							datMappingClientWithTheirMgrBO
									.setFkClientManagerId(isEmailExistInMasClientManagerBO
											.getPkClientManagerId());
							datMappingClientWithTheirMgrBOSave
									.add(datMappingClientWithTheirMgrBO);
							added += isEmailExistInMasClientManagerBO
									.getManagerName() + ",";
						}
					}
				}
			}
		}

		if (!newClientManagerBOList.isEmpty()) {
			try {
				savedClientManagerBOList = CollectionsUtil
						.getIterableAsList(projMasClientManagerRepository
								.save(newClientManagerBOList));
			} catch (Exception e) {
				 e.printStackTrace();
				throw new DataAccessException(
						"Some error occured while adding client representative details. Kindly get in touch with Nucleus Support team.");
			}
			if (savedClientManagerBOList != null
					&& !savedClientManagerBOList.isEmpty()) {
				for (ProjMasClientManagerBO projMasClientManagerBO : savedClientManagerBOList) {
					datMappingClientWithTheirMgrBO = new DatMappingClientWithTheirMgrBO();
					datMappingClientWithTheirMgrBO.setFkClientId(clientId);
					datMappingClientWithTheirMgrBO
							.setFkClientManagerId(projMasClientManagerBO
									.getPkClientManagerId());
					datMappingClientWithTheirMgrBOSave
							.add(datMappingClientWithTheirMgrBO);
					added += projMasClientManagerBO.getManagerName() + ",";
				}
			}
		}

		if (!datMappingClientWithTheirMgrBOSave.isEmpty())
			comments.append("The clientRepresentative(s) added to clientRepresentativeList : "
					+ added.substring(0, added.length() - 1) + ". ");

		if (clientDetails.getMasBusinessEntity() != null
				&& clientDetails.getMasBusinessEntity().length > 0) {
			
			Byte[] entity = clientDetails.getMasBusinessEntity();
			added = "";
			for (int i = 0; i < entity.length; i++) {
				DatClientEntityMappingBO datClientEntityMappingBO = null;

						try {
							datClientEntityMappingBO = datClientEntityMappingRepository
									.findByFkClientIdAndFkEntityId(clientId, entity[i]);
						} catch (Exception e) {
							// e.printStackTrace();
							throw new DataAccessException(
									"Some error occured while fetching business entity details.");
						}
					
					if(datClientEntityMappingBO == null) {

						MasBusinessEntityBO masBusinessEntityBO;
						try {
							masBusinessEntityBO = masBusinessEntityRepository
									.findByPkEntityId(entity[i]);
						} catch (Exception e) {
							// e.printStackTrace();
							throw new DataAccessException(
									"Some error occured while adding client details. Kindly get in touch with Nucleus Support team.");
						}
	
						if (masBusinessEntityBO == null) {
							throw new CommonCustomException(
									"Invalid business entity Id : " + entity[i]);
						}
	
						datClientEntityMappingBO = new DatClientEntityMappingBO();
						datClientEntityMappingBO.setFkClientId(clientId);
						datClientEntityMappingBO.setFkEntityId(entity[i]);
						datClientEntityMappingBOListSave
								.add(datClientEntityMappingBO);
						added += masBusinessEntityBO.getEntityName() + ",";
				}
			}
		}

		if (!datClientEntityMappingBOListSave.isEmpty())
			comments.append("Business entity(s) added to entityList : "
					+ added.substring(0, added.length() - 1) + ". ");
		
		
		List<MasClientAddressBO> masClientAddressBOList = null;
		if(clientDetails.getClientAddresses() != null && !clientDetails.getClientAddresses().isEmpty()) {
			boolean billToFlag = false;
			boolean shipToFlag = false;
			boolean ship = false;
			boolean bill = false;
			boolean correspondance = false;
			Integer count = 0;
			Integer total = 0;
			List <Short> addressIdList = new ArrayList<Short>();
			
			for (ClientAddressBean clientAddressBean : clientDetails
					.getClientAddresses()) {
				if(clientAddressBean.getClientAddressId() != null)
					addressIdList.add(clientAddressBean.getClientAddressId());
			}
			
			try {
				masClientAddressBOList =  masClientAddressRepository.findByFkClientId(clientId);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException("Some error occured while adding or updating client addresses");
			}
			
			for(MasClientAddressBO masClientAddressBO : masClientAddressBOList) {
				if(masClientAddressBO.getIsBillToAddress().equalsIgnoreCase(ProjectConstants.YES) && !addressIdList.contains(masClientAddressBO.getPkClientAddressId()) )
					billToFlag = true;
				if(masClientAddressBO.getIsShipToAddress().equalsIgnoreCase(ProjectConstants.YES) && !addressIdList.contains(masClientAddressBO.getPkClientAddressId()) )
					shipToFlag = true;
				if(billToFlag || shipToFlag)
					total++;
			}
	
			masClientAddressBOList = new ArrayList<MasClientAddressBO>();
			for (ClientAddressBean clientAddressBean : clientDetails
					.getClientAddresses()) {
				
				if(total > 4)
					throw new CommonCustomException(
							"Client can have only four addresses.");
				
				ship = false;
				bill = false;
				
				MasCountryBO masCountryBO;
				try {
					masCountryBO = countryRepository
							.getByCountryName(clientAddressBean.getCountryName());
				} catch (Exception e) {
					// e.printStackTrace();
					throw new DataAccessException(
							"Some error occured while adding client details. Kindly get in touch with Nucleus Support team.");
				}
	 
				if (masCountryBO != null)
					clientAddressBean.setCountryId(masCountryBO.getPkCountryId());
				else {
					throw new CommonCustomException(
							clientAddressBean.getCountryName()
									+ " doesn't exist in database. Kindly get in touch with Nucleus Support team.");
				}
	
				if (!(clientAddressBean.getIsBillToAddress().equalsIgnoreCase(
						ProjectConstants.YES) || clientAddressBean
						.getIsBillToAddress().equalsIgnoreCase(ProjectConstants.NO)))
					throw new CommonCustomException(
							"Invalid data for IsBillToAddress");
	
				if (!(clientAddressBean.getIsShipToAddress().equalsIgnoreCase(
						ProjectConstants.YES) || clientAddressBean
						.getIsShipToAddress().equalsIgnoreCase(ProjectConstants.NO)))
					throw new CommonCustomException(
							"Invalid data for IsShipToAddress");
	
				if (clientAddressBean.getIsBillToAddress().equalsIgnoreCase(
						ProjectConstants.YES)
						&& !billToFlag)
					billToFlag = true;
	
				else if (clientAddressBean.getIsBillToAddress().equalsIgnoreCase(
						ProjectConstants.YES)
						&& billToFlag)
					throw new CommonCustomException(
							"Only one billToAddress can be entered");
	
				if (clientAddressBean.getIsShipToAddress().equalsIgnoreCase(
						ProjectConstants.YES)
						&& !shipToFlag)
					shipToFlag = true;
	
				else if (clientAddressBean.getIsShipToAddress().equalsIgnoreCase(
						ProjectConstants.YES)
						&& shipToFlag)
					throw new CommonCustomException(
							"Only one shipToAddress can be entered");
				
				if (clientAddressBean.getZipCode() != null
						&& !clientAddressBean.getZipCode().matches(zipRegex)) {
					throw new CommonCustomException("Enter valid zipCode.");
				}
	
				MasClientAddressBO masClientAddressBO;
	
				if (clientAddressBean.getClientAddressId() != null) {
					StringBuffer addComments = new StringBuffer();
					try {
						masClientAddressBO = masClientAddressRepository
								.findByPkClientAddressId(clientAddressBean
										.getClientAddressId());
					} catch (Exception e) {
						// e.printStackTrace();
						throw new DataAccessException(
								"Some error occured while updating client address details");
					}
	
					if (masClientAddressBO == null)
						throw new CommonCustomException(
								"Some error occured while updating client address details");
	
					if (!masClientAddressBO.getAddressLineOne().equalsIgnoreCase(
							clientAddressBean.getAddressLineOne())) {
						addComments.append("AddressLineOne has been changed from "
								+ masClientAddressBO.getAddressLineOne() + " to "
								+ clientAddressBean.getAddressLineOne() + ". ");
						masClientAddressBO.setAddressLineOne(clientAddressBean
								.getAddressLineOne());
					}
	
					if (!masClientAddressBO.getAddressLineTwo().equalsIgnoreCase(
							clientAddressBean.getAddressLineTwo())) {
						addComments.append("AddressLineTwo has been changed from "
								+ masClientAddressBO.getAddressLineTwo() + " to "
								+ clientAddressBean.getAddressLineTwo() + ". ");
						masClientAddressBO.setAddressLineTwo(clientAddressBean
								.getAddressLineTwo());
					}
	
					if (!masClientAddressBO.getCityName().equalsIgnoreCase(
							clientAddressBean.getCityName())) {
						addComments.append("City name has been changed from "
								+ masClientAddressBO.getCityName() + " to "
								+ clientAddressBean.getCityName() + ". ");
						masClientAddressBO.setCityName(clientAddressBean
								.getCityName());
					}
	
					if (!masClientAddressBO.getStateName().equalsIgnoreCase(
							clientAddressBean.getStateName())) {
						addComments.append("State name has been changed from "
								+ masClientAddressBO.getStateName() + " to "
								+ clientAddressBean.getStateName() + ". ");
						masClientAddressBO.setStateName(clientAddressBean
								.getStateName());
					}
	
					if (!masClientAddressBO.getFkCountryId().equals(
							clientAddressBean.getCountryId())) {
						addComments.append("Country has been changed from "
								+ masClientAddressBO.getMasCountry()
										.getCountryName() + " to "
								+ clientAddressBean.getCountryName() + ". ");
						masClientAddressBO.setFkCountryId(clientAddressBean
								.getCountryId());
					}
	
					if (!masClientAddressBO.getIsBillToAddress().equalsIgnoreCase(
							clientAddressBean.getIsBillToAddress())) {
						addComments.append("IsBillToAddress has been changed from "
								+ masClientAddressBO.getIsBillToAddress() + " to "
								+ clientAddressBean.getIsBillToAddress() + ". ");
						masClientAddressBO.setIsBillToAddress(clientAddressBean
								.getIsBillToAddress());
						if(masClientAddressBO.getIsBillToAddress().equalsIgnoreCase(ProjectConstants.NO))
							bill = true;
					}
	
					if (!masClientAddressBO.getIsShipToAddress().equalsIgnoreCase(
							clientAddressBean.getIsShipToAddress())) {
						addComments.append("IsShipToAddress has been changed from "
								+ masClientAddressBO.getIsShipToAddress() + " to "
								+ clientAddressBean.getIsShipToAddress() + ". ");
						masClientAddressBO.setIsShipToAddress(clientAddressBean
								.getIsShipToAddress());
						if(masClientAddressBO.getIsShipToAddress().equalsIgnoreCase(ProjectConstants.NO))
							ship = true;
					}
					
					newTemp = clientAddressBean.getZipCode();
					oldTemp = masClientAddressBO.getZipCode();
					if ((newTemp != null && !newTemp.equals(oldTemp))
							|| (oldTemp != null && !oldTemp.equals(newTemp))) {
						addComments.append("ZipCode has been changed from "
								+ oldTemp + " to " + newTemp + ". ");
						masClientAddressBO.setZipCode(clientAddressBean
								.getZipCode());
					}
					
					if (addComments != null && addComments.length() > 0) {
						masClientAddressBO.setUpdatedBy(empId);
						masClientAddressBO.setUpdatedOn(today);
	
						masClientAddressBO.setUpdatedComments(addComments
								.toString());
						masClientAddressBOList.add(masClientAddressBO);
						
						if(bill && ship)
							comments.append("Billing and Shipping Address has been changed.");
						
						else if(bill)
							comments.append("Billing Address has been changed. ");
						
						else if(ship)
							comments.append("Shipping Address has been changed. ");
						
						else if(masClientAddressBO.getIsBillToAddress().equalsIgnoreCase(ProjectConstants.YES) && masClientAddressBO.getIsShipToAddress().equalsIgnoreCase(ProjectConstants.YES))
							comments.append("Billing and Shipping Address has been updated.");

						else if(masClientAddressBO.getIsBillToAddress().equalsIgnoreCase(ProjectConstants.YES))
							comments.append("Billing Address has been updated. ");

						else if(masClientAddressBO.getIsShipToAddress().equalsIgnoreCase(ProjectConstants.YES))
							comments.append("Shipping Address has been updated. ");

						else {
							if(!correspondance) {
								correspondance = true;
								comments.append("Correspondance Address has been updated. ");
							}
						}
						total++;
					}
	
				} else {
					masClientAddressBO = new MasClientAddressBO();
					masClientAddressBO.setFkClientId(clientId);
					masClientAddressBO.setAddressLineOne(clientAddressBean
							.getAddressLineOne());
					masClientAddressBO.setAddressLineTwo(clientAddressBean
							.getAddressLineTwo());
					masClientAddressBO.setCityName(clientAddressBean.getCityName());
					masClientAddressBO.setStateName(clientAddressBean
							.getStateName());
					masClientAddressBO.setFkCountryId(clientAddressBean
							.getCountryId());
					masClientAddressBO.setIsBillToAddress(clientAddressBean
							.getIsBillToAddress());
					masClientAddressBO.setIsShipToAddress(clientAddressBean
							.getIsShipToAddress());
					masClientAddressBO.setZipCode(clientAddressBean.getZipCode());
					masClientAddressBO.setCreatedBy(empId);
					masClientAddressBO.setCreatedOn(today);
					masClientAddressBOList.add(masClientAddressBO);
					count++;
					total++;
				}
			}
			
			if (!billToFlag || !shipToFlag)
				throw new CommonCustomException(
						"Both BillToAddress and ShipToAddress are mandatory");
			
			if(total > 4)
				throw new CommonCustomException(
						"Client can have only four addresses.");
			
			if (count > 0)
				comments.append(count + " new client address(s) has been added. ");
		}

		if (comments != null && comments.length() > 0) {
			
			masClientBO.setUpdatedBy(empId);
			masClientBO.setUpdatedOn(today);
			masClientBO.setUpdatedComments(clientDetails.getUpadtedComments());
			masClientBO.setClientHistory(comments.toString());
			flag = true;
			
		} else
			throw new CommonCustomException(
					"Updation is not required since no data is changed.");

		try {
			masClientBO = masClientRepository.save(masClientBO);
		} catch (Exception e) {
			 e.printStackTrace();
			throw new DataAccessException(
					"Some error occured while adding client details. Kindly get in touch with Nucleus Support team.");
		}

		if (masClientBO != null) {

			if (!datMappingClientWithTheirMgrBOSave.isEmpty()) {
				try {
					datMappingClientWithTheirMgrRepository
							.save(datMappingClientWithTheirMgrBOSave);
				} catch (Exception e) {
					// e.printStackTrace();
					throw new DataAccessException(
							"Some error occured while adding client representative details. Kindly get in touch with Nucleus Support team.");
				}
			}

			if (!datClientEntityMappingBOListSave.isEmpty()) {
				try {
					datClientEntityMappingRepository
							.save(datClientEntityMappingBOListSave);
				} catch (Exception e) {
					// e.printStackTrace();
					throw new DataAccessException(
							"Some error occured while adding business entity details. Kindly get in touch with Nucleus Support team.");
				}
			}

			if (masClientAddressBOList != null && !masClientAddressBOList.isEmpty()) {
				try {
					masClientAddressRepository.save(masClientAddressBOList);
				} catch (Exception e) {
					// e.printStackTrace();
					throw new DataAccessException(
							"Some error ocurred while saving client address details. Kindly get in touch with Nucleus Support team.");
				}
			}

			if (uploadSupportDocs != null && !uploadSupportDocs.isEmpty()) {

				uploadFlagDocs = uploadSupportDocuments(masClientBO,
						uploadSupportDocs);

				if (!uploadFlagDocs) {
					throw new CommonCustomException(
							"Client details has been updated, but failed to store your docs in server. Kindly get in touch with Nucleus Support team.");
				}
			}
			
			System.out.println("Notification starts..");
			
				projectNotificationService.sendClientUpdatedNotificationToFinance(
						clientId, oldClientName);
			System.out.println("Notification ends..");

		}

		return flag;
	}

	/**
	 * @category This service is used for UI page to show client details.
	 *           <Description getClientDetailsBasedOnClientId> : Fetch all
	 *           client details
	 * @param clientId
	 * @param empId
	 * @return List<ClientOutputBean>
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */

	public List<ClientOutputBean> getClientDetailsBasedOnClientId(
			Short clientId, Integer empId) throws DataAccessException,
			CommonCustomException {
		LOG.startUsecase("Start of get client details based on client id");
		List<ClientOutputBean> clientOutputBeanList = new ArrayList<ClientOutputBean>();
		ClientOutputBean clientOutputBean = new ClientOutputBean();
		MasClientBO masClientBO;
		List<DatClientEntityMappingBO> datClientEntityMappingBOList;
		List<DatMappingClientWithTheirMgrBO> datMappingClientWithTheirMgrBOList;
		List<MasBusinessEntityBean> masBusinessEntityBeanList;
		DatEmpDetailBO empDetail;

		try {
			empDetail = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get Logged in User Id Details");
		}
		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException(
					"Logged In Employee Id is not a Active Employee.");

		// Finance role check
		boolean doesFinRoleExists = Arrays
				.asList(financeRoleList.split(","))
				.stream()
				.anyMatch(
						status -> status.equals(empDetail.getMasEmpRoleBO()
								.getEmpRoleKeyName()));
		
		if(!doesFinRoleExists) {
			// pc Role check
			boolean doesPCRoleExists = Arrays
					.asList(pcRolesList.split(","))
					.stream()
					.anyMatch(
							status -> status.equals(empDetail.getMasEmpRoleBO()
									.getEmpRoleKeyName()));
			if (!doesPCRoleExists)
				throw new CommonCustomException(
						"You are not the authorized person to view client details.");
		}

		try {
			masClientBO = masClientRepository.findByPkClientId(clientId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Some error occured while fetching client details.");
		}

		if (masClientBO != null) {

			clientOutputBean.setClientId(clientId);
			clientOutputBean.setClientName(masClientBO.getClientName());
			clientOutputBean.setClientStatus(masClientBO.getClientStatus());
			clientOutputBean.setVendorCode(masClientBO.getVendorCode());
			clientOutputBean.setPaymentContractDocuments(masClientBO
					.getPaymentContractDocuments());
			clientOutputBean.setRiskAssessment(masClientBO.getRiskAssessment());
			clientOutputBean.setContactNumberOne(masClientBO.getContactNumberOne());
			clientOutputBean.setContactNumberTwo(masClientBO.getContactNumberTwo());
			clientOutputBean.setRegionId(masClientBO.getFkRegionId());
			clientOutputBean.setNoOfDays(masClientBO.getNoOfDaysPoMapping());
			clientOutputBean.setPanNumber(masClientBO.getPanNumber());
			clientOutputBean.setGstNumber(masClientBO.getGstNumber());
			clientOutputBean.setVatNumber(masClientBO.getVatNumber());
			clientOutputBean.setOtherReferenceNumber(masClientBO.getOtherReferenceNumber());
			clientOutputBean.setEmailId(masClientBO.getCompanyEmailAddress());
			clientOutputBean.setPoType(masClientBO.getPoType());
			clientOutputBean.setCreditPeriod(masClientBO.getCreditPeriod());
			clientOutputBean.setClientDocsPath(projectDocsBasePath + clientId
					+ "/");

			// Change regarding clientRepresentative
			try {
				datMappingClientWithTheirMgrBOList = datMappingClientWithTheirMgrRepository
						.findByFkClientId(clientId);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException(
						"Some error occured while fetching client details.");
			}
			List<ClientRepresentative> clientRepresentativeList = new ArrayList<ClientRepresentative>();
			if (datMappingClientWithTheirMgrBOList != null
					&& !datMappingClientWithTheirMgrBOList.isEmpty()) {
				ClientRepresentative clientRepresentative = new ClientRepresentative();
				clientRepresentative
						.setClientRepresentativeId(datMappingClientWithTheirMgrBOList
								.get(0).getFkClientManagerId());
				clientRepresentative
						.setClientRepresentativeName(datMappingClientWithTheirMgrBOList
								.get(0).getProjMasClientManager()
								.getManagerName());
				clientRepresentative
						.setClientRepresentativeEmailId(datMappingClientWithTheirMgrBOList
								.get(0).getProjMasClientManager()
								.getEmailAddress());
				clientRepresentativeList.add(clientRepresentative);
			}
			clientOutputBean.setClientRepresentative(clientRepresentativeList);

			try {
				datClientEntityMappingBOList = datClientEntityMappingRepository
						.findByFkClientId(clientId);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException(
						"Some error occured while fetching client details.");
			}

			if (datClientEntityMappingBOList != null
					&& !datClientEntityMappingBOList.isEmpty()) {
				masBusinessEntityBeanList = new ArrayList<MasBusinessEntityBean>();
				for (DatClientEntityMappingBO datClientEntityMappingBO : datClientEntityMappingBOList) {
					MasBusinessEntityBean masBusinessEntityBean = new MasBusinessEntityBean();
					masBusinessEntityBean.setEntityId(datClientEntityMappingBO
							.getFkEntityId());
					masBusinessEntityBean
							.setEntityName(datClientEntityMappingBO
									.getMasBusinessEntity().getEntityName());
					masBusinessEntityBeanList.add(masBusinessEntityBean);
				}
				clientOutputBean
						.setMasBusinessEntityBean(masBusinessEntityBeanList);
			}

			clientOutputBean.setClientAddresses(getClientAddress(clientId));

			// Client Docs Path
			
			clientOutputBean.setCreatedBy(masClientBO.getCreatedBy());
			clientOutputBean.setCreatedOn(masClientBO.getCreatedOn());
			clientOutputBean.setUpdatedBy(masClientBO.getUpdatedBy());
			clientOutputBean.setUpdatedOn(masClientBO.getUpdatedOn());
			
			clientOutputBeanList.add(clientOutputBean);
		}

		LOG.endUsecase("End of get client details based on client id");

		return clientOutputBeanList;
	}

	/**
	 * @category This service is used for UI page to show all client managers.
	 *           <Description getAllProjMasClientManager> : Fetch All Client
	 *           Managers.
	 * @return List<ProjMasClientManagerBean>
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */

	public List<ProjMasClientManagerBean> getAllProjMasClientManager()
			throws DataAccessException, CommonCustomException {
		List<ProjMasClientManagerBO> projMasClientManagerBOList = null;
		List<ProjMasClientManagerBean> projMasClientManagerBeanList = new ArrayList<ProjMasClientManagerBean>();

		try {
			projMasClientManagerBOList = (List<ProjMasClientManagerBO>) projMasClientManagerRepository
					.findAll();
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Failed to retrieve list of client managers :  "
							+ e.getMessage());
		}

		if (!projMasClientManagerBOList.isEmpty()) {
			for (ProjMasClientManagerBO projMasClientManagerBO : projMasClientManagerBOList) {
				ProjMasClientManagerBean projMasClientManagerBean = new ProjMasClientManagerBean();
				projMasClientManagerBean
						.setClientManagerId(projMasClientManagerBO
								.getPkClientManagerId());
				projMasClientManagerBean
						.setClientManagerName(projMasClientManagerBO
								.getManagerName());
				projMasClientManagerBean
						.setClientManagerEmailId(projMasClientManagerBO
								.getEmailAddress());
				projMasClientManagerBeanList.add(projMasClientManagerBean);
			}
		} else
			throw new CommonCustomException("No Client Managers Exist");

		return projMasClientManagerBeanList;
	}

	/**
	 * @category This service is used for UI page to show the client addresses.
	 *           <Description getClientAddress> : Fetch All Client Addresses.
	 * @param clientId
	 * @return List<MasClientAddressOutputBean>
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */

	public List<MasClientAddressOutputBean> getClientAddress(short clientId)
			throws DataAccessException, CommonCustomException {
		List<MasClientAddressOutputBean> masClientAddressOutputBeanList = new ArrayList<MasClientAddressOutputBean>();
		List<MasClientAddressBO> masClientAddressBOList;

		try {
			masClientAddressBOList = masClientAddressRepository
					.findByFkClientId(clientId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Some error occurred while fetching data from mas_client_address: "
							+ e.getMessage());
		}

		if (!masClientAddressBOList.isEmpty()) {
			for (MasClientAddressBO masClientAddressBO : masClientAddressBOList) {
				MasClientAddressOutputBean masClientAddressOutputBean = new MasClientAddressOutputBean();

				masClientAddressOutputBean
						.setClientAddressId(masClientAddressBO
								.getPkClientAddressId());
				masClientAddressOutputBean.setAddressLineOne(masClientAddressBO
						.getAddressLineOne());
				masClientAddressOutputBean.setAddressLineTwo(masClientAddressBO
						.getAddressLineTwo());
				masClientAddressOutputBean.setCityName(masClientAddressBO
						.getCityName());
				masClientAddressOutputBean.setStateName(masClientAddressBO
						.getStateName());
				masClientAddressOutputBean.setCountryId(masClientAddressBO.getFkCountryId());
				masClientAddressOutputBean.setCountryName(masClientAddressBO.getMasCountry()
						.getCountryName());
				masClientAddressOutputBean
						.setIsBillToAddress(masClientAddressBO
								.getIsBillToAddress());
				masClientAddressOutputBean
						.setIsShipToAddress(masClientAddressBO
								.getIsShipToAddress());
				masClientAddressOutputBean.setZipCode(masClientAddressBO
						.getZipCode());
				masClientAddressOutputBean.setCreatedOn(masClientAddressBO
						.getCreatedOn());
				masClientAddressOutputBean.setCreatedBy(masClientAddressBO
						.getCreatedBy());
				masClientAddressOutputBean.setUpdatedBy(masClientAddressBO
						.getUpdatedBy());
				masClientAddressOutputBean.setUpdatedOn(masClientAddressBO
						.getUpdatedOn());
				masClientAddressOutputBean
						.setUpdatedComments(masClientAddressBO
								.getUpdatedComments());
				masClientAddressOutputBeanList.add(masClientAddressOutputBean);
			}
		} else {
			MasClientBO masClientBO = null;
			try {
				masClientBO = masClientRepository.findByPkClientId(clientId);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException(
						"Some error occurred while fetching data from mas_client table: "
								+ e.getMessage());
			}

			if (masClientBO == null)
				throw new CommonCustomException("Invalid clientId");
		}

		return masClientAddressOutputBeanList;
	}

	// EOA by Rinta Mariam Jose

	// added by Fareen For upload Support document of client

	/**
	 * @category This service is used to upload client documents. <Description
	 *           uploadSupportDocuments> : Upload client documents.
	 * @param masClientBo
	 * @param uploadDocs
	 * @return flag
	 * @throws CommonCustomException
	 */

	public boolean uploadSupportDocuments(MasClientBO masClientBo,
			List<MultipartFile> uploadDocs) throws CommonCustomException {
		LOG.startUsecase("File upload Service");
		boolean flag = false;
		try {
			if (uploadDocs.size() > 0) {
				for (MultipartFile file : uploadDocs) {
					// String basePath = purchaseDocsBasePath;
					String newFileName = "EMP_" + file.getOriginalFilename();
					File dir = new File(projectDocsBasePath
							+ masClientBo.getPkClientId());

					if (dir.mkdirs()) {
						File fileToBeStored = new File(dir.getPath()
								+ File.separator + newFileName);
						FileCopyUtils.copy(file.getBytes(), fileToBeStored);
						flag = true;
					} else {
						File fileToBeStored = new File(dir.getPath()
								+ File.separator + newFileName);
						FileCopyUtils.copy(file.getBytes(), fileToBeStored);
						flag = true;
					}
				}
			}

		} catch (Exception e) {
			// e.getMessage();
			throw new CommonCustomException("File Uploadation Failed");
		}
		LOG.endUsecase("end of File upload Service");
		return flag;
	}

	// added by Fareen

	/**
	 * @category This service is used for UI page to search clients.
	 *           <Description getClientBasedOnSearch> : To Search Clients.
	 * @param inputObject
	 * @return List<ClientSearchOutputBean>
	 * @throws DataAccessException
	 * @throws CommonCustomException 
	 */

	public Page<ClientSearchOutputBean> getClientBasedOnSearch(
			ClientSearchInputBean searchBean) throws DataAccessException, CommonCustomException {
		List<ClientSearchOutputBean> clientSearchOutputBeanList = new ArrayList<ClientSearchOutputBean>();
		Page<MasClientBO> masClientBOList = null;
		Specification<MasClientBO> clientStatusSpecification = null;
		Page<ClientSearchOutputBean> finalPage;
		DatEmpDetailBO empDetail;
		
		Integer empId = searchBean.getLoggedInUser();
		
		if(searchBean.getClientId() != null && (searchBean.getClientStatus() != null)) //|| searchBean.getCountryId() != null))
			throw new CommonCustomException("Unable to perform client advanced search!!! While searching client using client name no need to pass client status or country name");
			
		try {
			empDetail = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get Logged in User Id Details");
		}

		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException(
					"Logged In Employee Id is not a Active Employee.");

		boolean doesRoleExists = Arrays
				.asList(financeRoleList.split(","))
				.stream()
				.anyMatch(
						status -> status.equalsIgnoreCase(empDetail
								.getMasEmpRoleBO().getEmpRoleKeyName()));
		if (!doesRoleExists)
			throw new CommonCustomException(
					"You are not the authorized person to search client.");
		
		try {
			clientStatusSpecification = ClientSearchSpecification.getClientStatus(searchBean);
		} catch (Exception e1) {
			throw new DataAccessException("Failed to get all the details of project");
		}
		Pageable pageable = new PageRequest(searchBean.getPageNumber(), searchBean.getPageSize(), Sort.Direction.DESC,
				"createdOn");
		try {
			masClientBOList = masClientRepository.findAll(clientStatusSpecification, pageable);
		} catch (Exception e) {
			throw new DataAccessException("Failed to get all the details of project");
		}
		if (masClientBOList == null) {
			throw new CommonCustomException("No data found for the requested input");
		}
		for (MasClientBO masClientBO : masClientBOList) {
			ClientSearchOutputBean clientSearchOutputBean=new ClientSearchOutputBean();
			clientSearchOutputBean.setClientName(masClientBO.getClientName());
			clientSearchOutputBean.setClientStatus(masClientBO.getClientStatus());
			clientSearchOutputBean.setRegionId(masClientBO.getFkRegionId());
			clientSearchOutputBean.setClientId(masClientBO.getPkClientId());
			clientSearchOutputBean.setCreditPeriod(masClientBO.getCreditPeriod());
			clientSearchOutputBean.setRiskAssessment(masClientBO.getRiskAssessment());			
			
			MasClientAddressBO clientAddress = null;
			try {
				clientAddress = masClientAddressRepository.findByFkClientIdAndIsBillToAddress(masClientBO.getPkClientId(),ProjectConstants.YES);
			} catch (Exception e) {
				//e.printStackTrace();
				throw new DataAccessException("Unable to perform client advance serach!!!");
			}
			if(clientAddress != null) {
				clientSearchOutputBean.setCountryId(clientAddress.getFkCountryId());
				clientSearchOutputBean.setCountryName(clientAddress.getMasCountry().getCountryName());
			}
			
			clientSearchOutputBeanList.add(clientSearchOutputBean);
		}
		finalPage = new PageImpl<ClientSearchOutputBean>(clientSearchOutputBeanList, pageable,
				clientSearchOutputBeanList.size());

		return finalPage;
	}

	// added by fareen download the client document service

	/**
	 * @category This service is used for UI page to download client documents.
	 *           <Description downloadSupportingDocsByClient> : Download Client
	 *           documents.
	 * @param clientId
	 * @param response
	 * @return isPoDownloaded
	 * @throws CommonCustomException
	 */

	public Boolean downloadSupportingDocsByClient(Short clientId,
			HttpServletResponse response) throws CommonCustomException {

		LOG.entering("download supporting docs for client service method");
		Boolean isPoDownloaded = false;
		try {

			MasClientBO clientBo;
			try {
				clientBo = masClientRepository
						.findByPkClientId(clientId);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException("Some error occured while downloading supporting docs for client");
			}
			
			if(clientBo == null)
				throw new CommonCustomException("Invalid client");
			
			String basePathClientDoc = projectDocsBasePath
					+ clientBo.getPkClientId() + "/";

			List<String> listOfFiles = new ArrayList<String>();
			File fileSysAdmin = new File(basePathClientDoc);
			File[] listOfSysFiles = fileSysAdmin.listFiles();

			if (fileSysAdmin.isDirectory()) {
				if (listOfSysFiles.length > 0) {
					listOfFiles.add(basePathClientDoc);
				}
			}

			if (listOfFiles.size() > 0) {
				DownloadFileUtil.downloadFilesTwoPath(response, listOfFiles);
				isPoDownloaded = true;
			} else {
				throw new CommonCustomException(
						"File does't exists in required server path ");
			}

		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}

		LOG.exiting("download supporting docs for Client");
		return isPoDownloaded;

	}

	// EOD download service

	/**
	 * @category This service is used for UI page to show active and inactive
	 *           clients. <Description getAllActiveAndInactiveClient> : Fetch
	 *           All Active and Inactive Clients.
	 * @param statusId
	 * @return List<MasClientOutputBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException 
	 */

	public List<MasClientOutputBean> getAllActiveAndInactiveClient(byte statusId)
			throws CommonCustomException, DataAccessException {
		List<MasClientOutputBean> masClientOutputBeanList = new ArrayList<MasClientOutputBean>();
		List<MasClientBO> masClientBoList = new ArrayList<MasClientBO>();
		try {
			switch (statusId) {
			case 1:
				LOG.info("Active Clients Case");
				masClientBoList = masClientRepository.getClient(ProjectConstants.ACTIVE);
				break;
			case 2:
				LOG.info("Inactive Clients Case");
				masClientBoList = masClientRepository.getClient(ProjectConstants.INACTIVE);
				break;
			case 3:
				LOG.info("Both active  and inactive Clients Case");
				masClientBoList = masClientRepository.getAllClientsByStatusId();
				break;

			default:
				throw new CommonCustomException("Enter valid statusId");

			}
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrive the client :"
					+ e.getMessage());
		}
		if (masClientBoList != null && !masClientBoList.isEmpty()) {
			masClientBoList
					.forEach(masBO -> {
						MasClientOutputBean masClientOutput = new MasClientOutputBean();
						masClientOutput.setClientId(masBO.getPkClientId());
						masClientOutput.setClientName(masBO.getClientName());
						masClientOutput.setClientStatus(masBO
								.getClientStatus());
						masClientOutputBeanList.add(masClientOutput);
					});
		}
		return masClientOutputBeanList;
	}

	/**
	 * @category This service is used for UI page to show clients based on BU.
	 *           <Description getClientDetailsBasedOnBuId> : Fetch all clients
	 *           based on BU.
	 * @param buId
	 * @return ClientDetailsOutputBean
	 * @throws DataAccessException
	 */

	public Set<ClientDetailsOutputBean> getClientDetailsBasedOnBuId(short buId)
			throws DataAccessException {
		Set<ClientDetailsOutputBean> clientDetailsOutputBeanList = null;

		try {
			clientDetailsOutputBeanList = masClientRepository
					.getClientDetailsBasedOnBuId(buId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Some error occured while fetching client details.");
		}

		return clientDetailsOutputBeanList;
	}

	/**
	 * @category This service is used for UI page to show clients based on
	 *           region. <Description getClientDetailsBasedOnRegionId> : Fetch
	 *           all clients based on region.
	 * @param regionId
	 * @return MasClientOutputBean
	 * @throws DataAccessException
	 */

	public Set<MasClientOutputBean> getClientDetailsBasedOnRegionId(
			short regionId) throws DataAccessException {
		Set<MasClientOutputBean> masClientOutputBeanList = null;

		try {
			masClientOutputBeanList = masClientRepository
					.getByFkRegionId(regionId);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataAccessException(
					"Some error occured while fetching client details.");
		}

		return masClientOutputBeanList;
	}

}