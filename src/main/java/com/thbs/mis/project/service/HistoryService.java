package com.thbs.mis.project.service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.gsrnotification.service.GSRNotificationService;
import com.thbs.mis.project.bean.ViewHistoryInputBean;
import com.thbs.mis.project.bean.ViewHistoryOutputBean;
import com.thbs.mis.project.bo.DatComponentAuditBO;
import com.thbs.mis.project.bo.DatComponentBO;
import com.thbs.mis.project.bo.DatMilestoneAuditBO;
import com.thbs.mis.project.bo.DatMilestoneBO;
import com.thbs.mis.project.bo.DatWorkorderAuditBO;
import com.thbs.mis.project.bo.DatWorkorderBO;
import com.thbs.mis.project.bo.MasClientAuditBO;
import com.thbs.mis.project.bo.MasClientBO;
import com.thbs.mis.project.bo.MasProjectAuditBO;
import com.thbs.mis.project.bo.MasProjectBO;
import com.thbs.mis.project.constants.ProjectConstants;
import com.thbs.mis.project.dao.DatComponentAuditRepository;
import com.thbs.mis.project.dao.DatComponentRepository;
import com.thbs.mis.project.dao.DatMilestoneAuditRepository;
import com.thbs.mis.project.dao.DatMilestoneRepository;
import com.thbs.mis.project.dao.DatWorkorderAuditRepository;
import com.thbs.mis.project.dao.DatWorkorderRepository;
import com.thbs.mis.project.dao.MasClientAuditRepository;
import com.thbs.mis.project.dao.MasClientRepository;
import com.thbs.mis.project.dao.MasProjectAuditRepository;
import com.thbs.mis.project.dao.MasProjectRepository;

@Service
public class HistoryService {

	@Autowired
	private MasProjectAuditRepository masProjectAuditRepository;
	
	@Autowired
	private DatWorkorderAuditRepository datWorkorderAuditRepository;
	
	@Autowired
	private DatWorkorderRepository datWorkorderRepository;
	
	@Autowired
	GSRNotificationService gsrNotificationService;

	@Autowired
	private DatComponentRepository datComponentRepository;
	@Autowired
	private DatMilestoneRepository datMilestoneRepository;
	@Autowired
	private DatComponentAuditRepository datComponentAuditRepository;
	
	@Autowired
	 private MasProjectRepository masProjectRepository;
	@Autowired
	DatMilestoneAuditRepository datMilestoneAuditRepository;
	
	@Autowired
	private MasClientRepository masClientRepository;
	
	@Autowired
	private MasClientAuditRepository masClientAuditRepository;
	
	@Autowired
	private EmpDetailRepository empDetailRepos;
	
	@Value("${ui.message.create}")
	String createMessage;

	@Value("${ui.message.update}")
	String updateMessage;
	
	@Value("${project.history.pattern}")
	String PROJECTHISTORYPATTERN;
	
	@Value("${role.projectFinanceTeam}")
	private String financeRoleList;
	
	@Value("${project.active.emp.status}")
	private byte activeEmpStatus;

	private static final AppLog LOG = LogFactory.getLog(ProjectService.class);


	public List<ViewHistoryOutputBean> viewHistory(ViewHistoryInputBean viewHistoryInputBean)
			throws CommonCustomException, DataAccessException {
		Integer type = viewHistoryInputBean.getType();
		String name = "";
		String message = "";
		List<ViewHistoryOutputBean> viewHistoryOutputBeanLst = new ArrayList<ViewHistoryOutputBean>();
		DatEmpDetailBO empDetail;
		boolean doesRoleExists;
		
		try {
			empDetail = empDetailRepos.findByPkEmpId(viewHistoryInputBean.getViewedBy());
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get Logged in User Id Details");
		}

		if (empDetail == null)
			throw new CommonCustomException("Invalid Logged In Emp Id");
		if (!empDetail.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException(
					"Logged In Employee Id is not a Active Employee.");
		
		switch (type) {
		
		// for client
		case 1:
			List<MasClientAuditBO> masClientAuditBOUpdateList = new ArrayList<MasClientAuditBO>();
			MasClientBO masClientBO = null;

			doesRoleExists = Arrays
					.asList(financeRoleList.split(","))
					.stream()
					.anyMatch(
							status -> status.equalsIgnoreCase(empDetail
									.getMasEmpRoleBO().getEmpRoleKeyName()));
			if (!doesRoleExists)
				throw new CommonCustomException(
						"You are not the authorized person to update client detail.");

			try {
				masClientBO = masClientRepository
						.findByPkClientId(viewHistoryInputBean.getId()
								.shortValue());
			} catch (Exception e) {
				// e.printStackTrace();
				throw new CommonCustomException("Unable to view the history");
			}

			if (masClientBO == null)
				throw new CommonCustomException("Invalid clientId");

			LOG.info("masClientBO--------->>>" + masClientBO);

			if (masClientBO.getUpdatedBy() == null) {
				ViewHistoryOutputBean viewHistoryOutputBean = new ViewHistoryOutputBean();

				if (!viewHistoryInputBean.getViewedBy().equals(
						masClientBO.getCreatedBy()))
					name = gsrNotificationService
							.getEmployeeNameByEmpId(viewHistoryInputBean
									.getViewedBy());
				else
					name = "You have";

				String comment = "Client" + "-" + masClientBO.getClientName();
				message = MessageFormat.format(createMessage, name, comment);
				viewHistoryOutputBean.setMessage(message);
				viewHistoryOutputBean.setCreatedOn(masClientBO.getCreatedOn());
				viewHistoryOutputBeanLst.add(viewHistoryOutputBean);
			}

			else {
				String client = "details of client "
						+ masClientBO.getClientName() + ". ";
				ViewHistoryOutputBean viewHistoryOutputBean = new ViewHistoryOutputBean();

				if (!viewHistoryInputBean.getViewedBy().equals(
						masClientBO.getUpdatedBy()))
					name = gsrNotificationService
							.getEmployeeNameByEmpId(masClientBO.getUpdatedBy());
				else
					name = "You have";

				message = MessageFormat.format(
						updateMessage,
						name,
						client
								+ masClientBO.getClientHistory()
										.substring(
												0,
												masClientBO.getClientHistory()
														.length() - 1));
				viewHistoryOutputBean.setMessage(message);
				viewHistoryOutputBean.setCreatedOn(masClientBO.getUpdatedOn());
				viewHistoryOutputBeanLst.add(viewHistoryOutputBean);

				Pageable topThreeCli = new PageRequest(0, 4,
						Sort.Direction.DESC, "pkClientAuditId");
				try {
					masClientAuditBOUpdateList.addAll(masClientAuditRepository
							.getAllUpdatedByFkClientId(viewHistoryInputBean
									.getId().shortValue(), topThreeCli));
				} catch (Exception e) {
					// e.printStackTrace();
					throw new CommonCustomException(
							"Unable to view the history");
				}

				if (masClientAuditBOUpdateList != null
						&& masClientAuditBOUpdateList.size() < 4) {
					MasClientAuditBO masClientAuditBO;
					try {
						masClientAuditBO = masClientAuditRepository
								.findByFkClientIdAndAction(viewHistoryInputBean
										.getId().shortValue(),
										ProjectConstants.CREATE);
					} catch (Exception e) {
						// e.printStackTrace();
						throw new CommonCustomException(
								"Unable to view the history");
					}
					if (masClientAuditBO != null)
						masClientAuditBOUpdateList.add(masClientAuditBO);
				}

				if (masClientAuditBOUpdateList != null
						&& !masClientAuditBOUpdateList.isEmpty()) {
					LOG.info("masClientAuditBOUpdateList::::>>>"
							+ masClientAuditBOUpdateList);
					for (MasClientAuditBO masClientAuditBO : masClientAuditBOUpdateList) {
						LOG.info("masClientAuditBO::::>>>" + masClientAuditBO);

						if (!viewHistoryInputBean.getViewedBy().equals(
								masClientAuditBO.getEnteredBy()))
							name = gsrNotificationService
									.getEmployeeNameByEmpId(masClientAuditBO
											.getEnteredBy());
						else
							name = "You have";

						if (masClientAuditBO.getAction().equals(
								ProjectConstants.CREATE)) {
							String comment = "Client" + "-"
									+ masClientAuditBO.getClientName();
							message = MessageFormat.format(createMessage, name,
									comment);
						} else {
							message = MessageFormat
									.format(updateMessage,
											name,
											client
													+ masClientAuditBO
															.getClientHistory()
															.substring(
																	0,
																	masClientAuditBO
																			.getClientHistory()
																			.length() - 2));
						}

						viewHistoryOutputBean = new ViewHistoryOutputBean();
						viewHistoryOutputBean.setMessage(message);
						viewHistoryOutputBean.setCreatedOn(masClientAuditBO
								.getEnteredOn());
						viewHistoryOutputBeanLst.add(viewHistoryOutputBean);
					}
				}
			}

			System.out.println(viewHistoryOutputBeanLst);
			break;
		// for project
		case 2:
			/*try {
				masProjectAuditBOLst =  masProjectAuditRepository.findByFkProjectId(viewHistoryInputBean.getId());
			} catch (Exception e) {
				e.printStackTrace();
				throw new CommonCustomException("Unable to view the history");
				}
			if(masProjectAuditBOLst!=null){
				for (MasProjectAuditBO masProjectAuditBO : masProjectAuditBOLst) {
					if (masProjectAuditBO.getCommentsForUpdation() != null) {
					//	String patternString = PROJECTHISTORYPATTERN;
						Pattern pattern = Pattern.compile(patternString);
						String[] split = pattern.split(masProjectAuditBO.getCommentsForUpdation());
						if (!split[0].isEmpty()) {
							ViewHistoryOutputBean viewHistoryOutputBean = new ViewHistoryOutputBean();
							if (!viewHistoryInputBean.getViewedBy()
									.equals(masProjectAuditBO.getMasProjectAuditEnteredBy()))
								name = gsrNotificationService
										.getEmployeeNameByEmpId(viewHistoryInputBean.getViewedBy());
							else
								name = "You have";
							if (masProjectAuditBO.getAction().equals(ProjectConstants.CREATE)) {
								String comment = "Project" + "-" + masProjectAuditBO.getProjectName();
								message = MessageFormat.format(createMessage, name, comment);
							} else {
								message = MessageFormat.format(updateMessage, name,
										split[0]);
							}
							viewHistoryOutputBean.setMessage(message);
							viewHistoryOutputBean.setCreatedOn(masProjectAuditBO.getEnteredOn());
							viewHistoryOutputBeanLst.add(viewHistoryOutputBean);
						}
					}
				}
			}*/
			List<MasProjectAuditBO> datProjectAuditBOLst = new ArrayList<MasProjectAuditBO>();
			List<MasProjectAuditBO> datProjectAuditBOLstFromAudit = null;
			MasProjectAuditBO masProjectAuditBo = null;
			MasProjectBO masProjectBO=null;
			try {
				masProjectBO=masProjectRepository.findByPkProjectId(viewHistoryInputBean.getId());
			} catch (Exception e) {
				//e.printStackTrace();
				throw new CommonCustomException("Unable to view the history");
			}
			if(masProjectBO==null)
				throw new CommonCustomException("Project does not exists...!");
			else
			{
				if(masProjectBO.getUpdatedBy()!=null)
				{
				ViewHistoryOutputBean viewHistoryOutputBean = new ViewHistoryOutputBean();
				if (!viewHistoryInputBean.getViewedBy().equals(masProjectBO.getUpdatedBy()))
					name = gsrNotificationService.getEmployeeNameByEmpId(masProjectBO.getUpdatedBy());
				else
					name = "You have";
					message = MessageFormat.format(updateMessage, name, masProjectBO.getProjHistory());
					viewHistoryOutputBean.setMessage(message);
					viewHistoryOutputBean.setCreatedOn(masProjectBO.getUpdatedOn());
					viewHistoryOutputBeanLst.add(viewHistoryOutputBean);
				
			Pageable topThreePrj = new PageRequest(0, 4, Sort.Direction.DESC,"pkProjectAuditId");
			try {
				datProjectAuditBOLstFromAudit =  masProjectAuditRepository.getByFkProjectId(viewHistoryInputBean.getId(),topThreePrj);
			} catch (Exception e) {
			//	e.printStackTrace();
				throw new CommonCustomException("Unable to view the history");
				}
			if(datProjectAuditBOLstFromAudit!=null && !datProjectAuditBOLstFromAudit.isEmpty())
			for (MasProjectAuditBO masProjectAuditBO : datProjectAuditBOLstFromAudit) {
				datProjectAuditBOLst.add(masProjectAuditBO);
			}
			if (datProjectAuditBOLstFromAudit.size() < 4) {
				try {
					masProjectAuditBo = masProjectAuditRepository.findByFkProjectId(viewHistoryInputBean.getId(),
							"create");
				} catch (Exception e) {
		//			e.printStackTrace();
					throw new CommonCustomException("Unable to view the history");
				}
				datProjectAuditBOLst.add(masProjectAuditBo);
			}
			if(!(datProjectAuditBOLst.isEmpty()) && datProjectAuditBOLst!= null){
				for (MasProjectAuditBO masProjectAudit : datProjectAuditBOLst) {
					LOG.info("datProjectAuditBOLst::::>>>"+datProjectAuditBOLst);
					LOG.info("masProjectAudit::::>>>"+masProjectAudit);
							ViewHistoryOutputBean viewHistoryOutputBean2 = new ViewHistoryOutputBean();
							if (!viewHistoryInputBean.getViewedBy()
									.equals(masProjectAudit.getEnteredBy()))
								name = gsrNotificationService
										.getEmployeeNameByEmpId(masProjectAudit.getEnteredBy());
							else
								name = "You have";
							if (masProjectAudit.getAction().equals(ProjectConstants.CREATE)) {
								String comment = "Project" + "-" + masProjectAudit.getProjectName();
								message = MessageFormat.format(createMessage, name, comment);
							} else {
								message = MessageFormat.format(updateMessage, name,
										"Client "+masProjectAudit.getProjHistory());
							}
							viewHistoryOutputBean2.setMessage(message);
							viewHistoryOutputBean2.setCreatedOn(masProjectAudit.getEnteredOn());
							viewHistoryOutputBeanLst.add(viewHistoryOutputBean2);
						}
				}
				}else
				{
					LOG.info("Only create record exists....!!!!!");
					
					ViewHistoryOutputBean viewHistoryOutputBean2 = new ViewHistoryOutputBean();
					if (!viewHistoryInputBean.getViewedBy().equals(masProjectBO.getCreatedBy()))
						name = gsrNotificationService.getEmployeeNameByEmpId(masProjectBO.getCreatedBy());
					else
						name = "You have";
						message = MessageFormat.format(createMessage, name,masProjectBO.getProjectName());
						viewHistoryOutputBean2.setMessage(message);
						viewHistoryOutputBean2.setCreatedOn(masProjectBO.getCreatedOn());
						viewHistoryOutputBeanLst.add(viewHistoryOutputBean2);
					
				}
				
			}
			
			break;
		// for work order
		case 3:
			List<DatWorkorderAuditBO> datWorkorderAuditBOLst = new ArrayList<DatWorkorderAuditBO>();
			List<DatWorkorderAuditBO> datWorkorderAuditBOLstFromAudit = null;
			DatWorkorderAuditBO datWorkorderAuditBo = null;
			DatWorkorderBO datWorkorderBO=null;
			try {
				 datWorkorderBO=datWorkorderRepository.findByPkWorkorderId(viewHistoryInputBean.getId());
			} catch (Exception e) {
	//			e.printStackTrace();
				throw new CommonCustomException("Unable to view the history");
			}
			if(datWorkorderBO==null)
					throw new CommonCustomException("Workorder does not exists...!");
			else		
			{
				if(datWorkorderBO.getWorkorderUpdatedBy()!=null)
				{
					LOG.info("Entering master table and audit table....!!!!!");
				ViewHistoryOutputBean viewHistoryOutputBean = new ViewHistoryOutputBean();
				if (!viewHistoryInputBean.getViewedBy().equals(datWorkorderBO.getWorkorderUpdatedBy()))
					name = gsrNotificationService.getEmployeeNameByEmpId(datWorkorderBO.getWorkorderUpdatedBy());
				else
					name = "You have";
					message = MessageFormat.format(updateMessage, name,datWorkorderBO.getWoHistory());
					viewHistoryOutputBean.setMessage(message);
					viewHistoryOutputBean.setCreatedOn(datWorkorderBO.getWorkorderUpdatedOn());
					viewHistoryOutputBeanLst.add(viewHistoryOutputBean);
				//}
			
			
			
			
			Pageable topThree = new PageRequest(0, 4, Sort.Direction.DESC,"pkWorkorderAuditId");
			try {
				datWorkorderAuditBOLstFromAudit =  datWorkorderAuditRepository.findByFkWorkorderIdAndPattern(viewHistoryInputBean.getId(),PROJECTHISTORYPATTERN,topThree);
			} catch (Exception e) {
		//		e.printStackTrace();
				throw new CommonCustomException("Unable to view the history");
				}
			if(datWorkorderAuditBOLstFromAudit!=null && !datWorkorderAuditBOLstFromAudit.isEmpty())
			for (DatWorkorderAuditBO datWorkorderAuditBO : datWorkorderAuditBOLstFromAudit) {
				datWorkorderAuditBOLst.add(datWorkorderAuditBO);
			}
			if (datWorkorderAuditBOLstFromAudit.size() < 4) {
				try {
					datWorkorderAuditBo = datWorkorderAuditRepository.findByFkWorkorderId(viewHistoryInputBean.getId(),
							"create");
				} catch (Exception e) {
		//			e.printStackTrace();
					throw new CommonCustomException("Unable to view the history");
				}
				datWorkorderAuditBOLst.add(datWorkorderAuditBo);
			}
			if(!datWorkorderAuditBOLst.isEmpty()){
				for (DatWorkorderAuditBO datWorkorderAudit : datWorkorderAuditBOLst) {
					LOG.info("datWorkorderAudit  Loop ::::>>>>>" + datWorkorderAudit);
							ViewHistoryOutputBean viewHistoryOutputBeanAudit = new ViewHistoryOutputBean();
							if (!viewHistoryInputBean.getViewedBy()
									.equals(datWorkorderAudit.getEnteredBy()))
								name = gsrNotificationService
										.getEmployeeNameByEmpId(datWorkorderAudit.getEnteredBy());
							else
								name = "You have";
							if (datWorkorderAudit.getAction().equals(ProjectConstants.CREATE)) {
								String comment = "Work order" + "-" + datWorkorderAudit.getWorkorderNumber();
								message = MessageFormat.format(createMessage, name, comment);
							} else {
								message = MessageFormat.format(updateMessage, name,
										datWorkorderAudit.getWoHistory());
							}
							viewHistoryOutputBeanAudit.setMessage(message);
							viewHistoryOutputBeanAudit.setCreatedOn(datWorkorderAudit.getEnteredOn());
							viewHistoryOutputBeanLst.add(viewHistoryOutputBeanAudit);
					
				}
			}
			}else
			{
				LOG.info("Only create record exists....!!!!!");
			
				ViewHistoryOutputBean viewHistoryOutputBean2 = new ViewHistoryOutputBean();
				if (!viewHistoryInputBean.getViewedBy().equals(datWorkorderBO.getWorkorderCreatedBy()))
					name = gsrNotificationService.getEmployeeNameByEmpId(datWorkorderBO.getWorkorderCreatedBy());
				else
					name = "You have";
					message = MessageFormat.format(createMessage, name,datWorkorderBO.getWoHistory());
					viewHistoryOutputBean2.setMessage(message);
					viewHistoryOutputBean2.setCreatedOn(datWorkorderBO.getWorkorderCreatedOn());
					viewHistoryOutputBeanLst.add(viewHistoryOutputBean2);
				//}
				
				
			}
			
			}
			
			break;
		
		// for client budget
		case 4:

			break;
		// for thbs budget
		case 5:

			break;
		// for budget approval
		case 6:

			break;
		// for component
		case 7:
			
			List<DatComponentAuditBO> datComponentAuditBOLst = new ArrayList<DatComponentAuditBO>();
			List<DatComponentAuditBO> datComponentAuditBOLstFromAudit = null;
			DatComponentAuditBO datComponentAuditBO = null;
			
		//	Pattern patterComponent = Pattern.compile(patternString);
			DatComponentBO datComponentBO=null;
			try {
				datComponentBO=datComponentRepository.findByPkComponentId(viewHistoryInputBean.getId());
			} catch (Exception e) {
			//	e.printStackTrace();
				throw new CommonCustomException("Unable to view the history");
			}
			if(datComponentBO==null)
				throw new CommonCustomException("Component does not exists...!");
			else				
			{
				if(datComponentBO.getUpdatedBy()!=null)
				{
				ViewHistoryOutputBean viewHistoryOutputBean = new ViewHistoryOutputBean();
				if (!viewHistoryInputBean.getViewedBy().equals(datComponentBO.getUpdatedBy()))
					name = gsrNotificationService.getEmployeeNameByEmpId(datComponentBO.getUpdatedBy());
				else
					name = "You have";
					message = MessageFormat.format(updateMessage, name,datComponentBO.getCompHistory());
					viewHistoryOutputBean.setMessage(message);
					viewHistoryOutputBean.setCreatedOn(datComponentBO.getUpdatedOn());
					viewHistoryOutputBeanLst.add(viewHistoryOutputBean);
			
			
			Pageable topThreeComp = new PageRequest(0, 4, Sort.Direction.DESC,"pkComponentAuditId");
			try {
				datComponentAuditBOLstFromAudit =  datComponentAuditRepository.getByComponentId(viewHistoryInputBean.getId(),topThreeComp);
			} catch (Exception e) {
				e.printStackTrace();
				throw new CommonCustomException("Unable to view the history");
				}
			if(datComponentAuditBOLstFromAudit!=null && !datComponentAuditBOLstFromAudit.isEmpty())
			for (DatComponentAuditBO datComponentAuditBO1 : datComponentAuditBOLstFromAudit) {
				datComponentAuditBOLst.add(datComponentAuditBO1);
			}
			if (datComponentAuditBOLstFromAudit.size() < 4) {
				try {
					datComponentAuditBO = datComponentAuditRepository.findByFkComponentId(viewHistoryInputBean.getId(),
							"create");
				} catch (Exception e) {
					e.printStackTrace();
					throw new CommonCustomException("Unable to view the history");
				}
				datComponentAuditBOLst.add(datComponentAuditBO);
			}
			if(!datComponentAuditBOLst.isEmpty()){
				for (DatComponentAuditBO datComponentAudit : datComponentAuditBOLst) {
					LOG.info("datComponentAudit loop :::>>>>>>>>    "+datComponentAudit);
							ViewHistoryOutputBean viewHistoryOutputBeanAudit = new ViewHistoryOutputBean();
							if (!viewHistoryInputBean.getViewedBy()
									.equals(datComponentAudit.getEnteredBy()))
								name = gsrNotificationService
										.getEmployeeNameByEmpId(datComponentAudit.getEnteredBy());
							else
								name = "You have";
							if (datComponentAudit.getAction().equals(ProjectConstants.CREATE)) {
								String comment = "Component" + "-" + datComponentAudit.getComponentName();
								message = MessageFormat.format(createMessage, name, comment);
							} else {
								message = MessageFormat.format(updateMessage, name,
										datComponentAudit.getCompHistory());
							}
							viewHistoryOutputBeanAudit.setMessage(message);
							viewHistoryOutputBeanAudit.setCreatedOn(datComponentAudit.getEnteredOn());
							viewHistoryOutputBeanLst.add(viewHistoryOutputBeanAudit);
			
					
				}
				}
				}else
				{
					LOG.info("Only create record exists....!!!!!");
					
					ViewHistoryOutputBean viewHistoryOutputBean2 = new ViewHistoryOutputBean();
					if (!viewHistoryInputBean.getViewedBy().equals(datComponentBO.getCreatedBy()))
						name = gsrNotificationService.getEmployeeNameByEmpId(datComponentBO.getCreatedBy());
					else
						name = "You have";
						message = MessageFormat.format(createMessage, name,datComponentBO.getCompHistory());
						viewHistoryOutputBean2.setMessage(message);
						viewHistoryOutputBean2.setCreatedOn(datComponentBO.getCreatedOn());
						viewHistoryOutputBeanLst.add(viewHistoryOutputBean2);
					
				}
			}
			

			break;
		// for milestone
		case 8:

			List<DatMilestoneAuditBO> datMilestoneAuditBOLst = new ArrayList<DatMilestoneAuditBO>();
			List<DatMilestoneAuditBO> datMilestoneAuditBOLstFromAudit = null;
			DatMilestoneAuditBO datMilestoneAuditBO = null;
			DatMilestoneBO datMilestoneBO;
			LOG.info("viewHistoryInputBean.getId()"
					+ viewHistoryInputBean.getId());

			try {
				datMilestoneBO = datMilestoneRepository
						.findByPkMilestoneId(viewHistoryInputBean.getId());
			} catch (Exception e) {
				// e.printStackTrace();
				throw new CommonCustomException("Unable to view the history");
			}

			if (datMilestoneBO == null)
				throw new CommonCustomException("Invalid milestoneId");

			LOG.info("datMilestoneBO" + datMilestoneBO);

			if (datMilestoneBO.getUpdatedBy() == null) {

				if (!viewHistoryInputBean.getViewedBy().equals(
						datMilestoneBO.getCreatedBy()))
					name = gsrNotificationService
							.getEmployeeNameByEmpId(datMilestoneBO
									.getCreatedBy());
				else
					name = "You have";

				String comment = "Milestone " + "-"
						+ datMilestoneBO.getMilestoneName();
				message = MessageFormat.format(createMessage, name, comment);

				ViewHistoryOutputBean viewHistoryOutputBean = new ViewHistoryOutputBean();
				viewHistoryOutputBean.setMessage(message);
				viewHistoryOutputBean.setCreatedOn(datMilestoneBO
						.getCreatedOn());
				viewHistoryOutputBeanLst.add(viewHistoryOutputBean);

			} else {

				if (!viewHistoryInputBean.getViewedBy().equals(
						datMilestoneBO.getUpdatedBy()))
					name = gsrNotificationService
							.getEmployeeNameByEmpId(datMilestoneBO
									.getUpdatedBy());
				else
					name = "You have";
				
				String comment = "Milestone" + " - "
						+ datMilestoneBO.getMilestoneName() + ". ";

				message = MessageFormat.format(updateMessage, name,
						comment+datMilestoneBO.getMilestoneHistory());
				ViewHistoryOutputBean viewHistoryOutputBean = new ViewHistoryOutputBean();
				viewHistoryOutputBean.setMessage(message);
				viewHistoryOutputBean.setCreatedOn(datMilestoneBO
						.getUpdatedOn());
				viewHistoryOutputBeanLst.add(viewHistoryOutputBean);

				Pageable topThreeMile = new PageRequest(0, 4,
						Sort.Direction.DESC, "pkMilestoneAuditId");
				try {
					datMilestoneAuditBOLstFromAudit = datMilestoneAuditRepository
							.findAllUpdatedByFkMilestoneId(
									viewHistoryInputBean.getId(), topThreeMile);
				} catch (Exception e) {
					// e.printStackTrace();
					throw new CommonCustomException(
							"Unable to view the history");
				}

				if (datMilestoneAuditBOLstFromAudit != null) {
					
					for (DatMilestoneAuditBO datMilestoneAuditBo : datMilestoneAuditBOLstFromAudit) {
						datMilestoneAuditBOLst.add(datMilestoneAuditBo);
					}

					if (datMilestoneAuditBOLstFromAudit.size() < 4) {
						try {
							datMilestoneAuditBO = datMilestoneAuditRepository
									.findByFkMilestoneIdAndAction(
											viewHistoryInputBean.getId(),
											"create");
						} catch (Exception e) {
							// e.printStackTrace();
							throw new CommonCustomException(
									"Unable to view the history");
						}
						datMilestoneAuditBOLst.add(datMilestoneAuditBO);
					}

					if (!datMilestoneAuditBOLst.isEmpty()) {
						for (DatMilestoneAuditBO datMilestoneAudit : datMilestoneAuditBOLst) {

							if (!viewHistoryInputBean.getViewedBy().equals(
									datMilestoneAudit.getUpdatedBy()))
								name = gsrNotificationService
										.getEmployeeNameByEmpId(datMilestoneAudit
												.getUpdatedBy());
							else
								name = "You have";

							comment = "Milestone" + " - "
									+ datMilestoneAudit.getMilestoneName();
							
							if (datMilestoneAudit.getAction().equals(
									ProjectConstants.CREATE)) {
								message = MessageFormat.format(createMessage,
										name, comment);
							} else {
								message = MessageFormat
										.format(updateMessage, name,
												comment + ". " + datMilestoneAudit
														.getMilestoneHistory());
							}

							viewHistoryOutputBean = new ViewHistoryOutputBean();

							viewHistoryOutputBean.setMessage(message);
							viewHistoryOutputBean
									.setCreatedOn(datMilestoneAudit
											.getUpdatedOn());
							viewHistoryOutputBeanLst.add(viewHistoryOutputBean);
						}
					}
				}

			}

			break;
		}

		return viewHistoryOutputBeanLst;
	}
	public static void main(String[] args) {
		List<String> i=new ArrayList<String>();
		i.add("a");
		i.add("b");
		i.add("c");
		for (String string : i) {
			System.out.println(string);
			
		}
	}
}
