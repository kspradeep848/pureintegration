package com.thbs.mis.project.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.MasEmpRolesRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.project.bean.GlobalRegionsAndProjectsBean;
import com.thbs.mis.project.bean.StepperInputBean;
import com.thbs.mis.project.bean.StepperOutputBean;
import com.thbs.mis.project.bo.DatComponentBO;
import com.thbs.mis.project.bo.DatWorkorderBO;
import com.thbs.mis.project.bo.MasClientBO;
import com.thbs.mis.project.bo.MasProjectBO;
import com.thbs.mis.project.constants.ProjectConstants;
import com.thbs.mis.project.dao.DatComponentRepository;
import com.thbs.mis.project.dao.DatWorkorderRepository;
import com.thbs.mis.project.dao.MasClientRepository;
import com.thbs.mis.project.dao.ProjDatClientBudgetAtResourceLevelRepository;
import com.thbs.mis.project.dao.ProjDatThbsBudgetAtResourceLevelRepository;
import com.thbs.mis.project.bean.ProjectOverviewBean;
import com.thbs.mis.project.bean.ProjectStatusBean;
import com.thbs.mis.project.dao.MasProjectRepository;
import com.thbs.mis.project.dao.ProjectLevelUserAccessGroupRepository;

@Service
public class DashboardService {

	private static final AppLog LOG = LogFactory.getLog(DashboardService.class);

	@Autowired
	private EmpDetailRepository empDetailRepos;
	@Autowired
	private MasEmpRolesRepository masEmpRolesRepository;
	@Autowired
	private DatEmployeeProfessionalRepository datEmployeeProfessionalRepository;
	@Autowired
	private ProjectLevelUserAccessGroupRepository projectLevelUserAccessGroupRepository;
	@Autowired
	private MasProjectRepository masProjectRepository;
	
	@Autowired
	private DatWorkorderRepository datWorkorderRepository;
	
	@Autowired
	private DatComponentRepository datComponentRepository;
	
	@Autowired
	private MasClientRepository masClientRepository;
	
	@Autowired
	private ProjDatClientBudgetAtResourceLevelRepository projDatClientBudgetAtResourceLevelRepository;
	
	@Autowired
	private ProjDatThbsBudgetAtResourceLevelRepository projDatThbsBudgetAtResourceLevelRepository;
	
	@Value("${project.active.emp.status}")
	private byte activeEmpStatus;

	@Value("${project.coordinator.role.names}")
	private String roleNames;

	// Added by Rinta Mariam Jose
	/**
	 * Description : This service is used to get the number of projects in a
	 * country associated to an employee
	 * 
	 * @param empId
	 * @return List<GlobalRegionsAndProjectsBean>
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	public List<GlobalRegionsAndProjectsBean> getProjectsBasedOnGlobalRegions(
			Integer empId) throws DataAccessException, CommonCustomException {
		LOG.startUsecase("Entering into getProjectsBasedOnGlobalRegions() service");

		List<GlobalRegionsAndProjectsBean> globalRegionsAndProjectsBeanList = new ArrayList<GlobalRegionsAndProjectsBean>();
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBO;
		Set<Integer> projectIdSet = null;
		DatEmpDetailBO datEmpDetailBO;
		List<String> accessRoleNamesList = Arrays.asList(roleNames.split(","));

		try {
			datEmpDetailBO = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException("No such employee id exists :  "
					+ e.getMessage());
		}

		if (datEmpDetailBO == null) {
			throw new CommonCustomException("Invalid employee id.");
		}

		if (!datEmpDetailBO.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException("Inactive employee.");
		try {
			datEmpProfessionalDetailBO = datEmployeeProfessionalRepository
					.findByFkMainEmpDetailId(empId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException("No such employee id exists :  "
					+ e.getMessage());
		}

		if (datEmpProfessionalDetailBO == null)
			throw new CommonCustomException("Invalid employee id.");

		if (accessRoleNamesList.contains(datEmpDetailBO.getMasEmpRoleBO()
				.getEmpRoleKeyName())) {
			try {
				projectIdSet = projectLevelUserAccessGroupRepository
						.getProjectIdByEmpId(empId,
								datEmpProfessionalDetailBO.getFkEmpBuUnit());
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException("Unable to fetch project details");
			}
			if (projectIdSet != null && !projectIdSet.isEmpty()) {
				try {
					globalRegionsAndProjectsBeanList = masProjectRepository
							.getNoOfProjectOnCountry(projectIdSet);
				} catch (Exception e) {
					// e.printStackTrace();
					throw new DataAccessException(
							"Unable to fetch project details");
				}
			}
		} else {
			throw new CommonCustomException("You are not an authorized person ");
		}
		LOG.endUsecase("Exiting getProjectsBasedOnGlobalRegions() service");
		return globalRegionsAndProjectsBeanList;
	}
	// EOA by Rinta Mariam Jose

	//Added by Lalith kumar for getting stepper status
	/**
	 * 
	 * @param stepperInputBean
	 * @return
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	public List<StepperOutputBean> getStepperStatus(StepperInputBean stepperInputBean) throws DataAccessException, CommonCustomException
	{
		LOG.startUsecase("Entering Stepper service...!");
		StepperOutputBean stepperOutputBean = new StepperOutputBean();
		DatWorkorderBO datWorkorderBO;
		DatComponentBO datComponentBO;
		MasClientBO masClientBO;
		MasProjectBO masProjectBO;
				
		switch(stepperInputBean.getType())
		{
		case 1 :	//client
			break;
		case 2 :	//project
			try{
				masClientBO = masClientRepository.findByPkClientId((short)stepperInputBean.getId());
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to get Stepper Status");
			}
			if(masClientBO==null)
			{
				throw new CommonCustomException("Client does not exists....!");
			}
			stepperOutputBean.setClientCreated("Client created");
			break;
		case 3: 	//wo
		    stepperOutputBean.setClientCreated("Client created");
			try{ 
				masProjectBO = masProjectRepository.findByPkProjectId(stepperInputBean.getId());
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to get Stepper Status");
			}
			if(masProjectBO == null)
				throw new CommonCustomException("Project does not exists....!");
		    
		    stepperOutputBean.setProjectCreated("Project created");
			break;
		case 4:		//wo docs
			
			try{
				datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(stepperInputBean.getId());
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to get Stepper Status");
			}
			if(datWorkorderBO==null)
				throw new CommonCustomException("Workorder doesnot exists....!");
			
			stepperOutputBean.setClientCreated("Client created");
			stepperOutputBean.setProjectCreated("Project created");
			stepperOutputBean.setWoCreated("Workorder created");
			
			
			if(datWorkorderBO.getIsInternal().equals("NO"))
			{
			
			if(datWorkorderBO.getSowUploadedFlag().equals("UPLOADED"))
			stepperOutputBean.setSowUploaded("Sow document uploaded");
			stepperOutputBean.setIsInternalWo((byte)0);
			
			}
			else
			{
				stepperOutputBean.setSowUploaded("Not applicable");
				stepperOutputBean.setClientBudgetCreated("Not applicable");
				stepperOutputBean.setThbsBudgetCreated("Not applicable");
				stepperOutputBean.setBudgetApproval("Not applicable");
				stepperOutputBean.setMilestoneCreated("Not applicable");
				stepperOutputBean.setIsInternalWo((byte)1);

			}
			
			break;
		case 5 :	//client budget
			/*try{
				projDatClientBudgetAtResourceLevelBO = projDatClientBudgetAtResourceLevelRepository.getClientBudgetByWoId(stepperInputBean.getId());
				
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to get Stepper Status");
			}*/
			try{
				datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(stepperInputBean.getId());
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to get Stepper Status");
			}
			
			//datWorkorderBO = projDatClientBudgetAtResourceLevelBO.getDatWorkorder();
			if(datWorkorderBO==null)
				throw new CommonCustomException("Workorder doesnot exists....!");
			stepperOutputBean.setClientCreated("Client created");
			stepperOutputBean.setProjectCreated("Project created");
			stepperOutputBean.setWoCreated("Workorder created");
			
			if(datWorkorderBO.getIsInternal().equals("NO"))
			{
			
			if(datWorkorderBO.getSowUploadedFlag().equals("UPLOADED"))
			stepperOutputBean.setSowUploaded("Sow document uploaded");
		//	if(projDatClientBudgetAtResourceLevelBO!=null)
		//	stepperOutputBean.setClientBudgetCreated("Client budget created");

			stepperOutputBean.setIsInternalWo((byte)0);

		
			}
			else
			{
				stepperOutputBean.setSowUploaded("Not applicable");
				stepperOutputBean.setClientBudgetCreated("Not applicable");
				stepperOutputBean.setThbsBudgetCreated("Not applicable");
				stepperOutputBean.setBudgetApproval("Not applicable");
				stepperOutputBean.setMilestoneCreated("Not applicable");
				stepperOutputBean.setIsInternalWo((byte)1);

				
			}
			break;
		case 6 :		//thbs budget
		/*	try{
				projDatThbsBudgetAtResourceLevelBO = projDatThbsBudgetAtResourceLevelRepository.getThbsBudgetByWoId(stepperInputBean.getId());
				
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to get Stepper Status");
			}*/
			
			try{
				datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(stepperInputBean.getId());
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to get Stepper Status");
			}
		//	datWorkorderBO = projDatThbsBudgetAtResourceLevelBO.getDatWorkorder();
			if(datWorkorderBO==null)
				throw new CommonCustomException("Workorder doesnot exists....!");
			
			stepperOutputBean.setClientCreated("Client created");
			stepperOutputBean.setProjectCreated("Project created");
			stepperOutputBean.setWoCreated("Workorder created");
			
			if(datWorkorderBO.getIsInternal().equals("NO"))
			{
			
			if(datWorkorderBO.getSowUploadedFlag().equals("UPLOADED"))
			{
			stepperOutputBean.setSowUploaded("Sow document uploaded");
			
			stepperOutputBean.setIsInternalWo((byte)0);

			stepperOutputBean.setClientBudgetCreated("Client budget created");
			/*if(projDatThbsBudgetAtResourceLevelBO!=null)
			{
				stepperOutputBean.setThbsBudgetCreated("Thbs budget created");
			}*/
			}
			}
			else
			{
				stepperOutputBean.setSowUploaded("Not applicable");
				stepperOutputBean.setClientBudgetCreated("Not applicable");
				stepperOutputBean.setThbsBudgetCreated("Not applicable");
				stepperOutputBean.setBudgetApproval("Not applicable");
				stepperOutputBean.setMilestoneCreated("Not applicable");
				stepperOutputBean.setIsInternalWo((byte)1);


			}
			
			
			break;
		case 7 :		//submission for approval
			try{
				datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(stepperInputBean.getId());
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to get Stepper Status");
			}
			if(datWorkorderBO==null)
				throw new CommonCustomException("Workorder doesnot exists....!");
			
			stepperOutputBean.setClientCreated("Client created");
			stepperOutputBean.setProjectCreated("Project created");
			stepperOutputBean.setWoCreated("Workorder created");
			
			
			if(datWorkorderBO.getIsInternal().equals("NO"))
			{
			if(datWorkorderBO.getSowUploadedFlag().equals("UPLOADED"))
			{
			stepperOutputBean.setSowUploaded("Sow document uploaded");
			stepperOutputBean.setIsInternalWo((byte)0);

			stepperOutputBean.setClientBudgetCreated("Client budget created");
			stepperOutputBean.setThbsBudgetCreated("Thbs budget created");
			}
			}
			else
			{
				stepperOutputBean.setSowUploaded("Not applicable");
				stepperOutputBean.setClientBudgetCreated("Not applicable");
				stepperOutputBean.setThbsBudgetCreated("Not applicable");
				stepperOutputBean.setBudgetApproval("Not applicable");
				stepperOutputBean.setMilestoneCreated("Not applicable");
				stepperOutputBean.setIsInternalWo((byte)1);


				
			}
			
			break;
		case 8 : 		//component 
			
			try{
				datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(stepperInputBean.getId());
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to get Stepper Status");
			}
			if(datWorkorderBO==null)
				throw new CommonCustomException("Workorder doesnot exists....!");
			
			stepperOutputBean.setClientCreated("Client created");
			stepperOutputBean.setProjectCreated("Project created");
			stepperOutputBean.setWoCreated("Workorder created");
			
			if(datWorkorderBO.getIsInternal().equals("NO"))
			{
				if(datWorkorderBO.getSowUploadedFlag().equals("UPLOADED"))
					{
					stepperOutputBean.setSowUploaded("Sow document uploaded");
					stepperOutputBean.setClientBudgetCreated("Client budget created");
					stepperOutputBean.setThbsBudgetCreated("Thbs budget created");
					stepperOutputBean.setBudgetApproval("Budget approved");
					stepperOutputBean.setIsInternalWo((byte)0);
					}
			}
			else
			{
				stepperOutputBean.setSowUploaded("Not applicable");
				stepperOutputBean.setClientBudgetCreated("Not applicable");
				stepperOutputBean.setThbsBudgetCreated("Not applicable");
				stepperOutputBean.setBudgetApproval("Not applicable");
				stepperOutputBean.setMilestoneCreated("Not applicable");
				stepperOutputBean.setIsInternalWo((byte)1);


			}
			break;
		case 9 :		//milestone
			try{
				datComponentBO = datComponentRepository.findByPkComponentId(stepperInputBean.getId());
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to get stepper status");
			}
			
			if(datComponentBO==null)
				throw new CommonCustomException("Component doesnot exists....!");
			datWorkorderBO = datComponentBO.getDatWorkorder();
			stepperOutputBean.setClientCreated("Client created");
			stepperOutputBean.setProjectCreated("Project created");
			stepperOutputBean.setWoCreated("Workorder created");
			
			if(datWorkorderBO.getIsInternal().equals("NO"))
			{

				if(datWorkorderBO.getSowUploadedFlag().equals("UPLOADED"))
					{
					
					stepperOutputBean.setSowUploaded("Sow document uploaded");
					stepperOutputBean.setClientBudgetCreated("Client budget created");
					stepperOutputBean.setThbsBudgetCreated("Thbs budget created");
					stepperOutputBean.setBudgetApproval("Budget approved");
					stepperOutputBean.setIsInternalWo((byte)0);
					}
				stepperOutputBean.setComponentCreated("Component created");
			}else
			{
				stepperOutputBean.setSowUploaded("Not applicable");
				stepperOutputBean.setClientBudgetCreated("Not applicable");
				stepperOutputBean.setThbsBudgetCreated("Not applicable");
				stepperOutputBean.setBudgetApproval("Not applicable");
				stepperOutputBean.setMilestoneCreated("Not applicable");
				stepperOutputBean.setIsInternalWo((byte)1);


			}
			if(datComponentBO!=null)
			{
			stepperOutputBean.setComponentCreated("Component created");
			if(datComponentBO.getFkComponentTypeId()==ProjectConstants.COMPONENT_TYPE_ID_OTHERS)
				stepperOutputBean.setMilestoneCreated("Not applicable");
			else
				stepperOutputBean.setMilestoneCreated("Milstone created");
			}
			
			break;
			
		}
		
		List<StepperOutputBean> stepperOutputBeanList = new ArrayList<StepperOutputBean>();
		stepperOutputBeanList.add(stepperOutputBean);
		return stepperOutputBeanList;
	}
	//EOA by Lalith kumar

	

	// Added by Rinta Mariam Jose
	/**
	 * Description : This service is used to get project overview
	 * 
	 * @param empId
	 * @return ProjectOverviewBean
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	public List<ProjectOverviewBean> getProjectsOverview(Integer empId)
			throws DataAccessException, CommonCustomException {
		LOG.startUsecase("Entering into getProjectsBasedOnGlobalRegions() service");

		List<ProjectOverviewBean> projectOverviewBeanList = new ArrayList<ProjectOverviewBean>();
		ProjectOverviewBean projectOverviewBean = new ProjectOverviewBean();
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBO;
		Set<Integer> projectIdSet = null;
		DatEmpDetailBO datEmpDetailBO;
		List<String> accessRoleNamesList = Arrays.asList(roleNames.split(","));

		try {
			datEmpDetailBO = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException("No such employee id exists :  "
					+ e.getMessage());
		}

		if (datEmpDetailBO == null) {
			throw new CommonCustomException("Invalid employee id.");
		}

		if (!datEmpDetailBO.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException("Inactive employee.");
		try {
			datEmpProfessionalDetailBO = datEmployeeProfessionalRepository
					.findByFkMainEmpDetailId(empId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException("No such employee id exists :  "
					+ e.getMessage());
		}

		if (datEmpProfessionalDetailBO == null)
			throw new CommonCustomException("Invalid employee id.");

		if (accessRoleNamesList.contains(datEmpDetailBO.getMasEmpRoleBO()
				.getEmpRoleKeyName())) {
			try {
				projectIdSet = projectLevelUserAccessGroupRepository
						.getProjectIdByEmpId(empId,
								datEmpProfessionalDetailBO.getFkEmpBuUnit());
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException("Unable to fetch project details");
			}
			if (projectIdSet != null && !projectIdSet.isEmpty()) {
				try {
					projectOverviewBean = masProjectRepository
							.getProjectOverview(projectIdSet);
				} catch (Exception e) {
					// e.printStackTrace();
					throw new DataAccessException(
							"Unable to fetch project details");
				}
				if (projectOverviewBean != null) {
					projectOverviewBean.setTotalEmployee((long) 0);
					projectOverviewBean
							.setProjectLink("https://projects.invisionapp.com/share/3ZG5XOJJ8E9#/screens/280586513");
					projectOverviewBean
							.setWorkOrderLink("https://projects.invisionapp.com/share/3ZG5XOJJ8E9#/screens/280586513");
					projectOverviewBean
							.setComponentLink("https://projects.invisionapp.com/share/3ZG5XOJJ8E9#/screens/280586513");
					projectOverviewBean
							.setMilestoneLink("https://projects.invisionapp.com/share/3ZG5XOJJ8E9#/screens/280586513");
					projectOverviewBean.setTotalEmployee(null);
					projectOverviewBean
							.setEmployeeLink("https://projects.invisionapp.com/share/3ZG5XOJJ8E9#/screens/280586513");
					projectOverviewBeanList.add(projectOverviewBean);
				}
			}
		} else {
			throw new CommonCustomException("You are not an authorized person ");
		}
		LOG.endUsecase("Exiting getProjectsBasedOnGlobalRegions() service");
		return projectOverviewBeanList;
	}

	/**
	 * Description : This service is used to get project status details.
	 * 
	 * @param empId
	 * @return List<ProjectStatusBean>
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	public List<ProjectStatusBean> getProjectStatus(Integer empId)
			throws DataAccessException, CommonCustomException {
		LOG.startUsecase("Entering into getProjectStatus() service");

		List<ProjectStatusBean> projectStatusBeanList = null;
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBO;
		Set<Integer> projectIdSet = null;
		DatEmpDetailBO datEmpDetailBO;
		List<String> accessRoleNamesList = Arrays.asList(roleNames.split(","));

		try {
			datEmpDetailBO = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException("No such employee id exists :  "
					+ e.getMessage());
		}

		if (datEmpDetailBO == null) {
			throw new CommonCustomException("Invalid employee id.");
		}

		if (!datEmpDetailBO.getFkEmpMainStatus().equals(activeEmpStatus))
			throw new CommonCustomException("Inactive employee.");
		try {
			datEmpProfessionalDetailBO = datEmployeeProfessionalRepository
					.findByFkMainEmpDetailId(empId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException("No such employee id exists :  "
					+ e.getMessage());
		}

		if (datEmpProfessionalDetailBO == null)
			throw new CommonCustomException("Invalid employee id.");

		if (accessRoleNamesList.contains(datEmpDetailBO.getMasEmpRoleBO()
				.getEmpRoleKeyName())) {
			try {
				projectIdSet = projectLevelUserAccessGroupRepository
						.getProjectIdByEmpId(empId,
								datEmpProfessionalDetailBO.getFkEmpBuUnit());
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException("Unable to fetch project details");
			}
			if (projectIdSet != null && !projectIdSet.isEmpty()) {
				try {
					projectStatusBeanList = masProjectRepository
							.getProjectStatus(projectIdSet);
				} catch (Exception e) {
					// e.printStackTrace();
					throw new DataAccessException(
							"Unable to fetch project details");
				}
				if (projectStatusBeanList != null) {
					for (ProjectStatusBean projectStatusBean : projectStatusBeanList) {
						projectStatusBean
								.setLink("https://projects.invisionapp.com/share/3ZG5XOJJ8E9#/screens/280586513");
					}
				}
			}
		} else {
			throw new CommonCustomException("You are not an authorized person ");
		}
		LOG.endUsecase("Exiting getProjectStatus() service");
		return projectStatusBeanList;
	}
}
