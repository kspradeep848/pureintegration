package com.thbs.mis.project.service;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.projectnotification.service.ProjectNotificationService;
import com.thbs.mis.project.bean.CompNoMilestoneAndMilestoneSumBean;
import com.thbs.mis.project.bean.ComponentDetailsOutputBean;
import com.thbs.mis.project.bean.ComponentDetailsOutputBean1;
import com.thbs.mis.project.bean.ComponentDetailsOutputBean2;
import com.thbs.mis.project.bean.ComponentInputBean;
import com.thbs.mis.project.bean.ComponentOutputBean;
import com.thbs.mis.project.bean.ComponentOwnerBean;
import com.thbs.mis.project.bean.SearchComponentInputBean;
import com.thbs.mis.project.bo.ComponentSpecificationDetails;
import com.thbs.mis.project.bo.DatComponentBO;
import com.thbs.mis.project.bo.DatWorkorderBO;
import com.thbs.mis.project.bo.ProjMasCompStatusBO;
import com.thbs.mis.project.constants.ProjectConstants;
import com.thbs.mis.project.controller.ComponentController;
import com.thbs.mis.project.dao.DatComponentRepository;
import com.thbs.mis.project.dao.DatMilestoneRepository;
import com.thbs.mis.project.dao.DatWorkorderRepository;
import com.thbs.mis.project.dao.ProjDatProjectMappedToProjectCoordinatorRepository;
import com.thbs.mis.project.dao.ProjMasCompStatusRepository;
@Service
public class ComponentService {
	
	private static final AppLog LOG = LogFactory
			.getLog(ComponentController.class);
	
	@Autowired
	private DatComponentRepository componentRepository;
	
	/*@Autowired
	private ComponentSpecificationDetails componentSpecification;*/
	
	@Autowired
	private DatWorkorderRepository datWorkorderRepository;
	
	@Autowired
	private EmpDetailRepository empDetailRepos;
	
	@Autowired
	private MilestoneService milestoneService;
	
	@Autowired
	private DatMilestoneRepository datMilestoneRepository;
	
	@Autowired
	private ProjMasCompStatusRepository projMasCompStatusRepository;
	
	@Autowired
	private ProjDatProjectMappedToProjectCoordinatorRepository projectMappedToCoordinatorRepos;
	
	@Autowired
	private ProjectNotificationService projectNotificationService;
	
	@Value("${project.message.specification.from.to}")
	private String specificationFromTo;

	
	@Value("${project.component.owner.level.names}")
	private String projectComponentOwnersListLevelNames;
	
	@Value("${project.history.pattern}")
	private String projectHistoryCommentsPattern;

	
	
	public ComponentOutputBean createComponent(ComponentInputBean inputBean) throws CommonCustomException, DataAccessException{
		LOG.startUsecase("ComponentService ==>> createComponent()");
		
		DatComponentBO compBoExists = new DatComponentBO();
		DatComponentBO compBo = new DatComponentBO();
		DatWorkorderBO datWorkorderBO;
		DatEmpDetailBO datEmpDetailBO;
		List<Integer> accessList;
		
		if(inputBean.getCompCreatedBy()==null)
			throw new CommonCustomException("CreatedBy cannot be null");
		try{
			datEmpDetailBO = empDetailRepos.findByPkEmpId(inputBean.getCompCreatedBy());
		}catch(Exception e)
		{
			throw new DataAccessException("unable to LoggedIn user details");
		}
		
		if(datEmpDetailBO==null)
			throw new CommonCustomException("LoggedIn user does not exists..!");
		
		try{
			accessList = projectMappedToCoordinatorRepos.getAllPcRelatedToProject(inputBean.getWoId());
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to create component");
		}
		
		if(!accessList.contains(inputBean.getCompCreatedBy()))
			throw new CommonCustomException("You are not authorized person to create component");
		
		try{
			datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(inputBean.getWoId());
		}catch(Exception e)
		{
		//	e.printStackTrace();
			throw new DataAccessException("Failed to create Component");
		}
		if(datWorkorderBO == null)
		{
			throw new CommonCustomException("Workorder does not exists.");
		}
		
		if(inputBean.getCompStartDate().before(datWorkorderBO.getWoStartDate()))
				throw new CommonCustomException("Component startdate cannot be after Workorder startdate.");
		if( inputBean.getCompEndDate().after(datWorkorderBO.getWoEndDate()))
			throw new CommonCustomException("Component enddate cannot be after Workorder enddate.");
		
		//Checking component name already exists for that workorder
		try{
			compBoExists = componentRepository.findByFkWorkorderIdAndComponentName(inputBean.getWoId(), inputBean.getCompName());
		}catch(Exception e)
		{
		//	e.printStackTrace();
			throw new DataAccessException("Failed to create component");
		}
		if(compBoExists!=null)
		{
			throw new CommonCustomException("Component name already exists for that workorder");
		}
		
		
		compBo.setCompDescription(inputBean.getCompDescription());
		compBo.setComponentName(inputBean.getCompName());
		
		compBo.setCreatedBy(inputBean.getCompCreatedBy());
		compBo.setFkWorkorderId(inputBean.getWoId());
		compBo.setIsSubprojectOrComponent(inputBean.getIsComponentOrSubProject());
		
		//checking whether Wo is internal
		if(datWorkorderBO.getIsInternal().equals("YES")){
			if(inputBean.getCompTypeId()!= ProjectConstants.COMPONENT_TYPE_ID_OTHERS )
				throw new CommonCustomException("For Internal Wo Component TypeId can only be 'Others'");
			compBo.setFkComponentTypeId(inputBean.getCompTypeId());
		}
		else
		{
			if(inputBean.getCompTypeId()== ProjectConstants.COMPONENT_TYPE_ID_OTHERS )
				throw new CommonCustomException("For non-internal Wo Component TypeId cannot be 'Others'");
			
			
			// Billing rates Available/not-Available should be checked after getting clarity. 			
			compBo.setFkComponentTypeId(inputBean.getCompTypeId());
			
		}
			//compBo.setFkComponentStatusId((short)inputBean.getCompStatusId());
			compBo.setCreatedOn(new Date());
			compBo.setCompEndDate(inputBean.getCompEndDate());
			compBo.setCompStartDate(inputBean.getCompStartDate());
			if(compBo.getCompStartDate().after(compBo.getCompEndDate())){
				throw new CommonCustomException("Invalid Component Date Range");
			}
			//checking woBal and component value
			if(inputBean.getCompTypeId()!=ProjectConstants.COMPONENT_TYPE_ID_OTHERS)
			{
			BigDecimal sumWo = new BigDecimal(0);
			try{
				sumWo =	componentRepository.getSumOfComponentAmount(inputBean.getWoId());
			}catch(Exception e)
			{
				throw new DataAccessException("Unable to create component");
			}
			LOG.info("sumWo"+sumWo);
			LOG.info("datWorkorderBO.getSowAmount()"+datWorkorderBO.getSowAmount());
			LOG.info("compBo.getComponentAmount()"+inputBean.getCompAmount());
		//	LOG.info("datWorkorderBO.getSowAmount().subtract(sumWo)"+datWorkorderBO.getSowAmount().subtract(sumWo));
			if(sumWo!=null){
			if(((datWorkorderBO.getSowAmount().subtract(sumWo)).compareTo(inputBean.getCompAmount()))<0)
				throw new CommonCustomException("Component amount cannot exceed workorder balance amount");
			}
			}
			switch(inputBean.getCompTypeId())
				{
				case ProjectConstants.COMPONENT_TYPE_ID_FIXED : 
					//checking Component amount is greater than zero or not
					if(inputBean.getCompAmount() == null || !(inputBean.getCompAmount().compareTo(new BigDecimal(0))==1))
						throw new CommonCustomException("Component value should be greater than zero.");
					compBo.setComponentAmount(inputBean.getCompAmount());
					
					if((new Date().after(compBo.getCompStartDate()) && (new Date().before(compBo.getCompEndDate())))){
						compBo.setFkComponentStatusId((short)1);
					}
					else if((new Date().before(compBo.getCompStartDate()) && (new Date().before(compBo.getCompEndDate())))){
						compBo.setFkComponentStatusId((short)2);
					}
					
					break;
				case ProjectConstants.COMPONENT_TYPE_ID_OTHERS :
					if(!(inputBean.getCompAmount().compareTo(new BigDecimal(0))==0))
						throw new CommonCustomException("Amount should be zero for internal component");
					compBo.setFkComponentStatusId(inputBean.getCompStatusId());
					compBo.setComponentAmount(inputBean.getCompAmount());
					break;
				case ProjectConstants.COMPONENT_TYPE_ID_TNM :
					//checking Component amount is greater than zero or not
					if(inputBean.getCompAmount() == null || !(inputBean.getCompAmount().compareTo(new BigDecimal(0))==1))
						throw new CommonCustomException("Component value should be greater than zero.");
					
					compBo.setComponentAmount(inputBean.getCompAmount());
					
					compBo.setFkComponentStatusId(inputBean.getCompStatusId());
				break;	
				default:
						throw  new CommonCustomException("Enter valid status id.");
				}
		
		try{
			compBo = componentRepository.save(compBo);
		}catch(Exception e)
		{
		//e.printStackTrace();
			throw new DataAccessException("Failed to create component...!");
		}
		if(compBo != null){
			List<DatComponentBO> allComponents =  componentRepository.findByFkWorkorderId(compBo.getFkWorkorderId());
			
			byte priorityStatus = 0;
			if(allComponents.size() > 2){
				for(int i = 0; i < allComponents.size(); i++){
					if(i == 0){
						priorityStatus = (byte) allComponents.get(0).getFkComponentStatusId();
					}
					if(priorityStatus > allComponents.get(i).getFkComponentStatusId()){
						priorityStatus = (byte) allComponents.get(i).getFkComponentStatusId();
					}
				}
			}
			try{
				milestoneService.updateWorkOrderStatus(datWorkorderBO.getPkWorkorderId());
				}catch(Exception e)
				{
					throw new CommonCustomException(e.getMessage());
				}
				
			}
		
		try{
			projectNotificationService.sendComponentCreatedNotificationToAllPC(compBo);
		}catch(Exception e)
		{
			throw new CommonCustomException(e.getMessage());
		}
		
		//Setting CreateComponent Output Bean
		ComponentOutputBean ComponentOutputBean = new ComponentOutputBean();
		ComponentOutputBean.setCompId(compBo.getPkComponentId());
		ComponentOutputBean.setCompName(compBo.getComponentName());
		ComponentOutputBean.setWoId(compBo.getFkWorkorderId());
	
	
		return ComponentOutputBean;
			
	}
	
	public Page<ComponentDetailsOutputBean2> searchComponent(SearchComponentInputBean searchComponentInput) throws CommonCustomException, DataAccessException{
		Page<ComponentDetailsOutputBean2> finalPage;
		Page<DatComponentBO> datComponentBOLst = null;
		List<ComponentDetailsOutputBean2> outputBeanList = new ArrayList<ComponentDetailsOutputBean2>();
		Specification<DatComponentBO> datComp = null;
		datComp = ComponentSpecificationDetails.searchComponents(searchComponentInput);
		if(searchComponentInput.getPageSize()<=0)
			throw new CommonCustomException("Page size should be greater than zero");
		PageRequest pageable = new PageRequest(searchComponentInput.getPageNumber(),
				searchComponentInput.getPageSize(), Sort.Direction.DESC, "createdOn");
		try {
			datComponentBOLst = componentRepository.findAll(datComp,pageable);
		} catch (Exception e) {
			throw new DataAccessException("Something went wrong while fetching");
		}
		if ( datComponentBOLst!= null) {
			for (DatComponentBO bo : datComponentBOLst) {
				ComponentDetailsOutputBean2 bean = new ComponentDetailsOutputBean2();
				bean.setComponentId(bo.getPkComponentId());
				bean.setComponentName(bo.getComponentName());
				bean.setNoOfResources(01);
				bean.setNoOfMileStones(0);
				bean.setComponentValue(bo.getComponentAmount());
				bean.setComponentStatus(bo.getProjMasCompStatus().getComponentStatus());
				bean.setComponentType(bo.getProjMasCompType().getComponentTypeName());
				bean.setComponentBalance(new BigDecimal(1));

				outputBeanList.add(bean);
			}
		}
		finalPage = new PageImpl<ComponentDetailsOutputBean2>(outputBeanList, pageable,
				outputBeanList.size());
		return finalPage;
		
	}

	public ComponentDetailsOutputBean componentFullView(
			Short componentId) throws CommonCustomException, DataAccessException{
		ComponentDetailsOutputBean outputBean = new ComponentDetailsOutputBean();
		DatComponentBO bo;
		CompNoMilestoneAndMilestoneSumBean compNoMilestoneAndMilestoneSumBean;
		
		try{
			bo = componentRepository.findByPkComponentId((int)componentId);
		}catch(Exception e)
		{
			throw new DataAccessException("Fail to fetch component details");
		}
		
		if(bo!=null){
			outputBean.setComponentId(bo.getPkComponentId());
			outputBean.setComponentName(bo.getComponentName());
			outputBean.setComponentValue(bo.getComponentAmount());
			outputBean.setComponentStatus(bo.getProjMasCompStatus().getComponentStatus());
			outputBean.setComponentType(bo.getProjMasCompType().getComponentTypeName());
			outputBean.setComponentStartDate(bo.getCompStartDate());
			outputBean.setComponentEndDate(bo.getCompEndDate());
			outputBean.setComponentOwnerId(1340);
			outputBean.setComponentOwnerName(1340);
			try{
				compNoMilestoneAndMilestoneSumBean = datMilestoneRepository.getSumOfMilesStonesOnCompId((int)componentId);
			}catch(Exception e)
			{
				//e.printStackTrace();
				throw new CommonCustomException("Fail to fetch component details...!");
			}
			outputBean.setNoOfResources(1);
			if(compNoMilestoneAndMilestoneSumBean!=null)
				outputBean.setNoOfMileStones(compNoMilestoneAndMilestoneSumBean.getNoOfMilesdtones());
				if(compNoMilestoneAndMilestoneSumBean.getSunOfMilestones()!=null)
					outputBean.setComponentBalance(bo.getComponentAmount().subtract(compNoMilestoneAndMilestoneSumBean.getSunOfMilestones()));
				else
					outputBean.setComponentBalance(bo.getComponentAmount());
		}else
		{
			throw new CommonCustomException("Component does not exists..!");
		}
		
		System.out.println("returning from searchComponentOnCompId()==>> service == >> size ==>> "+outputBean);
		return outputBean;
	}

	@Transactional
	public ComponentOutputBean updateComponent(ComponentInputBean updateBean) throws CommonCustomException, DataAccessException{
		
		DatComponentBO compBo = new DatComponentBO();
		DatComponentBO compUpdated = null;
		DatWorkorderBO datWorkorderBO;
		ComponentOutputBean updateCompOutBean;
		ProjMasCompStatusBO projMasCompStatusBO;
		
		DatEmpDetailBO datEmpDetailBO;
		List<Integer> accessList;
		StringBuilder updateComments = new StringBuilder();
		
		if(updateBean.getCompUpdatedBy()== null)
			throw new CommonCustomException("UpdatedBy cannot be null");
		
		if(updateBean.getCommentsForUpdate()== null)
				throw new CommonCustomException("Comments for update cannot be null");
			
		
		try{
			compBo = componentRepository.getcompIdDetails(updateBean.getCompId());
		}catch(Exception e)
		{
			throw new DataAccessException("Failed to update component...!");
		}
		
		if(compBo!=null){
			
		try{
			datEmpDetailBO = empDetailRepos.findByPkEmpId(updateBean.getCompUpdatedBy());
		}catch(Exception e)
		{
			throw new DataAccessException("unable to LoggedIn user details");
		}
		
		if(datEmpDetailBO==null)
			throw new CommonCustomException("LoggedIn user does not exists..!");
		
		try{
			accessList = projectMappedToCoordinatorRepos.getAllPcRelatedToProject(compBo.getFkWorkorderId());
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to update component");
		}
		
		if(!accessList.contains(updateBean.getCompUpdatedBy()))
			throw new CommonCustomException("You are not authorized person to update component");
		LOG.info("CompBo"+compBo);
		if(!compBo.getComponentName().equals(updateBean.getCompName()))
		{
		try{
			compUpdated = componentRepository.findByFkWorkorderIdAndComponentName(updateBean.getWoId(), updateBean.getCompName());
		}catch(Exception e)
		{
		//	e.printStackTrace();
			throw new DataAccessException("Failed to update component");
		}
		
		LOG.info("component name already exists....!"+compUpdated);
		if(compUpdated!=null)
			throw new CommonCustomException("Component name already exists..!");
		}
		
		if(updateBean.getCompTypeId()!= compBo.getFkComponentTypeId())
			throw new CommonCustomException("Component type cannot be changed.");
		
		
		try{
			projMasCompStatusBO = projMasCompStatusRepository.findByPkComponentStatusId(updateBean.getCompStatusId());
		}catch(Exception e)
		{
			throw new DataAccessException("unable to update component");
		}
		
		if(projMasCompStatusBO==null)
		{
			throw new CommonCustomException("Enter valid statusId");
		}

		if(!updateBean.getCompName().equals(compBo.getComponentName()))
			updateComments.append(MessageFormat.format(specificationFromTo,"Component name"
					,compBo.getComponentName(),updateBean.getCompName()));
		
		if(compBo.getFkComponentTypeId()!= ProjectConstants.COMPONENT_TYPE_ID_OTHERS)
		{
		if(updateBean.getCompAmount()==null)
			throw new CommonCustomException("Component amount cannot be null.");
		if(!(updateBean.getCompAmount().compareTo(compBo.getComponentAmount())==0))
			updateComments.append(MessageFormat.format(specificationFromTo,"Component amount"
					,compBo.getComponentAmount(),updateBean.getCompAmount()));
		}

		if(updateBean.getCompStartDate().after(updateBean.getCompEndDate()))
		{
			throw new CommonCustomException("Component start date should not be after component end date.");
		}
		
		
        	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        	String formattedNewStartDate = formatter.format(updateBean.getCompStartDate());
        	String formattedOldStartDate = formatter.format(compBo.getCompStartDate());
        	String formattedNewEndDate = formatter.format(updateBean.getCompEndDate());
        	String formattedOldEndDate = formatter.format(compBo.getCompEndDate());

        	LOG.info(formattedNewStartDate);
        	LOG.info(formattedOldStartDate);
        	
		if(!formattedNewStartDate.equals(formattedOldStartDate))
			updateComments.append(MessageFormat.format(specificationFromTo,"StartDate"
					,formattedOldStartDate,formattedNewStartDate));
		
		if(!formattedNewEndDate.equals(formattedOldEndDate))
			updateComments.append(MessageFormat.format(specificationFromTo,"EndDate"
					,formattedOldEndDate,formattedNewEndDate));
		
		if(!updateBean.getCompDescription().equals(compBo.getCompDescription()))
			updateComments.append(MessageFormat.format(specificationFromTo,"Component description"
					,compBo.getCompDescription(),updateBean.getCompDescription()));
		
		if(compBo.getFkComponentTypeId()!=ProjectConstants.COMPONENT_TYPE_ID_FIXED)
		{
		if(updateBean.getCompStatusId()!=compBo.getFkComponentStatusId())
			updateComments.append(MessageFormat.format(specificationFromTo,"Component status"
					,compBo.getProjMasCompStatus().getComponentStatus(),projMasCompStatusBO.getComponentStatus()));
		}		
	
		datWorkorderBO = compBo.getDatWorkorder();
		
		//checking woBal and component value
		if(updateBean.getCompTypeId()!=ProjectConstants.COMPONENT_TYPE_ID_OTHERS)
		{
		BigDecimal sumWo = new BigDecimal(0);
		try{
			sumWo =	componentRepository.getSumOfComponentAmount(updateBean.getWoId());
		}catch(Exception e)
		{
			throw new DataAccessException("Unable to create component");
		}
		LOG.info("sumWo"+sumWo);
		LOG.info("datWorkorderBO.getSowAmount()"+datWorkorderBO.getSowAmount());
		LOG.info("compBo.getComponentAmount()"+updateBean.getCompAmount());
	//	LOG.info("datWorkorderBO.getSowAmount().subtract(sumWo)"+datWorkorderBO.getSowAmount().subtract(sumWo));
		if(sumWo!=null){
		if(((datWorkorderBO.getSowAmount().subtract(sumWo)).compareTo(updateBean.getCompAmount()))<0)
			throw new CommonCustomException("Component amount cannot exceed workorder balance amount");
		}
		}
		
		compBo.setCompDescription(updateBean.getCompDescription());
		compBo.setUpdatedBy(updateBean.getCompUpdatedBy());
		compBo.setUpdatedOn(new Date());
		compBo.setComponentName(updateBean.getCompName());
		compBo.setCommentsForUpdation(updateBean.getCommentsForUpdate());
		compBo.setFkComponentStatusId(updateBean.getCompStatusId());
		compBo.setCompHistory(new  String(updateComments));

		switch(updateBean.getCompTypeId())	
		{
		case ProjectConstants.COMPONENT_TYPE_ID_FIXED : 
		if((new Date().after(compBo.getCompStartDate()) && (new Date().before(compBo.getCompEndDate())))){
			compBo.setFkComponentStatusId((short)1);
		}
		else if((new Date().before(compBo.getCompStartDate()) && (new Date().before(compBo.getCompEndDate())))){
			compBo.setFkComponentStatusId((short)2);
		}
		if(updateBean.getCompAmount()==null)
			throw new CommonCustomException("Component amount cannot be zero");
		compBo.setComponentAmount(updateBean.getCompAmount());
		break;
		
		case ProjectConstants.COMPONENT_TYPE_ID_TNM :

			if(updateBean.getCompAmount()==null)
				throw new CommonCustomException("Component amount cannot be zero");
			compBo.setComponentAmount(updateBean.getCompAmount());
			break;
		}
		
		if(updateBean.getCompStatusId() == ProjectConstants.COMPONENT_DISCARDED )
		{	compBo.setFkComponentStatusId(updateBean.getCompStatusId());
			compBo.setComponentAmount(new BigDecimal(0));
		}	
		
		if(updateBean.getCompStartDate().before(datWorkorderBO.getWoStartDate()))
			throw new CommonCustomException("Component start date should be after Workorder start date.");
		if( updateBean.getCompEndDate().after(datWorkorderBO.getWoEndDate()))
			datWorkorderBO.setWoEndDate(updateBean.getCompEndDate());

		compBo.setCompStartDate(updateBean.getCompStartDate());
		compBo.setCompEndDate(updateBean.getCompEndDate());
	
		
		try{
		 compUpdated = componentRepository.save(compBo);
		}catch(Exception e)
		{
		//	e.printStackTrace();
		throw new DataAccessException("Failed to update component...!");	
		}
		try{
			datWorkorderBO = datWorkorderRepository.save(datWorkorderBO);
		}catch(Exception e)
		{
		//e.printStackTrace();
			throw new DataAccessException("Failed to update component");
		}
		if(compUpdated.getFkComponentStatusId()==ProjectConstants.COMPONENT_WORK_COMPLETED)
		{
		try{
		projectNotificationService.sendComponentClosureNotificationToAllPC(compBo);	
		}catch(Exception e)
		{
			throw new DataAccessException("Failed to update component");
		}
		}
		
		try{
		milestoneService.updateWorkOrderStatus(datWorkorderBO.getPkWorkorderId());
		}catch(Exception e)
		{
			throw new CommonCustomException(e.getMessage());
		}
		try{
			projectNotificationService.sendComponentUpdatedNotificationToAllPC(compUpdated);
		}catch(Exception e)
		{
			throw new CommonCustomException(e.getMessage());
		}
		
		updateCompOutBean = new ComponentOutputBean();
		updateCompOutBean.setCompId(compUpdated.getPkComponentId());
		updateCompOutBean.setCompName(compUpdated.getComponentName());
		updateCompOutBean.setWoId(compUpdated.getFkWorkorderId());

	}else
		throw new CommonCustomException("No records found with that componentId");
		return updateCompOutBean;
	}

	public ComponentDetailsOutputBean1 SearchComponent(int compId) throws CommonCustomException, DataAccessException{

		DatComponentBO compBo;
		ComponentDetailsOutputBean1 bean=null;
		CompNoMilestoneAndMilestoneSumBean compNoMilestoneAndMilestoneSumBean;
		try{
		 compBo = componentRepository.findByPkComponentId(compId);
		}catch(Exception e)
		{
			throw new DataAccessException("Failed to fetch Component details...");
		}
		if(compBo!=null)
		{
		bean = new ComponentDetailsOutputBean1();
		bean.setCompId(compBo.getPkComponentId());
		bean.setCompDescription(compBo.getCompDescription());
		bean.setCompStartDate(compBo.getCompStartDate().toString());
		bean.setCompEndDate(compBo.getCompEndDate().toString());
		bean.setCompAmount(compBo.getComponentAmount());
		bean.setCompName(compBo.getComponentName());
		bean.setCompStatusId(compBo.getFkComponentStatusId());
		bean.setCompStatusName(compBo.getProjMasCompStatus().getComponentStatus());
		bean.setCompTypeId(compBo.getFkComponentTypeId());
		bean.setCompTypeName(compBo.getProjMasCompType().getComponentTypeName());
		bean.setWoId(compBo.getFkWorkorderId());
		bean.setCreatedBy(compBo.getCreatedBy());
		bean.setCompOwner(compBo.getCreatedBy());
		bean.setCompOwnerName(null);
		if(compBo.getCommentsForUpdation()!=null)
		{
		List<String> comments = Arrays.asList(compBo.getCommentsForUpdation().split(projectHistoryCommentsPattern));
		bean.setCommentsForUpdate(comments.get(comments.size()-1));
		}
		try{
			compNoMilestoneAndMilestoneSumBean = datMilestoneRepository.getSumOfMilesStonesOnCompId(compId);
		}catch(Exception e)
		{
			//e.printStackTrace();
			throw new CommonCustomException("Fail to fetch component details...!");
		}
	
		if(compNoMilestoneAndMilestoneSumBean!=null)
		bean.setNoOfMilestone(compNoMilestoneAndMilestoneSumBean.getNoOfMilesdtones().intValue());
		if(compNoMilestoneAndMilestoneSumBean.getSunOfMilestones()!=null)
			bean.setCompBalance(compBo.getComponentAmount().subtract(compNoMilestoneAndMilestoneSumBean.getSunOfMilestones()));
		else
			bean.setCompBalance(compBo.getComponentAmount());
		
		bean.setNoOfMilestoneTriggered(20);
		bean.setNoOfResource(15);
		}else
			throw new CommonCustomException("Component does not exists..!");
		
		return bean;
	}
	
	public List<ComponentOwnerBean> getAllComponentOwners(Short buId) throws CommonCustomException,DataAccessException
	{
		
		List<ComponentOwnerBean> componentOwnerBeanList = new ArrayList<ComponentOwnerBean>();
		List<DatEmpDetailBO> componentOwnerList;
		try {
			List<String> empLevelNameList = Arrays.asList(projectComponentOwnersListLevelNames.split(","));
			
			componentOwnerList = empDetailRepos.getAllMgrDetails(empLevelNameList,buId);
		} catch (Exception e) {
			
			throw new DataAccessException("Failed to retrieve list of direct reportees :  ", e);
		}

		if (!componentOwnerList.isEmpty()) {
			for (DatEmpDetailBO empBO : componentOwnerList) {
				ComponentOwnerBean bean = new ComponentOwnerBean();
				bean.setCompOwnerId(empBO.getPkEmpId());
				if (empBO.getEmpMiddleName() == null) {
					bean.setCompOwnerName(empBO.getEmpFirstName() + " " + empBO.getEmpLastName());
				} else {
					bean.setCompOwnerName(
							empBO.getEmpFirstName() + " " + empBO.getEmpMiddleName() + " " + empBO.getEmpLastName());
				}
				componentOwnerBeanList.add(bean);
			}
		
			Collections.sort(componentOwnerBeanList, new ComponentOwnerBean());
		}else
			throw new CommonCustomException("No records found....!");
		return componentOwnerBeanList;
	}

}
