package com.thbs.mis.project.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasEmpDesignationBO;
import com.thbs.mis.common.bo.MasEmpLevelBO;
import com.thbs.mis.common.bo.MasEmpRoleBO;
import com.thbs.mis.common.bo.MasLocationParentBO;
import com.thbs.mis.common.dao.DesignationRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.LevelRepository;
import com.thbs.mis.common.dao.MasEmpRolesRepository;
import com.thbs.mis.common.dao.ParentLocationRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.project.bean.BudgetDetailsOutputBean;
import com.thbs.mis.project.bean.CliBudgetDesignationListAndAdditionalCostOutputBean;
import com.thbs.mis.project.bean.ClientBudgetBean;
import com.thbs.mis.project.bean.ClientBudgetDesignationOutputBean;
import com.thbs.mis.project.bean.ClientBudgetInputBean;
import com.thbs.mis.project.bean.ClientBudgetSummaryBean;
import com.thbs.mis.project.bean.CompanyBudgetBean;
import com.thbs.mis.project.bean.CompanyBudgetSummaryBean;
import com.thbs.mis.project.bean.HolidayDetailsBean;
import com.thbs.mis.project.bean.InvoiceRateTypeBean;
import com.thbs.mis.project.bean.ProjDatThbsBudgetAtResourceLevelBean;
import com.thbs.mis.project.bean.ProjDatThbsBudgetForAdditionalCostBean;
import com.thbs.mis.project.bean.ProjMasClientBudgetDetailsBean;
import com.thbs.mis.project.bean.ProjMasClientBudgetSummaryBean;
import com.thbs.mis.project.bean.ProjMasCompanyBudgetDetailsBean;
import com.thbs.mis.project.bean.ProjMasCompanyBudgetSummaryBean;
import com.thbs.mis.project.bean.RateDetailsBean;
import com.thbs.mis.project.bean.WeeklyOffBean;
import com.thbs.mis.project.bo.DatWorkorderBO;
import com.thbs.mis.project.bo.MasProjectBO;
import com.thbs.mis.project.bo.ProjDatClientBudgetAtResourceLevelBO;
import com.thbs.mis.project.bo.ProjDatClientBudgetBO;
import com.thbs.mis.project.bo.ProjDatClientBudgetForAdditionalCostBO;
import com.thbs.mis.project.bo.ProjDatClientBudgetRateDetailBO;
import com.thbs.mis.project.bo.ProjDatProjectMappedToProjectCoordinatorBO;
import com.thbs.mis.project.bo.ProjDatThbsBudgetAtResourceLevelBO;
import com.thbs.mis.project.bo.ProjDatThbsBudgetForAdditionalCostBO;
import com.thbs.mis.project.constants.ProjectConstants;
import com.thbs.mis.project.controller.ProjectController;
import com.thbs.mis.project.dao.DatSowUploadHistoryRepository;
import com.thbs.mis.project.dao.DatWorkorderRepository;
import com.thbs.mis.project.dao.MasInvoiceRateTypeRepository;
import com.thbs.mis.project.dao.MasProjectRepository;
import com.thbs.mis.project.dao.MasRateTypeRepository;
import com.thbs.mis.project.dao.ProjClientBudgetApprovalDetailsRepository;
import com.thbs.mis.project.dao.ProjDatClientBudgetAtResourceLevelRepository;
import com.thbs.mis.project.dao.ProjDatClientBudgetForAdditionalCostRepository;
import com.thbs.mis.project.dao.ProjDatClientBudgetRateDetailsRepository;
import com.thbs.mis.project.dao.ProjDatProjectMappedToProjectCoordinatorRepository;
import com.thbs.mis.project.dao.ProjDatThbsBudgetAtResourceLevelRepository;
import com.thbs.mis.project.dao.ProjDatThbsBudgetForAdditionalCostRepository;
import com.thbs.mis.project.dao.ProjectDatClientBudgetRepository;
import com.thbs.mis.project.dao.ProjectLevelUserAccessGroupRepository;

@Service
public class BudgetService {

	@Autowired
	private EmpDetailRepository empDetailRepos;
	@Autowired
	private MasEmpRolesRepository masEmpRolesRepository;
	@Autowired
	private ProjDatThbsBudgetAtResourceLevelRepository projDatThbsBudgetAtResourceLevelRepository;
	@Autowired
	private ProjDatThbsBudgetForAdditionalCostRepository projDatThbsBudgetForAdditionalCostRepository;
	@Autowired
	private ProjDatClientBudgetAtResourceLevelRepository projDatClientBudgetAtResourceLevelRepository;
	@Autowired
	ProjDatClientBudgetForAdditionalCostRepository projDatClientBudgetForAdditionalCostRepository;

	@Autowired
	private ProjDatProjectMappedToProjectCoordinatorRepository projDatProjectMappedToProjectCoordinatorRepository;
	
	@Autowired
	private ProjDatClientBudgetRateDetailsRepository projDatClientBudgetRateDetailsRepository;

	@Autowired
	private MasProjectRepository masProjectRepository;

	@Autowired
	private DesignationRepository designationRepository;
	
	@Autowired
	private LevelRepository levelRepository;
	@Autowired
	private ProjectLevelUserAccessGroupRepository projectLevelUserAccessGroupRepository;
	@Autowired
	private DatWorkorderRepository datWorkorderRepository;

	@Autowired
	private ProjDatClientBudgetAtResourceLevelRepository projClientBudgetAtResourceLevelRepository;

	@Autowired
	private DatWorkorderRepository datWorkorderRepos;

	@Autowired
	private ProjDatProjectMappedToProjectCoordinatorRepository projectMappedToCoordinatorRepos;
	
	@Autowired
	private ProjectDatClientBudgetRepository projectDatClientBudgetRepository;
	
	@Autowired
	private ProjClientBudgetApprovalDetailsRepository projClientBudgetApprovalDetailsRepository;
    
	//Added by prathibha for creat/update client budget
	@Autowired
	private DatSowUploadHistoryRepository sowUploadRepository;
	
	@Autowired
	private MasInvoiceRateTypeRepository  masInvoiceRateTypeRepository;
	
	@Autowired
	private ParentLocationRepository parentLocationRepository;
	
	@Autowired
	private MasRateTypeRepository masRateTypeRepository;

	@Value("${project.budget.user.role.access.allowed}")
	private String projectBudgetAllowedUserRoleNames;

	@Value("${project.budget.user.role.access.notAllowed}")
	private String projectBudgetNotAllowedUserRoleNames;
	
	@Value("${project.budget.draftstatus}")
	private Integer draftStatus;
	

	private static final AppLog LOG = LogFactory
			.getLog(ProjectController.class);

	// Added by Lalith kumar

	public boolean createCompanyBudget(
			ProjDatThbsBudgetForAdditionalCostBean mainBean)
			throws DataAccessException, CommonCustomException {
		List<ProjDatThbsBudgetAtResourceLevelBean> projDatThbsBudgetAtResourceLevelBeanList = null;
		boolean created = false;

		List<String> allowedList = Arrays
				.asList(projectBudgetAllowedUserRoleNames.split(","));
		List<String> notAllowedList = Arrays
				.asList(projectBudgetNotAllowedUserRoleNames.split(","));

		ProjDatThbsBudgetForAdditionalCostBO thbsAdditionalCostBo = new ProjDatThbsBudgetForAdditionalCostBO();
		DatEmpDetailBO datEmpDetailBO;
		MasEmpRoleBO masEmpRoleBO;
		DatWorkorderBO datWorkorderBO;
		List<ProjDatThbsBudgetAtResourceLevelBO> projDatThbsBudgetAtResourceLevelBOList = new ArrayList<ProjDatThbsBudgetAtResourceLevelBO>();
		List<ProjDatThbsBudgetAtResourceLevelBO> updateProjDatThbsBudgetAtResourceLevelBOList = new ArrayList<ProjDatThbsBudgetAtResourceLevelBO>();

		try {
			projDatThbsBudgetAtResourceLevelBeanList = mainBean
					.getThbsRoleBudgetList();
			Integer createdBy = mainBean.getLoggedInId();
			List<Integer> accessList;
			try {
				datEmpDetailBO = empDetailRepos.findByPkEmpId(createdBy);
				LOG.info("@@@@@@@@  datEmpDetailBO.............!!!!"
						+ datEmpDetailBO);
			} catch (Exception e) {
				throw new DataAccessException(
						"Something went wrong while retriving data from DB");
			}
			if (datEmpDetailBO == null) {
				throw new CommonCustomException(
						"No records found with that EmpId(CreatedById)");
			} else {
				if (datEmpDetailBO.getFkEmpMainStatus() == 1) {
					try {
						accessList = new ArrayList<Integer>();
						datWorkorderBO = datWorkorderRepository
								.findByPkWorkorderId(mainBean.getWorkorderId());
						LOG.info("WorkorderBO..!!!!!!!!" + datWorkorderBO);
					} catch (Exception e) {
						throw new DataAccessException(
								"Error in getting accessList for this WorkOrderId.. :"
										+ e.getMessage());
					}
					if (datWorkorderBO != null) {
						accessList = projectMappedToCoordinatorRepos
								.getAllPcRelatedToProject(mainBean
										.getWorkorderId());
						LOG.info("@@@@@@@@AccessList.............!!!!"
								+ "WorkOrderID:" + mainBean.getWorkorderId()
								+ accessList);
					} else {
						throw new CommonCustomException(
								"No records found with that WorkOrderId...... ");
					}

					// Checking Employ is Active or Not...
					if (accessList.contains(createdBy)) {

						for (ProjDatThbsBudgetAtResourceLevelBean thbsBudgetBean : projDatThbsBudgetAtResourceLevelBeanList) {
							ProjDatThbsBudgetAtResourceLevelBO thbsBudgetBo = new ProjDatThbsBudgetAtResourceLevelBO();
							List<ProjDatThbsBudgetAtResourceLevelBO> thbsBudgetBoCheck = new ArrayList<ProjDatThbsBudgetAtResourceLevelBO>();

							if (thbsBudgetBean.getThbsBudgetResourceId() == null) {

								short desgId = thbsBudgetBean
										.getResourceRoleId();
								try {
									thbsBudgetBoCheck = projDatThbsBudgetAtResourceLevelRepository
											.findByFkWorkorderIdAndFkResourceDesignationId(
													mainBean.getWorkorderId(),
													desgId);
									LOG.startUsecase("Retrived...!!!!!!!!!!!!!!!"
											+ thbsBudgetBoCheck);
								} catch (Exception e) {
									// e.printStackTrace();
									throw new DataAccessException(
											"Something went wrong while retriving data from DB.. ");
								}

								if (thbsBudgetBoCheck.isEmpty()) {
									thbsBudgetBo.setCreatedBy(createdBy);
									thbsBudgetBo.setCreatedOn(new Date());
									thbsBudgetBo.setFkWorkorderId(mainBean
											.getWorkorderId());
									thbsBudgetBo
											.setFkResourceLevelId(thbsBudgetBean
													.getResourceLevelId());
									thbsBudgetBo
											.setResourceLocation(thbsBudgetBean
													.getResourceLocation());
									thbsBudgetBo
											.setFkResourceDesignationId(thbsBudgetBean
													.getResourceRoleId());
									thbsBudgetBo.setBudgetedDays(thbsBudgetBean
											.getBudgetedDays());
									thbsBudgetBo.setNoOfResource(thbsBudgetBean
											.getNoOfResource());

									masEmpRoleBO = datEmpDetailBO
											.getMasEmpRoleBO();

									if (allowedList.contains(masEmpRoleBO
											.getEmpRoleKeyName())) {

										thbsBudgetBo
												.setCostToCompany(thbsBudgetBean
														.getCostToCompany());
										thbsBudgetBo
												.setStandardRate(thbsBudgetBean
														.getStandardRate());
									} else if (notAllowedList
											.contains(masEmpRoleBO
													.getEmpRoleKeyName())) {

										thbsBudgetBo
												.setStandardRate(new BigDecimal(
														12.50));
										thbsBudgetBo
												.setCostToCompany(new BigDecimal(
														10));

									}

									LOG.startUsecase("Storing in list!!!!!!!!!!");
									projDatThbsBudgetAtResourceLevelBOList
											.add(thbsBudgetBo);
								} else {
									throw new CommonCustomException(
											"Resource Designation already exists for this WorkOrderId");
								}

							} else {
								thbsBudgetBo = projDatThbsBudgetAtResourceLevelRepository
										.findByPkThbsBudgetResourceId(thbsBudgetBean
												.getThbsBudgetResourceId());
								if (thbsBudgetBo != null) {
									thbsBudgetBo
											.setFkResourceLevelId(thbsBudgetBean
													.getResourceLevelId());
									thbsBudgetBo
											.setResourceLocation(thbsBudgetBean
													.getResourceLocation());
									thbsBudgetBo.setBudgetedDays(thbsBudgetBean
											.getBudgetedDays());
									thbsBudgetBo.setNoOfResource(thbsBudgetBean
											.getNoOfResource());
									thbsBudgetBo.setUpdatedOn(new Date());
									thbsBudgetBo.setUpdatedBy(mainBean
											.getLoggedInId());
									thbsBudgetBo
											.setCommentsForUpdation(mainBean
													.getCommentsForUpdation());
									BigDecimal standardRate;
									standardRate = new BigDecimal(12.05);
									thbsBudgetBo.setStandardRate(standardRate);
									BigDecimal costTOCompany;
									costTOCompany = new BigDecimal(100);
									thbsBudgetBo
											.setCostToCompany(costTOCompany);

									updateProjDatThbsBudgetAtResourceLevelBOList
											.add(thbsBudgetBo);
								}
							}
						}

						// creating budget for new roles
						Iterable<ProjDatThbsBudgetAtResourceLevelBO> create;
						Iterable<ProjDatThbsBudgetAtResourceLevelBO> updated;
						boolean crtd = false;
						boolean uptd = false;
						try {
							create = projDatThbsBudgetAtResourceLevelRepository
									.save(projDatThbsBudgetAtResourceLevelBOList);
							crtd = true;
						} catch (Exception e) {
							// e.printStackTrace();
							throw new DataAccessException(
									"Something went wrong while creating Company budget");
						}
						// Updating budget for already present roles
						try {
							updated = projDatThbsBudgetAtResourceLevelRepository
									.save(updateProjDatThbsBudgetAtResourceLevelBOList);
							uptd = true;
						} catch (Exception e) {
							throw new DataAccessException(
									"Something went wrong while updating Company budget");
						}
						LOG.startUsecase("After saving!!!!!!!!!!");
						if (uptd || crtd) {
							boolean flag = false;
							try {

								thbsAdditionalCostBo = projDatThbsBudgetForAdditionalCostRepository
										.findByFkWorkorderId(mainBean
												.getWorkorderId());
							} catch (Exception e) {
								throw new DataAccessException(
										"Something went wrong while creating budget");
							}
							if (thbsAdditionalCostBo != null) {
								thbsAdditionalCostBo.setTravelAmt(mainBean
										.getTravelAmt());
								thbsAdditionalCostBo.setExpenseAmt(mainBean
										.getExpenseAmt());
								thbsAdditionalCostBo.setPurchaseAmt(mainBean
										.getPurchaseAmt());
								thbsAdditionalCostBo
										.setCommentsForUpdation(mainBean
												.getCommentsForUpdation());
								thbsAdditionalCostBo.setUpdatedBy(mainBean
										.getLoggedInId());
								thbsAdditionalCostBo.setUpdatedOn(new Date());

								flag = true;
							}

							if (!flag) {

								thbsAdditionalCostBo.setCreatedBy(createdBy);
								thbsAdditionalCostBo.setCreatedOn(new Date());
								thbsAdditionalCostBo.setFkWorkorderId(mainBean
										.getWorkorderId());
								thbsAdditionalCostBo.setTravelAmt(mainBean
										.getTravelAmt());
								thbsAdditionalCostBo.setExpenseAmt(mainBean
										.getExpenseAmt());
								thbsAdditionalCostBo.setPurchaseAmt(mainBean
										.getPurchaseAmt());

							}
							try {

								projDatThbsBudgetForAdditionalCostRepository
										.save(thbsAdditionalCostBo);

							} catch (Exception e) {
								// e.printStackTrace();
								LOG.startUsecase("Exception !!!!!!!!!!");
								throw new DataAccessException(
										"unable to update budget");
							}
							created = true;
						}
					} else {
						throw new CommonCustomException(
								"Logged in employ doesnot have access to create company budget...");
					}
				} else {
					throw new CommonCustomException(
							"Logged in employ is Inactive ");
				}
				return created;
			}
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(e.getMessage());
		}
	}

	// Added by Rinta Mariam Jose

	public List<ProjMasClientBudgetSummaryBean> getClientBudgetSummary(
			Integer workorderId, Integer empId) throws DataAccessException,
			CommonCustomException {
		List<ProjMasClientBudgetSummaryBean> projMasClientBudgetSummaryBeanList = new ArrayList<ProjMasClientBudgetSummaryBean>();
		DatEmpDetailBO datEmpDetailBO;
		DatWorkorderBO datWorkorderBO;

		try {
			datWorkorderBO = datWorkorderRepository
					.findByPkWorkorderId(workorderId);
		} catch (Exception e2) {
			// e2.printStackTrace();
			throw new DataAccessException(
					"Some exception occurred while fetching data from dat_workorder table:  "
							+ e2.getMessage());
		}

		if (datWorkorderBO == null)
			throw new CommonCustomException("No such workorder id exists");
		else {
			try {
				datEmpDetailBO = empDetailRepos.findByPkEmpId(empId);
			} catch (Exception e1) {
				// e1.printStackTrace();
				throw new DataAccessException("No such employee id exists:  "
						+ e1.getMessage());
			}

			if (datEmpDetailBO == null)
				throw new CommonCustomException(
						"Invalid employee id. No such emp_id exists in dat_emp_detail table ");
			else {
				if (datEmpDetailBO.getFkEmpMainStatus() == 1) {
					List<Integer> accessList = new ArrayList<Integer>();
					try {
						accessList = projectLevelUserAccessGroupRepository
								.getUserAccessByWorkOrderId(workorderId);
					} catch (Exception e) {
						// e.printStackTrace();
						throw new DataAccessException(
								"No records found with that WorkOrderId : "
										+ e.getMessage());
					}

					if (accessList.contains(empId)) {
						MasEmpRoleBO masEmpRoleBO1 = datEmpDetailBO
								.getMasEmpRoleBO();

						List<String> projectBudgetNotAllowedUserRoleNamesList = Arrays
								.asList(projectBudgetNotAllowedUserRoleNames
										.split(","));

						Boolean b1 = projectBudgetNotAllowedUserRoleNamesList
								.contains(masEmpRoleBO1.getEmpRoleKeyName());
						/*
						 * List<String> projectBudgetAllowedUserRoleNamesList =
						 * Arrays
						 * .asList(projectBudgetAllowedUserRoleNames.split
						 * (","));
						 * 
						 * Boolean b2 =
						 * projectBudgetAllowedUserRoleNamesList.contains
						 * (masEmpRoleBO1.getEmpRoleKeyName());
						 */
						/*
						 * if( b1 || b2 ) {
						 */
						ProjMasClientBudgetSummaryBean projMasClientBudgetSummaryBean = new ProjMasClientBudgetSummaryBean();
						List<ClientBudgetSummaryBean> clientBudgetSummaryBeanList = new ArrayList<ClientBudgetSummaryBean>();
						List<ProjDatClientBudgetAtResourceLevelBO> projDatClientBudgetAtResourceLevelBOList;

						try {
							projDatClientBudgetAtResourceLevelBOList = projDatClientBudgetAtResourceLevelRepository
									.findByFkWorkorderId(workorderId);
						} catch (Exception e) {
							// e.printStackTrace();
							throw new DataAccessException(
									"Failed to retrieve list of client budget:  "
											+ e.getMessage());
						}
						if (!projDatClientBudgetAtResourceLevelBOList.isEmpty()) {
							for (ProjDatClientBudgetAtResourceLevelBO projDatClientBudgetAtResourceLevelBO : projDatClientBudgetAtResourceLevelBOList) {
								ClientBudgetSummaryBean clientBudgetSummaryBean = new ClientBudgetSummaryBean();
								MasEmpDesignationBO masEmpDesignationBO;
								try {
									masEmpDesignationBO = designationRepository
											.findByPkEmpDesignationId(projDatClientBudgetAtResourceLevelBO
													.getFkResourceDesignationId());
								} catch (Exception e) {
									// e.printStackTrace();
									throw new DataAccessException(
											"Failed to retrieve roleName:  "
													+ e.getMessage());
								}
								clientBudgetSummaryBean
										.setRole(masEmpDesignationBO
												.getEmpDesignationName());
								clientBudgetSummaryBean
										.setLocation(projDatClientBudgetAtResourceLevelBO
												.getResourceLocation());
								clientBudgetSummaryBean
										.setBudgetDays(projDatClientBudgetAtResourceLevelBO
												.getBudgetedDays());
								clientBudgetSummaryBean
										.setBillingRate(projDatClientBudgetAtResourceLevelBO
												.getInvoiceRate());
								clientBudgetSummaryBean
										.setCostToClient(projDatClientBudgetAtResourceLevelBO
												.getCostToClient());
								clientBudgetSummaryBeanList
										.add(clientBudgetSummaryBean);
							}

							projMasClientBudgetSummaryBean
									.setClientBudgetBean(clientBudgetSummaryBeanList);

							if (!b1) {
								projMasClientBudgetSummaryBean
										.setCostToCompanyFinal(null);
								projMasClientBudgetSummaryBean
										.setCostToClientLevel(null);
								projMasClientBudgetSummaryBean
										.setContribution(null);
								projMasClientBudgetSummaryBean
										.setPercentageContribution(null);
								projMasClientBudgetSummaryBean
										.setBudgetStatus(null);
							}
							projMasClientBudgetSummaryBeanList
									.add(projMasClientBudgetSummaryBean);
						} else
							throw new CommonCustomException(
									"Client Budget for the given workorderId is not available or the given workorderId doesn't exist.");
						/*
						 * } else throw new CommonCustomException(
						 * "This employee is not authorized to view client budget details"
						 * );
						 */} else
						throw new CommonCustomException(
								"This employee is not authorized to view client budget details");
				} else
					throw new CommonCustomException(
							"Inactive employee can't view company budget details");
			}
		}
		return projMasClientBudgetSummaryBeanList;
	}

	public List<ProjMasCompanyBudgetSummaryBean> getCompanyBudgetSummary(
			Integer workorderId, Integer empId) throws DataAccessException,
			CommonCustomException {
		List<ProjMasCompanyBudgetSummaryBean> projMasCompanyBudgetSummaryBeanList = new ArrayList<ProjMasCompanyBudgetSummaryBean>();
		DatEmpDetailBO datEmpDetailBO;
		MasEmpRoleBO masEmpRoleBO1;
		DatWorkorderBO datWorkorderBO;

		try {
			datWorkorderBO = datWorkorderRepository
					.findByPkWorkorderId(workorderId);
		} catch (Exception e2) {
			// e2.printStackTrace();
			throw new DataAccessException(
					"Some exception occurred while fetching data from dat_workorder table:  "
							+ e2.getMessage());
		}

		if (datWorkorderBO == null)
			throw new CommonCustomException("No such workorder id exists");
		else {
			try {
				datEmpDetailBO = empDetailRepos.findByPkEmpId(empId);
			} catch (Exception e1) {
				throw new DataAccessException(
						"Some exception occurred while fetching data from dat_emp_detail table:  "
								+ e1.getMessage());
			}

			if (datEmpDetailBO == null)
				throw new CommonCustomException("Invalid employee Id");
			else {
				if (datEmpDetailBO.getFkEmpMainStatus() == 1) {
					List<Integer> accessList = new ArrayList<Integer>();

					try {
						accessList = projectLevelUserAccessGroupRepository
								.getUserAccessByWorkOrderId(workorderId);
					} catch (Exception e) {
						// e.printStackTrace();
						throw new DataAccessException(
								"No records found with that WorkOrderId : "
										+ e.getMessage());
					}

					if (accessList.contains(empId)) {
						masEmpRoleBO1 = datEmpDetailBO.getMasEmpRoleBO();

						List<String> projectBudgetNotAllowedUserRoleNamesList = Arrays
								.asList(projectBudgetNotAllowedUserRoleNames
										.split(","));

						Boolean b1 = projectBudgetNotAllowedUserRoleNamesList
								.contains(masEmpRoleBO1.getEmpRoleKeyName());
						/*
						 * List<String> projectBudgetAllowedUserRoleNamesList =
						 * Arrays
						 * .asList(projectBudgetAllowedUserRoleNames.split
						 * (","));
						 * 
						 * Boolean b2 =
						 * projectBudgetAllowedUserRoleNamesList.contains
						 * (masEmpRoleBO1.getEmpRoleKeyName());
						 * 
						 * if( b1 || b2 ) {
						 */ProjMasCompanyBudgetSummaryBean projMasCompanyBudgetSummaryBean = new ProjMasCompanyBudgetSummaryBean();
						List<CompanyBudgetSummaryBean> companyBudgetSummaryBeanList = new ArrayList<CompanyBudgetSummaryBean>();
						List<ProjDatThbsBudgetAtResourceLevelBO> projDatThbsBudgetAtResourceLevelBOList;

						try {
							projDatThbsBudgetAtResourceLevelBOList = projDatThbsBudgetAtResourceLevelRepository
									.findByFkWorkorderId(workorderId);
						} catch (Exception e) {
							throw new DataAccessException(
									"Failed to retrieve list of company budget:  "
											+ e.getMessage());
						}

						if (!projDatThbsBudgetAtResourceLevelBOList.isEmpty()) {
							for (ProjDatThbsBudgetAtResourceLevelBO projDatThbsBudgetAtResourceLevelBO : projDatThbsBudgetAtResourceLevelBOList) {
								CompanyBudgetSummaryBean companyBudgetSummaryBean = new CompanyBudgetSummaryBean();
								MasEmpDesignationBO masEmpDesignationBO;
								MasEmpLevelBO masEmpLevelBO;
								try {
									masEmpDesignationBO = designationRepository
											.findByPkEmpDesignationId(projDatThbsBudgetAtResourceLevelBO
													.getFkResourceDesignationId());
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to retrieve roleName:  "
													+ e.getMessage());
								}

								try {
									masEmpLevelBO = levelRepository
											.findByPkEmpLevelId(projDatThbsBudgetAtResourceLevelBO
													.getFkResourceLevelId());
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to retrieve levelName:  "
													+ e.getMessage());
								}

								companyBudgetSummaryBean
										.setRole(masEmpDesignationBO
												.getEmpDesignationName());
								companyBudgetSummaryBean.setLevel(masEmpLevelBO
										.getEmpLevelName());
								companyBudgetSummaryBean
										.setLocation(projDatThbsBudgetAtResourceLevelBO
												.getResourceLocation());
								companyBudgetSummaryBean
										.setBudgetDays(projDatThbsBudgetAtResourceLevelBO
												.getBudgetedDays());

								if (!b1) {
									companyBudgetSummaryBean
											.setStandardRate(projDatThbsBudgetAtResourceLevelBO
													.getStandardRate());
									companyBudgetSummaryBean
											.setCostToCompany(projDatThbsBudgetAtResourceLevelBO
													.getCostToCompany());
								}
								companyBudgetSummaryBeanList
										.add(companyBudgetSummaryBean);
							}

							projMasCompanyBudgetSummaryBean
									.setCompanyBudgetBean(companyBudgetSummaryBeanList);
							;
							if (!b1) {
								projMasCompanyBudgetSummaryBean
										.setCostToCompanyFinal(null);
								projMasCompanyBudgetSummaryBean
										.setCostToClientLevel(null);
								projMasCompanyBudgetSummaryBean
										.setContribution(null);
								projMasCompanyBudgetSummaryBean
										.setPercentageContribution(null);
								projMasCompanyBudgetSummaryBean
										.setBudgetStatus(null);
							}
							projMasCompanyBudgetSummaryBeanList
									.add(projMasCompanyBudgetSummaryBean);
						} else
							throw new CommonCustomException(
									"Company Budget for the given workorderId is not available or the given workorderId doesn't exist.");
						/*
						 * } else throw new CommonCustomException(
						 * "This employee is not authorized to view company budget details"
						 * );
						 */} else
						throw new CommonCustomException(
								"This employee is not authorized to view client budget details");
				} else
					throw new CommonCustomException(
							"Inactive employee can't view company budget details");
			}
		}
		return projMasCompanyBudgetSummaryBeanList;
	}

	
	public List<ClientBudgetBean> getAllClientBudgetDetails(
			Integer workorderId, Integer empId) throws DataAccessException,
			CommonCustomException {

		List<ClientBudgetBean> clientBudgetBeanList = new ArrayList<ClientBudgetBean>();
		MasEmpRoleBO masEmpRolesBO = null;
		DatEmpDetailBO datEmpDetailBO = null;
		DatWorkorderBO datWorkorderBO = new DatWorkorderBO();
		try {
			datEmpDetailBO = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Some exception occurred while fetching data from Empdetails Table"
							+ e.getMessage());
		}
		if (datEmpDetailBO == null)
			throw new CommonCustomException("Invalid employee Id");
		else {
			if (datEmpDetailBO.getFkEmpMainStatus() == 1) {
				try {

					datWorkorderBO = datWorkorderRepository
							.findByPkWorkorderId(workorderId);
				} catch (Exception e) {
					throw new DataAccessException(
							" Some exception occurred while fetching WorkorderId "
									+ e.getMessage());
				}
				if (datWorkorderBO == null)
					throw new CommonCustomException("Invalid workorder Id");
				else {
					List<Integer> accessList;
					try {
						accessList = new ArrayList<Integer>();
						accessList = projectLevelUserAccessGroupRepository
								.getUserAccessByWorkOrderId(workorderId);
					} catch (Exception e) {
						throw new DataAccessException(
								"No records found with that WorkOrderId in projectLevel");
					}

					if (accessList.contains(empId)) {
						try {
							masEmpRolesBO = datEmpDetailBO.getMasEmpRoleBO();
						} catch (Exception e) {
							throw new DataAccessException(
									"Some exception occurred while fetching data from masEmprole table:  "
											+ e.getMessage());
						}
						List<String> projectBudgetNotAllowedUserRoleNamesList = Arrays
								.asList(projectBudgetNotAllowedUserRoleNames
										.split(","));

						Boolean b1 = projectBudgetNotAllowedUserRoleNamesList
								.contains(masEmpRolesBO.getEmpRoleKeyName());

						List<String> projectBudgetAllowedUserRoleNamesList = Arrays
								.asList(projectBudgetAllowedUserRoleNames
										.split(","));

						Boolean b2 = projectBudgetAllowedUserRoleNamesList
								.contains(masEmpRolesBO.getEmpRoleKeyName());

						if (b1 || b2) {
							List<ProjMasClientBudgetDetailsBean> projMasClientBudgetDetailsBeanList = new ArrayList<ProjMasClientBudgetDetailsBean>();
							try {

								List<ProjDatClientBudgetAtResourceLevelBO> cbudgetBoList = (List<ProjDatClientBudgetAtResourceLevelBO>) projDatClientBudgetAtResourceLevelRepository
										.findByFkWorkorderId(workorderId);
								// LOG.info("@@@@@Retrived..........!!!"+cbudgetBoList);
								if (!cbudgetBoList.isEmpty()) {
									for (ProjDatClientBudgetAtResourceLevelBO cbudgetBo : cbudgetBoList) {
										ProjMasClientBudgetDetailsBean projMasClientBudgetDetailsBean = new ProjMasClientBudgetDetailsBean();
										MasEmpDesignationBO masEmpDesignationBO;
										try {
											masEmpDesignationBO = designationRepository
													.findByPkEmpDesignationId(cbudgetBo
															.getFkResourceDesignationId());
										} catch (Exception e) {
											throw new DataAccessException(
													"Something went wrong while fetching from MasEmpRole");
										}

										projMasClientBudgetDetailsBean
												.setPkclientBudgetResourceId(cbudgetBo
														.getPkClientBudgetResourceId());
										projMasClientBudgetDetailsBean
												.setResourceRoleId(cbudgetBo
														.getFkResourceDesignationId());
										projMasClientBudgetDetailsBean
												.setResourceRoleName(masEmpDesignationBO
														.getEmpDesignationName());
										projMasClientBudgetDetailsBean
												.setLocation(cbudgetBo
														.getResourceLocation());
										projMasClientBudgetDetailsBean
												.setBudgetedDays(cbudgetBo
														.getBudgetedDays());
										projMasClientBudgetDetailsBean
												.setNoOfResource(cbudgetBo
														.getNoOfResource());
										if (b2)
											projMasClientBudgetDetailsBean
													.setBillingRate(cbudgetBo
															.getInvoiceRate());
										projMasClientBudgetDetailsBean
												.setCostToClient(cbudgetBo
														.getCostToClient());
										projMasClientBudgetDetailsBeanList
												.add(projMasClientBudgetDetailsBean);
									}
									// if
									// (!projMasClientBudgetDetailsBeanList.isEmpty())
									// {
									ProjDatClientBudgetForAdditionalCostBO caddbudgetBo;
									try {
										caddbudgetBo = projDatClientBudgetForAdditionalCostRepository
												.findByFkWorkorderId(workorderId);
									} catch (Exception e) {
										throw new DataAccessException(
												"some error occured while fecthing data from AdditionalCostRepository for client budget: "
														+ e.getMessage());
									}
									if (caddbudgetBo != null) {
										ClientBudgetBean clientBudgetBean = new ClientBudgetBean();
										clientBudgetBean
												.setProjMasClientBudgetDetailsBeanList(projMasClientBudgetDetailsBeanList);
										if (b2)
											clientBudgetBean
													.setAdditionalAmt(caddbudgetBo
															.getClientBudgetAdditionalCost());
										clientBudgetBean
												.setCommentForAdditionalAmt(null);
										clientBudgetBean.setWoId(caddbudgetBo
												.getFkWorkorderId());
										clientBudgetBeanList
												.add(clientBudgetBean);

									} else {
										throw new CommonCustomException(
												"Given WorkorderId doesn't Exists in AdditionalCost Table");

									}
									/*
									 * } else{
									 * 
									 * throw new DataAccessException(
									 * "Given WorkorderId doesn't Exists in Resource table"
									 * ); }
									 */

								} else {

									throw new CommonCustomException(
											"Given WorkorderId doesn't Exists in Resource Level table");
								}
							} catch (Exception e) {
								// e.printStackTrace();
								throw new DataAccessException(e.getMessage());
							}
						} else
							throw new CommonCustomException(
									"This employee can't view the client budget details");
					}

					else {
						throw new CommonCustomException(
								"This employee is not authorized to view the details");
					}
				}
			} else
				throw new CommonCustomException(
						"Inactive employee can't view client budget details");
		}
		return clientBudgetBeanList;
	}

	public List<CompanyBudgetBean> getAllCompanyBudgetDetails(
			Integer workorderId, Integer empId) throws DataAccessException,
			CommonCustomException {

		List<CompanyBudgetBean> companyBudgetBeanList = new ArrayList<CompanyBudgetBean>();

		MasEmpRoleBO masEmpRolesBO = new MasEmpRoleBO();
		DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
		DatWorkorderBO datWorkorderBO = new DatWorkorderBO();
		try {
			datEmpDetailBO = empDetailRepos.findByPkEmpId(empId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Some exception occurred while fetching data from Empdetails Table "
							+ e.getMessage());
		}
		if (datEmpDetailBO == null)
			throw new CommonCustomException("Invalid employee Id");
		else {
			if (datEmpDetailBO.getFkEmpMainStatus() == 1) {
				try {

					datWorkorderBO = datWorkorderRepository
							.findByPkWorkorderId(workorderId);
				} catch (Exception e) {
					throw new DataAccessException(
							" Some exception occurred while fetching WorkorderId "
									+ e.getMessage());
				}
				if (datWorkorderBO == null)
					throw new CommonCustomException("Invalid workorder Id");
				else {
					List<Integer> accessList;
					try {
						accessList = new ArrayList<Integer>();
						accessList = projectLevelUserAccessGroupRepository
								.getUserAccessByWorkOrderId(workorderId);
					} catch (Exception e) {
						throw new DataAccessException(
								"No records found with that WorkOrderId in projectLevel");
					}

					if (accessList.contains(empId)) {
						try {
							masEmpRolesBO = datEmpDetailBO.getMasEmpRoleBO();
						} catch (Exception e) {
							throw new DataAccessException(
									"Some exception occurred while fetching data from masEmprole table:  "
											+ e.getMessage());
						}

						List<String> projectBudgetNotAllowedUserRoleNamesList = Arrays
								.asList(projectBudgetNotAllowedUserRoleNames
										.split(","));

						Boolean b1 = projectBudgetNotAllowedUserRoleNamesList
								.contains(masEmpRolesBO.getEmpRoleKeyName());

						List<String> projectBudgetAllowedUserRoleNamesList = Arrays
								.asList(projectBudgetAllowedUserRoleNames
										.split(","));

						Boolean b2 = projectBudgetAllowedUserRoleNamesList
								.contains(masEmpRolesBO.getEmpRoleKeyName());

						if (b1 || b2) {
							List<ProjMasCompanyBudgetDetailsBean> projMasCompanytBudgetDetailsBeanList = new ArrayList<ProjMasCompanyBudgetDetailsBean>();
							try {

								List<ProjDatThbsBudgetAtResourceLevelBO> cbudgetBoList = (List<ProjDatThbsBudgetAtResourceLevelBO>) projDatThbsBudgetAtResourceLevelRepository
										.findByFkWorkorderId(workorderId);
								if (!cbudgetBoList.isEmpty()) {
									for (ProjDatThbsBudgetAtResourceLevelBO cbudgetBo : cbudgetBoList) {
										ProjMasCompanyBudgetDetailsBean projMasCompanyBudgetDetailsBean = new ProjMasCompanyBudgetDetailsBean();
										MasEmpLevelBO masEmpLevelBO;
										MasEmpDesignationBO masEmpDesignationBO;
										try {
											masEmpDesignationBO = designationRepository
													.findByPkEmpDesignationId(cbudgetBo
															.getFkResourceDesignationId());
										} catch (Exception e) {
											throw new DataAccessException(
													"Something went wrong while fetching from MasEmpDesignation");
										}

										try {
											masEmpLevelBO = levelRepository
													.findByPkEmpLevelId(cbudgetBo
															.getFkResourceLevelId());
										} catch (Exception e) {
											throw new DataAccessException(
													"Something went wrong while fetching from MasEmpLevel");
										}
										projMasCompanyBudgetDetailsBean
												.setPkcompanyBudgetResourceId(cbudgetBo
														.getPkThbsBudgetResourceId());
										projMasCompanyBudgetDetailsBean
												.setResourceRoleId(cbudgetBo
														.getFkResourceDesignationId());
										projMasCompanyBudgetDetailsBean
												.setResourceRoleName(masEmpDesignationBO
														.getEmpDesignationName());
										projMasCompanyBudgetDetailsBean
												.setResourceLevel(cbudgetBo
														.getFkResourceLevelId());
										projMasCompanyBudgetDetailsBean
												.setResourceLevelName(masEmpLevelBO
														.getEmpLevelName());
										projMasCompanyBudgetDetailsBean
												.setLocation(cbudgetBo
														.getResourceLocation());
										projMasCompanyBudgetDetailsBean
												.setBudgetedDays(cbudgetBo
														.getBudgetedDays());
										projMasCompanyBudgetDetailsBean
												.setNoOfResource(cbudgetBo
														.getNoOfResource());
										if (b2)
											projMasCompanyBudgetDetailsBean
													.setBillingRate(cbudgetBo
															.getStandardRate());
										projMasCompanyBudgetDetailsBean
												.setAmount(cbudgetBo
														.getCostToCompany());
										projMasCompanytBudgetDetailsBeanList
												.add(projMasCompanyBudgetDetailsBean);
									}

									// if
									// (!projMasCompanytBudgetDetailsBeanList.isEmpty())
									// {
									ProjDatThbsBudgetForAdditionalCostBO thbsbudget;
									try {

										thbsbudget = projDatThbsBudgetForAdditionalCostRepository
												.findByFkWorkorderId(workorderId);
									} catch (Exception e) {
										throw new DataAccessException(
												"some error occured while fecthing data from AdditionalCostRepository for thbs budget: "
														+ e.getMessage());
									}
									if (thbsbudget != null) {
										CompanyBudgetBean companyBudgetBean = new CompanyBudgetBean();
										companyBudgetBean
												.setProjMasCompanyBudgetDetailsBeanList(projMasCompanytBudgetDetailsBeanList);
										companyBudgetBean.setWoId(thbsbudget
												.getFkWorkorderId());
										if (b2) {
											companyBudgetBean
													.setAddtionalPurchaseAmt(thbsbudget
															.getPurchaseAmt());
											companyBudgetBean
													.setAdditionalExpenseAmt(thbsbudget
															.getExpenseAmt());
											companyBudgetBean
													.setAdditionalTravelAmt(thbsbudget
															.getTravelAmt());
										}
										companyBudgetBeanList
												.add(companyBudgetBean);

									} else {
										throw new CommonCustomException(
												"Given WorkorderId doesn't Exists in AddittionalCost Table");
									}
								}

								else {
									throw new CommonCustomException(
											"Given WorkorderId doesn't Exists in ResourceLevel Table");
								}
							} catch (Exception e) {
								throw new DataAccessException(e.getMessage());
							}
						} else {
							throw new CommonCustomException(
									"This employee can't view the company budget details");
						}
					} else {
						throw new CommonCustomException(
								"This employee is not authorized to view the details");
					}

				}
			} else {
				throw new CommonCustomException(
						"Inactive employee can't view company budget details");
			}
		}
		return companyBudgetBeanList;
	}

	public boolean createClientBudget(ClientBudgetInputBean bean)
			throws CommonCustomException, DataAccessException {

		boolean flag = false;
		DatWorkorderBO workorderDet = null;
		ProjDatClientBudgetBO clientBudgetBO = new ProjDatClientBudgetBO();
		ProjDatClientBudgetBO clientBudgetBO1 = null;
		BigDecimal costToClientTotal = null; 
		
		try {
			LOG.info("Workorder ID");
			workorderDet = datWorkorderRepos.findByPkWorkorderId(bean.getFkWorkorderId());
			LOG.debug("Workoder details"+workorderDet.toString());
		} catch (Exception e1) {
			throw new DataAccessException("Invalid workOrder Id.Please enter a valid work order-id");
		}
		//Work order validation	
		if(workorderDet != null)
		{   LOG.info("Inside Workoderdetails"+workorderDet.getPkWorkorderId());
			if(workorderDet.getSowUploadedFlag().equalsIgnoreCase("NOT UPLOADED"))
			{
				throw new CommonCustomException("Please upload SOW");
			}	
		}
		
		//Validation whether  project coordinator mapped to mentioned project or not
		ProjDatProjectMappedToProjectCoordinatorBO pcmapped;
		try {
				pcmapped = projectMappedToCoordinatorRepos.finddByProjectandPCorDMorDL
					        (workorderDet.getFkProjectId(),bean.getCreatedBy());
			} catch (Exception e1) {
					throw new DataAccessException("Unable to fetch records for credentials ");
			}
				LOG.info("Logged in person is PC OR NOT");
				if(pcmapped !=null)
				{
					LOG.info("Project details"+pcmapped.toString());
					flag = true;
				}
				else
				{
					LOG.info("Access denied for the logged in user");
					throw new CommonCustomException("Access denied for the logged in user");
				}
				
				//Validating sum of cost to client (Total)based on designations for particular work order does not exceed saw_amount(dat_workorder table)
				try
				{
					costToClientTotal = projectDatClientBudgetRepository.getCostToClientTotal(bean.getFkWorkorderId());
					LOG.info("Cost to client total CCCCCCCCCCC"+costToClientTotal.toBigInteger());
				}
				catch (Exception e)
				{
					throw new CommonCustomException("Fialed to fetch sum of cost_to client from DB");
				}
				
				if(costToClientTotal.compareTo(workorderDet.getSowAmount())== 1 || costToClientTotal.compareTo(workorderDet.getSowAmount())== 0 )
				{
					LOG.info("Comparing costToClient with sowAmount");
					throw new CommonCustomException("You cannot create cleint budget as costToclient Exceeds sow Amount OR costToclient is equal to sowAmount" +costToClientTotal+"  "+"SOW amount = "+workorderDet.getSowAmount());
				}
				//WeeklyOff Flag and Holiday Flag both cannot be yes
				 if(bean.getWeeklyOffFlag().equalsIgnoreCase("YES") && bean.getHolidayFlag().equalsIgnoreCase("YES"))
				 {
					 LOG.info("Inside WeeklyOff-No && HolidayFlag-NO");
					 throw new CommonCustomException("Both WeeklyOffFlag and HolidayFlag cannot be YES.Please change the status"); 
				 }
				

				//Add client budget
			    //Checking for duplicate records based on designation and Location(OFFHSORE/ONSITE)

				LOG.info("Add client budget aaaaaaaa");
				try{
					clientBudgetBO1 = projectDatClientBudgetRepository.findByFkWorkOrderIdAndFkResourceDesignationIdAndResourceLocation
						(bean.getFkWorkorderId(), bean.getFkResourceDesignationId(),bean.getResourceLocation());
				}
				catch(Exception e)
				{  
					e.printStackTrace();
					throw new CommonCustomException("Exception occures while fecthing files for duplicate records");
				}
				if(clientBudgetBO1 != null)
				{  
					LOG.info("Duplicate Records ddddddd");
					throw new CommonCustomException("Record for designationId " +bean.getFkResourceDesignationId() +"  "+"and resource location"+" "+ bean.getResourceLocation() 
							+ " already exists.");
				}
				
				//Adding row in clientBudget table
				clientBudgetBO.setFkWorkOrderId(bean.getFkWorkorderId());
				clientBudgetBO.setCreatedBy(bean.getCreatedBy());
				//Designation Id validation
				MasEmpDesignationBO designation = designationRepository.findByPkEmpDesignationId(bean.getFkResourceDesignationId());
					if(designation != null)
					{
						clientBudgetBO.setFkResourceDesignationId(bean.getFkResourceDesignationId());
					}
					else
					{
						throw new CommonCustomException("Please enter a valid designation Id");
					}
				clientBudgetBO.setResourceLocation(bean.getResourceLocation());
				
				//Parent location Id validation
				MasLocationParentBO masLocationParentBO	= parentLocationRepository.findByPkLocationParentId(bean.getFkParentLocationId());
				
					if(masLocationParentBO != null)
					{
						clientBudgetBO.setFkParentLocationId(bean.getFkParentLocationId());
					}
					else
					{
						throw new CommonCustomException("Please enter a valid location Id");
					}
				
				clientBudgetBO.setNoOfResource(bean.getNoOfResource());
				clientBudgetBO.setBudgetedDays(bean.getBudgetedDays());
				clientBudgetBO.setIsBillingRateAvailable(bean.getIsBillingRateAvailable());
				clientBudgetBO.setCostToClient(bean.getCostToClient());
				clientBudgetBO.setCreatedOn(new Date());

				try {
				projectDatClientBudgetRepository.save(clientBudgetBO);
				} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				}
				
				if (bean.getWeeklyOffFlag().equalsIgnoreCase("NO") && bean.getHolidayFlag().equalsIgnoreCase("NO"))
				{
					LOG.info("Normal ratings");
					LOG.info("Client budget Id"+clientBudgetBO.getPkClientBudgetId().toString());
					
					for(RateDetailsBean rateDetailsBean : bean.getNormalrateDetails())
					{ 
						ProjDatClientBudgetRateDetailBO clientBudgetRateDetailsBO = new ProjDatClientBudgetRateDetailBO();
						clientBudgetRateDetailsBO.setFkClientBudgetId(clientBudgetBO.getPkClientBudgetId());
						clientBudgetRateDetailsBO.setWeeklyOffFlag(bean.getWeeklyOffFlag());
						clientBudgetRateDetailsBO.setFkInvoiceRateTypeId(rateDetailsBean.getInvoiceRateTypeid());
						clientBudgetRateDetailsBO.setFkRateTypeId(rateDetailsBean.getFkRateTypeId());
						clientBudgetRateDetailsBO.setRateAmount(rateDetailsBean.getValue());
						clientBudgetRateDetailsBO.setHolidayFlag(bean.getHolidayFlag());
						clientBudgetRateDetailsBO.setFkWeeklyOffId(rateDetailsBean.getWeeklyOffId());
			
						try
						{	
							LOG.info("Saving Client budget rate details");
							projDatClientBudgetRateDetailsRepository.save(clientBudgetRateDetailsBO);
						}
						catch(Exception e)
						{
							// e.printStackTrace();
							throw new DataAccessException("Clould not save client budget rate details");
						
						}
					}
					flag=true;
				}
				else if(bean.getWeeklyOffFlag().equalsIgnoreCase("YES"))
				{	LOG.info("Weely-off flag YES");
					LOG.info("Client budget Id"+clientBudgetBO.getPkClientBudgetId().toString());
					
					for(RateDetailsBean rateDetailsBean : bean.getWeeklyOffRateDetails())
					{   
						ProjDatClientBudgetRateDetailBO clientBudgetRateDetailsBO = new ProjDatClientBudgetRateDetailBO();
						clientBudgetRateDetailsBO.setFkClientBudgetId(clientBudgetBO.getPkClientBudgetId());
						clientBudgetRateDetailsBO.setWeeklyOffFlag(bean.getWeeklyOffFlag());
						clientBudgetRateDetailsBO.setFkInvoiceRateTypeId(rateDetailsBean.getInvoiceRateTypeid());
						clientBudgetRateDetailsBO.setFkRateTypeId(rateDetailsBean.getFkRateTypeId());
						clientBudgetRateDetailsBO.setRateAmount(rateDetailsBean.getValue());
						clientBudgetRateDetailsBO.setHolidayFlag("NO");
						clientBudgetRateDetailsBO.setFkWeeklyOffId(rateDetailsBean.getWeeklyOffId());
					
						try
						{	
							LOG.info("Saving Client budget rate details");
							projDatClientBudgetRateDetailsRepository.save(clientBudgetRateDetailsBO);
						}
						catch(Exception e)
						{
							// e.printStackTrace();
							throw new DataAccessException("Clould not save client budget rate details");
						
						}
						
					}
					flag=true;

				}
					//if holiday flag set
				else if (bean.getHolidayFlag().equalsIgnoreCase("YES"))
				{   LOG.info("Holiday flag set YES");
				
					for(RateDetailsBean rateDetailsBean : bean.getHolidayRateDetails())
					{	
						ProjDatClientBudgetRateDetailBO clientBudgetRateDetailsBO = new ProjDatClientBudgetRateDetailBO();
						clientBudgetRateDetailsBO.setFkClientBudgetId(clientBudgetBO.getPkClientBudgetId());
						clientBudgetRateDetailsBO.setHolidayFlag(bean.getHolidayFlag());
						clientBudgetRateDetailsBO.setFkInvoiceRateTypeId(rateDetailsBean.getInvoiceRateTypeid());
						clientBudgetRateDetailsBO.setFkRateTypeId(rateDetailsBean.getFkRateTypeId());
						clientBudgetRateDetailsBO.setRateAmount(rateDetailsBean.getValue());
						clientBudgetRateDetailsBO.setWeeklyOffFlag("NO");
						clientBudgetRateDetailsBO.setFkWeeklyOffId(rateDetailsBean.getWeeklyOffId());
				
					try
					{	
						LOG.info("Saving Client budget rate details");
						projDatClientBudgetRateDetailsRepository.save(clientBudgetRateDetailsBO);
					}
					catch(Exception e)
					{
						// e.printStackTrace();
						throw new DataAccessException("Clould not save client budget rate details");
					
					}
				}
					flag=true;
		
				}
								
	//left out client amount for woId 
		return flag;
	
	}
	
	
	//Added by Sharanya
	public List<BudgetDetailsOutputBean> getClientBudgetDetails(
			Integer clientBudgetId, Integer empId) throws DataAccessException {
		List<BudgetDetailsOutputBean> budgetDetailsOutputBeanList = new ArrayList<BudgetDetailsOutputBean>();
		BudgetDetailsOutputBean budgetDetailsOutputBean = new BudgetDetailsOutputBean();
		MasProjectBO masProjectBO = new MasProjectBO();
		List<Integer> pcList = new ArrayList<Integer>();
		ProjDatClientBudgetBO projDatClientBudgetBO = new ProjDatClientBudgetBO();
		List<ProjDatClientBudgetRateDetailBO> projDatClientBudgetRateDetailBOList = null;
		MasLocationParentBO masLocationParentBO;
		Short versionId;
		DatWorkorderBO datWorkorderBO;
		Integer status;
		try {
			projDatClientBudgetBO = projectDatClientBudgetRepository
					.findByPkClientBudgetId(clientBudgetId);
		} catch (Exception e) {
			throw new DataAccessException("Failed to fetch the details", e);
		}
		if (projDatClientBudgetBO != null) {
			try {
				pcList = projectLevelUserAccessGroupRepository
						.getUserAccessByWorkOrderId(projDatClientBudgetBO
								.getFkWorkOrderId());
			} catch (Exception e) {
				throw new DataAccessException("Failed to fetch the details", e);
			}

			if (!pcList.isEmpty()) {
				try {
					datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(projDatClientBudgetBO
							.getFkWorkOrderId());							;
				} catch (Exception e) {
					throw new DataAccessException(
							"Failed to fetch the project details", e);
				}
				masProjectBO = datWorkorderBO.getMasProject();
				pcList.add(masProjectBO.getFkDeliveryMgrId());
				pcList.add(masProjectBO.getFkDomainMgrId());
				if (pcList.contains(empId)) {
					budgetDetailsOutputBean.setPkClientBudgetId(clientBudgetId);
					budgetDetailsOutputBean
							.setFkResourceDesignationId(projDatClientBudgetBO
									.getFkResourceDesignationId());
					budgetDetailsOutputBean
							.setFkResourceDesignationName(projDatClientBudgetBO
									.getMasEmpDesignationBO()
									.getEmpDesignationName());
					budgetDetailsOutputBean
							.setResourceLocation(projDatClientBudgetBO
									.getResourceLocation());
					budgetDetailsOutputBean
							.setFkParentLocationId(projDatClientBudgetBO
									.getFkParentLocationId());
					try {
						masLocationParentBO = parentLocationRepository
								.findByPkLocationParentId(projDatClientBudgetBO
										.getFkParentLocationId());
					} catch (Exception e) {
						throw new DataAccessException(
								"Failed to fetch location details", e);
					}
					if (masLocationParentBO != null) {
						budgetDetailsOutputBean
								.setFkParentLocationName(masLocationParentBO
										.getLocationParentName());
					}
					budgetDetailsOutputBean
							.setFkWorkorderId(projDatClientBudgetBO
									.getFkWorkOrderId());
					budgetDetailsOutputBean
							.setBudgetedDays(projDatClientBudgetBO
									.getBudgetedDays());
					budgetDetailsOutputBean
							.setIsBillingRateAvailable(projDatClientBudgetBO
									.getIsBillingRateAvailable());
					budgetDetailsOutputBean
							.setCostToClient(projDatClientBudgetBO
									.getCostToClient());
					budgetDetailsOutputBean.setCreatedBy(projDatClientBudgetBO
							.getCreatedBy());
					budgetDetailsOutputBean.setCreatedOn(projDatClientBudgetBO
							.getCreatedOn());
					budgetDetailsOutputBean
							.setNoOfResource(projDatClientBudgetBO
									.getNoOfResource());
					budgetDetailsOutputBean.setSowAmount(projDatClientBudgetBO
							.getDatWorkorder().getSowAmount());
					try {
						versionId = sowUploadRepository.getbyWorkorderId(projDatClientBudgetBO
								.getFkWorkOrderId());
					} catch (Exception e) {
						throw new DataAccessException("Failed to fetch the version details",e);
					}
					budgetDetailsOutputBean.setVersionId(versionId);
					budgetDetailsOutputBean
							.setCurrencyTypeId(projDatClientBudgetBO
									.getDatWorkorder().getSowCurrencyId());
					budgetDetailsOutputBean.setCurrencyTypeName(projDatClientBudgetBO
									.getDatWorkorder().getMasCurrencytype().getCurrencyTypeCode());

					try {
						projDatClientBudgetRateDetailBOList = projDatClientBudgetRateDetailsRepository
								.findByFkClientBudgetId(clientBudgetId);
					} catch (Exception e) {
						throw new DataAccessException(
								"Failed to fetch the details", e);
					}
					if (projDatClientBudgetRateDetailBOList != null) {
						List<WeeklyOffBean> weeklyOffBeanList = new ArrayList<WeeklyOffBean>();
						List<InvoiceRateTypeBean> fkInvoiceRateTypeIdList = new ArrayList<InvoiceRateTypeBean>();
						List<HolidayDetailsBean> holidayDetailsBeanList = new ArrayList<HolidayDetailsBean>();
						for (ProjDatClientBudgetRateDetailBO projDatClientBudgetRateDetailBO : projDatClientBudgetRateDetailBOList) {
							if (projDatClientBudgetRateDetailBO
									.getFkWeeklyOffId() != null
									&& projDatClientBudgetRateDetailBO
											.getWeeklyOffFlag().equals("YES")) {
								WeeklyOffBean weeklyOffBean = new WeeklyOffBean();
								weeklyOffBean
										.setClientBudgetRateDetailsId(projDatClientBudgetRateDetailBO
												.getPkClientBudgetRateDetailsId());
								weeklyOffBean
										.setRateTypeId(projDatClientBudgetRateDetailBO
												.getFkRateTypeId());
								weeklyOffBean.setInvoiceRateTypeId(projDatClientBudgetRateDetailBO
										     .getFkInvoiceRateTypeId());
								weeklyOffBean
										.setWeeklyOffId(projDatClientBudgetRateDetailBO
												.getFkWeeklyOffId());
								weeklyOffBean
										.setValue(projDatClientBudgetRateDetailBO
												.getRateAmount());
								if (budgetDetailsOutputBean.getWeeklyOffFlag() == null
										|| (budgetDetailsOutputBean
												.getWeeklyOffFlag() != null && !budgetDetailsOutputBean
												.getWeeklyOffFlag()
												.equalsIgnoreCase(
														ProjectConstants.YES))) {
									budgetDetailsOutputBean
											.setWeeklyOffFlag(projDatClientBudgetRateDetailBO
													.getWeeklyOffFlag());
								}
								weeklyOffBeanList.add(weeklyOffBean);
							}
							if (projDatClientBudgetRateDetailBO
									.getFkInvoiceRateTypeId() != null) {
								InvoiceRateTypeBean invoiceRateTypeBean = new InvoiceRateTypeBean();
								invoiceRateTypeBean
										.setClientBudgetRateDetailsId(projDatClientBudgetRateDetailBO
												.getPkClientBudgetRateDetailsId());
								invoiceRateTypeBean
										.setRateTypeId(projDatClientBudgetRateDetailBO
												.getFkRateTypeId());
								invoiceRateTypeBean
										.setInvoiceRateTypeId(projDatClientBudgetRateDetailBO
												.getFkInvoiceRateTypeId());
								invoiceRateTypeBean
										.setValue(projDatClientBudgetRateDetailBO
												.getRateAmount());
								fkInvoiceRateTypeIdList
										.add(invoiceRateTypeBean);
							}
							if (projDatClientBudgetRateDetailBO
									.getHolidayFlag().equals("YES")) {
								HolidayDetailsBean holidayDetailsBean = new HolidayDetailsBean();
								holidayDetailsBean
										.setClientBudgetRateDetailsId(projDatClientBudgetRateDetailBO
												.getPkClientBudgetRateDetailsId());
								holidayDetailsBean
										.setRateTypeId(projDatClientBudgetRateDetailBO
												.getFkRateTypeId());
								holidayDetailsBean
										.setValue(projDatClientBudgetRateDetailBO
												.getRateAmount());
								holidayDetailsBean.setInvoiceRateTypeId(projDatClientBudgetRateDetailBO
										.getFkInvoiceRateTypeId());
								holidayDetailsBeanList.add(holidayDetailsBean);
							}
							if(projDatClientBudgetRateDetailBO.getFkRateTypeId() != null){
								budgetDetailsOutputBean
										.setRateAmount(projDatClientBudgetRateDetailBO
												.getRateAmount());
								budgetDetailsOutputBean
										.setRateTypeId(projDatClientBudgetRateDetailBO
												.getFkRateTypeId());
								budgetDetailsOutputBean
										.setRateTypeName(projDatClientBudgetRateDetailBO
												.getMasRateType()
												.getRateTypeName());
								
							}					
						}
						if (!weeklyOffBeanList.isEmpty()) {
							budgetDetailsOutputBean
									.setWeeklyOffDetails(weeklyOffBeanList);
						}
						if (!fkInvoiceRateTypeIdList.isEmpty()) {
							budgetDetailsOutputBean
									.setInvoiceRateTypeDetails(fkInvoiceRateTypeIdList);
						}
						if (!holidayDetailsBeanList.isEmpty()){
							budgetDetailsOutputBean.setHolidayDetails(holidayDetailsBeanList);
						}
					}
					try {
						status = projClientBudgetApprovalDetailsRepository.findByFkClientBudgetId(clientBudgetId);
					} catch (Exception e) {
						throw new DataAccessException("Unable to get the status Id's",e);
					}
					if(status != null){
						budgetDetailsOutputBean.setStatus(status);
					}
					else
						budgetDetailsOutputBean.setStatus(draftStatus);
					budgetDetailsOutputBean
							.setAdditionalAmt(projDatClientBudgetBO
									.getDatWorkorder().getAdditionalAmount());
					budgetDetailsOutputBeanList.add(budgetDetailsOutputBean);

				} else
					throw new DataAccessException(
							"You are not authorized person to view the details");
			}
		} else
			throw new DataAccessException("No Such Client Budget Id Exsists");
		
		return budgetDetailsOutputBeanList;
	}
	//EOA by Sharanya
	//Added by Lalith kumar 

public 	CliBudgetDesignationListAndAdditionalCostOutputBean getClientBudgetDesignationList(Integer woId) throws DataAccessException,CommonCustomException
{

	
	CliBudgetDesignationListAndAdditionalCostOutputBean  clientBudgetDesignationAndAdditionCostBean ;
	List<ClientBudgetDesignationOutputBean> clientBudgetDesignationOutputBeanList = null;
	DatWorkorderBO datWorkorderBO;
	ProjDatThbsBudgetForAdditionalCostBO projDatThbsBudgetForAdditionalCostBO;
	
	
	try{
		datWorkorderBO = datWorkorderRepository.findByPkWorkorderId(woId);
	}catch(Exception e)
	{
		throw new DataAccessException("Unable to get client budget designation roles...!");
	}
	if(datWorkorderBO!=null)
	{
	try{
		clientBudgetDesignationOutputBeanList = projectDatClientBudgetRepository.getAllClientBugetDesignationListByWoId(woId);
	}catch(Exception e)
	{
	//	e.printStackTrace();
		throw new DataAccessException("Unable to get client budget designation roles...!");
	}
	
	try{
		projDatThbsBudgetForAdditionalCostBO = projDatThbsBudgetForAdditionalCostRepository.findByFkWorkorderId(woId);
		
	}catch(Exception e)
	{
		// e.printStackTrace();
		throw new DataAccessException("Unable to get client budget designation roles...!");
	}
	
	clientBudgetDesignationAndAdditionCostBean = new CliBudgetDesignationListAndAdditionalCostOutputBean();
	
	clientBudgetDesignationAndAdditionCostBean.setClientBudgetDesignationOutputBeanList(clientBudgetDesignationOutputBeanList);
	
	if(projDatThbsBudgetForAdditionalCostBO!=null)
	{
	clientBudgetDesignationAndAdditionCostBean.setExpenseAmt(projDatThbsBudgetForAdditionalCostBO.getExpenseAmt());
	clientBudgetDesignationAndAdditionCostBean.setPurchaseAmt(projDatThbsBudgetForAdditionalCostBO.getPurchaseAmt());
	clientBudgetDesignationAndAdditionCostBean.setTravelAmt(projDatThbsBudgetForAdditionalCostBO.getTravelAmt());
	}
	}else
	{
		throw new CommonCustomException("Workorder does not exists...!");
	}

	
	return 	clientBudgetDesignationAndAdditionCostBean;
}
	
//EOA by Lalith kumar

//Added by Sharanya
public List<BudgetDetailsOutputBean> getClientBudgetDetailsByWoId(
		Integer woId, Integer empId) throws DataAccessException {
	List<BudgetDetailsOutputBean> budgetDetailsOutputBeanList = new ArrayList<BudgetDetailsOutputBean>();
	MasProjectBO masProjectBO = new MasProjectBO();
	DatWorkorderBO datWorkorderBO;
	List<Integer> pcList = new ArrayList<Integer>();
	List<ProjDatClientBudgetBO> projDatClientBudgetBOList = null;
	List<ProjDatClientBudgetRateDetailBO> projDatClientBudgetRateDetailBOList = null;
	MasLocationParentBO masLocationParentBO;
	Short versionId;
	Integer statusId;
	try {
		projDatClientBudgetBOList = projectDatClientBudgetRepository
				.getByFkWorkOrderId(woId);
	} catch (Exception e) {
		throw new DataAccessException(
				"Failed to fetch the workorder details", e);
	}
	if (!projDatClientBudgetBOList.isEmpty()) {
		try {
			pcList = projectLevelUserAccessGroupRepository
					.getUserAccessByWorkOrderId(woId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to fetch the pclist details", e);
		}
		if (!pcList.isEmpty()) {
			for (ProjDatClientBudgetBO projDatClientBudgetBO : projDatClientBudgetBOList) {
				BudgetDetailsOutputBean budgetDetailsOutputBean = new BudgetDetailsOutputBean();
				try {
					datWorkorderBO = datWorkorderRepository
							.findByPkWorkorderId(woId);
				} catch (Exception e) {
					throw new DataAccessException(
							"Failed to fetch the project details", e);
				}
				masProjectBO = datWorkorderBO.getMasProject();
				pcList.add(masProjectBO.getFkDeliveryMgrId());
				pcList.add(masProjectBO.getFkDomainMgrId());
				if (pcList.contains(empId)) {
					budgetDetailsOutputBean
							.setPkClientBudgetId(projDatClientBudgetBO
									.getPkClientBudgetId());
					budgetDetailsOutputBean
							.setFkResourceDesignationId(projDatClientBudgetBO
									.getFkResourceDesignationId());
					budgetDetailsOutputBean
							.setFkResourceDesignationName(projDatClientBudgetBO
									.getMasEmpDesignationBO()
									.getEmpDesignationName());
					budgetDetailsOutputBean
							.setResourceLocation(projDatClientBudgetBO
									.getResourceLocation());
					budgetDetailsOutputBean
							.setFkParentLocationId(projDatClientBudgetBO
									.getFkParentLocationId());
					try {
						masLocationParentBO = parentLocationRepository
								.findByPkLocationParentId(projDatClientBudgetBO
										.getFkParentLocationId());
					} catch (Exception e) {
						throw new DataAccessException(
								"Failed to fetch location details", e);
					}
					if (masLocationParentBO != null) {
						budgetDetailsOutputBean
								.setFkParentLocationName(masLocationParentBO
										.getLocationParentName());
					}
					budgetDetailsOutputBean.setFkWorkorderId(woId);
					budgetDetailsOutputBean
							.setBudgetedDays(projDatClientBudgetBO
									.getBudgetedDays());
					budgetDetailsOutputBean
							.setIsBillingRateAvailable(projDatClientBudgetBO
									.getIsBillingRateAvailable());
					budgetDetailsOutputBean
							.setCostToClient(projDatClientBudgetBO
									.getCostToClient());
					budgetDetailsOutputBean
							.setCreatedBy(projDatClientBudgetBO
									.getCreatedBy());
					budgetDetailsOutputBean
							.setCreatedOn(projDatClientBudgetBO
									.getCreatedOn());
					budgetDetailsOutputBean
							.setNoOfResource(projDatClientBudgetBO
									.getNoOfResource());
					budgetDetailsOutputBean
							.setSowAmount(projDatClientBudgetBO
									.getDatWorkorder().getSowAmount());
					try {
						versionId = sowUploadRepository
								.getbyWorkorderId(projDatClientBudgetBO
										.getFkWorkOrderId());
					} catch (Exception e) {
						throw new DataAccessException(
								"Failed to fetch the version details", e);
					}
					budgetDetailsOutputBean.setVersionId(versionId);
					budgetDetailsOutputBean
							.setCurrencyTypeId(projDatClientBudgetBO
									.getDatWorkorder().getSowCurrencyId());
					budgetDetailsOutputBean
							.setCurrencyTypeName(projDatClientBudgetBO
									.getDatWorkorder().getMasCurrencytype()
									.getCurrencyTypeCode());
					try {
						projDatClientBudgetRateDetailBOList = projDatClientBudgetRateDetailsRepository
								.findByFkClientBudgetId(projDatClientBudgetBO
										.getPkClientBudgetId());
					} catch (Exception e) {
						throw new DataAccessException(
								"Failed to fetch the details", e);
					}
					if (!projDatClientBudgetRateDetailBOList.isEmpty()) {
						List<WeeklyOffBean> weeklyOffBeanList = new ArrayList<WeeklyOffBean>();
						List<InvoiceRateTypeBean> fkInvoiceRateTypeIdList = new ArrayList<InvoiceRateTypeBean>();
						List<HolidayDetailsBean> holidayDetailsBeanList = new ArrayList<HolidayDetailsBean>();
						for (ProjDatClientBudgetRateDetailBO projDatClientBudgetRateDetailBO : projDatClientBudgetRateDetailBOList) {
							if (projDatClientBudgetRateDetailBO
									.getFkWeeklyOffId() != null
									&& projDatClientBudgetRateDetailBO
											.getWeeklyOffFlag().equals(
													"YES")) {
								WeeklyOffBean weeklyOffBean = new WeeklyOffBean();
								weeklyOffBean
										.setClientBudgetRateDetailsId(projDatClientBudgetRateDetailBO
												.getPkClientBudgetRateDetailsId());
								weeklyOffBean
										.setRateTypeId(projDatClientBudgetRateDetailBO
												.getFkRateTypeId());
								weeklyOffBean.setInvoiceRateTypeId(projDatClientBudgetRateDetailBO
										.getFkInvoiceRateTypeId());
								weeklyOffBean
										.setWeeklyOffId(projDatClientBudgetRateDetailBO
												.getFkWeeklyOffId());
								weeklyOffBean
										.setValue(projDatClientBudgetRateDetailBO
												.getRateAmount());
								if (budgetDetailsOutputBean
										.getWeeklyOffFlag() == null
										|| (budgetDetailsOutputBean
												.getWeeklyOffFlag() != null && !budgetDetailsOutputBean
												.getWeeklyOffFlag()
												.equalsIgnoreCase(
														ProjectConstants.YES))) {
									budgetDetailsOutputBean
											.setWeeklyOffFlag(projDatClientBudgetRateDetailBO
													.getWeeklyOffFlag());
								}
								weeklyOffBeanList.add(weeklyOffBean);
							}
							if (projDatClientBudgetRateDetailBO
									.getFkInvoiceRateTypeId() != null) {
								
								InvoiceRateTypeBean invoiceRateTypeBean = new InvoiceRateTypeBean();
								invoiceRateTypeBean
										.setClientBudgetRateDetailsId(projDatClientBudgetRateDetailBO
												.getPkClientBudgetRateDetailsId());
								invoiceRateTypeBean
										.setRateTypeId(projDatClientBudgetRateDetailBO
												.getFkRateTypeId());
								invoiceRateTypeBean
										.setInvoiceRateTypeId(projDatClientBudgetRateDetailBO
												.getFkInvoiceRateTypeId());
								invoiceRateTypeBean
										.setValue(projDatClientBudgetRateDetailBO
												.getRateAmount());
								fkInvoiceRateTypeIdList
										.add(invoiceRateTypeBean);
							}
							if (projDatClientBudgetRateDetailBO
									.getHolidayFlag().equals("YES")) {
								
								HolidayDetailsBean holidayDetailsBean = new HolidayDetailsBean();
								holidayDetailsBean
										.setClientBudgetRateDetailsId(projDatClientBudgetRateDetailBO
												.getPkClientBudgetRateDetailsId());
								holidayDetailsBean
										.setRateTypeId(projDatClientBudgetRateDetailBO
												.getFkRateTypeId());
								holidayDetailsBean
										.setValue(projDatClientBudgetRateDetailBO
												.getRateAmount());
								holidayDetailsBean.setInvoiceRateTypeId(projDatClientBudgetRateDetailBO
										.getFkInvoiceRateTypeId());
								holidayDetailsBeanList
										.add(holidayDetailsBean);
							}
							if(projDatClientBudgetRateDetailBO.getFkRateTypeId() != null){
								
								budgetDetailsOutputBean
										.setRateAmount(projDatClientBudgetRateDetailBO
												.getRateAmount());
								budgetDetailsOutputBean
										.setRateTypeId(projDatClientBudgetRateDetailBO
												.getFkRateTypeId());
								budgetDetailsOutputBean
										.setRateTypeName(projDatClientBudgetRateDetailBO
												.getMasRateType()
												.getRateTypeName());									
							}
							
						}
						if (!weeklyOffBeanList.isEmpty()) {
							budgetDetailsOutputBean
									.setWeeklyOffDetails(weeklyOffBeanList);
						}
						if (!fkInvoiceRateTypeIdList.isEmpty()) {
							budgetDetailsOutputBean
									.setInvoiceRateTypeDetails(fkInvoiceRateTypeIdList);
						}
						if (!holidayDetailsBeanList.isEmpty()) {
							budgetDetailsOutputBean
									.setHolidayDetails(holidayDetailsBeanList);
						}
					}
					try {
						statusId = projClientBudgetApprovalDetailsRepository.findByFkClientBudgetId(projDatClientBudgetBO.getPkClientBudgetId());
					} catch (Exception e) {
						// e.printStackTrace();
						throw new DataAccessException("Unable to get status id's",e);
					}
					if(statusId != null)
					     budgetDetailsOutputBean.setStatus(statusId);
					else
						 budgetDetailsOutputBean.setStatus(draftStatus);
					budgetDetailsOutputBean
							.setAdditionalAmt(projDatClientBudgetBO
									.getDatWorkorder()
									.getAdditionalAmount());

					budgetDetailsOutputBeanList
							.add(budgetDetailsOutputBean);

				} else
					throw new DataAccessException(
							"You are not authorized person to view the details");
			}
		}
	} else
		throw new DataAccessException("Client Budget does not exist for a given work order");

	return budgetDetailsOutputBeanList;
}
//EOA by Sharanya 

//Added by prathibha for update client budget

public boolean updateClientBudget (ClientBudgetInputBean updateBean ) throws DataAccessException,CommonCustomException
{ 
	  LOG.info("Inside  update client budget service");
	  boolean updatedFlag = false;
	  ProjDatClientBudgetBO budgetBO1 = null;
	  BigDecimal costToClientTotal = null; 

	  
	//*****************************************************************
		DatWorkorderBO workorderDet = null;
		
		
		try {
			LOG.info("Workorder ID");
			workorderDet = datWorkorderRepos.findByPkWorkorderId(updateBean.getFkWorkorderId());
			LOG.debug("Workoder details"+workorderDet.toString());
		} catch (Exception e1) {
			throw new DataAccessException("Invalid workOrder Id.Please enter a valid work order-id");
		}
		//Work order validation	
		if(workorderDet != null)
		{   LOG.info("Inside Workoderdetails"+workorderDet.getPkWorkorderId());
			if(workorderDet.getSowUploadedFlag().equalsIgnoreCase("NOT UPLOADED"))
			{
				throw new CommonCustomException("Please upload SOW");
			}
			
		}
		//Validating sum of cost to client (Total)based on designations for particular work order does not exceed saw_amount(dat_workorder table)
		try
		{
			costToClientTotal = projectDatClientBudgetRepository.getCostToClientTotal(updateBean.getFkWorkorderId());
			LOG.info("Cost to client total CCCCCCCCCCC"+costToClientTotal.toBigInteger());
		}
		catch (Exception e)
		{
			throw new CommonCustomException("Fialed to fetch sum of cost_to client from DB");
		}
		
		if(costToClientTotal.compareTo(workorderDet.getSowAmount())== 1 || costToClientTotal.compareTo(workorderDet.getSowAmount())== 0 )
		{
			LOG.info("Comparing costToClient with sowAmount");
			throw new CommonCustomException("You cannot update cleint budget as costToclient Exceeds sowAmount OR costToclient is equal to sowAmount"+costToClientTotal+"SOW amount ="+workorderDet.getSowAmount());
		}
		
		//Validation whether  project coordinator mapped to mentioned project or not
		ProjDatProjectMappedToProjectCoordinatorBO pcmapped;
		try {
				pcmapped = projectMappedToCoordinatorRepos.finddByProjectandPCorDMorDL
					        (workorderDet.getFkProjectId(),updateBean.getUpdatedBy());
			} catch (Exception e1) {
					throw new DataAccessException("Unable to fetch records for credentials ");
			}
				LOG.info("Logged in person is PC OR NOT");
				if(pcmapped ==null)
				{
					LOG.info("Access denied for the logged in user");
					throw new CommonCustomException("Access denied for the logged in user");
				}
				
	  
	 if(updateBean.getPkClientBudgetId() != null)
	 {
	 budgetBO1 = projectDatClientBudgetRepository.findByPkClientBudgetId(updateBean.getPkClientBudgetId());
	 
	 if(budgetBO1 != null)
	 {   LOG.startUsecase("for saving updated data");
	 
	 	//Designation Id validation
		MasEmpDesignationBO designation = designationRepository.findByPkEmpDesignationId(updateBean.getFkResourceDesignationId());
			if(designation != null)
			{
				budgetBO1.setFkResourceDesignationId(updateBean.getFkResourceDesignationId());
			}
			else
			{
				throw new CommonCustomException("Please enter a valid designation Id");
			}
		 
		 budgetBO1.setResourceLocation(updateBean.getResourceLocation());
		//Parent location Id validation
			MasLocationParentBO masLocationParentBO	= parentLocationRepository.findByPkLocationParentId(updateBean.getFkParentLocationId());
			
				if(masLocationParentBO != null)
				{
					 budgetBO1.setFkParentLocationId(updateBean.getFkParentLocationId());
				}
				else
				{
					throw new CommonCustomException("Please enter a valid location Id");
				}
		 //updating client budget details 
		
		 budgetBO1.setFkWorkOrderId(updateBean.getFkWorkorderId());
		 budgetBO1.setBudgetedDays(updateBean.getBudgetedDays());
		 budgetBO1.setIsBillingRateAvailable(updateBean.getIsBillingRateAvailable());
		 budgetBO1.setCostToClient(updateBean.getCostToClient());
		 budgetBO1.setNoOfResource(updateBean.getNoOfResource());
		 budgetBO1.setUpdatedBy(updateBean.getUpdatedBy());
		 budgetBO1.setUpdatedOn(new Date());
		 try
		 {   LOG.endUsecase("for saving updated client data");
			 projectDatClientBudgetRepository.save(budgetBO1);
			 updatedFlag = true;
		 }
		 catch(Exception e)
		 {   
			 // e.printStackTrace();
			 throw new CommonCustomException("Exception occurred while saving data");
		 }
	 }
	 else
	 {
		 throw new CommonCustomException("Record for client budget Id not found");
	 }
}//if(updateBean.getPkClientBudgetId() != null) END
	 
	 if(updateBean.getWeeklyOffFlag().equalsIgnoreCase("YES") && updateBean.getHolidayFlag().equalsIgnoreCase("YES"))
	 {
		 LOG.info("Inside WeeklyOff-No && HolidayFlag-NO");
		 throw new CommonCustomException("Both WeeklyOffFlag and HolidayFlag cannot be YES.Please change the status"); 
	 }
	 
	 //Updateing rate details
	 if(updateBean.getWeeklyOffFlag().equalsIgnoreCase("NO") && updateBean.getHolidayFlag().equalsIgnoreCase("NO"))
	 {
		 LOG.info("WeeklyOff=NO && HolidayFlag=NO-------Normal day work");
		 
		 
		 for(RateDetailsBean updateRatedet : updateBean.getNormalrateDetails())
		 { 
			 LOG.startUsecase("Normal day work/////1");
			 
			 ProjDatClientBudgetRateDetailBO rateDetBO = new ProjDatClientBudgetRateDetailBO();
			 rateDetBO = projDatClientBudgetRateDetailsRepository
					 .findByPkClientBudgetRateDetailsId(updateRatedet.getPkClientBudgetRateDetId());
			 if(rateDetBO != null)
			 {
				 //Set budget rate table details
				 rateDetBO.setFkClientBudgetId(updateBean.getPkClientBudgetId());
				 rateDetBO.setFkInvoiceRateTypeId(updateRatedet.getInvoiceRateTypeid());
				 rateDetBO.setWeeklyOffFlag(updateBean.getWeeklyOffFlag());
				 rateDetBO.setHolidayFlag(updateBean.getHolidayFlag());
				 rateDetBO.setRateAmount(updateRatedet.getValue());
				 rateDetBO.setFkRateTypeId(updateRatedet.getFkRateTypeId());
				 rateDetBO.setFkWeeklyOffId(updateRatedet.getWeeklyOffId());
				 
				 try{
					 projDatClientBudgetRateDetailsRepository.save(rateDetBO);
				 }
				 catch (Exception e)
				 {
				 throw new CommonCustomException("Failed to save updated data");
				 }
				 
			 } //if(rateDetBO != null) End
			  
		 }//Normal day work for loop
		 updatedFlag = true;
		 
	 }
	 else if(updateBean.getWeeklyOffFlag().equalsIgnoreCase("YES"))
	{  
		LOG.startUsecase("Inside Weekly-Off = Yes") ;
		 for(RateDetailsBean updateRatedet : updateBean.getWeeklyOffRateDetails())
		 {  
			 LOG.startUsecase("Normal day work/////2");
			 
			 ProjDatClientBudgetRateDetailBO rateDetBO = new ProjDatClientBudgetRateDetailBO();
			 rateDetBO = projDatClientBudgetRateDetailsRepository
					 .findByPkClientBudgetRateDetailsId(updateRatedet.getPkClientBudgetRateDetId());
			 if(rateDetBO != null)
			 {
				 //Set budget rate table details
				 rateDetBO.setFkClientBudgetId(updateBean.getPkClientBudgetId());
				 rateDetBO.setFkInvoiceRateTypeId(updateRatedet.getInvoiceRateTypeid());
				 rateDetBO.setWeeklyOffFlag("YES");
				 rateDetBO.setHolidayFlag("NO");
				 rateDetBO.setRateAmount(updateRatedet.getValue());
				 rateDetBO.setFkRateTypeId(updateRatedet.getFkRateTypeId());
				 rateDetBO.setFkWeeklyOffId(updateRatedet.getWeeklyOffId());
				 try{
					 LOG.info("Saving Weely off data");
					 projDatClientBudgetRateDetailsRepository.save(rateDetBO);
				 }
				 catch (Exception e)
				 {
				 throw new CommonCustomException("Failed to save updated rate data ----Inside Weekly-Off = Yes");
				 }
				 
			 } //if(rateDetBO != null) End
			  
		 }//Normal day work for loop
		 LOG.endUsecase("Inside Weekly-Off = Yes");
		 updatedFlag = true;
	}
	
	//If Holiday flag is set yes 
	else if(updateBean.getHolidayFlag().equalsIgnoreCase("YES"))
	{
		LOG.startUsecase("Inside HolidayFlag=Yes ");
		for(RateDetailsBean updateRatedet : updateBean.getHolidayRateDetails())
		 {  
			 LOG.startUsecase("Normal day work/////3");
			 
			 ProjDatClientBudgetRateDetailBO rateDetBO = new ProjDatClientBudgetRateDetailBO();
			 try{
			 rateDetBO = projDatClientBudgetRateDetailsRepository
					 .findByPkClientBudgetRateDetailsId(updateRatedet.getPkClientBudgetRateDetId());
			 LOG.info("&&&&&&&&&&&"+updateRatedet.getPkClientBudgetRateDetId());
			 }
			 catch(Exception e)
			 {
				 // e.printStackTrace();
				 throw new CommonCustomException("Client rate detail record for given budget Id does not exist");
			 }
			 if(rateDetBO != null)
			 {   //ProjDatClientBudgetRateDetailBO   updateRateDetBO = new ProjDatClientBudgetRateDetailBO();
				 //Set budget rate table details
				 rateDetBO.setPkClientBudgetRateDetailsId(rateDetBO.getPkClientBudgetRateDetailsId());
				 rateDetBO.setFkClientBudgetId(updateBean.getPkClientBudgetId());
				 rateDetBO.setFkInvoiceRateTypeId(updateRatedet.getInvoiceRateTypeid());
				 rateDetBO.setWeeklyOffFlag("NO");
				 rateDetBO.setHolidayFlag(updateBean.getHolidayFlag());
				 rateDetBO.setRateAmount(updateRatedet.getValue());
				 rateDetBO.setFkRateTypeId(updateRatedet.getFkRateTypeId());
				 rateDetBO.setFkWeeklyOffId(updateRatedet.getWeeklyOffId());
				 LOG.info("UPDATED BO @@@@@@@@@@2"+rateDetBO.toString());
				 try{
					 projDatClientBudgetRateDetailsRepository.save(rateDetBO);
				 }
				 catch (Exception e)
				 {
				 throw new CommonCustomException("Failed to save updated rate data ----Inside Weekly-Off = Yes");
				 }
				 
			 } //if(rateDetBO != null) End
			  
		 }//Normal day work for loop
		 LOG.endUsecase("Inside HolidayFlag=Yes");
		 updatedFlag = true;
		
	}

	 return updatedFlag;  
}

}

