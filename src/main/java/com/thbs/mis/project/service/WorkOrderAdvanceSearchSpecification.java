package com.thbs.mis.project.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.project.bean.WorkOrderViewInputBean;
import com.thbs.mis.project.bo.DatWorkorderBO;

public class WorkOrderAdvanceSearchSpecification {

	public static Specification<DatWorkorderBO> getAllWorkOrderDetails(WorkOrderViewInputBean inputBean) {
		return new Specification<DatWorkorderBO>() {

			@Override
			public Predicate toPredicate(Root<DatWorkorderBO> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				final Collection<Predicate> predicates = new ArrayList<>();
				Date fromDate = new Date();
				Date toDate = new Date();
				if(inputBean.getFromDate()!= null &&inputBean.getToDate()!= null) {
					SimpleDateFormat sdfWithSeconds = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					String stringFromDate = sdf.format(inputBean.getFromDate());
					String stringToDate = sdf.format(inputBean.getToDate());
					stringFromDate = stringFromDate + " 00:00:00";
					stringToDate = stringToDate + " 23:59:59";
					try {
						fromDate = sdfWithSeconds.parse(stringFromDate);
						toDate = sdfWithSeconds.parse(stringToDate);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				if (inputBean.getProjectId() != null) {
					final Predicate project = cb.equal(root.get("fkProjectId"), inputBean.getProjectId().intValue());
					predicates.add(project);
				}
				/*if (inputBean.getPoMapped() != null) {
					final Predicate projectPoMapped = cb.equal(root.get("fkProjectId"), inputBean.getPoMapped());
					predicates.add(projectPoMapped);
				}*/
				if (inputBean.getWoNumber() != null) {
					final Predicate woNumber = cb.equal(root.get("workorderNumber"), inputBean.getWoNumber());
					predicates.add(woNumber);
				}
				if (inputBean.getWoStatus() != 0) {
					final Predicate woStatus = cb.equal(root.get("fkWorkorderStatusId"), inputBean.getWoStatus());
					predicates.add(woStatus);
				}
				if (inputBean.getWoType() != null) {
					final Predicate woType = cb.equal(root.get("isInternal"), inputBean.getWoType());
					predicates.add(woType);
				}
				if (inputBean.getFromDate() != null && inputBean.getToDate() != null) {
					final Predicate dateRangePredicate = cb.or(cb.and(
							cb.greaterThanOrEqualTo(root.get("woStartDate"), fromDate),
							cb.lessThanOrEqualTo(root.get("woStartDate"), toDate)),cb.and(cb
									.greaterThanOrEqualTo(root.get("woEndDate"),
											fromDate), cb.lessThanOrEqualTo(
									root.get("woEndDate"), toDate)));
					predicates.add(dateRangePredicate);
				}
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
	}
}