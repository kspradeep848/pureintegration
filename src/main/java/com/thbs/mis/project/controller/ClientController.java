package com.thbs.mis.project.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thbs.mis.common.bean.MasClientOutputBean;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.project.bean.ClientDetailsOutputBean;
import com.thbs.mis.project.bean.ClientInputBean;
import com.thbs.mis.project.bean.ClientOutputBean;
import com.thbs.mis.project.bean.ClientSearchInputBean;
import com.thbs.mis.project.bean.ClientSearchOutputBean;
import com.thbs.mis.project.bean.MasClientAddressOutputBean;
import com.thbs.mis.project.bean.ProjMasClientManagerBean;
import com.thbs.mis.project.constants.ProjectURIConstants;
import com.thbs.mis.project.service.ClientService;
import com.thbs.mis.project.service.ProjectService;

@Controller
public class ClientController {
	private static final AppLog LOG = LogFactory.getLog(ProjectService.class);

	@Autowired
	private ClientService clientService;

	// Added by Rinta Mariam Jose
	
	/**
	 * @category This service is used for UI page to add a new client.
	 *           <Description addClient> : Add a Client
	 * @param clientDetails
	 * @param uploadClientDocs
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @throws BusinessException 
	 */
	
	@ApiOperation(value = "Create client detail. This service will be called when user create new client detail"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to create the new client details."
			+ "<br>The Response will be the MISResponse.",

	nickname = "Create new client", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully created", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Successfully created", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Client Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Client Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.CREATE_CLIENT, method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> addNewClient(
			@Valid @RequestParam(value = "clientDetails", name = "clientDetails", required = true) String clientDetails,
			@RequestParam(value = "uploadClientDocs", name = "uploadClientDocs", required = false) List<MultipartFile> uploadClientDocs)
			throws CommonCustomException, DataAccessException, BusinessException, ConstraintViolationException{
		boolean addClientReq = false;
		ObjectMapper objectMapper = new ObjectMapper();
		ClientInputBean addClientInputBean = new ClientInputBean();

		try {
			addClientInputBean = objectMapper.readValue(clientDetails,
					ClientInputBean.class);
		} catch (JsonMappingException e) {
			// e.printStackTrace();
			throw new BusinessException("Some exception occurred while mapping bean ");
		} catch (IOException e) {
			// e.printStackTrace();
			throw new BusinessException("Some IOException occurred while mapping bean ");
		}			

		try {
			addClientReq = clientService.addClient(addClientInputBean,
					uploadClientDocs);
		} catch (DataAccessException e) {
			// e.printStackTrace();
			throw new DataAccessException(e.getMessage());
		} catch (ConstraintViolationException e) {
			// e.printStackTrace();
			throw new ConstraintViolationException(e.getConstraintViolations());
		} catch (CommonCustomException e) {
			// e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}

		if (addClientReq) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Client created successfully."));
		} else {
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE, "Failed to create client."));

		}
	}
	
	
	/**
	 * @category This service is used for UI page to update client details.
	 *           <Description updateClient> : Update client details.
	 * @param clientDetails
	 * @param uploadClientDocs
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @throws BusinessException
	 */
	
	@ApiOperation(value = "Update client details. This service will be called when user wants to update client detail"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to update client details."
			+ "<br>The Response will be the MISResponse.",

	nickname = "Update client", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully created", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Successfully created", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Update Client Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Update Client Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.UPDATE_CLIENT, method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> updateClient(
			@Valid @RequestParam(value = "clientDetails", name = "clientDetails", required = true) String clientDetails,
			@RequestParam(value = "uploadClientDocs", name = "uploadClientDocs", required = false) List<MultipartFile> uploadClientDocs)
			throws CommonCustomException, DataAccessException, BusinessException, ConstraintViolationException {

		boolean addClientReq = false;
		ObjectMapper objectMapper = new ObjectMapper();
		ClientInputBean addClientInputBean = new ClientInputBean();

		try {
			addClientInputBean = objectMapper.readValue(clientDetails,
					ClientInputBean.class);
		} catch (JsonMappingException e) {
			// e.printStackTrace();
			throw new BusinessException("Some exception occurred while mapping bean ");
		} catch (IOException e) {
			// e.printStackTrace();
			throw new BusinessException("Some IOException occurred while mapping bean ");
		}	

		try {
			addClientReq = clientService.updateClient(addClientInputBean,
					uploadClientDocs);
		} catch (DataAccessException e) {
			// e.printStackTrace();
			throw new DataAccessException(e.getMessage());
		} catch (ConstraintViolationException e) {
			// e.printStackTrace();
			throw new ConstraintViolationException(e.getConstraintViolations());
		} catch (CommonCustomException e) {
			// e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}

		if (addClientReq) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Client updated successfully."));
		} else {
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE, "Failed to update client."));
		}
	}
	
	
	/**
	 * <Description getClientDetailsBasedOnClientId> This service will be called
	 * when user want to view client details based on clientId
	 * 
	 * @param clientId
	 * @return MISResponse
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	@ApiOperation(value = "Get Client Details based on clientId. This service will be called when user want to view client details based on clientId"
			+ " .", httpMethod = "GET", notes = "This Service has been implemented to view client details based on clientId."
			+ "<br>The Response will be the ClientOutputBean.", nickname = "GET Client Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ClientOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = ClientOutputBean.class),
			@ApiResponse(code = 201, message = "Client Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Access denied", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Fail to fetch  the client details", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_CLIENT_DETAILS_BASED_ON_CLIENT_ID, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getClientDetailsBasedOnClientId(
			@PathVariable Short clientId, @PathVariable Integer empId)
			throws DataAccessException, CommonCustomException {
		List<ClientOutputBean> clientOutputBeanList;

		try {
			clientOutputBeanList = clientService
					.getClientDetailsBasedOnClientId(clientId, empId);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}

		if (!clientOutputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Client details are fetched successfully.",
							clientOutputBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No records found.",
							clientOutputBeanList));
		}
	}
	

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllProjMasClientManager> : Fetch All Client
	 *           Managers.
	 * @return MISResponse
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	@ApiOperation(value = "Get all Client Managers. This service will be called when user wants to view list of Client Managers in UI"
			+ " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch all present Client Managers."
			+ "<br>The Response will be the list of all Client Managers.",

	nickname = "Get all Client Managers", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ProjMasClientManagerBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = ProjMasClientManagerBean.class),
			@ApiResponse(code = 201, message = "All Client Manager Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Client Managers", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Client Managers", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_ALL_CLIENT_MANAGERS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllProjMasClientManager()
			throws DataAccessException, CommonCustomException {

		List<ProjMasClientManagerBean> projMasClientManagerBeanList = null;

		try {
			projMasClientManagerBeanList = clientService
					.getAllProjMasClientManager();
		} catch (CommonCustomException e) {
			throw new CommonCustomException(
					"Failed to retrieve list of client managers :  "
							+ e.getMessage());
		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Failed to retrieve list of client managers :  "
							+ e.getMessage());
		} catch (Exception e) {
			throw new CommonCustomException(
					"Failed to retrieve list of client managers :  "
							+ e.getMessage());
		}

		if (!projMasClientManagerBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully fetched the client Managers details",
							projMasClientManagerBeanList));
		} else {
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Failed to fetch the client manager details.",
							projMasClientManagerBeanList));
		}
	}
	

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getClientAddress> : Fetch Client Address according
	 *           to given clientId.
	 * @param clientId
	 * @return MISResponse
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	@ApiOperation(value = "Get Client Address according to given clientId.. This service will be called when user wants to view Client Address according to given clientId."
			+ " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch Client Address according to given clientId.."
			+ "<br>The Response will be Client Address according to given clientId..",

	nickname = "Get Client Address according to given clientId.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MasClientAddressOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MasClientAddressOutputBean.class),
			@ApiResponse(code = 201, message = "Client Address Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Address according to given clientId.", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Address according to given clientId.", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_CLIENT_ADDRESS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getClientAddress(
			@PathVariable short clientId) throws DataAccessException,
			CommonCustomException {
		List<MasClientAddressOutputBean> masClientAddressOutputBeanList = null;

		try {
			masClientAddressOutputBeanList = clientService
					.getClientAddress(clientId);
		} catch (CommonCustomException e) {
			throw new CommonCustomException("Failed to fetch client address: "
					+ e.getMessage());
		} catch (DataAccessException e) {
			throw new DataAccessException("Failed to fetch client address: "
					+ e.getMessage());
		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch client address: "
					+ e.getMessage());
		}

		if (!masClientAddressOutputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Fetched client address",
							masClientAddressOutputBeanList));
		} else {
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(
							HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Client address is not availble for the given clientId.",
							masClientAddressOutputBeanList));
		}
	}

	// EOA by Rinta Mariam Jose


	/**
	 * @category This service is used for UI page to Retrieve Client Detail
	 *           based on input search . <Description ClientSearchOutputBean> : Client
	 *           Detail based on input search.
	 * @param searchBean
	 * @return List<ClientSearchOutputBean>
	 * @throws DataAccessException
	 * @throws CommonCustomException 
	 */
	@ApiOperation(value = "Get Client detail Details. This service will be called when user want to view client detail based on search input"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to View client detail based on search input."
			+ "<br>The Response will be the ClientSearchOutputBean.",

	nickname = "GET client detail based on search input", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ClientSearchOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = ClientSearchOutputBean.class),
			@ApiResponse(code = 201, message = "All Client Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "ClientSearch Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "ClientSearch Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_CLIENT_BASED_ON_INPUT_SEARCH, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> getClientDetailBasedOnInput(
			@RequestBody ClientSearchInputBean searchBean)
			throws DataAccessException, CommonCustomException {
		Page<ClientSearchOutputBean> clientSearchOutputBeanList =null;
		try {
			clientSearchOutputBeanList = clientService
					.getClientBasedOnSearch(searchBean);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (!clientSearchOutputBeanList.getContent().isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Searched client details successfully.", clientSearchOutputBeanList));
		}
		else if (clientSearchOutputBeanList.getContent().isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No records found.", clientSearchOutputBeanList));
		}
		else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE,
							"Fail to search client details."));
		}
	}

	
	/**
	 * @category This service is used for UI page to download client documents.
	 *           <Description downloadSupportingDocsByClient> : Download Client
	 *           documents.
	 * @param clientId
	 * @param response
	 * @return MISResponse
	 * @throws CommonCustomException
	 */
	
	
	@ApiOperation(value = "Download client documents. This service will be called when user want to download client documents"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to download client documents."
			+ "<br>The Response will be the MISResponse.",

	nickname = "Download client documents", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "All Client documents are downloaded successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Download client documents", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Download client documents", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.DOWNLOAD_SUPPORTING_DOC_CLIENT, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> downloadSupportingDoc(
			@PathVariable Short clientId, HttpServletResponse response)
			throws CommonCustomException {
		LOG.entering("download supporting docs");
		Boolean isDownloaded = false;
		try {
			isDownloaded = clientService.downloadSupportingDocsByClient(
					clientId, response);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		
		LOG.exiting("download supporting docs");
		if (isDownloaded) {

			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "supporting docs downloaded"));
		} else {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE, "No file for given input"));
		}
	}

	
	/**
	 * @category This service is used for UI page to show active and inactive
	 *           clients. <Description getAllActiveAndInactiveClient> : Fetch
	 *           All Active and Inactive Clients.
	 * @param statusId
	 * @return MISResponse
	 * @throws BusinessException
	 * @throws CommonCustomException
	 */

	@ApiOperation(value = "Get Client details based on status. This service will be called when user want to view client details based on status"
			+ " .", httpMethod = "GET",

	notes = "This Service has been implemented to View client details based on status."
			+ "<br>The Response will be the MasClientOutputBean.",

	nickname = "GET client details based on status", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MasClientOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MasClientOutputBean.class),
			@ApiResponse(code = 201, message = "All Client Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Details based on status", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Details based on status", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_CLIENT_BASED_ON_STATUS_ID, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAllClientDetails(
			@PathVariable byte statusId) throws BusinessException,
			CommonCustomException {
		List<MasClientOutputBean> masClientOutputBeanList = null;
		try {
			masClientOutputBeanList = clientService
					.getAllActiveAndInactiveClient(statusId);
		} catch (CommonCustomException e) {
			// e.printStackTrace();
			throw new CommonCustomException("Failed to fetch :"
					+ e.getMessage());
		} catch (Exception e) {
			// e.printStackTrace();
			throw new CommonCustomException("Failed to fetch :"
					+ e.getMessage());
		}
		
		if (!masClientOutputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"successfully fetched the client details.",
							masClientOutputBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No record found.",
							masClientOutputBeanList));
		}
	}
	
	/**
	 * @category This service is used for UI page to show clients based on BU.
	 *           <Description getClientDetailsBasedOnBuId> : Fetch All clients
	 *           based on BU.
	 * @param buId
	 * @return MISResponse
	 * @throws BusinessException
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */

	@ApiOperation(value = "Get Client details based on BU. This service will be called when user want to view client details based on BU"
			+ " .", httpMethod = "GET",

	notes = "This Service has been implemented to View client details based on BU."
			+ "<br>The Response will be the ClientDetailsOutputBean.",

	nickname = "GET client details based on BU", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ClientDetailsOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = ClientDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "All Client Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Details based on BU", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Details based on BU", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_CLIENT_BASED_ON_BU_ID, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getClientDetailsBasedOnBuId(
			@PathVariable short buId) throws BusinessException,
			CommonCustomException, DataAccessException {
		Set<ClientDetailsOutputBean> clientDetailsOutputBean = null;
		try {
			clientDetailsOutputBean = clientService
					.getClientDetailsBasedOnBuId(buId);
		} catch (DataAccessException e) {
			// e.printStackTrace();
			throw new DataAccessException("Failed to fetch :"
					+ e.getMessage());
		} catch (Exception e) {
			// e.printStackTrace();
			throw new CommonCustomException("Failed to fetch :"
					+ e.getMessage());
		}
		
		if (clientDetailsOutputBean != null && !clientDetailsOutputBean.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"successfully fetched the client details.",
							new ArrayList<ClientDetailsOutputBean>(clientDetailsOutputBean)));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No record found."));
		}
	}
	
	/**
	 * @category This service is used for UI page to show clients based on
	 *           region. <Description getClientDetailsBasedOnRegionId> : Fetch
	 *           All clients based on region.
	 * @param regionId
	 * @return MISResponse
	 * @throws BusinessException
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */

	@ApiOperation(value = "Get Client details based on region. This service will be called when user want to view client detail based on region"
			+ " .", httpMethod = "GET",

	notes = "This Service has been implemented to View client detail based on region."
			+ "<br>The Response will be the MasClientOutputBean.",

	nickname = "GET client detail based on region", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MasClientOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MasClientOutputBean.class),
			@ApiResponse(code = 201, message = "All Client Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Details based on region", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Details based on region", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_CLIENT_BASED_ON_REGION_ID, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getClientDetailsBasedOnRegionId(
			@PathVariable short regionId) throws BusinessException,
			CommonCustomException, DataAccessException {
		Set<MasClientOutputBean> masClientOutputBeanList = null;
		try {
			masClientOutputBeanList = clientService
					.getClientDetailsBasedOnRegionId(regionId);
		} catch (DataAccessException e) {
			// e.printStackTrace();
			throw new DataAccessException("Failed to fetch :" + e.getMessage());
		} catch (Exception e) {
			// e.printStackTrace();
			throw new CommonCustomException("Failed to fetch :"
					+ e.getMessage());
		}

		if (masClientOutputBeanList != null
				&& !masClientOutputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"successfully fetched the client details.",
							new ArrayList<MasClientOutputBean>(
									masClientOutputBeanList)));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No record found."));
		}
	}

}