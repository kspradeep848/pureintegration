package com.thbs.mis.project.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.URI;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.project.bean.DatWorkorderBean;
import com.thbs.mis.project.bean.ProjMasWoStatusBean;
import com.thbs.mis.project.bean.ProjectHistoryOutputBean;
import com.thbs.mis.project.bean.ProjectNamesOutputBean;
import com.thbs.mis.project.bean.SowFileDownloadInput;
import com.thbs.mis.project.bean.SowFileUploadInput;
import com.thbs.mis.project.bean.WoNumberOutputBean;
import com.thbs.mis.project.bean.WorkOrderDocOutputBean;
import com.thbs.mis.project.bean.WorkOrderFullViewOutputBean;
import com.thbs.mis.project.bean.WorkOrderInputBean;
import com.thbs.mis.project.bean.WorkOrderOutputBean;
import com.thbs.mis.project.bean.WorkOrderViewInputBean;
import com.thbs.mis.project.bean.WorkOrderViewOutputBean;
import com.thbs.mis.project.constants.ProjectURIConstants;
import com.thbs.mis.project.service.WorkOrderService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@Controller
public class WorkOrderController {
	/**
	 * Instance of <code>LOG</code>
	 */
	private static final AppLog LOG = LogFactory.getLog(WorkOrderController.class);

	@Autowired
	private WorkOrderService workOrderService;

	ObjectMapper objectMapper = new ObjectMapper();

	@ApiOperation(tags = "Project Module", value = "create work order"
			+ "This service will be called from the front-end when dm/dl/pc create work order", httpMethod = "POST", notes = "This Service has been implemented to create work order"
					+ "<br>The Details will be fetched and submit data into database table"
					+ "<br> <b> Table : </b> </br>" + "dat_workorder table"
					+ "<br>", nickname = "create work order", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = WorkOrderOutputBean.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })

	@RequestMapping(value = ProjectURIConstants.WORK_ORDER, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> createWorkOrder(@Valid @RequestBody WorkOrderInputBean workOrderDetails)
			throws CommonCustomException, DataAccessException {
		WorkOrderOutputBean workOrderOutputBean = null;
		List<WorkOrderOutputBean> output = new ArrayList<WorkOrderOutputBean>();
		try {
			workOrderOutputBean = workOrderService.saveWorkOrder(workOrderDetails);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		output.add(workOrderOutputBean);
		if (workOrderOutputBean != null) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "WORK ORDER CREATED SUCCESSFULLY", output));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(
					HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, "UNABLE TO CREATE WORK ORDER"));
		}
	}

	@ApiOperation(tags = "Project Module", value = "update work order"
			+ "This service will be called from the front-end when dm/dl/pc update work order", httpMethod = "PUT", notes = "This Service has been implemented to update work order"
					+ "<br>The Details will be fetched and submit data into database table"
					+ "<br> <b> Table : </b> </br>" + "dat_workorder table"
					+ "<br>", nickname = "update work order", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = WorkOrderOutputBean.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.WORK_ORDER, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> updateWorkOrder(@Valid @RequestBody WorkOrderInputBean workOrderDetails)
			throws CommonCustomException, DataAccessException {
		WorkOrderOutputBean workOrderOutputBean = null;
		List<WorkOrderOutputBean> output = new ArrayList<WorkOrderOutputBean>();
		try {
			workOrderOutputBean = workOrderService.updateWorkOrder(workOrderDetails);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (workOrderOutputBean != null) {
			output.add(workOrderOutputBean);
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "WORK ORDER UPDATED SUCCESSFULLY", output));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(
					HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, "UNABLE TO UPDATE WORK ORDER"));
		}
	}

	@ApiOperation(tags = "Project Module", value = "view work order"
			+ "This service will be called from the front-end when dm/dl/pc/finance view work order", httpMethod = "POST", notes = "This Service has been implemented to view work order"
					+ "<br>The Details will be fetched data from database table" + "<br> <b> Table : </b> </br>"
					+ "dat_workorder table"
					+ "<br>", nickname = "view work order", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = WorkOrderViewOutputBean.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.VIEW_WORK_ORDER, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> viewWorkOrder(@Valid @RequestBody WorkOrderViewInputBean workOrderViewInputBean)
			throws CommonCustomException, DataAccessException {
		Page<WorkOrderViewOutputBean> workOrderViewOutputBean = null;
		System.out.println("workOrderDetailsworkOrderDetailsworkOrderDetails" + workOrderViewInputBean.toString());
		try {
			workOrderViewOutputBean = workOrderService.viewWorkOrder(workOrderViewInputBean);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (!workOrderViewOutputBean.getContent().isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "SEARCHED WORK ORDER DETAILS SUCCESSFULLY", workOrderViewOutputBean));
		}
		else if (workOrderViewOutputBean.getContent().isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No records found", workOrderViewOutputBean));
		}
		else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE,
							"FAIL TO SEARCH WORK ORDER DETAILS"));
		}
	}

	@ApiOperation(tags = "Project Module", value = "full view of particular work order"
			+ "This service will be called from the front-end when dm/dl/pc/finance full view of particular work order", httpMethod = "GET", notes = "This Service has been implemented full view of particular work order"
					+ "<br>The Details will be fetched data from database table" + "<br> <b> Table : </b> </br>"
					+ "dat_workorder table"
					+ "<br>", nickname = "full view work order", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = WorkOrderFullViewOutputBean.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.FULL_VIEW_WORK_ORDER, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> fullViewWorkOrder(@PathVariable("workorderId") Integer workorderId,
			@PathVariable("empId") Integer empId) throws CommonCustomException, DataAccessException {
		WorkOrderFullViewOutputBean workOrderFullViewOutputBean = null;
		try {
			workOrderFullViewOutputBean = workOrderService.fullViewWorkOrder(workorderId, empId);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		List<WorkOrderFullViewOutputBean> output = new ArrayList<WorkOrderFullViewOutputBean>();
		output.add(workOrderFullViewOutputBean);
		if (workOrderFullViewOutputBean != null) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "WORK ORDER FULL VIEW FETCHED SUCCESSFULLY", output));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(
					HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, "FAIL TO VIEW WORK ORDER DETAILS"));
		}
	}

	/*@ApiOperation(tags = "Project Module", value = "upload sow file"
			+ "This service will be called from the front-end when dm/dl/pc to upload sow file", httpMethod = "POST", notes = "This Service has been implemented to upload sow file for work order"
					+ "<br>The Details will be inserted  to database table" + "<br> <b> Table : </b> </br>"
					+ "dat_sow_upload_history table"
					+ "<br>", nickname = "upload sow file", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = WorkOrderFullViewOutputBean.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.SOW_UPLOAD, method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> uploadSowFile(
			@RequestParam(value = "workorderJson", name = "workorderJson", required = true, defaultValue = "") String workorderJson,
			@RequestParam(value = "sowFile", name = "sowFile", required = false) MultipartFile sowFile)
			throws CommonCustomException, DataAccessException, JSONException, IOException {
		boolean isUploaded = false;
		SowFileUploadInput sowFileUploadInput = new SowFileUploadInput();
		sowFileUploadInput = objectMapper.readValue(workorderJson, SowFileUploadInput.class);
		try {
			isUploaded = workOrderService.uploadSowFile(sowFile, sowFileUploadInput);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (isUploaded) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "SOW FILE UPLOADED SUCCESSFULLY"));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(
					HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, "FAIL TO UPLOAD SOW FILE"));
		}
	}
*/
	

@ApiOperation(tags = "Project Module", value = "upload sow file"
+ "This service will be called from the front-end when dm/dl/pc to upload sow file", httpMethod = "POST", notes = "This Service has been implemented to upload sow file for work order"
+ "<br>The Details will be inserted to database table" + "<br> <b> Table : </b> </br>"
+ "dat_sow_upload_history table"
+ "<br>", nickname = "upload sow file", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
@ApiResponses(value = {
@ApiResponse(code = 200, message = "Request Executed Successfully", response = WorkOrderFullViewOutputBean.class),
@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
@RequestMapping(value = ProjectURIConstants.SOW_UPLOAD, method = RequestMethod.POST, consumes = "multipart/form-data")
public ResponseEntity<MISResponse> uploadSowFile(
@RequestParam(value = "workorderJson", name = "workorderJson", required = true, defaultValue = "") String workorderJson,
@RequestParam(value = "sowFile", name = "sowFile", required = false) MultipartFile sowFile,
@RequestParam(value = "supportingDocs", name = "supportingDocs", required = false) List<MultipartFile> supportingDocs)
		throws CommonCustomException, DataAccessException, JSONException, IOException {
	boolean isUploaded = false;
	SowFileUploadInput sowFileUploadInput = new SowFileUploadInput();
	sowFileUploadInput = objectMapper.readValue(workorderJson, SowFileUploadInput.class);
	try {
		try {
			isUploaded = workOrderService.uploadSowFile(sowFile, sowFileUploadInput,supportingDocs);
		} catch (Exception e) {
		
			throw new CommonCustomException(e.getMessage());
		}
	} catch (Exception e) {
		throw new CommonCustomException(e.getMessage());
	}
	if (isUploaded) {
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "SOW FILE UPLOADED SUCCESSFULLY"));
	} else {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(
				HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, "FAIL TO UPLOAD SOW FILE"));
	}
}

	
	
	
	@ApiOperation(tags = "Project Module", value = "download sow file"
			+ "This service will be called from the front-end when dm/dl/pc to download sow file", httpMethod = "POST", notes = "This Service has been implemented to download sow file"
					+ "<br>The Details will be fetched data from database table" + "<br> <b> Table : </b> </br>"
					+ "dat_sow_upload_history table"
					+ "<br>", nickname = "download sow file", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = WorkOrderFullViewOutputBean.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.SOW_DOWNLOAD, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> downloadSowFile(
			@RequestBody SowFileDownloadInput inputBean,HttpServletResponse response)
			throws CommonCustomException, DataAccessException, JSONException, IOException {
		boolean downloaded = false;
		try {
			downloaded = workOrderService.downloadSowFile(inputBean, response);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (downloaded) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "FILE DOWNLOADED SUCCESSFULLY"));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(
					HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, "FAIL TO DOWNLOAD FILE"));
		}
	}

	
	@ApiOperation(tags = "Project Module", value = "get particular work order"
			+ "This service will be called from the front-end when dm/dl/pc/finance full get of particular work order", httpMethod = "GET", notes = "This Service has been implemented to get work order"
					+ "<br>The Details will be fetched data from database table" + "<br> <b> Table : </b> </br>"
					+ "dat_workorder table"
					+ "<br>", nickname = "get work order", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = WorkOrderOutputBean.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_WORK_ORDER, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getWorkOrder(@PathVariable("workorderId") Integer workorderId,
			@PathVariable("empId") Integer empId) throws CommonCustomException, DataAccessException {
		WorkOrderOutputBean workOrderFullViewOutputBean = null;
			try {
				workOrderFullViewOutputBean = workOrderService.getWorkOrder(workorderId,empId);
			} catch (Exception e) {
				throw new CommonCustomException(e.getMessage());
			}
			List<WorkOrderOutputBean> output = new ArrayList<WorkOrderOutputBean>();
			output.add(workOrderFullViewOutputBean);
			if (workOrderFullViewOutputBean != null) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS, "SUCCESSFULLY FETCHED WORK ORDER DETAILS",output));
			} else {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(
						HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, "FAIL TO GET WORK ORDER DETAILS"));
			}
		} 
	
	
	// Added by Rinta Mariam Jose

		/**
		 * @category This service is used for UI page to show the drop down list.
		 *           <Description getAllProjMasWoStatus> : Fetch All Work Order
		 *           Status.
		 * @param
		 * @return List<ProjMasWoStatusBean>
		 * @throws BusinessException
		 * @throws DataAccessException
		 * @throws CommonCustomException 
		 */
		@ApiOperation(value = "Get all Work Order Status. This service will be called when user wants to view list of Work Order Status in UI"
				+ " .", httpMethod = "GET",

		notes = "This Service has been implemented to fetch all present Work Order Status."
				+ "<br>The Response will be the list of all Work Order Status.",

		nickname = "Get all Work Order Status", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ProjMasWoStatusBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = ProjMasWoStatusBean.class),
				@ApiResponse(code = 201, message = "All Work Order Status Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Work Order Status", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Work Order Status", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = ProjectURIConstants.GET_ALL_WORK_ORDER_STATUS, method = RequestMethod.GET)
		public @ResponseBody ResponseEntity<MISResponse> getAllProjMasWoStatus()
				throws DataAccessException, CommonCustomException {

			List<ProjMasWoStatusBean> projMasWoStatusBeanList = null;

			try {
				projMasWoStatusBeanList = workOrderService
						.getAllProjMasWorkOrderStatus();
			} catch (Exception e) {
				throw new CommonCustomException(
						"Failed to fetch the work order status : "+ e.getMessage());
			}

			if (!projMasWoStatusBeanList.isEmpty()) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Successfully fetched the work order status",
								projMasWoStatusBeanList));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								MISConstants.FAILURE,
								"Failed to fetch the work order status.",
								projMasWoStatusBeanList));
			}
		}

		// EOA by Rinta Mariam Jose

		@ApiOperation(tags = "Project Module", value = "get doc details"
				+ "This service will be called from the front-end when dm/dl/pc/finance view doc details of work order", httpMethod = "GET", notes = "This Service has been implemented  view of doc details"
						+ "<br>The Details will be fetched data from database table" + "<br> <b> Table : </b> </br>"
						+ "dat_sow_upload_history & dat_sow_support_docs_upload_history table"
						+ "<br>", nickname = " view doc details of work order", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = WorkOrderFullViewOutputBean.class),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = ProjectURIConstants.DOC_DETAILS, method = RequestMethod.GET)
		public ResponseEntity<MISResponse> getDocDetails(@PathVariable("workorderId") Integer workorderId,
				@PathVariable("empId") Integer empId) throws CommonCustomException, DataAccessException {
			WorkOrderDocOutputBean workOrderDocOutputBean = null;
			try {
				workOrderDocOutputBean = workOrderService.getDocDetails(workorderId, empId);
			} catch (Exception e) {
				throw new CommonCustomException(e.getMessage());
			}
			List<WorkOrderDocOutputBean> output = new ArrayList<WorkOrderDocOutputBean>();
			output.add(workOrderDocOutputBean);
			if (workOrderDocOutputBean != null) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS, "DOC DETAILS FETCHED SUCCESSFULLY", output));
			} else {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(
						HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, "FAIL TO GET DOC DETAILS"));
			}
		}
		
		/**
		 * @category This service is used for UI page to show the drop down list.
		 *           <Description getAllProjMasWoStatus> : Fetch All Work Order
		 *           Status.
		 * @param
		 * @return List<ProjMasWoStatusBean>
		 * @throws BusinessException
		 * @throws DataAccessException
		 */
		@ApiOperation(value = "Get all Work Orders for a particular project. This service will be called when user wants to view list ofWork Orders for a particular project in UI"
				+ " .", httpMethod = "GET",

		notes = "This Service has been implemented to fetch all present Work Orders for a particular project."
				+ "<br>The Response will be the list of all Work Orders for a particular project.",

		nickname = "Get all Work Orders for a particular project", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = DatWorkorderBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = DatWorkorderBean.class),
				@ApiResponse(code = 201, message = "All Work Order Status Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Work Orders for a particular project", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Work Orders for a particular project", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = ProjectURIConstants.GET_ALL_WO_BY_PROJECTID, method = RequestMethod.GET)
		public @ResponseBody ResponseEntity<MISResponse> getAllWoByProjectId(@PathVariable("projectId") Integer projectId)
				throws DataAccessException,CommonCustomException {

			List<DatWorkorderBean> datWorkorderBeanList = null;

			try {
				datWorkorderBeanList = workOrderService.getAllWorkOrderByProjectId(projectId);
						
			} catch (Exception e) {
				throw new CommonCustomException(
						"Failed to fetch the work orders : "+e.getMessage());
			}

			if (!datWorkorderBeanList.isEmpty()) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Successfully fetched the work orders for given ptojectid",
								datWorkorderBeanList));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								MISConstants.FAILURE,
								"Failed to fetch the work orders for given projectid.",
								datWorkorderBeanList));
			}
		}

		//Added by Lalith kumar
		
		@ApiOperation(tags = "Project Module", value = "upload sow support file"
				+ "This service will be called from the front-end when dm/dl/pc to upload sow support file", httpMethod = "POST", notes = "This Service has been implemented to upload sow support file for work order"
						+ "<br>The Details will be inserted  to database table" + "<br> <b> Table : </b> </br>"
						+ "dat_sow_support_docs_upload_history table"
						+ "<br>", nickname = "upload sow support file", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = SowFileUploadInput.class),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = ProjectURIConstants.SOW_SUPPORT_UPLOAD, method = RequestMethod.POST)
		public ResponseEntity<MISResponse> uploadSowSupportFile(
				@RequestParam(value = "workorderJson", name = "workorderJson", required = true, defaultValue = "") String workorderJson,
				@RequestParam(value = "sowSupportFile", name = "sowSupportFile", required = false) MultipartFile sowSupportFile)
				throws CommonCustomException, DataAccessException, JSONException, IOException {
			boolean isUploaded = false;
			SowFileUploadInput sowFileUploadInput = new SowFileUploadInput();
			sowFileUploadInput = objectMapper.readValue(workorderJson, SowFileUploadInput.class);
			try {
				isUploaded = workOrderService.uploadSowSupportFile(sowSupportFile, sowFileUploadInput);
			} catch (Exception e) {
				throw new CommonCustomException(e.getMessage());
			}
			if (isUploaded) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "SOW SUPPORT FILE UPLOADED SUCCESSFULLY"));
			} else {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(
						HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, "FAIL TO UPLOAD SOW SUPPORT FILE"));
			}
		}
		
		//EOA by Lalith kumar
		
		//Added by Lalith kumar

		@ApiOperation(tags = "Project Module", value = "download sow support file"
				+ "This service will be called from the front-end when dm/dl/pc to download sow support file", httpMethod = "POST", notes = "This Service has been implemented to download sow support file"
						+ "<br>The Details will be fetched data from database table" + "<br> <b> Table : </b> </br>"
						+ "dat_sow_support_upload_history table"
						+ "<br>", nickname = "download sow support file", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = WorkOrderFullViewOutputBean.class),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = ProjectURIConstants.SOW_SUPPORT_DOWNLOAD, method = RequestMethod.POST)
		public ResponseEntity<MISResponse> downloadSowSupportFile(
				@Valid @RequestBody SowFileDownloadInput inputBean,HttpServletResponse response)
				throws CommonCustomException, DataAccessException, JSONException, IOException {
			boolean downloaded = false;
			/*SowFileDownloadInput sowFileUploadInput = new SowFileDownloadInput();
			sowFileUploadInput = objectMapper.readValue(workorderJson, SowFileDownloadInput.class);*/
			try {
				downloaded = workOrderService.downloadSowSupportFile(inputBean, response);
			} catch (Exception e) {
				throw new CommonCustomException(e.getMessage());
			}
			if (downloaded) {
				return ResponseEntity.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "FILE DOWNLOADED SUCCESSFULLY"));
			} else {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(
						HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, "FAIL TO DOWNLOAD FILE"));
			}
		}

		//EOA by Lalith kumar
		
		
		@RequestMapping(value = ProjectURIConstants.GET_WORK_ORDER_HISTORY, method = RequestMethod.GET)
		public @ResponseBody ResponseEntity<MISResponse> getWorkOrderHistory(
				@PathVariable Short projectId) throws CommonCustomException,
				DataAccessException {
			LOG.startUsecase("get work order history");
			ProjectHistoryOutputBean projectHistoryOutputBean = new ProjectHistoryOutputBean();
			List<ProjectHistoryOutputBean> projectHistoryOutputBeanList = new ArrayList<ProjectHistoryOutputBean>();
			try {
				projectHistoryOutputBean = workOrderService
						.getWorkOrderHistory(projectId);
			} catch (Exception e) {
				throw new CommonCustomException(e.getMessage());
			}
			projectHistoryOutputBeanList.add(projectHistoryOutputBean);
			if (!projectHistoryOutputBeanList.isEmpty()) {
				LOG.endUsecase("get workorder history");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Successfully retrieved work order history details",
								projectHistoryOutputBeanList));
			} else {
				LOG.endUsecase("get work order history");
				return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
						new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.FAILURE,
								"Error occurred while retriving workorder history details"));
			}

		}
		
		//Added by Lalith kumar
		/**
		 * @author Lalith kumar
		 * @param empId
		 * @return
		 * @throws CommonCustomException
		 * @throws DataAccessException
		 */
		
		@ApiOperation(tags = "Project Module", value = "Get all project names."
				+ "This service will be called from the front-end when dm/dl/pc wants to get all project names.", httpMethod = "POST", notes = "This Service has been implemented to Get all project names."
						+ "<br>The Details will be fetched data from database table" + "<br> <b> Table : </b> </br>"
						+ "mas_projects table"
						+ "<br>", nickname = "Get all project names.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = String.class),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
		@RequestMapping(value = ProjectURIConstants.GET_ALL_PROJECTS_BASED_ON_LOGGED_IN, method = RequestMethod.GET)
		public @ResponseBody ResponseEntity<MISResponse> getProjectNamesBasedOnEmpId(
				@PathVariable Integer empId) throws CommonCustomException,
				DataAccessException {
			List<ProjectNamesOutputBean> projectNames;
			
			try{
				projectNames =   workOrderService.getAllProjectNamesBasedOnLoggedIn(empId);
				
			}catch(Exception e)
			{
				throw new CommonCustomException(e.getMessage());
			}
			
			if (!projectNames.isEmpty()) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Successfully retrieved project names",
								 projectNames));
			} else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
						new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.FAILURE,
								"Error occurred while retriving project names"));
			}
			
			
		}
		//EOA by Lalith kumar
		
		//Added by Lalith kumar
		/**
		 * @author Lalith kumar
		 * @param empId
		 * @return
		 * @throws CommonCustomException
		 * @throws DataAccessException
		 */
		@ApiOperation(tags = "Project Module", value = "Get all Workorder numbers."
				+ "This service will be called from the front-end when dm/dl/pc wants to get all  Workorder numbers.", httpMethod = "POST", notes = "This Service has been implemented to Get all  Workorder numbers."
						+ "<br>The Details will be fetched data from database table" + "<br> <b> Table : </b> </br>"
						+ "dat_workorder table"
						+ "<br>", nickname = "Get all  Workorder numbers.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = String.class),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
		@RequestMapping(value = ProjectURIConstants.GET_ALL_WO_NUM_BASED_ON_LOGGEDIN, method = RequestMethod.GET)
		public @ResponseBody ResponseEntity<MISResponse> getAllWoNumbersBasedOnEmpId(
				@PathVariable Integer empId) throws CommonCustomException,
				DataAccessException {
			List<WoNumberOutputBean> woNumberOutputBeanList;
			
			try{
				woNumberOutputBeanList =   workOrderService.getAllWoNumbersBasedOnLoggedIn(empId);
				
			}catch(Exception e)
			{
				throw new CommonCustomException(e.getMessage());
			}
			
			if (!woNumberOutputBeanList.isEmpty()) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Successfully retrieved workorder numbers",
								woNumberOutputBeanList));
			} else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
						new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.FAILURE,
								"Error occurred while retriving workorder numbers"));
			}
			
			
		}
		//EOA by Lalith kumar
		
		
		
	// Added By Rinta
	/**
	 * Description: This service is used to get all workorder initials.
	 * 
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(tags = "Project Module", value = "Get all workorder initials."
			+ "This service will be called from the front-end to get all workorder initials."
			+ "<br>The Details will be fetched data from database table"
			+ "<br> <b> Table : </b> </br>"
			+ "1) dat_workorder_initials table - This table contains all the details related to workorder initials. <br>"
			+ "<br>", nickname = "Get all workorder initials.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "All workorder initials are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get all workorder initials.", response = MISResponse.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get all workorder initials.", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_ALL_WORK_ORDER_INITIALS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllWorkOrderInitials()
			throws CommonCustomException, DataAccessException {

		List<String> workOrderInitials;

		try {
			workOrderInitials = workOrderService.getAllWorkOrderInitials();

		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}

		if (workOrderInitials != null && !workOrderInitials.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully retrieved workorder initials.",
							workOrderInitials));
		} else {
			return ResponseEntity
					.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Error occurred while retriving workorder initials."));
		}
	}
	// EOA By Rinta
	
	
	//Added by Lalith kumar
	/**
	 * @author Lalith kumar
	 * @param empId
	 * @return
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(tags = "Project Module", value = "Get all Workorder numbers."
			+ "This service will be called from the front-end when dm/dl/pc wants to get all  Workorder numbers.", httpMethod = "POST", notes = "This Service has been implemented to Get all  Workorder numbers."
					+ "<br>The Details will be fetched data from database table" + "<br> <b> Table : </b> </br>"
					+ "dat_workorder table"
					+ "<br>", nickname = "Get all  Workorder numbers.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = String.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })

	@RequestMapping(value = ProjectURIConstants.GET_ALL_WO_NUM_BASED_ON_BUID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllWoNumbersBasedOnBuId(
			@PathVariable Short buId) throws CommonCustomException,
			DataAccessException {
		List<WoNumberOutputBean> woNumberOutputBeanList;
		
		try{
			woNumberOutputBeanList =   workOrderService.getAllWoBasedOnBuId(buId);
			
		}catch(Exception e)
		{
			throw new CommonCustomException(e.getMessage());
		}
		
		if (!woNumberOutputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully retrieved workorder numbers",
							woNumberOutputBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Error occurred while retriving workorder numbers"));
		}
		
		
	}
	//EOA by Lalith kumar
	
}
