package com.thbs.mis.project.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.project.bean.ViewHistoryInputBean;
import com.thbs.mis.project.bean.ViewHistoryOutputBean;
import com.thbs.mis.project.bean.WorkOrderOutputBean;
import com.thbs.mis.project.constants.ProjectURIConstants;
import com.thbs.mis.project.service.HistoryService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@Controller
public class HistoryController {

	@Autowired 
	private HistoryService historyService;
	
	@ApiOperation(tags = "Project Module", value = "view history"
			+ "This service will be called from the front-end when dm/dl/pc view history", httpMethod = "POST", notes = "This Service has been implemented to view history"
					+ "<br>The Details will be fetched and submit data into database table"
					+ "<br> <b> Table : </b> </br>" + "dat_workorder_audit,mas_project_audit,mas_client_audit table"
					+ "<br>", nickname = "view history", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = WorkOrderOutputBean.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })

	@RequestMapping(value = ProjectURIConstants.VIEW_HISTORY, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> viewHistory(@Valid @RequestBody ViewHistoryInputBean viewHistoryInputBean)
			throws CommonCustomException, DataAccessException {
		List<ViewHistoryOutputBean> output = new ArrayList<ViewHistoryOutputBean>();
		try {
			output = historyService.viewHistory(viewHistoryInputBean);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (!output.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "HISTORY FETCHED SUCCESSFULLY", output));
		} else if (output.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No records found", output));
		} else {
			System.out.println(output.toString());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(
					HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, "UNABLE TO FETCH HISTORY"));
		}
	}

	
}
