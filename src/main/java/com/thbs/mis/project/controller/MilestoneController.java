package com.thbs.mis.project.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.org.apache.xerces.internal.util.URI;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.project.bean.MasMilestoneStatusBean;
import com.thbs.mis.project.bean.MilestoneBean;
import com.thbs.mis.project.bean.MilestoneInputBean;
import com.thbs.mis.project.bean.MilestoneOutputBean;
import com.thbs.mis.project.bean.SearchMilestoneBean;
import com.thbs.mis.project.bean.SearchMilestoneOutputBean;
import com.thbs.mis.project.bean.TriggerInput;
import com.thbs.mis.project.bean.WorkOrderFullViewOutputBean;
import com.thbs.mis.project.constants.ProjectURIConstants;
import com.thbs.mis.project.service.MilestoneService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@Api(value = "milestone Operations", description = "milestone Operations", basePath = "http://localhost:8080/", consumes = "Rest Service", position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "milestone Services")
@Controller
public class MilestoneController {
	
	@Autowired
	MilestoneService milestoneService;
	
	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory.getLog(MilestoneController.class);
	
	@RequestMapping(value = ProjectURIConstants.CREATE_MILESTONE, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> createMilestoneService(@Valid @RequestBody MilestoneInputBean bean)
			throws DataAccessException, CommonCustomException {
		List<MilestoneOutputBean> lstMilestoneOutputBean=new ArrayList<MilestoneOutputBean>();
		MilestoneOutputBean milestoneOutputBean = null;
		try{
			milestoneOutputBean = milestoneService.createMilestoneService(bean);
		}catch(Exception e)
		{
			throw new CommonCustomException(e.getMessage());
		}
		if (milestoneOutputBean!=null) {
			lstMilestoneOutputBean.add(milestoneOutputBean);
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Milestone created successfully.",lstMilestoneOutputBean));
		} else {

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					MISConstants.FAILURE, "Fail to create mile stone."));
		}

	}
	
	@RequestMapping(value = ProjectURIConstants.CREATE_MILESTONE, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> updateMilestoneService(@RequestBody MilestoneInputBean bean)
			throws CommonCustomException, DataAccessException {
		List<MilestoneOutputBean> lstMilestoneOutputBean=new ArrayList<MilestoneOutputBean>();
		MilestoneOutputBean milestoneOutputBean = null;
		try{
			milestoneOutputBean = milestoneService.updateMilestoneService(bean);
		}catch(Exception e)
		{
			throw new CommonCustomException(e.getMessage());
		}
		if (milestoneOutputBean!=null) {
			lstMilestoneOutputBean.add(milestoneOutputBean);
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Milestone updated successfully.",lstMilestoneOutputBean));
		} else {

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					MISConstants.FAILURE, "Fail to update mile stone."));
		}

	}

	@RequestMapping(value = ProjectURIConstants.SEARCH_MILESTONE, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> searchMilestoneService(@RequestBody SearchMilestoneBean bean)
			throws BusinessException, ParseException, CommonCustomException {
		Page<SearchMilestoneOutputBean> milestoneBO = null;
		try{
		milestoneBO = milestoneService.searchMilestoneService(bean);
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			throw new CommonCustomException("Fail to fetch mile stone details.");
		}

		if (!milestoneBO.getContent().isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Fetched milestone details successfully.",milestoneBO));
		} else {

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					MISConstants.FAILURE, "No data found for given status name and status id.",milestoneBO));
		}

	}
	
	@RequestMapping(value = ProjectURIConstants.MILESTONE_DETAILS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getDomainDetailsByBuUnitId(@PathVariable("milestoneId") int milestoneId)
			throws BusinessException, DataAccessException, CommonCustomException {
		List<MilestoneOutputBean> milestoneBeanList = null;
		try
		{
			milestoneBeanList = milestoneService.getAllMilestoneDetailsByBuId(milestoneId);
		}catch(Exception e)
		{
			throw new CommonCustomException("Failed to fetch the domain Details");
		}

		if (!milestoneBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "milestone details fetched successfully.", milestoneBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					MISConstants.FAILURE, "Failed to fetch the milestone details.", milestoneBeanList));
		}

	}

	// Added by Rinta Mariam Jose

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getMileStoneStatus> : Fetch All MileStone
	 *           Status.
	 * @param
	 * @return List<MasMilestoneStatusBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get MileStone Status. This service will be called when user wants to view list of MileStone Status in UI"
			+ " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch all present MileStone Status."
			+ "<br>The Response will be the list of all MileStone Status.",

	nickname = "Get all MileStone Status", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MasMilestoneStatusBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MasMilestoneStatusBean.class),
			@ApiResponse(code = 201, message = "All MileStone Status Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All MileStone Status", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All MileStone Status", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = ProjectURIConstants.GET_MILESTONE_STATUS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getMileStoneStatus(@PathVariable("type") int type )
			throws BusinessException, DataAccessException, CommonCustomException {
		List<MasMilestoneStatusBean> masMilestoneStatusBeanList = null;
		
		try
		{
			masMilestoneStatusBeanList = milestoneService.getMilestoneStaus(type);
		}catch(Exception e)
		{
			throw new CommonCustomException("Failed to fetch the milestone details: "+e.getMessage());
		}

		if (!masMilestoneStatusBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "milestone details fetched successfully.", masMilestoneStatusBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					MISConstants.SUCCESS, "Failed to fetch the milestone details.", masMilestoneStatusBeanList));
		}
	}

	// EOA by Rinta Mariam Jose
	
	
	@ApiOperation(tags = "Project Module", value = "get ob10 value"
			+ "This service will be called from the front-end when dm/dl/pc/finance view ob10 value", httpMethod = "GET", notes = "This Service has been implemented  view ob10 value"
					+ "<br>The Details will be fetched data from database table" + "<br> <b> Table : </b> </br>"
					+ "dat_milestone table"
					+ "<br>", nickname = " get ob10 value", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = WorkOrderFullViewOutputBean.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.OB10_VALUE, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getOB10value(@PathVariable("milestoneId") Integer milestoneId) throws CommonCustomException, DataAccessException {
		BigDecimal ob10value;
		try {
			ob10value = milestoneService.getOB10value(milestoneId);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		List<BigDecimal> output = new ArrayList<BigDecimal>();
		output.add(ob10value);
		if (ob10value != null) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "OB10 VALUE FETCHED SUCCESSFULLY", output));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(
					HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, "FAIL TO GET OB10 VALUE"));
		}
	}
	
	
	@ApiOperation(tags = "Project Module", value = "trigger milestone"
			+ "This service will be called from the front-end when dm/dl/pc/finance trigger the milestone", httpMethod = "POST", notes = "This Service has been implemented  to trigger the milestone"
					+ "<br>The Details will be fetched data from database table" + "<br> <b> Table : </b> </br>"
					+ "dat_milestone  & proj_dat_milestone_trigger_details table"
					+ "<br>", nickname = "trigger milestone", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = WorkOrderFullViewOutputBean.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.TRIGGER_MILESTONE, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> triggermilestone(@Valid @RequestBody TriggerInput triggerInput) throws CommonCustomException, DataAccessException {
		BigDecimal ob10value;
		try {
			ob10value = milestoneService.triggermilestone(triggerInput);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		List<BigDecimal> output = new ArrayList<BigDecimal>();
		output.add(ob10value);
		if (ob10value != null) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "SUCCESSFULLY TRIGGERED THE MILESTONE", output));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(
					HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, "FAIL TO TRIGGER"));
		}
	}

			@ApiOperation(tags = "Project Module", value = "get milestone based on ComponentId"
			+ "This service will be called from the front-end to view milestone based on ComponentId", httpMethod = "GET", notes = "This Service has been implemented to view milestone based on ComponentId"
					+ "<br>The Details will be fetched data from database table" + "<br> <b> Table : </b> </br>"
					+ "dat_milestone table"
					+ "<br>", nickname = " get milestone based on ComponentId", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MilestoneBean.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_ALL_MILESTONE_COMPONENT_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllMilestoneByComponentId(@PathVariable("componentId") Integer componentId)
			throws DataAccessException,
			CommonCustomException {
		List<MilestoneBean> milestoneBeanList = null;

		try {
			milestoneBeanList = milestoneService
					.getAllMilestoneByComponentId(componentId);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}

		if (!milestoneBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully fetched the Milestone for given ComponentId",
							milestoneBeanList));
		} else {
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Failed to fetch the Milestone for given ComponentId."));

		}
	}
}
