/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  ProjectController.jav                             */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  05-March-2018                                     */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*   Date         Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 05-March-2018  THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.project.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.project.bean.CreateProjectBean;
import com.thbs.mis.project.bean.GetProjectsOutputBean;
import com.thbs.mis.project.bean.MasProjectOutputBean;
import com.thbs.mis.project.bean.ProjMasCompStatusBean;
import com.thbs.mis.project.bean.ProjMasCompTypeBean;
import com.thbs.mis.project.bean.ProjMasProjectClassificationBean;
import com.thbs.mis.project.bean.ProjMasProjectStatusBean;
import com.thbs.mis.project.bean.ProjectHistoryOutputBean;
import com.thbs.mis.project.bean.ProjectNamesOutputBean;
import com.thbs.mis.project.bean.ProjectOutputBean;
import com.thbs.mis.project.bean.ProjectUserAccessBean;
import com.thbs.mis.project.bean.ViewProjectInputBean;
import com.thbs.mis.project.bean.ViewProjectOutputBean;
import com.thbs.mis.project.bean.viewParticularProjectOutputBean;
import com.thbs.mis.project.constants.ProjectURIConstants;
import com.thbs.mis.project.service.ProjectService;

@Api(value = "Project Operations", description = "Project Operations", basePath = "http://localhost:8080/", consumes = "Rest Service", position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "Project Services")
@Controller
public class ProjectController {

	@Autowired
	private ProjectService projectService;

	private static final AppLog LOG = LogFactory
			.getLog(ProjectController.class);

	// Added by pratibha
	@RequestMapping(value = ProjectURIConstants.CREATE_PROJECT, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> createProject(
			@RequestBody @Valid CreateProjectBean createProjectBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Create project");
		ProjectOutputBean createProjectOutputBean = new ProjectOutputBean();
		List<ProjectOutputBean> createProjectOutputBeanList = new ArrayList<ProjectOutputBean>();
		try {
			createProjectOutputBean = projectService
					.createProject(createProjectBean);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		createProjectOutputBeanList.add(createProjectOutputBean);
		if (createProjectOutputBean != null) {
			LOG.endUsecase("Create project");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Project created successfully",
							createProjectOutputBeanList));
		} else {
			LOG.endUsecase("Create project");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Error occurred while creating project."));
		}
	}

	@RequestMapping(value = ProjectURIConstants.UPDATE_PROJECT, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> updateProject(
			@RequestBody @Valid CreateProjectBean updateProjectBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("update project");
		ProjectOutputBean updateProjectOutputBean = new ProjectOutputBean();
		List<ProjectOutputBean> updateProjectOutputBeanList = new ArrayList<ProjectOutputBean>();
		try {
			updateProjectOutputBean = projectService
					.updateProject(updateProjectBean);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		updateProjectOutputBeanList.add(updateProjectOutputBean);
		if (!updateProjectOutputBeanList.isEmpty()) {
			LOG.endUsecase("update project");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Project updated successfully",
							updateProjectOutputBeanList));
		} else {
			LOG.endUsecase("update project");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Error occurred while updating project."));
		}
	}

	@RequestMapping(value = ProjectURIConstants.VIEW_PROJECTS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewProject(
			@RequestBody @Valid ViewProjectInputBean viewProjectInputBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("View project");
		Page<ViewProjectOutputBean> viewProjectOutputBean = null;
		try {
			viewProjectOutputBean = projectService.searchProject(viewProjectInputBean);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (!viewProjectOutputBean.getContent().isEmpty()) {
			LOG.endUsecase("View project");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Successfully retrieved project details", viewProjectOutputBean));
		} else if (viewProjectOutputBean.getContent().isEmpty()) {
			LOG.endUsecase("View project");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(
					HttpStatus.OK.value(), MISConstants.FAILURE, "No records found.", viewProjectOutputBean));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new MISResponse(
					HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, "Fail to search project details"));
		}
	}

	@RequestMapping(value = ProjectURIConstants.GET_PROJECTS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getProjects(
			@PathVariable("clientId") Short clientId)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("get project details");
		List<GetProjectsOutputBean> getProjectsOutputBeanList = new ArrayList<GetProjectsOutputBean>();
		try {
			getProjectsOutputBeanList = projectService.getProjects(clientId);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (!getProjectsOutputBeanList.isEmpty()) {
			LOG.endUsecase("get project details");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully retrieved project details",
							getProjectsOutputBeanList));
		} else {
			LOG.endUsecase("get project details");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Error occurred while retriving project details"));
		}

	}

	@RequestMapping(value = ProjectURIConstants.VIEW_PERTICULAR_PROJECT, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> viewParticularProject(
			@PathVariable Integer projectId) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("view perticular project details");
		viewParticularProjectOutputBean viewPerticularProjectOutputBean = new viewParticularProjectOutputBean();
		List<viewParticularProjectOutputBean> viewParticularProjectOutputBeanList = new ArrayList<viewParticularProjectOutputBean>();
		try {
			viewPerticularProjectOutputBean = projectService
					.viewParticularProject(projectId);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		viewParticularProjectOutputBeanList
				.add(viewPerticularProjectOutputBean);
		if (!viewParticularProjectOutputBeanList.isEmpty()) {
			LOG.endUsecase("get project details");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully retrieved project details",
							viewParticularProjectOutputBeanList));
		} else {
			LOG.endUsecase("view perticular project details");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Error occurred while retriving project details"));
		}

	}

	@RequestMapping(value = ProjectURIConstants.GET_PROJECT_HISTORY, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getProjectHistory(
			@PathVariable Short clientId) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("get project history");
		ProjectHistoryOutputBean projectHistoryOutputBean = new ProjectHistoryOutputBean();
		List<ProjectHistoryOutputBean> projectHistoryOutputBeanList = new ArrayList<ProjectHistoryOutputBean>();
		try {
			projectHistoryOutputBean = projectService
					.getProjectHistory(clientId);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		projectHistoryOutputBeanList.add(projectHistoryOutputBean);
		if (!projectHistoryOutputBeanList.isEmpty()) {
			LOG.endUsecase("get project history");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully retrieved project history details",
							projectHistoryOutputBeanList));
		} else {
			LOG.endUsecase("get project history");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Error occurred while retriving project history details"));
		}

	}

	// EOA by pratibha
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllProjectStatus> : Fetch All ProjectStatus.
	 * @param
	 * @return List<ProjMasProjectStatusBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all ProjectStatus. This service will be called when user wants to view ProjectStatus in UI"
			+ " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch all present ProjectStatus."
			+ "<br>The Response will be the list of ProjectStatus.",

	nickname = "Get All ProjectStatus", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ProjMasProjectStatusBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = ProjMasProjectStatusBean.class),
			@ApiResponse(code = 201, message = "All Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "ProjectStatus Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "ProjectStatus Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_ALL_PROJECTSTATUS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAllProjectDetails()
			throws CommonCustomException, DataAccessException {
		List<ProjMasProjectStatusBean> projectStatus = new ArrayList<ProjMasProjectStatusBean>();
		try {
			projectStatus = projectService.getAllProjectStatus();

		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch the details.", e);
		}
		if (!projectStatus.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"successfully fetched the details", projectStatus));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No records founds."));
		}
	}

	// Added by Lalith kumar
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllComponentTypes> : Fetch All ComponentTypes.
	 * @param
	 * @return List<ProjMasCompTypeBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all ComponentTypes. This service will be called when user wants to view list of Verticals in UI"
			+ " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch all present ComponentTypes."
			+ "<br>The Response will be the list of ComponentTypes.",

	nickname = "Get All ComponentTypes", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ProjMasCompTypeBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = ProjMasCompTypeBean.class),
			@ApiResponse(code = 201, message = "All ComponentTypes are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All ComponentTypes", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All ComponentTypes", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_ALL_COMPONENT_TYPE, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllComponentTypes()
			throws BusinessException, DataAccessException {
		List<ProjMasCompTypeBean> projMasCompTypeBeanList = new ArrayList<ProjMasCompTypeBean>();
		try {
			projMasCompTypeBeanList = projectService.getAllCompType();
		} catch (DataAccessException e) {
			throw new BusinessException(
					"Failed to retrieve all ComponentTypes", e);
		}

		if (!projMasCompTypeBeanList.isEmpty()) {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(
							HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"All ComponentTypes have been retrieved successfully",
							projMasCompTypeBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No ComponentType found",
							projMasCompTypeBeanList));
		}

	}

	// EOA By Lalith kumar

	// Added by Lalith kumar
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllComponentTypes> : Fetch All ComponentTypes.
	 * @param
	 * @return List<ProjMasCompTypeBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get all Components Status. This service will be called when user wants to view list of Verticals in UI"
			+ " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch all present Components Status."
			+ "<br>The Response will be the list of all Components Status.",

	nickname = "Get All Components Status", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ProjMasCompStatusBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = ProjMasCompStatusBean.class),
			@ApiResponse(code = 201, message = "All Components Status are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All ComponentTypes", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All ComponentTypes", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_ALL_COMPONENT_STATUS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllComponentStatus()
			throws BusinessException, DataAccessException {
		List<ProjMasCompStatusBean> projMasCompStatusBeanList = new ArrayList<ProjMasCompStatusBean>();
		try {
			projMasCompStatusBeanList = projectService.getAllCompStatus();
		} catch (DataAccessException e) {
			throw new BusinessException(
					"Failed to retrieve all Components Status", e);
		}

		if (!projMasCompStatusBeanList.isEmpty()) {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(
							HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"All Components Status have been retrieved successfully",
							projMasCompStatusBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No Component found",
							projMasCompStatusBeanList));
		}

	}

	// EOA By Lalith kumar

	
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getAllProjMasProjectClassifications> : Fetch All
	 *           ProjectClassifications.
	 * @param
	 * @return List<ProjMasProjectClassificationBean>
	 * @throws DataAccessException
	 * @throws CommonCustomException 
	 */
	@ApiOperation(value = "Get all ProjectClassifications. This service will be called when user wants to view ProjectClassifications in UI"
			+ " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch all present ProjectClassifications."
			+ "<br>The Response will be the list of ProjectClassifications.",

	nickname = "Get All ProjectClassifications", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ProjMasProjectClassificationBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = ProjMasProjectClassificationBean.class),
			@ApiResponse(code = 201, message = "All Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "ProjectClassification Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "ProjectClassifications Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_ALL_PROJECT_CLASSIFICATION, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllProjectclassication()
			throws DataAccessException, CommonCustomException {
		List<ProjMasProjectClassificationBean> projectclabean = new ArrayList<ProjMasProjectClassificationBean>();
		try {
			projectclabean = projectService.getAllProjectclasications();

		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch the details."
					+ e.getMessage());
		}
		if (!projectclabean.isEmpty()) {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"successfully fetched the details", projectclabean));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No records founds."));
		}
	}

	/*@RequestMapping(value = ProjectURIConstants.CREATE_CLIENT_BUDGET, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> createCleintBudget(
			@RequestBody ClientBudgetInputBean bean) throws BusinessException,
			DataAccessException, ParseException,
			MethodArgumentNotValidException, CommonCustomException {
		LOG.startUsecase("Entering into createCleintBudget method in controller");

		Boolean flag = false;
		@SuppressWarnings("unused")
		ProjDatClientBudgetAtResourceLevelBO boBean = new ProjDatClientBudgetAtResourceLevelBO();
		ClientBudgetInputBean inputBean = new ClientBudgetInputBean();
		List<ClientBudgetInputBean> listBean = new ArrayList<ClientBudgetInputBean>();
		try {
			System.out.println("try block");
			flag = projectService.createClientBudget(bean);
			listBean.add(inputBean);

		} catch (Exception e) {

			throw new BusinessException("Exception occured", e);
		}
		if (flag) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"budget created successfully."));
		} else {
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Fail to create budget order."));
		}

	}
*/
	// Added by Rinta Mariam Jose
		
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getProjectUserAccess : Fetch project user access
	 *           list>
	 * @param
	 * @return List<ProjectUserAccessBean>
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get project user access list. This service will be called when user wants to view project user access list in UI"
			+ " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch project user access list."
			+ "<br>The Response will be the project user access list.",

	nickname = "Project User Access Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ProjectUserAccessBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = ProjectUserAccessBean.class),
			@ApiResponse(code = 201, message = "All project coordinator details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Project Coordinator Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Project User Access Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_PROJECT_USER_ACCESS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getProjectUserAccessList()
			throws BusinessException, DataAccessException,
			CommonCustomException {
		List<ProjectUserAccessBean> projectUserAccessBeanList = null;

		try {
			projectUserAccessBeanList = projectService
					.getProjectUserAccessList();
		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch the details: "
					+ e.getMessage());
		}

		if (!projectUserAccessBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully fetched the details",
							projectUserAccessBeanList));
		} else {
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Failed to fetch the details.",
							projectUserAccessBeanList));
		}
	}
	// EOA by Rinta Mariam Jose
	
	
	//Added By Rinta Mariam Jose
	@RequestMapping(value = ProjectURIConstants.VIEW_PROJECT_DETAILS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> viewProjectDetails(
			@PathVariable Integer projectId) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("view project details");
		List<MasProjectOutputBean> masProjectOutputBeanList;

		try {
			masProjectOutputBeanList = projectService
					.viewProjectDetails(projectId);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		
		if (!masProjectOutputBeanList.isEmpty()) {
			LOG.endUsecase("get project details");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully retrieved project details",
							masProjectOutputBeanList));
		} else {
			LOG.endUsecase("view perticular project details");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Error occurred while retriving project details"));
		}

	}
	// EOA by Rinta Mariam Jose


		
	//Added by Lalith kumar
			/**
			 * @author Lalith kumar
			 * @param empId
			 * @return
			 * @throws CommonCustomException
			 * @throws DataAccessException
			 */
			
			@ApiOperation(tags = "Project Module", value = "Get all project names."
					+ "This service will be called from the front-end when dm/dl/pc wants to get all project names.", httpMethod = "POST", notes = "This Service has been implemented to Get all project names."
							+ "<br>The Details will be fetched data from database table" + "<br> <b> Table : </b> </br>"
							+ "mas_projects table"
							+ "<br>", nickname = "Get all project names.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
			@ApiResponses(value = {
					@ApiResponse(code = 200, message = "Request Executed Successfully", response = String.class),
					@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
					@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		
			@RequestMapping(value = ProjectURIConstants.GET_ALL_PROJECTS_BASED_ON_BUID, method = RequestMethod.GET)
			public @ResponseBody ResponseEntity<MISResponse> getProjectNamesBasedOnEmpId(
					@PathVariable Short buId) throws CommonCustomException,
					DataAccessException {
				List<ProjectNamesOutputBean> projectNames;
				
				try{
					projectNames =   projectService.getAllProjectNamesBasedOnBuId(buId);
					
				}catch(Exception e)
				{
					throw new CommonCustomException(e.getMessage());
				}
				
				if (!projectNames.isEmpty()) {
					return ResponseEntity.status(HttpStatus.OK.value()).body(
							new MISResponse(HttpStatus.OK.value(),
									MISConstants.SUCCESS,
									"Successfully retrieved project names",
									 projectNames));
				} else {
					return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
							new MISResponse(HttpStatus.NOT_FOUND.value(),
									MISConstants.FAILURE,
									"Error occurred while retriving project names"));
				}
				
				
			}
			//EOA by Lalith kumar
			
	
	

	
	
}
