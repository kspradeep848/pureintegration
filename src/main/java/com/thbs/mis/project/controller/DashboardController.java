package com.thbs.mis.project.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.project.bean.GlobalRegionsAndProjectsBean;
import com.thbs.mis.project.bean.StepperInputBean;
import com.thbs.mis.project.bean.StepperOutputBean;
import com.thbs.mis.project.bean.ProjectOverviewBean;
import com.thbs.mis.project.bean.ProjectStatusBean;
import com.thbs.mis.project.constants.ProjectURIConstants;
import com.thbs.mis.project.service.DashboardService;

@Controller
public class DashboardController {

	private static final AppLog LOG = LogFactory
			.getLog(DashboardController.class);

	@Autowired
	private DashboardService dashboardService;

	// Added by Rinta Mariam Jose
	/**
	 * Description : This service is used to get the number of projects in a
	 * country associated to the employee
	 * 
	 * @param empId
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(tags = "Project Module", value = "Get the number of projects in a country associated to the employee."
			+ "This service will be called from the front-end when dm/dl/pc/bu_head/am wants to get the number of projects in a country associated to the employee.", httpMethod = "GET", notes = "This Service has been implemented to Get the number of projects in a country associated to an employee."
			+ "<br>The Details will be fetched data from database table"
			+ "<br> <b> Table : </b> </br>"
			+ "1) mas_projects table - This table contains all the details related to projects. <br>"
			+ "2) mas_client table - This table contains all the details related to clients. <br>"
			+ "<br> Inputs : " + "empId : empId is in format Integer." + "<br>", nickname = "Get the number of projects in a country associated to the employee.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Number of Projects based on countries are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get the number of projects in a country associated to the employee", response = MISResponse.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get the number of projects in a country associated to the employee", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_ALL_PROJECTS_BASED_ON_COUNTRIES, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getProjectsBasedOnGlobalRegions(
			@PathVariable Integer empId) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("Entering into getProjectsBasedOnGlobalRegions() controller.");

		List<GlobalRegionsAndProjectsBean> globalRegionsAndProjectsBeanList;

		try {
			globalRegionsAndProjectsBeanList = dashboardService
					.getProjectsBasedOnGlobalRegions(empId);

		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}

		if (!globalRegionsAndProjectsBeanList.isEmpty()) {
			LOG.endUsecase("Exiting getProjectsBasedOnGlobalRegions() controller.");
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(
							HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully retrieved number of projects in a country associated to the employee",
							globalRegionsAndProjectsBeanList));
		} else {
			LOG.endUsecase("Exiting getProjectsBasedOnGlobalRegions() controller.");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Error occurred while retriving details."));
		}
	}

	// EOA by Rinta Mariam Jose

	// Added by Rinta Mariam Jose
	/**
	 * Description : This service is used to get project overview details.
	 * 
	 * @param empId
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(tags = "Project Module", value = "Get project overview details."
			+ "This service will be called from the front-end when dm/dl/pc/bu_head/am wants to get project overview details."
			+ "<br>The Details will be fetched data from database table"
			+ "<br> <b> Table : </b> </br>"
			+ "1) mas_projects table - This table contains all the details related to projects. <br>"
			+ "2) dat_workorder table - This table contains all the details related to workorders. <br>"
			+ "3) dat_component table - This table contains all the details related to components. <br>"
			+ "4) dat_milestone table - This table contains all the details related to milestones. <br>"
			+ "<br> Inputs : " + "empId : empId is in format Integer." + "<br>", nickname = "Get project overview details.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Project Overview Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get project overview details.", response = MISResponse.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get project overview details.", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_PROJECT_OVERVIEW, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getProjectsOverview(
			@PathVariable Integer empId) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("Entering into getProjectsOverview() controller.");

		List<ProjectOverviewBean> projectOverviewBeanList;
		try {
			projectOverviewBeanList = dashboardService
					.getProjectsOverview(empId);

		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}

		if (!projectOverviewBeanList.isEmpty()) {
			LOG.endUsecase("Exiting getProjectsOverview() controller.");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Project overview fetched successfully.",
							projectOverviewBeanList));
		} else {
			LOG.endUsecase("Exiting getProjectsOverview() controller.");
			return ResponseEntity
					.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Error occurred while retriving project overview details."));
		}
	}

	/**
	 * Description : This service is used to get project status details.
	 * 
	 * @param empId
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(tags = "Project Module", value = "Get project status details."
			+ "This service will be called from the front-end when dm/dl/pc/bu_head/am wants to get project status details."
			+ "<br>The Details will be fetched data from database table"
			+ "<br> <b> Table : </b> </br>"
			+ "1) mas_projects table - This table contains all the details related to projects. <br>"
			+ "2) proj_mas_project_status table - This table contains all the details related to project status. <br>"
			+ "<br> Inputs : " + "empId : empId is in format Integer." + "<br>", nickname = "Get project status details.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Project Status Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get project status details.", response = MISResponse.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get project status details.", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_PROJECT_STATUS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getProjectStatus(
			@PathVariable Integer empId) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("Entering into getProjectStatus() controller.");

		List<ProjectStatusBean> projectStatusBeanList;
		try {
			projectStatusBeanList = dashboardService.getProjectStatus(empId);

		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}

		if (!projectStatusBeanList.isEmpty()) {
			LOG.endUsecase("Exiting getProjectStatus() controller.");
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(
							HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Project details with status fetched successfully.",
							projectStatusBeanList));
		} else {
			LOG.endUsecase("Exiting getProjectStatus() controller.");
			return ResponseEntity
					.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Error occurred while retriving project status details."));
		}
	}
	// EOA by Rinta Mariam Jose
	
	@ApiOperation(tags = "Project Module", value = "Get Stepper status."
			+ "This service will be called from the front-end when dm/dl/pc wants to get Stepper status.", httpMethod = "POST", notes = "This Service has been implemented to Get Stepper status."
					+ "<br>The Details will be fetched data from database table" + "<br> <b> Table : </b> </br>"
					+ "mas_projects,dat_workorder,dat_componet tables"
					+ "<br>", nickname = "Get Stepper status.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = StepperOutputBean.class),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })

	@RequestMapping(value = ProjectURIConstants.GET_STEPPER_STATUS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> getStepperStatus(
			@Valid	@RequestBody StepperInputBean stepperInputBean) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("Entering Stepper Controller");
		List<StepperOutputBean> stepperOutputBeanList = null;		
		stepperOutputBeanList = dashboardService.getStepperStatus(stepperInputBean);
		if (!stepperOutputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"stepper status retrived successfully",
							stepperOutputBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Error occurred while getting stepper status"));
		}
		
	}
		
}
