package com.thbs.mis.project.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.math.BigDecimal;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.project.bean.BudgetActionInputBean;
import com.thbs.mis.project.bean.BudgetDetailsOutputBean;
import com.thbs.mis.project.bean.BudgetSubmitInputBean;
import com.thbs.mis.project.bean.StandardRateInputBean;
import com.thbs.mis.project.bean.ThbsBudgetCreateInputBean;
import com.thbs.mis.project.bean.ThbsBudgetCreationOutputBean;
import com.thbs.mis.project.bean.ThbsBudgetForAdditionalCostBean;
import com.thbs.mis.project.bean.ThbsBudgetLocationOutputBean;
import com.thbs.mis.project.bean.ThbsBudgetOutputBean;
import com.thbs.mis.project.constants.ProjectURIConstants;
import com.thbs.mis.project.service.ThbsBudgetService;


@Controller
public class ThbsBudgetController
{
	@Autowired
	private ThbsBudgetService thbsBudgetService;
	
	
	
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description Create Thbs Budget> : Create Thbs Budget.
	 * @param
	 * @return List<ThbsBudgetCreationOutputBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Create Thbs Budget. This service will be called when user wants to create Thbs Budget"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to create Thbs Budget."
			+ "<br>The Response will be ThbsBudgetCreationOutputBean.",

	nickname = "Create Thbs Budget.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ThbsBudgetCreationOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BudgetDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "Thbs Budget created successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
@RequestMapping(value = ProjectURIConstants.THBS_BUDGET, method = RequestMethod.POST)
public ResponseEntity<MISResponse> createThbsBudget(
		 @Valid @RequestBody ThbsBudgetCreateInputBean thbsBudgetCreateInputBean)
		throws BusinessException, DataAccessException, CommonCustomException {
		ThbsBudgetCreationOutputBean thbsBudgetCreationOutputBean;
		List<ThbsBudgetCreationOutputBean> thbsBudgetCreationOutputBeanList
		= new ArrayList<ThbsBudgetCreationOutputBean>();
	try {
	  thbsBudgetCreationOutputBean=thbsBudgetService.createThbsBudget(thbsBudgetCreateInputBean);
				
	} catch (DataAccessException e) {
		throw new CommonCustomException(e.getMessage());
	}
	
	thbsBudgetCreationOutputBeanList.add(thbsBudgetCreationOutputBean);
	
	if (thbsBudgetCreationOutputBean!=null) {
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS,
						"Budget details fetched successfully.",
						thbsBudgetCreationOutputBeanList));
	} else {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
				new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						MISConstants.FAILURE, "No record found."));
	}
}
	
	
	
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description Create Thbs Budget> : Create Thbs Budget.
	 * @param
	 * @return List<ThbsBudgetCreationOutputBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Create Thbs Budget. This service will be called when user wants to create Thbs Budget"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to create Thbs Budget."
			+ "<br>The Response will be ThbsBudgetCreationOutputBean.",

	nickname = "Create Thbs Budget.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ThbsBudgetCreationOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BudgetDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "Thbs Budget created successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
@RequestMapping(value = ProjectURIConstants.THBS_BUDGET, method = RequestMethod.PUT)
public ResponseEntity<MISResponse> updateThbsBudget(
		@Valid @RequestBody ThbsBudgetCreateInputBean thbsBudgetCreateInputBean)
		throws BusinessException, DataAccessException, CommonCustomException {
		ThbsBudgetCreationOutputBean thbsBudgetCreationOutputBean;
		List<ThbsBudgetCreationOutputBean> thbsBudgetCreationOutputBeanList
		= new ArrayList<ThbsBudgetCreationOutputBean>();
	try {
	  thbsBudgetCreationOutputBean=thbsBudgetService.updateThbsBudget(thbsBudgetCreateInputBean);
				
	} catch (DataAccessException e) {
		throw new CommonCustomException(e.getMessage());
	}
	
	thbsBudgetCreationOutputBeanList.add(thbsBudgetCreationOutputBean);
	
	if (thbsBudgetCreationOutputBean!=null) {
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS,
						"Budget details fetched successfully.",
						thbsBudgetCreationOutputBeanList));
	} else {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
				new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						MISConstants.FAILURE, "No record found."));
	}
}
	

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description Create Thbs Budget> : Create Thbs Budget.
	 * @param
	 * @return List<ThbsBudgetCreationOutputBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Create Thbs Budget. This service will be called when user wants to create Thbs Budget"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to create Thbs Budget."
			+ "<br>The Response will be ThbsBudgetCreationOutputBean.",

	nickname = "Create Thbs Budget.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ThbsBudgetCreationOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BudgetDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "Thbs Budget created successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
@RequestMapping(value = ProjectURIConstants.GET_ALL_THBS_LOCATION_BY_CLIENT_BUDGET_ID, method = RequestMethod.GET)
public ResponseEntity<MISResponse> getThbsBudgetLocation(
	@PathVariable Integer clientBudgetId)
		throws BusinessException, DataAccessException, CommonCustomException {
		
		List<ThbsBudgetLocationOutputBean> thbsBudgetLocationOutputBeanList
		= new ArrayList<ThbsBudgetLocationOutputBean>();
		

//		List<MasThbsBudgetLocationBO> thbsBudgetLocationOutputBeanList
//		= new ArrayList<MasThbsBudgetLocationBO>();
		
	try {
		thbsBudgetLocationOutputBeanList=thbsBudgetService.getThbsBudgetLocationBasedOnClientBudgetId(clientBudgetId);
				
	} catch (DataAccessException e) {
		throw new CommonCustomException(e.getMessage());
	}
	
	if (!thbsBudgetLocationOutputBeanList.isEmpty()) {
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS,
						"Budget details fetched successfully.",
						thbsBudgetLocationOutputBeanList));
	} else {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
				new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						MISConstants.FAILURE, "No record found."));
	}
}
	
	
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description Create Thbs Budget> : Create Thbs Budget.
	 * @param
	 * @return List<ThbsBudgetCreationOutputBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Create Thbs Budget. This service will be called when user wants to create Thbs Budget"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to create Thbs Budget."
			+ "<br>The Response will be ThbsBudgetCreationOutputBean.",

	nickname = "Create Thbs Budget.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ThbsBudgetCreationOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BudgetDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "Thbs Budget created successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
@RequestMapping(value = ProjectURIConstants.GET_THBS_BUDGET_DETAILS, method = RequestMethod.GET)
public ResponseEntity<MISResponse> getThbsBudgetDetails(
	@PathVariable Integer budgetId,@PathVariable Integer empId)
		throws BusinessException, DataAccessException, CommonCustomException {
		
		List<ThbsBudgetOutputBean> thbsBudgetOutputBeanList=
		new ArrayList<ThbsBudgetOutputBean>();
		
		ThbsBudgetOutputBean thbsBudgetOutputBean;

	try {
		thbsBudgetOutputBean = thbsBudgetService.getThbsBudget(budgetId, empId);
				
	} catch (DataAccessException e) {
		throw new CommonCustomException(e.getMessage());
	}
	
	thbsBudgetOutputBeanList.add(thbsBudgetOutputBean);
	
	if (!thbsBudgetOutputBeanList.isEmpty()) {
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS,
						"Budget details fetched successfully.",
						thbsBudgetOutputBeanList));
	} else {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
				new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						MISConstants.FAILURE, "No record found."));
	}
}
	
	// Added By Rinta
	/**
	 * <Description Create Thbs Budget For Additional Cost> : Create Thbs Budget
	 * For Additional Cost.
	 * 
	 * @param thbsBudgetForAdditionalCostBean
	 * @return ThbsBudgetCreationOutputBean
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */

	@ApiOperation(value = "Create Thbs Budget For Additional Cost. This service will be called when user wants to create Thbs Budget For Additional Cost."
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to create Thbs Budget For Additional Cost."
			+ "<br>The Response will be ThbsBudgetCreationOutputBean.",

	nickname = "Create Thbs Budget For Additional Cost.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ThbsBudgetCreationOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BudgetDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "Thbs Budget for Additional Cost created successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget For Additional Cost", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget For Additional Cost", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.THBS_BUDGET_ADDITIONAL_AMT, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> createThbsBudgetForAdditionalCost(
			@Valid @RequestBody ThbsBudgetForAdditionalCostBean thbsBudgetForAdditionalCostBean)
			throws BusinessException, DataAccessException,
			CommonCustomException {
		List<ThbsBudgetCreationOutputBean> thbsBudgetCreationOutputBeanList = new ArrayList<ThbsBudgetCreationOutputBean>();
		try {
			thbsBudgetCreationOutputBeanList = thbsBudgetService
					.createThbsBudgetForAdditionalCost(thbsBudgetForAdditionalCostBean);
		} catch (DataAccessException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}

		if (thbsBudgetCreationOutputBeanList != null
				&& !thbsBudgetCreationOutputBeanList.isEmpty()) {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(
							HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Thbs budget for addtional cost is created successfully",
							thbsBudgetCreationOutputBeanList));
		} else {
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
							.value(), MISConstants.FAILURE,
							"Failed to create thbs budget for addtional cost."));
		}
	}

	// EOA By Rinta

	// Added By Rinta
	/**
	 * <Description Update Thbs Budget For Additional Cost> : Update Thbs Budget
	 * For Additional Cost.
	 * 
	 * @param thbsBudgetForAdditionalCostBean
	 * @return ThbsBudgetCreationOutputBean
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */

	@ApiOperation(value = "Update Thbs Budget For Additional Cost. This service will be called when user wants to Update Thbs Budget For Additional Cost."
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to Update Thbs Budget For Additional Cost."
			+ "<br>The Response will be ThbsBudgetCreationOutputBean.",

	nickname = "Update Thbs Budget For Additional Cost.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ThbsBudgetCreationOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BudgetDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "Thbs Budget for Additional Cost Updated successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Update Thbs Budget For Additional Cost", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Update Thbs Budget For Additional Cost", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.THBS_BUDGET_ADDITIONAL_AMT, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> updateThbsBudgetForAdditionalCost(
			@Valid @RequestBody ThbsBudgetForAdditionalCostBean thbsBudgetForAdditionalCostBean)
			throws BusinessException, DataAccessException,
			CommonCustomException {
		List<ThbsBudgetCreationOutputBean> thbsBudgetCreationOutputBeanList = new ArrayList<ThbsBudgetCreationOutputBean>();
		try {
			thbsBudgetCreationOutputBeanList = thbsBudgetService
					.updateThbsBudgetForAdditionalCost(thbsBudgetForAdditionalCostBean);
		} catch (DataAccessException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}

		if (thbsBudgetCreationOutputBeanList != null
				&& !thbsBudgetCreationOutputBeanList.isEmpty()) {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(
							HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Thbs budget for addtional cost is updated successfully",
							thbsBudgetCreationOutputBeanList));
		} else {
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
							.value(), MISConstants.FAILURE,
							"Failed to update thbs budget for addtional cost."));
		}
	}

	// EOA By Rinta
	
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description Create Thbs Budget> : Create Thbs Budget.
	 * @param
	 * @return List<ThbsBudgetCreationOutputBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Create Thbs Budget. This service will be called when user wants to create Thbs Budget"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to create Thbs Budget."
			+ "<br>The Response will be ThbsBudgetCreationOutputBean.",

	nickname = "Create Thbs Budget.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ThbsBudgetCreationOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BudgetDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "Thbs Budget created successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
@RequestMapping(value = ProjectURIConstants.GET_ALL_THBS_BUDGET_BY_CLIENT_BUDGET_ID, method = RequestMethod.GET)
public ResponseEntity<MISResponse> getAllThbsBudgetDetailsByClientBudgetId(
	@PathVariable Integer clientBudgetId,@PathVariable Integer empId)
		throws BusinessException, DataAccessException, CommonCustomException {
		
		List<ThbsBudgetOutputBean> thbsBudgetOutputBeanList=
		new ArrayList<ThbsBudgetOutputBean>();

	try {
		thbsBudgetOutputBeanList = thbsBudgetService.getAllThbsBudgetDetailsByClientBudgetId(clientBudgetId, empId);
				
	} catch (DataAccessException e) {
		throw new CommonCustomException(e.getMessage());
	}
	
	
	if (!thbsBudgetOutputBeanList.isEmpty()) {
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS,
						"Budget details fetched successfully.",
						thbsBudgetOutputBeanList));
	} else {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
				new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						MISConstants.FAILURE, "No record found."));
	}
}
	
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description Create Thbs Budget> : Create Thbs Budget.
	 * @param
	 * @return List<ThbsBudgetCreationOutputBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Create Thbs Budget. This service will be called when user wants to create Thbs Budget"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to create Thbs Budget."
			+ "<br>The Response will be ThbsBudgetCreationOutputBean.",

	nickname = "Create Thbs Budget.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ThbsBudgetCreationOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BudgetDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "Thbs Budget created successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
@RequestMapping(value = ProjectURIConstants.GET_STANDARD_RATE, method = RequestMethod.POST)
public ResponseEntity<MISResponse> getStandardRate(
	@Valid @RequestBody StandardRateInputBean standardRateInputBean,@PathVariable Integer empId)
		throws BusinessException, DataAccessException, CommonCustomException {
		
		List<BigDecimal> standardRateList ; 

	try {
		standardRateList = thbsBudgetService.getStandardRate(standardRateInputBean,empId);
				
	} catch (DataAccessException e) {
		throw new CommonCustomException(e.getMessage());
	}
	
	
	if (!standardRateList.isEmpty()) {
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS,
						"Standard rate fetched successfully.",
						standardRateList));
	} else {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
				new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						MISConstants.FAILURE, "No records found."));
	}
}
	
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description Create Thbs Budget> : Create Thbs Budget.
	 * @param
	 * @return List<ThbsBudgetCreationOutputBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Create Thbs Budget. This service will be called when user wants to create Thbs Budget"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to create Thbs Budget."
			+ "<br>The Response will be ThbsBudgetCreationOutputBean.",

	nickname = "Create Thbs Budget.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ThbsBudgetCreationOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BudgetDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "Thbs Budget created successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
@RequestMapping(value = ProjectURIConstants.GET_ALL_THBS_BUDGET_BY_WO_ID, method = RequestMethod.GET)
public ResponseEntity<MISResponse> getAllThbsBudgetDetailsByWoId(
	@PathVariable Integer woId,@PathVariable Integer empId)
		throws BusinessException, DataAccessException, CommonCustomException {
		
		List<ThbsBudgetOutputBean> thbsBudgetOutputBeanList=
		new ArrayList<ThbsBudgetOutputBean>();

	try {
		thbsBudgetOutputBeanList = thbsBudgetService.getAllThbsBudgetDetailsByWoId(woId, empId);
				
	} catch (DataAccessException e) {
		throw new CommonCustomException(e.getMessage());
	}
	
	
	if (!thbsBudgetOutputBeanList.isEmpty()) {
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS,
						"Budget details fetched successfully.",
						thbsBudgetOutputBeanList));
	} else {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
				new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						MISConstants.FAILURE, "No record found."));
	}
}
	

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description Create Thbs Budget> : Create Thbs Budget.
	 * @param
	 * @return List<ThbsBudgetCreationOutputBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Create Thbs Budget. This service will be called when user wants to create Thbs Budget"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to create Thbs Budget."
			+ "<br>The Response will be ThbsBudgetCreationOutputBean.",

	nickname = "Create Thbs Budget.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ThbsBudgetCreationOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BudgetDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "Thbs Budget created successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
@RequestMapping(value = ProjectURIConstants.SUBMIT_BUDGET_FOR_APPROVE, method = RequestMethod.POST)
public ResponseEntity<MISResponse> approveBudgetController(
		@Valid @RequestBody BudgetSubmitInputBean inputBean)
		throws BusinessException, DataAccessException, CommonCustomException {
		
		List<ThbsBudgetOutputBean> thbsBudgetOutputBeanList=
		new ArrayList<ThbsBudgetOutputBean>();
		boolean approved;

	try {
		 
		approved = thbsBudgetService.budgetSubmitForApproval(inputBean);
				
	} catch (DataAccessException e) {
		throw new CommonCustomException(e.getMessage());
	}
	
	
	if (approved) {
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS,
						"Budget submitted successfully.",
						thbsBudgetOutputBeanList));
	} else {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
				new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						MISConstants.FAILURE, "Fail to submit budget."));
	}
}
	

	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description Create Thbs Budget> : Create Thbs Budget.
	 * @param
	 * @return List<ThbsBudgetCreationOutputBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Create Thbs Budget. This service will be called when user wants to create Thbs Budget"
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to create Thbs Budget."
			+ "<br>The Response will be ThbsBudgetCreationOutputBean.",

	nickname = "Create Thbs Budget.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ThbsBudgetCreationOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BudgetDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "Thbs Budget created successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Thbs Budget", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
@RequestMapping(value = ProjectURIConstants.BUDGET_ACTION, method = RequestMethod.POST)
public ResponseEntity<MISResponse> budgetActionController(
		@Valid @RequestBody BudgetActionInputBean inputBean)
		throws BusinessException, DataAccessException, CommonCustomException {
		
		List<ThbsBudgetOutputBean> thbsBudgetOutputBeanList=
		new ArrayList<ThbsBudgetOutputBean>();
		boolean approved;

	try {
		 
		approved = thbsBudgetService.budgetAction(inputBean);
				
	} catch (DataAccessException e) {
		throw new CommonCustomException(e.getMessage());
	}
	
	
	if (approved) {
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS,
						"Your request processed successfully",
						thbsBudgetOutputBeanList));
	} else {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
				new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						MISConstants.FAILURE, "Fail to process your request."));
	}
}
	
	
}
