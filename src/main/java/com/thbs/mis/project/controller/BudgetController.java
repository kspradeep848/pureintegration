package com.thbs.mis.project.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ldap.core.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.project.bean.BudgetDetailsOutputBean;
import com.thbs.mis.project.bean.CliBudgetDesignationListAndAdditionalCostOutputBean;
import com.thbs.mis.project.bean.ClientBudgetBean;
import com.thbs.mis.project.bean.ClientBudgetInputBean;
import com.thbs.mis.project.bean.CompanyBudgetBean;
import com.thbs.mis.project.bean.ProjMasClientBudgetSummaryBean;
import com.thbs.mis.project.bean.ProjMasCompanyBudgetSummaryBean;
import com.thbs.mis.project.bean.ProjMasProjectClassificationBean;
import com.thbs.mis.project.bo.ProjDatClientBudgetAtResourceLevelBO;
import com.thbs.mis.project.constants.ProjectURIConstants;
import com.thbs.mis.project.service.BudgetService;





@Controller
public class BudgetController {
	
	
	
	@Autowired
	private BudgetService budgetService;
	
	
	
	
	private static final AppLog LOG = LogFactory
			.getLog(ProjectController.class);
/*

	//Added by Lalith kumar
	
		*//**
		 * @category This service is used for UI page to create company budget .
		 *           <Description ProjDatThbsBudgetForAdditionalCostBean> : Create company budget.
		 * @param
		 * @throws CommonCustomException
		 * @throws DataAccessException
		 *//*
		@ApiOperation(value = "Create company budget. This service will be called when user create new company budget"
				+ " .", httpMethod = "POST",

				notes = "This Service has been implemented to create the new company budget.",

				nickname = "Create new company budget", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Successfully created", response = MasClientBean.class),
				@ApiResponse(code = 201, message = "Successfully created", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create company budget", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create company budget", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
			
		@RequestMapping(value = ProjectURIConstants.COMPANY_BUDGET, method = RequestMethod.POST)
		public @ResponseBody ResponseEntity<MISResponse> createCompanyBudget(@Valid
				@RequestBody  ProjDatThbsBudgetForAdditionalCostBean thbsAdditionalCostBean)
				throws CommonCustomException, DataAccessException {
			//LOG.startUsecase("Creating Company Budget");

			boolean b = false;
			try {
				b = budgetService.createCompanyBudget(thbsAdditionalCostBean);

			} catch (Exception e) {
				throw new CommonCustomException(e.getMessage());
			}

			if (b) {


				return ResponseEntity.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Company Budget created successfully"));
			} else {

				return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(new MISResponse(
						HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "Error occurred while creating Company Budget."));
				
			}
		}
		//EOA by Lalith Kumar

		*/
		// Added by Rinta Mariam Jose
		
		/**
		 * @category This service is used for UI page to show the drop down list.
		 *           <Description getClientBudgetSummary> : Fetch Client Budget
		 *           Summary according to given workorderId.
		 * @param
		 * @return List<ProjMasClientBudgetSummaryBean>
		 * @throws BusinessException
		 * @throws DataAccessException
		 * @throws CommonCustomException 
		 */
		@ApiOperation(value = "Get Client Budget Summary. This service will be called when user wants to view Client Budget Summary according to given workorderId"
				+ " .", httpMethod = "GET",

		notes = "This Service has been implemented to fetch Client Budget Summary according to given workorderId."
				+ "<br>The Response will be Client Budget Summary according to given workorderId.",

		nickname = "Get Client Budget Summary according to given workorderId", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ProjMasClientBudgetSummaryBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = ProjMasClientBudgetSummaryBean.class),
				@ApiResponse(code = 201, message = "Client Budget Summary Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Budget Summary", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Budget Summary", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = ProjectURIConstants.GET_CLIENT_BUDGET_SUMMARY, method = RequestMethod.GET)
		public @ResponseBody ResponseEntity<MISResponse> getClientBudgetSummary(
				@PathVariable Integer workorderId, @PathVariable Integer empId)
				throws DataAccessException, CommonCustomException {
			List<ProjMasClientBudgetSummaryBean> projMasClientBudgetSummaryBeanList = null;

			try {
				projMasClientBudgetSummaryBeanList = budgetService
						.getClientBudgetSummary(workorderId, empId);
			} catch (Exception e) {
				throw new CommonCustomException(
						"Failed to fetch client budget summary: " + e.getMessage());
			}

			if (!projMasClientBudgetSummaryBeanList.isEmpty()) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Fetched client budget summary",
								projMasClientBudgetSummaryBeanList));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								MISConstants.FAILURE,
								"Failed to fetch client budget summary.",
								projMasClientBudgetSummaryBeanList));
			}
		}
		
		
		/**
		 * @category This service is used for UI page to show the drop down list.
		 *           <Description getCompanyBudgetSummary> : Fetch Company Budget
		 *           Summary according to given workorderId.
		 * @param
		 * @return List<ProjMasCompanyBudgetSummaryBean>
		 * @throws BusinessException
		 * @throws DataAccessException
		 * @throws CommonCustomException 
		 */
		@ApiOperation(value = "Get Company Budget Summary. This service will be called when user wants to view Company Budget Summary according to given workorderId"
				+ " .", httpMethod = "GET",

		notes = "This Service has been implemented to fetch Company Budget Summary according to given workorderId."
				+ "<br>The Response will be Company Budget Summary according to given workorderId.",

		nickname = "Get Company Budget Summary according to given workorderId", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ProjMasCompanyBudgetSummaryBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = ProjMasCompanyBudgetSummaryBean.class),
				@ApiResponse(code = 201, message = "Company Budget Summary Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Company Budget Summary", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Company Budget Summary", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = ProjectURIConstants.GET_COMPANY_BUDGET_SUMMARY, method = RequestMethod.GET)
		public @ResponseBody ResponseEntity<MISResponse> getCompanyBudgetSummary(
				@PathVariable Integer workorderId, @PathVariable Integer empId)
				throws DataAccessException, CommonCustomException {
			List<ProjMasCompanyBudgetSummaryBean> projMasCompanyBudgetSummaryBeanList = null;

			try {
				projMasCompanyBudgetSummaryBeanList = budgetService
						.getCompanyBudgetSummary(workorderId, empId);
			} catch (Exception e) {
				throw new CommonCustomException(
						"Failed to fetch company budget summary: " + e.getMessage());
			}

			if (!projMasCompanyBudgetSummaryBeanList.isEmpty()) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Fetched company budget summary",
								projMasCompanyBudgetSummaryBeanList));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								MISConstants.FAILURE,
								"Failed to fetch company budget summary.",
								projMasCompanyBudgetSummaryBeanList));
			}
		}
		
		// EOA by Rinta Mariam Jose

		
		

	/*	//Added by Lalith kumar
		
			*//**
			 * @category This service is used for UI page to update company budget .
			 *           <Description ProjDatThbsBudgetForAdditionalCostBean> : update company budget.
			 * @param
			 * @throws CommonCustomException
			 * @throws DataAccessException
			 *//*
			@ApiOperation(value = "update company budget. This service will be called when user update company budget"
					+ " .", httpMethod = "POST",

					notes = "This Service has been implemented to update the company budget.",

					nickname = "update company budget", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
			@ApiResponses(value = {
					@ApiResponse(code = 200, message = "Successfully created", response = MasClientBean.class),
					@ApiResponse(code = 201, message = "Successfully created", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "update company budget", response = URI.class)),
					@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "update company budget", response = URI.class)),
					@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
					@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
				
			@RequestMapping(value = ProjectURIConstants.COMPANY_BUDGET, method = RequestMethod.PUT)
			public @ResponseBody ResponseEntity<MISResponse> updateCompanyBudget(
					@RequestBody @Valid ProjDatThbsBudgetForAdditionalCostBean thbsAdditionalCostBean)
					throws CommonCustomException, DataAccessException {
				LOG.startUsecase("Updating Company Budget");

				boolean b = false;
				try {
					b = budgetService.updateCompanyBudget(thbsAdditionalCostBean);

				} catch (Exception e) {
					throw new CommonCustomException(e.getMessage());
				}

				if (b) {
					
					return ResponseEntity.status(HttpStatus.OK.value())
							.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Company Budget updated successfully"));
				} else {

					return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(new MISResponse(
							HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "Error occurred while updating Company Budget."));
					
				}
			}
			//EOA by Lalith Kumar
		
*/

/**
		 * @category This service is used for UI page to show the drop down list.
		 *           <Description getClientBudgetDetails> : Fetch Client Budget
		 *           Summary according to given workorderId.
		 * @param
		 * @return List<ClientBudgetBean>
		 * @throws DataAccessException
         * @throws CommonCustomException 
		 */
		@ApiOperation(value = "Get Client Budget Details. This service will be called when user wants to view Client Budget Details according to given workorderId"
				+ " .", httpMethod = "GET",

		notes = "This Service has been implemented to fetch Client Budget Details according to given workorderId."
				+ "<br>The Response will be Client Budget Details according to given workorderId.",

		nickname = "Get Client Budget Details according to given workorderId", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ClientBudgetBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = ClientBudgetBean.class),
				@ApiResponse(code = 201, message = "Client Budget Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Budget Details", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Budget Details", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = ProjectURIConstants.GET_CLIENT_BUDGET, method = RequestMethod.GET)
		public @ResponseBody ResponseEntity<MISResponse> getAllClientBudget(
				@PathVariable Integer workOrderId, @PathVariable Integer empId)
				throws DataAccessException, CommonCustomException {

			List<ClientBudgetBean> clientBudgetBeanList = null;

			try {
				clientBudgetBeanList = (List<ClientBudgetBean>) budgetService
						.getAllClientBudgetDetails(workOrderId, empId);
			} catch (Exception e) {
				throw new CommonCustomException("Failed to fetch Details : "
						+ e.getMessage());
			}

			if (!clientBudgetBeanList.isEmpty()) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Client budget details fetched successfully. ",
								clientBudgetBeanList));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								MISConstants.FAILURE,
								"Failed to fetch Client Budget Details.",
								clientBudgetBeanList));
			}
		}
		/**
		 * @category This service is used for UI page to show the drop down list.
		 *           <Description getCompanyBudgetDetails> : Fetch Company Budget
		 *           Details according to given workorderId.
		 * @param
		 * @return List<CompanyBudgetBean>
		 * @throws CommonCustomException
		 * @throws DataAccessException
		 */
		@ApiOperation(value = "Get Company Budget Details. This service will be called when user wants to view Company Budget Details according to given workorderId"
				+ " .", httpMethod = "GET",

		notes = "This Service has been implemented to fetch Company Budget details according to given workorderId."
				+ "<br>The Response will be Company Budget details according to given workorderId.",

		nickname = "Get Company Budget details according to given workorderId", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = CompanyBudgetBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = CompanyBudgetBean.class),
				@ApiResponse(code = 201, message = "Company Budget Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Company Budget Details", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Company Budget Details", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = ProjectURIConstants.GET_COMPANY_BUDGET, method = RequestMethod.GET)
		public @ResponseBody ResponseEntity<MISResponse> getAllCompanyBudget(
				@PathVariable Integer workOrderId, @PathVariable Integer empId)
				throws DataAccessException,CommonCustomException {

			List<CompanyBudgetBean> companyBudgetBeanList = null;

			try {
				companyBudgetBeanList = (List<CompanyBudgetBean>) budgetService
						.getAllCompanyBudgetDetails(workOrderId, empId);
			} catch (Exception e) {
				throw new CommonCustomException("Failed to fetch Details : "
						+ e.getMessage());
			}

			if (!companyBudgetBeanList.isEmpty()) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Company budget details fetched successfully. ",
								companyBudgetBeanList));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								MISConstants.FAILURE,
								"Failed to fetch Client Budget Details.",
								companyBudgetBeanList));
			}
		}
		
	//Added by prathibha for create client budget
		
		@ApiOperation(value = "Create Client Budget."+ " .", httpMethod = "POST",

				notes = "This Service has been implemented to create client budget."
						+ "<br>The Response will be sucessful message and records will be created.",

				nickname = "Create client budget", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
				@ApiResponses(value = {
						@ApiResponse(code = 200, message = "Request Executed Successfully", response = ProjMasProjectClassificationBean.class),
						@ApiResponse(code = 201, message = "All Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "ProjectClassification Details", response = URI.class)),
						@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "ProjectClassifications Details", response = URI.class)),
						@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
						@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })

				@RequestMapping(value = ProjectURIConstants.CREATE_CLIENT_BUDGET, method = RequestMethod.POST)
				public ResponseEntity<MISResponse> createCleintBudget(
						@RequestBody ClientBudgetInputBean bean) throws CommonCustomException,BusinessException,
						DataAccessException, ParseException,
						MethodArgumentNotValidException, CommonCustomException {
					LOG.startUsecase("Entering into createCleintBudget method in controller");

					Boolean flag = false;
					@SuppressWarnings("unused")
					ProjDatClientBudgetAtResourceLevelBO boBean = new ProjDatClientBudgetAtResourceLevelBO();
					ClientBudgetInputBean inputBean = new ClientBudgetInputBean();
					List<ClientBudgetInputBean> listBean = new ArrayList<ClientBudgetInputBean>();
					try {
						System.out.println("try block");
						flag = budgetService.createClientBudget(bean);
						listBean.add(inputBean);

					} catch ( CommonCustomException e) {
						throw new CommonCustomException(e.getMessage());
					}
					if (flag) {
						return ResponseEntity.status(HttpStatus.OK.value()).body(
								new MISResponse(HttpStatus.OK.value(),
										MISConstants.SUCCESS,
										"budget created successfully."));
					} else {
						return ResponseEntity.status(
								HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
								new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
										MISConstants.FAILURE,
										"Fail to create budget order."));
					}

				}

		/**
		 * @category This service is used for UI page to show the drop down list.
		 *           <Description getClientBudgetDetails> : Fetch Client Budget
		 *           Details according to given clientBudgetId.
		 * @param
		 * @return List<BudgetDetailsOutputBean>
		 * @throws CommonCustomException
		 * @throws DataAccessException
		 */
		@ApiOperation(value = "Get Client Budget Details. This service will be called when user wants to view Client Budget Details according to given clientBudgetId"
				+ " .", httpMethod = "GET",

		notes = "This Service has been implemented to fetch clientBudgetId Budget details according to given clientBudgetId."
				+ "<br>The Response will be Client Budget details according to given clientBudgetId.",

		nickname = "Get Client Budget details according to given clientBudgetId", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = BudgetDetailsOutputBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = BudgetDetailsOutputBean.class),
				@ApiResponse(code = 201, message = "Client Budget Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Budget Details", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Budget Details", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_ALL_CLIENT_BUDGET_DETAILS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getClientBudgetDetails(
			@PathVariable Integer clientBudgetId, @PathVariable Integer empId)
			throws BusinessException, DataAccessException, CommonCustomException {
		List<BudgetDetailsOutputBean> budgetDetailsOutputBeanList = new ArrayList<BudgetDetailsOutputBean>();
		try {
			budgetDetailsOutputBeanList = budgetService.getClientBudgetDetails(
					clientBudgetId, empId);
		} catch (DataAccessException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (!budgetDetailsOutputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Budget details fetched successfully.",
							budgetDetailsOutputBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No record found."));
		}
	}
		
		
		/**
		 * @category This service is used for UI page to show the drop down list.
		 *           <Description getClientBudgetDetails> : Fetch Client Budget
		 *           Details according to given clientBudgetId.
		 * @param
		 * @return List<BudgetDetailsOutputBean>
		 * @throws CommonCustomException
		 * @throws DataAccessException
		 */
		@ApiOperation(value = "Get Client Budget Details. This service will be called when user wants to view Client Budget Details according to given clientBudgetId"
				+ " .", httpMethod = "GET",

		notes = "This Service has been implemented to fetch clientBudgetId Budget details according to given clientBudgetId."
				+ "<br>The Response will be Client Budget details according to given clientBudgetId.",

		nickname = "Get Client Budget details according to given clientBudgetId", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = BudgetDetailsOutputBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = BudgetDetailsOutputBean.class),
				@ApiResponse(code = 201, message = "Client Budget Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Budget Details", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Client Budget Details", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_ALL_CLIENT_BUDGET_RESOURCE_DESIGNATION, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getClientBudgetDesignationList(
			 @PathVariable Integer woId)
			throws BusinessException, DataAccessException, CommonCustomException {
		List<CliBudgetDesignationListAndAdditionalCostOutputBean> budgetDetailsOutputBeanList = new ArrayList<CliBudgetDesignationListAndAdditionalCostOutputBean>();
		LOG.info("Entering the service");
		CliBudgetDesignationListAndAdditionalCostOutputBean clientBudgetDesignationAndAdditionCostBean;
		try {
			 clientBudgetDesignationAndAdditionCostBean = budgetService.getClientBudgetDesignationList(
					woId);
		} catch (DataAccessException e) {
			throw new CommonCustomException(e.getMessage());
		}
		
		budgetDetailsOutputBeanList.add(clientBudgetDesignationAndAdditionCostBean);
		if (!budgetDetailsOutputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Budget details fetched successfully.",
							budgetDetailsOutputBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No record found."));
		}
	}
		
		//Added by prathibha for update client budget
		
		@ApiOperation(value = "Update Client Budget."+ " .", httpMethod = "PUT",

				notes = "This Service has been implemented to update client budget."
						+ "<br>The Response will be sucessful message and records will be updted.",

				nickname = "Update client budget", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
				@ApiResponses(value = {
						@ApiResponse(code = 200, message = "Request Executed Successfully"),
						@ApiResponse(code = 201, message = "All Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "ProjectClassification Details", response = URI.class)),
						@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "ProjectClassifications Details", response = URI.class)),
						@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
						@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })

				@RequestMapping(value = ProjectURIConstants.UPDATE_CLIENT_BUDGET, method = RequestMethod.PUT)
				public ResponseEntity<MISResponse> updateCleintBudget(@RequestBody ClientBudgetInputBean bean) 
				throws CommonCustomException,BusinessException,DataAccessException, ParseException,
						MethodArgumentNotValidException, CommonCustomException {
					LOG.startUsecase("Entering into updateCleintBudget method in controller");
		
					//BudgetDetailsOutputBean outPutBean = new BudgetDetailsOutputBean();
					List<BudgetDetailsOutputBean> updatedBudgetList = new ArrayList<BudgetDetailsOutputBean>();
					
						System.out.println("try block");
						boolean upDatedFlag = false;
						
					try {
						upDatedFlag = budgetService.updateClientBudget(bean);
						//updatedBudgetList.add(outPutBean);

					} catch ( CommonCustomException e) {
						throw new CommonCustomException(e.getMessage());
					}
					if (upDatedFlag) 
					{
						return ResponseEntity.status(HttpStatus.OK.value()).body(
								new MISResponse(HttpStatus.OK.value(),
										MISConstants.SUCCESS,
										"budget updated successfully.",updatedBudgetList));
					} else {
						return ResponseEntity.status(
								HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
								new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
										MISConstants.FAILURE,
										"Fail to budget budget order."));
					}

				}
		
		
		
		
}

	

	

