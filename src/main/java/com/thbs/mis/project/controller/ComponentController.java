package com.thbs.mis.project.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.project.bean.ComponentDetailsOutputBean;
import com.thbs.mis.project.bean.ComponentDetailsOutputBean1;
import com.thbs.mis.project.bean.ComponentDetailsOutputBean2;
import com.thbs.mis.project.bean.ComponentInputBean;
import com.thbs.mis.project.bean.ComponentOutputBean;
import com.thbs.mis.project.bean.ComponentOwnerBean;
import com.thbs.mis.project.bean.SearchComponentInputBean;
import com.thbs.mis.project.bo.DatComponentBO;
import com.thbs.mis.project.constants.ProjectURIConstants;
import com.thbs.mis.project.service.ComponentService;

/**
 * @author rithesh_nagaraja
 *
 */
@Controller
public class ComponentController {

	private static final AppLog LOG = LogFactory
			.getLog(ComponentController.class);

	@Autowired
	private ComponentService componentService;

	/**
	 * 
	 * @return Newly Created Component Id And Component Name.
	 * @throws CommonCustomException
	 * @throws DataAccessException 
	 * @Description: This method is used to Create New Component.
	 * 
	 */

	@ApiOperation(tags = "CreateComponent", value = "CreateComponent"
			+ "This service will be called from the front-end to Create New Component.", httpMethod = "POST", notes = "This service will be called from the front-end to to Create New Component."
			+ "<br> <b> Table : </b> </br>"
			+ "dat_component -> It contains Components." + "<br>", nickname = "CreateComponent", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = DatComponentBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = DatComponentBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Component", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.CREATE_UPDATE_COMPONENT, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> createComponent(
		@Valid @RequestBody ComponentInputBean inputBean)
			throws CommonCustomException, DataAccessException {

		LOG.startUsecase("createComponent() === controller");

		ComponentOutputBean ComponentOutputBean = null;
		List<ComponentOutputBean> ComponentOutputBeanList = new ArrayList<ComponentOutputBean>();

		try {
			// Calling CreateComponent() method of ComponentService.
			ComponentOutputBean = componentService
					.createComponent(inputBean);
			ComponentOutputBeanList.add(ComponentOutputBean);
		} catch (Exception e) {
			//e.printStackTrace();
			LOG.startUsecase("Exception in createComponent() == >> "
					+ e.getMessage());
			throw new CommonCustomException(e.getMessage());
		}

		if (ComponentOutputBean != null) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Component created successfully.",
							ComponentOutputBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Component could not be created successfully.",
							ComponentOutputBeanList));
		}

	}

	/**
	 * 
	 * @return Components.
	 * @throws DataAccessException 
	 * @throws CommonCustomException
	 * @Description: This method is used to search Components.
	 * 
	 */
	@ApiOperation(tags = "SearchComponent", value = "SearchComponent"
			+ "This service will be called from the front-end to Search Component.", httpMethod = "POST", notes = "This service will be called from the front-end to Search Component."
			+ "<br> <b> Table : </b> </br>"
			+ "dat_component -> It contains Components." + "<br>", nickname = "CreateComponent", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = DatComponentBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = DatComponentBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Component", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.SEARCH_COMPONENTS, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> searchComponent(
			@RequestBody SearchComponentInputBean inputBean) throws DataAccessException, CommonCustomException {

		LOG.startUsecase("createComponent() === controller");

		Page<ComponentDetailsOutputBean2> componentDetailsOutputBeanlist = null;
		try {
			componentDetailsOutputBeanlist = componentService.searchComponent(inputBean);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (!componentDetailsOutputBeanlist.getContent().isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"successfully searched work order details.",
							componentDetailsOutputBeanlist));
		}
		else if(componentDetailsOutputBeanlist.getContent().isEmpty()){
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No records found", componentDetailsOutputBeanlist));
		}
		else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE,
							"FAIL TO SEARCH Component DETAILS"));
		}
		
	}
	
	/**
	 * 
	 * @return Components.
	 * @throws DataAccessException 
	 * @throws CommonCustomException
	 * @Description: This method is used to search Components.
	 * 
	 */
	@ApiOperation(tags = "SearchComponent", value = "SearchComponent"
			+ "This service will be called from the front-end to Search Component.", httpMethod = "GET", notes = "This service will be called from the front-end to Search Component."
			+ "<br> <b> Table : </b> </br>"
			+ "dat_component -> It contains Components." + "<br>", nickname = "CreateComponent", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = DatComponentBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = DatComponentBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Component", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.SEARCH_COMPONENTS_ON_COMPONENT_FULLVIEW_ID, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> searchComponentOnCompId(
			@PathVariable Short componentId) throws DataAccessException, CommonCustomException {

		LOG.startUsecase("searchComponentOnCompId() === ComponentController");

		ComponentDetailsOutputBean outputBean = new ComponentDetailsOutputBean();
		List<ComponentDetailsOutputBean> outputBeanList = new ArrayList<ComponentDetailsOutputBean>();

		try {
			outputBean = componentService.componentFullView(componentId);
			outputBeanList.add(outputBean);
		} catch (Exception e) {
			//e.printStackTrace();
			LOG.startUsecase("Exception in createComponent() == >> "
					+ e.getMessage());
			throw new CommonCustomException(e.getMessage());
		}

		if (!outputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"successfully searched work order details.",
							outputBeanList));
		} else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Failed to search the component details",
							outputBeanList));
		}
	}
	
	/**
	 * 
	 * @return Components.
	 * @throws DataAccessException 
	 * @throws CommonCustomException
	 * @Description: This method is used to search Components.
	 * 
	 */

	@ApiOperation(tags = "SearchComponent", value = "SearchComponent"
			+ "This service will be called from the front-end to Search Component.", httpMethod = "POST", notes = "This service will be called from the front-end to Search Component."
			+ "<br> <b> Table : </b> </br>"
			+ "dat_component -> It contains Components." + "<br>", nickname = "CreateComponent", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = DatComponentBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = DatComponentBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Component", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.CREATE_UPDATE_COMPONENT, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> updateComponent(
			@Valid @RequestBody ComponentInputBean updateBean) throws DataAccessException, CommonCustomException {

		LOG.startUsecase("updateComponent() === ComponentController");

		ComponentOutputBean outputBean = new ComponentOutputBean();
		List<ComponentOutputBean> outputBeanList = new ArrayList<ComponentOutputBean>();

		try {
			outputBean = componentService.updateComponent(updateBean);
			
		} catch (Exception e) {
			//e.printStackTrace();
			LOG.startUsecase("Exception in updateComponent() == >> "
					+ e.getMessage());
			throw new CommonCustomException(e.getMessage());
		}
		outputBeanList.add(outputBean);

		if (!outputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"component updated successfully.",
							outputBeanList));
		} else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"component can not be updated successfully.",
							outputBeanList));
		}
	}
	
	
	/**
	 * NUCLEUS_PRJ_056
	 * @return Components.
	 * @throws DataAccessException 
	 * @throws CommonCustomException
	 * @Description: This method is used to search Components.
	 * 
	 */
	@ApiOperation(tags = "SearchComponent", value = "SearchComponent"
			+ "This service will be called from the front-end to Search Component.", httpMethod = "GET", notes = "This service will be called from the front-end to Search Component."
			+ "<br> <b> Table : </b> </br>"
			+ "dat_component -> It contains Components." + "<br>", nickname = "CreateComponent", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = DatComponentBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = DatComponentBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Component", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_COMPONENT, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getComponent(@PathVariable Integer componentId) throws DataAccessException, CommonCustomException {

		LOG.startUsecase("getComponent() === ComponentController");

		ComponentDetailsOutputBean1 outputBean = new ComponentDetailsOutputBean1();
		List<ComponentDetailsOutputBean1> outputBeanList = new ArrayList<ComponentDetailsOutputBean1>();

		try {
			outputBean = componentService.SearchComponent(componentId);
			outputBeanList.add(outputBean);
		} catch (Exception e) {
			//e.printStackTrace();
			LOG.startUsecase("Exception in updateComponent() == >> "
					+ e.getMessage());
			throw new CommonCustomException(e.getMessage());
		}

		if (!outputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"component details fetched successfully.",
							outputBeanList));
		} else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Failed to get component details.",
							outputBeanList));
		}
	}
	
	/**
	 * @category This service is used for UI page to show the drop down list.
	 *           <Description getComponentOwners : Fetch Component Owners
	 *           list>
	 * @param
	 * @return List<ComponentOwnerBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Get Component Owner list. This service will be called when user wants to view Component Owner list in UI"
			+ " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch Component Owner list."
			+ "<br>The Response will be the Component Owner list.",

	nickname = "Component Owner Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ComponentOwnerBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = ComponentOwnerBean.class),
			@ApiResponse(code = 201, message = "All Component Owners details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Component Owners Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Component Owners Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ProjectURIConstants.GET_ALL_COMPONENT_OWNERS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllComponentOwner(@PathVariable Short buId)
			throws DataAccessException,
			CommonCustomException {
		List<ComponentOwnerBean> componentOwnerBeanList = null;

		try {
			componentOwnerBeanList = componentService
					.getAllComponentOwners(buId);
		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch the details");
		}

		if (!componentOwnerBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Component owners fetched successfully",
							componentOwnerBeanList));
		} else {
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Fail to fetch the component owner details."));

		}
	}
}
