/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  CorsFilter.java           		                 */
/*                                                                   */
/*  Author      :  Pradeep K S	                                     */
/*                                                                   */
/*  Date        :  16-Mar-2017                                       */
/*                                                                   */
/*  Description :  Enabling Cors Filter For Entire Application   	 */
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 16-Mar-2017    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.cors;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


public class CorsFilter extends org.springframework.web.filter.CorsFilter {

    public CorsFilter() {
        super(configurationSource());
    }

	@Value("${cors.url.path}")
	private static String corsUrlPath;
	

    private static UrlBasedCorsConfigurationSource configurationSource() {
    	List<String> allowedOrigins = new ArrayList<String>();
    	//allowedOrigins.add("https://my.thbs.com");
    	allowedOrigins.add(corsUrlPath);
    	allowedOrigins.add("*");
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowedOrigins(allowedOrigins);
        //config.addAllowedOrigin("*");
        config.addAllowedMethod("*");
        config.addAllowedHeader("*");
        config.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return source;
    }
 
}