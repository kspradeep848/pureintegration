/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thbs.mis.expense.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.googlecode.jmapper.annotations.JMap;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.bo.MasCurrencytypeBO;
import com.thbs.mis.expense.bo.ExpDatExpenseBO;
import com.thbs.mis.expense.bo.ExpReportMasStatusBO;
import com.thbs.mis.framework.util.CustomDateSerializer;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class ExpDatReportBean {
	@JMap
	private Integer pkReportId;
	@JMap
	private Integer approverMgr;
	@JMap
	private Integer approverFin;
	@JsonSerialize(using = CustomDateSerializer.class)
	@JMap
	private Date startDate;
	@JsonSerialize(using = CustomDateSerializer.class)
	@JMap
	private Date endDate;
	@JsonSerialize(using = CustomTimeSerializer.class)
	@JMap
	private Date submissionDate;
	@JMap
	private String reportDesc;
	@JMap
	private String reportTitle;
	@JsonSerialize(using = CustomTimeSerializer.class)
	@JMap
	private Date modificationDate;
	@JsonSerialize(using = CustomTimeSerializer.class)
	@JMap
	private Date managerApprovalDate;
	@JsonSerialize(using = CustomTimeSerializer.class)
	@JMap
	private Date financeApprovalDate;
	@JMap
	private String paymentType;
	@JMap
	private Integer fkEmpIdInt;
	@JMap
	private Double totalAmount;
	@JMap
	private Double rmApprovedAmount;
	@JMap
	private Double finReimbursedAmount;
	@JMap
	private Short fkStatusId;
	@JMap
	private String approverMgrComment;
	@JMap
	private String approverFinComment;
	@JMap
	private Short fkFinReimburser;
	@JMap
	private Short fkCurrencyTypeId;
	@JMap
	private Integer fkClientId;
	@JMap
	private Integer fkProjectId;
	@JMap
	private Byte fkBuId;
	@JMap
	private Integer fkForwardRmId;
	@JMap
	private Short recallStatus;
	@JMap
	private Short redraftedStatus;
	@JMap
	private ExpReportMasStatusBO expReportMasStatusBO;
	@JMap
	private MasBuUnitBO masBuUnitBO;
	@JMap
	private MasCurrencytypeBO masCurrencytypeBO;
	private String submittedByName;
	private String rmName;
	private String finApproveName;
	private String forwardRMName;
	private List<Integer> pkreportIdList;
	private List<Integer> fkExpenseIdList;
	private DatEmpDetailBO datEmpDetailBoByFkEmpId;
	private List<ExpDatExpenseBO> ExpDatExpenseBO;
	private Integer financeLogin;

	public ExpDatReportBean(Integer pkReportId, Integer approverMgr, Integer approverFin, Date startDate, Date endDate,
			Date submissionDate, String reportDesc, String reportTitle, Date modificationDate, String paymentType,
			Integer fkEmpIdInt) {

		this.pkReportId = pkReportId;
		this.approverMgr = approverMgr;
		this.approverFin = approverFin;
		this.startDate = startDate;
		this.endDate = endDate;
		this.submissionDate = submissionDate;
		this.reportDesc = reportDesc;
		this.reportTitle = reportTitle;
		this.modificationDate = modificationDate;
		this.paymentType = paymentType;
		this.fkEmpIdInt = fkEmpIdInt;
	}

	public ExpDatReportBean() {

	}

	public Integer getPkReportId() {
		return pkReportId;
	}

	public Integer getApproverMgr() {
		return approverMgr;
	}

	public Integer getApproverFin() {
		return approverFin;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public Date getSubmissionDate() {
		return submissionDate;
	}

	public String getReportDesc() {
		return reportDesc;
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public DatEmpDetailBO getDatEmpDetailBoByFkEmpId() {
		return datEmpDetailBoByFkEmpId;
	}

	public void setPkReportId(Integer pkReportId) {
		this.pkReportId = pkReportId;
	}

	public void setApproverMgr(Integer approverMgr) {
		this.approverMgr = approverMgr;
	}

	public void setApproverFin(Integer approverFin) {
		this.approverFin = approverFin;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}

	public void setReportDesc(String reportDesc) {
		this.reportDesc = reportDesc;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public void setDatEmpDetailBoByFkEmpId(DatEmpDetailBO datEmpDetailBoByFkEmpId) {
		this.datEmpDetailBoByFkEmpId = datEmpDetailBoByFkEmpId;
	}

	public Integer getFkEmpIdInt() {
		return fkEmpIdInt;
	}

	public void setFkEmpIdInt(Integer fkEmpIdInt) {
		this.fkEmpIdInt = fkEmpIdInt;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Short getFkStatusId() {
		return fkStatusId;
	}

	public void setFkStatusId(Short fkStatusId) {
		this.fkStatusId = fkStatusId;
	}

	public String getSubmittedByName() {
		return submittedByName;
	}

	public void setSubmittedByName(String submittedByName) {
		this.submittedByName = submittedByName;
	}

	public List<Integer> getPkreportIdList() {
		return pkreportIdList;
	}

	public void setPkreportIdList(List<Integer> pkreportIdList) {
		this.pkreportIdList = pkreportIdList;
	}

	public String getRmName() {
		return rmName;
	}

	public void setRmName(String rmName) {
		this.rmName = rmName;
	}

	public String getFinApproveName() {
		return finApproveName;
	}

	public void setFinApproveName(String finApproveName) {
		this.finApproveName = finApproveName;
	}

	public String getApproverMgrComment() {
		return approverMgrComment;
	}

	public void setApproverMgrComment(String approverMgrComment) {
		this.approverMgrComment = approverMgrComment;
	}

	public String getApproverFinComment() {
		return approverFinComment;
	}

	public void setApproverFinComment(String approverFinComment) {
		this.approverFinComment = approverFinComment;
	}

	public List<Integer> getFkExpenseIdList() {
		return fkExpenseIdList;
	}

	public void setFkExpenseIdList(List<Integer> fkExpenseIdList) {
		this.fkExpenseIdList = fkExpenseIdList;
	}

	public List<ExpDatExpenseBO> getExpDatExpenseBO() {
		return ExpDatExpenseBO;
	}

	public void setExpDatExpenseBO(List<ExpDatExpenseBO> expDatExpenseBO) {
		ExpDatExpenseBO = expDatExpenseBO;
	}

	public Short getFkFinReimburser() {
		return fkFinReimburser;
	}

	public void setFkFinReimburser(Short fkFinReimburser) {
		this.fkFinReimburser = fkFinReimburser;
	}

	public Short getFkCurrencyTypeId() {
		return fkCurrencyTypeId;
	}

	public void setFkCurrencyTypeId(Short fkCurrencyTypeId) {
		this.fkCurrencyTypeId = fkCurrencyTypeId;
	}

	public Date getManagerApprovalDate() {
		return managerApprovalDate;
	}

	public void setManagerApprovalDate(Date managerApprovalDate) {
		this.managerApprovalDate = managerApprovalDate;
	}

	public Date getFinanceApprovalDate() {
		return financeApprovalDate;
	}

	public void setFinanceApprovalDate(Date financeApprovalDate) {
		this.financeApprovalDate = financeApprovalDate;
	}

	public Integer getFkClientId() {
		return fkClientId;
	}

	public void setFkClientId(Integer fkClientId) {
		this.fkClientId = fkClientId;
	}

	public Integer getFkProjectId() {
		return fkProjectId;
	}

	public void setFkProjectId(Integer fkProjectId) {
		this.fkProjectId = fkProjectId;
	}

	public Byte getFkBuId() {
		return fkBuId;
	}

	public void setFkBuId(Byte fkBuId) {
		this.fkBuId = fkBuId;
	}

	public ExpReportMasStatusBO getExpReportMasStatusBO() {
		return expReportMasStatusBO;
	}

	public void setExpReportMasStatusBO(ExpReportMasStatusBO expReportMasStatusBO) {
		this.expReportMasStatusBO = expReportMasStatusBO;
	}

	public MasBuUnitBO getMasBuUnitBO() {
		return masBuUnitBO;
	}

	public void setMasBuUnitBO(MasBuUnitBO masBuUnitBO) {
		this.masBuUnitBO = masBuUnitBO;
	}

	public Integer getFinanceLogin() {
		return financeLogin;
	}

	public void setFinanceLogin(Integer financeLogin) {
		this.financeLogin = financeLogin;
	}

	public Double getRmApprovedAmount() {
		return rmApprovedAmount;
	}

	public void setRmApprovedAmount(Double rmApprovedAmount) {
		this.rmApprovedAmount = rmApprovedAmount;
	}

	public Double getFinReimbursedAmount() {
		return finReimbursedAmount;
	}

	public void setFinReimbursedAmount(Double finReimbursedAmount) {
		this.finReimbursedAmount = finReimbursedAmount;
	}

	public Integer getFkForwardRmId() {
		return fkForwardRmId;
	}

	public void setFkForwardRmId(Integer fkForwardRmId) {
		this.fkForwardRmId = fkForwardRmId;
	}

	public String getForwardRMName() {
		return forwardRMName;
	}

	public void setForwardRMName(String forwardRMName) {
		this.forwardRMName = forwardRMName;
	}

	public Short getRecallStatus() {
		return recallStatus;
	}

	public void setRecallStatus(Short recallStatus) {
		this.recallStatus = recallStatus;
	}

	public MasCurrencytypeBO getMasCurrencytypeBO() {
		return masCurrencytypeBO;
	}

	public void setMasCurrencytypeBO(MasCurrencytypeBO masCurrencytypeBO) {
		this.masCurrencytypeBO = masCurrencytypeBO;
	}

	public Short getRedraftedStatus() {
		return redraftedStatus;
	}

	public void setRedraftedStatus(Short redraftedStatus) {
		this.redraftedStatus = redraftedStatus;
	}

	@Override
	public String toString() {
		return "ExpDatReportBean [pkReportId=" + pkReportId + ", approverMgr=" + approverMgr + ", approverFin="
				+ approverFin + ", startDate=" + startDate + ", endDate=" + endDate + ", submissionDate="
				+ submissionDate + ", reportDesc=" + reportDesc + ", reportTitle=" + reportTitle + ", modificationDate="
				+ modificationDate + ", managerApprovalDate=" + managerApprovalDate + ", financeApprovalDate="
				+ financeApprovalDate + ", paymentType=" + paymentType + ", fkEmpIdInt=" + fkEmpIdInt + ", totalAmount="
				+ totalAmount + ", rmApprovedAmount=" + rmApprovedAmount + ", finReimbursedAmount="
				+ finReimbursedAmount + ", fkStatusId=" + fkStatusId + ", approverMgrComment=" + approverMgrComment
				+ ", approverFinComment=" + approverFinComment + ", fkFinReimburser=" + fkFinReimburser
				+ ", fkCurrencyTypeId=" + fkCurrencyTypeId + ", fkClientId=" + fkClientId + ", fkProjectId="
				+ fkProjectId + ", fkBuId=" + fkBuId + ", fkForwardRmId=" + fkForwardRmId + ", recallStatus="
				+ recallStatus + ", redraftedStatus=" + redraftedStatus + ", expReportMasStatusBO="
				+ expReportMasStatusBO + ", masBuUnitBO=" + masBuUnitBO + ", masCurrencytypeBO=" + masCurrencytypeBO
				+ ", submittedByName=" + submittedByName + ", rmName=" + rmName + ", finApproveName=" + finApproveName
				+ ", forwardRMName=" + forwardRMName + ", pkreportIdList=" + pkreportIdList + ", fkExpenseIdList="
				+ fkExpenseIdList + ", datEmpDetailBoByFkEmpId=" + datEmpDetailBoByFkEmpId + ", ExpDatExpenseBO="
				+ ExpDatExpenseBO + ", financeLogin=" + financeLogin + "]";
	}

}