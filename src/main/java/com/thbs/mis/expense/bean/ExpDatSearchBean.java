package com.thbs.mis.expense.bean;

import java.util.Date;
import java.util.List;

public class ExpDatSearchBean {

	private List<Integer> pkExpenseIds;
	private List<Integer> fkReportIds;
	private List<Integer> fkStatusIds;
	private List<Integer> fkEmpIds;
	private Float expenseAmountLessThan;
	private Float expenseAmountGreaterThan;
	private Float expenseAmountEqual;
	private String merchantLike;
	private Date expenseDateFrom;
	private Date expenseDateTo;
	private Date managerApprovalDateFrom;
	private Date managerApprovalDateTo;
	private Date financeApprovalDateFrom;
	private Date financeApprovalDateTo;
	private String currencyTypeLike;
	private Integer  currencyTypeIdEqual;
	public List<Integer> getPkExpenseIds() {
		return pkExpenseIds;
	}
	public List<Integer> getFkReportIds() {
		return fkReportIds;
	}
	public List<Integer> getFkStatusIds() {
		return fkStatusIds;
	}
	public List<Integer> getFkEmpIds() {
		return fkEmpIds;
	}
	public Float getExpenseAmountLessThan() {
		return expenseAmountLessThan;
	}
	public Float getExpenseAmountGreaterThan() {
		return expenseAmountGreaterThan;
	}
	public Float getExpenseAmountEqual() {
		return expenseAmountEqual;
	}
	public String getMerchantLike() {
		return merchantLike;
	}
	public Date getExpenseDateFrom() {
		return expenseDateFrom;
	}
	public Date getExpenseDateTo() {
		return expenseDateTo;
	}
	public Date getManagerApprovalDateFrom() {
		return managerApprovalDateFrom;
	}
	public Date getManagerApprovalDateTo() {
		return managerApprovalDateTo;
	}
	public Date getFinanceApprovalDateFrom() {
		return financeApprovalDateFrom;
	}
	public Date getFinanceApprovalDateTo() {
		return financeApprovalDateTo;
	}
	public String getCurrencyTypeLike() {
		return currencyTypeLike;
	}
	public Integer getCurrencyTypeIdEqual() {
		return currencyTypeIdEqual;
	}
	public void setPkExpenseIds(List<Integer> pkExpenseIds) {
		this.pkExpenseIds = pkExpenseIds;
	}
	public void setFkReportIds(List<Integer> fkReportIds) {
		this.fkReportIds = fkReportIds;
	}
	public void setFkStatusIds(List<Integer> fkStatusIds) {
		this.fkStatusIds = fkStatusIds;
	}
	public void setFkEmpIds(List<Integer> fkEmpIds) {
		this.fkEmpIds = fkEmpIds;
	}
	public void setExpenseAmountLessThan(Float expenseAmountLessThan) {
		this.expenseAmountLessThan = expenseAmountLessThan;
	}
	public void setExpenseAmountGreaterThan(Float expenseAmountGreaterThan) {
		this.expenseAmountGreaterThan = expenseAmountGreaterThan;
	}
	public void setExpenseAmountEqual(Float expenseAmountEqual) {
		this.expenseAmountEqual = expenseAmountEqual;
	}
	public void setMerchantLike(String merchantLike) {
		this.merchantLike = merchantLike;
	}
	public void setExpenseDateFrom(Date expenseDateFrom) {
		this.expenseDateFrom = expenseDateFrom;
	}
	public void setExpenseDateTo(Date expenseDateTo) {
		this.expenseDateTo = expenseDateTo;
	}
	public void setManagerApprovalDateFrom(Date managerApprovalDateFrom) {
		this.managerApprovalDateFrom = managerApprovalDateFrom;
	}
	public void setManagerApprovalDateTo(Date managerApprovalDateTo) {
		this.managerApprovalDateTo = managerApprovalDateTo;
	}
	public void setFinanceApprovalDateFrom(Date financeApprovalDateFrom) {
		this.financeApprovalDateFrom = financeApprovalDateFrom;
	}
	public void setFinanceApprovalDateTo(Date financeApprovalDateTo) {
		this.financeApprovalDateTo = financeApprovalDateTo;
	}
	public void setCurrencyTypeLike(String currencyTypeLike) {
		this.currencyTypeLike = currencyTypeLike;
	}
	public void setCurrencyTypeIdEqual(Integer currencyTypeIdEqual) {
		this.currencyTypeIdEqual = currencyTypeIdEqual;
	}
	@Override
	public String toString() {
		return "ExpDatSearchBean [pkExpenseIds=" + pkExpenseIds
				+ ", fkReportIds=" + fkReportIds + ", fkStatusIds="
				+ fkStatusIds + ", fkEmpIds=" + fkEmpIds
				+ ", expenseAmountLessThan=" + expenseAmountLessThan
				+ ", expenseAmountGreaterThan=" + expenseAmountGreaterThan
				+ ", expenseAmountEqual=" + expenseAmountEqual
				+ ", merchantLike=" + merchantLike + ", expenseDateFrom="
				+ expenseDateFrom + ", expenseDateTo=" + expenseDateTo
				+ ", managerApprovalDateFrom=" + managerApprovalDateFrom
				+ ", managerApprovalDateTo=" + managerApprovalDateTo
				+ ", financeApprovalDateFrom=" + financeApprovalDateFrom
				+ ", financeApprovalDateTo=" + financeApprovalDateTo
				+ ", currencyTypeLike=" + currencyTypeLike
				+ ", currencyTypeIdEqual=" + currencyTypeIdEqual + "]";
	}
 
	 }
