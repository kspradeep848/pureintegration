/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thbs.mis.expense.bean;

import java.io.Serializable;
import java.util.Collection;

import com.thbs.mis.expense.bo.ExpDatExpenseBO;

public class ExpMasCategoryBean implements Serializable {
	private Integer pkCategoryId;
	private String categoryName;
	private String categoryShortName;
	private Collection<ExpDatExpenseBO> ExpDatExpenseBOCollection;

	public Integer getPkCategoryId() {
		return pkCategoryId;
	}

	public void setPkCategoryId(Integer pkCategoryId) {
		this.pkCategoryId = pkCategoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryShortName() {
		return categoryShortName;
	}

	public void setCategoryShortName(String categoryShortName) {
		this.categoryShortName = categoryShortName;
	}

	public Collection<ExpDatExpenseBO> getExpDatExpenseBOCollection() {
		return ExpDatExpenseBOCollection;
	}

	public void setExpDatExpenseBOCollection(
			Collection<ExpDatExpenseBO> expDatExpenseBOCollection) {
		ExpDatExpenseBOCollection = expDatExpenseBOCollection;
	}

	@Override
	public String toString() {
		return "ExpMasCategoryBean [pkCategoryId=" + pkCategoryId
				+ ", categoryName=" + categoryName + ", categoryShortName="
				+ categoryShortName + ", ExpDatExpenseBOCollection="
				+ ExpDatExpenseBOCollection + "]";
	}

}