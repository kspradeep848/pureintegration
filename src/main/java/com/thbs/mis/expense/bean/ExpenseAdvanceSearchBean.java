package com.thbs.mis.expense.bean;

import java.util.Date;

public class ExpenseAdvanceSearchBean {

	private Integer submittedBy;

	private String reportName;

	private Short status;

	private Integer client;

	private Integer project;

	private Integer reimbursedBy;

	private Date startDate;

	private Date endDate;

	private Integer approver;
	
	private Integer financeLogin;
	
	private Integer rmLogin;

	public Integer getSubmittedBy() {
		return submittedBy;
	}

	public void setSubmittedBy(Integer submittedBy) {
		this.submittedBy = submittedBy;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Integer getClient() {
		return client;
	}

	public void setClient(Integer client) {
		this.client = client;
	}

	public Integer getProject() {
		return project;
	}

	public void setProject(Integer project) {
		this.project = project;
	}

	public Integer getReimbursedBy() {
		return reimbursedBy;
	}

	public void setReimbursedBy(Integer reimbursedBy) {
		this.reimbursedBy = reimbursedBy;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getApprover() {
		return approver;
	}

	public void setApprover(Integer approver) {
		this.approver = approver;
	}

	public Integer getFinanceLogin() {
		return financeLogin;
	}

	public void setFinanceLogin(Integer financeLogin) {
		this.financeLogin = financeLogin;
	}

	public Integer getRmLogin() {
		return rmLogin;
	}

	public void setRmLogin(Integer rmLogin) {
		this.rmLogin = rmLogin;
	}

	@Override
	public String toString() {
		return "ExpenseAdvanceSearchBean [submittedBy=" + submittedBy + ", reportName=" + reportName + ", status="
				+ status + ", client=" + client + ", project=" + project + ", reimbursedBy=" + reimbursedBy
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", approver=" + approver + ", financeLogin="
				+ financeLogin + ", rmLogin=" + rmLogin + "]";
	}
}
