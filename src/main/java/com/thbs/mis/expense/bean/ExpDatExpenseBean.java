package com.thbs.mis.expense.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.googlecode.jmapper.annotations.JMap;
import com.thbs.mis.expense.bo.ExpMasCategoryBO;
import com.thbs.mis.expense.bo.ExpMasStatusBO;
import com.thbs.mis.expense.bo.ExpMileageRatesBO;
import com.thbs.mis.framework.util.CustomDateSerializer;

public class ExpDatExpenseBean implements Serializable {

	private Integer pkExpenseId;
	private Integer fkReportId;
	private Double expenseAmount;
	private String merchant;
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date expenseDate;
	private String expenseDesc;
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date managerApprovalDate;
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date financeApprovalDate;
	private Integer currencyType;
	private Integer fkCategoryId;
	private Integer fkEmpId;
	private Integer fkBuId;
	private Integer fkLocOnsiteId;
	private Integer fkLocBaseId;
	private Short fkStatusId;
	private String receiptPath;
	private Double taxAmount;
	private Integer isTaxInclusive;
	private String categoryName;
	private Integer daysLeft;
	private Integer workorder;
	private String billlableToClient;
	private String reportName;
	private Double convertedAmountToPayroll;
	private Double finalExpAmount;
	private Short empOrg;
	private String fileName;
	private String pathName;
	private Integer fkMileageId;
	private Double miles;
	@JMap
	private ExpMasCategoryBO categoryBo;
	@JMap
	private String reportTitle;
	private List<Integer> listOfExpenseId;
	@JMap
	private ExpMasStatusBO expMasStatusBO;
	@JMap
	private ExpMileageRatesBO expMileageRatesBO;

	public ExpDatExpenseBean() {
	}

	public Integer getPkExpenseId() {
		return pkExpenseId;
	}

	public Integer getFkReportId() {
		return fkReportId;
	}

	public Double getExpenseAmount() {
		return expenseAmount;
	}

	public String getMerchant() {
		return merchant;
	}

	public Date getExpenseDate() {
		return expenseDate;
	}

	public String getExpenseDesc() {
		return expenseDesc;
	}

	public Date getManagerApprovalDate() {
		return managerApprovalDate;
	}

	public Date getFinanceApprovalDate() {
		return financeApprovalDate;
	}

	public Integer getCurrencyType() {
		return currencyType;
	}

	public Integer getFkCategoryId() {
		return fkCategoryId;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public Integer getFkBuId() {
		return fkBuId;
	}

	public Integer getFkLocOnsiteId() {
		return fkLocOnsiteId;
	}

	public Integer getFkLocBaseId() {
		return fkLocBaseId;
	}

	public Short getFkStatusId() {
		return fkStatusId;
	}

	public String getReceiptPath() {
		return receiptPath;
	}

	public void setPkExpenseId(Integer pkExpenseId) {
		this.pkExpenseId = pkExpenseId;
	}

	public void setFkReportId(Integer fkReportId) {
		this.fkReportId = fkReportId;
	}

	public void setExpenseAmount(Double expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public void setExpenseDate(Date expenseDate) {
		this.expenseDate = expenseDate;
	}

	public void setExpenseDesc(String expenseDesc) {
		this.expenseDesc = expenseDesc;
	}

	public void setManagerApprovalDate(Date managerApprovalDate) {
		this.managerApprovalDate = managerApprovalDate;
	}

	public void setFinanceApprovalDate(Date financeApprovalDate) {
		this.financeApprovalDate = financeApprovalDate;
	}

	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}

	public void setFkCategoryId(Integer fkCategoryId) {
		this.fkCategoryId = fkCategoryId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public void setFkBuId(Integer fkBuId) {
		this.fkBuId = fkBuId;
	}

	public void setFkLocOnsiteId(Integer fkLocOnsiteId) {
		this.fkLocOnsiteId = fkLocOnsiteId;
	}

	public void setFkLocBaseId(Integer fkLocBaseId) {
		this.fkLocBaseId = fkLocBaseId;
	}

	public void setFkStatusId(Short fkStatusId) {
		this.fkStatusId = fkStatusId;
	}

	public void setReceiptPath(String receiptPath) {
		this.receiptPath = receiptPath;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public Integer getIsTaxInclusive() {
		return isTaxInclusive;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public Integer getDaysLeft() {
		return daysLeft;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public void setIsTaxInclusive(Integer isTaxInclusive) {
		this.isTaxInclusive = isTaxInclusive;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public void setDaysLeft(Integer daysLeft) {
		this.daysLeft = daysLeft;
	}

	public Integer getWorkorder() {
		return workorder;
	}

	public void setWorkorder(Integer workorder) {
		this.workorder = workorder;
	}

	public String getBilllableToClient() {
		return billlableToClient;
	}

	public void setBilllableToClient(String billlableToClient) {
		this.billlableToClient = billlableToClient;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public Double getConvertedAmountToPayroll() {
		return convertedAmountToPayroll;
	}

	public void setConvertedAmountToPayroll(Double convertedAmountToPayroll) {
		this.convertedAmountToPayroll = convertedAmountToPayroll;
	}

	public Short getEmpOrg() {
		return empOrg;
	}

	public void setEmpOrg(Short empOrg) {
		this.empOrg = empOrg;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public ExpMasCategoryBO getCategoryBo() {
		return categoryBo;
	}

	public void setCategoryBo(ExpMasCategoryBO categoryBo) {
		this.categoryBo = categoryBo;
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}

	public List<Integer> getListOfExpenseId() {
		return listOfExpenseId;
	}

	public void setListOfExpenseId(List<Integer> listOfExpenseId) {
		this.listOfExpenseId = listOfExpenseId;
	}

	public String getPathName() {
		return pathName;
	}

	public void setPathName(String pathName) {
		this.pathName = pathName;
	}

	public Double getFinalExpAmount() {
		return finalExpAmount;
	}

	public void setFinalExpAmount(Double finalExpAmount) {
		this.finalExpAmount = finalExpAmount;
	}

	public ExpMasStatusBO getExpMasStatusBO() {
		return expMasStatusBO;
	}

	public void setExpMasStatusBO(ExpMasStatusBO expMasStatusBO) {
		this.expMasStatusBO = expMasStatusBO;
	}

	public Integer getFkMileageId() {
		return fkMileageId;
	}

	public void setFkMileageId(Integer fkMileageId) {
		this.fkMileageId = fkMileageId;
	}

	public ExpMileageRatesBO getExpMileageRatesBO() {
		return expMileageRatesBO;
	}

	public void setExpMileageRatesBO(ExpMileageRatesBO expMileageRatesBO) {
		this.expMileageRatesBO = expMileageRatesBO;
	}

	public Double getMiles() {
		return miles;
	}

	public void setMiles(Double miles) {
		this.miles = miles;
	}

	@Override
	public String toString() {
		return "ExpDatExpenseBean [pkExpenseId=" + pkExpenseId + ", fkReportId=" + fkReportId + ", expenseAmount="
				+ expenseAmount + ", merchant=" + merchant + ", expenseDate=" + expenseDate + ", expenseDesc="
				+ expenseDesc + ", managerApprovalDate=" + managerApprovalDate + ", financeApprovalDate="
				+ financeApprovalDate + ", currencyType=" + currencyType + ", fkCategoryId=" + fkCategoryId
				+ ", fkEmpId=" + fkEmpId + ", fkBuId=" + fkBuId + ", fkLocOnsiteId=" + fkLocOnsiteId + ", fkLocBaseId="
				+ fkLocBaseId + ", fkStatusId=" + fkStatusId + ", receiptPath=" + receiptPath + ", taxAmount="
				+ taxAmount + ", isTaxInclusive=" + isTaxInclusive + ", categoryName=" + categoryName + ", daysLeft="
				+ daysLeft + ", workorder=" + workorder + ", billlableToClient=" + billlableToClient + ", reportName="
				+ reportName + ", convertedAmountToPayroll=" + convertedAmountToPayroll + ", finalExpAmount="
				+ finalExpAmount + ", empOrg=" + empOrg + ", fileName=" + fileName + ", pathName=" + pathName
				+ ", fkMileageId=" + fkMileageId + ", miles=" + miles + ", categoryBo=" + categoryBo + ", reportTitle="
				+ reportTitle + ", listOfExpenseId=" + listOfExpenseId + ", expMasStatusBO=" + expMasStatusBO
				+ ", expMileageRatesBO=" + expMileageRatesBO + "]";
	}

}
