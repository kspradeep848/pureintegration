/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thbs.mis.expense.bean;

import java.io.Serializable;
import java.util.Collection;

import com.thbs.mis.expense.bo.ExpDatExpenseBO;

public class ExpMasStatusBean implements Serializable {
	private Short pkStatusId;
	private String statusDesc;
	private Collection<ExpDatExpenseBO> ExpDatExpenseBOCollection;

	public Short getPkStatusId() {
		return pkStatusId;
	}

	public void setPkStatusId(Short pkStatusId) {
		this.pkStatusId = pkStatusId;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public Collection<ExpDatExpenseBO> getExpDatExpenseBOCollection() {
		return ExpDatExpenseBOCollection;
	}

	public void setExpDatExpenseBOCollection(
			Collection<ExpDatExpenseBO> expDatExpenseBOCollection) {
		ExpDatExpenseBOCollection = expDatExpenseBOCollection;
	}

	@Override
	public String toString() {
		return "ExpMasStatusBean [pkStatusId=" + pkStatusId + ", statusDesc="
				+ statusDesc + ", ExpDatExpenseBOCollection="
				+ ExpDatExpenseBOCollection + "]";
	}

}