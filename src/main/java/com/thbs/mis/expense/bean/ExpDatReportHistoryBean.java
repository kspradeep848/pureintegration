package com.thbs.mis.expense.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.googlecode.jmapper.annotations.JMap;
import com.thbs.mis.expense.bo.ExpReportMasStatusBO;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class ExpDatReportHistoryBean {

	@JMap
	private Integer pkRecordHistoryId;
	@JMap
	private Integer fkReportId;
	@JMap
	private Integer fkEmpId;
	@JMap
	private Short fkStatusId;
	@JMap
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date recordDate;
	@JMap
	private Integer toEmpId;
	@JMap
	private ExpReportMasStatusBO expReportMasStatusBO;

	private String fkEmpIdName;
	private String toEmpIdName;
	private String statusName;

	public Integer getPkRecordHistoryId() {
		return pkRecordHistoryId;
	}

	public void setPkRecordHistoryId(Integer pkRecordHistoryId) {
		this.pkRecordHistoryId = pkRecordHistoryId;
	}

	public Integer getFkReportId() {
		return fkReportId;
	}

	public void setFkReportId(Integer fkReportId) {
		this.fkReportId = fkReportId;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public Short getFkStatusId() {
		return fkStatusId;
	}

	public void setFkStatusId(Short fkStatusId) {
		this.fkStatusId = fkStatusId;
	}

	public Date getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}

	public Integer getToEmpId() {
		return toEmpId;
	}

	public void setToEmpId(Integer toEmpId) {
		this.toEmpId = toEmpId;
	}

	public String getFkEmpIdName() {
		return fkEmpIdName;
	}

	public void setFkEmpIdName(String fkEmpIdName) {
		this.fkEmpIdName = fkEmpIdName;
	}

	public String getToEmpIdName() {
		return toEmpIdName;
	}

	public void setToEmpIdName(String toEmpIdName) {
		this.toEmpIdName = toEmpIdName;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public ExpReportMasStatusBO getExpReportMasStatusBO() {
		return expReportMasStatusBO;
	}

	public void setExpReportMasStatusBO(ExpReportMasStatusBO expReportMasStatusBO) {
		this.expReportMasStatusBO = expReportMasStatusBO;
	}

	@Override
	public String toString() {
		return "ExpDatReportHistoryBean [pkRecordHistoryId=" + pkRecordHistoryId + ", fkReportId=" + fkReportId
				+ ", fkEmpId=" + fkEmpId + ", fkStatusId=" + fkStatusId + ", recordDate=" + recordDate + ", toEmpId="
				+ toEmpId + ", expReportMasStatusBO=" + expReportMasStatusBO + ", fkEmpIdName=" + fkEmpIdName
				+ ", toEmpIdName=" + toEmpIdName + ", statusName=" + statusName + "]";
	}
}
