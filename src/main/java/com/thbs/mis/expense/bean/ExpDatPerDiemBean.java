/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thbs.mis.expense.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.googlecode.jmapper.annotations.JMap;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasCurrencytypeBO;
import com.thbs.mis.expense.bo.ExpMasPerDiemCountryRateBO;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class ExpDatPerDiemBean {

	@JMap
	private Integer pkId;
	@JMap
	private Integer fkEmpId;
	@JMap
	private Short fkCurrencyType;
	@JMap
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date createdDate;
	@JMap
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date perDiemStartDate;
	@JMap
	private Integer noOfDays;
	@JMap
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date finAppprovedDate;
	@JMap
	private Integer finApproverId;
	@JMap
	private Double perDiemRate;
	@JMap
	private Double amount;
	@JMap
	private String perDiemDesc;
	@JMap
	private Integer fkCountryId;
	@JMap
	private Short isWithAccomodation;
	@JMap
	private Short perDiemStatus;
	@JMap
	private String finComments;
	@JMap
	private DatEmpDetailBO datEmpDetailBO;
	@JMap
	private MasCurrencytypeBO masCurrencytypeBO;
	@JMap
	private ExpMasPerDiemCountryRateBO expMasPerDiemCountryRateBO;

	private String submitterName;
	private String finApproverName;
	private Date startDate;
	private Date endDate;

	public Integer getPkId() {
		return pkId;
	}

	public void setPkId(Integer pkId) {
		this.pkId = pkId;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public Short getFkCurrencyType() {
		return fkCurrencyType;
	}

	public void setFkCurrencyType(Short fkCurrencyType) {
		this.fkCurrencyType = fkCurrencyType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getPerDiemStartDate() {
		return perDiemStartDate;
	}

	public void setPerDiemStartDate(Date perDiemStartDate) {
		this.perDiemStartDate = perDiemStartDate;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Date getFinAppprovedDate() {
		return finAppprovedDate;
	}

	public void setFinAppprovedDate(Date finAppprovedDate) {
		this.finAppprovedDate = finAppprovedDate;
	}

	public Integer getFinApproverId() {
		return finApproverId;
	}

	public void setFinApproverId(Integer finApproverId) {
		this.finApproverId = finApproverId;
	}

	public Double getPerDiemRate() {
		return perDiemRate;
	}

	public void setPerDiemRate(Double perDiemRate) {
		this.perDiemRate = perDiemRate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPerDiemDesc() {
		return perDiemDesc;
	}

	public void setPerDiemDesc(String perDiemDesc) {
		this.perDiemDesc = perDiemDesc;
	}

	public Integer getFkCountryId() {
		return fkCountryId;
	}

	public void setFkCountryId(Integer fkCountryId) {
		this.fkCountryId = fkCountryId;
	}

	public Short getIsWithAccomodation() {
		return isWithAccomodation;
	}

	public void setIsWithAccomodation(Short isWithAccomodation) {
		this.isWithAccomodation = isWithAccomodation;
	}

	public Short getPerDiemStatus() {
		return perDiemStatus;
	}

	public void setPerDiemStatus(Short perDiemStatus) {
		this.perDiemStatus = perDiemStatus;
	}

	public String getFinComments() {
		return finComments;
	}

	public void setFinComments(String finComments) {
		this.finComments = finComments;
	}

	public DatEmpDetailBO getDatEmpDetailBO() {
		return datEmpDetailBO;
	}

	public void setDatEmpDetailBO(DatEmpDetailBO datEmpDetailBO) {
		this.datEmpDetailBO = datEmpDetailBO;
	}

	public MasCurrencytypeBO getMasCurrencytypeBO() {
		return masCurrencytypeBO;
	}

	public void setMasCurrencytypeBO(MasCurrencytypeBO masCurrencytypeBO) {
		this.masCurrencytypeBO = masCurrencytypeBO;
	}

	public ExpMasPerDiemCountryRateBO getExpMasPerDiemCountryRateBO() {
		return expMasPerDiemCountryRateBO;
	}

	public void setExpMasPerDiemCountryRateBO(ExpMasPerDiemCountryRateBO expMasPerDiemCountryRateBO) {
		this.expMasPerDiemCountryRateBO = expMasPerDiemCountryRateBO;
	}

	public String getSubmitterName() {
		return submitterName;
	}

	public void setSubmitterName(String submitterName) {
		this.submitterName = submitterName;
	}

	public String getFinApproverName() {
		return finApproverName;
	}

	public void setFinApproverName(String finApproverName) {
		this.finApproverName = finApproverName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "ExpDatPerDiemBean [pkId=" + pkId + ", fkEmpId=" + fkEmpId + ", fkCurrencyType=" + fkCurrencyType
				+ ", createdDate=" + createdDate + ", perDiemStartDate=" + perDiemStartDate + ", noOfDays=" + noOfDays
				+ ", finAppprovedDate=" + finAppprovedDate + ", finApproverId=" + finApproverId + ", perDiemRate="
				+ perDiemRate + ", amount=" + amount + ", perDiemDesc=" + perDiemDesc + ", fkCountryId=" + fkCountryId
				+ ", isWithAccomodation=" + isWithAccomodation + ", perDiemStatus=" + perDiemStatus + ", finComments="
				+ finComments + ", datEmpDetailBO=" + datEmpDetailBO + ", masCurrencytypeBO=" + masCurrencytypeBO
				+ ", expMasPerDiemCountryRateBO=" + expMasPerDiemCountryRateBO + ", submitterName=" + submitterName
				+ ", finApproverName=" + finApproverName + ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}

}