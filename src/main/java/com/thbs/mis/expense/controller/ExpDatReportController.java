package com.thbs.mis.expense.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.expense.bean.ExpDatReportBean;
import com.thbs.mis.expense.bean.ExpDatReportHistoryBean;
import com.thbs.mis.expense.bean.ExpenseAdvanceSearchBean;
import com.thbs.mis.expense.bo.ExpDatRecordHistoryBO;
import com.thbs.mis.expense.bo.ExpDatReportBO;
import com.thbs.mis.expense.bo.ExpReportMasStatusBO;
import com.thbs.mis.expense.constants.ExpenseURIConstants;
import com.thbs.mis.expense.service.ExpDatReportService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

@Controller
public class ExpDatReportController {

	@Autowired
	ExpDatReportService expDatReportService;

	List<ExpDatReportBO> expDatReportBOList;

	List<ExpDatReportBean> expDatReportBeanList;

	private static final AppLog LOG = LogFactory.getLog(ExpDatReportController.class);

	// create report
	@RequestMapping(value = ExpenseURIConstants.EXP_CREATE_REPORT, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> createReport(@RequestBody ExpDatReportBean inputBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("createReport Controller");
		expDatReportBOList = expDatReportService.createReport(inputBean);
		if (expDatReportBOList.size() > 0) {
			LOG.endUsecase("createReport Controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Report Created Successfully", expDatReportBOList));
		} else {
			LOG.endUsecase("createReport Controller");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Something went wrong, please contact Nucleus team"));
		}
	}

	// view Report by EmpId, RMId, FinMgrId, PkReportId and daterange
	@RequestMapping(value = ExpenseURIConstants.EXP_FIND_REPORT, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewReportByPkIdAndDateRange(
			@RequestBody ExpDatReportBean inputBean) throws CommonCustomException {
		LOG.startUsecase("viewReportByPkIdAndDateRange Controller");
		expDatReportBeanList = expDatReportService.viewReportByPKIdandDaterange(inputBean);
		if (expDatReportBeanList.size() > 0) {
			LOG.endUsecase("viewReportByPkIdAndDateRange Controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Report are Generated", expDatReportBeanList));
		} else {
			LOG.endUsecase("viewReportByPkIdAndDateRange Controller");
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No Records", new ArrayList()));
		}
	}

	// view Report Details By PkReportId
	@RequestMapping(value = ExpenseURIConstants.EXP_REPORT_DETAILS_BY_PKREPORTID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> viewReportDetailsByPkId(@PathVariable Integer pkReportId)
			throws CommonCustomException {
		expDatReportBeanList = expDatReportService.viewReportDetailsByPkId(pkReportId);
		if (expDatReportBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Report Details are ", expDatReportBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Some Problem occurred Please contact Nucleus Team", new ArrayList()));
		}
	}

	// Approve and Reject Report by RM
	@RequestMapping(value = ExpenseURIConstants.EXP_REPORT_APPROVE_REJECT_BY_RM, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> approveOrReject(@RequestBody ExpDatReportBean inputBean)
			throws CommonCustomException {
		expDatReportBeanList = expDatReportService.approveOrReject(inputBean);
		if (expDatReportBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Report Details are ", expDatReportBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some Problem occurred Please contact Nucleus Team", new ArrayList()));
		}
	}

	// Approve and Reject Report by Fin
	@RequestMapping(value = ExpenseURIConstants.EXP_REPORT_APPROVE_REJECT_BY_FIN, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> approveOrRejectbyFin(@RequestBody ExpDatReportBean inputBean)
			throws CommonCustomException {
		expDatReportBeanList = expDatReportService.approveOrRejectbyFin(inputBean);
		if (expDatReportBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Report Details are ", expDatReportBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some Problem occurred Please contact Nucleus Team", new ArrayList()));
		}
	}

	// Close Reimbursement and Cancel Reimbursement Report by Fin
	@RequestMapping(value = ExpenseURIConstants.EXP_REPORT_REIMBURSED_CANCEL_BY_FIN, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> closeAndCancelReimbursement(
			@RequestBody ExpDatReportBean inputBean) throws CommonCustomException {
		expDatReportBeanList = expDatReportService.closeAndCancelReimbursement(inputBean);
		if (expDatReportBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Report Details are ", expDatReportBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some Problem occurred Please contact Nucleus Team", new ArrayList()));
		}
	}

	// Report Status
	@RequestMapping(value = ExpenseURIConstants.EXP_REPORT_STATUS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllReportStatus() throws CommonCustomException {
		List<ExpReportMasStatusBO> expReportMasStatusBO = expDatReportService.getAllReportStatus();
		if (expReportMasStatusBO.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Report Status are ", expReportMasStatusBO));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some Problem occurred Please contact Nucleus Team", new ArrayList()));
		}
	}

	// RM forward to another RM
	@RequestMapping(value = ExpenseURIConstants.EXP_FORWARD_TO_RM, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> forwardToRm(@RequestBody ExpDatReportBean inputBean)
			throws CommonCustomException {
		expDatReportBeanList = expDatReportService.forwardToRm(inputBean);
		if (expDatReportBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Report Forwarded", expDatReportBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Report is not forwared, Please contact Nucleus team", new ArrayList()));
		}
	}

	// Emp recall report
	@RequestMapping(value = ExpenseURIConstants.EXP_RECALL_REPORT, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> empRecallReport(@RequestBody ExpDatReportBean inputBean)
			throws CommonCustomException {
		expDatReportBeanList = expDatReportService.empRecallReport(inputBean);
		if (expDatReportBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Report is recalled", expDatReportBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Error occured, please contact nucleus team", new ArrayList()));
		}
	}

	// Advance search
	@RequestMapping(value = ExpenseURIConstants.EXP_FIN_ADVANCE_SEARCH, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> finAdvanceSearch(@RequestBody ExpenseAdvanceSearchBean inputBean)
			throws CommonCustomException {
		expDatReportBeanList = expDatReportService.finAdvanceSearch(inputBean);
		if (expDatReportBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Advance Search Report data", expDatReportBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No records", new ArrayList()));
		}
	}

	// List of all reports Names
	@RequestMapping(value = ExpenseURIConstants.EXP_LIST_OF_REPORT_NAME, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> listOfAllReportName(@RequestBody ExpDatReportBean inputBean)
			throws CommonCustomException {
		List<String> reportNames = expDatReportService.listOfAllReportName(inputBean);
		if (reportNames.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Report Titles", reportNames));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No records", new ArrayList()));
		}
	}

	// Report History
	@RequestMapping(value = ExpenseURIConstants.EXP_REPORT_HISTORY, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getReportHistory(@PathVariable Integer fkReportId)
			throws CommonCustomException, DataAccessException {
		List<ExpDatReportHistoryBean> expDatReportBOList = expDatReportService.getReportHistory(fkReportId);
		if (expDatReportBOList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Report History", expDatReportBOList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Something went wrong, please contact Nucleus team"));
		}
	}

	// Report Filter for RM and Finance
	@RequestMapping(value = ExpenseURIConstants.EXP_FILTER_RM_FINANCE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> rmAndFinancefilter(@RequestBody ExpenseAdvanceSearchBean inputBean)
			throws CommonCustomException {
		expDatReportBeanList = expDatReportService.rmAndFinancefilter(inputBean);
		if (expDatReportBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Data retrived", expDatReportBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No records", new ArrayList()));
		}
	}

}