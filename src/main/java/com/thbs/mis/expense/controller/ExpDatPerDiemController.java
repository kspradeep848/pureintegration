package com.thbs.mis.expense.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.expense.bean.ExpDatPerDiemBean;
import com.thbs.mis.expense.bo.ExpDatPerDiemBO;
import com.thbs.mis.expense.bo.ExpMasPerDiemCountryRateBO;
import com.thbs.mis.expense.constants.ExpenseURIConstants;
import com.thbs.mis.expense.service.ExpDatPerDiemService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

@Controller
public class ExpDatPerDiemController {

	List<ExpDatPerDiemBean> expDatPerDiemBeanList;

	List<ExpDatPerDiemBO> expDatPerDiemBOList;

	@Autowired
	ExpDatPerDiemService expDatPerDiemService;

	private static final AppLog LOG = LogFactory.getLog(ExpDatPerDiemController.class);

	// Finance View Per-Diem
	@RequestMapping(value = ExpenseURIConstants.EXP_FIN_PERDIEM, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewAllExpensePerdiemList(@RequestBody ExpDatPerDiemBean inputBean)
			throws CommonCustomException, DataAccessException {
		expDatPerDiemBeanList = expDatPerDiemService.viewAllExpensePerdiem(inputBean);
		if (expDatPerDiemBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Perdiem deatils as below", expDatPerDiemBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Perdiem details not found", new ArrayList()));
		}
	}

	// Based on Pk id
	@RequestMapping(value = ExpenseURIConstants.EXP_PERDIEM_DETAILS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> viewDetailsByPkId(@PathVariable Integer pkId)
			throws CommonCustomException, DataAccessException {
		expDatPerDiemBeanList = expDatPerDiemService.viewDetailsByPkId(pkId);
		if (expDatPerDiemBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Perdiem deatils as below", expDatPerDiemBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Perdiem details not found", new ArrayList()));
		}
	}

	// Emp View per-Diem
	@RequestMapping(value = ExpenseURIConstants.EXP_FIND_PERDIEMS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewExpensePerdiemList(@RequestBody ExpDatPerDiemBean inputBean)
			throws CommonCustomException, DataAccessException {
		expDatPerDiemBeanList = expDatPerDiemService.viewExpensePerdiem(inputBean);
		if (expDatPerDiemBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Perdiem deatils as below", expDatPerDiemBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Perdiem details not found", new ArrayList()));
		}
	}

	// create per-diem
	@RequestMapping(value = ExpenseURIConstants.EXP_CREATE_PERDIEM, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> createExpensePerdiem(@RequestBody ExpDatPerDiemBean inputBean)
			throws CommonCustomException {
		expDatPerDiemBOList = expDatPerDiemService.createExpensePerDiem(inputBean);
		if (expDatPerDiemBOList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Created Successfully", expDatPerDiemBOList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(new MISResponse(
					HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "Error Occured while creating"));
		}
	}

	// Fin approve/Reject
	@RequestMapping(value = ExpenseURIConstants.EXP_APR_REJ_FIN_PER_DIEM, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> approveOrRejPerdiem(@RequestBody ExpDatPerDiemBean inputBean)
			throws CommonCustomException, DataAccessException {
		expDatPerDiemBeanList = expDatPerDiemService.approveOrRejPerdiem(inputBean);
		if (expDatPerDiemBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Perdiem Action is done", expDatPerDiemBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "Error Occured during Action"));
		}
	}

	// Fetch all country with rate
	@RequestMapping(value = ExpenseURIConstants.EXP_PER_DIEM_COUNTRY, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> perDiemRate() throws CommonCustomException, DataAccessException {
		List<ExpMasPerDiemCountryRateBO> expDatPerDiemBeanList = expDatPerDiemService.perDiemRate();
		if (expDatPerDiemBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Perdiem Rates", expDatPerDiemBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "Error Occured during Action"));
		}
	}

}
