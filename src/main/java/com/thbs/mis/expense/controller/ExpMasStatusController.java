package com.thbs.mis.expense.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.expense.bean.ExpMasStatusBean;
import com.thbs.mis.expense.bo.ExpMasStatusBO;
import com.thbs.mis.expense.constants.ExpenseURIConstants;
import com.thbs.mis.expense.service.ExpMasStatusService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

@Controller
public class ExpMasStatusController {

	@Autowired
	ExpMasStatusService expMasStatusService;

	List<ExpMasStatusBO> expMasStatusBOList;

	List<ExpMasStatusBean> expMasStatusBeanList;

	private static final AppLog LOG = LogFactory
			.getLog(ExpMasStatusController.class);

	@RequestMapping(value = ExpenseURIConstants.EXP_GET_ALL_EXPENSE_STATUS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> allStatus()
			throws CommonCustomException {
		LOG.startUsecase("allStatus");
		expMasStatusBOList = expMasStatusService.allStatus();
		if (expMasStatusBOList.size() > 0) {
			LOG.endUsecase("allStatus");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "All Status",
							expMasStatusBOList));
		} else {
			LOG.endUsecase("allStatus");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE, "failed"));
		}
	}

}