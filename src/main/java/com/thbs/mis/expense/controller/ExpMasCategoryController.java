package com.thbs.mis.expense.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.expense.bean.ExpMasCategoryBean;
import com.thbs.mis.expense.bo.ExpMasCategoryBO;
import com.thbs.mis.expense.constants.ExpenseMessageConstant;
import com.thbs.mis.expense.constants.ExpenseURIConstants;
import com.thbs.mis.expense.service.ExpMasCategoryService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

@Controller
public class ExpMasCategoryController {

	private static final AppLog LOG = LogFactory.getLog(ExpMasCategoryController.class);

	@Autowired
	ExpMasCategoryService categoryService;

	List<ExpMasCategoryBO> categoryBoList;

	@RequestMapping(value = ExpenseURIConstants.EXP_GET_CATEGORY, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getExpCategory() throws CommonCustomException {
		categoryBoList = categoryService.getExpCategory();
		if (categoryBoList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, ExpenseMessageConstant.EXP_CATEGORY_FIND_SUCCESS, categoryBoList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							ExpenseMessageConstant.EXP_CATEGORY_FIND_FAILED, new ArrayList()));
		}

	}

	@RequestMapping(value = ExpenseURIConstants.EXP_FIND_CATEGORY, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> findByExpenseCategory(@RequestBody ExpMasCategoryBean inputBean)
			throws CommonCustomException {
		categoryBoList = categoryService.findByExpenseCategory(inputBean);
		if (categoryBoList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, ExpenseMessageConstant.EXP_CATEGORY_FIND_SUCCESS, categoryBoList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, ExpenseMessageConstant.EXP_CATEGORY_FIND_FAILED, new ArrayList()));
		}

	}

	@RequestMapping(value = ExpenseURIConstants.EXP_CREATE_UPDATE_CATEGORY, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> updateExpenseCategory(@RequestBody ExpMasCategoryBean inputBean)
			throws CommonCustomException {
		categoryBoList = categoryService.updateCategory(inputBean);
		if (categoryBoList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, ExpenseMessageConstant.EXP_CATEGORY_SAVED_SUCCESS, categoryBoList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.NO_CONTENT.value(),
					MISConstants.FAILURE, ExpenseMessageConstant.EXP_CATEGORY_SAVED_SUCCESS, new ArrayList()));
		}
	}
}