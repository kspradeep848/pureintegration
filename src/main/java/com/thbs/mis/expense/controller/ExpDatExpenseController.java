package com.thbs.mis.expense.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thbs.mis.expense.bean.ExpDatExpenseBean;
import com.thbs.mis.expense.bo.ExpDatExpenseBO;
import com.thbs.mis.expense.bo.ExpMasCategoryBO;
import com.thbs.mis.expense.bo.ExpMileageRatesBO;
import com.thbs.mis.expense.constants.ExpenseURIConstants;
import com.thbs.mis.expense.dao.ExpMasCategoryRepository;
import com.thbs.mis.expense.service.ExpDatExpenseService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

@Controller
public class ExpDatExpenseController {

	private static final AppLog LOG = LogFactory.getLog(ExpDatExpenseController.class);

	List<ExpDatExpenseBean> expDatExpenseBeanList;

	List<ExpDatExpenseBO> expDatExpenseBOList;
	ExpDatExpenseBO expDatExpenseBO = new ExpDatExpenseBO();

	@Autowired
	ExpDatExpenseService expDatExpenseService;
	
	@Autowired
	ExpMasCategoryRepository expMasCategoryRepository;

	ObjectMapper objectMapper = new ObjectMapper();

	@RequestMapping(value = ExpenseURIConstants.EXP_CREATE_UPDATE_EXPENSE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> createOrUpdateExpense(
			@RequestParam(value = "expenseJson", name = "expenseJson", required = true, defaultValue = "") String expenseJson,
			@RequestParam(value = "receiptFile", name = "receiptFile", required = false) MultipartFile receiptFile)
			throws CommonCustomException, IOException {
		try {
			expDatExpenseBO = objectMapper.readValue(expenseJson, ExpDatExpenseBO.class);
		} catch (Exception e) {
			throw new CommonCustomException("Unable to parse expense json parse.");
		}
		if (expDatExpenseBO.getPkExpenseId() == null) {
			ExpMasCategoryBO bo = expMasCategoryRepository.findByPkCategoryId(expDatExpenseBO.getFkCategoryId());
			if(bo.getFileUploadMandatory().equalsIgnoreCase("YES"))
			if (receiptFile == null) {
				throw new CommonCustomException("File has to be uploaded");
			}
		}
		if (expDatExpenseBO != null) {
			expDatExpenseBO = expDatExpenseService.createOrUpdateExpense(expDatExpenseBO, receiptFile);
			List output = new ArrayList();
			output.add(expDatExpenseBO);
			if (expDatExpenseBO != null) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS, "CREATED SUCCESSFULLY AND SAVED RECEIPT", output));
			}
		}
		return ResponseEntity.status(HttpStatus.OK.value())
				.body(new MISResponse(HttpStatus.OK.value(), MISConstants.FAILURE, "UNABLE CREATE EXPENSE"));
	}

	@RequestMapping(value = ExpenseURIConstants.EXP_EMP_FIND_EXPENSE, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getEmployeeExpenseById(@PathVariable("empId") Integer empId)
			throws IOException {
		expDatExpenseBeanList = expDatExpenseService.getEmployeeExpenseById(empId);
		if (expDatExpenseBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Expense details is as below", expDatExpenseBeanList));
		}
		return ResponseEntity.status(HttpStatus.OK.value())
				.body(new MISResponse(HttpStatus.OK.value(), MISConstants.FAILURE, "Expense details not available"));

	}

	@RequestMapping(value = ExpenseURIConstants.EXP_RM_WORK_ORDER_APPROVE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> rmWorkOrderSelection(@RequestBody ExpDatExpenseBean inputBean)
			throws CommonCustomException, IOException {
		expDatExpenseBO = expDatExpenseService.rmWorkOrderSelection(inputBean);
		if (expDatExpenseBO != null) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Work Order Added"));
		} else
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "Unable to add work order"));
	}

	@RequestMapping(value = ExpenseURIConstants.EXP_EMP_DELETE_FILE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> deleteReceipt(@RequestBody ExpDatExpenseBean inputBean)
			throws CommonCustomException, IOException {
		Integer expDatExpenseBO = expDatExpenseService.deleteReceipt(inputBean);
		if (expDatExpenseBO != null && expDatExpenseBO > 0) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Receipt Deleted"));
		} else
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(new MISResponse(
					HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "Unable to Delete the Receipt"));
	}

	@RequestMapping(value = ExpenseURIConstants.EXP_DOWNLOAD_IMAGE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> downloadImage(@RequestBody ExpDatExpenseBean inputBean,
			HttpServletResponse response) throws CommonCustomException, IOException {
		Boolean download = expDatExpenseService.getImageDownload(inputBean, response);
		if (download) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Receipt Downloaded"));
		} else
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(new MISResponse(
					HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "Unable to Download the Receipt"));
	}

	@RequestMapping(value = ExpenseURIConstants.EXP_UPDATE_EXPENSE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> updateExpense(@RequestBody ExpDatExpenseBean inputBean)
			throws CommonCustomException, IOException {
		this.expDatExpenseBOList = new ArrayList<ExpDatExpenseBO>();
		ExpDatExpenseBO download = expDatExpenseService.updateExpense(inputBean);
		expDatExpenseBOList.add(download);
		if (expDatExpenseBOList != null && expDatExpenseBOList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Receipt Updated", expDatExpenseBOList));
		} else
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(new MISResponse(
					HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "Unable to Update the Receipt"));
	}

	@RequestMapping(value = ExpenseURIConstants.EXP_MILEAGE_RATES, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllMileageRates()
			throws IOException {
		List<ExpMileageRatesBO> expDatExpenseBeanList = expDatExpenseService.getAllMileageRates();
		if (expDatExpenseBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Expense details is as below", expDatExpenseBeanList));
		}
		return ResponseEntity.status(HttpStatus.OK.value())
				.body(new MISResponse(HttpStatus.OK.value(), MISConstants.FAILURE, "Expense details not available"));
	}
	
}