package com.thbs.mis.expense.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.expense.bo.ExpDatPerDiemBO;

@Repository
public interface ExpDatPerDiemRepository
		extends GenericRepository<ExpDatPerDiemBO, Integer>, JpaSpecificationExecutor<ExpDatPerDiemBO> {

	public ExpDatPerDiemBO findByPkId(Integer pkId);

	public List<ExpDatPerDiemBO> findByFkEmpId(Integer fkEmpId);

	@Query("SELECT rep " + " FROM ExpDatPerDiemBO rep " + " WHERE rep.createdDate BETWEEN :startDate AND :endDate "
			+ " ORDER BY rep.pkId DESC")
	public List<ExpDatPerDiemBO> findAllRecordsByDate(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);

	@Transactional
	@Modifying
	@Query("UPDATE ExpDatPerDiemBO rep " + " SET rep.perDiemStatus = :perDiemStatus, "
			+ " rep.finApproverId = :finApproverId , " + " rep.finComments = :finComments , "
			+ " rep.finAppprovedDate = now() " + " WHERE rep.pkId = :pkId ")
	public Integer changePerDiemStatus(@Param("pkId") Integer pkId, @Param("finApproverId") Integer finApproverId,
			@Param("perDiemStatus") Short perDiemStatus, @Param("finComments") String finComments);

	@Query("SELECT rep " + " FROM ExpDatPerDiemBO rep " + " WHERE rep.pkId = :pkId ")
	public ExpDatPerDiemBO fetchBypkId(@Param("pkId") Integer pkId);
}