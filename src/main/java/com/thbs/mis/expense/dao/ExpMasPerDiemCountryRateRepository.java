package com.thbs.mis.expense.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.expense.bo.ExpMasPerDiemCountryRateBO;

@Repository
public interface ExpMasPerDiemCountryRateRepository extends GenericRepository<ExpMasPerDiemCountryRateBO, Integer> {

	public List<ExpMasPerDiemCountryRateBO> findAllByOrderByPerDiemCountryAsc();

}
