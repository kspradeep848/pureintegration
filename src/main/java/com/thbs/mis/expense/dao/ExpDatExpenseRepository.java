package com.thbs.mis.expense.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.expense.bo.ExpDatExpenseBO;

@Repository
public interface ExpDatExpenseRepository extends GenericRepository<ExpDatExpenseBO, Integer> {

	public ExpDatExpenseBO findByPkExpenseId(Integer pkExpenseId);

	public List<ExpDatExpenseBO> findByPkExpenseIdIn(List<Integer> pkExpenseId);

	public List<ExpDatExpenseBO> findByFkReportIdIn(List<Integer> fkReportId);

	public List<ExpDatExpenseBO> findByFkEmpIdInAndPkExpenseIdIn(List<Integer> empId, List<Integer> pkExpenseId);

	public List<ExpDatExpenseBO> findByFkEmpIdInAndFkReportIdIn(List<Integer> empId, List<Integer> fkReportId);

	public List<ExpDatExpenseBO> findByFkEmpIdIn(List<Integer> empId);

	// Added by Rajesh for Report
	@Transactional
	@Modifying
	@Query(" UPDATE ExpDatExpenseBO exp " + " SET exp.fkStatusId = :fkStatusId, exp.fkReportId = null, exp.workorder = null, exp.billlableToClient = null  "
			+ " WHERE exp.fkStatusId NOT in (3,4,5,6,7,8)  AND exp.fkReportId = :fkReportId ")
	public Integer unCheckAllExpense(@Param("fkReportId") Integer fkReportId, @Param("fkStatusId") Short fkStatusId);

	@Transactional
	@Modifying
	@Query(" UPDATE ExpDatExpenseBO exp " + " SET exp.fkStatusId = :fkStatusId "
			+ " WHERE exp.fkStatusId =2  AND exp.fkReportId = :fkReportId ")
	public Integer approveOrRejectExpense(@Param("fkReportId") Integer fkReportId,
			@Param("fkStatusId") Short fkStatusId);

	@Transactional
	@Modifying
	@Query(" UPDATE ExpDatExpenseBO exp " + " SET exp.fkStatusId = :fkStatusId "
			+ " WHERE exp.fkReportId = :fkReportId ")
	public Integer unCheckExpenseOnRecall(@Param("fkReportId") Integer fkReportId,
			@Param("fkStatusId") Short fkStatusId);

	@Transactional
	@Modifying
	@Query(" UPDATE ExpDatExpenseBO exp " + " SET exp.fkStatusId = :fkStatusId "
			+ " WHERE exp.fkStatusId in (2,4)  AND exp.fkReportId = :fkReportId ")
	public Integer rejectExpenseRm(@Param("fkReportId") Integer fkReportId, @Param("fkStatusId") Short fkStatusId);

	@Transactional
	@Modifying
	@Query(" UPDATE ExpDatExpenseBO exp " + " SET exp.fkStatusId = :fkStatusId "
			+ " WHERE exp.fkStatusId =4  AND exp.fkReportId = :fkReportId ")
	public Integer approveOrRejectExpenseFinance(@Param("fkReportId") Integer fkReportId,
			@Param("fkStatusId") Short fkStatusId);

	@Transactional
	@Modifying
	@Query(" UPDATE ExpDatExpenseBO exp " + " SET exp.fkStatusId = :fkStatusId "
			+ " WHERE exp.fkStatusId =6  AND exp.fkReportId = :fkReportId ")
	public Integer cancelExpenseFinance(@Param("fkReportId") Integer fkReportId, @Param("fkStatusId") Short fkStatusId);

	@Transactional
	@Modifying
	@Query(" UPDATE ExpDatExpenseBO exp " + " SET exp.fkStatusId = :fkStatusId "
			+ " WHERE exp.pkExpenseId IN :pkExpenseId ")
	public Integer approveOrRejectExpensePartially(@Param("pkExpenseId") List<Integer> pkExpenseId,
			@Param("fkStatusId") Short fkStatusId);

	@Transactional
	@Modifying
	@Query(" UPDATE ExpDatExpenseBO exp SET exp.fkStatusId = :fkStatusId, exp.fkReportId = :fkReportId "
			+ " WHERE exp.pkExpenseId IN :pkExpenseId ")
	public Integer attachExpenseInReport(@Param("pkExpenseId") List<Integer> pkExpenseId,
			@Param("fkStatusId") Short fkStatusId, @Param("fkReportId") Integer fkReportId);

	@Query("SELECT SUM(rep.convertedAmountToPayroll) FROM ExpDatExpenseBO rep "
			+ " WHERE rep.pkExpenseId in :pkExpenseId ")
	public Double fetchByTotalAmount(@Param("pkExpenseId") List<Integer> pkExpenseId);

	// EOA by Rajesh
	@Query("SELECT rep FROM ExpDatExpenseBO rep " + " WHERE rep.fkStatusId NOT IN (8) AND rep.fkEmpId = :empId ")
	public List<ExpDatExpenseBO> findAllEmpId(@Param("empId") Integer empId);

	@Transactional
	@Modifying
	@Query(" UPDATE ExpDatExpenseBO exp " + " SET exp.fkStatusId = :fkStatusId "
			+ " WHERE exp.pkExpenseId IN :pkExpenseId ")
	public Integer deleteExpense(@Param("pkExpenseId") Integer pkExpenseId, @Param("fkStatusId") Short fkStatusId);
	
	@Transactional
	@Modifying
	@Query(" UPDATE ExpDatExpenseBO exp " + " SET exp.billlableToClient = null, exp.workorder = null "
			+ " WHERE exp.fkReportId = :fkReportId ")
	public Integer forwardExpenseRm(@Param("fkReportId") Integer fkReportId);
}