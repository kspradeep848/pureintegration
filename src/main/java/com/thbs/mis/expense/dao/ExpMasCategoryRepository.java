package com.thbs.mis.expense.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.expense.bo.ExpMasCategoryBO;

@Repository
public interface ExpMasCategoryRepository extends
		GenericRepository<ExpMasCategoryBO, Integer> {
	
	public List<ExpMasCategoryBO> findAll();

	public ExpMasCategoryBO findByPkCategoryId(Integer pkCategoryId);

	@Query("Select c from ExpMasCategoryBO c where c.categoryName LIKE  %?1%")
	public List<ExpMasCategoryBO> findByCategoryNameContaining(
			String categoryName);

	@Query("Select c from ExpMasCategoryBO c where c.categoryShortName LIKE  %?1%")
	public List<ExpMasCategoryBO> findByCategoryShortNameContaining(
			String categoryShortName);
}