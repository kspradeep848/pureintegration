package com.thbs.mis.expense.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.expense.bean.ExpenseAdvanceSearchBean;
import com.thbs.mis.expense.bo.ExpDatReportBO;

public class ExpenseAdvanceSearchSpecification {
	public static Specification<ExpDatReportBO> getAllDetailsForFin(ExpenseAdvanceSearchBean inputBean) {
		return new Specification<ExpDatReportBO>() {
			@Override
			public Predicate toPredicate(Root<ExpDatReportBO> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				final Collection<Predicate> predicates = new ArrayList<>();
				Date fromDate = new Date();
				Date toDate = new Date();
				if(inputBean.getStartDate()!= null &&inputBean.getEndDate()!= null) {
					SimpleDateFormat sdfWithSeconds = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					String stringFromDate = sdf.format(inputBean.getStartDate());
					String stringToDate = sdf.format(inputBean.getEndDate());
					stringFromDate = stringFromDate + " 00:00:00";
					stringToDate = stringToDate + " 23:59:59";
					try {
						fromDate = sdfWithSeconds.parse(stringFromDate);
						toDate = sdfWithSeconds.parse(stringToDate);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				if (inputBean.getApprover() != null) {
					final Predicate approver = cb.or(cb.equal(root.get("approverMgr"), inputBean.getApprover()),cb.equal(root.get("fkForwardRmId"), inputBean.getApprover()));
					predicates.add(approver);
				}
				if (inputBean.getClient() != null) {
					final Predicate client = cb.equal(root.get("fkClientId"), inputBean.getClient());
					predicates.add(client);
				}
				if (inputBean.getEndDate() != null && inputBean.getStartDate() != null) {
					final Predicate dateRangePredicate = cb.and(
							cb.greaterThanOrEqualTo(root.get("submissionDate"), fromDate),
							cb.lessThanOrEqualTo(root.get("submissionDate"), toDate));
					predicates.add(dateRangePredicate);
				}
				if (inputBean.getProject() != null) {
					final Predicate project = cb.equal(root.get("fkProjectId"), inputBean.getProject());
					predicates.add(project);
				}
				if (inputBean.getReimbursedBy() != null) {
					final Predicate reimburesedBy = cb.equal(root.get("fkFinReimburser"), inputBean.getReimbursedBy());
					predicates.add(reimburesedBy);
				}
				if (inputBean.getReportName() != null) {
					final Predicate reportName = cb.equal(root.get("reportTitle"), inputBean.getReportName());
					predicates.add(reportName);
				}
				if (inputBean.getStatus() != null) {
					final Predicate status = cb.equal(root.get("fkStatusId"), inputBean.getStatus());
					predicates.add(status);
				}
				if (inputBean.getSubmittedBy() != null) {
					final Predicate submittedBy = cb.equal(root.get("fkEmpIdInt"), inputBean.getSubmittedBy());
					predicates.add(submittedBy);
				}
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
	}

	
	public static Specification<ExpDatReportBO> filterRMandFinance(ExpenseAdvanceSearchBean inputBean) {
		return new Specification<ExpDatReportBO>() {
			@Override
			public Predicate toPredicate(Root<ExpDatReportBO> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				final Collection<Predicate> predicates = new ArrayList<>();
				Date fromDate = new Date();
				Date toDate = new Date();
				if(inputBean.getStartDate()!= null &&inputBean.getEndDate()!= null) {
					SimpleDateFormat sdfWithSeconds = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					String stringFromDate = sdf.format(inputBean.getStartDate());
					String stringToDate = sdf.format(inputBean.getEndDate());
					stringFromDate = stringFromDate + " 00:00:00";
					stringToDate = stringToDate + " 23:59:59";
					try {
						fromDate = sdfWithSeconds.parse(stringFromDate);
						toDate = sdfWithSeconds.parse(stringToDate);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				if (inputBean.getEndDate() != null && inputBean.getStartDate() != null) {
					final Predicate dateRangePredicate = cb.and(
							cb.greaterThanOrEqualTo(root.get("submissionDate"), fromDate),
							cb.lessThanOrEqualTo(root.get("submissionDate"), toDate));
					predicates.add(dateRangePredicate);
				}
				if (inputBean.getStatus() != null) {
					final Predicate status = cb.equal(root.get("fkStatusId"), inputBean.getStatus());
					predicates.add(status);
				}
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
	}
}
