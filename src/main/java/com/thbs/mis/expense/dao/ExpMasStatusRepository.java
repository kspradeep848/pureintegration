package com.thbs.mis.expense.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.expense.bo.ExpMasStatusBO;

@Repository
public interface ExpMasStatusRepository extends
		GenericRepository<ExpMasStatusBO, Integer> {
	
	@Query(" SELECT rep FROM ExpMasStatusBO rep ")
	public List<ExpMasStatusBO> fetchAllStatus();

	public ExpMasStatusBO findByPkStatusId(Short pkStatusId);
}