package com.thbs.mis.expense.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.expense.bo.ExpReportMasStatusBO;

@Repository
public interface ExpMasReportStatusRepository extends GenericRepository<ExpReportMasStatusBO, Integer> {

	List<ExpReportMasStatusBO> findAll();
}
