package com.thbs.mis.expense.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.expense.bo.ExpMileageRatesBO;

@Repository
public interface ExpMileageRatesRepository  extends
GenericRepository<ExpMileageRatesBO, Integer>{

	public List<ExpMileageRatesBO> findAll();
	
	public ExpMileageRatesBO findByPkMileageId(Integer pkMileageId);
	
}
