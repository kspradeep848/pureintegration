package com.thbs.mis.expense.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.expense.bo.ExpDatReportBO;

@Repository
public interface ExpDatReportRepository
		extends GenericRepository<ExpDatReportBO, Integer>, JpaSpecificationExecutor<ExpDatReportBO> {

	public ExpDatReportBO findByPkReportId(Integer pkReportId);

	@Query("SELECT rep " + " FROM ExpDatReportBO rep " + " WHERE rep.pkReportId in :pkReportId ")
	public List<ExpDatReportBO> fetchBypkIdList(@Param("pkReportId") List<Integer> pkReportId);

	@Query("SELECT rep " + " FROM ExpDatReportBO rep " + " WHERE rep.submissionDate BETWEEN :fromDate AND :toDate ")
	public List<ExpDatReportBO> fetchByDateRange(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	@Query("SELECT rep " + " FROM ExpDatReportBO rep " + " WHERE rep.pkReportId in :pkReportId AND "
			+ " rep.submissionDate BETWEEN :fromDate AND :toDate ")
	public List<ExpDatReportBO> fetchByPkReportIdAndDateRange(@Param("pkReportId") List<Integer> pkReportId,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	public List<ExpDatReportBO> findByFkEmpIdIntOrderByPkReportIdDesc(Integer fkEmpIdInt);

	@Query("SELECT rep " + " FROM ExpDatReportBO rep " + " WHERE rep.fkEmpIdInt = :fkEmpIdInt AND "
			+ " rep.submissionDate BETWEEN :fromDate AND :toDate " + " ORDER BY rep.pkReportId DESC ")
	public List<ExpDatReportBO> fetchByEmpIdAndDateRange(@Param("fkEmpIdInt") Integer fkEmpIdInt,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	@Query("SELECT rep " + " FROM ExpDatReportBO rep "
			+ " WHERE rep.fkStatusId in :rmStatus AND rep.approverMgr = :approverMgr "
			+ " OR rep.fkForwardRmId = :approverMgr AND " + " rep.submissionDate BETWEEN :fromDate AND :toDate "
			+ " ORDER BY rep.pkReportId DESC ")
	public List<ExpDatReportBO> fetchByRMIdAndDateRange(@Param("approverMgr") Integer approverMgr,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("rmStatus") List<Short> rmStatus);

	public List<ExpDatReportBO> findByApproverMgrOrderByPkReportIdDesc(Integer approverMgr);

	@Query("SELECT rep " + " FROM ExpDatReportBO rep " + " WHERE rep.approverFin = :approverFin AND "
			+ " rep.submissionDate BETWEEN :fromDate AND :toDate ")
	public List<ExpDatReportBO> fetchByFinIdAndDateRange(@Param("approverFin") Integer approverFin,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	public List<ExpDatReportBO> findByApproverFin(Integer approverFin);

	@Query("SELECT rep " + " FROM ExpDatReportBO rep " + " WHERE rep.approverMgr = :approverMgr AND "
			+ " rep.fkEmpIdInt = :fkEmpIdInt AND " + " rep.submissionDate BETWEEN :fromDate AND :toDate ")
	public List<ExpDatReportBO> fetchByEmpIdAndRMIdAndDateRange(@Param("fkEmpIdInt") Integer fkEmpIdInt,
			@Param("approverMgr") Integer approverMgr, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	public List<ExpDatReportBO> findByFkEmpIdIntAndApproverMgr(Integer fkEmpIdInt, Integer approverMgr);

	@Query("SELECT rep " + " FROM ExpDatReportBO rep " + " WHERE rep.approverFin = :approverFin AND "
			+ " rep.fkEmpIdInt = :fkEmpIdInt AND " + " rep.submissionDate BETWEEN :fromDate AND :toDate ")
	public List<ExpDatReportBO> fetchByFinIdAndEmpIdAndDateRange(@Param("fkEmpIdInt") Integer fkEmpIdInt,
			@Param("approverFin") Integer approverFin, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	public List<ExpDatReportBO> findByFkEmpIdIntAndApproverFin(Integer fkEmpIdInt, Integer approverFin);

	@Query("SELECT rep " + " FROM ExpDatReportBO rep " + " WHERE rep.approverMgr = :approverMgr AND "
			+ " rep.fkEmpIdInt = :fkEmpIdInt AND " + " rep.approverFin = :approverFin AND "
			+ " rep.submissionDate BETWEEN :fromDate AND :toDate ")
	public List<ExpDatReportBO> fetchByEmpIdAndRMIdAndFinIdAndDateRange(@Param("fkEmpIdInt") Integer fkEmpIdInt,
			@Param("approverMgr") Integer approverMgr, @Param("approverFin") Integer approverFin,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	public List<ExpDatReportBO> findByFkEmpIdIntAndApproverFinAndApproverMgr(Integer fkEmpIdInt, Integer approverFin,
			Integer approverMgr);

	@Query("SELECT SUM(expBO.convertedAmountToPayroll) FROM ExpDatReportBO rep "
			+ " JOIN ExpDatExpenseBO expBO ON expBO.fkReportId = rep.pkReportId "
			+ " WHERE expBO.fkReportId = :pkReportId ")
	public Double fetchByTotalAmount(@Param("pkReportId") Integer pkReportId);

	@Query("SELECT expBO.pkExpenseId FROM ExpDatExpenseBO expBO "
			+ " JOIN ExpDatReportBO rep ON expBO.fkReportId = rep.pkReportId "
			+ " WHERE expBO.fkReportId = :pkReportId ")
	public List<Integer> fetchByExpenseBoDetailsByReportId(@Param("pkReportId") Integer pkReportId);

	@Transactional
	@Modifying
	@Query("UPDATE ExpDatReportBO rep " + " SET rep.fkStatusId = :fkStatusId " + " WHERE rep.pkReportId = :pkReportId ")
	public ExpDatReportBO changeExpenseStatus(@Param("pkReportId") Integer pkReportId,
			@Param("fkStatusId") Byte fkStatusId);

	@Query("SELECT rep FROM ExpDatReportBO rep " + " WHERE rep.fkStatusId IN(:fkStatusId) "
	// + " AND rep.fkFinReimburser = :fkFinReimburser "
			+ " AND rep.submissionDate BETWEEN :fromDate AND :toDate " + " ORDER BY rep.pkReportId DESC")
	public List<ExpDatReportBO> fetchForFin(@Param("fkStatusId") List<Short> fkStatusId,
			// @Param("fkFinReimburser") Short fkFinReimburser,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	@Query("SELECT DISTINCT rep.reportTitle FROM ExpDatReportBO rep WHERE rep.fkStatusId in :listOfId ")
	public List<String> fetchAllFinReportName(@Param("listOfId") List<Short> listOfId);

	@Query("SELECT DISTINCT rep.reportTitle " + " FROM ExpDatReportBO rep "
			+ " WHERE rep.fkStatusId IN :rmStatus AND rep.approverMgr =:mgrId OR rep.fkForwardRmId =:mgrId ")
	public List<String> fetchAllMgrReportName(@Param("mgrId") Integer mgrId, @Param("rmStatus") List<Short> rmStatus);

	// Added by Deepa for Notification

	public List<ExpDatReportBO> findByApproverMgrAndFkStatusId(Integer approverMgr, Short fkStatusId);

	public List<ExpDatReportBO> findByFkEmpIdIntAndFkStatusId(Integer fkEmpIdInt, Short fkStatusId);

	public List<ExpDatReportBO> findByFkFinReimburserAndFkStatusId(Short fkFinReimburser, Short fkStatusId);

	public List<ExpDatReportBO> findByFkForwardRmIdAndFkStatusId(Integer fkForwardRmId, Short fkStatusId);
	
	@Query("SELECT DISTINCT rep.fkForwardRmId " + " FROM ExpDatReportBO rep "
			+ " WHERE rep.fkEmpIdInt =:fkEmpIdInt AND rep.fkStatusId IN :fkStatusId ")
	public List<Integer> groupBySkipLevelManager(@Param("fkEmpIdInt") Integer fkEmpIdInt,@Param("fkStatusId") short fkStatusId);

	public List<ExpDatReportBO> findByFkEmpIdIntAndFkStatusIdAndFkForwardRmId(Integer empId, Short fkStatusId,Integer fkForwardRmId);

	// EOA by Deepa
}