package com.thbs.mis.expense.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.expense.bo.ExpDatRecordHistoryBO;
import com.thbs.mis.expense.bo.ExpDatReportBO;

@Repository
public interface ExpDatRecordHistoryRepository
		extends GenericRepository<ExpDatRecordHistoryBO, Integer>, JpaSpecificationExecutor<ExpDatReportBO> {

	public ExpDatRecordHistoryBO findByPkRecordHistoryId(Integer pkRecordHistoryId);

	public List<ExpDatRecordHistoryBO> findByFkReportIdInOrderByPkRecordHistoryIdDesc(Integer fkReportId);

}
