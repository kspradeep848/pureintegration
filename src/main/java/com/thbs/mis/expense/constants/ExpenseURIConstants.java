package com.thbs.mis.expense.constants;

public class ExpenseURIConstants {

	public final static String EXP_GET_CATEGORY = "/exp/get/category";

	public final static String EXP_FIND_CATEGORY = "/exp/find/category";

	public final static String EXP_CREATE_UPDATE_CATEGORY = "/exp/createOrUpdate/category";

	public final static String EXP_CREATE_REPORT = "/exp/createOrUpdate/report";

	public final static String EXP_RECALL_REPORT = "/exp/recallOrReject/report";

	public final static String EXP_FIND_REPORT = "/exp/find/report";

	public final static String EXP_FORWARD_TO_RM = "/exp/forward/report";

	public final static String EXP_REPORT_DETAILS_BY_PKREPORTID = "/exp/report/details/{pkReportId}";

	public final static String EXP_REPORT_APPROVE_REJECT_BY_RM = "/exp/report/approveOrReject";

	public final static String EXP_REPORT_APPROVE_REJECT_BY_FIN = "/exp/reimbursement/approveOrReject";

	public final static String EXP_REPORT_REIMBURSED_CANCEL_BY_FIN = "/exp/reimbursement/doneOrCancel";

	public final static String EXP_FIN_PERDIEM = "/exp/fin/perdiem";

	public final static String EXP_FIND_PERDIEMS = "/exp/find/perdiem";

	public final static String EXP_CREATE_PERDIEM = "/exp/create/perdiem";

	public final static String EXP_PERDIEM_DETAILS = "/exp/perdiem/{pkId}";

	public final static String EXP_APR_REJ_FIN_PER_DIEM = "/exp/ApproveOrReject/perdiem";

	public final static String EXP_GET_ALL_EXPENSE_STATUS = "/exp/all/status";

	public final static String EXP_CREATE_UPDATE_EXPENSE = "/exp/createOrUpdate/expense";

	public final static String EXP_EMP_FIND_EXPENSE = "/exp/find/expense/{empId}";

	public final static String EXP_REPORT_STATUS = "/exp/all/report/status";

	public final static String EXP_FIN_ADVANCE_SEARCH = "/exp/advance/search";

	public final static String EXP_LIST_OF_REPORT_NAME = "/exp/list/report/name";

	public final static String EXP_RM_WORK_ORDER_APPROVE = "/exp/select/workOrder";

	public final static String EXP_EMP_DELETE_FILE = "/exp/delete/receipt";

	public final static String EXP_DOWNLOAD_IMAGE = "/exp/download/image";

	public final static String EXP_REPORT_HISTORY = "/exp/report/history/{fkReportId}";

	public final static String EXP_PER_DIEM_COUNTRY = "/exp/perDiem/country";

	public final static String EXP_UPDATE_EXPENSE = "/exp/update/expense";

	public final static String EXP_FILTER_RM_FINANCE = "/exp/filter";
	
	public final static String EXP_MILEAGE_RATES = "/exp/mileage/rates";
	
}