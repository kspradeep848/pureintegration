package com.thbs.mis.expense.constants;

public class ExpenseConstants {

	public final static Short Report_Draft = 1;
	public final static Short Report_AwatingRMApproval = 2;
	public final static Short Report_EmpRecalled = 3;
	public final static Short Report_Expired = 4;
	public final static Short Report_RMApproved = 5;
	public final static Short Report_RMRejected = 6;
	public final static Short Report_RMApprovedPartially = 7;
	public final static Short Report_EmpRejected = 8;
	public final static Short Report_ReimbursementInitiated = 9;
	public final static Short Report_ReimbursedPartially = 10;
	public final static Short Report_ReimbursementCancelled = 11;
	public final static Short Report_Reimbursed = 12;
	public final static Short Report_ForwardedbyRM = 13;
	public final static Short Report_ForwardedbyRMPartially = 14;
	public final static Short Report_FinRejected = 15;
	public final static Short Report_ReimbursementIntitatedPartially = 16;

	// Expense status
	public final static Short Expense_UNATTACHED = 1;
	public final static Short Expense_ATTACHED = 2;
	public final static Short Expense_EXPIRED = 3;
	public final static Short Expense_RM_APPROVED = 4;
	public final static Short Expense_RM_REJECTED = 5;
	public final static Short Expense_FINANCE_APPROVED = 6;
	public final static Short Expense_FINANCE_REJECTED = 7;
	public final static Short Expense_DELETED = 8;
	public final static Short Expense_CANCELLED_BY_SUBMITTER = 9;

	// Finace RoalId
	public final static Short FINANCE_HEAD = 12;
	public final static Short FINANCE_MANAGER = 13;
	public final static Short FINANCE_SENIOR_EXECUTIVE = 14;
	public final static Short FINANCE_EXECUTIVE = 15;

	// Per-Diem Status
	public final static Short PER_DIEM_WAITING_FOR_APPROVAL = 1;
	public final static Short PER_DIEM_FINANCE_APPROVED = 2;
	public final static Short PER_DIEM_FINANCE_REJECTED = 3;

}
