package com.thbs.mis.expense.constants;

public class ExpenseMessageConstant {
	
	public final static String NO_RECORD_FOUND = "NO RECORD FOUND";
	public final static String EXP_CATEGORY_FIND_SUCCESS = "Search category is found as below";
	public final static String EXP_CATEGORY_FIND_FAILED = "Category is not Available";
	public final static String EXP_CATEGORY_SAVED_SUCCESS = "Category  details has been saved to database";
	public final static String EXP_CATEGORY_SAVED_FAILED = "Category  details is not saved to database";
	public final static String EXP_CATEGORY_SAVED_DB_FAILED = "Category  details unabled to  save in  database";
 
}
