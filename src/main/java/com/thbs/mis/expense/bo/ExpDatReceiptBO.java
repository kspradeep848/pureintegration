package com.thbs.mis.expense.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.thbs.mis.common.bo.DatEmpDetailBO;


@Entity
@Table(name = "gsr_kpi_goal")
@NamedQuery(name = "ExpDatReceiptBO.findAll", query = "SELECT g FROM ExpDatReceiptBO g")
public class ExpDatReceiptBO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_receipt_id")
	Integer pkReceiptId;
	
	@Column(name = "fk_expense_id")
	private Integer fkExpenseId;
	
	@Column(name = "fk_emp_id")
	private Integer fkEmpId;
	
	@OneToOne
	@JoinColumn(name = "pk_expense_id", unique = true, nullable = true, insertable = true, updatable = true)
	private ExpDatExpenseBO ExpDatExpenseBO;
	
	public ExpDatReceiptBO(){
		
	}

	public ExpDatReceiptBO(Integer pkReceiptId, Integer fkExpenseId,
			Integer fkEmpId) {
		super();
		this.pkReceiptId = pkReceiptId;
		this.fkExpenseId = fkExpenseId;
		this.fkEmpId = fkEmpId;
	}

	public Integer getPkReceiptId() {
		return pkReceiptId;
	}

	public void setPkReceiptId(Integer pkReceiptId) {
		this.pkReceiptId = pkReceiptId;
	}

	public Integer getFkExpenseId() {
		return fkExpenseId;
	}

	public void setFkExpenseId(Integer fkExpenseId) {
		this.fkExpenseId = fkExpenseId;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	@Override
	public String toString() {
		return "ExpDatReceiptBO [pkReceiptId=" + pkReceiptId + ", fkExpenseId="
				+ fkExpenseId + ", fkEmpId=" + fkEmpId + "]";
	}
	
	
	

}
