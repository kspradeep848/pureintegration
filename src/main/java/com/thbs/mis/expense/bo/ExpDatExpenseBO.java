package com.thbs.mis.expense.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.bo.MasLocationParentBO;
import com.thbs.mis.common.bo.MasLocationPhysicalBO;
import com.thbs.mis.framework.util.CustomDateSerializer;

/**
 *
 * @author manikandan_chinnasam
 */
@Entity
@Table(name = "exp_dat_expense")
@NamedQueries({ @NamedQuery(name = "ExpDatExpenseBO.findAll", query = "SELECT e FROM ExpDatExpenseBO e") })
public class ExpDatExpenseBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_expense_id")
	private Integer pkExpenseId;

	@Basic(optional = false)
	@Column(name = "fk_report_id")
	private Integer fkReportId;

	@Basic(optional = false)
	@Column(name = "expense_amount")
	private Double expenseAmount;

	@Basic(optional = false)
	@Column(name = "merchant")
	private String merchant;

	@Basic(optional = false)
	@Column(name = "expense_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date expenseDate;

	@Column(name = "expense_desc")
	private String expenseDesc;

	@Column(name = "manager_approval_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date managerApprovalDate;

	@Column(name = "finance_approval_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date financeApprovalDate;

	@Column(name = "fk_currency_type")
	private Integer currencyType;

	@Column(name = "fk_status_id")
	Short fkStatusId;

	//
	@Basic(optional = false)
	@Column(name = "tax_amount")
	private Double taxAmount;

	@Column(name = "is_tax_inclusive")
	Integer isTaxInclusive;

	@Basic(optional = false)
	@Column(name = "converted_amount_to_payroll")
	private Double convertedAmountToPayroll;
	
	@Basic(optional = false)
	@Column(name = "final_exp_amount")
	private Double finalExpAmount;

	@Column(name = "emp_org")
	Short empOrg;
	
	@Column(name="miles")
	private Double miles;
	
	@Column(name = "fk_mileage_id")
	Integer fkMileageId;
	
	@Column(name = "file_name")
	private String fileName;
	
	@Column(name = "path_name")
	private String pathName;

	@JoinColumn(name = "fk_status_id", referencedColumnName = "pk_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	@OneToOne(optional = false)
	private ExpMasStatusBO expMasStatusBOByFkStatusId;

	// @JoinColumn(name = "pk_expense_id", referencedColumnName = "pk_report_id",
	// unique = true, nullable = true, insertable = false, updatable = false)
	// @OneToOne(optional = false)
	// private ExpDatReportBO ExpDatReportBO;

	@Column(name = "fk_loc_onsite_id")
	Integer fkLocOnsiteId;

	@JoinColumn(name = "fk_loc_onsite_id", referencedColumnName = "pk_location_physical_id", unique = true, nullable = true, insertable = false, updatable = false)
	@OneToOne
	private MasLocationPhysicalBO masLocBo;

	@Column(name = "fk_loc_base_id")
	Integer fkLocBaseId;

	@JoinColumn(name = "fk_loc_base_id", referencedColumnName = "pk_location_parent_id", unique = true, nullable = true, insertable = false, updatable = false)
	@OneToOne
	private MasLocationParentBO MasLocBo;

	@Column(name = "fk_emp_id")
	Integer fkEmpId;

	@JoinColumn(name = "fk_emp_id", referencedColumnName = "pk_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	@OneToOne
	private DatEmpDetailBO empBo;

	@Column(name = "fk_category_id")
	Integer fkCategoryId;

	@JoinColumn(name = "fk_category_id", referencedColumnName = "pk_category_id", unique = true, nullable = true, insertable = false, updatable = false)
	@OneToOne(optional = false)
	private ExpMasCategoryBO categoryBo;

	@Column(name = "fk_bu_id")
	Integer fkBuId;

	@Column(name = "workorder")
	Integer workorder;

	@Column(name = "billlable_to_client")
	String billlableToClient;

	@JoinColumn(name = "fk_bu_id", referencedColumnName = "pk_bu_unit_id", unique = true, nullable = true, insertable = false, updatable = false)
	@OneToOne(optional = false)
	private MasBuUnitBO masBuUnitBO;
	
	@JoinColumn(name = "fk_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	@OneToOne(optional = false)
	private ExpMasStatusBO expMasStatusBO;
	
	@JoinColumn(name = "fk_mileage_id", unique = true, nullable = true, insertable = false, updatable = false)
	@OneToOne(optional = false)
	private ExpMileageRatesBO expMileageRatesBO;

	public ExpDatExpenseBO() {
	}

	public Integer getPkExpenseId() {
		return pkExpenseId;
	}

	public Integer getFkReportId() {
		return fkReportId;
	}

	public Double getExpenseAmount() {
		return expenseAmount;
	}

	public String getMerchant() {
		return merchant;
	}

	public Date getExpenseDate() {
		return expenseDate;
	}

	public String getExpenseDesc() {
		return expenseDesc;
	}

	public Date getManagerApprovalDate() {
		return managerApprovalDate;
	}

	public Date getFinanceApprovalDate() {
		return financeApprovalDate;
	}

	public Integer getCurrencyType() {
		return currencyType;
	}

	public Short getFkStatusId() {
		return fkStatusId;
	}

	public ExpMasStatusBO getExpMasStatusBOByFkStatusId() {
		return expMasStatusBOByFkStatusId;
	}

	// public ExpDatReportBO getExpDatReportBO() {
	// return ExpDatReportBO;
	// }

	public Integer getFkLocOnsiteId() {
		return fkLocOnsiteId;
	}

	public MasLocationPhysicalBO getMasLocBoBFklocOnsiteId() {
		return masLocBo;
	}

	public Integer getFkLocBaseId() {
		return fkLocBaseId;
	}

	public MasLocationParentBO getMasLocBo() {
		return MasLocBo;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public DatEmpDetailBO getEmpBo() {
		return empBo;
	}

	public Integer getFkCategoryId() {
		return fkCategoryId;
	}

	public ExpMasCategoryBO getCategoryBo() {
		return categoryBo;
	}

	public Integer getFkBuid() {
		return fkBuId;
	}

	public MasBuUnitBO getMasBuUnitBO() {
		return masBuUnitBO;
	}

	public void setPkExpenseId(Integer pkExpenseId) {
		this.pkExpenseId = pkExpenseId;
	}

	public void setFkReportId(Integer fkReportId) {
		this.fkReportId = fkReportId;
	}

	public void setExpenseAmount(Double expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public void setExpenseDate(Date expenseDate) {
		this.expenseDate = expenseDate;
	}

	public void setExpenseDesc(String expenseDesc) {
		this.expenseDesc = expenseDesc;
	}

	public void setManagerApprovalDate(Date managerApprovalDate) {
		this.managerApprovalDate = managerApprovalDate;
	}

	public void setFinanceApprovalDate(Date financeApprovalDate) {
		this.financeApprovalDate = financeApprovalDate;
	}

	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}

	public void setFkStatusId(Short fkStatusId) {
		this.fkStatusId = fkStatusId;
	}

	public void setExpMasStatusBOByFkStatusId(ExpMasStatusBO expMasStatusBOByFkStatusId) {
		this.expMasStatusBOByFkStatusId = expMasStatusBOByFkStatusId;
	}

	// public void setExpDatReportBO(ExpDatReportBO expDatReportBO) {
	// ExpDatReportBO = expDatReportBO;
	// }

	public void setFkLocOnsiteId(Integer fkLocOnsiteId) {
		this.fkLocOnsiteId = fkLocOnsiteId;
	}

	public void setMasLocBoBFklocOnsiteId(MasLocationPhysicalBO masLocBoBFklocOnsiteId) {
		this.masLocBo = masLocBoBFklocOnsiteId;
	}

	public void setFkLocBaseId(Integer fkLocBaseId) {
		this.fkLocBaseId = fkLocBaseId;
	}

	public void setMasLocBo(MasLocationParentBO masLocBo) {
		MasLocBo = masLocBo;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public void setEmpBo(DatEmpDetailBO empBo) {
		this.empBo = empBo;
	}

	public void setFkCategoryId(Integer fkCategoryId) {
		this.fkCategoryId = fkCategoryId;
	}

	public void setCategoryBo(ExpMasCategoryBO categoryBo) {
		this.categoryBo = categoryBo;
	}

	public void setFkBuid(Integer fkBuid) {
		this.fkBuId = fkBuid;
	}

	public void setMasBuUnitBO(MasBuUnitBO masBuUnitBO) {
		this.masBuUnitBO = masBuUnitBO;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public Integer getIsTaxInclusive() {
		return isTaxInclusive;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public void setIsTaxInclusive(Integer isTaxInclusive) {
		this.isTaxInclusive = isTaxInclusive;
	}

	public Integer getWorkorder() {
		return workorder;
	}

	public void setWorkorder(Integer workorder) {
		this.workorder = workorder;
	}

	// public ExpDatReportBO getExpDatReportBO() {
	// return ExpDatReportBO;
	// }
	//
	// public void setExpDatReportBO(ExpDatReportBO expDatReportBO) {
	// ExpDatReportBO = expDatReportBO;
	// }

	public String getBilllableToClient() {
		return billlableToClient;
	}

	public void setBilllableToClient(String billlableToClient) {
		this.billlableToClient = billlableToClient;
	}

	public Double getConvertedAmountToPayroll() {
		return convertedAmountToPayroll;
	}

	public void setConvertedAmountToPayroll(Double convertedAmountToPayroll) {
		this.convertedAmountToPayroll = convertedAmountToPayroll;
	}

	public Short getEmpOrg() {
		return empOrg;
	}

	public void setEmpOrg(Short empOrg) {
		this.empOrg = empOrg;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPathName() {
		return pathName;
	}

	public void setPathName(String pathName) {
		this.pathName = pathName;
	}

	public Double getFinalExpAmount() {
		return finalExpAmount;
	}

	public void setFinalExpAmount(Double finalExpAmount) {
		this.finalExpAmount = finalExpAmount;
	}

	public ExpMasStatusBO getExpMasStatusBO() {
		return expMasStatusBO;
	}

	public void setExpMasStatusBO(ExpMasStatusBO expMasStatusBO) {
		this.expMasStatusBO = expMasStatusBO;
	}

	public Integer getFkMileageId() {
		return fkMileageId;
	}

	public void setFkMileageId(Integer fkMileageId) {
		this.fkMileageId = fkMileageId;
	}

	public Double getMiles() {
		return miles;
	}

	public void setMiles(Double miles) {
		this.miles = miles;
	}

	public ExpMileageRatesBO getExpMileageRatesBO() {
		return expMileageRatesBO;
	}

	public void setExpMileageRatesBO(ExpMileageRatesBO expMileageRatesBO) {
		this.expMileageRatesBO = expMileageRatesBO;
	}

	@Override
	public String toString() {
		return "ExpDatExpenseBO [pkExpenseId=" + pkExpenseId + ", fkReportId=" + fkReportId + ", expenseAmount="
				+ expenseAmount + ", merchant=" + merchant + ", expenseDate=" + expenseDate + ", expenseDesc="
				+ expenseDesc + ", managerApprovalDate=" + managerApprovalDate + ", financeApprovalDate="
				+ financeApprovalDate + ", currencyType=" + currencyType + ", fkStatusId=" + fkStatusId + ", taxAmount="
				+ taxAmount + ", isTaxInclusive=" + isTaxInclusive + ", convertedAmountToPayroll="
				+ convertedAmountToPayroll + ", finalExpAmount=" + finalExpAmount + ", empOrg=" + empOrg + ", miles="
				+ miles + ", fkMileageId=" + fkMileageId + ", fileName=" + fileName + ", pathName=" + pathName
				+ ", expMasStatusBOByFkStatusId=" + expMasStatusBOByFkStatusId + ", fkLocOnsiteId=" + fkLocOnsiteId
				+ ", masLocBo=" + masLocBo + ", fkLocBaseId=" + fkLocBaseId + ", MasLocBo=" + MasLocBo + ", fkEmpId="
				+ fkEmpId + ", empBo=" + empBo + ", fkCategoryId=" + fkCategoryId + ", categoryBo=" + categoryBo
				+ ", fkBuId=" + fkBuId + ", workorder=" + workorder + ", billlableToClient=" + billlableToClient
				+ ", masBuUnitBO=" + masBuUnitBO + ", expMasStatusBO=" + expMasStatusBO + ", expMileageRatesBO="
				+ expMileageRatesBO + "]";
	}

}
