/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thbs.mis.expense.bo;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author manikandan_chinnasam
 */
@Entity
@Table(name = "exp_report_mas_status")
@NamedQueries({
    @NamedQuery(name = "ExpReportMasStatusBO.findAll", query = "SELECT e FROM ExpReportMasStatusBO e")})
public class ExpReportMasStatusBO implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "pk_status_id")
    private Short pkStatusId;
    @Basic(optional = false)
    @Column(name = "status_desc")
    private String statusDesc;
 
    public ExpReportMasStatusBO() {
    }


	public ExpReportMasStatusBO(Short pkStatusId) {
        this.pkStatusId = pkStatusId;
    }

    public ExpReportMasStatusBO(Short pkStatusId, String statusDesc) {
        this.pkStatusId = pkStatusId;
        this.statusDesc = statusDesc;
    }

    public Short getPkStatusId() {
        return pkStatusId;
    }

    public void setPkStatusId(Short pkStatusId) {
        this.pkStatusId = pkStatusId;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

 
}
