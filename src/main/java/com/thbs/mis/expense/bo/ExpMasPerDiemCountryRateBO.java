package com.thbs.mis.expense.bo;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.thbs.mis.common.bo.MasCurrencytypeBO;

@Entity
@Table(name = "exp_mas_per_diem_country_rate")
@NamedQueries({
		@NamedQuery(name = "ExpMasPerDiemCountryRateBO.findAll", query = "SELECT e FROM ExpMasPerDiemCountryRateBO e") })
public class ExpMasPerDiemCountryRateBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_id")
	private Integer pkId;

	@Basic(optional = false)
	@Column(name = "per_diem_country")
	private String perDiemCountry;

	@Basic(optional = false)
	@Column(name = "per_diem_rate")
	private Double perDiemRate;

	@Basic(optional = false)
	@Column(name = "with_accommadation")
	private Double withAccommadation;

	@Basic(optional = false)
	@Column(name = "fk_currency_type")
	private Short fkCurrencyType;

	@OneToOne
	@JoinColumn(name = "fk_currency_type", unique = true, nullable = true, insertable = false, updatable = false)
	private MasCurrencytypeBO masCurrencytypeBO;

	public Integer getPkId() {
		return pkId;
	}

	public void setPkId(Integer pkId) {
		this.pkId = pkId;
	}

	public String getPerDiemCountry() {
		return perDiemCountry;
	}

	public void setPerDiemCountry(String perDiemCountry) {
		this.perDiemCountry = perDiemCountry;
	}

	public Double getPerDiemRate() {
		return perDiemRate;
	}

	public void setPerDiemRate(Double perDiemRate) {
		this.perDiemRate = perDiemRate;
	}

	public Double getWithAccommadation() {
		return withAccommadation;
	}

	public void setWithAccommadation(Double withAccommadation) {
		this.withAccommadation = withAccommadation;
	}

	public Short getFkCurrencyType() {
		return fkCurrencyType;
	}

	public void setFkCurrencyType(Short fkCurrencyType) {
		this.fkCurrencyType = fkCurrencyType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public MasCurrencytypeBO getMasCurrencytypeBO() {
		return masCurrencytypeBO;
	}

	public void setMasCurrencytypeBO(MasCurrencytypeBO masCurrencytypeBO) {
		this.masCurrencytypeBO = masCurrencytypeBO;
	}

	@Override
	public String toString() {
		return "ExpMasPerDiemCountryRateBO [pkId=" + pkId + ", perDiemCountry=" + perDiemCountry + ", perDiemRate="
				+ perDiemRate + ", withAccommadation=" + withAccommadation + ", fkCurrencyType=" + fkCurrencyType
				+ ", masCurrencytypeBO=" + masCurrencytypeBO + "]";
	}
}
