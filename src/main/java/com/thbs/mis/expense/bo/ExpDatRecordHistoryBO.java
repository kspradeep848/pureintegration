package com.thbs.mis.expense.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

@Entity
@Table(name = "exp_dat_record_history")
@NamedQueries({ @NamedQuery(name = "ExpDatRecordHistoryBO.findAll", query = "SELECT e FROM ExpDatRecordHistoryBO e") })
public class ExpDatRecordHistoryBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_record_history_id")
	private Integer pkRecordHistoryId;

	@Basic(optional = false)
	@Column(name = "fk_report_id")
	private Integer fkReportId;

	@Basic(optional = false)
	@Column(name = "fk_emp_id")
	private Integer fkEmpId;

	@Column(name = "fk_status_id")
	private Short fkStatusId;

	@Basic(optional = false)
	@Column(name = "record_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date recordDate;

	@Basic(optional = false)
	@Column(name = "to_emp_id")
	private Integer toEmpId;

	@OneToOne
	@JoinColumn(name = "fk_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ExpReportMasStatusBO expReportMasStatusBO;

	public Integer getPkRecordHistoryId() {
		return pkRecordHistoryId;
	}

	public void setPkRecordHistoryId(Integer pkRecordHistoryId) {
		this.pkRecordHistoryId = pkRecordHistoryId;
	}

	public Integer getFkReportId() {
		return fkReportId;
	}

	public void setFkReportId(Integer fkReportId) {
		this.fkReportId = fkReportId;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public Short getFkStatusId() {
		return fkStatusId;
	}

	public void setFkStatusId(Short fkStatusId) {
		this.fkStatusId = fkStatusId;
	}

	public Date getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}

	public Integer getToEmpId() {
		return toEmpId;
	}

	public void setToEmpId(Integer toEmpId) {
		this.toEmpId = toEmpId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ExpReportMasStatusBO getExpReportMasStatusBO() {
		return expReportMasStatusBO;
	}

	public void setExpReportMasStatusBO(ExpReportMasStatusBO expReportMasStatusBO) {
		this.expReportMasStatusBO = expReportMasStatusBO;
	}

	@Override
	public String toString() {
		return "ExpDatRecordHistoryBO [pkRecordHistoryId=" + pkRecordHistoryId + ", fkReportId=" + fkReportId
				+ ", fkEmpId=" + fkEmpId + ", fkStatusId=" + fkStatusId + ", recordDate=" + recordDate + ", toEmpId="
				+ toEmpId + ", expReportMasStatusBO=" + expReportMasStatusBO + "]";
	}
}
