/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thbs.mis.expense.bo;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author manikandan_chinnasam
 */
@Entity
@Table(name = "exp_mas_status")
@NamedQueries({
    @NamedQuery(name = "ExpMasStatusBO.findAll", query = "SELECT e FROM ExpMasStatusBO e")})
public class ExpMasStatusBO implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "pk_status_id")
    private Short pkStatusId;
    @Basic(optional = false)
    @Column(name = "status_desc")
    private String statusDesc;
 
    public ExpMasStatusBO() {
    }


	public ExpMasStatusBO(Short pkStatusId) {
        this.pkStatusId = pkStatusId;
    }

    public ExpMasStatusBO(Short pkStatusId, String statusDesc) {
        this.pkStatusId = pkStatusId;
        this.statusDesc = statusDesc;
    }

    public Short getPkStatusId() {
        return pkStatusId;
    }

    public void setPkStatusId(Short pkStatusId) {
        this.pkStatusId = pkStatusId;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

 
}
