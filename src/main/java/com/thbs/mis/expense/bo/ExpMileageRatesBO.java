package com.thbs.mis.expense.bo;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "exp_mileage_rates")
@NamedQueries({ @NamedQuery(name = "ExpMileageRatesBO.findAll", query = "SELECT e FROM ExpMileageRatesBO e") })
public class ExpMileageRatesBO implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_mileage_id")
	private Integer pkMileageId;

	@Column(name = "type_of_vehicle")
	private String typeOfVehicle;

	@Column(name = "first_10000miles")
	private Double first10000Miles;

	@Column(name = "above_10000miles")
	private Double above10000Miles;

	@Column(name = "currency_type")
	private String currencyType;

	public Integer getPkMileageId() {
		return pkMileageId;
	}

	public void setPkMileageId(Integer pkMileageId) {
		this.pkMileageId = pkMileageId;
	}

	public String getTypeOfVehicle() {
		return typeOfVehicle;
	}

	public void setTypeOfVehicle(String typeOfVehicle) {
		this.typeOfVehicle = typeOfVehicle;
	}

	public Double getFirst10000Miles() {
		return first10000Miles;
	}

	public void setFirst10000Miles(Double first10000Miles) {
		this.first10000Miles = first10000Miles;
	}

	public Double getAbove10000Miles() {
		return above10000Miles;
	}

	public void setAbove10000Miles(Double above10000Miles) {
		this.above10000Miles = above10000Miles;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ExpMileageRatesBO [pkMileageId=" + pkMileageId + ", typeOfVehicle=" + typeOfVehicle
				+ ", first10000Miles=" + first10000Miles + ", above10000Miles=" + above10000Miles + ", currencyType="
				+ currencyType + "]";
	}
}
