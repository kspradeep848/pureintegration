/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thbs.mis.expense.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasCurrencytypeBO;
import com.thbs.mis.framework.util.CustomTimeSerializer;

/**
 *
 * @author manikandan_chinnasam
 */
@Entity
@Table(name = "exp_dat_per_diem")
@NamedQueries({ @NamedQuery(name = "ExpDatPerDiemBO.findAll", query = "SELECT e FROM ExpDatPerDiemBO e") })
public class ExpDatPerDiemBO implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_id")
	private Integer pkId;

	@Column(name = "fk_emp_id")
	private Integer fkEmpId;

	@Column(name = "fk_currency_type")
	private Short fkCurrencyType;

	@Column(name = "created_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date createdDate;

	@Column(name = "per_diem_start_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date perDiemStartDate;

	@Basic(optional = false)
	@Column(name = "no_of_days")
	private Integer noOfDays;

	@Column(name = "fin_approver_id")
	private Integer finApproverId;

	@Column(name = "fin_appproved_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date finAppprovedDate;

	@Basic(optional = false)
	@Column(name = "per_diem_rate")
	private Double perDiemRate;

	@Basic(optional = false)
	@Column(name = "amount")
	private Double amount;

	@Column(name = "per_diem_desc")
	private String perDiemDesc;

	@Column(name = "fk_country_id")
	private Integer fkCountryId;

	@Column(name = "is_with_accomodation")
	private Short isWithAccomodation;

	@Column(name = "per_diem_status")
	private Short perDiemStatus;

	@Column(name = "fin_comments")
	private String finComments;

	@OneToOne
	@JoinColumn(name = "fk_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO;

	@OneToOne
	@JoinColumn(name = "fk_currency_type", unique = true, nullable = true, insertable = false, updatable = false)
	private MasCurrencytypeBO masCurrencytypeBO;

	@OneToOne
	@JoinColumn(name = "fk_country_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ExpMasPerDiemCountryRateBO expMasPerDiemCountryRateBO;

	public ExpDatPerDiemBO() {

	}

	public ExpDatPerDiemBO(Integer fkEmpId, Short fkCurrencyType, Date createdDate, Date perDiemStartDate,
			Integer noOfDays, Double perDiemRate, Double amount, String perDiemDesc, Integer fkCountryId,
			Short isWithAccomodation, Short perDiemStatus) {
		this.fkEmpId = fkEmpId;
		this.fkCurrencyType = fkCurrencyType;
		this.createdDate = createdDate;
		this.perDiemStartDate = perDiemStartDate;
		this.noOfDays = noOfDays;
		this.perDiemRate = perDiemRate;
		this.amount = amount;
		this.perDiemDesc = perDiemDesc;
		this.fkCountryId = fkCountryId;
		this.isWithAccomodation = isWithAccomodation;
		this.perDiemStatus = perDiemStatus;
	}

	public Integer getPkId() {
		return pkId;
	}

	public void setPkId(Integer pkId) {
		this.pkId = pkId;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public Short getFkCurrencyType() {
		return fkCurrencyType;
	}

	public void setFkCurrencyType(Short fkCurrencyType) {
		this.fkCurrencyType = fkCurrencyType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getPerDiemStartDate() {
		return perDiemStartDate;
	}

	public void setPerDiemStartDate(Date perDiemStartDate) {
		this.perDiemStartDate = perDiemStartDate;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Integer getFinApproverId() {
		return finApproverId;
	}

	public void setFinApproverId(Integer finApproverId) {
		this.finApproverId = finApproverId;
	}

	public Date getFinAppprovedDate() {
		return finAppprovedDate;
	}

	public void setFinAppprovedDate(Date finAppprovedDate) {
		this.finAppprovedDate = finAppprovedDate;
	}

	public Double getPerDiemRate() {
		return perDiemRate;
	}

	public void setPerDiemRate(Double perDiemRate) {
		this.perDiemRate = perDiemRate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPerDiemDesc() {
		return perDiemDesc;
	}

	public void setPerDiemDesc(String perDiemDesc) {
		this.perDiemDesc = perDiemDesc;
	}

	public Integer getFkCountryId() {
		return fkCountryId;
	}

	public void setFkCountryId(Integer fkCountryId) {
		this.fkCountryId = fkCountryId;
	}

	public Short getIsWithAccomodation() {
		return isWithAccomodation;
	}

	public void setIsWithAccomodation(Short isWithAccomodation) {
		this.isWithAccomodation = isWithAccomodation;
	}

	public Short getPerDiemStatus() {
		return perDiemStatus;
	}

	public void setPerDiemStatus(Short perDiemStatus) {
		this.perDiemStatus = perDiemStatus;
	}

	public String getFinComments() {
		return finComments;
	}

	public void setFinComments(String finComments) {
		this.finComments = finComments;
	}

	public DatEmpDetailBO getDatEmpDetailBO() {
		return datEmpDetailBO;
	}

	public void setDatEmpDetailBO(DatEmpDetailBO datEmpDetailBO) {
		this.datEmpDetailBO = datEmpDetailBO;
	}

	public MasCurrencytypeBO getMasCurrencytypeBO() {
		return masCurrencytypeBO;
	}

	public void setMasCurrencytypeBO(MasCurrencytypeBO masCurrencytypeBO) {
		this.masCurrencytypeBO = masCurrencytypeBO;
	}

	public ExpMasPerDiemCountryRateBO getExpMasPerDiemCountryRateBO() {
		return expMasPerDiemCountryRateBO;
	}

	public void setExpMasPerDiemCountryRateBO(ExpMasPerDiemCountryRateBO expMasPerDiemCountryRateBO) {
		this.expMasPerDiemCountryRateBO = expMasPerDiemCountryRateBO;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ExpDatPerDiemBO [pkId=" + pkId + ", fkEmpId=" + fkEmpId + ", fkCurrencyType=" + fkCurrencyType
				+ ", createdDate=" + createdDate + ", perDiemStartDate=" + perDiemStartDate + ", noOfDays=" + noOfDays
				+ ", finApproverId=" + finApproverId + ", finAppprovedDate=" + finAppprovedDate + ", perDiemRate="
				+ perDiemRate + ", amount=" + amount + ", perDiemDesc=" + perDiemDesc + ", fkCountryId=" + fkCountryId
				+ ", isWithAccomodation=" + isWithAccomodation + ", perDiemStatus=" + perDiemStatus + ", finComments="
				+ finComments + ", datEmpDetailBO=" + datEmpDetailBO + ", masCurrencytypeBO=" + masCurrencytypeBO
				+ ", expMasPerDiemCountryRateBO=" + expMasPerDiemCountryRateBO + "]";
	}

}
