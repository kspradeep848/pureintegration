/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thbs.mis.expense.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.bo.MasCurrencytypeBO;
import com.thbs.mis.framework.util.CustomDateSerializer;
import com.thbs.mis.framework.util.CustomTimeSerializer;

/**
 *
 * @author manikandan_chinnasam
 */
@Entity
@Table(name = "exp_dat_report")
@NamedQueries({ @NamedQuery(name = "ExpDatReportBO.findAll", query = "SELECT e FROM ExpDatReportBO e") })
public class ExpDatReportBO implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_report_id")
	private Integer pkReportId;
	@Basic(optional = false)
	@Column(name = "approver_mgr_id")
	private Integer approverMgr;
	@Basic(optional = false)
	@Column(name = "approver_mgr_comment")
	private String approverMgrComment;
	@Basic(optional = false)
	@Column(name = "approver_fin_id")
	private Integer approverFin;
	@Basic(optional = false)
	@Column(name = "approver_fin_comment")
	private String approverFinComment;
	@Basic(optional = false)
	@Column(name = "start_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date startDate;
	@Basic(optional = false)
	@Column(name = "end_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date endDate;
	@Column(name = "submission_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date submissionDate;
	@Basic(optional = false)
	@Column(name = "report_desc")
	private String reportDesc;
	@Basic(optional = false)
	@Column(name = "report_title")
	private String reportTitle;
	@Column(name = "modification_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date modificationDate;
	@Column(name = "manager_approval_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date managerApprovalDate;
	@Column(name = "finance_approval_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date financeApprovalDate;
	@Column(name = "payment_type")
	private String paymentType;
	@Column(name = "fk_emp_id")
	private Integer fkEmpIdInt;
	@Column(name = "total_amount")
	private Double totalAmount;
	@Column(name = "rm_approved_amount")
	private Double rmApprovedAmount;
	@Column(name = "fin_reimbursed_amount")
	private Double finReimbursedAmount;
	@Column(name = "fk_status_id")
	private Short fkStatusId;
	@Column(name = "fk_bu_id")
	private Byte fkBuId;
	@Column(name = "fk_client_id")
	private Integer fkClientId;
	@Column(name = "fk_project_id")
	private Integer fkProjectId;
	@Column(name = "fk_currency_type_id")
	private Short fkCurrencyTypeId;
	@Column(name = "fk_fin_reimburser")
	private Short fkFinReimburser;
	@Column(name = "is_client_reimbursing")
	private Byte isClientReimbursing;
	@Column(name = "fk_forward_rm_id")
	private Integer fkForwardRmId;
	@Column(name = "recall_status")
	private Short recallStatus;
	@Column(name = "redrafted_status")
	private Short redraftedStatus;
	@OneToOne
	@JoinColumn(name = "fk_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private ExpReportMasStatusBO expReportMasStatusBO;
	@OneToOne
	@JoinColumn(name = "fk_bu_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasBuUnitBO masBuUnitBO;
	@OneToOne
	@JoinColumn(name = "fk_currency_type_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasCurrencytypeBO masCurrencytypeBO;

	/*
	 * @JoinColumn(name = "fk_emp_id", referencedColumnName = "pk_emp_id")
	 * 
	 * @OneToOne(optional = false)
	 */
	/* private DatEmpDetailBO fkEmpId; */
	/*
	 * @OneToOne(cascade = CascadeType.ALL, mappedBy = "ExpDatReportBO") private
	 * ExpDatExpenseBO ExpDatExpenseBO;
	 */

	public ExpDatReportBO() {
	}

	public ExpDatReportBO(Integer pkReportId) {
		this.pkReportId = pkReportId;
	}

	public ExpDatReportBO(Integer pkReportId, Integer approverMgr, Integer approverFin, Date startDate, Date endDate,
			String reportDesc, String reportTitle, Integer empId, String payment, Date submissionDate,
			Date ModificationDate) {
		this.pkReportId = pkReportId;
		this.approverMgr = approverMgr;
		this.approverFin = approverFin;
		this.startDate = startDate;
		this.endDate = endDate;
		this.reportDesc = reportDesc;
		this.reportTitle = reportTitle;
		this.fkEmpIdInt = empId;
		this.paymentType = payment;
		this.submissionDate = submissionDate;
		this.modificationDate = ModificationDate;
	}

	public ExpDatReportBO(Integer approverMgr, Integer empId, Date startDate, Date endDate, String reportDesc,
			String reportTitle, Double totalAmount, Short fkStatusId, Date submissionDate, Byte fkBuId,
			Integer fkClientId, Integer fkProjectId, Short fkCurrencyTypeId, Short recallStatus, Short redraftedStatus) {
		this.approverMgr = approverMgr;
		this.fkEmpIdInt = empId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.reportDesc = reportDesc;
		this.reportTitle = reportTitle;
		this.totalAmount = totalAmount;
		this.fkStatusId = fkStatusId;
		this.submissionDate = submissionDate;
		this.fkBuId = fkBuId;
		this.fkClientId = fkClientId;
		this.fkProjectId = fkProjectId;
		this.fkCurrencyTypeId = fkCurrencyTypeId;
		this.recallStatus = recallStatus;
		this.redraftedStatus = redraftedStatus;
	}

	public Integer getPkReportId() {
		return pkReportId;
	}

	public void setPkReportId(Integer pkReportId) {
		this.pkReportId = pkReportId;
	}

	public Integer getApproverMgr() {
		return approverMgr;
	}

	public void setApproverMgr(Integer approverMgr) {
		this.approverMgr = approverMgr;
	}

	public String getApproverMgrComment() {
		return approverMgrComment;
	}

	public void setApproverMgrComment(String approverMgrComment) {
		this.approverMgrComment = approverMgrComment;
	}

	public Integer getApproverFin() {
		return approverFin;
	}

	public void setApproverFin(Integer approverFin) {
		this.approverFin = approverFin;
	}

	public String getApproverFinComment() {
		return approverFinComment;
	}

	public void setApproverFinComment(String approverFinComment) {
		this.approverFinComment = approverFinComment;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}

	public String getReportDesc() {
		return reportDesc;
	}

	public void setReportDesc(String reportDesc) {
		this.reportDesc = reportDesc;
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Integer getFkEmpIdInt() {
		return fkEmpIdInt;
	}

	public void setFkEmpIdInt(Integer fkEmpIdInt) {
		this.fkEmpIdInt = fkEmpIdInt;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Short getFkStatusId() {
		return fkStatusId;
	}

	public void setFkStatusId(Short fkStatusId) {
		this.fkStatusId = fkStatusId;
	}

	public Byte getFkBuId() {
		return fkBuId;
	}

	public void setFkBuId(Byte fkBuId) {
		this.fkBuId = fkBuId;
	}

	public Short getFkCurrencyTypeId() {
		return fkCurrencyTypeId;
	}

	public void setFkCurrencyTypeId(Short fkCurrencyTypeId) {
		this.fkCurrencyTypeId = fkCurrencyTypeId;
	}

	public Short getFkFinReimburser() {
		return fkFinReimburser;
	}

	public void setFkFinReimburser(Short fkFinReimburser) {
		this.fkFinReimburser = fkFinReimburser;
	}

	public Byte getIsClientReimbursing() {
		return isClientReimbursing;
	}

	public void setIsClientReimbursing(Byte isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}

	public Date getManagerApprovalDate() {
		return managerApprovalDate;
	}

	public void setManagerApprovalDate(Date managerApprovalDate) {
		this.managerApprovalDate = managerApprovalDate;
	}

	public Date getFinanceApprovalDate() {
		return financeApprovalDate;
	}

	public void setFinanceApprovalDate(Date financeApprovalDate) {
		this.financeApprovalDate = financeApprovalDate;
	}

	public Integer getFkClientId() {
		return fkClientId;
	}

	public void setFkClientId(Integer fkClientId) {
		this.fkClientId = fkClientId;
	}

	public Integer getFkProjectId() {
		return fkProjectId;
	}

	public void setFkProjectId(Integer fkProjectId) {
		this.fkProjectId = fkProjectId;
	}

	public ExpReportMasStatusBO getExpReportMasStatusBO() {
		return expReportMasStatusBO;
	}

	public void setExpReportMasStatusBO(ExpReportMasStatusBO expReportMasStatusBO) {
		this.expReportMasStatusBO = expReportMasStatusBO;
	}

	public MasBuUnitBO getMasBuUnitBO() {
		return masBuUnitBO;
	}

	public void setMasBuUnitBO(MasBuUnitBO masBuUnitBO) {
		this.masBuUnitBO = masBuUnitBO;
	}

	public Double getRmApprovedAmount() {
		return rmApprovedAmount;
	}

	public void setRmApprovedAmount(Double rmApprovedAmount) {
		this.rmApprovedAmount = rmApprovedAmount;
	}

	public Double getFinReimbursedAmount() {
		return finReimbursedAmount;
	}

	public void setFinReimbursedAmount(Double finReimbursedAmount) {
		this.finReimbursedAmount = finReimbursedAmount;
	}

	public Integer getFkForwardRmId() {
		return fkForwardRmId;
	}

	public void setFkForwardRmId(Integer fkForwardRmId) {
		this.fkForwardRmId = fkForwardRmId;
	}

	public Short getRecallStatus() {
		return recallStatus;
	}

	public void setRecallStatus(Short recallStatus) {
		this.recallStatus = recallStatus;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (pkReportId != null ? pkReportId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof ExpDatReportBO)) {
			return false;
		}
		ExpDatReportBO other = (ExpDatReportBO) object;
		if ((this.pkReportId == null && other.pkReportId != null)
				|| (this.pkReportId != null && !this.pkReportId.equals(other.pkReportId))) {
			return false;
		}
		return true;
	}

	public MasCurrencytypeBO getMasCurrencytypeBO() {
		return masCurrencytypeBO;
	}

	public void setMasCurrencytypeBO(MasCurrencytypeBO masCurrencytypeBO) {
		this.masCurrencytypeBO = masCurrencytypeBO;
	}

	public Short getRedraftedStatus() {
		return redraftedStatus;
	}

	public void setRedraftedStatus(Short redraftedStatus) {
		this.redraftedStatus = redraftedStatus;
	}

	@Override
	public String toString() {
		return "ExpDatReportBO [pkReportId=" + pkReportId + ", approverMgr=" + approverMgr + ", approverMgrComment="
				+ approverMgrComment + ", approverFin=" + approverFin + ", approverFinComment=" + approverFinComment
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", submissionDate=" + submissionDate
				+ ", reportDesc=" + reportDesc + ", reportTitle=" + reportTitle + ", modificationDate="
				+ modificationDate + ", managerApprovalDate=" + managerApprovalDate + ", financeApprovalDate="
				+ financeApprovalDate + ", paymentType=" + paymentType + ", fkEmpIdInt=" + fkEmpIdInt + ", totalAmount="
				+ totalAmount + ", rmApprovedAmount=" + rmApprovedAmount + ", finReimbursedAmount="
				+ finReimbursedAmount + ", fkStatusId=" + fkStatusId + ", fkBuId=" + fkBuId + ", fkClientId="
				+ fkClientId + ", fkProjectId=" + fkProjectId + ", fkCurrencyTypeId=" + fkCurrencyTypeId
				+ ", fkFinReimburser=" + fkFinReimburser + ", isClientReimbursing=" + isClientReimbursing
				+ ", fkForwardRmId=" + fkForwardRmId + ", recallStatus=" + recallStatus + ", redraftedStatus="
				+ redraftedStatus + ", expReportMasStatusBO=" + expReportMasStatusBO + ", masBuUnitBO=" + masBuUnitBO
				+ ", masCurrencytypeBO=" + masCurrencytypeBO + "]";
	}

}
