/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thbs.mis.expense.bo;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author manikandan_chinnasam
 */
@Entity
@Table(name = "exp_mas_category")
@NamedQueries({
    @NamedQuery(name = "ExpMasCategoryBO.findAll", query = "SELECT e FROM ExpMasCategoryBO e")})
public class ExpMasCategoryBO implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pk_category_id")
    private Integer pkCategoryId;
    @Basic(optional = false)
    @Column(name = "category_name")
    private String categoryName;
    @Basic(optional = false)
    @Column(name = "category_short_name")
    private String categoryShortName;
    @Basic(optional = false)
    @Column(name = "file_upload_mandatory")
    private String fileUploadMandatory;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkCategoryId")
//    private Collection<ExpDatExpenseBO> ExpDatExpenseBOCollection;

    public ExpMasCategoryBO() {
    }

    public ExpMasCategoryBO(Integer pkCategoryId) {
        this.pkCategoryId = pkCategoryId;
    }

    public ExpMasCategoryBO(Integer pkCategoryId, String categoryName, String categoryShortName) {
        this.pkCategoryId = pkCategoryId;
        this.categoryName = categoryName;
        this.categoryShortName = categoryShortName;
    }
    public ExpMasCategoryBO( String categoryName, String categoryShortName) {
        this.categoryName = categoryName;
        this.categoryShortName = categoryShortName;
    }

    public Integer getPkCategoryId() {
        return pkCategoryId;
    }

    public void setPkCategoryId(Integer pkCategoryId) {
        this.pkCategoryId = pkCategoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryShortName() {
        return categoryShortName;
    }

    public void setCategoryShortName(String categoryShortName) {
        this.categoryShortName = categoryShortName;
    }

    

    public String getFileUploadMandatory() {
		return fileUploadMandatory;
	}

	public void setFileUploadMandatory(String fileUploadMandatory) {
		this.fileUploadMandatory = fileUploadMandatory;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (pkCategoryId != null ? pkCategoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExpMasCategoryBO)) {
            return false;
        }
        ExpMasCategoryBO other = (ExpMasCategoryBO) object;
        if ((this.pkCategoryId == null && other.pkCategoryId != null) || (this.pkCategoryId != null && !this.pkCategoryId.equals(other.pkCategoryId))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "ExpMasCategoryBO [pkCategoryId=" + pkCategoryId + ", categoryName=" + categoryName
				+ ", categoryShortName=" + categoryShortName + ", fileUploadMandatory=" + fileUploadMandatory + "]";
	}

	 

    
}
