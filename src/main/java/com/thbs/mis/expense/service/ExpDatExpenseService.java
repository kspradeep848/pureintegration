package com.thbs.mis.expense.service;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.DatForexConversionsBO;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.DatForexConversionsRepository;
import com.thbs.mis.expense.bean.ExpDatExpenseBean;
import com.thbs.mis.expense.bo.ExpDatExpenseBO;
import com.thbs.mis.expense.bo.ExpMasCategoryBO;
import com.thbs.mis.expense.bo.ExpMileageRatesBO;
import com.thbs.mis.expense.constants.ExpenseConstants;
import com.thbs.mis.expense.dao.ExpDatExpenseRepository;
import com.thbs.mis.expense.dao.ExpDatReportRepository;
import com.thbs.mis.expense.dao.ExpMasCategoryRepository;
import com.thbs.mis.expense.dao.ExpMasStatusRepository;
import com.thbs.mis.expense.dao.ExpMileageRatesRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.report.Util.DownloadFileUtil;

@Configuration
@PropertySource("classpath:mis_expense.properties")
@Service
public class ExpDatExpenseService {

	List<ExpDatExpenseBean> expDatExpenseBeanList;

	List<ExpDatExpenseBO> expDatExpenseBOList;

	ExpDatExpenseBean expDatExpenseBean;

	ExpDatExpenseBO expDatExpenseBO;

	@Autowired
	ExpDatExpenseRepository expDatExpenseRepository;

	@Autowired
	ExpDatReportRepository expDatReportRepository;

	@Autowired
	ExpMileageRatesRepository expMileageRatesRepository;

	@Value("${expense.upload.directory}")
	private String EXPENSE_RECEIPT_UPLOAD_DIRECTORY;

	private static final AppLog LOG = LogFactory.getLog(ExpDatExpenseService.class);

	@Autowired
	DatEmployeeProfessionalRepository datEmployeeProfessionalRepository;

	@Autowired
	private DatForexConversionsRepository datForexConversionsRepository;

	@Autowired
	ExpMasCategoryRepository expMasCategoryRepository;

	@Autowired
	ExpMasStatusRepository expMasStatusRepository;
	Integer id = 0;
	Integer MileageAllowanceRelief = 11;

	public ExpDatExpenseBO createOrUpdateExpense(ExpDatExpenseBO expDatExpenseBO, MultipartFile receiptFile)
			throws IOException, CommonCustomException {
		List<DatForexConversionsBO> detailsBeanList = new ArrayList<DatForexConversionsBO>();
		try {
			if (receiptFile != null)
				if (receiptFile.getSize() > 1000000) {
					throw new CommonCustomException("File size is more than 1 MB");
				}
			DatEmpProfessionalDetailBO prop = datEmployeeProfessionalRepository
					.findByFkMainEmpDetailId(expDatExpenseBO.getFkEmpId());
			if (prop == null)
				throw new CommonCustomException("invalid employee id");
			else {
				Double amount = 0.0;
				Double isTaxAmount = 0.0;
				Double expenseAmount = 0.0;
				Short THBSUK = 1;
				Short THBSINDIA = 2;
				Short THBSCHINA = 3;
				Short THBSUSA = 4;
				Short THBSEUROPE = 5;
				Short THBSMIDEAST = 6;
				this.getvalidateBo(expDatExpenseBO);
				if (expDatExpenseBO.getFileName() != null) {
					String localFileName = receiptFile.getOriginalFilename().replaceAll("\\s+", "");
					expDatExpenseBO.setFileName(localFileName);
				}
				if (expDatExpenseBO.getIsTaxInclusive() == 0) {
					expenseAmount = expDatExpenseBO.getExpenseAmount();
					isTaxAmount = expDatExpenseBO.getTaxAmount();
					amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
				} else if (expDatExpenseBO.getIsTaxInclusive() == 1) {
					expenseAmount = expDatExpenseBO.getExpenseAmount();
					expDatExpenseBO.setTaxAmount(null);
					amount = expenseAmount.doubleValue();
				} else {
					throw new CommonCustomException("only 0 and 1 are the input");
				}
				// Double amount = expDatExpenseBO.getExpenseAmount();
				Byte currency = expDatExpenseBO.getCurrencyType().byteValue();
				Byte paymentCurrency = null;
				if (prop.getFkEmpOrganizationId() == THBSUK) {
					paymentCurrency = 3;
				} else if (prop.getFkEmpOrganizationId() == THBSINDIA) {
					paymentCurrency = 1;
				} else if (prop.getFkEmpOrganizationId() == THBSCHINA) {
					paymentCurrency = 3;
				} else if (prop.getFkEmpOrganizationId() == THBSUSA) {
					paymentCurrency = 2;
				} else if (prop.getFkEmpOrganizationId() == THBSEUROPE) {
					paymentCurrency = 4;
				} else if (prop.getFkEmpOrganizationId() == THBSMIDEAST) {
					paymentCurrency = 6;
				}
				Byte lastValue = paymentCurrency;
				expDatExpenseBO.setEmpOrg(prop.getFkEmpOrganizationId().shortValue());
				Date tempDate = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				tempDate = sdf.parse(sdf.format(expDatExpenseBO.getExpenseDate()));
				detailsBeanList = datForexConversionsRepository.findDetailsByDates(tempDate);
				if (expDatExpenseBO.getFkCategoryId().intValue() == MileageAllowanceRelief) {
					if (prop.getFkEmpOrganizationId() == THBSUK) {
						expDatExpenseBO.setCurrencyType(3);
						expDatExpenseBO.setConvertedAmountToPayroll(expDatExpenseBO.getExpenseAmount());
						expDatExpenseBO.setFinalExpAmount(expDatExpenseBO.getExpenseAmount());
						expDatExpenseBO.setExpenseAmount(expDatExpenseBO.getExpenseAmount());
					} else {
						throw new CommonCustomException("The employee is not mapped with the UK Organisation");
					}

				} else {
					if (detailsBeanList.isEmpty()) {
						throw new CommonCustomException("No Currency conversion value in DB for the given Date");
					} else {
						DatForexConversionsBO convertBo = detailsBeanList.stream()
								.filter(obj -> obj.getFkForexCurrencyTypeId().equals(lastValue)).findFirst().get();
						DatForexConversionsBO datForexConversionsBO = detailsBeanList.stream()
								.filter(obj -> obj.getFkForexCurrencyTypeId().equals(currency)).findFirst().get();
						switch (prop.getFkEmpOrganizationId()) {
						case 1:
							expDatExpenseBO.setConvertedAmountToPayroll(amount
									* datForexConversionsBO.getForexExchangeRate() / convertBo.getForexExchangeRate());
							break;
						case 2:
							expDatExpenseBO.setConvertedAmountToPayroll(amount
									* datForexConversionsBO.getForexExchangeRate() / convertBo.getForexExchangeRate());
							break;
						case 3:
							expDatExpenseBO.setConvertedAmountToPayroll(amount
									* datForexConversionsBO.getForexExchangeRate() / convertBo.getForexExchangeRate());
							break;
						case 4:
							expDatExpenseBO.setConvertedAmountToPayroll(amount
									* datForexConversionsBO.getForexExchangeRate() / convertBo.getForexExchangeRate());
							break;
						case 5:
							expDatExpenseBO.setConvertedAmountToPayroll(amount
									* datForexConversionsBO.getForexExchangeRate() / convertBo.getForexExchangeRate());
							break;
						case 6:
							expDatExpenseBO.setConvertedAmountToPayroll(amount
									* datForexConversionsBO.getForexExchangeRate() / convertBo.getForexExchangeRate());
							break;
						}
					}
					expDatExpenseBO.setFinalExpAmount(amount);
				}
				expDatExpenseBO.setFkStatusId(ExpenseConstants.Expense_UNATTACHED);
				expDatExpenseBO.setFkBuid(prop.getFkEmpBuUnit().intValue());
				expDatExpenseBO.setFkLocOnsiteId(prop.getFkEmpLocationPhysicalId().intValue());
				expDatExpenseBO.setFkLocBaseId(prop.getFkEmpLocationParentId().intValue());
				expDatExpenseBO = expDatExpenseRepository.save(expDatExpenseBO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		ExpMasCategoryBO bo = expMasCategoryRepository.findByPkCategoryId(expDatExpenseBO.getFkCategoryId());
		if (expDatExpenseBO != null) {
			if (bo.getFileUploadMandatory().equalsIgnoreCase("YES")) {
				String localFileName = receiptFile.getOriginalFilename().replaceAll("\\s+", "");
				String fileName1 = "EXP@" + expDatExpenseBO.getFkEmpId() + "@" + expDatExpenseBO.getPkExpenseId() + "^"
						+ localFileName;
				String name = fileName1;
				expDatExpenseBO.setPathName(name);
				expDatExpenseBO.setPkExpenseId(expDatExpenseBO.getPkExpenseId());
				expDatExpenseBO = expDatExpenseRepository.save(expDatExpenseBO);
				boolean result = this.upload(expDatExpenseBO.getFkEmpId(), expDatExpenseBO.getPkExpenseId(),
						receiptFile);
				if (result == false)
					throw new CommonCustomException(
							"expense data saved but upload failed.Please contact administrator.");
			} else {
				if (receiptFile != null) {
					String localFileName = receiptFile.getOriginalFilename().replaceAll("\\s+", "");
					String fileName1 = "EXP@" + expDatExpenseBO.getFkEmpId() + "@" + expDatExpenseBO.getPkExpenseId()
							+ "^" + localFileName;
					String name = fileName1;
					expDatExpenseBO.setPathName(name);
					expDatExpenseBO.setPkExpenseId(expDatExpenseBO.getPkExpenseId());
					expDatExpenseBO = expDatExpenseRepository.save(expDatExpenseBO);
					boolean result = this.upload(expDatExpenseBO.getFkEmpId(), expDatExpenseBO.getPkExpenseId(),
							receiptFile);
					if (result == false)
						throw new CommonCustomException(
								"expense data saved but upload failed.Please contact administrator.");
				} else {
					expDatExpenseBO.setPathName(null);
				}

			}

		}
		return expDatExpenseBO;
	}

	public boolean upload(Integer empId, Integer expenseId, MultipartFile file)
			throws IOException, CommonCustomException {
		boolean result = false;
		try {
			String localFileName = file.getOriginalFilename().replaceAll("\\s+", "");
			String fileName = "EXP@" + empId + "@" + expenseId + "^" + localFileName;
			// File directory = new File(EXPENSE_RECEIPT_UPLOAD_DIRECTORY);
			// if(directory.exists() == false)
			// directory.mkdir();
			File saveFile = new File(EXPENSE_RECEIPT_UPLOAD_DIRECTORY, fileName);
			FileUtils.writeByteArrayToFile(saveFile, file.getBytes());
			result = true;
		} catch (Exception e) {
			result = false;
			throw new CommonCustomException("" + file.getOriginalFilename() + " saved failed");
		}
		return result;

	}

	void getvalidateBo(ExpDatExpenseBO inputBean) throws CommonCustomException {
		if (Optional.ofNullable(inputBean.getExpenseAmount()).isPresent() == false)
			throw new CommonCustomException("expense Amount is Manditory");
		if (Optional.ofNullable(inputBean.getMerchant()).isPresent() == false)
			throw new CommonCustomException("merchant is Manditory");
		if (Optional.ofNullable(inputBean.getExpenseDate()).isPresent() == false)
			throw new CommonCustomException("expense Date is Manditory");
		if (Optional.ofNullable(inputBean.getExpenseDesc()).isPresent() == false)
			throw new CommonCustomException("expense Description is Manditory");
		if (Optional.ofNullable(inputBean.getCurrencyType()).isPresent() == false)
			throw new CommonCustomException("currency Type is Manditory");
		if (Optional.ofNullable(inputBean.getFkStatusId()).isPresent() == false)
			throw new CommonCustomException("status is Manditory");
		if (Optional.ofNullable(inputBean.getIsTaxInclusive()).isPresent() == false)
			throw new CommonCustomException("is Tax Inclusive field is Manditory");
		// if (Optional.ofNullable(inputBean.getFileName()).isPresent() == false)
		// throw new CommonCustomException("file Name is Manditory");
		if (Optional.ofNullable(inputBean.getFkCategoryId()).isPresent() == false)
			throw new CommonCustomException("Category Name is Manditory");
	}

	public List<ExpDatExpenseBean> getEmployeeExpenseById(Integer empId) throws IOException {
		List<ExpDatExpenseBO> expList = expDatExpenseRepository.findAllEmpId(empId);
		List<ExpDatExpenseBean> output = new ArrayList<ExpDatExpenseBean>();
		for (ExpDatExpenseBO bo : expList) {
			ExpDatExpenseBean bean = new ExpDatExpenseBean();
			bean.setCurrencyType(bo.getCurrencyType());
			bean.setExpenseAmount(bo.getExpenseAmount());
			bean.setExpenseDate(bo.getExpenseDate());
			bean.setExpenseDesc(bo.getExpenseDesc());
			bean.setFinanceApprovalDate(bo.getFinanceApprovalDate());
			bean.setFkBuId(bo.getFkBuid());
			bean.setFkCategoryId(bo.getFkCategoryId());
			bean.setFkEmpId(bo.getFkEmpId());
			bean.setFkLocBaseId(bo.getFkLocBaseId());
			bean.setFkLocOnsiteId(bo.getFkLocOnsiteId());
			bean.setFkStatusId(bo.getFkStatusId());
			bean.setMerchant(bo.getMerchant());
			bean.setManagerApprovalDate(bo.getManagerApprovalDate());
			bean.setPkExpenseId(bo.getPkExpenseId());
			bean.setFileName(bo.getFileName());
			bean.setPathName(bo.getPathName());
			// bean.setReceiptPath(getFilePath(bo.getFkEmpId(), bo.getPkExpenseId()));
			bean.setReceiptPath(bo.getFileName());
			bean.setExpMileageRatesBO(bo.getExpMileageRatesBO());
			bean.setFkMileageId(bo.getFkMileageId());
			bean.setMiles(bo.getMiles());
			bean.setCategoryName(expMasCategoryRepository.findByPkCategoryId(bo.getFkCategoryId()).getCategoryName());
			bean.setTaxAmount(bo.getTaxAmount());
			bean.setIsTaxInclusive(bo.getIsTaxInclusive());
			bean.setConvertedAmountToPayroll(bo.getConvertedAmountToPayroll());
			bean.setFinalExpAmount(bo.getFinalExpAmount());
			Date today = new Date();
			long difference = today.getTime() - bo.getExpenseDate().getTime();
			Integer daysBetween = (int) (difference / (1000 * 60 * 60 * 24));
			bean.setDaysLeft(daysBetween);
			bean.setWorkorder(bo.getWorkorder());
			bean.setCategoryBo(expMasCategoryRepository.findByPkCategoryId(bo.getFkCategoryId()));
			bean.setExpMasStatusBO(expMasStatusRepository.findByPkStatusId(bo.getFkStatusId()));
			bean.setBilllableToClient(bo.getBilllableToClient());
			if (bo.getFkReportId() != null) {
				bean.setFkReportId(bo.getFkReportId());
				bean.setReportName(expDatReportRepository.findByPkReportId(bo.getFkReportId()).getReportTitle());
				bean.setReportTitle(expDatReportRepository.findByPkReportId(bo.getFkReportId()).getReportTitle());
			}
			output.add(bean);
		}
		Collections.sort(output, new Comparator<ExpDatExpenseBean>() {
			public int compare(ExpDatExpenseBean o1, ExpDatExpenseBean o2) {
				return o2.getPkExpenseId().compareTo(o1.getPkExpenseId());
			}
		});
		return output;
	}

	String getFilePath(Integer empId, Integer expenseId) {
		File[] files = new File(EXPENSE_RECEIPT_UPLOAD_DIRECTORY).listFiles();
		String searchFile = "EXP@" + empId + "@" + expenseId;
		String filePath = "";
		for (File file : files) {
			if (file.isFile()) {
				if (file.getName().contains(searchFile)) {
					filePath = file.getAbsolutePath();
				}
			}
		}
		return filePath;
	}

	public ExpDatExpenseBO rmWorkOrderSelection(ExpDatExpenseBean inputBean) throws IOException, CommonCustomException {
		try {
			if (inputBean.getPkExpenseId() != null) {
				expDatExpenseBO = new ExpDatExpenseBO();
				expDatExpenseBO = expDatExpenseRepository.findByPkExpenseId(inputBean.getPkExpenseId());
				if (inputBean.getWorkorder() != null && inputBean.getBilllableToClient() != null) {
					expDatExpenseBO.setPkExpenseId(inputBean.getPkExpenseId());
					expDatExpenseBO.setBilllableToClient(inputBean.getBilllableToClient());
					expDatExpenseBO.setWorkorder(inputBean.getWorkorder());
					expDatExpenseRepository.save(expDatExpenseBO);
				} else {
					throw new CommonCustomException("work Order and Billable/not is mandatory");
				}
			} else {
				throw new CommonCustomException("expense Id is mandatory");
			}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		return expDatExpenseBO;
	}

	public Integer deleteReceipt(ExpDatExpenseBean inputBean) throws IOException, CommonCustomException {
		try {
			if (inputBean.getListOfExpenseId() != null && inputBean.getListOfExpenseId().size() > 0) {
				List<ExpDatExpenseBO> expDatExpenseBO = expDatExpenseRepository
						.findByPkExpenseIdIn(inputBean.getListOfExpenseId());
				if (expDatExpenseBO.size() > 0) {
					expDatExpenseBO.stream().forEach(obj -> {
						if (obj.getFkReportId() == null
								&& obj.getFkStatusId().equals(ExpenseConstants.Expense_UNATTACHED)) {
							id = expDatExpenseRepository.deleteExpense(obj.getPkExpenseId(),
									ExpenseConstants.Expense_DELETED);
						}
					});
				} else {
					throw new CommonCustomException("Expense id are not available");
				}
			} else {
				throw new CommonCustomException("Expense Id is mandatory");
			}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		return id;
	}

	public boolean getImageDownload(ExpDatExpenseBean inputBean, HttpServletResponse response)
			throws IOException, CommonCustomException {
		boolean result = false;
		try {
			if (inputBean.getFkEmpId() != null && inputBean.getPkExpenseId() != null) {
				expDatExpenseBO = new ExpDatExpenseBO();
				expDatExpenseBO = expDatExpenseRepository.findByPkExpenseId(inputBean.getPkExpenseId());
				if (expDatExpenseBO != null) {
					String download = expDatExpenseBO.getFileName();
					String basePath = EXPENSE_RECEIPT_UPLOAD_DIRECTORY;
					// String basePath = "D:/opt/Documents/Expense_Docs/";
					String fileName = "EXP@" + inputBean.getFkEmpId() + "@" + inputBean.getPkExpenseId() + "^"
							+ download;
					DownloadFileUtil.downloadFile(response, basePath, fileName);
					result = true;
				} else {
					throw new CommonCustomException("Not a valid receipt");
				}
			} else {
				throw new CommonCustomException("Emp id and expense id is mandatory");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		return result;
	}

	public ExpDatExpenseBO updateExpense(ExpDatExpenseBean inputBean) throws IOException, CommonCustomException {
		try {
			List<DatForexConversionsBO> detailsBeanList = new ArrayList<DatForexConversionsBO>();
			if (inputBean.getPkExpenseId() != null) {
				expDatExpenseBO = new ExpDatExpenseBO();
				expDatExpenseBO = expDatExpenseRepository.findByPkExpenseId(inputBean.getPkExpenseId());
				if (expDatExpenseBO != null) {
					if (expDatExpenseBO.getFkStatusId().equals(ExpenseConstants.Expense_CANCELLED_BY_SUBMITTER)
							|| expDatExpenseBO.getFkStatusId().equals(ExpenseConstants.Expense_DELETED)
							|| expDatExpenseBO.getFkStatusId().equals(ExpenseConstants.Expense_EXPIRED)
							|| expDatExpenseBO.getFkStatusId().equals(ExpenseConstants.Expense_FINANCE_APPROVED)
							|| expDatExpenseBO.getFkStatusId().equals(ExpenseConstants.Expense_FINANCE_REJECTED)
							|| expDatExpenseBO.getFkStatusId().equals(ExpenseConstants.Expense_RM_APPROVED)
							|| expDatExpenseBO.getFkStatusId().equals(ExpenseConstants.Expense_RM_REJECTED)) {
						throw new CommonCustomException("This Expense can not be edited");
					} else {
						DatEmpProfessionalDetailBO prop = datEmployeeProfessionalRepository
								.findByFkMainEmpDetailId(expDatExpenseBO.getFkEmpId());
						expDatExpenseBO.setPkExpenseId(inputBean.getPkExpenseId());
						// expDatExpenseBO.setFkCategoryId(inputBean.getFkCategoryId());
						expDatExpenseBO.setExpenseAmount(inputBean.getExpenseAmount());
						expDatExpenseBO.setMerchant(inputBean.getMerchant());
						expDatExpenseBO.setExpenseDesc(inputBean.getExpenseDesc());
						expDatExpenseBO.setFkLocBaseId(prop.getFkEmpLocationParentId().intValue());
						expDatExpenseBO.setFkLocOnsiteId(prop.getFkEmpLocationPhysicalId().intValue());
						expDatExpenseBO.setFkBuid(prop.getFkEmpBuUnit().intValue());
						expDatExpenseBO.setCurrencyType(inputBean.getCurrencyType());
						expDatExpenseBO.setIsTaxInclusive(inputBean.getIsTaxInclusive());
						expDatExpenseBO.setTaxAmount(inputBean.getTaxAmount());
						Short THBSUK = 1;
						Short THBSINDIA = 2;
						Short THBSCHINA = 3;
						Short THBSUSA = 4;
						Short THBSEUROPE = 5;
						Short THBSMIDEAST = 6;
						Double amount = 0.0;
						Double isTaxAmount = 0.0;
						Double expenseAmount = 0.0;
						if (inputBean.getIsTaxInclusive() == 0) {
							expenseAmount = inputBean.getExpenseAmount();
							isTaxAmount = inputBean.getTaxAmount();
							amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
						} else {
							expenseAmount = inputBean.getExpenseAmount();
							amount = expenseAmount.doubleValue();
						}
						expDatExpenseBO.setFinalExpAmount(amount);
						// Double amount = inputBean.getExpenseAmount();
						Byte currency = inputBean.getCurrencyType().byteValue();
						Byte paymentCurrency = null;
						if (prop.getFkEmpOrganizationId() == THBSUK) {
							paymentCurrency = 3;
						} else if (prop.getFkEmpOrganizationId() == THBSINDIA) {
							paymentCurrency = 1;
						} else if (prop.getFkEmpOrganizationId() == THBSCHINA) {
							paymentCurrency = 3;
						} else if (prop.getFkEmpOrganizationId() == THBSUSA) {
							paymentCurrency = 2;
						} else if (prop.getFkEmpOrganizationId() == THBSEUROPE) {
							paymentCurrency = 4;
						} else if (prop.getFkEmpOrganizationId() == THBSMIDEAST) {
							paymentCurrency = 6;
						}
						Byte lastValue = paymentCurrency;
						expDatExpenseBO.setEmpOrg(prop.getFkEmpOrganizationId().shortValue());
						Date tempDate = new Date();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						tempDate = sdf.parse(sdf.format(expDatExpenseBO.getExpenseDate()));
						detailsBeanList = datForexConversionsRepository.findDetailsByDates(tempDate);
						if (expDatExpenseBO.getFkCategoryId().intValue() == MileageAllowanceRelief) {
							if (prop.getFkEmpOrganizationId() == THBSUK) {
								expDatExpenseBO.setCurrencyType(3);
								expDatExpenseBO.setMiles(inputBean.getMiles());
								expDatExpenseBO.setFkMileageId(inputBean.getFkMileageId());
								expDatExpenseBO.setConvertedAmountToPayroll(inputBean.getExpenseAmount());
								expDatExpenseBO.setFinalExpAmount(inputBean.getExpenseAmount());
								expDatExpenseBO.setExpenseAmount(inputBean.getExpenseAmount());
							} else {
								throw new CommonCustomException("The employee is not mapped with the UK Organisation");
							}

						} else {
							if (detailsBeanList.isEmpty()) {
								throw new CommonCustomException("No currency conversion value in DB for selected Date");
							} else {
								DatForexConversionsBO convertBo = detailsBeanList.stream()
										.filter(obj -> obj.getFkForexCurrencyTypeId().equals(lastValue)).findFirst()
										.get();
								DatForexConversionsBO datForexConversionsBO = detailsBeanList.stream()
										.filter(obj -> obj.getFkForexCurrencyTypeId().equals(currency)).findFirst()
										.get();
								switch (prop.getFkEmpOrganizationId()) {
								case 1:
									expDatExpenseBO.setConvertedAmountToPayroll(
											amount * datForexConversionsBO.getForexExchangeRate()
													/ convertBo.getForexExchangeRate());
									break;
								case 2:
									expDatExpenseBO.setConvertedAmountToPayroll(
											amount * datForexConversionsBO.getForexExchangeRate()
													/ convertBo.getForexExchangeRate());
									break;
								case 3:
									expDatExpenseBO.setConvertedAmountToPayroll(
											amount * datForexConversionsBO.getForexExchangeRate()
													/ convertBo.getForexExchangeRate());
									break;
								case 4:
									expDatExpenseBO.setConvertedAmountToPayroll(
											amount * datForexConversionsBO.getForexExchangeRate()
													/ convertBo.getForexExchangeRate());
									break;
								case 5:
									expDatExpenseBO.setConvertedAmountToPayroll(
											amount * datForexConversionsBO.getForexExchangeRate()
													/ convertBo.getForexExchangeRate());
									break;
								case 6:
									expDatExpenseBO.setConvertedAmountToPayroll(
											amount * datForexConversionsBO.getForexExchangeRate()
													/ convertBo.getForexExchangeRate());
									break;
								}
							}
						}
						// detailsBeanList = datForexConversionsRepository
						// .findDetailsByDates(expDatExpenseBO.getExpenseDate());
						expDatExpenseRepository.save(expDatExpenseBO);
					}
				} else {
					throw new CommonCustomException("Expense Id is not available in DB");
				}
			} else {
				throw new CommonCustomException("expense Id is mandatory");
			}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		return expDatExpenseBO;
	}

	public List<ExpMileageRatesBO> getAllMileageRates() {
		List<ExpMileageRatesBO> expMileageRatesBO = new ArrayList<ExpMileageRatesBO>();
		expMileageRatesBO = expMileageRatesRepository.findAll();
		return expMileageRatesBO;
	}
	// Integer daysBetweenDates(LocalDate startDate,LocalDate endDate ){
	// LocalDate dateBefore;
	// LocalDate dateAfter;
	// long daysBetween = DAYS.between(dateBefore, dateAfter);
	// return 0;
	// }
}