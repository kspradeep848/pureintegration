package com.thbs.mis.expense.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbs.mis.expense.bean.ExpMasCategoryBean;
import com.thbs.mis.expense.bo.ExpMasCategoryBO;
import com.thbs.mis.expense.constants.ExpenseMessageConstant;
import com.thbs.mis.expense.dao.ExpMasCategoryRepository;
import com.thbs.mis.framework.exception.CommonCustomException;

@Service
public class ExpMasCategoryService {
	@Autowired
	ExpMasCategoryRepository expCategoryRepository;

	List<ExpMasCategoryBO> result;
	ExpMasCategoryBO inputBo;

	public List<ExpMasCategoryBO> getExpCategory() throws CommonCustomException {
		this.result = new ArrayList<ExpMasCategoryBO>();
		try {
			this.result = expCategoryRepository.findAll();
			return result;
		} catch (Exception e) {
			throw new CommonCustomException("Error occured " + e);
		}
	}

	public List<ExpMasCategoryBO> findByExpenseCategory(ExpMasCategoryBean inputBean) throws CommonCustomException {
		this.result = new ArrayList<ExpMasCategoryBO>();
		try {
			if (inputBean.getPkCategoryId() != null) {
				this.inputBo = expCategoryRepository.findByPkCategoryId(inputBean.getPkCategoryId());
				this.result.add(this.inputBo);
			} else {
				if (inputBean.getCategoryShortName() != null && inputBean.getCategoryShortName() != "") {
					if (inputBean.getCategoryShortName().length() > 0) {
						this.result = expCategoryRepository
								.findByCategoryShortNameContaining(inputBean.getCategoryShortName());
					} else {
						throw new CommonCustomException("Invalid category shortName ");
					}
				} else if (inputBean.getCategoryName() != null) {
					this.result = expCategoryRepository.findByCategoryNameContaining(inputBean.getCategoryName());
				}
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<ExpMasCategoryBO> updateCategory(ExpMasCategoryBean inputBean) throws CommonCustomException {
		this.result = new ArrayList<ExpMasCategoryBO>();
		try {
			if (inputBean.getPkCategoryId() == null) {
				this.inputBo = new ExpMasCategoryBO(inputBean.getCategoryName(), inputBean.getCategoryShortName());
				if (this.inputBo != null) {
					this.inputBo = expCategoryRepository.save(this.inputBo);
					this.result.add(this.inputBo);
				} else {
					throw new CommonCustomException(ExpenseMessageConstant.EXP_CATEGORY_SAVED_DB_FAILED);
				}
			} else {
				this.inputBo = new ExpMasCategoryBO(inputBean.getPkCategoryId(), inputBean.getCategoryName(),
						inputBean.getCategoryShortName());
				if (this.inputBo != null) {
					this.inputBo = expCategoryRepository.save(this.inputBo);
					this.result.add(this.inputBo);
				} else {
					throw new CommonCustomException(ExpenseMessageConstant.EXP_CATEGORY_SAVED_DB_FAILED);
				}
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}