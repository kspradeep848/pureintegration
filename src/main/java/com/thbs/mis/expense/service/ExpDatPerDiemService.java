package com.thbs.mis.expense.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.jmapper.JMapper;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.expense.bean.ExpDatPerDiemBean;
import com.thbs.mis.expense.bo.ExpDatPerDiemBO;
import com.thbs.mis.expense.bo.ExpMasPerDiemCountryRateBO;
import com.thbs.mis.expense.constants.ExpenseConstants;
import com.thbs.mis.expense.dao.ExpDatPerDiemRepository;
import com.thbs.mis.expense.dao.ExpMasPerDiemCountryRateRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

@Service
public class ExpDatPerDiemService {

	List<ExpDatPerDiemBean> expDatPerDiemBeanList;

	List<ExpDatPerDiemBO> expDatPerDiemBOList;

	ExpDatPerDiemBean expDatPerDiemBean;

	ExpDatPerDiemBO expDatPerDiemBO;

	@Autowired
	ExpDatPerDiemRepository expDatPerDiemRepository;

	@Autowired
	private CommonService commonService;

	@Autowired
	private DatEmployeeProfessionalRepository datEmployeeProfessionalRepository;

	@Autowired
	private EmployeePersonalDetailsRepository employeePersonalDetailRepo;

	@Autowired
	private ExpMasPerDiemCountryRateRepository expMasPerDiemCountryRateRepository;

	private static final AppLog LOG = LogFactory.getLog(ExpDatPerDiemService.class);

	JMapper<ExpDatPerDiemBean, ExpDatPerDiemBO> expDatReportBeanMapper = new JMapper<>(ExpDatPerDiemBean.class,
			ExpDatPerDiemBO.class);

	public String getEmployeeName(Integer empId) {
		DatEmpPersonalDetailBO empData = employeePersonalDetailRepo.findByFkEmpDetailId(empId);
		return empData.getEmpFirstName() + " " + empData.getEmpLastName();
	}

	boolean isValidEmpId(Integer empId) throws DataAccessException, CommonCustomException {
		DatEmpProfessionalDetailBO employeeInfo = new DatEmpProfessionalDetailBO();
		boolean result = true;
		if (empId < 0) {
			result = false;
			throw new CommonCustomException("Empid should not to be less than 1");
		} else {
			employeeInfo = commonService.getEmpoyeeDeatailsById(empId);
			if (employeeInfo == null) {
				result = false;
				throw new CommonCustomException("Invalid emp id");
			} else {
				result = true;
			}
		}
		return result;
	}

	List<ExpDatPerDiemBean> viewConvertedList(List<ExpDatPerDiemBO> boList) {
		this.expDatPerDiemBeanList = new ArrayList<ExpDatPerDiemBean>();
		if (Optional.ofNullable(boList).isPresent()) {
			boList.stream().forEach(obj -> {
				this.expDatPerDiemBean = new ExpDatPerDiemBean();
				this.expDatPerDiemBean = expDatReportBeanMapper.getDestination(this.expDatPerDiemBean, obj);
				this.expDatPerDiemBean.setSubmitterName(this.getEmployeeName(obj.getFkEmpId()));
				if (obj.getFinApproverId() != null)
					this.expDatPerDiemBean.setFinApproverName(this.getEmployeeName(obj.getFinApproverId()));
				this.expDatPerDiemBeanList.add(this.expDatPerDiemBean);
			});
			return this.expDatPerDiemBeanList;
		} else {
			return this.expDatPerDiemBeanList;
		}
	}

	public List<ExpDatPerDiemBean> viewExpensePerdiem(ExpDatPerDiemBean inputBean) throws CommonCustomException {
		this.expDatPerDiemBOList = new ArrayList<>();
		if (inputBean.getFkEmpId() != null) {
			this.expDatPerDiemBOList = expDatPerDiemRepository.findByFkEmpId(inputBean.getFkEmpId());
			return viewConvertedList(this.expDatPerDiemBOList);
		}
		return expDatPerDiemBeanList;
	}

	public List<ExpDatPerDiemBean> viewAllExpensePerdiem(ExpDatPerDiemBean inputBean) throws CommonCustomException {
		if (inputBean.getStartDate() != null && inputBean.getEndDate() != null) {
			SimpleDateFormat sdfWithSeconds = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String stringFromDate = sdf.format(inputBean.getStartDate());
			String stringToDate = sdf.format(inputBean.getEndDate());
			stringFromDate = stringFromDate + " 00:00:00";
			stringToDate = stringToDate + " 23:59:59";
			Date fromDate = new Date();
			Date toDate = new Date();
			try {
				fromDate = sdfWithSeconds.parse(stringFromDate);
				toDate = sdfWithSeconds.parse(stringToDate);
			} catch (ParseException e) {
				e.printStackTrace();
				throw new CommonCustomException(e.getMessage());
			}
			this.expDatPerDiemBOList = new ArrayList<>();
			this.expDatPerDiemBOList = expDatPerDiemRepository.findAllRecordsByDate(fromDate, toDate);
			return viewConvertedList(this.expDatPerDiemBOList);
		} else
			throw new CommonCustomException("Please enter startDate and endDate");
	}

	public List<ExpDatPerDiemBean> viewDetailsByPkId(Integer pkId) throws CommonCustomException {
		this.expDatPerDiemBOList = new ArrayList<>();
		this.expDatPerDiemBO = expDatPerDiemRepository.fetchBypkId(pkId);
		this.expDatPerDiemBOList.add(this.expDatPerDiemBO);
		return viewConvertedList(this.expDatPerDiemBOList);
	}

	public List<ExpDatPerDiemBO> createExpensePerDiem(ExpDatPerDiemBean inputBean) throws CommonCustomException {
		try {
			this.expDatPerDiemBOList = new ArrayList<>();
			if (inputBean.getPkId() == null) {
				if (inputBean.getFkEmpId() == null)
					throw new CommonCustomException("Please enter login ID");
				else {
					DatEmpProfessionalDetailBO empdetails = datEmployeeProfessionalRepository
							.findByFkMainEmpDetailId(inputBean.getFkEmpId());
					// inputBean.setFkCurrencyType(empdetails.getFkEmpOrganizationId());
					this.validAllFields(inputBean);
					boolean isvalidEmpid = isValidEmpId(inputBean.getFkEmpId());
					if (isvalidEmpid)
						this.expDatPerDiemBO = new ExpDatPerDiemBO(inputBean.getFkEmpId(),
								inputBean.getFkCurrencyType(), new Date(), inputBean.getPerDiemStartDate(),
								inputBean.getNoOfDays(), inputBean.getPerDiemRate(), inputBean.getAmount(),
								inputBean.getPerDiemDesc(), inputBean.getFkCountryId(),
								inputBean.getIsWithAccomodation(), ExpenseConstants.PER_DIEM_WAITING_FOR_APPROVAL);
					else
						throw new CommonCustomException("Submitter is not a valid Employee");
				}
			} else {
				this.expDatPerDiemBO = expDatPerDiemRepository.fetchBypkId(inputBean.getPkId());
				if (this.expDatPerDiemBO == null)
					throw new CommonCustomException("Entered Per Diem Id does not exist");
				else
					this.expDatPerDiemBO = this.setValidateBo(inputBean);
			}
			this.expDatPerDiemBO = expDatPerDiemRepository.save(this.expDatPerDiemBO);
			this.expDatPerDiemBOList.add(this.expDatPerDiemBO);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		return this.expDatPerDiemBOList;
	}

	ExpDatPerDiemBO setValidateBo(ExpDatPerDiemBean inputBean) throws CommonCustomException, DataAccessException {
		if (Optional.ofNullable(inputBean.getFkEmpId()).isPresent()) {
			boolean isvalidEmpid = isValidEmpId(inputBean.getFkEmpId());
			if (isvalidEmpid)
				this.expDatPerDiemBO.setFkEmpId(inputBean.getFkEmpId());
			else
				throw new CommonCustomException("Submitter is not a valid Employee");
		}
		if (Optional.ofNullable(inputBean.getFkCurrencyType()).isPresent())
			this.expDatPerDiemBO.setFkCurrencyType(inputBean.getFkCurrencyType());
		if (Optional.ofNullable(inputBean.getPerDiemStartDate()).isPresent())
			this.expDatPerDiemBO.setPerDiemStartDate(inputBean.getPerDiemStartDate());
		if (Optional.ofNullable(inputBean.getNoOfDays()).isPresent())
			this.expDatPerDiemBO.setNoOfDays(inputBean.getNoOfDays());
		if (Optional.ofNullable(inputBean.getPerDiemRate()).isPresent())
			this.expDatPerDiemBO.setPerDiemRate(inputBean.getPerDiemRate());
		if (Optional.ofNullable(inputBean.getAmount()).isPresent())
			this.expDatPerDiemBO.setAmount(inputBean.getAmount());
		if (Optional.ofNullable(inputBean.getPerDiemStatus()).isPresent())
			this.expDatPerDiemBO.setPerDiemStatus(inputBean.getPerDiemStatus());
		if (Optional.ofNullable(inputBean.getPerDiemDesc()).isPresent())
			this.expDatPerDiemBO.setPerDiemDesc(inputBean.getPerDiemDesc());
		if (Optional.ofNullable(inputBean.getFkCountryId()).isPresent())
			this.expDatPerDiemBO.setFkCountryId(inputBean.getFkCountryId());
		if (Optional.ofNullable(inputBean.getIsWithAccomodation()).isPresent())
			this.expDatPerDiemBO.setIsWithAccomodation(inputBean.getIsWithAccomodation());
		return this.expDatPerDiemBO;
	}

	void validAllFields(ExpDatPerDiemBean inputBo) throws CommonCustomException {
		if (Optional.ofNullable(inputBo.getFkEmpId()).isPresent() == false)
			throw new CommonCustomException("Emp ID is Manditory");
		if (Optional.ofNullable(inputBo.getFkCurrencyType()).isPresent() == false)
			throw new CommonCustomException("Currecy ID is Manditory");
		if (Optional.ofNullable(inputBo.getPerDiemStartDate()).isPresent() == false)
			throw new CommonCustomException("Start Date is Manditory");
		if (Optional.ofNullable(inputBo.getNoOfDays()).isPresent() == false)
			throw new CommonCustomException("No of days is Manditory");
		if (Optional.ofNullable(inputBo.getPerDiemRate()).isPresent() == false)
			throw new CommonCustomException("Per Diem rate is Manditory");
		if (Optional.ofNullable(inputBo.getAmount()).isPresent() == false)
			throw new CommonCustomException("Total Amount is Manditory");
		if (Optional.ofNullable(inputBo.getPerDiemDesc()).isPresent() == false)
			throw new CommonCustomException("Description is Manditory");
		if (Optional.ofNullable(inputBo.getFkCountryId()).isPresent() == false)
			throw new CommonCustomException("Country ID is Manditory");
		if (Optional.ofNullable(inputBo.getIsWithAccomodation()).isPresent() == false)
			throw new CommonCustomException("Accomadation flag is Manditory");
	}

	public List<ExpDatPerDiemBean> approveOrRejPerdiem(ExpDatPerDiemBean inputBean) throws CommonCustomException {
		this.expDatPerDiemBOList = new ArrayList<>();
		if (inputBean.getPkId() != null && inputBean.getFinApproverId() != null && inputBean.getPerDiemStatus() != null
				&& inputBean.getFinComments() != null) {
			this.expDatPerDiemBO = expDatPerDiemRepository.fetchBypkId(inputBean.getPkId());
			if (this.expDatPerDiemBO != null) {
				if (this.expDatPerDiemBO.getPerDiemStatus() == ExpenseConstants.PER_DIEM_WAITING_FOR_APPROVAL) {
					if (inputBean.getPerDiemStatus() == ExpenseConstants.PER_DIEM_FINANCE_APPROVED) {
						Integer id = expDatPerDiemRepository.changePerDiemStatus(inputBean.getPkId(),
								inputBean.getFinApproverId(), ExpenseConstants.PER_DIEM_FINANCE_APPROVED,
								inputBean.getFinComments());
						if (id == 0)
							throw new CommonCustomException("Some error occured while approving the record");
						else {
							this.expDatPerDiemBOList.add(this.expDatPerDiemBO);
							return viewConvertedList(this.expDatPerDiemBOList);
						}
					} else if (inputBean.getPerDiemStatus() == ExpenseConstants.PER_DIEM_FINANCE_REJECTED) {
						Integer id = expDatPerDiemRepository.changePerDiemStatus(inputBean.getPkId(),
								inputBean.getFinApproverId(), ExpenseConstants.PER_DIEM_FINANCE_REJECTED,
								inputBean.getFinComments());
						if (id == 0)
							throw new CommonCustomException("Some error occured while approving the record");
						else {
							this.expDatPerDiemBOList.add(this.expDatPerDiemBO);
							return viewConvertedList(this.expDatPerDiemBOList);
						}
					} else {
						throw new CommonCustomException("The entered Status is not valid");
					}
				} else {
					throw new CommonCustomException("Entered Per Diem Id's status is not in waiting for approval");
				}
			} else {
				throw new CommonCustomException("Entered Per Diem Id is not exist");
			}
		} else {
			throw new CommonCustomException("Please provide the Id, Login ID, Comments and Status");
		}
	}

	public List<ExpMasPerDiemCountryRateBO> perDiemRate() throws CommonCustomException {
		List<ExpMasPerDiemCountryRateBO> expDatPerDiemBOList = new ArrayList<ExpMasPerDiemCountryRateBO>();
		expDatPerDiemBOList = expMasPerDiemCountryRateRepository.findAllByOrderByPerDiemCountryAsc();
		return expDatPerDiemBOList;
	}

}
