package com.thbs.mis.expense.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.googlecode.jmapper.JMapper;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.DatEmpRoleMappingRepository;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.expense.bean.ExpDatReportBean;
import com.thbs.mis.expense.bean.ExpDatReportHistoryBean;
import com.thbs.mis.expense.bean.ExpenseAdvanceSearchBean;
import com.thbs.mis.expense.bo.ExpDatExpenseBO;
import com.thbs.mis.expense.bo.ExpDatRecordHistoryBO;
import com.thbs.mis.expense.bo.ExpDatReportBO;
import com.thbs.mis.expense.bo.ExpReportMasStatusBO;
import com.thbs.mis.expense.constants.ExpenseConstants;
import com.thbs.mis.expense.dao.ExpDatExpenseRepository;
import com.thbs.mis.expense.dao.ExpDatRecordHistoryRepository;
import com.thbs.mis.expense.dao.ExpDatReportRepository;
import com.thbs.mis.expense.dao.ExpMasReportStatusRepository;
import com.thbs.mis.expense.dao.ExpenseAdvanceSearchSpecification;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.expensenotification.service.ExpenseNotificationService;

@Service
public class ExpDatReportService {

	@Autowired
	ExpDatReportRepository expDatReportRepository;

	@Autowired
	ExpDatExpenseRepository expDatExpenseRepository;

	@Autowired
	EmpDetailRepository empDetailRepository;

	@Autowired
	private CommonService commonService;

	@Autowired
	private EmployeePersonalDetailsRepository employeePersonalDetailRepo;

	@Autowired
	private ExpMasReportStatusRepository expMasReportStatusRepository;

	@Autowired
	private DatEmployeeProfessionalRepository datEmployeeProfessionalRepository;

	@Autowired
	private DatEmpRoleMappingRepository datEmpRoleMappingRepository;

	@Autowired
	private ExpenseNotificationService notificationService;

	@Autowired
	private ExpDatRecordHistoryRepository expDatRecordHistoryRepository;

	@Value("${finance.role.key.name}")
	private String financeRoleKeyNames;

	List<ExpDatReportBO> expDatReportBOList;

	List<ExpDatReportBean> expDatReportBeanList;

	ExpDatReportBO expDatReportBO;

	ExpDatReportBean expDatReportBean;

	ExpDatExpenseBO expDatExpenseBO;

	ExpDatRecordHistoryBO expDatRecordHistoryBO;

	List<ExpDatExpenseBO> expDatExpenseBOList;

	Boolean check = false;

	JMapper<ExpDatReportBean, ExpDatReportBO> expDatReportBeanMapper = new JMapper<>(ExpDatReportBean.class,
			ExpDatReportBO.class);

	private static final AppLog LOG = LogFactory.getLog(ExpDatReportService.class);

	boolean isValidEmpId(Integer empId) throws DataAccessException, CommonCustomException {
		DatEmpProfessionalDetailBO employeeInfo = new DatEmpProfessionalDetailBO();
		LOG.startUsecase("isValidEmpId");
		boolean result = true;
		if (empId < 0) {
			result = false;
			throw new CommonCustomException("Empid should not to be less than 1");
		} else {
			employeeInfo = commonService.getEmpoyeeDeatailsById(empId);
			if (employeeInfo == null) {
				result = false;
				throw new CommonCustomException("Invalid emp id");
			} else {
				result = true;
			}
		}
		return result;
	}

	boolean isValidRM(Integer empId) throws DataAccessException, CommonCustomException {
		DatEmpProfessionalDetailBO employeeInfo = new DatEmpProfessionalDetailBO();
		LOG.startUsecase("isValidEmpId");
		boolean result = true;
		if (empId < 0) {
			result = false;
			throw new CommonCustomException("Empid should not to be less than 1");
		} else {
			employeeInfo = commonService.getEmpoyeeDeatailsById(empId);
			if (employeeInfo == null) {
				result = false;
				throw new CommonCustomException("Invalid emp id");
			} else {
				if (employeeInfo.getHasReporteesFlag().equalsIgnoreCase("NO_REPORTEES")) {
					result = false;
					throw new CommonCustomException("Employee Id is not a Manager");
				} else {
					result = true;
				}
			}
		}
		return result;
	}

	Integer count = 0;
	Integer expCount = 0;
	Integer expRmCount = 0;

	// create Report
	public List<ExpDatReportBO> createReport(ExpDatReportBean inputBean)
			throws CommonCustomException, DataAccessException {
		this.expDatRecordHistoryBO = new ExpDatRecordHistoryBO();
		LOG.startUsecase("createReport service");
		this.expDatReportBOList = new ArrayList<ExpDatReportBO>();
		boolean isvalidEmpid = false;
		this.check = false;
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBO = new DatEmpProfessionalDetailBO();
		if (inputBean.getFkEmpIdInt() != null) {
			isvalidEmpid = isValidEmpId(inputBean.getFkEmpIdInt());
			datEmpProfessionalDetailBO = datEmployeeProfessionalRepository
					.findByFkMainEmpDetailId(inputBean.getFkEmpIdInt());
		} else {
			throw new CommonCustomException("Emp Id is mandatory");
		}
		boolean isvalidRmId = false;
		if (inputBean.getApproverMgr() != null) {
			isvalidRmId = isValidRM(inputBean.getApproverMgr());
		}
//		try {
			this.expDatReportBO = new ExpDatReportBO();
			// Update part
			if (inputBean.getPkReportId() != null) {
				this.expDatReportBO = expDatReportRepository.findByPkReportId(inputBean.getPkReportId());
				if (this.expDatReportBO == null)
					throw new CommonCustomException("Report Id does not exist");
				else {
					if (this.expDatReportBO.getFkEmpIdInt().equals(inputBean.getFkEmpIdInt())) {
						if (inputBean.getFkStatusId().equals(ExpenseConstants.Report_Draft)) {
							try {
								List<Integer> ids = new ArrayList<Integer>();
								if (inputBean.getFkExpenseIdList() != null) {
									if (inputBean.getFkExpenseIdList().size() > 0) {
										List<ExpDatExpenseBO> idCheck = expDatExpenseRepository
												.findByPkExpenseIdIn(inputBean.getFkExpenseIdList());
										if (idCheck.size() != inputBean.getFkExpenseIdList().size()) {
											throw new CommonCustomException(
													"Some of the expense attached are not available in DB");
										}
										count = 0;
										expCount = 0;
										if (!inputBean.getFkExpenseIdList().isEmpty()) {
											List<ExpDatExpenseBO> idOfList = expDatExpenseRepository
													.findByPkExpenseIdIn(inputBean.getFkExpenseIdList());
											idOfList.stream().forEach(obj -> {
												if (obj.getFkEmpId().equals(inputBean.getFkEmpIdInt())) {
													if (obj.getFkStatusId().equals(ExpenseConstants.Expense_UNATTACHED)) {
														this.check = true;
														ids.add(obj.getPkExpenseId());
													} else if (obj.getFkStatusId().equals(ExpenseConstants.Expense_ATTACHED)
															&& obj.getFkReportId().equals(inputBean.getPkReportId())) {
														this.check = true;
														ids.add(obj.getPkExpenseId());
													} else {
														this.check = false;
														count = count + 1;
													}
												} else {
													expCount = expCount + 1;
												}
											});
										}
										if (expCount > 0) {
											throw new CommonCustomException("Expense are mapped with other employee");
										}
										if (count > 0) {
											throw new CommonCustomException(
													"Please attach only the valid/unattached expense in report");
										}
									}
								}
								if (check) {
									Integer unCheck = expDatExpenseRepository.unCheckAllExpense(inputBean.getPkReportId(),
											ExpenseConstants.Expense_UNATTACHED);
									inputBean.setTotalAmount(expDatExpenseRepository.fetchByTotalAmount(ids));
									if (!inputBean.getFkExpenseIdList().isEmpty()) {
										Integer id = expDatExpenseRepository.attachExpenseInReport(
												inputBean.getFkExpenseIdList(), ExpenseConstants.Expense_ATTACHED,
												this.expDatReportBO.getPkReportId());
										if (id == 0) {
											throw new CommonCustomException(
													"Error occured while attaching expense with report");
										}
									}
								}
								if (this.expDatReportBO.getFkStatusId().equals(ExpenseConstants.Report_EmpRecalled)) {
									Short reDraftStatus = 1;
									inputBean.setRedraftedStatus(reDraftStatus);
								} else {
									inputBean.setRedraftedStatus(this.expDatReportBO.getRedraftedStatus());
								}
								inputBean.setRecallStatus(this.expDatReportBO.getRecallStatus());
								inputBean.setFkCurrencyTypeId(this.expDatReportBO.getFkCurrencyTypeId());
								inputBean.setSubmissionDate(this.expDatReportBO.getSubmissionDate());
								this.expDatReportBO = this.setValidateBo(inputBean);
								this.expDatReportBO = expDatReportRepository.save(this.expDatReportBO);
								this.expDatReportBOList.add(this.expDatReportBO);
							}catch(Exception e) {
								throw new CommonCustomException(e.getMessage());
							}
						} else if (inputBean.getFkStatusId().equals(ExpenseConstants.Report_AwatingRMApproval)) {
							try {
								List<Integer> ids = new ArrayList<Integer>();
								this.getValidateBean(inputBean);
								if (inputBean.getFkExpenseIdList() != null) {
									if (inputBean.getFkExpenseIdList().size() > 0) {
										List<ExpDatExpenseBO> idCheck = expDatExpenseRepository
												.findByPkExpenseIdIn(inputBean.getFkExpenseIdList());
										if (idCheck.size() != inputBean.getFkExpenseIdList().size()) {
											throw new CommonCustomException(
													"Some of the expense attached are not available in DB");
										}
										count = 0;
										expCount = 0;
										List<ExpDatExpenseBO> idOfList = expDatExpenseRepository
												.findByPkExpenseIdIn(inputBean.getFkExpenseIdList());
										idOfList.stream().forEach(obj -> {
											if (obj.getFkEmpId().equals(inputBean.getFkEmpIdInt())) {
												if (obj.getFkStatusId().equals(ExpenseConstants.Expense_UNATTACHED)) {
													this.check = true;
													ids.add(obj.getPkExpenseId());
												} else if (obj.getFkStatusId().equals(ExpenseConstants.Expense_ATTACHED)
														&& obj.getFkReportId().equals(inputBean.getPkReportId())) {
													this.check = true;
													ids.add(obj.getPkExpenseId());
												} else {
													this.check = false;
													count = count + 1;
												}
											} else {
												expCount = expCount + 1;
											}
										});
										if (expCount > 0) {
											throw new CommonCustomException("Expense are mapped with other employee");
										}
										if (count > 0) {
											throw new CommonCustomException(
													"Please attach only the valid/unattached expense in report");
										}
										if (check) {
											inputBean.setTotalAmount(expDatExpenseRepository.fetchByTotalAmount(ids));
											inputBean.setRecallStatus(this.expDatReportBO.getRecallStatus());
											inputBean.setRedraftedStatus(this.expDatReportBO.getRedraftedStatus());
											inputBean.setFkCurrencyTypeId(this.expDatReportBO.getFkCurrencyTypeId());
											inputBean.setSubmissionDate(this.expDatReportBO.getSubmissionDate());
											this.expDatReportBO = this.setValidateBo(inputBean);
											this.expDatReportBO = expDatReportRepository.save(this.expDatReportBO);
											this.expDatRecordHistoryBO.setFkReportId(this.expDatReportBO.getPkReportId());
											this.expDatRecordHistoryBO.setFkEmpId(this.expDatReportBO.getFkEmpIdInt());
											this.expDatRecordHistoryBO.setToEmpId(this.expDatReportBO.getApproverMgr());
											this.expDatRecordHistoryBO.setRecordDate(new Date());
											this.expDatRecordHistoryBO.setFkStatusId(this.expDatReportBO.getFkStatusId());
											this.expDatRecordHistoryBO = expDatRecordHistoryRepository
													.save(this.expDatRecordHistoryBO);
											Integer unCheck = expDatExpenseRepository.unCheckAllExpense(
													inputBean.getPkReportId(), ExpenseConstants.Expense_UNATTACHED);
											Integer id = expDatExpenseRepository.attachExpenseInReport(
													inputBean.getFkExpenseIdList(), ExpenseConstants.Expense_ATTACHED,
													this.expDatReportBO.getPkReportId());
											this.expDatReportBOList.add(this.expDatReportBO);

											if (id == 0) {
												throw new CommonCustomException(
														"Error occured while attaching expense with report");
											}
										} else {
											throw new CommonCustomException(
													"Please attach only the valid/unattached expense in report");
										}
									} else {
										throw new CommonCustomException("please attach the expense with the report");
									}
								} else {
									throw new CommonCustomException("please attach the expense with the report");
								}
								// Added by Deepa for Notifications
								try {
									notificationService.setAwaitingApprovalToRm(this.expDatReportBO.getApproverMgr());
									notificationService.setReportRecallToRmScreen(this.expDatReportBO.getApproverMgr());
								} catch (Exception e) {
									LOG.info(
											"Some Exception happened while setting expense Report Notification to RM Screen");
								}
								// End of Addition by Deepa for Notifications
							}catch(Exception e) {
								throw new CommonCustomException(e.getMessage());
							}
						}
					} else {
						throw new CommonCustomException(
								"This report belongs to " + this.expDatReportBO.getFkEmpIdInt() + " Employee ID");
					}

				}
			}
			// Create part
			else {
				if (inputBean.getFkStatusId().equals(ExpenseConstants.Report_AwatingRMApproval)) {
					try {
						this.getValidateBean(inputBean);
						if (isvalidEmpid && isvalidRmId) {
							if (inputBean.getFkExpenseIdList() != null) {
								if (inputBean.getFkExpenseIdList().size() > 0) {
									List<ExpDatExpenseBO> idCheck = expDatExpenseRepository
											.findByPkExpenseIdIn(inputBean.getFkExpenseIdList());
									List<Integer> ids = new ArrayList<Integer>();
									if (idCheck.size() != inputBean.getFkExpenseIdList().size()) {
										throw new CommonCustomException(
												"Some of the expense attached are not available in DB");
									}
									count = 0;
									expCount = 0;
									if (!inputBean.getFkExpenseIdList().isEmpty()) {
										List<ExpDatExpenseBO> idOfList = expDatExpenseRepository
												.findByPkExpenseIdIn(inputBean.getFkExpenseIdList());
										idOfList.stream().forEach(obj -> {
											if (obj.getFkEmpId().equals(inputBean.getFkEmpIdInt())) {
												if (obj.getFkStatusId().equals(ExpenseConstants.Expense_UNATTACHED)) {
													this.check = true;
													ids.add(obj.getPkExpenseId());
												} else {
													this.check = false;
													count = count + 1;
												}
											} else {
												expCount = expCount + 1;
											}
										});
									}
									if (expCount > 0) {
										throw new CommonCustomException("Expense are mapped with other employee");
									}
									if (count > 0) {
										throw new CommonCustomException(
												"Please attach only the valid/unattached expense in report");
									}
									if (check) {
										Short recallStatus = 0;
										inputBean.setTotalAmount(expDatExpenseRepository.fetchByTotalAmount(ids));
										this.expDatReportBO = new ExpDatReportBO(inputBean.getApproverMgr(),
												inputBean.getFkEmpIdInt(), inputBean.getStartDate(), inputBean.getEndDate(),
												inputBean.getReportDesc(), inputBean.getReportTitle(),
												inputBean.getTotalAmount(), inputBean.getFkStatusId(), new Date(),
												inputBean.getFkBuId(), inputBean.getFkClientId(),
												inputBean.getFkProjectId(),
												datEmpProfessionalDetailBO.getFkEmpOrganizationId(), recallStatus,
												recallStatus);
										this.expDatReportBO = expDatReportRepository.save(this.expDatReportBO);
										this.expDatRecordHistoryBO.setFkReportId(this.expDatReportBO.getPkReportId());
										this.expDatRecordHistoryBO.setFkEmpId(this.expDatReportBO.getFkEmpIdInt());
										this.expDatRecordHistoryBO.setToEmpId(this.expDatReportBO.getApproverMgr());
										this.expDatRecordHistoryBO.setRecordDate(new Date());
										this.expDatRecordHistoryBO.setFkStatusId(this.expDatReportBO.getFkStatusId());
										this.expDatRecordHistoryBO = expDatRecordHistoryRepository
												.save(this.expDatRecordHistoryBO);
										if (!inputBean.getFkExpenseIdList().isEmpty()) {
											Integer id = expDatExpenseRepository.attachExpenseInReport(
													inputBean.getFkExpenseIdList(), ExpenseConstants.Expense_ATTACHED,
													this.expDatReportBO.getPkReportId());
											if (id == 0) {
												throw new CommonCustomException(
														"Error occured while attaching expense with report");
											}
										}
										this.expDatReportBOList.add(this.expDatReportBO);
									} else {
										throw new CommonCustomException("Please attach only valid expense in the report");
									}
								} else {
									throw new CommonCustomException("please attach the expense with the report");
								}
							} else {
								throw new CommonCustomException("please attach the expense with the report");
							}
						} else {
							throw new CommonCustomException("Either Approver or Submitter is not a valid Employee");
						}

						// Added by Deepa for Notifications
						try {
							notificationService.setAwaitingApprovalToRm(this.expDatReportBO.getApproverMgr());
						} catch (Exception e) {
							LOG.info("Some Exception happened while setting expense Report Notification to RM Screen");
						}
						// End of Addition by Deepa for Notifications

					}
					catch(Exception e) {
						throw new CommonCustomException(e.getMessage());
					}
				} else if (inputBean.getFkStatusId().equals(ExpenseConstants.Report_Draft)) {
					try {
						List<Integer> ids = new ArrayList<Integer>();
						if (inputBean.getFkExpenseIdList() != null) {
							if (inputBean.getFkExpenseIdList().size() > 0) {
								List<ExpDatExpenseBO> idCheck = expDatExpenseRepository
										.findByPkExpenseIdIn(inputBean.getFkExpenseIdList());
								if (idCheck.size() != inputBean.getFkExpenseIdList().size()) {
									throw new CommonCustomException(
											"Some of the expense attached are not available in DB");
								}
								count = 0;
								expCount = 0;
								List<ExpDatExpenseBO> idOfList = expDatExpenseRepository
										.findByPkExpenseIdIn(inputBean.getFkExpenseIdList());
								idOfList.stream().forEach(obj -> {
									if (obj.getFkEmpId().equals(inputBean.getFkEmpIdInt())) {
										if (obj.getFkStatusId().equals(ExpenseConstants.Expense_UNATTACHED)) {
											this.check = true;
											ids.add(obj.getPkExpenseId());
										} else {
											this.check = false;
											count = count + 1;
										}
									} else {
										expCount = expCount + 1;
									}
								});
								if (expCount > 0) {
									throw new CommonCustomException("Expense are mapped with other employee");
								}
								if (count > 0) {
									throw new CommonCustomException(
											"Please attach only the valid/unattached expense in report");
								}
							}
						}
						if (check && inputBean.getFkExpenseIdList().size() > 0) {
							inputBean.setTotalAmount(expDatExpenseRepository.fetchByTotalAmount(ids));
						}

						Short recallStatus = 0;
						 this.expDatReportBO = new ExpDatReportBO();
						 if(inputBean.getApproverMgr()!=null)
						 this.expDatReportBO.setApproverMgr(inputBean.getApproverMgr());
						 if(inputBean.getFkEmpIdInt() != null)
						 this.expDatReportBO.setFkEmpIdInt(inputBean.getFkEmpIdInt());
						 if(inputBean.getStartDate() != null)
						 this.expDatReportBO.setStartDate(inputBean.getStartDate());
						 if(inputBean.getEndDate()!=null)
						 this.expDatReportBO.setEndDate(inputBean.getEndDate());
						 if(inputBean.getReportDesc()!=null)
						 this.expDatReportBO.setReportDesc(inputBean.getReportDesc());
						 if(inputBean.getReportTitle()!=null)
						 this.expDatReportBO.setReportTitle(inputBean.getReportTitle());
						 if(inputBean.getTotalAmount()!=null)
						 this.expDatReportBO.setTotalAmount(inputBean.getTotalAmount());
						 if(inputBean.getFkStatusId()!=null)
						 this.expDatReportBO.setFkStatusId(inputBean.getFkStatusId());
						 this.expDatReportBO.setSubmissionDate(new Date());
						 if(inputBean.getFkBuId()!=null)
						 this.expDatReportBO.setFkBuId(inputBean.getFkBuId());
						 if(inputBean.getFkClientId()!=null)
						 this.expDatReportBO.setFkClientId(inputBean.getFkClientId());
						 if(inputBean.getFkProjectId()!=null)
						 this.expDatReportBO.setFkProjectId(inputBean.getFkProjectId());
						 if(datEmpProfessionalDetailBO.getFkEmpOrganizationId()!=null)
						 this.expDatReportBO.setFkCurrencyTypeId(datEmpProfessionalDetailBO.getFkEmpOrganizationId());
						 this.expDatReportBO.setRecallStatus(recallStatus);
						 this.expDatReportBO.setRedraftedStatus(recallStatus);
//						this.expDatReportBO = new ExpDatReportBO(inputBean.getApproverMgr(), inputBean.getFkEmpIdInt(),
//								inputBean.getStartDate(), inputBean.getEndDate(), inputBean.getReportDesc(),
//								inputBean.getReportTitle(), inputBean.getTotalAmount(), inputBean.getFkStatusId(),
//								new Date(), inputBean.getFkBuId(), inputBean.getFkClientId(),
//								inputBean.getFkProjectId(), datEmpProfessionalDetailBO.getFkEmpOrganizationId(),
//								recallStatus, recallStatus);
						this.expDatReportBO = expDatReportRepository.save(this.expDatReportBO);
						this.expDatRecordHistoryBO.setFkReportId(this.expDatReportBO.getPkReportId());
						this.expDatRecordHistoryBO.setFkEmpId(this.expDatReportBO.getFkEmpIdInt());
						// this.expDatRecordHistoryBO.setToEmpId(this.expDatReportBO.getApproverMgr());
						this.expDatRecordHistoryBO.setRecordDate(new Date());
						this.expDatRecordHistoryBO.setFkStatusId(this.expDatReportBO.getFkStatusId());
						this.expDatRecordHistoryBO = expDatRecordHistoryRepository.save(this.expDatRecordHistoryBO);
						if (check) {
							Integer id = expDatExpenseRepository.attachExpenseInReport(inputBean.getFkExpenseIdList(),
									ExpenseConstants.Expense_ATTACHED, this.expDatReportBO.getPkReportId());
							if (id == 0) {
								throw new CommonCustomException("Error occured while attaching expense with report");
							}
						}
						this.expDatReportBOList.add(this.expDatReportBO);

					} catch (Exception e) {
						e.printStackTrace();
						throw new CommonCustomException(e.getMessage());
					}

				}
			}
			// this.expDatReportBO = expDatReportRepository.save(this.expDatReportBO);
			// this.expDatReportBOList.add(this.expDatReportBO);
			LOG.endUsecase("createReport service");
			return this.expDatReportBOList;
//		} catch (Exception e) {
//
//			throw new CommonCustomException(e);
//		}
		// return expDatReportBOList;
	}

	ExpDatReportBO setValidateBo(ExpDatReportBean inputBean) throws CommonCustomException, DataAccessException {
		LOG.startUsecase("setValidateBo");
		this.expDatReportBO = new ExpDatReportBO();
		if (Optional.ofNullable(inputBean.getPkReportId()).isPresent())
			this.expDatReportBO.setPkReportId(inputBean.getPkReportId());
		if (Optional.ofNullable(inputBean.getFkEmpIdInt()).isPresent()) {
			boolean isvalidEmpid = isValidEmpId(inputBean.getFkEmpIdInt());
			if (isvalidEmpid)
				this.expDatReportBO.setFkEmpIdInt(inputBean.getFkEmpIdInt());
			else
				throw new CommonCustomException("Submitter is not a valid Employee");
		}
		if (Optional.ofNullable(inputBean.getApproverMgr()).isPresent()) {
			boolean isvalidRmId = isValidRM(inputBean.getApproverMgr());
			if (isvalidRmId)
				this.expDatReportBO.setApproverMgr(inputBean.getApproverMgr());
			else
				throw new CommonCustomException("Approver is not a valid Employee");
		}
		if (Optional.ofNullable(inputBean.getStartDate()).isPresent())
			this.expDatReportBO.setStartDate(inputBean.getStartDate());
		if (Optional.ofNullable(inputBean.getSubmissionDate()).isPresent())
			this.expDatReportBO.setSubmissionDate(inputBean.getSubmissionDate());
		if (Optional.ofNullable(inputBean.getEndDate()).isPresent())
			this.expDatReportBO.setEndDate(inputBean.getEndDate());
		if (Optional.ofNullable(inputBean.getReportDesc()).isPresent())
			this.expDatReportBO.setReportDesc(inputBean.getReportDesc());
		if (Optional.ofNullable(inputBean.getReportTitle()).isPresent())
			this.expDatReportBO.setReportTitle(inputBean.getReportTitle());
		if (Optional.ofNullable(inputBean.getTotalAmount()).isPresent())
			this.expDatReportBO.setTotalAmount(inputBean.getTotalAmount());
		if (Optional.ofNullable(inputBean.getFkStatusId()).isPresent())
			this.expDatReportBO.setFkStatusId(inputBean.getFkStatusId());
		if (Optional.ofNullable(inputBean.getFkBuId()).isPresent())
			this.expDatReportBO.setFkBuId(inputBean.getFkBuId());
		if (Optional.ofNullable(inputBean.getFkClientId()).isPresent())
			this.expDatReportBO.setFkClientId(inputBean.getFkClientId());
		if (Optional.ofNullable(inputBean.getFkProjectId()).isPresent())
			this.expDatReportBO.setFkProjectId(inputBean.getFkProjectId());
		this.expDatReportBO.setModificationDate(new Date());
		if (Optional.ofNullable(inputBean.getRecallStatus()).isPresent())
			this.expDatReportBO.setRecallStatus(inputBean.getRecallStatus());
		if (Optional.ofNullable(inputBean.getFkCurrencyTypeId()).isPresent())
			this.expDatReportBO.setFkCurrencyTypeId(inputBean.getFkCurrencyTypeId());
		if (Optional.ofNullable(inputBean.getRedraftedStatus()).isPresent())
			this.expDatReportBO.setRedraftedStatus(inputBean.getRedraftedStatus());
		// this.expDatReportBO.setSubmissionDate(new Date());
		LOG.endUsecase("setValidateBo");
		return this.expDatReportBO;
	}

	void getValidateBean(ExpDatReportBean inputBean) throws CommonCustomException {
		LOG.startUsecase("getValidateBean");
		if (Optional.ofNullable(inputBean.getApproverMgr()).isPresent() == false)
			throw new CommonCustomException("Approver MGR ID is Manditory");
		if (Optional.ofNullable(inputBean.getStartDate()).isPresent() == false)
			throw new CommonCustomException("Start Date is Manditory");
		if (Optional.ofNullable(inputBean.getEndDate()).isPresent() == false)
			throw new CommonCustomException("End Date is Manditory");
		if (Optional.ofNullable(inputBean.getFkEmpIdInt()).isPresent() == false)
			throw new CommonCustomException("EmpId is Manditory");
		if (Optional.ofNullable(inputBean.getReportDesc()).isPresent() == false)
			throw new CommonCustomException("Report Description is Manditory");
		if (Optional.ofNullable(inputBean.getReportTitle()).isPresent() == false)
			throw new CommonCustomException("Report Title is Manditory");
		if (Optional.ofNullable(inputBean.getTotalAmount()).isPresent() == false)
			throw new CommonCustomException("Total Amount is Manditory");
		if (Optional.ofNullable(inputBean.getFkStatusId()).isPresent() == false)
			throw new CommonCustomException("Status is Manditory");
		if (Optional.ofNullable(inputBean.getFkBuId()).isPresent() == false)
			throw new CommonCustomException("BU selection is Manditory");
		if (Optional.ofNullable(inputBean.getFkClientId()).isPresent() == false)
			throw new CommonCustomException("Client selection is Manditory");
		if (Optional.ofNullable(inputBean.getFkProjectId()).isPresent() == false)
			throw new CommonCustomException("Project selection is Manditory");
		if (Optional.ofNullable(inputBean.getFkExpenseIdList()).isPresent() == false) {
			// if (inputBean.getFkExpenseIdList().size() == 0) {
			// throw new CommonCustomException("Add expenses with the report");
			// }
			throw new CommonCustomException("Add expenses with the report");
		}
		LOG.endUsecase("getValidateBean");

	}

	// view Report by EmpId, RMId, FinMgrId, PkReportId and daterange
	public List<ExpDatReportBean> viewReportByPKIdandDaterange(ExpDatReportBean inputBean)
			throws CommonCustomException {
		LOG.startUsecase("viewReportByPKIdandDaterange Service");
		this.expDatReportBeanList = new ArrayList<ExpDatReportBean>();
		try {
			if (inputBean.getStartDate() != null && inputBean.getEndDate() != null) {
				SimpleDateFormat sdfWithSeconds = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String stringFromDate = sdf.format(inputBean.getStartDate());
				String stringToDate = sdf.format(inputBean.getEndDate());
				stringFromDate = stringFromDate + " 00:00:00";
				stringToDate = stringToDate + " 23:59:59";
				Date fromDate = new Date();
				Date toDate = new Date();
				try {
					fromDate = sdfWithSeconds.parse(stringFromDate);
					toDate = sdfWithSeconds.parse(stringToDate);
				} catch (ParseException e) {
					e.printStackTrace();
					throw new CommonCustomException(e.getMessage());
				}

				if (inputBean.getFinanceLogin() != null && fromDate != null && toDate != null) {
					List<Short> empRole = Arrays.asList(ExpenseConstants.FINANCE_HEAD, ExpenseConstants.FINANCE_MANAGER,
							ExpenseConstants.FINANCE_SENIOR_EXECUTIVE, ExpenseConstants.FINANCE_EXECUTIVE);
					if (empRole.size() > 0) {
						List<DatEmpProfessionalDetailBO> finEmpId = datEmployeeProfessionalRepository
								.fetchListOfEmpFinExp(empRole);
						if (finEmpId.size() > 0) {
							boolean validFinEmp = finEmpId.stream()
									.anyMatch(obj -> obj.getFkMainEmpDetailId().equals(inputBean.getFinanceLogin()));
							if (validFinEmp) {
								List<Short> finStatus = Arrays.asList(ExpenseConstants.Report_RMApproved,
										ExpenseConstants.Report_RMApprovedPartially,
										ExpenseConstants.Report_ReimbursementInitiated,
										ExpenseConstants.Report_ReimbursedPartially,
										ExpenseConstants.Report_ReimbursementCancelled,
										ExpenseConstants.Report_Reimbursed, ExpenseConstants.Report_FinRejected,
										ExpenseConstants.Report_ReimbursementIntitatedPartially);
								// DatEmpProfessionalDetailBO dataObj = finEmpId.stream().filter(obj -> obj
								// .getFkMainEmpDetailId().intValue() == inputBean.getFinanceLogin().intValue())
								// .findFirst().get();
								this.expDatReportBOList = expDatReportRepository.fetchForFin(finStatus,
										// dataObj.getFkEmpOrganizationId(),
										fromDate, toDate);
								return viewConvertedList(this.expDatReportBOList);
							}
						}
					}
				} else if (inputBean.getFkEmpIdInt() != null && fromDate != null && toDate != null) {
					this.expDatReportBOList = expDatReportRepository.fetchByEmpIdAndDateRange(inputBean.getFkEmpIdInt(),
							fromDate, toDate);
					return viewConvertedList(this.expDatReportBOList);
				} else if (inputBean.getApproverMgr() != null && fromDate != null && toDate != null) {
					List<Short> rmStatus = Arrays.asList(ExpenseConstants.Report_RMApproved,
							ExpenseConstants.Report_RMApprovedPartially, ExpenseConstants.Report_ReimbursementInitiated,
							ExpenseConstants.Report_ReimbursedPartially, ExpenseConstants.Report_ReimbursementCancelled,
							ExpenseConstants.Report_Reimbursed, ExpenseConstants.Report_FinRejected,
							ExpenseConstants.Report_ReimbursementIntitatedPartially,
							ExpenseConstants.Report_AwatingRMApproval, ExpenseConstants.Report_RMRejected,
							ExpenseConstants.Report_ForwardedbyRM);
					this.expDatReportBOList = expDatReportRepository.fetchByRMIdAndDateRange(inputBean.getApproverMgr(),
							fromDate, toDate, rmStatus);
					return viewConvertedList(this.expDatReportBOList);
				}
			} else {
				throw new CommonCustomException("Date range is mandatory");
			}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("viewReportByPKIdandDaterange Service");
		return expDatReportBeanList;
	}

	List<ExpDatReportBean> viewConvertedList(List<ExpDatReportBO> boList) {
		this.expDatReportBeanList = new ArrayList<ExpDatReportBean>();
		if (Optional.ofNullable(boList).isPresent()) {
			boList.stream().forEach(obj -> {
				this.expDatReportBean = new ExpDatReportBean();
				this.expDatReportBean = expDatReportBeanMapper.getDestination(this.expDatReportBean, obj);
				this.expDatReportBean.setTotalAmount(expDatReportRepository.fetchByTotalAmount(obj.getPkReportId()));
				this.expDatReportBean.setFkExpenseIdList(
						expDatReportRepository.fetchByExpenseBoDetailsByReportId(obj.getPkReportId()));
				this.expDatReportBean.setSubmittedByName(this.getEmployeeName(obj.getFkEmpIdInt()));
				if (obj.getApproverMgr() != null)
					this.expDatReportBean.setRmName(this.getEmployeeName(obj.getApproverMgr()));
				if (obj.getApproverFin() != null)
					this.expDatReportBean.setFinApproveName(this.getEmployeeName(obj.getApproverFin()));
				this.expDatReportBeanList.add(this.expDatReportBean);
			});
			LOG.endUsecase("viewReportByPKIdandDaterange Service");
			return this.expDatReportBeanList;
		} else {
			LOG.endUsecase("viewReportByPKIdandDaterange Service");
			return this.expDatReportBeanList;
		}
	}

	List<ExpDatReportBean> viewConvertedListForDetails(ExpDatReportBO boList) {
		this.expDatReportBeanList = new ArrayList<ExpDatReportBean>();
		if (Optional.ofNullable(boList).isPresent()) {
			List<Integer> listOfPkReportId = new ArrayList<Integer>();
			listOfPkReportId.add(boList.getPkReportId());
			this.expDatReportBean = new ExpDatReportBean();
			this.expDatReportBean = expDatReportBeanMapper.getDestination(this.expDatReportBean, boList);
			this.expDatReportBean.setTotalAmount(expDatReportRepository.fetchByTotalAmount(boList.getPkReportId()));
			this.expDatReportBean.setFkExpenseIdList(
					expDatReportRepository.fetchByExpenseBoDetailsByReportId(boList.getPkReportId()));
			this.expDatReportBean.setSubmittedByName(this.getEmployeeName(boList.getFkEmpIdInt()));
			if (boList.getApproverMgr() != null)
				this.expDatReportBean.setRmName(this.getEmployeeName(boList.getApproverMgr()));
			if (boList.getApproverFin() != null)
				this.expDatReportBean.setFinApproveName(this.getEmployeeName(boList.getApproverFin()));
			if (boList.getFkForwardRmId() != null)
				this.expDatReportBean.setForwardRMName(this.getEmployeeName(boList.getFkForwardRmId()));
			this.expDatReportBean.setExpDatExpenseBO(expDatExpenseRepository.findByFkReportIdIn(listOfPkReportId));
			this.expDatReportBeanList.add(this.expDatReportBean);
			LOG.endUsecase("viewConvertedListForDetails Service");
			return this.expDatReportBeanList;
		} else {
			LOG.endUsecase("viewConvertedListForDetails Service");
			return this.expDatReportBeanList;
		}
	}

	public String getEmployeeName(Integer empId) {
		LOG.startUsecase("Entered getEmployeeName");
		DatEmpPersonalDetailBO empData = employeePersonalDetailRepo.findByFkEmpDetailId(empId);
		LOG.endUsecase("Exited getEmployeeName");
		return empData.getEmpFirstName() + " " + empData.getEmpLastName();
	}

	public List<ExpDatReportBean> viewReportDetailsByPkId(Integer pkReportId) throws CommonCustomException {
		this.expDatReportBO = new ExpDatReportBO();
		try {
			this.expDatReportBO = expDatReportRepository.findByPkReportId(pkReportId);
			if (this.expDatReportBO != null) {
				return viewConvertedListForDetails(this.expDatReportBO);
			} else {
				throw new CommonCustomException("Entered Report Id doesn't exist");
			}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
	}

	public List<ExpDatReportBean> approveOrReject(ExpDatReportBean inputBean) throws CommonCustomException {
		this.expDatReportBeanList = new ArrayList<ExpDatReportBean>();
		this.expDatRecordHistoryBO = new ExpDatRecordHistoryBO();
		this.expDatReportBO = new ExpDatReportBO();
		boolean isApproved = false;
		boolean isApprovedPartially = false;
		try {
			if (inputBean.getPkReportId() != null && inputBean.getFkStatusId() != null
					&& inputBean.getApproverMgrComment() != null) {
				if (inputBean.getFkStatusId().equals(ExpenseConstants.Report_RMApproved)
						|| inputBean.getFkStatusId().equals(ExpenseConstants.Report_RMRejected)
						|| inputBean.getFkStatusId().equals(ExpenseConstants.Report_RMApprovedPartially)) {
					this.expDatReportBO = expDatReportRepository.findByPkReportId(inputBean.getPkReportId());
					List<Integer> listOfPkReportId = new ArrayList<Integer>();
					listOfPkReportId.add(inputBean.getPkReportId());
					this.expDatExpenseBOList = expDatExpenseRepository.findByFkReportIdIn(listOfPkReportId);
					if (this.expDatExpenseBOList.size() == 0) {
						throw new CommonCustomException("Expenses are not attached with this report");
					} else {
						expRmCount = 0;
						this.expDatExpenseBOList.stream().forEach(obj -> {
							if (obj.getWorkorder() == null) {
								expRmCount = expRmCount + 1;
							}
						});
						// List<Integer> expenseId = new ArrayList<Integer>();
						// this.expDatExpenseBOList.stream().forEach(obj -> {
						// expenseId.add(obj.getPkExpenseId());
						// });
						if (this.expDatReportBO != null) {
							if (this.expDatReportBO.getFkStatusId().equals(inputBean.getFkStatusId())) {
								throw new CommonCustomException("This Action is already performed");
							}
							if (this.expDatReportBO.getFkStatusId().equals(ExpenseConstants.Report_AwatingRMApproval)
									|| this.expDatReportBO.getFkStatusId()
											.equals(ExpenseConstants.Report_ForwardedbyRM)) {
								if (inputBean.getFkStatusId().equals(ExpenseConstants.Report_RMApproved) || inputBean
										.getFkStatusId().equals(ExpenseConstants.Report_RMApprovedPartially)) {
									if (inputBean.getFkFinReimburser() != null) {
										this.expDatReportBO.setFkFinReimburser(inputBean.getFkFinReimburser());
									} else {
										throw new CommonCustomException("Please select the reimburser to approve");
									}
									if (inputBean.getFkStatusId().equals(ExpenseConstants.Report_RMApproved)) {
										if (inputBean.getRmApprovedAmount() != null) {
											if (expRmCount > 0) {
												throw new CommonCustomException(
														"Work Order is not attached with the expense list");
											}
											Integer id = expDatExpenseRepository.approveOrRejectExpense(
													inputBean.getPkReportId(), ExpenseConstants.Expense_RM_APPROVED);
											if (id > 0) {
												this.expDatReportBO.setFkBuId(inputBean.getFkBuId());
												this.expDatReportBO.setFkClientId(inputBean.getFkClientId());
												this.expDatReportBO.setFkProjectId(inputBean.getFkProjectId());
												this.expDatReportBO.setFkStatusId(inputBean.getFkStatusId());
												this.expDatReportBO
														.setApproverMgrComment(inputBean.getApproverMgrComment());
												this.expDatReportBO
														.setRmApprovedAmount(inputBean.getRmApprovedAmount());
												this.expDatReportBO.setManagerApprovalDate(new Date());
											}
										} else {
											throw new CommonCustomException("please give Approved Amount input");
										}
									} else if (inputBean.getFkStatusId()
											.equals(ExpenseConstants.Report_RMApprovedPartially)) {
										if (inputBean.getFkExpenseIdList() != null) {
											if (inputBean.getRmApprovedAmount() != null) {
												if (inputBean.getFkExpenseIdList().size() > 0) {
													List<ExpDatExpenseBO> bOList = expDatExpenseRepository
															.findByPkExpenseIdIn(inputBean.getFkExpenseIdList());
													expRmCount = 0;
													bOList.stream().forEach(obj -> {
														if (obj.getWorkorder() == null) {
															expRmCount = expRmCount + 1;
														}
													});
													if (expRmCount > 0) {
														throw new CommonCustomException(
																"Work Order is not attached with the expense list");
													}
													Integer rejectOtherId = expDatExpenseRepository
															.approveOrRejectExpense(inputBean.getPkReportId(),
																	ExpenseConstants.Expense_RM_REJECTED);
													Integer id = expDatExpenseRepository
															.approveOrRejectExpensePartially(
																	inputBean.getFkExpenseIdList(),
																	ExpenseConstants.Expense_RM_APPROVED);
													if (id > 0) {
														this.expDatReportBO.setFkBuId(inputBean.getFkBuId());
														this.expDatReportBO.setFkClientId(inputBean.getFkClientId());
														this.expDatReportBO.setFkProjectId(inputBean.getFkProjectId());
														this.expDatReportBO.setFkStatusId(inputBean.getFkStatusId());
														this.expDatReportBO.setApproverMgrComment(
																inputBean.getApproverMgrComment());
														this.expDatReportBO
																.setRmApprovedAmount(inputBean.getRmApprovedAmount());
														this.expDatReportBO.setManagerApprovalDate(new Date());
													}
												} else {
													throw new CommonCustomException(
															"Please add the expense list to be approved");
												}
											} else {
												throw new CommonCustomException("please give Approved Amount input");
											}
										} else {
											throw new CommonCustomException(
													"Please add the expense list to be approved");
										}
									}
								} else if (inputBean.getFkStatusId().equals(ExpenseConstants.Report_RMRejected)) {
									Integer id = expDatExpenseRepository.rejectExpenseRm(inputBean.getPkReportId(),
											ExpenseConstants.Expense_RM_REJECTED);
									if (id > 0) {
										this.expDatReportBO.setFkStatusId(inputBean.getFkStatusId());
										this.expDatReportBO.setApproverMgrComment(inputBean.getApproverMgrComment());
										this.expDatReportBO.setManagerApprovalDate(new Date());
									}
								}
								expDatReportRepository.save(this.expDatReportBO);
								this.expDatRecordHistoryBO.setFkReportId(this.expDatReportBO.getPkReportId());
								if (this.expDatReportBO.getFkForwardRmId() != null)
									this.expDatRecordHistoryBO.setFkEmpId(this.expDatReportBO.getFkForwardRmId());
								else
									this.expDatRecordHistoryBO.setFkEmpId(this.expDatReportBO.getApproverMgr());
								this.expDatRecordHistoryBO.setRecordDate(new Date());
								this.expDatRecordHistoryBO.setFkStatusId(this.expDatReportBO.getFkStatusId());
								this.expDatRecordHistoryBO = expDatRecordHistoryRepository
										.save(this.expDatRecordHistoryBO);

								// Added by Deepa for Notifications
								try {
									if (inputBean.getFkStatusId().equals(ExpenseConstants.Report_RMApproved)) {
										notificationService
												.setReportApprovedAlertToEmpScreen(this.expDatReportBO.getFkEmpIdInt());
										List<String> financeRoleKeyNameList = Stream.of(financeRoleKeyNames.split(","))
												.collect(Collectors.toList());
										List<Integer> finEmpIds = datEmployeeProfessionalRepository.getFinEmpIds(
												financeRoleKeyNameList, this.expDatReportBO.getFkFinReimburser());
										for (Integer finEmpId : finEmpIds) {
											notificationService.setReportApprovedAlertToReimbursorScreen(finEmpId);
										}
										notificationService
												.setReportRecallToEmpScreen(this.expDatReportBO.getFkEmpIdInt());
									} else if (inputBean.getFkStatusId()
											.equals(ExpenseConstants.Report_RMApprovedPartially)) {
										notificationService.setReportPartiallyApprovedAlertToEmpScreen(
												this.expDatReportBO.getFkEmpIdInt());
										List<String> financeRoleKeyNameList = Stream.of(financeRoleKeyNames.split(","))
												.collect(Collectors.toList());
										List<Integer> finEmpIds = datEmployeeProfessionalRepository.getFinEmpIds(
												financeRoleKeyNameList, this.expDatReportBO.getFkFinReimburser());
										for (Integer finEmpId : finEmpIds) {
											notificationService
													.setReportPartiallyApprovedAlertToReimbursorScreen(finEmpId);
										}
										notificationService
												.setReportRecallToEmpScreen(this.expDatReportBO.getFkEmpIdInt());
									} else if (inputBean.getFkStatusId().equals(ExpenseConstants.Report_RMRejected)) {
										notificationService
												.setReportRejectedAlertToEmpScreen(this.expDatReportBO.getFkEmpIdInt());
										notificationService
												.setReportRecallToEmpScreen(this.expDatReportBO.getFkEmpIdInt());
										notificationService
												.setAwaitingApprovalToRm(this.expDatReportBO.getApproverMgr());
									}
								} catch (Exception e) {
									LOG.info(
											"Some Exception happened while setting expense Report Notification to RM Screen");
								}
								// End of Addition by Deepa for Notifications
							} else if (this.expDatReportBO.getFkStatusId().equals(ExpenseConstants.Report_RMApproved)
									|| this.expDatReportBO.getFkStatusId()
											.equals(ExpenseConstants.Report_RMApprovedPartially)
									|| this.expDatReportBO.getFkStatusId()
											.equals(ExpenseConstants.Report_ForwardedbyRM)) {
								if (inputBean.getFkStatusId().equals(ExpenseConstants.Report_RMRejected)) {
									if (this.expDatReportBO.getFkStatusId().equals(ExpenseConstants.Report_RMApproved))
										isApproved = true;
									if (this.expDatReportBO.getFkStatusId()
											.equals(ExpenseConstants.Report_RMApprovedPartially))
										isApprovedPartially = true;
									Integer id = expDatExpenseRepository.rejectExpenseRm(inputBean.getPkReportId(),
											ExpenseConstants.Expense_RM_REJECTED);
									if (id > 0) {
										this.expDatReportBO.setFkStatusId(inputBean.getFkStatusId());
										this.expDatReportBO.setApproverMgrComment(inputBean.getApproverMgrComment());
										this.expDatReportBO.setManagerApprovalDate(new Date());
									}
									expDatReportRepository.save(this.expDatReportBO);
									this.expDatRecordHistoryBO.setFkReportId(this.expDatReportBO.getPkReportId());
									if (this.expDatReportBO.getFkForwardRmId() != null)
										this.expDatRecordHistoryBO.setFkEmpId(this.expDatReportBO.getFkForwardRmId());
									else
										this.expDatRecordHistoryBO.setFkEmpId(this.expDatReportBO.getApproverMgr());
									this.expDatRecordHistoryBO.setRecordDate(new Date());
									this.expDatRecordHistoryBO.setFkStatusId(this.expDatReportBO.getFkStatusId());
									this.expDatRecordHistoryBO = expDatRecordHistoryRepository
											.save(this.expDatRecordHistoryBO);
									// Added by Deepa for Notifications
									try {
										notificationService
												.setReportRejectedAlertToEmpScreen(this.expDatReportBO.getFkEmpIdInt());
										List<String> financeRoleKeyNameList = Stream.of(financeRoleKeyNames.split(","))
												.collect(Collectors.toList());
										List<Integer> finEmpIds = datEmployeeProfessionalRepository.getFinEmpIds(
												financeRoleKeyNameList, this.expDatReportBO.getFkFinReimburser());
										for (Integer finEmpId : finEmpIds) {
											notificationService.setReportRejecteddAlertToReimbursorScreen(finEmpId);
											if (isApproved)
												notificationService.setReportApprovedAlertToReimbursorScreen(finEmpId);
											if (isApprovedPartially)
												notificationService
														.setReportPartiallyApprovedAlertToReimbursorScreen(finEmpId);
										}
										notificationService
												.setReportRecallToEmpScreen(this.expDatReportBO.getFkEmpIdInt());
										notificationService
												.setAwaitingApprovalToRm(this.expDatReportBO.getApproverMgr());
									} catch (Exception e) {
										LOG.info(
												"Some Exception happened while setting expense Report Notification to RM Screen");
									}
									// End of Addition by Deepa for Notifications
								} else {
									throw new CommonCustomException(
											"Entered reportId is already approved by RM and it can only be Rejected by RM");
								}
							} else {
								throw new CommonCustomException(
										"Entered reportId is not submitted for approval by submitter");
							}
							// convert BO to Bean
							return viewConvertedListForDetails(this.expDatReportBO);
						} else {
							throw new CommonCustomException("Entered Report Id doesn't exist");
						}
					}
				} else {
					throw new CommonCustomException(
							"Only Approved or Partially Approved or Rejected status are allowed");
				}
			} else {
				throw new CommonCustomException("enter ReportId,comments and status");
			}

		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
	}

	public List<ExpDatReportBean> approveOrRejectbyFin(ExpDatReportBean inputBean) throws CommonCustomException {
		this.expDatReportBeanList = new ArrayList<ExpDatReportBean>();
		this.expDatRecordHistoryBO = new ExpDatRecordHistoryBO();
		this.expDatReportBO = new ExpDatReportBO();
		try {
			if (inputBean.getPkReportId() != null && inputBean.getFkStatusId() != null
					&& inputBean.getApproverFinComment() != null && inputBean.getApproverFin() != null) {
				if (inputBean.getFkStatusId().equals(ExpenseConstants.Report_ReimbursementInitiated)
						|| inputBean.getFkStatusId().equals(ExpenseConstants.Report_FinRejected)
						|| inputBean.getFkStatusId().equals(ExpenseConstants.Report_ReimbursementIntitatedPartially)) {
					this.expDatReportBO = expDatReportRepository.findByPkReportId(inputBean.getPkReportId());
					if (this.expDatReportBO != null) {
						List<Short> empRole = Arrays.asList(ExpenseConstants.FINANCE_HEAD,
								ExpenseConstants.FINANCE_MANAGER, ExpenseConstants.FINANCE_SENIOR_EXECUTIVE,
								ExpenseConstants.FINANCE_EXECUTIVE);
						if (empRole.size() > 0) {
							List<DatEmpProfessionalDetailBO> finEmpId = datEmployeeProfessionalRepository
									.fetchListOfEmpFinExp(empRole);
							if (finEmpId.size() > 0) {
								boolean validFinEmp = finEmpId.stream()
										.anyMatch(obj -> obj.getFkMainEmpDetailId().equals(inputBean.getApproverFin()));
								if (validFinEmp) {
									DatEmpProfessionalDetailBO dataObj = finEmpId.stream().filter(obj -> obj
											.getFkMainEmpDetailId().intValue() == inputBean.getApproverFin().intValue())
											.findFirst().get();
									if (dataObj != null) {
										if (dataObj.getFkEmpOrganizationId()
												.equals(this.expDatReportBO.getFkFinReimburser())) {
											List<Integer> listOfPkReportId = new ArrayList<Integer>();
											listOfPkReportId.add(inputBean.getPkReportId());
											this.expDatExpenseBOList = expDatExpenseRepository
													.findByFkReportIdIn(listOfPkReportId);
											if (this.expDatExpenseBOList.size() == 0) {
												throw new CommonCustomException(
														"Expenses are not attached with this report");
											} else {
												if (this.expDatReportBO != null) {
													if (this.expDatReportBO.getFkStatusId()
															.equals(inputBean.getFkStatusId())) {
														throw new CommonCustomException(
																"This Action is already performed");
													}
													if (this.expDatReportBO.getFkStatusId()
															.equals(ExpenseConstants.Report_RMApproved)
															|| this.expDatReportBO.getFkStatusId().equals(
																	ExpenseConstants.Report_RMApprovedPartially)) {
														if (inputBean.getFkStatusId()
																.equals(ExpenseConstants.Report_ReimbursementInitiated)
																|| inputBean.getFkStatusId().equals(
																		ExpenseConstants.Report_ReimbursementIntitatedPartially)) {
															if (inputBean.getFkStatusId().equals(
																	ExpenseConstants.Report_ReimbursementInitiated)) {
																if (inputBean.getPaymentType() != null
																		&& inputBean.getFinReimbursedAmount() != null) {
																	Integer id = expDatExpenseRepository
																			.approveOrRejectExpenseFinance(
																					inputBean.getPkReportId(),
																					ExpenseConstants.Expense_FINANCE_APPROVED);
																	if (id > 0) {
																		this.expDatReportBO.setFkStatusId(
																				inputBean.getFkStatusId());
																		this.expDatReportBO.setApproverFinComment(
																				inputBean.getApproverFinComment());
																		this.expDatReportBO
																				.setFinanceApprovalDate(new Date());
																		this.expDatReportBO.setApproverFin(
																				inputBean.getApproverFin());
																		this.expDatReportBO.setPaymentType(
																				inputBean.getPaymentType());
																		this.expDatReportBO.setFkCurrencyTypeId(
																				inputBean.getFkCurrencyTypeId());
																		this.expDatReportBO.setFinReimbursedAmount(
																				inputBean.getFinReimbursedAmount());
																		expDatReportRepository
																				.save(this.expDatReportBO);
																		this.expDatRecordHistoryBO.setFkReportId(
																				this.expDatReportBO.getPkReportId());
																		this.expDatRecordHistoryBO.setFkEmpId(
																				this.expDatReportBO.getApproverFin());
																		this.expDatRecordHistoryBO
																				.setRecordDate(new Date());
																		this.expDatRecordHistoryBO.setFkStatusId(
																				this.expDatReportBO.getFkStatusId());
																		this.expDatRecordHistoryBO = expDatRecordHistoryRepository
																				.save(this.expDatRecordHistoryBO);
																		// Added by Deepa for Notifications
																		try {
																			if (this.expDatReportBO.getFkEmpIdInt()
																					.intValue() == this.expDatReportBO
																							.getApproverMgr()
																							.intValue()) {
																				notificationService
																						.setReportReimbursmentIntiatedToEmpScreen(
																								this.expDatReportBO
																										.getFkEmpIdInt());
																			} else {
																				notificationService
																						.setReportReimbursmentIntiatedToEmpScreen(
																								this.expDatReportBO
																										.getFkEmpIdInt());
																				notificationService
																						.setReportReimbursmentIntiatedToRmScreen(
																								this.expDatReportBO
																										.getApproverMgr());
																			}
																		} catch (Exception e) {
																			LOG.info(
																					"Some Exception happened while setting expense Report Notification to RM/EMP Screen");
																		}
																		// End of Addition by Deepa for Notifications
																	}
																} else {
																	throw new CommonCustomException(
																			"Payment Type and Amount is mandatory to record Reimbursement");
																}
															} else if (inputBean.getFkStatusId().equals(
																	ExpenseConstants.Report_ReimbursementIntitatedPartially)) {
																if (inputBean.getPaymentType() != null
																		&& inputBean.getFinReimbursedAmount() != null) {
																	if (inputBean.getFkExpenseIdList() != null) {
																		if (inputBean.getFkExpenseIdList().size() > 0) {
																			Integer rejectOtherId = expDatExpenseRepository
																					.approveOrRejectExpenseFinance(
																							inputBean.getPkReportId(),
																							ExpenseConstants.Expense_FINANCE_REJECTED);
																			Integer id = expDatExpenseRepository
																					.approveOrRejectExpensePartially(
																							inputBean
																									.getFkExpenseIdList(),
																							ExpenseConstants.Expense_FINANCE_APPROVED);
																			if (id > 0) {
																				this.expDatReportBO.setFkStatusId(
																						inputBean.getFkStatusId());
																				this.expDatReportBO
																						.setApproverFinComment(inputBean
																								.getApproverFinComment());
																				this.expDatReportBO
																						.setFinanceApprovalDate(
																								new Date());
																				this.expDatReportBO.setApproverFin(
																						inputBean.getApproverFin());
																				this.expDatReportBO.setPaymentType(
																						inputBean.getPaymentType());
																				this.expDatReportBO
																						.setFkCurrencyTypeId(inputBean
																								.getFkCurrencyTypeId());
																				this.expDatReportBO
																						.setFinReimbursedAmount(
																								inputBean
																										.getFinReimbursedAmount());
																				expDatReportRepository
																						.save(this.expDatReportBO);
																				this.expDatRecordHistoryBO
																						.setFkReportId(
																								this.expDatReportBO
																										.getPkReportId());
																				this.expDatRecordHistoryBO
																						.setFkEmpId(this.expDatReportBO
																								.getApproverFin());
																				this.expDatRecordHistoryBO
																						.setRecordDate(new Date());
																				this.expDatRecordHistoryBO
																						.setFkStatusId(
																								this.expDatReportBO
																										.getFkStatusId());
																				this.expDatRecordHistoryBO = expDatRecordHistoryRepository
																						.save(this.expDatRecordHistoryBO);
																				// Added by Deepa for Notifications
																				try {
																					if (this.expDatReportBO
																							.getFkEmpIdInt()
																							.intValue() == this.expDatReportBO
																									.getApproverMgr()
																									.intValue()) {
																						notificationService
																								.setReportReimbursmentPartialyIntiatedToEmpScreen(
																										this.expDatReportBO
																												.getFkEmpIdInt());
																					} else {
																						notificationService
																								.setReportReimbursmentPartialyIntiatedToEmpScreen(
																										this.expDatReportBO
																												.getFkEmpIdInt());
																						notificationService
																								.setReportReimbursmentPartialyIntiatedToRMScreen(
																										this.expDatReportBO
																												.getApproverMgr());
																					}
																				} catch (Exception e) {
																					LOG.info(
																							"Some Exception happened while setting expense Report Notification to RM/EMP Screen");
																				}
																				// End of Addition by Deepa for
																				// Notifications
																			}
																		} else {
																			throw new CommonCustomException(
																					"Please add the expense list to be approved");
																		}
																	} else {
																		throw new CommonCustomException(
																				"Please add the expense list to be approved");
																	}
																} else {
																	throw new CommonCustomException(
																			"Payment Type and Amount is mandatory to record Reimbursement");
																}
															}
														} else if (inputBean.getFkStatusId()
																.equals(ExpenseConstants.Report_FinRejected)) {
															Integer id = expDatExpenseRepository
																	.approveOrRejectExpenseFinance(
																			inputBean.getPkReportId(),
																			ExpenseConstants.Expense_FINANCE_REJECTED);
															if (id > 0) {
																this.expDatReportBO
																		.setFkStatusId(inputBean.getFkStatusId());
																this.expDatReportBO.setApproverFinComment(
																		inputBean.getApproverFinComment());
																this.expDatReportBO.setFinanceApprovalDate(new Date());
																this.expDatReportBO
																		.setApproverFin(inputBean.getApproverFin());
																expDatReportRepository.save(this.expDatReportBO);
																this.expDatRecordHistoryBO.setFkReportId(
																		this.expDatReportBO.getPkReportId());
																this.expDatRecordHistoryBO.setFkEmpId(
																		this.expDatReportBO.getApproverFin());
																this.expDatRecordHistoryBO.setRecordDate(new Date());
																this.expDatRecordHistoryBO.setFkStatusId(
																		this.expDatReportBO.getFkStatusId());
																this.expDatRecordHistoryBO = expDatRecordHistoryRepository
																		.save(this.expDatRecordHistoryBO);
																// Added by Deepa for Notifications
																try {
																	if (this.expDatReportBO.getFkEmpIdInt()
																			.intValue() == this.expDatReportBO
																					.getApproverMgr().intValue()) {
																		notificationService
																				.setReportReimbursmentRejectedToEmpScreen(
																						this.expDatReportBO
																								.getFkEmpIdInt());
																	} else {
																		notificationService
																				.setReportReimbursmentRejectedToEmpScreen(
																						this.expDatReportBO
																								.getFkEmpIdInt());
																		notificationService
																				.setReportReimbursmentRejectedToRMScreen(
																						this.expDatReportBO
																								.getApproverMgr());
																	}
																} catch (Exception e) {
																	LOG.info(
																			"Some Exception happened while setting expense Report Notification to RM/EMP Screen");
																}
																// End of Addition by Deepa for Notifications
															}
														}
														// expDatReportRepository.save(this.expDatReportBO);
													} else {
														throw new CommonCustomException(
																"entered ReportId is not approved by RM");
													}
													// convert BO to Bean
													return viewConvertedListForDetails(this.expDatReportBO);
												} else {
													throw new CommonCustomException("Entered Report Id doesn't exist");
												}
											}
										} else {
											throw new CommonCustomException(
													"Taking Action for this report belongs to other finance team");
										}
									} else {
										throw new CommonCustomException("Emp is not mapped with the finance team");
									}

								} else {
									throw new CommonCustomException("The emp is not mapped with the finance");
								}
							} else {
								throw new CommonCustomException("check the emp role mapping");
							}
						} else {
							throw new CommonCustomException("check the emp role");
						}
					} else {
						throw new CommonCustomException("Report does not exist");
					}
				} else {
					throw new CommonCustomException(
							"Only Approved or Partially Approved or Rejected status are allowed");
				}
			} else {
				throw new CommonCustomException("enter ReportId, Approver Id, comments and status");
			}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
	}

	public List<ExpDatReportBean> closeAndCancelReimbursement(ExpDatReportBean inputBean) throws CommonCustomException {
		this.expDatReportBeanList = new ArrayList<ExpDatReportBean>();
		this.expDatRecordHistoryBO = new ExpDatRecordHistoryBO();
		this.expDatReportBO = new ExpDatReportBO();
		try {
			if (inputBean.getPkReportId() != null && inputBean.getFkStatusId() != null
					&& inputBean.getApproverFin() != null) {
				this.expDatReportBO = expDatReportRepository.findByPkReportId(inputBean.getPkReportId());
				if (this.expDatReportBO != null) {
					List<Short> empRole = Arrays.asList(ExpenseConstants.FINANCE_HEAD, ExpenseConstants.FINANCE_MANAGER,
							ExpenseConstants.FINANCE_SENIOR_EXECUTIVE, ExpenseConstants.FINANCE_EXECUTIVE);
					if (empRole.size() > 0) {
						List<DatEmpProfessionalDetailBO> finEmpId = datEmployeeProfessionalRepository
								.fetchListOfEmpFinExp(empRole);
						if (finEmpId.size() > 0) {
							boolean validFinEmp = finEmpId.stream()
									.anyMatch(obj -> obj.getFkMainEmpDetailId().equals(inputBean.getApproverFin()));
							if (validFinEmp) {
								DatEmpProfessionalDetailBO dataObj = finEmpId.stream().filter(obj -> obj
										.getFkMainEmpDetailId().intValue() == inputBean.getApproverFin().intValue())
										.findFirst().get();
								if (dataObj != null) {
									if (dataObj.getFkEmpOrganizationId()
											.equals(this.expDatReportBO.getFkFinReimburser())) {
										if (inputBean.getFkStatusId()
												.equals(ExpenseConstants.Report_ReimbursementCancelled)
												|| inputBean.getFkStatusId()
														.equals(ExpenseConstants.Report_Reimbursed)) {
											if (this.expDatReportBO != null) {
												if (this.expDatReportBO.getFkStatusId()
														.equals(inputBean.getFkStatusId())) {
													throw new CommonCustomException("This Action is already perfomed");
												}
												if (this.expDatReportBO.getFkStatusId()
														.equals(ExpenseConstants.Report_ReimbursementInitiated)
														|| this.expDatReportBO.getFkStatusId().equals(
																ExpenseConstants.Report_ReimbursementIntitatedPartially)) {
													if (inputBean.getFkStatusId()
															.equals(ExpenseConstants.Report_Reimbursed)) {
														this.expDatReportBO.setFkStatusId(inputBean.getFkStatusId());
														this.expDatReportBO.setFinanceApprovalDate(new Date());
														this.expDatReportBO.setApproverFin(inputBean.getApproverFin());
													} else if (inputBean.getFkStatusId()
															.equals(ExpenseConstants.Report_ReimbursementCancelled)) {
														Integer id = expDatExpenseRepository.cancelExpenseFinance(
																inputBean.getPkReportId(),
																ExpenseConstants.Expense_FINANCE_REJECTED);
														if (id > 0) {
															this.expDatReportBO
																	.setFkStatusId(inputBean.getFkStatusId());
															this.expDatReportBO.setApproverFinComment(
																	inputBean.getApproverFinComment());
															this.expDatReportBO.setFinanceApprovalDate(new Date());
															this.expDatReportBO
																	.setApproverFin(inputBean.getApproverFin());
															this.expDatReportBO.setFinReimbursedAmount(null);
														} else {
															throw new CommonCustomException(
																	"Some problem occured while rejecting Expense from report");
														}
													}
													expDatReportRepository.save(this.expDatReportBO);
													this.expDatRecordHistoryBO
															.setFkReportId(this.expDatReportBO.getPkReportId());
													this.expDatRecordHistoryBO
															.setFkEmpId(this.expDatReportBO.getApproverFin());
													this.expDatRecordHistoryBO.setRecordDate(new Date());
													this.expDatRecordHistoryBO
															.setFkStatusId(this.expDatReportBO.getFkStatusId());
													this.expDatRecordHistoryBO = expDatRecordHistoryRepository
															.save(this.expDatRecordHistoryBO);
													// Added by Deepa for Notifications
													try {
														if (inputBean.getFkStatusId()
																.equals(ExpenseConstants.Report_Reimbursed)) {
															if (this.expDatReportBO.getFkEmpIdInt()
																	.intValue() == this.expDatReportBO.getApproverMgr()
																			.intValue()) {
																notificationService.setReportReimbursedToEmpScreen(
																		this.expDatReportBO.getFkEmpIdInt());
															} else {
																notificationService.setReportReimbursedToEmpScreen(
																		this.expDatReportBO.getFkEmpIdInt());
																notificationService.setReportReimbursedToRMScreen(
																		this.expDatReportBO.getApproverMgr());
															}
														} else if (inputBean.getFkStatusId().equals(
																ExpenseConstants.Report_ReimbursementCancelled)) {
															if (this.expDatReportBO.getFkEmpIdInt()
																	.intValue() == this.expDatReportBO.getApproverMgr()
																			.intValue()) {
																notificationService
																		.setReportReimbursmentCancelledToEmpScreen(
																				this.expDatReportBO.getFkEmpIdInt());
															} else {
																notificationService
																		.setReportReimbursmentCancelledToEmpScreen(
																				this.expDatReportBO.getFkEmpIdInt());
																notificationService
																		.setReportReimbursmentCancelledToRMScreen(
																				this.expDatReportBO.getApproverMgr());
															}
														}
													} catch (Exception e) {
														LOG.info(
																"Some Exception happened while setting expense Report Notification to RM/EMP Screen");
													}
													// End of Addition by Deepa for Notifications
												} else {
													throw new CommonCustomException(
															"entered ReportId is not approved by Fin");
												}
												// convert BO to Bean
												return viewConvertedListForDetails(this.expDatReportBO);
											} else {
												throw new CommonCustomException("Entered Report Id doesn't exist");
											}
										} else {
											throw new CommonCustomException(
													"Only Reimbursed or Reimbursement Cancel status are allowed");
										}
									} else {
										throw new CommonCustomException(
												"Taking Action for this report belongs to other finance team");
									}
								} else {
									throw new CommonCustomException("Emp is not mapped with the finance team");
								}
							} else {
								throw new CommonCustomException("The emp is not mapped with the finance");
							}
						} else {
							throw new CommonCustomException("check the emp role mapping");
						}
					} else {
						throw new CommonCustomException("check the emp role");
					}
				} else {
					throw new CommonCustomException("Report does not exist");
				}
			} else {
				throw new CommonCustomException("enter ReportId, Approver Id and status");
			}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
	}

	// All status
	public List<ExpReportMasStatusBO> getAllReportStatus() throws CommonCustomException {
		List<ExpReportMasStatusBO> expReportMasStatusBO = new ArrayList<ExpReportMasStatusBO>();
		try {
			expReportMasStatusBO = expMasReportStatusRepository.findAll();
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		return expReportMasStatusBO;
	}

	// forward To RM
	public List<ExpDatReportBean> forwardToRm(ExpDatReportBean inputBean) throws CommonCustomException {
		this.expDatReportBeanList = new ArrayList<ExpDatReportBean>();
		this.expDatRecordHistoryBO = new ExpDatRecordHistoryBO();
		this.expDatReportBO = new ExpDatReportBO();
		try {
			if (inputBean.getPkReportId() != null && inputBean.getApproverMgr() != null
					&& inputBean.getFkForwardRmId() != null) {
				this.expDatReportBO = expDatReportRepository.findByPkReportId(inputBean.getPkReportId());
				boolean isvalidEmpid = false;
				isvalidEmpid = isValidEmpId(inputBean.getFkForwardRmId());
				boolean isvalidRmId = false;
				isvalidRmId = isValidRM(inputBean.getFkForwardRmId());
				DatEmpProfessionalDetailBO rmInfo = commonService.getEmpoyeeDeatailsById(inputBean.getApproverMgr());
				DatEmpProfessionalDetailBO forwardRmInfo = commonService
						.getEmpoyeeDeatailsById(inputBean.getFkForwardRmId());
				if (isvalidRmId && isvalidEmpid) {
					if (rmInfo.getFkEmpBuUnit().equals(forwardRmInfo.getFkEmpBuUnit())) {
						if (this.expDatReportBO != null) {
							if (this.expDatReportBO.getApproverMgr().equals(inputBean.getApproverMgr())) {
								if (this.expDatReportBO.getFkStatusId()
										.equals(ExpenseConstants.Report_AwatingRMApproval)) {
									expDatExpenseRepository.forwardExpenseRm(inputBean.getPkReportId());
									this.expDatReportBO.setPkReportId(inputBean.getPkReportId());
									this.expDatReportBO.setModificationDate(new Date());
									this.expDatReportBO.setFkStatusId(ExpenseConstants.Report_ForwardedbyRM);
									this.expDatReportBO.setFkForwardRmId(inputBean.getFkForwardRmId());
									expDatReportRepository.save(this.expDatReportBO);
									this.expDatRecordHistoryBO.setFkReportId(this.expDatReportBO.getPkReportId());
									this.expDatRecordHistoryBO.setFkEmpId(this.expDatReportBO.getApproverMgr());
									this.expDatRecordHistoryBO.setRecordDate(new Date());
									if (this.expDatReportBO.getFkForwardRmId() != null)
										this.expDatRecordHistoryBO.setToEmpId(this.expDatReportBO.getFkForwardRmId());
									this.expDatRecordHistoryBO.setFkStatusId(this.expDatReportBO.getFkStatusId());
									this.expDatRecordHistoryBO = expDatRecordHistoryRepository
											.save(this.expDatRecordHistoryBO);
									// Added by Deepa for Notifications
									try {
										notificationService
												.setReportForwardAlertToEmpScreen(this.expDatReportBO.getFkEmpIdInt());
										notificationService.setReportForwardAlertToSkipLevelScreen(
												this.expDatReportBO.getFkForwardRmId());
									} catch (Exception e) {
										LOG.info(
												"Some Exception happened while setting expense Report Notification to EMP/SKip level RM Screen");
									}
									// End of Addition by Deepa for Notifications
								} else {
									throw new CommonCustomException("Report status should be Awaiting for Approval ");
								}
							} else {
								throw new CommonCustomException("only approver can forward to another RM");
							}
						} else {
							throw new CommonCustomException("Report ID does not exist");
						}
					} else {
						throw new CommonCustomException("Both manager BU id is not same");
					}
				} else {
					throw new CommonCustomException("Forward Employee ID is not a vaild Manager");
				}
			} else {
				throw new CommonCustomException("enter ReportId, Approver Id, forward to id and status");
			}
			return viewConvertedListForDetails(this.expDatReportBO);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
	}

	// emp recall report
	public List<ExpDatReportBean> empRecallReport(ExpDatReportBean inputBean) throws CommonCustomException {
		this.expDatReportBeanList = new ArrayList<ExpDatReportBean>();
		this.expDatRecordHistoryBO = new ExpDatRecordHistoryBO();
		this.expDatReportBO = new ExpDatReportBO();
		boolean isRM = false;
		try {
			if (inputBean.getPkReportId() != null && inputBean.getFkStatusId() != null) {
				this.expDatReportBO = expDatReportRepository.findByPkReportId(inputBean.getPkReportId());
				if (this.expDatReportBO != null) {
					if (inputBean.getFkStatusId().equals(ExpenseConstants.Report_EmpRecalled)) {
						if (this.expDatReportBO.getFkStatusId().equals(ExpenseConstants.Report_AwatingRMApproval)) {
							Short recallStatus = 1;
							Short notYetStatus = 0;
							if (this.expDatReportBO.getRecallStatus() == notYetStatus) {
								this.expDatReportBO.setPkReportId(inputBean.getPkReportId());
								this.expDatReportBO.setFkStatusId(ExpenseConstants.Report_EmpRecalled);
								this.expDatReportBO.setRecallStatus(recallStatus);
								this.expDatReportBO.setModificationDate(new Date());
								expDatReportRepository.save(this.expDatReportBO);
								this.expDatRecordHistoryBO.setFkReportId(this.expDatReportBO.getPkReportId());
								this.expDatRecordHistoryBO.setFkEmpId(this.expDatReportBO.getFkEmpIdInt());
								this.expDatRecordHistoryBO.setRecordDate(new Date());
								this.expDatRecordHistoryBO.setFkStatusId(this.expDatReportBO.getFkStatusId());
								this.expDatRecordHistoryBO = expDatRecordHistoryRepository
										.save(this.expDatRecordHistoryBO);
								// Added by Deepa for Notifications
								try {
									notificationService.setReportRecallToEmpScreen(this.expDatReportBO.getFkEmpIdInt());
									notificationService.setReportRecallToRmScreen(this.expDatReportBO.getApproverMgr());
									notificationService.setAwaitingApprovalToRm(this.expDatReportBO.getApproverMgr());
								} catch (Exception e) {
									LOG.info(
											"Some Exception happened while setting expense Report Notification to RM/EMP Screen");
								}
								// End of Addition by Deepa for Notifications
							} else {
								throw new CommonCustomException("Recall Option is available for only once");
							}
						} else if (this.expDatReportBO.getFkStatusId().equals(ExpenseConstants.Report_RMRejected)) {
							Short secondRecallStatus = 2;
							if (this.expDatReportBO.getRecallStatus() < secondRecallStatus) {
								Integer rejectOtherId = expDatExpenseRepository.unCheckExpenseOnRecall(
										inputBean.getPkReportId(), ExpenseConstants.Expense_ATTACHED);
								if (rejectOtherId > 0) {
									this.expDatReportBO.setPkReportId(inputBean.getPkReportId());
									this.expDatReportBO.setFkStatusId(ExpenseConstants.Report_EmpRecalled);
									this.expDatReportBO.setRecallStatus(secondRecallStatus);
									this.expDatReportBO.setModificationDate(new Date());
									expDatReportRepository.save(this.expDatReportBO);
									this.expDatRecordHistoryBO.setFkReportId(this.expDatReportBO.getPkReportId());
									this.expDatRecordHistoryBO.setFkEmpId(this.expDatReportBO.getFkEmpIdInt());
									this.expDatRecordHistoryBO.setRecordDate(new Date());
									this.expDatRecordHistoryBO.setFkStatusId(this.expDatReportBO.getFkStatusId());
									this.expDatRecordHistoryBO = expDatRecordHistoryRepository
											.save(this.expDatRecordHistoryBO);
									// Added by Deepa for Notifications
									try {
										notificationService
												.setReportRecallToEmpScreen(this.expDatReportBO.getFkEmpIdInt());
										notificationService
												.setReportRecallToRmScreen(this.expDatReportBO.getApproverMgr());
										notificationService
												.setReportRejectedAlertToEmpScreen(this.expDatReportBO.getFkEmpIdInt());
										notificationService
												.setAwaitingApprovalToRm(this.expDatReportBO.getApproverMgr());
									} catch (Exception e) {
										LOG.info(
												"Some Exception happened while setting expense Report Notification to RM/EMP Screen");
									}
									// End of Addition by Deepa for Notifications
								} else {
									throw new CommonCustomException(
											"Some problem occured while attaching expense status");
								}
							} else {
								throw new CommonCustomException("Recall Option is available for only once");
							}
						} else {
							throw new CommonCustomException(
									"To recall the report status should be in waiting for approval or report rejected by approver");
						}
					} else if (inputBean.getFkStatusId().equals(ExpenseConstants.Report_EmpRejected)) {
						if (this.expDatReportBO.getFkStatusId().equals(ExpenseConstants.Report_AwatingRMApproval)
								|| this.expDatReportBO.getFkStatusId().equals(ExpenseConstants.Report_Draft)
								|| this.expDatReportBO.getFkStatusId().equals(ExpenseConstants.Report_EmpRecalled)) {
							if (this.expDatReportBO.getFkStatusId().equals(ExpenseConstants.Report_AwatingRMApproval))
								isRM = true;
							Integer id = expDatExpenseRepository.unCheckExpenseOnRecall(inputBean.getPkReportId(),
									ExpenseConstants.Expense_CANCELLED_BY_SUBMITTER);
							this.expDatReportBO.setPkReportId(inputBean.getPkReportId());
							this.expDatReportBO.setFkStatusId(ExpenseConstants.Report_EmpRejected);
							this.expDatReportBO.setModificationDate(new Date());
							expDatReportRepository.save(this.expDatReportBO);
							this.expDatRecordHistoryBO.setFkReportId(this.expDatReportBO.getPkReportId());
							this.expDatRecordHistoryBO.setFkEmpId(this.expDatReportBO.getFkEmpIdInt());
							this.expDatRecordHistoryBO.setRecordDate(new Date());
							this.expDatRecordHistoryBO.setFkStatusId(this.expDatReportBO.getFkStatusId());
							this.expDatRecordHistoryBO = expDatRecordHistoryRepository.save(this.expDatRecordHistoryBO);
						} else {
							throw new CommonCustomException(
									"To reject the report status should be in waiting for approval, Draft, Recalled");
						}
						// Added by Deepa for Notifications
						try {
							if (isRM) {
								notificationService.setRejectByEmpToEmp(this.expDatReportBO.getFkEmpIdInt());
								notificationService.setRejectByEmpToRmScreen(this.expDatReportBO.getApproverMgr());
								notificationService.setAwaitingApprovalToRm(this.expDatReportBO.getApproverMgr());
							} else {
								notificationService.setRejectByEmpToEmp(this.expDatReportBO.getFkEmpIdInt());
							}
						} catch (Exception e) {
							LOG.info(
									"Some Exception happened while setting expense Report Notification to RM/EMP Screen");
						}
						// End of Addition by Deepa for Notifications
					}
				} else {
					throw new CommonCustomException("Report ID does not exist");
				}
			} else {
				throw new CommonCustomException("enter ReportId, Approver Id, forward to id and status");
			}
			return viewConvertedListForDetails(this.expDatReportBO);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
	}

	// fin advance Search
	public List<ExpDatReportBean> finAdvanceSearch(ExpenseAdvanceSearchBean inputBean) throws CommonCustomException {
		this.expDatReportBOList = new ArrayList<ExpDatReportBO>();
		ArrayList<ExpDatReportBO> pricesList = new ArrayList<ExpDatReportBO>();
		try {
			if (inputBean.getApprover() != null || inputBean.getClient() != null || inputBean.getEndDate() != null
					|| inputBean.getProject() != null || inputBean.getReimbursedBy() != null
					|| inputBean.getReportName() != null || inputBean.getStartDate() != null
					|| inputBean.getStatus() != null || inputBean.getSubmittedBy() != null) {
				Specification<ExpDatReportBO> expDatReportBOObj = null;
				expDatReportBOObj = ExpenseAdvanceSearchSpecification.getAllDetailsForFin(inputBean);
				this.expDatReportBOList = expDatReportRepository.findAll(expDatReportBOObj);
				if (inputBean.getFinanceLogin() != null) {
					List<Short> finStatus = Arrays.asList(ExpenseConstants.Report_RMApproved,
							ExpenseConstants.Report_RMApprovedPartially, ExpenseConstants.Report_ReimbursementInitiated,
							ExpenseConstants.Report_ReimbursedPartially, ExpenseConstants.Report_ReimbursementCancelled,
							ExpenseConstants.Report_Reimbursed, ExpenseConstants.Report_FinRejected,
							ExpenseConstants.Report_ReimbursementIntitatedPartially);
					List<Short> empRole = Arrays.asList(ExpenseConstants.FINANCE_HEAD, ExpenseConstants.FINANCE_MANAGER,
							ExpenseConstants.FINANCE_SENIOR_EXECUTIVE, ExpenseConstants.FINANCE_EXECUTIVE);
					if (empRole.size() > 0) {
						List<DatEmpProfessionalDetailBO> finEmpId = datEmployeeProfessionalRepository
								.fetchListOfEmpFinExp(empRole);
						if (finEmpId.size() > 0) {
							boolean validFinEmp = finEmpId.stream()
									.anyMatch(obj -> obj.getFkMainEmpDetailId().equals(inputBean.getFinanceLogin()));
							if (validFinEmp) {
								// DatEmpProfessionalDetailBO dataObj = finEmpId.stream().filter(obj -> obj
								// .getFkMainEmpDetailId().intValue() == inputBean.getFinanceLogin().intValue())
								// .findFirst().get();
								pricesList = this.expDatReportBOList.stream().filter(obj ->
								// obj.getFkFinReimburser() == dataObj.getFkEmpOrganizationId()&&
								finStatus.contains(obj.getFkStatusId())).map(obj -> obj)
										.collect(Collectors.toCollection(ArrayList::new));
								Collections.sort(pricesList, new Comparator<ExpDatReportBO>() {
									public int compare(ExpDatReportBO o1, ExpDatReportBO o2) {
										return o2.getPkReportId().compareTo(o1.getPkReportId());
									}
								});
								return viewConvertedList(pricesList);
							} else {
								throw new CommonCustomException("Login user is not mapped with finance roal");
							}
						}
					}
				} else if (inputBean.getRmLogin() != null) {
					List<Short> rmStatus = Arrays.asList(ExpenseConstants.Report_RMApproved,
							ExpenseConstants.Report_AwatingRMApproval, ExpenseConstants.Report_RMRejected,
							ExpenseConstants.Report_RMApprovedPartially, ExpenseConstants.Report_ReimbursementInitiated,
							ExpenseConstants.Report_ReimbursedPartially, ExpenseConstants.Report_ReimbursementCancelled,
							ExpenseConstants.Report_Reimbursed, ExpenseConstants.Report_FinRejected,
							ExpenseConstants.Report_ForwardedbyRM,
							ExpenseConstants.Report_ReimbursementIntitatedPartially);
					pricesList = this.expDatReportBOList.stream()
							.filter(obj -> ((obj.getApproverMgr() != null
									&& obj.getApproverMgr().equals(inputBean.getRmLogin()))
									|| (obj.getFkForwardRmId() != null
											&& obj.getFkForwardRmId().equals(inputBean.getRmLogin())))
									&& rmStatus.contains(obj.getFkStatusId()))
							.map(obj -> obj).collect(Collectors.toCollection(ArrayList::new));
					Collections.sort(pricesList, new Comparator<ExpDatReportBO>() {
						public int compare(ExpDatReportBO o1, ExpDatReportBO o2) {
							return o2.getPkReportId().compareTo(o1.getPkReportId());
						}
					});
					return viewConvertedList(pricesList);
				} else {
					throw new CommonCustomException("Either enter rmLogin or financeLogin id");
				}
			} else {
				throw new CommonCustomException(
						"Please enter either approver, client, date range, project, reimbusedBy, report name, status, submittedBy");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		return viewConvertedList(this.expDatReportBOList);
	}

	// List of all report Names
	public List<String> listOfAllReportName(ExpDatReportBean inputBean) throws CommonCustomException {
		List<String> reportNames = new ArrayList<String>();
		try {
			if (inputBean.getFinanceLogin() != null) {
				List<Short> finStatus = Arrays.asList(ExpenseConstants.Report_RMApproved,
						ExpenseConstants.Report_RMApprovedPartially, ExpenseConstants.Report_ReimbursementInitiated,
						ExpenseConstants.Report_ReimbursedPartially, ExpenseConstants.Report_ReimbursementCancelled,
						ExpenseConstants.Report_Reimbursed, ExpenseConstants.Report_FinRejected,
						ExpenseConstants.Report_ReimbursementIntitatedPartially);
				List<Short> empRole = Arrays.asList(ExpenseConstants.FINANCE_HEAD, ExpenseConstants.FINANCE_MANAGER,
						ExpenseConstants.FINANCE_SENIOR_EXECUTIVE, ExpenseConstants.FINANCE_EXECUTIVE);
				if (empRole.size() > 0) {
					List<DatEmpProfessionalDetailBO> finEmpId = datEmployeeProfessionalRepository
							.fetchListOfEmpFinExp(empRole);
					if (finEmpId.size() > 0) {
						boolean validFinEmp = finEmpId.stream()
								.anyMatch(obj -> obj.getFkMainEmpDetailId().equals(inputBean.getFinanceLogin()));
						if (validFinEmp) {
							reportNames = expDatReportRepository.fetchAllFinReportName(finStatus);
						}
					}
				}

			} else if (inputBean.getApproverMgr() != null) {
				List<Short> rmStatus = Arrays.asList(ExpenseConstants.Report_AwatingRMApproval,
						ExpenseConstants.Report_RMRejected, ExpenseConstants.Report_RMApproved,
						ExpenseConstants.Report_RMApprovedPartially, ExpenseConstants.Report_ReimbursementInitiated,
						ExpenseConstants.Report_ReimbursedPartially, ExpenseConstants.Report_ReimbursementCancelled,
						ExpenseConstants.Report_Reimbursed, ExpenseConstants.Report_FinRejected,
						ExpenseConstants.Report_ReimbursementIntitatedPartially, ExpenseConstants.Report_ForwardedbyRM);
				reportNames = expDatReportRepository.fetchAllMgrReportName(inputBean.getApproverMgr(), rmStatus);
			} else {
				throw new CommonCustomException("Please enter either approver, finance id");
			}
			return reportNames;
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
	}

	// Report History
	public List<ExpDatReportHistoryBean> getReportHistory(Integer fkReportId) throws CommonCustomException {
		List<ExpDatRecordHistoryBO> expReportMasStatusBO = new ArrayList<ExpDatRecordHistoryBO>();
		List<ExpDatReportHistoryBean> expDatReportHistoryBean = new ArrayList<ExpDatReportHistoryBean>();

		try {
			expReportMasStatusBO = expDatRecordHistoryRepository
					.findByFkReportIdInOrderByPkRecordHistoryIdDesc(fkReportId);
			if (expReportMasStatusBO.size() > 0) {
				expReportMasStatusBO.stream().forEach(obj -> {
					ExpDatReportHistoryBean expDatReportHistoryBeanObj = new ExpDatReportHistoryBean();
					expDatReportHistoryBeanObj.setFkEmpId(obj.getFkEmpId());
					expDatReportHistoryBeanObj.setFkEmpIdName(getEmployeeName(obj.getFkEmpId()));
					expDatReportHistoryBeanObj.setFkReportId(obj.getFkReportId());
					expDatReportHistoryBeanObj.setFkStatusId(obj.getFkStatusId());
					expDatReportHistoryBeanObj.setPkRecordHistoryId(obj.getPkRecordHistoryId());
					expDatReportHistoryBeanObj.setRecordDate(obj.getRecordDate());
					expDatReportHistoryBeanObj.setStatusName(obj.getExpReportMasStatusBO().getStatusDesc());
					if (obj.getToEmpId() != null)
						expDatReportHistoryBeanObj.setToEmpId(obj.getToEmpId());
					if (obj.getToEmpId() != null)
						expDatReportHistoryBeanObj.setToEmpIdName(getEmployeeName(obj.getToEmpId()));
					expDatReportHistoryBean.add(expDatReportHistoryBeanObj);
				});
			} else {
				throw new CommonCustomException("Entered Report ID don't have relavent history");
			}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		return expDatReportHistoryBean;
	}

	// Report filter for RM and Finance
	public List<ExpDatReportBean> rmAndFinancefilter(ExpenseAdvanceSearchBean inputBean) throws CommonCustomException {
		ArrayList<ExpDatReportBO> pricesList = new ArrayList<ExpDatReportBO>();
		try {
			if (inputBean.getStatus() != null || (inputBean.getStartDate() != null && inputBean.getEndDate() != null)) {
				Specification<ExpDatReportBO> expDatReportBOObj = null;
				expDatReportBOObj = ExpenseAdvanceSearchSpecification.filterRMandFinance(inputBean);
				this.expDatReportBOList = expDatReportRepository.findAll(expDatReportBOObj);
				if (inputBean.getFinanceLogin() != null) {
					List<Short> finStatus = Arrays.asList(ExpenseConstants.Report_RMApproved,
							ExpenseConstants.Report_RMApprovedPartially, ExpenseConstants.Report_ReimbursementInitiated,
							ExpenseConstants.Report_ReimbursedPartially, ExpenseConstants.Report_ReimbursementCancelled,
							ExpenseConstants.Report_Reimbursed, ExpenseConstants.Report_FinRejected,
							ExpenseConstants.Report_ReimbursementIntitatedPartially);
					List<Short> empRole = Arrays.asList(ExpenseConstants.FINANCE_HEAD, ExpenseConstants.FINANCE_MANAGER,
							ExpenseConstants.FINANCE_SENIOR_EXECUTIVE, ExpenseConstants.FINANCE_EXECUTIVE);
					if (empRole.size() > 0) {
						List<DatEmpProfessionalDetailBO> finEmpId = datEmployeeProfessionalRepository
								.fetchListOfEmpFinExp(empRole);
						if (finEmpId.size() > 0) {
							boolean validFinEmp = finEmpId.stream()
									.anyMatch(obj -> obj.getFkMainEmpDetailId().equals(inputBean.getFinanceLogin()));
							if (validFinEmp) {
								// DatEmpProfessionalDetailBO dataObj = finEmpId.stream().filter(obj -> obj
								// .getFkMainEmpDetailId().intValue() == inputBean.getFinanceLogin().intValue())
								// .findFirst().get();
								pricesList = this.expDatReportBOList.stream().filter(obj ->
								// obj.getFkFinReimburser() == dataObj.getFkEmpOrganizationId()&&
								finStatus.contains(obj.getFkStatusId())).map(obj -> obj)
										.collect(Collectors.toCollection(ArrayList::new));
								return viewConvertedList(pricesList);
							} else {
								throw new CommonCustomException("Login user is not mapped with finance roal");
							}
						}
					}
				} else if (inputBean.getRmLogin() != null) {
					List<Short> rmStatus = Arrays.asList(ExpenseConstants.Report_RMApproved,
							ExpenseConstants.Report_AwatingRMApproval, ExpenseConstants.Report_RMRejected,
							ExpenseConstants.Report_RMApprovedPartially, ExpenseConstants.Report_ReimbursementInitiated,
							ExpenseConstants.Report_ReimbursedPartially, ExpenseConstants.Report_ReimbursementCancelled,
							ExpenseConstants.Report_Reimbursed, ExpenseConstants.Report_FinRejected,
							ExpenseConstants.Report_ForwardedbyRM,
							ExpenseConstants.Report_ReimbursementIntitatedPartially);
					pricesList = this.expDatReportBOList.stream()
							.filter(obj -> ((obj.getApproverMgr() != null
									&& obj.getApproverMgr().equals(inputBean.getRmLogin()))
									|| (obj.getFkForwardRmId() != null
											&& obj.getFkForwardRmId().equals(inputBean.getRmLogin())))
									&& rmStatus.contains(obj.getFkStatusId()))
							.map(obj -> obj).collect(Collectors.toCollection(ArrayList::new));
					Collections.sort(pricesList, new Comparator<ExpDatReportBO>() {
						public int compare(ExpDatReportBO o1, ExpDatReportBO o2) {
							return o2.getPkReportId().compareTo(o1.getPkReportId());
						}
					});
					return viewConvertedList(pricesList);
				} else {
					throw new CommonCustomException("RM login or Finance login is required");
				}
			} else {
				throw new CommonCustomException("either status or dates are required");
			}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		return viewConvertedList(this.expDatReportBOList);
	}

}