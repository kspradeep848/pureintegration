package com.thbs.mis.expense.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbs.mis.expense.bean.ExpMasStatusBean;
import com.thbs.mis.expense.bo.ExpMasStatusBO;
import com.thbs.mis.expense.dao.ExpMasStatusRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

@Service
public class ExpMasStatusService {

	@Autowired
	ExpMasStatusRepository expMasStatusRepository;

	List<ExpMasStatusBO> expMasStatusBOList;

	List<ExpMasStatusBean> expMasStatusBeanList;

	private static final AppLog LOG = LogFactory
			.getLog(ExpMasStatusService.class);

	public List<ExpMasStatusBO> allStatus() throws CommonCustomException {
		LOG.startUsecase("allStatus");
		this.expMasStatusBOList = new ArrayList<ExpMasStatusBO>();
		try {
			this.expMasStatusBOList = expMasStatusRepository.fetchAllStatus();
			LOG.endUsecase("allStatus");
			return this.expMasStatusBOList;
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
	}

}