package com.thbs.mis.notification.birthday.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the dat_emp_birthday_message database table.
 * 
 */
@Entity
@Table(name="dat_emp_birthday_message")
@NamedQuery(name="DatEmpBirthdayMessageBO.findAll", query="SELECT d FROM DatEmpBirthdayMessageBO d")
public class DatEmpBirthdayMessageBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_birthday_message_id")
	private int pkBirthdayMessageId;

	@Column(name="birthday_message")
	private String birthdayMessage;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="birthday_message_created_date")
	private Date birthdayMessageCreatedDate;

	@Column(name="birthday_message_id")
	private String birthdayMessageId;
	
	@Column(name="fk_birthday_message_from_emp_id")
	private Integer fkBirthdayMessageFromEmpId;
	
	@Column(name="fk_birthday_message_to_emp_id")
	private Integer fkBirthdayMessageToEmpId;

	public DatEmpBirthdayMessageBO() {
	}

	public int getPkBirthdayMessageId() {
		return this.pkBirthdayMessageId;
	}

	public void setPkBirthdayMessageId(int pkBirthdayMessageId) {
		this.pkBirthdayMessageId = pkBirthdayMessageId;
	}

	public String getBirthdayMessage() {
		return this.birthdayMessage;
	}

	public void setBirthdayMessage(String birthdayMessage) {
		this.birthdayMessage = birthdayMessage;
	}

	public Date getBirthdayMessageCreatedDate() {
		return this.birthdayMessageCreatedDate;
	}

	public void setBirthdayMessageCreatedDate(Date birthdayMessageCreatedDate) {
		this.birthdayMessageCreatedDate = birthdayMessageCreatedDate;
	}

	public String getBirthdayMessageId() {
		return this.birthdayMessageId;
	}

	public void setBirthdayMessageId(String birthdayMessageId) {
		this.birthdayMessageId = birthdayMessageId;
	}


	
	public Integer getFkBirthdayMessageFromEmpId() {
		return fkBirthdayMessageFromEmpId;
	}

	public void setFkBirthdayMessageFromEmpId(Integer fkBirthdayMessageFromEmpId) {
		this.fkBirthdayMessageFromEmpId = fkBirthdayMessageFromEmpId;
	}

	public Integer getFkBirthdayMessageToEmpId() {
		return fkBirthdayMessageToEmpId;
	}

	public void setFkBirthdayMessageToEmpId(Integer fkBirthdayMessageToEmpId) {
		this.fkBirthdayMessageToEmpId = fkBirthdayMessageToEmpId;
	}

	@Override
	public String toString() {
		return "DatEmpBirthdayMessageBO [pkBirthdayMessageId="
				+ pkBirthdayMessageId + ", birthdayMessage=" + birthdayMessage
				+ ", birthdayMessageCreatedDate=" + birthdayMessageCreatedDate
				+ ", birthdayMessageId=" + birthdayMessageId
				+ ", fkBirthdayMessageFromEmpId=" + fkBirthdayMessageFromEmpId
				+ ", fkBirthdayMessageToEmpId=" + fkBirthdayMessageToEmpId
				+ "]";
	}

}