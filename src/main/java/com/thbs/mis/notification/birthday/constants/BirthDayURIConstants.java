/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrURIConstants.java                              */
/*                                                                   */
/*  Author      :  THBS                                              */
/*                                                                   */
/*  Date        : October 28, 2016                                   */
/*                                                                   */
/*  Description : This is used to create URI for all the services.   */
/*		 									                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        			Who     Version     Comments       			 */
/*-------------------------------------------------------------------*/
/* October 28, 2016     THBS     1.0        Initial version created  */
/*********************************************************************/

package com.thbs.mis.notification.birthday.constants;

/**
 * 
 * @author THBS
 * @Description: This is used to create URI for all the services.
 */
public class BirthDayURIConstants {
	
	/*	Please follow alphabetical order of uri */
	public static final String GET_RECENT_BIRTHDAY_LIST = "/notifications/recent/birthdays";
	public static final String GET_TODAY_BIRTHDAY_LIST = "/notifications/todays/birthdays";
	public static final String GET_UPCOMING_BIRTHDAY_LIST = "/notifications/upcoming/birthdays";
	public static final String SEND_BIRTHDAY_WISH_MESSAGE = "/notifications/birthday/messages";
	public static final String SEND_BIRTHDAY_REPLY_MESSAGE = "/notifications/birthday/messages/reply";
	
	
	
}
