/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  BirthDayService.java                              */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contain all the methods related to     */
/*                  the BirthDayService related calls			     */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        	  Who     Version      Comments            			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.notification.birthday.service;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.dao.EmployeeRepository;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notification.BirthdayEmpNameComparator;
import com.thbs.mis.notification.birthday.bean.BirthdayBean;
import com.thbs.mis.notification.birthday.bean.BirthdayMessageBean;
import com.thbs.mis.notification.birthday.bean.SendBirthdayReplyBean;
import com.thbs.mis.notification.birthday.bean.SendBirthdayWishBean;
import com.thbs.mis.notification.birthday.bo.DatEmpBirthdayMessageBO;
import com.thbs.mis.notification.birthday.dao.BirthDayRepository;

@Service
public class BirthDayService
{
	@Autowired
	private BirthDayRepository birthdayRepos;
	@Autowired
	private EmployeeRepository employeeRepos;
	
	@Value("${employee.photo.url}")
	private String empPhotoFilePathUrl;
	
	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory.getLog(BirthDayService.class);

	public BirthDayService()
	{
		super();
	}
	
	/**
	 * <Description getTodaysBirthdayList:> Fetch all today's birthday employee details
	 * @param
	 * @return List<BirthdayBean>
	 * @throws DataAccessException
	 */
	public List<BirthdayBean> getTodayBirthdayList() throws DataAccessException 
	{
		List<BirthdayBean> todayBirthdayBeanList = new ArrayList<BirthdayBean>();
		List<DatEmpPersonalDetailBO> datEmpPersonalDetailList = new ArrayList<DatEmpPersonalDetailBO>();
		List<DatEmpPersonalDetailBO> excludeEmpBirthdayList = new ArrayList<DatEmpPersonalDetailBO>();
		try {
			datEmpPersonalDetailList = employeeRepos.getAllTodaysBirthdayList();
			//Check the employee birthday notification flag is enabled or not
			excludeEmpBirthdayList = employeeRepos.getAllExcludeEmpBirthdayList();
		} 
		catch (Exception e) 
		{
			throw new DataAccessException("Failed to retrieve list of employees whose birthday's date is today : ", e);
		}
		
		if(!datEmpPersonalDetailList.isEmpty())
		{
			for (DatEmpPersonalDetailBO datEmpPersonalDetailBO : datEmpPersonalDetailList)
			{
				if(!excludeEmpBirthdayList.isEmpty())
				{
					for (DatEmpPersonalDetailBO excludeEmpBirthdayObj : excludeEmpBirthdayList) 
					{
						if(excludeEmpBirthdayObj.getFkEmpDetailId() != datEmpPersonalDetailBO.getFkEmpDetailId())
						{
							String photoUrlString;
							BirthdayBean birthdayBeanObject = new BirthdayBean();
							birthdayBeanObject.setEmployeeId(datEmpPersonalDetailBO.getFkEmpDetailId());
							birthdayBeanObject.setEmployeeFirstName(datEmpPersonalDetailBO.getEmpFirstName());
							birthdayBeanObject.setEmployeeLastName(datEmpPersonalDetailBO.getEmpLastName());
							birthdayBeanObject.setEmployeeGender(datEmpPersonalDetailBO.getEmpGender());
							birthdayBeanObject.setEmployeeDOB(datEmpPersonalDetailBO.getEmpDateOfBirth());
							birthdayBeanObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
									+ datEmpPersonalDetailBO.getEmpLastName() + "-" 
									+ datEmpPersonalDetailBO.getFkEmpDetailId());
							//Check whether photo is available or not
							photoUrlString = checkFileIsExistOrNot(empPhotoFilePathUrl + "\\" +datEmpPersonalDetailBO.getFkEmpDetailId() + ".png",  
									datEmpPersonalDetailBO.getEmpGender());
							birthdayBeanObject.setPhotoURL(photoUrlString);
							todayBirthdayBeanList.add(birthdayBeanObject);
						}
					}
				}else{
					String photoUrlString;
					BirthdayBean birthdayBeanObject = new BirthdayBean();
					birthdayBeanObject.setEmployeeId(datEmpPersonalDetailBO.getFkEmpDetailId());
					birthdayBeanObject.setEmployeeFirstName(datEmpPersonalDetailBO.getEmpFirstName());
					birthdayBeanObject.setEmployeeLastName(datEmpPersonalDetailBO.getEmpLastName());
					birthdayBeanObject.setEmployeeGender(datEmpPersonalDetailBO.getEmpGender());
					birthdayBeanObject.setEmployeeDOB(datEmpPersonalDetailBO.getEmpDateOfBirth());
					birthdayBeanObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
							+ datEmpPersonalDetailBO.getEmpLastName() + "-" 
							+ datEmpPersonalDetailBO.getFkEmpDetailId());
					//Check whether photo is available or not
					photoUrlString = checkFileIsExistOrNot(empPhotoFilePathUrl + "\\" +datEmpPersonalDetailBO.getFkEmpDetailId() + ".png",  
							datEmpPersonalDetailBO.getEmpGender());
					birthdayBeanObject.setPhotoURL(photoUrlString);
					todayBirthdayBeanList.add(birthdayBeanObject);
				}
			}
		
			//Sort by employee name
			Collections.sort(todayBirthdayBeanList, new BirthdayEmpNameComparator());
		}
		
		return todayBirthdayBeanList;
	}
	
	
	/*
	*<Description checkFileIsExistOrNot:> Check the employee photo is available at provided path.
	*  If it is not there then it will check the gender of an employee and show the general male or female image on their profile image  
	* @param String photoUrl, Byte gender
	* @return String
	* @throws 
	*/
	public String checkFileIsExistOrNot(String photoUrl, Byte gender)
	{
		String empPhotoPath="";
		File file = new File(photoUrl);
		if(file.exists() && !file.isDirectory())
		{
			empPhotoPath = photoUrl;
		}else{
			switch (gender) {
				case 1:
					empPhotoPath = empPhotoFilePathUrl + "\\MALE.png";
					break;
				case 2:
					empPhotoPath = empPhotoFilePathUrl + "\\FEMALE.png";
					break;
				default:
					empPhotoPath = null;
					LOG.error("Failed to get employee's gender.");
					break;
				}
		}
		return empPhotoPath; 
	}
	
	
	
	
	/**
	 * <Description getUpcomingBirthdayEmpList:> Fetch upcoming birthday details
	 * @param
	 * @return List<BirthdayBean>
	 * @throws DataAccessException
	 */
	public List<BirthdayBean> getUpcomingBirthdayEmpList() throws DataAccessException 
	{
		List<BirthdayBean> upcomingBirthdayBeanList = new ArrayList<BirthdayBean>();
		List<BirthdayBean> finalUpcomingBirthdayBeanList = new ArrayList<BirthdayBean>();
		List<DatEmpPersonalDetailBO> datEmpPersonalDetailList = new ArrayList<DatEmpPersonalDetailBO>();
		List<DatEmpPersonalDetailBO> excludeEmpBirthdayList = new ArrayList<DatEmpPersonalDetailBO>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM");
		Date todayDate = new Date();
		
		Calendar c = Calendar.getInstance(); 
		c.setTime(todayDate);
		c.add(Calendar.DATE,1);	
		Date firstDate = c.getTime();
		
		c.setTime(todayDate);
		c.add(Calendar.DATE,7);	
		Date lastDate = c.getTime();
		try 
		{
			datEmpPersonalDetailList = employeeRepos.getUpcomingBirthdayList(firstDate,lastDate);
			//Check the employee birthday notification flag is enabled or not
			excludeEmpBirthdayList = employeeRepos.getAllExcludeEmpBirthdayList();
		} 
		catch (Exception e) 
		{
			throw new DataAccessException("Failed to retrieve list of employees whose birthday's date are upcoming in 7 days : ", e);
		}
		
		if(!datEmpPersonalDetailList.isEmpty())
		{
			for (DatEmpPersonalDetailBO datEmpPersonalDetailBO : datEmpPersonalDetailList)
			{
				if(!excludeEmpBirthdayList.isEmpty())
				{
					for (DatEmpPersonalDetailBO excludeEmpBirthdayObj : excludeEmpBirthdayList) 
					{
						if(excludeEmpBirthdayObj.getFkEmpDetailId() != datEmpPersonalDetailBO.getFkEmpDetailId())
						{
							String photoUrlString;
							BirthdayBean birthdayBeanObject = new BirthdayBean();
							birthdayBeanObject.setEmployeeId(datEmpPersonalDetailBO.getFkEmpDetailId());
							birthdayBeanObject.setEmployeeFirstName(datEmpPersonalDetailBO.getEmpFirstName());
							birthdayBeanObject.setEmployeeLastName(datEmpPersonalDetailBO.getEmpLastName());
							birthdayBeanObject.setEmployeeGender(datEmpPersonalDetailBO.getEmpGender());
							birthdayBeanObject.setEmployeeDOB(datEmpPersonalDetailBO.getEmpDateOfBirth());
							birthdayBeanObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
									+ datEmpPersonalDetailBO.getEmpLastName() + "-" 
									+ datEmpPersonalDetailBO.getFkEmpDetailId());
							//Check whether photo is available or not
							photoUrlString = checkFileIsExistOrNot(empPhotoFilePathUrl + "\\" +datEmpPersonalDetailBO.getFkEmpDetailId() + ".png",  
									datEmpPersonalDetailBO.getEmpGender());
							birthdayBeanObject.setPhotoURL(photoUrlString);
							upcomingBirthdayBeanList.add(birthdayBeanObject);
						}
					}
				}else{
					String photoUrlString;
					BirthdayBean birthdayBeanObject = new BirthdayBean();
					birthdayBeanObject.setEmployeeId(datEmpPersonalDetailBO.getFkEmpDetailId());
					birthdayBeanObject.setEmployeeFirstName(datEmpPersonalDetailBO.getEmpFirstName());
					birthdayBeanObject.setEmployeeLastName(datEmpPersonalDetailBO.getEmpLastName());
					birthdayBeanObject.setEmployeeGender(datEmpPersonalDetailBO.getEmpGender());
					birthdayBeanObject.setEmployeeDOB(datEmpPersonalDetailBO.getEmpDateOfBirth());
					birthdayBeanObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
							+ datEmpPersonalDetailBO.getEmpLastName() + "-" 
							+ datEmpPersonalDetailBO.getFkEmpDetailId());
					//Check whether photo is available or not
					photoUrlString = checkFileIsExistOrNot(empPhotoFilePathUrl + "\\" +datEmpPersonalDetailBO.getFkEmpDetailId() + ".png",  
							datEmpPersonalDetailBO.getEmpGender());
					birthdayBeanObject.setPhotoURL(photoUrlString);
					upcomingBirthdayBeanList.add(birthdayBeanObject);
				}
			}	
		}
		
		if(!upcomingBirthdayBeanList.isEmpty())
		{
			for (int i = 1; i < 8; i++) 
			{
				List<BirthdayBean> birthdayBeanForADayList = new ArrayList<BirthdayBean>();
				Calendar cal = Calendar.getInstance(); 
				cal.setTime(todayDate);
				cal.add(Calendar.DATE, i);	
				Date dateCal = cal.getTime();
				
				String stringFormatDate = sdf.format(dateCal);
				Date dateFormatDate = new Date();
				try 
				{
					dateFormatDate = sdf.parse(stringFormatDate);
				} catch (ParseException e) 
				{
					LOG.error("Failed to convert from string to date " + e.getMessage());
					e.printStackTrace();
				}
				
				for (BirthdayBean birthdayBeanObj : upcomingBirthdayBeanList) 
				{
					String stringFormatDOB = sdf.format(birthdayBeanObj.getEmployeeDOB());
					Date dateFormatDOB = new Date();
					try {
						dateFormatDOB = sdf.parse(stringFormatDOB);
					} catch (ParseException e) 
					{
						LOG.error("Failed to convert from string to date " + e.getMessage());
						e.printStackTrace();
					}
					
					if(dateFormatDate.equals(dateFormatDOB))
					{
						birthdayBeanForADayList.add(birthdayBeanObj);
					}
				}
				
				Collections.sort(birthdayBeanForADayList, new BirthdayEmpNameComparator());
				finalUpcomingBirthdayBeanList.addAll(birthdayBeanForADayList);
			}
		}
		return finalUpcomingBirthdayBeanList;
	}

	
	
	/**
	 * <Description getRecentBirthdayEmpList:> Fetch recent birthday details
	 * @param
	 * @return List<BirthdayBean>
	 * @throws DataAccessException
	 */
	public List<BirthdayBean> getRecentBirthdayEmpList() throws DataAccessException 
	{
		List<BirthdayBean> recentBirthdayBeanList = new ArrayList<BirthdayBean>();
		List<BirthdayBean> empBirthdayBeanList = new ArrayList<BirthdayBean>();
		List<DatEmpPersonalDetailBO> excludeEmpBirthdayList = new ArrayList<DatEmpPersonalDetailBO>();
		List<DatEmpPersonalDetailBO> datEmpPersonalDetailList = new ArrayList<DatEmpPersonalDetailBO>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM");
		Date todayDate = new Date();
		
		Calendar c = Calendar.getInstance(); 
		c.setTime(todayDate);
		c.add(Calendar.DATE,-1);	
		Date firstDate = c.getTime();
		
		c.setTime(todayDate);
		c.add(Calendar.DATE,-7);	
		Date lastDate = c.getTime();
		try {
			datEmpPersonalDetailList = employeeRepos.getRecentBirthdayList(lastDate,firstDate);
			//Check the employee birthday notification flag is enabled or not
			excludeEmpBirthdayList = employeeRepos.getAllExcludeEmpBirthdayList();
		} 
		catch (Exception e) 
		{
			throw new DataAccessException("Failed to retrieve list of employees whose birthday's date falls on recent 7 days : ", e);
		}
		
		if(!datEmpPersonalDetailList.isEmpty())
		{
			for (DatEmpPersonalDetailBO datEmpPersonalDetailBO : datEmpPersonalDetailList)
			{
				if(!excludeEmpBirthdayList.isEmpty())
				{
					for (DatEmpPersonalDetailBO excludeEmpBirthdayObj : excludeEmpBirthdayList)
					{
						if(excludeEmpBirthdayObj.getFkEmpDetailId() != datEmpPersonalDetailBO.getFkEmpDetailId())
						{
							String photoUrlString;
							BirthdayBean birthdayBeanObject = new BirthdayBean();
							birthdayBeanObject.setEmployeeId(datEmpPersonalDetailBO.getFkEmpDetailId());
							birthdayBeanObject.setEmployeeFirstName(datEmpPersonalDetailBO.getEmpFirstName());
							birthdayBeanObject.setEmployeeLastName(datEmpPersonalDetailBO.getEmpLastName());
							birthdayBeanObject.setEmployeeGender(datEmpPersonalDetailBO.getEmpGender());
							birthdayBeanObject.setEmployeeDOB(datEmpPersonalDetailBO.getEmpDateOfBirth());
							birthdayBeanObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
									+ datEmpPersonalDetailBO.getEmpLastName() + "-" 
									+ datEmpPersonalDetailBO.getFkEmpDetailId());
							//Check whether photo is available or not
							photoUrlString = checkFileIsExistOrNot(empPhotoFilePathUrl + "\\" +datEmpPersonalDetailBO.getFkEmpDetailId() + ".png",  
									datEmpPersonalDetailBO.getEmpGender());
							birthdayBeanObject.setPhotoURL(photoUrlString);
							recentBirthdayBeanList.add(birthdayBeanObject);
						}
					}
				}else{
					String photoUrlString;
					BirthdayBean birthdayBeanObject = new BirthdayBean();
					birthdayBeanObject.setEmployeeId(datEmpPersonalDetailBO.getFkEmpDetailId());
					birthdayBeanObject.setEmployeeFirstName(datEmpPersonalDetailBO.getEmpFirstName());
					birthdayBeanObject.setEmployeeLastName(datEmpPersonalDetailBO.getEmpLastName());
					birthdayBeanObject.setEmployeeGender(datEmpPersonalDetailBO.getEmpGender());
					birthdayBeanObject.setEmployeeDOB(datEmpPersonalDetailBO.getEmpDateOfBirth());
					birthdayBeanObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
							+ datEmpPersonalDetailBO.getEmpLastName() + "-" 
							+ datEmpPersonalDetailBO.getFkEmpDetailId());
					//Check whether photo is available or not
					photoUrlString = checkFileIsExistOrNot(empPhotoFilePathUrl + "\\" +datEmpPersonalDetailBO.getFkEmpDetailId() + ".png",  
							datEmpPersonalDetailBO.getEmpGender());
					birthdayBeanObject.setPhotoURL(photoUrlString);
					recentBirthdayBeanList.add(birthdayBeanObject);
				}
			}	
		}
		
		//Need to sort according to date descending order
		if(!recentBirthdayBeanList.isEmpty())
		{
			for(int i = -1; i > -8; i--)
			{
				List<BirthdayBean> birthdayBeanForADayList = new ArrayList<BirthdayBean>();
				Calendar cal = Calendar.getInstance(); 
				cal.setTime(todayDate);
				cal.add(Calendar.DATE, i);	
				Date dateCal = cal.getTime();
				
				String stringFormatDate = sdf.format(dateCal);
				Date dateFormatDate = new Date();
				try 
				{
					dateFormatDate = sdf.parse(stringFormatDate);
				} catch (ParseException e) 
				{
					LOG.error("Failed to convert from string to date " + e.getMessage());
					e.printStackTrace();
				}
				
				for (BirthdayBean birthdayBeanObj : recentBirthdayBeanList) 
				{
					String stringFormatDOB = sdf.format(birthdayBeanObj.getEmployeeDOB());
					Date dateFormatDOB = new Date();
					try {
						dateFormatDOB = sdf.parse(stringFormatDOB);
					} catch (ParseException e) 
					{
						LOG.error("Failed to convert from string to date " + e.getMessage());
						e.printStackTrace();
					}
					
					if(dateFormatDate.equals(dateFormatDOB))
					{
						birthdayBeanForADayList.add(birthdayBeanObj);
					}
				}
				
				Collections.sort(birthdayBeanForADayList, new BirthdayEmpNameComparator());
				empBirthdayBeanList.addAll(birthdayBeanForADayList);
			}
		}		 
		return empBirthdayBeanList;
	}

	
	/**
	 * <Description sendBirthdayWishes:> Send birthday wishes details
	 * @param SendBirthdayWishBean birthdayWishBean
	 * @return BirthdayMessageBean
	 * @throws DataAccessException
	 */
	public BirthdayMessageBean sendBirthdayWishes(SendBirthdayWishBean birthdayWishBean)
			throws DataAccessException 
	{
		DatEmpBirthdayMessageBO datEmpBirthdayMessageObject = new DatEmpBirthdayMessageBO();
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");
		Date todayDate = new Date();
		String todayDateString = sdf.format(todayDate);
		LOG.info("todayDateString :: " + todayDateString);
		String messageIdString = birthdayWishBean.getFromEmpId() + "_"
							+ birthdayWishBean.getToEmpId() + "_"
							+ todayDateString;
		
		//Setting values into BO
		DatEmpBirthdayMessageBO datEmpBirthdayMessageBOObject = new DatEmpBirthdayMessageBO();
		datEmpBirthdayMessageBOObject.setFkBirthdayMessageFromEmpId(birthdayWishBean.getFromEmpId());
		datEmpBirthdayMessageBOObject.setFkBirthdayMessageToEmpId(birthdayWishBean.getToEmpId());
		datEmpBirthdayMessageBOObject.setBirthdayMessage(birthdayWishBean.getWishMessage());
		datEmpBirthdayMessageBOObject.setBirthdayMessageCreatedDate(new Date());
		datEmpBirthdayMessageBOObject.setBirthdayMessageId(messageIdString);
		
		datEmpBirthdayMessageObject = birthdayRepos.save(datEmpBirthdayMessageBOObject);
		
		//Setting values on Bean from BO
		BirthdayMessageBean birthdayMessageBeanObject = new BirthdayMessageBean(); 
		birthdayMessageBeanObject.setPkBirthdayMessageId(datEmpBirthdayMessageObject.getPkBirthdayMessageId());
		birthdayMessageBeanObject.setFkFromEmpid(datEmpBirthdayMessageObject.getFkBirthdayMessageFromEmpId());
		birthdayMessageBeanObject.setFkToEmpid(datEmpBirthdayMessageObject.getFkBirthdayMessageToEmpId());
		birthdayMessageBeanObject.setBirthdayMessage(datEmpBirthdayMessageObject.getBirthdayMessage());
		birthdayMessageBeanObject.setBirthdayMessageId(datEmpBirthdayMessageObject.getBirthdayMessageId());
		birthdayMessageBeanObject.setBirthdayMessageCreatedDate(datEmpBirthdayMessageObject.getBirthdayMessageCreatedDate());
		
		return birthdayMessageBeanObject;
	}
	
	
	/**
	 * <Description sendBirthdayReply:> Send birthday reply details
	 * @param SendBirthdayReplyBean birthdayReplyBean
	 * @return BirthdayMessageBean
	 * @throws DataAccessException
	 */
	public BirthdayMessageBean sendBirthdayReply(SendBirthdayReplyBean birthdayReplyBean)
			throws DataAccessException 
	{
		DatEmpBirthdayMessageBO datEmpBirthdayMessageObject = new DatEmpBirthdayMessageBO();
		
		//Setting values on BO
		DatEmpBirthdayMessageBO datEmpBirthdayMessageBOObj = new DatEmpBirthdayMessageBO();
		datEmpBirthdayMessageBOObj.setFkBirthdayMessageFromEmpId(birthdayReplyBean.getFromEmpId());
		datEmpBirthdayMessageBOObj.setFkBirthdayMessageToEmpId(birthdayReplyBean.getToEmpId());
		datEmpBirthdayMessageBOObj.setBirthdayMessage(birthdayReplyBean.getReplyMessage());
		datEmpBirthdayMessageBOObj.setBirthdayMessageId(birthdayReplyBean.getReplyMessageId());
		datEmpBirthdayMessageBOObj.setBirthdayMessageCreatedDate(new Date());
		
		datEmpBirthdayMessageObject = birthdayRepos.save(datEmpBirthdayMessageBOObj);
		
		//Setting values on Bean from BO
		BirthdayMessageBean birthdayMessageBeanObject = new BirthdayMessageBean(); 
		birthdayMessageBeanObject.setPkBirthdayMessageId(datEmpBirthdayMessageObject.getPkBirthdayMessageId());
		birthdayMessageBeanObject.setFkFromEmpid(datEmpBirthdayMessageObject.getFkBirthdayMessageFromEmpId());
		birthdayMessageBeanObject.setFkToEmpid(datEmpBirthdayMessageObject.getFkBirthdayMessageToEmpId());
		birthdayMessageBeanObject.setBirthdayMessage(datEmpBirthdayMessageObject.getBirthdayMessage());
		birthdayMessageBeanObject.setBirthdayMessageId(datEmpBirthdayMessageObject.getBirthdayMessageId());
		birthdayMessageBeanObject.setBirthdayMessageCreatedDate(datEmpBirthdayMessageObject.getBirthdayMessageCreatedDate());
		
		return birthdayMessageBeanObject;
	}
	
	
}
