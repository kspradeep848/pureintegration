/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  BirthdayBean.java                           		 */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  17-Jan-2017                                       */
/*                                                                   */
/*  Description :  This bean related to birthday. This bean have     */
/* 					employee id, employee first name, employee last  */
/* 					name, gender, photoUrl and DOB.    				 */  
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/*  Date          Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 23-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.notification.birthday.bean;

import java.util.Date;

public class BirthdayBean {
	
	private Integer employeeId;
	private String employeeFirstName;
	private String employeeLastName;
	private String empFullNameWithId;
	private Byte employeeGender;
	private Date employeeDOB;
	private String photoURL;
	
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeFirstName() {
		return employeeFirstName;
	}
	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}
	public String getEmployeeLastName() {
		return employeeLastName;
	}
	public void setEmployeeLastName(String employeeLastName) {
		this.employeeLastName = employeeLastName;
	}
	public Byte getEmployeeGender() {
		return employeeGender;
	}
	public void setEmployeeGender(Byte employeeGender) {
		this.employeeGender = employeeGender;
	}
	public Date getEmployeeDOB() {
		return employeeDOB;
	}
	public void setEmployeeDOB(Date employeeDOB) {
		this.employeeDOB = employeeDOB;
	}
	public String getPhotoURL() {
		return photoURL;
	}
	public void setPhotoURL(String photoURL) {
		this.photoURL = photoURL;
	}
	public String getEmpFullNameWithId() {
		return empFullNameWithId;
	}
	public void setEmpFullNameWithId(String empFullNameWithId) {
		this.empFullNameWithId = empFullNameWithId;
	}
	@Override
	public String toString() {
		return "BirthdayBean [employeeId=" + employeeId
				+ ", employeeFirstName=" + employeeFirstName
				+ ", employeeLastName=" + employeeLastName
				+ ", empFullNameWithId=" + empFullNameWithId
				+ ", employeeGender=" + employeeGender + ", employeeDOB="
				+ employeeDOB + ", photoURL=" + photoURL + "]";
	}
	
	

}
