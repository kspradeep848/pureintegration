/*********************************************************************/
/*                                                                   */
/*  FileName    :  BirthDayController.java                           */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the methods for the           */
/*                 BirthDayController                                */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        	   Who     Version      Comments           			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.notification.birthday.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.common.controller.CommonController;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notification.birthday.bean.BirthdayBean;
import com.thbs.mis.notification.birthday.bean.BirthdayMessageBean;
import com.thbs.mis.notification.birthday.bean.SendBirthdayReplyBean;
import com.thbs.mis.notification.birthday.bean.SendBirthdayWishBean;
import com.thbs.mis.notification.birthday.constants.BirthDayURIConstants;
import com.thbs.mis.notification.birthday.service.BirthDayService;

@Api(value = "Birthday Operations", 
description = "Birthday Operations", 
basePath = "http://localhost:8080/", consumes = "Rest Service", 
position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "Birthday Services")


@Controller
public class BirthDayController
{
	
	/**
	 * Instance of <code>Log</code>
	 */
	@SuppressWarnings("unused")
	private static final AppLog LOG = LogFactory.getLog(CommonController.class);
	
	@Autowired
	private BirthDayService birthdayService;
	
	/**@category This service is used for UI page to show birthday details.
	 * <Description getTodayBirthdayList : Fetch employee list whose birthday is today >
	 * @param
	 * @return List<BirthdayBean>
	 * @throws BusinessException
	 * @throws DataAccessException 
	 */
	@ApiOperation(value = "Get list of employees whose birthday's date is today. "
			+ "This service will be called from the front-end when user want to show/view list of employees(whose birthday's date is today )" + " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch all employees whose birthday's date is today."
			+ "<br>The Response will be the detailed of the employee whose birthday's date is today.",

	nickname = "All employees whose birthday's date is today",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = BirthdayBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BirthdayBean.class),
			@ApiResponse(code = 201, message = "All active employees whose birthday's date is today are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All employees detail whose birthday's date is today", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All employees detail whose birthday's date is today", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = BirthDayURIConstants.GET_TODAY_BIRTHDAY_LIST, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getTodayBirthdayList()
			throws BusinessException, DataAccessException
	{
		List<BirthdayBean> birthdayList =  null;
		try
		{
			birthdayList = birthdayService.getTodayBirthdayList();
		} 
		catch (DataAccessException e)
		{
			throw new BusinessException("Failed to retrieve all active employees whose birthday's date is on today ", e);
		}
		
		if(!birthdayList.isEmpty())
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"Today's birthday list of active employees have been retrieved successfully", birthdayList));
		}else{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"No today's birthday entry found", birthdayList));
		}
	}
	
	
	
	
	/**@category This service is used for UI page to show upcoming birthday details.
	 * <Description getUpcomingBirthdayList : Fetch all active employees list whose birthday are falls on coming 7 days (start from today). >
	 * @param
	 * @return List<BirthdayBean>
	 * @throws BusinessException
	 * @throws DataAccessException 
	 */
	@ApiOperation(value = "Get all active employees whose birthday's date are fall on coming 7 days. "
			+ "This service will be called from the front-end when user want to show/view list of employees, whose birthday's date are falls on coming 7 days" + " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch all employees whose birthday's date are falls on coming 7 days."
			+ "<br>The Response will be the detailed of the employee whose birthday's date are falls on coming 7 days.",

	nickname = "All employees whose birhday's date are falls on coming 7 days",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = BirthdayBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BirthdayBean.class),
			@ApiResponse(code = 201, message = "All active employee whose birthday's date are falls on coming 7 days, are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All employees detail whose birthday's date falls on coming 7 days", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All employees detail whose birthday's date falls on coming 7 days.", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = BirthDayURIConstants.GET_UPCOMING_BIRTHDAY_LIST, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getUpcomingBirthdayList()
			throws BusinessException, DataAccessException
	{
		List<BirthdayBean> birthdayList =  null;
		try
		{
			birthdayList = birthdayService.getUpcomingBirthdayEmpList();
		} 
		catch (DataAccessException e)
		{
			throw new BusinessException("Failed to retrieve all upcoming birthday list", e);
		}
		
		if(!birthdayList.isEmpty())
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"Upcoming birthday list of active employees have been retrieved successfully", birthdayList));
		}else{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"No upcoming birthday found", birthdayList));
		}
	}
	
	
	
	/**@category This service is used for UI page to show recent birthday details.
	 * <Description getRecentBirthdayList : Fetch all active employees list whose birthday's date are falls on recent 7 days. >
	 * @param
	 * @return List<BirthdayBean>
	 * @throws BusinessException
	 * @throws DataAccessException 
	 */
	@ApiOperation(value = "Get all active employee whose birthday's date are fall on recent 7 days. "
			+ "This service will be called from the front-end when user want to show/view list of employees, whose birthday's date are falls on recent 7 days" + " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch all employees whose birthday's date are falls on recent 7 days."
			+ "<br>The Response will be the detailed of the employee whose birthday's date are falls on recent 7 days.",

	nickname = "All active employees whose birhday's date are falls on recent 7 days",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = BirthdayBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BirthdayBean.class),
			@ApiResponse(code = 201, message = "All active employees whose birthday's date are falls on recent 7 days, are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All employees detail whose birthday's date falls on recent 7 days", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All employees detail whose birthday's date falls on recent 7 days.", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = BirthDayURIConstants.GET_RECENT_BIRTHDAY_LIST, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getRecentBirthdayList()
			throws BusinessException, DataAccessException
	{
		List<BirthdayBean> birthdayList =  null;
		try
		{
			birthdayList = birthdayService.getRecentBirthdayEmpList();
		} 
		catch (DataAccessException e)
		{
			throw new BusinessException("Failed to retrieve all recent birthday list", e);
		}
		
		if(!birthdayList.isEmpty())
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"Recent birthday list of active employees have been retrieved successfully", birthdayList));
		}else{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"No recent birthday found", birthdayList));
		}
	}
	
	
	
	
	/**@category This service is used for UI page to send birthday wish message.
	 * <Description sendBirthdayWishMessage : To send the birthday wishes services. >
	 * @param SendBirthdayWishBean
	 * @return ResponseEntity<MISResponse>
	 * @throws BusinessException
	 * @throws DataAccessException 
	 */
	@ApiOperation(value = "This service will be called from the front-end when user send birthday wish" + " .", httpMethod = "GET",

	notes = "This Service has been implemented to send birthday wishes."
			+ "<br>The Response will be show the success or failure message with http status code.",

	nickname = "Store birthday wish message into database",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = BirthdayMessageBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BirthdayMessageBean.class),
			@ApiResponse(code = 201, message = "Birthday Wish Message are stored successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Birthday wish message are stored successfully into database.", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Birthday wish message are stored successfully into database.", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = BirthDayURIConstants.SEND_BIRTHDAY_WISH_MESSAGE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> sendBirthdayWishMessage(
			@ApiParam(name = "SendBirthdayWishBean", example = "SendBirthdayWishBean", value = "Birthday Wish Bean have datas which are required to store birthday wish message into database.")
			@Valid @RequestBody SendBirthdayWishBean inputBean)
			throws BusinessException, DataAccessException
	{
		BirthdayMessageBean birthdayMassageBean =  null;
		try
		{
			birthdayMassageBean = birthdayService.sendBirthdayWishes(inputBean);
		} 
		catch (DataAccessException e)
		{
			throw new BusinessException("Operation failed while adding birthday wish message into database.", e);
		}
		
		if(null != birthdayMassageBean)
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"Birthday wish message have been stored successfully"));
		}else{
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, 
							"Error occurred while storing birthday wish message into database."));
		}
	}
	
	
	
	/**@category This service is used for UI page to send birthday reply message.
	 * <Description sendBirthdayReplyMessage : To store the birthday reply message services. >
	 * @param SendBirthdayReplyBean inputBean
	 * @return ResponseEntity<MISResponse>
	 * @throws BusinessException
	 * @throws DataAccessException 
	 */
	@ApiOperation(value = "This service will be called from the front-end when user reply to birthday wish" + " .", httpMethod = "GET",

	notes = "This Service has been implemented to send birthday reply message."
			+ "<br>The Response will be show the success or failure message with http status code.",

	nickname = "Store birthday reply message into database",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = BirthdayMessageBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BirthdayMessageBean.class),
			@ApiResponse(code = 201, message = "Birthday Reply Message are stored successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Birthday reply message are stored successfully into database", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Birthday reply message are stored successfully into database", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = BirthDayURIConstants.SEND_BIRTHDAY_REPLY_MESSAGE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> sendBirthdayReplyMessage(
			@ApiParam(name = "SendBirthdayReplyBean", example = "SendBirthdayReplyBean", value = "Birthday Reply Bean have datas which are required to store birthday reply message into database.")
			@Valid @RequestBody SendBirthdayReplyBean inputBean )
			throws BusinessException, DataAccessException
	{
		BirthdayMessageBean birthdayMassageBean =  null;
		try{
			birthdayMassageBean = birthdayService.sendBirthdayReply(inputBean);
		} 
		catch (DataAccessException e)
		{
			throw new BusinessException("Operation failed while storing birthday reply message into database.", e);
		}
		
		if(null != birthdayMassageBean)
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"Birthday reply message have been stored successfully"));
		}else{
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, 
							"Error occurred while storing birthday reply message."));
		}
	}
	
	
}
