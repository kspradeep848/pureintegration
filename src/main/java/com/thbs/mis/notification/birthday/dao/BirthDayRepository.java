/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  BirthDayRepository.java                           */
/*                                                                   */
/*  Author      :  THBS                                              */
/*                                                                   */
/*  Date        :  October 28, 2016                                  */
/*                                                                   */
/*  Description :  This class contains all the methods               */
/*                 for the BirthDayRepository                        */			 									
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        			Who     Version      Comments            	 */
/*-------------------------------------------------------------------*/
/* October 28, 2016     THBS     1.0        Initial version created  */
/*********************************************************************/


package com.thbs.mis.notification.birthday.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.notification.birthday.bo.DatEmpBirthdayMessageBO;

/**
 * 
 * @author THBS
 * @version:1.0
 * @Description: This is BirthDayRepository.
 */
@Repository
public interface BirthDayRepository extends GenericRepository<DatEmpBirthdayMessageBO,Long>
{
	
	
}
