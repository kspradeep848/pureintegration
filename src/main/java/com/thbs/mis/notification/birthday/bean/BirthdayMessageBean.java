/*********************************************************************/
/*                  FILE HEADER                                      */


/*********************************************************************/
/*                                                                   */
/*  FileName    :  BirthdayMessageBean.java                                		 */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        : 17-Jan-2017                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 17-Jan-2017   THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.notification.birthday.bean;

import java.util.Date;

public class BirthdayMessageBean {
	 
	private Integer pkBirthdayMessageId;
	private String birthdayMessage;
	private Date birthdayMessageCreatedDate;
	private String birthdayMessageId;
	private Integer fkFromEmpid;
	private Integer fkToEmpid;
	
	public Integer getPkBirthdayMessageId() {
		return pkBirthdayMessageId;
	}
	public void setPkBirthdayMessageId(Integer pkBirthdayMessageId) {
		this.pkBirthdayMessageId = pkBirthdayMessageId;
	}
	public String getBirthdayMessage() {
		return birthdayMessage;
	}
	public void setBirthdayMessage(String birthdayMessage) {
		this.birthdayMessage = birthdayMessage;
	}
	public Date getBirthdayMessageCreatedDate() {
		return birthdayMessageCreatedDate;
	}
	public void setBirthdayMessageCreatedDate(Date birthdayMessageCreatedDate) {
		this.birthdayMessageCreatedDate = birthdayMessageCreatedDate;
	}
	public String getBirthdayMessageId() {
		return birthdayMessageId;
	}
	public void setBirthdayMessageId(String birthdayMessageId) {
		this.birthdayMessageId = birthdayMessageId;
	}
	public Integer getFkFromEmpid() {
		return fkFromEmpid;
	}
	public void setFkFromEmpid(Integer fkFromEmpid) {
		this.fkFromEmpid = fkFromEmpid;
	}
	public Integer getFkToEmpid() {
		return fkToEmpid;
	}
	public void setFkToEmpid(Integer fkToEmpid) {
		this.fkToEmpid = fkToEmpid;
	}
	@Override
	public String toString() {
		return "BirthdayBean [pkBirthdayMessageId=" + pkBirthdayMessageId
				+ ", birthdayMessage=" + birthdayMessage
				+ ", birthdayMessageCreatedDate=" + birthdayMessageCreatedDate
				+ ", birthdayMessageId=" + birthdayMessageId + ", fkFromEmpid="
				+ fkFromEmpid + ", fkToEmpid=" + fkToEmpid + "]";
	}
	
}
