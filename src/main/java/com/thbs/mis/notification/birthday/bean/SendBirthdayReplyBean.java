package com.thbs.mis.notification.birthday.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;


public class SendBirthdayReplyBean 
{
	@Min(1)
	@NotNull(message = "fromEmpId field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "From Employee Id should be a numeric value")
	private Integer fromEmpId;
	
	@Min(1)
	@NotNull(message = "toEmpId field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "To Employee Id should be a numeric value")
	private Integer toEmpId;
	
	@Length(min=1,max=250)
	@NotBlank(message = "replyMessage field can not be empty")
	@NotNull(message = "replyMessage field can not be null")
	private String replyMessage;
	
	@Length(min=1,max=50)
	@NotEmpty(message = "replyMessageId field can not be empty")
	@NotNull(message = "replyMessageId field can not be null")
	private String replyMessageId;
	
	public Integer getFromEmpId() {
		return fromEmpId;
	}
	public void setFromEmpId(Integer fromEmpId) {
		this.fromEmpId = fromEmpId;
	}
	public Integer getToEmpId() {
		return toEmpId;
	}
	public void setToEmpId(Integer toEmpId) {
		this.toEmpId = toEmpId;
	}
	public String getReplyMessage() {
		return replyMessage;
	}
	public void setReplyMessage(String replyMessage) {
		this.replyMessage = replyMessage;
	}
	public String getReplyMessageId() {
		return replyMessageId;
	}
	public void setReplyMessageId(String replyMessageId) {
		this.replyMessageId = replyMessageId;
	}
	
	@Override
	public String toString() {
		return "SendBirthdayReplyBean [fromEmpId=" + fromEmpId + ", toEmpId="
				+ toEmpId + ", replyMessage=" + replyMessage
				+ ", replyMessageId=" + replyMessageId + "]";
	}
	
	
	
}
