package com.thbs.mis.notification;

import java.util.Comparator;

import com.thbs.mis.notification.anniversary.bean.AnniversaryBean;

public class AnniversaryEmpNameComparator implements Comparator<AnniversaryBean>
{
	public int compare(AnniversaryBean object1,AnniversaryBean object2)
	{
		return object1.getEmpFullNameWithId().compareTo(object2.getEmpFullNameWithId());
	}
}
