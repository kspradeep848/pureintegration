package com.thbs.mis.notification.anniversary.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class SendAnniversaryWishBean {
	
	@Min(1)
	@NotNull(message = "fromEmpId field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "From Employee Id should be a numeric value")
	private Integer fromEmpId;
	
	@Min(1)
	@NotNull(message = "fromEmpId field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "From Employee Id should be a numeric value")
	private Integer toEmpId;
	
	@NotNull(message = "wishMessage field can not be null")
	private String wishMessage;

	public Integer getFromEmpId() {
		return fromEmpId;
	}

	public void setFromEmpId(Integer fromEmpId) {
		this.fromEmpId = fromEmpId;
	}

	public Integer getToEmpId() {
		return toEmpId;
	}

	public void setToEmpId(Integer toEmpId) {
		this.toEmpId = toEmpId;
	}

	public String getWishMessage() {
		return wishMessage;
	}

	public void setWishMessage(String wishMessage) {
		this.wishMessage = wishMessage;
	}

	@Override
	public String toString() {
		return "SendAnniversaryWishBean [fromEmpId=" + fromEmpId + ", toEmpId="
				+ toEmpId + ", wishMessage=" + wishMessage + "]";
	}
	
	
}
