package com.thbs.mis.notification.anniversary.bean;

import java.util.Date;

public class AnniversaryBean {
	
	private Integer empId;
	private String empFirstName;
	private String empLastName;
	private String empFullNameWithId;
	private Byte empGender;
	private Date empDOJ;
	private String empPhotoURL;
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getEmpFirstName() {
		return empFirstName;
	}
	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}
	public String getEmpLastName() {
		return empLastName;
	}
	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}
	public String getEmpFullNameWithId() {
		return empFullNameWithId;
	}
	public void setEmpFullNameWithId(String empFullNameWithId) {
		this.empFullNameWithId = empFullNameWithId;
	}
	public Byte getEmpGender() {
		return empGender;
	}
	public void setEmpGender(Byte empGender) {
		this.empGender = empGender;
	}
	public Date getEmpDOJ() {
		return empDOJ;
	}
	public void setEmpDOJ(Date empDOJ) {
		this.empDOJ = empDOJ;
	}
	public String getEmpPhotoURL() {
		return empPhotoURL;
	}
	public void setEmpPhotoURL(String empPhotoURL) {
		this.empPhotoURL = empPhotoURL;
	}
	@Override
	public String toString() {
		return "AnniversaryBean [empId=" + empId + ", empFirstName="
				+ empFirstName + ", empLastName=" + empLastName
				+ ", empFullNameWithId=" + empFullNameWithId + ", empGender="
				+ empGender + ", empDOJ=" + empDOJ + ", empPhotoURL="
				+ empPhotoURL + "]";
	}
	
}
