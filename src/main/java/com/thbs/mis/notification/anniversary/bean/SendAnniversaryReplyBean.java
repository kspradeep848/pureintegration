package com.thbs.mis.notification.anniversary.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class SendAnniversaryReplyBean {
	
	@Min(1)
	@NotNull(message = "fromEmpId field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "From Employee Id should be a numeric value")
	private Integer fromEmpId;
	
	@Min(1)
	@NotNull(message = "toEmpId field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "To Employee Id should be a numeric value")
	private Integer toEmpId;
	
	@NotNull(message = "replyMessage field can not be null")
	private String replyMessage;
	
	@NotNull(message = "replyMessageId field can not be null")
	private String replyMessageId;

	public Integer getFromEmpId() {
		return fromEmpId;
	}

	public void setFromEmpId(Integer fromEmpId) {
		this.fromEmpId = fromEmpId;
	}

	public Integer getToEmpId() {
		return toEmpId;
	}

	public void setToEmpId(Integer toEmpId) {
		this.toEmpId = toEmpId;
	}

	public String getReplyMessage() {
		return replyMessage;
	}

	public void setReplyMessage(String replyMessage) {
		this.replyMessage = replyMessage;
	}

	public String getReplyMessageId() {
		return replyMessageId;
	}

	public void setReplyMessageId(String replyMessageId) {
		this.replyMessageId = replyMessageId;
	}

	@Override
	public String toString() {
		return "SendAnniversaryReplyMessageBean [fromEmpId=" + fromEmpId
				+ ", toEmpId=" + toEmpId + ", replyMessage=" + replyMessage
				+ ", replyMessageId=" + replyMessageId + "]";
	}
	

}
