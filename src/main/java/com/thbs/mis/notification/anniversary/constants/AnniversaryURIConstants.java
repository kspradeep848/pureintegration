/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrURIConstants.java                              */
/*                                                                   */
/*  Author      :  THBS                                              */
/*                                                                   */
/*  Date        : October 28, 2016                                   */
/*                                                                   */
/*  Description : This is used to create URI for all the services.   */
/*		 									                         */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* October 28, 2016     THBS     1.0        Initial version created  */
/*********************************************************************/

package com.thbs.mis.notification.anniversary.constants;

/**
 * 
 * @author THBS
 * @Description: This is used to create URI for all the services.
 */
public class AnniversaryURIConstants {
	
	/*	Please follow alphabetical order of uri */
	public static final String GET_RECENT_ANNIVERSARY_LIST = "/notifications/recent/anniversaries";
	public static final String GET_TODAY_ANNIVERSARY_LIST = "/notifications/todays/anniversaries";
	public static final String GET_UPCOMING_ANNIVERSARY_LIST = "/notifications/upcoming/anniversaries";
	public static final String SEND_ANNIVERSARY_WISH_MESSAGE = "/notifications/anniversary/messages";
	public static final String SEND_ANNIVERSARY_REPLY_MESSAGE = "/notifications/anniversary/messages/reply";
}
