package com.thbs.mis.notification.anniversary.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;


/**
 * The persistent class for the dat_emp_anniversary_message database table.
 * 
 */
@Entity
@Table(name="dat_emp_anniversary_message")
@NamedQuery(name="DatEmpAnniversaryMessageBO.findAll", query="SELECT d FROM DatEmpAnniversaryMessageBO d")
public class DatEmpAnniversaryMessageBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_anniversary_message_id")
	private int pkAnniversaryMessageId;

	@Column(name="anniversary_message")
	private String anniversaryMessage;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="anniversary_message_created_date")
	private Date anniversaryMessageCreatedDate;

	@Column(name="anniversary_message_id")
	private String anniversaryMessageId;

	@Column(name="fk_anniversary_message_from_emp_id")
	private Integer fkAnniversaryMessageFromEmpId;
	
	@Column(name="fk_anniversary_message_to_emp_id")
	private Integer fkAnniversaryMessageToEmpId;

	//bi-directional many-to-one association to DatEmpDetail
	/*@OneToOne
	@JoinColumn(name="fk_anniversary_message_from_emp_id")

	//uni-directional one-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="fk_anniversary_message_from_emp_id",unique = true, nullable = true, insertable = false, updatable = false)

	private DatEmpDetailBO datEmpDetail1;

	//uni-directional many-to-one association to DatEmpDetail
	@OneToOne

	@JoinColumn(name="fk_anniversary_message_to_emp_id")
	private DatEmpDetailBO datEmpDetail2;*/
/*
	@JoinColumn(name="fk_anniversary_message_to_emp_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail2;*/


	public int getPkAnniversaryMessageId() {
		return pkAnniversaryMessageId;
	}

	public void setPkAnniversaryMessageId(int pkAnniversaryMessageId) {
		this.pkAnniversaryMessageId = pkAnniversaryMessageId;
	}

	public String getAnniversaryMessage() {
		return anniversaryMessage;
	}

	public void setAnniversaryMessage(String anniversaryMessage) {
		this.anniversaryMessage = anniversaryMessage;
	}

	public Date getAnniversaryMessageCreatedDate() {
		return anniversaryMessageCreatedDate;
	}

	public void setAnniversaryMessageCreatedDate(Date anniversaryMessageCreatedDate) {
		this.anniversaryMessageCreatedDate = anniversaryMessageCreatedDate;
	}

	public String getAnniversaryMessageId() {
		return anniversaryMessageId;
	}

	public void setAnniversaryMessageId(String anniversaryMessageId) {
		this.anniversaryMessageId = anniversaryMessageId;
	}

	public Integer getFkAnniversaryMessageFromEmpId() {
		return fkAnniversaryMessageFromEmpId;
	}

	public void setFkAnniversaryMessageFromEmpId(
			Integer fkAnniversaryMessageFromEmpId) {
		this.fkAnniversaryMessageFromEmpId = fkAnniversaryMessageFromEmpId;
	}

	public Integer getFkAnniversaryMessageToEmpId() {
		return fkAnniversaryMessageToEmpId;
	}

	public void setFkAnniversaryMessageToEmpId(Integer fkAnniversaryMessageToEmpId) {
		this.fkAnniversaryMessageToEmpId = fkAnniversaryMessageToEmpId;
	}

	@Override
	public String toString() {
		return "DatEmpAnniversaryMessageBO [pkAnniversaryMessageId="
				+ pkAnniversaryMessageId + ", anniversaryMessage="
				+ anniversaryMessage + ", anniversaryMessageCreatedDate="
				+ anniversaryMessageCreatedDate + ", anniversaryMessageId="
				+ anniversaryMessageId + ", fkAnniversaryMessageFromEmpId="
				+ fkAnniversaryMessageFromEmpId
				+ ", fkAnniversaryMessageToEmpId="
				+ fkAnniversaryMessageToEmpId + "]";
	}

	
	/*public DatEmpDetailBO getDatEmpDetail1() {
		return this.datEmpDetail1;

	}

	public void setDatEmpDetail1(DatEmpDetailBO datEmpDetail1) {
		this.datEmpDetail1 = datEmpDetail1;
	}

	public DatEmpDetailBO getDatEmpDetail2() {
		return datEmpDetail2;
	}

	public void setDatEmpDetail2(DatEmpDetailBO datEmpDetail2) {
		this.datEmpDetail2 = datEmpDetail2;
	}*/

	
	

}