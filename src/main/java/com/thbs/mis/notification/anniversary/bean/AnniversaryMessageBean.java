package com.thbs.mis.notification.anniversary.bean;

import java.util.Date;

public class AnniversaryMessageBean {
	
	private Integer pkAnniversaryMessageId;
	private String anniversaryMessage;
	private Date anniversaryMessageCreatedDate;
	private String anniversaryMessageId;
	private Integer fkFromEmpid;
	private Integer fkToEmpid;
	
	public Integer getPkAnniversaryMessageId() {
		return pkAnniversaryMessageId;
	}
	public void setPkAnniversaryMessageId(Integer pkAnniversaryMessageId) {
		this.pkAnniversaryMessageId = pkAnniversaryMessageId;
	}
	public String getAnniversaryMessage() {
		return anniversaryMessage;
	}
	public void setAnniversaryMessage(String anniversaryMessage) {
		this.anniversaryMessage = anniversaryMessage;
	}
	public Date getAnniversaryMessageCreatedDate() {
		return anniversaryMessageCreatedDate;
	}
	public void setAnniversaryMessageCreatedDate(Date anniversaryMessageCreatedDate) {
		this.anniversaryMessageCreatedDate = anniversaryMessageCreatedDate;
	}
	public String getAnniversaryMessageId() {
		return anniversaryMessageId;
	}
	public void setAnniversaryMessageId(String anniversaryMessageId) {
		this.anniversaryMessageId = anniversaryMessageId;
	}
	public Integer getFkFromEmpid() {
		return fkFromEmpid;
	}
	public void setFkFromEmpid(Integer fkFromEmpid) {
		this.fkFromEmpid = fkFromEmpid;
	}
	public Integer getFkToEmpid() {
		return fkToEmpid;
	}
	public void setFkToEmpid(Integer fkToEmpid) {
		this.fkToEmpid = fkToEmpid;
	}
	@Override
	public String toString() {
		return "AnniversaryMessageBean [pkAnniversaryMessageId="
				+ pkAnniversaryMessageId + ", anniversaryMessage="
				+ anniversaryMessage + ", anniversaryMessageCreatedDate="
				+ anniversaryMessageCreatedDate + ", anniversaryMessageId="
				+ anniversaryMessageId + ", fkFromEmpid=" + fkFromEmpid
				+ ", fkToEmpid=" + fkToEmpid + "]";
	}

}
