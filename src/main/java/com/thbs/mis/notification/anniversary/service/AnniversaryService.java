/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrCommonService.java                             */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contain all the methods related to     */
/*                  the GsrCommonService related calls			     */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.notification.anniversary.service;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.dao.EmployeeRepository;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notification.AnniversaryEmpNameComparator;
import com.thbs.mis.notification.anniversary.bean.AnniversaryBean;
import com.thbs.mis.notification.anniversary.bean.AnniversaryMessageBean;
import com.thbs.mis.notification.anniversary.bean.SendAnniversaryReplyBean;
import com.thbs.mis.notification.anniversary.bean.SendAnniversaryWishBean;
import com.thbs.mis.notification.anniversary.bo.DatEmpAnniversaryMessageBO;
import com.thbs.mis.notification.anniversary.dao.AnniversaryRepository;

@Service
public class AnniversaryService
{

	@Autowired
	private AnniversaryRepository anniversaryRepos;
	@Autowired
	private EmployeeRepository employeeRepos;
	
	@Value("${employee.photo.url}")
	private String empPhotoFilePathUrl;

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory.getLog(AnniversaryService.class);

	public AnniversaryService()
	{
		super();
	}
	
	/*
	*<Description checkFileIsExistOrNot:> Check the employee photo is available at provided path.
	*  If it is not there then it will check the gender of an employee and show the general male or female image on their profile image  
	* @param String photoUrl, Byte gender
	* @return String
	* @throws 
	*/
	public String checkFileIsExistOrNot(String photoUrl, Byte gender)
	{
		String empPhotoPath="";
		File file = new File(photoUrl);
		if(file.exists() && !file.isDirectory())
		{
			empPhotoPath = photoUrl;
		}else{
			switch (gender) {
				case 1:
					empPhotoPath = empPhotoFilePathUrl + "\\MALE.png";
					break;
				case 2:
					empPhotoPath = empPhotoFilePathUrl + "\\FEMALE.png";
					break;
				default:
					empPhotoPath = null;
					LOG.error("Failed to get employee's gender.");
					break;
				}
		}
		return empPhotoPath; 
	}
	
	
	/**
	 * <Description getTodayAnniversaryList:> Fetch all today's anniversary employee details
	 * @param
	 * @return List<BirthdayBean>
	 * @throws DataAccessException
	 */
	public List<AnniversaryBean> getTodayAnniversaryList() throws DataAccessException 
	{
		List<AnniversaryBean> todayAnniversaryBeanList = new ArrayList<AnniversaryBean>();
		List<DatEmpPersonalDetailBO> datEmpPersonalDetailList = new ArrayList<DatEmpPersonalDetailBO>();
		List<DatEmpPersonalDetailBO> excludeEmpAnniversaryList = new ArrayList<DatEmpPersonalDetailBO>();
		try {
			datEmpPersonalDetailList = employeeRepos.getAllTodayAnniversaryList();
			//Check the employee birthday notification flag is enabled or not
			excludeEmpAnniversaryList = employeeRepos.getAllExcludeEmpAnniversaryList();
		} 
		catch (Exception e) 
		{
			throw new DataAccessException("Failed to retrieve list of employees whose birthday's date is today : ", e);
		}
		
		if(!datEmpPersonalDetailList.isEmpty())
		{
			for (DatEmpPersonalDetailBO empPersonalObject : datEmpPersonalDetailList)
			{
				if(!excludeEmpAnniversaryList.isEmpty())
				{
					for (DatEmpPersonalDetailBO excludeEmpAnniversaryObj : excludeEmpAnniversaryList) 
					{
						if(excludeEmpAnniversaryObj.getFkEmpDetailId() != empPersonalObject.getFkEmpDetailId())
						{
							String photoUrlString;
							AnniversaryBean anniversaryBeanObject = new AnniversaryBean();
							anniversaryBeanObject.setEmpId(empPersonalObject.getFkEmpDetailId());
							anniversaryBeanObject.setEmpFirstName(empPersonalObject.getEmpFirstName());
							anniversaryBeanObject.setEmpLastName(empPersonalObject.getEmpLastName());
							anniversaryBeanObject.setEmpGender(empPersonalObject.getEmpGender());
							anniversaryBeanObject.setEmpDOJ(empPersonalObject.getEmpDateOfJoining());
							anniversaryBeanObject.setEmpFullNameWithId(empPersonalObject.getEmpFirstName() + " "
									+ empPersonalObject.getEmpLastName() + "-" 
									+ empPersonalObject.getFkEmpDetailId());
							//Check whether photo is available or not
							photoUrlString = checkFileIsExistOrNot(empPhotoFilePathUrl + "\\" +empPersonalObject.getFkEmpDetailId() + ".png",  
									empPersonalObject.getEmpGender());
							anniversaryBeanObject.setEmpPhotoURL(photoUrlString);
							todayAnniversaryBeanList.add(anniversaryBeanObject);
						}
					}
				}else{
					String photoUrlString;
					AnniversaryBean anniversaryBeanObject = new AnniversaryBean();
					anniversaryBeanObject.setEmpId(empPersonalObject.getFkEmpDetailId());
					anniversaryBeanObject.setEmpFirstName(empPersonalObject.getEmpFirstName());
					anniversaryBeanObject.setEmpLastName(empPersonalObject.getEmpLastName());
					anniversaryBeanObject.setEmpGender(empPersonalObject.getEmpGender());
					anniversaryBeanObject.setEmpDOJ(empPersonalObject.getEmpDateOfJoining());
					anniversaryBeanObject.setEmpFullNameWithId(empPersonalObject.getEmpFirstName() + " "
							+ empPersonalObject.getEmpLastName() + "-" 
							+ empPersonalObject.getFkEmpDetailId());
					//Check whether photo is available or not
					photoUrlString = checkFileIsExistOrNot(empPhotoFilePathUrl + "\\" +empPersonalObject.getFkEmpDetailId() + ".png",  
							empPersonalObject.getEmpGender());
					anniversaryBeanObject.setEmpPhotoURL(photoUrlString);
					todayAnniversaryBeanList.add(anniversaryBeanObject);
				}
			}
			//Sort by employee name
			Collections.sort(todayAnniversaryBeanList, new AnniversaryEmpNameComparator());
		}
		return todayAnniversaryBeanList;
	}


	
	/**
	 * <Description getUpcomingAnniversaryEmpList:> Fetch upcoming anniversary details
	 * @param
	 * @return List<BirthdayBean>
	 * @throws DataAccessException
	 */
	public List<AnniversaryBean> getUpcomingAnniversaryEmpList() throws DataAccessException 
	{
		List<AnniversaryBean> upcomingAnniversaryBeanList = new ArrayList<AnniversaryBean>();
		List<AnniversaryBean> finalUpcomingAnniversaryBeanList = new ArrayList<AnniversaryBean>();
		List<DatEmpPersonalDetailBO> datEmpPersonalDetailList = new ArrayList<DatEmpPersonalDetailBO>();
		List<DatEmpPersonalDetailBO> excludeEmpAnniversaryList = new ArrayList<DatEmpPersonalDetailBO>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM");
		Date todayDate = new Date();
		
		Calendar c = Calendar.getInstance(); 
		c.setTime(todayDate);
		c.add(Calendar.DATE,1);	
		Date firstDate = c.getTime();
		
		c.setTime(todayDate);
		c.add(Calendar.DATE,7);	
		Date lastDate = c.getTime();
		try 
		{
			datEmpPersonalDetailList = employeeRepos.getUpcomingAnniversaryList(firstDate,lastDate);
			//Check the employee Anniversary notification flag is enabled or not
			excludeEmpAnniversaryList = employeeRepos.getAllExcludeEmpAnniversaryList();
		} 
		catch (Exception e) 
		{
			throw new DataAccessException("Failed to retrieve list of employees whose Anniversary's date are upcoming in 7 days : ", e);
		}
		
		if(!datEmpPersonalDetailList.isEmpty())
		{
			for (DatEmpPersonalDetailBO datEmpPersonalDetailBO : datEmpPersonalDetailList)
			{
				if(!excludeEmpAnniversaryList.isEmpty())
				{
					for (DatEmpPersonalDetailBO excludeEmpAnniversaryObj : excludeEmpAnniversaryList) 
					{
						if(excludeEmpAnniversaryObj.getFkEmpDetailId() != datEmpPersonalDetailBO.getFkEmpDetailId())
						{
							String photoUrlString;
							AnniversaryBean anniversaryBeanObject = new AnniversaryBean();
							anniversaryBeanObject.setEmpId(datEmpPersonalDetailBO.getFkEmpDetailId());
							anniversaryBeanObject.setEmpFirstName(datEmpPersonalDetailBO.getEmpFirstName());
							anniversaryBeanObject.setEmpLastName(datEmpPersonalDetailBO.getEmpLastName());
							anniversaryBeanObject.setEmpGender(datEmpPersonalDetailBO.getEmpGender());
							anniversaryBeanObject.setEmpDOJ(datEmpPersonalDetailBO.getEmpDateOfJoining());
							anniversaryBeanObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
									+ datEmpPersonalDetailBO.getEmpLastName() + "-" 
									+ datEmpPersonalDetailBO.getFkEmpDetailId());
							//Check whether photo is available or not
							photoUrlString = checkFileIsExistOrNot(empPhotoFilePathUrl + "\\" +datEmpPersonalDetailBO.getFkEmpDetailId() + ".png",  
									datEmpPersonalDetailBO.getEmpGender());
							anniversaryBeanObject.setEmpPhotoURL(photoUrlString);
							upcomingAnniversaryBeanList.add(anniversaryBeanObject);
						}
					}
				}else{
					String photoUrlString;
					AnniversaryBean anniversaryBeanObject = new AnniversaryBean();
					anniversaryBeanObject.setEmpId(datEmpPersonalDetailBO.getFkEmpDetailId());
					anniversaryBeanObject.setEmpFirstName(datEmpPersonalDetailBO.getEmpFirstName());
					anniversaryBeanObject.setEmpLastName(datEmpPersonalDetailBO.getEmpLastName());
					anniversaryBeanObject.setEmpGender(datEmpPersonalDetailBO.getEmpGender());
					anniversaryBeanObject.setEmpDOJ(datEmpPersonalDetailBO.getEmpDateOfJoining());
					anniversaryBeanObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
							+ datEmpPersonalDetailBO.getEmpLastName() + "-" 
							+ datEmpPersonalDetailBO.getFkEmpDetailId());
					//Check whether photo is available or not
					photoUrlString = checkFileIsExistOrNot(empPhotoFilePathUrl + "\\" +datEmpPersonalDetailBO.getFkEmpDetailId() + ".png",  
							datEmpPersonalDetailBO.getEmpGender());
					anniversaryBeanObject.setEmpPhotoURL(photoUrlString);
					upcomingAnniversaryBeanList.add(anniversaryBeanObject);
				}
			}	
		}
		
		if(!upcomingAnniversaryBeanList.isEmpty())
		{
			for (int i = 1; i < 8; i++) 
			{
				List<AnniversaryBean> anniversaryBeanForADayList = new ArrayList<AnniversaryBean>();
				Calendar cal = Calendar.getInstance(); 
				cal.setTime(todayDate);
				cal.add(Calendar.DATE, i);	
				Date dateCal = cal.getTime();
				
				String stringFormatDate = sdf.format(dateCal);
				Date dateFormatDate = new Date();
				try 
				{
					dateFormatDate = sdf.parse(stringFormatDate);
				} catch (ParseException e) 
				{
					LOG.error("Failed to convert from string to date " + e.getMessage());
					e.printStackTrace();
				}
				
				for (AnniversaryBean anniversaryBeanObj : upcomingAnniversaryBeanList) 
				{
					String stringFormatDOJ = sdf.format(anniversaryBeanObj.getEmpDOJ());
					Date dateFormatDOJ = new Date();
					try {
						dateFormatDOJ = sdf.parse(stringFormatDOJ);
					} catch (ParseException e) 
					{
						LOG.error("Failed to convert from string to date " + e.getMessage());
						e.printStackTrace();
					}
					
					if(dateFormatDate.equals(dateFormatDOJ))
					{
						anniversaryBeanForADayList.add(anniversaryBeanObj);
					}
				}
				
				Collections.sort(anniversaryBeanForADayList, new AnniversaryEmpNameComparator());
				finalUpcomingAnniversaryBeanList.addAll(anniversaryBeanForADayList);
			}
		}
		return finalUpcomingAnniversaryBeanList;
	}

	
	
	/**
	 * <Description getRecentBirthdayEmpList:> Fetch recent birthday details
	 * @param
	 * @return List<BirthdayBean>
	 * @throws DataAccessException
	 */
	public List<AnniversaryBean> getRecentAnniversaryEmpList() throws DataAccessException 
	{
		List<AnniversaryBean> recentAnniversaryBeanList = new ArrayList<AnniversaryBean>();
		List<AnniversaryBean> empAnniversaryBeanList = new ArrayList<AnniversaryBean>();
		List<DatEmpPersonalDetailBO> excludeEmpAnniversaryList = new ArrayList<DatEmpPersonalDetailBO>();
		List<DatEmpPersonalDetailBO> datEmpPersonalDetailList = new ArrayList<DatEmpPersonalDetailBO>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM");
		Date todayDate = new Date();
		
		Calendar c = Calendar.getInstance(); 
		c.setTime(todayDate);
		c.add(Calendar.DATE,-1);	
		Date firstDate = c.getTime();
		
		c.setTime(todayDate);
		c.add(Calendar.DATE,-7);	
		Date lastDate = c.getTime();
		try {
			datEmpPersonalDetailList = employeeRepos.getRecentAnniversaryList(lastDate,firstDate);
			//Check the employee birthday notification flag is enabled or not
			excludeEmpAnniversaryList = employeeRepos.getAllExcludeEmpAnniversaryList();
		} 
		catch (Exception e) 
		{
			throw new DataAccessException("Failed to retrieve list of employees whose birthday's date falls on recent 7 days : ", e);
		}
		
		if(!datEmpPersonalDetailList.isEmpty())
		{
			for (DatEmpPersonalDetailBO datEmpPersonalDetailBO : datEmpPersonalDetailList)
			{
				if(!excludeEmpAnniversaryList.isEmpty())
				{
					for (DatEmpPersonalDetailBO excludeEmpAnniversaryObj : excludeEmpAnniversaryList) 
					{
						if(excludeEmpAnniversaryObj.getFkEmpDetailId() != datEmpPersonalDetailBO.getFkEmpDetailId())
						{
							String photoUrlString;
							AnniversaryBean anniversaryBeanObject = new AnniversaryBean();
							anniversaryBeanObject.setEmpId(datEmpPersonalDetailBO.getFkEmpDetailId());
							anniversaryBeanObject.setEmpFirstName(datEmpPersonalDetailBO.getEmpFirstName());
							anniversaryBeanObject.setEmpLastName(datEmpPersonalDetailBO.getEmpLastName());
							anniversaryBeanObject.setEmpGender(datEmpPersonalDetailBO.getEmpGender());
							anniversaryBeanObject.setEmpDOJ(datEmpPersonalDetailBO.getEmpDateOfJoining());
							anniversaryBeanObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
									+ datEmpPersonalDetailBO.getEmpLastName() + "-" 
									+ datEmpPersonalDetailBO.getFkEmpDetailId());
							//Check whether photo is available or not
							photoUrlString = checkFileIsExistOrNot(empPhotoFilePathUrl + "\\" +datEmpPersonalDetailBO.getFkEmpDetailId() + ".png",  
									datEmpPersonalDetailBO.getEmpGender());
							anniversaryBeanObject.setEmpPhotoURL(photoUrlString);
							recentAnniversaryBeanList.add(anniversaryBeanObject);
						}
					}
				}else{
					String photoUrlString;
					AnniversaryBean anniversaryBeanObject = new AnniversaryBean();
					anniversaryBeanObject.setEmpId(datEmpPersonalDetailBO.getFkEmpDetailId());
					anniversaryBeanObject.setEmpFirstName(datEmpPersonalDetailBO.getEmpFirstName());
					anniversaryBeanObject.setEmpLastName(datEmpPersonalDetailBO.getEmpLastName());
					anniversaryBeanObject.setEmpGender(datEmpPersonalDetailBO.getEmpGender());
					anniversaryBeanObject.setEmpDOJ(datEmpPersonalDetailBO.getEmpDateOfJoining());
					anniversaryBeanObject.setEmpFullNameWithId(datEmpPersonalDetailBO.getEmpFirstName() + " "
							+ datEmpPersonalDetailBO.getEmpLastName() + "-" 
							+ datEmpPersonalDetailBO.getFkEmpDetailId());
					//Check whether photo is available or not
					photoUrlString = checkFileIsExistOrNot(empPhotoFilePathUrl + "\\" +datEmpPersonalDetailBO.getFkEmpDetailId() + ".png",  
							datEmpPersonalDetailBO.getEmpGender());
					anniversaryBeanObject.setEmpPhotoURL(photoUrlString);
					recentAnniversaryBeanList.add(anniversaryBeanObject);
				}
			}	
		}
		
		//Need to sort according to date descending order
		if(!recentAnniversaryBeanList.isEmpty())
		{
			for(int i = -1; i > -8; i--)
			{
				List<AnniversaryBean> anniversaryBeanForADayList = new ArrayList<AnniversaryBean>();
				Calendar cal = Calendar.getInstance(); 
				cal.setTime(todayDate);
				cal.add(Calendar.DATE, i);	
				Date dateCal = cal.getTime();
				
				String stringFormatDate = sdf.format(dateCal);
				Date dateFormatDate = new Date();
				try 
				{
					dateFormatDate = sdf.parse(stringFormatDate);
				} catch (ParseException e) 
				{
					LOG.error("Failed to convert from string to date " + e.getMessage());
					e.printStackTrace();
				}
				
				for (AnniversaryBean anniversaryBeanObj : recentAnniversaryBeanList) 
				{
					String stringFormatDOB = sdf.format(anniversaryBeanObj.getEmpDOJ());
					Date dateFormatDOB = new Date();
					try {
						dateFormatDOB = sdf.parse(stringFormatDOB);
					} catch (ParseException e) 
					{
						LOG.error("Failed to convert from string to date " + e.getMessage());
						e.printStackTrace();
					}
					
					if(dateFormatDate.equals(dateFormatDOB))
					{
						anniversaryBeanForADayList.add(anniversaryBeanObj);
					}
				}
				
				Collections.sort(anniversaryBeanForADayList, new AnniversaryEmpNameComparator());
				empAnniversaryBeanList.addAll(anniversaryBeanForADayList);
			}
		}		 
		return empAnniversaryBeanList;
	}

	
	
	/**
	 * <Description sendAnniversaryWishes:> Send anniversary wishes details
	 * @param SendAnniversaryWishBean anniversaryWishBean
	 * @return AnniversaryMessageBean
	 * @throws DataAccessException
	 */
	public AnniversaryMessageBean sendAnniversaryWishes(SendAnniversaryWishBean anniversaryWishBean)
			throws DataAccessException 
	{
		DatEmpAnniversaryMessageBO datEmpAnniversaryMessageObject = new DatEmpAnniversaryMessageBO();
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");
		Date todayDate = new Date();
		String todayDateString = sdf.format(todayDate);
		String messageIdString = anniversaryWishBean.getFromEmpId() + "_"
							+ anniversaryWishBean.getToEmpId() + "_"
							+ todayDateString;
		
		//Setting values into BO
		DatEmpAnniversaryMessageBO datEmpAnniversaryMessageBOObject = new DatEmpAnniversaryMessageBO();
		datEmpAnniversaryMessageBOObject.setFkAnniversaryMessageFromEmpId(anniversaryWishBean.getFromEmpId());
		datEmpAnniversaryMessageBOObject.setFkAnniversaryMessageToEmpId(anniversaryWishBean.getToEmpId());
		datEmpAnniversaryMessageBOObject.setAnniversaryMessage(anniversaryWishBean.getWishMessage());
		datEmpAnniversaryMessageBOObject.setAnniversaryMessageCreatedDate(new Date());
		datEmpAnniversaryMessageBOObject.setAnniversaryMessageId(messageIdString);
		
		datEmpAnniversaryMessageObject = anniversaryRepos.save(datEmpAnniversaryMessageBOObject);
		
		//Setting values on Bean from BO
		AnniversaryMessageBean anniversaryMessageBeanObject = new AnniversaryMessageBean(); 
		anniversaryMessageBeanObject.setPkAnniversaryMessageId(datEmpAnniversaryMessageObject.getPkAnniversaryMessageId());
		anniversaryMessageBeanObject.setFkFromEmpid(datEmpAnniversaryMessageObject.getFkAnniversaryMessageFromEmpId());
		anniversaryMessageBeanObject.setFkToEmpid(datEmpAnniversaryMessageObject.getFkAnniversaryMessageToEmpId());
		anniversaryMessageBeanObject.setAnniversaryMessage(datEmpAnniversaryMessageObject.getAnniversaryMessage());
		anniversaryMessageBeanObject.setAnniversaryMessageId(datEmpAnniversaryMessageObject.getAnniversaryMessageId());
		anniversaryMessageBeanObject.setAnniversaryMessageCreatedDate(datEmpAnniversaryMessageObject.getAnniversaryMessageCreatedDate());
		
		return anniversaryMessageBeanObject;
	}
	
	
	/**
	 * <Description sendAnniversaryReply:> Send anniversary reply details
	 * @param SendAnniversaryReplyBean anniversaryReplyBean
	 * @return AnniversaryMessageBean
	 * @throws DataAccessException
	 */
	public AnniversaryMessageBean sendAnniversaryReply(SendAnniversaryReplyBean anniversaryReplyBean)
			throws DataAccessException 
	{
		DatEmpAnniversaryMessageBO datEmpAnniversaryMessageObject = new DatEmpAnniversaryMessageBO();
				
		//Setting values on BO
		DatEmpAnniversaryMessageBO datEmpAnniversaryMessageBOObj = new DatEmpAnniversaryMessageBO();
		datEmpAnniversaryMessageBOObj.setFkAnniversaryMessageFromEmpId(anniversaryReplyBean.getFromEmpId());
		datEmpAnniversaryMessageBOObj.setFkAnniversaryMessageToEmpId(anniversaryReplyBean.getToEmpId());
		datEmpAnniversaryMessageBOObj.setAnniversaryMessage(anniversaryReplyBean.getReplyMessage());
		datEmpAnniversaryMessageBOObj.setAnniversaryMessageId(anniversaryReplyBean.getReplyMessageId());
		datEmpAnniversaryMessageBOObj.setAnniversaryMessageCreatedDate(new Date());
		
		datEmpAnniversaryMessageObject = anniversaryRepos.save(datEmpAnniversaryMessageBOObj);
		
		//Setting values on Bean from BO
		AnniversaryMessageBean anniversaryMessageBeanObject = new AnniversaryMessageBean(); 
		anniversaryMessageBeanObject.setPkAnniversaryMessageId(datEmpAnniversaryMessageObject.getPkAnniversaryMessageId());
		anniversaryMessageBeanObject.setFkFromEmpid(datEmpAnniversaryMessageObject.getFkAnniversaryMessageFromEmpId());
		anniversaryMessageBeanObject.setFkToEmpid(datEmpAnniversaryMessageObject.getFkAnniversaryMessageToEmpId());
		anniversaryMessageBeanObject.setAnniversaryMessage(datEmpAnniversaryMessageObject.getAnniversaryMessage());
		anniversaryMessageBeanObject.setAnniversaryMessageId(datEmpAnniversaryMessageObject.getAnniversaryMessageId());
		anniversaryMessageBeanObject.setAnniversaryMessageCreatedDate(datEmpAnniversaryMessageObject.getAnniversaryMessageCreatedDate());
		
		return anniversaryMessageBeanObject;
	}
	
	
}

