/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRWorkflowRepository.java                        */
/*                                                                   */
/*  Author      :  THBS                                              */
/*                                                                   */
/*  Date        :  October 28, 2016                                  */
/*                                                                   */
/*  Description :   This class contains all the methods              */
/*                 for the GSRWorkflowRepository                     */			 									
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* October 28, 2016     THBS     1.0        Initial version created  */
/*********************************************************************/


package com.thbs.mis.notification.anniversary.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.notification.anniversary.bo.DatEmpAnniversaryMessageBO;

/**
 * 
 * @author THBS
 * @version:1.0
 * @Description: This is AnniversaryRepository.
 */
@Repository
public interface AnniversaryRepository extends GenericRepository<DatEmpAnniversaryMessageBO,Long>{
	
}
