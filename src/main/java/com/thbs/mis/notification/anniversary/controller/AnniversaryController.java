/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrCommonService.java                            */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the methods for the           */
/*                 GSRCommonController                               */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.notification.anniversary.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notification.anniversary.bean.AnniversaryBean;
import com.thbs.mis.notification.anniversary.bean.AnniversaryMessageBean;
import com.thbs.mis.notification.anniversary.bean.SendAnniversaryReplyBean;
import com.thbs.mis.notification.anniversary.bean.SendAnniversaryWishBean;
import com.thbs.mis.notification.anniversary.constants.AnniversaryURIConstants;
import com.thbs.mis.notification.anniversary.service.AnniversaryService;
import com.thbs.mis.notification.birthday.bean.BirthdayBean;
import com.thbs.mis.notification.birthday.bean.BirthdayMessageBean;

@Controller
public class AnniversaryController
{
	/**
	 * Instance of <code>Log</code>
	 */
	@SuppressWarnings("unused")
	private static final AppLog LOG = LogFactory.getLog(AnniversaryController.class);
	
	@Autowired
	private AnniversaryService anniversaryService;
	
	
	/**@category This service is used for UI page to show anniversary details.
	 * <Description getTodayBirthdayList : Fetch employee list whose anniversary is today >
	 * @param
	 * @return List<BirthdayBean>
	 * @throws BusinessException
	 * @throws DataAccessException 
	 */
	@ApiOperation(tags="Anniversary",value = "Get all active employee details whose anniversary's date is today. "
			+ "This service will be called from the front-end when user want to show/view list of active employees, whose anniversary's date is today" + " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch all active employees whose anniversary's date is today."
			+ "<br>The Response will be the detailed of the active employee whose anniversary's date is today.",

	nickname = "All active employees whose anniversary's date is today",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = BirthdayBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BirthdayBean.class),
			@ApiResponse(code = 201, message = "All active employees (whose anniversary's date is today), are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All active employees detail whose anniversary's date is today", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All active employees detail whose anniversary's date is today", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = AnniversaryURIConstants.GET_TODAY_ANNIVERSARY_LIST , method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getTodayAnniversaryList()
			throws BusinessException, DataAccessException
	{
		List<AnniversaryBean> anniversaryList =  null;
		try
		{
			anniversaryList = anniversaryService.getTodayAnniversaryList();
		} 
		catch (DataAccessException e)
		{
			throw new BusinessException("Failed to retrieve all active employees whose anniversary's date is today ", e);
		}
		
		if(!anniversaryList.isEmpty())
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"Today's anniversary list have been retrieved successfully", 
							anniversaryList));
		}else{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"No anniversary found on today", anniversaryList));
		}
	}
	
	
	
	
	/**@category This service is used for UI page to show upcoming anniversary details.
	 * <Description getUpcomingAnniversaryList : Fetch all active employees list whose anniversary's date are falls on coming 7 days (start from today). >
	 * @param
	 * @return List<BirthdayBean>
	 * @throws BusinessException
	 * @throws DataAccessException 
	 */
	@ApiOperation(tags="Anniversary",value = "Get all active employee details whose anniversary's date will fall on coming 7 days. "
			+ "This service will be called from the front-end when user want to show/view list of active employees(whose anniversary's date are falls on coming 7 days )" + " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch all active employees whose anniversary's date are falls on coming 7 days."
			+ "<br>The Response will be the detailed of the active employee whose anniversary's date are falls on coming 7 days.",

	nickname = "All active employees whose anniversary's date are falls on coming 7 days",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = AnniversaryBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = AnniversaryBean.class),
			@ApiResponse(code = 201, message = "All active employees (whose anniversary's date are falls on coming 7 days), are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All active employees detail whose anniversary's date falls on coming 7 days", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All employees detail whose anniversary's date falls on coming 7 days.", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = AnniversaryURIConstants.GET_UPCOMING_ANNIVERSARY_LIST, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getUpcomingAnniversaryList()
			throws BusinessException, DataAccessException
	{
		List<AnniversaryBean> anniversaryList =  null;
		try
		{
			anniversaryList = anniversaryService.getUpcomingAnniversaryEmpList();
		} 
		catch (DataAccessException e)
		{
			throw new BusinessException("Failed to retrieve all upcoming 7 days birthday list", e);
		}
		
		if(!anniversaryList.isEmpty())
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"Upcoming anniversary list have been retrieved successfully", anniversaryList));
		}else{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"No upcoming anniversary found", anniversaryList));
		}
	}
	
	
	
	/**@category This service is used for UI page to show recent anniversary details.
	 * <Description getRecentAnniversaryList : Fetch all active employees list whose anniversary's date falls on recent 7 days. >
	 * @param
	 * @return List<BirthdayBean>
	 * @throws BusinessException
	 * @throws DataAccessException 
	 */
	@ApiOperation(tags="Anniversary",value = "Get all active employee details whose anniversary's date are fall on recent 7 days. "
			+ "This service will be called from the front-end when user want to show/view list of active employees(whose anniversary's date are falls on recent 7 days) in UI" + " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch all active employees whose anniversary's date are falls on recent 7 days."
			+ "<br>The Response will be the detailed of the active employee whose anniversary's date are falls on recent 7 days.",

	nickname = "All active employees whose anniversary's date are falls on recent 7 days",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = BirthdayBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BirthdayBean.class),
			@ApiResponse(code = 201, message = "All active employees (whose anniversary's date are falls on recent 7 days), are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All active employees detail whose anniversary's date falls on recent 7 days", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All active employees detail whose anniversary's date falls on recent 7 days.", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = AnniversaryURIConstants.GET_RECENT_ANNIVERSARY_LIST, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getRecentAnniversaryList()
			throws BusinessException, DataAccessException
	{
		List<AnniversaryBean> anniversaryList =  null;
		try
		{
			anniversaryList = anniversaryService.getRecentAnniversaryEmpList();
		} 
		catch (DataAccessException e)
		{
			throw new BusinessException("Failed to retrieve all recent 7 days anniversary list", e);
		}
		
		if(!anniversaryList.isEmpty())
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"Recent anniversary list have been retrieved successfully", anniversaryList));
		}else{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"No recent anniversary found", anniversaryList));
		}
	}
	
	
	/**@category This service is used for UI page to send anniversary wish message.
	 * <Description sendAnniversaryWishMessage : To send the anniversary wishes services. >
	 * @param SendAnniversaryWishBean
	 * @return ResponseEntity<MISResponse>
	 * @throws BusinessException
	 * @throws DataAccessException 
	*/
	@ApiOperation(tags="Anniversary",value = "This service will be called from the front-end when user send anniversary wishes" + " .", httpMethod = "GET",

	notes = "This Service has been implemented to send anniversary wishes."
			+ "<br>The Response will be show the success or failure message with status code.",

	nickname = "Store anniversary wish message into database",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = AnniversaryMessageBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = AnniversaryMessageBean.class),
			@ApiResponse(code = 201, message = "Anniversary Wish Message are stored successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Anniversary wish message are stored successfully into database.", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Anniversary wish message are stored successfully into database.", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = AnniversaryURIConstants.SEND_ANNIVERSARY_WISH_MESSAGE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> sendAnniversaryWishMessage(
			@ApiParam(name = "SendAnniversaryWishBean", example = "SendAnniversaryWishBean", value = "Anniversary Wish Bean have datas which are required to store anniversary wish message into database.")
			@Valid @RequestBody SendAnniversaryWishBean inputBean)
			throws BusinessException, DataAccessException
	{
		AnniversaryMessageBean anniversaryMassageBean =  null;
		try
		{
			anniversaryMassageBean = anniversaryService.sendAnniversaryWishes(inputBean);
		} 
		catch (DataAccessException e)
		{
			throw new BusinessException("Failed while adding birthday wish message into database.", e);
		}
		
		if(null != anniversaryMassageBean)
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"Anniversary wish message have been inserted successfully"));
		}else{
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, 
							"Error occurred while storing anniversary wish message."));
		}
	}
	
	
	
	/**@category This service is used for UI page to send birthday reply message.
	 * <Description sendBirthdayReplyMessage : To store the birthday reply message services. >
	 * @param SendBirthdayReplyBean inputBean
	 * @return ResponseEntity<MISResponse>
	 * @throws BusinessException
	 * @throws DataAccessException 
	 */
	@ApiOperation(tags="Anniversary",value = "This service will be called from the front-end when user reply to birthday wishes" + " .", httpMethod = "GET",

	notes = "This Service has been implemented to send birthday reply message."
			+ "<br>The Response will be show the success or failure message with status code.",

	nickname = "Store birthday reply message into database",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = BirthdayMessageBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = BirthdayMessageBean.class),
			@ApiResponse(code = 201, message = "Birthday Reply Message are stored successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Birthday reply message are stored successfully into database", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Birthday reply message are stored successfully into database", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = AnniversaryURIConstants.SEND_ANNIVERSARY_REPLY_MESSAGE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> sendAnniversaryReplyMessage(
			@ApiParam(name = "SendAnniversaryReplyBean", example = "SendAnniversaryReplyBean", value = "Anniversary Reply Bean have datas which are required to store birthday reply message into database.")
				@Valid @RequestBody SendAnniversaryReplyBean inputBean)
			throws BusinessException, DataAccessException
	{
		AnniversaryMessageBean anniversaryMassageBean =  null;
		try
		{
			anniversaryMassageBean = anniversaryService.sendAnniversaryReply(inputBean);
		} 
		catch (DataAccessException e)
		{
			throw new BusinessException("Failed while adding anniversary reply message into database.", e);
		}
		
		if(null != anniversaryMassageBean)
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"Anniversary reply message have been inserted successfully"));
		}else{
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE, 
							"Error occurred while storing anniversary reply message."));
		}
	}
	
	

	
}
