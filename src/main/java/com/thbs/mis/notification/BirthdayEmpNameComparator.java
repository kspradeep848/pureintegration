/**
 * 
 */
package com.thbs.mis.notification;

import java.util.Comparator;

import com.thbs.mis.notification.birthday.bean.BirthdayBean;


public class BirthdayEmpNameComparator implements Comparator<BirthdayBean>{

	public int compare(BirthdayBean object1,BirthdayBean object2)
	{
		return object1.getEmpFullNameWithId().compareTo(object2.getEmpFullNameWithId());
	}
}
