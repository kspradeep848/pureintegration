package com.thbs.mis.notification.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the dat_emp_exclude_birthday_anniversary database table.
 * 
 */
@Entity
@Table(name="dat_emp_exclude_birthday_anniversary")
@NamedQuery(name="DatEmpExcludeBirthdayAnniversaryBO.findAll", query="SELECT d FROM DatEmpExcludeBirthdayAnniversaryBO d")
public class DatEmpExcludeBirthdayAnniversaryBO implements Serializable 
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="pk_exclude_birthday_anniversary_id")
	private int pkExcludeBirthdayAnniversaryId;
	
	@Column(name="fk_exclude_birthday_anniversary_emp_id")
	private int fkExcludeBirthdayAnniversaryEmpId;

	@Column(name="exclude_anniversary_flag")
	private String excludeAnniversaryFlag;

	@Column(name="exclude_birthday_flag")
	private String excludeBirthdayFlag;

	
	public int getPkExcludeBirthdayAnniversaryId() {
		return this.pkExcludeBirthdayAnniversaryId;
	}

	public void setPkExcludeBirthdayAnniversaryId(int pkExcludeBirthdayAnniversaryId) {
		this.pkExcludeBirthdayAnniversaryId = pkExcludeBirthdayAnniversaryId;
	}

	public String getExcludeAnniversaryFlag() {
		return this.excludeAnniversaryFlag;
	}

	public void setExcludeAnniversaryFlag(String excludeAnniversaryFlag) {
		this.excludeAnniversaryFlag = excludeAnniversaryFlag;
	}

	public String getExcludeBirthdayFlag() {
		return this.excludeBirthdayFlag;
	}

	public void setExcludeBirthdayFlag(String excludeBirthdayFlag) {
		this.excludeBirthdayFlag = excludeBirthdayFlag;
	}

	public int getFkExcludeBirthdayAnniversaryEmpId() {
		return fkExcludeBirthdayAnniversaryEmpId;
	}

	public void setFkExcludeBirthdayAnniversaryEmpId(
			int fkExcludeBirthdayAnniversaryEmpId) {
		this.fkExcludeBirthdayAnniversaryEmpId = fkExcludeBirthdayAnniversaryEmpId;
	}

	@Override
	public String toString() {
		return "DatEmpExcludeBirthdayAnniversary [pkExcludeBirthdayAnniversaryId="
				+ pkExcludeBirthdayAnniversaryId
				+ ", fkExcludeBirthdayAnniversaryEmpId="
				+ fkExcludeBirthdayAnniversaryEmpId
				+ ", excludeAnniversaryFlag="
				+ excludeAnniversaryFlag
				+ ", excludeBirthdayFlag=" + excludeBirthdayFlag + "]";
	}

}