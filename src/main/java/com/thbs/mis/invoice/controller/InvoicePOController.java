package com.thbs.mis.invoice.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.confirmation.controller.ConfirmationByManagerController;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.invoice.bean.CreateInvoicePOWithDocInputBean;
import com.thbs.mis.invoice.bean.CreateInvoicePOWithoutDocInputBean;
import com.thbs.mis.invoice.bean.FilterPurchaseOrderInputBean;
import com.thbs.mis.invoice.bean.FilterPurchaseOrderOutputBean;
import com.thbs.mis.invoice.bean.InvoicePODetailsBean;
import com.thbs.mis.invoice.bean.InvoiceShiftingPOInputBean;
import com.thbs.mis.invoice.bean.MapPoToWorkorderInputBean;
import com.thbs.mis.invoice.bean.MapPoToWorkorderOutputBean;
import com.thbs.mis.invoice.bean.SearchPOInputBean;
import com.thbs.mis.invoice.bean.SearchPOOutPutBean;
import com.thbs.mis.invoice.bean.UpdateInvoicePOWithDocInputBean;
import com.thbs.mis.invoice.bo.InvoiceDatPurchaseOrderBO;
import com.thbs.mis.invoice.constant.InvoicePurchaseOrderURIConstants;
import com.thbs.mis.invoice.service.InvoicePOService;

@Controller
public class InvoicePOController {
	
	private static final AppLog LOG = LogFactory
			.getLog(ConfirmationByManagerController.class);

	@Autowired
	private InvoicePOService invoicePOService;
	
	
	// Added by Shashi Kant singh
	
		/**
		 * @category This service is used for UI page to show the drop down list.
		 *           <Description getAllInvoicePurchaseOrders> : Fetch All Purchase
		 *           Order Details.
		 * @param
		 * @return List<SearchPOOutPutBean>
		 * @throws BusinessException
		 * @throws DataAccessException
		 */
		@ApiOperation(value = "Get all Purchase Order Details. This service will be called when user wants to view list of Purchase Order Details in UI"
				+ " .", httpMethod = "POST",

		notes = "This Service has been implemented to fetch all Purchase Order Details."
				+ "<br>The Response will be the list of all Purchase Order Details.",

		nickname = "Get all Purchase Order Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = SearchPOOutPutBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = SearchPOOutPutBean.class),
				@ApiResponse(code = 201, message = "All Purchase Order Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Purchase Order Details", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Purchase Order Details", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = InvoicePurchaseOrderURIConstants.VIEW_ALL_INVOICE_PO, method = RequestMethod.POST)
		public @ResponseBody ResponseEntity<MISResponse> getAllInvoicePurchaseOrders(@Valid @RequestBody SearchPOInputBean inputBean )
				throws DataAccessException,CommonCustomException {

			List<SearchPOOutPutBean> searchPOOutPutBeanList = null;

			try {
				searchPOOutPutBeanList = invoicePOService
						.getAllInvoicePurchaseOrders(inputBean);
			}
			 catch (CommonCustomException e) {
					throw new CommonCustomException(e.getMessage());
				}
			catch (Exception e) {
				throw new DataAccessException(
						"Failed to retrieve list of Invoice purchase orders :  ", e);
			}

			if (!searchPOOutPutBeanList.isEmpty()) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Invoice Purchase orders displayed successfully.",
								searchPOOutPutBeanList));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								MISConstants.FAILURE,
								"Failed to fetch the Invoice purchase orders.",
								searchPOOutPutBeanList));
			}
		}
		
		/**
		 * @category This service is used for UI page to show the view Invoice Purchase order details.
		 *           <Description getAllInvoicePurchaseOrders> : view Invoice Purchase
		 *           Order with .
		 * @param
		 * @return List<invoicePODetailsBean>
		 * @throws BusinessException
		 * @throws DataAccessException
		 */
		@ApiOperation(value = "Get all Purchase Order Details. This service will be called when user wants to view list of Purchase Order Details in UI"
				+ " .", httpMethod = "GET",

		notes = "This Service has been implemented to fetch all Purchase Order Details."
				+ "<br>The Response will be the list of all Purchase Order Details.",

		nickname = "Get all Purchase Order Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = SearchPOOutPutBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = SearchPOOutPutBean.class),
				@ApiResponse(code = 201, message = "All Purchase Order Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Purchase Order Details", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Purchase Order Details", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = InvoicePurchaseOrderURIConstants.VIEW_INVOICE_PO_DETAILS, method = RequestMethod.GET)
		public @ResponseBody ResponseEntity<MISResponse> viewInvoicePurchaseOrdersDetails(@PathVariable String InvoicePONumber ,@PathVariable int loggedInEmpId)
				throws DataAccessException,CommonCustomException {

			InvoicePODetailsBean invoicePODetailsBean = new InvoicePODetailsBean();
			List<InvoicePODetailsBean> invoicePODetailsBeanList = new ArrayList<InvoicePODetailsBean>();
  
			try {
				invoicePODetailsBean = invoicePOService
						.viewInvoicePODetails(InvoicePONumber,loggedInEmpId);
				invoicePODetailsBeanList.add(invoicePODetailsBean);
			}  catch (CommonCustomException e) {
				throw new CommonCustomException(e.getMessage());
			}
			catch (Exception e) {
				throw new DataAccessException(
						"Failed to retrieve list of invoice purchase orders details :  ", e);
			}

			if (!invoicePODetailsBeanList.isEmpty()) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Invoice purchase orders details displayed successfully.",
								invoicePODetailsBeanList));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								MISConstants.FAILURE,
								"Failed to fetch the Invoice purchase orders details.",
								invoicePODetailsBeanList));
			}
			
			
		}
		
		
		/**
		 * @category This service is used for UI page to create Invoice Purchase order with document.
		 *           <Description getAllInvoicePurchaseOrders> : Create Invoice Purchase order
		 *            without Document.
		 * @param
		 * @return List<invoicePODetailsBean>
		 * @throws BusinessException
		 * @throws DataAccessException
		 */
		@ApiOperation(value = "Create Invoice Purchase Order without Document. This service will be called when user wants to Create Invoice Purchase Order without Document from UI"
				+ " .", httpMethod = "POST",

		notes = "This Service has been implemented to Create Invoice Purchase Order without Document.",

		nickname = "Create Invoice Purchase Order without Document", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = SearchPOOutPutBean.class),
				@ApiResponse(code = 201, message = "Invoice Purchase Order without Document is created successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Invoice Purchase Order without Document", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Invoice Purchase Order without Document", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = InvoicePurchaseOrderURIConstants.CREATE_INVOICE_PO_WITHOUT_DOC, method = RequestMethod.POST)
		public @ResponseBody ResponseEntity<MISResponse> createInvoicePOWithoutDoc(@Valid @RequestBody CreateInvoicePOWithoutDocInputBean crtInvoicePOInputBean )
				throws DataAccessException,CommonCustomException {

			boolean success = false;
			try {
				success = invoicePOService
						.createInvoicePOWithoutDocument(crtInvoicePOInputBean);
			} catch (CommonCustomException e) {
				throw new CommonCustomException(e.getMessage());
			} 
			catch (Exception e) {
				throw new DataAccessException(
						"Failed to create invoice purchase order without Document:  ", e);
			}

			if (success) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Invoice purchase order without Document is created successfully."));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								MISConstants.FAILURE,
								"Failed to create invoice purchase order without Document."));
			}
		}
		
		/**
		 * @category This service is used for UI page to create Invoice Purchase order with document.
		 *           <Description getAllInvoicePurchaseOrders> : Create Invoice Purchase order
		 *            with Document.
		 * @param
		 * @return List<invoicePODetailsBean>
		 * @throws BusinessException
		 * @throws DataAccessException
		 */
		@ApiOperation(value = "Create Invoice Purchase Order with Document. This service will be called when user wants to Create Invoice Purchase Order with Document from UI"
				+ " .", httpMethod = "POST",

		notes = "This Service has been implemented to Create Invoice Purchase Order with Document.",

		nickname = "Create Invoice Purchase Order with Document", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = SearchPOOutPutBean.class),
				@ApiResponse(code = 201, message = "Invoice Purchase Order with Document is created successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Invoice Purchase Order with Document", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create Invoice Purchase Order with Document", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = InvoicePurchaseOrderURIConstants.CREATE_INVOICE_PO_WITH_DOC, method = RequestMethod.POST)
		public @ResponseBody ResponseEntity<MISResponse> createInvoicePOWithDoc(@Valid @RequestBody CreateInvoicePOWithDocInputBean crtInvoicePOInputBean )
				throws DataAccessException,CommonCustomException {

			boolean success = false;
			try {
				success = invoicePOService
						.createInvoicePOWithDocument(crtInvoicePOInputBean);
			} catch (CommonCustomException e) {
				throw new CommonCustomException(e.getMessage());
			}
			catch (Exception e) {
				throw new DataAccessException(
						"Failed to create invoice purchase order with Document:  ", e);
			}

			if (success) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Invoice purchase order with Document is created successfully."));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								MISConstants.FAILURE,
								"Failed to create invoice purchase order with Document."));
			}
		}
		
		/**
		 * @category This service is used for UI page to Update Invoice Purchase order.
		 *           <Description updateInvoicePO> : Update Invoice Purchase order.
		 * @param
		 * @return boolean
		 * @throws BusinessException
		 * @throws DataAccessException
		 */
		@ApiOperation(value = "Update Invoice Purchase Order . This service will be called when user wants to Update Invoice Purchase Order from UI"
				+ " .", httpMethod = "PUT",

		notes = "This Service has been implemented to Update Invoice Purchase Order .",

		nickname = "Update Invoice Purchase Order ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = SearchPOOutPutBean.class),
				@ApiResponse(code = 201, message = "Invoice Purchase Order is Updated successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Update Invoice Purchase Order", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Update Invoice Purchase Order", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = InvoicePurchaseOrderURIConstants.UPDATE_INVOICE_PO, method = RequestMethod.PUT)
		public @ResponseBody ResponseEntity<MISResponse> updateInvoicePO(@Valid @RequestBody UpdateInvoicePOWithDocInputBean updInvoicePOInputBean )
				throws DataAccessException,CommonCustomException {

			boolean success = false;
			try {
				success = invoicePOService
						.updateInvoicePO(updInvoicePOInputBean);
			} catch (Exception e) {
				throw new DataAccessException(
						"Failed to update invoice purchase order :  ", e);
			}

			if (success) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Invoice purchase order is updated successfully."));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								MISConstants.FAILURE,
								"Failed to update invoice purchase order ."));
			}
		}
		
		/**
		 * @category This service is used for UI page to Update Invoice Purchase order.
		 *           <Description updateInvoicePO> : Archive Invoice Purchase order.
		 * @param
		 * @return boolean
		 * @throws BusinessException
		 * @throws DataAccessException
		 */
		@ApiOperation(value = "Archive Invoice Purchase Order . This service will be called when user wants to Archive Invoice Purchase Order from UI"
				+ " .", httpMethod = "GET",

		notes = "This Service has been implemented to Update Invoice Purchase Order .",

		nickname = "archive Invoice Purchase Order ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = URI.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = SearchPOOutPutBean.class),
				@ApiResponse(code = 201, message = "Invoice Purchase Order is Archived successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Archive Invoice Purchase Order", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Archive Invoice Purchase Order", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = InvoicePurchaseOrderURIConstants.ARCHIVED_INVOICE_PO, method = RequestMethod.GET)
		public @ResponseBody ResponseEntity<MISResponse> archiveInvoicePO(@PathVariable String InvoicePONumber,@PathVariable int loggedInEmpId )
				throws DataAccessException,CommonCustomException {

			boolean success = false;
			try {
				success = invoicePOService
						.archiveInvoicePO(InvoicePONumber,loggedInEmpId);
			} catch (Exception e) {
				throw new DataAccessException(
						"Failed to archive invoice purchase order :  ", e);
			}

			if (success) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Invoice Purchase order is archived successfully."));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								MISConstants.FAILURE,
								"Failed to archive invoice purchase order ."));
			}
		}
		// EOA by Shashi Kant singh	
		
		/*// Added by Balaji
		
		*//**
		 * @category This service is used for UI page to show the view Invoice Purchase order details.
		 *           <Description getAllInvoicePurchaseOrders> : view Invoice Purchase
		 *           Order with .
		 * @param
		 * @return List<invoicePODetailsBean>
		 * @throws BusinessException
		 * @throws DataAccessException
		 *//*
		@ApiOperation(value = "Get all Purchase Order Details. This service will be called when user wants to view list of Purchase Order Details in UI"
				+ " .", httpMethod = "POST",

		notes = "This Service has been implemented to fetch all Purchase Order Details."
				+ "<br>The Response will be the list of all Purchase Order Details.",

		nickname = "Get all Purchase Order Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = SearchPOOutPutBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = SearchPOOutPutBean.class),
				@ApiResponse(code = 201, message = "All Purchase Order Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Purchase Order Details", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Purchase Order Details", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = InvoicePurchaseOrderURIConstants.SEARCH_FILTERED_PURCHASE_ORDERS, method = RequestMethod.POST)
		public @ResponseBody ResponseEntity<MISResponse> searchFilteredPurchaseOrders(@Valid @RequestBody FilterPurchaseOrderInputBean filterPOInputBean)
				throws DataAccessException,CommonCustomException {
			
	 		LOG.startUsecase("Search Filtered PurchaseOrders");

	 		List<FilterPurchaseOrderOutputBean> filterPoOutputBeanList = new ArrayList<FilterPurchaseOrderOutputBean>();
	 		//FilterPurchaseOrderOutputBean filterPoOutputBean = new FilterPurchaseOrderOutputBean();
	 		
	 		filterPoOutputBeanList = invoicePOService.searchFilteredPurchaseOrders(filterPOInputBean);
			
	 		if(!filterPoOutputBeanList.isEmpty())
	 		{
	 			return ResponseEntity.status(HttpStatus.OK.value()).body(
	 				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
	 						"Details are displayed successfully.", filterPoOutputBeanList));
	 		}else{
	 			return ResponseEntity.status(HttpStatus.OK.value()).body(
	 					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
	 							"No details Found", filterPoOutputBeanList));
	 		}
			
		}
		*/
		
		/**
		 * @category This service is used for UI page to show the view Invoice Purchase order details.
		 *           <Description getAllInvoicePurchaseOrders> : view Invoice Purchase
		 *           Order with .
		 * @param
		 * @return List<invoicePODetailsBean>
		 * @throws BusinessException
		 * @throws DataAccessException
		 */
		@ApiOperation(value = "To shift Normal PO to Aggregate Purchase Order. This service will be called when user wants to change Normal PO into Aggregate PO in UI"
				+ " .", httpMethod = "POST",

		notes = "This Service has been implemented to to change Normal Purchase order into Aggregate Purchase order.",

		nickname = "Get all Purchase Order Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = SearchPOOutPutBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = SearchPOOutPutBean.class),
				@ApiResponse(code = 201, message = "All Purchase Order Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Purchase Order Details", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Purchase Order Details", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = InvoicePurchaseOrderURIConstants.SHIFTING_NORMAL_PO_TO_AGGREGATE, method = RequestMethod.POST)
		public @ResponseBody ResponseEntity<MISResponse> shiftingNormalPOtoAggregatePO(@Valid @RequestBody InvoiceShiftingPOInputBean invoiceShiftingPOInputBean)
				throws DataAccessException,CommonCustomException {
			
	 		LOG.startUsecase("Search Filtered PurchaseOrders");

	 		
	 		//InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrderBO = new InvoiceDatPurchaseOrderBO();
	 		
	 	/*	invoiceDatPurchaseOrderBO =*/ invoicePOService.shiftingNormalPOtoAggregatePO(invoiceShiftingPOInputBean); 
			
	 		/*if(invoiceDatPurchaseOrderBO != null)
	 		{*/
	 			return ResponseEntity.status(HttpStatus.OK.value()).body(
	 				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
	 						"Details are displayed successfully."));
	 		/*}else{
	 			return ResponseEntity.status(HttpStatus.OK.value()).body(
	 					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
	 							"No details Found"));
	 		}*/
			
		}
		
		//EOA by Balaji
		
		//Added by Smrithi
		
		/**
		 * @category This service is used map the purchase order to the workorder.
		 * @param
		 * @return MapPoToWorkorderOutputBean
		 * @throws BusinessException
		 * @throws DataAccessException
		 */
		@ApiOperation(value = "Map PO to WO. This service is used map the purchase order to the workorder."
				+ " .", httpMethod = "POST",

		notes = "This service is used to map the purchase order to the workorder.",

		nickname = "Map PO to WO", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MapPoToWorkorderOutputBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = SearchPOOutPutBean.class),
				@ApiResponse(code = 201, message = "All Purchase Order Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Map PO to WO", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Map PO to WO", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = InvoicePurchaseOrderURIConstants.MAP_TO_WORKORDER, method = RequestMethod.POST)
		public @ResponseBody ResponseEntity<MISResponse> mapPoToWo(@Valid @RequestBody MapPoToWorkorderInputBean mapPoToWoInputBean)
				throws DataAccessException,CommonCustomException {

			MapPoToWorkorderOutputBean mapPoToWoOutputBean = new MapPoToWorkorderOutputBean();
			List<MapPoToWorkorderOutputBean> mapPoToWoOutputBeanList = new ArrayList<MapPoToWorkorderOutputBean>();
  
			try {
				mapPoToWoOutputBean = invoicePOService
						.mapPoToWo(mapPoToWoInputBean);
				mapPoToWoOutputBeanList.add(mapPoToWoOutputBean);
			} catch (DataAccessException e) {
				throw new DataAccessException(e.getMessage());
			}
			 catch (CommonCustomException e) {
					throw new CommonCustomException(e.getMessage());
				}
			if (!mapPoToWoOutputBeanList.isEmpty()) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Mapped PO to WO details are displayed successfully.",
								mapPoToWoOutputBeanList));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								MISConstants.FAILURE,
								"Failed to fetch the details.",
								mapPoToWoOutputBeanList));
			}
		}
		
	/*	
		/**
		 * @category This service is used unmap the purchase order to the workorder.
		 * @param
		 * @return MapPoToWorkorderOutputBean
		 * @throws BusinessException
		 * @throws DataAccessException
		 *//*
		@ApiOperation(value = "Map PO to WO. This service is used unmap the purchase order to the workorder."
				+ " .", httpMethod = "POST",

		notes = "This service is used to unmap the purchase order to the workorder.",

		nickname = "Unmap PO to WO", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MapPoToWorkorderOutputBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = SearchPOOutPutBean.class),
				@ApiResponse(code = 201, message = "All Purchase Order Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Unmap PO to WO", response = URI.class)),
				@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Unmap PO to WO", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = InvoicePurchaseOrderURIConstants.UNMAPPING_TO_WORKORDER, method = RequestMethod.POST)
		public @ResponseBody ResponseEntity<MISResponse> unmapPoToWo(@Valid @RequestBody MapPoToWorkorderInputBean mapPoToWoInputBean)
				throws DataAccessException,CommonCustomException {

			MapPoToWorkorderOutputBean mapPoToWoOutputBean = new MapPoToWorkorderOutputBean();
			List<MapPoToWorkorderOutputBean> mapPoToWoOutputBeanList = new ArrayList<MapPoToWorkorderOutputBean>();
  
			try {
				mapPoToWoOutputBean = invoicePOService
						.unmapPoToWo(mapPoToWoInputBean);
				mapPoToWoOutputBeanList.add(mapPoToWoOutputBean);
			} catch (Exception e) {
				throw new DataAccessException(
						"Failed to retrieve details :  ", e);
			}

			if (!mapPoToWoOutputBeanList.isEmpty()) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Unmapped PO to WO details are displayed successfully."));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
								MISConstants.FAILURE,
								"Failed to fetch the details.",
								mapPoToWoOutputBeanList));
			}
		}
		*/
		//EOA by Smrithi
}
