package com.thbs.mis.invoice.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.thbs.mis.confirmation.controller.ConfirmationByManagerController;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.invoice.bean.SearchFilteredInvoiceOutputBean;
import com.thbs.mis.invoice.bean.SearchPOOutPutBean;
import com.thbs.mis.invoice.bean.SummaryForGenerateInvoiceInputBean;
import com.thbs.mis.invoice.bean.SummaryForGenerateInvoiceOutputBean;
import com.thbs.mis.invoice.constant.InvoicePurchaseOrderURIConstants;
import com.thbs.mis.invoice.constant.InvoiceURIConstants;
import com.thbs.mis.invoice.service.InvoiceService;

@Controller
public class InvoiceController {
	
	@Autowired
	private InvoiceService invoiceService;

	
	private static final AppLog LOG = LogFactory
			.getLog(ConfirmationByManagerController.class);
    
	
	//Added by Smrithi
	/**
	 * @category This service is used for UI page to show the summary for generate invoice.
	 * @return SummaryForGenerateInvoiceOutputBean
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Summary for Generate Invoice. This service will be called when user wants to view  the summary for generate invoice in UI."
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented toshow the summary for generate invoice.",

	nickname = "Summary for Generate Invoice", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = SummaryForGenerateInvoiceOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = SummaryForGenerateInvoiceOutputBean.class),
			@ApiResponse(code = 201, message = "Summary for Generate Invoice are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Summary for Generate Invoice", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Summary for Generate Invoice", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = InvoiceURIConstants.SUMMARY_FOR_GENERATE_INVOICE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> summaryForGenerateInvoice(@Valid @RequestBody SummaryForGenerateInvoiceInputBean inputBean )
			throws DataAccessException,CommonCustomException {

		List<SummaryForGenerateInvoiceOutputBean> outputBeanList = null;

		try {
			outputBeanList = invoiceService.summaryForGenerateInvoice(inputBean);
		}
		 catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve Summary For Generate Invoice :  ", e);
		}

		if (!outputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Summary For Generate Invoice displayed successfully.",
							outputBeanList));
		} else {
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Failed to fetch Summary For Generate Invoice.",
							outputBeanList));
		}
	}
	
	
	//EOA by Smrithi
	// Added by Balaji
			/**
			 * @category This service is used for UI page to show the view Invoice Purchase order details.
			 *           <Description getAllInvoicePurchaseOrders> : view Invoice Purchase
			 *           Order with .
			 * @param
			 * @return List<invoicePODetailsBean>
			 * @throws BusinessException
			 * @throws DataAccessException
			 */
			/*@ApiOperation(value = "Get all Invoices and its Details by using advanced filters. This service will be called when user wants to view list of Purchase Order Details in UI"
					+ " .", httpMethod = "POST",

			notes = "This Service has been implemented to fetch all Purchase Order Details."
					+ "<br>The Response will be the list of all Purchase Order Details.",

			nickname = "Get all Purchase Order Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = SearchPOOutPutBean.class)
			@ApiResponses(value = {
					@ApiResponse(code = 200, message = "Request Executed Successfully", response = SearchPOOutPutBean.class),
					@ApiResponse(code = 201, message = "All Purchase Order Details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Purchase Order Details", response = URI.class)),
					@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Purchase Order Details", response = URI.class)),
					@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
					@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
			@RequestMapping(value = InvoicePurchaseOrderURIConstants.SEARCH_FILTERED_PURCHASE_ORDERS, method = RequestMethod.POST)
			public @ResponseBody ResponseEntity<MISResponse> searchFilteredInvoices(@PathVariable short buId,@PathVariable int loggedInEmpId  )
					throws DataAccessException,CommonCustomException {
				
		 		LOG.startUsecase("Search Filtered Invoices ");

		 		List<SearchFilteredInvoiceOutputBean> filterInvoiceOutputBeanList = new ArrayList<SearchFilteredInvoiceOutputBean>();
		 		//FilterPurchaseOrderOutputBean filterPoOutputBean = new FilterPurchaseOrderOutputBean();
		 		
		 		filterInvoiceOutputBeanList = invoiceService.searchFilteredInvoices(buId,loggedInEmpId);
				
		 		if(!filterInvoiceOutputBeanList.isEmpty())
		 		{
		 			return ResponseEntity.status(HttpStatus.OK.value()).body(
		 				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
		 						"Invoice Details are displayed successfully.", filterInvoiceOutputBeanList));
		 		}else{
		 			return ResponseEntity.status(HttpStatus.OK.value()).body(
		 					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
		 							"No Invoices Found", filterInvoiceOutputBeanList));
		 		}
				
			}*/
			// EOA by Balaji
}
