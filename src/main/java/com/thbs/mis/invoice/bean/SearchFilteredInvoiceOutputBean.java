package com.thbs.mis.invoice.bean;

import java.math.BigDecimal;
import java.util.Date;

public class SearchFilteredInvoiceOutputBean {
	
	private String invoiceNumber;
	
	private int milestoneId;
	
	private String projectName;
	
	private Date invoiceDate;
	
	private BigDecimal invoiceAmount;
	
	private Date lastSubmissionDate;
	
	private int daysUntilDue;
	
	private String paymentStatus;
	
	private String invoiceComments;
	
	private String creditNoteGenerated;
	
	private Date creditNoteDate;
	
	private BigDecimal creditNoteAmount;
	
	private double poBalance;

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public int getMilestoneId() {
		return milestoneId;
	}

	public void setMilestoneId(int milestoneId) {
		this.milestoneId = milestoneId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public BigDecimal getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(BigDecimal invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public Date getLastSubmissionDate() {
		return lastSubmissionDate;
	}

	public void setLastSubmissionDate(Date lastSubmissionDate) {
		this.lastSubmissionDate = lastSubmissionDate;
	}

	public int getDaysUntilDue() {
		return daysUntilDue;
	}

	public void setDaysUntilDue(int daysUntilDue) {
		this.daysUntilDue = daysUntilDue;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getInvoiceComments() {
		return invoiceComments;
	}

	public void setInvoiceComments(String invoiceComments) {
		this.invoiceComments = invoiceComments;
	}

	public String getCreditNoteGenerated() {
		return creditNoteGenerated;
	}

	public void setCreditNoteGenerated(String creditNoteGenerated) {
		this.creditNoteGenerated = creditNoteGenerated;
	}

	public Date getCreditNoteDate() {
		return creditNoteDate;
	}

	public void setCreditNoteDate(Date creditNoteDate) {
		this.creditNoteDate = creditNoteDate;
	}

	public BigDecimal getCreditNoteAmount() {
		return creditNoteAmount;
	}

	public void setCreditNoteAmount(BigDecimal creditNoteAmount) {
		this.creditNoteAmount = creditNoteAmount;
	}

	public double getPoBalance() {
		return poBalance;
	}

	public void setPoBalance(double poBalance) {
		this.poBalance = poBalance;
	}

	@Override
	public String toString() {
		return "ViewInvoiceOutputBean [invoiceNumber=" + invoiceNumber
				+ ", milestoneId=" + milestoneId + ", projectName="
				+ projectName + ", invoiceDate=" + invoiceDate
				+ ", invoiceAmount=" + invoiceAmount + ", lastSubmissionDate="
				+ lastSubmissionDate + ", daysUntilDue=" + daysUntilDue
				+ ", paymentStatus=" + paymentStatus + ", invoiceComments="
				+ invoiceComments + ", creditNoteGenerated="
				+ creditNoteGenerated + ", creditNoteDate=" + creditNoteDate
				+ ", creditNoteAmount=" + creditNoteAmount + ", poBalance="
				+ poBalance + "]";
	}
	
	
	
	
}
