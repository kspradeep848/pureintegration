package com.thbs.mis.invoice.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class InvoicePODetailsBean {
	private String poNumber;
	private int workOrderCount;
	private String poType;
	private double poValue;
	private double poUtilized;
	private double poBalance;
	private String poMapType;
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date poDeliveryDate;
	private String isWitholdTax;
	private double withholdTaxAmount;
	private double surplusAmount;
	private String buyerName;
	private String buyerEmailId;
	private List<POLineItemsDetailsBean> poLineItemsDetailsBean;
	private double totalLineItem;
	private double taxAddition;
	private double poValuesGrandTotal;
	private int poCurrency;
	private int taxPOCurrency;
	private String poComments;
	private String poCreatedBy;
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date poCreatedOn;
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date poUpdatedOn;
	private String poUpdatedBy;
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public int getWorkOrderCount() {
		return workOrderCount;
	}
	public void setWorkOrderCount(int workOrderCount) {
		this.workOrderCount = workOrderCount;
	}
	public String getPoType() {
		return poType;
	}
	public void setPoType(String poType) {
		this.poType = poType;
	}
	public double getPoValue() {
		return poValue;
	}
	public void setPoValue(double poValue) {
		this.poValue = poValue;
	}
	public double getPoUtilized() {
		return poUtilized;
	}
	public void setPoUtilized(double poUtilized) {
		this.poUtilized = poUtilized;
	}
	public double getPoBalance() {
		return poBalance;
	}
	public void setPoBalance(double poBalance) {
		this.poBalance = poBalance;
	}
	public String getPoMapType() {
		return poMapType;
	}
	public void setPoMapType(String poMapType) {
		this.poMapType = poMapType;
	}
	public Date getPoDeliveryDate() {
		return poDeliveryDate;
	}
	public void setPoDeliveryDate(Date poDeliveryDate) {
		this.poDeliveryDate = poDeliveryDate;
	}
	public String getIsWitholdTax() {
		return isWitholdTax;
	}
	public void setIsWitholdTax(String isWitholdTax) {
		this.isWitholdTax = isWitholdTax;
	}
	
	public double getWithholdTaxAmount() {
		return withholdTaxAmount;
	}
	public void setWithholdTaxAmount(double withholdTaxAmount) {
		this.withholdTaxAmount = withholdTaxAmount;
	}
	public double getSurplusAmount() {
		return surplusAmount;
	}
	public void setSurplusAmount(double surplusAmount) {
		this.surplusAmount = surplusAmount;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getBuyerEmailId() {
		return buyerEmailId;
	}
	public void setBuyerEmailId(String buyerEmailId) {
		this.buyerEmailId = buyerEmailId;
	}
	
	public List<POLineItemsDetailsBean> getPoLineItemsDetailsBean() {
		return poLineItemsDetailsBean;
	}
	public void setPoLineItemsDetailsBean(List<POLineItemsDetailsBean> poLineItemsDetailsBean) {
		this.poLineItemsDetailsBean = poLineItemsDetailsBean;
	}
	public double getTotalLineItem() {
		return totalLineItem;
	}
	public void setTotalLineItem(double totalLineItem) {
		this.totalLineItem = totalLineItem;
	}
	public double getTaxAddition() {
		return taxAddition;
	}
	public void setTaxAddition(double taxAddition) {
		this.taxAddition = taxAddition;
	}
	public double getPoValuesGrandTotal() {
		return poValuesGrandTotal;
	}
	public void setPoValuesGrandTotal(double poValuesGrandTotal) {
		this.poValuesGrandTotal = poValuesGrandTotal;
	}
	public int getPoCurrency() {
		return poCurrency;
	}
	public void setPoCurrency(int poCurrency) {
		this.poCurrency = poCurrency;
	}
	public String getPoComments() {
		return poComments;
	}
	public void setPoComments(String poComments) {
		this.poComments = poComments;
	}
	public String getPoCreatedBy() {
		return poCreatedBy;
	}
	public void setPoCreatedBy(String poCreatedBy) {
		this.poCreatedBy = poCreatedBy;
	}
	public Date getPoCreatedOn() {
		return poCreatedOn;
	}
	public void setPoCreatedOn(Date poCreatedOn) {
		this.poCreatedOn = poCreatedOn;
	}
	public Date getPoUpdatedOn() {
		return poUpdatedOn;
	}
	public void setPoUpdatedOn(Date poUpdatedOn) {
		this.poUpdatedOn = poUpdatedOn;
	}
	public String getPoUpdatedBy() {
		return poUpdatedBy;
	}
	public void setPoUpdatedBy(String poUpdatedBy) {
		this.poUpdatedBy = poUpdatedBy;
	}
	
	public int getTaxPOCurrency() {
		return taxPOCurrency;
	}
	public void setTaxPOCurrency(int taxPOCurrency) {
		this.taxPOCurrency = taxPOCurrency;
	}
	@Override
	public String toString() {
		return "InvoicePODetailsBean [poNumber=" + poNumber + ", workOrderCount=" + workOrderCount + ", poType="
				+ poType + ", poValue=" + poValue + ", poUtilized=" + poUtilized + ", poBalance=" + poBalance
				+ ", poMapType=" + poMapType + ", poDeliveryDate=" + poDeliveryDate + ", isWitholdTax=" + isWitholdTax
				+ ", withholdTaxAmount=" + withholdTaxAmount + ", surplusAmount=" + surplusAmount + ", buyerName="
				+ buyerName + ", buyerEmailId=" + buyerEmailId + ", poLineItemsDetailsBean=" + poLineItemsDetailsBean
				+ ", totalLineItem=" + totalLineItem + ", taxAddition=" + taxAddition + ", poValuesGrandTotal="
				+ poValuesGrandTotal + ", poCurrency=" + poCurrency + ", taxPOCurrency=" + taxPOCurrency
				+ ", poComments=" + poComments + ", poCreatedBy=" + poCreatedBy + ", poCreatedOn=" + poCreatedOn
				+ ", poUpdatedOn=" + poUpdatedOn + ", poUpdatedBy=" + poUpdatedBy + "]";
	}

	
	
	
}
