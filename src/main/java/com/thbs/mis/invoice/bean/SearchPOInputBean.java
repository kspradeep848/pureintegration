package com.thbs.mis.invoice.bean;

import java.util.Date;

public class SearchPOInputBean {
 private String countryCode;
 private int loggedInEmpId;
 private Date startDate; 
 private Date endDate; 
 private String poNumber;
public String getCountryCode() {
	return countryCode;
}
public void setCountryCode(String countryCode) {
	this.countryCode = countryCode;
}


public int getLoggedInEmpId() {
	return loggedInEmpId;
}
public void setLoggedInEmpId(int loggedInEmpId) {
	this.loggedInEmpId = loggedInEmpId;
}
public Date getStartDate() {
	return startDate;
}
public void setStartDate(Date startDate) {
	this.startDate = startDate;
}
public Date getEndDate() {
	return endDate;
}
public void setEndDate(Date endDate) {
	this.endDate = endDate;
}
public String getPoNumber() {
	return poNumber;
}
public void setPoNumber(String poNumber) {
	this.poNumber = poNumber;
}
@Override
public String toString() {
	return "SearchPOInputBean [countryCode=" + countryCode + ", loggedInEmpId=" + loggedInEmpId + ", startDate=" + startDate
			+ ", endDate=" + endDate + ", poNumber=" + poNumber + "]";
} 
 
 
}
