package com.thbs.mis.invoice.bean;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.invoicePO.INVOICE_PO_NUMBER_NOT_VALID;

public class CreateInvoicePOWithDocInputBean {
	@NotBlank(message = "poNumber should not be blank.")
	@NotEmpty(message = "poNumber should not be empty.")
	@NotNull(message = "poNumber should not be null.")
	@INVOICE_PO_NUMBER_NOT_VALID
	private String poNumber;
	@NotBlank(message = "isPoDocument should not be blank.")
	@NotEmpty(message = "isPoDocument should not be empty.")
	@NotNull(message = "isPoDocument should not be null.")
	private String isPoDocument;
	@NotBlank(message = "poType should not be blank.")
	@NotEmpty(message = "poType should not be empty.")
	@NotNull(message = "poType should not be null.")
	private String poType;
	
	//private double poValue;
	private double poUtilized;
	private double poBalance;
	private Date poDeliveryDate;
	private double surplusAmount;
	@NumberFormat(style = Style.NUMBER, pattern = "clientMgrId should be numeric value")
	@NotNull(message = "clientMgrId can not be null")
	@Min(1)
	private short clientMgrId;
	@NumberFormat(style = Style.NUMBER, pattern = "clientBillToAddressId should be numeric value")
	@NotNull(message = "clientBillToAddressId can not be null")
	@Min(1)
	private short clientBillToAddressId;
	@NumberFormat(style = Style.NUMBER, pattern = "clientShipToAddressId should be numeric value")
	@NotNull(message = "clientShipToAddressId can not be null")
	@Min(1)
	private short clientShipToAddressId;
	//@NumberFormat(style = Style.NUMBER, pattern = "poTaxCurrency should be numeric value")
	//@NotNull(message = "poTaxCurrency can not be null")
	//@Min(1)
	//private short poTaxCurrency;
	
	private String poComments;
	@NumberFormat(style = Style.NUMBER, pattern = "poCreatedBy should be numeric value")
	@NotNull(message = "poCreatedBy can not be null")
	@Min(1)
	private int poCreatedBy;
	private Date poCreatedOn;
	private List<POLineItemsDetailsBean> poLineItemsDetailsBean;
	private double taxAddition;
	private double withholdTaxAmount;
	
	@NotBlank(message = "isWitholdTax should not be blank.")
	@NotEmpty(message = "isWitholdTax should not be empty.")
	@NotNull(message = "isWitholdTax should not be null.")
	private String isWithholdTax;
	
	@NumberFormat(style = Style.NUMBER, pattern = "poValueCurrencyType should be numeric value")
	@NotNull(message = "poValueCurrencyType can not be null")
	@Min(1)
	private short poValueCurrencyType;
	private double totalLineItemAmount;
	private double grandTotalAmount;
	
	
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getPoType() {
		return poType;
	}
	public void setPoType(String poType) {
		this.poType = poType;
	}
	
	public double getPoUtilized() {
		return poUtilized;
	}
	public void setPoUtilized(double poUtilized) {
		this.poUtilized = poUtilized;
	}
	public double getPoBalance() {
		return poBalance;
	}
	public void setPoBalance(double poBalance) {
		this.poBalance = poBalance;
	}
	public Date getPoDeliveryDate() {
		return poDeliveryDate;
	}
	public void setPoDeliveryDate(Date poDeliveryDate) {
		this.poDeliveryDate = poDeliveryDate;
	}
	public double getSurplusAmount() {
		return surplusAmount;
	}
	public void setSurplusAmount(double surplusAmount) {
		this.surplusAmount = surplusAmount;
	}
	
	
	public String getPoComments() {
		return poComments;
	}
	public void setPoComments(String poComments) {
		this.poComments = poComments;
	}
	public int getPoCreatedBy() {
		return poCreatedBy;
	}
	public void setPoCreatedBy(int poCreatedBy) {
		this.poCreatedBy = poCreatedBy;
	}
	public Date getPoCreatedOn() {
		return poCreatedOn;
	}
	public void setPoCreatedOn(Date poCreatedOn) {
		this.poCreatedOn = poCreatedOn;
	}
	
	public String getIsPoDocument() {
		return isPoDocument;
	}
	public void setIsPoDocument(String isPoDocument) {
		this.isPoDocument = isPoDocument;
	}
	
	public short getClientMgrId() {
		return clientMgrId;
	}
	public void setClientMgrId(short clientMgrId) {
		this.clientMgrId = clientMgrId;
	}
	
	
	public short getClientBillToAddressId() {
		return clientBillToAddressId;
	}
	public void setClientBillToAddressId(short clientBillToAddressId) {
		this.clientBillToAddressId = clientBillToAddressId;
	}
	public short getClientShipToAddressId() {
		return clientShipToAddressId;
	}
	public void setClientShipToAddressId(short clientShipToAddressId) {
		this.clientShipToAddressId = clientShipToAddressId;
	}
	public List<POLineItemsDetailsBean> getPoLineItemsDetailsBean() {
		return poLineItemsDetailsBean;
	}
	public void setPoLineItemsDetailsBean(List<POLineItemsDetailsBean> poLineItemsDetailsBean) {
		this.poLineItemsDetailsBean = poLineItemsDetailsBean;
	}
	public double getTaxAddition() {
		return taxAddition;
	}
	public void setTaxAddition(double taxAddition) {
		this.taxAddition = taxAddition;
	}
	
	public String getIsWithholdTax() {
		return isWithholdTax;
	}
	public void setIsWithholdTax(String isWithholdTax) {
		this.isWithholdTax = isWithholdTax;
	}
	public double getWithholdTaxAmount() {
		return withholdTaxAmount;
	}
	public void setWithholdTaxAmount(double withholdTaxAmount) {
		this.withholdTaxAmount = withholdTaxAmount;
	}
	public short getPoValueCurrencyType() {
		return poValueCurrencyType;
	}
	public void setPoValueCurrencyType(short poValueCurrencyType) {
		this.poValueCurrencyType = poValueCurrencyType;
	}
	public double getTotalLineItemAmount() {
		return totalLineItemAmount;
	}
	public void setTotalLineItemAmount(double totalLineItemAmount) {
		this.totalLineItemAmount = totalLineItemAmount;
	}
	public double getGrandTotalAmount() {
		return grandTotalAmount;
	}
	public void setGrandTotalAmount(double grandTotalAmount) {
		this.grandTotalAmount = grandTotalAmount;
	}
	@Override
	public String toString() {
		return "CreateInvoicePOWithDocInputBean [poNumber=" + poNumber + ", isPoDocument=" + isPoDocument + ", poType="
				+ poType + ", poUtilized=" + poUtilized + ", poBalance=" + poBalance + ", poDeliveryDate="
				+ poDeliveryDate + ", surplusAmount=" + surplusAmount + ", clientMgrId=" + clientMgrId
				+ ", clientBillToAddressId=" + clientBillToAddressId + ", clientShipToAddressId="
				+ clientShipToAddressId + ", poComments=" + poComments + ", poCreatedBy=" + poCreatedBy
				+ ", poCreatedOn=" + poCreatedOn + ", poLineItemsDetailsBean=" + poLineItemsDetailsBean
				+ ", taxAddition=" + taxAddition + ", withholdTaxAmount=" + withholdTaxAmount + ", isWithholdTax="
				+ isWithholdTax + ", poValueCurrencyType=" + poValueCurrencyType + ", totalLineItemAmount="
				+ totalLineItemAmount + ", grandTotalAmount=" + grandTotalAmount + "]";
	}
	
	
}
