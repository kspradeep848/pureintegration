package com.thbs.mis.invoice.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class InvoiceShiftingPOInputBean {
	
	
	
	@NotNull(message = "loggedInEmpId cannot be Null")
	@NumberFormat(style = Style.NUMBER, pattern = "loggedInEmpId should be numeric value")
	@Min(1)
	private Integer loggedInEmpId;
	
	@NumberFormat(style = Style.NUMBER, pattern = "purchaseOrderId should be numeric value")
	@NotNull(message = "purchaseOrderId cannot be Null")
	@Min(1)
	private Integer purchaseOrderId;

	public Integer getLoggedInEmpId() {
		return loggedInEmpId;
	}

	public void setLoggedInEmpId(Integer loggedInEmpId) {
		this.loggedInEmpId = loggedInEmpId;
	}

	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}

	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}

	@Override
	public String toString() {
		return "InvoiceShiftingPOInputBean [loggedInEmpId=" + loggedInEmpId
				+ ", purchaseOrderId=" + purchaseOrderId + "]";
	}
	
	

}
