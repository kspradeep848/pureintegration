package com.thbs.mis.invoice.bean;

import java.util.List;

public class SummaryForGenerateInvoiceInputBean {

	private Integer empId;
	private List<Integer>  triggerId;
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public List<Integer> getTriggerId() {
		return triggerId;
	}
	public void setTriggerId(List<Integer> triggerId) {
		this.triggerId = triggerId;
	}
	@Override
	public String toString() {
		return "SummaryForGenerateInvoiceInputBean [empId=" + empId
				+ ", triggerId=" + triggerId + "]";
	}
	
}
