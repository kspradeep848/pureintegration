package com.thbs.mis.invoice.bean;

import java.util.Date;

public class FilterPurchaseOrderOutputBean {
	
	private String poNumber;
	
	private int workOrderCount;
	
	private String poType;
	
	private double poValue;
	
	private double poUtilized;
	
	private double poBalance;
	
	private String poMapType;
	
	private Date poDeliveryDate;
	
	private double witholdTaxAmount;
	
	private double surplusAmount;
	
	private Date createdOn;
	
	private Date updatedOn;
	
	private int lastUpdatedBy;
	
	private String clientName;

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public int getWorkOrderCount() {
		return workOrderCount;
	}

	public void setWorkOrderCount(int workOrderCount) {
		this.workOrderCount = workOrderCount;
	}

	public String getPoType() {
		return poType;
	}

	public void setPoType(String poType) {
		this.poType = poType;
	}

	public double getPoValue() {
		return poValue;
	}

	public void setPoValue(double poValue) {
		this.poValue = poValue;
	}

	public double getPoUtilized() {
		return poUtilized;
	}

	public void setPoUtilized(double poUtilized) {
		this.poUtilized = poUtilized;
	}

	public double getPoBalance() {
		return poBalance;
	}

	public void setPoBalance(double poBalance) {
		this.poBalance = poBalance;
	}

	public String getPoMapType() {
		return poMapType;
	}

	public void setPoMapType(String poMapType) {
		this.poMapType = poMapType;
	}

	public Date getPoDeliveryDate() {
		return poDeliveryDate;
	}

	public void setPoDeliveryDate(Date poDeliveryDate) {
		this.poDeliveryDate = poDeliveryDate;
	}

	public double getWitholdTaxAmount() {
		return witholdTaxAmount;
	}

	public void setWitholdTaxAmount(double witholdTaxAmount) {
		this.witholdTaxAmount = witholdTaxAmount;
	}

	public double getSurplusAmount() {
		return surplusAmount;
	}

	public void setSurplusAmount(double surplusAmount) {
		this.surplusAmount = surplusAmount;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public int getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(int lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	@Override
	public String toString() {
		return "FilterPurchaseOrderOutputBean [poNumber=" + poNumber
				+ ", workOrderCount=" + workOrderCount + ", poType=" + poType
				+ ", poValue=" + poValue + ", poUtilized=" + poUtilized
				+ ", poBalance=" + poBalance + ", poMapType=" + poMapType
				+ ", poDeliveryDate=" + poDeliveryDate + ", witholdTaxAmount="
				+ witholdTaxAmount + ", surplusAmount=" + surplusAmount
				+ ", createdOn=" + createdOn + ", updatedOn=" + updatedOn
				+ ", lastUpdatedBy=" + lastUpdatedBy + ", clientName="
				+ clientName + "]";
	}
	

}
