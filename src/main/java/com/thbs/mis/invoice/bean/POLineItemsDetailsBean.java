package com.thbs.mis.invoice.bean;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class POLineItemsDetailsBean {
	@NotBlank(message = "lineItemNumber should not be blank.")
	@NotEmpty(message = "lineItemNumber should not be empty.")
	@NotNull(message = "lineItemNumber should not be null.")
	private String lineItemNumber;
	@NotBlank(message = "poDescription should not be blank.")
	@NotEmpty(message = "poDescription should not be empty.")
	@NotNull(message = "poDescription should not be null.")
	private String poDescription;
	@NotBlank(message = "unitOfMeasure should not be blank.")
	@NotEmpty(message = "unitOfMeasure should not be empty.")
	@NotNull(message = "unitOfMeasure should not be null.")
	private String unitOfMeasure;
	private double quantity;
	private double unitCost;
	private double amount;
	
	public String getLineItemNumber() {
		return lineItemNumber;
	}
	public void setLineItemNumber(String lineItemNumber) {
		this.lineItemNumber = lineItemNumber;
	}
	public String getPoDescription() {
		return poDescription;
	}
	public void setPoDescription(String poDescription) {
		this.poDescription = poDescription;
	}
	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}
	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	public double getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(double unitCost) {
		this.unitCost = unitCost;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "poLineItemsDetailsBean [lineItemNumber=" + lineItemNumber + ", poDescription=" + poDescription
				+ ", unitOfMeasure=" + unitOfMeasure + ", quantity=" + quantity + ", unitCost=" + unitCost + ", amount="
				+ amount + "]";
	}
	
	
	
}
