package com.thbs.mis.invoice.bean;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.invoicePO.INVOICE_PO_NUMBER_NOT_VALID;

public class UpdateInvoicePOWithDocInputBean {
	@NotBlank(message = "poNumber should not be blank.")
	@NotEmpty(message = "poNumber should not be empty.")
	@NotNull(message = "poNumber should not be null.")
	@INVOICE_PO_NUMBER_NOT_VALID
	private String poNumber;
	@NotBlank(message = "isPoDocument should not be blank.")
	@NotEmpty(message = "isPoDocument should not be empty.")
	@NotNull(message = "isPoDocument should not be null.")
	private String isPoDocument;
	@NotBlank(message = "poType should not be blank.")
	@NotEmpty(message = "poType should not be empty.")
	@NotNull(message = "poType should not be null.")
	private String poType;
	private double poValue;
	private double poUtilized;
	private double poBalance;
	private Date poDeliveryDate;
	private double surplusAmount;
	@NumberFormat(style = Style.NUMBER, pattern = "clientMgrId should be numeric value")
	@NotNull(message = "clientMgrId can not be null")
	@Min(1)
	private short clientMgrId;
	@NumberFormat(style = Style.NUMBER, pattern = "clientBillToAddressIdId should be numeric value")
	@NotNull(message = "clientBillToAddressIdId can not be null")
	@Min(1)
	private short clientBillToAddressIdId;
	@NumberFormat(style = Style.NUMBER, pattern = "clientShipToAddressIdId should be numeric value")
	@NotNull(message = "clientShipToAddressIdId can not be null")
	@Min(1)
	private short clientShipToAddressIdId;
	//@NumberFormat(style = Style.NUMBER, pattern = "poTaxCurrency should be numeric value")
	//@NotNull(message = "poTaxCurrency can not be null")
	//@Min(1)
	//private short poTaxCurrency;
	@NotBlank(message = "poUpdationComments should not be blank.")
	@NotEmpty(message = "poUpdationComments should not be empty.")
	@NotNull(message = "poUpdationComments should not be null.")
	private String poUpdationComments;
	@NumberFormat(style = Style.NUMBER, pattern = "poUpdatedBy should be numeric value")
	@NotNull(message = "poUpdatedBy can not be null")
	@Min(1)
	private int poUpdatedBy;
	private Date poUpdatedOn;
	private List<POLineItemsDetailsBean> poLineItemsDetailsBean;
	private double taxAddition;
	@NotBlank(message = "isWitholdTax should not be blank.")
	@NotEmpty(message = "isWitholdTax should not be empty.")
	@NotNull(message = "isWitholdTax should not be null.")
	private String isWitholdTax;
	private double witholdTaxAmount;
	@NumberFormat(style = Style.NUMBER, pattern = "poValueCurrencyType should be numeric value")
	@NotNull(message = "poValueCurrencyType can not be null")
	@Min(1)
	private short poValueCurrencyType;
	private double totalLineItemAmount;
	private double grandTotalAmount;
	
	
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getPoType() {
		return poType;
	}
	public void setPoType(String poType) {
		this.poType = poType;
	}
	public double getPoValue() {
		return poValue;
	}
	public void setPoValue(double poValue) {
		this.poValue = poValue;
	}
	public double getPoUtilized() {
		return poUtilized;
	}
	public void setPoUtilized(double poUtilized) {
		this.poUtilized = poUtilized;
	}
	public double getPoBalance() {
		return poBalance;
	}
	public void setPoBalance(double poBalance) {
		this.poBalance = poBalance;
	}
	public Date getPoDeliveryDate() {
		return poDeliveryDate;
	}
	public void setPoDeliveryDate(Date poDeliveryDate) {
		this.poDeliveryDate = poDeliveryDate;
	}
	public double getSurplusAmount() {
		return surplusAmount;
	}
	public void setSurplusAmount(double surplusAmount) {
		this.surplusAmount = surplusAmount;
	}
	
	
	public String getIsPoDocument() {
		return isPoDocument;
	}
	public void setIsPoDocument(String isPoDocument) {
		this.isPoDocument = isPoDocument;
	}
	
	public short getClientMgrId() {
		return clientMgrId;
	}
	public void setClientMgrId(short clientMgrId) {
		this.clientMgrId = clientMgrId;
	}
	public short getClientBillToAddressIdId() {
		return clientBillToAddressIdId;
	}
	public void setClientBillToAddressIdId(short clientBillToAddressIdId) {
		this.clientBillToAddressIdId = clientBillToAddressIdId;
	}
	public short getClientShipToAddressIdId() {
		return clientShipToAddressIdId;
	}
	public void setClientShipToAddressIdId(short clientShipToAddressIdId) {
		this.clientShipToAddressIdId = clientShipToAddressIdId;
	}
	
	
	public List<POLineItemsDetailsBean> getPoLineItemsDetailsBean() {
		return poLineItemsDetailsBean;
	}
	public void setPoLineItemsDetailsBean(List<POLineItemsDetailsBean> poLineItemsDetailsBean) {
		this.poLineItemsDetailsBean = poLineItemsDetailsBean;
	}
	public double getTaxAddition() {
		return taxAddition;
	}
	public void setTaxAddition(double taxAddition) {
		this.taxAddition = taxAddition;
	}
	public String getIsWitholdTax() {
		return isWitholdTax;
	}
	public void setIsWitholdTax(String isWitholdTax) {
		this.isWitholdTax = isWitholdTax;
	}
	public double getWitholdTaxAmount() {
		return witholdTaxAmount;
	}
	public void setWitholdTaxAmount(double witholdTaxAmount) {
		this.witholdTaxAmount = witholdTaxAmount;
	}
	public short getPoValueCurrencyType() {
		return poValueCurrencyType;
	}
	public void setPoValueCurrencyType(short poValueCurrencyType) {
		this.poValueCurrencyType = poValueCurrencyType;
	}
	public String getPoUpdationComments() {
		return poUpdationComments;
	}
	public void setPoUpdationComments(String poUpdationComments) {
		this.poUpdationComments = poUpdationComments;
	}
	public int getPoUpdatedBy() {
		return poUpdatedBy;
	}
	public void setPoUpdatedBy(int poUpdatedBy) {
		this.poUpdatedBy = poUpdatedBy;
	}
	public Date getPoUpdatedOn() {
		return poUpdatedOn;
	}
	public void setPoUpdatedOn(Date poUpdatedOn) {
		this.poUpdatedOn = poUpdatedOn;
	}
	public double getTotalLineItemAmount() {
		return totalLineItemAmount;
	}
	public void setTotalLineItemAmount(double totalLineItemAmount) {
		this.totalLineItemAmount = totalLineItemAmount;
	}
	public double getGrandTotalAmount() {
		return grandTotalAmount;
	}
	public void setGrandTotalAmount(double grandTotalAmount) {
		this.grandTotalAmount = grandTotalAmount;
	}
	@Override
	public String toString() {
		return "UpdateInvoicePOWithDocInputBean [poNumber=" + poNumber + ", isPoDocument=" + isPoDocument + ", poType="
				+ poType + ", poValue=" + poValue + ", poUtilized=" + poUtilized + ", poBalance=" + poBalance
				+ ", poDeliveryDate=" + poDeliveryDate + ", surplusAmount=" + surplusAmount + ", clientMgrId="
				+ clientMgrId + ", clientBillToAddressIdId=" + clientBillToAddressIdId + ", clientShipToAddressIdId="
				+ clientShipToAddressIdId + ", poUpdationComments="
				+ poUpdationComments + ", poUpdatedBy=" + poUpdatedBy + ", poUpdatedOn=" + poUpdatedOn
				+ ", poLineItemsDetailsBean=" + poLineItemsDetailsBean + ", taxAddition=" + taxAddition
				+ ", isWitholdTax=" + isWitholdTax + ", witholdTaxAmount=" + witholdTaxAmount + ", poValueCurrencyType="
				+ poValueCurrencyType + ", totalLineItemAmount=" + totalLineItemAmount + ", grandTotalAmount="
				+ grandTotalAmount + "]";
	}
	
}
