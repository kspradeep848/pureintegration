package com.thbs.mis.invoice.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class MapPoToWorkorderInputBean {
	
	@NumberFormat(style = Style.NUMBER, pattern = "fkWorkorderId should be numeric value")
	@NotNull(message = "fkWorkorderId can not be null")
	@Min(1)
	private Integer fkWorkorderId;
	
	@NumberFormat(style = Style.NUMBER, pattern = "pkPurchaseOrderId should be numeric value")
	@NotNull(message = "pkPurchaseOrderId can not be null")
	@Min(1)
	private Integer pkPurchaseOrderId;
	
	@NumberFormat(style = Style.NUMBER, pattern = "loggedInEmpId should be numeric value")
	@NotNull(message = "loggedInEmpId can not be null")
	@Min(1)
	private Integer loggedInEmpId;
	
	public Integer getFkWorkorderId() {
		return fkWorkorderId;
	}
	
	public Integer getLoggedInEmpId() {
		return loggedInEmpId;
	}

	public void setLoggedInEmpId(Integer loggedInEmpId) {
		this.loggedInEmpId = loggedInEmpId;
	}

	public void setFkWorkorderId(Integer fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}
	public Integer getPkPurchaseOrderId() {
		return pkPurchaseOrderId;
	}
	public void setPkPurchaseOrderId(Integer pkPurchaseOrderId) {
		this.pkPurchaseOrderId = pkPurchaseOrderId;
	}
	@Override
	public String toString() {
		return "MapPoToWorkorderInputBean [fkWorkorderId=" + fkWorkorderId
				+ ", pkPurchaseOrderId=" + pkPurchaseOrderId + "]";
	}
	
	
}
