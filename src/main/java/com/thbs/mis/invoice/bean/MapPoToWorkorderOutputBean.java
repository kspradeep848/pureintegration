package com.thbs.mis.invoice.bean;

import java.math.BigDecimal;

public class MapPoToWorkorderOutputBean {

	private String projectName;
	private Integer noOfcomponents;
	private String purchaseNumber;
	private String woType;
	private String workorderNumber;
	private String sowCurrencyName;
	private Integer noOfPo;
	private String WoStatusName;
	private BigDecimal sowAmount;
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Integer getNoOfcomponents() {
		return noOfcomponents;
	}
	public void setNoOfcomponents(Integer noOfcomponents) {
		this.noOfcomponents = noOfcomponents;
	}
	public String getPurchaseNumber() {
		return purchaseNumber;
	}
	public void setPurchaseNumber(String purchaseNumber) {
		this.purchaseNumber = purchaseNumber;
	}
	public String getWoType() {
		return woType;
	}
	public void setWoType(String woType) {
		this.woType = woType;
	}
	public String getWorkorderNumber() {
		return workorderNumber;
	}
	public void setWorkorderNumber(String workorderNumber) {
		this.workorderNumber = workorderNumber;
	}
	public String getSowCurrencyName() {
		return sowCurrencyName;
	}
	public void setSowCurrencyName(String sowCurrencyName) {
		this.sowCurrencyName = sowCurrencyName;
	}
	public Integer getNoOfPo() {
		return noOfPo;
	}
	public void setNoOfPo(Integer noOfPo) {
		this.noOfPo = noOfPo;
	}
	public String getWoStatusName() {
		return WoStatusName;
	}
	public void setWoStatusName(String woStatusName) {
		WoStatusName = woStatusName;
	}
	public BigDecimal getSowAmount() {
		return sowAmount;
	}
	public void setSowAmount(BigDecimal sowAmount) {
		this.sowAmount = sowAmount;
	}
	@Override
	public String toString() {
		return "MapPoToWorkorderOutputBean [projectName=" + projectName
				+ ", noOfcomponents=" + noOfcomponents + ", purchaseNumber="
				+ purchaseNumber + ", woType=" + woType + ", workorderNumber="
				+ workorderNumber + ", sowCurrencyName=" + sowCurrencyName
				+ ", noOfPo=" + noOfPo + ", WoStatusName=" + WoStatusName
				+ ", sowAmount=" + sowAmount + "]";
	}
	
	
	
}
