package com.thbs.mis.invoice.bean;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.invoicePO.INVOICE_PO_NUMBER_NOT_VALID;

public class CreateInvoicePOWithoutDocInputBean {
	@NotBlank(message = "poNumber should not be blank.")
	@NotEmpty(message = "poNumber should not be empty.")
	@NotNull(message = "poNumber should not be null.")
	@INVOICE_PO_NUMBER_NOT_VALID
	private String poNumber;
	@NotBlank(message = "isPoDocument should not be blank.")
	@NotEmpty(message = "isPoDocument should not be empty.")
	@NotNull(message = "isPoDocument should not be null.")
	private String isPoDocument;
	@NotBlank(message = "poType should not be blank.")
	@NotEmpty(message = "poType should not be empty.")
	@NotNull(message = "poType should not be null.")
	private String poType;
	
	private double poValue;
	private double poBalance;
	private Date poDeliveryDate;
	private double surplusAmount;
	@NumberFormat(style = Style.NUMBER, pattern = "clientMgrId should be numeric value")
	@NotNull(message = "clientMgrId can not be null")
	@Min(1)
	private short clientMgrId;
	@NumberFormat(style = Style.NUMBER, pattern = "clientBillToAddressId should be numeric value")
	@NotNull(message = "clientBillToAddressId can not be null")
	@Min(1)
	private short clientBillToAddressId;
	@NumberFormat(style = Style.NUMBER, pattern = "clientShipToAddressId should be numeric value")
	@NotNull(message = "clientShipToAddressId can not be null")
	@Min(1)
	private short clientShipToAddressId;
	@NumberFormat(style = Style.NUMBER, pattern = "poCurrency should be numeric value")
	@NotNull(message = "poCurrency can not be null")
	@Min(1)
	private short poCurrency;
	
	private String poComments;
	@NumberFormat(style = Style.NUMBER, pattern = "poCreatedBy should be numeric value")
	@NotNull(message = "poCreatedBy can not be null")
	@Min(1)
	private int poCreatedBy;
	private Date poCreatedOn;
	
	
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getPoType() {
		return poType;
	}
	public void setPoType(String poType) {
		this.poType = poType;
	}
	public double getPoValue() {
		return poValue;
	}
	public void setPoValue(double poValue) {
		this.poValue = poValue;
	}

	
	public Date getPoDeliveryDate() {
		return poDeliveryDate;
	}
	public void setPoDeliveryDate(Date poDeliveryDate) {
		this.poDeliveryDate = poDeliveryDate;
	}
	public double getSurplusAmount() {
		return surplusAmount;
	}
	public void setSurplusAmount(double surplusAmount) {
		this.surplusAmount = surplusAmount;
	}
	
	public short getPoCurrency() {
		return poCurrency;
	}
	public void setPoCurrency(short poCurrency) {
		this.poCurrency = poCurrency;
	}
	public String getPoComments() {
		return poComments;
	}
	public void setPoComments(String poComments) {
		this.poComments = poComments;
	}
	public int getPoCreatedBy() {
		return poCreatedBy;
	}
	public void setPoCreatedBy(int poCreatedBy) {
		this.poCreatedBy = poCreatedBy;
	}
	public Date getPoCreatedOn() {
		return poCreatedOn;
	}
	public void setPoCreatedOn(Date poCreatedOn) {
		this.poCreatedOn = poCreatedOn;
	}
	
	public String getIsPoDocument() {
		return isPoDocument;
	}
	public void setIsPoDocument(String isPoDocument) {
		this.isPoDocument = isPoDocument;
	}
	
	public short getClientMgrId() {
		return clientMgrId;
	}
	public void setClientMgrId(short clientMgrId) {
		this.clientMgrId = clientMgrId;
	}
	
	public short getClientBillToAddressId() {
		return clientBillToAddressId;
	}
	public void setClientBillToAddressId(short clientBillToAddressId) {
		this.clientBillToAddressId = clientBillToAddressId;
	}
	public short getClientShipToAddressId() {
		return clientShipToAddressId;
	}
	public void setClientShipToAddressId(short clientShipToAddressId) {
		this.clientShipToAddressId = clientShipToAddressId;
	}
	
	public double getPoBalance() {
		return poBalance;
	}
	public void setPoBalance(double poBalance) {
		this.poBalance = poBalance;
	}
	@Override
	public String toString() {
		return "CreateInvoicePOWithoutDocInputBean [poNumber=" + poNumber + ", isPoDocument=" + isPoDocument
				+ ", poType=" + poType + ", poValue=" + poValue + ", poBalance=" + poBalance + ", poDeliveryDate="
				+ poDeliveryDate + ", surplusAmount=" + surplusAmount + ", clientMgrId=" + clientMgrId
				+ ", clientBillToAddressId=" + clientBillToAddressId + ", clientShipToAddressId="
				+ clientShipToAddressId + ", poCurrency=" + poCurrency + ", poComments=" + poComments + ", poCreatedBy="
				+ poCreatedBy + ", poCreatedOn=" + poCreatedOn + "]";
	}
	
	
}
