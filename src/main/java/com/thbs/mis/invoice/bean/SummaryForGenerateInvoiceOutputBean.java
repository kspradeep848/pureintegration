package com.thbs.mis.invoice.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class SummaryForGenerateInvoiceOutputBean {

	private String companyName;
	private String companyAddress;
	private String purchaseNumber;
	private Date poDeliveryDate;
	private String gstNumber;
	private Integer sacCode;
	private String clientManager;
	private String workorderNumber;
	private String projectName;
	private String billedToAddress;
	private String billedToState;
	private Integer billedToStateCode;
	private String shippedToAddress;
	private String shippedToState;
	private Integer shippedToStateCode;
	private String poDescription;
	private Double poValue;
	private Double previousInvoiced;
	private Double billedThisInvoice;
	private Double billedToDate;
	private Double balanecInPo;
	private BigDecimal cGST;
	private BigDecimal sGST;
	private BigDecimal iGST;
	private BigDecimal vat;
	private BigDecimal educationalCess;
	private Double totalAmount;
	private String paymentTerms;
	private String bank;
	private Double acountNumber;
	private Double micrCode;
	private String ifscCode;
	private String bankAddress;
	
	private List<MilestoneDetailsList> milestoneDetailsList;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getPurchaseNumber() {
		return purchaseNumber;
	}

	public void setPurchaseNumber(String purchaseNumber) {
		this.purchaseNumber = purchaseNumber;
	}

	public Date getPoDeliveryDate() {
		return poDeliveryDate;
	}

	public void setPoDeliveryDate(Date poDeliveryDate) {
		this.poDeliveryDate = poDeliveryDate;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public Integer getSacCode() {
		return sacCode;
	}

	public void setSacCode(Integer sacCode) {
		this.sacCode = sacCode;
	}

	public String getClientManager() {
		return clientManager;
	}

	public void setClientManager(String clientManager) {
		this.clientManager = clientManager;
	}

	public String getWorkorderNumber() {
		return workorderNumber;
	}

	public void setWorkorderNumber(String workorderNumber) {
		this.workorderNumber = workorderNumber;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getBilledToAddress() {
		return billedToAddress;
	}

	public void setBilledToAddress(String billedToAddress) {
		this.billedToAddress = billedToAddress;
	}

	public String getBilledToState() {
		return billedToState;
	}

	public void setBilledToState(String billedToState) {
		this.billedToState = billedToState;
	}

	public Integer getBilledToStateCode() {
		return billedToStateCode;
	}

	public void setBilledToStateCode(Integer billedToStateCode) {
		this.billedToStateCode = billedToStateCode;
	}

	public String getShippedToAddress() {
		return shippedToAddress;
	}

	public void setShippedToAddress(String shippedToAddress) {
		this.shippedToAddress = shippedToAddress;
	}

	public String getShippedToState() {
		return shippedToState;
	}

	public void setShippedToState(String shippedToState) {
		this.shippedToState = shippedToState;
	}

	public Integer getShippedToStateCode() {
		return shippedToStateCode;
	}

	public void setShippedToStateCode(Integer shippedToStateCode) {
		this.shippedToStateCode = shippedToStateCode;
	}

	public String getPoDescription() {
		return poDescription;
	}

	public void setPoDescription(String poDescription) {
		this.poDescription = poDescription;
	}

	public Double getPoValue() {
		return poValue;
	}

	public void setPoValue(Double poValue) {
		this.poValue = poValue;
	}

	public Double getPreviousInvoiced() {
		return previousInvoiced;
	}

	public void setPreviousInvoiced(Double previousInvoiced) {
		this.previousInvoiced = previousInvoiced;
	}

	public Double getBilledThisInvoice() {
		return billedThisInvoice;
	}

	public void setBilledThisInvoice(Double billedThisInvoice) {
		this.billedThisInvoice = billedThisInvoice;
	}

	public Double getBilledToDate() {
		return billedToDate;
	}

	public void setBilledToDate(Double billedToDate) {
		this.billedToDate = billedToDate;
	}

	public Double getBalanecInPo() {
		return balanecInPo;
	}

	public void setBalanecInPo(Double balanecInPo) {
		this.balanecInPo = balanecInPo;
	}

	public BigDecimal getcGST() {
		return cGST;
	}

	public void setcGST(BigDecimal cGST) {
		this.cGST = cGST;
	}

	public BigDecimal getsGST() {
		return sGST;
	}

	public void setsGST(BigDecimal sGST) {
		this.sGST = sGST;
	}

	public BigDecimal getiGST() {
		return iGST;
	}

	public void setiGST(BigDecimal iGST) {
		this.iGST = iGST;
	}

	public BigDecimal getVat() {
		return vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public BigDecimal getEducationalCess() {
		return educationalCess;
	}

	public void setEducationalCess(BigDecimal educationalCess) {
		this.educationalCess = educationalCess;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public Double getAcountNumber() {
		return acountNumber;
	}

	public void setAcountNumber(Double acountNumber) {
		this.acountNumber = acountNumber;
	}

	public Double getMicrCode() {
		return micrCode;
	}

	public void setMicrCode(Double micrCode) {
		this.micrCode = micrCode;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public List<MilestoneDetailsList> getMilestoneDetailsList() {
		return milestoneDetailsList;
	}

	public void setMilestoneDetailsList(
			List<MilestoneDetailsList> milestoneDetailsList) {
		this.milestoneDetailsList = milestoneDetailsList;
	}

	@Override
	public String toString() {
		return "SummaryForGenerateInvoiceOutputBean [companyName="
				+ companyName + ", companyAddress=" + companyAddress
				+ ", purchaseNumber=" + purchaseNumber + ", poDeliveryDate="
				+ poDeliveryDate + ", gstNumber=" + gstNumber + ", sacCode="
				+ sacCode + ", clientManager=" + clientManager
				+ ", workorderNumber=" + workorderNumber + ", projectName="
				+ projectName + ", billedToAddress=" + billedToAddress
				+ ", billedToState=" + billedToState + ", billedToStateCode="
				+ billedToStateCode + ", shippedToAddress=" + shippedToAddress
				+ ", shippedToState=" + shippedToState
				+ ", shippedToStateCode=" + shippedToStateCode
				+ ", poDescription=" + poDescription + ", poValue=" + poValue
				+ ", previousInvoiced=" + previousInvoiced
				+ ", billedThisInvoice=" + billedThisInvoice
				+ ", billedToDate=" + billedToDate + ", balanecInPo="
				+ balanecInPo + ", cGST=" + cGST + ", sGST=" + sGST + ", iGST="
				+ iGST + ", vat=" + vat + ", educationalCess="
				+ educationalCess + ", totalAmount=" + totalAmount
				+ ", paymentTerms=" + paymentTerms + ", bank=" + bank
				+ ", acountNumber=" + acountNumber + ", micrCode=" + micrCode
				+ ", ifscCode=" + ifscCode + ", bankAddress=" + bankAddress
				+ ", milestoneDetailsList=" + milestoneDetailsList + "]";
	}
	
	
}
