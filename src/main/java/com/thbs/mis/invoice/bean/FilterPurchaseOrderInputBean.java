package com.thbs.mis.invoice.bean;

import java.util.Date;

public class FilterPurchaseOrderInputBean {
	
	private Integer loggedInEmpId;
	
	private Short clientId;
	
	private String poStatus;
	
	private Date poStartDate;
	
	private Date poEndDate;

	public Integer getLoggedInEmpId() {
		return loggedInEmpId;
	}

	public void setLoggedInEmpId(Integer loggedInEmpId) {
		this.loggedInEmpId = loggedInEmpId;
	}

	public Short getClientId() {
		return clientId;
	}

	public void setClientId(Short clientId) {
		this.clientId = clientId;
	}

	public String getPoStatus() {
		return poStatus;
	}

	public void setPoStatus(String poStatus) {
		this.poStatus = poStatus;
	}

	public Date getPoStartDate() {
		return poStartDate;
	}

	public void setPoStartDate(Date poStartDate) {
		this.poStartDate = poStartDate;
	}

	public Date getPoEndDate() {
		return poEndDate;
	}

	public void setPoEndDate(Date poEndDate) {
		this.poEndDate = poEndDate;
	}

	@Override
	public String toString() {
		return "FilterPurchaseOrderInputBean [loggedInEmpId=" + loggedInEmpId
				+ ", clientId=" + clientId + ", poStatus=" + poStatus
				+ ", poStartDate=" + poStartDate + ", poEndDate=" + poEndDate
				+ "]";
	}

	

	
	

}
