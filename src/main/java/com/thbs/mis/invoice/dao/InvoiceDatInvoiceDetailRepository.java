package com.thbs.mis.invoice.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.invoice.bo.InvoiceDatInvoiceDetailBO;

@Repository
public interface InvoiceDatInvoiceDetailRepository extends GenericRepository<InvoiceDatInvoiceDetailBO,Long>{

}
