package com.thbs.mis.invoice.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.invoice.bo.InvoiceDatPurchaseOrderBO;
@Repository
public interface InvoiceDatPurchaseOrderRepository extends GenericRepository<InvoiceDatPurchaseOrderBO,Long>,JpaSpecificationExecutor<InvoiceDatPurchaseOrderBO> {

	// Added By Shashi
	@Query("SELECT new com.thbs.mis.invoice.bo.InvoiceDatPurchaseOrderBO(po.pkPurchaseOrderId,po.commentsForUpdation,po.createdOn,po.poDeliveryDate," 
			+ " po.poDescription,po.poRemainingValue,po.poStatus,po.poType,po.poValue," 
			+ " po.purchaseNumber, po.surplusValue, po.updatedBy, po.updatedOn, poTx.withholdTaxAmount,"  
			+ " poWo.fkWorkorderId) from InvoiceDatPurchaseOrderBO po "
			+ " left join InvoiceDatPoLineItemsTaxDetailBO poTx on po.pkPurchaseOrderId = poTx.fkPurchaseOrderId"
			+ " left join InvoiceDatMappingPoWithWoBO poWo on po.pkPurchaseOrderId = poWo.fkPurchaseOrderId"
			+ " WHERE po.purchaseNumber =:invoicePONumber ")
	public List<InvoiceDatPurchaseOrderBO> getAllInvoicePOByInvoicePONumber(@Param("invoicePONumber") String invoicePONumber);

	@Query("SELECT new com.thbs.mis.invoice.bo.InvoiceDatPurchaseOrderBO(po.pkPurchaseOrderId,po.commentsForUpdation,po.createdOn,po.poDeliveryDate," 
			+ " po.poDescription,po.poRemainingValue,po.poStatus,po.poType,po.poValue," 
			+ " po.purchaseNumber, po.surplusValue, po.updatedBy, po.updatedOn, poTx.withholdTaxAmount,"  
			+ " poWo.fkWorkorderId) from InvoiceDatPurchaseOrderBO po "
			+ " left join InvoiceDatPoLineItemsTaxDetailBO poTx on po.pkPurchaseOrderId = poTx.fkPurchaseOrderId"
			+ " left join InvoiceDatMappingPoWithWoBO poWo on po.pkPurchaseOrderId = poWo.fkPurchaseOrderId"
			+ " WHERE po.poPrefix LIKE %:countryCode% ")
	public List<InvoiceDatPurchaseOrderBO> getAllInvoicePOByCountryCode(@Param("countryCode") String countryCode);

	@Query("SELECT new com.thbs.mis.invoice.bo.InvoiceDatPurchaseOrderBO(po.pkPurchaseOrderId,po.commentsForUpdation,po.createdOn,po.poDeliveryDate," 
			+ " po.poDescription,po.poRemainingValue,po.poStatus,po.poType,po.poValue," 
			+ " po.purchaseNumber, po.surplusValue, po.updatedBy, po.updatedOn, poTx.withholdTaxAmount,"  
			+ " poWo.fkWorkorderId) from InvoiceDatPurchaseOrderBO po "
			+ " inner join InvoiceDatPoLineItemsTaxDetailBO poTx on po.pkPurchaseOrderId = poTx.fkPurchaseOrderId"
			+ " inner join InvoiceDatMappingPoWithWoBO poWo on po.pkPurchaseOrderId = poWo.fkPurchaseOrderId"
			+ " WHERE po.createdOn BETWEEN ?1 AND ?2 ")
	public List<InvoiceDatPurchaseOrderBO> getAllInvoicePOByStartAndEndDate(Date startDate , Date endDate);


	
	@Query(nativeQuery = true, value ="SELECT msclt.client_name from mas_client msclt "
			+ " inner join mas_projects proj on proj.fk_client_id = msclt.pk_client_id "
			+ " where proj.pk_project_id =(select fk_project_id "
			+ " from dat_workorder datWo where datWo.pk_workorder_id =?1) ")
	public String getCleintName(int fkWorkorderId);
	
	@Query("SELECT PO from InvoiceDatPurchaseOrderBO PO "
			+ " where PO.purchaseNumber =?1) ")
	public InvoiceDatPurchaseOrderBO getPurchaseOrderDetail(String InvoicePONumber);
	
	public InvoiceDatPurchaseOrderBO findByPurchaseNumber(String purchaseNumber);
	
	// EOA By Shashi

	// Added by Balaji
	@Query("SELECT po FROM InvoiceDatPurchaseOrderBO po "
			+ " JOIN MasClientAddressBO a on a.pkClientAddressId = po.clientBillToAddressId "
			+ " JOIN MasClientBO b on b.pkClientId = a.fkClientId "
			+ " WHERE b.pkClientId = ?1 ")
	public List<InvoiceDatPurchaseOrderBO> getPODetailsByClientId(Short clientId);
	
	//EOA by Balaji 
	
	
	//Added by Smrithi 
	
	public InvoiceDatPurchaseOrderBO findByPkPurchaseOrderId(Integer pkPurchaseOrderId);
	
	//EOA by Smrithi 
	/*// EOA By Shashi
	
	//Added by deepa
	public InvoiceDatPurchaseOrderBO findByPkPurchaseOrderId(Integer integer);*/

}
