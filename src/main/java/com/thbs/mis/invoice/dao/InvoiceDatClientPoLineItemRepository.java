package com.thbs.mis.invoice.dao;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.invoice.bo.InvoiceDatClientPoLineItemBO;
@Repository
public interface InvoiceDatClientPoLineItemRepository extends GenericRepository<InvoiceDatClientPoLineItemBO,Long>{

	public List<InvoiceDatClientPoLineItemBO> findByFkPurchaseOrderId(int fkPurchaseOrderId);
}
