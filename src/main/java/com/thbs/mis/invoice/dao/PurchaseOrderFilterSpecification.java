package com.thbs.mis.invoice.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.confirmation.bean.SearchDetailsInputBean;
import com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO;
import com.thbs.mis.invoice.bean.FilterPurchaseOrderInputBean;
import com.thbs.mis.invoice.bo.InvoiceDatPurchaseOrderBO;

public class PurchaseOrderFilterSpecification {
	

	/**
	 * 
	 * @param filterInputBean
	 * @return
	 */
	public static Specification<InvoiceDatPurchaseOrderBO> getPurchaseOrderStatusAndDateRange(
			FilterPurchaseOrderInputBean filterInputBean,List<InvoiceDatPurchaseOrderBO> invoiceDatPurchaseBOList ) {
		
		return new Specification<InvoiceDatPurchaseOrderBO>() {
			
			final Collection<Predicate> predicates = new ArrayList<>();
			
			
			@Override
			public Predicate toPredicate(Root<InvoiceDatPurchaseOrderBO> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				
				if(filterInputBean.getPoStatus() != null){
					System.out.println("Inside PO status not null ");
					final Predicate statusPredicate = cb.equal(
							root.get("poStatus"), filterInputBean.getPoStatus());
					predicates.add(statusPredicate);
				}
				
				if (filterInputBean.getPoStartDate() != null
						&& filterInputBean.getPoEndDate() != null) {
					System.out.println("Inside ddate range");
					final Predicate dateRangePredicate = cb.and(cb
							.greaterThanOrEqualTo(root.get("createdOn"),
									filterInputBean.getPoStartDate()), cb
							.lessThanOrEqualTo(root.get("createdOn"),
									filterInputBean.getPoEndDate()));
					predicates.add(dateRangePredicate);
				}
				if (!invoiceDatPurchaseBOList.isEmpty()) {
					List<Integer> poIdList = new ArrayList<Integer>();
					poIdList.addAll(invoiceDatPurchaseBOList.stream().map(InvoiceDatPurchaseOrderBO::getPkPurchaseOrderId)
							.collect(Collectors.toList()));
					final Predicate poIdPredicate = root.get(
							"pkPurchaseOrderId").in(poIdList);
					predicates.add(poIdPredicate);
				}
				return cb.and(predicates.toArray(new Predicate[predicates
				                       						.size()]));
				
			}
			
		};
	}
	
}
