package com.thbs.mis.invoice.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.invoice.bo.InvoiceDatMappingPoWithWoBO;
import com.thbs.mis.invoice.bo.InvoiceDatPurchaseOrderBO;
import com.thbs.mis.project.bo.DatMilestoneBO;
@Repository
public interface InvoiceDatMappingPoWithWoRepository extends GenericRepository<InvoiceDatMappingPoWithWoBO,Long> {

    @Query("Select fkWorkorderId from InvoiceDatMappingPoWithWoBO where fkPurchaseOrderId = ?")	
	public List<Integer> getwoId(int fkPurchaseOrderId);
    
	public List<InvoiceDatMappingPoWithWoBO> findByFkPurchaseOrderId(int fkPurchaseOrderId);
		
	public List<InvoiceDatMappingPoWithWoBO> findByFkPurchaseOrderIdAndFkWorkorderId(int fkPurchaseOrderId,int fkWorkorderId);
	
	/*//Added by deepa
	@Query(value = "select invoice.fkWorkorderId from InvoiceDatMappingPoWithWoBO invoice where invoice.fkPurchaseOrderId = :fkPurchaseOrderId")
	public List<Integer> getByPurchaseOrderId(@Param("fkPurchaseOrderId") int fkPurchaseOrderId);
	
	*/
    
	//Added by Smrithi
    public InvoiceDatMappingPoWithWoBO findByFkWorkorderIdAndFkPurchaseOrderId(Integer woId, Integer poId);
    
    public List<InvoiceDatMappingPoWithWoBO> findByFkWorkorderId(Integer woId);
    
    @Query("Select mapping from InvoiceDatMappingPoWithWoBO mapping "
    		+ " JOIN InvoiceDatPurchaseOrderBO po ON po.pkPurchaseOrderId = mapping.fkPurchaseOrderId "
    		+ " WHERE mapping.fkPurchaseOrderId = ? AND po.poType = 'AGGREGATE' ")	
    public List<InvoiceDatMappingPoWithWoBO> getMappingDetails (Integer poId);
    
    public List<InvoiceDatMappingPoWithWoBO> findByFkPurchaseOrderId(Integer poId);
    
    //EOA by Smrithi
    
}
