package com.thbs.mis.invoice.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.invoice.bo.InvoiceDatInvoiceUniqueNumberBO;
@Repository
public interface InvoiceDatInvoiceUniqueNumberRepository extends GenericRepository<InvoiceDatInvoiceUniqueNumberBO,Long> {

}
