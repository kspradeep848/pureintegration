package com.thbs.mis.invoice.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.invoice.bo.InvoiceDatInvoiceDetailsAuditBO;

@Repository
public interface InvoiceDatInvoiceDetailsAuditRepository extends GenericRepository<InvoiceDatInvoiceDetailsAuditBO,Long> {

}
