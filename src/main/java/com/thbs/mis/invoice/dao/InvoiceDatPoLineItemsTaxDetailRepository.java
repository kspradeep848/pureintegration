package com.thbs.mis.invoice.dao;

import java.util.List;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.invoice.bo.InvoiceDatPoLineItemsTaxDetailBO;

public interface InvoiceDatPoLineItemsTaxDetailRepository extends GenericRepository<InvoiceDatPoLineItemsTaxDetailBO,Long>{

	//Added by shashi
	
	public List<InvoiceDatPoLineItemsTaxDetailBO> findByFkPurchaseOrderId(int fkPurchaseOrderId);
	
	//EOA by sahshi
	
}
