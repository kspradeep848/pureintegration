package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the invoice_dat_credit_notes_details_audit database table.
 * 
 */
@Entity
@Table(name="invoice_dat_credit_notes_details_audit")
@NamedQuery(name="InvoiceDatCreditNotesDetailsAuditBO.findAll", query="SELECT i FROM InvoiceDatCreditNotesDetailsAuditBO i")
public class InvoiceDatCreditNotesDetailsAuditBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_credit_notes_audit_details_id")
	private int pkCreditNotesAuditDetailsId;

	private String action;

	@Column(name="comments_for_updation")
	private String commentsForUpdation;

	@Column(name="credit_note_amount")
	private BigDecimal creditNoteAmount;

	@Column(name="credit_note_comments")
	private String creditNoteComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="credit_note_date")
	private Date creditNoteDate;

	@Column(name="credit_note_number")
	private String creditNoteNumber;

	@Column(name="entered_by")
	private int enteredBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="entered_on")
	private Date enteredOn;

	//bi-directional one-to-one association to InvoiceDatCreditNotesDetail
	@OneToOne
	@JoinColumn(name="fk_credit_notes_details_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceDatCreditNotesDetailBO invoiceDatCreditNotesDetail;

	//bi-directional one-to-one association to InvoiceDatInvoiceDetail
	@OneToOne
	@JoinColumn(name="fk_invoice_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceDatInvoiceDetailBO invoiceDatInvoiceDetail;

	public InvoiceDatCreditNotesDetailsAuditBO() {
	}

	public int getPkCreditNotesAuditDetailsId() {
		return this.pkCreditNotesAuditDetailsId;
	}

	public void setPkCreditNotesAuditDetailsId(int pkCreditNotesAuditDetailsId) {
		this.pkCreditNotesAuditDetailsId = pkCreditNotesAuditDetailsId;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCommentsForUpdation() {
		return this.commentsForUpdation;
	}

	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}

	public BigDecimal getCreditNoteAmount() {
		return this.creditNoteAmount;
	}

	public void setCreditNoteAmount(BigDecimal creditNoteAmount) {
		this.creditNoteAmount = creditNoteAmount;
	}

	public String getCreditNoteComments() {
		return this.creditNoteComments;
	}

	public void setCreditNoteComments(String creditNoteComments) {
		this.creditNoteComments = creditNoteComments;
	}

	public Date getCreditNoteDate() {
		return this.creditNoteDate;
	}

	public void setCreditNoteDate(Date creditNoteDate) {
		this.creditNoteDate = creditNoteDate;
	}

	public String getCreditNoteNumber() {
		return this.creditNoteNumber;
	}

	public void setCreditNoteNumber(String creditNoteNumber) {
		this.creditNoteNumber = creditNoteNumber;
	}

	public int getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(int enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredOn() {
		return this.enteredOn;
	}

	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}

	public InvoiceDatCreditNotesDetailBO getInvoiceDatCreditNotesDetail() {
		return this.invoiceDatCreditNotesDetail;
	}

	public void setInvoiceDatCreditNotesDetail(InvoiceDatCreditNotesDetailBO invoiceDatCreditNotesDetail) {
		this.invoiceDatCreditNotesDetail = invoiceDatCreditNotesDetail;
	}

	public InvoiceDatInvoiceDetailBO getInvoiceDatInvoiceDetail() {
		return this.invoiceDatInvoiceDetail;
	}

	public void setInvoiceDatInvoiceDetail(InvoiceDatInvoiceDetailBO invoiceDatInvoiceDetail) {
		this.invoiceDatInvoiceDetail = invoiceDatInvoiceDetail;
	}

	@Override
	public String toString() {
		return "InvoiceDatCreditNotesDetailsAuditBO [pkCreditNotesAuditDetailsId=" + pkCreditNotesAuditDetailsId
				+ ", action=" + action + ", commentsForUpdation=" + commentsForUpdation + ", creditNoteAmount="
				+ creditNoteAmount + ", creditNoteComments=" + creditNoteComments + ", creditNoteDate=" + creditNoteDate
				+ ", creditNoteNumber=" + creditNoteNumber + ", enteredBy=" + enteredBy + ", enteredOn=" + enteredOn
				+ ", invoiceDatCreditNotesDetail=" + invoiceDatCreditNotesDetail + ", invoiceDatInvoiceDetail="
				+ invoiceDatInvoiceDetail + "]";
	}

}