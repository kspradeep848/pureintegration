package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;

import com.thbs.mis.project.bo.DatWorkorderBO;


/**
 * The persistent class for the invoice_dat_mapping_po_with_wo database table.
 * 
 */
@Entity
@Table(name="invoice_dat_mapping_po_with_wo")
@NamedQuery(name="InvoiceDatMappingPoWithWoBO.findAll", query="SELECT i FROM InvoiceDatMappingPoWithWoBO i")
public class InvoiceDatMappingPoWithWoBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_dat_mapping_po_with_wo_id")
	private int pkDatMappingPoWithWoId;

	@Column(name="fk_workorder_id")
	private int fkWorkorderId;
	
	@Column(name="fk_purchase_order_id")
	private int fkPurchaseOrderId;

	//bi-directional one-to-one association to InvoiceDatPurchaseOrder
	@OneToOne
	@JoinColumn(name="fk_purchase_order_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrder;

	@OneToOne
	@JoinColumn(name="fk_workorder_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatWorkorderBO datWorkorderBOObject;

	public InvoiceDatMappingPoWithWoBO() {
	}

	public int getPkDatMappingPoWithWoId() {
		return this.pkDatMappingPoWithWoId;
	}

	public void setPkDatMappingPoWithWoId(int pkDatMappingPoWithWoId) {
		this.pkDatMappingPoWithWoId = pkDatMappingPoWithWoId;
	}

	public int getFkWorkorderId() {
		return this.fkWorkorderId;
	}

	public void setFkWorkorderId(int fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}

	public InvoiceDatPurchaseOrderBO getInvoiceDatPurchaseOrder() {
		return this.invoiceDatPurchaseOrder;
	}

	public void setInvoiceDatPurchaseOrder(InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrder) {
		this.invoiceDatPurchaseOrder = invoiceDatPurchaseOrder;
	}

	public int getFkPurchaseOrderId() {
		return fkPurchaseOrderId;
	}

	public void setFkPurchaseOrderId(int FkPurchaseOrderId) {
		this.fkPurchaseOrderId = FkPurchaseOrderId;
	}

	public DatWorkorderBO getDatWorkorderBOObject() {
		return datWorkorderBOObject;
	}

	public void setDatWorkorderBOObject(DatWorkorderBO datWorkorderBOObject) {
		this.datWorkorderBOObject = datWorkorderBOObject;
	}

	@Override
	public String toString() {
		return "InvoiceDatMappingPoWithWoBO [pkDatMappingPoWithWoId=" + pkDatMappingPoWithWoId + ", fkWorkorderId="
				+ fkWorkorderId + ", fkPurchaseOrderId=" + fkPurchaseOrderId + ", invoiceDatPurchaseOrder="
				+ invoiceDatPurchaseOrder + ", datWorkorderBOObject=" + datWorkorderBOObject + "]";
	}

}