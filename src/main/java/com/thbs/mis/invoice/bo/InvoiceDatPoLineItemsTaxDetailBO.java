package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasCurrencytypeBO;


/**
 * The persistent class for the invoice_dat_po_line_items_tax_details database table.
 * 
 */
@Entity
@Table(name="invoice_dat_po_line_items_tax_details")
@NamedQuery(name="InvoiceDatPoLineItemsTaxDetailBO.findAll", query="SELECT i FROM InvoiceDatPoLineItemsTaxDetailBO i")
public class InvoiceDatPoLineItemsTaxDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_dat_po_line_items_tax_details_id")
	private Integer pkDatPoLineItemsTaxDetailsId;

	@Column(name="created_by")
	private Integer createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="fk_purchase_order_id")
	private Integer fkPurchaseOrderId;
	
	@Column(name="fk_currency_id")
	private Short fkCurrencyId;

	@Column(name="grand_total_amount")
	private Double grandTotalAmount;

	@Column(name="tax_amount")
	private Double taxAmount;

	@Column(name="total_amount_of_line_items")
	private Double totalAmountOfLineItems;

	@Column(name="updated_by")
	private Integer updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;

	@Column(name="updation_comment")
	private String updationComment;

	@Column(name="withhold_tax_amount")
	private Double withholdTaxAmount;

	//bi-directional one-to-one association to InvoiceDatPurchaseOrder
	@OneToOne
	@JoinColumn(name="fk_purchase_order_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrder;

	@OneToOne
	@JoinColumn(name="fk_currency_id",unique = true, nullable = true, insertable = false, updatable = false)
	private MasCurrencytypeBO masCurrencytypeBO;
	
	@OneToOne
	@JoinColumn(name="created_by",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOCreatedBy;
	
	@OneToOne
	@JoinColumn(name="updated_by",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOUpdatedBy;
	
	
	public InvoiceDatPoLineItemsTaxDetailBO() {
	}

	public Integer getPkDatPoLineItemsTaxDetailsId() {
		return pkDatPoLineItemsTaxDetailsId;
	}

	public void setPkDatPoLineItemsTaxDetailsId(Integer pkDatPoLineItemsTaxDetailsId) {
		this.pkDatPoLineItemsTaxDetailsId = pkDatPoLineItemsTaxDetailsId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getFkPurchaseOrderId() {
		return fkPurchaseOrderId;
	}

	public void setFkPurchaseOrderId(Integer fkPurchaseOrderId) {
		this.fkPurchaseOrderId = fkPurchaseOrderId;
	}

	public Short getFkCurrencyId() {
		return fkCurrencyId;
	}

	public void setFkCurrencyId(Short fkCurrencyId) {
		this.fkCurrencyId = fkCurrencyId;
	}

	public Double getGrandTotalAmount() {
		return grandTotalAmount;
	}

	public void setGrandTotalAmount(Double grandTotalAmount) {
		this.grandTotalAmount = grandTotalAmount;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Double getTotalAmountOfLineItems() {
		return totalAmountOfLineItems;
	}

	public void setTotalAmountOfLineItems(Double totalAmountOfLineItems) {
		this.totalAmountOfLineItems = totalAmountOfLineItems;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdationComment() {
		return updationComment;
	}

	public void setUpdationComment(String updationComment) {
		this.updationComment = updationComment;
	}

	public Double getWithholdTaxAmount() {
		return withholdTaxAmount;
	}

	public void setWithholdTaxAmount(Double withholdTaxAmount) {
		this.withholdTaxAmount = withholdTaxAmount;
	}

	public InvoiceDatPurchaseOrderBO getInvoiceDatPurchaseOrder() {
		return invoiceDatPurchaseOrder;
	}

	public void setInvoiceDatPurchaseOrder(InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrder) {
		this.invoiceDatPurchaseOrder = invoiceDatPurchaseOrder;
	}

	public MasCurrencytypeBO getMasCurrencytypeBO() {
		return masCurrencytypeBO;
	}

	public void setMasCurrencytypeBO(MasCurrencytypeBO masCurrencytypeBO) {
		this.masCurrencytypeBO = masCurrencytypeBO;
	}

	public DatEmpDetailBO getDatEmpDetailBOCreatedBy() {
		return datEmpDetailBOCreatedBy;
	}

	public void setDatEmpDetailBOCreatedBy(DatEmpDetailBO datEmpDetailBOCreatedBy) {
		this.datEmpDetailBOCreatedBy = datEmpDetailBOCreatedBy;
	}

	public DatEmpDetailBO getDatEmpDetailBOUpdatedBy() {
		return datEmpDetailBOUpdatedBy;
	}

	public void setDatEmpDetailBOUpdatedBy(DatEmpDetailBO datEmpDetailBOUpdatedBy) {
		this.datEmpDetailBOUpdatedBy = datEmpDetailBOUpdatedBy;
	}

	@Override
	public String toString() {
		return "InvoiceDatPoLineItemsTaxDetailBO [pkDatPoLineItemsTaxDetailsId=" + pkDatPoLineItemsTaxDetailsId
				+ ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", fkPurchaseOrderId=" + fkPurchaseOrderId
				+ ", fkCurrencyId=" + fkCurrencyId + ", grandTotalAmount=" + grandTotalAmount + ", taxAmount="
				+ taxAmount + ", totalAmountOfLineItems=" + totalAmountOfLineItems + ", updatedBy=" + updatedBy
				+ ", updatedOn=" + updatedOn + ", updationComment=" + updationComment + ", withholdTaxAmount="
				+ withholdTaxAmount + ", invoiceDatPurchaseOrder=" + invoiceDatPurchaseOrder + ", masCurrencytypeBO="
				+ masCurrencytypeBO + ", datEmpDetailBOCreatedBy=" + datEmpDetailBOCreatedBy
				+ ", datEmpDetailBOUpdatedBy=" + datEmpDetailBOUpdatedBy + "]";
	}

	
	
}