package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the invoice_dat_invoice_auto_increment_series database table.
 * 
 */
@Entity
@Table(name="invoice_dat_invoice_auto_increment_series")
@NamedQuery(name="InvoiceDatInvoiceAutoIncrementSeryBO.findAll", query="SELECT i FROM InvoiceDatInvoiceAutoIncrementSeryBO i")
public class InvoiceDatInvoiceAutoIncrementSeryBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_invoice_auto_increment_id")
	private byte pkInvoiceAutoIncrementId;

	@Column(name="fk_business_entity_id")
	private byte fkBusinessEntityId;

	@Column(name="next_invoice_number")
	private String nextInvoiceNumber;

	public InvoiceDatInvoiceAutoIncrementSeryBO() {
	}

	public byte getPkInvoiceAutoIncrementId() {
		return this.pkInvoiceAutoIncrementId;
	}

	public void setPkInvoiceAutoIncrementId(byte pkInvoiceAutoIncrementId) {
		this.pkInvoiceAutoIncrementId = pkInvoiceAutoIncrementId;
	}

	public byte getFkBusinessEntityId() {
		return this.fkBusinessEntityId;
	}

	public void setFkBusinessEntityId(byte fkBusinessEntityId) {
		this.fkBusinessEntityId = fkBusinessEntityId;
	}

	public String getNextInvoiceNumber() {
		return this.nextInvoiceNumber;
	}

	public void setNextInvoiceNumber(String nextInvoiceNumber) {
		this.nextInvoiceNumber = nextInvoiceNumber;
	}

	@Override
	public String toString() {
		return "InvoiceDatInvoiceAutoIncrementSeryBO [pkInvoiceAutoIncrementId=" + pkInvoiceAutoIncrementId
				+ ", fkBusinessEntityId=" + fkBusinessEntityId + ", nextInvoiceNumber=" + nextInvoiceNumber + "]";
	}

}