package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the invoice_mas_submission_mode database table.
 * 
 */
@Entity
@Table(name="invoice_mas_submission_mode")
@NamedQuery(name="InvoiceMasSubmissionModeBO.findAll", query="SELECT i FROM InvoiceMasSubmissionModeBO i")
public class InvoiceMasSubmissionModeBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_submission_mode_id")
	private byte pkSubmissionModeId;

	@Column(name="submission_mode_name")
	private String submissionModeName;

	public InvoiceMasSubmissionModeBO() {
	}

	public byte getPkSubmissionModeId() {
		return this.pkSubmissionModeId;
	}

	public void setPkSubmissionModeId(byte pkSubmissionModeId) {
		this.pkSubmissionModeId = pkSubmissionModeId;
	}

	public String getSubmissionModeName() {
		return this.submissionModeName;
	}

	public void setSubmissionModeName(String submissionModeName) {
		this.submissionModeName = submissionModeName;
	}

	@Override
	public String toString() {
		return "InvoiceMasSubmissionModeBO [pkSubmissionModeId=" + pkSubmissionModeId + ", submissionModeName="
				+ submissionModeName + "]";
	}

}