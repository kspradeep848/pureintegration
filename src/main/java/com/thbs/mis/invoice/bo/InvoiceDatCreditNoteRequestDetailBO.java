package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the invoice_dat_credit_note_request_details database table.
 * 
 */
@Entity
@Table(name="invoice_dat_credit_note_request_details")
@NamedQuery(name="InvoiceDatCreditNoteRequestDetailBO.findAll", query="SELECT i FROM InvoiceDatCreditNoteRequestDetailBO i")
public class InvoiceDatCreditNoteRequestDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_credit_note_request_id")
	private int pkCreditNoteRequestId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="approved_on")
	private Date approvedOn;

	@Column(name="fk_approved_by")
	private int fkApprovedBy;

	@Column(name="fk_requested_by")
	private int fkRequestedBy;

	@Column(name="request_status")
	private String requestStatus;

	@Column(name="requested_comments")
	private String requestedComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="requested_on")
	private Date requestedOn;

	//bi-directional one-to-one association to InvoiceDatInvoiceDetail
	@OneToOne
	@JoinColumn(name="fk_invoice_details_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceDatInvoiceDetailBO invoiceDatInvoiceDetail;

	

	public InvoiceDatCreditNoteRequestDetailBO() {
	}



	public int getPkCreditNoteRequestId() {
		return pkCreditNoteRequestId;
	}



	public void setPkCreditNoteRequestId(int pkCreditNoteRequestId) {
		this.pkCreditNoteRequestId = pkCreditNoteRequestId;
	}



	public Date getApprovedOn() {
		return approvedOn;
	}



	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}



	public int getFkApprovedBy() {
		return fkApprovedBy;
	}



	public void setFkApprovedBy(int fkApprovedBy) {
		this.fkApprovedBy = fkApprovedBy;
	}



	public int getFkRequestedBy() {
		return fkRequestedBy;
	}



	public void setFkRequestedBy(int fkRequestedBy) {
		this.fkRequestedBy = fkRequestedBy;
	}



	public String getRequestStatus() {
		return requestStatus;
	}



	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}



	public String getRequestedComments() {
		return requestedComments;
	}



	public void setRequestedComments(String requestedComments) {
		this.requestedComments = requestedComments;
	}



	public Date getRequestedOn() {
		return requestedOn;
	}



	public void setRequestedOn(Date requestedOn) {
		this.requestedOn = requestedOn;
	}



	public InvoiceDatInvoiceDetailBO getInvoiceDatInvoiceDetail() {
		return invoiceDatInvoiceDetail;
	}



	public void setInvoiceDatInvoiceDetail(InvoiceDatInvoiceDetailBO invoiceDatInvoiceDetail) {
		this.invoiceDatInvoiceDetail = invoiceDatInvoiceDetail;
	}



	@Override
	public String toString() {
		return "InvoiceDatCreditNoteRequestDetailBO [pkCreditNoteRequestId=" + pkCreditNoteRequestId + ", approvedOn="
				+ approvedOn + ", fkApprovedBy=" + fkApprovedBy + ", fkRequestedBy=" + fkRequestedBy
				+ ", requestStatus=" + requestStatus + ", requestedComments=" + requestedComments + ", requestedOn="
				+ requestedOn + ", invoiceDatInvoiceDetail=" + invoiceDatInvoiceDetail + "]";
	}

	

}