package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the invoice_dat_credit_note_unique_number database table.
 * 
 */
@Entity
@Table(name="invoice_dat_credit_note_unique_number")
@NamedQuery(name="InvoiceDatCreditNoteUniqueNumberBO.findAll", query="SELECT i FROM InvoiceDatCreditNoteUniqueNumberBO i")
public class InvoiceDatCreditNoteUniqueNumberBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_credit_note_unique_number_id")
	private byte pkCreditNoteUniqueNumberId;

	@Column(name="credit_note_unique_number")
	private String creditNoteUniqueNumber;

	public InvoiceDatCreditNoteUniqueNumberBO() {
	}

	public byte getPkCreditNoteUniqueNumberId() {
		return this.pkCreditNoteUniqueNumberId;
	}

	public void setPkCreditNoteUniqueNumberId(byte pkCreditNoteUniqueNumberId) {
		this.pkCreditNoteUniqueNumberId = pkCreditNoteUniqueNumberId;
	}

	public String getCreditNoteUniqueNumber() {
		return this.creditNoteUniqueNumber;
	}

	public void setCreditNoteUniqueNumber(String creditNoteUniqueNumber) {
		this.creditNoteUniqueNumber = creditNoteUniqueNumber;
	}

	@Override
	public String toString() {
		return "InvoiceDatCreditNoteUniqueNumberBO [pkCreditNoteUniqueNumberId=" + pkCreditNoteUniqueNumberId
				+ ", creditNoteUniqueNumber=" + creditNoteUniqueNumber + "]";
	}

}