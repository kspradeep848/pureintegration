package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasBankPaymentDetailBO;
import com.thbs.mis.common.bo.MasCurrencytypeBO;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the invoice_dat_payment_received database table.
 * 
 */
@Entity
@Table(name="invoice_dat_payment_received")
@NamedQuery(name="InvoiceDatPaymentReceivedBO.findAll", query="SELECT i FROM InvoiceDatPaymentReceivedBO i")
public class InvoiceDatPaymentReceivedBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_payment_received_id")
	private int pkPaymentReceivedId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="entered_on")
	private Date enteredOn;

	@Column(name="firc_number")
	private String fircNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="firc_number_entered_on")
	private Date fircNumberEnteredOn;

	@Column(name="received_amount")
	private BigDecimal receivedAmount;

	@Temporal(TemporalType.DATE)
	@Column(name="received_date")
	private Date receivedDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;

	@Column(name="updation_comment")
	private String updationComment;

	//bi-directional many-to-one association to MasCurrencytype
	@OneToOne
	@JoinColumn(name="fk_currency_id",unique = true, nullable = true, insertable = false, updatable = false)
	private MasCurrencytypeBO masCurrencytype;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="entered_by",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail1;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="firc_number_entered_by",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail2;

	//bi-directional many-to-one association to InvoiceDatInvoiceDetail
	@OneToOne
	@JoinColumn(name="fk_invoice_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceDatInvoiceDetailBO invoiceDatInvoiceDetail;

	//bi-directional many-to-one association to InvoiceDatPaymentBatchCodeDetail
	@OneToOne
	@JoinColumn(name="fk_payment_batch_code_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceDatPaymentBatchCodeDetailBO invoiceDatPaymentBatchCodeDetail;

	//bi-directional many-to-one association to MasBankPaymentDetail
	@OneToOne
	@JoinColumn(name="fk_payment_received_bank_id",unique = true, nullable = true, insertable = false, updatable = false)
	private MasBankPaymentDetailBO masBankPaymentDetail;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="updated_by")
	private DatEmpDetailBO datEmpDetail3;

	public InvoiceDatPaymentReceivedBO() {
	}

	public int getPkPaymentReceivedId() {
		return this.pkPaymentReceivedId;
	}

	public void setPkPaymentReceivedId(int pkPaymentReceivedId) {
		this.pkPaymentReceivedId = pkPaymentReceivedId;
	}

	public Date getEnteredOn() {
		return this.enteredOn;
	}

	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}

	public String getFircNumber() {
		return this.fircNumber;
	}

	public void setFircNumber(String fircNumber) {
		this.fircNumber = fircNumber;
	}

	public Date getFircNumberEnteredOn() {
		return this.fircNumberEnteredOn;
	}

	public void setFircNumberEnteredOn(Date fircNumberEnteredOn) {
		this.fircNumberEnteredOn = fircNumberEnteredOn;
	}

	public BigDecimal getReceivedAmount() {
		return this.receivedAmount;
	}

	public void setReceivedAmount(BigDecimal receivedAmount) {
		this.receivedAmount = receivedAmount;
	}

	public Date getReceivedDate() {
		return this.receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdationComment() {
		return this.updationComment;
	}

	public void setUpdationComment(String updationComment) {
		this.updationComment = updationComment;
	}

	public MasCurrencytypeBO getMasCurrencytype() {
		return masCurrencytype;
	}

	public void setMasCurrencytype(MasCurrencytypeBO masCurrencytype) {
		this.masCurrencytype = masCurrencytype;
	}

	public DatEmpDetailBO getDatEmpDetail1() {
		return datEmpDetail1;
	}

	public void setDatEmpDetail1(DatEmpDetailBO datEmpDetail1) {
		this.datEmpDetail1 = datEmpDetail1;
	}

	public DatEmpDetailBO getDatEmpDetail2() {
		return datEmpDetail2;
	}

	public void setDatEmpDetail2(DatEmpDetailBO datEmpDetail2) {
		this.datEmpDetail2 = datEmpDetail2;
	}

	public InvoiceDatInvoiceDetailBO getInvoiceDatInvoiceDetail() {
		return invoiceDatInvoiceDetail;
	}

	public void setInvoiceDatInvoiceDetail(InvoiceDatInvoiceDetailBO invoiceDatInvoiceDetail) {
		this.invoiceDatInvoiceDetail = invoiceDatInvoiceDetail;
	}

	public InvoiceDatPaymentBatchCodeDetailBO getInvoiceDatPaymentBatchCodeDetail() {
		return invoiceDatPaymentBatchCodeDetail;
	}

	public void setInvoiceDatPaymentBatchCodeDetail(InvoiceDatPaymentBatchCodeDetailBO invoiceDatPaymentBatchCodeDetail) {
		this.invoiceDatPaymentBatchCodeDetail = invoiceDatPaymentBatchCodeDetail;
	}

	public MasBankPaymentDetailBO getMasBankPaymentDetail() {
		return masBankPaymentDetail;
	}

	public void setMasBankPaymentDetail(MasBankPaymentDetailBO masBankPaymentDetail) {
		this.masBankPaymentDetail = masBankPaymentDetail;
	}

	public DatEmpDetailBO getDatEmpDetail3() {
		return datEmpDetail3;
	}

	public void setDatEmpDetail3(DatEmpDetailBO datEmpDetail3) {
		this.datEmpDetail3 = datEmpDetail3;
	}

	@Override
	public String toString() {
		return "InvoiceDatPaymentReceived [pkPaymentReceivedId=" + pkPaymentReceivedId + ", enteredOn=" + enteredOn
				+ ", fircNumber=" + fircNumber + ", fircNumberEnteredOn=" + fircNumberEnteredOn + ", receivedAmount="
				+ receivedAmount + ", receivedDate=" + receivedDate + ", updatedOn=" + updatedOn + ", updationComment="
				+ updationComment + ", masCurrencytype=" + masCurrencytype + ", datEmpDetail1=" + datEmpDetail1
				+ ", datEmpDetail2=" + datEmpDetail2 + ", invoiceDatInvoiceDetail=" + invoiceDatInvoiceDetail
				+ ", masBankPaymentDetail=" + masBankPaymentDetail + ", datEmpDetail3=" + datEmpDetail3 + "]";
	}

	

}