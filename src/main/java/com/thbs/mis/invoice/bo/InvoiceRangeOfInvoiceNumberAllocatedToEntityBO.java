package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the invoice_range_of_invoice_number_allocated_to_entity database table.
 * 
 */
@Entity
@Table(name="invoice_range_of_invoice_number_allocated_to_entity")
@NamedQuery(name="InvoiceRangeOfInvoiceNumberAllocatedToEntityBO.findAll", query="SELECT i FROM InvoiceRangeOfInvoiceNumberAllocatedToEntityBO i")
public class InvoiceRangeOfInvoiceNumberAllocatedToEntityBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_range_of_invoice_number_allocated_to_entity_id")
	private short pkRangeOfInvoiceNumberAllocatedToEntityId;

	@Column(name="first_invoice_number")
	private int firstInvoiceNumber;

	@Column(name="fk_business_entity_id")
	private byte fkBusinessEntityId;

	@Column(name="is_active")
	private String isActive;

	@Column(name="last_invoice_number")
	private int lastInvoiceNumber;

	public InvoiceRangeOfInvoiceNumberAllocatedToEntityBO() {
	}

	public short getPkRangeOfInvoiceNumberAllocatedToEntityId() {
		return this.pkRangeOfInvoiceNumberAllocatedToEntityId;
	}

	public void setPkRangeOfInvoiceNumberAllocatedToEntityId(short pkRangeOfInvoiceNumberAllocatedToEntityId) {
		this.pkRangeOfInvoiceNumberAllocatedToEntityId = pkRangeOfInvoiceNumberAllocatedToEntityId;
	}

	public int getFirstInvoiceNumber() {
		return this.firstInvoiceNumber;
	}

	public void setFirstInvoiceNumber(int firstInvoiceNumber) {
		this.firstInvoiceNumber = firstInvoiceNumber;
	}

	public byte getFkBusinessEntityId() {
		return this.fkBusinessEntityId;
	}

	public void setFkBusinessEntityId(byte fkBusinessEntityId) {
		this.fkBusinessEntityId = fkBusinessEntityId;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public int getLastInvoiceNumber() {
		return this.lastInvoiceNumber;
	}

	public void setLastInvoiceNumber(int lastInvoiceNumber) {
		this.lastInvoiceNumber = lastInvoiceNumber;
	}

	@Override
	public String toString() {
		return "InvoiceRangeOfInvoiceNumberAllocatedToEntityBO [pkRangeOfInvoiceNumberAllocatedToEntityId="
				+ pkRangeOfInvoiceNumberAllocatedToEntityId + ", firstInvoiceNumber=" + firstInvoiceNumber
				+ ", fkBusinessEntityId=" + fkBusinessEntityId + ", isActive=" + isActive + ", lastInvoiceNumber="
				+ lastInvoiceNumber + "]";
	}

}