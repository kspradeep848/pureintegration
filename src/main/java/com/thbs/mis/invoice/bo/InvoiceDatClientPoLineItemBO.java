package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;

import java.util.Date;


/**
 * The persistent class for the invoice_dat_client_po_line_items database table.
 * 
 */
@Entity
@Table(name="invoice_dat_client_po_line_items")
@NamedQuery(name="InvoiceDatClientPoLineItemBO.findAll", query="SELECT i FROM InvoiceDatClientPoLineItemBO i")
public class InvoiceDatClientPoLineItemBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_client_po_line_items_id")
	private int pkClientPoLineItemsId;

	@Column(name="fk_purchase_order_id")
	private int fkPurchaseOrderId;
	
	@Column(name="amount")
	private double amount;

	@Column(name="line_item_description")
	private String lineItemDescription;

	@Column(name="line_item_number")
	private String lineItemNumber;

	@Column(name="quantity")
	private double quantity;

	@Column(name="unit_cost")
	private double unitCost;

	@Column(name="unit_of_measure")
	private String unitOfMeasure;

	@Column(name="updated_by")
	private int updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;

	//bi-directional one-to-one association to InvoiceDatPurchaseOrder
	@OneToOne
	@JoinColumn(name="fk_purchase_order_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrder;

	@OneToOne
	@JoinColumn(name="updated_by",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOUpdatedBy;
	
	public InvoiceDatClientPoLineItemBO() {
	}

	public int getPkClientPoLineItemsId() {
		return this.pkClientPoLineItemsId;
	}

	public void setPkClientPoLineItemsId(int pkClientPoLineItemsId) {
		this.pkClientPoLineItemsId = pkClientPoLineItemsId;
	}

	
	public int getFkPurchaseOrderId() {
		return fkPurchaseOrderId;
	}

	public void setFkPurchaseOrderId(int fkPurchaseOrderId) {
		this.fkPurchaseOrderId = fkPurchaseOrderId;
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getLineItemDescription() {
		return this.lineItemDescription;
	}

	public void setLineItemDescription(String lineItemDescription) {
		this.lineItemDescription = lineItemDescription;
	}

	public String getLineItemNumber() {
		return this.lineItemNumber;
	}

	public void setLineItemNumber(String lineItemNumber) {
		this.lineItemNumber = lineItemNumber;
	}

	public double getQuantity() {
		return this.quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getUnitCost() {
		return this.unitCost;
	}

	public void setUnitCost(double unitCost) {
		this.unitCost = unitCost;
	}

	public String getUnitOfMeasure() {
		return this.unitOfMeasure;
	}

	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}

	public int getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public InvoiceDatPurchaseOrderBO getInvoiceDatPurchaseOrder() {
		return this.invoiceDatPurchaseOrder;
	}

	public void setInvoiceDatPurchaseOrder(InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrder) {
		this.invoiceDatPurchaseOrder = invoiceDatPurchaseOrder;
	}

	public DatEmpDetailBO getDatEmpDetailBOUpdatedBy() {
		return datEmpDetailBOUpdatedBy;
	}

	public void setDatEmpDetailBOUpdatedBy(DatEmpDetailBO datEmpDetailBOUpdatedBy) {
		this.datEmpDetailBOUpdatedBy = datEmpDetailBOUpdatedBy;
	}

	@Override
	public String toString() {
		return "InvoiceDatClientPoLineItemBO [pkClientPoLineItemsId=" + pkClientPoLineItemsId + ", fkPurchaseOrderId="
				+ fkPurchaseOrderId + ", amount=" + amount + ", lineItemDescription=" + lineItemDescription
				+ ", lineItemNumber=" + lineItemNumber + ", quantity=" + quantity + ", unitCost=" + unitCost
				+ ", unitOfMeasure=" + unitOfMeasure + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn
				+ ", invoiceDatPurchaseOrder=" + invoiceDatPurchaseOrder + ", datEmpDetailBOUpdatedBy="
				+ datEmpDetailBOUpdatedBy + "]";
	}

}