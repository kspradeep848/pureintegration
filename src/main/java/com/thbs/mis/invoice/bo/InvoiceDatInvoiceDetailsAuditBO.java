package com.thbs.mis.invoice.bo;


import java.io.Serializable;
import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasBusinessEntityBO;
import com.thbs.mis.project.bo.DatComponentBO;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the invoice_dat_invoice_details_audit database table.
 * 
 */
@Entity
@Table(name="invoice_dat_invoice_details_audit")
@NamedQuery(name="InvoiceDatInvoiceDetailsAuditBO.findAll", query="SELECT i FROM InvoiceDatInvoiceDetailsAuditBO i")
public class InvoiceDatInvoiceDetailsAuditBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_invoice_details_history_id")
	private int pkInvoiceDetailsHistoryId;

	@Column(name="action")
	private String action;
	
	@Column(name="comments")
	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="entered_on")
	private Date enteredOn;

	@Column(name="invoice_amount")
	private BigDecimal invoiceAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="invoice_date")
	private Date invoiceDate;

	@Column(name="invoice_number")
	private String invoiceNumber;

	@Column(name="is_payment_received_status")
	private String isPaymentReceivedStatus;

	@Column(name="updation_comments")
	private String updationComments;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="entered_by",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail;

	//bi-directional many-to-one association to MasBusinessEntity
	@OneToOne
	@JoinColumn(name="fk_business_entity_id",unique = true, nullable = true, insertable = false, updatable = false)
	private MasBusinessEntityBO masBusinessEntity;

	//bi-directional many-to-one association to DatComponent
	@OneToOne
	@JoinColumn(name="fk_component_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatComponentBO datComponent;

	//bi-directional many-to-one association to InvoiceDatInvoiceDetail
	@OneToOne
	@JoinColumn(name="fk_invoice_details_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceDatInvoiceDetailBO invoiceDatInvoiceDetail;

	//bi-directional many-to-one association to InvoiceDatPurchaseOrder
	@OneToOne
	@JoinColumn(name="fk_purchase_order_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrder;

	public InvoiceDatInvoiceDetailsAuditBO() {
	}

	public int getPkInvoiceDetailsHistoryId() {
		return this.pkInvoiceDetailsHistoryId;
	}

	public void setPkInvoiceDetailsHistoryId(int pkInvoiceDetailsHistoryId) {
		this.pkInvoiceDetailsHistoryId = pkInvoiceDetailsHistoryId;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getEnteredOn() {
		return this.enteredOn;
	}

	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}

	public BigDecimal getInvoiceAmount() {
		return this.invoiceAmount;
	}

	public void setInvoiceAmount(BigDecimal invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceNumber() {
		return this.invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getIsPaymentReceivedStatus() {
		return this.isPaymentReceivedStatus;
	}

	public void setIsPaymentReceivedStatus(String isPaymentReceivedStatus) {
		this.isPaymentReceivedStatus = isPaymentReceivedStatus;
	}

	public String getUpdationComments() {
		return this.updationComments;
	}

	public void setUpdationComments(String updationComments) {
		this.updationComments = updationComments;
	}

	public DatEmpDetailBO getDatEmpDetail() {
		return datEmpDetail;
	}

	public void setDatEmpDetail(DatEmpDetailBO datEmpDetail) {
		this.datEmpDetail = datEmpDetail;
	}

	public MasBusinessEntityBO getMasBusinessEntity() {
		return masBusinessEntity;
	}

	public void setMasBusinessEntity(MasBusinessEntityBO masBusinessEntity) {
		this.masBusinessEntity = masBusinessEntity;
	}

	public DatComponentBO getDatComponent() {
		return datComponent;
	}

	public void setDatComponent(DatComponentBO datComponent) {
		this.datComponent = datComponent;
	}

	public InvoiceDatInvoiceDetailBO getInvoiceDatInvoiceDetail() {
		return invoiceDatInvoiceDetail;
	}

	public void setInvoiceDatInvoiceDetail(InvoiceDatInvoiceDetailBO invoiceDatInvoiceDetail) {
		this.invoiceDatInvoiceDetail = invoiceDatInvoiceDetail;
	}

	public InvoiceDatPurchaseOrderBO getInvoiceDatPurchaseOrder() {
		return invoiceDatPurchaseOrder;
	}

	public void setInvoiceDatPurchaseOrder(InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrder) {
		this.invoiceDatPurchaseOrder = invoiceDatPurchaseOrder;
	}

	@Override
	public String toString() {
		return "InvoiceDatInvoiceDetailsAudit [pkInvoiceDetailsHistoryId=" + pkInvoiceDetailsHistoryId + ", action="
				+ action + ", comments=" + comments + ", enteredOn=" + enteredOn + ", invoiceAmount=" + invoiceAmount
				+ ", invoiceDate=" + invoiceDate + ", invoiceNumber=" + invoiceNumber + ", isPaymentReceivedStatus="
				+ isPaymentReceivedStatus + ", updationComments=" + updationComments + ", datEmpDetail=" + datEmpDetail
				+ ", masBusinessEntity=" + masBusinessEntity + ", datComponent=" + datComponent
				+ ", invoiceDatInvoiceDetail=" + invoiceDatInvoiceDetail + ", invoiceDatPurchaseOrder="
				+ invoiceDatPurchaseOrder + "]";
	}

	
}