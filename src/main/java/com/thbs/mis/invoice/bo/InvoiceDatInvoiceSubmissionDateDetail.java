package com.thbs.mis.invoice.bo;


import java.io.Serializable;
import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;

import java.util.Date;


/**
 * The persistent class for the invoice_dat_invoice_submission_date_details database table.
 * 
 */
@Entity
@Table(name="invoice_dat_invoice_submission_date_details")
@NamedQuery(name="InvoiceDatInvoiceSubmissionDateDetail.findAll", query="SELECT i FROM InvoiceDatInvoiceSubmissionDateDetail i")
public class InvoiceDatInvoiceSubmissionDateDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_submision_date_details")
	private int pkSubmisionDateDetails;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="sequence_number")
	private int sequenceNumber;

	@Temporal(TemporalType.DATE)
	@Column(name="submission_date")
	private Date submissionDate;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="created_by",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail;

	//bi-directional many-to-one association to InvoiceDatInvoiceDetail
	@OneToOne
	@JoinColumn(name="fk_invoice_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceDatInvoiceDetailBO invoiceDatInvoiceDetail;

	//bi-directional many-to-one association to InvoiceMasSubmissionMode
	@OneToOne
	@JoinColumn(name="fk_mode_of_submission_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceMasSubmissionModeBO invoiceMasSubmissionMode;

	public InvoiceDatInvoiceSubmissionDateDetail() {
	}

	public int getPkSubmisionDateDetails() {
		return this.pkSubmisionDateDetails;
	}

	public void setPkSubmisionDateDetails(int pkSubmisionDateDetails) {
		this.pkSubmisionDateDetails = pkSubmisionDateDetails;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public int getSequenceNumber() {
		return this.sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public Date getSubmissionDate() {
		return this.submissionDate;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}

	public DatEmpDetailBO getDatEmpDetail() {
		return datEmpDetail;
	}

	public void setDatEmpDetail(DatEmpDetailBO datEmpDetail) {
		this.datEmpDetail = datEmpDetail;
	}

	public InvoiceDatInvoiceDetailBO getInvoiceDatInvoiceDetail() {
		return invoiceDatInvoiceDetail;
	}

	public void setInvoiceDatInvoiceDetail(InvoiceDatInvoiceDetailBO invoiceDatInvoiceDetail) {
		this.invoiceDatInvoiceDetail = invoiceDatInvoiceDetail;
	}

	public InvoiceMasSubmissionModeBO getInvoiceMasSubmissionMode() {
		return invoiceMasSubmissionMode;
	}

	public void setInvoiceMasSubmissionMode(InvoiceMasSubmissionModeBO invoiceMasSubmissionMode) {
		this.invoiceMasSubmissionMode = invoiceMasSubmissionMode;
	}

	@Override
	public String toString() {
		return "InvoiceDatInvoiceSubmissionDateDetail [pkSubmisionDateDetails=" + pkSubmisionDateDetails + ", comments="
				+ comments + ", createdOn=" + createdOn + ", sequenceNumber=" + sequenceNumber + ", submissionDate="
				+ submissionDate + ", datEmpDetail=" + datEmpDetail + ", invoiceDatInvoiceDetail="
				+ invoiceDatInvoiceDetail + ", invoiceMasSubmissionMode=" + invoiceMasSubmissionMode + "]";
	}

	

}