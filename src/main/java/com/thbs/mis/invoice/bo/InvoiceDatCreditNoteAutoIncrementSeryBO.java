package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the invoice_dat_credit_note_auto_increment_series database table.
 * 
 */
@Entity
@Table(name="invoice_dat_credit_note_auto_increment_series")
@NamedQuery(name="InvoiceDatCreditNoteAutoIncrementSeryBO.findAll", query="SELECT i FROM InvoiceDatCreditNoteAutoIncrementSeryBO i")
public class InvoiceDatCreditNoteAutoIncrementSeryBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_credit_note_auto_increment_id")
	private byte pkCreditNoteAutoIncrementId;

	@Column(name="fk_business_entity_id")
	private byte fkBusinessEntityId;

	@Column(name="last_credit_note_number")
	private String lastCreditNoteNumber;

	public InvoiceDatCreditNoteAutoIncrementSeryBO() {
	}

	public byte getPkCreditNoteAutoIncrementId() {
		return this.pkCreditNoteAutoIncrementId;
	}

	public void setPkCreditNoteAutoIncrementId(byte pkCreditNoteAutoIncrementId) {
		this.pkCreditNoteAutoIncrementId = pkCreditNoteAutoIncrementId;
	}

	public byte getFkBusinessEntityId() {
		return this.fkBusinessEntityId;
	}

	public void setFkBusinessEntityId(byte fkBusinessEntityId) {
		this.fkBusinessEntityId = fkBusinessEntityId;
	}

	public String getLastCreditNoteNumber() {
		return this.lastCreditNoteNumber;
	}

	public void setLastCreditNoteNumber(String lastCreditNoteNumber) {
		this.lastCreditNoteNumber = lastCreditNoteNumber;
	}

	@Override
	public String toString() {
		return "InvoiceDatCreditNoteAutoIncrementSeryBO [pkCreditNoteAutoIncrementId=" + pkCreditNoteAutoIncrementId
				+ ", fkBusinessEntityId=" + fkBusinessEntityId + ", lastCreditNoteNumber=" + lastCreditNoteNumber + "]";
	}

}