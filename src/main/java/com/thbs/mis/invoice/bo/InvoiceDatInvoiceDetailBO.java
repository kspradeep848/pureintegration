package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasBusinessEntityBO;
import com.thbs.mis.project.bo.DatComponentBO;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the invoice_dat_invoice_details database table.
 * 
 */
@Entity
@Table(name="invoice_dat_invoice_details")
@NamedQuery(name="InvoiceDatInvoiceDetailBO.findAll", query="SELECT i FROM InvoiceDatInvoiceDetailBO i")
public class InvoiceDatInvoiceDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_invoice_details_id")
	private int pkInvoiceDetailsId;

	private String comments;

	public MasBusinessEntityBO getMasBusinessEntity() {
		return masBusinessEntity;
	}



	public void setMasBusinessEntity(MasBusinessEntityBO masBusinessEntity) {
		this.masBusinessEntity = masBusinessEntity;
	}



	public DatComponentBO getDatComponent() {
		return datComponent;
	}



	public void setDatComponent(DatComponentBO datComponent) {
		this.datComponent = datComponent;
	}



	public DatEmpDetailBO getDatEmpDetail1CreatedBy() {
		return datEmpDetail1CreatedBy;
	}



	public void setDatEmpDetail1CreatedBy(DatEmpDetailBO datEmpDetail1CreatedBy) {
		this.datEmpDetail1CreatedBy = datEmpDetail1CreatedBy;
	}



	public DatEmpDetailBO getDatEmpDetailBOUpdatedBy() {
		return DatEmpDetailBOUpdatedBy;
	}



	public void setDatEmpDetailBOUpdatedBy(DatEmpDetailBO datEmpDetailBOUpdatedBy) {
		DatEmpDetailBOUpdatedBy = datEmpDetailBOUpdatedBy;
	}



	@Column(name="created_by")
	private int createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="fk_business_entity_id")
	private byte fkBusinessEntityId;

	@Column(name="fk_component_id")
	private int fkComponentId;

	@Column(name="invoice_amount")
	private BigDecimal invoiceAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="invoice_date")
	private Date invoiceDate;

	@Column(name="invoice_number")
	private String invoiceNumber;

	@Column(name="is_payment_received_status")
	private String isPaymentReceivedStatus;

	/*@Temporal(TemporalType.DATE)
	@Column(name="submission_date")
	private Date submissionDate;*/

	@Column(name="updated_by")
	private int updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;

	@Column(name="updation_comments")
	private String updationComments;

	
	//bi-directional one-to-one association to InvoiceDatPurchaseOrder
	@OneToOne
	@JoinColumn(name="fk_purchase_order_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrder;

	@OneToOne
	@JoinColumn(name="fk_business_entity_id",unique = true, nullable = true, insertable = false, updatable = false)
	private MasBusinessEntityBO masBusinessEntity;

	//bi-directional many-to-one association to DatComponent
	@OneToOne
	@JoinColumn(name="fk_component_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatComponentBO datComponent;

	//bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name="created_by",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail1CreatedBy;

	@OneToOne
	@JoinColumn(name="updated_by",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO DatEmpDetailBOUpdatedBy;

	public InvoiceDatInvoiceDetailBO() {
	}



	public int getPkInvoiceDetailsId() {
		return pkInvoiceDetailsId;
	}



	public void setPkInvoiceDetailsId(int pkInvoiceDetailsId) {
		this.pkInvoiceDetailsId = pkInvoiceDetailsId;
	}



	public String getComments() {
		return comments;
	}



	public void setComments(String comments) {
		this.comments = comments;
	}



	public int getCreatedBy() {
		return createdBy;
	}



	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}



	public Date getCreatedOn() {
		return createdOn;
	}



	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}



	public byte getFkBusinessEntityId() {
		return fkBusinessEntityId;
	}



	public void setFkBusinessEntityId(byte fkBusinessEntityId) {
		this.fkBusinessEntityId = fkBusinessEntityId;
	}



	public int getFkComponentId() {
		return fkComponentId;
	}



	public void setFkComponentId(int fkComponentId) {
		this.fkComponentId = fkComponentId;
	}



	public BigDecimal getInvoiceAmount() {
		return invoiceAmount;
	}



	public void setInvoiceAmount(BigDecimal invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}



	public Date getInvoiceDate() {
		return invoiceDate;
	}



	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}



	public String getInvoiceNumber() {
		return invoiceNumber;
	}



	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}



	public String getIsPaymentReceivedStatus() {
		return isPaymentReceivedStatus;
	}



	public void setIsPaymentReceivedStatus(String isPaymentReceivedStatus) {
		this.isPaymentReceivedStatus = isPaymentReceivedStatus;
	}





	public int getUpdatedBy() {
		return updatedBy;
	}



	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}



	public Date getUpdatedOn() {
		return updatedOn;
	}



	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}



	public String getUpdationComments() {
		return updationComments;
	}



	public void setUpdationComments(String updationComments) {
		this.updationComments = updationComments;
	}



	public InvoiceDatPurchaseOrderBO getInvoiceDatPurchaseOrder() {
		return invoiceDatPurchaseOrder;
	}



	public void setInvoiceDatPurchaseOrder(InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrder) {
		this.invoiceDatPurchaseOrder = invoiceDatPurchaseOrder;
	}



	@Override
	public String toString() {
		return "InvoiceDatInvoiceDetailBO [pkInvoiceDetailsId=" + pkInvoiceDetailsId + ", comments=" + comments
				+ ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", fkBusinessEntityId=" + fkBusinessEntityId
				+ ", fkComponentId=" + fkComponentId + ", invoiceAmount=" + invoiceAmount + ", invoiceDate="
				+ invoiceDate + ", invoiceNumber=" + invoiceNumber + ", isPaymentReceivedStatus="
				+ isPaymentReceivedStatus + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn
				+ ", updationComments=" + updationComments + ", invoiceDatPurchaseOrder=" + invoiceDatPurchaseOrder
				+ ", masBusinessEntity=" + masBusinessEntity + ", datComponent=" + datComponent
				+ ", datEmpDetail1CreatedBy=" + datEmpDetail1CreatedBy + ", DatEmpDetailBOUpdatedBy="
				+ DatEmpDetailBOUpdatedBy + "]";
	}

	
}