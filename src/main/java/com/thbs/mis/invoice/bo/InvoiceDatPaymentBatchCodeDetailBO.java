package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the invoice_dat_payment_batch_code_details database table.
 * 
 */
@Entity
@Table(name="invoice_dat_payment_batch_code_details")
@NamedQuery(name="InvoiceDatPaymentBatchCodeDetailBO.findAll", query="SELECT i FROM InvoiceDatPaymentBatchCodeDetailBO i")
public class InvoiceDatPaymentBatchCodeDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_dat_payment_batch_code_details")
	private int pkDatPaymentBatchCodeDetails;

	@Column(name="batch_code")
	private String batchCode;

	@Temporal(TemporalType.DATE)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="sequence_number")
	private int sequenceNumber;

	
	public InvoiceDatPaymentBatchCodeDetailBO() {
	}


	public int getPkDatPaymentBatchCodeDetails() {
		return pkDatPaymentBatchCodeDetails;
	}


	public void setPkDatPaymentBatchCodeDetails(int pkDatPaymentBatchCodeDetails) {
		this.pkDatPaymentBatchCodeDetails = pkDatPaymentBatchCodeDetails;
	}


	public String getBatchCode() {
		return batchCode;
	}


	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	public int getSequenceNumber() {
		return sequenceNumber;
	}


	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}


	@Override
	public String toString() {
		return "InvoiceDatPaymentBatchCodeDetailBO [pkDatPaymentBatchCodeDetails=" + pkDatPaymentBatchCodeDetails
				+ ", batchCode=" + batchCode + ", createdOn=" + createdOn + ", sequenceNumber=" + sequenceNumber + "]";
	}

	



}