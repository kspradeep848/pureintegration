package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the invoice_dat_credit_notes_details database table.
 * 
 */
@Entity
@Table(name="invoice_dat_credit_notes_details")
@NamedQuery(name="InvoiceDatCreditNotesDetailBO.findAll", query="SELECT i FROM InvoiceDatCreditNotesDetailBO i")
public class InvoiceDatCreditNotesDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_credit_notes_details_id")
	private int pkCreditNotesDetailsId;

	@Column(name="comments_for_updation")
	private String commentsForUpdation;

	@Column(name="credit_note_amount")
	private BigDecimal creditNoteAmount;

	@Column(name="credit_note_comments")
	private String creditNoteComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="credit_note_date")
	private Date creditNoteDate;

	@Column(name="credit_note_number")
	private String creditNoteNumber;

	@Column(name="fk_business_entity_id")
	private byte fkBusinessEntityId;

	@Column(name="fk_created_by")
	private int fkCreatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="generated_on")
	private Date generatedOn;

	@Column(name="updated_by")
	private int updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;

	//bi-directional one-to-one association to InvoiceDatCreditNoteRequestDetail
	@OneToOne
	@JoinColumn(name="fk_credit_note_request_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceDatCreditNoteRequestDetailBO invoiceDatCreditNoteRequestDetail;

	//bi-directional one-to-one association to InvoiceDatInvoiceDetail
	@OneToOne
	@JoinColumn(name="fk_invoice_id",unique = true, nullable = true, insertable = false, updatable = false)
	private InvoiceDatInvoiceDetailBO invoiceDatInvoiceDetail;


	public InvoiceDatCreditNotesDetailBO() {
	}


	public int getPkCreditNotesDetailsId() {
		return pkCreditNotesDetailsId;
	}


	public void setPkCreditNotesDetailsId(int pkCreditNotesDetailsId) {
		this.pkCreditNotesDetailsId = pkCreditNotesDetailsId;
	}


	public String getCommentsForUpdation() {
		return commentsForUpdation;
	}


	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}


	public BigDecimal getCreditNoteAmount() {
		return creditNoteAmount;
	}


	public void setCreditNoteAmount(BigDecimal creditNoteAmount) {
		this.creditNoteAmount = creditNoteAmount;
	}


	public String getCreditNoteComments() {
		return creditNoteComments;
	}


	public void setCreditNoteComments(String creditNoteComments) {
		this.creditNoteComments = creditNoteComments;
	}


	public Date getCreditNoteDate() {
		return creditNoteDate;
	}


	public void setCreditNoteDate(Date creditNoteDate) {
		this.creditNoteDate = creditNoteDate;
	}


	public String getCreditNoteNumber() {
		return creditNoteNumber;
	}


	public void setCreditNoteNumber(String creditNoteNumber) {
		this.creditNoteNumber = creditNoteNumber;
	}


	public byte getFkBusinessEntityId() {
		return fkBusinessEntityId;
	}


	public void setFkBusinessEntityId(byte fkBusinessEntityId) {
		this.fkBusinessEntityId = fkBusinessEntityId;
	}


	public int getFkCreatedBy() {
		return fkCreatedBy;
	}


	public void setFkCreatedBy(int fkCreatedBy) {
		this.fkCreatedBy = fkCreatedBy;
	}


	public Date getGeneratedOn() {
		return generatedOn;
	}


	public void setGeneratedOn(Date generatedOn) {
		this.generatedOn = generatedOn;
	}


	public int getUpdatedBy() {
		return updatedBy;
	}


	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}


	public Date getUpdatedOn() {
		return updatedOn;
	}


	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}


	public InvoiceDatCreditNoteRequestDetailBO getInvoiceDatCreditNoteRequestDetail() {
		return invoiceDatCreditNoteRequestDetail;
	}


	public void setInvoiceDatCreditNoteRequestDetail(
			InvoiceDatCreditNoteRequestDetailBO invoiceDatCreditNoteRequestDetail) {
		this.invoiceDatCreditNoteRequestDetail = invoiceDatCreditNoteRequestDetail;
	}


	public InvoiceDatInvoiceDetailBO getInvoiceDatInvoiceDetail() {
		return invoiceDatInvoiceDetail;
	}


	public void setInvoiceDatInvoiceDetail(InvoiceDatInvoiceDetailBO invoiceDatInvoiceDetail) {
		this.invoiceDatInvoiceDetail = invoiceDatInvoiceDetail;
	}


	@Override
	public String toString() {
		return "InvoiceDatCreditNotesDetailBO [pkCreditNotesDetailsId=" + pkCreditNotesDetailsId
				+ ", commentsForUpdation=" + commentsForUpdation + ", creditNoteAmount=" + creditNoteAmount
				+ ", creditNoteComments=" + creditNoteComments + ", creditNoteDate=" + creditNoteDate
				+ ", creditNoteNumber=" + creditNoteNumber + ", fkBusinessEntityId=" + fkBusinessEntityId
				+ ", fkCreatedBy=" + fkCreatedBy + ", generatedOn=" + generatedOn + ", updatedBy=" + updatedBy
				+ ", updatedOn=" + updatedOn + ", invoiceDatCreditNoteRequestDetail="
				+ invoiceDatCreditNoteRequestDetail + ", invoiceDatInvoiceDetail=" + invoiceDatInvoiceDetail + "]";
	}

	
}