package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasCurrencytypeBO;
import com.thbs.mis.project.bo.MasClientBO;
import com.thbs.mis.project.bo.ProjMasClientManagerBO;


/**
 * The persistent class for the invoice_dat_purchase_order database table.
 * 
 */
@Entity
@Table(name="invoice_dat_purchase_order")
@NamedQuery(name="InvoiceDatPurchaseOrderBO.findAll", query="SELECT i FROM InvoiceDatPurchaseOrderBO i")
public class InvoiceDatPurchaseOrderBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_purchase_order_id")
	private Integer pkPurchaseOrderId;

	@Column(name="client_bill_to_address_id")
	private Short clientBillToAddressId;

	@Column(name="client_ship_to_address_id")
	private Short clientShipToAddressId;

	@Column(name="comments_for_updation")
	private String commentsForUpdation;

	@Column(name="created_by")
	private Integer createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="doc_upload_status")
	private String docUploadStatus;

	@Column(name="fk_client_mgr_id")
	private Short fkClientMgrId;

	@Column(name="fk_currency_id")
	private Short fkCurrencyId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="po_delivery_date")
	private Date poDeliveryDate;

	@Column(name="po_description")
	private String poDescription;

	@Column(name="po_remaining_value")
	private Double poRemainingValue;

	@Column(name="po_status")
	private String poStatus;

	@Column(name="po_type")
	private String poType;

	@Column(name="po_value")
	private Double poValue;

	@Column(name="purchase_number")
	private String purchaseNumber;

	@Column(name="surplus_value")
	private Double surplusValue;

	@Column(name="updated_by")
	private Integer updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;

	@Column(name="po_prefix")
	private String poPrefix;

	
	@OneToOne
	@JoinColumn(name="created_by",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOCreatedBy;
	
	@OneToOne
	@JoinColumn(name="updated_by",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOUpdatedBy;
	
	@OneToOne
	@JoinColumn(name="fk_client_mgr_id",unique = true, nullable = true, insertable = false, updatable = false)
	private ProjMasClientManagerBO projMasClientManagerBO;
	
	@OneToOne
	@JoinColumn(name="client_bill_to_address_id",unique = true, nullable = true, insertable = false, updatable = false)
	private MasClientBO masClientBOBillAddress;
	
	@OneToOne
	@JoinColumn(name="client_ship_to_address_id",unique = true, nullable = true, insertable = false, updatable = false)
	private MasClientBO masClientBOShipAddress;
	
	@OneToOne
	@JoinColumn(name="fk_currency_id",unique = true, nullable = true, insertable = false, updatable = false)
	private MasCurrencytypeBO masCurrencytypeBO;
	
	
	public String getPoPrefix() {
		return poPrefix;
	}



	public void setPoPrefix(String poPrefix) {
		this.poPrefix = poPrefix;
	}



	public InvoiceDatPurchaseOrderBO() {
	}
	
	@Transient
	private Double withholdTaxAmount;
	@Transient
	private Integer fkWorkorderId;
	
	

	/**
	 * <Description InvoiceDatPurchaseOrderBO:> TODO
	 * 
	 * @param commentsForUpdation
	 * @param createdOn
	 * @param poDeliveryDate
	 * @param poDescription
	 * @param poRemainingValue
	 * @param poRemainingValue
	 * @param poStatus
	 * @param poType
	 * @param poValue
	 * @param purchaseNumber
	 * @param surplusValue
	 * @param updatedBy
	 * @param updatedOn
	 * @param withholdTaxAmount
	 * @param fkWorkorderId
	 */
	
	
	public InvoiceDatPurchaseOrderBO(Integer pkPurchaseOrderId,String commentsForUpdation, Date createdOn, Date poDeliveryDate,
			String poDescription, Double poRemainingValue, String poStatus, String poType, Double poValue,
			String purchaseNumber, Double surplusValue, Integer updatedBy, Date updatedOn, Double withholdTaxAmount,
			Integer fkWorkorderId) {
		super();
		this.pkPurchaseOrderId = pkPurchaseOrderId;
		this.commentsForUpdation = commentsForUpdation;
		this.createdOn = createdOn;
		this.poDeliveryDate = poDeliveryDate;
		this.poDescription = poDescription;
		this.poRemainingValue = poRemainingValue;
		this.poStatus = poStatus;
		this.poType = poType;
		this.poValue = poValue;
		this.purchaseNumber = purchaseNumber;
		this.surplusValue = surplusValue;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.withholdTaxAmount = withholdTaxAmount;
		this.fkWorkorderId = fkWorkorderId;
	}



	public Integer getPkPurchaseOrderId() {
		return pkPurchaseOrderId;
	}


	public void setPkPurchaseOrderId(Integer pkPurchaseOrderId) {
		this.pkPurchaseOrderId = pkPurchaseOrderId;
	}


	public Short getClientBillToAddressId() {
		return clientBillToAddressId;
	}


	public void setClientBillToAddressId(Short clientBillToAddressId) {
		this.clientBillToAddressId = clientBillToAddressId;
	}


	public Short getClientShipToAddressId() {
		return clientShipToAddressId;
	}


	public void setClientShipToAddressId(Short clientShipToAddressId) {
		this.clientShipToAddressId = clientShipToAddressId;
	}


	public String getCommentsForUpdation() {
		return commentsForUpdation;
	}


	public void setCommentsForUpdation(String commentsForUpdation) {
		this.commentsForUpdation = commentsForUpdation;
	}


	public Integer getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	public String getDocUploadStatus() {
		return docUploadStatus;
	}


	public void setDocUploadStatus(String docUploadStatus) {
		this.docUploadStatus = docUploadStatus;
	}


	public Short getFkClientMgrId() {
		return fkClientMgrId;
	}


	public void setFkClientMgrId(Short fkClientMgrId) {
		this.fkClientMgrId = fkClientMgrId;
	}


	public Short getFkCurrencyId() {
		return fkCurrencyId;
	}


	public void setFkCurrencyId(Short fkCurrencyId) {
		this.fkCurrencyId = fkCurrencyId;
	}


	public Date getPoDeliveryDate() {
		return poDeliveryDate;
	}


	public void setPoDeliveryDate(Date poDeliveryDate) {
		this.poDeliveryDate = poDeliveryDate;
	}


	public String getPoDescription() {
		return poDescription;
	}


	public void setPoDescription(String poDescription) {
		this.poDescription = poDescription;
	}


	public Double getPoRemainingValue() {
		return poRemainingValue;
	}


	public void setPoRemainingValue(Double poRemainingValue) {
		this.poRemainingValue = poRemainingValue;
	}


	public String getPoStatus() {
		return poStatus;
	}


	public void setPoStatus(String poStatus) {
		this.poStatus = poStatus;
	}


	public String getPoType() {
		return poType;
	}


	public void setPoType(String poType) {
		this.poType = poType;
	}


	public Double getPoValue() {
		return poValue;
	}


	public void setPoValue(Double poValue) {
		this.poValue = poValue;
	}


	public String getPurchaseNumber() {
		return purchaseNumber;
	}


	public void setPurchaseNumber(String purchaseNumber) {
		this.purchaseNumber = purchaseNumber;
	}


	public Double getSurplusValue() {
		return surplusValue;
	}


	public void setSurplusValue(Double surplusValue) {
		this.surplusValue = surplusValue;
	}


	public Integer getUpdatedBy() {
		return updatedBy;
	}


	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}


	public Date getUpdatedOn() {
		return updatedOn;
	}


	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}


	public Double getWithholdTaxAmount() {
		return withholdTaxAmount;
	}



	public void setWithholdTaxAmount(Double withholdTaxAmount) {
		this.withholdTaxAmount = withholdTaxAmount;
	}



	public Integer getFkWorkorderId() {
		return fkWorkorderId;
	}



	public void setFkWorkorderId(Integer fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}



	public DatEmpDetailBO getDatEmpDetailBOCreatedBy() {
		return datEmpDetailBOCreatedBy;
	}



	public void setDatEmpDetailBOCreatedBy(DatEmpDetailBO datEmpDetailBOCreatedBy) {
		this.datEmpDetailBOCreatedBy = datEmpDetailBOCreatedBy;
	}



	public DatEmpDetailBO getDatEmpDetailBOUpdatedBy() {
		return datEmpDetailBOUpdatedBy;
	}



	public void setDatEmpDetailBOUpdatedBy(DatEmpDetailBO datEmpDetailBOUpdatedBy) {
		this.datEmpDetailBOUpdatedBy = datEmpDetailBOUpdatedBy;
	}



	public ProjMasClientManagerBO getProjMasClientManagerBO() {
		return projMasClientManagerBO;
	}



	public void setProjMasClientManagerBO(ProjMasClientManagerBO projMasClientManagerBO) {
		this.projMasClientManagerBO = projMasClientManagerBO;
	}



	public MasClientBO getMasClientBOBillAddress() {
		return masClientBOBillAddress;
	}



	public void setMasClientBOBillAddress(MasClientBO masClientBOBillAddress) {
		this.masClientBOBillAddress = masClientBOBillAddress;
	}



	public MasClientBO getMasClientBOShipAddress() {
		return masClientBOShipAddress;
	}



	public void setMasClientBOShipAddress(MasClientBO masClientBOShipAddress) {
		this.masClientBOShipAddress = masClientBOShipAddress;
	}



	public MasCurrencytypeBO getMasCurrencytypeBO() {
		return masCurrencytypeBO;
	}



	public void setMasCurrencytypeBO(MasCurrencytypeBO masCurrencytypeBO) {
		this.masCurrencytypeBO = masCurrencytypeBO;
	}



	@Override
	public String toString() {
		return "InvoiceDatPurchaseOrderBO [pkPurchaseOrderId=" + pkPurchaseOrderId + ", clientBillToAddressId="
				+ clientBillToAddressId + ", clientShipToAddressId=" + clientShipToAddressId + ", commentsForUpdation="
				+ commentsForUpdation + ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", docUploadStatus="
				+ docUploadStatus + ", fkClientMgrId=" + fkClientMgrId + ", fkCurrencyId=" + fkCurrencyId
				+ ", poDeliveryDate=" + poDeliveryDate + ", poDescription=" + poDescription + ", poRemainingValue="
				+ poRemainingValue + ", poStatus=" + poStatus + ", poType=" + poType + ", poValue=" + poValue
				+ ", purchaseNumber=" + purchaseNumber + ", surplusValue=" + surplusValue + ", updatedBy=" + updatedBy
				+ ", updatedOn=" + updatedOn + ", poPrefix=" + poPrefix + ", datEmpDetailBOCreatedBy="
				+ datEmpDetailBOCreatedBy + ", datEmpDetailBOUpdatedBy=" + datEmpDetailBOUpdatedBy
				+ ", projMasClientManagerBO=" + projMasClientManagerBO + ", masClientBOBillAddress="
				+ masClientBOBillAddress + ", masClientBOShipAddress=" + masClientBOShipAddress + ", masCurrencytypeBO="
				+ masCurrencytypeBO + ", withholdTaxAmount=" + withholdTaxAmount + ", fkWorkorderId=" + fkWorkorderId
				+ "]";
	}



}