package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the invoice_dat_invoice_unique_number database table.
 * 
 */
@Entity
@Table(name="invoice_dat_invoice_unique_number")
@NamedQuery(name="InvoiceDatInvoiceUniqueNumberBO.findAll", query="SELECT i FROM InvoiceDatInvoiceUniqueNumberBO i")
public class InvoiceDatInvoiceUniqueNumberBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_invoice_number_id")
	private int pkInvoiceNumberId;

	@Column(name="unique_invoice_number")
	private String uniqueInvoiceNumber;

	public InvoiceDatInvoiceUniqueNumberBO() {
	}

	public int getPkInvoiceNumberId() {
		return this.pkInvoiceNumberId;
	}

	public void setPkInvoiceNumberId(int pkInvoiceNumberId) {
		this.pkInvoiceNumberId = pkInvoiceNumberId;
	}

	public String getUniqueInvoiceNumber() {
		return this.uniqueInvoiceNumber;
	}

	public void setUniqueInvoiceNumber(String uniqueInvoiceNumber) {
		this.uniqueInvoiceNumber = uniqueInvoiceNumber;
	}

	@Override
	public String toString() {
		return "InvoiceDatInvoiceUniqueNumberBO [pkInvoiceNumberId=" + pkInvoiceNumberId + ", uniqueInvoiceNumber="
				+ uniqueInvoiceNumber + "]";
	}

}