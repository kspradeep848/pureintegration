package com.thbs.mis.invoice.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the invoice_range_of_credit_note_number_allocated_to_entity database table.
 * 
 */
@Entity
@Table(name="invoice_range_of_credit_note_number_allocated_to_entity")
@NamedQuery(name="InvoiceRangeOfCreditNoteNumberAllocatedToEntityBO.findAll", query="SELECT i FROM InvoiceRangeOfCreditNoteNumberAllocatedToEntityBO i")
public class InvoiceRangeOfCreditNoteNumberAllocatedToEntityBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_range_of_credit_note_number_allocated_to_entity_id")
	private short pkRangeOfCreditNoteNumberAllocatedToEntityId;

	@Column(name="first_credit_note_number")
	private int firstCreditNoteNumber;

	@Column(name="fk_business_entity_id")
	private byte fkBusinessEntityId;

	@Column(name="is_active")
	private String isActive;

	@Column(name="last_credit_note_number")
	private int lastCreditNoteNumber;

	public InvoiceRangeOfCreditNoteNumberAllocatedToEntityBO() {
	}

	public short getPkRangeOfCreditNoteNumberAllocatedToEntityId() {
		return this.pkRangeOfCreditNoteNumberAllocatedToEntityId;
	}

	public void setPkRangeOfCreditNoteNumberAllocatedToEntityId(short pkRangeOfCreditNoteNumberAllocatedToEntityId) {
		this.pkRangeOfCreditNoteNumberAllocatedToEntityId = pkRangeOfCreditNoteNumberAllocatedToEntityId;
	}

	public int getFirstCreditNoteNumber() {
		return this.firstCreditNoteNumber;
	}

	public void setFirstCreditNoteNumber(int firstCreditNoteNumber) {
		this.firstCreditNoteNumber = firstCreditNoteNumber;
	}

	public byte getFkBusinessEntityId() {
		return this.fkBusinessEntityId;
	}

	public void setFkBusinessEntityId(byte fkBusinessEntityId) {
		this.fkBusinessEntityId = fkBusinessEntityId;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public int getLastCreditNoteNumber() {
		return this.lastCreditNoteNumber;
	}

	public void setLastCreditNoteNumber(int lastCreditNoteNumber) {
		this.lastCreditNoteNumber = lastCreditNoteNumber;
	}

	@Override
	public String toString() {
		return "InvoiceRangeOfCreditNoteNumberAllocatedToEntityBO [pkRangeOfCreditNoteNumberAllocatedToEntityId="
				+ pkRangeOfCreditNoteNumberAllocatedToEntityId + ", firstCreditNoteNumber=" + firstCreditNoteNumber
				+ ", fkBusinessEntityId=" + fkBusinessEntityId + ", isActive=" + isActive + ", lastCreditNoteNumber="
				+ lastCreditNoteNumber + "]";
	}

}