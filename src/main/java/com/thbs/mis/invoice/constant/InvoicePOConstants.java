package com.thbs.mis.invoice.constant;

public class InvoicePOConstants {

	
	// Added by Shashi Kant
		public final static String INVOICE_PO_MAPPED = "MAPPED";
		public final static String INVOICE_PO_UNMAPPED = "UNMAPPED";
		public final static String INVOICE_PO_ARCHIVED = "ARCHIVED";
		public final static String INVOICE_PO_UPLOADED = "UPLOADED";
		public final static String INVOICE_PO_UNUPLOADED = "UNUPLOADED";
		public final static String INVOICE_NORMAL_PO = "NORMAL";
		public final static String INVOICE_AGGREGATE_PO = "AGGREGATE";
	// EOA by Shashi Kant
		
	
}
