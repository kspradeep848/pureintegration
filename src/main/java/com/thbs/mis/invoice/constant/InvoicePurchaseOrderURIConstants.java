package com.thbs.mis.invoice.constant;

public class InvoicePurchaseOrderURIConstants {

	
	// Added by Shashi Kant
		public final static String VIEW_ALL_INVOICE_PO = "/invoice/view/invoicePO";
		
		public final static String VIEW_INVOICE_PO_DETAILS = "/invoice/view/invoicePODetails/{InvoicePONumber}/{loggedInEmpId}";
		
		public final static String CREATE_INVOICE_PO_WITHOUT_DOC = "invoice/createwWithoutDocument/invoicePO";
		
		public final static String CREATE_INVOICE_PO_WITH_DOC = "invoice/createWithDocument/invoicePO";

		public final static String UPDATE_INVOICE_PO = "invoice/update/invoicePO";

		public final static String ARCHIVED_INVOICE_PO = "invoice/archive/invoicePO/{InvoicePONumber}/{loggedInEmpId}";

	// EOA by Shashi Kant
		
	//Added by Smrithi 
		 
		public final static String MAP_TO_WORKORDER = "/invoice/map/poToWo";
		 
		 public final static String UNMAPPING_TO_WORKORDER = "/invoice/unmap/poToWo";
		 
	//EOA by Smrithi 
		 
	//Added by Balaji
		
		 public final static String SEARCH_FILTERED_PURCHASE_ORDERS = "/invoice/searchFiltered/purchaseOrders";
		 
		 public final static String SHIFTING_NORMAL_PO_TO_AGGREGATE = "/invoice/normalPO/toAggregatePO";
		 
	// EOA by Balaji
}
