package com.thbs.mis.invoice.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.DateTimeUtil;
import com.thbs.mis.invoice.bean.CreateInvoicePOWithDocInputBean;
import com.thbs.mis.invoice.bean.CreateInvoicePOWithoutDocInputBean;
import com.thbs.mis.invoice.bean.FilterPurchaseOrderInputBean;
import com.thbs.mis.invoice.bean.FilterPurchaseOrderOutputBean;
import com.thbs.mis.invoice.bean.InvoicePODetailsBean;
import com.thbs.mis.invoice.bean.InvoiceShiftingPOInputBean;
import com.thbs.mis.invoice.bean.MapPoToWorkorderInputBean;
import com.thbs.mis.invoice.bean.MapPoToWorkorderOutputBean;
import com.thbs.mis.invoice.bean.POLineItemsDetailsBean;
import com.thbs.mis.invoice.bean.SearchPOInputBean;
import com.thbs.mis.invoice.bean.SearchPOOutPutBean;
import com.thbs.mis.invoice.bean.UpdateInvoicePOWithDocInputBean;
import com.thbs.mis.invoice.bo.InvoiceDatClientPoLineItemBO;
import com.thbs.mis.invoice.bo.InvoiceDatMappingPoWithWoBO;
import com.thbs.mis.invoice.bo.InvoiceDatPoLineItemsTaxDetailBO;
import com.thbs.mis.invoice.bo.InvoiceDatPurchaseOrderBO;
import com.thbs.mis.invoice.constant.InvoicePOConstants;
import com.thbs.mis.invoice.dao.InvoiceDatClientPoLineItemRepository;
import com.thbs.mis.invoice.dao.InvoiceDatMappingPoWithWoRepository;
import com.thbs.mis.invoice.dao.InvoiceDatPoLineItemsTaxDetailRepository;
import com.thbs.mis.invoice.dao.InvoiceDatPurchaseOrderRepository;
import com.thbs.mis.invoice.dao.PurchaseOrderFilterSpecification;
import com.thbs.mis.project.bean.WorkOrderOutputBean;
import com.thbs.mis.project.bo.DatWorkorderBO;
import com.thbs.mis.project.bo.MasClientAddressBO;
import com.thbs.mis.project.bo.MasClientBO;
import com.thbs.mis.project.bo.MasProjectBO;
import com.thbs.mis.project.bo.ProjDatMilestoneTriggerDetailBO;
import com.thbs.mis.project.bo.ProjMasClientManagerBO;
import com.thbs.mis.project.dao.DatWorkorderRepository;
import com.thbs.mis.project.dao.MasClientAddressRepository;
import com.thbs.mis.project.dao.MasClientRepository;
import com.thbs.mis.project.dao.MasProjectRepository;
import com.thbs.mis.project.dao.ProjClientAddressRepository;
import com.thbs.mis.project.dao.ProjDatMilestoneTriggerDetailRepository;
import com.thbs.mis.project.dao.ProjMasClientManagerRepository;
import com.thbs.mis.project.service.WorkOrderService;

@Service
public class InvoicePOService {

@Autowired
private InvoiceDatPurchaseOrderRepository invoiceDatPORepository;

@Autowired
private InvoicePOCommonService invoicePOCommonService;

@Autowired
private WorkOrderService workOrderService;

@Autowired
private ProjMasClientManagerRepository projMasClientManagerRepository;

@Autowired
private InvoiceDatClientPoLineItemRepository invoiceDatClientPoLineItemRepository;

@Autowired
private Validator validator;

@Autowired
private InvoiceDatPoLineItemsTaxDetailRepository invoiceDatPoTaxDetailRepository;

@Autowired
private InvoiceDatMappingPoWithWoRepository invoiceDatMappingPoWithWoRepository;

@Autowired
private DatWorkorderRepository datWorkorderRepository;

@Autowired
private ProjClientAddressRepository projClientAddressRepository;

@Autowired
private EmpDetailRepository empDetailRepository;

@Autowired
private MasProjectRepository masProjectRepository;

@Autowired
private MasClientRepository masClientRepository;

@Autowired
private MasClientAddressRepository masClientAddressRepository;

@Autowired
private ProjDatMilestoneTriggerDetailRepository projDatMilestoneTriggerDetailRepository;

@Value("${finance.role.keyName}") 
private String financeRoleKeyNames;

private static final AppLog LOG = LogFactory.getLog(InvoicePOService.class);

	// Added by Shashi
	
		public List<SearchPOOutPutBean> getAllInvoicePurchaseOrders(SearchPOInputBean inputBean)
				throws DataAccessException,CommonCustomException {
			
			List<InvoiceDatPurchaseOrderBO> invoiceDatPurchaseOrderBOList = new ArrayList<>();
			List<InvoiceDatMappingPoWithWoBO> invoiceDatMappingPoWithWoBOList = new ArrayList<>();
			List<SearchPOOutPutBean> searchPOOutPutBeanList = new ArrayList<>();
	 		List<DatEmpDetailBO> datEmpDetailBOList = new ArrayList<DatEmpDetailBO>();

	 		List<String> financeNameList = Stream.of(financeRoleKeyNames.split(","))
			           .collect(Collectors.toList());
	 		try {
				datEmpDetailBOList = empDetailRepository.getAllFinanceRolesByEmpId(inputBean.getLoggedInEmpId(), financeNameList);
				
			}catch (Exception e) {
				throw new DataAccessException("Failed to fetch details from DB",e);
			}
			if(datEmpDetailBOList.isEmpty()){
				throw new CommonCustomException("Access denied since you are not an authorized person");
			}
			
			try {
				if(inputBean.getPoNumber() != null && inputBean.getPoNumber().length() !=0 && !inputBean.getPoNumber().isEmpty()) {
					
				invoiceDatPurchaseOrderBOList = invoiceDatPORepository.getAllInvoicePOByInvoicePONumber(inputBean.getPoNumber());
				
				}
				else if(inputBean.getCountryCode() != null && inputBean.getCountryCode().length() !=0 && !inputBean.getCountryCode().isEmpty())
				{
					invoiceDatPurchaseOrderBOList = invoiceDatPORepository.getAllInvoicePOByCountryCode(inputBean.getCountryCode());

				} else if(inputBean.getStartDate() != null && inputBean.getEndDate() != null) {
					
					if (inputBean.getStartDate().after(inputBean.getEndDate())) {
						throw new CommonCustomException("Start Date must be less than End Date");
					}
					else {
					invoiceDatPurchaseOrderBOList = invoiceDatPORepository.getAllInvoicePOByStartAndEndDate(inputBean.getStartDate(), inputBean.getEndDate());
					}
				}
			}catch (Exception e) {
					throw new DataAccessException(
							"Failed to retrieve list of invoice purchase order :  ", e);
				}
				if (!invoiceDatPurchaseOrderBOList.isEmpty()) {
					for (InvoiceDatPurchaseOrderBO invoiceDatPOBO : invoiceDatPurchaseOrderBOList) {
						SearchPOOutPutBean searchPOOutPutBean = new SearchPOOutPutBean();
						searchPOOutPutBean.setPoNumber(invoiceDatPOBO.getPurchaseNumber());
						searchPOOutPutBean.setPoType(invoiceDatPOBO.getPoType());
						searchPOOutPutBean.setPoValue(invoiceDatPOBO.getPoValue());
						searchPOOutPutBean.setPoUtilized(invoiceDatPOBO.getPoValue()-invoiceDatPOBO.getPoRemainingValue());
						searchPOOutPutBean.setPoBalance(invoiceDatPOBO.getPoRemainingValue());
                     
						// fetch mapped or unmapped po details
						if(invoiceDatPOBO.getFkWorkorderId() != null && invoiceDatPOBO.getFkWorkorderId()>0) {
							searchPOOutPutBean.setPoMapType(InvoicePOConstants.INVOICE_PO_MAPPED);
							
							/*invoiceDatMappingPoWithWoBOList = invoiceDatMappingPoWithWoRepository
									.findByFkPurchaseOrderIdAndFkWorkorderId(invoiceDatPOBO.getPkPurchaseOrderId(), invoiceDatPOBO.getFkWorkorderId());
							*/
							searchPOOutPutBean.setWorkOrderCount(invoiceDatMappingPoWithWoBOList.size()); 
						   //set client name 
							searchPOOutPutBean.setClientName(invoiceDatPORepository.getCleintName(invoiceDatPOBO.getFkWorkorderId()));
						
						}else {
							searchPOOutPutBean.setPoMapType(InvoicePOConstants.INVOICE_PO_UNMAPPED);

						}
						
						searchPOOutPutBean.setPoDeliveryDate(invoiceDatPOBO.getPoDeliveryDate());
						if(invoiceDatPOBO.getWithholdTaxAmount() != null) {
						searchPOOutPutBean.setWithholdTaxAmount(invoiceDatPOBO.getWithholdTaxAmount());
						}
						searchPOOutPutBean.setSurplusAmount(invoiceDatPOBO.getSurplusValue());
						searchPOOutPutBean.setCreatedOn(invoiceDatPOBO.getCreatedOn());
						searchPOOutPutBean.setUpdatedOn(invoiceDatPOBO.getUpdatedOn());
						if(invoiceDatPOBO.getUpdatedBy()!= null) {
						searchPOOutPutBean.setLastUpdatedBy(invoicePOCommonService.getEmployeeName(invoiceDatPOBO.getUpdatedBy()));
						}
						searchPOOutPutBeanList.add(searchPOOutPutBean);
					}
				} else {
					throw new CommonCustomException("No Invoice PO details Exist");
			}

			

			return searchPOOutPutBeanList;
		}

	
		public InvoicePODetailsBean viewInvoicePODetails(String InvoicePONumber,int loggedInEmpId)
				throws DataAccessException,CommonCustomException {
			ProjMasClientManagerBO projMasClientManagerBO = new ProjMasClientManagerBO();
			InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrderBO = new InvoiceDatPurchaseOrderBO();
			InvoicePODetailsBean invoicePODetailsBean = new InvoicePODetailsBean();	
			List<POLineItemsDetailsBean> poLineItemsDetailsBeanList = new ArrayList<>();
			List<InvoiceDatClientPoLineItemBO> invoiceDatClientPoLineItemBOList = new ArrayList<>();
			List<InvoiceDatPoLineItemsTaxDetailBO> invoicePoTaxDetailBOList = new ArrayList<>();
			//double totalLineItme = 0;
			List<InvoiceDatMappingPoWithWoBO> invoiceDatMappingPoWithWoBOList = new ArrayList<InvoiceDatMappingPoWithWoBO>();
			List<DatEmpDetailBO> datEmpDetailBOList = new ArrayList<DatEmpDetailBO>();

	 		List<String> financeNameList = Stream.of(financeRoleKeyNames.split(","))
			           .collect(Collectors.toList());
	 		try {
				datEmpDetailBOList = empDetailRepository.getAllFinanceRolesByEmpId(loggedInEmpId, financeNameList);
				
			}catch (Exception e) {
				throw new DataAccessException("Failed to fetch details from DB",e);
			}
			if(datEmpDetailBOList.isEmpty()){
				throw new CommonCustomException("Access denied since you are not an authorized person");
			}
			
			try {
				invoiceDatPurchaseOrderBO = invoiceDatPORepository.getPurchaseOrderDetail(InvoicePONumber);
			}catch (Exception e) {
				throw new DataAccessException(
						"Failed to retrieve list of invoice purchase order :  ", e);
			}
				
			if (invoiceDatPurchaseOrderBO != null ) {
					
					invoicePODetailsBean.setPoNumber(invoiceDatPurchaseOrderBO.getPurchaseNumber());
					invoicePODetailsBean.setPoType(invoiceDatPurchaseOrderBO.getPoType());
					invoicePODetailsBean.setPoValue(invoiceDatPurchaseOrderBO.getPoValue());
					invoicePODetailsBean.setPoCurrency(invoiceDatPurchaseOrderBO.getFkCurrencyId());
					invoicePODetailsBean.setPoUtilized(invoiceDatPurchaseOrderBO.getPoValue()-invoiceDatPurchaseOrderBO.getPoRemainingValue());
					invoicePODetailsBean.setPoBalance(invoiceDatPurchaseOrderBO.getPoRemainingValue());
					invoicePODetailsBean.setPoDeliveryDate(invoiceDatPurchaseOrderBO.getPoDeliveryDate());
					invoicePODetailsBean.setSurplusAmount(invoiceDatPurchaseOrderBO.getSurplusValue());
					invoicePODetailsBean.setPoCreatedBy(invoicePOCommonService.getEmployeeName(invoiceDatPurchaseOrderBO.getCreatedBy()));
					invoicePODetailsBean.setPoCreatedOn(invoiceDatPurchaseOrderBO.getCreatedOn());
					if(invoiceDatPurchaseOrderBO.getUpdatedBy() != null) {
					invoicePODetailsBean.setPoUpdatedBy(invoicePOCommonService.getEmployeeName(invoiceDatPurchaseOrderBO.getUpdatedBy()));
					}
					invoicePODetailsBean.setPoUpdatedOn(invoiceDatPurchaseOrderBO.getUpdatedOn());
					
					try {
						invoiceDatMappingPoWithWoBOList = invoiceDatMappingPoWithWoRepository.findByFkPurchaseOrderId(invoiceDatPurchaseOrderBO.getPkPurchaseOrderId());
					}catch (Exception e) {
						throw new DataAccessException("Failed to fetch details from DB",e);
					}
					
					
					if(!invoiceDatMappingPoWithWoBOList.isEmpty()) {
						invoicePODetailsBean.setPoMapType(InvoicePOConstants.INVOICE_PO_MAPPED);	
						//buyer details
						try {
						projMasClientManagerBO = projMasClientManagerRepository.findByPkClientManagerId(invoiceDatPurchaseOrderBO.getFkClientMgrId());
						}catch (Exception e) {
							throw new DataAccessException("Failed to fetch details from DB",e);
						}
						if(projMasClientManagerBO != null) {
						invoicePODetailsBean.setBuyerName(projMasClientManagerBO.getManagerName());
						invoicePODetailsBean.setBuyerEmailId(projMasClientManagerBO.getEmailAddress());
						}
					}else {
						invoicePODetailsBean.setPoMapType(InvoicePOConstants.INVOICE_PO_UNMAPPED);	
	
					}
					
					try {
					invoiceDatClientPoLineItemBOList = invoiceDatClientPoLineItemRepository.findByFkPurchaseOrderId(invoiceDatPurchaseOrderBO.getPkPurchaseOrderId());
					}catch (Exception e) {
						throw new DataAccessException("Failed to fetch details from DB",e);
					}
					
					if(!invoiceDatClientPoLineItemBOList.isEmpty()) {
						for(InvoiceDatClientPoLineItemBO poLineItemBO:invoiceDatClientPoLineItemBOList) {
						
						POLineItemsDetailsBean poLineItemsDetailsBean = new POLineItemsDetailsBean();
						
						poLineItemsDetailsBean.setLineItemNumber(poLineItemBO.getLineItemNumber());
						poLineItemsDetailsBean.setPoDescription(poLineItemBO.getLineItemDescription());
						poLineItemsDetailsBean.setQuantity(poLineItemBO.getQuantity());
						poLineItemsDetailsBean.setUnitCost(poLineItemBO.getUnitCost());
						poLineItemsDetailsBean.setUnitOfMeasure(poLineItemBO.getUnitOfMeasure());
						poLineItemsDetailsBean.setAmount(poLineItemBO.getAmount());
						//totalLineItme =+poLineItemBO.getAmount(); 
						poLineItemsDetailsBeanList.add(poLineItemsDetailsBean);
						}
						invoicePODetailsBean.setPoLineItemsDetailsBean(poLineItemsDetailsBeanList);
						//tax amount
						try {
						invoicePoTaxDetailBOList = invoiceDatPoTaxDetailRepository.findByFkPurchaseOrderId(invoiceDatPurchaseOrderBO.getPkPurchaseOrderId());
					
						}catch (Exception e) {
							throw new DataAccessException("Failed to fetch details from DB",e);
						}
					
					//invoicePODetailsBean.setTotalLineItem(totalLineItme);

					if(!invoicePoTaxDetailBOList.isEmpty()) {
						InvoiceDatPoLineItemsTaxDetailBO invoicePoTaxDetailBO = invoicePoTaxDetailBOList.get(0);
						invoicePODetailsBean.setTaxAddition(invoicePoTaxDetailBO.getTaxAmount());
						invoicePODetailsBean.setWithholdTaxAmount(invoicePoTaxDetailBO.getWithholdTaxAmount());
						invoicePODetailsBean.setTaxPOCurrency(invoicePoTaxDetailBO.getFkCurrencyId());

						if(invoicePoTaxDetailBO.getWithholdTaxAmount() >0) {
							invoicePODetailsBean.setIsWitholdTax("YES");
						}else {
							invoicePODetailsBean.setIsWitholdTax("NO");
						}
						invoicePODetailsBean.setTotalLineItem(invoicePoTaxDetailBO.getTotalAmountOfLineItems());
						//invoicePODetailsBean.setPoValuesGrandTotal(totalLineItme+invoicePoTaxDetailBO.getWithholdTaxAmount()+invoicePoTaxDetailBO.getTaxAmount());
						invoicePODetailsBean.setPoValuesGrandTotal(invoicePoTaxDetailBO.getGrandTotalAmount());

					}
					
				
				} else {
					throw new CommonCustomException("Invoice PO details are not exist");
				}

			}

			return invoicePODetailsBean;
		}
		

		public boolean createInvoicePOWithoutDocument(CreateInvoicePOWithoutDocInputBean crtInvoicePOInputBean)
				throws DataAccessException,CommonCustomException {
			
			InvoiceDatPurchaseOrderBO invoiceDatPOBO = new InvoiceDatPurchaseOrderBO();
			MasClientAddressBO masClientAddressBO = new MasClientAddressBO();
			boolean success = false;
			SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
			final String ALPHABATE_PATTERN = "[a-zA-Z]+";
			
			DateFormat df =new SimpleDateFormat("yyyy-MM-dd");
            Date deliveryDate = null;
            Date currentDate = null;
			List<DatEmpDetailBO> datEmpDetailBOList = new ArrayList<DatEmpDetailBO>();

	 		List<String> financeNameList = Stream.of(financeRoleKeyNames.split(","))
			           .collect(Collectors.toList());
	 		
	 		if (crtInvoicePOInputBean.getPoNumber() != null) {
				if (Pattern.matches(ALPHABATE_PATTERN, crtInvoicePOInputBean.getPoNumber())) {
					throw new CommonCustomException("Invoice PO number cannot be alphabate");
				}
			}
	 		
	 		if(crtInvoicePOInputBean.getPoValue() <= 0) {
				 throw new CommonCustomException("PO value field cannot be null");
			}
			if(crtInvoicePOInputBean.getPoDeliveryDate() == null) {
				 throw new CommonCustomException("PO delivery date field cannot be null");
			}
			if(crtInvoicePOInputBean.getSurplusAmount() <= 0) {
				 throw new CommonCustomException("Surplus Amount field cannot be null");
			}
				
	 		try {
				datEmpDetailBOList = empDetailRepository.getAllFinanceRolesByEmpId(crtInvoicePOInputBean.getPoCreatedBy(), financeNameList);
				
			}catch (Exception e) {
				throw new DataAccessException("Failed to fetch details from DB",e);
			}
			if(datEmpDetailBOList.isEmpty()){
				throw new CommonCustomException("Access denied since you are not an authorized person");
			}
			
			try {
				invoiceDatPOBO = invoiceDatPORepository.findByPurchaseNumber(crtInvoicePOInputBean.getPoNumber());
			}catch (Exception e) {
				throw new DataAccessException(
						"Failed to fetch Invoice purchase order details :  ", e);
			}
			
			try {
				masClientAddressBO = masClientAddressRepository.findByPkClientAddressId(crtInvoicePOInputBean.getClientBillToAddressId());
			}catch (Exception e) {
				throw new DataAccessException(
						"Failed to fetch cuntry code from db :  ", e);
			}
			if(masClientAddressBO.getMasCountry().getCountryCode() == null) {
				 throw new CommonCustomException("Billing address cuntry code cannot be null");
			}	
			
			try{
				 deliveryDate = df.parse(df.format(crtInvoicePOInputBean.getPoDeliveryDate()));
				 currentDate = df.parse(df.format(new Date()));
		  		} catch (Exception e) {
		  			throw new DataAccessException("Exception occured at or validate lastWorkingDate", e);
		  	    }
			
			 if(deliveryDate.getTime() < currentDate.getTime()  ) {
			  			throw new CommonCustomException("Delivery Date should be future date");
			 }
			 
			if(invoiceDatPOBO != null) {
					throw new CommonCustomException("Invoice purchase order already exist");
				}else {
					InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrderBO = new InvoiceDatPurchaseOrderBO();

					invoiceDatPurchaseOrderBO.setPurchaseNumber(crtInvoicePOInputBean.getPoNumber());
					invoiceDatPurchaseOrderBO.setDocUploadStatus(crtInvoicePOInputBean.getIsPoDocument());
					//po value and op balance both are same at time of invoice po creation
					invoiceDatPurchaseOrderBO.setPoValue(crtInvoicePOInputBean.getPoValue());
					invoiceDatPurchaseOrderBO.setPoRemainingValue(crtInvoicePOInputBean.getPoValue());
					
					invoiceDatPurchaseOrderBO.setPoType(crtInvoicePOInputBean.getPoType());
					invoiceDatPurchaseOrderBO.setFkCurrencyId(crtInvoicePOInputBean.getPoCurrency());
					invoiceDatPurchaseOrderBO.setPoDeliveryDate(crtInvoicePOInputBean.getPoDeliveryDate());
					invoiceDatPurchaseOrderBO.setSurplusValue(crtInvoicePOInputBean.getSurplusAmount());
					invoiceDatPurchaseOrderBO.setPoDescription(crtInvoicePOInputBean.getPoComments());
					invoiceDatPurchaseOrderBO.setCreatedBy(crtInvoicePOInputBean.getPoCreatedBy());
					invoiceDatPurchaseOrderBO.setCreatedOn(crtInvoicePOInputBean.getPoCreatedOn());
					invoiceDatPurchaseOrderBO.setClientBillToAddressId(crtInvoicePOInputBean.getClientBillToAddressId());
					invoiceDatPurchaseOrderBO.setClientShipToAddressId(crtInvoicePOInputBean.getClientShipToAddressId());
					invoiceDatPurchaseOrderBO.setFkClientMgrId(crtInvoicePOInputBean.getClientMgrId());
					invoiceDatPurchaseOrderBO.setCreatedOn(new Date());
					//set Prefix value
					invoiceDatPurchaseOrderBO.setPoPrefix(masClientAddressBO.getMasCountry().getCountryCode()+"/"+sdf.format(new Date()));
					//Po status is unmapped at time of invoice po creation
					invoiceDatPurchaseOrderBO.setPoStatus(InvoicePOConstants.INVOICE_PO_UNMAPPED);	
					invoiceDatPORepository.save(invoiceDatPurchaseOrderBO);
					success = true;

				}
			return success;
		}
		
		@Transactional(rollbackFor = Exception.class)
		public boolean createInvoicePOWithDocument(CreateInvoicePOWithDocInputBean crtInvoicePOInputBean)
				throws DataAccessException,CommonCustomException {
			
			InvoiceDatPurchaseOrderBO invoiceDatPOBO = new InvoiceDatPurchaseOrderBO();
			MasClientAddressBO masClientAddressBO = new MasClientAddressBO();

			//double totalAmount = 0 ;
			//double grandTatalAmount = 0;
			boolean success = false;
			final String ALPHABATE_PATTERN = "[a-zA-Z]+";
			SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
			DateFormat df =new SimpleDateFormat("yyyy-MM-dd");
			List<DatEmpDetailBO> datEmpDetailBOList = new ArrayList<DatEmpDetailBO>();
            Date deliveryDate = null;
            Date currentDate = null;
	 		List<String> financeNameList = Stream.of(financeRoleKeyNames.split(","))
			           .collect(Collectors.toList());
	 		
	 		
	 		for (POLineItemsDetailsBean bean : crtInvoicePOInputBean.getPoLineItemsDetailsBean()) {
				Set violations = validator.validate(bean);
				if (!violations.isEmpty()) {
					LOG.info("inside !violations.isEmpty() ");
					throw new ConstraintViolationException(violations);
				} 
			}
	 		if (crtInvoicePOInputBean.getPoNumber() != null) {
				if (Pattern.matches(ALPHABATE_PATTERN, crtInvoicePOInputBean.getPoNumber())) {
					throw new CommonCustomException("Invoice PO number cannot be alphabate");
				}
			}
	 		/*if(crtInvoicePOInputBean.getPoValue() <= 0) {
				 throw new CommonCustomException("PO value field cannot be null");
			}*/
			if(crtInvoicePOInputBean.getPoDeliveryDate() == null) {
				 throw new CommonCustomException("PO delivery date field cannot be null");
			}
			if(crtInvoicePOInputBean.getSurplusAmount() <= 0) {
				 throw new CommonCustomException("Surplus Amount field cannot be null");
			}
			if(crtInvoicePOInputBean.getTotalLineItemAmount() <= 0) {
				 throw new CommonCustomException("Total Line Item Amount field cannot be null");
			}
			if(crtInvoicePOInputBean.getGrandTotalAmount() <= 0) {
				 throw new CommonCustomException("Grand Total Amount field cannot be null");
			}
			if(crtInvoicePOInputBean.getTaxAddition() <= 0) {
				 throw new CommonCustomException("Tax Addition field cannot be null");
			}
			if("YES".equalsIgnoreCase(crtInvoicePOInputBean.getIsWithholdTax()) && crtInvoicePOInputBean.getWithholdTaxAmount() <= 0) {
				 throw new CommonCustomException("Withhold Tax Amount field cannot be null");
			}
	 		
			 try{
				 deliveryDate = df.parse(df.format(crtInvoicePOInputBean.getPoDeliveryDate()));
				 currentDate = df.parse(df.format(new Date()));
		  		} catch (Exception e) {
		  			throw new DataAccessException("Exception occured at or validate lastWorkingDate", e);
		  	    }
			
			 if(deliveryDate.getTime() < currentDate.getTime()  ) {
			  			throw new CommonCustomException("Delivery Date should be future date");
			 }
			 
	 		try {
				datEmpDetailBOList = empDetailRepository.getAllFinanceRolesByEmpId(crtInvoicePOInputBean.getPoCreatedBy(), financeNameList);
				
			}catch (Exception e) {
				throw new DataAccessException("Failed to fetch details from DB",e);
			}
			if(datEmpDetailBOList.isEmpty()){
				throw new CommonCustomException("Access denied since you are not an authorized person");
			}
								
			try {
				invoiceDatPOBO = invoiceDatPORepository.findByPurchaseNumber(crtInvoicePOInputBean.getPoNumber());
			
			}catch (Exception e) {
				throw new DataAccessException(
						"Failed to fetch Invoice purchase order details :  ", e);
			}
			try {
				masClientAddressBO = masClientAddressRepository.findByPkClientAddressId(crtInvoicePOInputBean.getClientBillToAddressId());
			}catch (Exception e) {
				throw new DataAccessException(
						"Failed to fetch cuntry code from db :  ", e);
			}
			if(masClientAddressBO.getMasCountry().getCountryCode() == null) {
				 throw new CommonCustomException("Billing address cuntry code cannot be null");
			}
			if(invoiceDatPOBO != null) {
					throw new CommonCustomException("Invoice purchase order already exist");
				}else {
					InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrderBO = new InvoiceDatPurchaseOrderBO();
						 
					invoiceDatPurchaseOrderBO.setPurchaseNumber(crtInvoicePOInputBean.getPoNumber());
					invoiceDatPurchaseOrderBO.setDocUploadStatus(crtInvoicePOInputBean.getIsPoDocument());
					invoiceDatPurchaseOrderBO.setPoType(crtInvoicePOInputBean.getPoType());
					//po value and op balance both are same at time of invoice po creation
					invoiceDatPurchaseOrderBO.setPoValue(crtInvoicePOInputBean.getGrandTotalAmount());
					invoiceDatPurchaseOrderBO.setPoRemainingValue(crtInvoicePOInputBean.getGrandTotalAmount());
					invoiceDatPurchaseOrderBO.setFkCurrencyId(crtInvoicePOInputBean.getPoValueCurrencyType());
					invoiceDatPurchaseOrderBO.setPoDeliveryDate(crtInvoicePOInputBean.getPoDeliveryDate());
					invoiceDatPurchaseOrderBO.setSurplusValue(crtInvoicePOInputBean.getSurplusAmount());
					invoiceDatPurchaseOrderBO.setPoDescription(crtInvoicePOInputBean.getPoComments());
					invoiceDatPurchaseOrderBO.setCreatedBy(crtInvoicePOInputBean.getPoCreatedBy());
					invoiceDatPurchaseOrderBO.setCreatedOn(crtInvoicePOInputBean.getPoCreatedOn());
					invoiceDatPurchaseOrderBO.setClientBillToAddressId(crtInvoicePOInputBean.getClientBillToAddressId());
					invoiceDatPurchaseOrderBO.setClientShipToAddressId(crtInvoicePOInputBean.getClientShipToAddressId());
					invoiceDatPurchaseOrderBO.setFkClientMgrId(crtInvoicePOInputBean.getClientMgrId());
					invoiceDatPurchaseOrderBO.setCreatedOn(new Date());
					//set Prefix value
					invoiceDatPurchaseOrderBO.setPoPrefix(masClientAddressBO.getMasCountry().getCountryCode()+"/"+sdf.format(new Date()));
					//Po status is unmapped at time of invoice po creation
					invoiceDatPurchaseOrderBO.setPoStatus(InvoicePOConstants.INVOICE_PO_UNMAPPED);	
					try {
					invoiceDatPORepository.save(invoiceDatPurchaseOrderBO);
					}catch (Exception e) {
						 throw new DataAccessException("Failed while to save data in DB",e);
					}
					
					//Submission of PO Line items details
					InvoiceDatClientPoLineItemBO invoicedatClientPoLineItemBO = null;
					for(POLineItemsDetailsBean poLineItemsDetailsBean : crtInvoicePOInputBean.getPoLineItemsDetailsBean()) {
						invoicedatClientPoLineItemBO = new InvoiceDatClientPoLineItemBO();
						if(poLineItemsDetailsBean.getAmount() <= 0) {
							 throw new CommonCustomException("Amount field cannot be null");
						}
						if(poLineItemsDetailsBean.getQuantity() <= 0) {
							 throw new CommonCustomException("Quantity field cannot be null");
						}
						if(poLineItemsDetailsBean.getUnitCost() <= 0) {
							 throw new CommonCustomException("Unit Cost field cannot be null");
						}
						
						invoicedatClientPoLineItemBO.setFkPurchaseOrderId(invoiceDatPurchaseOrderBO.getPkPurchaseOrderId());
						invoicedatClientPoLineItemBO.setLineItemNumber(poLineItemsDetailsBean.getLineItemNumber());
						invoicedatClientPoLineItemBO.setLineItemDescription(poLineItemsDetailsBean.getPoDescription());
						invoicedatClientPoLineItemBO.setUnitOfMeasure(poLineItemsDetailsBean.getUnitOfMeasure());
						invoicedatClientPoLineItemBO.setQuantity(poLineItemsDetailsBean.getQuantity());
						//multiplication b/w quantity and unit cost for Amount
						//invoicedatClientPoLineItemBO.setAmount((poLineItemsDetailsBean.getQuantity()*poLineItemsDetailsBean.getUnitCost()));
						invoicedatClientPoLineItemBO.setAmount(poLineItemsDetailsBean.getAmount());
						invoicedatClientPoLineItemBO.setUnitCost(poLineItemsDetailsBean.getUnitCost());
						invoicedatClientPoLineItemBO.setUpdatedBy(crtInvoicePOInputBean.getPoCreatedBy());
						invoicedatClientPoLineItemBO.setUpdatedOn(new Date());
						//Total amount
						//totalAmount = +poLineItemsDetailsBean.getQuantity()*poLineItemsDetailsBean.getUnitCost();
						try {
						invoiceDatClientPoLineItemRepository.save(invoicedatClientPoLineItemBO);
						}catch (Exception e) {
							 throw new DataAccessException("Failed while to save data in DB",e);
						}
					}
					//Submission of Tax details
					InvoiceDatPoLineItemsTaxDetailBO invoiceDatPoLineItemsTaxDetailBO = new InvoiceDatPoLineItemsTaxDetailBO();
					
					invoiceDatPoLineItemsTaxDetailBO.setFkPurchaseOrderId(invoiceDatPurchaseOrderBO.getPkPurchaseOrderId());
					invoiceDatPoLineItemsTaxDetailBO.setWithholdTaxAmount(crtInvoicePOInputBean.getWithholdTaxAmount());
					invoiceDatPoLineItemsTaxDetailBO.setTaxAmount(crtInvoicePOInputBean.getTaxAddition());
					invoiceDatPoLineItemsTaxDetailBO.setTotalAmountOfLineItems(crtInvoicePOInputBean.getTotalLineItemAmount());
					//Grand total amount
					//grandTatalAmount = totalAmount+crtInvoicePOInputBean.getTaxAddition()+crtInvoicePOInputBean.getWitholdTaxAmount();
					invoiceDatPoLineItemsTaxDetailBO.setGrandTotalAmount(crtInvoicePOInputBean.getGrandTotalAmount());
					//invoiceDatPoLineItemsTaxDetailBO.setFkCurrencyId(crtInvoicePOInputBean.getPoTaxCurrency());
					invoiceDatPoLineItemsTaxDetailBO.setFkCurrencyId(crtInvoicePOInputBean.getPoValueCurrencyType());
					invoiceDatPoLineItemsTaxDetailBO.setCreatedBy(crtInvoicePOInputBean.getPoCreatedBy());
					invoiceDatPoLineItemsTaxDetailBO.setUpdatedBy(crtInvoicePOInputBean.getPoCreatedBy());
					invoiceDatPoLineItemsTaxDetailBO.setCreatedOn(new Date());
					try {
						invoiceDatPoTaxDetailRepository.save(invoiceDatPoLineItemsTaxDetailBO);
					}catch (Exception e) {
						 throw new DataAccessException("Failed while to save data in DB",e);
					}
					success = true;

				}

			

			return success;
		}
		
		@Transactional(rollbackFor = Exception.class)
		public boolean updateInvoicePO(UpdateInvoicePOWithDocInputBean updInvoicePOInputBean)
				throws DataAccessException,CommonCustomException {
			
			InvoiceDatPurchaseOrderBO invoiceDatPOBO = new InvoiceDatPurchaseOrderBO();
			List<InvoiceDatMappingPoWithWoBO> invcDatMappingPoWithWoBOList = new ArrayList<>();
			//double totalAmount = 0 ;
			//double grandTatalAmount = 0;
			DateFormat df =new SimpleDateFormat("yyyy-MM-dd");
            Date deliveryDate = null;
            Date currentDate = null;
			boolean success = false;
			List<DatEmpDetailBO> datEmpDetailBOList = new ArrayList<DatEmpDetailBO>();

	 		List<String> financeNameList = Stream.of(financeRoleKeyNames.split(","))
			           .collect(Collectors.toList());
	 		try {
				datEmpDetailBOList = empDetailRepository.getAllFinanceRolesByEmpId(updInvoicePOInputBean.getPoUpdatedBy(), financeNameList);
				
			}catch (Exception e) {
				throw new DataAccessException("Failed to fetch details from DB",e);
			}
			if(datEmpDetailBOList.isEmpty()){
				throw new CommonCustomException("Access denied since you are not an authorized person");
			}
			if(InvoicePOConstants.INVOICE_PO_UPLOADED.equalsIgnoreCase(updInvoicePOInputBean.getIsPoDocument())) {
				throw new CommonCustomException("Please Upload the document");
			}
			
			try{
				 deliveryDate = df.parse(df.format(updInvoicePOInputBean.getPoDeliveryDate()));
				 currentDate = df.parse(df.format(new Date()));
		  		} catch (Exception e) {
		  			throw new DataAccessException("Exception occured at or validate lastWorkingDate", e);
		  	    }
			
			 if(deliveryDate.getTime() < currentDate.getTime()  ) {
			  			throw new CommonCustomException("Delivery Date should be future date");
			 }
			 
			try {
				invoiceDatPOBO = invoiceDatPORepository.findByPurchaseNumber(updInvoicePOInputBean.getPoNumber());
			}catch (Exception e) {
				 throw new DataAccessException("Failed while to fetch data from DB",e);
			}
				
			if(invoiceDatPOBO == null) {
					
				throw new CommonCustomException("Invoice PO data is not exist");
				
			}else {
				
				try {
					invcDatMappingPoWithWoBOList = invoiceDatMappingPoWithWoRepository.findByFkPurchaseOrderId(invoiceDatPOBO.getPkPurchaseOrderId());
				}catch (Exception e) {
					 throw new DataAccessException("Failed while to fetch data from DB",e);
				}
				
					InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrderBO = new InvoiceDatPurchaseOrderBO();
						 
					invoiceDatPurchaseOrderBO.setDocUploadStatus(updInvoicePOInputBean.getIsPoDocument());
					invoiceDatPurchaseOrderBO.setPoValue(invoiceDatPOBO.getPoValue()+updInvoicePOInputBean.getGrandTotalAmount());
					invoiceDatPurchaseOrderBO.setPoRemainingValue(invoiceDatPOBO.getPoRemainingValue()+updInvoicePOInputBean.getGrandTotalAmount());
					invoiceDatPurchaseOrderBO.setFkCurrencyId(updInvoicePOInputBean.getPoValueCurrencyType());
					if(InvoicePOConstants.INVOICE_AGGREGATE_PO.equalsIgnoreCase(updInvoicePOInputBean.getPoType())) {
					invoiceDatPurchaseOrderBO.setPoType(updInvoicePOInputBean.getPoType());
					}else {
						throw new CommonCustomException("PO type should be Aggregate PO only");
					}
					invoiceDatPurchaseOrderBO.setPoDeliveryDate(updInvoicePOInputBean.getPoDeliveryDate());
					invoiceDatPurchaseOrderBO.setSurplusValue(updInvoicePOInputBean.getSurplusAmount());
					invoiceDatPurchaseOrderBO.setCommentsForUpdation(updInvoicePOInputBean.getPoUpdationComments());
					invoiceDatPurchaseOrderBO.setUpdatedBy(updInvoicePOInputBean.getPoUpdatedBy());
					invoiceDatPurchaseOrderBO.setUpdatedOn(updInvoicePOInputBean.getPoUpdatedOn());
					if(invcDatMappingPoWithWoBOList.isEmpty()) {
					invoiceDatPurchaseOrderBO.setClientBillToAddressId(updInvoicePOInputBean.getClientBillToAddressIdId());
					invoiceDatPurchaseOrderBO.setClientShipToAddressId(updInvoicePOInputBean.getClientShipToAddressIdId());
					invoiceDatPurchaseOrderBO.setFkClientMgrId(updInvoicePOInputBean.getClientMgrId());
					}
                 try {
					invoiceDatPORepository.save(invoiceDatPurchaseOrderBO);
				}catch (Exception e) {
					 throw new DataAccessException("Failed while to save data in DB",e);
				}
              
                 //updation of PO Line items details
                 try {
					
					InvoiceDatClientPoLineItemBO invoicedatClientPoLineItemBO = new InvoiceDatClientPoLineItemBO();
					for(POLineItemsDetailsBean poLineItemsDetailsBean : updInvoicePOInputBean.getPoLineItemsDetailsBean()) {
                      
						invoicedatClientPoLineItemBO.setFkPurchaseOrderId(invoiceDatPurchaseOrderBO.getPkPurchaseOrderId());
						invoicedatClientPoLineItemBO.setLineItemNumber(poLineItemsDetailsBean.getLineItemNumber());
						invoicedatClientPoLineItemBO.setLineItemDescription(poLineItemsDetailsBean.getPoDescription());
						invoicedatClientPoLineItemBO.setUnitOfMeasure(poLineItemsDetailsBean.getUnitOfMeasure());
						invoicedatClientPoLineItemBO.setQuantity(poLineItemsDetailsBean.getQuantity());
						//multiplication b/w quantity and unit cost for Amount
						invoicedatClientPoLineItemBO.setAmount(poLineItemsDetailsBean.getAmount());
						invoicedatClientPoLineItemBO.setUnitCost(poLineItemsDetailsBean.getUnitCost());
						//Total amount
						//totalAmount = +poLineItemsDetailsBean.getQuantity()*poLineItemsDetailsBean.getUnitCost();
						
						invoiceDatClientPoLineItemRepository.save(invoicedatClientPoLineItemBO);
					}
				}catch (Exception e) {
					 throw new DataAccessException("Failed while to save data in DB",e);
				}
					
                 //updation of Tax details
					InvoiceDatPoLineItemsTaxDetailBO invoiceDatPoLineItemsTaxDetailBO = new InvoiceDatPoLineItemsTaxDetailBO();
					
					invoiceDatPoLineItemsTaxDetailBO.setFkPurchaseOrderId(invoiceDatPurchaseOrderBO.getPkPurchaseOrderId());
					invoiceDatPoLineItemsTaxDetailBO.setWithholdTaxAmount(updInvoicePOInputBean.getWitholdTaxAmount());
					invoiceDatPoLineItemsTaxDetailBO.setTaxAmount(updInvoicePOInputBean.getTaxAddition());
					invoiceDatPoLineItemsTaxDetailBO.setTotalAmountOfLineItems(updInvoicePOInputBean.getTotalLineItemAmount());
					//Grand total amount
					///grandTatalAmount = totalAmount+updInvoicePOInputBean.getTaxAddition()+updInvoicePOInputBean.getWitholdTaxAmount();
					invoiceDatPoLineItemsTaxDetailBO.setGrandTotalAmount(updInvoicePOInputBean.getGrandTotalAmount());
					invoiceDatPoLineItemsTaxDetailBO.setFkCurrencyId(updInvoicePOInputBean.getPoValueCurrencyType());
					invoiceDatPoLineItemsTaxDetailBO.setUpdatedBy(updInvoicePOInputBean.getPoUpdatedBy());
					invoiceDatPoLineItemsTaxDetailBO.setUpdatedOn(updInvoicePOInputBean.getPoUpdatedOn());
				
					try {
					invoiceDatPoTaxDetailRepository.save(invoiceDatPoLineItemsTaxDetailBO);
				  }catch (Exception e) {
					 throw new DataAccessException("Failed while to save data in DB",e);
				}
					
				}

			return success;
		}
		
		public boolean archiveInvoicePO(String invoicePONumber, int loggedInEmpId)
				throws DataAccessException,CommonCustomException {
			
			InvoiceDatPurchaseOrderBO invoiceDatPOBO = new InvoiceDatPurchaseOrderBO();
			boolean success = false;
			List<DatEmpDetailBO> datEmpDetailBOList = new ArrayList<DatEmpDetailBO>();

	 		List<String> financeNameList = Stream.of(financeRoleKeyNames.split(","))
			           .collect(Collectors.toList());
	 		try {
				datEmpDetailBOList = empDetailRepository.getAllFinanceRolesByEmpId(loggedInEmpId, financeNameList);
				
			}catch (Exception e) {
				throw new DataAccessException("Failed to fetch details from DB",e);
			}
			if(datEmpDetailBOList.isEmpty()){
				throw new CommonCustomException("Access denied since you are not an authorized person");
			}
			
			
			try {
				invoiceDatPOBO = invoiceDatPORepository.findByPurchaseNumber(invoicePONumber);
			}catch (Exception e) {
				throw new DataAccessException("Failed to fetch details from DB",e);
			} 
				
			if(invoiceDatPOBO == null) {
					throw new CommonCustomException("Invoice purchase order is not exist");
				}
				
			if(InvoicePOConstants.INVOICE_PO_ARCHIVED.equalsIgnoreCase(invoiceDatPOBO.getPoStatus())) {
					throw new CommonCustomException("Invoice purchase order is already archived");
				}
				
					InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrderBO = new InvoiceDatPurchaseOrderBO();
					invoiceDatPurchaseOrderBO.setPkPurchaseOrderId(invoiceDatPOBO.getPkPurchaseOrderId());
					invoiceDatPurchaseOrderBO.setPoStatus(InvoicePOConstants.INVOICE_PO_ARCHIVED);
					
					try {	
					invoiceDatPORepository.save(invoiceDatPurchaseOrderBO);
		    	
					}catch (Exception e) {
				     throw new DataAccessException(
						"Failed to archive Invoice purchase order :  ", e);
			        }

		    	return success;
		}
		
		// EOA by Shashi	
		
		//Added By Balaji 
		
		public List<FilterPurchaseOrderOutputBean> searchFilteredPurchaseOrders(
				FilterPurchaseOrderInputBean filterPOInputBean) throws CommonCustomException, DataAccessException {
			
			LOG.entering("searchFilteredPurchaseOrders");
			
	    	DatEmpDetailBO empDetailBO = new DatEmpDetailBO();
	 		List<FilterPurchaseOrderOutputBean> filterPoOutputBeanList = new ArrayList<FilterPurchaseOrderOutputBean>();
	 		//FilterPurchaseOrderOutputBean filterPurchaseOrderOutputBean = new FilterPurchaseOrderOutputBean();
	 		List<InvoiceDatPurchaseOrderBO> invoiceDatPurchaseBOList = new ArrayList<InvoiceDatPurchaseOrderBO>();
	 		//InvoiceDatPurchaseOrderBO invoiceDatPurchaseOrderBO = new InvoiceDatPurchaseOrderBO();
	 		List<DatEmpDetailBO> datEmpDetailBOList = new ArrayList<DatEmpDetailBO>();
	 		Specification<InvoiceDatPurchaseOrderBO> purchaseOrderFilterSpecification = null;
			List<InvoiceDatMappingPoWithWoBO> invoiceDatMappingPoWithWoBO = new ArrayList<InvoiceDatMappingPoWithWoBO>();
	 		List<String> financeNameList = Stream.of(financeRoleKeyNames.split(","))
			           .collect(Collectors.toList());
	 		 		
	 		Integer empId = null;
			String empName = null;
	 		Boolean hasAccess = false;
			Date startDate = null;
			Date endDate = null;
			List<Integer> poIdList = new ArrayList<Integer>();
			
		     //get Active employee id from dat_emp_detail table
			try {
				System.out.println("Filter Input bean value of logged in emp id is: "+filterPOInputBean.getLoggedInEmpId());
				empDetailBO = empDetailRepository.findByPkEmpId(filterPOInputBean.getLoggedInEmpId());
				LOG.info("Get Employee id by empId"+ empDetailBO);
			}catch(Exception e){
				throw new DataAccessException("Failed to fetch Employee id from DB");
			}if (empDetailBO == null) {
				throw new CommonCustomException("Invalid Employee ID.");
			}
			//get employee personal details by empId
	 		
			try {
				datEmpDetailBOList = empDetailRepository.getAllFinanceRolesByEmpId(filterPOInputBean.getLoggedInEmpId(), financeNameList);
				
			}catch (Exception e) {
				throw new DataAccessException("Failed to fetch details from DB");
			}
			if(datEmpDetailBOList.isEmpty()){
				throw new DataAccessException("You are not an Authorized User");
			}
			
			if(!datEmpDetailBOList.isEmpty()){
				hasAccess = true;
			}
			if(filterPOInputBean.getClientId() != null)
			{
			try{
	 			invoiceDatPurchaseBOList = invoiceDatPORepository.getPODetailsByClientId(filterPOInputBean.getClientId());
	 		}catch(Exception e){
	 			throw new DataAccessException("Failed to fetch details from masClientBO");
	 		}
			
			try{
				 invoiceDatMappingPoWithWoBO = invoiceDatMappingPoWithWoRepository.findByFkPurchaseOrderId(invoiceDatPurchaseBOList.get(0).getPkPurchaseOrderId());
		 			System.out.println("Coming inside invoiceDatMappingPoWithWoBO@@@@@@@@@@@@@ ");
		 			System.out.println("invoiceDatMappingPoWithWoBO.get(0).getFkWorkorderId() "+invoiceDatMappingPoWithWoBO.size());

				}
				catch(Exception e){
					throw new DataAccessException("Failed to fetch details from invoiceDatMappingPoWithWoBO DB" );
				}
			
	 		if(invoiceDatPurchaseBOList.isEmpty()){
	 			throw new CommonCustomException("No Details found in invoiceDatPurchaseOrderBO");
	 		}
			}
	 		
	 		if (filterPOInputBean.getPoStartDate() != null || filterPOInputBean.getPoEndDate() != null) {
				startDate = DateTimeUtil.dateWithMinTimeofDay(filterPOInputBean.getPoStartDate());
				endDate = DateTimeUtil.dateWithMaxTimeofDay( filterPOInputBean.getPoEndDate());
				filterPOInputBean.setPoStartDate(startDate);
				filterPOInputBean.setPoEndDate(endDate);
			}
	 		if (!invoiceDatPurchaseBOList.isEmpty() || hasAccess==true) {
	 			purchaseOrderFilterSpecification = PurchaseOrderFilterSpecification.getPurchaseOrderStatusAndDateRange(filterPOInputBean, invoiceDatPurchaseBOList);
	 			invoiceDatPurchaseBOList = invoiceDatPORepository.findAll(purchaseOrderFilterSpecification);
	 			
	 				 	for(InvoiceDatPurchaseOrderBO invoicePurchaseOrderBO : invoiceDatPurchaseBOList){
	 				 		FilterPurchaseOrderOutputBean purchaseOrderOutputBean = new FilterPurchaseOrderOutputBean();
	 				 		List<InvoiceDatPoLineItemsTaxDetailBO> invoiceDatPoLineItemsTaxDetailBOList = new ArrayList<InvoiceDatPoLineItemsTaxDetailBO>();
	 				 		
	 				 		try{
	 				 			invoiceDatPoLineItemsTaxDetailBOList = invoiceDatPoTaxDetailRepository.findByFkPurchaseOrderId(invoicePurchaseOrderBO.getPkPurchaseOrderId());
	 				 			System.out.println("invoiceDatPoLineItemsTaxDetailBOList"+invoiceDatPoLineItemsTaxDetailBOList);
	 						}
	 						catch (Exception e) {
	 							System.out.println("invoicePurchaseOrderBO.getPkPurchaseOrderId(): "+invoicePurchaseOrderBO.getPkPurchaseOrderId());
	 							throw new DataAccessException("Failed to fetch details from DB",e);
	 						} 
	 				 		for(InvoiceDatPoLineItemsTaxDetailBO invoiceDatPoLineItemsTaxDetailBO : invoiceDatPoLineItemsTaxDetailBOList){
	 				 		
	 				 		purchaseOrderOutputBean.setClientName(invoicePurchaseOrderBO.getMasClientBOBillAddress().getClientName());	//Have to get client name
	 				 		purchaseOrderOutputBean.setPoNumber(invoicePurchaseOrderBO.getPoPrefix()+""+invoicePurchaseOrderBO.getPurchaseNumber());
	 				 		purchaseOrderOutputBean.setWorkOrderCount(0);  // Have to get workorder count
	 				 		purchaseOrderOutputBean.setPoType(invoicePurchaseOrderBO.getPoType());
	 				 		purchaseOrderOutputBean.setPoValue(invoicePurchaseOrderBO.getPoValue());
	 				 		purchaseOrderOutputBean.setPoUtilized(invoicePurchaseOrderBO.getPoValue()-invoicePurchaseOrderBO.getPoRemainingValue());
	 				 		purchaseOrderOutputBean.setPoBalance(invoicePurchaseOrderBO.getPoRemainingValue());
	 				 		purchaseOrderOutputBean.setPoMapType(invoicePurchaseOrderBO.getPoStatus());
	 				 		purchaseOrderOutputBean.setPoDeliveryDate(invoicePurchaseOrderBO.getPoDeliveryDate());
	 				 		
	 				 		purchaseOrderOutputBean.setWitholdTaxAmount(invoiceDatPoLineItemsTaxDetailBO.getWithholdTaxAmount());
	 				 		purchaseOrderOutputBean.setSurplusAmount(invoicePurchaseOrderBO.getSurplusValue());
	 				 		purchaseOrderOutputBean.setUpdatedOn(invoicePurchaseOrderBO.getUpdatedOn());
	 				 		purchaseOrderOutputBean.setLastUpdatedBy(invoicePurchaseOrderBO.getUpdatedBy());
	 				 		purchaseOrderOutputBean.setCreatedOn(invoicePurchaseOrderBO.getCreatedOn());
	 				 		
	 				 		filterPoOutputBeanList.add(purchaseOrderOutputBean);
	 				 		}
	 				 	}
	 			
	 			// set values for output bean
	 			//filterPurchaseOrderOutputBean.setClientName();
	 			
			} else {
				if  (invoiceDatPurchaseBOList.isEmpty() || hasAccess==true) {
					purchaseOrderFilterSpecification = PurchaseOrderFilterSpecification.getPurchaseOrderStatusAndDateRange(filterPOInputBean, 
							invoiceDatPurchaseBOList);
					invoiceDatPurchaseBOList = invoiceDatPORepository.findAll(purchaseOrderFilterSpecification);
					
				 	for(InvoiceDatPurchaseOrderBO invoicePurchaseOrderBO : invoiceDatPurchaseBOList){
 				 		FilterPurchaseOrderOutputBean purchaseOrderOutputBean = new FilterPurchaseOrderOutputBean();
 				 		List<InvoiceDatPoLineItemsTaxDetailBO> invoiceDatPoLineItemsTaxDetailBOList = new ArrayList<InvoiceDatPoLineItemsTaxDetailBO>();
 				 		
 				 		try{
 				 			invoiceDatPoLineItemsTaxDetailBOList = invoiceDatPoTaxDetailRepository.findByFkPurchaseOrderId(invoicePurchaseOrderBO.getPkPurchaseOrderId());
 						}
 						catch (Exception e) {
 							throw new DataAccessException("Failed to fetch details from DB");
 						} 
 				 		for(InvoiceDatPoLineItemsTaxDetailBO invoiceDatPoLineItemsTaxDetailBO : invoiceDatPoLineItemsTaxDetailBOList){
 				 		purchaseOrderOutputBean.setClientName(" ");	//Have to get client name
 				 		purchaseOrderOutputBean.setPoNumber(invoicePurchaseOrderBO.getPoPrefix()+""+invoicePurchaseOrderBO.getPurchaseNumber());
 				 		purchaseOrderOutputBean.setWorkOrderCount(0);  // Have to get workorder count
 				 		purchaseOrderOutputBean.setPoType(invoicePurchaseOrderBO.getPoType());
 				 		purchaseOrderOutputBean.setPoValue(invoicePurchaseOrderBO.getPoValue());
 				 		purchaseOrderOutputBean.setPoUtilized(invoicePurchaseOrderBO.getPoValue()-invoicePurchaseOrderBO.getPoRemainingValue());
 				 		purchaseOrderOutputBean.setPoBalance(invoicePurchaseOrderBO.getPoRemainingValue());
 				 		purchaseOrderOutputBean.setPoMapType(invoicePurchaseOrderBO.getPoStatus());
 				 		purchaseOrderOutputBean.setPoDeliveryDate(invoicePurchaseOrderBO.getPoDeliveryDate());
 				 		
 				 		purchaseOrderOutputBean.setWitholdTaxAmount(invoiceDatPoLineItemsTaxDetailBO.getWithholdTaxAmount());
 				 		purchaseOrderOutputBean.setSurplusAmount(invoicePurchaseOrderBO.getSurplusValue());
 				 		purchaseOrderOutputBean.setUpdatedOn(invoicePurchaseOrderBO.getUpdatedOn());
 				 		purchaseOrderOutputBean.setLastUpdatedBy(invoicePurchaseOrderBO.getUpdatedBy());
 				 		purchaseOrderOutputBean.setCreatedOn(invoicePurchaseOrderBO.getCreatedOn());
 				 		
 				 		filterPoOutputBeanList.add(purchaseOrderOutputBean);
 				 		}
 				 	}
					
					//set values for output bean
					
				} else {
					if(hasAccess==true){
						//set values for output bean
						return filterPoOutputBeanList;
					}
					
				}
			}
	 		
	 		
			return filterPoOutputBeanList;
		}
		
		
		public InvoiceDatPurchaseOrderBO shiftingNormalPOtoAggregatePO(
				InvoiceShiftingPOInputBean invoiceShiftingPOInputBean) throws CommonCustomException, DataAccessException {
			
			DatEmpDetailBO empDetailBO = new DatEmpDetailBO();
	 		List<DatEmpDetailBO> datEmpDetailBOList = new ArrayList<DatEmpDetailBO>();
	 		List<String> financeNameList = Stream.of(financeRoleKeyNames.split(","))
			           .collect(Collectors.toList());
			InvoiceDatPurchaseOrderBO invoiceDatPurchaseBO = new InvoiceDatPurchaseOrderBO();
			List<InvoiceDatMappingPoWithWoBO> invoiceDatMappingPoWithWoBO = new ArrayList<InvoiceDatMappingPoWithWoBO>();
			DatWorkorderBO workorderBO = new DatWorkorderBO();
			MasProjectBO masProjectBO = new MasProjectBO();
			MasClientBO masClientBO = new MasClientBO();
			
			Boolean hasAccess = false;
			
			  //get Active employee id from dat_emp_detail table
			try {
				empDetailBO = empDetailRepository.findByPkEmpId(invoiceShiftingPOInputBean.getLoggedInEmpId());
				LOG.info("Get Employee id by empId"+ empDetailBO);
			}catch(Exception e){
				throw new CommonCustomException("Failed to fetch Employee id from DB");
			}if (empDetailBO == null) {
				throw new CommonCustomException("Invalid Employee ID.");
			}
			//get employee personal details by empId
	 		
			try {
				datEmpDetailBOList = empDetailRepository.getAllFinanceRolesByEmpId(invoiceShiftingPOInputBean.getLoggedInEmpId(), financeNameList);
				
			}catch (Exception e) {
				throw new DataAccessException("Failed to fetch details from DB");
			}
			
			try{
				
				System.out.println("Coming inside invoiceDatPurchaseBO!!!!!!!!! "+invoiceShiftingPOInputBean.getPurchaseOrderId());
	 			invoiceDatPurchaseBO = invoiceDatPORepository.findByPkPurchaseOrderId(invoiceShiftingPOInputBean.getPurchaseOrderId());
	 			
	 		}catch(Exception e){
	 			throw new CommonCustomException("Failed to fetch details from invoiceDatPurchaseBO");
	 		}
			if (invoiceDatPurchaseBO == null) {
				throw new CommonCustomException("Invalid Purchase Order ID.");
			}
			 try{
				 invoiceDatMappingPoWithWoBO = invoiceDatMappingPoWithWoRepository.findByFkPurchaseOrderId(invoiceShiftingPOInputBean.getPurchaseOrderId());
		 			System.out.println("Coming inside invoiceDatMappingPoWithWoBO@@@@@@@@@@@@@ ");
		 			System.out.println("invoiceDatMappingPoWithWoBO.get(0).getFkWorkorderId() "+invoiceDatMappingPoWithWoBO.size());

				}
				catch(Exception e){
					throw new CommonCustomException("Failed to fetch details from invoiceDatMappingPoWithWoBO DB" );
				}
			 if(invoiceDatMappingPoWithWoBO != null){
				 
			 
			 try{
					workorderBO = datWorkorderRepository.findByPkWorkorderId(invoiceDatMappingPoWithWoBO.get(0).getFkWorkorderId());
				}
				catch(Exception e){
					throw new CommonCustomException("Failed to fetch details from workorderBO DB" );
				}
			 try{
				 masProjectBO =  masProjectRepository.findByPkProjectId(workorderBO.getFkProjectId());
				}
				catch(Exception e){
					throw new CommonCustomException("Failed to fetch details from masProjectBO DB" );
				}
			 
			 try{
				 masClientBO =  masClientRepository.findByPkClientId(masProjectBO.getFkClientId());
				}
				catch(Exception e){
					throw new CommonCustomException("Failed to fetch details from masClientBO DB" );
				}
		}
			 if(!datEmpDetailBOList.isEmpty()){
					hasAccess = true;
				
		if("AGGREGATE".equals(masClientBO.getPoType()) && "YES".equals(masProjectBO.getIsHavingAggregatePo())){
			
			System.out.println("Entering Client APO check: "+masClientBO.getPoType());
			System.out.println("Entering Project APO check: "+masProjectBO.getIsHavingAggregatePo());

			if("NORMAL".equals(invoiceDatPurchaseBO.getPoType())){
				
				if(!invoiceDatMappingPoWithWoBO.isEmpty()){
					
					if(invoiceDatPurchaseBO.getPoValue().intValue() > workorderBO.getSowAmount().intValue() ){
						
						invoiceDatPurchaseBO.setPoType("AGGREGATE");
						invoiceDatPurchaseBO.setUpdatedBy(invoiceShiftingPOInputBean.getLoggedInEmpId());
						invoiceDatPurchaseBO.setUpdatedOn(new Date());
						invoiceDatPORepository.save(invoiceDatPurchaseBO);
						
					}
					else{
						throw new CommonCustomException("PO Value should be greater than WO Sow amount" );
					}
					
				}
					invoiceDatPurchaseBO.setPoType("AGGREGATE");
					invoiceDatPurchaseBO.setUpdatedBy(invoiceShiftingPOInputBean.getLoggedInEmpId());
					invoiceDatPurchaseBO.setUpdatedOn(new Date());
					invoiceDatPORepository.save(invoiceDatPurchaseBO);
				
			}else{
				throw new CommonCustomException("Shifting is not possible, because it is already an AGGREGATE PO");
			}
		}
			else{
				throw new CommonCustomException("Shifting is not possible, because Aggregate PO is not allowed for this Client and Project");

			}
			 }else{
				 throw new CommonCustomException("Access denied, since you are not an authorized person");
			 }
			// TODO Auto-generated method stub
			return invoiceDatPurchaseBO;
		}

		//EOA by Balaji 
		
		
		//Added by Smrithi
		
		public MapPoToWorkorderOutputBean mapPoToWo(
				MapPoToWorkorderInputBean mapPoToWoInputBean) throws CommonCustomException, DataAccessException {
			
			List<InvoiceDatMappingPoWithWoBO> invoiceDatMappingPoWithWoBOList = new ArrayList<>();
			InvoiceDatMappingPoWithWoBO invoiceDatMappingPoWithWoBO = new InvoiceDatMappingPoWithWoBO();
			MapPoToWorkorderOutputBean outputBean = new MapPoToWorkorderOutputBean();
			InvoiceDatPurchaseOrderBO purchaseOrderBO = new InvoiceDatPurchaseOrderBO();
			DatWorkorderBO workorderBO = new DatWorkorderBO();
			List<Integer> woIdList = new ArrayList<Integer>();
			MasProjectBO masProjectBO = new MasProjectBO();
			List<DatEmpDetailBO> empDetailList = new ArrayList<DatEmpDetailBO>();
			DatEmpDetailBO empDetailBO = new DatEmpDetailBO();
			List<String> financeRoleKeyNamesList = Stream.of(financeRoleKeyNames.split(","))
	                .collect(Collectors.toList());
			//Logged in access check
			try {
				empDetailList = empDetailRepository.getAllFinanceRolesByEmpId(mapPoToWoInputBean.getLoggedInEmpId(), financeRoleKeyNamesList );
			}
			catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB");
			}
			try {
				empDetailBO = empDetailRepository.findByPkEmpId(mapPoToWoInputBean.getLoggedInEmpId() );
			}
			catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB");
			}
			//Checking with purchaseOrderBO
			try{
				purchaseOrderBO = invoiceDatPORepository.findByPkPurchaseOrderId(mapPoToWoInputBean.getPkPurchaseOrderId());
			}
			catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB" );
			}
			//Checking with invoiceDatMappingPoWithWoBO
			try{
				woIdList = invoiceDatMappingPoWithWoRepository.getwoId(mapPoToWoInputBean.getPkPurchaseOrderId());
			}
			catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB" );
			}
			
			 try{
				// invoiceDatMappingPoWithWoBOList = invoiceDatMappingPoWithWoRepository.findByFkWorkorderId(mapPoToWoInputBean.getFkWorkorderId());
				 invoiceDatMappingPoWithWoBOList = invoiceDatMappingPoWithWoRepository.findByFkPurchaseOrderIdAndFkWorkorderId(mapPoToWoInputBean.getPkPurchaseOrderId(),mapPoToWoInputBean.getFkWorkorderId());
				}
				catch(Exception e){
					throw new CommonCustomException("Failed to fetch details from DB" );
				}
			//Checking with workorderBO
			try{
				workorderBO = datWorkorderRepository.findByPkWorkorderId(mapPoToWoInputBean.getFkWorkorderId());
			}
			catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB" );
			}
			//Validation check for InputBean fields.
			if(workorderBO == null){
				throw new CommonCustomException("Invalid WorkOrder Id.");
			}
			if(purchaseOrderBO == null){
				throw new CommonCustomException("Invalid Purchase Order Id");
			}
			if(empDetailBO == null){
				throw new CommonCustomException("Invalid empId");
			}
			
			
			if(!empDetailList.isEmpty())
		{
				 if("AGGREGATE".equalsIgnoreCase(purchaseOrderBO.getPoType())){
						
					  if(invoiceDatMappingPoWithWoBOList.isEmpty()){
						  //Set values to outputBean
							
							 WorkOrderOutputBean workOrderOutputBean = new WorkOrderOutputBean();
							 try{
							 workOrderOutputBean = workOrderService.getWorkOrder(mapPoToWoInputBean.getFkWorkorderId(),mapPoToWoInputBean.getLoggedInEmpId());
							 } catch(Exception e){
								 throw new DataAccessException("Some Exception happened while getting Work Order Details");
							 }
									if(workOrderOutputBean != null){
									outputBean.setProjectName(workOrderOutputBean.getProjectName());
									 outputBean.setNoOfcomponents(workOrderOutputBean.getNoOfcomponents());
									 outputBean.setPurchaseNumber(purchaseOrderBO.getPoPrefix()+""+purchaseOrderBO.getPurchaseNumber());
									 outputBean.setWoType(workOrderOutputBean.getWoType());
									 outputBean.setWorkorderNumber(workOrderOutputBean.getWoNumber());
									 outputBean.setSowCurrencyName(workOrderOutputBean.getSowCurrencyName());
									 outputBean.setNoOfPo(workOrderOutputBean.getNoOfPo());
									 outputBean.setWoStatusName(workOrderOutputBean.getWoStatusName());
									outputBean.setSowAmount(workOrderOutputBean.getSowAmount());
									//Saving details to invoiceDatMappingPoWithWoBO
									invoiceDatMappingPoWithWoBO.setFkPurchaseOrderId(mapPoToWoInputBean.getPkPurchaseOrderId());
									invoiceDatMappingPoWithWoBO.setFkWorkorderId(mapPoToWoInputBean.getFkWorkorderId());
									invoiceDatMappingPoWithWoRepository.save(invoiceDatMappingPoWithWoBO);
									//Saving details to purchaseOrderBO
									purchaseOrderBO.setPoStatus("MAPPED");
									purchaseOrderBO.setUpdatedBy(mapPoToWoInputBean.getLoggedInEmpId());
									purchaseOrderBO.setUpdatedOn(new Date());
									invoiceDatPORepository.save(purchaseOrderBO);
								 }
								 else{
									 throw new CommonCustomException("Data not found in workOrderOutputBean ");
								 }
					 }
					  else if(!(invoiceDatMappingPoWithWoBOList.isEmpty())){
							
						 /* List<InvoiceDatMappingPoWithWoBO> mappingPoWithWoBOList = new ArrayList<InvoiceDatMappingPoWithWoBO>();
							try{
								mappingPoWithWoBOList = invoiceDatMappingPoWithWoRepository.getMappingDetails(mapPoToWoInputBean.getPkPurchaseOrderId());
							}
							catch(Exception e){
								throw new CommonCustomException("Failed to fetch details from DB" );
							}
							if(!(mappingPoWithWoBOList.isEmpty())){
								throw new CommonCustomException("Cannot Map WO to PO" );
							}
							else{
								//Set values to outputBean
								
								 WorkOrderOutputBean workOrderOutputBean = new WorkOrderOutputBean();
								 try{
									 workOrderOutputBean = workOrderService.getWorkOrder(mapPoToWoInputBean.getFkWorkorderId(),mapPoToWoInputBean.getLoggedInEmpId());
									 } catch(Exception e){
										 throw new DataAccessException("Some Exception happened while getting Work Order Details");
									 }
								 if(workOrderOutputBean != null){
										outputBean.setProjectName(workOrderOutputBean.getProjectName());
										 outputBean.setNoOfcomponents(workOrderOutputBean.getNoOfcomponents());
										 outputBean.setPurchaseNumber(purchaseOrderBO.getPoPrefix()+""+purchaseOrderBO.getPurchaseNumber());
										 outputBean.setWoType(workOrderOutputBean.getWoType());
										 outputBean.setWorkorderNumber(workOrderOutputBean.getWoNumber());
										 outputBean.setSowCurrencyName(workOrderOutputBean.getSowCurrencyName());
										 outputBean.setNoOfPo(workOrderOutputBean.getNoOfPo());
										 outputBean.setWoStatusName(workOrderOutputBean.getWoStatusName());
										outputBean.setSowAmount(workOrderOutputBean.getSowAmount());
										//Saving details to invoiceDatMappingPoWithWoBO
										invoiceDatMappingPoWithWoBO.setFkPurchaseOrderId(mapPoToWoInputBean.getPkPurchaseOrderId());
										invoiceDatMappingPoWithWoBO.setFkWorkorderId(mapPoToWoInputBean.getFkWorkorderId());
										invoiceDatMappingPoWithWoRepository.save(invoiceDatMappingPoWithWoBO);
										//Saving details to purchaseOrderBO
										purchaseOrderBO.setPoStatus("MAPPED");
										purchaseOrderBO.setUpdatedBy(mapPoToWoInputBean.getLoggedInEmpId());
										purchaseOrderBO.setUpdatedOn(new Date());
										invoiceDatPORepository.save(purchaseOrderBO);
									 }
									 else{
										 throw new CommonCustomException("Data not found in workOrderOutputBean ");
									 }
							}*/
						  throw new CommonCustomException("Cannot map WO to PO since it's already mapped.");
					  }
					/*  else if(woIdList.isEmpty()){
						
						 //Set values to outputBean
						
						 WorkOrderOutputBean workOrderOutputBean = new WorkOrderOutputBean();
						 try{
							 workOrderOutputBean = workOrderService.getWorkOrder(mapPoToWoInputBean.getFkWorkorderId(),mapPoToWoInputBean.getLoggedInEmpId());
							 } catch(Exception e){
								 throw new DataAccessException("Some Exception happened while getting Work Order Details");
							 }
						 if(workOrderOutputBean != null){
								outputBean.setProjectName(workOrderOutputBean.getProjectName());
								 outputBean.setNoOfcomponents(workOrderOutputBean.getNoOfcomponents());
								 outputBean.setPurchaseNumber(purchaseOrderBO.getPoPrefix()+""+purchaseOrderBO.getPurchaseNumber());
								 outputBean.setWoType(workOrderOutputBean.getWoType());
								 outputBean.setWorkorderNumber(workOrderOutputBean.getWoNumber());
								 outputBean.setSowCurrencyName(workOrderOutputBean.getSowCurrencyName());
								 outputBean.setNoOfPo(workOrderOutputBean.getNoOfPo());
								 outputBean.setWoStatusName(workOrderOutputBean.getWoStatusName());
								outputBean.setSowAmount(workOrderOutputBean.getSowAmount());
								//Saving details to invoiceDatMappingPoWithWoBO
								invoiceDatMappingPoWithWoBO.setFkPurchaseOrderId(mapPoToWoInputBean.getPkPurchaseOrderId());
								invoiceDatMappingPoWithWoBO.setFkWorkorderId(mapPoToWoInputBean.getFkWorkorderId());
								invoiceDatMappingPoWithWoRepository.save(invoiceDatMappingPoWithWoBO);
								//Saving details to purchaseOrderBO
								purchaseOrderBO.setPoStatus("MAPPED");
								purchaseOrderBO.setUpdatedBy(mapPoToWoInputBean.getLoggedInEmpId());
								purchaseOrderBO.setUpdatedOn(new Date());
								invoiceDatPORepository.save(purchaseOrderBO);
							 }
							 else{
								 throw new CommonCustomException("Data not found in workOrderOutputBean ");
							 }
					 }
					
				    else if(!woIdList.isEmpty() &&  woIdList.contains(mapPoToWoInputBean.getFkWorkorderId().intValue()))
				    {
				    	throw new CommonCustomException("Cannot map WO to PO since it's already mapped.");
					 }
				    	
				    else{
				    	//Set values to outputBean
						 WorkOrderOutputBean workOrderOutputBean = new WorkOrderOutputBean();
						 try{
							 workOrderOutputBean = workOrderService.getWorkOrder(mapPoToWoInputBean.getFkWorkorderId(),mapPoToWoInputBean.getLoggedInEmpId());
							 } catch(Exception e){
								 throw new DataAccessException("Some Exception happened while getting Work Order Details");
							 }
						 if(workOrderOutputBean != null){
								outputBean.setProjectName(workOrderOutputBean.getProjectName());
								 outputBean.setNoOfcomponents(workOrderOutputBean.getNoOfcomponents());
								 outputBean.setPurchaseNumber(purchaseOrderBO.getPoPrefix()+""+purchaseOrderBO.getPurchaseNumber());
								 outputBean.setWoType(workOrderOutputBean.getWoType());
								 outputBean.setWorkorderNumber(workOrderOutputBean.getWoNumber());
								 outputBean.setSowCurrencyName(workOrderOutputBean.getSowCurrencyName());
								 outputBean.setNoOfPo(workOrderOutputBean.getNoOfPo());
								 outputBean.setWoStatusName(workOrderOutputBean.getWoStatusName());
								outputBean.setSowAmount(workOrderOutputBean.getSowAmount());
								//Saving details to invoiceDatMappingPoWithWoBO
								invoiceDatMappingPoWithWoBO.setFkPurchaseOrderId(mapPoToWoInputBean.getPkPurchaseOrderId());
								invoiceDatMappingPoWithWoBO.setFkWorkorderId(mapPoToWoInputBean.getFkWorkorderId());
								invoiceDatMappingPoWithWoRepository.save(invoiceDatMappingPoWithWoBO);
								//Saving details to purchaseOrderBO
								purchaseOrderBO.setPoStatus("MAPPED");
								purchaseOrderBO.setUpdatedBy(mapPoToWoInputBean.getLoggedInEmpId());
								purchaseOrderBO.setUpdatedOn(new Date());
								invoiceDatPORepository.save(purchaseOrderBO);
							 }
							 
					 }*/
					
					}
				   else if("NORMAL".equalsIgnoreCase(purchaseOrderBO.getPoType())){
					
				/*	if( (!woIdList.isEmpty())){
						throw new CommonCustomException("Cannot map PO to WO");
					}
					else{*/
						List<InvoiceDatMappingPoWithWoBO> mappingPoWithWoBO = new ArrayList<InvoiceDatMappingPoWithWoBO>();
						try{
							mappingPoWithWoBO = invoiceDatMappingPoWithWoRepository.findByFkPurchaseOrderIdAndFkWorkorderId(mapPoToWoInputBean.getFkWorkorderId(),mapPoToWoInputBean.getPkPurchaseOrderId());
						}
						catch(Exception e){
							throw new CommonCustomException("Failed to fetch details from DB" );
						}
						if(!(mappingPoWithWoBO==null)){
							throw new CommonCustomException("Cannot Map WO to PO" );
						}
						else{
					 //Set values to outputBean
					
					 WorkOrderOutputBean workOrderOutputBean = new WorkOrderOutputBean();
					 try{
						 workOrderOutputBean = workOrderService.getWorkOrder(mapPoToWoInputBean.getFkWorkorderId(),mapPoToWoInputBean.getLoggedInEmpId());
						 } catch(Exception e){
							 throw new DataAccessException("Some Exception happened while getting Work Order Details");
						 }
					 if(workOrderOutputBean != null){
							outputBean.setProjectName(workOrderOutputBean.getProjectName());
							 outputBean.setNoOfcomponents(workOrderOutputBean.getNoOfcomponents());
							 outputBean.setPurchaseNumber(purchaseOrderBO.getPoPrefix()+""+purchaseOrderBO.getPurchaseNumber());
							 outputBean.setWoType(workOrderOutputBean.getWoType());
							 outputBean.setWorkorderNumber(workOrderOutputBean.getWoNumber());
							 outputBean.setSowCurrencyName(workOrderOutputBean.getSowCurrencyName());
							 outputBean.setNoOfPo(workOrderOutputBean.getNoOfPo());
							 outputBean.setWoStatusName(workOrderOutputBean.getWoStatusName());
							outputBean.setSowAmount(workOrderOutputBean.getSowAmount());
							//Saving details to invoiceDatMappingPoWithWoBO
							invoiceDatMappingPoWithWoBO.setFkPurchaseOrderId(mapPoToWoInputBean.getPkPurchaseOrderId());
							invoiceDatMappingPoWithWoBO.setFkWorkorderId(mapPoToWoInputBean.getFkWorkorderId());
							invoiceDatMappingPoWithWoRepository.save(invoiceDatMappingPoWithWoBO);
							//Saving details to purchaseOrderBO
							purchaseOrderBO.setPoStatus("MAPPED");
							purchaseOrderBO.setUpdatedBy(mapPoToWoInputBean.getLoggedInEmpId());
							purchaseOrderBO.setUpdatedOn(new Date());
							invoiceDatPORepository.save(purchaseOrderBO);
						 }
						 else{
							 throw new CommonCustomException("Data not found in workOrderOutputBean ");
						 }
					
					}
					}}	
			   else{
				throw new CommonCustomException("Access denied, since you are not an authorized person.");
			}
			return outputBean;
		}

		public MapPoToWorkorderOutputBean unmapPoToWo(
				MapPoToWorkorderInputBean mapPoToWoInputBean) throws CommonCustomException {
			List<ProjDatMilestoneTriggerDetailBO> projDatMilestoneTriggerDetailBOList = new ArrayList<ProjDatMilestoneTriggerDetailBO>();
			InvoiceDatMappingPoWithWoBO invoiceDatMappingPoWithWoBO = new InvoiceDatMappingPoWithWoBO();
			InvoiceDatPurchaseOrderBO purchaseOrderBO = new InvoiceDatPurchaseOrderBO();
			DatWorkorderBO workorderBO = new DatWorkorderBO();
			List<DatEmpDetailBO> empDetailList = new ArrayList<DatEmpDetailBO>();
			DatEmpDetailBO empDetailBO = new DatEmpDetailBO();
			List<String> financeRoleKeyNamesList = Stream.of(financeRoleKeyNames.split(","))
	                .collect(Collectors.toList());
			//Logged in access check
			try {
				empDetailList = empDetailRepository.getAllFinanceRolesByEmpId(mapPoToWoInputBean.getLoggedInEmpId(), financeRoleKeyNamesList );
			}
			catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB");
			}
			try {
				empDetailBO = empDetailRepository.findByPkEmpId(mapPoToWoInputBean.getLoggedInEmpId() );
			}
			catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB");
			}
			if(empDetailBO == null){
				throw new CommonCustomException("Invalid empId");
			}
			//Checking whether mapping is present or not
			try{
				invoiceDatMappingPoWithWoBO = invoiceDatMappingPoWithWoRepository.findByFkWorkorderIdAndFkPurchaseOrderId(mapPoToWoInputBean.getFkWorkorderId(),mapPoToWoInputBean.getPkPurchaseOrderId());
			}
			catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB" );
			}
			if(invoiceDatMappingPoWithWoBO==null){
				throw new CommonCustomException("There is no mapping for the given inputs.");
			}
			//Checking with workorderBO
			try{
				workorderBO = datWorkorderRepository.findByPkWorkorderId(mapPoToWoInputBean.getFkWorkorderId());
			}
			catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB" );
			}
			if(workorderBO == null){
				throw new CommonCustomException("Invalid WorkOrder Id.");
			}
			//Checking componentBO
			try{
				projDatMilestoneTriggerDetailBOList = projDatMilestoneTriggerDetailRepository.undoPoMapping(mapPoToWoInputBean.getFkWorkorderId());
			}catch(Exception e){
				e.printStackTrace();
				throw new CommonCustomException("Failed to fetch details from DB");
			}
			
			//UNDO MAPPING IF COMP TYPE IS FIXED PRICE / MILESTONE
			if(!empDetailList.isEmpty())
			{
			
			if(projDatMilestoneTriggerDetailBOList.isEmpty()){
				
				invoiceDatMappingPoWithWoRepository.delete(invoiceDatMappingPoWithWoBO);
				
								//Checking with purchaseOrderBO
								try{
									purchaseOrderBO = invoiceDatPORepository.findByPkPurchaseOrderId(mapPoToWoInputBean.getPkPurchaseOrderId());
								}
								catch(Exception e){
									throw new CommonCustomException("Failed to fetch details from DB" );
								}
								if(purchaseOrderBO == null){
									throw new CommonCustomException("Invalid Purchase Order Id");
								}
								
						if("NORMAL".equalsIgnoreCase(purchaseOrderBO.getPoType())){
							purchaseOrderBO.setPoStatus("UNMAPPED");
							purchaseOrderBO.setUpdatedBy(mapPoToWoInputBean.getLoggedInEmpId());
							purchaseOrderBO.setUpdatedOn(new Date());
							invoiceDatPORepository.save(purchaseOrderBO);
						}
						else{
							    //Checking no of workorders for the given po id
									List<Integer> woIdList = new ArrayList<Integer>();
									try{
										woIdList = invoiceDatMappingPoWithWoRepository.getwoId(mapPoToWoInputBean.getPkPurchaseOrderId());
									}
									catch(Exception e){
										throw new CommonCustomException("Failed to fetch details from DB" );
									}
									if(woIdList.size()==1 && mapPoToWoInputBean.getFkWorkorderId() == woIdList.get(0) ){
										purchaseOrderBO.setPoStatus("UNMAPPED");
										purchaseOrderBO.setUpdatedBy(mapPoToWoInputBean.getLoggedInEmpId());
										purchaseOrderBO.setUpdatedOn(new Date());
										invoiceDatPORepository.save(purchaseOrderBO);
									}else{
										throw new CommonCustomException("Cannot change status to UNMAPPED because it is mapped to other workorders.");
									}
						}
			}else{
				throw new CommonCustomException("UNMAP cannot be done because milestones have been triggerred for the given workorder.");
			}
		}else{
			throw new CommonCustomException("Access denied, since you are not an authorized person.");
		}
			return null;
		}

	
		
		//EOA by Smrithi 
}
