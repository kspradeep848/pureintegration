package com.thbs.mis.invoice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.framework.exception.CommonCustomException;

@Service
public class InvoicePOCommonService {
	
	@Autowired
	private EmpDetailRepository empDetailRepos;
	
	@Autowired
	private EmployeePersonalDetailsRepository empPersonalDetailsRepository;

//This method use to fetch employee personal and professional details by empId
    
    public String getEmployeeName(Integer empId)throws CommonCustomException {
    	
    	DatEmpDetailBO empDetailBO = new DatEmpDetailBO();
		DatEmpPersonalDetailBO empPersonalDetailBO = new DatEmpPersonalDetailBO();
		String employeeName = "";
		
		     //get Active employee id from dat_emp_detail table
    			try {
    				empDetailBO = empDetailRepos.findByPkEmpId(empId);
    			}catch(Exception e){
    				throw new CommonCustomException("Failed to fetch Employee id from DB");
    			}if (empDetailBO == null) {
    				throw new CommonCustomException("Invalid Employee ID.");
    			}
    			//get employee personal details by empId
    			try{
    				empPersonalDetailBO = empPersonalDetailsRepository.findByFkEmpDetailId(empId);
    			}catch(Exception e){
    				throw new CommonCustomException("Failed to fetch Employee Record from DB");
    			}
    		
    			if(empPersonalDetailBO.getEmpMiddleName()!=null && empPersonalDetailBO.getEmpLastName()!=null) {
    				employeeName = empPersonalDetailBO.getEmpFirstName()+" "
                            +empPersonalDetailBO.getEmpMiddleName()+" "
                            +empPersonalDetailBO.getEmpLastName();
    			}
    			else if(empPersonalDetailBO.getEmpMiddleName()!=null) {
    				employeeName = empPersonalDetailBO.getEmpFirstName()+" "
                            +empPersonalDetailBO.getEmpMiddleName();
                }
    			else if(empPersonalDetailBO.getEmpLastName()!=null) {
    				employeeName = empPersonalDetailBO.getEmpFirstName()+" "
                            +empPersonalDetailBO.getEmpLastName();
    			}else {
    				employeeName = empPersonalDetailBO.getEmpFirstName()+" "
                            +empPersonalDetailBO.getEmpMiddleName()+" "
                            +empPersonalDetailBO.getEmpLastName();
    			
    			}
    			
    	return employeeName;
    }
    
   
}
