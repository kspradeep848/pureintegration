package com.thbs.mis.invoice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.invoice.bean.SearchFilteredInvoiceOutputBean;
import com.thbs.mis.invoice.bean.SummaryForGenerateInvoiceInputBean;
import com.thbs.mis.invoice.bean.SummaryForGenerateInvoiceOutputBean;
import com.thbs.mis.invoice.bo.InvoiceDatPurchaseOrderBO;
import com.thbs.mis.project.bo.MasProjectBO;
import com.thbs.mis.project.dao.MasProjectRepository;

@Service
public class InvoiceService {
	
	private static final AppLog LOG = LogFactory.getLog(InvoicePOService.class);
	
	@Autowired
	private EmpDetailRepository empDetailRepository;
	
	@Autowired
	private MasProjectRepository masProjRepository;
	
	@Value("${finance.role.keyName}") 
	private String financeRoleKeyNames;


	//Added by Smrithi
	

	public List<SummaryForGenerateInvoiceOutputBean> summaryForGenerateInvoice(
			SummaryForGenerateInvoiceInputBean inputBean) {
		// TODO Auto-generated method stub
		return null;
	}
	
	//EOA by Smrithi
	
// Added by Balaji

	public List<SearchFilteredInvoiceOutputBean> searchFilteredInvoices(
			short buId, int loggedInEmpId) throws CommonCustomException, DataAccessException {

				DatEmpDetailBO empDetailBO = new DatEmpDetailBO();
		 		List<InvoiceDatPurchaseOrderBO> invoiceDatPurchaseBOList = new ArrayList<InvoiceDatPurchaseOrderBO>();
		 		List<DatEmpDetailBO> datEmpDetailBOList = new ArrayList<DatEmpDetailBO>();
		 		List<MasProjectBO> masProjectBOList = new ArrayList<MasProjectBO>();
		 		List<String> financeNameList = Stream.of(financeRoleKeyNames.split(","))
				           .collect(Collectors.toList());
				
			     //get Active employee id from dat_emp_detail table
				try {
					System.out.println("Filter Input bean value of logged in emp id is: "+loggedInEmpId);
					empDetailBO = empDetailRepository.findByPkEmpId(loggedInEmpId);
					LOG.info("Get Employee id by empId"+ empDetailBO);
				}catch(Exception e){
					throw new DataAccessException("Failed to fetch Employee id from DB");
				}if (empDetailBO == null) {
					throw new CommonCustomException("Invalid Employee ID.");
				}
				//get employee personal details by empId
				
				try {
					datEmpDetailBOList = empDetailRepository.getAllFinanceRolesByEmpId(loggedInEmpId, financeNameList);
					
				}catch (Exception e) {
					throw new DataAccessException("Failed to fetch details from DB");
				}
				if(datEmpDetailBOList.isEmpty()){
					throw new DataAccessException("You are not an Authorized User");
				}
				try{
					masProjectBOList = masProjRepository.findByFkBuUnitId(buId);
				}catch(Exception e){
					throw new DataAccessException("Failed to fetch Project Details");
				}
				
				
				return null;
	}

	// EOA by Balaji
	
}
