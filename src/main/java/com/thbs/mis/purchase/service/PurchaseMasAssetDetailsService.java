package com.thbs.mis.purchase.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.PurchaseMasAssetDetailOutputBean;
import com.thbs.mis.purchase.bean.PurchaseMassAssetDetailOutputBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;
import com.thbs.mis.purchase.bo.PurchaseMasAssetDetailsBO;
import com.thbs.mis.purchase.controller.PurchaseMasSettingController;
import com.thbs.mis.purchase.dao.PurchaseAssetRequestRepository;
import com.thbs.mis.purchase.dao.PurchaseMasAssetDetailsRepository;


@Service
public class PurchaseMasAssetDetailsService {
	
	private static final AppLog LOG = LogFactory
			.getLog(PurchaseMasSettingController.class);
	
	@Autowired
	PurchaseMasAssetDetailsRepository  purchaseMasAssetDetailsRepository;
	
	@Autowired
	PurchaseAssetRequestRepository assetRequestRepository;
	
	@Value("${purchase.request.type.new.asset.request}")
	private Integer purchaseRequestTypeNewAssetRequest;
	

    public PurchaseMasAssetDetailOutputBean getAssestDetailsBysubcrptId(String subscriptionId )
    		throws DataAccessException,CommonCustomException
    {
    	PurchaseMasAssetDetailOutputBean assetDetOPBean = new PurchaseMasAssetDetailOutputBean(); 
  	 
  	  PurchaseMasAssetDetailsBO  purMasAssetDetBO =new PurchaseMasAssetDetailsBO();
  	  
  	  try{
  		  LOG.startUsecase("Asset Detail Service try");
  		  purMasAssetDetBO=purchaseMasAssetDetailsRepository.getAssetDetBySoftwareLicenseOrSubscriptionId(subscriptionId);
  		
  	  }
  			
  	  
  	  catch(Exception e)
  	  {
  		  throw new DataAccessException("Exception occured in Controller");
  		  
  	  }
  			
 	if(purMasAssetDetBO==null)
	  {
		  throw new CommonCustomException("Invalid subscriptionId");
	  }
	else{
	  assetDetOPBean.setSoftwareLicenseOrSubscriptionId(purMasAssetDetBO.getSoftwareLicenseOrSubscriptionId());
	  assetDetOPBean.setAssetAvailableFromDate(purMasAssetDetBO.getAssetAvailableFromDate());
	  assetDetOPBean.setAssetAvailableToDate(purMasAssetDetBO.getAssetAvailableToDate());
	  assetDetOPBean.setFkMasAssetsId(purMasAssetDetBO.getFkMasAssetsId());
	  assetDetOPBean.setAssetDetailCreatedDate(purMasAssetDetBO.getCreatedDate());
	  assetDetOPBean.setAssetDetailCreatedBy(purMasAssetDetBO.getCreatedBy());
	  assetDetOPBean.setAssetDetailModifiedDate(purMasAssetDetBO.getModifiedDate());
	  assetDetOPBean.setAssetDetailModifiedBy(purMasAssetDetBO.getModifiedBy());
	  assetDetOPBean.setPkAssetDetailId(purMasAssetDetBO.getPkAssetDetailId());
	  assetDetOPBean.setAssetDescription(purMasAssetDetBO.getAssetDescription());
	  
	  
	  

	  assetDetOPBean.setPkAssetId(purMasAssetDetBO.getPurchaseMasAssetsBO().getPkAssetId());
	  assetDetOPBean.setAssetType(purMasAssetDetBO.getPurchaseMasAssetsBO().getAssetType());
	  assetDetOPBean.setAssetNameAndVersionOrModel(purMasAssetDetBO.getPurchaseMasAssetsBO()
			  .getAssetNameAndVersionOrModel());
	  assetDetOPBean.setCreatedBy(purMasAssetDetBO.getPurchaseMasAssetsBO().getCreatedBy());
	  assetDetOPBean.setAssetCreatedDate(purMasAssetDetBO.getPurchaseMasAssetsBO().getCreatedDate());
	  assetDetOPBean.setAssetModifiedBy(purMasAssetDetBO.getPurchaseMasAssetsBO().getModifiedBy());
	  assetDetOPBean.setAssetModifiedDate(purMasAssetDetBO.getPurchaseMasAssetsBO().getModifiedDate());
	  assetDetOPBean.setIsAssetAvailable(purMasAssetDetBO.getPurchaseMasAssetsBO().getIsAvailable());
	  
	  
	  
  }
  	  
  	  
  	  
  	 return assetDetOPBean ;
    }
    /**
	 * 
	 * @param assetType
	 * @return List<PurchaseAssetDetailsOutputBean>
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to view the asset and it
	 *               detail.
	 */
	public List<PurchaseMassAssetDetailOutputBean> getAssetAndItsDetailFromAssetId(Integer assetId)
			throws CommonCustomException {
		LOG.startUsecase("Get Asset And Its Detail From AssetId for system admin");
		List<PurchaseMassAssetDetailOutputBean> massAssetDetailOutputBeanList = new ArrayList<PurchaseMassAssetDetailOutputBean>();
        List<PurchaseMasAssetDetailsBO> masAssetDetailBOList = new ArrayList<PurchaseMasAssetDetailsBO>();
		try
		 {
			masAssetDetailBOList = purchaseMasAssetDetailsRepository.getSoftwareOrLicenseIdFromAssetId(assetId);
		 } 
		catch (Exception ex)
		   {
			throw new CommonCustomException("Unable to retrieve asset details. Kindly contact to Nucleus Support team for this issue.");
					
		   }
		
		for (PurchaseMasAssetDetailsBO purchaseMasAssetDetailsBO : masAssetDetailBOList)
		 {
			PurchaseMassAssetDetailOutputBean purchaseMassAssetDetailOutputBean = new PurchaseMassAssetDetailOutputBean();
			purchaseMassAssetDetailOutputBean.setPkAssetDetailId(purchaseMasAssetDetailsBO.getPkAssetDetailId());
			purchaseMassAssetDetailOutputBean.setSoftwareLicenseOrSubscriptionId(purchaseMasAssetDetailsBO.getSoftwareLicenseOrSubscriptionId());
			purchaseMassAssetDetailOutputBean.setAssetAvailableFromDate(purchaseMasAssetDetailsBO.getAssetAvailableFromDate());
			purchaseMassAssetDetailOutputBean.setAssetAvailableToDate(purchaseMasAssetDetailsBO.getAssetAvailableToDate());
			purchaseMassAssetDetailOutputBean.setAssetDescription(purchaseMasAssetDetailsBO.getAssetDescription());
			massAssetDetailOutputBeanList.add(purchaseMassAssetDetailOutputBean);
			
		 }
		
		LOG.endUsecase("Get Asset And Its Detail From AssetId for system admin");
		return massAssetDetailOutputBeanList;

	}
	
	
	
	public PurchaseMassAssetDetailOutputBean getSubscriptionIdFromAssetRequestId(Integer requestId) 
			throws CommonCustomException
	{
		PurchaseMassAssetDetailOutputBean output = new PurchaseMassAssetDetailOutputBean();
		PurchaseMasAssetDetailsBO purchaseMasAssetDetailsBO = new PurchaseMasAssetDetailsBO();
		
		//For validation
		PurchaseDatAssetRequestBO assetRequestBo = new PurchaseDatAssetRequestBO();
		try{
			assetRequestBo = assetRequestRepository.findByPkPurchaseRequestId(requestId);
		}catch(Exception ex){
			throw new CommonCustomException("Failed to fetch asset request detail from asset request id. Kindly get in touch with Nucleus Support team.");
		}
		
		if(assetRequestBo == null)
			throw new CommonCustomException("No asset request record found.");
			
		if(assetRequestBo.getFkRequestTypeId().intValue() == purchaseRequestTypeNewAssetRequest.intValue())
			throw new CommonCustomException("This service is not for 'New Asset Request' type.");
			
		try{
			purchaseMasAssetDetailsBO = purchaseMasAssetDetailsRepository.findByFkPurchaseRequestId(requestId);
		}catch(Exception ex){
			throw new CommonCustomException("Failed to get asset details from asset request id. Kindly get in touch with Nucleus Support team.");
		}
		
		if(purchaseMasAssetDetailsBO == null)
			throw new CommonCustomException("No asset details record found.");
		
		output.setAssetDescription(purchaseMasAssetDetailsBO.getAssetDescription());
		output.setPkAssetDetailId(purchaseMasAssetDetailsBO.getPkAssetDetailId());
		output.setSoftwareLicenseOrSubscriptionId(purchaseMasAssetDetailsBO.
				getSoftwareLicenseOrSubscriptionId());
		return output;
	}

}
