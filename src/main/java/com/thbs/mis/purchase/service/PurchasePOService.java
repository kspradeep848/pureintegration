package com.thbs.mis.purchase.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.dao.BusinessUnitRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.report.Util.DownloadFileUtil;
import com.thbs.mis.gsr.service.GSRGoalService;
import com.thbs.mis.gsr.service.GsrCommonService;
import com.thbs.mis.notificationframework.puchasenotification.service.PurchaseNotificationService;
import com.thbs.mis.purchase.bean.ClientAndProjectDetailsFromCompIdBean;
import com.thbs.mis.purchase.bean.ClientAndProjectDetailsInputBean;
import com.thbs.mis.purchase.bean.PurchaseCancelPoBean;
import com.thbs.mis.purchase.bean.PurchaseCapexOutputBean;
import com.thbs.mis.purchase.bean.PurchaseOrderDetailsSaveBean;
import com.thbs.mis.purchase.bean.PurchaseOrderSysAdminOutputBean;
import com.thbs.mis.purchase.bean.PurchasePOCreateBean;
import com.thbs.mis.purchase.bean.PurchasePOPdfGenerationBean;
import com.thbs.mis.purchase.bean.PurchaseQuotesOutputBean;
import com.thbs.mis.purchase.bean.PurchaseSavePoOutputBean;
import com.thbs.mis.purchase.bean.ViewAllAssetRequestBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetIncrementBO;
import com.thbs.mis.purchase.bo.PurchaseDatAssetQuoteBO;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;
import com.thbs.mis.purchase.bo.PurchaseDatPoBO;
import com.thbs.mis.purchase.bo.PurchaseMasAssetDetailsBO;
import com.thbs.mis.purchase.dao.AssetRequestSpecification;
import com.thbs.mis.purchase.dao.PurchaseAssetIncrementRepository;
import com.thbs.mis.purchase.dao.PurchaseAssetQuoteRepository;
import com.thbs.mis.purchase.dao.PurchaseAssetRequestRepository;
import com.thbs.mis.purchase.dao.PurchaseMasAssetDetailsRepository;
import com.thbs.mis.purchase.dao.PurchasePORepository;

@Service
@Transactional
public class PurchasePOService {

	private static final AppLog LOG = LogFactory.getLog(PurchasePOService.class);
	
	@Autowired
	private PurchaseAssetRequestRepository assetRequestRepository;
	
	@Autowired
	private PurchasePORepository poRepository;
	
	@Autowired
	private PurchaseMasAssetDetailsRepository masAssetDetailsRepository;
	
	@Autowired
	private PurchaseCapexService purchaseCapexSrvc;
	
	@Autowired
	private PurchaseAssetQuoteRepository assetQuoteRepository;
	
	@Autowired
	private EmpDetailRepository empDetailRepository;
	
	@Autowired
	private PurchaseAssetIncrementRepository assetIncrementRepository;
	
	@Autowired
	private GsrCommonService gsrCommonService;
	
	@Autowired
	private GSRGoalService gsrGoalService;

	@Autowired
	private BusinessUnitRepository buRepository;
	
	@Value("${purchase.file.basepath}")
	private String purchaseDocsBasePath;
	
	@Value("${purchase.po.createdBySysAdmin}")
	private Integer purchasePoCreatedStatus;
	
	@Value("${purchase.uploaded}")
	private String purchaseCapexUploaded;
	
	@Value("${request.poRejectedByFinance.status}")
	private Integer requestPoRejectedByFinanceStatus;
	
	@Value("${purchase.po.createdBySysAdmin}")
	private Integer purchasePoCreatedBySysAdmin;
	
	@Value("${purchase.po.closedBySysAdmin}")
	private Integer purchasePoClosedBySysAdmin;
	
	@Value("${purchase.po.rejectedBySysAdmin}")
	private Integer purchasePoRejectedBySysAdmin;
	
	@Value("${systemseniorAdminRoles}")
	private Integer seniorSystemAdmin;
	
	@Value ("${systemAdminRoles}")
	private Integer systemAdmin;
	
	@Value("${systemAdminProcurementRoleId}")
	private Integer systemAdminProcurment;
	
	@Value("${purchase.assetRequest.closed}")
	private Integer purchaseRequestClosed;
	
	@Value("${request.poApprovedByFinance.status}")
	private Integer purchaseRequestApprovedByFinance;
	
	@Value("${request.capexApprovedByBuHead.status}")
	private Integer purchaseCapexApprovedByBuHead;
	
	@Value("${thbs.bangalore.address}")
	private String thbsBangaloreAddress;
	
	@Value("${thbs.bangalore.location.id}")
	private Integer bangaloreLocationId;
	
	@Value("${thbs.gurgaon.location.id}")
	private Integer gurgaonLocationId;
	
	@Value("${thbs.gurgaon.address}")
	private String thbsGurgaonAddress;
	
	@Value("${thbs.mumbai.location.id}")
	private Integer mumbaiLocationId;
	
	@Value("${purchase.po.templates.basepath}")
	private String poTemplateBasePath;
	
	@Value("${legacy.service.url}")
	private String purchaseLegacyUrl;
	
	@Value("${purchase.bangalore.representative.name}")
	private String bangaloreReprentativeName;
	
	@Value("${bangalore.representative.phone.number}")
	private String bangaloreRepresentativePhoneNumber;
	
	@Value("${purchase.gurgaon.representative.name}")
	private String gurgaonRepresentativeName;
	
	@Value("${gurgaon.representative.phone.number}")
	private String gurgaonRepresentativePhoneNumber;
	
	@PersistenceContext
	private EntityManager em;
	
	//Added by Kamal Anand for Notifications
	@Autowired
	private PurchaseNotificationService notificationService;		
	
	
	@Value("${finance.head.role.Id}")
	String financeHeadRoleId;
	
	@Value("${system.admin.procurement.roleId}")
	String sysAdminProcurementRoleId;
	
	@Value("${purchase.nbuit.bu.unit.id}")
	private short itBuId;

	//End of Addition by Kamal Anand for Notifications
	
	@Value("${purchase.request.type.new.asset.request}")
	private Integer purchaseRequestTypeNewAssetRequest;
	
	
	public Boolean createPoService(PurchasePOCreateBean poBean,
			List<MultipartFile> files) throws DataAccessException {

		PurchaseDatPoBO poBo = new PurchaseDatPoBO();
		PurchaseDatAssetRequestBO assetReqBo = new PurchaseDatAssetRequestBO();
		DatEmpDetailBO sysAdminEmpDetailBO = new DatEmpDetailBO();
		Boolean success = false;
		try {

			sysAdminEmpDetailBO = empDetailRepository.findByPkEmpId(poBean
					.getCreatedBy());
			assetReqBo = assetRequestRepository
					.findByPkPurchaseRequestId(poBean.getAssetReqId());
			if (sysAdminEmpDetailBO == null) {
				throw new CommonCustomException(
						"Given logged in employee Id does't exist, Please pass proper employee Id");
			}
			if (sysAdminEmpDetailBO.getFkEmpMainStatus() == 2) {
				throw new CommonCustomException(
						"Given logged in employee Id is inactive, Please pass proper employee Id");
			}
			if(assetReqBo == null){
				throw new CommonCustomException(
						"The asset request Id does't exist, please pass proper id");
			}
		
			if (sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdminProcurment.intValue()
					|| sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdmin.intValue()
					|| sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == seniorSystemAdmin.intValue()) 
			{
				if(assetReqBo.getPurchaseRequestStatus() == purchaseCapexApprovedByBuHead)
				{
					if (files.size() > 0) {
						for (MultipartFile file : files) {
	
							long fileSizeInKB = file.getSize() / 1024;
	
							//long fileSizeInMB = fileSizeInKB / 1024;
	
							/*if (!file.getOriginalFilename().endsWith(".pdf")) {
								throw new CommonCustomException(
										"uploaded file is in different format, Please upload only pdf files");
							} else*/ //Commented by Shyam for phase-1 bug fix
							if (fileSizeInKB > 1024) {
								throw new CommonCustomException(
										"uploaded file should be 1 MB or less than 1 MB");
							}
						}
					}
					Calendar calendar = new GregorianCalendar();
					calendar.setTime(assetReqBo.getCreatedDate());
					int year = calendar.get(Calendar.YEAR);
					int month = calendar.get(Calendar.MONTH) + 1;
					
					if(poBean.getIsPoNeedToCreate() == true){
						String newPoNum  = assetReqBo.getPurchaseDatCapexBO().getCapexNo();
		
						poBo.setPoNo(newPoNum);
						poBo.setPreparedOn(new Date());
						poBo.setPreparedBy(poBean.getCreatedBy());
						poBo.setPoUploaded(purchaseCapexUploaded);
						poBo = poRepository.save(poBo);

						assetReqBo.setFkPurchasePoId(poBo.getPkPoId());
							
						//Added by Shyam for pdf generation
						List<PurchasePOPdfGenerationBean> beanList = new ArrayList<PurchasePOPdfGenerationBean>();
						PurchasePOPdfGenerationBean pdfGenerationBean = new PurchasePOPdfGenerationBean();
						
						//Need default or selected or agreed quote object, for vendor details
						PurchaseDatAssetQuoteBO agreedQuoteObject = new PurchaseDatAssetQuoteBO();
						try{
							agreedQuoteObject = assetQuoteRepository.getAgreeQuoteDetails(assetReqBo.
									getPkPurchaseRequestId());
						}catch(Exception ex){
							throw new CommonCustomException("Unable to get agreed quote details from backend. "
									+ "Kindly get in touch with Nucleus Support team.");
						}
								
						if(agreedQuoteObject == null)
						{
							throw new CommonCustomException("Default quote not found in Nucleus.");
						}
						String poCreatorName = gsrCommonService.getEmployeeName(poBean.getCreatedBy());
						pdfGenerationBean.setPoNumber(newPoNum);
						pdfGenerationBean.setCreatedDate(new Date());
						
						if(assetReqBo.getLocationId() == bangaloreLocationId.shortValue() 
								|| assetReqBo.getLocationId() != gurgaonLocationId.shortValue())
						{
							pdfGenerationBean.setThbsAddress(thbsBangaloreAddress);
							pdfGenerationBean.setThbsPhoneNumber(bangaloreRepresentativePhoneNumber);
							pdfGenerationBean.setThbsRepresentative(bangaloreReprentativeName);
						}
						else if(assetReqBo.getLocationId() == gurgaonLocationId.shortValue())
						{
							//If asset request is hardware then it will be buy from Gurgaon.
							//If it is software then it will be buy from Bangalore.
							
							if(assetReqBo.getPurchaseMasAssetsBO().getAssetType().equalsIgnoreCase("HARDWARE"))
							{
								pdfGenerationBean.setThbsAddress(thbsGurgaonAddress);
								pdfGenerationBean.setThbsPhoneNumber(gurgaonRepresentativePhoneNumber);
								pdfGenerationBean.setThbsRepresentative(gurgaonRepresentativeName);
							}else{
								pdfGenerationBean.setThbsAddress(thbsBangaloreAddress);
								pdfGenerationBean.setThbsPhoneNumber(bangaloreRepresentativePhoneNumber);
								pdfGenerationBean.setThbsRepresentative(bangaloreReprentativeName);
							}
						}
						else{
							throw new CommonCustomException("Asset requested location did not match with the expected address. Kindly get in touch with Nucleus Support team.");
						}
				
						DecimalFormat df = new DecimalFormat("#.##");      
						Double doubleNumber = Double.valueOf(df.format(agreedQuoteObject.getTotalCost()));
						
						pdfGenerationBean.setVendorAddress(agreedQuoteObject.getPurchaseDatSupplierDetailBO().
								getVendorAddress());
					
						pdfGenerationBean.setVendorRepresentative(agreedQuoteObject.getPurchaseDatSupplierDetailBO().
								getContactPerson());
						
						pdfGenerationBean.setVendorPhoneNumber(agreedQuoteObject.getPurchaseDatSupplierDetailBO().
								getVendorContactNo().toString());
						pdfGenerationBean.setReferenceQuoteNumber(agreedQuoteObject.getVendorQuoteReferenceNo());
						//pdfGenerationBean.setDescription(assetReqBo.getBriefDescription());
						pdfGenerationBean.setDescription(agreedQuoteObject.getAssetDescriptionEnteredBySysadmin());
						pdfGenerationBean.setQuantity(assetReqBo.getNoOfUnits());
						pdfGenerationBean.setUnitPrice(agreedQuoteObject.getIndividualLicenseCost());
						pdfGenerationBean.setAmount(doubleNumber);
						pdfGenerationBean.setGrandTotal(doubleNumber);
						pdfGenerationBean.setVendorName(agreedQuoteObject.getPurchaseDatSupplierDetailBO().getVendorName());
						pdfGenerationBean.setPoCreatorName(poCreatorName);
						pdfGenerationBean.setCurrencyType(agreedQuoteObject.getMasCurrencyType().getCurrencyTypeCode());
						
						String amountInwords = "";
						amountInwords = convertDoubleIntoWords(doubleNumber);
						pdfGenerationBean.setAmountInWords(amountInwords);
						
						beanList.add(pdfGenerationBean);
						//==============================
				
						//Added by Shyam for pdf generation
						String poFileName = newPoNum.replace("/", "_");
						
						FileOutputStream fos = null;
						try {
							String filePath = purchaseDocsBasePath + year + File.separator + month + File.separator
									+ assetReqBo.getPkPurchaseRequestId().intValue() + File.separator + "PO";
							File someFile = new File(purchaseDocsBasePath + year + File.separator + month + File.separator
									+ assetReqBo.getPkPurchaseRequestId().intValue() + File.separator + "PO");
							
							if (!someFile.exists()) {
								someFile.mkdirs();
							}
		
							filePath = filePath + File.separator + poFileName + ".pdf";
							someFile = new File(filePath);
							fos = new FileOutputStream(someFile);
		
							purchaseCapexSrvc.generateJasperReportPDFPO(poTemplateBasePath, fos, beanList);
		
							LOG.debug("<<<<<<<<<<<< PO  Created >>>>>>>>");
		
						} catch (FileNotFoundException e) {
							LOG.debug("Error in creating PO file : " + e);
						}  finally {
							if (fos != null) {
								try {
									fos.flush();
									fos.close();
								} catch (IOException e) {
									LOG.debug("Error in creating PO file : closing file " + e);
								}
							}
						}
					}
					//----------------------------
				
					//EOA by Shyam
				
					assetReqBo.setPurchaseRequestStatus(purchasePoCreatedStatus);
					assetReqBo.setSysAdminUpdatedOn(new Date());
					assetReqBo.setSysAdminId(poBean.getCreatedBy());
					assetReqBo.setModifiedBy(poBean.getCreatedBy());
					assetReqBo.setModifiedOn(new Date());
				
					assetRequestRepository.save(assetReqBo);
					

					try {
						if (files.size() > 0) {
							for (MultipartFile file : files) {
	
								// String basePath = purchaseDocsBasePath;
								String newFileName = "SysAdmin_"
										+ file.getOriginalFilename();
								File dir = new File(purchaseDocsBasePath + year + "/" + month + "/"
										+ assetReqBo.getPkPurchaseRequestId()
										+ "/Supporting docs/SystemAdmin");
	
								if (dir.mkdirs()) {
									File fileToBeStored = new File(dir.getPath()
											+ "/" + newFileName);
									FileCopyUtils.copy(file.getBytes(),
											fileToBeStored);
								} else {
									File fileToBeStored = new File(dir.getPath()
											+ "/" + newFileName);
									FileCopyUtils.copy(file.getBytes(),
											fileToBeStored);
								}
							
							}
						}
						
						success =true;
						
						} catch (IOException e) {
						
							throw new CommonCustomException(
								"Supporting document files not uploaded");
						}
					}
					else{
						throw new CommonCustomException(
							"To Create PO asset request status should be in Capex Approved by BU Head status, Please pass proper status ");
					}
				}
				else {
					throw new CommonCustomException(
						"The logged in user don't have access to Create PO.Please pass proper employee id with sysadmin access");
				}
			} catch (Exception e) {
				success = false;
				e.printStackTrace();
				LOG.info("exception : "+e.getMessage());
				throw new DataAccessException(e.getMessage());
			}

			//Added by Kamal for Notifications
			try{
				List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(financeHeadRoleId));
				empDetail.forEach(emp -> {
					try {
						notificationService.setPOCreatedNotificationToFinanceHeadScreen(emp.getPkEmpId());
					} catch (Exception e) {
						LOG.info("Exception occurred while setting PO Created Notification");
					}
				});
				List<DatEmpDetailBO> empDetail1 = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
				if(empDetail != null)
				{
					empDetail1.forEach(emp -> {
						try {
							notificationService.setCapexApprovedNotificationToSysAdminScreen(emp.getPkEmpId());
						} catch (Exception e) {
							LOG.info("Exception occured while setting notification in Update and Approve Asset Request");
						}
					});
				}
			} catch(Exception e){
				LOG.info("Exception occurred while getting employee details");
			}
			//End of Addition by Kamal for Notifications
			return success;
		}
	
	
	
	public List<PurchaseCapexOutputBean> getAllPORequests(
			ViewAllAssetRequestBean assetReqBean) throws DataAccessException, CommonCustomException {
		LOG.entering("getAllPORequests service method");
		List<PurchaseDatAssetRequestBO> listOfAssetRequest = new ArrayList<PurchaseDatAssetRequestBO>();
		List<PurchaseCapexOutputBean> listOfAssetRequestBean = new ArrayList<PurchaseCapexOutputBean>();
		try {
			Specification<PurchaseDatAssetRequestBO> assetRequestSpecification = AssetRequestSpecification
					.getAllAssetRequests(assetReqBean);

			listOfAssetRequest = assetRequestRepository
					.findAll(assetRequestSpecification);
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception occured at service method getAllPORequests ",
					e);
		}

		PurchaseCapexOutputBean capexReqOutputBean;
		for (PurchaseDatAssetRequestBO assetReqBo : listOfAssetRequest) {
			capexReqOutputBean = new PurchaseCapexOutputBean();
			if(	assetReqBo.getPurchaseRequestStatus() == purchaseRequestClosed
					|| assetReqBo.getPurchaseRequestStatus() == purchaseRequestApprovedByFinance
					|| assetReqBo.getPurchaseRequestStatus() == requestPoRejectedByFinanceStatus){
			/*capexReqOutputBean.setCapexNumber(assetReqBo
					.getPurchaseDatCapexBO().getCapexNo());*/
			capexReqOutputBean.setBusinessUnit(assetReqBo.getMasBuUnitBO()
					.getBuUnitName());
			capexReqOutputBean.setRaisedOn(assetReqBo.getPurchaseDatCapexBO()
					.getCreatedOn());
			//capexReqOutputBean.setProjectName(assetReqBo.getProjectName());
			// get component & client details from legacy service
			ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean(); 
			clientProjCompDetailsBean.setClientId( assetReqBo.getClientId());
			clientProjCompDetailsBean.setProjectId( assetReqBo.getProjectId());
			clientProjCompDetailsBean.setComponentId(assetReqBo.getProjectComponentId());
			clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
						
			PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
			ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService.
					getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
						
			if(clientProjectCompDetails == null){
				throw new CommonCustomException("Unable to fetch project, client and component details from legacy MIS.");
			}else{
				capexReqOutputBean.setProjectName(clientProjectCompDetails.getProjectName());
			}
			capexReqOutputBean.setStatus(assetReqBo
					.getPurchaseMasRequestStatusBO()
					.getPurchaseRequestStatusName().trim());
			capexReqOutputBean.setStatusId(assetReqBo
					.getPurchaseMasRequestStatusBO().getPurchaseRequestStatusId());
			capexReqOutputBean.setAssetRequestId(assetReqBo.getPkPurchaseRequestId());
			capexReqOutputBean.setPoDetails(assetReqBo.getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());
			listOfAssetRequestBean.add(capexReqOutputBean);
			}
		}

		LOG.exiting("getAllPORequests service method");
		return listOfAssetRequestBean;

	}
	
	public Boolean downloadPo(Integer assetId,HttpServletResponse response) throws DataAccessException{
		
		LOG.entering("downloadPo service method");
		Boolean isPoDownloaded = false;
		try{
			
			PurchaseDatAssetRequestBO assetReqBO = assetRequestRepository.findByPkPurchaseRequestId(assetId);
			
			String outputFileName = assetReqBO.getPurchaseDatPoBO().getPoNo()+".pdf";
			outputFileName = outputFileName.replace("/", "_");
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(assetReqBO.getCreatedDate());
			int year = calendar.get(Calendar.YEAR);
			
			//String BASEPATH = "D:/Anil/pdf/"+year+"/"+(assetReqBO.getCreatedDate().getMonth()+1)+"/"+assetReqBO.getPkPurchaseRequestId()+"/PO/";
			
			String BASEPATH = purchaseDocsBasePath + year+"/"+(assetReqBO.getCreatedDate().getMonth()+1)+"/"+assetReqBO.getPkPurchaseRequestId()+"/PO/";
			
			File file = new File(BASEPATH+outputFileName);
			
			if(file.exists()){
				DownloadFileUtil.downloadFile(response, BASEPATH,
						outputFileName);
				isPoDownloaded = true;
			}else{
				throw new CommonCustomException("File does't exists in required server path ");
			}		
			
		}catch(Exception e){
			throw new DataAccessException(e.getMessage());
		}
		
		LOG.exiting("downloadPo service method");
		return isPoDownloaded;
		
	}
	
	public Boolean downloadSupportingDocsByEmployeeOrRm(Integer assetId,HttpServletResponse response) throws DataAccessException{
		
		LOG.entering("download downloadSupportingDocsByEmployeeOrRm service method");
		Boolean isPoDownloaded = false;
		try{
			
			PurchaseDatAssetRequestBO assetReqBO = assetRequestRepository.findByPkPurchaseRequestId(assetId);
			
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(assetReqBO.getCreatedDate());
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH) + 1;
			String BASEPATH = purchaseDocsBasePath + year+"/"+month+"/"+assetReqBO.getPkPurchaseRequestId()+"/Supporting docs/EmpOrRm/";
		
			File file = new File(BASEPATH);
			if(file.exists()){
				DownloadFileUtil.downloadAllFile(response, BASEPATH);
				isPoDownloaded = true;
			}else{
				throw new CommonCustomException("File does't exists in required server path ");
			}
			
		}catch(Exception e){
			throw new DataAccessException(e.getMessage());
		}
		
		LOG.exiting("download downloadSupportingDocsByEmployeeOrRm service method");
		return isPoDownloaded;
		
	}
	
	public Boolean downloadSupportingDocsBySysAdmin(Integer assetId,HttpServletResponse response) throws DataAccessException{
		
		LOG.entering("download supporting docs for sysadmin service method");
		Boolean isPoDownloaded = false;
		try{
			
			PurchaseDatAssetRequestBO assetReqBO = assetRequestRepository.findByPkPurchaseRequestId(assetId);
			
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(assetReqBO.getCreatedDate());
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH) + 1;
			String basePathSysAdmin = purchaseDocsBasePath + year+"/"+month+"/"+assetReqBO.getPkPurchaseRequestId()+"/Supporting docs/SystemAdmin/";
			String basePathEmp = purchaseDocsBasePath +year+"/"+month+"/"+assetReqBO.getPkPurchaseRequestId()+"/Supporting docs/EmpOrRm/";
			List<String> listOfFiles = new ArrayList<String>();
			File fileSysAdmin = new File(basePathSysAdmin);
			File[] listOfSysFiles = fileSysAdmin.listFiles(); 
			
			File fileEmp = new File(basePathEmp);
			File[] listOfEmp = fileEmp.listFiles(); 
			
			if(fileSysAdmin.isDirectory()){
				if(listOfSysFiles.length > 0 ){
					listOfFiles.add(basePathSysAdmin);
				}
				}
			
			if( fileEmp.isDirectory()){
				if(listOfEmp.length > 0){
					listOfFiles.add(basePathEmp);
				}
				}
			
			
			
			if(listOfFiles.size() > 0){
				DownloadFileUtil.downloadFilesTwoPath(response, listOfFiles);
				isPoDownloaded = true;
			}else{
				throw new CommonCustomException("File does't exists in required server path ");
			}
			
		}catch(Exception e){
			throw new DataAccessException(e.getMessage());
		}
		
		LOG.exiting("download supporting docs for sysadmin service method");
		return isPoDownloaded;
		
	}
	
	public PurchaseSavePoOutputBean savePurchaseOrderDetails(
			PurchaseOrderDetailsSaveBean savePoDetails,List<MultipartFile> files)
			throws DataAccessException 
	{
		LOG.entering("savePurchaseOrderDetails service method");
		PurchaseSavePoOutputBean saveOutputBean = new PurchaseSavePoOutputBean();
		DatEmpDetailBO sysAdminEmpDetailBO = new DatEmpDetailBO();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		
		try {
			sysAdminEmpDetailBO = empDetailRepository.findByPkEmpId(savePoDetails.getSystemAdminId());
			PurchaseDatAssetRequestBO assetReqBO = assetRequestRepository.findByPkPurchaseRequestId(
					savePoDetails.getAssetReqId());
			if(assetReqBO.getPurchaseMasAssetsBO().getAssetType().equalsIgnoreCase("SOFTWARE")
					&& savePoDetails.getIsSoftwareOrHardwarePurchased().equalsIgnoreCase("YES")
					&& assetReqBO.getFkRequestTypeId().intValue() == purchaseRequestTypeNewAssetRequest.intValue())
				//Added by Shyam for phase-1 bug fix
			{
				if(savePoDetails.getSoftwareLicOrSubscriptionId() == null || 
						savePoDetails.getSoftwareLicOrSubscriptionId().isEmpty())
				{
					throw new CommonCustomException("Software subscription id is mandatory for software type, Please pass proper id");
				}
			}
			
			if (sysAdminEmpDetailBO.getFkEmpMainStatus().intValue() == 2) 
			{
				throw new CommonCustomException("Given logged in employee Id is inactive, Please pass proper employee Id");
			}
			
			if (sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdminProcurment.intValue()
					|| sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdmin.intValue()
					|| sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == seniorSystemAdmin.intValue()) 
			{
				if (assetReqBO.getPurchaseRequestStatus().intValue() == purchaseRequestApprovedByFinance.intValue()) 
				{
					if (files.size() > 0) 
					{
						for (MultipartFile file : files) 
						{
							long fileSizeInKB = file.getSize() / 1024;
							
							if (!file.getOriginalFilename().endsWith(".pdf") && !file.getOriginalFilename().endsWith(".xls")) {
								throw new CommonCustomException(
										"uploaded file is in different format, Please upload only pdf or xls files");
							} else if (fileSizeInKB > 1024) {
								throw new CommonCustomException(
										"uploaded file should be 1 MB or less than 1 MB");
							}
						}
					}

					Calendar calendar = new GregorianCalendar();
					calendar.setTime(assetReqBO.getCreatedDate());
					int year = calendar.get(Calendar.YEAR);
					int month = calendar.get(Calendar.MONTH) + 1;
					
					
					assetReqBO.setSysAdminId(savePoDetails.getSystemAdminId());
					assetReqBO.setSysAdminUpdatedOn(new Date());
					assetReqBO.setModifiedBy(savePoDetails.getSystemAdminId());
					assetReqBO.setModifiedOn(new Date());
					assetReqBO.setPurchaseRequestStatus(purchasePoClosedBySysAdmin);

					if (savePoDetails.getIsSoftwareOrHardwarePurchased().equalsIgnoreCase("YES")) 
					{
						PurchaseMasAssetDetailsBO masAssetDetailBo = null;
						//PurchaseDatAssetIncrementBO assetIncrementBO = null;
						assetReqBO.setIsSoftwareOrHardwarePurchased("YES");
						
						if (assetReqBO.getPurchaseMasAssetsBO().getAssetType().equalsIgnoreCase("HARDWARE")) 
						{
							if (assetReqBO.getFkRequestTypeId().intValue() == 1) 
							{
								StoredProcedureQuery query = this.em.createNamedStoredProcedureQuery("ADD_ASSET_DEATILS");
								query.setParameter("assetDescription", assetReqBO.getBriefDescription());
								query.setParameter("fromDate", savePoDetails.getAssetAvailableFromDate());
								query.setParameter("toDate", savePoDetails.getAssetAvailableToDate());
								query.setParameter("assetId", assetReqBO.getFkAssetId());
								query.setParameter("createdBy", savePoDetails.getSystemAdminId());
								query.setParameter("locationId", ((Short) assetReqBO.getLocationId()).intValue());
								query.setParameter("noOfUnits", assetReqBO.getNoOfUnits());
								query.setParameter("purchaseReqId", assetReqBO.getPkPurchaseRequestId());
								query.execute();
								String firstAssetId = (String) query.getOutputParameterValue("firstAssetID");
								String lastAssetID = (String) query.getOutputParameterValue("lastAssetID");
								saveOutputBean.setFirstAssetId(firstAssetId);
								saveOutputBean.setLastAssetId(lastAssetID);
								if(assetReqBO.getNoOfUnits().intValue() > 1)
								{
									saveOutputBean.setMessage("Asset subscription Id from " + saveOutputBean.getFirstAssetId()
												+ " to " + saveOutputBean.getLastAssetId()
												+ " has been created successfully");
								}else{
									saveOutputBean.setMessage("Asset subscription Id " + saveOutputBean.getFirstAssetId()
											+ " has been created successfully");
								}
							}
							else if (assetReqBO.getFkRequestTypeId().intValue() == 2) //For AMC
							{
								//Update existing record added by Shyam
								masAssetDetailBo = new PurchaseMasAssetDetailsBO();
								masAssetDetailBo = masAssetDetailsRepository.findByFkPurchaseRequestId(assetReqBO.getPkPurchaseRequestId());
								
								if(masAssetDetailBo == null)
									throw new CommonCustomException("For AMC, Asset details record not found");
							
								masAssetDetailBo.setAssetAvailableFromDate(savePoDetails.getAssetAvailableFromDate());
								masAssetDetailBo.setAssetAvailableToDate(savePoDetails.getAssetAvailableToDate());
								masAssetDetailBo.setModifiedBy(savePoDetails.getSystemAdminId());
								masAssetDetailBo.setModifiedDate(new Date());
							
								masAssetDetailBo = masAssetDetailsRepository.save(masAssetDetailBo);
								//EOA by Shyam
								
								saveOutputBean.setMessage("Asset request has been updated successfully for AMC");
							} 
							else if (assetReqBO.getFkRequestTypeId().intValue() == 3) //For rental
							{
								masAssetDetailBo = new PurchaseMasAssetDetailsBO();
								masAssetDetailBo = masAssetDetailsRepository.findByFkPurchaseRequestId(assetReqBO.getPkPurchaseRequestId());
								
								if(masAssetDetailBo == null)
									throw new CommonCustomException("Asset details record not found");
								
								if(masAssetDetailBo.getSoftwareLicenseOrSubscriptionId() == null)
								{
									throw new CommonCustomException("Hardware Id is null, kindly get in touch with Nucleus Support team.");								
								}
								
								masAssetDetailBo.setAssetAvailableFromDate(savePoDetails.getAssetAvailableFromDate());
								masAssetDetailBo.setAssetAvailableToDate(savePoDetails.getAssetAvailableToDate());
								masAssetDetailBo.setModifiedBy(savePoDetails.getSystemAdminId());
								masAssetDetailBo.setModifiedDate(new Date());
								
								masAssetDetailBo = masAssetDetailsRepository.save(masAssetDetailBo);
								
								saveOutputBean.setMessage("Asset " + masAssetDetailBo.getSoftwareLicenseOrSubscriptionId()
												+ " has been rented from " + sdf.format(savePoDetails.getAssetAvailableFromDate())
												+ " to " + sdf.format(savePoDetails.getAssetAvailableToDate())
												+ " successfully");
							}
						} 
						else if (assetReqBO.getPurchaseMasAssetsBO().getAssetType().equalsIgnoreCase("SOFTWARE")) 
						{
							if (assetReqBO.getFkRequestTypeId().intValue() == 1) 
							{
								for(int i = 0;i < assetReqBO.getNoOfUnits() ; i++)
								{
									masAssetDetailBo = new PurchaseMasAssetDetailsBO();
									masAssetDetailBo.setSoftwareLicenseOrSubscriptionId(savePoDetails.getSoftwareLicOrSubscriptionId());
									masAssetDetailBo.setAssetAvailableFromDate(savePoDetails.getAssetAvailableFromDate());
									masAssetDetailBo.setAssetAvailableToDate(savePoDetails.getAssetAvailableToDate());
									masAssetDetailBo.setCreatedBy(savePoDetails.getSystemAdminId());
									masAssetDetailBo.setCreatedDate(new Date());
									masAssetDetailBo.setAssetDescription(assetReqBO.getBriefDescription());
									masAssetDetailBo.setFkMasAssetsId(assetReqBO.getFkAssetId());
									masAssetDetailBo.setFklocationId(((Short) assetReqBO.getLocationId()).intValue());
									masAssetDetailBo.setFkPurchaseRequestId(assetReqBO.getPkPurchaseRequestId());
								
									masAssetDetailBo = masAssetDetailsRepository.save(masAssetDetailBo);
								}
								
								saveOutputBean.setSoftwareLicenseId(masAssetDetailBo.getSoftwareLicenseOrSubscriptionId());
								saveOutputBean.setMessage("Asset " + saveOutputBean.getSoftwareLicenseId()
										+ " has been created successfully");
							}
							
							else if(assetReqBO.getFkRequestTypeId().intValue() == 4)//For renewal
							{
								//Added by Shyam for phase-2 changes
								masAssetDetailBo = new PurchaseMasAssetDetailsBO();
								masAssetDetailBo = masAssetDetailsRepository.findByFkPurchaseRequestId(assetReqBO.getPkPurchaseRequestId());
								
								if(masAssetDetailBo == null)
									throw new CommonCustomException("For renewal, Asset details record not found");
								
								masAssetDetailBo.setModifiedBy(savePoDetails.getSystemAdminId());
								masAssetDetailBo.setModifiedDate(new Date());
								masAssetDetailBo.setAssetAvailableFromDate(savePoDetails.getAssetAvailableFromDate());
								masAssetDetailBo.setAssetAvailableToDate(savePoDetails.getAssetAvailableToDate());
								//masAssetDetailBo.setAssetDescription(assetReqBO.getBriefDescription());
								masAssetDetailBo = masAssetDetailsRepository.save(masAssetDetailBo);
								
								saveOutputBean.setMessage("Software " + masAssetDetailBo.getSoftwareLicenseOrSubscriptionId() +
										" has been renewed successfully"); 
							}
						}
						
						assetReqBO = assetRequestRepository.save(assetReqBO);
						
						//Added by Kamal Anand for Notifications
						List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
						if(empDetail != null)
						{
							empDetail.forEach(emp -> {
								try {
									notificationService.setPOApprovedNotificationtoSysAdminScreen(emp.getPkEmpId());
								} catch (Exception e) {
									LOG.info("Exception occured while setting notification in Approve PO");
								}
							});
						}
						//End of Addition by Kamal Anand for Notifications 
						
						try {
							if (files.size() > 0) {
								for (MultipartFile file : files) {
									// String basePath = purchaseDocsBasePath;
									String newFileName = file.getOriginalFilename();
									File dir = new File(purchaseDocsBasePath + year + "/" + month + "/"
											+ assetReqBO.getPkPurchaseRequestId() + "/Invoice");
	
									if (dir.mkdirs()) {
										File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
										FileCopyUtils.copy(file.getBytes(),	fileToBeStored);
									} else {
										File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
										FileCopyUtils.copy(file.getBytes(), fileToBeStored);
									}
								}
							}
						} catch (IOException e) {
							throw new CommonCustomException("Invoice files not uploaded");
						}
						
					}
					
					else if (savePoDetails.getIsSoftwareOrHardwarePurchased().equalsIgnoreCase("NO"))
					{
						//Added by Shyam for phase-2
						if(savePoDetails.getSysAdminClosureComments() == null)
							throw new CommonCustomException("Closure comments can not be blank or null");
						assetReqBO.setClosureComment(savePoDetails.getSysAdminClosureComments());
						assetReqBO.setIsSoftwareOrHardwarePurchased("NO");
						assetReqBO.setModifiedBy(savePoDetails.getSystemAdminId());
						assetReqBO.setModifiedOn(new Date());
						assetRequestRepository.save(assetReqBO);
						
						if(assetReqBO.getFkRequestTypeId().intValue() == 1 || 
								assetReqBO.getFkRequestTypeId().intValue() == 2 ||
								assetReqBO.getFkRequestTypeId().intValue() == 3 || 
								assetReqBO.getFkRequestTypeId().intValue() == 4)
						{//EOA by Shyam for phase-2
							//Added by Kamal Anand for Notifications
							List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
							if(empDetail != null)
							{
								empDetail.forEach(emp -> {
									try {
										notificationService.setPOApprovedNotificationtoSysAdminScreen(emp.getPkEmpId());
									} catch (Exception e) {
										LOG.info("Exception occured while setting notification in Approve PO");
									}
								});
							}
							//End of Addition by Kamal Anand for Notifications 

							saveOutputBean.setMessage("Asset has not been purchased");
						}
										
						try {
							if (files.size() > 0) {
								for (MultipartFile file : files) {
									// String basePath = purchaseDocsBasePath;
									String newFileName = file.getOriginalFilename();
									File dir = new File(purchaseDocsBasePath + year + "/" + month + "/"
											+ assetReqBO.getPkPurchaseRequestId() + "/Invoice");
	
									if (dir.mkdirs()) {
										File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
										FileCopyUtils.copy(file.getBytes(),	fileToBeStored);
									} else {
										File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
										FileCopyUtils.copy(file.getBytes(), fileToBeStored);
									}
								}
							}
						} catch (IOException e) {
							throw new CommonCustomException("Supporting document files not uploaded");
						}
					}
				} else {
					throw new CommonCustomException(
						"To save PO details asset request status should be in PO Approved by finance status, Please pass proper status ");
				}
			} else {
				throw new CommonCustomException(
					"The logged in user don't have access to Save PO details .Please pass proper employee id with sysadmin access");
			}
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}

		LOG.exiting("savePurchaseOrderDetails service method");
		return saveOutputBean;

	}
	
	
	public PurchaseDatAssetRequestBO cancelPOBySysAdmin(
			PurchaseCancelPoBean cancelPoBean) throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Cancel PO by sys admin");
		
		PurchaseDatAssetRequestBO purchaseDatAssetRequestDetails = new PurchaseDatAssetRequestBO();
		DatEmpDetailBO sysAdminEmpDetailBO = new DatEmpDetailBO();
		try {

			purchaseDatAssetRequestDetails = assetRequestRepository
					.findByPkPurchaseRequestId(cancelPoBean
							.getPurchaseRequestId());
		
		
				sysAdminEmpDetailBO = empDetailRepository.findByPkEmpId(cancelPoBean.getLoggedInUser());
		
			
				if(sysAdminEmpDetailBO.getFkEmpMainStatus() == 2){
					throw new CommonCustomException("Given logged in employee Id is inactive, Please pass proper employee Id");
				}

		if(sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdminProcurment.intValue()||
				sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdmin.intValue() ||
				sysAdminEmpDetailBO.getFkEmpRoleId().shortValue()== seniorSystemAdmin.intValue())	
			{
		if (purchaseDatAssetRequestDetails == null) {
			throw new CommonCustomException("Invalid Asset request Id.");
		}

		else if (purchaseDatAssetRequestDetails.getPurchaseRequestStatus() == requestPoRejectedByFinanceStatus) {

			throw new CommonCustomException("PO is already cancelled by Finance");
		} else if (purchaseDatAssetRequestDetails.getPurchaseRequestStatus() == purchasePoRejectedBySysAdmin) {

			throw new CommonCustomException("PO is already cancelled by SysAdmin");
		} else if (purchaseDatAssetRequestDetails.getPurchaseRequestStatus() == purchaseRequestClosed) {

			throw new CommonCustomException("Purchase asset request is closed");
		} 
		else if (purchaseDatAssetRequestDetails.getPurchaseRequestStatus() == purchasePoCreatedBySysAdmin ||
				purchaseDatAssetRequestDetails.getPurchaseRequestStatus() == purchaseRequestApprovedByFinance) {
		
			purchaseDatAssetRequestDetails
					.setPurchaseRequestStatus(purchasePoRejectedBySysAdmin);
			purchaseDatAssetRequestDetails.setSysAdminUpdatedOn(new Date());
			purchaseDatAssetRequestDetails.setSysAdminId(cancelPoBean
					.getLoggedInUser());
			purchaseDatAssetRequestDetails.setSysAdminComment(cancelPoBean
					.getFinanceExComment());

			purchaseDatAssetRequestDetails.setModifiedOn(new Date());
			purchaseDatAssetRequestDetails.setModifiedBy(cancelPoBean.getLoggedInUser());
			assetRequestRepository.save(purchaseDatAssetRequestDetails);

			if (purchaseDatAssetRequestDetails.getFkPurchasePoId() != null) {

				PurchaseDatPoBO purchaseDatPoBO = poRepository
						.findByPkPoId(purchaseDatAssetRequestDetails
								.getFkPurchasePoId());

				purchaseDatPoBO.setModifiedBy(cancelPoBean.getLoggedInUser());
				purchaseDatPoBO.setModifiedOn(new Date());
				poRepository.save(purchaseDatPoBO);

			}
			
		//Added by Kamal Anand for Notifications
		List<DatEmpDetailBO> empDetail1 = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(financeHeadRoleId));
		empDetail1.forEach(emp -> {
			try {
				notificationService.setPOCreatedNotificationToFinanceHeadScreen(emp.getPkEmpId());
			} catch (Exception e) {
				LOG.info("Exception occurred while setting PO Created Notification");
			}
		});
		//End of Addition by Kamal Anand for Notifications
			 
		} else {
			throw new CommonCustomException(
					"PO can be cancelled only if it is in created status by SystemAdmin. Please pass asset request Id with proper status ");
		}
			 } else { throw new CommonCustomException(
					  "The logged in user don't have access to cancel PO.Please pass proper employee id with sysadmin access"
					  ); }
		

		} catch (Exception ex) {
			throw new DataAccessException(ex.getMessage());
		}
		LOG.endUsecase("Cancel PO");
		return purchaseDatAssetRequestDetails;
	}
	

	public PurchaseOrderSysAdminOutputBean getPurchaseOrderDetailsSysAdmin(
			Integer purchaseRequestId) throws CommonCustomException, DataAccessException 
	{
		LOG.startUsecase("Get Purchase Order Details");
		
		PurchaseDatAssetRequestBO purchaseDatAssetRequestDetails = new PurchaseDatAssetRequestBO();
		PurchaseDatAssetIncrementBO assetIncrementData = new PurchaseDatAssetIncrementBO();
		PurchaseOrderSysAdminOutputBean detailsOutputBean = new PurchaseOrderSysAdminOutputBean();
		MasBuUnitBO itBuDetail = null;
		try {
			purchaseDatAssetRequestDetails = assetRequestRepository.findByPkPurchaseRequestId(purchaseRequestId);
			
			if (purchaseDatAssetRequestDetails == null) {
				throw new CommonCustomException("The asset request Id does't exist, please pass proper id");
			}
		} catch (Exception ex) {
			throw new CommonCustomException("Failed to fecth details from db.");
		}
		
		try{
			assetIncrementData = assetIncrementRepository.getLastAssetId(
					purchaseDatAssetRequestDetails.getFkAssetId(), purchaseDatAssetRequestDetails.getLocationId());
			PurchaseDatAssetQuoteBO purchaseDatAssetQuoteBO = new PurchaseDatAssetQuoteBO();
			purchaseDatAssetQuoteBO = assetQuoteRepository.findByFkPurchaseRequestIdAndQuoteStatus(purchaseRequestId,"AGREE");
		
		/*if(purchaseDatAssetRequestDetails.getFkPurchasePoId() == null && purchaseDatAssetQuoteBO.getTotalCost() > 5000){
			throw new CommonCustomException("PO has not created for this asset request, Please pass request Id with PO");
		}else*/ 
			if(purchaseDatAssetRequestDetails.getFkPurchasePoId() != null)
			{
				detailsOutputBean.setPoNumber(purchaseDatAssetRequestDetails.getPurchaseDatPoBO().getPoNo());
			}
			
			detailsOutputBean.setSystemAdminComments(purchaseDatAssetRequestDetails.getSysAdminComment());
			detailsOutputBean.setPriority(purchaseDatAssetRequestDetails.getPriority());
			detailsOutputBean.setCapexNumber(purchaseDatAssetRequestDetails.getPurchaseDatCapexBO().getCapexNo());
			detailsOutputBean.setCastCenter(purchaseDatAssetRequestDetails.getMasBuUnitBO().getBuUnitName());
			detailsOutputBean.setIsClientReimbursing(purchaseDatAssetRequestDetails.getIsClientReimbursing());
		
			//added by pratibha
			detailsOutputBean.setFkRequestTypeId(purchaseDatAssetRequestDetails.getFkRequestTypeId());
			detailsOutputBean.setPurchaseRequestTypeName(purchaseDatAssetRequestDetails.getPurchaseDatRequestTypeBO().getPurchaseRequestTypeName());
			detailsOutputBean.setRmComment(purchaseDatAssetRequestDetails.getRmComment());
			detailsOutputBean.setSysAdminComment(purchaseDatAssetRequestDetails.getSysAdminComment());
			detailsOutputBean.setFinanceExComment(purchaseDatAssetRequestDetails.getFinanceEx_comment());
			detailsOutputBean.setBuHeadComment(purchaseDatAssetRequestDetails.getClosureComment());
			
			detailsOutputBean.setRequestorName(gsrGoalService.getEmpNameByEmpId(
					purchaseDatAssetRequestDetails.getRequestorId().intValue()) + '-' 
					+ purchaseDatAssetRequestDetails.getRequestorId());
		
			if(purchaseDatAssetRequestDetails.getRmToApprove() != null)
				detailsOutputBean.setReportingManager(gsrGoalService.getEmpNameByEmpId(
					purchaseDatAssetRequestDetails.getRmToApprove().intValue()) + '-' 
					+ purchaseDatAssetRequestDetails.getRmToApprove());
			
			if(purchaseDatAssetRequestDetails.getSysAdminId() != null)	
				detailsOutputBean.setSysAdminName(gsrGoalService.getEmpNameByEmpId(
						purchaseDatAssetRequestDetails.getSysAdminId().intValue()) + '-'
						+ purchaseDatAssetRequestDetails.getSysAdminId());
			
			if(purchaseDatAssetRequestDetails.getFinanceEx_updated_by() != null)
				detailsOutputBean.setFinanceHeadName(gsrGoalService.getEmpNameByEmpId(
						purchaseDatAssetRequestDetails.getFinanceEx_updated_by().intValue()) + '-' 
						+ purchaseDatAssetRequestDetails.getFinanceEx_updated_by());
			
			if(purchaseDatAssetQuoteBO != null)
			{
				detailsOutputBean.setFkCurrencyType(purchaseDatAssetQuoteBO.getFkCurrencyType());
				if(purchaseDatAssetQuoteBO.getCostBorneBy().equalsIgnoreCase("shared"))
				{
					itBuDetail = buRepository.findByPkBuUnitId(itBuId);
					
					detailsOutputBean.setBuHeadName(gsrGoalService.getEmpNameByEmpId(
							purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId()) + '-' 
							+ purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId());
				 
					if(purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId() != 
							itBuDetail.getFkBuHeadEmpId())
					{
						detailsOutputBean.setItHeadName(gsrGoalService.getEmpNameByEmpId(itBuDetail.getFkBuHeadEmpId()) 
								+ '-' + itBuDetail.getFkBuHeadEmpId());
					}
				}
			
				if(purchaseDatAssetQuoteBO.getCostBorneBy().equalsIgnoreCase("bu"))
				{
					detailsOutputBean.setBuHeadName(gsrGoalService.getEmpNameByEmpId(
							purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId()) + '-' 
							+ purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId());
				}
			
				if(purchaseDatAssetQuoteBO.getCostBorneBy().equalsIgnoreCase("IT"))
				{
					itBuDetail = buRepository.findByPkBuUnitId(itBuId);
					
					detailsOutputBean.setItHeadName(gsrGoalService.getEmpNameByEmpId(itBuDetail.getFkBuHeadEmpId()) 
							+ '-' + itBuDetail.getFkBuHeadEmpId());
				}
			}
			
			// EOA pratibha
			
			detailsOutputBean.setLocation(purchaseDatAssetRequestDetails.getMasLocationParentBO().getLocationParentName());
			detailsOutputBean.setAssetType(purchaseDatAssetRequestDetails.getPurchaseMasAssetsBO().getAssetType());
			detailsOutputBean.setPurchaseAssetStatusId(purchaseDatAssetRequestDetails.getPurchaseRequestStatus());
			detailsOutputBean.setPurchaseAssetStatus(purchaseDatAssetRequestDetails.getPurchaseMasRequestStatusBO().
					getPurchaseRequestStatusName().trim());
			
			if(purchaseDatAssetRequestDetails.getFkRequestTypeId().intValue() == 1)
			{
				if(purchaseDatAssetRequestDetails.getPurchaseRequestStatus().intValue() == 
						purchasePoClosedBySysAdmin.intValue() && purchaseDatAssetRequestDetails.
						getIsSoftwareOrHardwarePurchased().equalsIgnoreCase("YES"))
				{
					detailsOutputBean.setIsHardWareOrSoftwarePurchased(purchaseDatAssetRequestDetails.
							getIsSoftwareOrHardwarePurchased());
					List<PurchaseMasAssetDetailsBO> listOfAssetDetails = masAssetDetailsRepository.
							findByFkPurchaseRequestIdAndFklocationIdAndFkMasAssetsId(
									purchaseDatAssetRequestDetails.getPkPurchaseRequestId()
									, (int)purchaseDatAssetRequestDetails.getLocationId(), 
									purchaseDatAssetRequestDetails.getFkAssetId());
			
					if(listOfAssetDetails.size() > 0 )
					{
						if(purchaseDatAssetRequestDetails.getPurchaseMasAssetsBO().getAssetType().
								equalsIgnoreCase("HARDWARE"))
						{
							detailsOutputBean.setAvailableFromDate(listOfAssetDetails.get(0).
									getAssetAvailableFromDate());
							detailsOutputBean.setAvailableToDate(listOfAssetDetails.get(0).
									getAssetAvailableToDate());
							List<String> listAssets = new ArrayList<String>();
							for(PurchaseMasAssetDetailsBO assetDetail : listOfAssetDetails)
							{
								listAssets.add(assetDetail.getSoftwareLicenseOrSubscriptionId());
							}
							detailsOutputBean.setGeneratedAssetId(listAssets);
						}else{
							detailsOutputBean.setAvailableFromDate(listOfAssetDetails.get(0).
									getAssetAvailableFromDate());
							detailsOutputBean.setAvailableToDate(listOfAssetDetails.get(0).getAssetAvailableToDate());
							detailsOutputBean.setSoftwareLicenseOrSubscriptionId(listOfAssetDetails.get(0).
									getSoftwareLicenseOrSubscriptionId());
						}
					}
				}else{
					detailsOutputBean.setIsHardWareOrSoftwarePurchased(purchaseDatAssetRequestDetails.
							getIsSoftwareOrHardwarePurchased());
					if(assetIncrementData != null)
					{	
						String lastAssetId = String.valueOf(assetIncrementData.getLastAssetId());
						if(lastAssetId.length() >= 4)
						{
							detailsOutputBean.setLastAssetId(assetIncrementData.getCommonAssetName() + "" 
									+ assetIncrementData.getLastAssetId());
						}else{
							String formatted = String.format("%04d", assetIncrementData.getLastAssetId());
							detailsOutputBean.setLastAssetId(assetIncrementData.getCommonAssetName() + "" + formatted);
						}
					}
				}
			}else{
				PurchaseMasAssetDetailsBO assetDetailBO = masAssetDetailsRepository.
						findByFkPurchaseRequestId(purchaseRequestId);
				if(assetDetailBO != null)
				{
					detailsOutputBean.setAvailableFromDate(assetDetailBO.getAssetAvailableFromDate());
					detailsOutputBean.setAvailableToDate(assetDetailBO.getAssetAvailableToDate());
					detailsOutputBean.setSoftwareLicenseOrSubscriptionId(assetDetailBO.
							getSoftwareLicenseOrSubscriptionId());
				}
			}
			
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(purchaseDatAssetRequestDetails.getCreatedDate());
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH) + 1;
			String pathForSuprDocSysAdmin = purchaseDocsBasePath + year + "/" + month + "/" 
					+ purchaseDatAssetRequestDetails.getPkPurchaseRequestId() + "/Supporting docs/SystemAdmin/";
			String pathForSuprDocEmp = purchaseDocsBasePath + year + "/" + month + "/" 
					+ purchaseDatAssetRequestDetails.getPkPurchaseRequestId() + "/Supporting docs/EmpOrRm/";
			String BASEPATHForPo = purchaseDocsBasePath + year + "/" + month + "/" 
					+ purchaseDatAssetRequestDetails.getPkPurchaseRequestId() + "/PO/";
			String BASEPATHForInvoice = purchaseDocsBasePath + year + "/" + month + "/" 
					+ purchaseDatAssetRequestDetails.getPkPurchaseRequestId() + "/Invoice/";
			String BASEPATHForCapex = purchaseDocsBasePath + year + "/" + month + "/" 
					+ purchaseDatAssetRequestDetails.getPkPurchaseRequestId() + "/Capex/";
				
			File fileSupportingDocSysAdmin = new File(pathForSuprDocSysAdmin);
			File fileSupportingDocEmp = new File(pathForSuprDocEmp);
			File fileForPo = new File(BASEPATHForPo);
			File fileForInvoice = new File(BASEPATHForInvoice);
			File fileForCapex = new File(BASEPATHForCapex);
			
			detailsOutputBean.setIsSupportingDocsAvailable(false);
			detailsOutputBean.setIsPoAvailable(false);
			detailsOutputBean.setIsInvoiceAvailable(false);
			detailsOutputBean.setIsCapexAvailable(false);
			
			File[] listOfSupportingDocSysAdmin = fileSupportingDocSysAdmin.listFiles();
			File[] listOfSupportingDocEmp = fileSupportingDocEmp.listFiles();
			File[] listOfPo = fileForPo.listFiles(); 
			File[] listOfInvoice = fileForInvoice.listFiles(); 
			File[] listOfCapex = fileForCapex.listFiles(); 
				
			if( fileSupportingDocEmp.isDirectory())
			{
				if(listOfSupportingDocEmp.length > 0)
				{
					detailsOutputBean.setIsSupportingDocsAvailable(true);
				}
			}
			
			if(fileSupportingDocSysAdmin.isDirectory())
			{
				if(listOfSupportingDocSysAdmin.length > 0)
				{
					detailsOutputBean.setIsSupportingDocsAvailable(true);
				}
			}

			if(fileForPo.isDirectory())
			{
				if(listOfPo.length > 0)
				{
					detailsOutputBean.setIsPoAvailable(true);
				}
			}
			
			if(fileForInvoice.isDirectory())
			{
				if(listOfInvoice.length > 0)
				{
					detailsOutputBean.setIsInvoiceAvailable(true);
				}
			}
			
			if(fileForCapex.isDirectory())
			{
				if(listOfCapex.length > 0)
				{
					detailsOutputBean.setIsCapexAvailable(true);
				}
			}
				
			//calling of legacy service
			ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean(); 
			clientProjCompDetailsBean.setClientId( purchaseDatAssetRequestDetails.getClientId());
			clientProjCompDetailsBean.setProjectId( purchaseDatAssetRequestDetails.getProjectId());
			clientProjCompDetailsBean.setComponentId(purchaseDatAssetRequestDetails.getProjectComponentId());
			clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);

			PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
			ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService.
			getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);

			if(clientProjectCompDetails == null){
				throw new CommonCustomException("Unable to fetch project, client and component details from legacy MIS.");
			}else{
				detailsOutputBean.setProjectName(clientProjectCompDetails.getProjectName());
			}
			// EOA of legacy service			
			
			detailsOutputBean.setPurpose(purchaseDatAssetRequestDetails.getBriefDescription());
			detailsOutputBean.setSoftwareTitleAndVersion(purchaseDatAssetRequestDetails.getPurchaseMasAssetsBO()
				.getAssetNameAndVersionOrModel());

			if (purchaseDatAssetQuoteBO !=null) 
			{
				PurchaseQuotesOutputBean quoteBean = new PurchaseQuotesOutputBean();
				quoteBean.setQuoteId(purchaseDatAssetQuoteBO.getPkQuoteId());
				quoteBean.setQuoteStatus(purchaseDatAssetQuoteBO.getQuoteStatus());
				quoteBean.setTotalCost(purchaseDatAssetQuoteBO.getTotalCost());
				quoteBean.setBuContribution(purchaseDatAssetQuoteBO.getBuContribution());
				quoteBean.setCostBorneBy(purchaseDatAssetQuoteBO.getCostBorneBy());
				quoteBean.setDeliveryDate(purchaseDatAssetQuoteBO.getDeliveryDate());
				quoteBean.setIndividualLicenseCost(purchaseDatAssetQuoteBO.getIndividualLicenseCost());
				quoteBean.setVendorName(purchaseDatAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorName());
				quoteBean.setVendorAddress(purchaseDatAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorAddress());
				quoteBean.setVendorContactNumber(purchaseDatAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorContactNo());
				quoteBean.setCurrencyType(purchaseDatAssetQuoteBO.getFkCurrencyType()); //Added by prathibha
				detailsOutputBean.setQuote(quoteBean);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new DataAccessException(e.getMessage());
		}
		
		return detailsOutputBean;
	}
	
	
	
	//Added by Shyam for conversion of amount into words
	
		public String convertDoubleIntoWords(Double doubleNumber)
		{
			String outputString ="";
			//Divide double number into two parts.
			// firstString will store number before decimal point
			// secondString will store number after decimal point
			
			DecimalFormat df= new DecimalFormat("#0.00");
			String firstString = df.format(doubleNumber.doubleValue()); 
			
			char[] strArr = firstString.toCharArray();
			int index = firstString.indexOf(".");	

			int itemIndex = 0;
			String firstNumber = "";
			for (char item : strArr) 
			{
				if(itemIndex == index)
					break;
				else 
					firstNumber = firstNumber + item;
				itemIndex++;
			}
			
			long firstLong = Long.parseLong(firstNumber);
			
			firstString = convert(firstLong);
			
			//For cent or paisa part
			LOG.info("index :: " + index);
			itemIndex = 1;
			String secondNumber = "";
			for (char item1 : strArr) 
			{
				if(itemIndex > index + 1)
					secondNumber = secondNumber + item1;
				itemIndex++;
			}
			
			if(!secondNumber.equalsIgnoreCase("00"))
			{
				long secondLong = Long.parseLong(secondNumber);
				secondNumber = convert(secondLong);
			}
			
			if(!secondNumber.equalsIgnoreCase("00")){
				outputString = firstString + " and " + secondNumber + " Hundredths Only";
			}else{
				outputString = firstString + " Only";
			}
	
			
			return outputString;
		}
		
		 private static final String[] specialNames = {
		        "",
		        " Thousand",
		        " Million",
		        " Billion",
		        " Trillion",
		        " Quadrillion",
		        " Quintillion"
		    };
		 
		 private static final String[] tensNames = {
		        "",
		        " Ten",
		        " Twenty",
		        " Thirty",
		        " Forty",
		        " Fifty",
		        " Sixty",
		        " Seventy",
		        " Eighty",
		        " Ninety"
		    };
		 
		 private static final String[] numNames = {
		        "",
		        " One",
		        " Two",
		        " Three",
		        " Four",
		        " Five",
		        " Six",
		        " Seven",
		        " Eight",
		        " Nine",
		        " Ten",
		        " Eleven",
		        " Twelve",
		        " Thirteen",
		        " Fourteen",
		        " Fifteen",
		        " Sixteen",
		        " Seventeen",
		        " Eighteen",
		        " Nineteen"
		    };
		 
		 private String convertLessThanOneThousand(int number) 
		 {
			 String current;
		        
		     if (number % 100 < 20){
		    	 current = numNames[number % 100];
		         number /= 100;
		     }else {
		         current = numNames[number % 10];
		         number /= 10;
		            
		         current = tensNames[number % 10] + current;
		         number /= 10;
		     }
		     if (number == 0) return current;
		        return numNames[number] + " Hundred" + current;
		 }
		    
		 public String convert(long number) {
			 if (number == 0) { return "zero"; }
		        
		     String prefix = "";
		        
		     if (number < 0) {
		    	 number = -number;
		         prefix = "negative";
		     }
		        
		     String current = "";
		     int place = 0;
		        
		     do {
		    	 int n = (int) (number % 1000);
		    	 if (n != 0){
		    		 String s = convertLessThanOneThousand(n);
		    		 current = s + specialNames[place] + current;
		    	 }
		    	 place++;
		    	 number /= 1000;
		     } while (number > 0);
		        
		     return (prefix + current).trim();
		 }
		//End of addition by Shyam 

	
			public Boolean downloadInvoice(Integer assetId,HttpServletResponse response) throws DataAccessException{
				
				LOG.entering("download invoice service method");
				Boolean isPoDownloaded = false;
				try{
					
					PurchaseDatAssetRequestBO assetReqBO = assetRequestRepository.findByPkPurchaseRequestId(assetId);
					
					Calendar calendar = new GregorianCalendar();
					calendar.setTime(assetReqBO.getCreatedDate());
					int year = calendar.get(Calendar.YEAR);
					int month = calendar.get(Calendar.MONTH) + 1;
					String BASEPATH = purchaseDocsBasePath+year+"/"+month+"/"+assetReqBO.getPkPurchaseRequestId()+"/Invoice/";
				
					File file = new File(BASEPATH);
					if(file.exists()){
						DownloadFileUtil.downloadAllFile(response, BASEPATH);
						isPoDownloaded = true;
					}else{
						throw new CommonCustomException("File does't exists in required server path ");
					}
					
				}catch(Exception e){
					throw new DataAccessException(e.getMessage());
				}
				
				LOG.exiting("download invoice service method");
				return isPoDownloaded;
				
			}
}
