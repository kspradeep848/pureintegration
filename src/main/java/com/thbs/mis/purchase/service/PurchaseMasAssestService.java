package com.thbs.mis.purchase.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.PurcahseAssetDetailsQoutesByRequestIdOutputBean;
import com.thbs.mis.purchase.bean.PurcahseQuotesOutPutBean;
import com.thbs.mis.purchase.bean.PurchaseFetchAssetsByAssetTypeAndLicenseTypeInputBean;
import com.thbs.mis.purchase.bean.PurchaseFetchAssetsByAssetTypeAndLicenseTypeOutputBean;
import com.thbs.mis.purchase.bean.PurchaseMasAssetDetailOutputBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetQuoteBO;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;
import com.thbs.mis.purchase.bo.PurchaseMasAssetDetailsBO;
import com.thbs.mis.purchase.bo.PurchaseMasAssetsBO;
import com.thbs.mis.purchase.controller.PurchaseMasSettingController;
import com.thbs.mis.purchase.dao.PurchaseAssetQuoteRepository;
import com.thbs.mis.purchase.dao.PurchaseAssetRequestRepository;
import com.thbs.mis.purchase.dao.PurchaseMasAssetDetailsRepository;
import com.thbs.mis.purchase.dao.PurchaseMasAssetsRepository;

@Service
public class PurchaseMasAssestService {
	
	private static final AppLog LOG = LogFactory
			.getLog(PurchaseMasSettingController.class);
	
	@Autowired
	PurchaseMasAssetsRepository purchaseMasAssestRepository;
	
	@Autowired
	PurchaseMasAssetDetailsRepository  purchaseMasAssetDetailsRepository;
	
	@Autowired
	PurchaseAssetQuoteRepository assetQuoteRepository; 
	
	@Autowired
	PurchaseAssetRequestRepository assetReqRepository;
	
	@Value("${hardware.license.type}")
	private String hardwareLicenseType;
	
	@Value("${software.license.type}")
	private String softwareLicenseType;
	
	
	//Added by Prathibha for Fetch assest by Asset Id 
	@Deprecated
	public PurchaseMasAssetDetailOutputBean getAssestsByAssetId(Integer assetId)throws CommonCustomException
	{
		PurchaseMasAssetDetailsBO  purchaseAssestBO=new PurchaseMasAssetDetailsBO();
		PurchaseMasAssetDetailOutputBean assetOutptbean=new PurchaseMasAssetDetailOutputBean();
		
		try
		{   LOG.startUsecase("PurcahseMasAsset Service try block");
			
			
			//purchaseAssestBO=purchaseMasAssetDetailsRepository.findByFkMasAssetsId(assetId);
				
			LOG.endUsecase("EXITING");
		}
		
		catch (Exception e)
		{    LOG.debug("------PurcahseMasAsset Service catch block-----");
			
			throw new CommonCustomException("Error occuredin in getMasAssestsByAssestId Service");
			
		}
		
	/*	if (purchaseAssestBO==null)
		{
			throw new CommonCustomException("Invalid Asset Id");
			
		}*/
		
		assetOutptbean.setPkAssetDetailId(purchaseAssestBO.getPkAssetDetailId());
		assetOutptbean.setAssetAvailableFromDate(purchaseAssestBO.getAssetAvailableFromDate());
		assetOutptbean.setAssetAvailableToDate(purchaseAssestBO.getAssetAvailableToDate());
		assetOutptbean.setSoftwareLicenseOrSubscriptionId(purchaseAssestBO.getSoftwareLicenseOrSubscriptionId());
		assetOutptbean.setFkMasAssetsId(purchaseAssestBO.getFkMasAssetsId());
		assetOutptbean.setAssetDetailModifiedBy(purchaseAssestBO.getModifiedBy());
		assetOutptbean.setAssetDetailModifiedDate(purchaseAssestBO.getModifiedDate());
		assetOutptbean.setAssetDetailCreatedBy(purchaseAssestBO.getCreatedBy());
		/*assetOutptbean.setCreatedDateDet(purchaseAssestBO.getAsstCreatedDate());*/
		assetOutptbean.setAssetDetailCreatedDate(purchaseAssestBO.getPurchaseMasAssetsBO().getCreatedDate());
		
		
		assetOutptbean.setPkAssetId(purchaseAssestBO.getPurchaseMasAssetsBO().getPkAssetId());
		assetOutptbean.setAssetNameAndVersionOrModel(purchaseAssestBO.getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());
		assetOutptbean.setAssetType(purchaseAssestBO.getPurchaseMasAssetsBO().getAssetType());
		assetOutptbean.setAssetCreatedDate(purchaseAssestBO.getCreatedDate());
		assetOutptbean.setCreatedBy(purchaseAssestBO.getPurchaseMasAssetsBO().getCreatedBy());
		assetOutptbean.setAssetModifiedBy(purchaseAssestBO.getPurchaseMasAssetsBO().getModifiedBy());
		assetOutptbean.setAssetModifiedDate(purchaseAssestBO.getPurchaseMasAssetsBO().getModifiedDate());
	

		return assetOutptbean;
	
	}
	
	 //EOA by Prathibha for Fetch assest by Asset Id 
	
	
	/*************************Fetch AssetandQuoteDetails by RequestId******************************************/
	/**
	 * <Descripton> This method is been implemented to Fetch Asset details along with quotes added to the required request  
	 * @param -AssetRequestId
	 * @returns PurcahseAssetDetailsQoutesByRequestIdOutputBean
	 * @throws CommonCustomException
	 *
	 */
	public  PurcahseAssetDetailsQoutesByRequestIdOutputBean getAssetandQuotesListByRequestId(Integer PurchaseReqId)throws CommonCustomException
	{   
		PurchaseDatAssetRequestBO assetReqBO=new PurchaseDatAssetRequestBO();
		PurcahseAssetDetailsQoutesByRequestIdOutputBean assetQuoteBean=new PurcahseAssetDetailsQoutesByRequestIdOutputBean();
		
		try{
			LOG.startUsecase("getAssetandQuotesListByRequestId Service-try");
			assetReqBO =assetReqRepository.findByPkPurchaseRequestId(PurchaseReqId);
		}
		catch (Exception e)
		{  LOG.endUsecase("Exception occured while fecthing asset details ");
		   throw new CommonCustomException("Invalid purchase request Id-Error occured while fetching asset Details",e);
		}
		
		if(assetReqBO!=null)
		{
			assetQuoteBean.setAssetRequestId(PurchaseReqId);
			assetQuoteBean.setAssetType(assetReqBO.getPurchaseDatRequestTypeBO().getPurchaseRequestTypeName());
			assetQuoteBean.setRquestTypeId(assetReqBO.getFkRequestTypeId());
			assetQuoteBean.setPriority(assetReqBO.getPriority());
			assetQuoteBean.setAssetNameAndVersionOrModel(assetReqBO.getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());
			assetQuoteBean.setNoOfUnits(assetReqBO.getNoOfUnits());
			assetQuoteBean.setAssetRequiredBy(assetReqBO.getAssetRequiredBy());
			assetQuoteBean.setDuration(assetReqBO.getDuration());
			assetQuoteBean.setAssetDescription(assetReqBO.getBriefDescription());
			assetQuoteBean.setIsClientReimbursing(assetReqBO.getIsClientReimbursing());
			assetQuoteBean.setProjectComponentId(assetReqBO.getProjectComponentId());
			assetQuoteBean.setAssetDetailCreatedBy(assetReqBO.getRequestorId());
			assetQuoteBean.setAssetDetailCreatedDate(assetReqBO.getCreatedDate());
			assetQuoteBean.setFkMasAssetsId(assetReqBO.getFkAssetId());
			
	
			List<PurchaseDatAssetQuoteBO>quotesList=new ArrayList<PurchaseDatAssetQuoteBO>();
			List<PurcahseQuotesOutPutBean> quotesList1=new ArrayList<PurcahseQuotesOutPutBean>();
			try
			{
				LOG.startUsecase("fetching quotes details for thos requestiD");
				quotesList = assetQuoteRepository.findByFkPurchaseRequestId(PurchaseReqId);
			}
			
			catch(Exception e)
			{
				LOG.endUsecase("Error occured while fetching quote details based on requestId");
				throw new CommonCustomException("Error Occured while retreving quotes",e);
				
			}
			
			for (PurchaseDatAssetQuoteBO assetQuoteBo : quotesList)
			{
				PurcahseQuotesOutPutBean quoteBean=new PurcahseQuotesOutPutBean();
				quoteBean.setPkQuoteId(assetQuoteBo.getPkQuoteId());
				quoteBean.setCostBorneBy(assetQuoteBo.getCostBorneBy());
				quoteBean.setBuContribution(assetQuoteBo.getBuContribution());
				quoteBean.setIndividualLicenseCost(assetQuoteBo.getIndividualLicenseCost());
				quoteBean.setFkSupplierAddress(assetQuoteBo.getFkSupplierAddress());
				quoteBean.setTotalCost(assetQuoteBo.getTotalCost());
				quoteBean.setDeliveryDate(assetQuoteBo.getDeliveryDate());
				quoteBean.setVendorName(assetQuoteBo.getPurchaseDatSupplierDetailBO().getVendorName());
				quoteBean.setVendorContactNo(assetQuoteBo.getPurchaseDatSupplierDetailBO().getVendorContactNo());
				quoteBean.setQuoteStatus(assetQuoteBo.getQuoteStatus());
				quoteBean.setCreatedBy(assetQuoteBo.getCreatedBy());
				quoteBean.setCreatedDate(assetQuoteBo.getCreatedDate());
				quoteBean.setFkPurchaseRequestId(assetQuoteBo.getFkPurchaseRequestId());
				quoteBean.setModifiedDate(assetQuoteBo.getModifiedDate());
				quoteBean.setModifiedBy(assetQuoteBo.getModifiedBy());
				quoteBean.setAssetDescriptionEnteredBySysadmin(assetQuoteBo.getAssetDescriptionEnteredBySysadmin());
				
				
				quotesList1.add(quoteBean);
			}
			  
			assetQuoteBean.setQutesList(quotesList1);
		}
		else
		{
			throw new CommonCustomException("Invalid PurcahseRequestId");
			
		}
		
		
		
	   return assetQuoteBean;
	}
	
	
	//Added by Smrithi
	public List<PurchaseFetchAssetsByAssetTypeAndLicenseTypeOutputBean> fetchAssets(
			PurchaseFetchAssetsByAssetTypeAndLicenseTypeInputBean purchaseAssetsInputBean) throws CommonCustomException {

		LOG.startUsecase("Fetch assets based on asset type and license type.");
		
		List<PurchaseFetchAssetsByAssetTypeAndLicenseTypeOutputBean> outputBeanList = new ArrayList<PurchaseFetchAssetsByAssetTypeAndLicenseTypeOutputBean>();
		List<PurchaseMasAssetsBO> fetchAssetsOutput = new ArrayList<PurchaseMasAssetsBO>();
		PurchaseMasAssetsBO masAssets = new PurchaseMasAssetsBO();
		
		if (purchaseAssetsInputBean.getAssetType().equalsIgnoreCase("HARDWARE")) {
			List<String> hardwareLicenceTypeList = Stream.of(hardwareLicenseType.split(","))
	                .collect(Collectors.toList());
			
			if(hardwareLicenceTypeList.contains(purchaseAssetsInputBean.getLicenseType().toUpperCase())){
			 masAssets.setLicenseType(purchaseAssetsInputBean.getLicenseType().toUpperCase());
			}else{
				throw new CommonCustomException("Data mismatch with asset type and license type. Please check again.");
			}
		}
		if (purchaseAssetsInputBean.getAssetType().equalsIgnoreCase("SOFTWARE")){
			List<String> softwareLicenceTypeList = Stream.of(softwareLicenseType.split(","))
	                .collect(Collectors.toList());
			if(softwareLicenceTypeList.contains(purchaseAssetsInputBean.getLicenseType().toUpperCase())){
			 masAssets.setLicenseType(purchaseAssetsInputBean.getLicenseType().toUpperCase());
			}else{
				throw new CommonCustomException("Data mismatch with asset type and license type. Please check again.");
			}
		}
		try{
			fetchAssetsOutput = purchaseMasAssestRepository.fetchAssetsBasedOnAssetTypeAndLicenseType(
					purchaseAssetsInputBean.getAssetType(),
					purchaseAssetsInputBean.getLicenseType());
		}catch(Exception ex){
			throw new CommonCustomException("Failed to fetch details from db.");
		} 
		
		/*if(fetchAssetsOutput.isEmpty())
		{
			throw new CommonCustomException(" No assets found for the given inputs.");
		}*/
		
		/*else{*/
			for (PurchaseMasAssetsBO purchaseMasAssetsBO : fetchAssetsOutput) {
				PurchaseFetchAssetsByAssetTypeAndLicenseTypeOutputBean outputBean = new PurchaseFetchAssetsByAssetTypeAndLicenseTypeOutputBean();
				outputBean.setPkAssetId(purchaseMasAssetsBO.getPkAssetId());
				outputBean.setAssetType(purchaseMasAssetsBO.getAssetType());
				outputBean.setAssetNameAndVersionOrModel(purchaseMasAssetsBO.getAssetNameAndVersionOrModel());
				outputBean.setIsAvailable(purchaseMasAssetsBO.getIsAvailable());
				outputBean.setLicenseType(purchaseMasAssetsBO.getLicenseType());
				
				outputBeanList.add(outputBean);
			}
		/*}*/
		
		LOG.endUsecase("Fetch assets based on asset type and license type.");
		return outputBeanList;
	}
	
	//EOA by Smrithi

	
	
}
