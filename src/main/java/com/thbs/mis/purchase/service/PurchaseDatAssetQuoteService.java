package com.thbs.mis.purchase.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.dao.BusinessUnitRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.PurchaseDatAssetQuotesInputBean;
import com.thbs.mis.purchase.bean.UpdateQuoteInputBean;
import com.thbs.mis.purchase.bean.UpdateQuoteOutputBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetQuoteBO;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;
import com.thbs.mis.purchase.bo.PurchaseDatBuBO;
import com.thbs.mis.purchase.dao.PurchaseAssetQuoteRepository;
import com.thbs.mis.purchase.dao.PurchaseAssetRequestRepository;
import com.thbs.mis.purchase.dao.PurchaseDatBuRepository;
//Added by Prathibha for Add and Update quotes
@Service
public class PurchaseDatAssetQuoteService {
	
	private static final AppLog LOG = LogFactory.getLog(PurchaseDatAssetQuoteService.class);
	
	@Autowired
	PurchaseAssetQuoteRepository purcahseAssetQuotesRepository;
	
	@Autowired
	EmpDetailRepository empDetailRepository;
	
	@Autowired
	PurchaseAssetRequestRepository assetReqRepo;
	
	@Value("${systemseniorAdminRoles}")
	private Integer seniorSystemAdmin;
	
	@Value ("${systemAdminRoles}")
	private Integer systemAdmin;
	
	@Value("${systemAdminProcurementRoleId}")
	private Integer systemAdminProcurment;
	
	@Value("${request.approvedByRm.status}")
	private Integer requestApprovedByRm;
	
	@Value("${purchase.capex.createdBySysAdmin}")
	private Integer CapexCreatedBySysAdmin;
	
	@Value("${purchase.quote.status.disagree}")
	private String purchaseQuoteStatusDisAgree;
	
	@Value("${request.capexApprovedByBuHead.status}")
	private Integer requestCapexApprovedByBuHeadStatus;
	
	@Value("${nbu.it.business.unit.id}")
	private short nbuITBusinessUnitId;
	
	@Value("${request.waitingRmApproval.status}")
	private int requestWaitingRmApprovalStatus;
	
	@Value("${purchase.quote.maximum.quantity}")
	private int maximumQuantityOfQuote;
	
	@Value("${purchase.add.asset.systemAdminRoles}")
	private String systemAdminRoles;
	
	@Value("${purchase.request.type.new.asset.request}")
	private int purchaseNewAssetRequest;

	@Autowired
	PurchaseDatBuRepository datBuRepo;
	
	@Autowired
	private BusinessUnitRepository businessUnitRepository;
	
	/*********************************Add Quotes**************************************************/
	
	/**
	 * <description>Method is used to add quotes by System Admin for perticular asset request based on Request Id
	 * @param (PurchaseDatAssetQuotesInputBean) quotesIpBean
	 * @return PurchaseDatAssetQuoteBO
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	
	
	public boolean addQuotes(PurchaseDatAssetQuotesInputBean quotesIpBean)
			throws CommonCustomException 
	{
		PurchaseDatAssetQuoteBO assetQuoteBO = new PurchaseDatAssetQuoteBO();
		//PurchaseDatAssetQuoteBO result = new PurchaseDatAssetQuoteBO();
		List<PurchaseDatAssetQuoteBO> uploadedquotsLst = new ArrayList<PurchaseDatAssetQuoteBO>();
		PurchaseDatAssetRequestBO assetReqBo = new PurchaseDatAssetRequestBO();
		//PurcahseQuotesOutPutBean quoteOuputBean = new PurcahseQuotesOutPutBean();
		// to check if quotes added for this request
		boolean quoteAdded = false;
		PurchaseDatAssetQuoteBO outputBO = new PurchaseDatAssetQuoteBO();
		try {
			LOG.startUsecase("Fetching qoutes details for requestId");
			uploadedquotsLst = purcahseAssetQuotesRepository
					.findByFkPurchaseRequestId(quotesIpBean.getFkPurchaseRequestId());
			// System.out.println("uploadedquotsLst"+uploadedquotsLst.size());
		} catch (Exception e) {
			LOG.endUsecase("Exception occured while retvieng ");
			throw new CommonCustomException("Exception Occured in addQuotes Service", e);
		}
		// only 3 request for one request
		if (uploadedquotsLst.size() >= 3) {
			LOG.startUsecase("Maximun 3 quotes can be uploaded");
			throw new CommonCustomException("Only 3 quotes for one asset request is allowed");
		} else {
			LOG.startUsecase("Fetching employee details");
			DatEmpDetailBO sysAdminEmpDetailBO = new DatEmpDetailBO();
			try {
				sysAdminEmpDetailBO = empDetailRepository.findByPkEmpId(quotesIpBean.getCreatedBy());
			} catch (Exception ex) {
				LOG.endUsecase("Exception in addQuotes Service while fetching details");
				throw new CommonCustomException("Unable to retrieve employee details. "
						+ "Kindly contact to Nuclues Supprt team for this issue.");
			}
			if (sysAdminEmpDetailBO == null) {
				throw new CommonCustomException("Unable to retrieve employee details. "
						+ "Kindly contact to Nuclues Supprt team for this issue.");
			} else {
				// reject access only for senior system admin/systemAdmin/sysAdminProcurement
				if (sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdminProcurment.intValue()
						|| sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdmin.intValue()
						|| sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == seniorSystemAdmin.intValue()) 
				{
					
					// quotes can be added only if the request is approved by RM
					assetReqBo = assetReqRepo.findByPkPurchaseRequestId(quotesIpBean.getFkPurchaseRequestId());
					if (assetReqBo != null) 
					{
						if (assetReqBo.getPurchaseRequestStatus().intValue() == requestApprovedByRm.intValue()) 
						{
							// If the cost is borne by IT
							if (quotesIpBean.getCostBorneBy().equalsIgnoreCase("IT")
									|| quotesIpBean.getCostBorneBy().equalsIgnoreCase("BU")) 
							{ // Total cost should be greater than equal to getIndividualLicenseCost
								if (quotesIpBean.getIndividualLicenseCost() > quotesIpBean.getTotalCost()) 
								{
									throw new CommonCustomException("Individual license cost cannot exceed total cost");
								} else {
									assetQuoteBO.setCostBorneBy(quotesIpBean.getCostBorneBy());
									assetQuoteBO.setBuContribution(quotesIpBean.getTotalCost());
								}
							}
							// if cost is shared BUcontribution is required
							else if (quotesIpBean.getCostBorneBy().equalsIgnoreCase("SHARED")) 
							{
								if (quotesIpBean.getBuContribution() >= quotesIpBean.getTotalCost()) 
								{
									throw new CommonCustomException("BU contribution cannot be equal to OR more than total cost");
								}  else if (quotesIpBean.getIndividualLicenseCost() > quotesIpBean.getTotalCost())
								{
									throw new CommonCustomException("Individual license coast cannot be more than total cost");
								} else {
									assetQuoteBO.setCostBorneBy(quotesIpBean.getCostBorneBy());
									if (quotesIpBean.getBuContribution() > 0 )
									{
									assetQuoteBO.setBuContribution(quotesIpBean.getBuContribution());
									}
									else
									{
										throw new CommonCustomException("Please enter bu Contribution");
									}
									
								}
							}
							
							
							// modifying quotes columns
							if (quotesIpBean.getIndividualLicenseCost() > 0 )
							{
							assetQuoteBO.setIndividualLicenseCost(Double.valueOf(quotesIpBean.getIndividualLicenseCost()));
							}
							else {
									throw new CommonCustomException("Individual license cost cannot be zero");
							     }
							assetQuoteBO.setTotalCost(Double.valueOf(quotesIpBean.getTotalCost()));
							//Code to check if same supplier quote exists
							if (!uploadedquotsLst.isEmpty())
							{
								for (PurchaseDatAssetQuoteBO previousQuote : uploadedquotsLst )
								{
									if (previousQuote.getFkSupplierAddress() != quotesIpBean.getFkSupplierAddress() )
									{
										assetQuoteBO.setFkSupplierAddress(quotesIpBean.getFkSupplierAddress());
									}
									else {
										   throw new CommonCustomException("This supplier quote already exists");
										 }
								}
								
							} else {
								assetQuoteBO.setFkSupplierAddress(quotesIpBean.getFkSupplierAddress());
							      }
							assetQuoteBO.setDeliveryDate(quotesIpBean.getDeliveryDate());
							assetQuoteBO.setCreatedBy(quotesIpBean.getCreatedBy());
							assetQuoteBO.setCreatedDate(new Date());
							if (quotesIpBean.getFkPurchaseRequestId() != null)
							{
							assetQuoteBO.setFkPurchaseRequestId(quotesIpBean.getFkPurchaseRequestId());
							}
							else {
								
							}
							if (quotesIpBean.getCurrencyType() != null && quotesIpBean.getCurrencyType()> 0)
							{
							assetQuoteBO.setFkCurrencyType(quotesIpBean.getCurrencyType());
							}
							else
							{
								throw new CommonCustomException("Please enter currencyType");
							}
							if(quotesIpBean.getVendorQuoteReferenceNo() != null)
								assetQuoteBO.setVendorQuoteReferenceNo(quotesIpBean.getVendorQuoteReferenceNo());
							
							//Adding of asset description/ specification entered by SysAdmin
							//Added by Shyam for Change Request by Manohar
							if(quotesIpBean.getAssetDescriptionEnteredBySysadmin().isEmpty()){
								throw new CommonCustomException("Please enter some asset description/ specification");
							}else{
								assetQuoteBO.setAssetDescriptionEnteredBySysadmin(quotesIpBean.
										getAssetDescriptionEnteredBySysadmin());
							}
							
							if (!quotesIpBean.getIsSetDefault()) 
							{
								assetQuoteBO.setQuoteStatus("DISAGREE");
								outputBO = purcahseAssetQuotesRepository.save(assetQuoteBO);
							}else {
								assetQuoteBO.setQuoteStatus("AGREE");
								outputBO = purcahseAssetQuotesRepository.save(assetQuoteBO);
								
								if (!uploadedquotsLst.isEmpty()) 
								{
									for (PurchaseDatAssetQuoteBO quoteBo : uploadedquotsLst)
									{  
										if (quoteBo.getQuoteStatus().equalsIgnoreCase("AGREE")) 
										{
											quoteBo.setQuoteStatus("DISAGREE");
											quoteBo.setModifiedBy(quotesIpBean.getCreatedBy());
											quoteBo.setModifiedDate(new Date());
											outputBO = purcahseAssetQuotesRepository.save(quoteBo);
											
											
	
										}
									}	
								}
							} 
							
						}
						
						else {throw new CommonCustomException("Cannot add quote for this request as request asset status is not in manager approved status");
						}
					} else {
						throw new CommonCustomException("Invalid RequestId");
					}
  				} else {
					throw new CommonCustomException("You are not authorized to add the quote");
				}
				
					
				
			}
		}
		if (outputBO != null)
		{
			quoteAdded = true;
		}
		else
		{
			throw new CommonCustomException("Could not add quote for this request");
		}
		//return result;
		return quoteAdded;
	}
	
	
	 /**************************************update Quotes based on Quote-Id********************************************/
	 
	 /**
	  * <Description> Method called by System admin login to update the added quotes based on Quote-Id
	  * @param (UpdateQuoteInputBean) quoteBean (quoteId)
	  * @return PurchaseDatAssetQuoteBO
	  * @throws DataAccessException
	  * @throws CommonCustomException
	  */
	 //
	 
	
	public  UpdateQuoteOutputBean updateQuotes (UpdateQuoteInputBean quoteBean ) 
			throws CommonCustomException
	{
		PurchaseDatAssetQuoteBO prevQuoteBO = null;
		PurchaseDatAssetQuoteBO updatedQuoteBO = new PurchaseDatAssetQuoteBO();
		boolean atLeastOneQuoteShouldBeDefaultFlag = false;
		List<PurchaseDatAssetQuoteBO> quoteList = new ArrayList<PurchaseDatAssetQuoteBO>();
		PurchaseDatAssetQuoteBO updatedQuoteResult = new PurchaseDatAssetQuoteBO();
		//double defaultBUcontibution = 100;
		UpdateQuoteOutputBean outputBean = new UpdateQuoteOutputBean();
		LOG.startUsecase("Update quotes Service"+ quoteBean);
		try{
			updatedQuoteBO = purcahseAssetQuotesRepository.findByPkQuoteId(quoteBean.getPkQuoteId());
		}
		catch (Exception e)	{
			//LOG.endUsecase("Exception occured in updateQuotes Service");
			throw new CommonCustomException("Exception occured while fetching quote");
		}
		
		if (updatedQuoteBO == null)	{
			throw new CommonCustomException("Invalid Quote Id");
		}
		
		DatEmpDetailBO sysAdminEmpDetailBO = new DatEmpDetailBO();
		try{
			sysAdminEmpDetailBO = empDetailRepository.findByPkEmpId(quoteBean.getModifiedBy());
		}
		catch(Exception ex){
			throw new CommonCustomException("Unable to retrieve employee details. "
					+ "Kindly contact to Nuclues Supprt team for this issue.");
		}
			
		if(sysAdminEmpDetailBO == null)
		{
			throw new CommonCustomException("Unable to retrieve employee details. "
					+ "Kindly contact to Nuclues Supprt team for this issue.");
		}else{
			if(sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdminProcurment.intValue()||
				sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdmin.intValue() ||
				sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == seniorSystemAdmin.intValue())
				
			 {    
				if (updatedQuoteBO.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() 
					  == requestCapexApprovedByBuHeadStatus )  
				{ 
				  throw new CommonCustomException("This quote is approved by BU Head- Cannot update approved quote  ");
				}
				
				 if (quoteBean.getBuContribution() > quoteBean.getTotalCost() )
				 {
					throw new CommonCustomException("BU Contribution cannot "
							+ "be more than Total cost"); 
				}
				else if (quoteBean.getIndividualLicenseCost() > quoteBean.getTotalCost())
				{ 
					throw new CommonCustomException("Individual license cost cannot be more than toltal cost");   
				}
				
				try{
					quoteList = purcahseAssetQuotesRepository.findByFkPurchaseRequestId(updatedQuoteBO.getFkPurchaseRequestId());
				}catch(Exception ex){
					throw new CommonCustomException("Failed to get all quotes under this asset request.");
				}
				
				if(quoteList.isEmpty()){
					throw new CommonCustomException("No quotes found.");
				}
				
				if ( updatedQuoteBO.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() 
						== requestApprovedByRm.intValue()  ) //|| updatedQuoteBO.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() == CapexCreatedBySysAdmin.intValue())           
				{  
					for (PurchaseDatAssetQuoteBO assetQuoteBO : quoteList) 
					{   
						if(assetQuoteBO.getQuoteStatus().equalsIgnoreCase("AGREE")) //checking for any agreed quote
						{
							prevQuoteBO = assetQuoteBO;
							break;
						}	
					}
					LOG.info("quoteList :: " + quoteList);
					LOG.info("prevQuoteBO :: " + prevQuoteBO);
					LOG.info("updatedQuoteBO :: " + updatedQuoteBO);
					//There is no default quote for this asset request
					//if(prevQuoteBO == null)
					//if (previousAndUpdateQuotesAreSameFlag)
					//{
					//Setting of costBorneBy
					if(!quoteBean.getCostBorneBy().equalsIgnoreCase(updatedQuoteBO.getCostBorneBy()))
					{
						updatedQuoteBO.setCostBorneBy(quoteBean.getCostBorneBy());
					}
						
					//Setting of BUContribution
					if(quoteBean.getCostBorneBy().equalsIgnoreCase("SHARED"))
					{
						if(updatedQuoteBO.getCostBorneBy().equalsIgnoreCase("SHARED"))
						{
							if(quoteBean.getBuContribution() != updatedQuoteBO.getBuContribution().doubleValue())
							{
								updatedQuoteBO.setBuContribution(Double.valueOf(quoteBean.getBuContribution()));
							}
						}else{
							updatedQuoteBO.setBuContribution(Double.valueOf(quoteBean.getBuContribution()));
						}
					}else{
						updatedQuoteBO.setBuContribution(Double.valueOf(quoteBean.getTotalCost()));
						
					}
						
					//Setting of individualLicenseCost
					if(quoteBean.getIndividualLicenseCost() != updatedQuoteBO.getIndividualLicenseCost().doubleValue())
					{
						updatedQuoteBO.setIndividualLicenseCost(Double.valueOf(quoteBean.getIndividualLicenseCost()));
					}
						
					//Setting of totalCost
					if(quoteBean.getTotalCost() != updatedQuoteBO.getTotalCost().doubleValue()){
						updatedQuoteBO.setTotalCost(Double.valueOf(quoteBean.getTotalCost()));
					}
						
					//Setting of SupplierAddress id 
					if (!quoteList.isEmpty())
					{   //validations for duplicate quotes based on supplier id
						for (PurchaseDatAssetQuoteBO previousQuotes : quoteList )
						{
							if (previousQuotes.getPkQuoteId().intValue() == quoteBean.getPkQuoteId().intValue()  &&  
									previousQuotes.getFkSupplierAddress().intValue()== quoteBean.getFkSupplierAddress().intValue() )
							{
								updatedQuoteBO.setFkSupplierAddress(quoteBean.getFkSupplierAddress());
							}
							else if (previousQuotes.getFkSupplierAddress().intValue() != quoteBean.getFkSupplierAddress().intValue() )
							{
								updatedQuoteBO.setFkSupplierAddress(quoteBean.getFkSupplierAddress());
							}
							else 
							{
								throw new CommonCustomException("Quote of this supplier already exists");
							}
						}
						
					} //EO validation for one supplier one quote for a request
					
					
					/*if(quoteBean.getFkSupplierAddress().intValue() != updatedQuoteBO.getFkSupplierAddress().intValue())
					{
						updatedQuoteBO.setFkSupplierAddress(quoteBean.getFkSupplierAddress());
					}*/
					
					//Setting Currency_Type
					if(quoteBean.getCurrencyType() != null)
					{
						updatedQuoteBO.setFkCurrencyType(quoteBean.getCurrencyType());
					}
					else
					{
						throw new CommonCustomException("Currency type of this quote not found.");
					}
					
					//Setting of ModifiedBy
					if(updatedQuoteBO.getModifiedBy() != null)
					{
						if(quoteBean.getModifiedBy().intValue() != updatedQuoteBO.getModifiedBy().intValue())
						{
							updatedQuoteBO.setModifiedBy(quoteBean.getModifiedBy());
						}
					}
					else{
						updatedQuoteBO.setModifiedBy(quoteBean.getModifiedBy());
					}
					//setting vendorQuoteRefNo
					if(quoteBean.getVendorQuoteReferenceNo() != null)
					{
						updatedQuoteBO.setVendorQuoteReferenceNo(quoteBean.getVendorQuoteReferenceNo());
					}
					
					//Added by Shyam for new Change Request by Manohar
					if(quoteBean.getAssetDescriptionEnteredBySysadmin().isEmpty())
					{
						throw new CommonCustomException("Please enter some asset description / specification");
					}else{
						updatedQuoteBO.setAssetDescriptionEnteredBySysadmin(quoteBean.
								getAssetDescriptionEnteredBySysadmin());
					}
					//EOA
					
					//Added for make agree quote to disagree (only when the current quote is agreed - added by anu)
					if(prevQuoteBO != null && quoteBean.getFkQuoteStatus().equalsIgnoreCase("AGREE"))
					{
						prevQuoteBO.setQuoteStatus("DISAGREE");
						prevQuoteBO.setModifiedDate(new Date());
						prevQuoteBO.setModifiedBy(quoteBean.getModifiedBy());
						purcahseAssetQuotesRepository.save(prevQuoteBO);
					}
						
					if (quoteBean.getFkQuoteStatus() == null || quoteBean.getFkQuoteStatus().equalsIgnoreCase("DISAGREE")) 
					{
						updatedQuoteBO.setQuoteStatus("DISAGREE");
						updatedQuoteBO.setModifiedDate(new Date());
						updatedQuoteBO.setDeliveryDate(quoteBean.getDeliveryDate());
						updatedQuoteBO = purcahseAssetQuotesRepository.save(updatedQuoteBO);
						
					} else {
						updatedQuoteBO.setQuoteStatus("AGREE");
						updatedQuoteBO.setModifiedDate(new Date());
						updatedQuoteBO.setDeliveryDate(quoteBean.getDeliveryDate());
						updatedQuoteResult = purcahseAssetQuotesRepository.save(updatedQuoteBO);
						
						outputBean.setPkQuoteId(updatedQuoteResult.getPkQuoteId());
							
						if(updatedQuoteBO.getBuContribution() != null)
						{
							updatedQuoteResult.setBuContribution(updatedQuoteResult.getBuContribution());
						}
						else
						{
							throw new CommonCustomException("Please enter BU Contribution");
						}
						
						outputBean.setCostBorneBy(updatedQuoteResult.getCostBorneBy());
						outputBean.setDeliveryDate(updatedQuoteResult.getDeliveryDate());
						outputBean.setFkSupplierAddress(updatedQuoteResult.getFkSupplierAddress());
						outputBean.setIndividualLicenseCost(updatedQuoteResult.getIndividualLicenseCost().doubleValue());
						outputBean.setModifiedBy(updatedQuoteResult.getModifiedBy());
						outputBean.setQuoteStatus(updatedQuoteResult.getQuoteStatus());
						outputBean.setCurrencyType(updatedQuoteResult.getFkCurrencyType());
						if(updatedQuoteResult.getVendorQuoteReferenceNo() != null)
							outputBean.setVendorQuoteReferenceNo(updatedQuoteResult.getVendorQuoteReferenceNo());
					
						//------------------------------------------------------------------------
					}
				} else if(updatedQuoteBO.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() 
							== CapexCreatedBySysAdmin.intValue())
				{
					for (PurchaseDatAssetQuoteBO purchaseDatAssetQuoteBO : quoteList) 
					{
					   if(purchaseDatAssetQuoteBO.getQuoteStatus().equalsIgnoreCase("AGREE"))
					   {
							atLeastOneQuoteShouldBeDefaultFlag = true;
							prevQuoteBO = purchaseDatAssetQuoteBO;
							break;
						}
					}
					
					if(!atLeastOneQuoteShouldBeDefaultFlag)
						throw new CommonCustomException("There is no default quote for this request.");
					
					
					//Validation for purchase_status in dat_bu_table
					List<PurchaseDatBuBO> datBUboList = new ArrayList<PurchaseDatBuBO>();
					try {	
						
						datBUboList = datBuRepo.findByFkPurchaseQuoteId(prevQuoteBO.getPkQuoteId());
						}
					catch(Exception e)
					{
						throw new CommonCustomException("Error occured wile fetching purchase status from dat bu table for this request");
					}
					
					if (datBUboList.isEmpty())
					{
						throw new CommonCustomException("Capex not yet created for this quote");
					}
					
					else 
					{   //checking the purchase status for quote to be updated in dat_bu table
						for ( PurchaseDatBuBO buBO  : datBUboList)
						{   
							  //-------------------
							if(buBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("AGREE"))
							{
								if(prevQuoteBO.getPurchaseDatAssetRequestBO().getFkBuId() == buBO.getFkMasBuId()) 
									throw new CommonCustomException("This request already has been approved by BU Head.");
								else
									throw new CommonCustomException("This request already has been approved by IT Head.Hence canot be updated");
							}else if(buBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
							{
								if(prevQuoteBO.getPurchaseDatAssetRequestBO().getFkBuId() == buBO.getFkMasBuId()) 
									throw new CommonCustomException("This request already has been rejected by BU Head.");
								/*else
									throw new CommonCustomException("This request already has been approved by IT Head.");*/
								
							}	
						}	
							
						 //Set the value in 	updatedQuoteBO
						if(Double.valueOf(quoteBean.getBuContribution()) != null)
							updatedQuoteBO.setBuContribution(Double.valueOf(quoteBean.getBuContribution()));	
						updatedQuoteBO.setCostBorneBy(quoteBean.getCostBorneBy());	
						updatedQuoteBO.setDeliveryDate(quoteBean.getDeliveryDate());
						updatedQuoteBO.setTotalCost(Double.valueOf(quoteBean.getTotalCost()));
						
						if (!quoteList.isEmpty())
						{
							for (PurchaseDatAssetQuoteBO previousQuotes : quoteList )
							{
								if (previousQuotes.getPkQuoteId().equals(quoteBean.getPkQuoteId())  &&  previousQuotes.getFkSupplierAddress().intValue()== quoteBean.getFkSupplierAddress().intValue() )
								{
									updatedQuoteBO.setFkSupplierAddress(quoteBean.getFkSupplierAddress());
								}
								else if (previousQuotes.getFkSupplierAddress().intValue() != quoteBean.getFkSupplierAddress().intValue() )
								{
									updatedQuoteBO.setFkSupplierAddress(quoteBean.getFkSupplierAddress());
								}
								else 
								{
									throw new CommonCustomException("Quote of this supplier already exists");
								}
							}
							
						}
						//updatedQuoteBO.setFkSupplierAddress(quoteBean.getFkSupplierAddress());
						updatedQuoteBO.setIndividualLicenseCost(Double.valueOf(quoteBean.getIndividualLicenseCost()));	
						updatedQuoteBO.setModifiedBy(quoteBean.getModifiedBy());
						updatedQuoteBO.setModifiedDate(new Date());
						updatedQuoteBO.setFkCurrencyType(quoteBean.getCurrencyType());
						if(quoteBean.getVendorQuoteReferenceNo() != null)
							updatedQuoteBO.setVendorQuoteReferenceNo(quoteBean.getVendorQuoteReferenceNo());	
							
						updatedQuoteResult = purcahseAssetQuotesRepository.save(updatedQuoteBO);	
					
						outputBean.setPkQuoteId(updatedQuoteResult.getPkQuoteId());
						
						if(updatedQuoteResult.getBuContribution() != null)
						{
							outputBean.setBuContribution(updatedQuoteResult.getBuContribution());
						}
						
						outputBean.setCostBorneBy(updatedQuoteResult.getCostBorneBy());
						outputBean.setDeliveryDate(updatedQuoteResult.getDeliveryDate());
						outputBean.setFkSupplierAddress(updatedQuoteResult.getFkSupplierAddress());
						outputBean.setIndividualLicenseCost(updatedQuoteResult.getIndividualLicenseCost().doubleValue());
						outputBean.setModifiedBy(updatedQuoteResult.getModifiedBy());
						outputBean.setQuoteStatus(updatedQuoteResult.getQuoteStatus());
						outputBean.setCurrencyType(updatedQuoteResult.getFkCurrencyType());
						if(updatedQuoteResult.getVendorQuoteReferenceNo() != null)
							outputBean.setVendorQuoteReferenceNo(updatedQuoteResult.getVendorQuoteReferenceNo());
					}
				}else{
					throw new CommonCustomException("Request status neither be 'Approved By RM' nor 'Capex Created By System Admin'.");
				}
			}//sysadmin authorization
			else{ 
				throw new CommonCustomException("You are not authorized to update this quote");
			}
		}
		
		return outputBean;
	}
	 
	//EOA by Prathibha for Add and Update quotes
	
	
	//Added by Shyam for phase-2 update quote
	/**
	 * <description>Method is used to update quote by System Admin for particular asset request based on Request Id
	 * @param (UpdateQuoteInputBean) updateQuoteInputBean
	 * @return boolean
	 * @throws CommonCustomException
	 */
	@SuppressWarnings("unused")
	public boolean updateQuotesBeforeRMApproval(UpdateQuoteInputBean updateQuoteInputBean)
			throws CommonCustomException 
	{
		boolean updateQuoteSuccessFlag = false;
		boolean updatingDefaultQuoteFlag = false;
		DatEmpDetailBO sysAdminEmpDetailBO = new DatEmpDetailBO();

		PurchaseDatAssetQuoteBO prevQuoteBO = null;
		PurchaseDatAssetQuoteBO agreedQuoteBO = null;
		PurchaseDatAssetQuoteBO updatedQuoteBO = new PurchaseDatAssetQuoteBO();
		PurchaseDatAssetQuoteBO updatedQuoteResult = new PurchaseDatAssetQuoteBO();
		List<PurchaseDatAssetQuoteBO> quoteList = new ArrayList<PurchaseDatAssetQuoteBO>();

		//Collect all role ids of System Admin.
		List<Short> systemAdminRoleIdList = new ArrayList<Short>();
		String[] empIdArray = systemAdminRoles.split(",");
		for (String s : empIdArray)
			systemAdminRoleIdList.add(Short.parseShort(s));

		DatEmpDetailBO systemAdminEmpDetailBo = new DatEmpDetailBO();
	
		try{
			systemAdminEmpDetailBo = empDetailRepository.getSystemAdminRoleDetailsForAddAsset(
					updateQuoteInputBean.getModifiedBy(),systemAdminRoleIdList);
			updatedQuoteBO = purcahseAssetQuotesRepository.findByPkQuoteId(updateQuoteInputBean.getPkQuoteId());
		}
		catch (Exception ex) {
			throw new CommonCustomException("Exception occured while fetching quote");
		}

		if (updatedQuoteBO == null) {
			throw new CommonCustomException("Quote does not exists.");
		}

		if(systemAdminEmpDetailBo == null)
		{
			throw new CommonCustomException("The requestor is not having role of system admin");
		}
	
		try{
			sysAdminEmpDetailBO = empDetailRepository.findByPkEmpId(updateQuoteInputBean.
					getModifiedBy());
		}
		catch(Exception ex){
			throw new CommonCustomException("Unable to retrieve employee details. "
					+ "Kindly contact to Nuclues Supprt team for this issue.");
		}

		if(sysAdminEmpDetailBO == null)
		{
			throw new CommonCustomException("System admin details information not found");
		}

		if (updatedQuoteBO.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() 
				!= requestWaitingRmApprovalStatus) 
		{ 
			throw new CommonCustomException("Asset request can be updated only when it is in waiting for RM approval status.");
		}

		if (updateQuoteInputBean.getBuContribution() > updateQuoteInputBean.getTotalCost() )
		{
			throw new CommonCustomException("BU Contribution cannot be more than Total cost"); 
		}
		else if (updateQuoteInputBean.getIndividualLicenseCost() > updateQuoteInputBean.getTotalCost())
		{ 
			throw new CommonCustomException("Individual license cost cannot be more than total cost"); 
		}

		try{
			quoteList = purcahseAssetQuotesRepository.findByFkPurchaseRequestId(updatedQuoteBO.getFkPurchaseRequestId());
		}catch(Exception ex){
			throw new CommonCustomException("Failed to get all quotes under this asset request.");
		}
	
		if(quoteList.isEmpty()){
			throw new CommonCustomException("No quotes found.");
		}
		
		boolean agreedQuote = false;  
		for (PurchaseDatAssetQuoteBO assetQuoteBO : quoteList) 
		{   
			if(assetQuoteBO.getQuoteStatus().equalsIgnoreCase("AGREE")) //checking for any agreed quote
			{
				prevQuoteBO = assetQuoteBO;
				agreedQuote = true;
				break;
			}	
		}
		LOG.info("quoteList :: " + quoteList);
		LOG.info("prevQuoteBO :: " + prevQuoteBO);
		LOG.info("updatedQuoteBO :: " + updatedQuoteBO);
		//There is no default quote for this asset request
		//if(prevQuoteBO == null)
		//if (previousAndUpdateQuotesAreSameFlag)
		//{
		//Setting of costBorneBy
		if(!updateQuoteInputBean.getCostBorneBy().equalsIgnoreCase(updatedQuoteBO.getCostBorneBy()))
		{
			updatedQuoteBO.setCostBorneBy(updateQuoteInputBean.getCostBorneBy());
		}
				
		//Setting of BUContribution
		if(updateQuoteInputBean.getCostBorneBy().equalsIgnoreCase("SHARED"))
		{
			if(updatedQuoteBO.getCostBorneBy().equalsIgnoreCase("SHARED"))
			{
				if(updateQuoteInputBean.getBuContribution() != updatedQuoteBO.getBuContribution().
						doubleValue())
				{
					updatedQuoteBO.setBuContribution(Double.valueOf(updateQuoteInputBean.
							getBuContribution()));
				}
			}else{
				updatedQuoteBO.setBuContribution(Double.valueOf(updateQuoteInputBean.getBuContribution()));
			}
		}else{
			updatedQuoteBO.setBuContribution(Double.valueOf(updateQuoteInputBean.getTotalCost()));
		}
				
		//Setting of individualLicenseCost
		if(updateQuoteInputBean.getIndividualLicenseCost() != updatedQuoteBO.
				getIndividualLicenseCost().doubleValue())
		{
			updatedQuoteBO.setIndividualLicenseCost(Double.valueOf(updateQuoteInputBean.
					getIndividualLicenseCost()));
		}
				
		//Setting of totalCost
		if(updateQuoteInputBean.getTotalCost() != updatedQuoteBO.getTotalCost().doubleValue())
		{
			updatedQuoteBO.setTotalCost(Double.valueOf(updateQuoteInputBean.getTotalCost()));
		}
				
		//Setting of SupplierAddress id 
		if (!quoteList.isEmpty())
		{   //validations for duplicate quotes based on supplier id
			for (PurchaseDatAssetQuoteBO previousQuotes : quoteList )
			{
				if (previousQuotes.getPkQuoteId().equals(updateQuoteInputBean.getPkQuoteId())  &&  previousQuotes.getFkSupplierAddress().intValue()== updateQuoteInputBean.getFkSupplierAddress().intValue() )
				{
					updatedQuoteBO.setFkSupplierAddress(updateQuoteInputBean.getFkSupplierAddress());
				}
				else if (previousQuotes.getFkSupplierAddress().intValue() != 
						updateQuoteInputBean.getFkSupplierAddress().intValue() )
				{
					updatedQuoteBO.setFkSupplierAddress(updateQuoteInputBean.getFkSupplierAddress());
				}
				else {
					throw new CommonCustomException("Quote of this supplier already exists");
				}
			}
				
		} //EO validation for one supplier one quote for a request
			
		//Setting Currency_Type
		if(updateQuoteInputBean.getCurrencyType() != null)
		{
			updatedQuoteBO.setFkCurrencyType(updateQuoteInputBean.getCurrencyType());
		}
		else{
			throw new CommonCustomException("Currency type of this quote not found.");
		}
			
		//Setting of ModifiedBy
		if(updatedQuoteBO.getModifiedBy() != null)
		{
			if(updateQuoteInputBean.getModifiedBy().intValue() != updatedQuoteBO.getModifiedBy().intValue())
			{
				updatedQuoteBO.setModifiedBy(updateQuoteInputBean.getModifiedBy());
			}
		}else{
			updatedQuoteBO.setModifiedBy(updateQuoteInputBean.getModifiedBy());
		}
		
		//setting vendorQuoteRefNo
		if(updateQuoteInputBean.getVendorQuoteReferenceNo() != null)
		{
			updatedQuoteBO.setVendorQuoteReferenceNo(updateQuoteInputBean.
					getVendorQuoteReferenceNo());
		}
		
		if(!updateQuoteInputBean.getAssetDescriptionEnteredBySysadmin().isEmpty()){
			updatedQuoteBO.setAssetDescriptionEnteredBySysadmin(updateQuoteInputBean.
					getAssetDescriptionEnteredBySysadmin());
		}else{
			throw new CommonCustomException("Please enter asset descritpion / specification");
		}
		
		//if updating agreed quote
		if (agreedQuote)
		{
			if (prevQuoteBO.getPkQuoteId().equals(updateQuoteInputBean.getPkQuoteId()))
			{
				if (updateQuoteInputBean.getFkQuoteStatus().equalsIgnoreCase("DISAGREE"))
				{
					throw new CommonCustomException("Select any one quote as default before updating the status of this quote");
				}
			
				updatedQuoteBO.setQuoteStatus("AGREE");
				updatedQuoteBO.setModifiedDate(new Date());
				updatedQuoteBO.setDeliveryDate(updateQuoteInputBean.getDeliveryDate());
				
				if (!quoteList.isEmpty()) 
				{
					for (PurchaseDatAssetQuoteBO quoteBo : quoteList)
					{  
						if (quoteBo.getQuoteStatus().equalsIgnoreCase("AGREE")) 
						{
							quoteBo.setQuoteStatus("DISAGREE");
							quoteBo.setModifiedBy(updateQuoteInputBean.getModifiedBy());
							quoteBo.setModifiedDate(new Date());
							updatedQuoteResult = purcahseAssetQuotesRepository.save(quoteBo);
						}
					}	
				} //end of making agreed previous quote to disagree
			
				updatedQuoteResult = purcahseAssetQuotesRepository.save(updatedQuoteBO);
				if(updatedQuoteBO.getBuContribution() != null)
				{
					updatedQuoteResult.setBuContribution(updatedQuoteResult.getBuContribution());
				}
				else{
					throw new CommonCustomException("Please enter BU Contribution");
				}
					
			} //end of agreed quote check
			
			if (updateQuoteInputBean.getFkQuoteStatus() == null || 
					updateQuoteInputBean.getFkQuoteStatus().equalsIgnoreCase("DISAGREE")) 
			{
				updatedQuoteBO.setQuoteStatus("DISAGREE");
				updatedQuoteBO.setModifiedDate(new Date());
				updatedQuoteBO.setDeliveryDate(updateQuoteInputBean.getDeliveryDate());
				//updatedQuoteBO = purcahseAssetQuotesRepository.save(updatedQuoteBO);
			} else {
				updatedQuoteBO.setQuoteStatus("AGREE");
				updatedQuoteBO.setModifiedDate(new Date());
				updatedQuoteBO.setDeliveryDate(updateQuoteInputBean.getDeliveryDate());
				//updatedQuoteResult = purcahseAssetQuotesRepository.save(updatedQuoteBO);
				
				updateQuoteInputBean.setPkQuoteId(updatedQuoteResult.getPkQuoteId());
					
				if(updatedQuoteBO.getBuContribution() != null)
				{
					updatedQuoteResult.setBuContribution(updatedQuoteResult.getBuContribution());
				}
				else {
					throw new CommonCustomException("Please enter BU Contribution");
				}
				
				if (!quoteList.isEmpty()) 
				{
					for (PurchaseDatAssetQuoteBO quoteBo : quoteList)
					{  
						if (quoteBo.getQuoteStatus().equalsIgnoreCase("AGREE")) 
						{
							quoteBo.setQuoteStatus("DISAGREE");
							quoteBo.setModifiedBy(updateQuoteInputBean.getModifiedBy());
							quoteBo.setModifiedDate(new Date());
							updatedQuoteResult = purcahseAssetQuotesRepository.save(quoteBo);
						}
					}	
				} //end of making agreed previous quote to disagree
			}
				
				
			updatedQuoteResult = purcahseAssetQuotesRepository.save(updatedQuoteBO);
		}
		else{
			throw new CommonCustomException("there is no default quote");
		}
			
				/*if (updateQuoteInputBean.getFkQuoteStatus() == null || updateQuoteInputBean.getFkQuoteStatus().equalsIgnoreCase("DISAGREE")) 
				{
					updatedQuoteBO.setQuoteStatus("DISAGREE");
					updatedQuoteBO.setModifiedDate(new Date());
					updatedQuoteBO.setDeliveryDate(updateQuoteInputBean.getDeliveryDate());
					//updatedQuoteBO = purcahseAssetQuotesRepository.save(updatedQuoteBO);
					
				} else {
					updatedQuoteBO.setQuoteStatus("AGREE");
					updatedQuoteBO.setModifiedDate(new Date());
					updatedQuoteBO.setDeliveryDate(updateQuoteInputBean.getDeliveryDate());
					//updatedQuoteResult = purcahseAssetQuotesRepository.save(updatedQuoteBO);
					
					updateQuoteInputBean.setPkQuoteId(updatedQuoteResult.getPkQuoteId());
						
					if(updatedQuoteBO.getBuContribution() != null)
					{
						updatedQuoteResult.setBuContribution(updatedQuoteResult.getBuContribution());
					}
					else
					{
						throw new CommonCustomException("Please enter BU Contribution");
					}
				
			       }
				if (!quoteList.isEmpty()) 
				{
					for (PurchaseDatAssetQuoteBO quoteBo : quoteList)
					{  
						if (quoteBo.getQuoteStatus().equalsIgnoreCase("AGREE")) 
						{
							quoteBo.setQuoteStatus("DISAGREE");
							quoteBo.setModifiedBy(updateQuoteInputBean.getModifiedBy());
							quoteBo.setModifiedDate(new Date());
							updatedQuoteResult = purcahseAssetQuotesRepository.save(quoteBo);
						
						}
					}	
				} //end of making agreed previous quote to disagree
				
				updatedQuoteResult = purcahseAssetQuotesRepository.save(updatedQuoteBO);*/
			

			/*//Added for make agree quote to disagree (only when the current quote is agreed - added by anu)
			if(prevQuoteBO != null && updateQuoteInputBean.getFkQuoteStatus().equalsIgnoreCase("AGREE"))
			{
				prevQuoteBO.setQuoteStatus("DISAGREE");
				prevQuoteBO.setModifiedDate(new Date());
				prevQuoteBO.setModifiedBy(updateQuoteInputBean.getModifiedBy());
				purcahseAssetQuotesRepository.save(prevQuoteBO);
			}
				
			if (updateQuoteInputBean.getFkQuoteStatus() == null || updateQuoteInputBean.getFkQuoteStatus().equalsIgnoreCase("DISAGREE")) 
			{
				updatedQuoteBO.setQuoteStatus("DISAGREE");
				updatedQuoteBO.setModifiedDate(new Date());
				updatedQuoteBO.setDeliveryDate(updateQuoteInputBean.getDeliveryDate());
				updatedQuoteBO = purcahseAssetQuotesRepository.save(updatedQuoteBO);
				
			} else {
				updatedQuoteBO.setQuoteStatus("AGREE");
				updatedQuoteBO.setModifiedDate(new Date());
				updatedQuoteBO.setDeliveryDate(updateQuoteInputBean.getDeliveryDate());
				updatedQuoteResult = purcahseAssetQuotesRepository.save(updatedQuoteBO);
				
				updateQuoteInputBean.setPkQuoteId(updatedQuoteResult.getPkQuoteId());
					
				if(updatedQuoteBO.getBuContribution() != null)
				{
					updatedQuoteResult.setBuContribution(updatedQuoteResult.getBuContribution());
				}
				else
				{
					throw new CommonCustomException("Please enter BU Contribution");
				}
				
				if (updatedQuoteResult != null)
				{
					updateQuoteSuccessFlag =true;
				}
		}*/
		if (updatedQuoteResult != null)
		{
			updateQuoteSuccessFlag =true;
		}
		
		return updateQuoteSuccessFlag;
	}
	//EOA by Shyam
	
	 //Add quotes for phase 2 -added by prathibha
	
	 public boolean addQuotesforRequestType2(PurchaseDatAssetQuotesInputBean quotesIpBean)
		throws CommonCustomException 
	{	
		PurchaseDatAssetQuoteBO assetQuoteBO = new PurchaseDatAssetQuoteBO();
		//PurchaseDatAssetQuoteBO result = new PurchaseDatAssetQuoteBO();
		List<PurchaseDatAssetQuoteBO> uploadedquotsLst = new ArrayList<PurchaseDatAssetQuoteBO>();
		PurchaseDatAssetRequestBO assetReqBo = new PurchaseDatAssetRequestBO();
	    //PurcahseQuotesOutPutBean quoteOuputBean = new PurcahseQuotesOutPutBean();
		PurchaseDatAssetQuoteBO quoteOuputBO = new PurchaseDatAssetQuoteBO();
		boolean quoteaddflag = false;
		// boolean agreedQuoteFlag = false;
		// to check if quotes added for this request
		try {
			LOG.startUsecase("Fetching qoutes details for requestId");
			uploadedquotsLst = purcahseAssetQuotesRepository
					.findByFkPurchaseRequestId(quotesIpBean.getFkPurchaseRequestId());
			// System.out.println("uploadedquotsLst"+uploadedquotsLst.size());
		} catch (Exception e) {
			LOG.endUsecase("Exception occured while retvieng ");
			throw new CommonCustomException("Exception Occured in addQuotes Service", e);
		}
	
		// only 3 request for one request
		if (uploadedquotsLst.size() >= 3) {
			LOG.startUsecase("Maximun 3 quotes can be uploaded");
			throw new CommonCustomException("Only 3 quotes for one asset request is allowed");
		} else {
			LOG.startUsecase("Fetching employee details");
			DatEmpDetailBO sysAdminEmpDetailBO = new DatEmpDetailBO();
			try {
				sysAdminEmpDetailBO = empDetailRepository.findByPkEmpId(quotesIpBean.getCreatedBy());
			} catch (Exception ex) {
				LOG.endUsecase("Exception in addQuotes Service while fetching details");
				throw new CommonCustomException("Unable to retrieve employee details. "
						+ "Kindly contact to Nuclues Supprt team for this issue.");
			}
		
			if (sysAdminEmpDetailBO == null) {
				throw new CommonCustomException("Unable to retrieve employee details. "
					+ "Kindly contact to Nuclues Supprt team for this issue.");
			} else {
				// restrict access only for senior system admin/systemAdmin/sysAdminProcurement
				if (sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdminProcurment.intValue()
						|| sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdmin.intValue()
						|| sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == seniorSystemAdmin.intValue()) 
				{
					// quotes can be added only if the request is approved by RM
					assetReqBo = assetReqRepo.findByPkPurchaseRequestId(quotesIpBean.getFkPurchaseRequestId());
					if (assetReqBo != null) 
					{
						 //check If the request type is requestType=2 
						if (assetReqBo.getFkRequestTypeId() != purchaseNewAssetRequest )
						{    //Checking for requestStatus in waiting
							if (assetReqBo.getPurchaseRequestStatus().intValue() == requestWaitingRmApprovalStatus) 
							{
								// If the cost is borne by IT
								if (quotesIpBean.getCostBorneBy().equalsIgnoreCase("IT")
										|| quotesIpBean.getCostBorneBy().equalsIgnoreCase("BU")) 
								{ // Total cost should be greater than equal to getIndividualLicenseCost
									if (quotesIpBean.getIndividualLicenseCost() > quotesIpBean.getTotalCost()) 
									{
										throw new CommonCustomException("Individual license cost cannot exceed total cost");
									} else {
										assetQuoteBO.setCostBorneBy(quotesIpBean.getCostBorneBy());
										assetQuoteBO.setBuContribution(quotesIpBean.getTotalCost());
									}
								}
								// if cost is shared BUcontribution is required
								else if (quotesIpBean.getCostBorneBy().equalsIgnoreCase("SHARED")) 
								{
									if (quotesIpBean.getBuContribution() >= quotesIpBean.getTotalCost()) 
									{
										throw new CommonCustomException("BU contribution cannot be equal to OR more than total cost");
									} else if (quotesIpBean.getBuContribution() > quotesIpBean.getIndividualLicenseCost()) {
										throw new CommonCustomException(
												"BU contribution cannot be more than Individual license cost");
									} else if (quotesIpBean.getIndividualLicenseCost() > quotesIpBean.getTotalCost())
									{
										throw new CommonCustomException("Individual license cost cannot be more than total cost");
									} else {
										if (quotesIpBean.getCostBorneBy()!= null)
										{
											assetQuoteBO.setCostBorneBy(quotesIpBean.getCostBorneBy());
										}
										else{
											throw new CommonCustomException("Please enter costborneby");
										}
												 
										if (quotesIpBean.getBuContribution()> 0)
										{ 
											assetQuoteBO.setBuContribution(quotesIpBean.getBuContribution()); 
										}
										else {
											throw new CommonCustomException("Please enter bu contribution  ");
										}
										
									}
								}
								
								// modifying quotes columns
								assetQuoteBO.setIndividualLicenseCost(Double.valueOf(quotesIpBean.getIndividualLicenseCost()));
								assetQuoteBO.setTotalCost(Double.valueOf(quotesIpBean.getTotalCost()));
								//Code to check if same supplier quote exists
								if (!uploadedquotsLst.isEmpty())
								{
									for (PurchaseDatAssetQuoteBO previousQuote : uploadedquotsLst )
									{
										if (previousQuote.getFkSupplierAddress() != quotesIpBean.getFkSupplierAddress() )
										{
											assetQuoteBO.setFkSupplierAddress(quotesIpBean.getFkSupplierAddress());
										}
										else {throw new CommonCustomException("This supplier quote already exists");}
									}
								}else {
									assetQuoteBO.setFkSupplierAddress(quotesIpBean.getFkSupplierAddress());
								}
								
								assetQuoteBO.setDeliveryDate(quotesIpBean.getDeliveryDate());
								assetQuoteBO.setCreatedBy(quotesIpBean.getCreatedBy());
								assetQuoteBO.setCreatedDate(new Date());
								assetQuoteBO.setFkPurchaseRequestId(quotesIpBean.getFkPurchaseRequestId());
								
								//Added by Shyam for new column / change request by Manohar
								if(!quotesIpBean.getAssetDescriptionEnteredBySysadmin().isEmpty())
								{
									assetQuoteBO.setAssetDescriptionEnteredBySysadmin(
											quotesIpBean.getAssetDescriptionEnteredBySysadmin());
								}else{
									throw new CommonCustomException("Please enter asset description / specification");
								}
								
								if (quotesIpBean.getFkPurchaseRequestId()!= null)
								{
									assetQuoteBO.setFkCurrencyType(quotesIpBean.getCurrencyType());
								}
								else	{
									throw new CommonCustomException("Currencytype for this quote is not found");
								}
								
								if(quotesIpBean.getVendorQuoteReferenceNo() != null)
									assetQuoteBO.setVendorQuoteReferenceNo(quotesIpBean.getVendorQuoteReferenceNo());
												
								//check for any disagreed quote
								int disAgreeQuotes = 0;
							
								for (PurchaseDatAssetQuoteBO previousQuote : uploadedquotsLst)
								{
									if (previousQuote.getQuoteStatus().equalsIgnoreCase("DISAGREE"))
									{
										disAgreeQuotes++;
									}
								}//End of check for any disagreed quote
								
								//check for two agree quotes
								if (uploadedquotsLst.size() > 1)
								{
									 if (disAgreeQuotes == 0)
									 {
										 throw new CommonCustomException("Not more than one quote can be set as default quotes");
									 }
								}
								// End of check for two agree quotes added on 15-11-2017
								 
								//check for previous quote status
								if ( disAgreeQuotes == 2 &&  quotesIpBean.getIsSetDefault() == false ) 
								{
									throw new CommonCustomException("Any one quote should be set as default");
								}
								else if (!quotesIpBean.getIsSetDefault()) 
								{
									assetQuoteBO.setQuoteStatus("DISAGREE");
									quoteOuputBO =	purcahseAssetQuotesRepository.save(assetQuoteBO);
								}else {
									assetQuoteBO.setQuoteStatus("AGREE");
									quoteOuputBO = purcahseAssetQuotesRepository.save(assetQuoteBO);
									
									if (!uploadedquotsLst.isEmpty()) 
									{
										for (PurchaseDatAssetQuoteBO quoteBo : uploadedquotsLst)
										{  
											if (quoteBo.getQuoteStatus().equalsIgnoreCase("AGREE")) 
										 	{
										 		quoteBo.setQuoteStatus("DISAGREE");
										 		quoteBo.setModifiedBy(quotesIpBean.getCreatedBy());
										 		quoteBo.setModifiedDate(new Date());
										 		quoteOuputBO = purcahseAssetQuotesRepository.save(quoteBo);
										 	}
										 }	
									}
								}
							}else {
								throw new CommonCustomException("Cannot add quote for this request as request asset status is not in waiting for RM approval");
							}
						} //EO checking for request type
						else {
							throw new CommonCustomException("Cannot add quote this request as the request type is in New request");
						}
					} else {
						throw new CommonCustomException("Invalid RequestId");
					}
				} else {
					throw new CommonCustomException("You are not authorized to add the quote");
				}
			}
		}
		
		if (quoteOuputBO != null)
		{
			quoteaddflag = true;
		}
		else{
			throw new CommonCustomException("Exception occured while adding quote");
		}
				
		//return quoteOuputBean;
		return quoteaddflag;
	}  //EO AddquotesPhase2
	
}


	