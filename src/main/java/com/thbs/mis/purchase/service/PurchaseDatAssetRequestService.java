//Added by Prathibha for Reject Asset request by System Admin

package com.thbs.mis.purchase.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.puchasenotification.service.PurchaseNotificationService;
import com.thbs.mis.purchase.bean.RejectAssetReqBySysAdminInputBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;
import com.thbs.mis.purchase.controller.PurchaseMasSettingController;
import com.thbs.mis.purchase.dao.PurchaseAssetQuoteRepository;
import com.thbs.mis.purchase.dao.PurchaseAssetRequestRepository;
import com.thbs.mis.purchase.dao.PurchaseDatBuRepository;

@Service
public class PurchaseDatAssetRequestService {
	
	private static final AppLog LOG 
	= LogFactory.getLog(PurchaseMasSettingController.class);
	
	@Autowired
	private PurchaseAssetRequestRepository assetReqRepository;
	
	@Autowired
	private EmpDetailRepository empDetailRepository;
	
	@Autowired
	PurchaseAssetQuoteRepository  assetQuoteRepository;
	//Added by Kamal Anand for Notifications
	@Autowired
	private PurchaseNotificationService notificationService;
	
	@Autowired
	private PurchaseDatBuRepository datBuRepository;
	
	@Value("${system.admin.procurement.roleId}")
	String sysAdminProcurementRoleId;
	//End of Addition by Kamal Anand for Notifications		
	
	@Value("${asset.request.rejected.by.sysadmin}")
	private Integer assetRequestRejectedBySysAdmin;		
	
	@Value("${systemseniorAdminRoles}")
	private Integer seniorSystemAdmin;
	
	@Value ("${systemAdminRoles}")
	private Integer systemAdmin;
	
	@Value ("${systemAdminProcurementRoleId}")
	private Integer  systemAdminProcurement;
	
	@Value("${purchase.assetRequest.closed}")
	private Integer assetRequestClosed;  
	
    @Value("${request.approvedByRm.status}")
    private Integer requestapprovedByRm; 
	
	/**
	 * 
	 * <Description reject asset request by system admin> method is to  call the repository method to
	 * reject asset request by system admin  . 
	 * 
	 * @author THBS
	 * 
	 * @param RejectAssetReqBySysAdminInputBean
	 *            is the input parameters(asset_request_Id) for this method.
	 * 
	 * @return PurchaseDatAssetRequestBO 
	 * 
	 * @throws DataAccessException,CommoncustomException
	 *            is thrown if any exception occurs during the data retrieval.
	 * 
	 */
	
	
	public PurchaseDatAssetRequestBO rejectAssetRequestBySysAdmin(RejectAssetReqBySysAdminInputBean assetBean)
			throws CommonCustomException
	{   LOG.startUsecase("System Admin reject Asset Request -Service Method");
		PurchaseDatAssetRequestBO assetRequestBo = new PurchaseDatAssetRequestBO();
		PurchaseDatAssetRequestBO outputBO = new PurchaseDatAssetRequestBO();
		try{
			assetRequestBo = assetReqRepository.findByPkPurchaseRequestId(assetBean.getPkPurchaseRequestId());
			
		} 
		catch(Exception e)
		{   LOG.endUsecase("Exception Occured in System Admin reject Asset Request -Service Method ");
			throw new CommonCustomException("Unable to retrieve your asset request information. "
					+ "Kindly contact Nucleus Support team for this issue.");
		}
		
		if(assetRequestBo == null)
		{    
			throw new CommonCustomException("Asset request does not exist.");	
		}
		else if (assetRequestBo.getPurchaseRequestStatus().intValue() == assetRequestRejectedBySysAdmin.intValue())
		{    
			throw new CommonCustomException("This asset request is already rejected by System Admin");
		}else if (assetRequestBo.getPurchaseRequestStatus().intValue()== assetRequestClosed.intValue())
		{
			throw new CommonCustomException("This request is already closed-cannot reject  closed request");
		}
		else {
			//Check modified by employee is sys-admin or not
			DatEmpDetailBO sysAdminEmpDetailBO = new DatEmpDetailBO();
			try{
				sysAdminEmpDetailBO = empDetailRepository.findByPkEmpId(assetBean.getModifiedBy());
			}catch(Exception ex){
				throw new CommonCustomException("Unable to retrieve employee details. "
						+ "Kindly contact to Nuclues Supprt team for this issue.");
			}
			
			if(sysAdminEmpDetailBO == null){
				throw new CommonCustomException("Unable to retrieve employee details. "
						+ "Kindly contact to Nuclues Supprt team for this issue.");
			}else{
				
				
				if(sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == seniorSystemAdmin.intValue() || 
						sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdmin.intValue()|| 
								sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdminProcurement.intValue() )
					
				{  if (assetRequestBo.getPurchaseRequestStatus().intValue()==requestapprovedByRm.intValue())
					{
					assetRequestBo.setPurchaseRequestStatus(assetRequestRejectedBySysAdmin);
					assetRequestBo.setModifiedOn(new Date());
					assetRequestBo.setModifiedBy(assetBean.getModifiedBy());
					assetRequestBo.setSysAdminId(assetBean.getModifiedBy());
					assetRequestBo.setSysAdminComment(assetBean.getSysAdminComment());
					assetRequestBo.setSysAdminUpdatedOn(new Date());
					try{
						outputBO = assetReqRepository.save(assetRequestBo);
					}catch(Exception ex){
						throw new CommonCustomException("Unable to update asset request data. "
								+ "Kindly contact to Nucleus Support team for this issue.");
					}
					}
				else {
					throw new CommonCustomException("Only RM approved request can be rejected ");
				    }
				}else{
					throw new CommonCustomException("Your role is not autenticated to reject this request");
				}
			}
			
		
		}
		//Added by Kamal Anand for Notifications
		List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
		if(empDetail != null)
		{
			empDetail.forEach(emp -> {
				try {
					notificationService.setAssetRequestApprovedNotificationToSysAdminScreen(emp.getPkEmpId());
				} catch (Exception e) {
					LOG.info("Exception occured while setting notification in Reject Asset Request");
				}
			});
		}
		//End of Addition by Kamal Anand
		
		return outputBO;
		
	}
	
}
