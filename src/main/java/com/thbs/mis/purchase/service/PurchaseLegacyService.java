package com.thbs.mis.purchase.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.client.RestTemplate;

import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.ClientAndProjectDetailsFromCompIdBean;
import com.thbs.mis.purchase.bean.ClientAndProjectDetailsInputBean;

public class PurchaseLegacyService 
{
	
	
	private static final AppLog LOG = LogFactory.getLog(PurchaseLegacyService.class);
	
	/*@Value("${legacy.uri}") 
	private String legacyUri;*/
	
	
	public ClientAndProjectDetailsFromCompIdBean getClientAndProjectAndComponentDetails(
			ClientAndProjectDetailsInputBean inputBean) throws CommonCustomException
	{
		RestTemplate restTemplate = new RestTemplate();
		ClientAndProjectDetailsFromCompIdBean outputBean = new ClientAndProjectDetailsFromCompIdBean();
		List<ClientAndProjectDetailsFromCompIdBean> listBean = new ArrayList<ClientAndProjectDetailsFromCompIdBean>();
		String urlString = inputBean.getLegacyUrl() + "legacyGsr/clientAndProjectAndCompDetail";
		//LOG.info("inputBean ::: " + inputBean);
		//LOG.info("urlString ::: " + urlString);
		MISResponse misResponse = restTemplate.postForObject(urlString, inputBean, MISResponse.class );
		
		if(misResponse != null)
		{
			listBean = (List<ClientAndProjectDetailsFromCompIdBean>) misResponse.getResult();
			char[] temp = listBean.toString().toCharArray();
			String finalString = ""; 
			for(int i =0; i < temp.length ; i++)
			{
				if(temp[i] == '[' ){}
				else if(temp[i] == ']'){}
				else if(temp[i] == '{'){}
				else if(temp[i] == '}'){}
				else{
					finalString = finalString + temp[i];
				}
			}
			//System.out.println("finalString :: " + finalString);
			String[] arrayString = finalString.split(",");
			List<String> listString = new ArrayList<String>();
			for(int j=0; j<arrayString.length; j++)
			{
				String[] subArrayString = arrayString[j].split("=");
				for(int k=0; k<subArrayString.length; k++)
				{
					if(k==1){
						listString.add(subArrayString[k]);
					}
				}
			}
			LOG.info("listString   :: " + listString);
		
			int count = 0;
			for (String string : listString) 
			{
				if(count == 0){
					outputBean.setClientId(Integer.valueOf(string));
				}else if(count == 1){
					outputBean.setClientName(string);
				}else if(count == 2){
					outputBean.setProjectId(Integer.valueOf(string));
				}else if(count == 3){
					outputBean.setProjectName(string);
				}else if(count == 4){
					outputBean.setComponentId(Integer.valueOf(string));
				}else if(count == 5){
					outputBean.setComponentName(string);
				}else if(count == 6){
					outputBean.setInternalProjectFlag(Byte.valueOf(string));
				}
				++count;
			}
			//outputBean = (ClientAndProjectDetailsFromCompIdBean) misResponse.getResult();
		}else{
			throw new CommonCustomException("Unable to get project details from legacy. Kindly contact to Nucleus Support Team.");
		}
		return outputBean;
	}

	//@SuppressWarnings({ "unused", "unchecked" })
	/*public List<ProjectAllocatedToEmpOutputBean> getClientAndProjectDetailsFromCompId(int requestorId) 
		throws CommonCustomException
	{
		RestTemplate restTemplate = new RestTemplate();
		String urlString = legacyUri + "/" + "legacyGsr/projects/components/{employeeId}";

		MISResponse response = restTemplate.getForObject(urlString, MISResponse.class, requestorId);

		Map<String,List<ProjectAllocatedToEmpOutputBean>> map = new 
		LinkedHashMap<String, List<ProjectAllocatedToEmpOutputBean>>();
		map.put("outputBean", (List<ProjectAllocatedToEmpOutputBean>) response.getResult());

		// convert map to JSON string 
		ObjectMapper jsonMapper = new ObjectMapper();
		String json = "";
		try {
			json = jsonMapper.writeValueAsString(map);
			json = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
		
		CompAndClientDetailsOutputBean bean = new CompAndClientDetailsOutputBean();
		try{
			bean = jsonMapper.readValue(json, CompAndClientDetailsOutputBean.class);
		}catch(Exception ex){
			LOG.error("Failed to convert from String into CompAndClientDetailsOutputBean class." + ex.getMessage());
			throw new CommonCustomException("Getting error while processing. Kindly contact to support team."); 
		}

		List<ProjectAllocatedToEmpOutputBean> projAllocatedList = new ArrayList<ProjectAllocatedToEmpOutputBean>();
		projAllocatedList.addAll(bean.getOutputBean());
		return projAllocatedList;
	}
*/
	
	
}
