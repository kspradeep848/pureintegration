package com.thbs.mis.purchase.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.AddquoteUpdateVendordetailInputBean;
import com.thbs.mis.purchase.bean.PurchaseAssetAndVendorInfoBean;
import com.thbs.mis.purchase.bean.PurchaseDatSupplierDetailsOutPutBean;
import com.thbs.mis.purchase.bean.PurchaseDatSupplierQuoteDetailsOutPutBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetQuoteBO;
import com.thbs.mis.purchase.bo.PurchaseDatSupplierDetailBO;
import com.thbs.mis.purchase.bo.PurchaseMasAssetsBO;
import com.thbs.mis.purchase.controller.PurchaseMasSettingController;
import com.thbs.mis.purchase.dao.PurchaseAssetQuoteRepository;
import com.thbs.mis.purchase.dao.PurchaseDatSupplierDetailRepository;
import com.thbs.mis.purchase.dao.PurchaseMasAssetsRepository;

//Added by Prathibha for Fetch Vendor details based on Asset Id
@Service
public class PurchaseDatSupplierDetailsService {

	private static final AppLog LOG = LogFactory.getLog(PurchaseMasSettingController.class);

	@Autowired
	PurchaseDatSupplierDetailRepository purchaseDatSupplierDetailRepository;

	@Autowired
	PurchaseMasAssetsRepository assetsRepository;

	@Autowired
	PurchaseAssetQuoteRepository quoteRepository;

	public PurchaseAssetAndVendorInfoBean getSupplierDetailsByAssetId(Integer assetId)
			throws CommonCustomException {
		LOG.startUsecase("Supplier Details Service");
		
		PurchaseAssetAndVendorInfoBean assetAndVendorInfo = null;
		PurchaseMasAssetsBO assetBO=new PurchaseMasAssetsBO(); 

		try {

			 assetBO = assetsRepository.findByPkAssetId(assetId);
		}

		catch (Exception e) {
			throw new CommonCustomException("Exception Occured in Supplier Details Service Block", e);
		}
		
		if (assetBO == null) {

			throw new CommonCustomException("Invalid AssetId");
		} else {

			 assetAndVendorInfo = new PurchaseAssetAndVendorInfoBean();
			assetAndVendorInfo.setAssetId(assetBO.getPkAssetId());
			assetAndVendorInfo.setAssetType(assetBO.getAssetType());
			assetAndVendorInfo.setLicenceType(assetBO.getLicenseType());
			assetAndVendorInfo.setAssetVersionAndModel(assetBO.getAssetNameAndVersionOrModel());
			

			List<PurchaseDatSupplierDetailBO> supplierDetBo = purchaseDatSupplierDetailRepository
					.findByFkAssetIdAndIsAvailable(assetId,"YES");

			List<PurchaseDatSupplierDetailsOutPutBean> supperDetails = null;

			if (supplierDetBo != null && !supplierDetBo.isEmpty()) {

				supperDetails = new ArrayList<PurchaseDatSupplierDetailsOutPutBean>();

				for (Iterator<PurchaseDatSupplierDetailBO> iterator = supplierDetBo.iterator(); iterator
						.hasNext();) {
					PurchaseDatSupplierDetailBO purchaseDatSupplierDetailBO = (PurchaseDatSupplierDetailBO) iterator
							.next();
					PurchaseDatSupplierDetailsOutPutBean supplierBean = new PurchaseDatSupplierDetailsOutPutBean();
					supplierBean.setPkSupplierId(purchaseDatSupplierDetailBO.getPkSupplierId());
					supplierBean.setVendorName(purchaseDatSupplierDetailBO.getVendorName());
					supplierBean.setVendorAddress(purchaseDatSupplierDetailBO.getVendorAddress());
					supplierBean.setVendorContactNo(purchaseDatSupplierDetailBO.getVendorContactNo());
					supplierBean.setContactPerson(purchaseDatSupplierDetailBO.getContactPerson());
					supplierBean.setEmailId(purchaseDatSupplierDetailBO.getEmailId());
					supplierBean.setCreatedBy(purchaseDatSupplierDetailBO.getCreatedBy());
					supplierBean.setCreatedDate(purchaseDatSupplierDetailBO.getCreatedDate());
					supplierBean.setModifiedBy(purchaseDatSupplierDetailBO.getModifiedBy());
					supplierBean.setModifiedOn(purchaseDatSupplierDetailBO.getModifiedOn());
					supplierBean.setPriceQuotedByVendor(purchaseDatSupplierDetailBO.getPriceQuotedByVendor());
					supplierBean.setFkAssetId(purchaseDatSupplierDetailBO.getFkAssetId());
					supplierBean.setDeliveryTime(purchaseDatSupplierDetailBO.getDeliveryTime());
					supplierBean.setIsAvailable(purchaseDatSupplierDetailBO.getIsAvailable());
					

					supperDetails.add(supplierBean);

				}

				assetAndVendorInfo.setVendorDetails(supperDetails);

			}
		}
		
		LOG.endUsecase("Supplier Details Service-return");
		return assetAndVendorInfo;
	}

	// EOA by Prathibha for Fetch Vendor details based on Asset Id

	/*************************
	 * fetch Supplier Quotes details based on Asset Id
	 *******************/

	// Added by Prathibha for Fetch Supplier details along with thier quotes based
	// on Purchase request Id

	public List<PurchaseDatSupplierQuoteDetailsOutPutBean> getSupplierQuoteDetailsByRequestId(Integer purchaseReqId)
			throws CommonCustomException {
		List<PurchaseDatAssetQuoteBO> supQuateList = new ArrayList<PurchaseDatAssetQuoteBO>();
		List<PurchaseDatSupplierQuoteDetailsOutPutBean> suplrDetQuteBeanList = new ArrayList<PurchaseDatSupplierQuoteDetailsOutPutBean>();

		try {
			LOG.startUsecase("getSupplierQuoteDetailsByRequestId Service -try block");

			supQuateList = quoteRepository.findByFkPurchaseRequestId(purchaseReqId);

		} catch (Exception e) {
			LOG.endUsecase("getSupplierQuoteDetailsBy purchase request-Id Service -Catch block ");
			throw new CommonCustomException("Exception Occured in  Service");
		}

		LOG.endUsecase("getSupplierQuoteDetailsByRequestId Servic for block ");

		if (supQuateList.size() == 0) {
			throw new CommonCustomException("Invalid Request-Id");
		}

		for (PurchaseDatAssetQuoteBO purchaseDatAssetQuoteBO : supQuateList) {

			PurchaseDatSupplierQuoteDetailsOutPutBean bean = new PurchaseDatSupplierQuoteDetailsOutPutBean();
			bean.setVendorName(purchaseDatAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorName());
			bean.setVendorAddress(purchaseDatAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorAddress());
			bean.setDeliveryDate(purchaseDatAssetQuoteBO.getDeliveryDate());
			bean.setIndividualLicenseCost(purchaseDatAssetQuoteBO.getIndividualLicenseCost());
			bean.setVendorContactNo(purchaseDatAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorContactNo());
			bean.setPkQuoteId(purchaseDatAssetQuoteBO.getPkQuoteId());
			bean.setFkAssetId(purchaseDatAssetQuoteBO.getPurchaseDatSupplierDetailBO().getFkAssetId());
			bean.setFkPurchaseRequestId(purchaseDatAssetQuoteBO.getFkPurchaseRequestId());
			suplrDetQuteBeanList.add(bean);

		}

		return suplrDetQuteBeanList;

	}

	/***********************************************************************************************/

	public List<PurchaseDatSupplierQuoteDetailsOutPutBean> getSupplierQuoteDetailsByAssetId(Integer assetId)
			throws CommonCustomException {
		List<PurchaseDatAssetQuoteBO> supQuoteList = new ArrayList<PurchaseDatAssetQuoteBO>();
		List<PurchaseDatSupplierQuoteDetailsOutPutBean> suplrDetQuteBeanList = new ArrayList<PurchaseDatSupplierQuoteDetailsOutPutBean>();
		try {
			supQuoteList = quoteRepository.getSupplierQuoteDetailsByAssetId(assetId);
		} catch (Exception e) {
			throw new CommonCustomException("Exception Occured in  Service",e);
		}

		if (supQuoteList.isEmpty()) {
			throw new CommonCustomException("Invalid Asset-Id");
		} else {

			for (PurchaseDatAssetQuoteBO purchaseDatAssetQuoteBO : supQuoteList) {

				PurchaseDatSupplierQuoteDetailsOutPutBean bean = new PurchaseDatSupplierQuoteDetailsOutPutBean();
				bean.setVendorName(purchaseDatAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorName());
				bean.setVendorAddress(purchaseDatAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorAddress());
				bean.setDeliveryDate(purchaseDatAssetQuoteBO.getDeliveryDate());
				bean.setIndividualLicenseCost(purchaseDatAssetQuoteBO.getIndividualLicenseCost());
				bean.setVendorContactNo(purchaseDatAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorContactNo());
				bean.setPkQuoteId(purchaseDatAssetQuoteBO.getPkQuoteId());
				bean.setFkAssetId(purchaseDatAssetQuoteBO.getPurchaseDatSupplierDetailBO().getFkAssetId());
				suplrDetQuteBeanList.add(bean);

			}
		}

		return suplrDetQuteBeanList;

	}

	//// getSupplierQuoteDetailsByAssetId

/***********************************Add quote update vendor details****************/
	
	  public boolean addQuoteUpdateVendor(Integer vendorId )throws CommonCustomException
	  {   
		  AddquoteUpdateVendordetailInputBean vendorInputBean=new AddquoteUpdateVendordetailInputBean();
		  PurchaseDatSupplierDetailBO supplierdet=new PurchaseDatSupplierDetailBO(); 
		  boolean updatedFlag=false;
		  try{
		  
		  supplierdet=purchaseDatSupplierDetailRepository.findByPkSupplierId(vendorId);
		  }
		  catch (Exception e)
		  {
			 throw new CommonCustomException("Exception occured wile fetch vendor details",e); 
			  
		  }
		  
		  if (supplierdet!= null)
		  {
			  PurchaseDatSupplierDetailBO suppBo=new PurchaseDatSupplierDetailBO();
			  
			  suppBo.setVendorAddress(vendorInputBean.getVendorAddress());
			  suppBo.setVendorContactNo(vendorInputBean.getVendorContactNo());
			  suppBo.setVendorName(vendorInputBean.getVendorName());
			  suppBo.setCreatedBy(supplierdet.getCreatedBy());
			  suppBo.setCreatedDate(supplierdet.getCreatedDate());
			  suppBo.setFkAssetId(supplierdet.getFkAssetId());
			  
			  purchaseDatSupplierDetailRepository.save(suppBo);
			  
			  updatedFlag=true;
		  }
		
		  
		  
		 return updatedFlag; 
	  }
	

}
