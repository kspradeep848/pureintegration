package com.thbs.mis.purchase.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.PurchaseAddVendorDetailsInputBean;
import com.thbs.mis.purchase.bean.PurchaseAddVendorForExistingAssetBean;
import com.thbs.mis.purchase.bean.PurchaseInputBean;
import com.thbs.mis.purchase.bean.PurchaseUpdateVendorDetailsInputBean;
import com.thbs.mis.purchase.bean.PurchaseVendorDetailsBean;
import com.thbs.mis.purchase.bean.PurchaseVendorDetailsUpdateBean;
import com.thbs.mis.purchase.bo.PurchaseDatSupplierDetailBO;
import com.thbs.mis.purchase.bo.PurchaseMasAssetsBO;
import com.thbs.mis.purchase.dao.PurchaseMasAssetsRepository;
import com.thbs.mis.purchase.dao.PurchaseSupplierDetailsRepository;

@Service
public class VendorDetailsService {

	private static final AppLog LOG = LogFactory
			.getLog(PurchaseCapexService.class);

	@Autowired
	private PurchaseMasAssetsRepository purchaseMasAssetsRepo;

	@Autowired
	private PurchaseSupplierDetailsRepository purchaseSupplierDetailsRepo;
	
	@Autowired
	private EmpDetailRepository empDetailRepository;
	
	
	@Value("${hardware.license.type}")
	private String hardwareLicenseType;
	
	@Value("${software.license.type}")
	private String softwareLicenseType;
	
	@Value("${purchase.delete.software.vendor.include}")
	private String systemAdminIds;
	
	final static int MAX_NUMBER_OF_VENDORS_ALLOWED = 3;

	// Added by Prathibha

	/**
	 * 
	 * @param assetId
	 * @return
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to delete software.
	 */

	public PurchaseMasAssetsBO deleteSoftware(PurchaseInputBean deleteSoftwareBean)
			throws CommonCustomException {
		LOG.startUsecase("Delete software");
		PurchaseMasAssetsBO purchaseMassAssetsDetails = new PurchaseMasAssetsBO();
		DatEmpDetailBO empDetailBo = new DatEmpDetailBO();
		try {

			purchaseMassAssetsDetails = purchaseMasAssetsRepo
					.findByPkAssetId(deleteSoftwareBean.getId());

		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to retrieve asset details. Kindly contact to Nucleus Support team for this issue.");
		}

		if (purchaseMassAssetsDetails == null) {
			throw new CommonCustomException("Asset request does not exist.");
		}

		else if (purchaseMassAssetsDetails.getIsAvailable().equals("NO")) {
			throw new CommonCustomException("Software is already deleted.");
		}
		else{
		  	List<Short> systemAdminIdList = new ArrayList<Short>();
		  	String[] empIdArray = systemAdminIds.split(",");
		  	for (String s : empIdArray)
		  		systemAdminIdList.add(Short.parseShort(s));
						
				try{
					empDetailBo = empDetailRepository.getSystemAdminRoleDetails(deleteSoftwareBean.getLoggedInUser(),systemAdminIdList);
				}
				catch(Exception ex)
				{
					throw new CommonCustomException("Failed to fetch data from db.");
				}
				if(empDetailBo!=null)
				{	
					purchaseMassAssetsDetails.setIsAvailable("NO");
					purchaseMassAssetsDetails.setModifiedDate(new Date());
					purchaseMassAssetsDetails.setModifiedBy(deleteSoftwareBean.getLoggedInUser());
					purchaseMasAssetsRepo.save(purchaseMassAssetsDetails);
				}
		
			else
			{
				throw new CommonCustomException("Neither you are having role of 'Senior system administrator' nor 'System admin procurement'.Hence you are not allowed to delete software.");
			}
			}
		LOG.endUsecase("Delete software");
		return purchaseMassAssetsDetails;
		}
	

	/**
	 * 
	 * @param supplierId
	 * @return
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to delete vendor.
	 */

	public PurchaseDatSupplierDetailBO deleteVendor(PurchaseInputBean deleteVendorBean)
			throws CommonCustomException {
		LOG.startUsecase("Delete vendor");
		PurchaseDatSupplierDetailBO supplierDetails = new PurchaseDatSupplierDetailBO();
		DatEmpDetailBO empDetailBo = new DatEmpDetailBO();
		try {

			supplierDetails = purchaseSupplierDetailsRepo
					.findByPkSupplierId(deleteVendorBean.getId());

		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to retrieve asset details. Kindly contact to Nucleus Support team for this issue.");
		}

		if (supplierDetails == null) {
			throw new CommonCustomException("Supplier details does not exist.");
		}

		if (supplierDetails.getIsAvailable().equals("NO")) {
			throw new CommonCustomException("Vendor is already deleted.");
	}
			else{
			  	List<Short> systemAdminIdList = new ArrayList<Short>();
			  	String[] empIdArray = systemAdminIds.split(",");
			  	for (String s : empIdArray)
			  		systemAdminIdList.add(Short.parseShort(s));
							
					try{
						empDetailBo = empDetailRepository.getSystemAdminRoleDetails(deleteVendorBean.getLoggedInUser(),systemAdminIdList);
					}
					catch(Exception ex)
					{
						throw new CommonCustomException("Failed to fetch data from db.");
					}
					if(empDetailBo!=null)
					{	
						supplierDetails.setIsAvailable("NO");
						supplierDetails.setModifiedOn(new Date());
						supplierDetails.setModifiedBy(deleteVendorBean.getLoggedInUser());
						purchaseSupplierDetailsRepo.save(supplierDetails);
					}
			
				else
				{
					throw new CommonCustomException("Neither you are having role of 'Senior system administrator' nor 'System admin procurement'.Hence you are not allowed to delete vendor.");
				}
			}
					LOG.endUsecase("Delete software");
					return supplierDetails;
		}
				
	public boolean addVendorDetailsForExistingAsset(
			PurchaseAddVendorForExistingAssetBean purchaseAddVendorForExistingAssetBean)
			throws CommonCustomException {
		LOG.startUsecase("Add Vendor Details for existing asset");
		boolean outputFlag = false;
		boolean duplicateVendorEntryFlag = false;
		PurchaseMasAssetsBO masAssets = new PurchaseMasAssetsBO();
		List<PurchaseDatSupplierDetailBO> supplierDetailsList = new ArrayList<PurchaseDatSupplierDetailBO>();
		LOG.info("purchaseAddVendorForExistingAssetBean.getVendorDetails().size():"+purchaseAddVendorForExistingAssetBean.getVendorDetails().size());
		System.out.println("purchaseAddVendorForExistingAssetBean.getVendorDetails().size():"+purchaseAddVendorForExistingAssetBean.getVendorDetails().size());
		
		
		//get list of vendors 
		try {
			supplierDetailsList = purchaseSupplierDetailsRepo
					.getVendorsDetails(purchaseAddVendorForExistingAssetBean.getPkAssetId());

		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to retrieve asset details. Kindly contact to Nucleus Support team for this issue.");
		}
		
		if(supplierDetailsList.size() == MAX_NUMBER_OF_VENDORS_ALLOWED)
		{
			throw new CommonCustomException("Already 3 vendor details present for the requested asset.");
		} 
		if (purchaseAddVendorForExistingAssetBean.getVendorDetails().size() > 0)
		{
			
				List<PurchaseVendorDetailsBean> inputVendorsList = purchaseAddVendorForExistingAssetBean.getVendorDetails();
				for (PurchaseDatSupplierDetailBO purchaseDatSupplierDetailBO : supplierDetailsList) 
				{
					
					for (PurchaseVendorDetailsBean purchaseVendorDetailsBean : inputVendorsList) 
					{
						
						if(purchaseVendorDetailsBean.getVendorName().equalsIgnoreCase(purchaseDatSupplierDetailBO.getVendorName()) 
							&& purchaseVendorDetailsBean.getEmailId().equalsIgnoreCase(purchaseDatSupplierDetailBO.getEmailId()))
						{
							duplicateVendorEntryFlag = true;
							break;
						}
						
					}
				}
				
				if(duplicateVendorEntryFlag)
				{
					throw new CommonCustomException("Entered vendor details is already exists.");
				}
				//-------------------------------------------
				/*int duplicateInputCount = 0;
				for (PurchaseVendorDetailsBean vendorBean : inputVendorsList) 
				{
					duplicateInputCount = Collections.frequency(inputVendorsList, vendorBean.getEmailId());
				}
				
				if(duplicateInputCount > 0)
				{
					throw new CommonCustomException("You entered duplicate vendors under same asset.");
				}*/
		}else 
		{
			throw new CommonCustomException("Atleast one vendor details must be given.");
		}
		
   	masAssets = purchaseMasAssetsRepo.findByPkAssetId(purchaseAddVendorForExistingAssetBean.getPkAssetId());
		
		if(masAssets == null)
		{
			throw new CommonCustomException("Asset details does not exist");
		}	
		if(supplierDetailsList.size() == 0)
		{
			if(purchaseAddVendorForExistingAssetBean.getVendorDetails().size() > 3)
			{
				throw new CommonCustomException("Only 3 vendor details can be added.");
			}
			else
			{
				outputFlag = saveVendorDetails(purchaseAddVendorForExistingAssetBean.getVendorDetails(),masAssets.getPkAssetId(),purchaseAddVendorForExistingAssetBean.getCreatedBy());
			}
		}
		if(supplierDetailsList.size() == 2)
		{
			if(purchaseAddVendorForExistingAssetBean.getVendorDetails().size() > 1)
			{
				throw new CommonCustomException("Only 1 vendor details can be added.");
			}
			else
			{
				outputFlag = saveVendorDetails(purchaseAddVendorForExistingAssetBean.getVendorDetails(),masAssets.getPkAssetId(),purchaseAddVendorForExistingAssetBean.getCreatedBy());
			}
		}
		if(supplierDetailsList.size() == 1)
		{
			if(purchaseAddVendorForExistingAssetBean.getVendorDetails().size() > 2)
			{
				throw new CommonCustomException("Only 2 vendor details can be added.");
			}
			else
			{
				outputFlag = saveVendorDetails(purchaseAddVendorForExistingAssetBean.getVendorDetails(),masAssets.getPkAssetId(),purchaseAddVendorForExistingAssetBean.getCreatedBy());
			}
		}

		LOG.endUsecase("Add Vendor Details for existing asset");

		return outputFlag;
			}

	public boolean saveVendorDetails(List<PurchaseVendorDetailsBean> vendorDetailList,int assetId,int createdById){
		LOG.startUsecase("save Vendor Details");
		boolean outputFlag = false;
		if(!vendorDetailList.isEmpty()){
			for(PurchaseVendorDetailsBean vendorBean : vendorDetailList)
			{
				PurchaseDatSupplierDetailBO addVendorDetailsOutput = new PurchaseDatSupplierDetailBO();
				
				addVendorDetailsOutput.setVendorName(vendorBean
						.getVendorName());
				addVendorDetailsOutput.setVendorAddress(vendorBean
						.getVendorAddress());
				addVendorDetailsOutput
						.setPriceQuotedByVendor(vendorBean
								.getPriceQuotedByVendor());
				addVendorDetailsOutput.setVendorContactNo(vendorBean
						.getVendorContactNo());
				addVendorDetailsOutput.setDeliveryTime(vendorBean
					.getDeliveryTime());
				
				addVendorDetailsOutput.setCreatedDate(new Date());
				addVendorDetailsOutput.setFkAssetId(assetId);
				addVendorDetailsOutput
						.setCreatedBy(createdById);
				addVendorDetailsOutput.setContactPerson(vendorBean
						.getContactPerson());
				addVendorDetailsOutput.setEmailId(vendorBean
						.getEmailId());
				addVendorDetailsOutput.setIsAvailable("YES");
				purchaseSupplierDetailsRepo
						.save(addVendorDetailsOutput);
				outputFlag = true;
			}
		}
		LOG.endUsecase("save Vendor Details");
		return outputFlag;
		
	}
		
	//}


	// Added by Smrithi

	public Boolean addVendorDetails(
			PurchaseAddVendorDetailsInputBean purchaseAddVendorDetailsInputBean)
			throws CommonCustomException {

		LOG.startUsecase("Add Vendor Details");
		boolean flag = false;
		PurchaseMasAssetsBO masAssets = new PurchaseMasAssetsBO();
		PurchaseMasAssetsBO masAssetsResult = new PurchaseMasAssetsBO();
		//PurchaseAddVendorDetailsInputBean finalInputBean = new PurchaseAddVendorDetailsInputBean();
		//List<PurchaseVendorDetailsBean> vendorList = new ArrayList<PurchaseVendorDetailsBean>();

		if (purchaseAddVendorDetailsInputBean.getVendorDetails() != null){
			if (purchaseAddVendorDetailsInputBean.getVendorDetails().size() > 3)
			{
				throw new CommonCustomException(
						"Only 3 vendor details can be added.");
			}
		}
		
	/*	if (purchaseAddVendorDetailsInputBean.getVendorDetails().size() > 0) {
			for (PurchaseVendorDetailsBean vendorBean : purchaseAddVendorDetailsInputBean
					.getVendorDetails()) {
				
			}
		}*/
		
		try{
			masAssetsResult = purchaseMasAssetsRepo.getAssetName(purchaseAddVendorDetailsInputBean.getAssetNameAndVersionOrModel());
		}
		catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from db");
		}
		if(masAssetsResult != null){
			throw new CommonCustomException("Asset name is already present. Please give another name.");
		}
		masAssets
				.setAssetNameAndVersionOrModel(purchaseAddVendorDetailsInputBean
						.getAssetNameAndVersionOrModel());
		masAssets
				.setAssetType(purchaseAddVendorDetailsInputBean.getAssetType());
		masAssets.setCreatedDate(new Date());
		masAssets
				.setCreatedBy(purchaseAddVendorDetailsInputBean.getCreatedBy());
		masAssets.setIsAvailable("YES");
		
		
		if (purchaseAddVendorDetailsInputBean.getAssetType().equalsIgnoreCase("HARDWARE")) {
			List<String> hardwareLicenceTypeList = Stream.of(hardwareLicenseType.split(","))
	                .collect(Collectors.toList());
			
			if(hardwareLicenceTypeList.contains(purchaseAddVendorDetailsInputBean.getLicenseType().toUpperCase())){
			masAssets.setLicenseType(purchaseAddVendorDetailsInputBean.getLicenseType().toUpperCase());
			}else{
				throw new CommonCustomException("Data mismatch with asset type and license type. Please check again.");
			}
		}
		if (purchaseAddVendorDetailsInputBean.getAssetType().equalsIgnoreCase("SOFTWARE")){
			List<String> softwareLicenceTypeList = Stream.of(softwareLicenseType.split(","))
	                .collect(Collectors.toList());
			if(softwareLicenceTypeList.contains(purchaseAddVendorDetailsInputBean.getLicenseType().toUpperCase())){
			masAssets.setLicenseType(purchaseAddVendorDetailsInputBean.getLicenseType().toUpperCase());
			}else{
				throw new CommonCustomException("Data mismatch with asset type and license type. Please check again.");
			}
		}
		
		
		try {
			masAssetsResult = purchaseMasAssetsRepo.save(masAssets);
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (purchaseAddVendorDetailsInputBean.getVendorDetails() != null) {
			if (purchaseAddVendorDetailsInputBean.getVendorDetails().size() > 0) {
				if (purchaseAddVendorDetailsInputBean.getVendorDetails().size() <= 3) {
					for (PurchaseVendorDetailsBean vendorBean : purchaseAddVendorDetailsInputBean
							.getVendorDetails()) {
						PurchaseDatSupplierDetailBO addVendorDetailsOutput = new PurchaseDatSupplierDetailBO();
						
						addVendorDetailsOutput.setVendorName(vendorBean
								.getVendorName());
						addVendorDetailsOutput.setVendorAddress(vendorBean
								.getVendorAddress());
						addVendorDetailsOutput
								.setPriceQuotedByVendor(vendorBean
										.getPriceQuotedByVendor());
						/*if(vendorBean
								.getVendorContactNo() == null)
						{
							throw new CommonCustomException("Vendor Contact number cannot be null");
						} else if(vendorBean
								.getVendorContactNo().intValue()<=0){
							throw new CommonCustomException("Vendor Contact number must be greater than or equal to 1");
						} else{	*/
							addVendorDetailsOutput.setVendorContactNo(vendorBean
								.getVendorContactNo());
						//}
						/*if(vendorBean
								.getDeliveryTime() == null)
						{
							throw new CommonCustomException("Delivery time cannot be null");
						} else if(vendorBean
								.getVendorContactNo().intValue()<=0){
							throw new CommonCustomException("Delivery time must be greater than or equal to 1");
						} else{*/
						addVendorDetailsOutput.setDeliveryTime(vendorBean
								.getDeliveryTime());
						//}
						addVendorDetailsOutput.setCreatedDate(new Date());
						addVendorDetailsOutput.setFkAssetId(masAssetsResult
								.getPkAssetId());
						addVendorDetailsOutput
								.setCreatedBy(purchaseAddVendorDetailsInputBean
										.getCreatedBy());
						addVendorDetailsOutput.setContactPerson(vendorBean
								.getContactPerson());
						addVendorDetailsOutput.setEmailId(vendorBean
								.getEmailId());
						addVendorDetailsOutput.setIsAvailable("YES");
						purchaseSupplierDetailsRepo
								.save(addVendorDetailsOutput);
						flag = true;
					}
				} else {
					throw new CommonCustomException(
							"Only 3 vendor details can be added.");
				}
			}
		}

		LOG.endUsecase("Add Vendor Details");

		return flag;
	}

	public boolean updateVendorDetails(
			PurchaseUpdateVendorDetailsInputBean purchaseUpdateVendorDetailsInputBean)
			throws CommonCustomException {
		LOG.startUsecase("Update Vendor Details");
		boolean flag = false;
	//	PurchaseMasAssetsBO masAssets = new PurchaseMasAssetsBO();
		PurchaseMasAssetsBO masAssetsResult = new PurchaseMasAssetsBO();

		try {
			masAssetsResult = purchaseMasAssetsRepo
					.findByPkAssetId(purchaseUpdateVendorDetailsInputBean
							.getPkAssetId());
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (masAssetsResult == null) {
			throw new CommonCustomException("Asset ID does not exist.");
		} else {

			if (purchaseUpdateVendorDetailsInputBean.getVendorDetails() != null)
				if (purchaseUpdateVendorDetailsInputBean.getVendorDetails()
						.size() > 3)
					throw new CommonCustomException(
							"Only 3 vendor details can be added.");
			
			masAssetsResult
					.setAssetNameAndVersionOrModel(purchaseUpdateVendorDetailsInputBean
							.getAssetNameAndVersionOrModel());
			masAssetsResult.setAssetType(purchaseUpdateVendorDetailsInputBean
					.getAssetType());
			masAssetsResult.setModifiedDate(new Date());
			masAssetsResult.setModifiedBy(purchaseUpdateVendorDetailsInputBean
					.getModifiedBy());
			masAssetsResult.setIsAvailable("YES");
			
			if (purchaseUpdateVendorDetailsInputBean.getAssetType().equalsIgnoreCase("HARDWARE")) {
				List<String> hardwareLicenceTypeList = Stream.of(hardwareLicenseType.split(","))
		                .collect(Collectors.toList());
				
				if(hardwareLicenceTypeList.contains(purchaseUpdateVendorDetailsInputBean.getLicenseType().toUpperCase())){
					masAssetsResult.setLicenseType(purchaseUpdateVendorDetailsInputBean.getLicenseType().toUpperCase());
				}else{
					throw new CommonCustomException("Data mismatch with asset type and license type. Please check again.");
				}
			}
			if (purchaseUpdateVendorDetailsInputBean.getAssetType().equalsIgnoreCase("SOFTWARE")){
				List<String> softwareLicenceTypeList = Stream.of(softwareLicenseType.split(","))
		                .collect(Collectors.toList());
				if(softwareLicenceTypeList.contains(purchaseUpdateVendorDetailsInputBean.getLicenseType().toUpperCase())){
					masAssetsResult.setLicenseType(purchaseUpdateVendorDetailsInputBean.getLicenseType().toUpperCase());
				}else{
					throw new CommonCustomException("Data mismatch with asset type and license type. Please check again.");
				}
			}
			

			try {
				purchaseMasAssetsRepo.save(masAssetsResult);
				flag = true;
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (purchaseUpdateVendorDetailsInputBean.getVendorDetails() != null) {
				if (purchaseUpdateVendorDetailsInputBean.getVendorDetails()
						.size() > 0) {
					if (purchaseUpdateVendorDetailsInputBean.getVendorDetails()
							.size() <= 3) {

						for (PurchaseVendorDetailsUpdateBean vendorBean : purchaseUpdateVendorDetailsInputBean
								.getVendorDetails()) {

							PurchaseDatSupplierDetailBO updateVendorDetailsOutput = new PurchaseDatSupplierDetailBO();
							try {
								updateVendorDetailsOutput = purchaseSupplierDetailsRepo
										.findByPkSupplierIdAndFkAssetId(
												vendorBean.getPkSupplierId(),
												purchaseUpdateVendorDetailsInputBean
														.getPkAssetId());
							} catch (Exception e) {
								e.printStackTrace();
							}
							if (updateVendorDetailsOutput == null) {
								throw new CommonCustomException(
										"Supplier ID does not match with the asset id.");
							} else {
								updateVendorDetailsOutput
										.setPkSupplierId(vendorBean
												.getPkSupplierId());
								updateVendorDetailsOutput
										.setVendorName(vendorBean
												.getVendorName());
								updateVendorDetailsOutput
										.setVendorAddress(vendorBean
												.getVendorAddress());
								updateVendorDetailsOutput
										.setPriceQuotedByVendor(vendorBean
												.getPriceQuotedByVendor());
								/*if(vendorBean
										.getVendorContactNo() == null)
								{
									throw new CommonCustomException("Vendor Contact number cannot be null");
								} else if(vendorBean
										.getVendorContactNo().intValue()<=0){
									throw new CommonCustomException("Vendor Contact number must be greater than or equal to 1");
								}
								else{	*/
									updateVendorDetailsOutput.setVendorContactNo(vendorBean
										.getVendorContactNo());
								//}
								/*if(vendorBean
										.getDeliveryTime() == null)
								{
									throw new CommonCustomException("Delivery time cannot be null");
								}
								 else if(vendorBean
											.getVendorContactNo().intValue()<=0){
										throw new CommonCustomException("Delivery time must be greater than or equal to 1");
									}
								else{*/
									updateVendorDetailsOutput.setDeliveryTime(vendorBean
										.getDeliveryTime());
								//}
								updateVendorDetailsOutput
										.setModifiedOn(new Date());
								updateVendorDetailsOutput
										.setFkAssetId(masAssetsResult
												.getPkAssetId());
								updateVendorDetailsOutput
										.setModifiedBy(purchaseUpdateVendorDetailsInputBean
												.getModifiedBy());
								updateVendorDetailsOutput
										.setContactPerson(vendorBean
												.getContactPerson());
								updateVendorDetailsOutput.setEmailId(vendorBean
										.getEmailId());
								updateVendorDetailsOutput.setIsAvailable("YES");
								purchaseSupplierDetailsRepo
										.save(updateVendorDetailsOutput);
								flag = true;
							}
						}
					} else {
						throw new CommonCustomException(
								"Only 3 vendor details can be added.");
					}
				}
			}

		}

		LOG.endUsecase("Updated Vendor Details");

		return flag;

	}

	// EOA by Smrithi

}
