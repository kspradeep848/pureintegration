package com.thbs.mis.purchase.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.PurchaseMasSettingBean;
import com.thbs.mis.purchase.bean.SysAdminSettingsOutputBean;
import com.thbs.mis.purchase.dao.PurchaseMasAssetsRepository;
import com.thbs.mis.purchase.dao.PurchaseMasSettingRepository;
import com.thbs.mis.purchase.dao.PurchaseSupplierDetailsRepository;

@Service
public class PurchaseMasSettingService {
	private static final AppLog LOG = LogFactory.getLog(PurchaseMasSettingService.class);

	@Autowired
	private PurchaseMasSettingRepository purchaseMasSettingRepository;

	@Autowired
	private EmpDetailRepository empDetailRepository;
	@Autowired
	private EmployeePersonalDetailsRepository employeePersonalDetailsRepository;

	@Autowired
	private PurchaseSupplierDetailsRepository purchaseSupplierDetailsRepo;

	@Autowired
	private PurchaseMasAssetsRepository purchaseMasAssetsRepo;	

	@Value("${systemseniorAdminRoles}")
	private String systemseniorAdminRoles;
	
	@Value("${systemAdminRoles}")
	private String systemAdminRoles;
	
	@Value("${systemAdminProcurementRoleId}")
	private Short systemAdminProcurementRole;
	
	@Value("${nbu.it.business.unit.id}")
	private Short nbuItBusinessUnitId;
	
	@Value("${purchase.add.asset.systemAdminRoles}")
	private String systemAdminRolesList;
	

	public boolean addAccessBySysAdmin(PurchaseMasSettingBean settingBean)
			throws CommonCustomException
	{
		boolean accessProvidedFlag = false;
		DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
		DatEmpDetailBO updatedDatEmpDetailBO = new DatEmpDetailBO();
		try{
			datEmpDetailBO = empDetailRepository.findByPkEmpId(settingBean.getSystemAdminId());
		}catch(Exception ex){
			throw new CommonCustomException("Unable to get emp detials. Kindly get touch with Nucleus Support team.");
		}
		
		if(datEmpDetailBO.getFkEmpRoleId().shortValue() != systemAdminProcurementRole.shortValue())
		{
			
			if(settingBean.getSettingType().equalsIgnoreCase("ENABLE"))
				datEmpDetailBO.setFkEmpRoleId(systemAdminProcurementRole);
			else
				throw new CommonCustomException("You are not allowed to revoke their role. Kinldy get in touch with Nucleus Support team.");
			datEmpDetailBO.setModifiedBy(settingBean.getUpdatedBy());
			datEmpDetailBO.setModifiedDate(new Date());
			updatedDatEmpDetailBO = empDetailRepository.save(datEmpDetailBO);
		}else{
			throw new CommonCustomException("This employee already have the role.");
		}
		
		if(updatedDatEmpDetailBO != null){
			accessProvidedFlag = true;
		}
		return accessProvidedFlag;
	}

	public boolean updateAccessBySysAdmin(PurchaseMasSettingBean settingBean)
			throws CommonCustomException
	{
		boolean accessRemovedFlag = false;
		DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
		DatEmpDetailBO updatedDatEmpDetailBO = new DatEmpDetailBO();
		
		try{
			datEmpDetailBO = empDetailRepository.findByPkEmpId(settingBean.getSystemAdminId());
		}catch(Exception ex){
			throw new CommonCustomException("Unable to get emp detials. Kindly get in touch with Nucleus Support team.");
		}
		
		if(datEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdminProcurementRole.shortValue())
		{
			if(settingBean.getSettingType().equalsIgnoreCase("DISABLE"))
				datEmpDetailBO.setFkEmpRoleId(Short.valueOf(systemAdminRoles));
			else
				throw new CommonCustomException("You are not allowed to provide privilege to this employee. Kindly get in touch with Nucleus Support team.");
			datEmpDetailBO.setModifiedBy(settingBean.getUpdatedBy());
			datEmpDetailBO.setModifiedDate(new Date());
			updatedDatEmpDetailBO = empDetailRepository.save(datEmpDetailBO);
		}
		if(updatedDatEmpDetailBO != null){
			accessRemovedFlag = true;
		}
		return accessRemovedFlag;
	}
	public List<SysAdminSettingsOutputBean> getSystemAdminEmpIds() throws DataAccessException

	{
		LOG.startUsecase("getSystemAdminEmpIds");
		List<SysAdminSettingsOutputBean> finalList = new ArrayList<SysAdminSettingsOutputBean>();
		SysAdminSettingsOutputBean sysAdminSettingsOutputBean;
		try {
			List<DatEmpDetailBO> empDetailBoList = new  ArrayList<DatEmpDetailBO>();
			DatEmpPersonalDetailBO datEmpPersonalDetailBO;
			try {

				List<Short> systemAdminIdList = new ArrayList<Short>();
			  	String[] empIdArray = systemAdminRolesList.split(",");
			  	for (String s : empIdArray)
			  		systemAdminIdList.add(Short.parseShort(s));
							
					try{
						empDetailBoList = empDetailRepository.getSystemAdminRoleDetails(systemAdminIdList);
					}
					catch(Exception ex)
					{
						throw new CommonCustomException("Failed to fetch data from db.");
					}
				for (DatEmpDetailBO bo : empDetailBoList) {
					try {
						datEmpPersonalDetailBO = employeePersonalDetailsRepository.findByFkEmpDetailId(bo.getPkEmpId());
					
					} catch (Exception e) {
						throw new DataAccessException("Error occured in getSystemAdminEmpIds");
					}
					sysAdminSettingsOutputBean = new SysAdminSettingsOutputBean();
					sysAdminSettingsOutputBean.setSystemAdminId(bo.getPkEmpId());
					sysAdminSettingsOutputBean.setSystenAdminName(
							datEmpPersonalDetailBO.getEmpFirstName() + " " + datEmpPersonalDetailBO.getEmpLastName());
					if(bo.getFkEmpRoleId().shortValue() == systemAdminProcurementRole.shortValue())
					{
						sysAdminSettingsOutputBean.setSettingType("ENABLE");
					}else{
						sysAdminSettingsOutputBean.setSettingType("DISABLE");
					}					
					finalList.add(sysAdminSettingsOutputBean);
					LOG.info("finalList" + finalList);

				}
			} catch (Exception e) {
				throw new DataAccessException("Failed to Save : ", e);

			}
		} catch (Exception e) {
			throw new DataAccessException("Error occured in updateAccessBySysAdmin");
		}
		LOG.endUsecase("updateAccessBySysAdmin");
		return finalList;
	}
	
	

	
}
