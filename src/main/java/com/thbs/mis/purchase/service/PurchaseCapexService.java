package com.thbs.mis.purchase.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.dao.BusinessUnitRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.report.Util.DownloadFileUtil;
import com.thbs.mis.gsr.service.GSRGoalService;
import com.thbs.mis.notificationframework.puchasenotification.service.PurchaseNotificationService;
import com.thbs.mis.purchase.bean.ClientAndProjectDetailsFromCompIdBean;
import com.thbs.mis.purchase.bean.ClientAndProjectDetailsInputBean;
import com.thbs.mis.purchase.bean.CreatePDFForCapexReportBean;
import com.thbs.mis.purchase.bean.CreatePDFForCapexSharedReportBean;
import com.thbs.mis.purchase.bean.PurchaseAssetRequestDetailOutputBean;
import com.thbs.mis.purchase.bean.PurchaseCapexDetailsOutputBean;
import com.thbs.mis.purchase.bean.PurchaseCapexOutputBean;
import com.thbs.mis.purchase.bean.PurchaseGetCapexRequestBean;
import com.thbs.mis.purchase.bean.PurchaseInputBean;
import com.thbs.mis.purchase.bean.PurchasePOCreateBean;
import com.thbs.mis.purchase.bean.PurchasePOCreatePhase2Bean;
import com.thbs.mis.purchase.bean.PurchaseQuotesOutputBean;
import com.thbs.mis.purchase.bean.PurchaseRejectCapexInputBean;
import com.thbs.mis.purchase.bean.PurchaseUpdateCapexInputBean;
import com.thbs.mis.purchase.bean.PurchaseViewCapexRequestsByDateRangeAndBuIdInputBean;
import com.thbs.mis.purchase.bean.PurchaseViewCapexRequestsByDateRangeAndBuIdOutputBean;
import com.thbs.mis.purchase.bean.ViewAllAssetRequestBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetQuoteBO;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;
import com.thbs.mis.purchase.bo.PurchaseDatBuBO;
import com.thbs.mis.purchase.bo.PurchaseDatCapexBO;
import com.thbs.mis.purchase.dao.AssetRequestSpecification;
import com.thbs.mis.purchase.dao.PurchaseAssetQuoteRepository;
import com.thbs.mis.purchase.dao.PurchaseAssetRequestRepository;
import com.thbs.mis.purchase.dao.PurchaseCapexRepository;
import com.thbs.mis.purchase.dao.PurchaseDatBuRepository;

@SuppressWarnings("deprecation")
@Service
public class PurchaseCapexService {

	private static final AppLog LOG = LogFactory.getLog(PurchaseCapexService.class);

	@Autowired
	private PurchaseAssetQuoteRepository assetQuoteRepository;

	@Autowired
	private PurchaseCapexRepository capexRepository;

	@Autowired
	private PurchaseAssetRequestRepository purchaseAssetRequestRepo;

	@Autowired
	private BusinessUnitRepository businessUnitRepository;
	
	@Autowired
	private GSRGoalService gsrGoalService;
	
	@Autowired 
	private PurchaseDatBuRepository purchaseDatBuRepo;
	
	@Autowired
	private EmpDetailRepository empDetailRepo;
		
	@Value("${request.capexApprovedByBuHead.status}")
	private Integer requestCapexApprovedByBuHeadStatus;

	@Value("${purchase.capex.rejectByBuHead.status}")
	private int purchaseCapexRejectByBuHeadStatus;
	
	@Value("${purchase.capex.createdBySysAdmin}")
	private int purchaseCapexCreatedBySysAdminStatus;	

	@Value("${purchase.quote.status.agree}")
	private String purchaseQuoteStatusAgree;

	@Value("${purchase.quote.status.disagree}")
	private String purchaseQuoteStatusDisAgree;

	@Value("${purchase.capex.createdBySysAdmin}")
	private Integer purchaseCreateCapexBySysAdmin;
	
	@Value("${purchase.capex.rejectedBySysAdmin}")
	private Integer purchaseRejectCapexBySysAdmin;

	@Value("${purchase.uploaded}")
	private String purchaseCapexUploaded;
	
	@Value("${purchase.file.basepath}")
	private String purchaseDocsBasePath;
	
	@Value("${purcahse.capex.shared.templates.basepath}")
	private String capexSharedTemplatePath;
	
	@Value("${nbu.it.business.unit.id}")
	private short nbuITBusinessUnitId;
	
	@Value("${bu.role.id}")
	private short buRoleId;
	
	@Value("${nbu.it.component.name}")
	private String nbuItComponentName;
	
	@Value("${nbu.it.project.name}")
	private String nbuItProjectName;
	
	@Value("${purchase.po.createdBySysAdmin}")
	private Integer purchasePoCreatedBySysAdmin;
	
	@Value("${request.poApprovedByFinance.status}")
	private Integer purchasePoApprovedByFinance;
	
	@Value("${request.poRejectedByFinance.status}")
	private Integer purchasePoRejectedByFinance;
	
	@Value("${purchase.assetRequest.closed}")
	private Integer purchaseAssetRequestClosed;
	
	@Value("${purchase.po.rejectedBySysAdmin}")
	private Integer purchasePoRejectedBySysAdmin;
	
	@Value("${purchase.nbuit.bu.unit.id}")
	private Short purchaseNbuItUnitId;
	
	@Value("${request.approvedByRm.status}")
	private Integer purchaseRequestApprovedByRm;
	
	@Value("${systemseniorAdminRoles}")
	private Integer seniorSystemAdmin;
	
	@Value ("${systemAdminRoles}")
	private Integer systemAdmin;
	
	@Value("${systemAdminProcurementRoleId}")
	private Integer systemAdminProcurment;	
	
	@Value("${purchase.capex.templates.basepath}")
	private String capexTemplateBasePath;
	
	@Value("${purchase.add.asset.systemAdminRoles}")
	private String sysAdminRoles;
	
	@Value("${purchase.nbuit.bu.unit.id}")
	private short itBuId;

	@Value("${purchase.finance.bu.id}")
	private short financeBuId;
	
	@Value("${legacy.service.url}")
	private String purchaseLegacyUrl;
	
	@Value("${request.waitingRmApproval.status}")
	private Integer requestWaitingRmApprovalStatus;
	
	@Value("${purchase.request.type.new.asset.request}")
	private Integer purchaseNewAssetRequestType;
	
	
	//Added by Kamal Anand for Notifications
	@Autowired
	PurchaseNotificationService notificationService;
	
	@Autowired
	private EmpDetailRepository empDetailRepository;
	
	@Value("${system.admin.procurement.roleId}")
	String sysAdminProcurementRoleId;
	//End of Addition by Kamal Anand for notifications

	public List<PurchaseCapexOutputBean> getAllCapexRequests(
			ViewAllAssetRequestBean assetReqBean) throws DataAccessException, CommonCustomException {
		LOG.entering("getAllCapexRequests service method");
		List<PurchaseDatAssetRequestBO> listOfAssetRequest = new ArrayList<PurchaseDatAssetRequestBO>();
		List<PurchaseCapexOutputBean> listOfAssetRequestBean = new ArrayList<PurchaseCapexOutputBean>();
		try {
			Specification<PurchaseDatAssetRequestBO> assetRequestSpecification = AssetRequestSpecification
					.getAllAssetRequests(assetReqBean);

			listOfAssetRequest = purchaseAssetRequestRepo
					.findAll(assetRequestSpecification);
			
	
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception occured at service method getAllCapexRequests ",
					e);
		}

		PurchaseCapexOutputBean capexReqOutputBean;
		if(listOfAssetRequest.size() > 0){
		for (PurchaseDatAssetRequestBO assetReqBo : listOfAssetRequest) {
			capexReqOutputBean = new PurchaseCapexOutputBean();
		
			if(assetReqBo.getPurchaseRequestStatus() == requestCapexApprovedByBuHeadStatus
					|| assetReqBo.getPurchaseRequestStatus() == purchasePoCreatedBySysAdmin
					|| assetReqBo.getPurchaseRequestStatus() == purchasePoRejectedBySysAdmin
					|| assetReqBo.getPurchaseRequestStatus() == purchasePoApprovedByFinance
					|| assetReqBo.getPurchaseRequestStatus() == purchasePoRejectedByFinance
					|| assetReqBo.getPurchaseRequestStatus() == purchaseAssetRequestClosed){
			/*capexReqOutputBean.setCapexNumber(assetReqBo
					.getPurchaseDatCapexBO().getCapexNo());*/
			capexReqOutputBean.setBusinessUnit(assetReqBo.getMasBuUnitBO()
					.getBuUnitName());
			capexReqOutputBean.setRaisedOn(assetReqBo.getPurchaseDatCapexBO()
					.getCreatedOn());
			capexReqOutputBean.setPoDetails(assetReqBo.getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());
			//capexReqOutputBean.setProjectName(assetReqBo.getProjectName());
			
			
			// get component & client details from legacy service
						ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean(); 
						clientProjCompDetailsBean.setClientId( assetReqBo.getClientId());
						clientProjCompDetailsBean.setProjectId( assetReqBo.getProjectId());
						clientProjCompDetailsBean.setComponentId(assetReqBo.getProjectComponentId());
						clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
									
						PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
						ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService.
								getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
									
						if(clientProjectCompDetails == null){
							throw new CommonCustomException("Unable to fetch project, client and component details from legacy MIS.");
						}else{
							capexReqOutputBean.setProjectName(clientProjectCompDetails.getProjectName());
						}
						capexReqOutputBean.setAssetRequestId(assetReqBo.getPkPurchaseRequestId());
			capexReqOutputBean.setStatus(assetReqBo
					.getPurchaseMasRequestStatusBO()
					.getPurchaseRequestStatusName().trim());
			capexReqOutputBean.setStatusId(assetReqBo.getPurchaseRequestStatus());
			listOfAssetRequestBean.add(capexReqOutputBean);
			}
			
		}
		}
		LOG.exiting("getAllCapexRequests service method");
		return listOfAssetRequestBean;

	}

	public PurchaseGetCapexRequestBean getCapexRequests(Integer assetReqId)
			throws DataAccessException {

		PurchaseDatAssetRequestBO assetRequestBo = new PurchaseDatAssetRequestBO();
		PurchaseGetCapexRequestBean assetReqBean = new PurchaseGetCapexRequestBean();
		PurchaseDatAssetQuoteBO purchaseDatAssetQuote = new PurchaseDatAssetQuoteBO();
		List<PurchaseDatAssetQuoteBO> listOfAssetQuote = new ArrayList<PurchaseDatAssetQuoteBO>();
		MasBuUnitBO itBuDetail = null;
		try {

			assetRequestBo = purchaseAssetRequestRepo
					.findByPkPurchaseRequestId(assetReqId);
			purchaseDatAssetQuote = assetQuoteRepository.getAgreeQuoteDetails(assetReqId);

			listOfAssetQuote = assetQuoteRepository
					.findByFkPurchaseRequestId(assetReqId);

			Double finalCost = null;
			if (listOfAssetQuote.size() > 0) {
				for (PurchaseDatAssetQuoteBO quote : listOfAssetQuote) {
					if (quote.getQuoteStatus().equals(purchaseQuoteStatusAgree)) {
						finalCost = quote.getTotalCost();
					}
				}
			}

			if (assetRequestBo != null) {
				assetReqBean.setPriority(assetRequestBo.getPriority());
				assetReqBean.setApprovedQuoteFinalcost(finalCost);
				assetReqBean.setAssetNameAndVersion(assetRequestBo
						.getPurchaseMasAssetsBO()
						.getAssetNameAndVersionOrModel());
				assetReqBean.setBriefDescription(assetRequestBo
						.getBriefDescription());
				assetReqBean.setBuName(assetRequestBo.getMasBuUnitBO()
						.getBuUnitName());
				assetReqBean.setCapexNumber(assetRequestBo
						.getPurchaseDatCapexBO().getCapexNo());
				assetReqBean.setCreatedDate(assetRequestBo.getCreatedDate());
				assetReqBean.setIsClientReimbursing(assetRequestBo
						.getIsClientReimbursing());
				assetReqBean.setIsSoftwareOrHardwarePurchased(assetRequestBo
						.getIsSoftwareOrHardwarePurchased());
				assetReqBean.setLocation(assetRequestBo
						.getMasLocationParentBO().getLocationParentName());
				assetReqBean.setPkPurchaseRequestId(assetRequestBo
						.getPkPurchaseRequestId());
				assetReqBean.setSystemAdminComments(assetRequestBo.getSysAdminComment());
				assetReqBean.setFkCurrencyType(purchaseDatAssetQuote.getFkCurrencyType());
				//Added by pratibha
				assetReqBean.setRequestorName(
						gsrGoalService.getEmpNameByEmpId(assetRequestBo.getRequestorId().intValue()) + '-'+ assetRequestBo.getRequestorId());
			
				if(assetRequestBo.getRmToApprove()!=null)
					assetReqBean.setReportingManager(
							gsrGoalService.getEmpNameByEmpId(assetRequestBo.getRmToApprove().intValue()) + '-'+ assetRequestBo.getRmToApprove());
				
				if(assetRequestBo.getSysAdminId()!=null)	
					assetReqBean.setSysAdminName(
							gsrGoalService.getEmpNameByEmpId(assetRequestBo.getSysAdminId().intValue()) + '-'+assetRequestBo.getSysAdminId());
				
				if(assetRequestBo.getFinanceEx_updated_by()!=null)
					assetReqBean.setFinanceHeadName(
							gsrGoalService.getEmpNameByEmpId(assetRequestBo.getFinanceEx_updated_by().intValue()) + '-'+assetRequestBo.getFinanceEx_updated_by());
				
				if(assetRequestBo.getFinanceEx_comment() != null)
					assetReqBean.setFinanceExComment(assetRequestBo.getFinanceEx_comment());
				
		 if(purchaseDatAssetQuote != null)
			 
		   {
				if(purchaseDatAssetQuote.getCostBorneBy().equalsIgnoreCase("shared"))
				{
					 itBuDetail = businessUnitRepository.findByPkBuUnitId(itBuId);
						
					 assetReqBean.setBuHeadName(
								gsrGoalService.getEmpNameByEmpId(assetRequestBo.getMasBuUnitBO().getFkBuHeadEmpId())+ '-'+assetRequestBo.getMasBuUnitBO().getFkBuHeadEmpId());
					 
					if(assetRequestBo.getMasBuUnitBO().getFkBuHeadEmpId()!= itBuDetail.getFkBuHeadEmpId()){
					assetReqBean.setItHeadName(
							gsrGoalService.getEmpNameByEmpId(itBuDetail.getFkBuHeadEmpId()) + '-'+itBuDetail.getFkBuHeadEmpId());
					 }
				}
				
				if(purchaseDatAssetQuote.getCostBorneBy().equalsIgnoreCase("bu"))
				{
					assetReqBean.setBuHeadName(
							gsrGoalService.getEmpNameByEmpId(assetRequestBo.getMasBuUnitBO().getFkBuHeadEmpId())+ '-'+assetRequestBo.getMasBuUnitBO().getFkBuHeadEmpId());
				}
				
				if(purchaseDatAssetQuote.getCostBorneBy().equalsIgnoreCase("IT"))
				{
					 itBuDetail = businessUnitRepository.findByPkBuUnitId(itBuId);
						
					assetReqBean.setItHeadName(
							gsrGoalService.getEmpNameByEmpId(itBuDetail.getFkBuHeadEmpId()) + '-'+itBuDetail.getFkBuHeadEmpId());
				}
			 }
				//EOA pratibha
				//assetReqBean.setProjectName(assetRequestBo.getProjectName());
				
				// get component & client details from legacy service
				ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean(); 
				clientProjCompDetailsBean.setClientId( assetRequestBo.getClientId());
				clientProjCompDetailsBean.setProjectId( assetRequestBo.getProjectId());
				clientProjCompDetailsBean.setComponentId(assetRequestBo.getProjectComponentId());
				clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
							
				PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
				ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService.
						getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
							
				if(clientProjectCompDetails == null){
					throw new CommonCustomException("Unable to fetch project, client and component details from legacy MIS.");
				}else{
					assetReqBean.setProjectName(clientProjectCompDetails.getProjectName());
				}
				
				assetReqBean.setPurchaseRequestStatus(assetRequestBo
						.getPurchaseRequestStatus());
				
				PurchaseDatAssetQuoteBO purchaseDatAssetQuoteBO = new PurchaseDatAssetQuoteBO();
				purchaseDatAssetQuoteBO = assetQuoteRepository
						.findByFkPurchaseRequestIdAndQuoteStatus(assetReqId,"AGREE");
			
				if (purchaseDatAssetQuoteBO !=null) {
					
					PurchaseQuotesOutputBean quoteBean = new PurchaseQuotesOutputBean();
						quoteBean
								.setQuoteId(purchaseDatAssetQuoteBO.getPkQuoteId());
						quoteBean.setQuoteStatus(purchaseDatAssetQuoteBO
								.getQuoteStatus());
						quoteBean.setTotalCost(purchaseDatAssetQuoteBO
								.getTotalCost());
						quoteBean.setBuContribution(purchaseDatAssetQuoteBO.getBuContribution());
						quoteBean.setCostBorneBy(purchaseDatAssetQuoteBO.getCostBorneBy());
						quoteBean.setDeliveryDate(purchaseDatAssetQuoteBO.getDeliveryDate());
						quoteBean.setIndividualLicenseCost(purchaseDatAssetQuoteBO.getIndividualLicenseCost());
						quoteBean.setVendorName(purchaseDatAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorName());
						quoteBean.setVendorAddress(purchaseDatAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorAddress());
						quoteBean.setVendorContactNumber(purchaseDatAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorContactNo());
						quoteBean.setCurrencyType(purchaseDatAssetQuoteBO.getFkCurrencyType()); //Added by prathibha for fetch service
						quoteBean.setSupplierAddressId(purchaseDatAssetQuoteBO.getFkSupplierAddress());
						quoteBean.setVendorQuoteReferenceNumber(purchaseDatAssetQuoteBO.getVendorQuoteReferenceNo());
						assetReqBean.setQuote(quoteBean);
				}

			}else{
				throw new CommonCustomException("The asset request Id does't exist, please pass proper id");
			}

			LOG.exiting("getCapexRequests service method");

		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}

		return assetReqBean;

	}

	// Added by Pratibha TR

	/**
	 * 
	 * @param assetRequestId
	 * @return PurchaseDatAssetRequestBO
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to approve capex.
	 */
	public boolean approveCapex(PurchaseInputBean approveCapexBean)
			throws CommonCustomException {

		LOG.startUsecase("Approve capex");
		PurchaseDatAssetRequestBO purchaseDatAssetRequestDetails = new PurchaseDatAssetRequestBO();
		PurchaseDatAssetQuoteBO approvedQuoteBO = new PurchaseDatAssetQuoteBO();
		//PurchaseApproveCapexOutputBean capexOutputBean = new PurchaseApproveCapexOutputBean();
		MasBuUnitBO masBuUnitBo = new MasBuUnitBO();
		MasBuUnitBO nbuITBO = new MasBuUnitBO();
		boolean successFlag = false;
		
		try {
			approvedQuoteBO = assetQuoteRepository.getAgreeQuoteDetails(approveCapexBean.getId());
			purchaseDatAssetRequestDetails = purchaseAssetRequestRepo.findByPkPurchaseRequestId(approveCapexBean.getId());
		} catch (Exception ex) {
			throw new CommonCustomException("Failed to fetch Quote details from db.");
		}
		if(purchaseDatAssetRequestDetails == null)
		{
			throw new CommonCustomException("Asset request does not exixt.");
		}
		if (approvedQuoteBO == null) {
			throw new CommonCustomException("Selected asset request doesn't have default quote.");
		}
		else if (approvedQuoteBO.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() == 
				requestCapexApprovedByBuHeadStatus.intValue()) 
		{
			throw new CommonCustomException("Capex is already approved.");
		}
		else if(approvedQuoteBO.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() == 
				purchaseCapexCreatedBySysAdminStatus)
		{
			try {
				masBuUnitBo = businessUnitRepository
						.findByPkBuUnitId(approvedQuoteBO.getPurchaseDatAssetRequestBO().getFkBuId());
				nbuITBO = businessUnitRepository.findByPkBuUnitId(nbuITBusinessUnitId);
				//get all more BU object for NBU-IT
			} catch (Exception e) {
				throw new CommonCustomException("BU Head ID does not exist.");
			}
			
			//LOG.info("masBuUnitBo ::: "  + masBuUnitBo);
			//LOG.info("nbuITBO ::: " + nbuITBO);
			//LOG.info("approveCapexBean.getLoggedInUser().intValue() ::: " 
			//		+ approveCapexBean.getLoggedInUser().intValue());
			if(approveCapexBean.getLoggedInUser().intValue() == masBuUnitBo.getFkBuHeadEmpId() ||
					approveCapexBean.getLoggedInUser().intValue() == nbuITBO.getFkBuHeadEmpId() ||
					masBuUnitBo.getFkBuHeadEmpId() == nbuITBO.getFkBuHeadEmpId())
			{		
				boolean buHeadFlag = false;
				boolean nbuITHeadFlag = false;
				boolean sameBuHeadIdInBUAndNbuIT = false;
				
				//Check loggedInUser is BU Head or NBU-IT Head? 
				List<PurchaseDatBuBO> purchaseDatBuBoList = new ArrayList<PurchaseDatBuBO>();
				try{
					purchaseDatBuBoList = purchaseDatBuRepo.getAllPurchaseDatBuRecords(
							approveCapexBean.getId());
				}catch(Exception ex){
					throw new CommonCustomException("Failed to retrieve BU head details.");
				}
				
				for (PurchaseDatBuBO purchaseDatBuBO : purchaseDatBuBoList) 
				{
					if(approveCapexBean.getLoggedInUser().intValue() == masBuUnitBo.getFkBuHeadEmpId() &&
							approveCapexBean.getLoggedInUser().intValue() == nbuITBO.getFkBuHeadEmpId() &&
							masBuUnitBo.getPkBuUnitId() == nbuITBusinessUnitId &&
							purchaseDatBuBO.getBuHeadId() == approveCapexBean.getLoggedInUser().intValue() &&
							purchaseDatBuBO.getMasBuUnitBO().getPkBuUnitId() == nbuITBusinessUnitId)
					{
						sameBuHeadIdInBUAndNbuIT = true;
						break;
					}
					else if(approveCapexBean.getLoggedInUser().intValue() == masBuUnitBo.getFkBuHeadEmpId() &&
							approveCapexBean.getLoggedInUser().intValue() == nbuITBO.getFkBuHeadEmpId() &&
							masBuUnitBo.getPkBuUnitId() != nbuITBusinessUnitId &&
							purchaseDatBuBO.getBuHeadId() == approveCapexBean.getLoggedInUser().intValue() &&
							purchaseDatBuBO.getMasBuUnitBO().getPkBuUnitId() != nbuITBusinessUnitId)
					{
						sameBuHeadIdInBUAndNbuIT = true;
						break;
					}else if(approveCapexBean.getLoggedInUser().intValue() == masBuUnitBo.getFkBuHeadEmpId()  && 
							approveCapexBean.getLoggedInUser().intValue() != nbuITBO.getFkBuHeadEmpId() && 
							masBuUnitBo.getPkBuUnitId() != nbuITBusinessUnitId && 
							purchaseDatBuBO.getBuHeadId() == approveCapexBean.getLoggedInUser().intValue() &&
							purchaseDatBuBO.getMasBuUnitBO().getPkBuUnitId() != nbuITBusinessUnitId) 
					{
						buHeadFlag = true;
						break;
					}else if(approveCapexBean.getLoggedInUser().intValue() == masBuUnitBo.getFkBuHeadEmpId()  && 
							approveCapexBean.getLoggedInUser().intValue() == nbuITBO.getFkBuHeadEmpId() && 
							masBuUnitBo.getPkBuUnitId() == nbuITBusinessUnitId && 
							purchaseDatBuBO.getBuHeadId() == approveCapexBean.getLoggedInUser().intValue() &&
							purchaseDatBuBO.getMasBuUnitBO().getPkBuUnitId() == nbuITBusinessUnitId)
					{//For When request raised by IT.
						nbuITHeadFlag = true;
						break;
					}else if(approveCapexBean.getLoggedInUser().intValue() != masBuUnitBo.getFkBuHeadEmpId()  && 
							approveCapexBean.getLoggedInUser().intValue() == nbuITBO.getFkBuHeadEmpId() && 
							masBuUnitBo.getPkBuUnitId() != nbuITBusinessUnitId && 
							purchaseDatBuBO.getBuHeadId() == approveCapexBean.getLoggedInUser().intValue() &&
							purchaseDatBuBO.getMasBuUnitBO().getPkBuUnitId() == nbuITBusinessUnitId)
					{//This condition occurs when NBU-IT head try to approve the shared capex. 
						nbuITHeadFlag = true;
						break;
						//throw new CommonCustomException("Neither you are a BU Head nor IT Head of the requestor, hence it is restricting you to proceed.");
					}else if(approveCapexBean.getLoggedInUser().intValue() == masBuUnitBo.getFkBuHeadEmpId()  && 
							approveCapexBean.getLoggedInUser().intValue() == nbuITBO.getFkBuHeadEmpId() && 
							masBuUnitBo.getPkBuUnitId() != nbuITBusinessUnitId && 
							purchaseDatBuBO.getBuHeadId() == approveCapexBean.getLoggedInUser().intValue() &&
							purchaseDatBuBO.getMasBuUnitBO().getPkBuUnitId() == nbuITBusinessUnitId)
					{//This condition occurs when NBU-IT head try to approve the IT capex. 
						nbuITHeadFlag = true;
						break;
					}
				}
				
				if(sameBuHeadIdInBUAndNbuIT)
				{		
				}else if(buHeadFlag){
				}else if(nbuITHeadFlag){
				}else{
					if(approvedQuoteBO.getCostBorneBy().equalsIgnoreCase("SHARED")){
						throw new CommonCustomException("Neither you are a BU Head of the requestor nor NBU-IT Head, hence you are restricted to approve this capex.");
					}else if(approvedQuoteBO.getCostBorneBy().equalsIgnoreCase("BU"))
					{
						throw new CommonCustomException("You are not a BU Head of the requestor, hence you are restricted to approve this capex.");
					}else{
						throw new CommonCustomException("You are not a NBU-IT Head , hence you are restricted to approve this capex.");
					}
				}
		        /*LOG.info("sameBuHeadIdInBUAndNbuIT : " + sameBuHeadIdInBUAndNbuIT);
				LOG.info("buHeadFlag : " + buHeadFlag);
				LOG.info("nbuITHeadFlag : " + nbuITHeadFlag);*/
				if(approvedQuoteBO.getCostBorneBy().equalsIgnoreCase("SHARED"))
				{	
					boolean buHeadApprovalFlag = false;
					if(sameBuHeadIdInBUAndNbuIT)
					{						
						PurchaseDatBuBO purchaseDatBuBO = purchaseDatBuBoList.get(0); //Only have one record
						
						if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
						{
							throw new CommonCustomException("This capex has been already rejected.");
						}
						else if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("AGREE"))
						{
							throw new CommonCustomException("This capex has been already approved.");
						}else if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("WAITING FOR APPROVAL"))
						{
							purchaseDatBuBO.setFkPurchaseDatBuStatus("AGREE");
							purchaseDatBuBO.setModifiedBy(approveCapexBean.getLoggedInUser());
							purchaseDatBuBO.setModifiedDate(new Date());
							purchaseDatBuRepo.save(purchaseDatBuBO);
						
							purchaseDatAssetRequestDetails.setPurchaseRequestStatus(requestCapexApprovedByBuHeadStatus);
							purchaseDatAssetRequestDetails.setModifiedBy(approveCapexBean.getLoggedInUser());
							purchaseDatAssetRequestDetails.setModifiedOn(new Date());
							purchaseAssetRequestRepo.save(purchaseDatAssetRequestDetails);
							//Added by Kamal Anand for Notifications
							try{
								notificationService.setCapexApprovedNotificationToEmployeeScreen(purchaseDatAssetRequestDetails.getRequestorId());
								notificationService.setCapexCreatedNotificationToBUHeadScreen(purchaseDatAssetRequestDetails.getPkPurchaseRequestId());
								List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
								if(empDetail != null)
								{
									empDetail.forEach(emp -> {
										try {
											notificationService.setCapexApprovedNotificationToSysAdminScreen(emp.getPkEmpId());
										} catch (Exception e) {
											LOG.info("Exception occured while setting notification in Update and Approve Asset Request");
										}
									});
								}
							} catch(Exception e) {
								LOG.info("Exception occurred while setting Capex Approved by BU Head Notification");
							}
							successFlag = true;
						}
						//return successFlag;
					}else if(buHeadFlag)
					{
						for (PurchaseDatBuBO purchaseDatBuBO : purchaseDatBuBoList) 
						{
							/*LOG.info("purchaseDatBuBO ::: " + purchaseDatBuBO);
							LOG.info("Logged in User :: " + approveCapexBean.getLoggedInUser().intValue());
							LOG.info("BU head id ::: " + masBuUnitBo.getFkBuHeadEmpId());*/
							//if(approveCapexBean.getLoggedInUser().intValue() == masBuUnitBo.getFkBuHeadEmpId())
							if(purchaseDatBuBO.getBuHeadId() == Integer.valueOf(masBuUnitBo.getFkBuHeadEmpId()).shortValue())
							{
								/*LOG.info("Inside if purchaseDatBuBO ::: " + purchaseDatBuBO);*/
								
								if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
								{
									throw new CommonCustomException("This capex has been already rejected.");
								}
								else if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("AGREE"))
								{
									throw new CommonCustomException("This capex has been already approved.");
								}
								else if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("WAITING FOR APPROVAL"))
								{
									purchaseDatBuBO.setFkPurchaseDatBuStatus("AGREE");
									purchaseDatBuBO.setModifiedBy(approveCapexBean.getLoggedInUser());
									purchaseDatBuBO.setModifiedDate(new Date());
									purchaseDatBuRepo.save(purchaseDatBuBO);
									successFlag = true;
									break;
								}
							}
						}//end of for loop
						
						//return successFlag;
					}else if(nbuITHeadFlag)
					{
						for (PurchaseDatBuBO purchaseDatBuBO : purchaseDatBuBoList) 
						{
							if(approveCapexBean.getLoggedInUser().intValue() == nbuITBO.getFkBuHeadEmpId())
							{
								if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
								{
									throw new CommonCustomException("This capex has been already rejected.");
								}
								else if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("AGREE"))
								{
									if(purchaseDatBuBO.getBuHeadId() == nbuITBO.getFkBuHeadEmpId())
										throw new CommonCustomException("This capex has been already approved.");
									else
										buHeadApprovalFlag = true;
								}
								else if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("WAITING FOR APPROVAL"))
								{
									//To check whether BU Head is approved or not
									for (PurchaseDatBuBO purchaseDatBuBOTempObj : purchaseDatBuBoList) 
									{
										if(masBuUnitBo.getPkBuUnitId() == purchaseDatBuBOTempObj.getFkMasBuId() 
												&& purchaseDatBuBOTempObj.getFkPurchaseDatBuStatus().equalsIgnoreCase("AGREE"))
										{
											buHeadApprovalFlag = true;
											break;
										}
									}
									
									if(buHeadApprovalFlag){
										purchaseDatBuBO.setFkPurchaseDatBuStatus("AGREE");
										purchaseDatBuBO.setModifiedBy(approveCapexBean.getLoggedInUser());
										purchaseDatBuBO.setModifiedDate(new Date());
										purchaseDatBuRepo.save(purchaseDatBuBO);
										
										purchaseDatAssetRequestDetails.setPurchaseRequestStatus(requestCapexApprovedByBuHeadStatus);
										purchaseDatAssetRequestDetails.setModifiedBy(approveCapexBean.getLoggedInUser());
										purchaseDatAssetRequestDetails.setModifiedOn(new Date());
										purchaseAssetRequestRepo.save(purchaseDatAssetRequestDetails);
										successFlag = true;
										
										//Added by Kamal Anand for Notifications
										try{
											notificationService.setCapexApprovedNotificationToEmployeeScreen(purchaseDatAssetRequestDetails.getRequestorId());
											notificationService.setCapexCreatedNotificationToBUHeadScreen(purchaseDatAssetRequestDetails.getPkPurchaseRequestId());
											List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
											if(empDetail != null)
											{
												empDetail.forEach(emp -> {
													try {
														notificationService.setCapexApprovedNotificationToSysAdminScreen(emp.getPkEmpId());
													} catch (Exception e) {
														LOG.info("Exception occured while setting notification in Update and Approve Asset Request");
													}
												});
											}
										} catch(Exception e) {
											LOG.info("Exception occurred while setting Capex Approved by BU Head Notification");
										}
										//End of Addition by Kamal Anand for Notifications
										
									}else{
										throw new CommonCustomException("BU head approval is pending for this capex request.");
									}
									
									/*if(purchaseDatBuBO.getBuHeadId() != nbuITBO.getFkBuHeadEmpId())
									{
										throw new CommonCustomException("BU head approval is pending for this capex request.");
									}else{
										purchaseDatBuBO.setFkPurchaseDatBuStatus("AGREE");
										purchaseDatBuBO.setModifiedBy(approveCapexBean.getLoggedInUser());
										purchaseDatBuBO.setModifiedDate(new Date());
										purchaseDatBuRepo.save(purchaseDatBuBO);
										successFlag = true;
										
										if(buHeadApprovalFlag)
										{*/
											/*purchaseDatAssetRequestDetails.setPurchaseRequestStatus(requestCapexApprovedByBuHeadStatus);
											purchaseDatAssetRequestDetails.setModifiedBy(approveCapexBean.getLoggedInUser());
											purchaseDatAssetRequestDetails.setModifiedOn(new Date());
											purchaseAssetRequestRepo.save(purchaseDatAssetRequestDetails);
											
											//Added by Kamal Anand for Notifications
											try{
											notificationService.setCapexApprovedNotificationToEmployeeScreen(purchaseDatAssetRequestDetails.getRequestorId());
											notificationService.setCapexCreatedNotificationToBUHeadScreen(purchaseDatAssetRequestDetails.getPkPurchaseRequestId());
											List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
											if(empDetail != null)
											{
												empDetail.forEach(emp -> {
													try {
														notificationService.setCapexApprovedNotificationToSysAdminScreen(emp.getPkEmpId());
													} catch (Exception e) {
														LOG.info("Exception occured while setting notification in Update and Approve Asset Request");
													}
												});
											}
											} catch(Exception e) {
												LOG.info("Exception occurred while setting Capex Approved by BU Head Notification");
											}*/
											//End of Addition by Kamal Anand for Notifications
										/*}
									}*/
									//return successFlag;
								}
							}
						}//end of for loop
					}else{
						throw new CommonCustomException("Neither you have a role of BU Head nor IT Head. Kindly get in touch with Nucleus Support team.");
					}
				}else if(approvedQuoteBO.getCostBorneBy().equalsIgnoreCase("BU"))
				{
					PurchaseDatBuBO purchaseDatBuBo = new PurchaseDatBuBO();
					try{
						purchaseDatBuBo = purchaseDatBuRepo.getPurchaseDatBuObject(approveCapexBean.getId());
					}catch(Exception ex){
						throw new CommonCustomException("Unable to get business unit head details from backend. Kindly get in touch with Nucleus Support team.");
					}
					
					if(purchaseDatBuBo == null){
						throw new CommonCustomException("Failed to get business unit head details from backend. Kindly get in touch with Nucleus Support team.");
					}
					
					if(sameBuHeadIdInBUAndNbuIT || buHeadFlag)
					{
						if(purchaseDatBuBo.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
						{
							throw new CommonCustomException("This capex has been already rejected.");
						}
						else if(purchaseDatBuBo.getFkPurchaseDatBuStatus().equalsIgnoreCase("AGREE"))
						{
							throw new CommonCustomException("This capex has been already approved.");
						}
						else if(purchaseDatBuBo.getFkPurchaseDatBuStatus().equalsIgnoreCase("WAITING FOR APPROVAL"))
						{
							purchaseDatBuBo.setFkPurchaseDatBuStatus("AGREE");
							purchaseDatBuBo.setModifiedBy(approveCapexBean.getLoggedInUser());
							purchaseDatBuBo.setModifiedDate(new Date());
							purchaseDatBuRepo.save(purchaseDatBuBo);
							
							purchaseDatAssetRequestDetails.setPurchaseRequestStatus(requestCapexApprovedByBuHeadStatus);
							purchaseDatAssetRequestDetails.setModifiedBy(approveCapexBean.getLoggedInUser());
							purchaseDatAssetRequestDetails.setModifiedOn(new Date());
							purchaseAssetRequestRepo.save(purchaseDatAssetRequestDetails);
							successFlag = true;
							//Added by Kamal Anand for Notifications
							try{
							notificationService.setCapexApprovedNotificationToEmployeeScreen(purchaseDatAssetRequestDetails.getRequestorId());
							notificationService.setCapexCreatedNotificationToBUHeadScreen(purchaseDatAssetRequestDetails.getPkPurchaseRequestId());
							List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
							if(empDetail != null)
							{
								empDetail.forEach(emp -> {
									try {
										notificationService.setCapexApprovedNotificationToSysAdminScreen(emp.getPkEmpId());
									} catch (Exception e) {
										LOG.info("Exception occured while setting notification in Update and Approve Asset Request");
									}
								});
							}
							} catch(Exception e) {
								LOG.info("Exception occurred while setting Capex Approved by BU Head Notification");
							}
							//End of Addition by Kamal Anand for Notifications
						}
						//return successFlag;
					}else if(nbuITHeadFlag)
					{
						throw new CommonCustomException("This capex's cost is borne only by BU. Hence IT head approval is not required.");
					}
				}else if(approvedQuoteBO.getCostBorneBy().equalsIgnoreCase("IT"))
				{
					PurchaseDatBuBO purchaseDatBuBo1 = new PurchaseDatBuBO();
					try{
						purchaseDatBuBo1 = purchaseDatBuRepo.getPurchaseDatBuObject(approveCapexBean.getId());
					}catch(Exception ex){
						throw new CommonCustomException("Unable to get business unit head details from backend. Kindly get in touch with Nucleus Support team.");
					}
					
					if(purchaseDatBuBo1 == null){
						throw new CommonCustomException("Failed to get business unit head details from backend. Kindly get in touch with Nucleus Support team.");
					}
					
					if(sameBuHeadIdInBUAndNbuIT || nbuITHeadFlag)
					{
						if(purchaseDatBuBo1.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
						{
							throw new CommonCustomException("This capex has been already rejected.");
						}
						else if(purchaseDatBuBo1.getFkPurchaseDatBuStatus().equalsIgnoreCase("AGREE"))
						{
							throw new CommonCustomException("This capex has been already approved.");
						}
						else if(purchaseDatBuBo1.getFkPurchaseDatBuStatus().equalsIgnoreCase("WAITING FOR APPROVAL"))
						{
							purchaseDatBuBo1.setFkPurchaseDatBuStatus("AGREE");
							purchaseDatBuBo1.setModifiedBy(approveCapexBean.getLoggedInUser());
							purchaseDatBuBo1.setModifiedDate(new Date());
							purchaseDatBuRepo.save(purchaseDatBuBo1);
							
							purchaseDatAssetRequestDetails.setPurchaseRequestStatus(requestCapexApprovedByBuHeadStatus);
							purchaseDatAssetRequestDetails.setModifiedBy(approveCapexBean.getLoggedInUser());
							purchaseDatAssetRequestDetails.setModifiedOn(new Date());
							purchaseAssetRequestRepo.save(purchaseDatAssetRequestDetails);
							successFlag = true;
							//Added by Kamal Anand for Notifications
							try{
							notificationService.setCapexApprovedNotificationToEmployeeScreen(purchaseDatAssetRequestDetails.getRequestorId());
							notificationService.setCapexCreatedNotificationToBUHeadScreen(purchaseDatAssetRequestDetails.getPkPurchaseRequestId());
							List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
							if(empDetail != null)
							{
								empDetail.forEach(emp -> {
									try {
										notificationService.setCapexApprovedNotificationToSysAdminScreen(emp.getPkEmpId());
									} catch (Exception e) {
										LOG.info("Exception occured while setting notification in Update and Approve Asset Request");
									}
								});
							}
							} catch(Exception e) {
								LOG.info("Exception occurred while setting Capex Approved by BU Head Notification");
							}
							//End of Addition by Kamal Anand for Notifications
						}
						//return successFlag;
					}else if(buHeadFlag)
					{
						throw new CommonCustomException("This capex's cost is borne only by IT. Hence BU head approval is not required.");
					}
				}
			}else{
				throw new CommonCustomException("Neither you are a BU Head nor IT Head, hence it is restricting you to access.");
			}
		}else{
			throw new CommonCustomException("In order to approve Capex, capex should be created by System Admin"); 
		}
		LOG.endUsecase("Approve capex");
		return successFlag;
	
	}

	// EOA by pratibha TR

	// Added by Smrithi

	public PurchaseCapexDetailsOutputBean getCapexDetails(
			Integer pkPurchaseRequestId) throws CommonCustomException {

		LOG.startUsecase("Get Capex Details based on asset request ID.");
		PurchaseCapexDetailsOutputBean outputBean = new PurchaseCapexDetailsOutputBean();
		PurchaseDatAssetQuoteBO capexDetailsOutput = new PurchaseDatAssetQuoteBO();
		PurchaseDatAssetRequestBO assetOutput = new PurchaseDatAssetRequestBO();
		PurchaseDatAssetQuoteBO purchaseDatAssetQuote = new PurchaseDatAssetQuoteBO();
		MasBuUnitBO itBuDetail = null;
		try {
			capexDetailsOutput = assetQuoteRepository.getCapexDetailsByPurchaseRequestID(pkPurchaseRequestId.intValue());
			assetOutput = purchaseAssetRequestRepo.findByPkPurchaseRequestId(pkPurchaseRequestId.intValue());
			purchaseDatAssetQuote = assetQuoteRepository.getAgreeQuoteDetails(pkPurchaseRequestId);
		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch details from db.");
		}
		if(assetOutput == null){
			throw new CommonCustomException("ID does not exist.");

		}
		if (capexDetailsOutput == null) {
			throw new CommonCustomException("Selected asset request doesn't have default quote");
		}
		else if (capexDetailsOutput.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() == purchaseCapexCreatedBySysAdminStatus ||
				capexDetailsOutput.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() == purchaseRejectCapexBySysAdmin || 
				capexDetailsOutput.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() == requestCapexApprovedByBuHeadStatus || 
				capexDetailsOutput.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() == purchaseCapexRejectByBuHeadStatus ||
		        capexDetailsOutput.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() == purchasePoCreatedBySysAdmin ||
			    capexDetailsOutput.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() == purchasePoApprovedByFinance ||
			    capexDetailsOutput.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() == purchasePoRejectedByFinance ||
			    capexDetailsOutput.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() == purchaseAssetRequestClosed ||
			    capexDetailsOutput.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus().intValue() == purchasePoRejectedBySysAdmin)
		{ 
			outputBean.setPurchaseRequestId(capexDetailsOutput.getPurchaseDatAssetRequestBO().getPkPurchaseRequestId());
			outputBean.setPurchaseRequestStatus(capexDetailsOutput.getPurchaseDatAssetRequestBO().getPurchaseRequestStatus());
			outputBean.setPurchaseRequestStatusName(capexDetailsOutput.getPurchaseDatAssetRequestBO().getPurchaseMasRequestStatusBO().getPurchaseRequestStatusName());
			outputBean.setBuUnitName(capexDetailsOutput.getPurchaseDatAssetRequestBO().getMasBuUnitBO().getBuUnitName());
			outputBean.setCapexNo(capexDetailsOutput.getPurchaseDatAssetRequestBO().getPurchaseDatCapexBO().getCapexNo());
			outputBean.setAssetType(capexDetailsOutput.getPurchaseDatAssetRequestBO().getPurchaseMasAssetsBO().getAssetType());
			outputBean.setAssetNameAndVersionOrModel(capexDetailsOutput.getPurchaseDatAssetRequestBO().getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());
			outputBean.setSpecifications(capexDetailsOutput.getPurchaseDatAssetRequestBO().getSpecifications());
			outputBean.setLocationParentName(capexDetailsOutput.getPurchaseDatAssetRequestBO().getMasLocationParentBO()
					.getLocationParentName());
			outputBean.setFkCurrencyType(purchaseDatAssetQuote.getFkCurrencyType());
			//Added by pratibha
			outputBean.setRequestorName(
							gsrGoalService.getEmpNameByEmpId(assetOutput.getRequestorId().intValue()) + '-'+ assetOutput.getRequestorId());
				
					if(assetOutput.getRmToApprove()!=null)
						outputBean.setReportingManager(
								gsrGoalService.getEmpNameByEmpId(assetOutput.getRmToApprove().intValue()) + '-'+ assetOutput.getRmToApprove());
					
					if(assetOutput.getSysAdminId()!=null)	
						outputBean.setSysAdminName(
								gsrGoalService.getEmpNameByEmpId(assetOutput.getSysAdminId().intValue()) + '-'+assetOutput.getSysAdminId());
					
					if(assetOutput.getFinanceEx_updated_by()!=null)
						outputBean.setFinanceHeadName(
								gsrGoalService.getEmpNameByEmpId(assetOutput.getFinanceEx_updated_by().intValue()) + '-'+assetOutput.getFinanceEx_updated_by());
					
			 if(purchaseDatAssetQuote != null)
				 
			   {
					if(purchaseDatAssetQuote.getCostBorneBy().equalsIgnoreCase("shared"))
					{
						 itBuDetail = businessUnitRepository.findByPkBuUnitId(itBuId);
							
						 outputBean.setBuHeadName(
									gsrGoalService.getEmpNameByEmpId(assetOutput.getMasBuUnitBO().getFkBuHeadEmpId())+ '-'+assetOutput.getMasBuUnitBO().getFkBuHeadEmpId());
						 
						if(assetOutput.getMasBuUnitBO().getFkBuHeadEmpId()!= itBuDetail.getFkBuHeadEmpId()){
						outputBean.setItHeadName(
								gsrGoalService.getEmpNameByEmpId(itBuDetail.getFkBuHeadEmpId()) + '-'+itBuDetail.getFkBuHeadEmpId());
						 }
					}
					
					if(purchaseDatAssetQuote.getCostBorneBy().equalsIgnoreCase("bu"))
					{
						outputBean.setBuHeadName(
								gsrGoalService.getEmpNameByEmpId(assetOutput.getMasBuUnitBO().getFkBuHeadEmpId())+ '-'+assetOutput.getMasBuUnitBO().getFkBuHeadEmpId());
					}
					
					if(purchaseDatAssetQuote.getCostBorneBy().equalsIgnoreCase("IT"))
					{
						 itBuDetail = businessUnitRepository.findByPkBuUnitId(itBuId);
							
						outputBean.setItHeadName(
								gsrGoalService.getEmpNameByEmpId(itBuDetail.getFkBuHeadEmpId()) + '-'+itBuDetail.getFkBuHeadEmpId());
					}
				 }
				 //EOA pratibha
			
			outputBean.setSupportingDocumentsByClient(capexDetailsOutput.getPurchaseDatAssetRequestBO().getSupportingDocumentsByClient());
			outputBean.setIsClientReimbursing(capexDetailsOutput.getPurchaseDatAssetRequestBO()
					.getIsClientReimbursing());
			outputBean.setBriefDescription(capexDetailsOutput.getPurchaseDatAssetRequestBO()
					.getBriefDescription());
			outputBean.setTotalCost(capexDetailsOutput.getTotalCost());
			outputBean.setIndividualLicenseCost(capexDetailsOutput
					.getIndividualLicenseCost());
			outputBean.setAssetRequiredBy(capexDetailsOutput.getPurchaseDatAssetRequestBO().getAssetRequiredBy());
			outputBean.setAssetCreatedDate(capexDetailsOutput.getPurchaseDatAssetRequestBO().getCreatedDate());
			outputBean.setAssetModifiedDate(capexDetailsOutput.getPurchaseDatAssetRequestBO().getModifiedOn());
			outputBean.setCostBorneBy(capexDetailsOutput.getCostBorneBy());
			if(capexDetailsOutput.getCostBorneBy().equals("BU")){
				outputBean.setBuContribution(capexDetailsOutput.getBuContribution());}
			
			if(capexDetailsOutput.getCostBorneBy().equals("IT")){
				outputBean.setItContribution(capexDetailsOutput.getBuContribution());}
			
			if(capexDetailsOutput.getCostBorneBy().equals("SHARED")){
				outputBean.setItContribution(capexDetailsOutput.getTotalCost()-capexDetailsOutput.getBuContribution());
				outputBean.setBuContribution(capexDetailsOutput.getBuContribution());
			}
			
			// get component & client details from legacy service
			ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean(); 
			clientProjCompDetailsBean.setClientId( capexDetailsOutput.getPurchaseDatAssetRequestBO().getClientId());
			clientProjCompDetailsBean.setProjectId( capexDetailsOutput.getPurchaseDatAssetRequestBO().getProjectId());
			clientProjCompDetailsBean.setComponentId(capexDetailsOutput.getPurchaseDatAssetRequestBO().getProjectComponentId());
			clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
						
			PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
			ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService.
					getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
						
			if(clientProjectCompDetails == null){
				throw new CommonCustomException("Unable to fetch project, client and component details from legacy MIS.");
			}else{
				outputBean.setProjectName(clientProjectCompDetails.getProjectName());
				outputBean.setInternalFlag(clientProjectCompDetails.getInternalProjectFlag());
			}
			
			//Added by Shyam for reject comments
			if(assetOutput.getRmComment() != null)
				outputBean.setReportingManager(assetOutput.getRmComment());
			
			if(assetOutput.getSysAdminComment() != null)
				outputBean.setSysAdminComment(assetOutput.getSysAdminComment());
			
			if(assetOutput.getFinanceEx_comment() != null)
				outputBean.setFinanceExComment(assetOutput.getFinanceEx_comment());
			
			if(assetOutput.getClosureComment() != null)
				outputBean.setSysAdminComment(assetOutput.getClosureComment());
			
			//For BU head reject comments
			List<PurchaseDatBuBO> purchaseDatBuList = new ArrayList<PurchaseDatBuBO>();
			try{
				purchaseDatBuList = purchaseDatBuRepo.findByFkPurchaseQuoteId(purchaseDatAssetQuote.getPkQuoteId());
			}catch(Exception ex){
				throw new CommonCustomException("Failed to fetch bu records, kindly get in touch with Nucleus Support team");
			}
			
			if(purchaseDatBuList.isEmpty())
				throw new CommonCustomException("No bu records found for approve capex");
			
			for (PurchaseDatBuBO purchaseDatBuBO : purchaseDatBuList) 
			{
				if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
				{
					if(purchaseDatBuBO.getBuHeadComment() == null)
						throw new CommonCustomException("Rejected comment not found. KIndly get in touch with Nucleus Support team ");
					else
						outputBean.setBuHeadComment(purchaseDatBuBO.getBuHeadComment());
					break;
				}
			}
			
			//EOA by Shyam for reject comments
			
		}else{
			throw new CommonCustomException("No capex details to display.");
		}

		LOG.endUsecase("Get Capex Details based on asset request ID.");

		return outputBean;
	}

	public PurchaseAssetRequestDetailOutputBean rejectCapex(PurchaseRejectCapexInputBean rejectCapexInputBean)
			throws CommonCustomException {

		LOG.startUsecase("Reject Capex ");
		PurchaseDatAssetRequestBO assetRequestBO = new PurchaseDatAssetRequestBO();
		PurchaseDatAssetRequestBO updatedAssetRequest = new PurchaseDatAssetRequestBO();
		
		PurchaseAssetRequestDetailOutputBean assetRequestOutputBean = new PurchaseAssetRequestDetailOutputBean();

		//1.Purchase request bo details
		try {
			assetRequestBO = purchaseAssetRequestRepo.findByPkPurchaseRequestId(rejectCapexInputBean
							.getPkPurchaseRequestId());
		} catch (Exception e) {
			throw new CommonCustomException("Unable to fetch asset request detail from backend. "
					+ "Kindly get in touch with Nucleus Support team.");
		}
		
		if(assetRequestBO == null){
			throw new CommonCustomException("Selected asset request does not exist.");
		}
		
		if(assetRequestBO.getPurchaseRequestStatus() == purchaseCapexRejectByBuHeadStatus ){
			throw new CommonCustomException("Capex is already rejected.");
		}
		
		if(assetRequestBO.getPurchaseRequestStatus().intValue() == purchaseCapexCreatedBySysAdminStatus)
		{
			PurchaseDatAssetQuoteBO defaultQuote = new PurchaseDatAssetQuoteBO();
			//2.Purchase quote  details
			try{
				defaultQuote = assetQuoteRepository.getCapexDetailsByPurchaseRequestID(rejectCapexInputBean
							.getPkPurchaseRequestId());
			}catch(Exception ex){
				throw new CommonCustomException("Failed to fetch details from db.");
			}
				
			if(defaultQuote == null){
				throw new CommonCustomException("Selected asset request doesn't have default quote.");
			}
			
			List<PurchaseDatBuBO> purchaseDatBuList = new ArrayList<PurchaseDatBuBO>();
			//3.Purchase BU  details
			try{
				purchaseDatBuList = purchaseDatBuRepo.getAllPurchaseDatBuRecords(rejectCapexInputBean
						.getPkPurchaseRequestId());
			}catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from db.");
			}
					
			if(purchaseDatBuList.isEmpty()){
				throw new CommonCustomException("No BU head enteries found for reject capex. Kindly get in touch with Nucleus Support team."); 
			}
					
			boolean buHeadFlag = false;
			boolean itHeadFlag = false;
			boolean buHeadAndItHeadIsSame = false;
					
			MasBuUnitBO masBuUnitBo = assetRequestBO.getMasBuUnitBO();
			MasBuUnitBO nbuITBO = new MasBuUnitBO();
			try{
				nbuITBO = businessUnitRepository.findByPkBuUnitId(nbuITBusinessUnitId);
			} catch(Exception ex){
				throw new CommonCustomException("Unable to get NBU-IT details from backend. Kindly get in touch with Nucleus Support team.");
			}
					
			for (PurchaseDatBuBO purchaseDatBuBO : purchaseDatBuList) 
			{
				if(rejectCapexInputBean.getLoggedInUser().intValue() == masBuUnitBo.getFkBuHeadEmpId() &&
						rejectCapexInputBean.getLoggedInUser().intValue() == nbuITBO.getFkBuHeadEmpId() &&
						masBuUnitBo.getPkBuUnitId() == nbuITBusinessUnitId &&
						purchaseDatBuBO.getBuHeadId() == rejectCapexInputBean.getLoggedInUser().intValue() &&
						purchaseDatBuBO.getMasBuUnitBO().getPkBuUnitId() == nbuITBusinessUnitId)
				{
					buHeadAndItHeadIsSame = true;
					break;
				}else if(rejectCapexInputBean.getLoggedInUser().intValue() == masBuUnitBo.getFkBuHeadEmpId() &&
					rejectCapexInputBean.getLoggedInUser().intValue() == nbuITBO.getFkBuHeadEmpId() &&
					masBuUnitBo.getPkBuUnitId() != nbuITBusinessUnitId &&
					purchaseDatBuBO.getBuHeadId() == rejectCapexInputBean.getLoggedInUser().intValue() &&
					purchaseDatBuBO.getMasBuUnitBO().getPkBuUnitId() != nbuITBusinessUnitId)
				{
					buHeadAndItHeadIsSame = true;
					break;
				}else if(rejectCapexInputBean.getLoggedInUser().intValue() == masBuUnitBo.getFkBuHeadEmpId()  && 
					rejectCapexInputBean.getLoggedInUser().intValue() != nbuITBO.getFkBuHeadEmpId() && 
					masBuUnitBo.getPkBuUnitId() != nbuITBusinessUnitId && 
					purchaseDatBuBO.getBuHeadId() == rejectCapexInputBean.getLoggedInUser().intValue() &&
					purchaseDatBuBO.getMasBuUnitBO().getPkBuUnitId() != nbuITBusinessUnitId) 
				{
					buHeadFlag = true;
					break;
				}else if(rejectCapexInputBean.getLoggedInUser().intValue() == masBuUnitBo.getFkBuHeadEmpId()  && 
					rejectCapexInputBean.getLoggedInUser().intValue() == nbuITBO.getFkBuHeadEmpId() && 
					masBuUnitBo.getPkBuUnitId() == nbuITBusinessUnitId && 
					purchaseDatBuBO.getBuHeadId() == rejectCapexInputBean.getLoggedInUser().intValue() &&
					purchaseDatBuBO.getMasBuUnitBO().getPkBuUnitId() == nbuITBusinessUnitId)
				{//For When request raised by IT.
					itHeadFlag = true;
					break;
				}else if(rejectCapexInputBean.getLoggedInUser().intValue() != masBuUnitBo.getFkBuHeadEmpId()  && 
					rejectCapexInputBean.getLoggedInUser().intValue() == nbuITBO.getFkBuHeadEmpId() && 
					masBuUnitBo.getPkBuUnitId() != nbuITBusinessUnitId && 
					purchaseDatBuBO.getBuHeadId() == rejectCapexInputBean.getLoggedInUser().intValue() &&
					purchaseDatBuBO.getMasBuUnitBO().getPkBuUnitId() == nbuITBusinessUnitId)
				{//This condition occurs when NBU-IT head try to approve the shared capex. 
					itHeadFlag = true;
					break;
				}else if(rejectCapexInputBean.getLoggedInUser().intValue() == masBuUnitBo.getFkBuHeadEmpId()  && 
						rejectCapexInputBean.getLoggedInUser().intValue() == nbuITBO.getFkBuHeadEmpId() && 
						masBuUnitBo.getPkBuUnitId() != nbuITBusinessUnitId && 
						purchaseDatBuBO.getBuHeadId() == rejectCapexInputBean.getLoggedInUser().intValue() &&
						purchaseDatBuBO.getMasBuUnitBO().getPkBuUnitId() == nbuITBusinessUnitId)
				{//This condition occurs when NBU-IT head try to approve the IT capex. 
					itHeadFlag = true;
					break;
				}
			}//End of for loop
					
			if(buHeadAndItHeadIsSame){		
			}else if(buHeadFlag){
			}else if(itHeadFlag){
			}else{
				if(defaultQuote.getCostBorneBy().equalsIgnoreCase("SHARED")){
					throw new CommonCustomException("Neither you are a BU Head of the requestor nor NBU-IT Head, hence you are restricted to reject this capex.");
				}else if(defaultQuote.getCostBorneBy().equalsIgnoreCase("BU"))
				{
					throw new CommonCustomException("You are not a BU Head of the requestor, hence you are restricted to reject this capex.");
				}else{
					throw new CommonCustomException("You are not a NBU-IT Head , hence you are restricted to reject this capex.");
				}
			}
					
			if(defaultQuote.getCostBorneBy().equalsIgnoreCase("SHARED"))
			{	
				boolean buHeadApprovalFlag = false;
				if(buHeadAndItHeadIsSame)
				{
					PurchaseDatBuBO purchaseDatBuBO = purchaseDatBuList.get(0); //Only have one record
					
					if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
					{
						throw new CommonCustomException("This capex has been already rejected.");
					}
					else if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("AGREE"))
					{
						buHeadApprovalFlag = true;
					}else if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("WAITING FOR APPROVAL"))
					{
						purchaseDatBuBO.setFkPurchaseDatBuStatus("DISAGREE");
						purchaseDatBuBO.setModifiedBy(rejectCapexInputBean.getLoggedInUser());
						purchaseDatBuBO.setModifiedDate(new Date());
						purchaseDatBuBO.setBuHeadComment(rejectCapexInputBean.getClosureComment());//Added by Shyam
						purchaseDatBuRepo.save(purchaseDatBuBO);
								
						assetRequestBO.setPkPurchaseRequestId(rejectCapexInputBean.getPkPurchaseRequestId());
						//assetRequestBO.setClosureComment(rejectCapexInputBean.getClosureComment());//Commented by Shyam
						assetRequestBO.setModifiedOn(new Date());
						assetRequestBO.setModifiedBy(rejectCapexInputBean.getLoggedInUser());
						assetRequestBO.setPurchaseRequestStatus(
							Integer.valueOf(purchaseCapexRejectByBuHeadStatus));
						updatedAssetRequest = purchaseAssetRequestRepo.save(assetRequestBO);
								
						//Added by Kamal Anand for Notifications
						try{
							notificationService.setCapexCreatedNotificationToBUHeadScreen(assetRequestBO.getPkPurchaseRequestId());
							notificationService.setCapexRejectedNotificationToBuHeadScreen(assetRequestBO.getPkPurchaseRequestId());
							notificationService.setCapexRejectedNotificationToRMScreen(assetRequestBO.getRmToApprove());
							notificationService.setCapexRejectedNotificationToEmployeeScreen(assetRequestBO.getRequestorId());
							List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
							if(empDetail != null)
							{
								empDetail.forEach(emp -> {
									try {
										notificationService.setCapexRejectedNotificationToSysAdminScreen(emp.getPkEmpId());
									} catch (Exception e) {
										LOG.info("Exception occured while setting notification in Reject Capex");
									}
								});
							}
						} catch(Exception e) {
							LOG.info("Exception occurred while setting Capex Rejected by BU Head Notification");
						}
						//End of Addition by Kamal Anand for Notifications
								
						assetRequestOutputBean.setPurchaseRequestId(assetRequestBO.getPkPurchaseRequestId());
						assetRequestOutputBean.setAssetType(assetRequestBO.getPurchaseMasAssetsBO().getAssetType());
						assetRequestOutputBean.setAssetId(assetRequestBO.getFkAssetId());
						assetRequestOutputBean.setNumberOfLincenses(assetRequestBO.getNoOfUnits());
						assetRequestOutputBean.setProjectComponentId(assetRequestBO.getProjectComponentId());
						assetRequestOutputBean.setCreatedDate(assetRequestBO.getCreatedDate());
						assetRequestOutputBean.setAssetRequiredBy(assetRequestBO.getAssetRequiredBy());
						assetRequestOutputBean.setModifiedOn(assetRequestBO.getModifiedOn());
						assetRequestOutputBean.setModifiedBy(assetRequestBO.getModifiedBy());
						assetRequestOutputBean.setDuration(assetRequestBO.getDuration());
						assetRequestOutputBean.setPurchaseRequestStatus(assetRequestBO.getPurchaseRequestStatus());
						assetRequestOutputBean.setLocationId(assetRequestBO.getLocationId());
						assetRequestOutputBean.setClientId(assetRequestBO.getClientId());
						assetRequestOutputBean.setRmId(assetRequestBO.getRmToApprove());
						assetRequestOutputBean.setIsClientReimbursing(assetRequestBO.getIsClientReimbursing());
						assetRequestOutputBean.setBuHeadComment(rejectCapexInputBean.getClosureComment());//Added by Shyam
								
						return assetRequestOutputBean;
					}
					//}//End of for loop
				}else if(buHeadFlag)
				{
					for (PurchaseDatBuBO purchaseDatBuBO : purchaseDatBuList) 
					{	
						if(rejectCapexInputBean.getLoggedInUser().intValue() == masBuUnitBo.getFkBuHeadEmpId())
						{
							if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
							{
								throw new CommonCustomException("This capex has been already rejected.");
							}
							else if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("AGREE"))
							{
								throw new CommonCustomException("This capex has been already approved.");
							}
							else if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("WAITING FOR APPROVAL"))
							{
								purchaseDatBuBO.setFkPurchaseDatBuStatus("DISAGREE");
								purchaseDatBuBO.setModifiedBy(rejectCapexInputBean.getLoggedInUser());
								purchaseDatBuBO.setModifiedDate(new Date());
								purchaseDatBuBO.setBuHeadComment(rejectCapexInputBean.getClosureComment());//Added by Shyam
								purchaseDatBuRepo.save(purchaseDatBuBO);
										
								assetRequestBO.setPkPurchaseRequestId(rejectCapexInputBean.getPkPurchaseRequestId());
								//assetRequestBO.setClosureComment(rejectCapexInputBean.getClosureComment()); Commented by Shyam
								assetRequestBO.setModifiedOn(new Date());
								assetRequestBO.setModifiedBy(rejectCapexInputBean.getLoggedInUser());
								assetRequestBO.setPurchaseRequestStatus(Integer.valueOf(purchaseCapexRejectByBuHeadStatus));
								updatedAssetRequest = purchaseAssetRequestRepo.save(assetRequestBO);
										
								//Added by Kamal Anand for Notifications
								try{
									notificationService.setCapexCreatedNotificationToBUHeadScreen(assetRequestBO.getPkPurchaseRequestId());
									notificationService.setCapexRejectedNotificationToBuHeadScreen(assetRequestBO.getPkPurchaseRequestId());
									notificationService.setCapexRejectedNotificationToRMScreen(assetRequestBO.getRmToApprove());
									notificationService.setCapexRejectedNotificationToEmployeeScreen(assetRequestBO.getRequestorId());
									List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
									if(empDetail != null)
									{
										empDetail.forEach(emp -> {
											try {
												notificationService.setCapexRejectedNotificationToSysAdminScreen(emp.getPkEmpId());
											} catch (Exception e) {
												LOG.info("Exception occured while setting notification in Reject Capex");
											}
										});
									}
								} catch(Exception e) {
									LOG.info("Exception occurred while setting Capex Rejected by BU Head Notification");
								}
								//End of Addition by Kamal Anand for Notifications
										
								assetRequestOutputBean.setPurchaseRequestId(assetRequestBO.getPkPurchaseRequestId());
								assetRequestOutputBean.setAssetType(assetRequestBO.getPurchaseMasAssetsBO().getAssetType());
								assetRequestOutputBean.setAssetId(assetRequestBO.getFkAssetId());
								assetRequestOutputBean.setNumberOfLincenses(assetRequestBO.getNoOfUnits());
								assetRequestOutputBean.setProjectComponentId(assetRequestBO.getProjectComponentId());
								assetRequestOutputBean.setCreatedDate(assetRequestBO.getCreatedDate());
								assetRequestOutputBean.setAssetRequiredBy(assetRequestBO.getAssetRequiredBy());
								assetRequestOutputBean.setModifiedOn(assetRequestBO.getModifiedOn());
								assetRequestOutputBean.setModifiedBy(assetRequestBO.getModifiedBy());
								assetRequestOutputBean.setDuration(assetRequestBO.getDuration());
								assetRequestOutputBean.setPurchaseRequestStatus(assetRequestBO.getPurchaseRequestStatus());
								assetRequestOutputBean.setLocationId(assetRequestBO.getLocationId());
								assetRequestOutputBean.setClientId(assetRequestBO.getClientId());
								assetRequestOutputBean.setRmId(assetRequestBO.getRmToApprove());
								assetRequestOutputBean.setIsClientReimbursing(assetRequestBO.getIsClientReimbursing());
								assetRequestOutputBean.setBuHeadComment(rejectCapexInputBean.getClosureComment());//Added by Shyam
								
								return assetRequestOutputBean;
							}
						}
					}//End of for loop
				}else if(itHeadFlag)
				{
					for (PurchaseDatBuBO purchaseDatBuBO : purchaseDatBuList) 
					{
						if(rejectCapexInputBean.getLoggedInUser().intValue() == nbuITBO.getFkBuHeadEmpId())
						{
							if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
							{
								throw new CommonCustomException("This capex has been already rejected.");
							}
							else if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("AGREE"))
							{
								if(purchaseDatBuBO.getBuHeadId() == nbuITBO.getFkBuHeadEmpId())
									throw new CommonCustomException("This capex has been already approved.");
								else
									buHeadApprovalFlag = true;
							}
							else if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("WAITING FOR APPROVAL"))
							{
								//To check whether BU Head is approved or not
								for (PurchaseDatBuBO purchaseDatBuBOTempObj : purchaseDatBuList) 
								{
									if(masBuUnitBo.getPkBuUnitId() == purchaseDatBuBOTempObj.getFkMasBuId() 
											&& purchaseDatBuBOTempObj.getFkPurchaseDatBuStatus().equalsIgnoreCase("AGREE"))
									{
										buHeadApprovalFlag = true;
										break;
									}
								}
								
								if(buHeadApprovalFlag)
								{
									purchaseDatBuBO.setFkPurchaseDatBuStatus("DISAGREE");
									purchaseDatBuBO.setModifiedBy(rejectCapexInputBean.getLoggedInUser());
									purchaseDatBuBO.setModifiedDate(new Date());
									purchaseDatBuBO.setBuHeadComment(rejectCapexInputBean.getClosureComment());//Added by Shyam
									purchaseDatBuRepo.save(purchaseDatBuBO);
									
									assetRequestBO.setPkPurchaseRequestId(rejectCapexInputBean.getPkPurchaseRequestId());
									//assetRequestBO.setClosureComment(rejectCapexInputBean.getClosureComment());Commented by Shyam
									assetRequestBO.setModifiedOn(new Date());
									assetRequestBO.setModifiedBy(rejectCapexInputBean.getLoggedInUser());
									assetRequestBO.setPurchaseRequestStatus(Integer.valueOf(purchaseCapexRejectByBuHeadStatus));
									updatedAssetRequest = purchaseAssetRequestRepo.save(assetRequestBO);
									
									//Added by Kamal Anand for Notifications
									try{
										notificationService.setCapexCreatedNotificationToBUHeadScreen(assetRequestBO.getPkPurchaseRequestId());
										notificationService.setCapexRejectedNotificationToBuHeadScreen(assetRequestBO.getPkPurchaseRequestId());
										notificationService.setCapexRejectedNotificationToRMScreen(assetRequestBO.getRmToApprove());
										notificationService.setCapexRejectedNotificationToEmployeeScreen(assetRequestBO.getRequestorId());
										List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
										if(empDetail != null)
										{
											empDetail.forEach(emp -> {
												try {
													notificationService.setCapexRejectedNotificationToSysAdminScreen(emp.getPkEmpId());
												} catch (Exception e) {
													LOG.info("Exception occured while setting notification in Reject Capex");
												}
											});
										}
									} catch(Exception e) {
										LOG.info("Exception occurred while setting Capex Rejected by BU Head Notification");
									}
									//End of Addition by Kamal Anand for Notifications
									
									assetRequestOutputBean.setPurchaseRequestId(assetRequestBO.getPkPurchaseRequestId());
									assetRequestOutputBean.setAssetType(assetRequestBO.getPurchaseMasAssetsBO().getAssetType());
									assetRequestOutputBean.setAssetId(assetRequestBO.getFkAssetId());
									assetRequestOutputBean.setNumberOfLincenses(assetRequestBO.getNoOfUnits());
									assetRequestOutputBean.setProjectComponentId(assetRequestBO.getProjectComponentId());
									assetRequestOutputBean.setCreatedDate(assetRequestBO.getCreatedDate());
									assetRequestOutputBean.setAssetRequiredBy(assetRequestBO.getAssetRequiredBy());
									assetRequestOutputBean.setModifiedOn(assetRequestBO.getModifiedOn());
									assetRequestOutputBean.setModifiedBy(assetRequestBO.getModifiedBy());
									assetRequestOutputBean.setDuration(assetRequestBO.getDuration());
									assetRequestOutputBean.setPurchaseRequestStatus(assetRequestBO.getPurchaseRequestStatus());
									assetRequestOutputBean.setLocationId(assetRequestBO.getLocationId());
									assetRequestOutputBean.setClientId(assetRequestBO.getClientId());
									assetRequestOutputBean.setRmId(assetRequestBO.getRmToApprove());
									assetRequestOutputBean.setIsClientReimbursing(assetRequestBO.getIsClientReimbursing());
									assetRequestOutputBean.setBuHeadComment(rejectCapexInputBean.getClosureComment());//Added by Shyam
									
									return assetRequestOutputBean;
									
								}else{
									throw new CommonCustomException("BU head approval is pending for this capex request.");
								}
							}
						}
					}//End of for loop
				}else{
					throw new CommonCustomException("Neither you have a role of BU Head nor IT Head. Kindly get in touch with Nucleus Support team.");
				}
			}else if(defaultQuote.getCostBorneBy().equalsIgnoreCase("BU"))
			{
				PurchaseDatBuBO purchaseDatBuBo = new PurchaseDatBuBO();
				try{
					purchaseDatBuBo = purchaseDatBuRepo.getPurchaseDatBuObject(rejectCapexInputBean.getPkPurchaseRequestId());
				}catch(Exception ex){
					throw new CommonCustomException("Unable to get business unit head details from backend. Kindly get in touch with Nucleus Support team.");
				}
						
				if(purchaseDatBuBo == null){
					throw new CommonCustomException("Failed to get business unit head details from backend. Kindly get in touch with Nucleus Support team.");
				}
						
				if(buHeadAndItHeadIsSame || buHeadFlag)
				{
					if(purchaseDatBuBo.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
					{
						throw new CommonCustomException("This capex has been already rejected.");
					}
					else if(purchaseDatBuBo.getFkPurchaseDatBuStatus().equalsIgnoreCase("AGREE"))
					{
						throw new CommonCustomException("This capex has been already approved.");
					}
					else if(purchaseDatBuBo.getFkPurchaseDatBuStatus().equalsIgnoreCase("WAITING FOR APPROVAL"))
					{
						purchaseDatBuBo.setFkPurchaseDatBuStatus("DISAGREE");
						purchaseDatBuBo.setModifiedBy(rejectCapexInputBean.getLoggedInUser());
						purchaseDatBuBo.setModifiedDate(new Date());
						purchaseDatBuBo.setBuHeadComment(rejectCapexInputBean.getClosureComment());//Added by Shyam
						purchaseDatBuRepo.save(purchaseDatBuBo);
						
						assetRequestBO.setPkPurchaseRequestId(rejectCapexInputBean.getPkPurchaseRequestId());
						//assetRequestBO.setClosureComment(rejectCapexInputBean.getClosureComment()); Commented by Shyam
						assetRequestBO.setModifiedOn(new Date());
						assetRequestBO.setModifiedBy(rejectCapexInputBean.getLoggedInUser());
						assetRequestBO.setPurchaseRequestStatus(
								Integer.valueOf(purchaseCapexRejectByBuHeadStatus));
						updatedAssetRequest = purchaseAssetRequestRepo.save(assetRequestBO);
								
						//Added by Kamal Anand for Notifications
						try{
							notificationService.setCapexCreatedNotificationToBUHeadScreen(assetRequestBO.getPkPurchaseRequestId());
							notificationService.setCapexRejectedNotificationToBuHeadScreen(assetRequestBO.getPkPurchaseRequestId());
							notificationService.setCapexRejectedNotificationToRMScreen(assetRequestBO.getRmToApprove());
							notificationService.setCapexRejectedNotificationToEmployeeScreen(assetRequestBO.getRequestorId());
							List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
							if(empDetail != null)
							{
								empDetail.forEach(emp -> {
									try {
										notificationService.setCapexRejectedNotificationToSysAdminScreen(emp.getPkEmpId());
									} catch (Exception e) {
										LOG.info("Exception occured while setting notification in Reject Capex");
									}
								});
							}
						} catch(Exception e) {
							LOG.info("Exception occurred while setting Capex Rejected by BU Head Notification");
						}
						//End of Addition by Kamal Anand for Notifications
						
						assetRequestOutputBean.setPurchaseRequestId(assetRequestBO.getPkPurchaseRequestId());
						assetRequestOutputBean.setAssetType(assetRequestBO.getPurchaseMasAssetsBO().getAssetType());
						assetRequestOutputBean.setAssetId(assetRequestBO.getFkAssetId());
						assetRequestOutputBean.setNumberOfLincenses(assetRequestBO.getNoOfUnits());
						assetRequestOutputBean.setProjectComponentId(assetRequestBO.getProjectComponentId());
						assetRequestOutputBean.setCreatedDate(assetRequestBO.getCreatedDate());
						assetRequestOutputBean.setAssetRequiredBy(assetRequestBO.getAssetRequiredBy());
						assetRequestOutputBean.setModifiedOn(assetRequestBO.getModifiedOn());
						assetRequestOutputBean.setModifiedBy(assetRequestBO.getModifiedBy());
						assetRequestOutputBean.setDuration(assetRequestBO.getDuration());
						assetRequestOutputBean.setPurchaseRequestStatus(assetRequestBO.getPurchaseRequestStatus());
						assetRequestOutputBean.setLocationId(assetRequestBO.getLocationId());
						assetRequestOutputBean.setClientId(assetRequestBO.getClientId());
						assetRequestOutputBean.setRmId(assetRequestBO.getRmToApprove());
						assetRequestOutputBean.setIsClientReimbursing(assetRequestBO.getIsClientReimbursing());
						assetRequestOutputBean.setBuHeadComment(rejectCapexInputBean.getClosureComment());//Added by Shyam
						
						return assetRequestOutputBean;
					}
				}else if(itHeadFlag)
				{
					throw new CommonCustomException("This capex's cost is borne only by BU. Hence IT head cannot reject this capex.");
				}
			}else if(defaultQuote.getCostBorneBy().equalsIgnoreCase("IT"))
			{
				PurchaseDatBuBO purchaseDatBuBo1 = new PurchaseDatBuBO();
				try{
					purchaseDatBuBo1 = purchaseDatBuRepo.getPurchaseDatBuObject(rejectCapexInputBean.getPkPurchaseRequestId());
				}catch(Exception ex){
					throw new CommonCustomException("Unable to get business unit head details from backend. Kindly get in touch with Nucleus Support team.");
				}			
						
				if(purchaseDatBuBo1 == null){
					throw new CommonCustomException("Failed to get business unit head details from backend. Kindly get in touch with Nucleus Support team.");
				}
						
				if(buHeadAndItHeadIsSame || itHeadFlag)
				{
					if(purchaseDatBuBo1.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
					{
						throw new CommonCustomException("This capex has been already rejected.");
					}
					else if(purchaseDatBuBo1.getFkPurchaseDatBuStatus().equalsIgnoreCase("AGREE"))
					{
						throw new CommonCustomException("This capex has been already approved.");
					}
					else if(purchaseDatBuBo1.getFkPurchaseDatBuStatus().equalsIgnoreCase("WAITING FOR APPROVAL"))
					{
						purchaseDatBuBo1.setFkPurchaseDatBuStatus("DISAGREE");
						purchaseDatBuBo1.setModifiedBy(rejectCapexInputBean.getLoggedInUser());
						purchaseDatBuBo1.setModifiedDate(new Date());
						purchaseDatBuBo1.setBuHeadComment(rejectCapexInputBean.getClosureComment());//Added by Shyam
						purchaseDatBuRepo.save(purchaseDatBuBo1);
						
						assetRequestBO.setPkPurchaseRequestId(rejectCapexInputBean.getPkPurchaseRequestId());
						//assetRequestBO.setClosureComment(rejectCapexInputBean.getClosureComment());Commented by Shyam
						assetRequestBO.setModifiedOn(new Date());
						assetRequestBO.setModifiedBy(rejectCapexInputBean.getLoggedInUser());
						assetRequestBO.setPurchaseRequestStatus(Integer.valueOf(purchaseCapexRejectByBuHeadStatus));
						updatedAssetRequest = purchaseAssetRequestRepo.save(assetRequestBO);
						//Added by Kamal Anand for Notifications
						try{
							notificationService.setCapexCreatedNotificationToBUHeadScreen(assetRequestBO.getPkPurchaseRequestId());
							notificationService.setCapexRejectedNotificationToBuHeadScreen(assetRequestBO.getPkPurchaseRequestId());
							notificationService.setCapexRejectedNotificationToRMScreen(assetRequestBO.getRmToApprove());
							notificationService.setCapexRejectedNotificationToEmployeeScreen(assetRequestBO.getRequestorId());
							List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
							if(empDetail != null)
							{
								empDetail.forEach(emp -> {
									try {
										notificationService.setCapexRejectedNotificationToSysAdminScreen(emp.getPkEmpId());
									} catch (Exception e) {
										LOG.info("Exception occured while setting notification in Reject Capex");
									}
								});
							}
						} catch(Exception e) {
							LOG.info("Exception occurred while setting Capex Rejected by BU Head Notification");
						}
						//End of Addition by Kamal Anand for Notifications
							
						assetRequestOutputBean.setPurchaseRequestId(assetRequestBO.getPkPurchaseRequestId());
						assetRequestOutputBean.setAssetType(assetRequestBO.getPurchaseMasAssetsBO().getAssetType());
						assetRequestOutputBean.setAssetId(assetRequestBO.getFkAssetId());
						assetRequestOutputBean.setNumberOfLincenses(assetRequestBO.getNoOfUnits());
						assetRequestOutputBean.setProjectComponentId(assetRequestBO.getProjectComponentId());
						assetRequestOutputBean.setCreatedDate(assetRequestBO.getCreatedDate());
						assetRequestOutputBean.setAssetRequiredBy(assetRequestBO.getAssetRequiredBy());
						assetRequestOutputBean.setModifiedOn(assetRequestBO.getModifiedOn());
						assetRequestOutputBean.setModifiedBy(assetRequestBO.getModifiedBy());
						assetRequestOutputBean.setDuration(assetRequestBO.getDuration());
						assetRequestOutputBean.setPurchaseRequestStatus(assetRequestBO.getPurchaseRequestStatus());
						assetRequestOutputBean.setLocationId(assetRequestBO.getLocationId());
						assetRequestOutputBean.setClientId(assetRequestBO.getClientId());
						assetRequestOutputBean.setRmId(assetRequestBO.getRmToApprove());
						assetRequestOutputBean.setIsClientReimbursing(assetRequestBO.getIsClientReimbursing());
						assetRequestOutputBean.setBuHeadComment(rejectCapexInputBean.getClosureComment());//Added by Shyam
						
						return assetRequestOutputBean;
					}
				}else if(buHeadFlag)
				{
					throw new CommonCustomException("This capex's cost is borne only by IT. Hence BU head cannot reject this capex.");
				}
			}
			
		}else{
			throw new CommonCustomException("In order to reject Capex, capex should be created by System Admin");
		}
		
		
		LOG.endUsecase("Reject Capex ");

		return assetRequestOutputBean;

	}
	
	public List<PurchaseViewCapexRequestsByDateRangeAndBuIdOutputBean> viewCapexRequests(
			PurchaseViewCapexRequestsByDateRangeAndBuIdInputBean 
			purchaseViewCapexRequestsByDateRangeAndBuIdInputBean) throws CommonCustomException 
	{

		LOG.startUsecase("View Capex Requests based on PurchaseViewCapexRequestsByDateRangeAndBuIdInputBean.");
		
		List<PurchaseViewCapexRequestsByDateRangeAndBuIdOutputBean> outputBeanList = new ArrayList<PurchaseViewCapexRequestsByDateRangeAndBuIdOutputBean>();
		List<PurchaseDatAssetRequestBO> capexRequestsOutput = new ArrayList<PurchaseDatAssetRequestBO>();
		List<MasBuUnitBO> masBuUnitBoList = new ArrayList<MasBuUnitBO>();
		DatEmpDetailBO empDetailBo = new DatEmpDetailBO();
			
		if(purchaseViewCapexRequestsByDateRangeAndBuIdInputBean.getStartDate() != null && purchaseViewCapexRequestsByDateRangeAndBuIdInputBean.getEndDate() != null){
			if (purchaseViewCapexRequestsByDateRangeAndBuIdInputBean.getStartDate().equals(
					purchaseViewCapexRequestsByDateRangeAndBuIdInputBean.getEndDate())) {
				throw new CommonCustomException("startDate and endDate are same.");
	
			}
	
			if (purchaseViewCapexRequestsByDateRangeAndBuIdInputBean.getStartDate().after(
					purchaseViewCapexRequestsByDateRangeAndBuIdInputBean.getEndDate())) {
				throw new CommonCustomException(
						"startDate should be less than endDate.");
	
			}
		}
		Date fromDate = purchaseViewCapexRequestsByDateRangeAndBuIdInputBean.getStartDate();
		Calendar c = Calendar.getInstance(); 
		c.setTime(fromDate); 
		c.add(Calendar.DATE, -1);
		fromDate = c.getTime();
		Date toDate = purchaseViewCapexRequestsByDateRangeAndBuIdInputBean.getEndDate();
		Calendar cal = Calendar.getInstance(); 
		cal.setTime(toDate); 
		cal.add(Calendar.DATE, 1);
		toDate = cal.getTime();
		try{
			masBuUnitBoList = businessUnitRepository.getAllActiveBusinessUnitsUnderBUHead(
					purchaseViewCapexRequestsByDateRangeAndBuIdInputBean.getLoggedInUser());
		}catch(Exception ex){
			throw new CommonCustomException("Unable to fetch Business Unit details from backend. Kindly get in touch with Nucleus Support team.");
		} 
		empDetailBo=empDetailRepo.getBuDetails(purchaseViewCapexRequestsByDateRangeAndBuIdInputBean
				.getLoggedInUser());
		if(empDetailBo!= null){
			if(empDetailBo.getFkEmpRoleId() == buRoleId || empDetailBo.getFkEmpRoleId() == financeBuId ){
					
				List<Short> buIdList = new ArrayList<Short>();
				if(!masBuUnitBoList.isEmpty())
				{
					for (MasBuUnitBO masBuUnitBo : masBuUnitBoList) 
					{
						buIdList.add(Short.valueOf(masBuUnitBo.getPkBuUnitId()));
					}
						
					List<PurchaseDatBuBO> purchaseDatBuList = new ArrayList<PurchaseDatBuBO>();
					try{
						purchaseDatBuList = purchaseDatBuRepo.getListOfPurchaseDatBU(buIdList,fromDate,toDate);
					}catch(Exception ex)
					{
						throw new CommonCustomException("Failed to fetch purchase BU details. Kinldy get in touch with Nucleus Support team.");
					}
					if(!purchaseDatBuList.isEmpty())
					{
						PurchaseDatAssetRequestBO tempAssetRequestBO = new PurchaseDatAssetRequestBO();
						for (PurchaseDatBuBO purchaseDatBuBO : purchaseDatBuList) 
						{
							PurchaseDatAssetRequestBO assetRequestBO = new PurchaseDatAssetRequestBO();
							
							if(tempAssetRequestBO.getPkPurchaseRequestId() != null){
								if(tempAssetRequestBO.getPkPurchaseRequestId().intValue() != purchaseDatBuBO.
										getPurchaseDatAssetQuoteBO().getPurchaseDatAssetRequestBO().
										getPkPurchaseRequestId().intValue())
								{
									assetRequestBO = purchaseDatBuBO.getPurchaseDatAssetQuoteBO().getPurchaseDatAssetRequestBO();
									capexRequestsOutput.add(assetRequestBO);
									tempAssetRequestBO = assetRequestBO;
								}
							}else{
								assetRequestBO = purchaseDatBuBO.getPurchaseDatAssetQuoteBO().getPurchaseDatAssetRequestBO();
								capexRequestsOutput.add(assetRequestBO);
								tempAssetRequestBO = assetRequestBO;
							}
							
						}
					}
				}
					
				if(!capexRequestsOutput.isEmpty())
				{
					for (PurchaseDatAssetRequestBO assetReqBo : capexRequestsOutput)
					{
						if(assetReqBo.getPurchaseRequestStatus().intValue() ==  purchaseCapexCreatedBySysAdminStatus ||
								//assetReqBo.getPurchaseRequestStatus().intValue() == purchaseRejectCapexBySysAdmin  ||
							assetReqBo.getPurchaseRequestStatus().intValue() == requestCapexApprovedByBuHeadStatus ||
							assetReqBo.getPurchaseRequestStatus().intValue() == purchaseCapexRejectByBuHeadStatus ||
							assetReqBo.getPurchaseRequestStatus().intValue() == purchasePoCreatedBySysAdmin ||
							assetReqBo.getPurchaseRequestStatus().intValue() == purchasePoApprovedByFinance ||
							assetReqBo.getPurchaseRequestStatus().intValue() == purchasePoRejectedByFinance ||
							assetReqBo.getPurchaseRequestStatus().intValue() == purchaseAssetRequestClosed ||
							assetReqBo.getPurchaseRequestStatus().intValue() == purchasePoRejectedBySysAdmin)
						{
							PurchaseViewCapexRequestsByDateRangeAndBuIdOutputBean outputBean = 
									new PurchaseViewCapexRequestsByDateRangeAndBuIdOutputBean();
							outputBean.setPurchaseRequestId(assetReqBo.getPkPurchaseRequestId());
							outputBean.setCapexId(assetReqBo.getPurchaseDatCapexBO().getPkCapexId());
							outputBean.setCreatedOn(assetReqBo.getPurchaseDatCapexBO().getCreatedOn());
							outputBean.setPurchaseRequestStatusId(assetReqBo.getPurchaseRequestStatus());
							outputBean.setPurchaseRequestStatusName(assetReqBo.getPurchaseMasRequestStatusBO().
									getPurchaseRequestStatusName());
							outputBean.setProjectId(assetReqBo.getProjectId());
							outputBean.setAssetNameAndVersionOrModel(assetReqBo.getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());
							
							if(assetReqBo.getFkRequestTypeId().intValue() == purchaseNewAssetRequestType.intValue())
							{
								outputBean.setEmpId(assetReqBo.getRequestorId());
								outputBean.setEmpName(gsrGoalService.getEmpNameByEmpId(assetReqBo.getRequestorId()));
							}else{
								outputBean.setEmpId(assetReqBo.getRmToApprove());
								outputBean.setEmpName(gsrGoalService.getEmpNameByEmpId(assetReqBo.getRmToApprove()));
							}
									
							// get component & client details from legacy service
							ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean(); 
							clientProjCompDetailsBean.setClientId( assetReqBo.getClientId());
							clientProjCompDetailsBean.setProjectId( assetReqBo.getProjectId());
							clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
							clientProjCompDetailsBean.setComponentId(assetReqBo.getProjectComponentId());
										
							PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
							ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService.
									getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
										
							if(clientProjectCompDetails == null){
								throw new CommonCustomException("Unable to fetch project, client and component details from legacy MIS.");
							}else{
								outputBean.setProjectName(clientProjectCompDetails.getProjectName());
							}
							outputBeanList.add(outputBean);
				
						}/*else{
						    	throw new CommonCustomException("No capex requests to display.");
						    }*/
					}
					LOG.endUsecase("View Capex Requests based on PurchaseViewCapexRequestsByDateRangeAndBuIdInputBean.");
				}
			}
			else{
				throw new CommonCustomException("The logged in user is not a Bu head.");
			}
		}else{
			throw new CommonCustomException("The logged in user is inactive.");
		}
		return outputBeanList;
	}
	
	// EOA by Smrithi
	
	//Added by Anil
	
	public Boolean createCapexService(PurchasePOCreateBean capexBean) throws CommonCustomException, DataAccessException 
	{
		PurchaseDatCapexBO capexBo = new PurchaseDatCapexBO();
		PurchaseDatCapexBO previousCapex = new PurchaseDatCapexBO();
		PurchaseDatAssetRequestBO assetReqBo = new PurchaseDatAssetRequestBO();
		PurchaseDatBuBO buBo = new PurchaseDatBuBO();
		PurchaseDatBuBO buBoIt = new PurchaseDatBuBO();
		Boolean success = false;
		Boolean isSysAdmin = false;
		DatEmpDetailBO sysAdminEmpDetailBO = new DatEmpDetailBO();
		PurchaseDatAssetQuoteBO assetQuoteBo = new PurchaseDatAssetQuoteBO();
		try {

			sysAdminEmpDetailBO = empDetailRepository.findByPkEmpId(capexBean.getCreatedBy());
			assetQuoteBo = assetQuoteRepository.findByFkPurchaseRequestIdAndQuoteStatus(
					capexBean.getAssetReqId(), "AGREE");
			assetReqBo = purchaseAssetRequestRepo.findByPkPurchaseRequestId(capexBean.getAssetReqId());
			if(assetReqBo == null){
				throw new CommonCustomException(
						"The asset request Id does't exist, please pass proper id");
			}
			if (sysAdminEmpDetailBO == null) {
				throw new CommonCustomException(
						"Given logged in employee Id does't exist, Please pass proper employee Id");
			}
			if (sysAdminEmpDetailBO.getFkEmpMainStatus() == 2) {
				throw new CommonCustomException(
						"Given logged in employee Id is inactive, Please pass proper employee Id");
			}
			
			String[] arrayOfSysAdminRoles = sysAdminRoles.split(",");
		
			for(String sysAdmin : arrayOfSysAdminRoles){
				if(sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == Short.parseShort(sysAdmin)){
					isSysAdmin = true;
					if(assetReqBo.getPurchaseRequestStatus() == purchaseRequestApprovedByRm){
						if(assetQuoteBo != null){
							previousCapex = capexRepository.findTop1ByOrderByPkCapexIdDesc();
							String capexNumber = previousCapex.getCapexNo();

							//Auto increment of capex number
							String[] capexArray = capexNumber.split("-");

							Integer capexNum = Integer.parseInt(capexArray[2]);
		
							String incrementPoNum = String.valueOf(++capexNum);
							if (incrementPoNum.length() < 3) {
								while (incrementPoNum.length() != 3) {
									incrementPoNum = "0" + incrementPoNum;
								}
							}
							
							String newPoNum;
							Date date = new Date();
							Calendar cal =  new GregorianCalendar();
							cal.setTime(date);
							int newYear = cal.get(Calendar.YEAR);
		
							//For new year capex number starts with 001
							if(!capexArray[1].contains(String.valueOf(newYear))){
								String currentYear = capexArray[1].replace(capexArray[1].substring(3), String.valueOf(newYear));
								
								newPoNum = capexArray[0] + "-" + currentYear + "-"
										+ "001";
							}else{
								newPoNum = capexArray[0] + "-" + capexArray[1] + "-"
										+ incrementPoNum;
							}
			
							capexBo.setCapexNo(newPoNum);
							capexBo.setCreatedOn(new Date());
							capexBo.setCreatedBy(capexBean.getCreatedBy());
							capexBo.setCapexUploaded(purchaseCapexUploaded);
							capexBo = capexRepository.save(capexBo);
			
							assetReqBo.setFkPurchaseCapexId(capexBo.getPkCapexId());
							assetReqBo.setPurchaseRequestStatus(purchaseCreateCapexBySysAdmin);
							assetReqBo.setSysAdminUpdatedOn(new Date());
							assetReqBo.setSysAdminId(capexBean.getCreatedBy());
							assetReqBo.setModifiedBy(capexBean.getCreatedBy());
							assetReqBo.setModifiedOn(new Date());
							assetReqBo.setPriority(capexBean.getAssetPriority());
							purchaseAssetRequestRepo.save(assetReqBo);

							MasBuUnitBO buUnit = businessUnitRepository.findByPkBuUnitId(assetReqBo.getFkBuId());
							
							if(assetQuoteBo.getCostBorneBy().equalsIgnoreCase("BU"))
							{
								buBo.setBuHeadId((short)buUnit.getFkBuHeadEmpId());
								buBo.setFkMasBuId(buUnit.getPkBuUnitId());
								buBo.setCreatedBy(capexBean.getCreatedBy());
								buBo.setCreatedDate(new Date());
								buBo.setFkPurchaseQuoteId(assetQuoteBo.getPkQuoteId());
								buBo.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
								purchaseDatBuRepo.save(buBo);
							}else if(assetQuoteBo.getCostBorneBy().equalsIgnoreCase("IT")){
								//for Nbu It
								MasBuUnitBO nbuUnit = businessUnitRepository.findByPkBuUnitId(purchaseNbuItUnitId);
								buBoIt.setBuHeadId((short)nbuUnit.getFkBuHeadEmpId());
								buBoIt.setFkMasBuId(nbuUnit.getPkBuUnitId());
								buBoIt.setCreatedBy(capexBean.getCreatedBy());
								buBoIt.setCreatedDate(new Date());
								buBoIt.setFkPurchaseQuoteId(assetQuoteBo.getPkQuoteId());
								buBoIt.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
								purchaseDatBuRepo.save(buBoIt);
							}
							
							else if(assetQuoteBo.getCostBorneBy().equalsIgnoreCase("SHARED")){
								
								MasBuUnitBO nbuUnit = businessUnitRepository.findByPkBuUnitId(purchaseNbuItUnitId);
								if(buUnit.getFkBuHeadEmpId() == nbuUnit.getFkBuHeadEmpId()){
									buBo.setBuHeadId((short)buUnit.getFkBuHeadEmpId());
									buBo.setFkMasBuId(buUnit.getPkBuUnitId());
									buBo.setCreatedBy(capexBean.getCreatedBy());
									buBo.setCreatedDate(new Date());
									buBo.setFkPurchaseQuoteId(assetQuoteBo.getPkQuoteId());
									buBo.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
									purchaseDatBuRepo.save(buBo);
								}else{
									buBo.setBuHeadId((short)buUnit.getFkBuHeadEmpId());
									buBo.setFkMasBuId(buUnit.getPkBuUnitId());
									buBo.setCreatedBy(capexBean.getCreatedBy());
									buBo.setCreatedDate(new Date());
									buBo.setFkPurchaseQuoteId(assetQuoteBo.getPkQuoteId());
									buBo.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
									purchaseDatBuRepo.save(buBo);
								
									//for Nbu It
									buBoIt.setBuHeadId((short)nbuUnit.getFkBuHeadEmpId());
									buBoIt.setFkMasBuId(nbuUnit.getPkBuUnitId());
									buBoIt.setCreatedBy(capexBean.getCreatedBy());
									buBoIt.setCreatedDate(new Date());
									buBoIt.setFkPurchaseQuoteId(assetQuoteBo.getPkQuoteId());
									buBoIt.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
									purchaseDatBuRepo.save(buBoIt);
								}
							}
							
							PurchaseDatAssetQuoteBO requestQuoteBO = new PurchaseDatAssetQuoteBO();
							requestQuoteBO = assetQuoteBo;
							LOG.info("requestQuoteBO ::: " + requestQuoteBO.toString());
				
							//PDF file creation
							String buName = assetReqBo.getMasBuUnitBO().getBuUnitName();
							String buHeadName = gsrGoalService.getEmpNameByEmpId(assetReqBo.getMasBuUnitBO().getFkBuHeadEmpId());
							MasBuUnitBO nbuUnit = businessUnitRepository.findByPkBuUnitId(purchaseNbuItUnitId);
							String buItHeadName = gsrGoalService.getEmpNameByEmpId(nbuUnit.getFkBuHeadEmpId());
							
							MasBuUnitBO financeBUBo = businessUnitRepository.findByPkBuUnitId(financeBuId);
							String financeHeadName = gsrGoalService.getEmpNameByEmpId(financeBUBo.getFkBuHeadEmpId());
							String buComponentName = "";
							String projectName = "";
							List<CreatePDFForCapexReportBean> list = new ArrayList<CreatePDFForCapexReportBean>();
							//Added by Kamal Anand
							List<CreatePDFForCapexSharedReportBean> pdfList = new ArrayList<CreatePDFForCapexSharedReportBean>();
							CreatePDFForCapexSharedReportBean createCapexPdfBean = new CreatePDFForCapexSharedReportBean();
							//End of Addition by Kamal Anand
							CreatePDFForCapexReportBean createPDFBean = new CreatePDFForCapexReportBean();
							
							//Need quote object data to get cost
							createPDFBean.setCost(requestQuoteBO.getTotalCost());
							createPDFBean.setCapexNumber(capexBo.getCapexNo());	
							createPDFBean.setCreatedDate(capexBo.getCreatedOn());
							createPDFBean.setLocationName(assetReqBo.getMasLocationParentBO().getLocationParentName());
							
							//Change due to NUC-463 issue raised by Tester
							createPDFBean.setProductDetails(assetReqBo.getPurchaseMasAssetsBO().
									getAssetNameAndVersionOrModel());
							
							createPDFBean.setPurpose(requestQuoteBO.getAssetDescriptionEnteredBySysadmin());
							createPDFBean.setNumberOfUnits(assetReqBo.getNoOfUnits());
							createPDFBean.setFinanceHeadName(financeHeadName);
							createPDFBean.setCurrencyType(requestQuoteBO.getMasCurrencyType().getCurrencyTypeCode());
			
							//Get from leagcy service
							// get component & client details from legacy service
							ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean();
							clientProjCompDetailsBean.setClientId(assetReqBo.getClientId());
							clientProjCompDetailsBean.setProjectId(assetReqBo.getProjectId());
							clientProjCompDetailsBean.setComponentId(assetReqBo.getProjectComponentId());
							clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
							//LOG.info("clientProjCompDetailsBean :: " + clientProjCompDetailsBean);
							PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
							ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService
									.getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
							if (clientProjectCompDetails == null) {
								throw new CommonCustomException("Unable to fetch project, client and component details from legacy MIS.");
							} else {
								buComponentName = clientProjectCompDetails.getComponentName();
								projectName = clientProjectCompDetails.getProjectName();
							}
				
							if(requestQuoteBO.getCostBorneBy().equalsIgnoreCase("SHARED"))
							{
								//Added by Kamal Anand
								createCapexPdfBean.setCost(requestQuoteBO.getTotalCost());
								createCapexPdfBean.setCapexNumber(capexBo.getCapexNo());
								createCapexPdfBean.setCreatedDate(capexBo.getCreatedOn());
								createCapexPdfBean.setLocationName(assetReqBo.getMasLocationParentBO().getLocationParentName());
								createCapexPdfBean.setProductDetails(assetReqBo.getPurchaseMasAssetsBO().
										getAssetNameAndVersionOrModel());
								//createCapexPdfBean.setPurpose(assetReqBo.getBriefDescription());
								createCapexPdfBean.setPurpose(requestQuoteBO.getAssetDescriptionEnteredBySysadmin());
								createCapexPdfBean.setNumberOfUnits(assetReqBo.getNoOfUnits());
								createCapexPdfBean.setBuHeadName(buHeadName);
								createCapexPdfBean.setBuItHeadName(buItHeadName);
								createCapexPdfBean.setCurrencyType(requestQuoteBO.getMasCurrencyType().getCurrencyTypeCode());
								
								createCapexPdfBean.setCostCenter("SHARED");
								createCapexPdfBean.setBuName(assetReqBo.getMasBuUnitBO().getBuUnitName());
								createCapexPdfBean.setItBuName(nbuUnit.getBuUnitName());
								createCapexPdfBean.setComponentNumber(buComponentName);
								createCapexPdfBean.setItComponentNumber(nbuItComponentName);
								createCapexPdfBean.setItBuProjectName(nbuItProjectName);
								createCapexPdfBean.setBuProjectName(projectName);
								createCapexPdfBean.setBuContribution(requestQuoteBO.getBuContribution());
								createCapexPdfBean.setFinanceHeadName(financeHeadName);
								//End of Addition by Kamal Anand
								
							}else if(requestQuoteBO.getCostBorneBy().equalsIgnoreCase("BU"))
							{
								createPDFBean.setBuHeadName(buHeadName);
								//createPDFBean.setItHeadName(buItHeadName);
								createPDFBean.setCostCenter("BU");
								createPDFBean.setBuName(buName);
								createPDFBean.setComponentNumber(buComponentName);
								createPDFBean.setProjectName(projectName);
									
							}else if(requestQuoteBO.getCostBorneBy().equalsIgnoreCase("IT"))
							{
								//createPDFBean.setBuHeadName(buItHeadName);
								createPDFBean.setItHeadName(buItHeadName);
								createPDFBean.setCostCenter("IT");
								createPDFBean.setBuName(nbuUnit.getBuUnitName());
								createPDFBean.setComponentNumber(nbuItComponentName);
								createPDFBean.setProjectName(nbuItProjectName);
							}
							LOG.info("Create Capex ---> createPDFBean ::: " + createPDFBean);
							LOG.info("Create Capex ---> createCapexPdfBean ::: " + createCapexPdfBean); 
							list.add(createPDFBean);
							pdfList.add(createCapexPdfBean);
							
							Calendar calendar = new GregorianCalendar();
							calendar.setTime(assetReqBo.getCreatedDate());
							int year = calendar.get(Calendar.YEAR);
							int month = calendar.get(Calendar.MONTH)+1;
							
							String capexFileName = capexBo.getCapexNo().replace("/","_");
					
						    FileOutputStream fos = null;
							try {
								String filePath = purchaseDocsBasePath + year + File.separator + month + File.separator
													+ assetReqBo.getPkPurchaseRequestId().intValue() + File.separator + "Capex";
								File someFile = new File(purchaseDocsBasePath + year + File.separator + month + File.separator
													+ assetReqBo.getPkPurchaseRequestId().intValue() + File.separator + "Capex");
								
								if (!someFile.exists()) {
									LOG.info("Entering if (!someFile.exists())");
									someFile.mkdirs();
									LOG.info("Exiting if (!someFile.exists())");
								}
				
								filePath = filePath + File.separator + capexFileName + ".pdf";
								someFile = new File(filePath);
								fos = new FileOutputStream(someFile);
				
								
								if(requestQuoteBO.getCostBorneBy().equalsIgnoreCase("SHARED"))
								{
									LOG.info("FOR SHARED");
									LOG.startUsecase("Comming inside shared list");
									LOG.startUsecase("pdfList:"+pdfList);
									generateJasperReportPDF(capexSharedTemplatePath, fos, pdfList);
								} else {
									LOG.info("FOR BU and IT");
									generateJasperReportPDF(capexTemplateBasePath, fos, list);
								}

								LOG.debug("<<<<<<<<<<<< Report Created >>>>>>>>");
				
							} catch (FileNotFoundException e) {
								LOG.debug("Error in creating capex file : " + e);
							} catch (Exception ex) {
								LOG.info("Error ::: " + ex.getMessage());
							}
							finally {
								if (fos != null) {
									try {
										fos.flush();
										fos.close();
									} catch (IOException e) {
										LOG.debug("Error in creating capex file : closing file " + e);
									}
								}
							}
							success = true;
							//Added by Kamal Anand for Notifications
							try{
								notificationService.setCapexCreatedNotificationToBUHeadScreen(capexBean.getAssetReqId());
								notificationService.setAssetRequestApprovedNotificationToEmpScreen(assetReqBo.getRequestorId());
								List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
								if(empDetail != null)
								{
									empDetail.forEach(emp -> {
										try {
											notificationService.setAssetRequestApprovedNotificationToSysAdminScreen(emp.getPkEmpId());
										} catch (Exception e) {
											LOG.info("Exception occured while setting notification in Create Capex");
										}
									});
								}
							} catch(Exception e){
								LOG.info("Exception occured while setting Capex Created Notification");
							}
							//End of Addition by Kamal Anand for Notifications
						}else{
							throw new CommonCustomException("Create the agreed quote before creating capex");
						}
					}else{
						throw new CommonCustomException("To Create capex, asset request data should be in RM approve status, Please pass proper status");
					}
					break;
				}
			}
			if(!isSysAdmin){
				throw new CommonCustomException("The logged in user don't have access to Create Capex. Please pass proper employee id with sysadmin access");
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("Exceptiohn in create capex : "+e.getMessage());
			success = false;
			throw new DataAccessException(e.getMessage());
		}
		
		return success;

	}
	
	//EOA by Anil
	
	@SuppressWarnings("deprecation")
	public Boolean downloadCapex(Integer assetId, HttpServletResponse response)
			throws DataAccessException {

		LOG.entering("download capex service method");
		Boolean isPoDownloaded = false;
		try {

			PurchaseDatAssetRequestBO assetReqBO = purchaseAssetRequestRepo
					.findByPkPurchaseRequestId(assetId);

			String outputFileName = assetReqBO.getPurchaseDatCapexBO()
					.getCapexNo() + ".pdf";
			outputFileName = outputFileName.replace("/", "_");
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(assetReqBO.getCreatedDate());
			int year = calendar.get(Calendar.YEAR);

			String BASEPATH = purchaseDocsBasePath + year + "/"
					+ (assetReqBO.getCreatedDate().getMonth() + 1) + "/"
					+ assetReqBO.getPkPurchaseRequestId() + "/Capex/";
			
			File file = new File(BASEPATH+outputFileName);
			if(file.exists()){
			DownloadFileUtil.downloadFile(response, BASEPATH, outputFileName);
			isPoDownloaded = true;
			}else{
				throw new CommonCustomException("File does't exists in required server path ");
			}

		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}

		LOG.exiting("download capex service method");
		return isPoDownloaded;

	}

	
	
	public PurchaseDatAssetRequestBO rejectCapexBySysAdmin(
			PurchaseRejectCapexInputBean purchaseRejectCapexInputBean)
			throws CommonCustomException {

		LOG.startUsecase("Reject Capex by sysAdmin ");
		PurchaseDatAssetRequestBO rejectCapexOutput = new PurchaseDatAssetRequestBO();
		DatEmpDetailBO sysAdminEmpDetailBO = new DatEmpDetailBO();

		try {

			rejectCapexOutput = purchaseAssetRequestRepo
					.findByPkPurchaseRequestId(purchaseRejectCapexInputBean
							.getPkPurchaseRequestId());

			sysAdminEmpDetailBO = empDetailRepository
					.findByPkEmpId(purchaseRejectCapexInputBean
							.getLoggedInUser());

			if (sysAdminEmpDetailBO == null) {
				throw new CommonCustomException(
						"Given logged in employee Id does't exist, Please pass proper employee Id");
			}
			if (sysAdminEmpDetailBO.getFkEmpMainStatus() == 2) {
				throw new CommonCustomException(
						"Given logged in employee Id is inactive, Please pass proper employee Id");
			}
			if (sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdminProcurment
					.intValue()
					|| sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == systemAdmin
							.intValue()
					|| sysAdminEmpDetailBO.getFkEmpRoleId().shortValue() == seniorSystemAdmin
							.intValue()) {
				if (rejectCapexOutput == null) {
					throw new CommonCustomException(
							"Purchase request id does not exist, Please pass proper request id");
				} else if (rejectCapexOutput.getPurchaseRequestStatus() == purchaseRejectCapexBySysAdmin) {
					throw new CommonCustomException(
							"Capex is already rejected.");
				} else if (rejectCapexOutput.getPurchaseRequestStatus() == purchaseCapexCreatedBySysAdminStatus ||
						rejectCapexOutput.getPurchaseRequestStatus() == requestCapexApprovedByBuHeadStatus) {

					rejectCapexOutput
							.setPkPurchaseRequestId(purchaseRejectCapexInputBean
									.getPkPurchaseRequestId());
					rejectCapexOutput
							.setSysAdminComment(purchaseRejectCapexInputBean
									.getClosureComment());
					rejectCapexOutput.setSysAdminUpdatedOn(new Date());
					rejectCapexOutput
							.setSysAdminId(purchaseRejectCapexInputBean
									.getLoggedInUser());
					rejectCapexOutput
							.setPurchaseRequestStatus(purchaseRejectCapexBySysAdmin);
					purchaseAssetRequestRepo.save(rejectCapexOutput);
					if (rejectCapexOutput.getFkPurchaseCapexId() != null) {
						PurchaseDatCapexBO purchaseDatCapexBO = capexRepository
								.findByPkCapexId(rejectCapexOutput
										.getFkPurchaseCapexId());
						purchaseDatCapexBO
								.setModifiedBy(purchaseRejectCapexInputBean
										.getLoggedInUser());
						purchaseDatCapexBO.setModifiedOn(new Date());
						capexRepository.save(purchaseDatCapexBO);
					}

				} else {
					throw new CommonCustomException(
							"In order to reject Capex, capex should be in created by System Admin status. Please pass proper status");
				}
			} else {
				throw new CommonCustomException(
						"The logged in user don't have access to cancel Capex. Please pass proper employee id with sysadmin access");
			}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}

		//Added by Kamal Anand for Notifications
		try{
		notificationService.setCapexCreatedNotificationToBUHeadScreen(rejectCapexOutput.getPkPurchaseRequestId());
		notificationService.setCapexRejectedNotificationToBuHeadScreen(rejectCapexOutput.getPkPurchaseRequestId());
		notificationService.setCapexRejectedNotificationToRMScreen(rejectCapexOutput.getRmToApprove());
		notificationService.setCapexRejectedNotificationToEmployeeScreen(rejectCapexOutput.getRequestorId());
		List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
		if(empDetail != null)
		{
			empDetail.forEach(emp -> {
				try {
					notificationService.setCapexRejectedNotificationToSysAdminScreen(emp.getPkEmpId());
				} catch (Exception e) {
					LOG.info("Exception occured while setting notification in Reject Capex");
				}
			});
		}
		} catch(Exception e) {
			LOG.info("Exception occurred while setting Capex Rejected by BU Head Notification");
		}
		//End of Addition by Kamal Anand for Notifications
		
		LOG.endUsecase("Reject Capex by sysAdmin ");

		return rejectCapexOutput;

	}
	
	//Added by Kamal Anand
	public void generateJasperReportPDFPO(String jasperReportName, FileOutputStream outputStream,
			@SuppressWarnings("rawtypes") List reportList) throws IOException {
		
		try {

			LOG.startUsecase("reportList:"+reportList);;
			File poTemplate = new File(jasperReportName);

			LOG.debug("File is present : " + poTemplate.exists());

			JasperDesign jd = JRXmlLoader.load(poTemplate);
			
			JasperReport jasperReport = JasperCompileManager.compileReport(jd);
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null,
					new JRBeanCollectionDataSource(reportList));
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

		} catch (Exception e) {
			LOG.debug("Error in writing to jasper report : " + e);

		}
		
	}
	//End of Addition by Kamal Anand

	public void generateJasperReportPDF(String jasperReportName, FileOutputStream outputStream,
			@SuppressWarnings("rawtypes") List reportList) {
		
		try {

			File capexTemplate = new File(jasperReportName);
			LOG.info("File is present : " + capexTemplate.exists());

			JasperDesign jd = JRXmlLoader.load(capexTemplate);
			
			LOG.startUsecase("Report List in generate report pdf:"+reportList);
			
			LOG.startUsecase("Template Path:"+jasperReportName);

			JasperReport jasperReport = JasperCompileManager.compileReport(jd);
			LOG.startUsecase("jasperReport:"+jasperReport);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null,
					new JRBeanCollectionDataSource(reportList));
			LOG.startUsecase("jasperPrint::"+jasperPrint);

			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
			LOG.info("----------END ----------------------");

		} catch (Exception e) {
			LOG.debug("Error in writing to jasper report : " + e);

		}
	}
	
	
	
	//Added by Shyam
	public boolean updateCapexBySystemAdmin(PurchaseUpdateCapexInputBean updateCapexInputBean) 
			throws CommonCustomException
	{
		boolean updateSameQuoteFlag = false;
		boolean validationBuheadApprovalFlag = false;
		//For loggedInUser validation
		DatEmpDetailBO loggedInUserDetail = new DatEmpDetailBO();
		try{
			loggedInUserDetail = empDetailRepo.findByPkEmpId(updateCapexInputBean.getLoggedInUser());
		}catch(Exception ex){
			throw new CommonCustomException("Failed to get logged in user detail. Kindly get in touch with Nucleus Support team.");
		}
		
		if(loggedInUserDetail == null)
			throw new CommonCustomException("User detail not found.");
		
		if(loggedInUserDetail.getFkEmpRoleId().shortValue() !=  seniorSystemAdmin.intValue() &&
				loggedInUserDetail.getFkEmpRoleId().shortValue() != systemAdmin.intValue() &&
				loggedInUserDetail.getFkEmpRoleId().shortValue() != systemAdminProcurment.intValue())
		{
			throw new CommonCustomException("You are neither have a role of 'Senior System Admin' nor 'System Admin' nor 'System Admin Procurement'.");
		}
		
		//Get AssetRequest BO object from capex id.
		PurchaseDatAssetRequestBO requestBo = new PurchaseDatAssetRequestBO();
		try{
			requestBo = purchaseAssetRequestRepo.findByPkPurchaseRequestId(updateCapexInputBean.getAssetRequestId());
		}catch(Exception ex){
			throw new CommonCustomException("Failed to get asset request detail from backend. Kindly get in touch with Nucleus Support team.");
		}
			
		if(requestBo == null){
			throw new CommonCustomException("No asset request found.");
		}
		
		if(requestBo.getPurchaseRequestStatus().intValue() != purchaseCapexCreatedBySysAdminStatus)
		{
			throw new CommonCustomException("The status of this request is notmatch with the expected one. Kindly get in touch with Nucleus Support team.");
		}
			
		//Get capex bo object and update it
		PurchaseDatCapexBO capexBo = new PurchaseDatCapexBO();
		try{
			capexBo = capexRepository.findByPkCapexId(requestBo.getFkPurchaseCapexId());
		}catch(Exception ex){
			throw new CommonCustomException("Failed to get capex detail from backend. Kindly get in touch with Nucleus Support team.");
		}
			
		if(capexBo == null){
			throw new CommonCustomException("No capex found.");
		}
			
		//Update capexBo and save it
		capexBo.setModifiedBy(updateCapexInputBean.getLoggedInUser());
		capexBo.setModifiedOn(new Date());
		capexRepository.save(capexBo);
			
		//Get previous and updated quote details
		PurchaseDatAssetQuoteBO previousQuoteBo = new PurchaseDatAssetQuoteBO();
		try{
			previousQuoteBo = assetQuoteRepository.findByPkQuoteId(updateCapexInputBean.getPreviousAgreedQuoteId());
		}catch(Exception ex){
			throw new CommonCustomException("Failed to get previous quote detail from backend. Kindly get in touch with Nucleus Support team.");
		}
			
		if(previousQuoteBo ==  null){
			throw new CommonCustomException("No previous quote found.");
		}
			
		PurchaseDatAssetQuoteBO updatedQuoteBo = new PurchaseDatAssetQuoteBO();
		try{
			updatedQuoteBo = assetQuoteRepository.findByPkQuoteId(updateCapexInputBean.getUpdatedAgreedQuoteId());
		}catch(Exception ex){
			throw new CommonCustomException("Failed to get updated quote detail from backend. Kindly get in touch with Nucleus Support team.");
		}
			
		if(updatedQuoteBo == null){
			throw new CommonCustomException("Updated quote not found."); 
		}
		
		//Fetch NBU-IT business unit detail
		MasBuUnitBO nbuITBo = new MasBuUnitBO();
		try{
			nbuITBo = businessUnitRepository.findByPkBuUnitId(nbuITBusinessUnitId);
		}catch(Exception ex){
			throw new CommonCustomException("Failed to get NBU-IT business unit detail from backend. Kindly get in touch with Nucleus Support team.");
		}
			
		if(nbuITBo == null){
			throw new CommonCustomException("NBU-IT business unit detail not found.");
		}
			
		//Requestor BU details
		MasBuUnitBO requestorBuDetail = new MasBuUnitBO();
		try{
			requestorBuDetail = businessUnitRepository.findByPkBuUnitId(requestBo.getFkBuId());
		}catch(Exception ex){
			throw new CommonCustomException("Failed to get requestor business unit detail from backend. Kindly get in touch with Nucleus Support team.");
		}
			
		if(requestorBuDetail == null){
			throw new CommonCustomException("Requestor business unit detail not found.");
		}
			
		//Purchase bu records details		
		List<PurchaseDatBuBO> purchaseDatBuList = new ArrayList<PurchaseDatBuBO>();
		try{
			purchaseDatBuList = purchaseDatBuRepo.findByFkPurchaseQuoteId(previousQuoteBo.getPkQuoteId());
		}catch(Exception ex){
			throw new CommonCustomException("Failed to get list of bu entries detail from backend. Kindly get in touch with Nucleus Support team.");
		}
			
		if(purchaseDatBuList.isEmpty()){
			throw new CommonCustomException("No business unit record found.");
		}
			
		for (PurchaseDatBuBO purchaseDatBuBO4 : purchaseDatBuList) 
		{
			if(!purchaseDatBuBO4.getFkPurchaseDatBuStatus().equalsIgnoreCase("WAITING FOR APPROVAL") )
			{
				validationBuheadApprovalFlag = true;
				break;
			}
		}
		
		if(validationBuheadApprovalFlag){
			throw new CommonCustomException("This capex already has been approved by BU Head. Hence, it is not allowing to do update.");
		}
		
		//if it is Same
		if(previousQuoteBo.getPkQuoteId().intValue() == updatedQuoteBo.getPkQuoteId().intValue())
		{
			updateSameQuoteFlag = true;
		}
		
		//Update the quote properties and save it in DB
		updatedQuoteBo.setCostBorneBy(updateCapexInputBean.getCostBorneBy());
		
		if(updateCapexInputBean.getCostBorneBy().equalsIgnoreCase("SHARED"))
		{
			updatedQuoteBo.setBuContribution(updateCapexInputBean.getBuContribution());
		}else{
			updatedQuoteBo.setBuContribution(updateCapexInputBean.getTotalCost());
		}
		updatedQuoteBo.setDeliveryDate(updateCapexInputBean.getDeliveryDate());
		updatedQuoteBo.setFkSupplierAddress(updateCapexInputBean.getSupplierId());
		updatedQuoteBo.setIndividualLicenseCost(updateCapexInputBean.getIndividualLicenseCost());
		updatedQuoteBo.setModifiedBy(updateCapexInputBean.getLoggedInUser());
		updatedQuoteBo.setModifiedDate(new Date());
		
		//Added by Shyam for new Change Request (Manohar)
		if(updateCapexInputBean.getAssetDescriptionEnteredBySysadmin().isEmpty())
			throw new CommonCustomException("Please enter asset description / specification");
		else
			updatedQuoteBo.setAssetDescriptionEnteredBySysadmin(updateCapexInputBean.
					getAssetDescriptionEnteredBySysadmin());
		//EOA
				
		if(updatedQuoteBo.getFkCurrencyType() != null){
			if(updateCapexInputBean.getCurrencyType() != null){
				updatedQuoteBo.setFkCurrencyType(updateCapexInputBean.getCurrencyType());
			}else{
				updatedQuoteBo.setFkCurrencyType(updatedQuoteBo.getFkCurrencyType());
			}
		}else{
			throw new CommonCustomException("Currency type of quote is not found.");
		}
				
		updatedQuoteBo.setQuoteStatus("AGREE");
		updatedQuoteBo.setTotalCost(updateCapexInputBean.getTotalCost());
		
		if(updateCapexInputBean.getVendorQuoteReferenceNo() != null)
		{
			updatedQuoteBo.setVendorQuoteReferenceNo(updateCapexInputBean.getVendorQuoteReferenceNo());
		}	
		//Updating the new selected quote in DB
		updatedQuoteBo = assetQuoteRepository.save(updatedQuoteBo);
		
		
		//For updating same quote
		if(!updateSameQuoteFlag)
		{
			previousQuoteBo.setQuoteStatus("DISAGREE");
			previousQuoteBo.setModifiedBy(updateCapexInputBean.getLoggedInUser());
			previousQuoteBo.setModifiedDate(new Date());
			//If any earlier default quote is there, then update the status of that quote to "DISAGREE"
			assetQuoteRepository.save(previousQuoteBo);
		}
		
		//Need to check bu head are same or not for It and BU
		boolean buHeadAndItHeadIsSame = false;				
		MasBuUnitBO masBuUnitBo = requestBo.getMasBuUnitBO();
		
		for (PurchaseDatBuBO purchaseDatBuBO : purchaseDatBuList) 
		{
			if(masBuUnitBo.getPkBuUnitId() == nbuITBo.getPkBuUnitId() &&
				purchaseDatBuBO.getBuHeadId() == masBuUnitBo.getFkBuHeadEmpId())
			{
				buHeadAndItHeadIsSame = true;
				break;
			}else if(masBuUnitBo.getPkBuUnitId() != nbuITBo.getPkBuUnitId() &&
					masBuUnitBo.getFkBuHeadEmpId() == nbuITBo.getFkBuHeadEmpId())
			{
				buHeadAndItHeadIsSame = true;
				break;
			}
		}//End of for loop
				
		LOG.info("buHeadAndItHeadIsSame :: " + buHeadAndItHeadIsSame);
		
		
		//Created this object for the issue (In pdf, currency type is not updating)
		PurchaseDatAssetQuoteBO modifiedQuoteDetail = new PurchaseDatAssetQuoteBO();
		try{
			modifiedQuoteDetail = assetQuoteRepository.findByPkQuoteId(updatedQuoteBo.getPkQuoteId());
		}catch(Exception ex){
			throw new CommonCustomException("Unable to get quote details from back end.");
		}
		
		LOG.info("modifiedQuoteDetail :: " + modifiedQuoteDetail.toString());
		
		//Update PurchaseDatBuBO object
		if(previousQuoteBo.getCostBorneBy().equalsIgnoreCase("SHARED"))
		{
			//For Shared
			if(modifiedQuoteDetail.getCostBorneBy().equalsIgnoreCase("SHARED"))
			{
				for (PurchaseDatBuBO purchaseDatBuBO : purchaseDatBuList) 
				{
					purchaseDatBuBO.setModifiedBy(updateCapexInputBean.getLoggedInUser());
					purchaseDatBuBO.setModifiedDate(new Date());
					purchaseDatBuBO.setFkPurchaseQuoteId(modifiedQuoteDetail.getPkQuoteId());
					purchaseDatBuRepo.save(purchaseDatBuBO);
				}
			}else if(modifiedQuoteDetail.getCostBorneBy().equalsIgnoreCase("BU"))
			{
				for (PurchaseDatBuBO purchaseDatBuBO1 : purchaseDatBuList) 
				{
					if(purchaseDatBuBO1.getFkMasBuId() != nbuITBusinessUnitId)
					{
						purchaseDatBuBO1.setModifiedBy(updateCapexInputBean.getLoggedInUser());
						purchaseDatBuBO1.setModifiedDate(new Date());
						purchaseDatBuBO1.setFkPurchaseQuoteId(modifiedQuoteDetail.getPkQuoteId());
						purchaseDatBuRepo.save(purchaseDatBuBO1);
					}else{
						if(!buHeadAndItHeadIsSame && purchaseDatBuBO1.getFkMasBuId() == nbuITBusinessUnitId)
						{//Added by Shyam
							LOG.info("Change from SHARED to BU");
							purchaseDatBuRepo.delete(purchaseDatBuBO1);
						}else{
							if(buHeadAndItHeadIsSame)
							{
								purchaseDatBuBO1.setModifiedBy(updateCapexInputBean.getLoggedInUser());
								purchaseDatBuBO1.setModifiedDate(new Date());
								purchaseDatBuBO1.setFkPurchaseQuoteId(modifiedQuoteDetail.getPkQuoteId());
								purchaseDatBuRepo.save(purchaseDatBuBO1);
							}
						}
					}
				}
			}else if(modifiedQuoteDetail.getCostBorneBy().equalsIgnoreCase("IT"))
			{
				for (PurchaseDatBuBO purchaseDatBuBO2 : purchaseDatBuList) 
				{
					if(purchaseDatBuBO2.getFkMasBuId() == nbuITBusinessUnitId)
					{
						purchaseDatBuBO2.setModifiedBy(updateCapexInputBean.getLoggedInUser());
						purchaseDatBuBO2.setModifiedDate(new Date());
						purchaseDatBuBO2.setFkPurchaseQuoteId(modifiedQuoteDetail.getPkQuoteId());
						purchaseDatBuRepo.save(purchaseDatBuBO2);
					}else{
						if(!buHeadAndItHeadIsSame && purchaseDatBuBO2.getFkMasBuId() != nbuITBusinessUnitId){//Added by Shyam
							LOG.info("Change from SHARED to IT");
							purchaseDatBuRepo.delete(purchaseDatBuBO2);
						}
					}
				}
			}else{
				throw new CommonCustomException("Updated quotes cost borne neither by 'SHARED' nor 'BU' nor 'IT'. ");
			}
				
		}
		//------------------------------------------------------------------------------------------
		else if(previousQuoteBo.getCostBorneBy().equalsIgnoreCase("BU"))
		{
			//For Shared
			if(modifiedQuoteDetail.getCostBorneBy().equalsIgnoreCase("SHARED"))
			{
				for (PurchaseDatBuBO purchaseDatBuBO : purchaseDatBuList) 
				{
					purchaseDatBuBO.setModifiedBy(updateCapexInputBean.getLoggedInUser());
					purchaseDatBuBO.setModifiedDate(new Date());
					purchaseDatBuBO.setFkPurchaseQuoteId(modifiedQuoteDetail.getPkQuoteId());
					purchaseDatBuRepo.save(purchaseDatBuBO);
				}
					
				//Add one more record for NBU-IT head approval
				//If BU head and NBU-IT head are same then store only one record in purchase_dat_bu
				if(requestorBuDetail.getFkBuHeadEmpId() != nbuITBo.getFkBuHeadEmpId())
				{
					PurchaseDatBuBO nbuITRecord = new PurchaseDatBuBO();
					nbuITRecord.setBuHeadId((short) nbuITBo.getFkBuHeadEmpId());
					nbuITRecord.setCreatedBy(updateCapexInputBean.getLoggedInUser());
					nbuITRecord.setCreatedDate(new Date());
					nbuITRecord.setFkMasBuId(nbuITBusinessUnitId);
					nbuITRecord.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
					nbuITRecord.setFkPurchaseQuoteId(modifiedQuoteDetail.getPkQuoteId());
					nbuITRecord.setModifiedBy(updateCapexInputBean.getLoggedInUser());
					nbuITRecord.setModifiedDate(new Date());
					purchaseDatBuRepo.save(nbuITRecord);//Insert new record for NBU-IT
				}
			}else if(modifiedQuoteDetail.getCostBorneBy().equalsIgnoreCase("BU"))
			{
				for (PurchaseDatBuBO purchaseDatBuBO1 : purchaseDatBuList) 
				{
					purchaseDatBuBO1.setModifiedBy(updateCapexInputBean.getLoggedInUser());
					purchaseDatBuBO1.setModifiedDate(new Date());
					purchaseDatBuBO1.setFkPurchaseQuoteId(modifiedQuoteDetail.getPkQuoteId());
					purchaseDatBuRepo.save(purchaseDatBuBO1);
				}
			}else if(modifiedQuoteDetail.getCostBorneBy().equalsIgnoreCase("IT"))
			{
				for (PurchaseDatBuBO purchaseDatBuBO2 : purchaseDatBuList) 
				{
					purchaseDatBuBO2.setBuHeadId((short) nbuITBo.getFkBuHeadEmpId());
					purchaseDatBuBO2.setFkMasBuId(nbuITBusinessUnitId);
					purchaseDatBuBO2.setModifiedBy(updateCapexInputBean.getLoggedInUser());
					purchaseDatBuBO2.setModifiedDate(new Date());
					purchaseDatBuBO2.setFkPurchaseQuoteId(modifiedQuoteDetail.getPkQuoteId());
					purchaseDatBuRepo.save(purchaseDatBuBO2);
				}
			}else{
				throw new CommonCustomException("Updated quotes cost borne neither by 'SHARED' nor 'BU' nor 'IT'. ");
			}
			}
			//------------------------------------------------------------------------------------------
			else if(previousQuoteBo.getCostBorneBy().equalsIgnoreCase("IT"))
			{
				//For Shared
				if(modifiedQuoteDetail.getCostBorneBy().equalsIgnoreCase("SHARED"))
				{
					for (PurchaseDatBuBO purchaseDatBuBO : purchaseDatBuList) 
					{
						purchaseDatBuBO.setModifiedBy(updateCapexInputBean.getLoggedInUser());
						purchaseDatBuBO.setModifiedDate(new Date());
						purchaseDatBuBO.setFkPurchaseQuoteId(modifiedQuoteDetail.getPkQuoteId());
						purchaseDatBuRepo.save(purchaseDatBuBO);
					}
					
					//Add one more record for BU head approval
					//if(requestorBuDetail.getPkBuUnitId() != nbuITBusinessUnitId)
					if(requestorBuDetail.getFkBuHeadEmpId() != nbuITBo.getFkBuHeadEmpId())
					{
						PurchaseDatBuBO buRecord = new PurchaseDatBuBO();
						buRecord.setBuHeadId( Integer.valueOf(requestorBuDetail.getFkBuHeadEmpId()).shortValue());
						buRecord.setCreatedBy(updateCapexInputBean.getLoggedInUser());
						buRecord.setCreatedDate(new Date());
						buRecord.setFkMasBuId(requestorBuDetail.getPkBuUnitId());
						buRecord.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
						buRecord.setFkPurchaseQuoteId(modifiedQuoteDetail.getPkQuoteId());
						//buRecord.setModifiedBy(updateCapexInputBean.getLoggedInUser());
						//buRecord.setModifiedDate(new Date());
						purchaseDatBuRepo.save(buRecord);//Insert new record for BU
					}/*else{
						throw new CommonCustomException("Requestor's buisness unit name should not be NBU-IT.");
					}*/
				}else if(modifiedQuoteDetail.getCostBorneBy().equalsIgnoreCase("BU"))
				{
					for (PurchaseDatBuBO purchaseDatBuBO1 : purchaseDatBuList) 
					{
						purchaseDatBuBO1.setFkMasBuId(requestorBuDetail.getPkBuUnitId());
						purchaseDatBuBO1.setModifiedBy(updateCapexInputBean.getLoggedInUser());
						purchaseDatBuBO1.setBuHeadId(Integer.valueOf(requestorBuDetail.getFkBuHeadEmpId()).shortValue());
						purchaseDatBuBO1.setModifiedDate(new Date());
						purchaseDatBuBO1.setFkPurchaseQuoteId(modifiedQuoteDetail.getPkQuoteId());
						purchaseDatBuRepo.save(purchaseDatBuBO1);
					}
				}else if(modifiedQuoteDetail.getCostBorneBy().equalsIgnoreCase("IT"))
				{
					for (PurchaseDatBuBO purchaseDatBuBO2 : purchaseDatBuList) 
					{
						//purchaseDatBuBO2.setBuHeadId((short) nbuITBo.getFkBuHeadEmpId());
						//purchaseDatBuBO2.setFkMasBuId(nbuITBusinessUnitId);
						purchaseDatBuBO2.setModifiedBy(updateCapexInputBean.getLoggedInUser());
						purchaseDatBuBO2.setModifiedDate(new Date());
						purchaseDatBuBO2.setFkPurchaseQuoteId(modifiedQuoteDetail.getPkQuoteId());
						purchaseDatBuRepo.save(purchaseDatBuBO2);
					}
				}else{
					throw new CommonCustomException("Updated quotes cost borne neither by 'SHARED' nor 'BU' nor 'IT'. ");
				}
			}
			
			
			//File upload and delete part starts from here
			//PDF file creation
			String buName = requestorBuDetail.getBuUnitName();
			String buHeadName = gsrGoalService.getEmpNameByEmpId(requestBo.getMasBuUnitBO().getFkBuHeadEmpId());
			MasBuUnitBO nbuUnit = businessUnitRepository.findByPkBuUnitId(purchaseNbuItUnitId);
			String buItHeadName = gsrGoalService.getEmpNameByEmpId(nbuUnit.getFkBuHeadEmpId());
			
			MasBuUnitBO financeBUBo = businessUnitRepository.findByPkBuUnitId(financeBuId);
			String financeHeadName = gsrGoalService.getEmpNameByEmpId(financeBUBo.getFkBuHeadEmpId());
			
			String buComponentName = "";
			String projectName = "";
			List<CreatePDFForCapexReportBean> list = new ArrayList<CreatePDFForCapexReportBean>();
			List<CreatePDFForCapexSharedReportBean> pdfList = new ArrayList<CreatePDFForCapexSharedReportBean>();
			CreatePDFForCapexSharedReportBean createCapexPdfBean = new CreatePDFForCapexSharedReportBean();
			CreatePDFForCapexReportBean createPDFBean = new CreatePDFForCapexReportBean();
			
			//Need quote object data to get cost
			createPDFBean.setCost(modifiedQuoteDetail.getTotalCost());
			createPDFBean.setCapexNumber(capexBo.getCapexNo());
				
			createPDFBean.setCreatedDate(capexBo.getCreatedOn());
			createPDFBean.setLocationName(requestBo.getMasLocationParentBO().getLocationParentName());
			createPDFBean.setProductDetails(requestBo.getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());
			//createPDFBean.setPurpose(requestBo.getBriefDescription());
			createPDFBean.setPurpose(modifiedQuoteDetail.getAssetDescriptionEnteredBySysadmin());
			createPDFBean.setNumberOfUnits(requestBo.getNoOfUnits());
			createPDFBean.setFinanceHeadName(financeHeadName);
			createPDFBean.setCurrencyType(modifiedQuoteDetail.getMasCurrencyType().getCurrencyTypeCode());
			
			//Get from leagcy service
			// get component & client details from legacy service
			ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean();
			clientProjCompDetailsBean.setClientId(requestBo.getClientId());
			clientProjCompDetailsBean.setProjectId(requestBo.getProjectId());
			clientProjCompDetailsBean.setComponentId(requestBo.getProjectComponentId());
		//	LOG.info("clientProjCompDetailsBean :: " + clientProjCompDetailsBean);
			clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
			PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
			ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService
					.getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
			if (clientProjectCompDetails == null) {
				throw new CommonCustomException("Unable to fetch project, client and component details from legacy MIS.");
			} else {
				buComponentName = clientProjectCompDetails.getComponentName();
				projectName = clientProjectCompDetails.getProjectName();
			}
				
			if(updatedQuoteBo.getCostBorneBy().equalsIgnoreCase("SHARED"))
			{
				createCapexPdfBean.setCost(modifiedQuoteDetail.getTotalCost());
				createCapexPdfBean.setCapexNumber(capexBo.getCapexNo());
				
				createCapexPdfBean.setCreatedDate(capexBo.getCreatedOn());
				createCapexPdfBean.setLocationName(requestBo.getMasLocationParentBO().getLocationParentName());
				createCapexPdfBean.setProductDetails(requestBo.getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());
				//createCapexPdfBean.setPurpose(requestBo.getBriefDescription());
				createCapexPdfBean.setPurpose(modifiedQuoteDetail.getAssetDescriptionEnteredBySysadmin());
				createCapexPdfBean.setNumberOfUnits(requestBo.getNoOfUnits());
				createCapexPdfBean.setBuHeadName(buHeadName);
				createCapexPdfBean.setBuItHeadName(buItHeadName);
				
				createCapexPdfBean.setCostCenter("SHARED");
				createCapexPdfBean.setBuName(requestBo.getMasBuUnitBO().getBuUnitName());
				createCapexPdfBean.setItBuName(nbuUnit.getBuUnitName());
				createCapexPdfBean.setComponentNumber(buComponentName);
				createCapexPdfBean.setItComponentNumber(nbuItComponentName);
				createCapexPdfBean.setItBuProjectName(nbuItProjectName);
				createCapexPdfBean.setBuProjectName(projectName);
				createCapexPdfBean.setBuContribution(modifiedQuoteDetail.getBuContribution());
				createCapexPdfBean.setFinanceHeadName(financeHeadName);
				createCapexPdfBean.setCurrencyType(modifiedQuoteDetail.getMasCurrencyType().getCurrencyTypeCode());
				
			}else if(updatedQuoteBo.getCostBorneBy().equalsIgnoreCase("BU"))
			{
				createPDFBean.setBuHeadName(buHeadName);
				//createPDFBean.setItHeadName(buItHeadName);
				createPDFBean.setCostCenter("BU");
				createPDFBean.setBuName(buName);
				createPDFBean.setComponentNumber(buComponentName);
				createPDFBean.setProjectName(projectName);
			}
			else if(updatedQuoteBo.getCostBorneBy().equalsIgnoreCase("IT"))
			{
				//createPDFBean.setBuHeadName(buItHeadName);
				createPDFBean.setItHeadName(buItHeadName);
				createPDFBean.setCostCenter("IT");
				createPDFBean.setBuName(nbuUnit.getBuUnitName());
				createPDFBean.setComponentNumber(nbuItComponentName);
				createPDFBean.setProjectName(nbuItProjectName);
			}
			LOG.info("Update Capex ---> createPDFBean :: " + createPDFBean);
			LOG.info("Update Capex ---> createCapexPdfBean :: " + createCapexPdfBean);
			list.add(createPDFBean);
			pdfList.add(createCapexPdfBean);
				
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(requestBo.getCreatedDate());
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH)+1;
			
			String capexFileName = capexBo.getCapexNo().replace("/","_");
			
			FileOutputStream fos = null;
			try {

				String filePath = purchaseDocsBasePath + year + File.separator + month + File.separator
						+ requestBo.getPkPurchaseRequestId().intValue() + File.separator + "Capex";
				File fileDir = new File(purchaseDocsBasePath + year + File.separator + month 
						+ File.separator + requestBo.getPkPurchaseRequestId().intValue() 
						+ File.separator + "Capex");
				
				filePath = filePath + File.separator + capexFileName + ".pdf";
				File capexFile = new File(filePath);
				
				
				if(capexFile.exists())
	        	{
	        		if(!capexFile.delete())
	        			throw new CommonCustomException("Failed to delete the existing capex in server.");
	        	} else {
	        		if (!fileDir.exists()) {
	        			fileDir.mkdirs();
					}
	        		capexFile = new File(filePath);
	        	}	        	
				
				fos = new FileOutputStream(capexFile);

				if(updatedQuoteBo.getCostBorneBy().equalsIgnoreCase("SHARED"))
				{
					LOG.startUsecase("Comming inside shared list");
					LOG.startUsecase("pdfList:"+pdfList);
					generateJasperReportPDF(capexSharedTemplatePath, fos, pdfList);
				} else {
					generateJasperReportPDF(capexTemplateBasePath, fos, list);
				}
				LOG.debug("<<<<<<<<<<<< Report Created >>>>>>>>");

			} catch(FileNotFoundException e) {
				LOG.debug("Error in creating capex file : " + e);
			} catch(Exception ex){
				LOG.info("Error :::: " + ex.getMessage());
			} finally {
				if (fos != null) {
					try {
						fos.flush();
						fos.close();
					} catch (IOException e) {
						LOG.debug("Error in creating capex file : closing file " + e);
					}
				}
			}	
			//Added by Kamal Anand for Notifications
			try{
			notificationService.setCapexUpdatedNotificationToBUHeadScreen(updateCapexInputBean.getAssetRequestId());
			notificationService.setCapexCreatedNotificationToBUHeadScreen(updateCapexInputBean.getAssetRequestId());
			} catch(DataAccessException e) {
				LOG.info("Exception occured while setting Capex Updated Notification");
			}
			//End of Addition by Kamal Anand
			return true;
		}
		
		//EOA by Shyam
	
	
	
	//Added by Shyam for create capex for phase-2
	public boolean createCapexServiceForPhase2(PurchasePOCreatePhase2Bean capexBean, 
			List<MultipartFile> uploadSupportDocs) 
			throws CommonCustomException
	{
		PurchaseDatCapexBO capexBo = new PurchaseDatCapexBO();
		PurchaseDatCapexBO previousCapex = new PurchaseDatCapexBO();
		PurchaseDatAssetRequestBO assetReqBo = new PurchaseDatAssetRequestBO();
		PurchaseDatBuBO buBo = new PurchaseDatBuBO();
		PurchaseDatBuBO buBoIt = new PurchaseDatBuBO();
		boolean success = false;
		boolean uploadDocFlag = false;
		//Boolean isSysAdmin = false;
		DatEmpDetailBO mgrEmpDetailBO = new DatEmpDetailBO();
		PurchaseDatAssetQuoteBO assetQuoteBo = new PurchaseDatAssetQuoteBO();
		
		try {
			mgrEmpDetailBO = empDetailRepository.findByPkEmpId(capexBean.getCreatedBy());
			assetQuoteBo = assetQuoteRepository.findByFkPurchaseRequestIdAndQuoteStatus(
					capexBean.getAssetReqId(), "AGREE");
			assetReqBo = purchaseAssetRequestRepo.findByPkPurchaseRequestId(capexBean.getAssetReqId());
		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch data from backend.");
		}	
		
		if (mgrEmpDetailBO == null) {
			throw new CommonCustomException(
				"Given logged in employee Id does't exist, Please pass proper employee Id");
		}
		
		if (mgrEmpDetailBO.getFkEmpMainStatus() == 2) {
			throw new CommonCustomException(
				"Given logged in employee Id is inactive, Please pass proper employee Id");
		}
			
		if(assetReqBo == null)
			throw new CommonCustomException("No asset request found.");
				
		if(assetReqBo.getRmToApprove().intValue() != capexBean.getCreatedBy().intValue())
			throw new CommonCustomException("You are not reporting manager of this request. Kindly get in touch with Nucleus Support team.");
			
		if(assetQuoteBo == null)
			throw new CommonCustomException("Default quote of this request not found. Kindly get in touch with Nucleus Supoort team.");
			
		if(assetReqBo.getPurchaseRequestStatus().intValue() == requestWaitingRmApprovalStatus.intValue())
		{
			previousCapex = capexRepository.findTop1ByOrderByPkCapexIdDesc();
			String capexNumber = previousCapex.getCapexNo();
			//Auto increment of capex number
			String[] capexArray = capexNumber.split("-");

			Integer capexNum = Integer.parseInt(capexArray[2]);
			
			String incrementPoNum = String.valueOf(++capexNum);
			if (incrementPoNum.length() < 3) {
				while (incrementPoNum.length() != 3) {
					incrementPoNum = "0" + incrementPoNum;
				}
			}
							
			String newPoNum;
			Date date = new Date();
			Calendar cal =  new GregorianCalendar();
			cal.setTime(date);
			int newYear = cal.get(Calendar.YEAR);
		
			//For new year capex number starts with 001
			if(!capexArray[1].contains(String.valueOf(newYear)))
			{
				String currentYear = capexArray[1].replace(capexArray[1].substring(3), String.valueOf(newYear));
				newPoNum = capexArray[0] + "-" + currentYear + "-" + "001";
			}else{
				newPoNum = capexArray[0] + "-" + capexArray[1] + "-" + incrementPoNum;
			}
			
			LOG.info("newPoNum ::: " + newPoNum);
				
			capexBo.setCapexNo(newPoNum);
			capexBo.setCreatedOn(new Date());
			capexBo.setCreatedBy(assetReqBo.getRequestorId());
			capexBo.setCapexUploaded(purchaseCapexUploaded);
			capexBo = capexRepository.save(capexBo);
			
			assetReqBo.setFkPurchaseCapexId(capexBo.getPkCapexId());
			assetReqBo.setPurchaseRequestStatus(purchaseCreateCapexBySysAdmin);
			assetReqBo.setSysAdminUpdatedOn(new Date());
			assetReqBo.setSysAdminId(assetReqBo.getRequestorId());
			assetReqBo.setModifiedBy(capexBean.getCreatedBy());
			assetReqBo.setModifiedOn(new Date());
			assetReqBo.setPriority(capexBean.getAssetPriority());
				
			//Added extra fields for phase-2 only
			assetReqBo.setProjectComponentId(capexBean.getProjectComponentId());
			assetReqBo.setClientId(capexBean.getClientId());
			assetReqBo.setProjectId(capexBean.getProjectId());
			assetReqBo.setIsClientReimbursing(capexBean.getIsClientReimbursing());
				
			//For Support docs
			if(!uploadSupportDocs.isEmpty()){
				assetReqBo.setSupportingDocumentsByClient("UPLOADED");
			}
				
			try {
				assetReqBo = purchaseAssetRequestRepo.save(assetReqBo);
			} catch (Exception e) {
				throw new CommonCustomException("Unable to store your asset request. Kindly contact Nucleus Support team for this issue.");
			}
			
			if (assetReqBo != null) 
			{
				AssetRequestService assetRequestService = new AssetRequestService();
				uploadDocFlag = assetRequestService.uploadSupportDocuments(assetReqBo, uploadSupportDocs);
					
				if(!uploadDocFlag){
					throw new CommonCustomException("Your request has been saved, but failed to store your docs in server. Kindly get in touch with Nucleus Support team.");
				}
			}
			//EOA by Shyam
				
			MasBuUnitBO buUnit = businessUnitRepository.findByPkBuUnitId(assetReqBo.getFkBuId());
							
			if(assetQuoteBo.getCostBorneBy().equalsIgnoreCase("BU"))
			{
				buBo.setBuHeadId((short)buUnit.getFkBuHeadEmpId());
				buBo.setFkMasBuId(buUnit.getPkBuUnitId());
				buBo.setCreatedBy(assetReqBo.getRequestorId());
				buBo.setCreatedDate(new Date());
				buBo.setFkPurchaseQuoteId(assetQuoteBo.getPkQuoteId());
				buBo.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
				purchaseDatBuRepo.save(buBo);
			}else if(assetQuoteBo.getCostBorneBy().equalsIgnoreCase("IT")){
				//for Nbu It
				MasBuUnitBO nbuUnit = businessUnitRepository.findByPkBuUnitId(purchaseNbuItUnitId);
				buBoIt.setBuHeadId((short)nbuUnit.getFkBuHeadEmpId());
				buBoIt.setFkMasBuId(nbuUnit.getPkBuUnitId());
				buBoIt.setCreatedBy(assetReqBo.getRequestorId());
				buBoIt.setCreatedDate(new Date());
				buBoIt.setFkPurchaseQuoteId(assetQuoteBo.getPkQuoteId());
				buBoIt.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
				purchaseDatBuRepo.save(buBoIt);
			}
			else if(assetQuoteBo.getCostBorneBy().equalsIgnoreCase("SHARED"))
			{
				MasBuUnitBO nbuUnit = businessUnitRepository.findByPkBuUnitId(purchaseNbuItUnitId);
				if(buUnit.getFkBuHeadEmpId() == nbuUnit.getFkBuHeadEmpId())
				{
					buBo.setBuHeadId((short)buUnit.getFkBuHeadEmpId());
					buBo.setFkMasBuId(buUnit.getPkBuUnitId());
					buBo.setCreatedBy(assetReqBo.getRequestorId());
					buBo.setCreatedDate(new Date());
					buBo.setFkPurchaseQuoteId(assetQuoteBo.getPkQuoteId());
					buBo.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
					purchaseDatBuRepo.save(buBo);
				}else{
					buBo.setBuHeadId((short)buUnit.getFkBuHeadEmpId());
					buBo.setFkMasBuId(buUnit.getPkBuUnitId());
					buBo.setCreatedBy(assetReqBo.getRequestorId());
					buBo.setCreatedDate(new Date());
					buBo.setFkPurchaseQuoteId(assetQuoteBo.getPkQuoteId());
					buBo.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
					purchaseDatBuRepo.save(buBo);
							
					//for Nbu It
					buBoIt.setBuHeadId((short)nbuUnit.getFkBuHeadEmpId());
					buBoIt.setFkMasBuId(nbuUnit.getPkBuUnitId());
					buBoIt.setCreatedBy(assetReqBo.getRequestorId());
					buBoIt.setCreatedDate(new Date());
					buBoIt.setFkPurchaseQuoteId(assetQuoteBo.getPkQuoteId());
					buBoIt.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
					purchaseDatBuRepo.save(buBoIt);
				}
			}
							
			PurchaseDatAssetQuoteBO requestQuoteBO = new PurchaseDatAssetQuoteBO();
			requestQuoteBO = assetQuoteBo;
								
			//PDF file creation
			String buName = assetReqBo.getMasBuUnitBO().getBuUnitName();
			String buHeadName = gsrGoalService.getEmpNameByEmpId(assetReqBo.getMasBuUnitBO().getFkBuHeadEmpId());
			MasBuUnitBO nbuUnit = businessUnitRepository.findByPkBuUnitId(purchaseNbuItUnitId);
			String buItHeadName = gsrGoalService.getEmpNameByEmpId(nbuUnit.getFkBuHeadEmpId());
						
			MasBuUnitBO financeBUBo = businessUnitRepository.findByPkBuUnitId(financeBuId);
			String financeHeadName = gsrGoalService.getEmpNameByEmpId(financeBUBo.getFkBuHeadEmpId());
			String buComponentName = "";
			String projectName = "";
			List<CreatePDFForCapexReportBean> list = new ArrayList<CreatePDFForCapexReportBean>();
			
			List<CreatePDFForCapexSharedReportBean> pdfList = new ArrayList<CreatePDFForCapexSharedReportBean>();
			CreatePDFForCapexSharedReportBean createCapexPdfBean = new CreatePDFForCapexSharedReportBean();
			CreatePDFForCapexReportBean createPDFBean = new CreatePDFForCapexReportBean();
							
			//Need quote object data to get cost
			createPDFBean.setCost(requestQuoteBO.getTotalCost());
			createPDFBean.setCapexNumber(capexBo.getCapexNo());
			createPDFBean.setCreatedDate(capexBo.getCreatedOn());
			createPDFBean.setLocationName(assetReqBo.getMasLocationParentBO().getLocationParentName());
						
			//Change due to NUC-463 issue raised by Tester
			//createPDFBean.setProductDetails(assetReqBo.getSpecifications());
			createPDFBean.setProductDetails(assetReqBo.getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());
			//createPDFBean.setPurpose(assetReqBo.getBriefDescription());
			createPDFBean.setPurpose(requestQuoteBO.getAssetDescriptionEnteredBySysadmin());
			
			createPDFBean.setNumberOfUnits(assetReqBo.getNoOfUnits());
			createPDFBean.setFinanceHeadName(financeHeadName);
			createPDFBean.setCurrencyType(requestQuoteBO.getMasCurrencyType().getCurrencyTypeCode());
			//Get from leagcy service
			// get component & client details from legacy service
			ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean();
			clientProjCompDetailsBean.setClientId(assetReqBo.getClientId());
			clientProjCompDetailsBean.setProjectId(assetReqBo.getProjectId());
			clientProjCompDetailsBean.setComponentId(assetReqBo.getProjectComponentId());
			clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
			//LOG.info("clientProjCompDetailsBean :: " + clientProjCompDetailsBean);
			PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
			ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService
					.getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
			if (clientProjectCompDetails == null) 
			{
				throw new CommonCustomException("Unable to fetch project, client and component details from legacy MIS.");
			} else {
				buComponentName = clientProjectCompDetails.getComponentName();
				projectName = clientProjectCompDetails.getProjectName();
			}
				
			if(requestQuoteBO.getCostBorneBy().equalsIgnoreCase("SHARED"))
			{
				createCapexPdfBean.setCost(requestQuoteBO.getTotalCost());
				createCapexPdfBean.setCapexNumber(capexBo.getCapexNo());
				createCapexPdfBean.setCreatedDate(capexBo.getCreatedOn());
				createCapexPdfBean.setLocationName(assetReqBo.getMasLocationParentBO().getLocationParentName());
				//createCapexPdfBean.setProductDetails(assetReqBo.getSpecifications());
				createCapexPdfBean.setProductDetails(assetReqBo.getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());
				//createCapexPdfBean.setPurpose(assetReqBo.getBriefDescription());
				createCapexPdfBean.setPurpose(requestQuoteBO.getAssetDescriptionEnteredBySysadmin());
				createCapexPdfBean.setNumberOfUnits(assetReqBo.getNoOfUnits());
				createCapexPdfBean.setBuHeadName(buHeadName);
				createCapexPdfBean.setBuItHeadName(buItHeadName);
				createCapexPdfBean.setCurrencyType(requestQuoteBO.getMasCurrencyType().getCurrencyTypeCode());
								
				createCapexPdfBean.setCostCenter("SHARED");
				createCapexPdfBean.setBuName(assetReqBo.getMasBuUnitBO().getBuUnitName());
				createCapexPdfBean.setItBuName(nbuUnit.getBuUnitName());
				createCapexPdfBean.setComponentNumber(buComponentName);
				createCapexPdfBean.setItComponentNumber(nbuItComponentName);
				createCapexPdfBean.setItBuProjectName(nbuItProjectName);
				createCapexPdfBean.setBuProjectName(projectName);
				createCapexPdfBean.setBuContribution(requestQuoteBO.getBuContribution());
				createCapexPdfBean.setFinanceHeadName(financeHeadName);
			}else if(requestQuoteBO.getCostBorneBy().equalsIgnoreCase("BU"))
			{
				createPDFBean.setBuHeadName(buHeadName);
				//createPDFBean.setItHeadName(buItHeadName);
				createPDFBean.setCostCenter("BU");
				createPDFBean.setBuName(buName);
				createPDFBean.setComponentNumber(buComponentName);
				createPDFBean.setProjectName(projectName);
									
			}else if(requestQuoteBO.getCostBorneBy().equalsIgnoreCase("IT"))
			{
				//createPDFBean.setBuHeadName(buItHeadName);
				createPDFBean.setItHeadName(buItHeadName);
				createPDFBean.setCostCenter("IT");
				createPDFBean.setBuName(nbuUnit.getBuUnitName());
				createPDFBean.setComponentNumber(nbuItComponentName);
				createPDFBean.setProjectName(nbuItProjectName);
			}
				
			LOG.info("Create Capex ---> createPDFBean ::: " + createPDFBean);
			LOG.info("Create Capex ---> createCapexPdfBean ::: " + createCapexPdfBean); 
			list.add(createPDFBean);
			pdfList.add(createCapexPdfBean);
							
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(assetReqBo.getCreatedDate());
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH)+1;
							
			String capexFileName = capexBo.getCapexNo().replace("/","_");
					
			FileOutputStream fos = null;
			try {
				String filePath = purchaseDocsBasePath + year + File.separator + month + File.separator
						+ assetReqBo.getPkPurchaseRequestId().intValue() + File.separator +"Capex";
				File someFile = new File(purchaseDocsBasePath + year + File.separator + month + File.separator
						+ assetReqBo.getPkPurchaseRequestId().intValue() + File.separator +"Capex");
								
				if (!someFile.exists()) {
					someFile.mkdirs();
					LOG.info("Exiting if (!someFile.exists())");
				}
				
				filePath = filePath + File.separator + capexFileName + ".pdf";
				someFile = new File(filePath);
				fos = new FileOutputStream(someFile);
				
				if(requestQuoteBO.getCostBorneBy().equalsIgnoreCase("SHARED"))
				{
					LOG.info("FOR SHARED");
					LOG.startUsecase("Comming inside shared list");
					LOG.startUsecase("pdfList:"+pdfList);
					generateJasperReportPDF(capexSharedTemplatePath, fos, pdfList);
				} else {
					LOG.info("FOR BU and IT");
					generateJasperReportPDF(capexTemplateBasePath, fos, list);
				}

				LOG.debug("<<<<<<<<<<<< Report Created >>>>>>>>");
			} catch (FileNotFoundException e) {
				LOG.debug("Error in creating capex file : " + e);
			} catch (Exception ex) {
				LOG.info("Error ::: " + ex.getMessage());
			}
			
			finally {
				if (fos != null) {
					try {
						fos.flush();
						fos.close();
					} catch (IOException e) {
						LOG.debug("Error in creating capex file : closing file " + e);
					}
				}
			}
			
			success = true;
			//Added by Kamal Anand for Notifications
			try{
				notificationService.setCapexCreatedNotificationToBUHeadScreen(capexBean.getAssetReqId());
				notificationService.setAssetRequestApprovedNotificationToEmpScreen(assetReqBo.getRequestorId());
				List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
				if(empDetail != null)
				{
					empDetail.forEach(emp -> {
						try {
							notificationService.setAssetRequestApprovedNotificationToSysAdminScreen(emp.getPkEmpId());
						} catch (Exception e) {
							LOG.info("Exception occured while setting notification in Create Capex");
						}
					});
				}
			} catch(Exception e){
				LOG.info("Exception occured while setting Capex Created Notification");
			}
			//End of Addition by Kamal Anand for Notifications
		}else{
			throw new CommonCustomException("To Create capex, asset request data should be in Waiting for RM approval status.");
		}
					
		return success;

	}
	//EOA by Shyam
		
	
}
