package com.thbs.mis.purchase.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.dao.BusinessUnitRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.service.GSRGoalService;
import com.thbs.mis.notificationframework.puchasenotification.service.PurchaseNotificationService;
import com.thbs.mis.purchase.bean.ClientAndProjectDetailsFromCompIdBean;
import com.thbs.mis.purchase.bean.ClientAndProjectDetailsInputBean;
import com.thbs.mis.purchase.bean.PurchaseCancelPoBean;
import com.thbs.mis.purchase.bean.PurchaseInputBean;
import com.thbs.mis.purchase.bean.PurchaseOrderDetailsOutputBean;
import com.thbs.mis.purchase.bean.PurchaseOrderRequestOutputBean;
import com.thbs.mis.purchase.bean.PurchaseOrderRequestsBean;
import com.thbs.mis.purchase.bean.QuoteDetailsForPOApprovalBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetQuoteBO;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;
import com.thbs.mis.purchase.bo.PurchaseDatBuBO;
import com.thbs.mis.purchase.dao.PurchaseAssetQuoteRepository;
import com.thbs.mis.purchase.dao.PurchaseAssetRequestRepository;
import com.thbs.mis.purchase.dao.PurchaseCapexRepository;
import com.thbs.mis.purchase.dao.PurchaseDatBuRepository;

@Service
public class PurchaseOrderService {
	private static final AppLog LOG = LogFactory
			.getLog(PurchaseCapexService.class);

	@Autowired
	private PurchaseAssetRequestRepository purchaseAssetRequestRepo;

	@Autowired
	private PurchaseAssetQuoteRepository assetQuoteRepository;

	@Autowired
	PurchaseCapexRepository capexRepository;
	
	@Autowired
	private EmpDetailRepository empDetailRepository;
	
	@Autowired
	private GSRGoalService gsrGoalService;
	
	@Autowired
	private BusinessUnitRepository buRepository;
	
	@Autowired 
	private PurchaseDatBuRepository purchaseDatBuRepo;	

	@Value("${request.waitingRmApproval.status}")
	private Integer requestWaitingRmApprovalStatus;

	@Value("${request.approvedByRm.status}")
	private Integer requestApprovedByRmStatus;

	@Value("${request.capexApprovedByBuHead.status}")
	private Integer requestCapexApprovedByBuHeadStatus;

	@Value("${request.poApprovedByFinance.status}")
	private Integer requestPoApprovedByFinanceStatus;

	@Value("${request.poRejectedByFinance.status}")
	private Integer requestPoRejectedByFinanceStatus;
	
	@Value("${purchase.assetRequest.closed}")
	private Integer assetRequestClosed;  
	
	@Value("${purchase.po.rejectedBySysAdmin}")
	private Integer purchasePoRejectedBySysAdmin;
	
	@Value("${purchase.po.createdBySysAdmin}")
	private Integer purchasePoCreatedBySysAdmin;
	
	@Value("${po.approvals.by.finance.include}")
	private String financeIds;
	
	@Value("${purchase.nbuit.bu.unit.id}")
	private short itBuId;
	
	@Value("${purchase.file.basepath}")
	private String purchaseDocsBasePath;
	
	@Value("${legacy.service.url}")
	private String purchaseLegacyUrl;
	
	
	//Added by Kamal Anand for Notifications
	@Autowired
	private PurchaseNotificationService notificationService;
	
	@Value("${system.admin.procurement.roleId}")
	String sysAdminProcurementRoleId;
	
	@Value("${finance.head.role.Id}")
	String financeHeadRoleId;
	//End of Addition by Kamal Anand for Notifications


	/**
	 * 
	 * @param purchaseOrderRequestsBean
	 * @return List<PurchaseDatAssetRequestBO>
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to View Purchase Order
	 *               requests based on date range and status.
	 */
	public List<PurchaseOrderRequestOutputBean> viewPurchaseOrderRequestsBasedOnDateRange(
			PurchaseOrderRequestsBean purchaseOrderRequestsBean)
			throws CommonCustomException {
		LOG.startUsecase("View Purchase Order requests based on date range and status");
		List<PurchaseOrderRequestOutputBean> requestOutputBeanList = new ArrayList<PurchaseOrderRequestOutputBean>();
		List<PurchaseDatAssetRequestBO> purchaseDatAssetRequestDetailsList = new ArrayList<PurchaseDatAssetRequestBO>();
		if(purchaseOrderRequestsBean.getFromDate() != null && purchaseOrderRequestsBean.getToDate() != null){
			if (purchaseOrderRequestsBean.getFromDate().equals(
					purchaseOrderRequestsBean.getToDate())) {
				throw new CommonCustomException("FromDate and ToDate are same.");
	
			}
	
			if (purchaseOrderRequestsBean.getFromDate().after(
					purchaseOrderRequestsBean.getToDate())) {
				throw new CommonCustomException(
						"FromDate should be less than ToDate.");
	
			}
		}
		Date fromDate = purchaseOrderRequestsBean.getFromDate();
		Calendar c = Calendar.getInstance(); 
		c.setTime(fromDate); 
		c.add(Calendar.DATE, -1);
		fromDate = c.getTime();
		Date toDate = purchaseOrderRequestsBean.getToDate();
		Calendar cal = Calendar.getInstance(); 
		cal.setTime(toDate); 
		cal.add(Calendar.DATE, 1);
		toDate = cal.getTime();
		System.out.println("from"  +fromDate);
		System.out.println("to"  +toDate);
		try {

			purchaseDatAssetRequestDetailsList = purchaseAssetRequestRepo
					.fetchOrderDetailsBasedOnDateAndStatus(fromDate,toDate);

		} catch (Exception ex) {
			throw new CommonCustomException("Failed to fecth details from db.");
		}
        
	
		for (PurchaseDatAssetRequestBO purchaseDatAssetRequestBO : purchaseDatAssetRequestDetailsList) 
		{
			
			if(purchaseDatAssetRequestBO.getPurchaseRequestStatus() == purchasePoCreatedBySysAdmin 
				|| purchaseDatAssetRequestBO.getPurchaseRequestStatus() == requestPoApprovedByFinanceStatus
				|| purchaseDatAssetRequestBO.getPurchaseRequestStatus() == requestPoRejectedByFinanceStatus 
				|| purchaseDatAssetRequestBO.getPurchaseRequestStatus() == assetRequestClosed )
				//|| purchaseDatAssetRequestBO.getPurchaseRequestStatus() == purchasePoRejectedBySysAdmin)
			{
				if(purchaseDatAssetRequestBO.getFkPurchaseCapexId()!=null){
				PurchaseOrderRequestOutputBean requestOutputBean = new PurchaseOrderRequestOutputBean();
				requestOutputBean.setAssetRequestId(purchaseDatAssetRequestBO
					.getPkPurchaseRequestId());
				requestOutputBean.setCreatedOn(purchaseDatAssetRequestBO
					.getCreatedDate());
				requestOutputBean.setBuId(purchaseDatAssetRequestBO.getFkBuId());
				requestOutputBean.setBuName(purchaseDatAssetRequestBO
					.getMasBuUnitBO().getBuUnitName());
				requestOutputBean.setProjectId(purchaseDatAssetRequestBO
					.getProjectId());
				requestOutputBean.setPoDetails(purchaseDatAssetRequestBO.getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());

				// get component & client details from legacy service
				ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean();
				clientProjCompDetailsBean.setClientId(purchaseDatAssetRequestBO
						.getClientId());
				clientProjCompDetailsBean.setProjectId(purchaseDatAssetRequestBO
						.getProjectId());
				clientProjCompDetailsBean.setComponentId(purchaseDatAssetRequestBO
						.getProjectComponentId());
				clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
	
				PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
				ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService
						.getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
	
				if (clientProjectCompDetails == null) {
					throw new CommonCustomException(
							"Unable to fetch project, client and component details from legacy MIS.");
				} else {
					requestOutputBean.setProjectName(clientProjectCompDetails
							.getProjectName());
				}
	
				requestOutputBean.setStatusId(purchaseDatAssetRequestBO
						.getPurchaseRequestStatus());
				requestOutputBean.setStatusInfoString(purchaseDatAssetRequestBO
						.getPurchaseMasRequestStatusBO()
						.getPurchaseRequestStatusName());
				if(purchaseDatAssetRequestBO.getFkPurchasePoId()!=null)
				{
				requestOutputBean.setPoId(purchaseDatAssetRequestBO
						.getFkPurchasePoId());				
				requestOutputBean.setPoNumber(purchaseDatAssetRequestBO
						.getPurchaseDatPoBO().getPoNo());
				}
				requestOutputBeanList.add(requestOutputBean);
			}
			}
		}
		LOG.endUsecase("View Purchase Order requests based on date range and status");
		return requestOutputBeanList;

	}

	public List<PurchaseOrderDetailsOutputBean> getPurchaseOrderDetails(
			int purchaseRequestId) throws CommonCustomException {
		LOG.startUsecase("Get Purchase Order Details");
		PurchaseDatAssetRequestBO purchaseDatAssetRequestDetails = new PurchaseDatAssetRequestBO();
		PurchaseDatAssetQuoteBO purchaseDatAssetQuote = new PurchaseDatAssetQuoteBO();
		List<PurchaseOrderDetailsOutputBean> detailsOutputBeanList = new ArrayList<PurchaseOrderDetailsOutputBean>();
		MasBuUnitBO itBuDetail = null;
		try {
			purchaseDatAssetRequestDetails = purchaseAssetRequestRepo
					.findByPkPurchaseRequestId(purchaseRequestId);
			purchaseDatAssetQuote = assetQuoteRepository.getAgreeQuoteDetails(purchaseRequestId);

		} catch (Exception ex) {
			throw new CommonCustomException("Failed to fecth details from db.");
		}

		if (purchaseDatAssetRequestDetails == null) {
			throw new CommonCustomException("Asset request does not exist.");
		}
		
		if(purchaseDatAssetRequestDetails.getFkPurchaseCapexId() == null)
		{
			throw new CommonCustomException("Capex is not created.");	
		}
			
		PurchaseOrderDetailsOutputBean detailsOutputBean = new PurchaseOrderDetailsOutputBean();
		detailsOutputBean.setPkPurchaseRequestId(purchaseDatAssetRequestDetails.getPkPurchaseRequestId());
		detailsOutputBean.setCapexNumber(purchaseDatAssetRequestDetails
				.getPurchaseDatCapexBO().getCapexNo());
		detailsOutputBean.setCastCenter(purchaseDatAssetRequestDetails
				.getMasBuUnitBO().getBuUnitName());
		detailsOutputBean
				.setIsClientReimbursing(purchaseDatAssetRequestDetails
						.getIsClientReimbursing());
		detailsOutputBean.setLocation(purchaseDatAssetRequestDetails
				.getMasLocationParentBO().getLocationParentName());
		detailsOutputBean.setAssetType(purchaseDatAssetRequestDetails.getPurchaseMasAssetsBO().getAssetType());
		detailsOutputBean.setAssetCreatedDate(purchaseDatAssetRequestDetails.getCreatedDate());
		detailsOutputBean.setAssetRequiredBy(purchaseDatAssetRequestDetails.getAssetRequiredBy());
		detailsOutputBean.setAssetModifiedDate(purchaseDatAssetRequestDetails.getModifiedOn());
		detailsOutputBean.setPurchaseRequestStatus(purchaseDatAssetRequestDetails.getPurchaseRequestStatus());
		detailsOutputBean.setPurchaseRequestStatusName(purchaseDatAssetRequestDetails.getPurchaseMasRequestStatusBO().getPurchaseRequestStatusName());
		
		//Update by Shyam for rejected comments
		if(purchaseDatAssetRequestDetails.getRmComment() != null)
			detailsOutputBean.setRmComment(purchaseDatAssetRequestDetails.getRmComment());
		
		if(purchaseDatAssetRequestDetails.getSysAdminComment() != null)
			detailsOutputBean.setSysAdminComment(purchaseDatAssetRequestDetails.getSysAdminComment());
		
		if(purchaseDatAssetRequestDetails.getFinanceEx_comment() != null)
			detailsOutputBean.setFinanceExComment(purchaseDatAssetRequestDetails.getFinanceEx_comment());
		
		if(purchaseDatAssetRequestDetails.getClosureComment() != null)
			detailsOutputBean.setSysAdminComment(purchaseDatAssetRequestDetails.getClosureComment());
		
		//For BU head reject comments
		List<PurchaseDatBuBO> purchaseDatBuList = new ArrayList<PurchaseDatBuBO>();
		try{
			purchaseDatBuList = purchaseDatBuRepo.findByFkPurchaseQuoteId(purchaseDatAssetQuote.getPkQuoteId());
		}catch(Exception ex){
			throw new CommonCustomException("Failed to fetch bu records, kindly get in touch with Nucleus Support team");
		}
		
		if(purchaseDatBuList.isEmpty())
			throw new CommonCustomException("No bu records found for approve capex");
		
		for (PurchaseDatBuBO purchaseDatBuBO : purchaseDatBuList) 
		{
			if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
			{
				if(purchaseDatBuBO.getBuHeadComment() == null)
					throw new CommonCustomException("Rejected comment not found. KIndly get in touch with Nucleus Support team ");
				else
					detailsOutputBean.setBuHeadComment(purchaseDatBuBO.getBuHeadComment());
				break;
			}
		}
		
		//detailsOutputBean.setBuHeadComment(purchaseDatAssetRequestDetails.getClosureComment());
			
		//Added by pratibha
		detailsOutputBean.setRequestorName(
				gsrGoalService.getEmpNameByEmpId(purchaseDatAssetRequestDetails.getRequestorId().intValue()) + '-'+ purchaseDatAssetRequestDetails.getRequestorId());
				
		if(purchaseDatAssetRequestDetails.getRmToApprove()!=null)
			detailsOutputBean.setReportingManager(
				gsrGoalService.getEmpNameByEmpId(purchaseDatAssetRequestDetails.getRmToApprove().intValue()) + '-'+ purchaseDatAssetRequestDetails.getRmToApprove());
					
		if(purchaseDatAssetRequestDetails.getSysAdminId()!=null)	
			detailsOutputBean.setSysAdminName(
				gsrGoalService.getEmpNameByEmpId(purchaseDatAssetRequestDetails.getSysAdminId().intValue()) + '-'+purchaseDatAssetRequestDetails.getSysAdminId());
					
		if(purchaseDatAssetRequestDetails.getFinanceEx_updated_by()!=null)
			detailsOutputBean.setFinanceHeadName(
				gsrGoalService.getEmpNameByEmpId(purchaseDatAssetRequestDetails.getFinanceEx_updated_by().intValue()) + '-'+purchaseDatAssetRequestDetails.getFinanceEx_updated_by());
					
		if(purchaseDatAssetQuote != null)
		{
			if(purchaseDatAssetQuote.getCostBorneBy().equalsIgnoreCase("shared"))
			{
				 itBuDetail = buRepository.findByPkBuUnitId(itBuId);
						
				 detailsOutputBean.setBuHeadName(
						gsrGoalService.getEmpNameByEmpId(purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId())+ '-'+purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId());
						 
				if(purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId()!= itBuDetail.getFkBuHeadEmpId()){
					detailsOutputBean.setItHeadName(
						gsrGoalService.getEmpNameByEmpId(itBuDetail.getFkBuHeadEmpId()) + '-'+itBuDetail.getFkBuHeadEmpId());
				 }
			}
					
			if(purchaseDatAssetQuote.getCostBorneBy().equalsIgnoreCase("bu"))
			{
				detailsOutputBean.setBuHeadName(
					gsrGoalService.getEmpNameByEmpId(purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId())+ '-'+purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId());
			}
					
			if(purchaseDatAssetQuote.getCostBorneBy().equalsIgnoreCase("IT"))
			{
				 itBuDetail = buRepository.findByPkBuUnitId(itBuId);
							
				detailsOutputBean.setItHeadName(
					gsrGoalService.getEmpNameByEmpId(itBuDetail.getFkBuHeadEmpId()) + '-'+itBuDetail.getFkBuHeadEmpId());
			}
		}
			 

		Calendar calendar = new GregorianCalendar();
		calendar.setTime(purchaseDatAssetRequestDetails.getCreatedDate());
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		String pathForSuprDocSysAdmin = purchaseDocsBasePath+year+"/"+month+"/"+purchaseDatAssetRequestDetails.getPkPurchaseRequestId()+"/Supporting docs/SystemAdmin/";
		String pathForSuprDocEmp = purchaseDocsBasePath+year+"/"+month+"/"+purchaseDatAssetRequestDetails.getPkPurchaseRequestId()+"/Supporting docs/EmpOrRm/";
		String BASEPATHForPo = purchaseDocsBasePath+year+"/"+month+"/"+purchaseDatAssetRequestDetails.getPkPurchaseRequestId()+"/PO/";
		String BASEPATHForInvoice = purchaseDocsBasePath+year+"/"+month+"/"+purchaseDatAssetRequestDetails.getPkPurchaseRequestId()+"/Invoice/";
		String BASEPATHForCapex = purchaseDocsBasePath + year + "/"+ month + "/"+ purchaseDatAssetRequestDetails.getPkPurchaseRequestId() + "/Capex/";
		File fileSupportingDocSysAdmin = new File(pathForSuprDocSysAdmin);
		File fileSupportingDocEmp = new File(pathForSuprDocEmp);
		File fileForPo = new File(BASEPATHForPo);
		File fileForInvoice = new File(BASEPATHForInvoice);
		File fileForCapex = new File(BASEPATHForCapex);
		detailsOutputBean.setIsSupportingDocsAvailable(false);
		detailsOutputBean.setIsPoAvailable(false);
		detailsOutputBean.setIsInvoiceAvailable(false);
		detailsOutputBean.setIsCapexAvailable(false);
				
				
		File[] listOfSupportingDocSysAdmin = fileSupportingDocSysAdmin.listFiles();
				File[] listOfSupportingDocEmp = fileSupportingDocEmp.listFiles();
				File[] listOfPo = fileForPo.listFiles(); 
				File[] listOfInvoice = fileForInvoice.listFiles(); 
				File[] listOfCapex = fileForCapex.listFiles(); 
				
				if( fileSupportingDocEmp.isDirectory()){
				if(listOfSupportingDocEmp.length > 0){
					detailsOutputBean.setIsSupportingDocsAvailable(true);
				}
				}
				if(fileSupportingDocSysAdmin.isDirectory()){
					if(listOfSupportingDocSysAdmin.length > 0){
						detailsOutputBean.setIsSupportingDocsAvailable(true);
					}
					}

				if(fileForPo.isDirectory()){
				if(listOfPo.length > 0){
					detailsOutputBean.setIsPoAvailable(true);
				}
				}
				if(fileForInvoice.isDirectory()){
				if(listOfInvoice.length > 0){
					detailsOutputBean.setIsInvoiceAvailable(true);
				}
				}
				if(fileForCapex.isDirectory()){
				if(listOfCapex.length > 0){
					detailsOutputBean.setIsCapexAvailable(true);
				}
				}
				 //EOA pratibha
			
			//calling of legacy service
			ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean(); 
			clientProjCompDetailsBean.setClientId( purchaseDatAssetRequestDetails.getClientId());
			clientProjCompDetailsBean.setProjectId( purchaseDatAssetRequestDetails.getProjectId());
			clientProjCompDetailsBean.setComponentId(purchaseDatAssetRequestDetails.getProjectComponentId());
			clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);

			PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
			ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService.
			getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);

			if(clientProjectCompDetails == null){
			throw new CommonCustomException("Unable to fetch project, client and component details from legacy MIS.");
			}else{
				detailsOutputBean.setProjectName(clientProjectCompDetails.getProjectName());
				
			}
			 // EOA of legacy service			
			if(purchaseDatAssetRequestDetails.getFkPurchasePoId()!= null)
			detailsOutputBean.setPoNumber(purchaseDatAssetRequestDetails
					.getPurchaseDatPoBO().getPoNo());
			detailsOutputBean.setPurpose(purchaseDatAssetRequestDetails
					.getBriefDescription());
			detailsOutputBean
					.setSoftwareTitleAndVersion(purchaseDatAssetRequestDetails
							.getPurchaseMasAssetsBO()
							.getAssetNameAndVersionOrModel());

			List<QuoteDetailsForPOApprovalBean> quoteBeanList = new ArrayList<QuoteDetailsForPOApprovalBean>();
			List<PurchaseDatAssetQuoteBO> quoteBoList = new ArrayList<PurchaseDatAssetQuoteBO>();
			quoteBoList = assetQuoteRepository
					.findByFkPurchaseRequestId(purchaseRequestId);

			if (!quoteBoList.isEmpty()) {
				for (PurchaseDatAssetQuoteBO purchaseDatAssetQuoteBO : quoteBoList) {
					QuoteDetailsForPOApprovalBean quoteBean = new QuoteDetailsForPOApprovalBean();
					quoteBean
							.setQuoteId(purchaseDatAssetQuoteBO.getPkQuoteId());
					quoteBean.setQuoteStatus(purchaseDatAssetQuoteBO
							.getQuoteStatus());
					quoteBean.setTotalCost(purchaseDatAssetQuoteBO
							.getTotalCost());
					quoteBean.setFkCurrencyType(purchaseDatAssetQuoteBO.getFkCurrencyType());
					quoteBeanList.add(quoteBean);
				}

				detailsOutputBean.setQuoteList(quoteBeanList);
				detailsOutputBeanList.add(detailsOutputBean);
			}
			else
			{
				throw new CommonCustomException("Selected asset request doesn't have default quote");
			}
		
		return detailsOutputBeanList;

	}

	/**
	 * 
	 * @param purchaseRequestId
	 * @return PurchaseDatAssetRequestBO
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to approve PO.
	 */
	public PurchaseDatAssetRequestBO approvePO(PurchaseInputBean approvePoBean)
			throws CommonCustomException {
		LOG.startUsecase("Approve PO");
		PurchaseDatAssetRequestBO purchaseDatAssetRequestDetails = new PurchaseDatAssetRequestBO();
		DatEmpDetailBO empDetailBo = new DatEmpDetailBO();
		try {

			purchaseDatAssetRequestDetails = purchaseAssetRequestRepo
					.findByPkPurchaseRequestId(approvePoBean.getId());

		} catch (Exception ex) {
			throw new CommonCustomException("Failed to fecth details from db.");
		}

		if (purchaseDatAssetRequestDetails == null) {
			throw new CommonCustomException("Asset request does not exist.");
		}

		else if (purchaseDatAssetRequestDetails.getPurchaseRequestStatus() == requestPoApprovedByFinanceStatus) {
			throw new CommonCustomException("PO is already approved.");
		}
		
		else if(purchaseDatAssetRequestDetails.getPurchaseRequestStatus() == purchasePoCreatedBySysAdmin)
		{
			try{
				List<Short> financeIDList = new ArrayList<Short>();
				String[] empIdArray = financeIds.split(",");
				for (String s : empIdArray)
					financeIDList.add(Short.parseShort(s));
				empDetailBo = empDetailRepository.getFinanceRoleDetails(approvePoBean.getLoggedInUser(),financeIDList);
			}
			catch(Exception ex)
			{
				throw new CommonCustomException("Failed to fetch data from db.");
			}
			if(empDetailBo!=null)
			{
		purchaseDatAssetRequestDetails
				.setPurchaseRequestStatus(requestPoApprovedByFinanceStatus);	
		purchaseDatAssetRequestDetails.setFinanceEx_updated_on(new Date());
		purchaseDatAssetRequestDetails.setModifiedOn(new Date());
		purchaseDatAssetRequestDetails.setFinanceEx_updated_by(approvePoBean.getLoggedInUser());
		purchaseAssetRequestRepo.save(purchaseDatAssetRequestDetails);
		//Added by Kamal Anand for Notifications
		
				try{
					notificationService.setPOApprovedNotificationToEmployeeScreen(purchaseDatAssetRequestDetails.getRequestorId());
					List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
					if(empDetail != null)
					{
						empDetail.forEach(emp -> {
							try {
								notificationService.setPOApprovedNotificationtoSysAdminScreen(emp.getPkEmpId());
							} catch (Exception e) {
								LOG.info("Exception occured while setting notification in Approve PO");
							}
						});
					}
					List<DatEmpDetailBO> empDetail1 = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(financeHeadRoleId));
					empDetail1.forEach(emp -> {
						try {
							notificationService.setPOCreatedNotificationToFinanceHeadScreen(emp.getPkEmpId());
						} catch (Exception e) {
							LOG.info("Exception occurred while setting PO Created Notification");
						}
					});
				} catch(Exception e){
					LOG.info("Exception occured while setting notification in Approve PO");
				}
				//End of Addition by Kamal Anand for Notifications
		}
			else
			{
				throw new CommonCustomException("The logged in user is  not Finance Head or Finance Manager or Finance Senior Executive.Hence he/she can not approve PO.");
			}
			}
		else
		{
			throw new CommonCustomException("PO can be approved only if it is created by SystemAdmin.");
		}
		LOG.endUsecase("Approve PO");
		return purchaseDatAssetRequestDetails;
	}

	/**
	 * 
	 * @param purchaseRequestId
	 * @return PurchaseDatAssetRequestBO
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to cancel PO.
	 */
	public PurchaseDatAssetRequestBO cancelPO(PurchaseCancelPoBean cancelPoBean)
			throws CommonCustomException {
		LOG.startUsecase("Cancel PO");
		DatEmpDetailBO empDetailBo = new DatEmpDetailBO();
		PurchaseDatAssetRequestBO purchaseDatAssetRequestDetails = new PurchaseDatAssetRequestBO();
		try {

			purchaseDatAssetRequestDetails = purchaseAssetRequestRepo
					.findByPkPurchaseRequestId(cancelPoBean.getPurchaseRequestId());

		} catch (Exception ex) {
			throw new CommonCustomException("Failed to fecth details from db.");
		}

		if (purchaseDatAssetRequestDetails == null) {
			throw new CommonCustomException("Asset request does not exist.");
		}

		else if (purchaseDatAssetRequestDetails.getPurchaseRequestStatus() == requestPoRejectedByFinanceStatus)
		{
			
			throw new CommonCustomException("PO is already cancelled.");
		}
		else if(purchaseDatAssetRequestDetails.getPurchaseRequestStatus() == purchasePoCreatedBySysAdmin)
		{
			try{
				List<Short> financeIDList = new ArrayList<Short>();
				String[] empIdArray = financeIds.split(",");
				for (String s : empIdArray)
					financeIDList.add(Short.parseShort(s));
				empDetailBo = empDetailRepository.getFinanceRoleDetails(cancelPoBean.getLoggedInUser(),financeIDList);
			}
				catch(Exception ex)
				{
					throw new CommonCustomException("Failed to fecth details from db.");
				}
			if(empDetailBo!=null)
			{
		purchaseDatAssetRequestDetails
				.setPurchaseRequestStatus(requestPoRejectedByFinanceStatus);	
		purchaseDatAssetRequestDetails.setFinanceEx_updated_on(new Date());
		purchaseDatAssetRequestDetails.setModifiedOn(new Date());
		purchaseDatAssetRequestDetails.setFinanceEx_updated_by(cancelPoBean.getLoggedInUser());
		purchaseDatAssetRequestDetails.setFinanceEx_comment(cancelPoBean.getFinanceExComment());
		purchaseAssetRequestRepo.save(purchaseDatAssetRequestDetails);
		}
				else
				{
					throw new CommonCustomException("The logged in user is  not Finance Head.Hence he/she can not cancel PO.");
				}
				}
		else
		{
			throw new CommonCustomException("PO can be cancelled only if it is created by SystemAdmin.");
		}	
		
		//Added by Kamal Anand for Notifications
		List<DatEmpDetailBO> empDetail1 = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(financeHeadRoleId));
		empDetail1.forEach(emp -> {
			try {
				notificationService.setPOCreatedNotificationToFinanceHeadScreen(emp.getPkEmpId());
			} catch (Exception e) {
				LOG.info("Exception occurred while setting PO Created Notification");
			}
		});
		//End of Addition by Kamal Anand for Notifications
		LOG.endUsecase("Cancel PO");
		return purchaseDatAssetRequestDetails;
	}

}
