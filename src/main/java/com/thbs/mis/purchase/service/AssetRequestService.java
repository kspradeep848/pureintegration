package com.thbs.mis.purchase.service;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.dao.BusinessUnitRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.service.GSRGoalService;
import com.thbs.mis.notificationframework.puchasenotification.service.PurchaseNotificationService;
import com.thbs.mis.purchase.bean.AddQuoteInputBeanPhase2;
import com.thbs.mis.purchase.bean.ClientAndProjectDetailsFromCompIdBean;
import com.thbs.mis.purchase.bean.ClientAndProjectDetailsInputBean;
import com.thbs.mis.purchase.bean.CreatePDFForCapexReportBean;
import com.thbs.mis.purchase.bean.CreatePDFForCapexSharedReportBean;
import com.thbs.mis.purchase.bean.FileBean;
import com.thbs.mis.purchase.bean.PurchaseAddNewAssetRequestInputBean;
import com.thbs.mis.purchase.bean.PurchaseAssetDetailForAssetRequestOutputBean;
import com.thbs.mis.purchase.bean.PurchaseAssetDetailsOutputBean;
import com.thbs.mis.purchase.bean.PurchaseAssetRequestBean;
import com.thbs.mis.purchase.bean.PurchaseAssetRequestDetailOutputBean;
import com.thbs.mis.purchase.bean.PurchaseAssetRequestUpdateInputBean;
import com.thbs.mis.purchase.bean.PurchaseGetAssetRequestBean;
import com.thbs.mis.purchase.bean.PurchaseNewAssetRequestCreateInputBean;
import com.thbs.mis.purchase.bean.PurchaseQuotesOutputBean;
import com.thbs.mis.purchase.bean.PurchaseUpdateNewAssetRequestInputBean;
import com.thbs.mis.purchase.bean.PurchaseViewAssetRequestByDateRangeAndEmpIdUnderEmpLoginOutputBean;
import com.thbs.mis.purchase.bean.PurchaseViewAssetRequestByDateRangeAndEmployeeIdInputBean;
import com.thbs.mis.purchase.bean.PurchaseViewAssetRequestByDateRangeAndMgrIdUnderMgrLoginOutputBean;
import com.thbs.mis.purchase.bean.QuoteDetailsForPOApprovalBean;
import com.thbs.mis.purchase.bean.RejectAssetReqBySysAdminInputBean;
import com.thbs.mis.purchase.bean.RejectAssetRequestInputBean;
import com.thbs.mis.purchase.bean.UpdateAndApproveAssetRequestForSystemAdminBean;
import com.thbs.mis.purchase.bean.ViewAllAssetRequestBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetQuoteBO;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;
import com.thbs.mis.purchase.bo.PurchaseDatBuBO;
import com.thbs.mis.purchase.bo.PurchaseDatCapexBO;
import com.thbs.mis.purchase.bo.PurchaseMasAssetDetailsBO;
import com.thbs.mis.purchase.bo.PurchaseMasAssetsBO;
import com.thbs.mis.purchase.dao.AssetRequestSpecification;
import com.thbs.mis.purchase.dao.PurchaseAssetQuoteRepository;
import com.thbs.mis.purchase.dao.PurchaseAssetRequestRepository;
import com.thbs.mis.purchase.dao.PurchaseCapexRepository;
import com.thbs.mis.purchase.dao.PurchaseDatBuRepository;
import com.thbs.mis.purchase.dao.PurchaseMasAssetDetailsRepository;
import com.thbs.mis.purchase.dao.PurchaseMasAssetsRepository;
@Service
public class AssetRequestService {
	private static final AppLog LOG = LogFactory.getLog(AssetRequestService.class);
	@Autowired
	private EmployeePersonalDetailsRepository employeePersonalDetailRepo;
	@Autowired
	private EmployeeProfessionalRepository employeeProfessionalRepository;
	@Autowired
	private PurchaseAssetRequestRepository purchaseAssetRequestRepo;
	@Autowired
	private PurchaseMasAssetsRepository purchaseMasAssetsRepo;
	
	@Autowired
	private PurchaseMasAssetDetailsRepository purchaseAssetDetailRepo;
	
	@Autowired
	private EmpDetailRepository empDetailRepo;
	
	@Autowired
	private GSRGoalService gsrGoalService;
	
	@Autowired
	private PurchaseCapexService purchaseCapexService;
	
	@Autowired
	private PurchaseAssetQuoteRepository assetQuoteRepository;
	
	@Autowired
	private BusinessUnitRepository buRepository;
	
	@Autowired
	private PurchaseCapexRepository capexRepository;
	
	//Added by Kamal Anand for Notifications
	@Autowired
	private PurchaseNotificationService notificationService;
	
	@Autowired
	private EmpDetailRepository empDetailRepository;
	
	@Autowired
	private PurchaseDatBuRepository datBuRepository;
	
	@Value("${system.admin.procurement.roleId}")
	String sysAdminProcurementRoleId;
	//End of Addition by Kamal Anand for Notifications							
	@Value("${request.waitingRmApproval.status}")
	private int requestWaitingRmApprovalStatus;
	@Value("${request.approvedByRm.status}")
	private int requestApprovedByRmStatus;
	
	@Value("${purchase.capex.createdBySysAdmin}")
	private int purchaseCapexCreatedBySysAdmin;
	@Value("${request.capexApprovedByBuHead.status}")
	private int requestCapexApprovedByBuHeadStatus;
	@Value("${request.poApprovedByFinance.status}")
	private int requestPoApprovedByFinanceStatus;
	@Value("${request.poRejectedByFinance.status}")
	private int requestPoRejectedByFinanceStatus;
	@Value("${purchase.request.rejectByRm.status}")
	private int requestRejectByRmStatus;
	@Value("${purchase.capex.rejectByBuHead.status}")
	private int purchaseCapexRejectByBuHeadStatus;
	@Value("${purchase.request.rejectByRm.status}")
	private int purchaseRequestRejectByRmStatus;
	
	@Value("${purchase.file.basepath}")
	private String purchaseDocsBasePath;
	
	@Value("${buHead.Role.Id}")
	private short buHeadRoleId;
	
	@Value("${finance.Head.Role.Id}")
	private short financeHeadRoleId;
	
	@Value("${purchase.add.asset.systemAdminRoles}")
	private String systemAdminRoles;
	
	@Value("${purchase.nbuit.bu.unit.id}")
	private short itBuId;
	
	@Value("${purchase.quote.maximum.quantity}")
	private int maximumQuantityOfQuote;
	
	@Value("${purchase.uploaded}")
	private String purchaseCapexUploaded;
	
	@Value("${purchase.nbuit.bu.unit.id}")
	private Short purchaseNbuItUnitId;
	
	@Value("${purchase.finance.bu.id}")
	private short financeBuId;
	
	@Value("${nbu.it.component.name}")
	private String nbuItComponentName;
	
	@Value("${nbu.it.project.name}")
	private String nbuItProjectName;
	
	@Value("${purchase.capex.templates.basepath}")
	private String capexTemplateBasePath;
	
	@Value("${purcahse.capex.shared.templates.basepath}")
	private String capexSharedTemplatePath;
	
	@Value("${purchase.request.type.new.asset.request}")
	private Integer purchaseRequestTypeNewAssetRequest;
	
	@Value("${legacy.service.url}")
	private String purchaseLegacyUrl;
	
	@Value("${purchase.no.project.found}")
	private String purchaseNoProjectFound;
	
	@Value("${asset.request.rejected.by.sysadmin}")
	private Integer assetRequestRejectedBySysAdmin;	
	
	@Value("${purchase.assetRequest.closed}")
	private Integer assetRequestClosed;  
	
	@Value("${thbs.gurgaon.location.id}")
	private Integer gurgaonLocationId;
	
	@Value("${thbs.bangalore.location.id}")
	private Integer bangaloreLocationId;
	
	
	public List<PurchaseAssetRequestBean> getAllAssetRequests(ViewAllAssetRequestBean assetReqBean)
			throws DataAccessException {
		LOG.entering("getAllAssetRequests service method");
		List<PurchaseDatAssetRequestBO> listOfAssetRequest = new ArrayList<PurchaseDatAssetRequestBO>();
		List<PurchaseAssetRequestBean> listOfAssetRequestBean = new ArrayList<PurchaseAssetRequestBean>();
		try {
			Specification<PurchaseDatAssetRequestBO> assetRequestSpecification = AssetRequestSpecification
					.getAllAssetRequests(assetReqBean);
			listOfAssetRequest = purchaseAssetRequestRepo.findAll(assetRequestSpecification);
		} catch (Exception e) {
			throw new DataAccessException("Exception occured at service method getAllAssetRequests ", e);
		}
		if(listOfAssetRequest.size() > 0){
			PurchaseAssetRequestBean assetReqOutputBean;
			for (PurchaseDatAssetRequestBO assetReqBo : listOfAssetRequest) {
				if((assetReqBo.getPurchaseRequestStatus() != requestWaitingRmApprovalStatus ||
						assetReqBo.getPurchaseRequestStatus() != requestRejectByRmStatus) 
						&& assetReqBo.getFkRequestTypeId().intValue() == purchaseRequestTypeNewAssetRequest.intValue())//Added by Shyam for phase-2
				{
					assetReqOutputBean = new PurchaseAssetRequestBean();
					assetReqOutputBean.setAssetRequestId(assetReqBo.getPkPurchaseRequestId());
					assetReqOutputBean.setAssetDetail(assetReqBo.getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());
					assetReqOutputBean.setBusinessUnit(assetReqBo.getMasBuUnitBO().getBuUnitName());
					assetReqOutputBean.setRaisedOn(assetReqBo.getCreatedDate());
					assetReqOutputBean.setRequesterEmpId(assetReqBo.getRequestorId());
					DatEmpPersonalDetailBO personal = employeePersonalDetailRepo
							.findByFkEmpDetailId(assetReqBo.getRequestorId());
					assetReqOutputBean.setRequesterEmpName(personal.getEmpFirstName() + " " + personal.getEmpLastName());
					assetReqOutputBean.setStatus(assetReqBo.getPurchaseMasRequestStatusBO().getPurchaseRequestStatusName());
					assetReqOutputBean.setStatusId(assetReqBo.getPurchaseRequestStatus());
					assetReqOutputBean.setFkRequestTypeId(assetReqBo.getFkRequestTypeId());
					listOfAssetRequestBean.add(assetReqOutputBean);
				}
				//For phase -2 implementation
				else{
					assetReqOutputBean = new PurchaseAssetRequestBean();
					assetReqOutputBean.setAssetRequestId(assetReqBo.getPkPurchaseRequestId());
					assetReqOutputBean.setAssetDetail(assetReqBo.getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());
					assetReqOutputBean.setBusinessUnit(assetReqBo.getMasBuUnitBO().getBuUnitName());
					assetReqOutputBean.setRaisedOn(assetReqBo.getCreatedDate());
					
					assetReqOutputBean.setRequesterEmpId(assetReqBo.getRequestorId());
					DatEmpPersonalDetailBO mgrProfDetail = employeePersonalDetailRepo
							.findByFkEmpDetailId(assetReqBo.getRmToApprove());
					assetReqOutputBean.setRequesterEmpId(assetReqBo.getRmToApprove());
					assetReqOutputBean.setRequesterEmpName(mgrProfDetail.getEmpFirstName() + " " + mgrProfDetail.getEmpLastName());
					assetReqOutputBean.setStatus(assetReqBo.getPurchaseMasRequestStatusBO().getPurchaseRequestStatusName());
					assetReqOutputBean.setStatusId(assetReqBo.getPurchaseRequestStatus());
					assetReqOutputBean.setFkRequestTypeId(assetReqBo.getFkRequestTypeId());
					listOfAssetRequestBean.add(assetReqOutputBean);
				}
			}
		}
		LOG.exiting("getAllAssetRequests service method");
		return listOfAssetRequestBean;
	}
	/*
	 * public Boolean downloadPo(String poNumber,HttpServletResponse response)
	 * throws DataAccessException{
	 * LOG.entering("downloadPo service method"+poNumber); Boolean isPoDownloaded =
	 * false; try{ String BASEPATH = "D:/Anil/pdf/";
	 * 
	 * 
	 * String outputFileName = poNumber+".pdf";
	 * 
	 * LOG.info("outputFileName : "+outputFileName);
	 * LOG.info("BASEPATH : "+BASEPATH); DownloadFileUtil.downloadFile(response,
	 * BASEPATH, outputFileName); isPoDownloaded = true;
	 * 
	 * }catch(Exception e){ throw new DataAccessException(
	 * "Exception occured at service method downloadPo ", e); }
	 * 
	 * LOG.exiting("downloadPo service method"); return isPoDownloaded;
	 * 
	 * }
	 */
	public PurchaseGetAssetRequestBean getAssetRequests(Integer assetReqId) throws DataAccessException {
		PurchaseDatAssetRequestBO assetRequestBo = new PurchaseDatAssetRequestBO();
		PurchaseGetAssetRequestBean assetReqBean = new PurchaseGetAssetRequestBean();
		LOG.entering("getAssetRequests service method");
		try {
			assetRequestBo = purchaseAssetRequestRepo.findByPkPurchaseRequestId(assetReqId);
			if (assetRequestBo != null) 
			{
				assetReqBean.setAssetRequestType(assetRequestBo.getPurchaseDatRequestTypeBO().getPurchaseRequestTypeName());
				assetReqBean.setAssetNameVersionAndModel(assetRequestBo.getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());
				assetReqBean.setAssetRequiredBy(assetRequestBo.getAssetRequiredBy());
				assetReqBean.setAssetType(assetRequestBo.getPurchaseMasAssetsBO().getAssetType());
				assetReqBean.setBreifDiscription(assetRequestBo.getBriefDescription());
				assetReqBean.setBuName(assetRequestBo.getMasBuUnitBO().getBuUnitName());
				assetReqBean.setBuId((int)assetRequestBo.getFkBuId());
				assetReqBean.setDuration(assetRequestBo.getDuration());
				assetReqBean.setIsClientReimbursing(assetRequestBo.getIsClientReimbursing());
				assetReqBean.setNoOfUnitsOrLicence(assetRequestBo.getNoOfUnits());
				assetReqBean.setPkPurchaseRequestId(assetRequestBo.getPkPurchaseRequestId());
				assetReqBean.setPriority(assetRequestBo.getPriority());
				assetReqBean.setAssetRequestStatus(assetRequestBo.getPurchaseMasRequestStatusBO().getPurchaseRequestStatusName().trim());
				assetReqBean.setAssetRequestStatusId(assetRequestBo.getPurchaseRequestStatus());
				assetReqBean.setAssetId(assetRequestBo.getFkAssetId());
				assetReqBean.setModifiedDate(assetRequestBo.getModifiedOn());
				assetReqBean.setSystemAdminCommets(assetRequestBo.getSysAdminComment());
				assetReqBean.setReportingManager(
						gsrGoalService.getEmpNameByEmpId(assetRequestBo.getRmToApprove().intValue()) + '-'+ assetRequestBo.getRmToApprove());
				assetReqBean.setRmId(assetRequestBo.getRmToApprove());
				assetReqBean.setSpecification(assetRequestBo.getSpecifications());
				
				List<PurchaseQuotesOutputBean> listOfQuotes = new ArrayList<PurchaseQuotesOutputBean>();
				List<PurchaseDatAssetQuoteBO> listQuotes = assetQuoteRepository.findByFkPurchaseRequestId(assetRequestBo.getPkPurchaseRequestId());
				
				//Added by Shyam for RM & BU head reject Comments
				assetReqBean.setRmRejectComments(assetRequestBo.getRmComment());
				assetReqBean.setFinanceRejectComments(assetRequestBo.getFinanceEx_comment());
				
				if(assetReqBean.getAssetRequestStatusId().intValue() == purchaseCapexRejectByBuHeadStatus)
				{
					PurchaseDatAssetQuoteBO defaultQuote = null;
					for (PurchaseDatAssetQuoteBO purchaseDatAssetQuoteBO : listQuotes) 
					{
						if(purchaseDatAssetQuoteBO.getQuoteStatus().equalsIgnoreCase("AGREE"))
						{
							defaultQuote = purchaseDatAssetQuoteBO;
							break;
						}
					}
					
					if(defaultQuote == null) {
						throw new CommonCustomException("Agree quote of this request is not found");
					}
					
					List<PurchaseDatBuBO> buEntryList = new ArrayList<PurchaseDatBuBO>();
					try{
						buEntryList = datBuRepository.findByFkPurchaseQuoteId(defaultQuote.getPkQuoteId());
					}catch(Exception ex){
						throw new CommonCustomException("Failed to fetch BU head entries from backend. Kindly get in touch with Nucleus Support team");
					}
					
					for (PurchaseDatBuBO purchaseDatBuBO : buEntryList) 
					{
						if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
						{
							assetReqBean.setBuHeadRejectComments(purchaseDatBuBO.getBuHeadComment());
							break;
						}
					}
				}
				
				//EOA  ------->
				
				
				if(listQuotes.size() > 0){
					PurchaseQuotesOutputBean quoteBean;
					for( PurchaseDatAssetQuoteBO quote : listQuotes)
					{
						quoteBean = new PurchaseQuotesOutputBean();
						quoteBean.setQuoteId(quote.getPkQuoteId());
						quoteBean.setQuoteStatus(quote.getQuoteStatus());
						quoteBean.setTotalCost(quote.getTotalCost());
						quoteBean.setBuContribution(quote.getBuContribution());
						quoteBean.setCostBorneBy(quote.getCostBorneBy());
						quoteBean.setDeliveryDate(quote.getDeliveryDate());
						quoteBean.setIndividualLicenseCost(quote.getIndividualLicenseCost());
						quoteBean.setVendorName(quote.getPurchaseDatSupplierDetailBO().getVendorName());
						quoteBean.setVendorAddress(quote.getPurchaseDatSupplierDetailBO().getVendorAddress());
						quoteBean.setVendorQuoteReferenceNumber(quote.getVendorQuoteReferenceNo());
						quoteBean.setVendorContactNumber(quote.getPurchaseDatSupplierDetailBO().getVendorContactNo());
						quoteBean.setSupplierAddressId(quote.getFkSupplierAddress());
						quoteBean.setCurrencyType(quote.getFkCurrencyType());
						quoteBean.setAssetDescriptionEnteredBySysadmin(quote.getAssetDescriptionEnteredBySysadmin());
						listOfQuotes.add(quoteBean);
						
					}
				}
				
				if(assetRequestBo.getFkRequestTypeId().intValue() == purchaseRequestTypeNewAssetRequest.intValue())//Added by Shyam
				{	
					assetReqBean.setComponentId(assetRequestBo.getProjectComponentId());
					assetReqBean.setClientId(assetRequestBo.getClientId());
					assetReqBean.setModifiedDate(assetRequestBo.getModifiedOn());
					assetReqBean.setSystemAdminCommets(assetRequestBo.getSysAdminComment());
					// get component & client details from legacy service
					ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean();
					clientProjCompDetailsBean.setClientId(assetRequestBo.getClientId());
					clientProjCompDetailsBean.setProjectId(assetRequestBo.getProjectId());
					clientProjCompDetailsBean.setComponentId(assetRequestBo.getProjectComponentId());
					clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
					
					PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
					ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService
							.getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
					if (clientProjectCompDetails == null) {
						throw new CommonCustomException("Unable to fetch project, client and component details from legacy MIS.");
					} else {
						assetReqBean.setClientName(clientProjectCompDetails.getClientName());
						assetReqBean.setComponentName(clientProjectCompDetails.getComponentName());
						assetReqBean.setInternalFlag(clientProjectCompDetails.getInternalProjectFlag());
					}
					
				}else{//For phase-2, added by Shyam
					if(assetRequestBo.getPurchaseRequestStatus().intValue() != requestWaitingRmApprovalStatus
							&& assetRequestBo.getPurchaseRequestStatus().intValue() != requestRejectByRmStatus)
					{
						// get component & client details from legacy service
						ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean();
						clientProjCompDetailsBean.setClientId(assetRequestBo.getClientId());
						clientProjCompDetailsBean.setProjectId(assetRequestBo.getProjectId());
						clientProjCompDetailsBean.setComponentId(assetRequestBo.getProjectComponentId());
						clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
						
						PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
						ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService
							.getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
						if (clientProjectCompDetails == null) {
							throw new CommonCustomException("Unable to fetch project, client and component details from legacy MIS.");
						} else {
							assetReqBean.setClientName(clientProjectCompDetails.getClientName());
							assetReqBean.setComponentName(clientProjectCompDetails.getComponentName());
							assetReqBean.setInternalFlag(clientProjectCompDetails.getInternalProjectFlag());
						}
					}
				
				}
				assetReqBean.setListOfQuotes(listOfQuotes);
			}else{
				throw new CommonCustomException("No Data for given asset request Id, pass proper id");
			}
			LOG.exiting("getAssetRequests service method");
		} catch (Exception e) {
			throw new DataAccessException("Exception occured at service method getAssetRequests ", e);
		}
		return assetReqBean;
	}
	// Added by pratibha
	
	
	public List<PurchaseAssetDetailsOutputBean> getallAssetDetails()
			throws CommonCustomException {
		LOG.startUsecase("Get Asset And Its Detail From Asset Type for employee");
		List<PurchaseMasAssetsBO> assetDetailsList = new ArrayList<PurchaseMasAssetsBO>();
		List<PurchaseAssetDetailsOutputBean> assetsOutputBeanList = new ArrayList<PurchaseAssetDetailsOutputBean>();
		try {
			assetDetailsList = purchaseMasAssetsRepo.findAll();
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to retrieve asset details. Kindly contact to Nucleus Support team for this issue.");
		}
		for (PurchaseMasAssetsBO purchaseMasAssetsBO : assetDetailsList) 
			{	
				if(purchaseMasAssetsBO.getIsAvailable().equalsIgnoreCase("YES"))
				{
				PurchaseAssetDetailsOutputBean outputBean = new PurchaseAssetDetailsOutputBean();
				outputBean.setAssetId(purchaseMasAssetsBO.getPkAssetId());
				outputBean.setAssetType(purchaseMasAssetsBO.getAssetType());
				outputBean.setAssetNameAndVersionOrModel(purchaseMasAssetsBO.getAssetNameAndVersionOrModel());
				assetsOutputBeanList.add(outputBean);
			}
		}
		
		LOG.endUsecase("Get Asset And Its Detail From Asset Type for employee.");
		return assetsOutputBeanList;
	}
	
	/**
	 * 
	 * @param assetType
	 * @return List<PurchaseAssetDetailForAssetRequestOutputBean>
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to view the asset and it
	 *               detail.
	 */
	public List<PurchaseAssetDetailForAssetRequestOutputBean> getAssetAndItsDetailFromAssetTypeForSystemAdmin(String assetType)
			throws CommonCustomException {
		LOG.startUsecase("Get Asset And Its Detail From Asset Type for system admin");
		//List<PurchaseMasAssetDetailsBO> assetDetailsList = new ArrayList<PurchaseMasAssetDetailsBO>();
		List<PurchaseMasAssetsBO> assetDetailsList =  new ArrayList<PurchaseMasAssetsBO>();
		List<PurchaseAssetDetailForAssetRequestOutputBean> assetsRequestOutputBeanList = new ArrayList<PurchaseAssetDetailForAssetRequestOutputBean>();
		try {
			//assetDetailsList = purchaseMasAssetsRepo.getAssetDetailsBasedOnAssetType(assetType);
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to retrieve asset details. Kindly contact to Nucleus Support team for this issue.");
		}
		for (PurchaseMasAssetsBO purchaseMasAssetsBO : assetDetailsList) {
			{	
				PurchaseAssetDetailForAssetRequestOutputBean outputBean = new PurchaseAssetDetailForAssetRequestOutputBean();
				outputBean.setAssetId(purchaseMasAssetsBO.getPkAssetId());
				outputBean.setAssetType(purchaseMasAssetsBO.getAssetType());
				outputBean.setAssetNameAndVersionOrModel(purchaseMasAssetsBO.getAssetNameAndVersionOrModel());
				//outputBean.setAssetDetailId(purchaseMasAssetsBO.getPkAssetDetailId());
				//outputBean.setSoftwareLicenseOrSubscriptionId(purchaseMasAssetsBO.getSoftwareLicenseOrSubscriptionId());
				outputBean.setLicenceType(purchaseMasAssetsBO.getLicenseType()); 
				assetsRequestOutputBeanList.add(outputBean);
			}
		}
		LOG.endUsecase("Get Asset And Its Detail From Asset Type for system admin");
		return assetsRequestOutputBeanList;
	}	
	/**
	 * 
	 * @param assetType
	 * @return List<PurchaseAssetDetailsOutputBean>
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to view the asset and it
	 *               detail.
	 */
	public List<PurchaseAssetDetailsOutputBean> getAssetAndItsDetailFromAssetTypeForEmployee(String assetType)
			throws CommonCustomException {
		LOG.startUsecase("Get Asset And Its Detail From Asset Type for employee");
		List<PurchaseMasAssetsBO> assetDetailsList = new ArrayList<PurchaseMasAssetsBO>();
		List<PurchaseAssetDetailsOutputBean> assetsRequestOutputBeanList = new ArrayList<PurchaseAssetDetailsOutputBean>();
		try {
			assetDetailsList = purchaseMasAssetsRepo.findByAssetType(assetType);
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to retrieve asset details. Kindly contact to Nucleus Support team for this issue.");
		}
		for (PurchaseMasAssetsBO purchaseMasAssetsBO : assetDetailsList) 
			{	
				if(purchaseMasAssetsBO.getIsAvailable().equalsIgnoreCase("YES"))
				{
					PurchaseAssetDetailsOutputBean outputBean = new PurchaseAssetDetailsOutputBean();
				outputBean.setAssetId(purchaseMasAssetsBO.getPkAssetId());
				outputBean.setAssetType(purchaseMasAssetsBO.getAssetType());
				outputBean.setAssetNameAndVersionOrModel(purchaseMasAssetsBO.getAssetNameAndVersionOrModel());
				outputBean.setLicenceType(purchaseMasAssetsBO.getLicenseType());
				assetsRequestOutputBeanList.add(outputBean);
			}
		}
		
		LOG.endUsecase("Get Asset And Its Detail From Asset Type for employee.");
		return assetsRequestOutputBeanList;
	}
	/**
	 * 
	 * @param assetRequestId
	 * @return PurchaseAssetRequestDetailOutputBean
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to view the asset and it
	 *               detail.
	 */
	public List<PurchaseAssetRequestDetailOutputBean> getAssetRequestDetailsByAssetRequestId(Integer assetRequestId)
			throws CommonCustomException 
	{
		PurchaseDatAssetRequestBO purchaseDatAssetRequestDetails = new PurchaseDatAssetRequestBO();
		List<PurchaseDatAssetQuoteBO> assetQuoteDetailsList = new ArrayList<PurchaseDatAssetQuoteBO>();
		PurchaseDatAssetQuoteBO purchaseDatAssetQuoteBO = new PurchaseDatAssetQuoteBO();
	
		PurchaseAssetRequestDetailOutputBean requestDetailOutputBean = new PurchaseAssetRequestDetailOutputBean();
		List<PurchaseAssetRequestDetailOutputBean> detailsOutputBeanList = new ArrayList<PurchaseAssetRequestDetailOutputBean>();
		MasBuUnitBO itBuDetail = null;
		//PurchaseMasAssetDetailsBO assetDetailBO = new PurchaseMasAssetDetailsBO();
		try {
			purchaseDatAssetRequestDetails = purchaseAssetRequestRepo.findByPkPurchaseRequestId(assetRequestId);
			assetQuoteDetailsList = assetQuoteRepository.findByFkPurchaseRequestId(assetRequestId);
			purchaseDatAssetQuoteBO = assetQuoteRepository.getAgreeQuoteDetails(assetRequestId);
			//assetDetailBO = purchaseAssetDetailRepo.findByFkPurchaseRequestId(assetRequestId);
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to retrieve asset details. Kindly contact to Nucleus Support team for this issue.");
		}
		
		if (purchaseDatAssetRequestDetails == null) {
			throw new CommonCustomException("Asset request does not exist.");
		}
        
	/*	if(assetDetailBO == null)
			throw new CommonCustomException("Failed to get asset detail record with subscription or license id. "
					+ "Kindly get in touch with Nucleus Support team.");*/
		
		if(!assetQuoteDetailsList.isEmpty())
        {
			requestDetailOutputBean.setQuoteFlag("true");
        }else {	
        	requestDetailOutputBean.setQuoteFlag("false");
        }
		
		requestDetailOutputBean.setPurchaseRequestId(assetRequestId);
		requestDetailOutputBean.setRequestorId(purchaseDatAssetRequestDetails.getRequestorId());
		requestDetailOutputBean.setAssetType(purchaseDatAssetRequestDetails.getPurchaseMasAssetsBO().getAssetType());
		requestDetailOutputBean.setAssetId(purchaseDatAssetRequestDetails.getFkAssetId());
		//requestDetailOutputBean.setAssetDetailId(purchaseDatAssetRequestDetails.getFkAssetDetailId()); //Commented by Anil for removal of fk detail id in asset request Table
		requestDetailOutputBean.setSoftwareTitleAndVersion(
				purchaseDatAssetRequestDetails.getPurchaseMasAssetsBO().getAssetNameAndVersionOrModel());
		requestDetailOutputBean.setNumberOfLincenses(purchaseDatAssetRequestDetails.getNoOfUnits());
		requestDetailOutputBean.setFkBuId(purchaseDatAssetRequestDetails.getFkBuId());
		requestDetailOutputBean.setBuUnitName(purchaseDatAssetRequestDetails.getMasBuUnitBO().getBuUnitName());

		
		//Added condition for Phase -2
		if(purchaseDatAssetRequestDetails.getFkRequestTypeId().intValue() == purchaseRequestTypeNewAssetRequest.intValue())
		{
			requestDetailOutputBean.setProjectComponentId(purchaseDatAssetRequestDetails.getProjectComponentId());
			requestDetailOutputBean.setClientId(purchaseDatAssetRequestDetails.getClientId());
			requestDetailOutputBean.setProjectId(purchaseDatAssetRequestDetails.getProjectId());
		}
		//EOA

		requestDetailOutputBean.setDuration(purchaseDatAssetRequestDetails.getDuration());
		requestDetailOutputBean.setBriefDescription(purchaseDatAssetRequestDetails.getBriefDescription());
		requestDetailOutputBean.setLocationId(purchaseDatAssetRequestDetails.getLocationId());
		requestDetailOutputBean.setLocationName(purchaseDatAssetRequestDetails.getMasLocationParentBO().getLocationParentName());
		
		requestDetailOutputBean.setPurchaseRequestStatus(purchaseDatAssetRequestDetails.getPurchaseRequestStatus());
		requestDetailOutputBean.setStatusInfoString(purchaseDatAssetRequestDetails.getPurchaseMasRequestStatusBO().getPurchaseRequestStatusName());
		requestDetailOutputBean.setRmId(purchaseDatAssetRequestDetails.getRmToApprove());

		
		if(purchaseDatAssetRequestDetails.getPriority() != null)
			requestDetailOutputBean.setPriority(purchaseDatAssetRequestDetails.getPriority());

		requestDetailOutputBean.setPurchaseRequestTypeId(purchaseDatAssetRequestDetails.getPurchaseDatRequestTypeBO().getPurchaseRequestTypeId());
		requestDetailOutputBean.setPurchaseRequestTypeName(purchaseDatAssetRequestDetails.getPurchaseDatRequestTypeBO().getPurchaseRequestTypeName());
		requestDetailOutputBean.setLicenceType(purchaseDatAssetRequestDetails.getPurchaseMasAssetsBO().getLicenseType());
		
		requestDetailOutputBean.setRmComment(purchaseDatAssetRequestDetails.getRmComment());
		requestDetailOutputBean.setSysAdminComment(purchaseDatAssetRequestDetails.getSysAdminComment());
		requestDetailOutputBean.setFinanceExComment(purchaseDatAssetRequestDetails.getFinanceEx_comment());
		
		//For Bu Head rejection, added by Shyam
		List<PurchaseDatBuBO> purchaseDatBuBoList = new ArrayList<PurchaseDatBuBO>();
		if(purchaseDatAssetRequestDetails.getPurchaseRequestStatus().intValue() == purchaseCapexRejectByBuHeadStatus)
		{
			
			if(purchaseDatAssetQuoteBO != null){
				try{
					purchaseDatBuBoList = datBuRepository.findByFkPurchaseQuoteId(
							purchaseDatAssetQuoteBO.getPkQuoteId());
				}catch(Exception ex){
					throw new CommonCustomException("Failed to fetch BU head comments details from back end");
				}
			}
		}
		
		if(!purchaseDatBuBoList.isEmpty())
		{
			for (PurchaseDatBuBO purchaseDatBuBO : purchaseDatBuBoList) 
			{
				if(purchaseDatBuBO.getFkPurchaseDatBuStatus().equalsIgnoreCase("DISAGREE"))
				{
					requestDetailOutputBean.setBuHeadComment(purchaseDatBuBO.getBuHeadComment());
					break;
				}
			}
		}
		
		
		
		List<PurchaseMasAssetDetailsBO> listOfAssetDetails = null;
		try {
	      listOfAssetDetails = 
				purchaseAssetDetailRepo.findByFkPurchaseRequestIdAndFklocationIdAndFkMasAssetsId(
					purchaseDatAssetRequestDetails.getPkPurchaseRequestId()
					, (int)purchaseDatAssetRequestDetails.getLocationId(), purchaseDatAssetRequestDetails.getFkAssetId());
		}
		catch (Exception e)
		{
			throw new CommonCustomException("Error occured while fetching details from assetdetail table based on requestId,locatinid,fkassetId ");
			
		}

		if(!listOfAssetDetails.isEmpty())
		{
			if(purchaseDatAssetRequestDetails.getPurchaseMasAssetsBO().getAssetType().equalsIgnoreCase("HARDWARE"))
			{
				List<String> listAssets = new ArrayList<String>();
				for(PurchaseMasAssetDetailsBO assetDetail : listOfAssetDetails)
				{
					listAssets.add(assetDetail.getSoftwareLicenseOrSubscriptionId());
				}
				requestDetailOutputBean.setGeneratedAssetId(listAssets);
			}
			else{
				requestDetailOutputBean.setSoftwareLicenseOrSubscriptionId(
						listOfAssetDetails.get(0).getSoftwareLicenseOrSubscriptionId());
			}
		}
		
		requestDetailOutputBean.setRequestorName(gsrGoalService.getEmpNameByEmpId(
				purchaseDatAssetRequestDetails.getRequestorId().intValue()) + '-' 
				+ purchaseDatAssetRequestDetails.getRequestorId());
	
		if(purchaseDatAssetRequestDetails.getRmToApprove() != null)
			requestDetailOutputBean.setReportingManager(gsrGoalService.getEmpNameByEmpId(
				purchaseDatAssetRequestDetails.getRmToApprove().intValue()) + '-'
				+ purchaseDatAssetRequestDetails.getRmToApprove());
		
		if(purchaseDatAssetRequestDetails.getSysAdminId() != null)	
			requestDetailOutputBean.setSysAdminName(gsrGoalService.getEmpNameByEmpId(
				purchaseDatAssetRequestDetails.getSysAdminId().intValue()) + '-'
				+ purchaseDatAssetRequestDetails.getSysAdminId());
		
		if(purchaseDatAssetRequestDetails.getFinanceEx_updated_by() != null)
			requestDetailOutputBean.setFinanceHeadName(gsrGoalService.getEmpNameByEmpId(
				purchaseDatAssetRequestDetails.getFinanceEx_updated_by().intValue()) + '-'
				+ purchaseDatAssetRequestDetails.getFinanceEx_updated_by());
		
		if(purchaseDatAssetQuoteBO != null)
	    {
			if(purchaseDatAssetQuoteBO.getCostBorneBy().equalsIgnoreCase("shared"))
			{
				itBuDetail = buRepository.findByPkBuUnitId(itBuId);
				requestDetailOutputBean.setBuHeadName(gsrGoalService.getEmpNameByEmpId(
					purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId())+ '-'
					+ purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId());
			 
				//if(purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId() != itBuDetail.getFkBuHeadEmpId())
			//	{
					requestDetailOutputBean.setItHeadName(gsrGoalService.getEmpNameByEmpId(itBuDetail.getFkBuHeadEmpId()) + '-' 
							+itBuDetail.getFkBuHeadEmpId());
			//	}
			}else if(purchaseDatAssetQuoteBO.getCostBorneBy().equalsIgnoreCase("bu"))
			{
				requestDetailOutputBean.setBuHeadName(gsrGoalService.getEmpNameByEmpId(
					purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId()) + '-' 
					+ purchaseDatAssetRequestDetails.getMasBuUnitBO().getFkBuHeadEmpId());
			}
			else if(purchaseDatAssetQuoteBO.getCostBorneBy().equalsIgnoreCase("IT"))
			{
				itBuDetail = buRepository.findByPkBuUnitId(itBuId);
				requestDetailOutputBean.setItHeadName(gsrGoalService.getEmpNameByEmpId(itBuDetail.getFkBuHeadEmpId()) + '-'
						+ itBuDetail.getFkBuHeadEmpId());
			}
	    }
		
		//IF request type is "NEW ASSET REQUEST" , then it will set the client , project and component name and id. 
		if(purchaseDatAssetRequestDetails.getFkRequestTypeId().intValue() == purchaseRequestTypeNewAssetRequest.intValue())
		{
			// get component & client details from legacy service
			ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean();
			clientProjCompDetailsBean.setClientId(purchaseDatAssetRequestDetails.getClientId());
			clientProjCompDetailsBean.setProjectId(purchaseDatAssetRequestDetails.getProjectId());
			clientProjCompDetailsBean.setComponentId(purchaseDatAssetRequestDetails.getProjectComponentId());
			clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
			PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
			ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService
					.getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
			if (clientProjectCompDetails == null) {
				throw new CommonCustomException("Unable to fetch project, client and component details from legacy MIS.");
			} else {
				requestDetailOutputBean.setClientName(clientProjectCompDetails.getClientName());
				requestDetailOutputBean.setComponentName(clientProjectCompDetails.getComponentName());
				requestDetailOutputBean.setInternalFlag(clientProjectCompDetails.getInternalProjectFlag());
			}
		}else{
			if(purchaseDatAssetRequestDetails.getClientId() != null && 
					purchaseDatAssetRequestDetails.getProjectComponentId() != null &&
							purchaseDatAssetRequestDetails.getProjectId() != null)
			{
				requestDetailOutputBean.setClientId(purchaseDatAssetRequestDetails.getClientId());
				requestDetailOutputBean.setProjectComponentId(purchaseDatAssetRequestDetails.
						getProjectComponentId());
				requestDetailOutputBean.setProjectId(purchaseDatAssetRequestDetails.getProjectId());
				
				// get component & client details from legacy service
				ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean();
				clientProjCompDetailsBean.setClientId(purchaseDatAssetRequestDetails.getClientId());
				clientProjCompDetailsBean.setProjectId(purchaseDatAssetRequestDetails.getProjectId());
				clientProjCompDetailsBean.setComponentId(purchaseDatAssetRequestDetails.getProjectComponentId());
				clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
				PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
				ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService
						.getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
				if (clientProjectCompDetails == null) {
					throw new CommonCustomException("Unable to fetch project, client and component details from legacy MIS.");
				} else {
					requestDetailOutputBean.setClientName(clientProjectCompDetails.getClientName());
					requestDetailOutputBean.setComponentName(clientProjectCompDetails.getComponentName());
					requestDetailOutputBean.setInternalFlag(clientProjectCompDetails.getInternalProjectFlag());
				}
			}
			
			/*if(assetDetailBO.getSoftwareLicenseOrSubscriptionId() == null && 
					purchaseDatAssetRequestDetails.getFkRequestTypeId().intValue() != purchaseRequestTypeNewAssetRequest.intValue())
			{
				throw new CommonCustomException("Failed to get subscription or license id of the provided asset. Kindly get in touch with Nucleus Support team.");
			}else{
				requestDetailOutputBean.setSoftwareLicenseOrSubscriptionId(assetDetailBO.getSoftwareLicenseOrSubscriptionId());
			}*/
		}
		//EOA
		requestDetailOutputBean.setAssetRequiredBy(purchaseDatAssetRequestDetails.getAssetRequiredBy());
		requestDetailOutputBean.setCreatedDate(purchaseDatAssetRequestDetails.getCreatedDate());
		requestDetailOutputBean.setModifiedOn(purchaseDatAssetRequestDetails.getModifiedOn());
		requestDetailOutputBean.setModifiedBy(purchaseDatAssetRequestDetails.getModifiedBy());
		requestDetailOutputBean.setIsClientReimbursing(purchaseDatAssetRequestDetails.getIsClientReimbursing());
		requestDetailOutputBean.setSpecifications(purchaseDatAssetRequestDetails.getSpecifications());
		
		//code for fetching supporting documents file
		if(purchaseDatAssetRequestDetails.getSupportingDocumentsByClient() != null)
		{
			if(purchaseDatAssetRequestDetails.getSupportingDocumentsByClient().equalsIgnoreCase("UPLOADED"))
			{ 		
				Date CreatedDate = purchaseDatAssetRequestDetails.getCreatedDate();			
				Calendar calendar = new GregorianCalendar();
				calendar.setTime(CreatedDate);
				int year = calendar.get(Calendar.YEAR);
				int month = calendar.get(Calendar.MONTH)+1;
				File directory = new File(purchaseDocsBasePath + File.separator + year + File.separator + month 
						+ File.separator + assetRequestId + File.separator + "Supporting docs" +
						File.separator + "EmpOrRm");
				List<FileBean> fileBeanList = new ArrayList<FileBean>();
				if(directory.isDirectory())
				{
					if(directory.list().length>0)
					{
						//Create an object of file list
						
						//get all the files from a directory
				        File[] fList = directory.listFiles();
				        for (File file : fList)
				        {
					       	FileBean fileBean = new FileBean();
				        	if(file.getName().startsWith("EMP_"))
				        	{
					        	fileBean.setFileName(file.getName());
					        	
					        	double bytes = file.length();
					        	int totalSize = (int) bytes/1024;
					        	if((bytes/1024) > totalSize) 
					        		++totalSize;
					        	
					        	fileBean.setFileSize(Integer.valueOf(totalSize));
					           // System.out.println(file.getName());
					           // System.out.println(file.getTotalSpace());
					            //System.out.println();
					            fileBeanList.add(fileBean);
				        	}
				        }
						LOG.info("Directory is not empty!");
					}
				}
		
				requestDetailOutputBean.setFileBeanList(fileBeanList);
			}
		}
		
		List<QuoteDetailsForPOApprovalBean> quoteBeanList = new ArrayList<QuoteDetailsForPOApprovalBean>();
	/*	List<PurchaseDatAssetQuoteBO> quoteBoList = new ArrayList<PurchaseDatAssetQuoteBO>();
		quoteBoList = assetQuoteRepository.findByFkPurchaseRequestId(assetRequestId);*/
		if (!assetQuoteDetailsList.isEmpty()) 
		{
			for (PurchaseDatAssetQuoteBO purchaseAssetQuoteBO : assetQuoteDetailsList) 
			{
				QuoteDetailsForPOApprovalBean quoteBean = new QuoteDetailsForPOApprovalBean();
				quoteBean.setQuoteId(purchaseAssetQuoteBO.getPkQuoteId());
				quoteBean.setQuoteStatus(purchaseAssetQuoteBO.getQuoteStatus());
				quoteBean.setTotalCost(purchaseAssetQuoteBO.getTotalCost());
				quoteBean.setFkCurrencyType(purchaseAssetQuoteBO.getFkCurrencyType());
				quoteBean.setCostBorneBy(purchaseAssetQuoteBO.getCostBorneBy());
				quoteBean.setIndividualLicenseCost(purchaseAssetQuoteBO.getIndividualLicenseCost());
				quoteBean.setBuContribution(purchaseAssetQuoteBO.getBuContribution());
				quoteBean.setDeliveryDate(purchaseAssetQuoteBO.getDeliveryDate());
				quoteBean.setVendorName(purchaseAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorName());
				quoteBean.setVendorAddress(purchaseAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorAddress());
				quoteBean.setVendorContactNo(purchaseAssetQuoteBO.getPurchaseDatSupplierDetailBO().getVendorContactNo());
				quoteBean.setVendorQuoteReferenceNo(purchaseAssetQuoteBO.getVendorQuoteReferenceNo());
				quoteBean.setFkSupplierAddress(purchaseAssetQuoteBO.getFkSupplierAddress());
				quoteBean.setAssetDescriptionEnteredBySysadmin(purchaseAssetQuoteBO.
						getAssetDescriptionEnteredBySysadmin());
				quoteBeanList.add(quoteBean);
			}
			requestDetailOutputBean.setQuoteList(quoteBeanList);
		}
		detailsOutputBeanList.add(requestDetailOutputBean);
		
		/*}
		else
		{
			throw new CommonCustomException("Selected asset request doesn't have default quote");
		}*/
	
		return detailsOutputBeanList;
	}
	/**
	 * 
	 * @param assetRequestId
	 * @return PurchaseDatAssetRequestBO
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to cancel asset request.
	 */
	public PurchaseDatAssetRequestBO cancelAssetRequest(Integer assetRequestId) throws CommonCustomException {
		LOG.startUsecase("cancel Asset Request");
		PurchaseDatAssetRequestBO purchaseDatAssetRequestDetails = new PurchaseDatAssetRequestBO();
		try {
			purchaseDatAssetRequestDetails = purchaseAssetRequestRepo.findByPkPurchaseRequestId(assetRequestId);
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to retrieve asset details. Kindly contact to Nucleus Support team for this issue.");
		}
		if (purchaseDatAssetRequestDetails == null) {
			throw new CommonCustomException("Asset request does not exist.");
		} else {
			if (purchaseDatAssetRequestDetails.getPurchaseRequestStatus()
					.intValue() == requestWaitingRmApprovalStatus) {
				try {
					purchaseAssetRequestRepo.delete(purchaseDatAssetRequestDetails);
				} catch (Exception ex) {
					throw new CommonCustomException("");
				}
			} else {
				throw new CommonCustomException(
						"Asset request can be cancelled only if it under waiting for RM approval.");
			}         
		}
		LOG.endUsecase("cancel Asset Request");
		return purchaseDatAssetRequestDetails;
	}
	/**
	 * 
	 * @param requestBean
	 * @return PurchaseUpdateAndApproveAssetRequestBean
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to update and approve asset
	 *               request.
	 */
		public boolean updateAndApproveAssetRequest(PurchaseAssetRequestUpdateInputBean purchaseAssetRequestUpdateInputBean,
				List<MultipartFile> uploadSupportDocs )throws CommonCustomException {
					LOG.startUsecase("Update and approve asset request");
					boolean flag = false;
					boolean uploadDocFlag = false;
					PurchaseDatAssetRequestBO outputObject = new PurchaseDatAssetRequestBO();
					List<PurchaseDatAssetQuoteBO> assetQuoteDetailsList = new ArrayList<PurchaseDatAssetQuoteBO>();
					PurchaseDatAssetRequestBO updateAssetRequestOutput = new PurchaseDatAssetRequestBO();
					/*Calendar c = Calendar.getInstance();
					c.setTime(new Date()); 
					c.add(Calendar.DATE, 14); 
					Date twoWeeksCheck = c.getTime();*/
					try {
						updateAssetRequestOutput = purchaseAssetRequestRepo
								.findByPkPurchaseRequestId(purchaseAssetRequestUpdateInputBean.getPurchaseRequestId());
						assetQuoteDetailsList = assetQuoteRepository.findByFkPurchaseRequestId(purchaseAssetRequestUpdateInputBean.getPurchaseRequestId());
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (updateAssetRequestOutput == null) {
						throw new CommonCustomException("Asset request does not exist");
					}
					if(assetQuoteDetailsList.size()>0)
					{
						throw new CommonCustomException("Requested asset request is already having quote.Hence it is not allowed to update");
					}
					/*if(updateAssetRequestOutput.getPurchaseRequestStatus() == requestApprovedByRmStatus ){
						
						throw new CommonCustomException("Asset request is already approved");
						
			    	} */
					if(updateAssetRequestOutput.getRmToApprove().intValue() != purchaseAssetRequestUpdateInputBean.getModifiedBy())
							{
						throw new CommonCustomException("Only requestor RM can update and approve asset request");
							}
					else if (updateAssetRequestOutput.getPurchaseRequestStatus() == requestWaitingRmApprovalStatus ||  
							updateAssetRequestOutput.getPurchaseRequestStatus() == requestApprovedByRmStatus)		
			    	{
						updateAssetRequestOutput.setFkAssetId(purchaseAssetRequestUpdateInputBean.getFkAssetId());
						//updateAssetRequestOutput.setFkAssetDetailId(purchaseAssetRequestUpdateInputBean.getFkAssetDetailsId());//Commented by Anil for removal of fk detail id in asset request Table
						updateAssetRequestOutput.setNoOfUnits(purchaseAssetRequestUpdateInputBean.getNoOfUnits());
						updateAssetRequestOutput.setSpecifications(purchaseAssetRequestUpdateInputBean.getSpecifications());
						updateAssetRequestOutput.setBriefDescription(purchaseAssetRequestUpdateInputBean.getBriefDescription());
						//if(purchaseAssetRequestUpdateInputBean.getAssetRequiredDate().after(twoWeeksCheck)  )
						//{
							updateAssetRequestOutput.setAssetRequiredBy(purchaseAssetRequestUpdateInputBean.getAssetRequiredDate());
						//}
						/*else{
							throw new CommonCustomException("Asset required Date should be greater than 2 weeks from the current Date.");
						}*/
						updateAssetRequestOutput.setDuration(purchaseAssetRequestUpdateInputBean.getDuration());
						updateAssetRequestOutput
								.setIsClientReimbursing(purchaseAssetRequestUpdateInputBean.getIsClientReimbursing());
						updateAssetRequestOutput.setClientId(purchaseAssetRequestUpdateInputBean.getClientId());
						updateAssetRequestOutput.setModifiedOn(new Date());
						updateAssetRequestOutput.setRmUpdatedOn(new Date());
						updateAssetRequestOutput.setModifiedBy(purchaseAssetRequestUpdateInputBean.getModifiedBy());
						updateAssetRequestOutput.setProjectComponentId(purchaseAssetRequestUpdateInputBean.getProjectComponentId());
						updateAssetRequestOutput.setPkPurchaseRequestId(purchaseAssetRequestUpdateInputBean.getPurchaseRequestId());
						updateAssetRequestOutput.setPurchaseRequestStatus(requestApprovedByRmStatus);
						updateAssetRequestOutput.setProjectId(Integer.valueOf(purchaseAssetRequestUpdateInputBean.getProjectId()));
					    if(!uploadSupportDocs.isEmpty()){
							updateAssetRequestOutput.setSupportingDocumentsByClient("UPLOADED");
						}
					}
					else
					{
						throw new CommonCustomException("Asset request can be updated only when it is in 'Waiting RM approval' and it is 'Approved by RM'.");
					}
					try {
						outputObject = purchaseAssetRequestRepo.save(updateAssetRequestOutput);
					} catch (Exception ex) {
						throw new CommonCustomException("Failed to update asset request details.", ex);
					}if (outputObject != null) 
					{
						uploadDocFlag = uploadSupportDocuments(outputObject, uploadSupportDocs);
						
						if(!uploadDocFlag){
							throw new CommonCustomException("Your request has been saved, but failed to store your docs in server. Kindly get in touch with Nucleus Support team.");
						}
						
						flag = true;
					}
									
					//Added by Kamal Anand for Notification Service
					try{
						notificationService.setAssetRequestApprovedNotificationToEmpScreen(updateAssetRequestOutput.getRequestorId());
						notificationService.setAssetRequestNotificationByEmpToRMScreen(updateAssetRequestOutput.getRmToApprove());
						
						List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId((short)Integer.parseInt(sysAdminProcurementRoleId));
						if(empDetail != null)
						{
							empDetail.forEach(emp -> {
								try {
									notificationService.setAssetRequestApprovedNotificationToSysAdminScreen(emp.getPkEmpId());
								} catch (Exception e) {
									LOG.info("Exception occured while setting notification in Update and Approve Asset Request");
								}
							});
						}
						} catch(Exception e){
							LOG.info("Exception occured while setting notification in Update and Approve Asset Request");
						}
					//End of Addition by Kamal Anand for Notification Service
					LOG.endUsecase("Update and approve asset request");
					return flag;
					
				}
		
	//Modified by Shyam for PHASE-2 add new request
	public boolean addNewAssetRequestAsSystemAdmin(PurchaseAddNewAssetRequestInputBean 
			purchaseAddNewAssetRequestInputBean, List<MultipartFile> uploadSupportDocs) 	
					throws CommonCustomException 
	{
		boolean uploadDocFlag = false;
		boolean successfullyAddedflag = false;
		PurchaseMasAssetsBO assetObject = new PurchaseMasAssetsBO();
		PurchaseDatAssetRequestBO newAssetRequest = new PurchaseDatAssetRequestBO();
		PurchaseDatAssetRequestBO assetRequestOutput = new PurchaseDatAssetRequestBO();
		List<PurchaseDatAssetQuoteBO> addedQuotesList = new ArrayList<PurchaseDatAssetQuoteBO>();
		
		//quotesList in inputBean
		int disagreeCount = 0;
	
		//Collect all role ids of System Admin.
		List<Short> systemAdminRoleIdList = new ArrayList<Short>();
	  	String[] empIdArray = systemAdminRoles.split(",");
	  	for (String s : empIdArray)
	  		systemAdminRoleIdList.add(Short.parseShort(s));
			
		DatEmpDetailBO systemAdminEmpDetailBo = new DatEmpDetailBO();
		try{
			systemAdminEmpDetailBo = empDetailRepo.getSystemAdminRoleDetailsForAddAsset(
					purchaseAddNewAssetRequestInputBean.getRequestorId(),systemAdminRoleIdList);
		}catch(Exception ex){
			throw new CommonCustomException("Failed to get system admin details from backend");
		}
			
		if(systemAdminEmpDetailBo == null)
			throw new CommonCustomException("The requestor is not having role of system admin");
				
		//Two weeks check
		Calendar c = Calendar.getInstance();
	 	c.setTime(new Date()); 
	 	c.add(Calendar.DATE, 14);
	 	Date twoWeeksCheck = c.getTime();
	 		
	 	if(purchaseAddNewAssetRequestInputBean.getAssetRequiredDate().after(twoWeeksCheck)  )
		{
	 		newAssetRequest.setAssetRequiredBy(purchaseAddNewAssetRequestInputBean.getAssetRequiredDate());
		}else{
			throw new CommonCustomException("Asset required Date should be greater 2 weeks from the current Date.");
		}
			
		//--------------------------------------------
		//Set the value for new Asset request
			
		newAssetRequest.setRequestorId(purchaseAddNewAssetRequestInputBean.getRequestorId());
		newAssetRequest.setFkAssetId(purchaseAddNewAssetRequestInputBean.getFkAssetId());
		newAssetRequest.setNoOfUnits(purchaseAddNewAssetRequestInputBean.getNoOfUnits());
		newAssetRequest.setSpecifications(purchaseAddNewAssetRequestInputBean.getSpecifications());
		newAssetRequest.setBriefDescription(purchaseAddNewAssetRequestInputBean.getBriefDescription());
		newAssetRequest.setFkRequestTypeId(purchaseAddNewAssetRequestInputBean.getPurchaseRequestTypeId());
		newAssetRequest.setDuration(purchaseAddNewAssetRequestInputBean.getDuration());
		newAssetRequest.setIsClientReimbursing(purchaseAddNewAssetRequestInputBean.getIsClientReimbursing());
		newAssetRequest.setRmToApprove(purchaseAddNewAssetRequestInputBean.getRmToApprove());
		newAssetRequest.setCreatedDate(new Date());
		newAssetRequest.setPurchaseRequestStatus(Integer.valueOf(requestWaitingRmApprovalStatus));
		newAssetRequest.setLocationId(purchaseAddNewAssetRequestInputBean.getLocationId());
		newAssetRequest.setFkBuId(purchaseAddNewAssetRequestInputBean.getFkBuId());			
		newAssetRequest.setPriority(purchaseAddNewAssetRequestInputBean.getPriority());
		
		if(!uploadSupportDocs.isEmpty()){
			newAssetRequest.setSupportingDocumentsByClient("UPLOADED");
		}
					
		//--------------------------------------------
		 		
	 	try{
	 		assetObject = purchaseMasAssetsRepo.findByPkAssetId(purchaseAddNewAssetRequestInputBean.getFkAssetId());
	 	}catch(Exception ex){
	 		throw new CommonCustomException("Failed to retrieve asset information from backend.");
	 	}
		
		if(assetObject == null)
			throw new CommonCustomException("Asset not found.");
		
		//Input quotes validations
		List<AddQuoteInputBeanPhase2> phase2AddquoteBeanList = purchaseAddNewAssetRequestInputBean.getQuoteList();
		
		if ( phase2AddquoteBeanList != null)
		{   
			if (phase2AddquoteBeanList.size() > maximumQuantityOfQuote )
			{
				 throw new CommonCustomException("Only 3 quote can be added for one request");
			}
			else { 
				//check for disagree quotes in passed input quote list
				for ( AddQuoteInputBeanPhase2 quoteBo : phase2AddquoteBeanList) 
				{
					if (quoteBo.getIsSetDefault().equalsIgnoreCase("DISAGREE")) 
					{ 
						disagreeCount++;
					}
				}
				//check if all no quote set as default agreed 	
				if (disagreeCount == maximumQuantityOfQuote)
				{   
					throw new CommonCustomException("All the quotes cannot be disagreed");
				}
				//check if more than one quote is set as default
				//check if 3 quotes added and more than one quote is set as default quote
				else if (phase2AddquoteBeanList.size() == maximumQuantityOfQuote)
				{      
					if (disagreeCount == 0)
					{
						throw new CommonCustomException("Not more than one quote can be set as default");
					}
					else if (disagreeCount == 1)
					{
						throw new CommonCustomException("Only one quote can be set as agreed");
					}
					 		
				}else {  //check for when 2 quotes are uploaded and both are set as default quote
					if (phase2AddquoteBeanList.size() == 2)
					{
						if (disagreeCount == 0)
						{
							throw new CommonCustomException("All quotes cannot be set as default");
						}
					}
				}
			}

			try {
				assetRequestOutput = purchaseAssetRequestRepo.save(newAssetRequest);				
			}catch(Exception ex)
			{
				throw new CommonCustomException("Failed to store new asset request.");
			}
					
			//---------------------------------------------------------
			//Upload of Support document, if any
			if (assetRequestOutput != null) 
			{	
				uploadDocFlag = uploadSupportDocuments(assetRequestOutput, uploadSupportDocs);
			
				if(!uploadDocFlag){
					throw new CommonCustomException("Your request has been saved, but failed to store your docs in server. Kindly get in touch with Nucleus Support team.");
				}
			}
				
			//-----------------------------------------------------------------
			//Number of quotes validations
			try{
				addedQuotesList = assetQuoteRepository
						.findByFkPurchaseRequestId(assetRequestOutput.getPkPurchaseRequestId());
			}catch(Exception ex){
				throw new CommonCustomException("Failed to retrieve quotes list. Kindly get in touch with Nucleus Support team.");
			}
			
				
			if(addedQuotesList.size() == maximumQuantityOfQuote)
			{
				throw new CommonCustomException("Maximun 3 quotes can be added.");
			}
			
			//Always create a new record for new and exist subscription/license id
			
			PurchaseMasAssetDetailsBO newAssetDetails = new PurchaseMasAssetDetailsBO();
			newAssetDetails.setAssetDescription(assetRequestOutput.getBriefDescription());
			newAssetDetails.setCreatedBy(purchaseAddNewAssetRequestInputBean.getRequestorId());
			newAssetDetails.setCreatedDate(new Date());
			
			//location either be bangalore or Gurgaon
			if(assetRequestOutput.getLocationId() == gurgaonLocationId.shortValue() 
					&& assetObject.getAssetType().equalsIgnoreCase("HARDWARE"))
			{
				newAssetDetails.setFklocationId(Integer.valueOf(gurgaonLocationId));
			}
			else{
				newAssetDetails.setFklocationId(bangaloreLocationId);
			}
			
			newAssetDetails.setFkMasAssetsId(assetRequestOutput.getFkAssetId());
			newAssetDetails.setFkPurchaseRequestId(assetRequestOutput.getPkPurchaseRequestId());
			newAssetDetails.setSoftwareLicenseOrSubscriptionId(
					purchaseAddNewAssetRequestInputBean.getSoftwareLicenseOrSubscriptionId());
			purchaseAssetDetailRepo.save(newAssetDetails);
		
			successfullyAddedflag = addQuotePhase2(purchaseAddNewAssetRequestInputBean.getQuoteList(),				
							assetRequestOutput.getPkPurchaseRequestId(),purchaseAddNewAssetRequestInputBean.getRequestorId());
		}else {
			throw new CommonCustomException("Please enter quotes for this request");
		}

		return successfullyAddedflag;
	}
	
	public boolean addQuotePhase2 (List<AddQuoteInputBeanPhase2> quoteList , int requestId , int createdBy ) throws CommonCustomException
	{   
		//Add quotes code
		LOG.info("Addquote method" +quoteList.size()+ requestId + createdBy);
		boolean output=false;
		
		for (AddQuoteInputBeanPhase2 quoteinputBean : quoteList)
		{
			PurchaseDatAssetQuoteBO newQuoteObject = new PurchaseDatAssetQuoteBO();
					
			//Validations for costBorneBy
			if (quoteinputBean.getCostBorneBy().equalsIgnoreCase("IT")
					|| quoteinputBean.getCostBorneBy().equalsIgnoreCase("BU")) 
			{ // Total cost should be greater than equal to getIndividualLicenseCost
				if (quoteinputBean.getIndividualLicenseCost() > quoteinputBean.getTotalCost()) 
				{
					throw new CommonCustomException("Individual license cost cannot exceed total cost");
				} else {
					newQuoteObject.setCostBorneBy(quoteinputBean.getCostBorneBy());
					newQuoteObject.setBuContribution(quoteinputBean.getTotalCost());
				}
			}// if cost is shared BUcontribution is required
			else if (quoteinputBean.getCostBorneBy().equalsIgnoreCase("SHARED")) 
			{
				if(quoteinputBean.getBuContribution() == null)
					throw new CommonCustomException("Please enter amount in BU Contribution field");
						
				if (quoteinputBean.getBuContribution().doubleValue() >= quoteinputBean.getTotalCost()) 
				{
					throw new CommonCustomException("BU contribution cannot be equal to OR more than total cost");
				} 
				else if (quoteinputBean.getIndividualLicenseCost() > quoteinputBean.getTotalCost())
				{
					throw new CommonCustomException("Individual license cost cannot be more than total cost");
				} else {
					newQuoteObject.setCostBorneBy(quoteinputBean.getCostBorneBy());
					newQuoteObject.setBuContribution(quoteinputBean.getBuContribution());
				}
			}
						
			//--------------------------------------------------
					
			if (quoteinputBean.getIsSetDefault().equalsIgnoreCase("DISAGREE")) 
			{
				newQuoteObject.setQuoteStatus("DISAGREE");	
			}else if (quoteinputBean.getIsSetDefault().equalsIgnoreCase("AGREE")) 
				newQuoteObject.setQuoteStatus("AGREE");
					
			newQuoteObject.setCreatedBy(createdBy);
			newQuoteObject.setCreatedDate(new Date());
			newQuoteObject.setDeliveryDate(quoteinputBean.getDeliveryDate());
			newQuoteObject.setFkPurchaseRequestId(requestId);
			newQuoteObject.setFkSupplierAddress(quoteinputBean.getFkSupplierAddress());
			newQuoteObject.setIndividualLicenseCost(Double.valueOf(quoteinputBean.getIndividualLicenseCost()));
			newQuoteObject.setTotalCost(Double.valueOf(quoteinputBean.getTotalCost()));
			newQuoteObject.setFkCurrencyType(quoteinputBean.getCurrencyType());
						
			if(quoteinputBean.getVendorQuoteReferenceNo() != null)
				newQuoteObject.setVendorQuoteReferenceNo(quoteinputBean.getVendorQuoteReferenceNo());
			
			if(quoteinputBean.getAssetDescriptionEnteredBySysadmin() != null)
				if(quoteinputBean.getAssetDescriptionEnteredBySysadmin().isEmpty()){
					throw new CommonCustomException("Please enter asset description /specification");
				}else{
					newQuoteObject.setAssetDescriptionEnteredBySysadmin(quoteinputBean.
							getAssetDescriptionEnteredBySysadmin());
				}
			else
				throw new CommonCustomException("Asset description / specification cannot be null");
									
			try{
				assetQuoteRepository.save(newQuoteObject);
				output = true;
			}catch(Exception ex){
				throw new CommonCustomException("Failed to save quote status.");
			}
		}
		return output;
	}
	
	
	
	//EOA by Shyam for PHASE-2 add new request	
	
	public boolean updateNewAssetRequestAsSystemAdmin(PurchaseUpdateNewAssetRequestInputBean 
			purchaseUpdateNewAssetRequestInputBean,
			List<MultipartFile> uploadSupportDocs) throws CommonCustomException 
	{
		LOG.startUsecase("update Asset Request Details as a System admin");
		boolean flag = false;
		boolean uploadDocFlag = false;
		PurchaseDatAssetRequestBO outputObject = new PurchaseDatAssetRequestBO();
	//	PurchaseMasAssetDetailsBO masassetDetailsBo = new PurchaseMasAssetDetailsBO();
		PurchaseMasAssetsBO masassetDetails = new PurchaseMasAssetsBO();
		PurchaseDatAssetRequestBO updateAssetRequestOutput = new PurchaseDatAssetRequestBO();
		DatEmpDetailBO loggedInUserEmpDetailBO = new DatEmpDetailBO();
		Calendar c = Calendar.getInstance();
		c.setTime(new Date()); 
		c.add(Calendar.DATE, 14);
		Date twoWeeksCheck = c.getTime();
		try {
			updateAssetRequestOutput = purchaseAssetRequestRepo
					.findByPkPurchaseRequestId(purchaseUpdateNewAssetRequestInputBean.getPkPurchaseRequestId());
			masassetDetails = purchaseMasAssetsRepo.findByPkAssetId(
					purchaseUpdateNewAssetRequestInputBean.getFkAssetId());
			//masassetDetailsBo = purchaseAssetDetailRepo.findByFkPurchaseRequestId(purchaseUpdateNewAssetRequestInputBean.getPkPurchaseRequestId());
			loggedInUserEmpDetailBO = empDetailRepository.findByPkEmpId(
					purchaseUpdateNewAssetRequestInputBean.getModifiedBy());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (updateAssetRequestOutput == null) {
			throw new CommonCustomException("Asset request does not exist.");
		}
		
		if(loggedInUserEmpDetailBO == null){
			throw new CommonCustomException("Failed to fetch logged in user detail. Kindly get in touch with Nucleus Support team.");
		}
		
		//Short[] arrayOfSysAdminRoles = systemAdminRoles.split(",");
		List<Short> systemAdminRoleIdList = new ArrayList<Short>();
		String[] empIdArray = systemAdminRoles.split(",");
		for (String s : empIdArray)
			systemAdminRoleIdList.add(Short.parseShort(s));
		
		if(!systemAdminRoleIdList.contains(loggedInUserEmpDetailBO.getFkEmpRoleId())){
			throw new CommonCustomException("Only the system admin roles can update the asset request.");
		}
		
	/*	if(updateAssetRequestOutput.getRequestorId().intValue() != purchaseUpdateNewAssetRequestInputBean.getModifiedBy())
		{
			throw new CommonCustomException("Only the respective requestor can update the asset request.");
		}*/
		else if (updateAssetRequestOutput.getPurchaseRequestStatus().intValue() == requestWaitingRmApprovalStatus)
		{
			//updateAssetRequestOutput.setRequestorId(purchaseUpdateNewAssetRequestInputBean.getRequestorId());
			updateAssetRequestOutput.setFkAssetId(purchaseUpdateNewAssetRequestInputBean.getFkAssetId());
			//updateAssetRequestOutput.setFkAssetDetailId(purchaseUpdateNewAssetRequestInputBean.getFkAssetDetailsId());//Commented by Anil for removal of fk detail id in asset request Table
			updateAssetRequestOutput.setNoOfUnits(purchaseUpdateNewAssetRequestInputBean.getNoOfUnits());
			updateAssetRequestOutput.setSpecifications(purchaseUpdateNewAssetRequestInputBean.getSpecifications());
			updateAssetRequestOutput.setBriefDescription(purchaseUpdateNewAssetRequestInputBean.getBriefDescription());
			updateAssetRequestOutput.setFkRequestTypeId(purchaseUpdateNewAssetRequestInputBean.getPurchaseRequestTypeId());
			if(purchaseUpdateNewAssetRequestInputBean.getAssetRequiredDate().after(twoWeeksCheck) )
			{
				updateAssetRequestOutput.setAssetRequiredBy(purchaseUpdateNewAssetRequestInputBean.getAssetRequiredDate());
			}else{
				throw new CommonCustomException("Asset required Date should be greater 2 weeks from the current Date.");
			}
			updateAssetRequestOutput.setDuration(purchaseUpdateNewAssetRequestInputBean.getDuration());
			updateAssetRequestOutput.setIsClientReimbursing(
					purchaseUpdateNewAssetRequestInputBean.getIsClientReimbursing());
			//updateAssetRequestOutput.setClientId(purchaseUpdateNewAssetRequestInputBean.getClientId());
			updateAssetRequestOutput.setRmToApprove(purchaseUpdateNewAssetRequestInputBean.getRmToApprove());
			updateAssetRequestOutput.setModifiedOn(new Date());
			updateAssetRequestOutput.setModifiedBy(purchaseUpdateNewAssetRequestInputBean.getModifiedBy());
			updateAssetRequestOutput.setPurchaseRequestStatus(1);
			//updateAssetRequestOutput.setProjectComponentId(purchaseUpdateNewAssetRequestInputBean.getProjectComponentId());
			//updateAssetRequestOutput.setProjectId(purchaseUpdateNewAssetRequestInputBean.getProjectId());
			updateAssetRequestOutput.setLocationId(purchaseUpdateNewAssetRequestInputBean.getLocationId());
			updateAssetRequestOutput.setFkBuId(purchaseUpdateNewAssetRequestInputBean.getFkBuId());
			updateAssetRequestOutput.setPriority(purchaseUpdateNewAssetRequestInputBean.getPriority());
			
			if(masassetDetails == null)
			{
				throw new CommonCustomException("Asset details not found for the requested assetId.");
			}
			
			/*if(masassetDetailsBo == null)
			{
				throw new CommonCustomException("Asset details not found for the requested assetDetailId");
			}
			*/
			masassetDetails.setLicenseType(purchaseUpdateNewAssetRequestInputBean.getLicenceType());
			/*masassetDetailsBo.setSoftwareLicenseOrSubscriptionId(
					purchaseUpdateNewAssetRequestInputBean.getSoftwareLicenseOrSubscriptionId());
			*/
			if(!uploadSupportDocs.isEmpty()){
				updateAssetRequestOutput.setSupportingDocumentsByClient("UPLOADED");
			}
		}else{
			throw new CommonCustomException("Asset request can be updated only when it is in waiting for RM approval.");
		}
		
		try {
			outputObject = purchaseAssetRequestRepo.save(updateAssetRequestOutput);
			purchaseMasAssetsRepo.save(masassetDetails);
		//	purchaseAssetDetailRepo.save(masassetDetailsBo);
		} catch (Exception e) {
			throw new CommonCustomException("Unable to store your asset request. Kindly contact Nucleus Support team for this issue.");
		}
			
		if (outputObject != null) 
		{
			uploadDocFlag = uploadSupportDocuments(outputObject, uploadSupportDocs);
			if(!uploadDocFlag){
				throw new CommonCustomException("Your request has been saved, but failed to store your docs in server. Kindly get in touch with Nucleus Support team.");
			}
			flag = true;
		}
		
		LOG.endUsecase("update Asset Request Details as a System admin");
		return flag;
	}
		
		public boolean updateAndApproveAssetRequestForSystemAdmin(UpdateAndApproveAssetRequestForSystemAdminBean 
				updateAndApproveAssetRequestForSystemAdminBean,
				List<MultipartFile> uploadSupportDocs )throws CommonCustomException 
		{
			LOG.startUsecase("Update and approve asset request for system admin");
			boolean flag = false;
			boolean uploadDocFlag = false;
			Boolean success = false;
			PurchaseDatAssetRequestBO outputObject = new PurchaseDatAssetRequestBO();
			PurchaseDatAssetQuoteBO assetQuoteDetails = new PurchaseDatAssetQuoteBO();
			PurchaseDatAssetRequestBO updateAssetRequest = new PurchaseDatAssetRequestBO();
			PurchaseDatCapexBO previousCapex = new PurchaseDatCapexBO();
			PurchaseDatCapexBO capexBo = new PurchaseDatCapexBO();
			PurchaseDatAssetRequestBO assetReqBo = new PurchaseDatAssetRequestBO();
			PurchaseDatAssetQuoteBO assetQuoteBo = new PurchaseDatAssetQuoteBO();
			try {
				updateAssetRequest = purchaseAssetRequestRepo
						.findByPkPurchaseRequestId(updateAndApproveAssetRequestForSystemAdminBean.getPurchaseRequestId());
				assetQuoteDetails = assetQuoteRepository.getAgreeQuoteDetails(updateAndApproveAssetRequestForSystemAdminBean.getPurchaseRequestId());
				assetQuoteBo = assetQuoteRepository
						.findByFkPurchaseRequestIdAndQuoteStatus(
								updateAndApproveAssetRequestForSystemAdminBean.getPurchaseRequestId(), "AGREE");
			//	outputObject = 
				assetReqBo = purchaseAssetRequestRepo
						.findByPkPurchaseRequestId(updateAndApproveAssetRequestForSystemAdminBean.getPurchaseRequestId());
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (assetReqBo == null) {
				throw new CommonCustomException(
						"The asset request Id does't exist, please pass proper id");
			}
			if (updateAssetRequest == null) {
				throw new CommonCustomException("Asset request does not exist");
			}
			
			if(updateAssetRequest.getPurchaseRequestStatus().intValue() == purchaseCapexCreatedBySysAdmin)
			{
				throw new CommonCustomException("Asset request is already approved.");
			}
					
			if(assetQuoteDetails == null)
			{
				throw new CommonCustomException("Requested asset request is not having any default quote");
			}
			
			if(updateAssetRequest.getRmToApprove().intValue() != 
					updateAndApproveAssetRequestForSystemAdminBean.getModifiedBy())
			{
				throw new CommonCustomException("Only requestor RM can update and approve asset request");
			}
			else if (updateAssetRequest.getPurchaseRequestStatus() == requestWaitingRmApprovalStatus ||  
					updateAssetRequest.getPurchaseRequestStatus() == requestApprovedByRmStatus)			  		
			{
				updateAssetRequest.setIsClientReimbursing(updateAndApproveAssetRequestForSystemAdminBean.
						getIsClientReimbursing());
				updateAssetRequest.setClientId(updateAndApproveAssetRequestForSystemAdminBean.getClientId());
				updateAssetRequest.setModifiedOn(new Date());
				updateAssetRequest.setRmUpdatedOn(new Date());
				updateAssetRequest.setModifiedBy(updateAndApproveAssetRequestForSystemAdminBean.getModifiedBy());
				updateAssetRequest.setProjectComponentId(updateAndApproveAssetRequestForSystemAdminBean.getProjectComponentId());
				updateAssetRequest.setPurchaseRequestStatus(purchaseCapexCreatedBySysAdmin);
				updateAssetRequest.setProjectId(updateAndApproveAssetRequestForSystemAdminBean.getProjectId());
				if(!uploadSupportDocs.isEmpty()){
				   	updateAssetRequest.setSupportingDocumentsByClient("UPLOADED");
				}
			}else{
				throw new CommonCustomException("Asset request can be updated only when it is in 'Waiting RM approval' and it is 'Approved by RM'.");
			}
          
			
            //Find out default quote object
            PurchaseDatAssetQuoteBO defaultQuote = new PurchaseDatAssetQuoteBO();
            try{
            	defaultQuote = assetQuoteRepository.findByFkPurchaseRequestIdAndQuoteStatus(
            			updateAndApproveAssetRequestForSystemAdminBean.getPurchaseRequestId() , "AGREE");
            }catch(Exception ex){
            	throw new CommonCustomException("Failed to retrieve default quote of the provided request id.");
            }
            
            if(defaultQuote == null)
            {
            	throw new CommonCustomException("No default quote found.");
            }
            
            //NBU-IT Business Unit detail
            MasBuUnitBO nbuITDetail = new MasBuUnitBO();
            try{
            	nbuITDetail = buRepository.findByPkBuUnitId(itBuId);
            }catch(Exception ex){
            	throw new CommonCustomException("Failed to fetch NBU IT details from backend.");
            }
            
            if(nbuITDetail == null){
            	throw new CommonCustomException("NBu IT business unit not found.");
            }
            
            //Enter records to purchase_dat_bu table for BU head approval
            PurchaseDatBuBO buHeadRecord = null;
            PurchaseDatBuBO itHeadRecord = null;
            
            if(defaultQuote.getCostBorneBy().equalsIgnoreCase("SHARED"))
            {
            	//Enter BU Head entry
            	buHeadRecord =  new PurchaseDatBuBO();
            	buHeadRecord.setFkMasBuId(updateAssetRequest.getFkBuId());
            	buHeadRecord.setBuHeadId(
            			Integer.valueOf(updateAssetRequest.getMasBuUnitBO().getFkBuHeadEmpId()).shortValue());
            	buHeadRecord.setFkPurchaseQuoteId(defaultQuote.getPkQuoteId());
            	buHeadRecord.setCreatedBy(updateAndApproveAssetRequestForSystemAdminBean.getModifiedBy());
            	buHeadRecord.setCreatedDate(new Date());
            	buHeadRecord.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
            	datBuRepository.save(buHeadRecord);
            	
            	if(updateAssetRequest.getMasBuUnitBO().getFkBuHeadEmpId() != nbuITDetail.getFkBuHeadEmpId())
            	{
            		itHeadRecord = new PurchaseDatBuBO();
            		itHeadRecord.setFkMasBuId(nbuITDetail.getPkBuUnitId());
            		itHeadRecord.setBuHeadId(Integer.valueOf(nbuITDetail.getFkBuHeadEmpId()).shortValue());
            		itHeadRecord.setFkPurchaseQuoteId(defaultQuote.getPkQuoteId());
            		itHeadRecord.setCreatedBy(updateAndApproveAssetRequestForSystemAdminBean.getModifiedBy());
            		itHeadRecord.setCreatedDate(new Date());
            		itHeadRecord.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
                	datBuRepository.save(itHeadRecord);
            	}
            	flag = true;
            }else if(defaultQuote.getCostBorneBy().equalsIgnoreCase("BU"))
            {
            	//Enter BU Head entry
            	buHeadRecord =  new PurchaseDatBuBO();
            	buHeadRecord.setFkMasBuId(updateAssetRequest.getFkBuId());
            	buHeadRecord.setBuHeadId(
            			Integer.valueOf(updateAssetRequest.getMasBuUnitBO().getFkBuHeadEmpId()).shortValue());
            	buHeadRecord.setFkPurchaseQuoteId(defaultQuote.getPkQuoteId());
            	buHeadRecord.setCreatedBy(updateAndApproveAssetRequestForSystemAdminBean.getModifiedBy());
            	buHeadRecord.setCreatedDate(new Date());
            	buHeadRecord.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
            	datBuRepository.save(buHeadRecord);
            	flag = true;
            }
            else if(defaultQuote.getCostBorneBy().equalsIgnoreCase("IT"))
            {
        		itHeadRecord = new PurchaseDatBuBO();
        		itHeadRecord.setFkMasBuId(nbuITDetail.getPkBuUnitId());
        		itHeadRecord.setBuHeadId(Integer.valueOf(nbuITDetail.getFkBuHeadEmpId()).shortValue());
        		itHeadRecord.setFkPurchaseQuoteId(defaultQuote.getPkQuoteId());
        		itHeadRecord.setCreatedBy(updateAndApproveAssetRequestForSystemAdminBean.getModifiedBy());
        		itHeadRecord.setCreatedDate(new Date());
        		itHeadRecord.setFkPurchaseDatBuStatus("WAITING FOR APPROVAL");
            	datBuRepository.save(itHeadRecord);
            	flag = true;
            }
            else{
            	throw new CommonCustomException("Default quote's cost borne by neither be BU nor IT nor shared.");
            }
            
            //Update asset request table
            try {
				outputObject = purchaseAssetRequestRepo.save(updateAssetRequest);
			} catch (Exception ex) {
				throw new CommonCustomException("Failed to update asset request details.", ex);
			}
            
            if (outputObject != null) 
			{
				uploadDocFlag = uploadSupportDocuments(outputObject, uploadSupportDocs);
				if(!uploadDocFlag)
				{
					throw new CommonCustomException("Your request has been saved, but failed to store your docs in server. Kindly get in touch with Nucleus Support team.");
				}
			}
            
            // create capex documentation
            previousCapex = capexRepository.findTop1ByOrderByPkCapexIdDesc();
			String capexNumber = previousCapex.getCapexNo();
			// Auto increment of capex number
			String[] capexArray = capexNumber.split("-");
			Integer capexNum = Integer.parseInt(capexArray[2]);
			String incrementPoNum = String.valueOf(++capexNum);
			if (incrementPoNum.length() < 3) {
				while (incrementPoNum.length() != 3) {
					incrementPoNum = "0" + incrementPoNum;
				}
			}
			String newPoNum;
			Date date = new Date();
			Calendar cal = new GregorianCalendar();
			cal.setTime(date);
			int newYear = cal.get(Calendar.YEAR);
			// For new year capex number starts with 001
			if (!capexArray[1].contains(String.valueOf(newYear))) 
			{
				String currentYear = capexArray[1].replace(capexArray[1].substring(3),String.valueOf(newYear));
				newPoNum = capexArray[0] + "-" + currentYear + "-" + "001";
			} else {
				newPoNum = capexArray[0] + "-" + capexArray[1] + "-" + incrementPoNum;
			}
			capexBo.setCapexNo(newPoNum);
			capexBo.setCreatedOn(new Date());
			capexBo.setCreatedBy(updateAndApproveAssetRequestForSystemAdminBean.getModifiedBy());
			capexBo.setCapexUploaded(purchaseCapexUploaded);
			capexBo = capexRepository.save(capexBo);
			
			PurchaseDatAssetQuoteBO requestQuoteBO = new PurchaseDatAssetQuoteBO();
			requestQuoteBO = assetQuoteBo;
            
			// PDF file creation for purchase order.
			String buName = assetReqBo.getMasBuUnitBO().getBuUnitName();
			String buHeadName = gsrGoalService.getEmpNameByEmpId(assetReqBo.getMasBuUnitBO().getFkBuHeadEmpId());
			MasBuUnitBO nbuUnit = buRepository.findByPkBuUnitId(purchaseNbuItUnitId);
			String buItHeadName = gsrGoalService.getEmpNameByEmpId(nbuUnit.getFkBuHeadEmpId());
			MasBuUnitBO financeBUBo = buRepository.findByPkBuUnitId(financeBuId);
			String financeHeadName = gsrGoalService.getEmpNameByEmpId(financeBUBo.getFkBuHeadEmpId());
			String buComponentName = "";
			String projectName = "";
			List<CreatePDFForCapexReportBean> list = new ArrayList<CreatePDFForCapexReportBean>();
			// Added by Kamal Anand
			List<CreatePDFForCapexSharedReportBean> pdfList = new ArrayList<CreatePDFForCapexSharedReportBean>();
			CreatePDFForCapexSharedReportBean createCapexPdfBean = new CreatePDFForCapexSharedReportBean();
			// End of Addition by Kamal Anand
			CreatePDFForCapexReportBean createPDFBean = new CreatePDFForCapexReportBean();
			// Need quote object data to get cost
			createPDFBean.setCost(requestQuoteBO.getTotalCost());
			createPDFBean.setCapexNumber(capexBo.getCapexNo());
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			createPDFBean.setCreatedDate(capexBo.getCreatedOn());
			createPDFBean.setLocationName(assetReqBo.getMasLocationParentBO().getLocationParentName());
			
			//createPDFBean.setProductDetails(assetReqBo.getSpecifications());
			createPDFBean.setProductDetails(requestQuoteBO.getAssetDescriptionEnteredBySysadmin());
			
			createPDFBean.setPurpose(assetReqBo.getBriefDescription());
			createPDFBean.setNumberOfUnits(assetReqBo.getNoOfUnits().intValue());
			// Get from leagcy service
			// get component & client details from legacy service
			ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean();
			clientProjCompDetailsBean.setClientId(outputObject.getClientId());
			clientProjCompDetailsBean.setProjectId(outputObject.getProjectId());
			clientProjCompDetailsBean.setComponentId(outputObject.getProjectComponentId());
			clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
			LOG.info("clientProjCompDetailsBean :: " + clientProjCompDetailsBean);
			PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
			ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService
					.getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
			if (clientProjectCompDetails == null) {
				throw new CommonCustomException(
						"Unable to fetch project, client and component details from legacy MIS.");
			} else {
				buComponentName = clientProjectCompDetails.getComponentName();
				projectName = clientProjectCompDetails.getProjectName();
			}
			
			if (requestQuoteBO.getCostBorneBy().equalsIgnoreCase("SHARED")) 
			{
				// Added by Kamal Anand
				createCapexPdfBean.setCost(requestQuoteBO.getTotalCost());
				createCapexPdfBean.setCapexNumber(capexBo.getCapexNo());
				//SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
				createCapexPdfBean.setCreatedDate(capexBo.getCreatedOn());
				createCapexPdfBean.setLocationName(assetReqBo.getMasLocationParentBO().getLocationParentName());
				//createCapexPdfBean.setProductDetails(assetReqBo.getSpecifications());
				createPDFBean.setProductDetails(requestQuoteBO.getAssetDescriptionEnteredBySysadmin());
				
				createCapexPdfBean.setPurpose(assetReqBo.getBriefDescription());
				SimpleDateFormat sdfYear1 = new SimpleDateFormat("yyyy");
				createCapexPdfBean.setYear(sdfYear1.format(capexBo.getCreatedOn()));
				createCapexPdfBean.setNumberOfUnits(assetReqBo.getNoOfUnits());
				createCapexPdfBean.setBuHeadName(buHeadName);
				createCapexPdfBean.setBuItHeadName(buItHeadName);
				createCapexPdfBean.setCostCenter("SHARED");
				createCapexPdfBean.setBuName(assetReqBo.getMasBuUnitBO().getBuUnitName());
				createCapexPdfBean.setItBuName("NBU-IT");
				createCapexPdfBean.setComponentNumber(buComponentName);
				createCapexPdfBean.setItComponentNumber(nbuItComponentName);
				createCapexPdfBean.setItBuProjectName(nbuItProjectName);
				createCapexPdfBean.setBuProjectName(projectName);
				// End of Addition by Kamal Anand
			} else if (requestQuoteBO.getCostBorneBy().equalsIgnoreCase("BU")) 
			{
				createPDFBean.setBuHeadName(buHeadName);
				createPDFBean.setItHeadName(buItHeadName);
				createPDFBean.setFinanceHeadName(financeHeadName);
				createPDFBean.setCostCenter("BU");
				createPDFBean.setBuName(buName);
				createPDFBean.setComponentNumber(buComponentName);
				createPDFBean.setProjectName(projectName);
			} else if (requestQuoteBO.getCostBorneBy().equalsIgnoreCase("IT")) 
			{
				createPDFBean.setBuHeadName(buItHeadName);
				createPDFBean.setItHeadName(buItHeadName);
				createPDFBean.setFinanceHeadName(financeHeadName);
				createPDFBean.setCostCenter("IT");
				createPDFBean.setBuName(nbuUnit.getBuUnitName());
				createPDFBean.setComponentNumber(nbuItComponentName);
				createPDFBean.setProjectName(nbuItProjectName);
			}
			
			list.add(createPDFBean);
			pdfList.add(createCapexPdfBean);
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(assetReqBo.getCreatedDate());
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH) + 1;
			String capexFileName = capexBo.getCapexNo().replace("/", "_");
			FileOutputStream fos = null;
			try {
				String filePath = purchaseDocsBasePath + File.separator + year + File.separator 
						+ month + File.separator + assetReqBo.getPkPurchaseRequestId().intValue() 
						+ File.separator + "Capex" + File.separator;
				File someFile = new File(purchaseDocsBasePath + year + File.separator
						+ month + File.separator + assetReqBo.getPkPurchaseRequestId().intValue() 
						+ File.separator + "Capex");
				
				if (!someFile.exists()) {
					someFile.mkdirs();
				}
				filePath = filePath + capexFileName + ".pdf";
				someFile = new File(filePath);
				fos = new FileOutputStream(someFile);
				if (requestQuoteBO.getCostBorneBy().equalsIgnoreCase("SHARED")) {
					LOG.startUsecase("Comming inside shared list");
					LOG.startUsecase("pdfList:" + pdfList);
					purchaseCapexService.generateJasperReportPDF(capexSharedTemplatePath, fos,pdfList);
				} else {
					purchaseCapexService.generateJasperReportPDF(capexTemplateBasePath, fos, list);
				}
				LOG.debug("<<<<<<<<<<<< Report Created >>>>>>>>");
			} catch (FileNotFoundException e) {
				LOG.debug("Error in creating capex file : " + e);
			} finally {
				if (fos != null) {
					try {
						fos.flush();
						fos.close();
					} catch (IOException e) {
						LOG.debug("Error in creating capex file : closing file "+ e);
					}
				}
			}
			success = true;
			LOG.endUsecase("Update and approve asset request for system admin");
			return flag;
					
		}
	// EOA by pratibha TR
		
	// Added by Smrithi
	public boolean addNewAssetRequest(PurchaseNewAssetRequestCreateInputBean purchaseNewAssetRequestCreateInputBean,
			List<MultipartFile> uploadSupportDocs) 	throws CommonCustomException 
	{
		LOG.startUsecase("Add asset request");
		boolean flag = false;
		boolean uploadDocFlag = false;
		PurchaseDatAssetRequestBO outputObject = new PurchaseDatAssetRequestBO();
		PurchaseDatAssetRequestBO addNewAssetRequestOutput = new PurchaseDatAssetRequestBO();
		Calendar c = Calendar.getInstance();
 		c.setTime(new Date()); // Now use today date.
 		c.add(Calendar.DATE, 14); // Adding 14 days(2 weeks)
 		Date twoWeeksCheck = c.getTime();
 		//System.out.println("day of month:" + twoWeeksCheck);
		addNewAssetRequestOutput.setRequestorId(purchaseNewAssetRequestCreateInputBean.getEmployeeId());
		addNewAssetRequestOutput.setFkAssetId(purchaseNewAssetRequestCreateInputBean.getFkAssetId());
		addNewAssetRequestOutput.setNoOfUnits(purchaseNewAssetRequestCreateInputBean.getNoOfUnits());
		addNewAssetRequestOutput.setSpecifications(purchaseNewAssetRequestCreateInputBean.getSpecifications());
		addNewAssetRequestOutput.setBriefDescription(purchaseNewAssetRequestCreateInputBean.getBriefDescription());
		if(purchaseNewAssetRequestCreateInputBean.getAssetRequiredDate().after(twoWeeksCheck)  )
		{
			addNewAssetRequestOutput.setAssetRequiredBy(purchaseNewAssetRequestCreateInputBean.getAssetRequiredDate());
		}
		else{
			throw new CommonCustomException("Asset required Date should be greater 2 weeks from the current Date.");
		}
		addNewAssetRequestOutput.setDuration(purchaseNewAssetRequestCreateInputBean.getDuration());
		addNewAssetRequestOutput
				.setIsClientReimbursing(purchaseNewAssetRequestCreateInputBean.getIsClientReimbursing());
		addNewAssetRequestOutput.setClientId(purchaseNewAssetRequestCreateInputBean.getClientId());
		addNewAssetRequestOutput.setRmToApprove(purchaseNewAssetRequestCreateInputBean.getReportingMgrId());
		addNewAssetRequestOutput.setCreatedDate(new Date());
		addNewAssetRequestOutput.setProjectComponentId(purchaseNewAssetRequestCreateInputBean.getProjectComponentId());
		
		DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
		try{
			datEmpDetailBO = empDetailRepo.findByPkEmpId(purchaseNewAssetRequestCreateInputBean.getEmployeeId());
		}catch(Exception ex){
			throw new CommonCustomException("Unable to get employee details from backend. kindly get in touch with Nucleus Support team.");
		}
		
		if(datEmpDetailBO.getFkEmpRoleId().shortValue() == buHeadRoleId || 
				datEmpDetailBO.getFkEmpRoleId().shortValue() == financeHeadRoleId)
		{
			addNewAssetRequestOutput.setPurchaseRequestStatus(Integer.valueOf(requestApprovedByRmStatus));
			addNewAssetRequestOutput.setModifiedOn(addNewAssetRequestOutput.getCreatedDate());
			addNewAssetRequestOutput.setModifiedBy(addNewAssetRequestOutput.getRequestorId());
			
		}else{
			addNewAssetRequestOutput.setPurchaseRequestStatus(Integer.valueOf(requestWaitingRmApprovalStatus));
		}	
		
		addNewAssetRequestOutput.setProjectId(purchaseNewAssetRequestCreateInputBean.getProjectId());
		
		//Added by Shyam
		DatEmpProfessionalDetailBO requestorProfDetail = new DatEmpProfessionalDetailBO();
		try{
			requestorProfDetail = employeeProfessionalRepository.findByFkMainEmpDetailId(
				purchaseNewAssetRequestCreateInputBean.getEmployeeId().intValue());
		}catch(Exception ex){
			throw new CommonCustomException("Unable to fetch employee profession detail. Kindly get in touch with Nucleus Support team.");
		}
		
		if(requestorProfDetail == null){
			throw new CommonCustomException("Requestor's professional detail not found.");
		}
		
		addNewAssetRequestOutput.setLocationId(requestorProfDetail.getFkEmpLocationParentId().shortValue());
		//EOA by Shyam
		
		addNewAssetRequestOutput.setFkBuId(purchaseNewAssetRequestCreateInputBean.getFkBuId());
		addNewAssetRequestOutput.setFkRequestTypeId(purchaseRequestTypeNewAssetRequest);
		if(!uploadSupportDocs.isEmpty()){
			addNewAssetRequestOutput.setSupportingDocumentsByClient("UPLOADED");
		}
		
		try {
			outputObject = purchaseAssetRequestRepo.save(addNewAssetRequestOutput);
		} catch (Exception e) {
			throw new CommonCustomException(
					"Unable to store your asset request. Kindly contact Nucleus Support team for this issue.");
		}
		if (outputObject != null) 
		{
			uploadDocFlag = uploadSupportDocuments(outputObject, uploadSupportDocs);
			
			if(!uploadDocFlag){
				throw new CommonCustomException("Your request has been saved, but failed to store your docs in server. Kindly get in touch with Nucleus Support team.");
			}
			
			flag = true;
			//Added by Kamal Anand for Notifications
			try{
				notificationService.setAssetRequestNotificationByEmpToRMScreen(purchaseNewAssetRequestCreateInputBean.getReportingMgrId());
			} catch(Exception e) {
				LOG.info("Some Exception happened while setting Asset Request Notification to RM Screen");
			}
			//End of Addition by Kamal Anand for Notifications
			
		}
		LOG.endUsecase("Add asset request");
		return flag;
	}
	public boolean updateAssetRequest(PurchaseAssetRequestUpdateInputBean purchaseAssetRequestUpdateInputBean,
	List<MultipartFile> uploadSupportDocs )throws CommonCustomException {
		LOG.startUsecase("Update asset request");
		boolean flag = false;
		boolean uploadDocFlag = false;
		PurchaseDatAssetRequestBO outputObject = new PurchaseDatAssetRequestBO();
		PurchaseDatAssetRequestBO updateAssetRequestOutput = new PurchaseDatAssetRequestBO();
		/*Calendar c = Calendar.getInstance();
 		c.setTime(new Date()); // Now use today date.
 		c.add(Calendar.DATE, 14); // Adding 14 days(2 weeks)
 		Date twoWeeksCheck = c.getTime();
 		System.out.println("day of month:" + twoWeeksCheck);*/
		try {
			updateAssetRequestOutput = purchaseAssetRequestRepo
					.findByPkPurchaseRequestId(purchaseAssetRequestUpdateInputBean.getPurchaseRequestId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (updateAssetRequestOutput == null) {
			throw new CommonCustomException("Purchase request ID does not exist.");
		}
			if(updateAssetRequestOutput.getRequestorId().intValue() != purchaseAssetRequestUpdateInputBean.getModifiedBy())
			{
				throw new CommonCustomException("Only the respective requestor can update the asset request.");
			}
		 else if (updateAssetRequestOutput.getPurchaseRequestStatus() == requestWaitingRmApprovalStatus)
		 {
			updateAssetRequestOutput.setRequestorId(purchaseAssetRequestUpdateInputBean.getEmployeeId());
			updateAssetRequestOutput.setFkAssetId(purchaseAssetRequestUpdateInputBean.getFkAssetId());
			//updateAssetRequestOutput.setFkAssetDetailId(purchaseAssetRequestUpdateInputBean.getFkAssetDetailsId());//Commented by Anil for removal of fk detail id in asset request Table
			updateAssetRequestOutput.setNoOfUnits(purchaseAssetRequestUpdateInputBean.getNoOfUnits());
			updateAssetRequestOutput.setSpecifications(purchaseAssetRequestUpdateInputBean.getSpecifications());
			updateAssetRequestOutput.setBriefDescription(purchaseAssetRequestUpdateInputBean.getBriefDescription());
			
			Calendar c = Calendar.getInstance();
	 		c.setTime(updateAssetRequestOutput.getCreatedDate()); 
	 		c.add(Calendar.DATE, 14); // Adding 14 days(2 weeks)
	 		Date twoWeeksCheck = c.getTime();
	 		System.out.println("day of month:" + twoWeeksCheck);
			
			if(purchaseAssetRequestUpdateInputBean.getAssetRequiredDate().after(twoWeeksCheck)  )
			{
				updateAssetRequestOutput.setAssetRequiredBy(purchaseAssetRequestUpdateInputBean.getAssetRequiredDate());
			}else{
				throw new CommonCustomException("Asset required Date should be greater than 2 weeks from the current Date.");
			}
			updateAssetRequestOutput.setDuration(purchaseAssetRequestUpdateInputBean.getDuration());
			updateAssetRequestOutput
					.setIsClientReimbursing(purchaseAssetRequestUpdateInputBean.getIsClientReimbursing());
			updateAssetRequestOutput.setClientId(purchaseAssetRequestUpdateInputBean.getClientId());
			updateAssetRequestOutput.setRmToApprove(purchaseAssetRequestUpdateInputBean.getReportingMgrId());
			updateAssetRequestOutput.setModifiedOn(new Date());
			updateAssetRequestOutput.setModifiedBy(purchaseAssetRequestUpdateInputBean.getModifiedBy());
			updateAssetRequestOutput.setProjectComponentId(purchaseAssetRequestUpdateInputBean.getProjectComponentId());
			updateAssetRequestOutput.setPkPurchaseRequestId(purchaseAssetRequestUpdateInputBean.getPurchaseRequestId());
			
			DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
 			try{
 				datEmpDetailBO = empDetailRepo.findByPkEmpId(purchaseAssetRequestUpdateInputBean.getEmployeeId());
 			}catch(Exception ex){
 				throw new CommonCustomException("Unable to get employee details from backend. kindly get in touch with Nucleus Support team.");
 			}
 			
 			if(datEmpDetailBO.getFkEmpRoleId().shortValue() == buHeadRoleId || 
 					datEmpDetailBO.getFkEmpRoleId().shortValue() == financeHeadRoleId)
 			{
				updateAssetRequestOutput.setPurchaseRequestStatus(Integer.valueOf(requestApprovedByRmStatus));
 			}else{
 				updateAssetRequestOutput.setPurchaseRequestStatus(Integer.valueOf(requestWaitingRmApprovalStatus));
 			}	
			
			updateAssetRequestOutput.setProjectId(purchaseAssetRequestUpdateInputBean.getProjectId());
			//updateAssetRequestOutput.setLocationId(purchaseAssetRequestUpdateInputBean.getLocationId());
			updateAssetRequestOutput.setFkBuId(purchaseAssetRequestUpdateInputBean.getFkBuId());
			if(!uploadSupportDocs.isEmpty()){
				updateAssetRequestOutput.setSupportingDocumentsByClient("UPLOADED");
			}
		}
		else
		{
			throw new CommonCustomException("Asset request can be updated only when it is in waiting for RM approval.");
		}
		try {
			outputObject = purchaseAssetRequestRepo.save(updateAssetRequestOutput);
		} catch (Exception ex) {
			throw new CommonCustomException("Failed to update asset request details.", ex);
		}if (outputObject != null) 
		{
			uploadDocFlag = uploadSupportDocuments(outputObject, uploadSupportDocs);
			
			if(!uploadDocFlag){
				throw new CommonCustomException("Your request has been saved, but failed to store your docs in server. Kindly get in touch with Nucleus Support team.");
			}
			
			flag = true;
		}
		LOG.endUsecase("Update asset request");
		return flag;
		
	}
	public PurchaseDatAssetRequestBO rejectAssetRequest(RejectAssetRequestInputBean rejectAssetRequestInputBean)
			throws CommonCustomException {
		LOG.startUsecase("Reject asset request");
		PurchaseDatAssetRequestBO rejectAssetRequestOutput = new PurchaseDatAssetRequestBO();
		try {
			rejectAssetRequestOutput = purchaseAssetRequestRepo
					.findByPkPurchaseRequestId(rejectAssetRequestInputBean.getPkPurchaseRequestId());
		} catch (Exception e) {
			throw new CommonCustomException("Failed to fetch details from db.");
		}
		if (rejectAssetRequestOutput == null) {
			throw new CommonCustomException("ID does not exist.");
		} else if (rejectAssetRequestOutput.getPurchaseRequestStatus() == requestRejectByRmStatus) {
			throw new CommonCustomException("Asset is already rejected.");
		} else if (rejectAssetRequestOutput.getPurchaseRequestStatus() == requestWaitingRmApprovalStatus 
				|| rejectAssetRequestOutput.getPurchaseRequestStatus() == requestApprovedByRmStatus){
			rejectAssetRequestOutput.setPkPurchaseRequestId(rejectAssetRequestInputBean.getPkPurchaseRequestId());
			rejectAssetRequestOutput.setRmComment(rejectAssetRequestInputBean.getRmComment());
			rejectAssetRequestOutput.setRmUpdatedOn(new Date());
			rejectAssetRequestOutput.setModifiedOn(new Date());
			rejectAssetRequestOutput.setModifiedBy(rejectAssetRequestInputBean.getModifiedBy());
			rejectAssetRequestOutput.setPurchaseRequestStatus(purchaseRequestRejectByRmStatus);
			purchaseAssetRequestRepo.save(rejectAssetRequestOutput);
		}else{
			throw new CommonCustomException("To reject a asset, it should be only in waiting for approval status.");
		}
		LOG.endUsecase("Reject asset request");
		return rejectAssetRequestOutput;
	}
	// EOA by Smrithi
	// Added by Shyam
	public List<PurchaseViewAssetRequestByDateRangeAndEmpIdUnderEmpLoginOutputBean> viewAssetRequestDetailsUnderEmpLogin(
			PurchaseViewAssetRequestByDateRangeAndEmployeeIdInputBean viewAssetRequestInputBean)
			throws CommonCustomException 
	{
		List<PurchaseViewAssetRequestByDateRangeAndEmpIdUnderEmpLoginOutputBean> outputList = 
				new ArrayList<PurchaseViewAssetRequestByDateRangeAndEmpIdUnderEmpLoginOutputBean>();
		List<PurchaseDatAssetRequestBO> assetRequestList = new ArrayList<PurchaseDatAssetRequestBO>();
		if(viewAssetRequestInputBean.getFromDate() != null && viewAssetRequestInputBean.getToDate() != null)
		{
			if (viewAssetRequestInputBean.getFromDate().equals(viewAssetRequestInputBean.getToDate())) 
			{
				throw new CommonCustomException("FromDate and ToDate are same.");
			}

			if (viewAssetRequestInputBean.getFromDate().after(viewAssetRequestInputBean.getToDate())) {
				throw new CommonCustomException("FromDate should be less than ToDate.");
			}
		}

		//Added by Shyam for date range filter
		SimpleDateFormat sdfWithSeconds = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String stringFromDate = sdf.format(viewAssetRequestInputBean.getFromDate());
		String stringToDate = sdf.format(viewAssetRequestInputBean.getToDate());
		stringFromDate = stringFromDate + " 00:00:00" ;
		stringToDate = stringToDate + " 23:59:59";
		Date fromDate = new Date();
		Date toDate = new Date();
		try {
			fromDate = sdfWithSeconds.parse(stringFromDate);
			toDate = sdfWithSeconds.parse(stringToDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		//EOA 

		try {
			assetRequestList = purchaseAssetRequestRepo.getAssetRequestDetailsByDateRangeAndRequestorId(
					fromDate, toDate,viewAssetRequestInputBean.getRequestorId());
		} catch (Exception ex) {
			throw new CommonCustomException("Failed to fetch your asset request details. " + ex.getMessage());
		}
		
		if (!assetRequestList.isEmpty()) 
		{
			for (PurchaseDatAssetRequestBO purchaseDatAssetRequestBO : assetRequestList) 
			{
				PurchaseViewAssetRequestByDateRangeAndEmpIdUnderEmpLoginOutputBean outputBean = 
						new PurchaseViewAssetRequestByDateRangeAndEmpIdUnderEmpLoginOutputBean();
				outputBean.setAssetDetails(purchaseDatAssetRequestBO.getBriefDescription());
				outputBean.setAssetRequestDetail(purchaseDatAssetRequestBO.getPurchaseMasAssetsBO().
						getAssetNameAndVersionOrModel()); 
				outputBean.setAssetRequestId(purchaseDatAssetRequestBO.getPkPurchaseRequestId());
				outputBean.setNumberOfUnits(purchaseDatAssetRequestBO.getNoOfUnits());
				outputBean.setRequestStatus(purchaseDatAssetRequestBO.getPurchaseMasRequestStatusBO().
						getPurchaseRequestStatusName());
				outputBean.setRequestStatusId(purchaseDatAssetRequestBO.getPurchaseRequestStatus());
				outputBean.setRequiredDate(purchaseDatAssetRequestBO.getAssetRequiredBy());
				outputBean.setCreatedDate(purchaseDatAssetRequestBO.getCreatedDate());
				outputBean.setFkRequestTypeId(purchaseDatAssetRequestBO.getFkRequestTypeId());
				
				
				if(purchaseDatAssetRequestBO.getFkRequestTypeId().intValue() == 
						purchaseRequestTypeNewAssetRequest.intValue())
				{
					LOG.info("Request Type :: " + purchaseRequestTypeNewAssetRequest.intValue());
					// get component & client details from legacy service
					ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean();
					clientProjCompDetailsBean.setClientId(purchaseDatAssetRequestBO.getClientId());
					clientProjCompDetailsBean.setProjectId(purchaseDatAssetRequestBO.getProjectId());
					clientProjCompDetailsBean.setComponentId(purchaseDatAssetRequestBO.getProjectComponentId());
					clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
	
					PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
					ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService
							.getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
					if (clientProjectCompDetails == null) 
					{
						throw new CommonCustomException(
								"Unable to fetch project, client and component details from legacy MIS.");
					} else {
						outputBean.setProjectName(clientProjectCompDetails.getProjectName());
					}
				}else{
					LOG.info("Request TYpe --> " + purchaseDatAssetRequestBO.getFkRequestTypeId().intValue());
					LOG.info("Request Status --> " + purchaseDatAssetRequestBO.getPurchaseRequestStatus().intValue());
					if(purchaseDatAssetRequestBO.getPurchaseRequestStatus().intValue() == 
							requestWaitingRmApprovalStatus || 
							purchaseDatAssetRequestBO.getPurchaseRequestStatus().intValue() 
							== assetRequestRejectedBySysAdmin || 
							purchaseDatAssetRequestBO.getPurchaseRequestStatus().intValue() == 
							requestRejectByRmStatus)
					{
						outputBean.setProjectName(purchaseNoProjectFound);
					}else{	
						// get component & client details from legacy service
						ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean();
						clientProjCompDetailsBean.setClientId(purchaseDatAssetRequestBO.getClientId());
						clientProjCompDetailsBean.setProjectId(purchaseDatAssetRequestBO.getProjectId());
						clientProjCompDetailsBean.setComponentId(purchaseDatAssetRequestBO.getProjectComponentId());
						clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
		
						PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
						ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService
								.getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
						if (clientProjectCompDetails == null) 
						{
							throw new CommonCustomException(
									"Unable to fetch project, client and component details from legacy MIS.");
						} else {
							outputBean.setProjectName(clientProjectCompDetails.getProjectName());
						}
					}
					
				}
				outputList.add(outputBean);
			}
		}
		return outputList;
	
	}
	
	
	public List<PurchaseViewAssetRequestByDateRangeAndMgrIdUnderMgrLoginOutputBean> viewAssetRequestDetailsUnderMgrLogin(
			PurchaseViewAssetRequestByDateRangeAndEmployeeIdInputBean viewAssetRequestInputBean)
			throws CommonCustomException 
	{

		List<PurchaseViewAssetRequestByDateRangeAndMgrIdUnderMgrLoginOutputBean> reporteesAssetRequestList 
			= new ArrayList<PurchaseViewAssetRequestByDateRangeAndMgrIdUnderMgrLoginOutputBean>();
		List<PurchaseDatAssetRequestBO> assetRequestByReportees = new ArrayList<PurchaseDatAssetRequestBO>();
		if(viewAssetRequestInputBean.getFromDate() != null && viewAssetRequestInputBean.getToDate() != null)
		{
			if (viewAssetRequestInputBean.getFromDate().equals(viewAssetRequestInputBean.getToDate())) 
			{
				throw new CommonCustomException("FromDate and ToDate are same.");
			}
	
			if (viewAssetRequestInputBean.getFromDate().after(viewAssetRequestInputBean.getToDate())) 
			{
				throw new CommonCustomException("FromDate should be less than ToDate.");
			}
		}
		
		//Added by Shyam for date range filter
		SimpleDateFormat sdfWithSeconds = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String stringFromDate = sdf.format(viewAssetRequestInputBean.getFromDate());
		String stringToDate = sdf.format(viewAssetRequestInputBean.getToDate());
		stringFromDate = stringFromDate + " 00:00:00" ;
		stringToDate = stringToDate + " 23:59:59";
		Date fromDate = new Date();
		Date toDate = new Date();
		try {
			fromDate = sdfWithSeconds.parse(stringFromDate);
			toDate = sdfWithSeconds.parse(stringToDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		//EOA 
		
		try {
			assetRequestByReportees = purchaseAssetRequestRepo.getAssetRequestDetailsByDateRangeAndReporteesIds(
					fromDate, toDate,viewAssetRequestInputBean.getRequestorId());
		} catch (Exception ex) {
			throw new CommonCustomException(
						"Failed to fetch your reportees asset request details. " + ex.getMessage());
		}
		//LOG.info("assetRequestByReportees :: " + assetRequestByReportees);
		if (!assetRequestByReportees.isEmpty()) 
		{
			for (PurchaseDatAssetRequestBO reporteesAssetRequest : assetRequestByReportees) 
			{
				PurchaseViewAssetRequestByDateRangeAndMgrIdUnderMgrLoginOutputBean outputBean = 
						new PurchaseViewAssetRequestByDateRangeAndMgrIdUnderMgrLoginOutputBean();
				outputBean.setAssetRequestDetail(reporteesAssetRequest.getPurchaseMasAssetsBO().
						getAssetNameAndVersionOrModel());
				outputBean.setAssetRequestId(reporteesAssetRequest.getPkPurchaseRequestId());
				outputBean.setRequestorId(reporteesAssetRequest.getRequestorId());
				outputBean.setNumberOfUnits(reporteesAssetRequest.getNoOfUnits());
				outputBean.setPurchaseRequestTypeId(reporteesAssetRequest.getPurchaseDatRequestTypeBO().getPurchaseRequestTypeId());
				outputBean.setPurchaseRequestTypeName(reporteesAssetRequest.getPurchaseDatRequestTypeBO().
						getPurchaseRequestTypeName());
				if(reporteesAssetRequest.getRequestorId() != null)
				{
					outputBean.setRequestorName(getEmployeeName(reporteesAssetRequest.getRequestorId()));
				}
				
				if(reporteesAssetRequest.getFkRequestTypeId().intValue() == 
						purchaseRequestTypeNewAssetRequest.intValue())
				{
					// get component & client details from legacy service
					ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean();
					clientProjCompDetailsBean.setClientId(reporteesAssetRequest.getClientId());
					clientProjCompDetailsBean.setProjectId(reporteesAssetRequest.getProjectId());
					clientProjCompDetailsBean.setComponentId(reporteesAssetRequest.getProjectComponentId());
					clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
					//LOG.info("clientProjCompDetailsBean :: " + clientProjCompDetailsBean);
					PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
					ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService
							.getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
					if (clientProjectCompDetails == null) {
						throw new CommonCustomException(
								"Unable to fetch project, client and component details from legacy MIS.");
					} else {
						outputBean.setProjectName(clientProjectCompDetails.getProjectName());
					}
				}else{
					if(reporteesAssetRequest.getPurchaseRequestStatus().intValue() == requestWaitingRmApprovalStatus 
							|| reporteesAssetRequest.getPurchaseRequestStatus().intValue() == 
							purchaseRequestRejectByRmStatus || reporteesAssetRequest.getPurchaseRequestStatus().intValue()
							== assetRequestRejectedBySysAdmin.intValue())
					{
						outputBean.setProjectName(purchaseNoProjectFound);
					}else{
						// get component & client details from legacy service
						ClientAndProjectDetailsInputBean clientProjCompDetailsBean = new ClientAndProjectDetailsInputBean();
						clientProjCompDetailsBean.setClientId(reporteesAssetRequest.getClientId());
						clientProjCompDetailsBean.setProjectId(reporteesAssetRequest.getProjectId());
						clientProjCompDetailsBean.setComponentId(reporteesAssetRequest.getProjectComponentId());
						clientProjCompDetailsBean.setLegacyUrl(purchaseLegacyUrl);
		
						PurchaseLegacyService purchaseLegacyService = new PurchaseLegacyService();
						ClientAndProjectDetailsFromCompIdBean clientProjectCompDetails = purchaseLegacyService
								.getClientAndProjectAndComponentDetails(clientProjCompDetailsBean);
						if (clientProjectCompDetails == null) 
						{
							throw new CommonCustomException(
									"Unable to fetch project, client and component details from legacy MIS.");
						} else {
							outputBean.setProjectName(clientProjectCompDetails.getProjectName());
						}
					}
				}
				
				outputBean.setRequestStatus(
						reporteesAssetRequest.getPurchaseMasRequestStatusBO().getPurchaseRequestStatusName());
				outputBean.setRequestStatusId(reporteesAssetRequest.getPurchaseRequestStatus());
				outputBean.setRequiredDate(reporteesAssetRequest.getCreatedDate());
				
				reporteesAssetRequestList.add(outputBean);
			}
		}
		
		return reporteesAssetRequestList;
	
		
	}
	
	
	
	public String getEmployeeName(Integer empId) {
		LOG.startUsecase("Entered getEmployeeName");
		DatEmpPersonalDetailBO empData = employeePersonalDetailRepo
				.findByFkEmpDetailId(empId);
		LOG.endUsecase("Exited getEmployeeName");
		return empData.getEmpFirstName() + " " + empData.getEmpLastName();
	}
	// EOA by Shyam
	
	
	//For Support document upload in new asset request
	public boolean uploadSupportDocuments(PurchaseDatAssetRequestBO assetRequestBO, 
			List<MultipartFile> uploadDocs) 
	{
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(new Date());
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH)+1;
		try {
			if(uploadDocs.size() > 0)
			{
				for(MultipartFile file : uploadDocs)
				{
					//String basePath = purchaseDocsBasePath;
					String newFileName = "EMP_"+ file.getOriginalFilename();
					File dir = new File(purchaseDocsBasePath + year + File.separator 
							+ month + File.separator + assetRequestBO.getPkPurchaseRequestId() 
							+ File.separator + "Supporting docs" + File.separator + "EmpOrRm");
					
					if(dir.mkdirs()){
						File fileToBeStored = new File(dir.getPath() + File.separator + newFileName);
						FileCopyUtils.copy(file.getBytes(), fileToBeStored);
					}else{
						File fileToBeStored = new File(dir.getPath() + File.separator + newFileName);
						FileCopyUtils.copy(file.getBytes(), fileToBeStored);
					}
				}
			}
			
		} catch (IOException e) {
			e.getMessage();
		}
		return true;
	}
	
	
	public Boolean rejectAssetRequestBySysAdmin(RejectAssetReqBySysAdminInputBean assetBean)
			throws CommonCustomException
	{   LOG.startUsecase("System Admin reject Asset Request -Service Method");
		Boolean success = false;
		PurchaseDatAssetRequestBO assetRequestBo = new PurchaseDatAssetRequestBO();
		List<Short> systemAdminRoleIdList = new ArrayList<Short>();
		String[] empIdArray = systemAdminRoles.split(",");
		for (String s : empIdArray)
			systemAdminRoleIdList.add(Short.parseShort(s));
		DatEmpDetailBO empDetail = new DatEmpDetailBO();
		try{
			
			empDetail = empDetailRepository.getSystemAdminRoleDetailsForAddAsset(
					assetBean.getModifiedBy(),systemAdminRoleIdList);
			assetRequestBo = purchaseAssetRequestRepo.findByPkPurchaseRequestId(assetBean.getPkPurchaseRequestId());
			
		} 
		catch(Exception e)
		{   LOG.endUsecase("Exception Occured in System Admin reject Asset Request -Service Method ");
			throw new CommonCustomException("Unable to retrieve your asset request information. "
					+ "Kindly contact Nucleus Support team for this issue.");
		}
		
		
		if(assetRequestBo == null)
		{    
			throw new CommonCustomException("Asset request does not exist.");	
		}
		else if (assetRequestBo.getPurchaseRequestStatus().intValue() == assetRequestRejectedBySysAdmin.intValue())
		{    
			throw new CommonCustomException("This asset request is already rejected by System Admin");
		}else if (assetRequestBo.getPurchaseRequestStatus().intValue()== assetRequestClosed.intValue())
		{
			throw new CommonCustomException("This request is already closed-cannot reject  closed request");
		}
		else {
			
			//Check modified by employee is sys-admin or not
			
			if(empDetail == null){
				throw new CommonCustomException("Login employee does't have the system admin role");
			}else{
				
		
					assetRequestBo.setPurchaseRequestStatus(assetRequestRejectedBySysAdmin);
					assetRequestBo.setModifiedOn(new Date());
					assetRequestBo.setModifiedBy(assetBean.getModifiedBy());
					assetRequestBo.setSysAdminId(assetBean.getModifiedBy());
					assetRequestBo.setSysAdminComment(assetBean.getSysAdminComment());
					assetRequestBo.setSysAdminUpdatedOn(new Date());
					try{
						purchaseAssetRequestRepo.save(assetRequestBo);
						success = true;
					}catch(Exception ex){
						success = false;
						throw new CommonCustomException("Unable to update asset request data. "
								+ "Kindly contact to Nucleus Support team for this issue.");
					}
					
			
			}

		}
		return success;
	}
}