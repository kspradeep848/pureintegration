package com.thbs.mis.purchase.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.httpclient.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.PurchaseAddNewAssetRequestInputBean;
import com.thbs.mis.purchase.bean.PurchaseAssetDetailForAssetRequestOutputBean;
import com.thbs.mis.purchase.bean.PurchaseAssetDetailsOutputBean;
import com.thbs.mis.purchase.bean.PurchaseAssetRequestBean;
import com.thbs.mis.purchase.bean.PurchaseAssetRequestDetailOutputBean;
import com.thbs.mis.purchase.bean.PurchaseAssetRequestUpdateInputBean;
import com.thbs.mis.purchase.bean.PurchaseGetAssetRequestBean;
import com.thbs.mis.purchase.bean.PurchaseNewAssetRequestCreateInputBean;
import com.thbs.mis.purchase.bean.PurchaseUpdateNewAssetRequestInputBean;
import com.thbs.mis.purchase.bean.PurchaseViewAssetRequestByDateRangeAndEmpIdUnderEmpLoginOutputBean;
import com.thbs.mis.purchase.bean.PurchaseViewAssetRequestByDateRangeAndEmployeeIdInputBean;
import com.thbs.mis.purchase.bean.PurchaseViewAssetRequestByDateRangeAndMgrIdUnderMgrLoginOutputBean;
import com.thbs.mis.purchase.bean.RejectAssetReqBySysAdminInputBean;
import com.thbs.mis.purchase.bean.RejectAssetRequestInputBean;
import com.thbs.mis.purchase.bean.UpdateAndApproveAssetRequestForSystemAdminBean;
import com.thbs.mis.purchase.bean.ViewAllAssetRequestBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;
import com.thbs.mis.purchase.constants.PurchaseManagementURIConstants;
import com.thbs.mis.purchase.service.AssetRequestService;

@Controller
public class AssetRequestController {

	private static final AppLog LOG = LogFactory.getLog(AssetRequestController.class);

	@Autowired
	private AssetRequestService assetRequestService;

	@RequestMapping(value = PurchaseManagementURIConstants.GET_ALL_ASSET_REQUESTS, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> getAllAssetRequests(
			@RequestBody ViewAllAssetRequestBean assetReq)
			throws DataAccessException {
		LOG.entering("getAllAssetRequests");
		List<PurchaseAssetRequestBean> listOfAssetRequest = new ArrayList<PurchaseAssetRequestBean>();
		try {
			listOfAssetRequest = assetRequestService
					.getAllAssetRequests(assetReq);
		} catch (Exception e) {
			throw new DataAccessException(
					"Error while getting getAllAssetRequests", e);
		}
		LOG.exiting("getAllAssetRequests");
		if (listOfAssetRequest.size() > 0) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Asset requests retrieved successfully",
							listOfAssetRequest));
		} else {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No data for given input"));
		}

	}

	/*
	 * @RequestMapping(value=PurchaseManagementURIConstants.DOWNLOAD_PO,
	 * method=RequestMethod.GET ) public ResponseEntity<MISResponse>
	 * downloadPo(@PathVariable String ponum,HttpServletResponse response)
	 * throws DataAccessException{ LOG.entering("downloadPo"+ponum); Boolean
	 * isDownloaded = false; try{ isDownloaded =
	 * assetRequestService.downloadPo(ponum,response); }catch(Exception e){
	 * 
	 * throw new DataAccessException( "Error while getting downloadPo", e); }
	 * LOG.exiting("downloadPo"); if (isDownloaded) {
	 * 
	 * return ResponseEntity.status(HttpStatus.OK.value()).body( new
	 * MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
	 * "PO file downloaded" )); } else {
	 * 
	 * return ResponseEntity .status(HttpStatus.OK.value()) .body(new
	 * MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
	 * "No file for given input")); }
	 * 
	 * 
	 * }
	 */

	@RequestMapping(value = PurchaseManagementURIConstants.GET_ASSET_REQUESTS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAssetRequests(
			@PathVariable Integer assetReqId) throws DataAccessException {
		LOG.entering("getAssetRequests");
		PurchaseGetAssetRequestBean assetRequest = new PurchaseGetAssetRequestBean();
		List<PurchaseGetAssetRequestBean> listAssetRequest = new ArrayList<PurchaseGetAssetRequestBean>();
		try {
			assetRequest = assetRequestService.getAssetRequests(assetReqId);
			listAssetRequest.add(assetRequest);
		} catch (Exception e) {
			throw new DataAccessException(
					"Error while getting getAssetRequests", e);
		}
		LOG.exiting("getAllAssetRequests");
		if (listAssetRequest.size() > 0) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Asset requests retrieved successfully",
							listAssetRequest));
		} else {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No date for given input"));
		}

	}
	
	// Added by Pratibha TR
	/**
	 * 
	 * @param assetType
	 * @return List<PurchaseAssetDetailForAssetRequestOutputBean>
	 * @throws CommonCustomException
     * @Description: This Service has been implemented to view the asset and it
	 *               detail.
	 */
	
	
	@ApiOperation(tags = "Purchase", value = "Get all asset details for system admin"
			+ "This service will be called from the front-end to view the all asset details", httpMethod = "GET", notes = "This Service has been implemented to view the asset and it detail."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)purchase_mas_assets -> It contains asset type and its detail."
			+ "<br>",
     nickname = "Asset and its detail", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseAssetDetailForAssetRequestOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseAssetDetailForAssetRequestOutputBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of asset and its detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.GET_ALL_ASSET_DETAILS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getallAssetDetails()
			 throws CommonCustomException {
		LOG.startUsecase("Get Asset And Its Detail From Asset Type for system admin");
		List<PurchaseAssetDetailsOutputBean> assetsOutputBeanList = new ArrayList<PurchaseAssetDetailsOutputBean>();

		assetsOutputBeanList = assetRequestService
				.getallAssetDetails();

		if (!assetsOutputBeanList.isEmpty()) {
			LOG.endUsecase("Get Asset And Its Detail From Asset Type");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully retrieved asset details.",
							assetsOutputBeanList));
		} else {
			LOG.endUsecase("Get Asset And Its Detail From Asset Type for system admin");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No asset details found."));
		}

	}
	
	@ApiOperation(tags = "Purchase", value = "Get Asset And Its Detail From Asset Type for system admin"
			+ "This service will be called from the front-end to view the asset and it detail", httpMethod = "GET", notes = "This Service has been implemented to view the asset and it detail."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)purchase_mas_assets -> It contains asset type and its detail."
			+ "2)purchase_mas_asset_detail -> It contains details about the requested asset."
			+ "<br>"
			+ "<br><b>Path variable :</b><br>"
			+ "<br> assetType : assetType  in the format of string <br>",
     nickname = "Asset and its detail", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseAssetDetailForAssetRequestOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseAssetDetailForAssetRequestOutputBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of asset and its detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.GET_ASSET_AND_ITS_DETAIL_FROM_ASSET_TYPE_FOR_SYSTEM_ADMIN, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAssetAndItsDetailFromAssetTypeForSystemAdmin(
			@ApiParam(name = "Get Asset And Its Detail From Asset Type for system admin.", example = "1", value = "{"
					+ "<br> assetType : hardware/software, <br>" + "}")
			@PathVariable String assetType) throws CommonCustomException {
		LOG.startUsecase("Get Asset And Its Detail From Asset Type for system admin");
		List<PurchaseAssetDetailForAssetRequestOutputBean> assetsRequestOutputBeanList = new ArrayList<PurchaseAssetDetailForAssetRequestOutputBean>();

		assetsRequestOutputBeanList = assetRequestService
				.getAssetAndItsDetailFromAssetTypeForSystemAdmin(assetType);

		if (!assetsRequestOutputBeanList.isEmpty()) {
			LOG.endUsecase("Get Asset And Its Detail From Asset Type");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully retrieved asset details.",
							assetsRequestOutputBeanList));
		} else {
			LOG.endUsecase("Get Asset And Its Detail From Asset Type for system admin");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No asset details found."));
		}

	}
	/**
	 * 
	 * @param assetType
	 * @return List<PurchaseAssetDetailsOutputBean>
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to view the asset and it
	 *               detail.
	 */
	
	@ApiOperation(tags = "Purchase", value = "Get Asset And Its Detail From Asset Type for employee"
			+ "This service will be called from the front-end to view the asset and it detail", httpMethod = "GET", notes = "This Service has been implemented to view the asset and it detail."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)purchase_mas_assets -> It contains asset type and its detail."
			+ "<br>"
			+ "<br><b>Path variable :</b><br>"
			+ "<br> assetType : assetType  in the format of string <br>",
     nickname = "Asset and its detail", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseAssetDetailsOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseAssetDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of asset and its detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.GET_ASSET_AND_ITS_DETAIL_FROM_ASSET_TYPE_FOR_EMPLOYEE, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAssetAndItsDetailFromAssetTypeForEmployee(
			@ApiParam(name = "Get Asset And Its Detail From Asset Type for employee.", example = "1", value = "{"
					+ "<br> assetType : hardware/software, <br>" + "}")
			@PathVariable String assetType) throws CommonCustomException {
		LOG.startUsecase("Get Asset And Its Detail From Asset Type for Employee");
		List<PurchaseAssetDetailsOutputBean> assetsRequestOutputBeanList = new ArrayList<PurchaseAssetDetailsOutputBean>();

		assetsRequestOutputBeanList = assetRequestService
				.getAssetAndItsDetailFromAssetTypeForEmployee(assetType);

		if (!assetsRequestOutputBeanList.isEmpty()) {
			LOG.endUsecase("Get Asset And Its Detail From Asset Type");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully retrieved asset details.",
							assetsRequestOutputBeanList));
		} else {
			LOG.endUsecase("Get Asset And Its Detail From Asset Type for employee");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No asset details found."));
		}

	}
	/**
	 * 
	 * @param assetRequestId
	 * @return PurchaseAssetRequestDetailOutputBean
	 * @throws CommonCustomException
	 * @throws IOException
	 * @Description: This Service has been implemented to view the asset and it
	 *               detail.
	 */
	@ApiOperation(tags = "Purchase", value = "Get asset request details by asset request ID"
			+ "This service will be called from the front-end to view the asset and it detail", httpMethod = "GET", notes = "This Service has been implemented to view the asset and it detail."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)purchase_mas_assets -> It contains asset type and its detail."
			+ "2)purchase_dat_asset_request -> It contains details about the requested asset."
			+ "<br>"
			+ "<br><b>Path variable :</b><br>"
			+ "<br> pkPurchaseRequestId : pkPurchaseRequestId  in the format of integer <br>",
			nickname = "Asset and its detail", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseAssetRequestDetailOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseAssetRequestDetailOutputBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Asset and its detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.GET_ASSET_REQUEST_DETAILS_FROM_ASSET_REQUEST_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAssetRequestDetailsByAssetRequestId(
			@ApiParam(name = "Get asset request details by asset request ID.", example = "1", value = "{"
					+ "<br> pkPurchaseRequestId : 1, <br>" + "}")
			@PathVariable Integer assetRequestId) throws CommonCustomException,
			IOException {
		LOG.startUsecase("Get asset request details by asset request ID");

		List<PurchaseAssetRequestDetailOutputBean> outputBeanList = new ArrayList<PurchaseAssetRequestDetailOutputBean>();
		//PurchaseAssetRequestDetailOutputBean requestDetailOutputBean = new PurchaseAssetRequestDetailOutputBean();

		outputBeanList = assetRequestService
				.getAssetRequestDetailsByAssetRequestId(assetRequestId);

	//	if (requestDetailOutputBean != null)
			//outputBeanList.add(requestDetailOutputBean);

		if (!outputBeanList.isEmpty()) {
			LOG.endUsecase("Get asset request details by asset request ID");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully retrieved asset details.",
							outputBeanList));
		} else {
			LOG.endUsecase("Get asset request details by asset request ID");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No asset details found."));
		}

	}

	/**
	 * 
	 * @param assetRequestId
	 * @return PurchaseDatAssetRequestBO
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to cancel asset request.
	 */
	@ApiOperation(tags = "Purchase", value = "Cancel asset request"
			+ "This service will be called from the front-end inorder to cancel asset request", httpMethod = "DELETE", notes = "This Service has been implemented to cancel asset request."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)purchase_dat_asset_request -> It contains details about the requested asset."
			+ "<br>"
			+ "<br><b>Path variable :</b><br>"
			+ "<br> pkPurchaseRequestId : pkPurchaseRequestId  in the format of integer <br>",
			nickname = "Cancel asset request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseDatAssetRequestBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseDatAssetRequestBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Cancel asset request detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.CANCEL_ASSET_REQUEST, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<MISResponse> cancelAssetRequest(
			@ApiParam(name = "Get asset request details by asset request ID.", example = "1", value = "{"
					+ "<br> pkPurchaseRequestId : 1, <br>" + "}")
			@PathVariable Integer assetRequestId) throws 
			CommonCustomException {
		LOG.startUsecase("cancel Asset Request");
		
         assetRequestService.cancelAssetRequest(assetRequestId);

		LOG.endUsecase("cancel Asset Request");
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"Asset request is cancelled successfully."));

	}

	/**
	 * 
	 * @param requestBean
	 * @return PurchaseUpdateAndApproveAssetRequestBean
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to update and approve
	 *               asset request.
	 */
	@ApiOperation(tags = "Purchase", value = "Update and approve asset request"
			+ "This service will be called from the front-end to update and approve asset request", httpMethod = "POST", notes = "This Service has been implemented to update and approve asset request."
				+ "<br>The Details will be fetched from database."
					+ "<br> <b> Table : </b> </br>"
					+ "1) purchase_dat_asset_request -> It contains all asset request details"
					+ "<br>"
					+ "<br><b>Inputs :</b><br> PurchaseAssetRequestUpdateInputBean <br><br>"
					+ "<br> employeeId : employee id in the format of integer <br>"
					+ "<br> fkAssetId : fkAssetId in the format of integer <br>"
					+ "<br> fkAssetDetailsId : fkAssetDetailsId in the format of integer <br>"
					+ "<br> noOfUnits : noOfUnits in the format of integer<br>"
					+ "<br> specifications : specifications in the format of String<br>"
					+ "<br> briefDescription : briefDescription in the format of String<br>"
					+ "<br> assetRequiredDate : assetRequiredDate in the format yyyy-MM-dd. <br>"
					+ "<br> duration : duration in the format of integer<br>"
					+ "<br> isClientReimbursing : isClientReimbursing in the format of String <br>"
					+ "<br> clientId : clientId in the format of integer<br>"
					+ "<br> reportingMgrId : reportingMgrId in the format of Integer<br>"
					+ "<br>createdDate: createdDate in the format yyyy-MM-dd.<br>"
					+ "<br>projectComponentId : projectComponentId in the format of integer<br>"
					+ "<br>purchaseRequestId : purchaseRequestId in the format of integer<br>"
					+ "<br> purchaseRequestStatus: purchaseRequestStatus in the format of integer<br>",

			nickname = "Purchase Management System Update Asset Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
			@ApiResponses(value = {
					@ApiResponse(code = 200, message = "Request Executed Successfully"),
					@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of updated asset requests", response = URI.class)),
					@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
					@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.UPDATE_AND_APPROVE_ASSET_REQUEST, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> updateAndApproveAssetRequest(
			@RequestParam(value = "purchaseAssetRequestUpdateInputBean", name = "purchaseAssetRequestUpdateInputBean") 
			String PurchaseAssetRequestUpdateInputBeanString,
			@RequestParam(value = "uploadSupportDocs", name = "uploadSupportDocs", required = false) 
			List<MultipartFile> uploadSupportDocs)
			throws CommonCustomException {
		LOG.startUsecase("Update and approve asset Request");
		boolean updateAssetRequest = false;
		ObjectMapper objectMapper = new ObjectMapper();
		PurchaseAssetRequestUpdateInputBean inputBean = new PurchaseAssetRequestUpdateInputBean();
		
		try {
			inputBean = objectMapper.readValue(PurchaseAssetRequestUpdateInputBeanString,
					PurchaseAssetRequestUpdateInputBean.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		updateAssetRequest = assetRequestService
				.updateAndApproveAssetRequest(inputBean, uploadSupportDocs);
	
		if (!updateAssetRequest) {
			LOG.endUsecase("update and approve asset Request");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Updated and approved asset request detail is not found."));
		} else {

			LOG.endUsecase("update and approve asset Request");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Asset Request is updated and approved successfully."));

			}
	}
	
	@ApiOperation(tags = "Purchase Management System", value = "Add Asset Request Details as a System admin"
			+ "This service will be called from the front-end when the system admin wants to add asset request "
			+ "raised by himself or herself based on PurchaseAddNewAssetRequestInputBean", httpMethod = "POST", notes = "This Service has been implemented to add asset request record based on PurchaseAddNewAssetRequestInputBean"
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) purchase_dat_asset_request -> It contains all asset request details"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseAddNewAssetRequestInputBean <br><br>"
			+ "<br> requestorId : requestorId id in the format of integer <br>"
			+ "<br> fkAssetId : fkAssetId in the format of integer <br>"
			+ "<br> purchaseRequestTypeId : purchaseRequestTypeId in the format of integer <br>"
			+ "<br> fkAssetDetailsId : fkAssetDetailsId in the format of integer <br>"
			+ "<br> noOfUnits : noOfUnits in the format of integer<br>"
			+ "<br> specifications : specifications in the format of String<br>"
			+ "<br> briefDescription : briefDescription in the format of String<br>"
			+ "<br> assetRequiredDate : assetRequiredDate in the format yyyy-MM-dd. <br>"
			+ "<br> duration : duration in the format of integer<br>"
			+ "<br> isClientReimbursing : isClientReimbursing in the format of String <br>"
			+ "<br> clientId : clientId in the format of integer<br>"
			+ "<br> rmToApprove : rmToApprove in the format of Integer<br>"
			+ "<br> projectComponentId : projectComponentId in the format of integer<br>"
			+ "<br> fkBuId: fkBuId in the format of short<br>"
			+ "<br> locationId: locationId in the format of short<br>"
			+ "<br> projectId: purchaseRequestStatus in the format of integer<br>",

	nickname = "Purchase Management System Add Asset Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of asset request", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.ADD_NEW_ASSET_REQUEST_AS_SYSTEM_ADMIN, method = RequestMethod.POST,consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> addNewAssetRequestAsSystemAdmin(
			@RequestParam(value = "purchaseAddNewAssetRequestInputBean", name = "purchaseAddNewAssetRequestInputBean") 
			String purchaseAddNewAssetRequestInputBeanString,
			@RequestParam(value = "uploadSupportDocs", name = "uploadSupportDocs", required = false) 
			List<MultipartFile> uploadSupportDocs)
		throws CommonCustomException 
	{
		LOG.startUsecase("Add Asset Request Details as a System admin");
		boolean addAssetRequest = false;
		ObjectMapper objectMapper = new ObjectMapper();
		PurchaseAddNewAssetRequestInputBean inputBean = new PurchaseAddNewAssetRequestInputBean();
		
		try {
			inputBean = objectMapper.readValue(purchaseAddNewAssetRequestInputBeanString,
					PurchaseAddNewAssetRequestInputBean.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		addAssetRequest = assetRequestService
				.addNewAssetRequestAsSystemAdmin(inputBean, uploadSupportDocs);

		if (!addAssetRequest) {
			LOG.endUsecase("Add Asset Request Details as a System admin");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Error occured while creating new asset request."));
		} else {
			LOG.endUsecase("Add Asset Request Details as a System admin");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Asset Request is added successfully."));
		}
	}
	
	@ApiOperation(tags = "Purchase Management System", value = "Update Asset Request Details as a System admin"
			+ "This service will be called from the front-end when the system admin wants to update asset request "
			+ "raised by himself or herself based on PurchaseUpdateNewAssetRequestInputBean", httpMethod = "POST", notes = "This Service has been implemented to add asset request record based on PurchaseUpdateNewAssetRequestInputBean "
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) purchase_dat_asset_request -> It contains all asset request details"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseUpdateNewAssetRequestInputBean <br><br>"
			+ "<br> pkPurchaseRequestId : pkPurchaseRequestId id in the format of integer <br>"
			+ "<br> requestorId : requestorId id in the format of integer <br>"
			+ "<br> fkAssetId : fkAssetId in the format of integer <br>"
			+ "<br> purchaseRequestTypeId : purchaseRequestTypeId in the format of integer <br>"
			+ "<br> fkAssetDetailsId : fkAssetDetailsId in the format of integer <br>"
			+ "<br> noOfUnits : noOfUnits in the format of integer<br>"
			+ "<br> specifications : specifications in the format of String<br>"
			+ "<br> briefDescription : briefDescription in the format of String<br>"
			+ "<br> assetRequiredDate : assetRequiredDate in the format yyyy-MM-dd. <br>"
			+ "<br> duration : duration in the format of integer<br>"
			+ "<br> isClientReimbursing : isClientReimbursing in the format of String <br>"
			+ "<br> clientId : clientId in the format of integer<br>"
			+ "<br> rmToApprove : rmToApprove in the format of Integer<br>"
			+ "<br> modifiedBy : modifiedBy in the format of Integer<br>"
			+ "<br> projectComponentId : projectComponentId in the format of integer<br>"
			+ "<br> fkBuId: fkBuId in the format of short<br>"
			+ "<br> locationId: locationId in the format of short<br>"
			+ "<br> projectId: purchaseRequestStatus in the format of integer<br>",
		

	nickname = "Purchase Management System Add Asset Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of asset request", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.UPDATE_NEW_ASSET_REQUEST_AS_SYSTEM_ADMIN, method = RequestMethod.POST,consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> updateNewAssetRequestAsSystemAdmin(
			@RequestParam(value = "purchaseUpdateNewAssetRequestInputBean", name = "purchaseUpdateNewAssetRequestInputBean") 
			String purchaseUpdateNewAssetRequestInputBeanString,
			@RequestParam(value = "uploadSupportDocs", name = "uploadSupportDocs", required = false) 
			List<MultipartFile> uploadSupportDocs)
		throws CommonCustomException 
	{
		LOG.startUsecase("update Asset Request Details as a System admin");
		boolean addAssetRequest = false;
		ObjectMapper objectMapper = new ObjectMapper();
		PurchaseUpdateNewAssetRequestInputBean inputBean = new PurchaseUpdateNewAssetRequestInputBean();
		
		try {
			inputBean = objectMapper.readValue(purchaseUpdateNewAssetRequestInputBeanString,
					PurchaseUpdateNewAssetRequestInputBean.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		addAssetRequest = assetRequestService
				.updateNewAssetRequestAsSystemAdmin(inputBean, uploadSupportDocs);

		if (!addAssetRequest) {
			LOG.endUsecase("update Asset Request Details as a System admin");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Error occurred while updating asset request."));
		} else {
			LOG.endUsecase("update Asset Request Details as a System admin");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Asset Request is updated successfully."));
		}
	}
	
	@ApiOperation(tags = "Purchase", value = "Update and approve asset request for system admin"
			+ "This service will be called from the front-end to update and approve asset request", httpMethod = "POST", notes = "This Service has been implemented to update and approve asset request."
				+ "<br>The Details will be fetched from database."
					+ "<br> <b> Table : </b> </br>"
					+ "1) purchase_dat_asset_request -> It contains all asset request details"
					+ "<br>"
					+ "<br><b>Inputs :</b><br> UpdateAndApproveAssetRequestForSystemAdminBean <br><br>"
					+ "<br> assetRequiredDate : assetRequiredDate in the format yyyy-MM-dd. <br>"
					+ "<br> isClientReimbursing : isClientReimbursing in the format of String <br>"
					+ "<br> clientId : clientId in the format of integer<br>"
					+ "<br> modifiedBy : modifiedBy in the format of integer<br>"
					+ "<br>projectComponentId : projectComponentId in the format of integer<br>"
					+ "<br>purchaseRequestId : purchaseRequestId in the format of integer<br>"
					+ "<br> purchaseRequestStatus: purchaseRequestStatus in the format of integer<br>",

			nickname = "Purchase Management System Update Asset Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
			@ApiResponses(value = {
					@ApiResponse(code = 200, message = "Request Executed Successfully"),
					@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of updated asset requests", response = URI.class)),
					@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
					@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.UPDATE_AND_APPROVE_ASSET_REQUEST_FOR_SYSTEM_ADMIN, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> updateAndApproveAssetRequestForSystemAdmin(
			@RequestParam(value = "updateAndApproveAssetRequestForSystemAdminBean", name = "updateAndApproveAssetRequestForSystemAdminBean") 
			String updateAndApproveAssetRequestForSystemAdminBeanString,
			@RequestParam(value = "uploadSupportDocs", name = "uploadSupportDocs", required = false) 
			List<MultipartFile> uploadSupportDocs)
			throws CommonCustomException {
		LOG.startUsecase("Update and approve asset request for system admin");
		boolean updateAssetRequest = false;
		ObjectMapper objectMapper = new ObjectMapper();
		UpdateAndApproveAssetRequestForSystemAdminBean inputBean = new UpdateAndApproveAssetRequestForSystemAdminBean();
		
		try {
			inputBean = objectMapper.readValue(updateAndApproveAssetRequestForSystemAdminBeanString,
					UpdateAndApproveAssetRequestForSystemAdminBean.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		updateAssetRequest = assetRequestService
				.updateAndApproveAssetRequestForSystemAdmin(inputBean, uploadSupportDocs);
	
		if (!updateAssetRequest) {
			LOG.endUsecase("Update and approve asset request for system admin");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Updated and approved asset request detail is not found."));
		} else {
			LOG.endUsecase("Update and approve asset request for system admin");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Asset Request is updated and approved successfully."));

			}
	}
	

	// EOA pratibha TR

	// added by Smrithi

	/**
	 * @param PurchaseNewAssetRequestCreateInputBean
	 * @return
	 * @throws CommonCustomException
	 * @Description: This method is used to add asset request records by
	 *               employee himself/herself.
	 */
	@ApiOperation(tags = "Purchase Management System", value = " Add Asset Request Details Under Employee Login"
			+ "This service will be called from the front-end when the user wants to add asset request "
			+ "raised by himself or herself based on PurchaseNewAssetRequestCreateInputBean", httpMethod = "POST", notes = "This Service has been implemented to add asset request record based on PurchaseNewAssetRequestCreateInputBean "
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) purchase_dat_asset_request -> It contains all asset request details"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseNewAssetRequestCreateInputBean <br><br>"
			+ "<br> employeeId : employee id in the format of integer <br>"
			+ "<br> fkAssetId : fkAssetId in the format of integer <br>"
			+ "<br> fkAssetDetailsId : fkAssetDetailsId in the format of integer <br>"
			+ "<br> noOfUnits : noOfUnits in the format of integer<br>"
			+ "<br> specifications : specifications in the format of String<br>"
			+ "<br> briefDescription : briefDescription in the format of String<br>"
			+ "<br> assetRequiredDate : assetRequiredDate in the format yyyy-MM-dd. <br>"
			+ "<br> duration : duration in the format of integer<br>"
			+ "<br> isClientReimbursing : isClientReimbursing in the format of String <br>"
			+ "<br> clientId : clientId in the format of integer<br>"
			+ "<br> reportingMgrId : reportingMgrId in the format of Integer<br>"
			+ "<br>createdDate: createdDate in the format yyyy-MM-dd.<br>"
			+ "<br>projectComponentId : projectComponentId in the format of integer<br>"
			+ "<br>purchaseRequestId : purchaseRequestId in the format of integer<br>"
			+ "<br> purchaseRequestStatus: purchaseRequestStatus in the format of integer<br>",

	nickname = "Purchase Management System Add Asset Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of asset request", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.POST_CREATE_NEW_ASSET_REQUEST, method = RequestMethod.POST,consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> addNewAssetRequest(
			@RequestParam(value = "purchaseNewAssetRequestCreateInputBean", name = "purchaseNewAssetRequestCreateInputBean") 
			String purchaseNewAssetRequestCreateInputBeanString,
			@RequestParam(value = "uploadSupportDocs", name = "uploadSupportDocs", required = false) 
			List<MultipartFile> uploadSupportDocs)
		throws CommonCustomException 
	{

		boolean addAssetRequest = false;
		ObjectMapper objectMapper = new ObjectMapper();
		PurchaseNewAssetRequestCreateInputBean inputBean = new PurchaseNewAssetRequestCreateInputBean();
		
		try {
			inputBean = objectMapper.readValue(purchaseNewAssetRequestCreateInputBeanString,
					PurchaseNewAssetRequestCreateInputBean.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	//	LOG.info("inputBean :: " + inputBean);
		addAssetRequest = assetRequestService
				.addNewAssetRequest(inputBean, uploadSupportDocs);

		if (!addAssetRequest) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Asset Request cannot be added."));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							" Asset Request is added successfully."));
		}
	}

	/**
	 * @param PurchaseAssetRequestUpdateInputBean
	 * @return
	 * @throws CommonCustomException
	 * @Description: This method is used to update asset request records by the
	 *               employees.
	 */
	@ApiOperation(tags = "Purchase Management System", value = " Update Asset Request Details Under Employee Login"
			+ "This service will be called from the front-end when the user wants to update asset request "
			+ "raised by himself or herself based on PurchaseAssetRequestUpdateInputBean", httpMethod = "POST", notes = "This Service has been implemented to update asset request record based on PurchaseAssetRequestUpdateInputBean "
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) purchase_dat_asset_request -> It contains all asset request details"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseAssetRequestUpdateInputBean <br><br>"
			+ "<br> employeeId : employee id in the format of integer <br>"
			+ "<br> fkAssetId : fkAssetId in the format of integer <br>"
			+ "<br> fkAssetDetailsId : fkAssetDetailsId in the format of integer <br>"
			+ "<br> noOfUnits : noOfUnits in the format of integer<br>"
			+ "<br> specifications : specifications in the format of String<br>"
			+ "<br> briefDescription : briefDescription in the format of String<br>"
			+ "<br> assetRequiredDate : assetRequiredDate in the format yyyy-MM-dd. <br>"
			+ "<br> duration : duration in the format of integer<br>"
			+ "<br> isClientReimbursing : isClientReimbursing in the format of String <br>"
			+ "<br> clientId : clientId in the format of integer<br>"
			+ "<br> reportingMgrId : reportingMgrId in the format of Integer<br>"
			+ "<br>createdDate: createdDate in the format yyyy-MM-dd.<br>"
			+ "<br>projectComponentId : projectComponentId in the format of integer<br>"
			+ "<br>purchaseRequestId : purchaseRequestId in the format of integer<br>"
			+ "<br> purchaseRequestStatus: purchaseRequestStatus in the format of integer<br>",

	nickname = "Purchase Management System Update Asset Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of updated asset requests", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.UPDATE_ASSET_REQUEST, method = RequestMethod.POST,consumes = "multipart/form-data")
	public @ResponseBody ResponseEntity<MISResponse> updateAssetRequest(
			@RequestParam(value = "purchaseAssetRequestUpdateInputBean", name = "purchaseAssetRequestUpdateInputBean") 
			String PurchaseAssetRequestUpdateInputBeanString,
			@RequestParam(value = "uploadSupportDocs", name = "uploadSupportDocs", required = false) 
			List<MultipartFile> uploadSupportDocs)
			throws CommonCustomException {
		
		boolean updateAssetRequest = false;
		System.out.println("Entered controller");
		
		ObjectMapper objectMapper = new ObjectMapper();
		PurchaseAssetRequestUpdateInputBean inputBean = new PurchaseAssetRequestUpdateInputBean();
		
		try {
			inputBean = objectMapper.readValue(PurchaseAssetRequestUpdateInputBeanString,
					PurchaseAssetRequestUpdateInputBean.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		updateAssetRequest = assetRequestService
				.updateAssetRequest(inputBean, uploadSupportDocs);
	
		if (!updateAssetRequest) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Asset Request cannot be updated."));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							" Asset Request is updated successfully."));
		}
	}

	/**
	 * @param RejectAssetRequestInputBean
	 * @return
	 * @throws CommonCustomException
	 * @Description: This method is used to reject asset request records by the
	 *               reporting manager.
	 */
	@ApiOperation(tags = "Purchase Management System", value = " Reject Asset Request Details By Reporting Manager."
			+ "This service will be called from the front-end when the reporting manager wants to reject the asset request ", httpMethod = "POST", notes = "This Service has been implemented to reject asset request record based on RejectAssetRequestInputBean "
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) purchase_dat_asset_request -> It contains all asset request details"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseAssetRequestUpdateInputBean <br><br>"
			+ "<br> pkPurchaseRequestId : pkPurchaseRequestId  in the format of integer <br>"
			+ "<br> rmComment : rmComment in the format of String <br>",

	nickname = "Purchase Management System Reject Asset Request", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Reject Asset request", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.REJECT_ASSET_REQUEST, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> rejectAssetRequest(
			@ApiParam(name = "Purchase Management System Update Asset Request", example = "1", value = "{"
					+ "<br> pkPurchaseRequestId : 1, <br>"
					+ "<br> fkAssetId : 1, <br>"
					+ "<br> rmComment : Invalid request,  <br>" + "}") @Valid @RequestBody RejectAssetRequestInputBean rejectAssetRequestInputBean)
			throws CommonCustomException {
           assetRequestService.rejectAssetRequest(rejectAssetRequestInputBean);
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						" Asset Request is rejected."));
	}
	// EOA by Smrithi
	
	//Added by Shyam
	
	/**
	 * @param PurchaseViewAssetRequestByDateRangeOrRequestorIdInEmpRoleInputBean
	 * @return List<PurchaseViewAssetRequestByDateRangeOrRequestorIdInEmpRoleOutputBean>
	 * @throws CommonCustomException
	 * @Description: This method is used to view all asset request records by employee himself/herself.
	 */
	@ApiOperation(tags = "Purchase Management System", value = " View All Asset Request Details Under Employee Login"
			+ "This service will be called from the front-end when the user wants to view all asset request "
			+ "raised by himself or herself based on requestorId, fromDate and toDate", httpMethod = "POST", 
			notes = "This Service has been implemented to view all asset request record based on empId, fromDate "
			+ "and empEndDate.<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) purchase_dat_asset_request -> It contains all asset request details"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseViewAssetRequestByDateRangeAndEmployeeIdInputBean <br><br>"
			+ "<br> requestorId : employee id in the format of integer <br>"
			+ "<br> fromDate : Date in the format yyyy-MM-dd. <br>"
			+ "<br> toDate : Date in the format yyyy-MM-dd. <br>",

	nickname = "Purchase Management System View Asset Requests Under Employee Login", protocols = "HTTP", responseReference = "application/json", 
		produces = "application/json", response = PurchaseViewAssetRequestByDateRangeAndEmpIdUnderEmpLoginOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseViewAssetRequestByDateRangeAndEmpIdUnderEmpLoginOutputBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of asset request", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.POST_VIEW_ALL_ASSET_REQUEST_UNDER_EMP_LOGIN, 
		method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewAssetRequestUnderEmployeeLogin(
			@Valid @ApiParam(name = "Purchase Management System View Asset Requests Under Employee Login", example = "1", value = "{"
					+ "<br> \"requestorId\" : 2188,"
					+ "<br> \"fromDate\": \"2016-10-01\","
					+ "<br> \"toDate\": \"2016-12-31\"" + "<br> " + "}") @RequestBody 
					PurchaseViewAssetRequestByDateRangeAndEmployeeIdInputBean viewAssetRequestInputBean) 
					throws CommonCustomException 
	{
		List<PurchaseViewAssetRequestByDateRangeAndEmpIdUnderEmpLoginOutputBean> viewAssetRequestUnderEmpLogin =
				new ArrayList<PurchaseViewAssetRequestByDateRangeAndEmpIdUnderEmpLoginOutputBean>();
		
		viewAssetRequestUnderEmpLogin = assetRequestService.viewAssetRequestDetailsUnderEmpLogin(viewAssetRequestInputBean);
		
		if(viewAssetRequestUnderEmpLogin.isEmpty()) 
		{
			return ResponseEntity.status(HttpStatus.OK.value())
				.body(new MISResponse(HttpStatus.OK.value(),MISConstants.SUCCESS,
					"No Asset Request Found"));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
						"View Asset Request List", viewAssetRequestUnderEmpLogin));
		}
	}
	
	

	/**
	 * @param PurchaseViewAssetRequestByDateRangeAndEmployeeIdInputBean
	 * @return List<PurchaseViewAssetRequestByDateRangeAndEmployeeIdOutputBean>
	 * @throws CommonCustomException
	 * @Description: This method is used to view all asset request records by manager.
	 */
	@ApiOperation(tags = "Purchase Management System", value = " View All Asset Request Details Under Reporting Manager Login"
			+ "This service will be called from the front-end when the user wants to view all asset request "
			+ "raised by his/her reportees based on requestorId, fromDate and toDate", httpMethod = "POST", 
			notes = "This Service has been implemented to view all asset request record based on mgrId, fromDate "
			+ "and toDate.<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) purchase_dat_asset_request -> It contains all asset request details"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseViewAssetRequestByDateRangeAndEmployeeIdInputBean <br><br>"
			+ "<br> requestorId : Manager id in the format of integer <br>"
			+ "<br> fromDate : Date in the format yyyy-MM-dd. <br>"
			+ "<br> toDate : Date in the format yyyy-MM-dd. <br>",

	nickname = "Purchase Management System View Asset Requests Under Manager Login", protocols = "HTTP", responseReference = "application/json", 
		produces = "application/json", response = PurchaseViewAssetRequestByDateRangeAndMgrIdUnderMgrLoginOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseViewAssetRequestByDateRangeAndMgrIdUnderMgrLoginOutputBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of asset request", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.POST_VIEW_ALL_ASSET_REQUEST_UNDER_REPORTING_MGR_LOGIN, 
		method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewAssetRequestUnderManagerLogin(
			@Valid @ApiParam(name = "Purchase Management System View Asset Requests Under Manager Login", example = "1", value = "{"
					+ "<br> \"requestorId\" : 2188,"
					+ "<br> \"fromDate\": \"2016-10-01\","
					+ "<br> \"toDate\": \"2016-12-31\"," + "<br> " + "}") @RequestBody 
					PurchaseViewAssetRequestByDateRangeAndEmployeeIdInputBean viewAssetRequestInputBean) 
					throws CommonCustomException 
	{
		List<PurchaseViewAssetRequestByDateRangeAndMgrIdUnderMgrLoginOutputBean> viewAssetRequestUnderMgrLogin =
				new ArrayList<PurchaseViewAssetRequestByDateRangeAndMgrIdUnderMgrLoginOutputBean>();
		
		viewAssetRequestUnderMgrLogin = assetRequestService.viewAssetRequestDetailsUnderMgrLogin(viewAssetRequestInputBean);
		
		if(viewAssetRequestUnderMgrLogin.isEmpty()) 
		{
			return ResponseEntity.status(HttpStatus.OK.value())
				.body(new MISResponse(HttpStatus.OK.value(),MISConstants.SUCCESS,
					"No Asset Request Found"));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
						"View Asset Request List", viewAssetRequestUnderMgrLogin));
		}
	}
	//EOA by Shyam
	
	
	@ApiOperation(tags = "Purchase Module", value = "Reject asset request by System Admin"
			, httpMethod = "POST", notes = "This Service has been implemented to reject asset requests by System Admin"
			+ "<br>The Response will be Success / Failure message (few columns of purchase_dat_asset_request tables will be updated )",
			nickname = "System Admin reject asset request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response =PurchaseDatAssetRequestBO.class )
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseDatAssetRequestBO.class),
			@ApiResponse(code = 201, message = "Fetched Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location",
			description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.SYS_ADMIN_REJECT_ASSET_REQ_PHASETWO, method = RequestMethod.POST)

	public ResponseEntity<MISResponse>rejectAssetRequestBySysAdmin(
			 @Valid @RequestBody RejectAssetReqBySysAdminInputBean assetBean) 
			throws BusinessException,DataAccessException,CommonCustomException
	
	{  
		
		Boolean success = false;
		
		LOG.startUsecase("----Reject Asset request by system admin Contoller-----");
		success=assetRequestService.rejectAssetRequestBySysAdmin(assetBean);
	
		if(success)
		{
			LOG.endUsecase("getAssestsByAssestId");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS, "Asset request rejected successfully"));
	}
		else
		{
			LOG.endUsecase("getAssestsByAssestId");
			return ResponseEntity
					.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"could not update the request staus"));
			
		}
		
	
	}
}
