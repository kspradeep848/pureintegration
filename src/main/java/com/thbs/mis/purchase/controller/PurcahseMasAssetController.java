package com.thbs.mis.purchase.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.PurcahseAssetDetailsQoutesByRequestIdOutputBean;
import com.thbs.mis.purchase.bean.PurcahseMasAssetOutputBean;
import com.thbs.mis.purchase.bean.PurchaseFetchAssetsByAssetTypeAndLicenseTypeInputBean;
import com.thbs.mis.purchase.bean.PurchaseFetchAssetsByAssetTypeAndLicenseTypeOutputBean;
import com.thbs.mis.purchase.bean.PurchaseMasAssetDetailOutputBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;
import com.thbs.mis.purchase.constants.PurchaseManagementURIConstants;
import com.thbs.mis.purchase.service.PurchaseDatAssetRequestService;
import com.thbs.mis.purchase.service.PurchaseMasAssestService;



@Controller
public class PurcahseMasAssetController {
	
	//************Added by Prathibha ****************************//
	
	private static final AppLog LOG = LogFactory
			.getLog(PurchaseMasSettingController.class);
	 
	  @Autowired
	   PurchaseMasAssestService purchaseMasAssestService;
		
	  @Autowired
	  PurchaseDatAssetRequestService  assetRequestService;
	  
	  @Deprecated
		@SuppressWarnings("unused")
		@ApiOperation(tags = "Purchase Module", value = "get Asset based on AssetId"
				, httpMethod = "GET", notes = "This Service has been implemented to fetch perticular asset"
				+ "<br>The Response will purcahse_mas_assets columns",
				nickname = "Fetch Assets based on asset_ID", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurcahseMasAssetOutputBean.class)
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurcahseMasAssetOutputBean.class),
				@ApiResponse(code = 201, message = "Fetched Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location",
				description = "The created Program Details resource", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = PurchaseManagementURIConstants.FETCH_ASSETS, method = RequestMethod.GET)
		 public ResponseEntity<MISResponse> getAssestsBySoftwareLicenseId(@PathVariable Integer assetId) 
				throws BusinessException,DataAccessException,CommonCustomException 
		
		{
			
			
			LOG.debug("Inside getAssestsByAssestId Contoller");
			List<PurchaseMasAssetDetailOutputBean> massAssetList =new ArrayList<PurchaseMasAssetDetailOutputBean>();
			PurchaseMasAssetDetailOutputBean massAsset=new PurchaseMasAssetDetailOutputBean();
			
				LOG.debug("****Fetch Assets Contoller try block ********");
				System.out.println("purcahseMasAsset Contoller try Block");
				
				massAsset=purchaseMasAssestService.getAssestsByAssetId(assetId);		
			massAssetList.add(massAsset);
			if(massAssetList!=null)
			{
				LOG.endUsecase("getAssestsByAssestId");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Assest List are",massAssetList));
		}
			else
			{
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"cold not retrive asset details"));
				
			}
		}
	
 //EOA by Prathibha for Fetch assest by Asset Id 
	  
	  
	  /**************Fetch Asset Details based on purchaseRequestId**********************/
		@SuppressWarnings("unused")	
		@ApiOperation(tags = "Purchase Module", value = "Fetch assetDetails by request_Id"
				, httpMethod = "POST", notes = "This Service has been implemented to Fetch assetDetails by request_Id",
				nickname = "Fetch assetDetails by request_Id by System Admin", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response =PurchaseDatAssetRequestBO.class )
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseDatAssetRequestBO.class),
				@ApiResponse(code = 201, message = "Fetched Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location",
				description = "The created Program Details resource", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = PurchaseManagementURIConstants.GET_ASSET_DETAILS_WITH_QUOTE_LIST, method = RequestMethod.GET)

		public ResponseEntity<MISResponse> getAssetDetailsalongwithQuoteslistByReqId(@PathVariable Integer purchaseRequestId) 
				throws BusinessException,DataAccessException ,CommonCustomException
		
		{
			LOG.debug("Inside getAssetDetailsalongwithQuoteslistByReqId Contoller");
			List<PurcahseAssetDetailsQoutesByRequestIdOutputBean> assetsQuotesBeanList =new ArrayList<PurcahseAssetDetailsQoutesByRequestIdOutputBean>();
			PurcahseAssetDetailsQoutesByRequestIdOutputBean assetsQuotesBean=new PurcahseAssetDetailsQoutesByRequestIdOutputBean();
			
			
			assetsQuotesBean=purchaseMasAssestService.getAssetandQuotesListByRequestId(purchaseRequestId);
			
			assetsQuotesBeanList.add(assetsQuotesBean);		
		
			if(assetsQuotesBeanList!=null)
			{
				LOG.endUsecase("getAssestsByAssestId");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Assest List are",assetsQuotesBeanList));
		}
			else
			{
				LOG.endUsecase("getAssestsByAssestId");
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"No asset details found."));
				
			}
		}
	  
		//Added by Smrithi
		
		/**
		 * @return
		 * @throws CommonCustomException
		 * @Description: This method is used to fetch the assets by asset
		 *               type and license type.
		 */
		@ApiOperation(tags = "Purchase Management System", value = "Fetch the assets by asset type and license type"
				+ "This service will be called from the front-end when the Sys Admin  wants to view the assets. ", httpMethod = "POST", notes = "This Service has been implemented to fetch the assets based on asset type and license type. "
				+ "<br>The Details will be fetched from database."
				+ "<br> <b> Table : </b> </br>"
				+ "1) purchase_mas_assets ->  It contains all assets <br>"
				+ "<br>",

		nickname = "Purchase Management System Fetch the assets by asset type and license type.", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully"),
				@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Fetch the assets by asset type and license type.", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = PurchaseManagementURIConstants.POST_FETCH_ASSESTS_BASED_ON_ASSET_TYPE_AND_LICENSE_TYPE, method = RequestMethod.POST)
		public @ResponseBody ResponseEntity<MISResponse> fetchAssets(
				@ApiParam(name = "Purchase Management System Fetch the assets by asset type and license type.", example = "1", value = "{"
						+ "<br> assetType : Software,"
						+ "<br> licenseType : Renewal," + "}") @Valid @RequestBody PurchaseFetchAssetsByAssetTypeAndLicenseTypeInputBean purchaseAssetsInputBean )
				throws CommonCustomException {

			List<PurchaseFetchAssetsByAssetTypeAndLicenseTypeOutputBean> assetsList = new ArrayList<PurchaseFetchAssetsByAssetTypeAndLicenseTypeOutputBean>();
			assetsList = purchaseMasAssestService
					.fetchAssets(purchaseAssetsInputBean);

			//assetsList.add(assetsOutput);

			if(!assetsList.isEmpty()){
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							" Assets are displayed.", assetsList));
			}else{
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
								" No Assets to display.", assetsList));
			}
		}

		
		
		//EOA of Smrithi
		
		
	  
}
