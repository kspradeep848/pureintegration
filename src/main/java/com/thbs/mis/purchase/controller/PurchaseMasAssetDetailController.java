package com.thbs.mis.purchase.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.PurcahseMasAssetOutputBean;
import com.thbs.mis.purchase.bean.PurchaseAssetDetailForAssetRequestOutputBean;
import com.thbs.mis.purchase.bean.PurchaseAssetDetailsOutputBean;
import com.thbs.mis.purchase.bean.PurchaseMasAssetDetailOutputBean;
import com.thbs.mis.purchase.bean.PurchaseMassAssetDetailOutputBean;
import com.thbs.mis.purchase.constants.PurchaseManagementURIConstants;
import com.thbs.mis.purchase.service.AssetRequestService;
import com.thbs.mis.purchase.service.PurchaseMasAssetDetailsService;
//Added by Prathibha -3610

@Controller
public class PurchaseMasAssetDetailController {

	
	private static final AppLog LOG = LogFactory
			.getLog(PurchaseMasSettingController.class);
	
	@Autowired
	PurchaseMasAssetDetailsService purchaseMasAssetDetailsService;
	
	@Autowired
	AssetRequestService assetRequestService;
	
	
	/***Get Asset Details by subscription id**********************/
	
	@SuppressWarnings("unused")	
	
	@ApiOperation(tags = "Purchase Module", value = "get Asset details based on SuscriptionId"
			, httpMethod = "GET", notes = "This Service has been implemented to fetch perticular asset"
			+ "<br>The Response will purcahse_mas_assets_details and purcahse_mas_asset table columns",
			nickname = "Fetch Assets details based on subcriptionId", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurcahseMasAssetOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseMasAssetDetailOutputBean.class),
			@ApiResponse(code = 201, message = "Fetched Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location",
			description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.GET_ASSETS, method = RequestMethod.GET)
	 
	public ResponseEntity<MISResponse> getAssestDetailsBysubcrptId(@PathVariable String subscriptionId) 
			throws BusinessException,DataAccessException ,CommonCustomException
	
	{
		LOG.debug("Inside getAssestsByAssestId Contoller");
		List<PurchaseMasAssetDetailOutputBean> massAssetDetList =new ArrayList<PurchaseMasAssetDetailOutputBean>();
		PurchaseMasAssetDetailOutputBean massAsset=new PurchaseMasAssetDetailOutputBean();
		
			LOG.debug("****Fetch Assets Contoller try block ********");
			System.out.println("purcahseMasAsset Contoller try Block");
			
			massAsset=purchaseMasAssetDetailsService.getAssestDetailsBysubcrptId(subscriptionId);		
			massAssetDetList.add(massAsset);
		if(massAssetDetList!=null)
		{
			LOG.endUsecase("getAssestsByAssestId");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS, "Assest List are",massAssetDetList));
	}
		else
		{
			LOG.endUsecase("getAssestsByAssestId");
			return ResponseEntity
					.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"cold not retrive asset details"));
			
		}
	}

	
	
	/************************************************Fetch all Assets based on subscriptionId****************************/
	
	//EOA by Prathibha
	
	// Added by pratibha
	
	@ApiOperation(tags = "Purchase", value = "Get Asset And Its Detail From AssetId for system admin"
			+ "This service will be called from the front-end to view the asset and it detail", httpMethod = "GET", notes = "This Service has been implemented to view the asset and it detail."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)purchase_mas_asset_detail -> It contains asset type and its detail."
			+ "<br>"
			+ "<br><b>Path variable :</b><br>"
			+ "<br> assetId : fkMasAssetsId  in the format of integer <br>",
     nickname = "Asset and its detail", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseMassAssetDetailOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseMassAssetDetailOutputBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of asset and its detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.GET_ASSET_DETAIL_FROM_ASSET_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAssetAndItsDetailFromAssetId(
			@ApiParam(name = "Get Asset And Its Detail From AssetId for system admin.", example = "1", value = "{"
					+ "<br> assetId : 1, <br>" + "}")
			@PathVariable Integer assetId) throws CommonCustomException {
		LOG.startUsecase("Get Asset And Its Detail From AssetId for system admin");
		List<PurchaseMassAssetDetailOutputBean> massAssetDetailOutputBeanList = new ArrayList<PurchaseMassAssetDetailOutputBean>();

		massAssetDetailOutputBeanList = purchaseMasAssetDetailsService
				.getAssetAndItsDetailFromAssetId(assetId);

		if (!massAssetDetailOutputBeanList.isEmpty()) {
			LOG.endUsecase("Get Asset And Its Detail From AssetId for system admin");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully retrieved asset details.",
							massAssetDetailOutputBeanList));
		} else {
			LOG.endUsecase("Get Asset And Its Detail From AssetId for system admin");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No asset details found."));
		}

	}
	
	// EOA pratibha
	
	
	//Added by Shyam for phase-2 subscription id
	@ApiOperation(tags = "Purchase Phase-2 ", value = "Get all asset details with subscription id from asset request id"
			+ "This service will be called from the front-end to view get the subscrition id for that asset request", 
			httpMethod = "GET", notes = "This Service has been implemented to view the subscription id."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)purchase_mas_asset_detail -> It contains asset details with subscription id."
			+ "<br>",
     nickname = "Subscription Id based on asset request id", protocols = "HTTP", responseReference = "application/json", produces = "application/json", 
     response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Subscription id detail from asset request", description = "Subscription id detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.GET_EXISTING_SUBSCRIPTION_ID_FROM_ASSET_REQUEST, method = RequestMethod.GET)
	 
	public ResponseEntity<MISResponse> getSubscriptionIdFromAssetRequestId(@PathVariable Integer requestId) 
			throws CommonCustomException
	{
		List<PurchaseMassAssetDetailOutputBean> massAssetDetList =new ArrayList<PurchaseMassAssetDetailOutputBean>();
		PurchaseMassAssetDetailOutputBean outputBean = new PurchaseMassAssetDetailOutputBean();
		outputBean = purchaseMasAssetDetailsService.getSubscriptionIdFromAssetRequestId(requestId);		
		
		massAssetDetList.add(outputBean);
		if(!massAssetDetList.isEmpty())
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS, "Subscription details found",massAssetDetList));
		}else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,"No Subscription Id Found"));
			
		}
	}
	//EOA by Shyam
	
}
	
	
	


