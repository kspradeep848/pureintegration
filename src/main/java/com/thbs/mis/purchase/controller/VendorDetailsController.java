package com.thbs.mis.purchase.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.util.Set;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.Validator;

import org.apache.commons.httpclient.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.PurchaseAddVendorDetailsInputBean;
import com.thbs.mis.purchase.bean.PurchaseAddVendorForExistingAssetBean;
import com.thbs.mis.purchase.bean.PurchaseInputBean;
import com.thbs.mis.purchase.bean.PurchaseUpdateVendorDetailsInputBean;
import com.thbs.mis.purchase.bean.PurchaseVendorDetailsBean;
import com.thbs.mis.purchase.bean.PurchaseVendorDetailsUpdateBean;
import com.thbs.mis.purchase.bo.PurchaseDatSupplierDetailBO;
import com.thbs.mis.purchase.bo.PurchaseMasAssetsBO;
import com.thbs.mis.purchase.constants.PurchaseManagementURIConstants;
import com.thbs.mis.purchase.service.VendorDetailsService;

@Controller
public class VendorDetailsController {

	private static final AppLog LOG = LogFactory
			.getLog(VendorDetailsController.class);

	@Autowired
	private VendorDetailsService vendorDetailsService;

	/**
	 * 
	 * @param assetId
	 * @return
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to delete software.
	 */

	@ApiOperation(tags = "Purchase", value = "Delete Software"
			+ "This service will be called from the front-end inorder to delete software", httpMethod = "PUT", notes = "This Service has been implemented to delete software."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)purchase_mas_assets -> It contains details about the requested asset."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseInputBean <br><br>"
			+ "<br> id : pkAssetId  in the format of integer <br>"
			+ "<br> loggedInUser : loggedInUser  in the format of integer <br>",nickname = "Purchase Delete Software", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseMasAssetsBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseMasAssetsBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Deleted software request detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.DELETE_SOFTWARE, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> deleteSoftware(
			@ApiParam(name = "Delete software by system admin ", example = "1", value = "{"
					+ "<br> id : 1, <br>"
					+ "<br> loggedInUser : 115,  <br>" + "}")
			@Valid @RequestBody PurchaseInputBean deleteSoftwareBean) throws CommonCustomException {
		LOG.startUsecase("Delete software");

		vendorDetailsService.deleteSoftware(deleteSoftwareBean);

		LOG.endUsecase("Delete software");
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"Software is deleted successfully."));
	}

	/**
	 * 
	 * @param supplierId
	 * @return
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to delete vendor.
	 */

	@ApiOperation(tags = "Purchase", value = "Delete vendor"
			+ "This service will be called from the front-end inorder to delete vendor", httpMethod = "PUT", notes = "This Service has been implemented to delete vendor."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)purchase_dat_supplier_details -> It contains details about the vendor."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseInputBean <br><br>"
			+ "<br> id : pkSupplierId  in the format of integer <br>"
			+ "<br> loggedInUser : loggedInUser  in the format of integer <br>",nickname = "Purchase delete vendor", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseDatSupplierDetailBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseDatSupplierDetailBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Deleted vendor detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.DELETE_VENDOR, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> deleteVendor(
			@ApiParam(name = "Delete vendor by system admin", example = "1", value = "{"
					+ "<br> pkSupplierId : 1, <br>" + "}")
			@Valid @RequestBody PurchaseInputBean deleteVendorBean) throws CommonCustomException {
		LOG.startUsecase("Delete vendor");

		vendorDetailsService.deleteVendor(deleteVendorBean);

		LOG.endUsecase("Delete vendor");
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"Vendor is deleted successfully."));
	}
	
	@RequestMapping(value = PurchaseManagementURIConstants.ADD_VENDOR_DETAILS_FOR_EXISTING_ASSET, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> addVendorDetailsForExistingAsset(
			@ApiParam(name = "Purchase Management System Add Vendor Details ", example = "1", value = "{"
					+ "<br> assetType : Hardware, <br>"
					+ "<br> assetNameAndVersionOrModel : MAC book, <br>"
					+ "<br> vendorName : abc pvt limited,  <br>"
					+ "<br> vendorAddress : Millers road,Bangalore, <br>"
					+ "<br> priceQuotedByVendor : 2000,  <br>" + "}") @Valid @RequestBody PurchaseAddVendorForExistingAssetBean purchaseAddVendorForExistingAssetBean)
			throws CommonCustomException, ConstraintViolationException {
		LOG.startUsecase("Add Vendor Details for existing asset");
		if (purchaseAddVendorForExistingAssetBean.getVendorDetails() != null
				&& purchaseAddVendorForExistingAssetBean.getVendorDetails().size() > 0)
			for (PurchaseVendorDetailsBean vendorDetail : purchaseAddVendorForExistingAssetBean
					.getVendorDetails()) {
				Set violations = validator.validate(vendorDetail);
				if (!violations.isEmpty())
					throw new ConstraintViolationException(violations);
			}

		boolean addVendorDetails = false;
		addVendorDetails = vendorDetailsService
				.addVendorDetailsForExistingAsset(purchaseAddVendorForExistingAssetBean);

		if (addVendorDetails) {			
			LOG.endUsecase("Add Vendor Details for existing asset");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Vendor details added successfully."));
		}
		else {
			LOG.endUsecase("Add Vendor Details for existing asset");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Vendor details cannot be added ."));
		}
	}
	
	
	
	
	

	// Added by Smrithi

	@Autowired
	private Validator validator;

	/**
	 * @param PurchaseAddVendorDetailsInputBean
	 * @return
	 * @throws CommonCustomException
	 * @Description: This method is used to add vendor details by
	 *               PurchaseAddVendorDetailsInputBean .
	 */
	@ApiOperation(tags = "Purchase Management System", value = " Add Vendor Details by PurchaseAddVendorDetailsInputBean."
			+ "This service will be called from the front-end when the system admin wants to add the vendor details.", httpMethod = "POST", notes = "This Service has been implemented to add vendor details by PurchaseAddVendorDetailsInputBean. "
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) purchase_mas_assets -> It contains all asset details. <br>"
			+ "2) purchase_dat_supplier_details -> It contains all the vendor details."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseAddVendorDetailsInputBean <br><br>"
			+ "<br> assetType : assetType in the format of String <br>"
			+ "<br> assetNameAndVersionOrModel : assetNameAndVersionOrModel in the format of String <br>"
			+ "<br> vendorName : vendorName in the format of String <br> "
			+ " <br> vendorAddress : vendorAddress in the format of String <br>"
			+ "<br> priceQuotedByVendor : priceQuotedByVendor  in the format of double <br>", nickname = "Purchase Management System Add Vendor Details ", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Reject Capex ", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.ADD_VENDOR_DETAILS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> addVendorDetails(
			@ApiParam(name = "Purchase Management System Add Vendor Details ", example = "1", value = "{"
					+ "<br> assetType : Hardware, <br>"
					+ "<br> assetNameAndVersionOrModel : MAC book, <br>"
					+ "<br> vendorName : abc pvt limited,  <br>"
					+ "<br> vendorAddress : Millers road,Bangalore, <br>"
					+ "<br> priceQuotedByVendor : 2000,  <br>" + "}") @Valid @RequestBody PurchaseAddVendorDetailsInputBean purchaseAddVendorDetailsInputBean)
			throws CommonCustomException, ConstraintViolationException {
		if (purchaseAddVendorDetailsInputBean.getVendorDetails() != null
				&& purchaseAddVendorDetailsInputBean.getVendorDetails().size() > 0)
			for (PurchaseVendorDetailsBean vendorDetail : purchaseAddVendorDetailsInputBean
					.getVendorDetails()) {
				Set violations = validator.validate(vendorDetail);
				if (!violations.isEmpty())
					throw new ConstraintViolationException(violations);
			}

		boolean addVendorDetails = false;
		addVendorDetails = vendorDetailsService
				.addVendorDetails(purchaseAddVendorDetailsInputBean);

		if (addVendorDetails) {
			
			if(purchaseAddVendorDetailsInputBean.getVendorDetails() != null)
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Software and Vendor details added successfully."));
			else
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Software details added successfully."));
		}
		else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							" Software and Vendor details cannot be added ."));
		}
	}

	/**
	 * @param PurchaseUpdateVendorDetailsInputBean
	 * @return
	 * @throws CommonCustomException
	 * @Description: This method is used to update vendor details by
	 *               PurchaseUpdateVendorDetailsInputBean .
	 */
	@ApiOperation(tags = "Purchase Management System", value = " Update Vendor Details by PurchaseUpdateVendorDetailsInputBean."
			+ "This service will be called from the front-end when the system admin wants to update the vendor details.", httpMethod = "POST", notes = "This Service has been implemented to update vendor details by PurchaseUpdateVendorDetailsInputBean. "
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) purchase_mas_assets -> It contains all asset details. <br>"
			+ "2) purchase_dat_supplier_details -> It contains all the vendor details."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseAddVendorDetailsInputBean <br><br>"
			+ "<br> assetType : assetType in the format of String <br>"
			+ "<br> assetNameAndVersionOrModel : assetNameAndVersionOrModel in the format of String <br>"
			+ "<br> vendorName : vendorName in the format of String <br> "
			+ " <br> vendorAddress : vendorAddress in the format of String <br>"
			+ "<br> priceQuotedByVendor : priceQuotedByVendor  in the format of double <br>"
			+ "<br> deliveryTime : deliveryTime in the format of Integer <br> "
			+ "<br> contactPerson : contactPerson in the format of String <br>"
			+ "<br> emailId : emailId in the format of String <br>", nickname = "Purchase Management System Update Vendor Details ", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Update Vendor Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.UPDATE_VENDOR_DETAILS, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> updateVendorDetails(
			@ApiParam(name = "Purchase Management System Update Vendor Details ", example = "1", value = "{"
					+ "<br> assetType : Hardware, <br>"
					+ "<br> assetNameAndVersionOrModel : MAC book, <br>"
					+ "<br> vendorName : abc pvt limited,  <br>"
					+ "<br> vendorAddress : Millers road,Bangalore, <br>"
					+ "<br> priceQuotedByVendor : 2000,  <br>"
					+ "<br> deliveryTime : 5,"
					+ "<br> contactPerson : Farooq"
					+ "" + "}") @Valid @RequestBody PurchaseUpdateVendorDetailsInputBean purchaseUpdateVendorDetailsInputBean)
			throws CommonCustomException {

		if (purchaseUpdateVendorDetailsInputBean.getVendorDetails() != null
				&& purchaseUpdateVendorDetailsInputBean.getVendorDetails()
						.size() > 0)
			for (PurchaseVendorDetailsUpdateBean vendorDetail : purchaseUpdateVendorDetailsInputBean
					.getVendorDetails())

			{
				Set violations = validator.validate(vendorDetail);
				if (!violations.isEmpty())
					throw new ConstraintViolationException(violations);
			}
		boolean updateVendorDetails = false;
		updateVendorDetails = vendorDetailsService
				.updateVendorDetails(purchaseUpdateVendorDetailsInputBean);

		if (!updateVendorDetails) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Vendor details cannot be updated."));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							" Vendor details updated successfully."));
		}
	}

	// EOA by Smrithi

}