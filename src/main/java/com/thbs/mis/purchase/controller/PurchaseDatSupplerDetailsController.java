package com.thbs.mis.purchase.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.AddquoteUpdateVendordetailInputBean;
import com.thbs.mis.purchase.bean.PurchaseAssetAndVendorInfoBean;
import com.thbs.mis.purchase.bean.PurchaseDatSupplierDetailsOutPutBean;
import com.thbs.mis.purchase.bean.PurchaseDatSupplierQuoteDetailsOutPutBean;
import com.thbs.mis.purchase.constants.PurchaseManagementURIConstants;
import com.thbs.mis.purchase.service.PurchaseDatSupplierDetailsService;



@Controller
public class PurchaseDatSupplerDetailsController {

	private static final AppLog LOG = LogFactory
			.getLog(PurchaseMasSettingController.class);
	
	//Added by Prathibha for Fetch Vendor details based on Asset Id
	
	@Autowired
	PurchaseDatSupplierDetailsService supplierDetService;
	
	@SuppressWarnings("unused")
	@ApiOperation(tags = "Purchase Module", value = "Get Supplier details based on Asset-Id"
			, httpMethod = "GET", notes = "This Service has been implemented to fetch supplier details"
			+ "<br>The Response will purcahse_dat_supplier_details table columns",
			nickname = "Fetch supplier details based on AssetId", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseDatSupplierDetailsOutPutBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseDatSupplierDetailsOutPutBean.class),
			@ApiResponse(code = 201, message = "Fetched Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location",
			description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.GET_SUPPLIER_DETAILS, method = RequestMethod.GET)

	public ResponseEntity<MISResponse> getSupplierDetailsByassetId(@PathVariable Integer assetId) 
			throws BusinessException,DataAccessException,CommonCustomException
	
	{
		 PurchaseAssetAndVendorInfoBean supplerDetBean=new  PurchaseAssetAndVendorInfoBean();
		List<PurchaseAssetAndVendorInfoBean> supplerDetBeanList=new ArrayList<PurchaseAssetAndVendorInfoBean>();
		
		LOG.startUsecase("Supplier Details Controller");
			supplerDetBean=supplierDetService.getSupplierDetailsByAssetId(assetId);
			supplerDetBeanList.add(supplerDetBean);
		
			if (supplerDetBeanList!=null)
			{
				LOG.endUsecase("Supplier Details Contoller if block");
				
				LOG.endUsecase("getAssestsByAssestId");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Supplier details are",supplerDetBeanList));
				
			}
			else
			{
				
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"could not retrive Supplier details for this asset Id",supplerDetBeanList));
			
			}
			
	}
	//EOA by Prathibha for Fetch Vendor details based on Asset Id
	
	
	//Added by Prathibha for Fetch Supplier details along with their quotes based on Asset_id
	
	
	@SuppressWarnings("unused")
	@ApiOperation(tags = "Purchase Module", value = "Get Supplier details along with quotes details based on Asset-Id"
			, httpMethod = "GET", notes = "This Service has been implemented to fetch supplier details along with quotes details based on Asset-Id"
			+ "<br>The Response will purcahse_dat_supplier_details table columns",
			nickname = "Fetch supplier and quote details  based on AssetId", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseDatSupplierQuoteDetailsOutPutBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseDatSupplierQuoteDetailsOutPutBean.class),
			@ApiResponse(code = 201, message = "Fetched Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location",
			description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.GET_SUPPLIER_QUOTE_DETAILS_BY_PURREQID, method = RequestMethod.GET)

	public ResponseEntity<MISResponse> getSupplierQuoteDetailsByRequestId(@PathVariable("purchaseReqId") Integer purchaseReqId) 
			throws BusinessException,DataAccessException,CommonCustomException
	
	{
		List <PurchaseDatSupplierQuoteDetailsOutPutBean> supplerQuoteBeanList=new ArrayList<PurchaseDatSupplierQuoteDetailsOutPutBean>();
		
		LOG.startUsecase("Supplier Details Controller");
		supplerQuoteBeanList=supplierDetService.getSupplierQuoteDetailsByRequestId(purchaseReqId);
			
		
			if (supplerQuoteBeanList!=null)
			{
				LOG.endUsecase("Supplier Details Contoller if block");
				
				LOG.endUsecase("getAssestsByPurchaseRequestId");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Supplier details are",supplerQuoteBeanList));
				
			}
			else
			{
				LOG.endUsecase("getSupplierQuoteDetailsByPurchaseReqId Controller");
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"cold not retrive Supplier details for this Request Id "));
			
			}
			
	}
	
	
	
	@SuppressWarnings("unused")
	@ApiOperation(tags = "Purchase Module", value = "Get Supplier details along with quotes details based on Asset-Id"
			, httpMethod = "GET", notes = "This Service has been implemented to fetch supplier details along with quotes details based on Asset-Id"
			+ "<br>The Response will purcahse_dat_supplier_details table columns",
			nickname = "Fetch supplier and quote details  based on AssetId", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseDatSupplierQuoteDetailsOutPutBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseDatSupplierQuoteDetailsOutPutBean.class),
			@ApiResponse(code = 201, message = "Fetched Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location",
			description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.GET_SUPPLIER_QUOTE_DETAILS_BY_ASSETID, method = RequestMethod.GET)

	public ResponseEntity<MISResponse> getSupplierQuoteDetailsByAssetIdId(@PathVariable("assetId") Integer assetId) 
			throws BusinessException,DataAccessException,CommonCustomException
	
	{
		List <PurchaseDatSupplierQuoteDetailsOutPutBean> supplerQuoteBeanList=new ArrayList<PurchaseDatSupplierQuoteDetailsOutPutBean>();
		
		LOG.startUsecase("Supplier Details Controller");
		supplerQuoteBeanList=supplierDetService.getSupplierQuoteDetailsByAssetId(assetId);
			
		
			if (supplerQuoteBeanList!=null)
			{
				LOG.endUsecase("Supplier Details Contoller if block");
				
				LOG.endUsecase("getAssestsByAssestId");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Supplier details are",supplerQuoteBeanList));
				
			}
			else
			{
				LOG.endUsecase("getSupplierQuoteDetailsByassetId Controller");
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"cold not retrive Supplier details for this asset Id"));
			
			}
			
	}
	
	
	
	//EOA by Prathibha for Fetch Supplier details along with thier quotes based on Asset_id

	/***************Update Vendor details for AddQuotes based on vendor Id**********************/
	
	
	@ApiOperation(tags = "Purchase Management System", value = " Update Vendor Details for add quotes based on vendorId."
			+ "This service will be called from the front-end when the system admin wants add quotes and update the vendor details. in the quote details", httpMethod = "PUT", notes = "This Service has been implemented to update vendor details by system admin. "
			+ "<br>The Details will be saved to supplier details table in database."
			+ "<br> <b> Table : </b> </br>"
			, nickname = " Update Vendor details for addQuote  ", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Updated Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Add quote-Update Vendor Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.ADD_QUOTE_UPDATE_VENDOR_DETAILS, method = RequestMethod.POST)

	public @ResponseBody ResponseEntity<MISResponse> addQuoteUpdateVendorDetails(
			 @Valid @RequestBody AddquoteUpdateVendordetailInputBean  updateVendorInputBean )
			throws CommonCustomException {
          
		  boolean vendorDetailsUpdated=false;
		  
		  try{
		  vendorDetailsUpdated=supplierDetService.addQuoteUpdateVendor(updateVendorInputBean.getSupplierId());
		  }
		  catch (Exception e)
		  {
			  throw new CommonCustomException("Exception occured in Controlle",e.getMessage());
			  
		  }
		  
		  if (!vendorDetailsUpdated) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
						new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.SUCCESS,
								"Vendor details cannot be updated."));
			} else {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								" Vendor details updated successfully."));
			}
		
		
		
	}
	


}
	
	

		
		
		
	
	

		
		


