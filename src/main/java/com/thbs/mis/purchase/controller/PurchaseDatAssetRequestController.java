package com.thbs.mis.purchase.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.RejectAssetReqBySysAdminInputBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;
import com.thbs.mis.purchase.constants.PurchaseManagementURIConstants;
import com.thbs.mis.purchase.service.PurchaseDatAssetRequestService;

//Added by Prathibha for Reject Asset request by System Admin

@Controller
public class PurchaseDatAssetRequestController {

	private static final AppLog LOG = LogFactory
			.getLog(PurchaseMasSettingController.class);
	
	@Autowired
	PurchaseDatAssetRequestService assetReqService;
	
	@ApiOperation(tags = "Purchase Module", value = "Reject asset request by System Admin"
			, httpMethod = "POST", notes = "This Service has been implemented to reject asset requests by System Admin"
			+ "<br>The Response will be Success / Failure message (few columns of purchase_dat_asset_request tables will be updated )",
			nickname = "System Admin reject asset request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response =PurchaseDatAssetRequestBO.class )
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseDatAssetRequestBO.class),
			@ApiResponse(code = 201, message = "Fetched Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location",
			description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.SYS_ADMIN_REJECT_ASSET_REQ, method = RequestMethod.POST)

	public ResponseEntity<MISResponse>rejectAssetRequestBySysAdmin(
			 @Valid @RequestBody RejectAssetReqBySysAdminInputBean assetBean) 
			throws BusinessException,DataAccessException,CommonCustomException
	
	{  
		
		PurchaseDatAssetRequestBO assetReqBO=new PurchaseDatAssetRequestBO();
		
		LOG.startUsecase("----Reject Asset request by system admin Contoller-----");
		assetReqBO=assetReqService.rejectAssetRequestBySysAdmin(assetBean);
	
		if(assetReqBO!=null)
		{
			LOG.endUsecase("getAssestsByAssestId");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS, "Asset rejected successfully"));
	}
		else
		{
			LOG.endUsecase("getAssestsByAssestId");
			return ResponseEntity
					.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"could not update the request staus"));
			
		}
		
	
	}
	
	
}

	
	
	
	
	
	
	
	
	
	

