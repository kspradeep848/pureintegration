package com.thbs.mis.purchase.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.PurchaseDatAssetQuotesInputBean;
import com.thbs.mis.purchase.bean.UpdateQuoteInputBean;
import com.thbs.mis.purchase.bean.UpdateQuoteOutputBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetQuoteBO;
import com.thbs.mis.purchase.constants.PurchaseManagementURIConstants;
import com.thbs.mis.purchase.service.PurchaseDatAssetQuoteService;


//Added by Prathibha for Add and Update Quotes

@Controller
public class PurchaseDatAssetQuoteController {

	private static final AppLog LOG = LogFactory
			.getLog(PurchaseMasSettingController.class);
	
	
	@Autowired
	PurchaseDatAssetQuoteService assetQuoteService;
	
	
	
     	/*************Add Quotes*******************************************/
	
	
	/**
	 * @Description method to add quotes
	 * @param PurchaseDatAssetQuotesInputBean
	 * @return Message
	 * @throws CommonCustomException 
	 * @throws BusinessException
	 */
	
	@ApiOperation(tags="Purchase",value = "Add Quotes.", httpMethod = "POST", 
			notes = "This Service has been implemented to add quotes "
			+ "<br><b>Inputs :</b><br> PurchaseDatAssetQuotesInputBean <br><br>",

	nickname = "Add Quotes", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response =PurchaseDatAssetQuotesInputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response =PurchaseDatAssetQuoteBO .class),
			@ApiResponse(code = 201, message = "Added Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value =PurchaseManagementURIConstants.PURCHASE_ADD_QUOTES , method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> addQuotes(
			@Valid @RequestBody PurchaseDatAssetQuotesInputBean quotesInputBean) throws  CommonCustomException
	{
		/*PurchaseDatAssetQuoteBO quoteBO=new PurchaseDatAssetQuoteBO();*/
		/*PurcahseQuotesOutPutBean quoteOuputBean = new PurcahseQuotesOutPutBean();*/ 
		boolean quoteAdded = false;
		
		
		//quoteOuputBean=assetQuoteService.addQuotes(quotesInputBean);
		
		quoteAdded = assetQuoteService.addQuotes(quotesInputBean);
	        
		         
		
				/*if (quoteOuputBean!=null)*/
		       if (!quoteAdded)
				{
		    	   LOG.endUsecase("Add quotes Controller...Couldnt add quotes");
					return ResponseEntity.status(HttpStatus.OK.value()).body(
							new MISResponse(HttpStatus.OK.value(),
									MISConstants.SUCCESS,
									"Quotes could not be added"));
				}
				else
				{
					LOG.endUsecase("Add quotes Controller...Quotes Added ");
					return ResponseEntity.status(HttpStatus.OK.value())
							.body(new MISResponse(HttpStatus.OK.value(),
									MISConstants.SUCCESS, "Quotes added successfuly"));
				}	
				
		
			}
	
	
	
	
	
	/****************Update Quotes Based on Quote-Id**************************//*
	
	*//**
	 * @Description method to update quotes based on Quote-Id
	 * @param PurchaseDatAssetQuotesInputBean
	 * @return Message
	 * @throws BusinessException,DataAccessException,CommonCustomException
	 */
	
	
	
	@ApiOperation(tags = "Purchase Module", value = "Update quotes based on Quote-Id"
			, httpMethod = "POST", notes = "This Service has been implemented to update quote details"
			+ "<br>The Response will be Success Message",
			nickname = "Fetch supplier details based on AssetId", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseDatAssetQuoteBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseDatAssetQuoteBO.class),
			@ApiResponse(code = 201, message = "Fetched Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location",
			description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.UPDATE_QUOTES, method = RequestMethod.PUT)
	public  @ResponseBody ResponseEntity<MISResponse> updateQuotesByQuoteId(
			@Valid @RequestBody UpdateQuoteInputBean quoteInputBean )
			throws CommonCustomException
	
	{   PurchaseDatAssetQuoteBO quotesBo=new PurchaseDatAssetQuoteBO();
	     UpdateQuoteOutputBean quoteOutputBean = new UpdateQuoteOutputBean();
	     
	     LOG.debug("Inside controller : "+quoteInputBean);
	  
	     quoteOutputBean=assetQuoteService.updateQuotes(quoteInputBean);
	    
	      
	    
	    if(quotesBo!=null)
	    {
	    	LOG.startUsecase("Update quotes Controller...updated quotes successfuly");
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Quotes updated successfuly"));
	    	
	    }
	     
	    else
		{
			LOG.endUsecase("Update quotes Controller...could not update quotes");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Quotes could not be updated"));
		}	
	    
	 
	
	}
	
	//EOA by Prathibha for Add and Update Quotes
	
	
	//Added by Shyam for phase 2 service
	/**
	 * @param PurchaseUpdateCapexInputBean
	 * @return
	 * @throws CommonCustomException
	 * @Description: This method is used to do update capex.
	 */
	@ApiOperation(tags = "Purchase Management System Phase-2 ", value = "Update Quote By System Admin."
			+ "This service will be called from the front-end when System Admin wants to update the quote.", httpMethod = "PUT", 
			notes = "This Service has been implemented to update the quote by System Admin before RM approved."
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) purchase_dat_asset_quotes -> It conatains all quotes details related to the asset request id. <br>"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseUpdateCapexInputBean <br><br>"
			+ "<br> costBorneBy : costBorneBy in the format of String <br>"
			+ "<br> individualLicenseCost : individualLicenseCost  in the format of Double <br>"
			+ "<br> totalCost : totalCost  in the format of Double <br>"
			+ "<br> buContribution : buContribution  in the format of Double <br>"
			+ "<br> supplierId : supplierId  in the format of Integer <br>"
			+ "<br> deliveryDate : deliveryDate  in the format of Date <br>"
			+ "<br> loggedInUser : loggedInUser  in the format of Integer <br>"
			+ "<br> assetRequestId : assetRequestId  in the format of Integer <br>"
			+ "<br> vendorQuoteReferenceNo : vendorQuoteReferenceNo  in the format of String <br>"	,
			
	nickname = "Purchase Management System Phase-2 Update Quote ", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Phase-2 Update Quote", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.PUT_UPDATE_QUOTE_FOR_PHASE_2, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> updateQuoteControllerForPhase2(
			@RequestBody @Valid UpdateQuoteInputBean updateQuoteInputBean)
			throws CommonCustomException
	{
		boolean successFlag = false;
		
		successFlag = assetQuoteService.updateQuotesBeforeRMApproval(updateQuoteInputBean);
		
		if(successFlag){
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Quotes updated successfuly"));
		}else{
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Quotes could not be updated"));
		}
	}
	//EOA by Shyam for phase 2 service
	
	//Add quote phase-2
	
	@ApiOperation(tags="Purchase",value = "Add Quotes.", httpMethod = "POST", 
			notes = "This Service has been implemented to add quotes "
			+ "<br><b>Inputs :</b><br> PurchaseDatAssetQuotesInputBean <br><br>",

	nickname = "Add Quotes", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response =PurchaseDatAssetQuotesInputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response =PurchaseDatAssetQuoteBO .class),
			@ApiResponse(code = 201, message = "Added Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value =PurchaseManagementURIConstants.PURCHASE_ADD_QUOTES_BY_SYS_ADMIN , method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> addQuotesBySysAdmin(
			@Valid @RequestBody PurchaseDatAssetQuotesInputBean quotesInputBean) throws  CommonCustomException
	{
		/*PurchaseDatAssetQuoteBO quoteBO=new PurchaseDatAssetQuoteBO();*/
		//PurcahseQuotesOutPutBean quoteOuputBean = new PurcahseQuotesOutPutBean(); 
		
		boolean quoteOuputBean =false;
		
		quoteOuputBean=assetQuoteService.addQuotesforRequestType2(quotesInputBean);
	        
		         
		
				//if (quoteOuputBean!=null)
		         if (!quoteOuputBean)
				{
					LOG.endUsecase("Add quotes Controller...Couldnt add quotes");
					return ResponseEntity.status(HttpStatus.OK.value()).body(
							new MISResponse(HttpStatus.OK.value(),
									MISConstants.SUCCESS,
									"Quotes could not be added"));
				}
				else
				{
					LOG.endUsecase("Add quotes Controller...Quotes Added ");
					return ResponseEntity.status(HttpStatus.OK.value())
							.body(new MISResponse(HttpStatus.OK.value(),
									MISConstants.SUCCESS, "Quotes added successfuly"));
					
				}	
				
		
			}
	
	
	
}

