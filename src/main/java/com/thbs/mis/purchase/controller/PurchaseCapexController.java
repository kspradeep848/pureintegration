package com.thbs.mis.purchase.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.httpclient.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.PurchaseAssetRequestDetailOutputBean;
import com.thbs.mis.purchase.bean.PurchaseCapexDetailsOutputBean;
import com.thbs.mis.purchase.bean.PurchaseCapexOutputBean;
import com.thbs.mis.purchase.bean.PurchaseGetCapexRequestBean;
import com.thbs.mis.purchase.bean.PurchaseInputBean;
import com.thbs.mis.purchase.bean.PurchasePOCreateBean;
import com.thbs.mis.purchase.bean.PurchasePOCreatePhase2Bean;
import com.thbs.mis.purchase.bean.PurchaseRejectCapexInputBean;
import com.thbs.mis.purchase.bean.PurchaseUpdateCapexInputBean;
import com.thbs.mis.purchase.bean.PurchaseViewCapexRequestsByDateRangeAndBuIdInputBean;
import com.thbs.mis.purchase.bean.PurchaseViewCapexRequestsByDateRangeAndBuIdOutputBean;
import com.thbs.mis.purchase.bean.ViewAllAssetRequestBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;
import com.thbs.mis.purchase.constants.PurchaseManagementURIConstants;
import com.thbs.mis.purchase.service.PurchaseCapexService;

@Controller
public class PurchaseCapexController {

	private static final AppLog LOG = LogFactory
			.getLog(PurchaseCapexController.class);

	@Autowired
	private PurchaseCapexService capexService;
	
	
	// Added by Anil
	
	@RequestMapping(value=PurchaseManagementURIConstants.CREATE_CAPEX, method=RequestMethod.POST)
	public ResponseEntity<MISResponse> createCapex(@RequestBody PurchasePOCreateBean capexBean) throws DataAccessException{
		LOG.entering("createCapex");
		
		Boolean success = false;
		
		try{
			
			success = capexService.createCapexService(capexBean);
			
		}catch(Exception e){
			throw new DataAccessException(e.getMessage());
		}
		LOG.exiting("createCapex");
		if (success == true) {
			
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Capex created successfully"
							));
		} else {
		
			return ResponseEntity
					.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"capex not created"));
		}
		
		
	}

	@RequestMapping(value = PurchaseManagementURIConstants.GET_ALL_CAPEX_REQUESTS, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> getAllCapexRequests(
			@RequestBody ViewAllAssetRequestBean capexReq)
			throws DataAccessException {
		LOG.entering("getAllCapexRequests");
		List<PurchaseCapexOutputBean> listOfCapexRequest = new ArrayList<PurchaseCapexOutputBean>();
		try {
			
			listOfCapexRequest = capexService.getAllCapexRequests(capexReq);
			
		} catch (Exception e) {

			throw new DataAccessException(
					"Error while getting getAllAssetRequests", e);

		}
		LOG.exiting("getAllAssetRequests");
		if (listOfCapexRequest.size() > 0) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"All capex requests retrieved successfully",
							listOfCapexRequest));
		} else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE, "No date for given input"));
		}

	}

	@RequestMapping(value = PurchaseManagementURIConstants.GET_CAPEX_REQUEST, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getCapexRequests(
			@PathVariable Integer assetReqId) throws DataAccessException {
		LOG.entering("getCapexRequests : ");
		PurchaseGetCapexRequestBean capexRequestBean = new PurchaseGetCapexRequestBean();

		List<PurchaseGetCapexRequestBean> listOfCapexRequestBean = new ArrayList<PurchaseGetCapexRequestBean>();
		try {
			
			capexRequestBean = capexService.getCapexRequests(assetReqId);
			listOfCapexRequestBean.add(capexRequestBean);
			
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.exiting("getCapexRequests");
		if (listOfCapexRequestBean.size() > 0) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"capex requests retrieved successfully",
							listOfCapexRequestBean));
		} else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE, "No date for given input"));
		}

	}
	
	@RequestMapping(value=PurchaseManagementURIConstants.DOWNLOAD_CAPEX, method=RequestMethod.GET )
	public ResponseEntity<MISResponse> downloadCapex(@PathVariable Integer assetId,HttpServletResponse response) throws DataAccessException{
		LOG.entering("download capex");
		Boolean isDownloaded = false;
		try{
			isDownloaded = capexService.downloadCapex(assetId,response);
		}catch(Exception e){

			throw new DataAccessException(e.getMessage());
		}
		LOG.exiting("downloadPo");
		if (isDownloaded) {
			
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Capex file downloaded"
							));
		} else {
		
			return ResponseEntity
					.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"No file for given input"));
		}
		
		
	}
	
	// EOA by Anil

	// Added by Pratibha TR

	/**
	 * 
	 * @param assetRequestId
	 * @return PurchaseDatAssetRequestBO
	 * @throws BusinessException
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to approve capex.
	 */
	@ApiOperation(tags = "Purchase", value = "Approve capex"
			+ "This service will be called from the front-end inorder to approve the capex", httpMethod = "PUT", notes = "This Service has been implemented to approve capex."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)purchase_dat_asset_request -> It contains details about the requested asset. <br>"
			+ "2)purchase_dat_asset_quotes -> It contains details about the asset quotes.<br>"
			+ "3)mas_bu_unit -> It contains details about the bu's of the employees.<br>"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseInputBean <br><br>"
			+ "<br> id : pkPurchaseRequestId  in the format of integer <br>"
			+ "<br> loggedInUser : loggedInUser  in the format of integer <br>"
			, nickname = "Cancel asset request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseDatAssetRequestBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseDatAssetRequestBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Cancel asset request detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.APPROVE_CAPEX, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> approveCapex(
			@ApiParam(name = "Approve capex by BuHead ", example = "1", value = "{"
					+ "<br> id : 1, <br>"
					+ "<br> loggedInUser : 115, <br>" + "}")
			@Valid @RequestBody	PurchaseInputBean approveCaprxBean) throws BusinessException,
			CommonCustomException {
		LOG.startUsecase("Approve capex");
		//PurchaseApproveCapexOutputBean capexOutputBean = new PurchaseApproveCapexOutputBean();
		//List<PurchaseApproveCapexOutputBean> capexOutputBeanList = new ArrayList<PurchaseApproveCapexOutputBean>();
		boolean successFlag = false;
		
		successFlag = capexService.approveCapex(approveCaprxBean);
		
		LOG.endUsecase("Approve capex");
		if (successFlag) {
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"Capex is approved successfully."));
		}
		else {
			LOG.endUsecase("Approve capex");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No capex details found."));
		}
	}

	// EOA Pratibha TR

	// Added by Smrithi
	/**
	 * @return
	 * @throws CommonCustomException
	 * @Description: This method is used to get the capex details by asset
	 *               request id.
	 */
	@ApiOperation(tags = "Purchase Management System", value = " Get Capex Details based on asset request id."
			+ "This service will be called from the front-end when the BU head wants to view the capex details. ", httpMethod = "GET", notes = "This Service has been implemented to get the capex details based on asset request id. "
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) purchase_dat_asset_request -> It contains all asset request details <br>"
			+ "2) purchase_mas_asset_detail ->  It contains all asset details <br>"
			+ "3)purchase_dat_asset_quotes ->  It contains all asset quotes details <br>"
			+ "4)mas_bu_unit ->  It contains all the BU details <br>"
			+ "5) mas_location_parent ->  It contains all location details <br>"
			+ "6)purchase_dat_capex ->  It contains all purchase capex details  <br>"
			+ "<br>"
			+ "<br><b>Path variable :</b><br>"
			+ "<br> pkPurchaseRequestId : pkPurchaseRequestId  in the format of integer <br>",

	nickname = "Purchase Management System Get Capex Details based on asset request id.", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get Capex Details based on asset request id.", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.GET_CAPEX_DETAILS_BASED_ON_ASSET_REQUEST_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getCapexDetails(
			@ApiParam(name = "Purchase Management System Get Capex Details based on asset request id.", example = "1", value = "{"
					+ "<br> pkPurchaseRequestId : 1, <br>" + "}") @PathVariable("pkPurchaseRequestId") Integer pkPurchaseRequestId)
			throws CommonCustomException {

		List<PurchaseCapexDetailsOutputBean> capexDetailsList = new ArrayList<PurchaseCapexDetailsOutputBean>();
		PurchaseCapexDetailsOutputBean capexDetails = capexService
				.getCapexDetails(pkPurchaseRequestId);

		capexDetailsList.add(capexDetails);

		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						" Capex details is displayed.", capexDetailsList));
	}

	/**
	 * @param PurchaseRejectCapexInputBean
	 * @return
	 * @throws CommonCustomException
	 * @Description: This method is used to reject capex records by BU Head.
	 */
	@ApiOperation(tags = "Purchase Management System", value = " Reject Capex Details By BU Head."
			+ "This service will be called from the front-end when the reporting manager wants to reject the asset request ", httpMethod = "PUT", notes = "This Service has been implemented to reject asset request record based on RejectAssetRequestInputBean "
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) purchase_dat_asset_request -> It contains all asset request details"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseAssetRequestUpdateInputBean <br><br>"
			+ "<br> pkPurchaseRequestId : pkPurchaseRequestId  in the format of integer <br>"
			+ "<br> requestorId : requestorId  in the format of integer <br>"
			+ "<br> closureComment : closureComment in the format of String <br>",

	nickname = "Purchase Management System Reject Capex ", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Reject Capex ", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.REJECT_CAPEX, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> rejectCapex(
			@ApiParam(name = "Purchase Management System Reject Capex ", example = "1", value = "{"
					+ "<br> pkPurchaseRequestId : 1, <br>"
					+ "<br> requestorId : 1, <br>"
					+ "<br> closureComment : Invalid request,  <br>" + "}") @Valid @RequestBody PurchaseRejectCapexInputBean purchaseRejectCapexInputBean)
			throws CommonCustomException 
	{
		List<PurchaseAssetRequestDetailOutputBean> outputList = new ArrayList<PurchaseAssetRequestDetailOutputBean>();
		PurchaseAssetRequestDetailOutputBean rejectCapexOutput = new PurchaseAssetRequestDetailOutputBean();
		rejectCapexOutput = capexService
				.rejectCapex(purchaseRejectCapexInputBean);

		if(rejectCapexOutput != null)
			outputList.add(rejectCapexOutput);
		
		if(!outputList.isEmpty())
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						" Capex is rejected successfully.", outputList));
		}else{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"No Capex Found", outputList));
		}
	}
	
	/**
	 * @return
	 * @throws CommonCustomException
	 * @Description: This method is used to get the capex details by asset
	 *               request id.
	 */
	@ApiOperation(tags = "Purchase Management System", value = " View Capex requests based on date range and bu id."
			+ "This service will be called from the front-end when the BU head wants to view the capex requests based on date range and bu id. ", httpMethod = "GET", notes = "This Service has been implemented to get the capex requests based on date range and bu id. "
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) purchase_dat_asset_request -> It contains all asset request details <br>"
			+ "2) purchase_mas_asset_detail ->  It contains all asset details <br>"
			+ "3)purchase_dat_asset_quotes ->  It contains all asset quotes details <br>"
			+ "4)mas_bu_unit ->  It contains all the BU details <br>"
			+ "5) mas_location_parent ->  It contains all location details <br>"
			+ "6)purchase_dat_capex ->  It contains all purchase capex details  <br>"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseViewCapexRequestsByDateRangeAndBuIdInputBean <br><br>"
			+ "<br> loggedInUser : buId  in the format of integer <br>"
			+ "<br> startDate : startDate in the format of Date<br>"
			+ "<br> endDate : endDate in the format of Date<br>",

	nickname = "Purchase Management System  View Capex requests based on date range and bu id..", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = " View Capex requests based on date range and bu id.", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.POST_VIEW_ALL_CAPEX_REQUESTS_BY_DATE_RANGE_AND_BU_ID, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewCapexRequests(
			@ApiParam(name = "Purchase Management System Get Capex Details based on asset request id.", example = "1", value = "{"
			+ "<br> loggedInUser : 1, <br>"
			+ "<br> startDate : 01-01-2017<br>"
			+ "<br> endDate : 30-03-2017 <br>" + "}") @Valid @RequestBody 
			PurchaseViewCapexRequestsByDateRangeAndBuIdInputBean 
			purchaseViewCapexRequestsByDateRangeAndBuIdInputBean)
			throws CommonCustomException 
	{

		List<PurchaseViewCapexRequestsByDateRangeAndBuIdOutputBean> capexRequestsList = new ArrayList<PurchaseViewCapexRequestsByDateRangeAndBuIdOutputBean>();
		 capexRequestsList = capexService
				.viewCapexRequests(purchaseViewCapexRequestsByDateRangeAndBuIdInputBean);

	if(!capexRequestsList.isEmpty())
	{
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"Successfully retrieved capex details", capexRequestsList));

	}
	 else {
		LOG.endUsecase("Get asset request details by asset request ID");
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS,
						"No capex details found."));
	}

}

	
	// EOA by Smrithi
	
	//Added by Anil
	@RequestMapping(value = PurchaseManagementURIConstants.REJECT_CAPEX_SYSADMIN, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> rejectCapexBySysAdmin(
			@RequestBody PurchaseRejectCapexInputBean purchaseRejectCapexInputBean)
			throws CommonCustomException {

	try{
		capexService
				.rejectCapexBySysAdmin(purchaseRejectCapexInputBean);

			} catch (Exception e) {
				throw new CommonCustomException(e.getMessage());
			}

		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						" Capex is rejected successfully."));
	
				
	}
	//EOA by Anil
	
	//Added by Shyam
	/**
	 * @param PurchaseUpdateCapexInputBean
	 * @return
	 * @throws CommonCustomException
	 * @Description: This method is used to do update capex.
	 */
	@ApiOperation(tags = "Purchase Management System", value = "Update Capex By System Admin."
			+ "This service will be called from the front-end when System Admin wants to update the quote after creation of capex", httpMethod = "POST", 
			notes = "This Service has been implemented to update the existing capex in Nucleus."
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) purchase_dat_asset_request -> It contains all asset request details. <br>"
			+ "2) purchase_dat_asset_quotes -> It conatains all quotes details related to the asset request id. <br>"
			+ "3) purchase_dat_bu -> It contains BU head's capex approval records. <br>"
			+ "4) purchase_dat_capex -> It contains all capex details."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseUpdateCapexInputBean <br><br>"
			+ "<br> assetRequestId : assetRequestId  in the format of Integer <br>"
			+ "<br> loggedInUser : loggedInUser  in the format of Integer <br>"
			+ "<br> previousAgreedQuoteId : previousAgreedQuoteId in the format of Integer <br>"
			+ "<br> updatedAgreedQuoteId : updatedAgreedQuoteId in the format of Integer <br>"
			+ "<br> buContribution : buContribution  in the format of Double <br>"
			+ "<br> costBorneBy : costBorneBy  in the format of String <br>"
			+ "<br> deliveryDate : deliveryDate  in the format of Date <br>"
			+ "<br> individualLicenseCost : individualLicenseCost  in the format of Double <br>"
			+ "<br> totalCost : totalCost  in the format of Double <br>"
			+ "<br> supplierId : supplierId  in the format of Integer <br>"
			+ "<br> vendorQuoteReferenceNo : vendorQuoteReferenceNo  in the format of String <br>"	,
			
	nickname = "Purchase Management System Update Capex ", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Update Capex ", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.POST_UPDATE_CAPEX_BY_SYSTEM_ADMIN, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> updateCapexBySystemAdmin(
			@RequestBody PurchaseUpdateCapexInputBean updateCapexInputBean)
			throws CommonCustomException
	{
		boolean success = false;
		success = capexService.updateCapexBySystemAdmin(updateCapexInputBean);
		if (success) 
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"Capex updated successfully"));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"Capex updation failed."));
		}
	}
		//EOA by Shyam
	
	//Added by Shyam for phase-2 create capex service
	@RequestMapping(value= PurchaseManagementURIConstants.POST_CREATE_CAPEX_FOR_PHASE_2, 
			method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> createCapexForPhase2(
			@RequestParam(value = "updateAndApproveAssetRequestForSystemAdminBean", 
				name = "updateAndApproveAssetRequestForSystemAdminBean") 
			String updateAndApproveAssetRequestForSystemAdminBean,
			@RequestParam(value = "uploadSupportDocs", name = "uploadSupportDocs", required = false) 
			List<MultipartFile> uploadSupportDocs)
			throws CommonCustomException, DataAccessException
	{
		boolean success = false;
		ObjectMapper objectMapper = new ObjectMapper();
		PurchasePOCreatePhase2Bean inputBean = new PurchasePOCreatePhase2Bean();
		
		try {
			inputBean = objectMapper.readValue(updateAndApproveAssetRequestForSystemAdminBean,
					PurchasePOCreatePhase2Bean.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LOG.info("inputBean :: " + inputBean);
		
		try{
			success = capexService.createCapexServiceForPhase2(inputBean, uploadSupportDocs);
		}catch(Exception e){
			throw new DataAccessException(e.getMessage());
		}
		
		if (success == true) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS,"Capex created successfully"));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
				new MISResponse(HttpStatus.NOT_FOUND.value(),
					MISConstants.FAILURE,"Capex not created"));
		}
	}		
	//EOA by Shyam
	
}
