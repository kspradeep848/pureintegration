package com.thbs.mis.purchase.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.httpclient.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.PurchaseCancelPoBean;
import com.thbs.mis.purchase.bean.PurchaseInputBean;
import com.thbs.mis.purchase.bean.PurchaseOrderDetailsOutputBean;
import com.thbs.mis.purchase.bean.PurchaseOrderRequestOutputBean;
import com.thbs.mis.purchase.bean.PurchaseOrderRequestsBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;
import com.thbs.mis.purchase.constants.PurchaseManagementURIConstants;
import com.thbs.mis.purchase.service.PurchaseOrderService;

@Controller
public class PurchaseOrderController {
	private static final AppLog LOG = LogFactory
			.getLog(AssetRequestController.class);
	@Autowired
	PurchaseOrderService purchaseOrderService;

	/**
	 * 
	 * @param purchaseOrderRequestsBean
	 * @return List<PurchaseDatAssetRequestBO>
	 * @throws BusinessException
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to View Purchase Order
	 *               requests based on date range and status.
	 */
	@ApiOperation(tags = "Purchase", value = "View Purchase Order requests based on date range."
			+ "This service will be called from the front-end inorder to view Purchase Order requests based on date range.", httpMethod = "POST", notes = "This Service has been implemented to View Purchase Order requests based on date range."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)purchase_dat_asset_request -> It contains details about the requested asset."
			+ "<br><b>Inputs :</b><br> PurchaseOrderRequestsBean <br><br>"
			+ "<br> fromDate : fromDate in the format of date <br>"
			+ "<br> toDate : toDate in the format of date <br>"
			+ "<br>", nickname = "Cancel asset request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseOrderRequestOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseOrderRequestOutputBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of purchase order equest details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.VIEW_PURCHASE_ORDER_REQUESTS_BY_DATE_RANGE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewPurchaseOrderRequestsBasedOnDateRange(
			@Valid @ApiParam(name = "View Purchase Order requests based on date range.", example = "1", value = "{"
					+ "<br> \"fromDate\": \"2016-10-01\","
					+ "<br> \"toDate\": \"2016-12-31\"," + "<br> " + "}")
		 @RequestBody PurchaseOrderRequestsBean purchaseOrderRequestsBean)
			throws BusinessException, CommonCustomException {
		LOG.startUsecase("View Purchase Order requests based on date range and status");
		List<PurchaseOrderRequestOutputBean> requestOutputBeanList = new ArrayList<PurchaseOrderRequestOutputBean>();

		requestOutputBeanList = purchaseOrderService
				.viewPurchaseOrderRequestsBasedOnDateRange(purchaseOrderRequestsBean);

		if (!requestOutputBeanList.isEmpty()) {
			LOG.endUsecase("Get asset request details by asset request ID");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully retrieved purchase order details.",
							requestOutputBeanList));
		} else {
			LOG.endUsecase("Get asset request details by asset request ID");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"No purchase order details found."));
		}
	}
    
	@ApiOperation(tags = "Purchase", value = "Get purchase order details."
			+ "This service will be called from the front-end inorder to view the purchase order details based on purchaseRequestId", httpMethod = "GET", notes = "This Service has been implemented to View Purchase Order requests."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)purchase_dat_asset_request -> It contains details about the requested asset."
			+ "<br><b>Path variable :</b><br>"
			+ "<br> pkPurchaseRequestId : pkPurchaseRequestId  in the format of integer <br>", nickname = "Cancel asset request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseOrderDetailsOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseOrderDetailsOutputBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of purchase order equest details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.GET_PURCHASE_ORDER_DETAILS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getPurchaseOrderDetails(
			@ApiParam(name = "Get purchase order details based on asset request id.", example = "1", value = "{"
					+ "<br> pkPurchaseRequestId : 1, <br>" + "}") 
			@PathVariable int purchaseRequestId) throws BusinessException,
			CommonCustomException {
		LOG.startUsecase("Get Purchase Order Details");
		List<PurchaseOrderDetailsOutputBean> detailsOutputBeanList = new ArrayList<PurchaseOrderDetailsOutputBean>();
		detailsOutputBeanList = purchaseOrderService
				.getPurchaseOrderDetails(purchaseRequestId);
		if (!detailsOutputBeanList.isEmpty()) {
			LOG.endUsecase("Get Purchase Order Details");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Successfully retrieved purchase order details.",
							detailsOutputBeanList));
		} else {
			LOG.endUsecase("Get Purchase Order Details");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"No purchase order details found."));
		}
	}

	/**
	 * 
	 * @param purchaseRequestId
	 * @return PurchaseDatAssetRequestBO
	 * @throws BusinessException
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to approve PO.
	 */
	@ApiOperation(tags = "Purchase", value = "Approve PO"
			+ "This service will be called from the front-end inorder to approve the PO", httpMethod = "PUT", notes = "This Service has been implemented to approve PO."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)purchase_dat_asset_request -> It contains details about the requested asset.<br>"
			+ "2)dat_emp_detail -> It contains details about the employee.<br>"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseApprovePoBean <br><br>"
			+ "<br> purchaseRequestId : pkPurchaseRequestId  in the format of integer <br>"
			+ "<br> loggedInUser : loggedInUser  in the format of integer <br>",
			 nickname = "Approve purchase order request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseDatAssetRequestBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseDatAssetRequestBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Approve purchase oder request detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.APPROVE_PO, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> approvePO(
			@ApiParam(name = "Approve capex by BuHead ", example = "1", value = "{"
					+ "<br> purchaseRequestId : 1, <br>"
					+ "<br> loggedInUser : 115,  <br>" + "}")
			@Valid @RequestBody PurchaseInputBean approvePoBean) throws BusinessException,
			CommonCustomException {
		LOG.startUsecase("Approve PO");

		purchaseOrderService.approvePO(approvePoBean);

		LOG.endUsecase("Approve PO");
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"PO is approved successfully."));
	}

	/**
	 * 
	 * @param purchaseRequestId
	 * @return PurchaseDatAssetRequestBO
	 * @throws BusinessException
	 * @throws CommonCustomException
	 * @Description: This Service has been implemented to cancel PO.
	 */
	@ApiOperation(tags = "Purchase", value = "Cancel PO"
			+ "This service will be called from the front-end inorder to cancel PO", httpMethod = "PUT", notes = "This Service has been implemented to cancel PO."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)purchase_dat_asset_request -> It contains details about the requested asset."
			+ "2)dat_emp_detail -> It contains details about the employee.<br>"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> PurchaseCancelPoBean <br><br>"
			+ "<br> purchaseRequestId : pkPurchaseRequestId  in the format of integer <br>"
			+ "<br> loggedInUser : loggedInUser  in the format of integer <br>"
			+ "<br> financeExComment : loggedInUser  in the format of string <br>",
			nickname = "Cancel purchase order request", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = PurchaseDatAssetRequestBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = PurchaseDatAssetRequestBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Cancel purchase order request detail", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = PurchaseManagementURIConstants.CANCEL_PO, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> cancelPO(
			@ApiParam(name = "Approve capex by BuHead ", example = "1", value = "{"
					+ "<br> purchaseRequestId : 1, <br>"
					+ "<br> loggedInUser : 115,  <br>"
					+ "<br> financeExComment :Cancelled,  <br>" + "}")
			@Valid @RequestBody PurchaseCancelPoBean cancelPoBean) throws BusinessException,
			CommonCustomException {
		LOG.startUsecase("Cancel PO");

		purchaseOrderService.cancelPO(cancelPoBean);

		LOG.endUsecase("Cancel PO");
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"PO is cancelled successfully."));
	}
}
