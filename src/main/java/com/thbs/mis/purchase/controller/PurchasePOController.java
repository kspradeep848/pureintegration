package com.thbs.mis.purchase.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.PurchaseCancelPoBean;
import com.thbs.mis.purchase.bean.PurchaseCapexOutputBean;
import com.thbs.mis.purchase.bean.PurchaseOrderDetailsSaveBean;
import com.thbs.mis.purchase.bean.PurchaseOrderSysAdminOutputBean;
import com.thbs.mis.purchase.bean.PurchasePOCreateBean;
import com.thbs.mis.purchase.bean.PurchaseSavePoOutputBean;
import com.thbs.mis.purchase.bean.ViewAllAssetRequestBean;
import com.thbs.mis.purchase.constants.PurchaseManagementURIConstants;
import com.thbs.mis.purchase.service.PurchasePOService;

@Controller
public class PurchasePOController {

	private static final AppLog LOG = LogFactory
			.getLog(PurchasePOController.class);
	
	@Autowired
	private PurchasePOService poService;
	
	@RequestMapping(value=PurchaseManagementURIConstants.CREATE_PO, method=RequestMethod.POST,consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> createPo(
			@RequestParam(value = "file", name = "file", required = false) List<MultipartFile> files,
			@RequestParam(value = "poBean", name = "poBean") String poBean
			) throws DataAccessException{
		LOG.entering("createPo");
		
		Boolean success = false;
		ObjectMapper obj = new ObjectMapper();
		 PurchasePOCreateBean inputDate = new PurchasePOCreateBean();
		try{
			
			inputDate = obj.readValue(poBean,
					PurchasePOCreateBean.class);
			
			success = poService.createPoService(inputDate,files);
			
		}catch(Exception e){
			throw new DataAccessException(e.getMessage());
		}
		LOG.exiting("createPo");
		if (success == true) {
			
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Purchase order created successfully"
							));
		} else {
		
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE,
							"Purchase order not created"));
		}
		
		
	}
	
	@RequestMapping(value = PurchaseManagementURIConstants.GET_ALL_PO, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> getAllPurchaseOrder(@RequestBody ViewAllAssetRequestBean poReq) throws DataAccessException{
		LOG.entering("getAllPurchaseOrder");
		List<PurchaseCapexOutputBean> listOfPo = new ArrayList<PurchaseCapexOutputBean>();
		try{
			listOfPo = poService.getAllPORequests(poReq);
		}catch(Exception e){
			throw new DataAccessException(
					"Error while getting getAllPurchaseOrder", e);
		}
		LOG.exiting("getAllAssetRequests");
		if (listOfPo.size() > 0) {
			
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"PO retrieved successfully",listOfPo
							));
		} else {
		
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"No date for given input"));
		}
	}
	
	@RequestMapping(value=PurchaseManagementURIConstants.DOWNLOAD_PO, method=RequestMethod.GET )
	public ResponseEntity<MISResponse> downloadPo(@PathVariable Integer assetId,HttpServletResponse response) throws DataAccessException{
		LOG.entering("downloadPo");
		Boolean isDownloaded = false;
		try{
			isDownloaded = poService.downloadPo(assetId,response);
		}catch(Exception e){

			throw new DataAccessException(e.getMessage());
		}
		LOG.exiting("downloadPo");
		if (isDownloaded) {
			
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"PO file downloaded"
							));
		} else {
		
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE,
							"No file for given input"));
		}
		
		
	}
	
	@RequestMapping(value=PurchaseManagementURIConstants.DOWNLOAD_SUPPORTING_DOC_SYSADMIN, method=RequestMethod.GET )
	public ResponseEntity<MISResponse> downloadSupportingDocsBySysAdmin(@PathVariable Integer assetId,HttpServletResponse response) throws DataAccessException{
		LOG.entering("download supporting docs");
		Boolean isDownloaded = false;
		try{
			isDownloaded = poService.downloadSupportingDocsBySysAdmin(assetId,response);
		}catch(Exception e){

			throw new DataAccessException(e.getMessage());
		}
		LOG.exiting("download supporting docs");
		if (isDownloaded) {
			
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"supporting docs downloaded"
							));
		} else {
		
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE,
							"No file for given input"));
		}
		
		
	}
	
	
	
	@RequestMapping(value=PurchaseManagementURIConstants.DOWNLOAD_SUPPORTING_DOC_EMP, method=RequestMethod.GET )
	public ResponseEntity<MISResponse> downloadSupportingDocsByEmpOrRm(@PathVariable Integer assetId,HttpServletResponse response) throws DataAccessException{
		LOG.entering("download supporting docs");
		Boolean isDownloaded = false;
		try{
			isDownloaded = poService.downloadSupportingDocsByEmployeeOrRm(assetId,response);
		}catch(Exception e){

			throw new DataAccessException(e.getMessage());
		}
		LOG.exiting("download supporting docs");
		if (isDownloaded) {
			
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"supporting docs downloaded"
							));
		} else {
		
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE,
							"No file for given input"));
		}
		
		
	}
	@RequestMapping(value=PurchaseManagementURIConstants.SAVE_PURCHASE_DETAILS, method=RequestMethod.POST,consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> savePurchaseOrderDetails(
			@RequestParam(value = "file", name = "file", required = false) List<MultipartFile> files,
			@RequestParam(value = "poBean", name = "poBean")String poBean
			) throws DataAccessException{
		LOG.entering("Save purchase order details");
		PurchaseSavePoOutputBean saveOutputBean = new PurchaseSavePoOutputBean();
		List<PurchaseSavePoOutputBean> listOfSavePoBean = new ArrayList<PurchaseSavePoOutputBean>();
		
		ObjectMapper obj = new ObjectMapper();
		PurchaseOrderDetailsSaveBean savePoDetails = new PurchaseOrderDetailsSaveBean();
		try{
			savePoDetails = obj.readValue(poBean,
					PurchaseOrderDetailsSaveBean.class);
			
			saveOutputBean = poService.savePurchaseOrderDetails(savePoDetails,files);
			listOfSavePoBean.add(saveOutputBean);
		}catch(Exception e){

			throw new DataAccessException(e.getMessage());
		}
		LOG.exiting("Save purchase order details");
		
		if(listOfSavePoBean.size() > 0){
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"purchase order details Saved Successfully",listOfSavePoBean));
		}else{
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE,
							"purchase order details not saved"));
		}
	
		
		
	}
	
	@RequestMapping(value = PurchaseManagementURIConstants.CANCEL_PO_SYSADMIN, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> cancelPOBySysAdmin(
			@Valid @RequestBody PurchaseCancelPoBean cancelPoBean) throws BusinessException,
			CommonCustomException {
		LOG.startUsecase("Cancel PO by sys addmin");

		try{
		poService.cancelPOBySysAdmin(cancelPoBean);

		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("Cancel PO by sys admin");
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"PO is cancelled successfully."));
	}
	
	@RequestMapping(value = PurchaseManagementURIConstants.GET_PURCHASE_ORDER_DETAILS_SYSADMIN, method = RequestMethod.GET)
 	public @ResponseBody ResponseEntity<MISResponse> getPurchaseOrderDetails(
 			@PathVariable Integer purchaseRequestId) throws BusinessException,
 			CommonCustomException, DataAccessException {
 		LOG.startUsecase("Get Purchase Order Details");
 		PurchaseOrderSysAdminOutputBean detailsOutputBean = new
 				PurchaseOrderSysAdminOutputBean();
 		List<PurchaseOrderSysAdminOutputBean> detailsOutputBeanList = new ArrayList<PurchaseOrderSysAdminOutputBean>();
 		try{
 		detailsOutputBeanList = new ArrayList<PurchaseOrderSysAdminOutputBean>();
 		detailsOutputBean = poService
 				.getPurchaseOrderDetailsSysAdmin(purchaseRequestId);
 		detailsOutputBeanList.add(detailsOutputBean);
 		}catch(Exception e){
 			throw new DataAccessException(e.getMessage());
 		}
 		if (!detailsOutputBeanList.isEmpty()) {
 			LOG.endUsecase("Get Purchase Order Details");
 			return ResponseEntity.status(HttpStatus.OK.value()).body(
 					new MISResponse(HttpStatus.OK.value(),
 							MISConstants.SUCCESS,
 							"Successfully retrieved purchase order details.",
 							detailsOutputBeanList));
 		} else {
 			LOG.endUsecase("Get Purchase Order Details");
 			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
 					new MISResponse(HttpStatus.NOT_FOUND.value(),
 							MISConstants.FAILURE,
 							"No purchase order details found."));
 		}
 	}
 	
	@RequestMapping(value=PurchaseManagementURIConstants.DOWNLOAD_INVOICE, method=RequestMethod.GET )
	public ResponseEntity<MISResponse> downloadInvoice(@PathVariable Integer assetId,HttpServletResponse response) throws DataAccessException{
		LOG.entering("download Invoice docs");
		Boolean isDownloaded = false;
		try{
			isDownloaded = poService.downloadInvoice(assetId,response);
		}catch(Exception e){

			throw new DataAccessException(e.getMessage());
		}
		LOG.exiting("download Invoice docs");
		if (isDownloaded) {
			
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Invoice docs downloaded"
							));
		} else {
		
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE,
							"No file for given input"));
		}
		
		
	}
}
