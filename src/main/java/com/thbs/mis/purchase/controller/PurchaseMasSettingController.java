package com.thbs.mis.purchase.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.purchase.bean.PurchaseMasSettingBean;
import com.thbs.mis.purchase.bean.SysAdminSettingsOutputBean;
import com.thbs.mis.purchase.constants.PurchaseManagementURIConstants;
import com.thbs.mis.purchase.service.PurchaseMasSettingService;

@Controller
public class PurchaseMasSettingController {
	private static final AppLog LOG = LogFactory
			.getLog(PurchaseMasSettingController.class);

	@Autowired
	private PurchaseMasSettingService purchaseMasSettingService;

	@RequestMapping(value = PurchaseManagementURIConstants.ADD_ACESS_BY_SYS_ADMIN, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> addAccessBySysAdmin(
			@RequestBody PurchaseMasSettingBean purchaseMasSettingBean)
			throws CommonCustomException
	{
		boolean accessSuccessFlag = false;
		try {
			accessSuccessFlag = purchaseMasSettingService.addAccessBySysAdmin(purchaseMasSettingBean);
		} catch (Exception e) {
			throw new CommonCustomException("Exception thrown from addAccessBySysAdmin controller");
		}
		
		if (accessSuccessFlag) 
		{
			LOG.endUsecase("addAccessBySysAdmin");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,	"Access provided successfully."));
		} else {
			LOG.endUsecase("addAccessBySysAdmin");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Failed to provide access to selected employee."));
		}

	}

	@RequestMapping(value = PurchaseManagementURIConstants.ADD_ACESS_BY_SYS_ADMIN, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> updateAccessBySysAdmin(
			@RequestBody PurchaseMasSettingBean purchaseMasSettingBean)
			throws BusinessException, ParseException

	{
		boolean accessRemoveSuccessFlag = false;
		try {
			accessRemoveSuccessFlag = purchaseMasSettingService.updateAccessBySysAdmin(purchaseMasSettingBean);

		} catch (Exception e) {
			throw new BusinessException(
					"Exception thrown from addAccessBySysAdmin controller");
		}
		if (accessRemoveSuccessFlag) 
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Access removed successfully."));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Failed to remove access of selected employee."));
		}

	}

	@RequestMapping(value = PurchaseManagementURIConstants.GET_SYSTEM_ADMINS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getSystemAdminEmpIds()
			throws BusinessException, ParseException

	{
		LOG.startUsecase("getSystemAdminEmpIds");
		List<SysAdminSettingsOutputBean> purchaseMasSettingBeanList = new ArrayList<SysAdminSettingsOutputBean>();

		try {

			purchaseMasSettingBeanList = purchaseMasSettingService
					.getSystemAdminEmpIds();

		} catch (Exception e) {
			throw new BusinessException(
					"Exception thrown from addAccessBySysAdmin controller");
		}
		if (purchaseMasSettingBeanList.size() > 0) {
			LOG.endUsecase("getSystemAdminEmpIds");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Retrieving the sysAdmins list.",
							purchaseMasSettingBeanList));
		} else {
			LOG.endUsecase("getSystemAdminEmpIds");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Retrieving the sysAdmins list.",
							purchaseMasSettingBeanList));
		}

	}
	
	
}
