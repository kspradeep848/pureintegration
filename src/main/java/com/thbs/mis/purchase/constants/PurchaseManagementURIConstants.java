package com.thbs.mis.purchase.constants;

public class PurchaseManagementURIConstants {

	/* Please follow alphabetical order of uri */
	public static final String ADD_ACESS_BY_SYS_ADMIN = "/purchase/sysadmin/settings/createorupdate";
	public static final String GET_SYSTEM_ADMINS = "/purchase/sysadmin/empids";
	public static final String GET_ALL_ASSET_REQUESTS = "/purchase/sysadmin/assetrequest";

	// Added by Pratibha TR
	public static final String GET_ALL_ASSET_DETAILS = "/purchase/systemAdmin/asset/details";
	public static final String GET_ASSET_AND_ITS_DETAIL_FROM_ASSET_TYPE_FOR_EMPLOYEE = "/purchase/emp/assetRequest/assetDetails/{assetType}";
	public static final String GET_ASSET_AND_ITS_DETAIL_FROM_ASSET_TYPE_FOR_SYSTEM_ADMIN = "/purchase/systemAdmin/assetRequest/assetDetails/{assetType}";
	public static final String GET_ASSET_REQUEST_DETAILS_FROM_ASSET_REQUEST_ID = "/purchase/emp/assetRequest/assetRequestDetail/{assetRequestId}";
	public static final String GET_ASSET_DETAIL_FROM_ASSET_ID = "/purchase/systemAdmin/assetRequestdetail/{assetId}";
	public static final String CANCEL_ASSET_REQUEST = "/purchase/emp/assetRequest/cancel/{assetRequestId}";
	public static final String UPDATE_AND_APPROVE_ASSET_REQUEST = "/purchase/rm/assetRequest/updateAndApprove";
	public static final String APPROVE_CAPEX = "/purchase/BuHead/capex/approve";
	public static final String VIEW_PURCHASE_ORDER_REQUESTS_BY_DATE_RANGE = "/purchase/finance/orderRequests/details";
	public static final String GET_PURCHASE_ORDER_DETAILS = "/purchase/finance/order/details/{purchaseRequestId}";
	public static final String APPROVE_PO = "/purchase/finance/order/approve";
	public static final String CANCEL_PO = "/purchase/finance/order/cancel";
	public static final String DELETE_SOFTWARE = "/purchase/sysAdmin/software/delete";
	public static final String DELETE_VENDOR = "/purchase/sysAdmin/vendor/delete";
	public static final String ADD_NEW_ASSET_REQUEST_AS_SYSTEM_ADMIN = "/purchase/systemSdmin/assetRequest/create";
	public static final String UPDATE_NEW_ASSET_REQUEST_AS_SYSTEM_ADMIN = "/purchase/systemSdmin/assetRequest/update";
	public static final String UPDATE_AND_APPROVE_ASSET_REQUEST_FOR_SYSTEM_ADMIN = "/purchase/assetRequest/updateAndApprove";
	public static final String ADD_VENDOR_DETAILS_FOR_EXISTING_ASSET = "/purchase/asset/addVendorDetails";
	// EOA by Pratibha TR
	
	// added by Smrithi
	public static final String POST_CREATE_NEW_ASSET_REQUEST = "/purchase/assetRequest/create";
	public static final String REJECT_ASSET_REQUEST = "/purchase/assetRequest/reject";
	public static final String UPDATE_ASSET_REQUEST = "/purchase/assetRequest/update";
	public static final String GET_CAPEX_DETAILS_BASED_ON_ASSET_REQUEST_ID = "/purchase/capexDetails/get/{pkPurchaseRequestId}";
	public static final String REJECT_CAPEX = "/purchase/capex/reject";
	public static final String ADD_VENDOR_DETAILS = "/purchase/vendorDetails/add";
	public static final String UPDATE_VENDOR_DETAILS = "/purchase/vendorDetails/update";
	public static final String POST_VIEW_ALL_ASSET_REQUEST_BY_DATE_RANGE_OR_REQUESTOR_ID = "/purchase/assetRequest/view";
	public static final String POST_VIEW_ALL_CAPEX_REQUESTS_BY_DATE_RANGE_AND_BU_ID = "/purchase/capexRequest/dateRange/buId/view";
	public static final String POST_FETCH_ASSESTS_BASED_ON_ASSET_TYPE_AND_LICENSE_TYPE = "/purchase/assets/fetch/assetType/licenseType";
	// EOA by Smrithi

	// Added by Shyam

	public static final String POST_VIEW_ALL_ASSET_REQUEST_UNDER_EMP_LOGIN = "/purchase/assetRequest/view/emp";
	public static final String POST_VIEW_ALL_ASSET_REQUEST_UNDER_REPORTING_MGR_LOGIN = "/purchase/assetRequest/view/mgr";
	public static final String POST_UPDATE_CAPEX_BY_SYSTEM_ADMIN = "/purchase/capex/update";
	public static final String PUT_UPDATE_QUOTE_FOR_PHASE_2 = "/purchase/sysAdmin/quote";
	public static final String POST_CREATE_CAPEX_FOR_PHASE_2 = "/purchase/phase2/capex/create";
	public static final String GET_EXISTING_SUBSCRIPTION_ID_FROM_ASSET_REQUEST = "/purchase/phase2/subscriptionId/{requestId}";
	// EOA by Shyam

	// Added by Prathibha on 01/09/2017

	public static final String FETCH_ASSETS = "/purchase/fetchassest/{assetId}";

	public static final String GET_ASSETS = "/purchase/getassetdetails/{subscriptionId}";

	public static final String PURCHASE_ADD_QUOTES = "/purchase/sysadmin/addquotes";

	public static final String FETCH_ASSETS_QUOTES = "/purcahse/fetchquotes/byRequestId/{asstRequestId}";

	public static final String GET_SUPPLIER_DETAILS = "/purchase/sysadmin/supplierdetails/{assetId}";

	public static final String UPDATE_QUOTES = "/purchase/sysadmin/updatequotes";

	public static final String GET_SUPPLIER_QUOTE_DETAILS_BY_ASSETID = "/purchase/sysadmin/supplier/quotedetails/{assetId}";

	public static final String GET_SUPPLIER_QUOTE_DETAILS_BY_PURREQID = "/purchase/sysadmin/supplier/quotedetails/byrequestId/{purchaseReqId}";

	public static final String SYS_ADMIN_REJECT_ASSET_REQ = "/purchase/sysadmin/reject/assetrequest";
	
	public static final String GET_ASSET_DETAILS_WITH_QUOTE_LIST ="/purchase/fecth/assetdetails/byrequestID/history/{purchaseRequestId}";
	
	public static final String ADD_QUOTE_UPDATE_VENDOR_DETAILS="/purchase/sysadmin/addquote/updatevendor";
	
	public static final String PURCHASE_ADD_QUOTES_BY_SYS_ADMIN = "/purchase/sysadmin/addquote/bysysadmin";

	// End of Added by Prathibha on 01/09/2017

	// Added by Anil
	public static final String DOWNLOAD_PO = "/purchase/sysadmin/download/po/{assetId}";

	public static final String GET_ALL_CAPEX_REQUESTS = "/purchase/sysadmin/viewcapex";

	public static final String GET_CAPEX_REQUEST = "/purchase/sysadmin/capex/{assetReqId}";

	public static final String GET_ASSET_REQUESTS = "/purchase/sysadmin/assetrequest/{assetReqId}";

	public static final String GET_ALL_PO = "/purchase/sysadmin/viewpo";

	public static final String DOWNLOAD_CAPEX = "/purchase/sysadmin/download/capex/{assetId}";

	public static final String DOWNLOAD_SUPPORTING_DOC_SYSADMIN = "/purchase/sysadmin/download/supportingdoc/{assetId}";

	public static final String CREATE_PO = "/purchase/sysadmin/po";

	public static final String CREATE_CAPEX = "/purchase/sysadmin/capex";
	
	public static final String SAVE_PURCHASE_DETAILS = "/purchase/sysadmin/podetails";
	
	public static final String REJECT_CAPEX_SYSADMIN = "/purchase/sysadmin/capex/reject";
	
	public static final String CANCEL_PO_SYSADMIN = "/purchase/sysadmin/po/reject";
	
	public static final String GET_PURCHASE_ORDER_DETAILS_SYSADMIN = "/purchase/sysadmin/po/details/{purchaseRequestId}";
	
	public static final String DOWNLOAD_INVOICE = "/purchase/sysadmin/download/invoice/{assetId}";
	
	public static final String DOWNLOAD_SUPPORTING_DOC_EMP = "/purchase/emp/download/supportingdoc/{assetId}";
	
	public static final String SYS_ADMIN_REJECT_ASSET_REQ_PHASETWO = "/purchase/sysadmin/reject/phasetwo/assetrequest";

	// EOA by Anil

}
