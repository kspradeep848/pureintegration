package com.thbs.mis.purchase.bean;

import java.util.List;

public class CompAndClientDetailsOutputBean {
	private List<ProjectAllocatedToEmpOutputBean> outputBean;

	public List<ProjectAllocatedToEmpOutputBean> getOutputBean() {
		return outputBean;
	}

	public void setOutputBean(List<ProjectAllocatedToEmpOutputBean> outputBean) {
		this.outputBean = outputBean;
	}

	@Override
	public String toString() {
		return "CompAndClientDetailsOutputBean [outputBean=" + outputBean + "]";
	}
}
