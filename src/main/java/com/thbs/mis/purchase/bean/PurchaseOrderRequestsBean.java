package com.thbs.mis.purchase.bean;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;

public class PurchaseOrderRequestsBean {
	
	@JsonSerialize(using = DateSerializer.class)
	@NotNull(message = "fromDate field can not be null.")
	private Date fromDate;	
	
	@JsonSerialize(using = DateSerializer.class)
	@NotNull(message = "toDate field can not be null.")
	private Date toDate;
	
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	@Override
	public String toString() {
		return "PurchaseOrderRequestsBean [fromDate=" + fromDate + ", toDate="
				+ toDate + "]";
	}
}	