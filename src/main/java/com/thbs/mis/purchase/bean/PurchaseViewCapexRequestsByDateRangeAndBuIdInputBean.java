package com.thbs.mis.purchase.bean;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;


public class PurchaseViewCapexRequestsByDateRangeAndBuIdInputBean {
	
	@JsonSerialize(using = DateSerializer.class)
	@NotNull(message = "assetRequiredDate field can not be null.")
	private Date startDate;
	
	@JsonSerialize(using = DateSerializer.class)
	@NotNull(message = "assetRequiredDate field can not be null.")
	private Date endDate;
	
	@Employee_Id_Not_Found
	@NumberFormat(style = Style.NUMBER, pattern = "loggedInUser should be numeric value")
	@Min(1)
	@NotNull(message = "loggedInUser field can not be null")
	private Integer loggedInUser;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Integer loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	@Override
	public String toString() {
		return "PurchaseViewCapexRequestsByDateRangeAndBuIdInputBean [startDate="
				+ startDate
				+ ", endDate="
				+ endDate
				+ ", loggedInUser="
				+ loggedInUser + "]";
	}
    
}
