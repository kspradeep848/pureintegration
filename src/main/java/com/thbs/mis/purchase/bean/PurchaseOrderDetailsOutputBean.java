package com.thbs.mis.purchase.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseOrderDetailsOutputBean {
	
	private Integer pkPurchaseRequestId;
	private String capexNumber;
	private String poNumber;
	private String castCenter;
	private String projectName;
	private String softwareTitleAndVersion;
	private String assetType;
	private String location;
	private String isClientReimbursing;
	private String purpose;
	private Integer purchaseRequestStatus;
	private String purchaseRequestStatusName;
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date assetRequiredBy;
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date assetModifiedDate;
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date assetCreatedDate;
	private Boolean isSupportingDocsAvailable;
	private Boolean isPoAvailable;
	private Boolean isCapexAvailable;
	private Boolean isInvoiceAvailable;
    private String requestorName;
	private String reportingManager;
	private String sysAdminName;
	private String financeHeadName;
	private String buHeadName;
	private String itHeadName;
	private String rmComment;
	private String sysAdminComment;
	private String financeExComment;
	private String buHeadComment;
	private List<QuoteDetailsForPOApprovalBean> quoteList;
	
	public String getCapexNumber() {
		return capexNumber;
	}
	public void setCapexNumber(String capexNumber) {
		this.capexNumber = capexNumber;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getCastCenter() {
		return castCenter;
	}
	public void setCastCenter(String castCenter) {
		this.castCenter = castCenter;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getSoftwareTitleAndVersion() {
		return softwareTitleAndVersion;
	}
	public void setSoftwareTitleAndVersion(String softwareTitleAndVersion) {
		this.softwareTitleAndVersion = softwareTitleAndVersion;
	}
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}
	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public List<QuoteDetailsForPOApprovalBean> getQuoteList() {
		return quoteList;
	}
	public void setQuoteList(List<QuoteDetailsForPOApprovalBean> quoteList) {
		this.quoteList = quoteList;
	}
	
	public Integer getPurchaseRequestStatus() {
		return purchaseRequestStatus;
	}
	public void setPurchaseRequestStatus(Integer purchaseRequestStatus) {
		this.purchaseRequestStatus = purchaseRequestStatus;
	}
	public String getPurchaseRequestStatusName() {
		return purchaseRequestStatusName;
	}
	public void setPurchaseRequestStatusName(String purchaseRequestStatusName) {
		this.purchaseRequestStatusName = purchaseRequestStatusName;
	}
	public Date getAssetRequiredBy() {
		return assetRequiredBy;
	}
	public void setAssetRequiredBy(Date assetRequiredBy) {
		this.assetRequiredBy = assetRequiredBy;
	}
	public Date getAssetModifiedDate() {
		return assetModifiedDate;
	}
	public void setAssetModifiedDate(Date assetModifiedDate) {
		this.assetModifiedDate = assetModifiedDate;
	}
	public Date getAssetCreatedDate() {
		return assetCreatedDate;
	}
	public void setAssetCreatedDate(Date assetCreatedDate) {
		this.assetCreatedDate = assetCreatedDate;
	}
	public Integer getPkPurchaseRequestId() {
		return pkPurchaseRequestId;
	}
	public void setPkPurchaseRequestId(Integer pkPurchaseRequestId) {
		this.pkPurchaseRequestId = pkPurchaseRequestId;
	}
	
	public Boolean getIsSupportingDocsAvailable() {
		return isSupportingDocsAvailable;
	}
	public void setIsSupportingDocsAvailable(Boolean isSupportingDocsAvailable) {
		this.isSupportingDocsAvailable = isSupportingDocsAvailable;
	}
	public Boolean getIsPoAvailable() {
		return isPoAvailable;
	}
	public void setIsPoAvailable(Boolean isPoAvailable) {
		this.isPoAvailable = isPoAvailable;
	}
	public Boolean getIsCapexAvailable() {
		return isCapexAvailable;
	}
	public void setIsCapexAvailable(Boolean isCapexAvailable) {
		this.isCapexAvailable = isCapexAvailable;
	}
	public Boolean getIsInvoiceAvailable() {
		return isInvoiceAvailable;
	}
	public void setIsInvoiceAvailable(Boolean isInvoiceAvailable) {
		this.isInvoiceAvailable = isInvoiceAvailable;
	}
	public String getRequestorName() {
		return requestorName;
	}
	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}
	public String getReportingManager() {
		return reportingManager;
	}
	public void setReportingManager(String reportingManager) {
		this.reportingManager = reportingManager;
	}
	public String getSysAdminName() {
		return sysAdminName;
	}
	public void setSysAdminName(String sysAdminName) {
		this.sysAdminName = sysAdminName;
	}
	public String getFinanceHeadName() {
		return financeHeadName;
	}
	public void setFinanceHeadName(String financeHeadName) {
		this.financeHeadName = financeHeadName;
	}
	public String getBuHeadName() {
		return buHeadName;
	}
	public void setBuHeadName(String buHeadName) {
		this.buHeadName = buHeadName;
	}
	public String getItHeadName() {
		return itHeadName;
	}
	public void setItHeadName(String itHeadName) {
		this.itHeadName = itHeadName;
	}
	public String getRmComment() {
		return rmComment;
	}
	public void setRmComment(String rmComment) {
		this.rmComment = rmComment;
	}
	public String getSysAdminComment() {
		return sysAdminComment;
	}
	public void setSysAdminComment(String sysAdminComment) {
		this.sysAdminComment = sysAdminComment;
	}
	public String getFinanceExComment() {
		return financeExComment;
	}
	public void setFinanceExComment(String financeExComment) {
		this.financeExComment = financeExComment;
	}
	public String getBuHeadComment() {
		return buHeadComment;
	}
	public void setBuHeadComment(String buHeadComment) {
		this.buHeadComment = buHeadComment;
	}
	@Override
	public String toString() {
		return "PurchaseOrderDetailsOutputBean [pkPurchaseRequestId="
				+ pkPurchaseRequestId + ", capexNumber=" + capexNumber
				+ ", poNumber=" + poNumber + ", castCenter=" + castCenter
				+ ", projectName=" + projectName + ", softwareTitleAndVersion="
				+ softwareTitleAndVersion + ", assetType=" + assetType
				+ ", location=" + location + ", isClientReimbursing="
				+ isClientReimbursing + ", purpose=" + purpose
				+ ", purchaseRequestStatus=" + purchaseRequestStatus
				+ ", purchaseRequestStatusName=" + purchaseRequestStatusName
				+ ", assetRequiredBy=" + assetRequiredBy
				+ ", assetModifiedDate=" + assetModifiedDate
				+ ", assetCreatedDate=" + assetCreatedDate
				+ ", isSupportingDocsAvailable=" + isSupportingDocsAvailable
				+ ", isPoAvailable=" + isPoAvailable + ", isCapexAvailable="
				+ isCapexAvailable + ", isInvoiceAvailable="
				+ isInvoiceAvailable + ", requestorName=" + requestorName
				+ ", reportingManager=" + reportingManager + ", sysAdminName="
				+ sysAdminName + ", financeHeadName=" + financeHeadName
				+ ", buHeadName=" + buHeadName + ", itHeadName=" + itHeadName
				+ ", rmComment=" + rmComment + ", sysAdminComment="
				+ sysAdminComment + ", financeExComment=" + financeExComment
				+ ", buHeadComment=" + buHeadComment + ", quoteList="
				+ quoteList + "]";
	}
}