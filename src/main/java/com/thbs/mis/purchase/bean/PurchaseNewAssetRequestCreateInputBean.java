package com.thbs.mis.purchase.bean;

import java.util.Date;

public class PurchaseNewAssetRequestCreateInputBean 
{
	private Integer employeeId;
	private Integer fkAssetId;
	private Integer noOfUnits;
	private String specifications;
	private String briefDescription;
	private Date assetRequiredDate;
	private Integer duration;
	private String isClientReimbursing;
	private Integer clientId;
	private Integer reportingMgrId;
	private Integer projectComponentId;
	private Short fkBuId;
	private Integer projectId;	
	
	public Short getFkBuId() {
		return fkBuId;
	}
	public void setFkBuId(Short fkBuId) {
		this.fkBuId = fkBuId;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public Integer getProjectComponentId() {
		return projectComponentId;
	}
	public void setProjectComponentId(Integer projectComponentId) {
		this.projectComponentId = projectComponentId;
	}
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public Integer getFkAssetId() {
		return fkAssetId;
	}
	public void setFkAssetId(Integer fkAssetId) {
		this.fkAssetId = fkAssetId;
	}
	public Integer getNoOfUnits() {
		return noOfUnits;
	}
	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	public String getSpecifications() {
		return specifications;
	}
	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}
	public String getBriefDescription() {
		return briefDescription;
	}
	public void setBriefDescription(String briefDescription) {
		this.briefDescription = briefDescription;
	}
	public Date getAssetRequiredDate() {
		return assetRequiredDate;
	}
	public void setAssetRequiredDate(Date assetRequiredDate) {
		this.assetRequiredDate = assetRequiredDate;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}
	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	public Integer getReportingMgrId() {
		return reportingMgrId;
	}
	public void setReportingMgrId(Integer reportingMgrId) {
		this.reportingMgrId = reportingMgrId;
	}
	@Override
	public String toString() {
		return "PurchaseNewAssetRequestCreateInputBean [employeeId="
				+ employeeId + ", fkAssetId=" + fkAssetId + ", noOfUnits="
				+ noOfUnits + ", specifications=" + specifications
				+ ", briefDescription=" + briefDescription
				+ ", assetRequiredDate=" + assetRequiredDate + ", duration="
				+ duration + ", isClientReimbursing=" + isClientReimbursing
				+ ", clientId=" + clientId + ", reportingMgrId="
				+ reportingMgrId + ", projectComponentId=" + projectComponentId
				+ ", fkBuId=" + fkBuId + ", projectId=" + projectId + "]";
	}
				

}
