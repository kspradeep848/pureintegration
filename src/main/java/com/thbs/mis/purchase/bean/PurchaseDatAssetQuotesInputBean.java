//Added by Prathibha for Add quotes

package com.thbs.mis.purchase.bean;

import java.util.Date;

import org.hibernate.validator.constraints.NotEmpty;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class PurchaseDatAssetQuotesInputBean 
{  
	private double buContribution;
	
	@NotEmpty
	private String costBorneBy;
	
	@Employee_Id_Not_Found
	private Integer createdBy;
	private Date deliveryDate;
	private double individualLicenseCost;
	private double totalCost;
	private Integer fkPurchaseRequestId;
	private Integer fkSupplierAddress;
	private boolean isSetDefault;
	private String vendorQuoteReferenceNo;
	private Integer currencyType;
	private String assetDescriptionEnteredBySysadmin;
		
	public boolean getIsSetDefault() {
		return isSetDefault;
	}

	public Integer getFkSupplierAddress() {
		return fkSupplierAddress;
	}

	public void setFkSupplierAddress(Integer fkSupplierAddress) {
		this.fkSupplierAddress = fkSupplierAddress;
	}

	public void setIsSetDefault(boolean isSetDefault) {
		this.isSetDefault = isSetDefault;
	}
	public double getBuContribution() {
		return buContribution;
	}

	public void setBuContribution(double buContribution) {
		this.buContribution = buContribution;
	}

	public String getCostBorneBy() {
		return costBorneBy;
	}

	public void setCostBorneBy(String costBorneBy) {
		this.costBorneBy = costBorneBy;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	
	public double getIndividualLicenseCost() {
		return individualLicenseCost;
	}

	public void setIndividualLicenseCost(double individualLicenseCost) {
		this.individualLicenseCost = individualLicenseCost;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	public Integer getFkPurchaseRequestId() {
		return fkPurchaseRequestId;
	}

	public void setFkPurchaseRequestId(Integer fkPurchaseRequestId) {
		this.fkPurchaseRequestId = fkPurchaseRequestId;
	}
	

	public String getVendorQuoteReferenceNo() {
		return vendorQuoteReferenceNo;
	}

	public void setVendorQuoteReferenceNo(String vendorQuoteReferenceNo) {
		this.vendorQuoteReferenceNo = vendorQuoteReferenceNo;
	}

	public Integer getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}

	public void setSetDefault(boolean isSetDefault) {
		this.isSetDefault = isSetDefault;
	}

	public String getAssetDescriptionEnteredBySysadmin() {
		return assetDescriptionEnteredBySysadmin;
	}

	public void setAssetDescriptionEnteredBySysadmin(
			String assetDescriptionEnteredBySysadmin) {
		this.assetDescriptionEnteredBySysadmin = assetDescriptionEnteredBySysadmin;
	}

	@Override
	public String toString() {
		return "PurchaseDatAssetQuotesInputBean [buContribution="
				+ buContribution + ", costBorneBy=" + costBorneBy
				+ ", createdBy=" + createdBy + ", deliveryDate=" + deliveryDate
				+ ", individualLicenseCost=" + individualLicenseCost
				+ ", totalCost=" + totalCost + ", fkPurchaseRequestId="
				+ fkPurchaseRequestId + ", fkSupplierAddress="
				+ fkSupplierAddress + ", isSetDefault=" + isSetDefault
				+ ", vendorQuoteReferenceNo=" + vendorQuoteReferenceNo
				+ ", currencyType=" + currencyType
				+ ", assetDescriptionEnteredBySysadmin="
				+ assetDescriptionEnteredBySysadmin + "]";
	}
	
}
//EOA by Prathibha for Add quotes