package com.thbs.mis.purchase.bean;

import java.util.List;

public class ProjectAllocatedToEmpOutputBean {
	
	private Integer clientId;
	private String clientName;
	private int employeeId;
	private List<ProjectDetailsBean> projectDetail;
	
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public List<ProjectDetailsBean> getProjectDetail() {
		return projectDetail;
	}
	public void setProjectDetail(List<ProjectDetailsBean> projectDetail) {
		this.projectDetail = projectDetail;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	@Override
	public String toString() {
		return "ProjectAllocatedToEmpOutputBean [clientId=" + clientId
				+ ", clientName=" + clientName + ", employeeId=" + employeeId
				+ ", projectDetail=" + projectDetail + "]";
	}

}
