package com.thbs.mis.purchase.bean;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;



public class PurchasePOCreateBean {

	@NotNull(message = "softwareLicOrSubscriptionId should not be null")
	@NotBlank(message = "softwareLicOrSubscriptionId should not be blank")
	@NotEmpty(message = "softwareLicOrSubscriptionId should not be empty")
	private Integer assetReqId;
	
	private Integer createdBy;
	
	private Boolean isPoNeedToCreate;
	
	private String assetPriority;
	
	
	
	public String getAssetPriority() {
		return assetPriority;
	}

	public void setAssetPriority(String assetPriority) {
		this.assetPriority = assetPriority;
	}

	public Boolean getIsPoNeedToCreate() {
		return isPoNeedToCreate;
	}

	public void setIsPoNeedToCreate(Boolean isPoNeedToCreate) {
		this.isPoNeedToCreate = isPoNeedToCreate;
	}

	public Integer getAssetReqId() {
		return assetReqId;
	}

	public void setAssetReqId(Integer assetReqId) {
		this.assetReqId = assetReqId;
	}

	
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public String toString() {
		return "PurchasePOCreateBean [assetReqId=" + assetReqId
				+ ", createdBy=" + createdBy + ", isPoNeedToCreate="
				+ isPoNeedToCreate + ", assetPriority=" + assetPriority + "]";
	}

	
	
	
}
