package com.thbs.mis.purchase.bean;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class PurchaseAddVendorDetailsInputBean {

	@NotBlank(message = "assetType should not be blank.")
	@NotEmpty(message = "assetType should not be empty.")
	@NotNull(message = "assetType should not be null.")
	private String assetType;
	
	@NotBlank(message = "assetNameAndVersionOrModel should not be blank.")
	@NotEmpty(message = "assetNameAndVersionOrModel should not be empty.")
	@NotNull(message = "assetNameAndVersionOrModel should not be null.")
	private String assetNameAndVersionOrModel;
	
	@NumberFormat(style = Style.NUMBER, pattern = "createdBy should be numeric value")
	@NotNull(message = "createdBy can not be null")
	@Min(1)
	private int createdBy;
	
	@NotBlank(message = "licenseType should not be blank.")
	@NotEmpty(message = "licenseType should not be empty.")
	@NotNull(message = "licenseType should not be null.")
	private String licenseType;
	
	
	private List<PurchaseVendorDetailsBean> vendorDetails;
	
	
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
		
		
		
	}
	public String getAssetNameAndVersionOrModel() {
		return assetNameAndVersionOrModel;
	}
	public void setAssetNameAndVersionOrModel(String assetNameAndVersionOrModel) {
		this.assetNameAndVersionOrModel = assetNameAndVersionOrModel;
	}
	public List<PurchaseVendorDetailsBean> getVendorDetails() {
		return vendorDetails;
	}
	public void setVendorDetails(List<PurchaseVendorDetailsBean> vendorDetails) {
		this.vendorDetails = vendorDetails;
	}
	
	
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getLicenseType() {
		return licenseType;
	}
	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}
	@Override
	public String toString() {
		return "PurchaseAddVendorDetailsInputBean [assetType=" + assetType
				+ ", assetNameAndVersionOrModel=" + assetNameAndVersionOrModel
				+ ", createdBy=" + createdBy + ", licenseType=" + licenseType
				+ ", vendorDetails=" + vendorDetails + "]";
	}
	
	
	
	

	
	
}
