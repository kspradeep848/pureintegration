package com.thbs.mis.purchase.bean;


public class PurchaseAssetDetailForAssetRequestOutputBean 
{
	private Integer assetId;
	private String assetType;
	private String assetNameAndVersionOrModel;
	private Integer assetDetailId;
	private String softwareLicenseOrSubscriptionId;
	private String licenceType;
	public Integer getAssetId() {
		return assetId;
	}
	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public String getAssetNameAndVersionOrModel() {
		return assetNameAndVersionOrModel;
	}
	public void setAssetNameAndVersionOrModel(String assetNameAndVersionOrModel) {
		this.assetNameAndVersionOrModel = assetNameAndVersionOrModel;
	}
	
	public Integer getAssetDetailId() {
		return assetDetailId;
	}
	public void setAssetDetailId(Integer assetDetailId) {
		this.assetDetailId = assetDetailId;
	}
	public String getSoftwareLicenseOrSubscriptionId() {
		return softwareLicenseOrSubscriptionId;
	}
	public void setSoftwareLicenseOrSubscriptionId(
			String softwareLicenseOrSubscriptionId) {
		this.softwareLicenseOrSubscriptionId = softwareLicenseOrSubscriptionId;
	}
	public String getLicenceType() {
		return licenceType;
	}
	public void setLicenceType(String licenceType) {
		this.licenceType = licenceType;
	}
	@Override
	public String toString() {
		return "PurchaseAssetDetailForAssetRequestOutputBean [assetId="
				+ assetId + ", assetType=" + assetType
				+ ", assetNameAndVersionOrModel=" + assetNameAndVersionOrModel
				+ ", assetDetailId=" + assetDetailId
				+ ", softwareLicenseOrSubscriptionId="
				+ softwareLicenseOrSubscriptionId + ", licenceType="
				+ licenceType + "]";
	}
}
