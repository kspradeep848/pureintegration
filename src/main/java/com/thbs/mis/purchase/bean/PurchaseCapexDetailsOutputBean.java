package com.thbs.mis.purchase.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseCapexDetailsOutputBean {

	private String isClientReimbursing;
	private String briefDescription;
	private String buUnitName;
	private String capexNo;
	private String assetType;
	private String assetNameAndVersionOrModel;
	private String specifications;
	private String locationParentName;
	private Double buContribution;
	private Double individualLicenseCost;
	private Double totalCost;
	private String costBorneBy;
	private String projectName;
	private Byte internalFlag;
	private Double itContribution;
	private String supportingDocumentsByClient;
	private Integer purchaseRequestStatus;
	private String purchaseRequestStatusName;
	private Integer purchaseRequestId;
	private Integer fkCurrencyType;
	
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date assetRequiredBy;
	
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date assetModifiedDate;
	
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date assetCreatedDate;
    private String requestorName;
	private String reportingManager;
	private String sysAdminName;
	private String financeHeadName;
	private String buHeadName;
	private String itHeadName;
	
	private String rmComment;
	private String sysAdminComment;
	private String financeExComment;
	private String buHeadComment;

	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}

	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}

	public String getBriefDescription() {
		return briefDescription;
	}

	public void setBriefDescription(String briefDescription) {
		this.briefDescription = briefDescription;
	}

	public String getBuUnitName() {
		return buUnitName;
	}

	public void setBuUnitName(String buUnitName) {
		this.buUnitName = buUnitName;
	}

	public String getCapexNo() {
		return capexNo;
	}

	public void setCapexNo(String capexNo) {
		this.capexNo = capexNo;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public String getAssetNameAndVersionOrModel() {
		return assetNameAndVersionOrModel;
	}

	public void setAssetNameAndVersionOrModel(String assetNameAndVersionOrModel) {
		this.assetNameAndVersionOrModel = assetNameAndVersionOrModel;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	public String getLocationParentName() {
		return locationParentName;
	}

	public void setLocationParentName(String locationParentName) {
		this.locationParentName = locationParentName;
	}

	public Double getBuContribution() {
		return buContribution;
	}

	public void setBuContribution(Double buContribution) {
		this.buContribution = buContribution;
	}

	public Double getIndividualLicenseCost() {
		return individualLicenseCost;
	}

	public void setIndividualLicenseCost(Double individualLicenseCost) {
		this.individualLicenseCost = individualLicenseCost;
	}

	public Double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}

	public String getCostBorneBy() {
		return costBorneBy;
	}

	public void setCostBorneBy(String costBorneBy) {
		this.costBorneBy = costBorneBy;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Byte getInternalFlag() {
		return internalFlag;
	}

	public void setInternalFlag(Byte internalFlag) {
		this.internalFlag = internalFlag;
	}

	public Double getItContribution() {
		return itContribution;
	}

	public void setItContribution(Double itContribution) {
		this.itContribution = itContribution;
	}

	public String getSupportingDocumentsByClient() {
		return supportingDocumentsByClient;
	}

	public void setSupportingDocumentsByClient(String supportingDocumentsByClient) {
		this.supportingDocumentsByClient = supportingDocumentsByClient;
	}

	public Integer getPurchaseRequestStatus() {
		return purchaseRequestStatus;
	}

	public void setPurchaseRequestStatus(Integer purchaseRequestStatus) {
		this.purchaseRequestStatus = purchaseRequestStatus;
	}

	public String getPurchaseRequestStatusName() {
		return purchaseRequestStatusName;
	}

	public void setPurchaseRequestStatusName(String purchaseRequestStatusName) {
		this.purchaseRequestStatusName = purchaseRequestStatusName;
	}

	public Integer getPurchaseRequestId() {
		return purchaseRequestId;
	}

	public void setPurchaseRequestId(Integer purchaseRequestId) {
		this.purchaseRequestId = purchaseRequestId;
	}

	public Date getAssetRequiredBy() {
		return assetRequiredBy;
	}

	public void setAssetRequiredBy(Date assetRequiredBy) {
		this.assetRequiredBy = assetRequiredBy;
	}

	public Date getAssetModifiedDate() {
		return assetModifiedDate;
	}

	public void setAssetModifiedDate(Date assetModifiedDate) {
		this.assetModifiedDate = assetModifiedDate;
	}

	public Date getAssetCreatedDate() {
		return assetCreatedDate;
	}

	public void setAssetCreatedDate(Date assetCreatedDate) {
		this.assetCreatedDate = assetCreatedDate;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getReportingManager() {
		return reportingManager;
	}

	public void setReportingManager(String reportingManager) {
		this.reportingManager = reportingManager;
	}

	public String getSysAdminName() {
		return sysAdminName;
	}

	public void setSysAdminName(String sysAdminName) {
		this.sysAdminName = sysAdminName;
	}

	public String getFinanceHeadName() {
		return financeHeadName;
	}

	public void setFinanceHeadName(String financeHeadName) {
		this.financeHeadName = financeHeadName;
	}

	public String getBuHeadName() {
		return buHeadName;
	}

	public void setBuHeadName(String buHeadName) {
		this.buHeadName = buHeadName;
	}

	public String getItHeadName() {
		return itHeadName;
	}

	public void setItHeadName(String itHeadName) {
		this.itHeadName = itHeadName;
	}

	public String getRmComment() {
		return rmComment;
	}

	public void setRmComment(String rmComment) {
		this.rmComment = rmComment;
	}

	public String getSysAdminComment() {
		return sysAdminComment;
	}

	public void setSysAdminComment(String sysAdminComment) {
		this.sysAdminComment = sysAdminComment;
	}

	public String getFinanceExComment() {
		return financeExComment;
	}

	public void setFinanceExComment(String financeExComment) {
		this.financeExComment = financeExComment;
	}

	public String getBuHeadComment() {
		return buHeadComment;
	}

	public void setBuHeadComment(String buHeadComment) {
		this.buHeadComment = buHeadComment;
	}

	public Integer getFkCurrencyType() {
		return fkCurrencyType;
	}

	public void setFkCurrencyType(Integer fkCurrencyType) {
		this.fkCurrencyType = fkCurrencyType;
	}

	@Override
	public String toString() {
		return "PurchaseCapexDetailsOutputBean [isClientReimbursing="
				+ isClientReimbursing + ", briefDescription="
				+ briefDescription + ", buUnitName=" + buUnitName
				+ ", capexNo=" + capexNo + ", assetType=" + assetType
				+ ", assetNameAndVersionOrModel=" + assetNameAndVersionOrModel
				+ ", specifications=" + specifications
				+ ", locationParentName=" + locationParentName
				+ ", buContribution=" + buContribution
				+ ", individualLicenseCost=" + individualLicenseCost
				+ ", totalCost=" + totalCost + ", costBorneBy=" + costBorneBy
				+ ", projectName=" + projectName + ", internalFlag="
				+ internalFlag + ", itContribution=" + itContribution
				+ ", supportingDocumentsByClient="
				+ supportingDocumentsByClient + ", purchaseRequestStatus="
				+ purchaseRequestStatus + ", purchaseRequestStatusName="
				+ purchaseRequestStatusName + ", purchaseRequestId="
				+ purchaseRequestId + ", fkCurrencyType=" + fkCurrencyType
				+ ", assetRequiredBy=" + assetRequiredBy
				+ ", assetModifiedDate=" + assetModifiedDate
				+ ", assetCreatedDate=" + assetCreatedDate + ", requestorName="
				+ requestorName + ", reportingManager=" + reportingManager
				+ ", sysAdminName=" + sysAdminName + ", financeHeadName="
				+ financeHeadName + ", buHeadName=" + buHeadName
				+ ", itHeadName=" + itHeadName + ", rmComment=" + rmComment
				+ ", sysAdminComment=" + sysAdminComment
				+ ", financeExComment=" + financeExComment + ", buHeadComment="
				+ buHeadComment + "]";
	}	
}
