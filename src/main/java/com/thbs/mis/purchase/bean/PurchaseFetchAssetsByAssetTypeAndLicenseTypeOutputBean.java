package com.thbs.mis.purchase.bean;

public class PurchaseFetchAssetsByAssetTypeAndLicenseTypeOutputBean {

	private Integer pkAssetId;
	private String assetType;
	private String assetNameAndVersionOrModel;
	private String licenseType;
	private String isAvailable;
	public Integer getPkAssetId() {
		return pkAssetId;
	}
	public void setPkAssetId(Integer pkAssetId) {
		this.pkAssetId = pkAssetId;
	}
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public String getAssetNameAndVersionOrModel() {
		return assetNameAndVersionOrModel;
	}
	public void setAssetNameAndVersionOrModel(String assetNameAndVersionOrModel) {
		this.assetNameAndVersionOrModel = assetNameAndVersionOrModel;
	}
	public String getLicenseType() {
		return licenseType;
	}
	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}
	public String getIsAvailable() {
		return isAvailable;
	}
	public void setIsAvailable(String isAvailable) {
		this.isAvailable = isAvailable;
	}
	@Override
	public String toString() {
		return "PurchaseFetchAssetsByAssetTypeAndLicenseTypeOutputBean [pkAssetId="
				+ pkAssetId
				+ ", assetType="
				+ assetType
				+ ", assetNameAndVersionOrModel="
				+ assetNameAndVersionOrModel
				+ ", licenseType="
				+ licenseType
				+ ", isAvailable="
				+ isAvailable + "]";
	}
	
	
	
	
}
