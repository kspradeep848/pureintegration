package com.thbs.mis.purchase.bean;

public class PurchaseAssetDetailsOutputBean {
	private Integer assetId;
	private String assetType;
	private String assetNameAndVersionOrModel;
	private String licenceType;
	public Integer getAssetId() {
		return assetId;
	}
	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public String getAssetNameAndVersionOrModel() {
		return assetNameAndVersionOrModel;
	}
	public void setAssetNameAndVersionOrModel(String assetNameAndVersionOrModel) {
		this.assetNameAndVersionOrModel = assetNameAndVersionOrModel;
	}
	public String getLicenceType() {
		return licenceType;
	}
	public void setLicenceType(String licenceType) {
		this.licenceType = licenceType;
	}
	@Override
	public String toString() {
		return "PurchaseAssetDetailsOutputBean [assetId=" + assetId
				+ ", assetType=" + assetType + ", assetNameAndVersionOrModel="
				+ assetNameAndVersionOrModel + ", licenceType=" + licenceType
				+ "]";
	}
}
