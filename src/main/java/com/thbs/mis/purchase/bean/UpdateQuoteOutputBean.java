package com.thbs.mis.purchase.bean;

import java.util.Date;

public class UpdateQuoteOutputBean {
	
	private Integer pkQuoteId;
	private double buContribution;
	private String costBorneBy;
	private Date deliveryDate;
	private String quoteStatus;
	private double individualLicenseCost;
	private double totalCost;
	private Integer fkSupplierAddress;
    private Integer modifiedBy;
	private String vendorQuoteReferenceNo;
	private Integer currencyType;
	
	public Integer getPkQuoteId() {
		return pkQuoteId;
	}
	public void setPkQuoteId(Integer pkQuoteId) {
		this.pkQuoteId = pkQuoteId;
	}
	public double getBuContribution() {
		return buContribution;
	}
	public void setBuContribution(double buContribution) {
		this.buContribution = buContribution;
	}
	public String getCostBorneBy() {
		return costBorneBy;
	}
	public void setCostBorneBy(String costBorneBy) {
		this.costBorneBy = costBorneBy;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
	public String getQuoteStatus() {
		return quoteStatus;
	}
	public void setQuoteStatus(String quoteStatus) {
		this.quoteStatus = quoteStatus;
	}
	public double getIndividualLicenseCost() {
		return individualLicenseCost;
	}
	public void setIndividualLicenseCost(double individualLicenseCost) {
		this.individualLicenseCost = individualLicenseCost;
	}
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	public Integer getFkSupplierAddress() {
		return fkSupplierAddress;
	}
	public void setFkSupplierAddress(Integer fkSupplierAddress) {
		this.fkSupplierAddress = fkSupplierAddress;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getVendorQuoteReferenceNo() {
		return vendorQuoteReferenceNo;
	}
	public void setVendorQuoteReferenceNo(String vendorQuoteReferenceNo) {
		this.vendorQuoteReferenceNo = vendorQuoteReferenceNo;
	}
	public Integer getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}
	@Override
	public String toString() {
		return "UpdateQuoteOutputBean [pkQuoteId=" + pkQuoteId
				+ ", buContribution=" + buContribution + ", costBorneBy="
				+ costBorneBy + ", deliveryDate=" + deliveryDate
				+ ", quoteStatus=" + quoteStatus + ", individualLicenseCost="
				+ individualLicenseCost + ", totalCost=" + totalCost
				+ ", fkSupplierAddress=" + fkSupplierAddress + ", modifiedBy="
				+ modifiedBy + ", vendorQuoteReferenceNo="
				+ vendorQuoteReferenceNo + ", currencyType=" + currencyType
				+ "]";
	}
	
	
}
