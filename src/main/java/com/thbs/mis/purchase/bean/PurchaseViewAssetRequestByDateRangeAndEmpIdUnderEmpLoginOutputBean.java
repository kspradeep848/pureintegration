package com.thbs.mis.purchase.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseViewAssetRequestByDateRangeAndEmpIdUnderEmpLoginOutputBean 
{
	private Integer assetRequestId;
	private Date requiredDate;
	private String assetRequestDetail;
	private String projectName;
	private Integer numberOfUnits;
	private String assetDetails;
	private Integer requestStatusId;
	private String requestStatus;
	private Date createdDate;
	private Integer fkRequestTypeId;
	public Integer getAssetRequestId() {
		return assetRequestId;
	}
	public void setAssetRequestId(Integer assetRequestId) {
		this.assetRequestId = assetRequestId;
	}
	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getRequiredDate() {
		return requiredDate;
	}
	public void setRequiredDate(Date requiredDate) {
		this.requiredDate = requiredDate;
	}
	public String getAssetRequestDetail() {
		return assetRequestDetail;
	}
	public void setAssetRequestDetail(String assetRequestDetail) {
		this.assetRequestDetail = assetRequestDetail;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Integer getNumberOfUnits() {
		return numberOfUnits;
	}
	public void setNumberOfUnits(Integer numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}
	public String getAssetDetails() {
		return assetDetails;
	}
	public void setAssetDetails(String assetDetails) {
		this.assetDetails = assetDetails;
	}
	public Integer getRequestStatusId() {
		return requestStatusId;
	}
	public void setRequestStatusId(Integer requestStatusId) {
		this.requestStatusId = requestStatusId;
	}
	public String getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getFkRequestTypeId() {
		return fkRequestTypeId;
	}
	public void setFkRequestTypeId(Integer fkRequestTypeId) {
		this.fkRequestTypeId = fkRequestTypeId;
	}
	@Override
	public String toString() {
		return "PurchaseViewAssetRequestByDateRangeAndEmpIdUnderEmpLoginOutputBean [assetRequestId="
				+ assetRequestId
				+ ", requiredDate="
				+ requiredDate
				+ ", assetRequestDetail="
				+ assetRequestDetail
				+ ", projectName="
				+ projectName
				+ ", numberOfUnits="
				+ numberOfUnits
				+ ", assetDetails="
				+ assetDetails
				+ ", requestStatusId="
				+ requestStatusId
				+ ", requestStatus="
				+ requestStatus
				+ ", createdDate="
				+ createdDate
				+ ", fkRequestTypeId=" + fkRequestTypeId + "]";
	}
	
	
}
