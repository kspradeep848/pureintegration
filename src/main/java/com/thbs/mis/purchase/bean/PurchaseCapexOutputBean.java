package com.thbs.mis.purchase.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseCapexOutputBean {
	
	private Date raisedOn;
	
	private String businessUnit;

	private String projectName;
	
	private String status;
	
	private Integer statusId;
	
	private Integer assetRequestId;

	private String poDetails;
	
	
	public String getPoDetails() {
		return poDetails;
	}

	public void setPoDetails(String poDetails) {
		this.poDetails = poDetails;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}


	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getRaisedOn() {
		return raisedOn;
	}

	public void setRaisedOn(Date raisedOn) {
		this.raisedOn = raisedOn;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

	public Integer getAssetRequestId() {
		return assetRequestId;
	}

	public void setAssetRequestId(Integer assetRequestId) {
		this.assetRequestId = assetRequestId;
	}

	@Override
	public String toString() {
		return "PurchaseCapexOutputBean [raisedOn=" + raisedOn + ", businessUnit=" + businessUnit
				+ ", projectName=" + projectName + ", status=" + status
				+ ", statusId=" + statusId + ", assetRequestId="
				+ assetRequestId + ", poDetails=" + poDetails + "]";
	}
	
}
