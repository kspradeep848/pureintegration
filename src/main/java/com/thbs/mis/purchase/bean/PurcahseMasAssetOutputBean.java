package com.thbs.mis.purchase.bean;

import java.util.Date;




public class PurcahseMasAssetOutputBean {
	
	
	private Integer pkAssetId;
	private String assetType;
	private String assetNameAndVersionOrModel;
	private String isAvailable;
	private Integer createdBy;
	private Date createdDate;
	private Integer modifiedBy;
	private Date modifiedDate;

	public Integer getPkAssetId() {
		return pkAssetId;
	}
	public void setPkAssetId(Integer pkAssetId) {
		this.pkAssetId = pkAssetId;
	}
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public String getIsAvailable() {
		return isAvailable;
	}
	public void setIsAvailable(String isAvailable) {
		this.isAvailable = isAvailable;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getAssetNameAndVersionOrModel() {
		return assetNameAndVersionOrModel;
	}
	public void setAssetNameAndVersionOrModel(String assetNameAndVersionOrModel) {
		this.assetNameAndVersionOrModel = assetNameAndVersionOrModel;
	}
	
	
	
	private Integer pkAssetDetailId;
	private Date assetAvailableFromDate;
	private Date assetAvailableToDate;
	private Integer assestDetail_createdBy;
	private Date assestDetail_createdDate;
	private Integer assestDetail_modifiedBy;
	private Date assestDetail_modifiedDate;
	private String softwareLicenseOrSubscriptionId;
	private Integer fkMasAssetsId;

	public Integer getPkAssetDetailId() {
		return pkAssetDetailId;
	}
	public void setPkAssetDetailId(Integer pkAssetDetailId) {
		this.pkAssetDetailId = pkAssetDetailId;
	}
	public Date getAssetAvailableFromDate() {
		return assetAvailableFromDate;
	}
	public void setAssetAvailableFromDate(Date assetAvailableFromDate) {
		this.assetAvailableFromDate = assetAvailableFromDate;
	}
	public Date getAssetAvailableToDate() {
		return assetAvailableToDate;
	}
	public void setAssetAvailableToDate(Date assetAvailableToDate) {
		this.assetAvailableToDate = assetAvailableToDate;
	}
	public Integer getAssestDetail_createdBy() {
		return assestDetail_createdBy;
	}
	public void setAssestDetail_createdBy(Integer assestDetail_createdBy) {
		this.assestDetail_createdBy = assestDetail_createdBy;
	}
	public Date getAssestDetail_createdDate() {
		return assestDetail_createdDate;
	}
	public void setAssestDetail_createdDate(Date assestDetail_createdDate) {
		this.assestDetail_createdDate = assestDetail_createdDate;
	}
	public Integer getAssestDetail_modifiedBy() {
		return assestDetail_modifiedBy;
	}
	public void setAssestDetail_modifiedBy(Integer assestDetail_modifiedBy) {
		this.assestDetail_modifiedBy = assestDetail_modifiedBy;
	}
	public Date getAssestDetail_modifiedDate() {
		return assestDetail_modifiedDate;
	}
	public void setAssestDetail_modifiedDate(Date assestDetail_modifiedDate) {
		this.assestDetail_modifiedDate = assestDetail_modifiedDate;
	}
	public String getSoftwareLicenseOrSubscriptionId() {
		return softwareLicenseOrSubscriptionId;
	}
	public void setSoftwareLicenseOrSubscriptionId(
			String softwareLicenseOrSubscriptionId) {
		this.softwareLicenseOrSubscriptionId = softwareLicenseOrSubscriptionId;
	}
	public Integer getFkMasAssetsId() {
		return fkMasAssetsId;
	}
	public void setFkMasAssetsId(Integer fkMasAssetsId) {
		this.fkMasAssetsId = fkMasAssetsId;
	}
	@Override
	public String toString() {
		return "PurcahseMasAssetOutputBean [pkAssetId=" + pkAssetId
				+ ", assetType=" + assetType + ", assetNameAndVersionOrModel="
				+ assetNameAndVersionOrModel + ", isAvailable=" + isAvailable
				+ ", createdBy=" + createdBy + ", createdDate=" + createdDate
				+ ", modifiedBy=" + modifiedBy + ", modifiedDate="
				+ modifiedDate + ", pkAssetDetailId=" + pkAssetDetailId
				+ ", assetAvailableFromDate=" + assetAvailableFromDate
				+ ", assetAvailableToDate=" + assetAvailableToDate
				+ ", assestDetail_createdBy=" + assestDetail_createdBy
				+ ", assestDetail_createdDate=" + assestDetail_createdDate
				+ ", assestDetail_modifiedBy=" + assestDetail_modifiedBy
				+ ", assestDetail_modifiedDate=" + assestDetail_modifiedDate
				+ ", softwareLicenseOrSubscriptionId="
				+ softwareLicenseOrSubscriptionId + ", fkMasAssetsId="
				+ fkMasAssetsId + "]";
	}
		
}
