package com.thbs.mis.purchase.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class PurchaseInputBean {

	@NumberFormat(style = Style.NUMBER, pattern = "purchaseRequestId should be numeric value")
	@Min(1)
	@NotNull(message = "id field can not be null")
	private Integer id;
	
    @Employee_Id_Not_Found
	@NumberFormat(style = Style.NUMBER, pattern = "loggedInUser should be numeric value")
	@Min(1)
	@NotNull(message = "loggedInUser field can not be null")
	private Integer loggedInUser;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Integer loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	@Override
	public String toString() {
		return "PurchaseApprovePoBean [id=" + id + ", loggedInUser="
				+ loggedInUser + "]";
	}

}
