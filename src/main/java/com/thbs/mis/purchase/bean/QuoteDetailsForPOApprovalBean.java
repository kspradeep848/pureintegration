package com.thbs.mis.purchase.bean;

import java.util.Date;

public class QuoteDetailsForPOApprovalBean 
{
	private int quoteId;
	private double totalCost;
	private String quoteStatus;
	private Integer fkCurrencyType;
	private String costBorneBy;
	private Double individualLicenseCost;
	private Double buContribution;
	private Date deliveryDate;
	private String vendorName;
	private String vendorAddress;
	private String vendorContactNo;
	private String vendorQuoteReferenceNo;
	private Integer fkSupplierAddress;
	private String assetDescriptionEnteredBySysadmin; 
	
	
	public int getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(int quoteId) {
		this.quoteId = quoteId;
	}
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	public String getQuoteStatus() {
		return quoteStatus;
	}
	public void setQuoteStatus(String quoteStatus) {
		this.quoteStatus = quoteStatus;
	}
	
	public Integer getFkCurrencyType() {
		return fkCurrencyType;
	}
	public void setFkCurrencyType(Integer fkCurrencyType) {
		this.fkCurrencyType = fkCurrencyType;
	}
	public String getCostBorneBy() {
		return costBorneBy;
	}
	public void setCostBorneBy(String costBorneBy) {
		this.costBorneBy = costBorneBy;
	}
	public Double getIndividualLicenseCost() {
		return individualLicenseCost;
	}
	public void setIndividualLicenseCost(Double individualLicenseCost) {
		this.individualLicenseCost = individualLicenseCost;
	}
	public Double getBuContribution() {
		return buContribution;
	}
	public void setBuContribution(Double buContribution) {
		this.buContribution = buContribution;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorAddress() {
		return vendorAddress;
	}
	public void setVendorAddress(String vendorAddress) {
		this.vendorAddress = vendorAddress;
	}
	public String getVendorContactNo() {
		return vendorContactNo;
	}
	public void setVendorContactNo(String vendorContactNo) {
		this.vendorContactNo = vendorContactNo;
	}
	public String getVendorQuoteReferenceNo() {
		return vendorQuoteReferenceNo;
	}
	public void setVendorQuoteReferenceNo(String vendorQuoteReferenceNo) {
		this.vendorQuoteReferenceNo = vendorQuoteReferenceNo;
	}
	public Integer getFkSupplierAddress() {
		return fkSupplierAddress;
	}
	public void setFkSupplierAddress(Integer fkSupplierAddress) {
		this.fkSupplierAddress = fkSupplierAddress;
	}
	public String getAssetDescriptionEnteredBySysadmin() {
		return assetDescriptionEnteredBySysadmin;
	}
	public void setAssetDescriptionEnteredBySysadmin(
			String assetDescriptionEnteredBySysadmin) {
		this.assetDescriptionEnteredBySysadmin = assetDescriptionEnteredBySysadmin;
	}
	@Override
	public String toString() {
		return "QuoteDetailsForPOApprovalBean [quoteId=" + quoteId
				+ ", totalCost=" + totalCost + ", quoteStatus=" + quoteStatus
				+ ", fkCurrencyType=" + fkCurrencyType + ", costBorneBy="
				+ costBorneBy + ", individualLicenseCost="
				+ individualLicenseCost + ", buContribution=" + buContribution
				+ ", deliveryDate=" + deliveryDate + ", vendorName="
				+ vendorName + ", vendorAddress=" + vendorAddress
				+ ", vendorContactNo=" + vendorContactNo
				+ ", vendorQuoteReferenceNo=" + vendorQuoteReferenceNo
				+ ", fkSupplierAddress=" + fkSupplierAddress
				+ ", assetDescriptionEnteredBySysadmin="
				+ assetDescriptionEnteredBySysadmin + "]";
	}
	
}