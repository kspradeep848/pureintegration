package com.thbs.mis.purchase.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurcahseAssetDetailsQoutesByRequestIdOutputBean {
	
	private Integer assetRequestId;
	/*private Integer pkAssetDetailId;*/
	private Integer rquestTypeId;
	private String priority;
	private String assetNameAndVersionOrModel;
    private Integer noOfUnits;
    private String assetType;
    private Date assetRequiredBy;
    private Integer duration;
    private String assetDescription;
    private String isClientReimbursing;
    private Integer projectComponentId;
    
	
	/*private Date assetAvailableFromDate;
	private Date assetAvailableToDate;*/
	private Integer assetDetailCreatedBy;
	private Date assetDetailCreatedDate;
	/*private Integer assetDetailModifiedBy;
	private Date assetDetailModifiedDate;
	private String softwareLicenseOrSubscriptionId;*/
	private Integer fkMasAssetsId;		
	
	/*private String isAssetAvailable;*/
	
	List<PurcahseQuotesOutPutBean> qutesList;
	
	public List<PurcahseQuotesOutPutBean> getQutesList() {
		return qutesList;
	}

	public void setQutesList(List<PurcahseQuotesOutPutBean> qutesList) {
		this.qutesList = qutesList;
	}

	public Integer getAssetRequestId() {
		return assetRequestId;
	}

	public void setAssetRequestId(Integer assetRequestId) {
		this.assetRequestId = assetRequestId;
	}

	public Integer getProjectComponentId() {
		return projectComponentId;
	}

	public void setProjectComponentId(Integer projectComponentId) {
		this.projectComponentId = projectComponentId;
	}

	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}

	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}

	public String getAssetDescription() {
		return assetDescription;
	}

	public void setAssetDescription(String assetDescription) {
		this.assetDescription = assetDescription;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}
    
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getAssetRequiredBy() {
		return assetRequiredBy;
	}

	public void setAssetRequiredBy(Date assetRequiredBy) {
		this.assetRequiredBy = assetRequiredBy;
	}

	public Integer getNoOfUnits() {
		return noOfUnits;
	}

	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public Integer getRquestTypeId() {
		return rquestTypeId;
	}

	public void setRquestTypeId(Integer rquestTypeId) {
		this.rquestTypeId = rquestTypeId;
	}

	/*public Integer getPkAssetDetailId() {
		return pkAssetDetailId;
	}

	public void setPkAssetDetailId(Integer pkAssetDetailId) {
		this.pkAssetDetailId = pkAssetDetailId;
	}*/

	/*public Date getAssetAvailableFromDate() {
		return assetAvailableFromDate;
	}

	public void setAssetAvailableFromDate(Date assetAvailableFromDate) {
		this.assetAvailableFromDate = assetAvailableFromDate;
	}

	public Date getAssetAvailableToDate() {
		return assetAvailableToDate;
	}

	public void setAssetAvailableToDate(Date assetAvailableToDate) {
		this.assetAvailableToDate = assetAvailableToDate;
	}*/

	public Integer getAssetDetailCreatedBy() {
		return assetDetailCreatedBy;
	}

	public void setAssetDetailCreatedBy(Integer assetDetailCreatedBy) {
		this.assetDetailCreatedBy = assetDetailCreatedBy;
	}
     
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getAssetDetailCreatedDate() {
		return assetDetailCreatedDate;
	}

	public void setAssetDetailCreatedDate(Date assetDetailCreatedDate) {
		this.assetDetailCreatedDate = assetDetailCreatedDate;
	}

	/*public Integer getAssetDetailModifiedBy() {
		return assetDetailModifiedBy;
	}

	public void setAssetDetailModifiedBy(Integer assetDetailModifiedBy) {
		this.assetDetailModifiedBy = assetDetailModifiedBy;
	}

	public Date getAssetDetailModifiedDate() {
		return assetDetailModifiedDate;
	}

	public void setAssetDetailModifiedDate(Date assetDetailModifiedDate) {
		this.assetDetailModifiedDate = assetDetailModifiedDate;
	}

	public String getSoftwareLicenseOrSubscriptionId() {
		return softwareLicenseOrSubscriptionId;
	}

	public void setSoftwareLicenseOrSubscriptionId(
			String softwareLicenseOrSubscriptionId) {
		this.softwareLicenseOrSubscriptionId = softwareLicenseOrSubscriptionId;
	}*/

	public Integer getFkMasAssetsId() {
		return fkMasAssetsId;
	}

	public void setFkMasAssetsId(Integer fkMasAssetsId) {
		this.fkMasAssetsId = fkMasAssetsId;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public String getAssetNameAndVersionOrModel() {
		return assetNameAndVersionOrModel;
	}

	public void setAssetNameAndVersionOrModel(String assetNameAndVersionOrModel) {
		this.assetNameAndVersionOrModel = assetNameAndVersionOrModel;
	}

	/*public String getIsAssetAvailable() {
		return isAssetAvailable;
	}

	public void setIsAssetAvailable(String isAssetAvailable) {
		this.isAssetAvailable = isAssetAvailable;
	}*/

	

	@Override
	public String toString() {
		return "PurcahseAssetDetailsQoutesByRequestIdOutputBean [ assetDetailCreatedBy="
				+ assetDetailCreatedBy
				+ ", assetDetailCreatedDate="
				+ assetDetailCreatedDate
				+ ", fkMasAssetsId="
				+ fkMasAssetsId
				+ ", rquestTypeId="
				+ rquestTypeId
				+ ", priority="
				+ priority
				+ ", assetType="
				+ assetType
				+ ", assetNameAndVersionOrModel="
				+ assetNameAndVersionOrModel
				+ ", qutesList="
				+ qutesList + "]";
	}

	

}
