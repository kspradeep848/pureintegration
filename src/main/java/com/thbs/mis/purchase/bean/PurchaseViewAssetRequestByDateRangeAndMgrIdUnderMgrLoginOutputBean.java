package com.thbs.mis.purchase.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseViewAssetRequestByDateRangeAndMgrIdUnderMgrLoginOutputBean 
{
	private Integer assetRequestId;
	private Integer requestorId;
	private Date requiredDate;
	private String assetRequestDetail;
	private String projectName;
	private Integer numberOfUnits;
	private String assetDetails;
	private Integer requestStatusId;
	private String requestStatus;
	private String requestorName;
	private Integer purchaseRequestTypeId;
	private String purchaseRequestTypeName;
	
	public Integer getAssetRequestId() {
		return assetRequestId;
	}
	public void setAssetRequestId(Integer assetRequestId) {
		this.assetRequestId = assetRequestId;
	}
	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getRequiredDate() {
		return requiredDate;
	}
	public void setRequiredDate(Date requiredDate) {
		this.requiredDate = requiredDate;
	}
	public String getAssetRequestDetail() {
		return assetRequestDetail;
	}
	public void setAssetRequestDetail(String assetRequestDetail) {
		this.assetRequestDetail = assetRequestDetail;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Integer getNumberOfUnits() {
		return numberOfUnits;
	}
	public void setNumberOfUnits(Integer numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}
	public String getAssetDetails() {
		return assetDetails;
	}
	public void setAssetDetails(String assetDetails) {
		this.assetDetails = assetDetails;
	}
	public Integer getRequestStatusId() {
		return requestStatusId;
	}
	public void setRequestStatusId(Integer requestStatusId) {
		this.requestStatusId = requestStatusId;
	}
	public String getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
	public String getRequestorName() {
		return requestorName;
	}
	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}
	
	public Integer getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Integer requestorId) {
		this.requestorId = requestorId;
	}
			
	public Integer getPurchaseRequestTypeId() {
		return purchaseRequestTypeId;
	}
	public void setPurchaseRequestTypeId(Integer purchaseRequestTypeId) {
		this.purchaseRequestTypeId = purchaseRequestTypeId;
	}
	public String getPurchaseRequestTypeName() {
		return purchaseRequestTypeName;
	}
	public void setPurchaseRequestTypeName(String purchaseRequestTypeName) {
		this.purchaseRequestTypeName = purchaseRequestTypeName;
	}
	@Override
	public String toString() {
		return "PurchaseViewAssetRequestByDateRangeAndMgrIdUnderMgrLoginOutputBean [assetRequestId="
				+ assetRequestId
				+ ", requestorId="
				+ requestorId
				+ ", requiredDate="
				+ requiredDate
				+ ", assetRequestDetail="
				+ assetRequestDetail
				+ ", projectName="
				+ projectName
				+ ", numberOfUnits="
				+ numberOfUnits
				+ ", assetDetails="
				+ assetDetails
				+ ", requestStatusId="
				+ requestStatusId
				+ ", requestStatus="
				+ requestStatus
				+ ", requestorName="
				+ requestorName
				+ ", purchaseRequestTypeId="
				+ purchaseRequestTypeId
				+ ", purchaseRequestTypeName="
				+ purchaseRequestTypeName + "]";
	}
}
