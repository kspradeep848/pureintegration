package com.thbs.mis.purchase.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseOrderSysAdminOutputBean {


	
	private String capexNumber;
	private String poNumber;
	private String castCenter;
	private String projectName;
	private String softwareTitleAndVersion;
	private String assetType;
	private String location;
	private String isClientReimbursing;
	private String purpose;

	private String purchaseAssetStatus;
	private Integer purchaseAssetStatusId;
	private PurchaseQuotesOutputBean quote;
	private String isHardWareOrSoftwarePurchased;
	private String priority;
	private String systemAdminComments;

	private String sysAdminName;
	private String financeHeadName;
	private String buHeadName;
	private String itHeadName;
	private String requestorName;
	private String reportingManager;

	private Date availableFromDate;
	private Date availableToDate;
	private String softwareLicenseOrSubscriptionId;
	private String lastAssetId;
	private List<String> generatedAssetId;
	private Boolean isSupportingDocsAvailable;
	private Boolean isPoAvailable;
	private Boolean isCapexAvailable;
	private Boolean isInvoiceAvailable;
	
	private String rmComment;
	private String sysAdminComment;
	private String financeExComment;
	private String buHeadComment;
	
	private Integer fkCurrencyType;
	private Integer fkRequestTypeId;
	private String purchaseRequestTypeName;
	
	
	public List<String> getGeneratedAssetId() {
		return generatedAssetId;
	}
	public void setGeneratedAssetId(List<String> generatedAssetId) {
		this.generatedAssetId = generatedAssetId;
	}
	public String getCapexNumber() {
		return capexNumber;
	}
	public void setCapexNumber(String capexNumber) {
		this.capexNumber = capexNumber;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getCastCenter() {
		return castCenter;
	}
	public void setCastCenter(String castCenter) {
		this.castCenter = castCenter;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getSoftwareTitleAndVersion() {
		return softwareTitleAndVersion;
	}
	public void setSoftwareTitleAndVersion(String softwareTitleAndVersion) {
		this.softwareTitleAndVersion = softwareTitleAndVersion;
	}
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}
	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getAvailableFromDate() {
		return availableFromDate;
	}
	public void setAvailableFromDate(Date availableFromDate) {
		this.availableFromDate = availableFromDate;
	}
	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getAvailableToDate() {
		return availableToDate;
	}
	public void setAvailableToDate(Date availableToDate) {
		this.availableToDate = availableToDate;
	}
	public String getSoftwareLicenseOrSubscriptionId() {
		return softwareLicenseOrSubscriptionId;
	}
	public void setSoftwareLicenseOrSubscriptionId(
			String softwareLicenseOrSubscriptionId) {
		this.softwareLicenseOrSubscriptionId = softwareLicenseOrSubscriptionId;
	}
	public String getLastAssetId() {
		return lastAssetId;
	}
	public void setLastAssetId(String lastAssetId) {
		this.lastAssetId = lastAssetId;
	}
	public String getPurchaseAssetStatus() {
		return purchaseAssetStatus;
	}
	public void setPurchaseAssetStatus(String purchaseAssetStatus) {
		this.purchaseAssetStatus = purchaseAssetStatus;
	}
	public Integer getPurchaseAssetStatusId() {
		return purchaseAssetStatusId;
	}
	public void setPurchaseAssetStatusId(Integer purchaseAssetStatusId) {
		this.purchaseAssetStatusId = purchaseAssetStatusId;
	}
	public PurchaseQuotesOutputBean getQuote() {
		return quote;
	}
	public void setQuote(PurchaseQuotesOutputBean quote) {
		this.quote = quote;
	}
	public String getIsHardWareOrSoftwarePurchased() {
		return isHardWareOrSoftwarePurchased;
	}
	public void setIsHardWareOrSoftwarePurchased(
			String isHardWareOrSoftwarePurchased) {
		this.isHardWareOrSoftwarePurchased = isHardWareOrSoftwarePurchased;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getSystemAdminComments() {
		return systemAdminComments;
	}
	public void setSystemAdminComments(String systemAdminComments) {
		this.systemAdminComments = systemAdminComments;
	}
	public String getSysAdminName() {
		return sysAdminName;
	}
	public void setSysAdminName(String sysAdminName) {
		this.sysAdminName = sysAdminName;
	}
	public String getFinanceHeadName() {
		return financeHeadName;
	}
	public void setFinanceHeadName(String financeHeadName) {
		this.financeHeadName = financeHeadName;
	}
	public String getBuHeadName() {
		return buHeadName;
	}
	public void setBuHeadName(String buHeadName) {
		this.buHeadName = buHeadName;
	}
	public String getItHeadName() {
		return itHeadName;
	}
	public void setItHeadName(String itHeadName) {
		this.itHeadName = itHeadName;
	}
	public String getRequestorName() {
		return requestorName;
	}
	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}
	public String getReportingManager() {
		return reportingManager;
	}
	public void setReportingManager(String reportingManager) {
		this.reportingManager = reportingManager;
	}
	public Boolean getIsSupportingDocsAvailable() {
		return isSupportingDocsAvailable;
	}
	public void setIsSupportingDocsAvailable(Boolean isSupportingDocsAvailable) {
		this.isSupportingDocsAvailable = isSupportingDocsAvailable;
	}
	public Boolean getIsPoAvailable() {
		return isPoAvailable;
	}
	public void setIsPoAvailable(Boolean isPoAvailable) {
		this.isPoAvailable = isPoAvailable;
	}
	public Boolean getIsCapexAvailable() {
		return isCapexAvailable;
	}
	public void setIsCapexAvailable(Boolean isCapexAvailable) {
		this.isCapexAvailable = isCapexAvailable;
	}
	public Boolean getIsInvoiceAvailable() {
		return isInvoiceAvailable;
	}
	public void setIsInvoiceAvailable(Boolean isInvoiceAvailable) {
		this.isInvoiceAvailable = isInvoiceAvailable;
	}
	
	public String getRmComment() {
		return rmComment;
	}
	public void setRmComment(String rmComment) {
		this.rmComment = rmComment;
	}
	public String getSysAdminComment() {
		return sysAdminComment;
	}
	public void setSysAdminComment(String sysAdminComment) {
		this.sysAdminComment = sysAdminComment;
	}
	public String getFinanceExComment() {
		return financeExComment;
	}
	public void setFinanceExComment(String financeExComment) {
		this.financeExComment = financeExComment;
	}
	public String getBuHeadComment() {
		return buHeadComment;
	}
	public void setBuHeadComment(String buHeadComment) {
		this.buHeadComment = buHeadComment;
	}
	
	public Integer getFkCurrencyType() {
		return fkCurrencyType;
	}
	public void setFkCurrencyType(Integer fkCurrencyType) {
		this.fkCurrencyType = fkCurrencyType;
	}
	
	public Integer getFkRequestTypeId() {
		return fkRequestTypeId;
	}
	public void setFkRequestTypeId(Integer fkRequestTypeId) {
		this.fkRequestTypeId = fkRequestTypeId;
	}
	public String getPurchaseRequestTypeName() {
		return purchaseRequestTypeName;
	}
	public void setPurchaseRequestTypeName(String purchaseRequestTypeName) {
		this.purchaseRequestTypeName = purchaseRequestTypeName;
	}
	@Override
	public String toString() {
		return "PurchaseOrderSysAdminOutputBean [capexNumber=" + capexNumber
				+ ", poNumber=" + poNumber + ", castCenter=" + castCenter
				+ ", projectName=" + projectName + ", softwareTitleAndVersion="
				+ softwareTitleAndVersion + ", assetType=" + assetType
				+ ", location=" + location + ", isClientReimbursing="
				+ isClientReimbursing + ", purpose=" + purpose
				+ ", purchaseAssetStatus=" + purchaseAssetStatus
				+ ", purchaseAssetStatusId=" + purchaseAssetStatusId
				+ ", quote=" + quote + ", isHardWareOrSoftwarePurchased="
				+ isHardWareOrSoftwarePurchased + ", priority=" + priority
				+ ", systemAdminComments=" + systemAdminComments
				+ ", sysAdminName=" + sysAdminName + ", financeHeadName="
				+ financeHeadName + ", buHeadName=" + buHeadName
				+ ", itHeadName=" + itHeadName + ", requestorName="
				+ requestorName + ", reportingManager=" + reportingManager
				+ ", availableFromDate=" + availableFromDate
				+ ", availableToDate=" + availableToDate
				+ ", softwareLicenseOrSubscriptionId="
				+ softwareLicenseOrSubscriptionId + ", lastAssetId="
				+ lastAssetId + ", generatedAssetId=" + generatedAssetId
				+ ", isSupportingDocsAvailable=" + isSupportingDocsAvailable
				+ ", isPoAvailable=" + isPoAvailable + ", isCapexAvailable="
				+ isCapexAvailable + ", isInvoiceAvailable="
				+ isInvoiceAvailable + ", rmComment=" + rmComment
				+ ", sysAdminComment=" + sysAdminComment
				+ ", financeExComment=" + financeExComment + ", buHeadComment="
				+ buHeadComment + ", fkCurrencyType=" + fkCurrencyType
				+ ", fkRequestTypeId=" + fkRequestTypeId
				+ ", purchaseRequestTypeName=" + purchaseRequestTypeName + "]";
	}

}
	