package com.thbs.mis.purchase.bean;

import java.util.Date;

public class ViewAllAssetRequestBean {

	private Integer statusId;
	
	
	private Date startDate;
	
	private Date endDate;
	
	private Integer buId;
	
	private Integer assetId;

	
	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public ViewAllAssetRequestBean(Integer statusId, Integer assetId) {
		super();
		this.statusId = statusId;
		this.assetId = assetId;
	}



	public ViewAllAssetRequestBean(Integer statusId) {
		super();
		this.statusId = statusId;
	}

	public ViewAllAssetRequestBean(Integer statusId, Integer buId, Integer assetId) {
		super();
		this.statusId = statusId;
		this.buId = buId;
		this.assetId = assetId;
	}

	public ViewAllAssetRequestBean(Integer statusId, Date startDate, Date endDate,
			Integer buId, Integer assetId) {
		super();
		this.statusId = statusId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.buId = buId;
		this.assetId = assetId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getBuId() {
		return buId;
	}

	public void setBuId(Integer buId) {
		this.buId = buId;
	}

	public Integer getAssetId() {
		return assetId;
	}

	public ViewAllAssetRequestBean() {
		super();
	}

	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}

	@Override
	public String toString() {
		return "VeiwAssetRequestBean [statusId=" + statusId + ", startDate="
				+ startDate + ", endDate=" + endDate + ", buId=" + buId
				+ ", assetId=" + assetId + "]";
	}
	
	
	
}
