package com.thbs.mis.purchase.bean;

public class SysAdminSettingsOutputBean {
	private Integer systemAdminId;
	private String systenAdminName;
	private String settingType;
	
	
	public String getSettingType() {
		return settingType;
	}
	public void setSettingType(String settingType) {
		this.settingType = settingType;
	}
	public Integer getSystemAdminId() {
		return systemAdminId;
	}
	public void setSystemAdminId(Integer systemAdminId) {
		this.systemAdminId = systemAdminId;
	}
	public String getSystenAdminName() {
		return systenAdminName;
	}
	public void setSystenAdminName(String systenAdminName) {
		this.systenAdminName = systenAdminName;
	}
	@Override
	public String toString() {
		return "SysAdminSettingsOutputBean [systemAdminId=" + systemAdminId
				+ ", systenAdminName=" + systenAdminName + ", settingType="
				+ settingType + "]";
	}
	
	
}
