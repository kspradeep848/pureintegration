package com.thbs.mis.purchase.bean;

public class FileBean {
	
	private String fileName;
	private Integer fileSize;
	public String getFileName() {
		return fileName;
	}
	public Integer getFileSize() {
		return fileSize;
	}
	public void setFileSize(Integer fileSize) {
		this.fileSize = fileSize;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	@Override
	public String toString() {
		return "FileBean [fileName=" + fileName + ", fileSize=" + fileSize
				+ "]";
	}

}
