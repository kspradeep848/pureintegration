package com.thbs.mis.purchase.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;


public class RejectAssetRequestInputBean {

	
	@NumberFormat(style = Style.NUMBER, pattern = "pkPurchaseRequestId should be numeric value")
	@NotNull(message = "pkPurchaseRequestId can not be null")
	@Min(1)
	private Integer pkPurchaseRequestId;
	
	
	@NotBlank(message = "rmComment should not be blank.")
	@NotEmpty(message = "rmComment should not be empty.")
	@NotNull(message = "rmComment should not be null.")
	private String rmComment;
	
	@NumberFormat(style = Style.NUMBER, pattern = "modifiedBy should be numeric value")
	@NotNull(message = "modifiedBy can not be null")
	@Min(1)
	private Integer modifiedBy;
	
	public Integer getPkPurchaseRequestId() {
		return pkPurchaseRequestId;
	}
	public void setPkPurchaseRequestId(Integer pkPurchaseRequestId) {
		this.pkPurchaseRequestId = pkPurchaseRequestId;
	}
	public String getRmComment() {
		return rmComment;
	}
	public void setRmComment(String rmComment) {
		this.rmComment = rmComment;
	}
	
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@Override
	public String toString() {
		return "RejectAssetRequestInputBean [pkPurchaseRequestId="
				+ pkPurchaseRequestId + ", rmComment=" + rmComment
				+ ", modifiedBy=" + modifiedBy + "]";
	}
	
	
	
}
