package com.thbs.mis.purchase.bean;

import java.util.Date;

public class UpdateQuoteInputBean {
	
	private Integer pkQuoteId;
	private double buContribution;
	private String costBorneBy;
	private Date deliveryDate;
	private String fkQuoteStatus;
	private double individualLicenseCost;
	private double totalCost;
	private Integer fkSupplierAddress;
    private Integer modifiedBy;
	private String vendorQuoteReferenceNo;
	private Integer currencyType;
	private String assetDescriptionEnteredBySysadmin;
	
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	
	public String getFkQuoteStatus() {
		return fkQuoteStatus;
	}

	public void setFkQuoteStatus(String fkQuoteStatus) {
		this.fkQuoteStatus = fkQuoteStatus;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Integer getPkQuoteId() {
		return pkQuoteId;
	}

	public void setPkQuoteId(Integer pkQuoteId) {
		this.pkQuoteId = pkQuoteId;
	}

	public double getBuContribution() {
		return buContribution;
	}

	public void setBuContribution(double buContribution) {
		this.buContribution = buContribution;
	}

	public String getCostBorneBy() {
		return costBorneBy;
	}

	public void setCostBorneBy(String costBorneBy) {
		this.costBorneBy = costBorneBy;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}


	public double getIndividualLicenseCost() {
		return individualLicenseCost;
	}

	public void setIndividualLicenseCost(double individualLicenseCost) {
		this.individualLicenseCost = individualLicenseCost;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	public Integer getFkSupplierAddress() {
		return fkSupplierAddress;
	}

	public void setFkSupplierAddress(Integer fkSupplierAddress) {
		this.fkSupplierAddress = fkSupplierAddress;
	}
	
	public String getVendorQuoteReferenceNo() {
		return vendorQuoteReferenceNo;
	}

	public void setVendorQuoteReferenceNo(String vendorQuoteReferenceNo) {
		this.vendorQuoteReferenceNo = vendorQuoteReferenceNo;
	}

	public Integer getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}

	public String getAssetDescriptionEnteredBySysadmin() {
		return assetDescriptionEnteredBySysadmin;
	}

	public void setAssetDescriptionEnteredBySysadmin(
			String assetDescriptionEnteredBySysadmin) {
		this.assetDescriptionEnteredBySysadmin = assetDescriptionEnteredBySysadmin;
	}

	@Override
	public String toString() {
		return "UpdateQuoteInputBean [pkQuoteId=" + pkQuoteId
				+ ", buContribution=" + buContribution + ", costBorneBy="
				+ costBorneBy + ", deliveryDate=" + deliveryDate
				+ ", fkQuoteStatus=" + fkQuoteStatus
				+ ", individualLicenseCost=" + individualLicenseCost
				+ ", totalCost=" + totalCost + ", fkSupplierAddress="
				+ fkSupplierAddress + ", modifiedBy=" + modifiedBy
				+ ", vendorQuoteReferenceNo=" + vendorQuoteReferenceNo
				+ ", currencyType=" + currencyType
				+ ", assetDescriptionEnteredBySysadmin="
				+ assetDescriptionEnteredBySysadmin + "]";
	}

	
}
