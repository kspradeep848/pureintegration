package com.thbs.mis.purchase.bean;

import java.util.Date;

public class PurchaseAssetRequestUpdateInputBean
 {
	private Integer employeeId;
	
	private Integer fkAssetId;
	
	//private Integer fkAssetDetailsId;
	
	private Integer noOfUnits;
	
	private String specifications; 
	
	private String briefDescription;
	
	private Date assetRequiredDate; 
	
	private Integer duration;
	
	private String isClientReimbursing;
	
	private Integer clientId;
	
	private Integer reportingMgrId;
	
	private Date createdDate;
	
	private Integer projectComponentId;
	
	private Integer purchaseRequestId; 
	
	private Date modifiedOn;
	
	private Integer modifiedBy;
	
	private short fkBuId;
	
	//private short locationId;
	
	private int projectId;
	
	public short getFkBuId() {
		return fkBuId;
	}
	public void setFkBuId(short fkBuId) {
		this.fkBuId = fkBuId;
	}
	/*public short getLocationId() {
		return locationId;
	}
	public void setLocationId(short locationId) {
		this.locationId = locationId;
	}*/
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public Integer getFkAssetId() {
		return fkAssetId;
	}
	public void setFkAssetId(Integer fkAssetId) {
		this.fkAssetId = fkAssetId;
	}
	public Integer getNoOfUnits() {
		return noOfUnits;
	}
	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	public String getSpecifications() {
		return specifications;
	}
	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}
	public String getBriefDescription() {
		return briefDescription;
	}
	public void setBriefDescription(String briefDescription) {
		this.briefDescription = briefDescription;
	}
	public Date getAssetRequiredDate() {
		return assetRequiredDate;
	}
	public void setAssetRequiredDate(Date assetRequiredDate) {
		this.assetRequiredDate = assetRequiredDate;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}
	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	public Integer getReportingMgrId() {
		return reportingMgrId;
	}
	public void setReportingMgrId(Integer reportingMgrId) {
		this.reportingMgrId = reportingMgrId;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getProjectComponentId() {
		return projectComponentId;
	}
	public void setProjectComponentId(Integer projectComponentId) {
		this.projectComponentId = projectComponentId;
	}
	public Integer getPurchaseRequestId() {
		return purchaseRequestId;
	}
	public void setPurchaseRequestId(Integer purchaseRequestId) {
		this.purchaseRequestId = purchaseRequestId;
	}
	/*public Integer getPurchaseRequestStatus() {
		return purchaseRequestStatus;
	}
	public void setPurchaseRequestStatus(Integer purchaseRequestStatus) {
		this.purchaseRequestStatus = purchaseRequestStatus;
	}*/
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@Override
	public String toString() {
		return "PurchaseAssetRequestUpdateInputBean [employeeId=" + employeeId
				+ ", fkAssetId=" + fkAssetId + ", noOfUnits=" + noOfUnits
				+ ", specifications=" + specifications + ", briefDescription="
				+ briefDescription + ", assetRequiredDate=" + assetRequiredDate
				+ ", duration=" + duration + ", isClientReimbursing="
				+ isClientReimbursing + ", clientId=" + clientId
				+ ", reportingMgrId=" + reportingMgrId + ", createdDate="
				+ createdDate + ", projectComponentId=" + projectComponentId
				+ ", purchaseRequestId=" + purchaseRequestId + ", modifiedOn="
				+ modifiedOn + ", modifiedBy=" + modifiedBy + ", fkBuId="
				+ fkBuId + ", projectId=" + projectId + "]";
	}
	
		
}
