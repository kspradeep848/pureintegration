package com.thbs.mis.purchase.bean;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;
public class PurchaseAssetRequestDetailOutputBean {
	
	private Integer purchaseRequestId;
	private Integer requestorId;
	private String requestorName;
	private String assetType;
	private Integer assetId;
	//private Integer assetDetailId;
	private String softwareTitleAndVersion;
	private Integer numberOfLincenses;
	private Integer projectComponentId;
	private String componentName;
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date createdDate;
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date assetRequiredBy;
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date modifiedOn;
	private Integer modifiedBy;
	private Integer duration;
	private String briefDescription;
	private Integer purchaseRequestStatus;
	private String statusInfoString;
	private short locationId;
	private String locationName;
	private Integer clientId;
	private String clientName;
	private Integer rmId;
	private String reportingManager;
	private String isClientReimbursing;
	private String specifications;
	private Byte internalFlag;
	private Integer projectId;
	private String quoteFlag;
	private String sysAdminName;
	private String financeHeadName;
	private String buHeadName;
	private String itHeadName;
	private String priority;
	private Integer purchaseRequestTypeId;
	private String purchaseRequestTypeName;
	private String licenceType;
	private String softwareLicenseOrSubscriptionId;
	private String rmComment;
	private String sysAdminComment;
	private String financeExComment;
	private String buHeadComment;
	private Short fkBuId;
	private String buUnitName;
	private List<String> generatedAssetId;
	private List<QuoteDetailsForPOApprovalBean> quoteList;
	private List<FileBean> fileBeanList;
	
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public String getSoftwareTitleAndVersion() {
		return softwareTitleAndVersion;
	}
	public void setSoftwareTitleAndVersion(String softwareTitleAndVersion) {
		this.softwareTitleAndVersion = softwareTitleAndVersion;
	}
	public Integer getNumberOfLincenses() {
		return numberOfLincenses;
	}
	public void setNumberOfLincenses(Integer numberOfLincenses) {
		this.numberOfLincenses = numberOfLincenses;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public Date getAssetRequiredBy() {
		return assetRequiredBy;
	}
	public void setAssetRequiredBy(Date assetRequiredBy) {
		this.assetRequiredBy = assetRequiredBy;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public short getLocationId() {
		return locationId;
	}
	public void setLocationId(short locationId) {
		this.locationId = locationId;
	}
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getReportingManager() {
		return reportingManager;
	}
	public void setReportingManager(String reportingManager) {
		this.reportingManager = reportingManager;
	}
	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}
	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}
	public String getSpecifications() {
		return specifications;
	}
	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}	
	public Integer getAssetId() {
		return assetId;
	}
	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}
	public Byte getInternalFlag() {
		return internalFlag;
	}
	public void setInternalFlag(Byte internalFlag) {
		this.internalFlag = internalFlag;
	}
	public Integer getProjectComponentId() {
		return projectComponentId;
	}
	public void setProjectComponentId(Integer projectComponentId) {
		this.projectComponentId = projectComponentId;
	}
	public String getBriefDescription() {
		return briefDescription;
	}
	public void setBriefDescription(String briefDescription) {
		this.briefDescription = briefDescription;
	}
	public Integer getPurchaseRequestStatus() {
		return purchaseRequestStatus;
	}
	public void setPurchaseRequestStatus(Integer purchaseRequestStatus) {
		this.purchaseRequestStatus = purchaseRequestStatus;
	}
	public Integer getRmId() {
		return rmId;
	}
	public void setRmId(Integer rmId) {
		this.rmId = rmId;
	}
	public String getStatusInfoString() {
		return statusInfoString;
	}
	public void setStatusInfoString(String statusInfoString) {
		this.statusInfoString = statusInfoString;
	}
	
	public Integer getPurchaseRequestId() {
		return purchaseRequestId;
	}
	public void setPurchaseRequestId(Integer purchaseRequestId) {
		this.purchaseRequestId = purchaseRequestId;
	}
	
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	
	public String getRequestorName() {
		return requestorName;
	}
	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}
	public List<FileBean> getFileBeanList() {
		return fileBeanList;
	}
	public void setFileBeanList(List<FileBean> fileBeanList) {
		this.fileBeanList = fileBeanList;
	}
		
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	
	public Integer getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Integer requestorId) {
		this.requestorId = requestorId;
	}
	
	public String getQuoteFlag() {
		return quoteFlag;
	}
	public void setQuoteFlag(String quoteFlag) {
		this.quoteFlag = quoteFlag;
	}
	public String getSysAdminName() {
		return sysAdminName;
	}
	public void setSysAdminName(String sysAdminName) {
		this.sysAdminName = sysAdminName;
	}
	public String getFinanceHeadName() {
		return financeHeadName;
	}
	public void setFinanceHeadName(String financeHeadName) {
		this.financeHeadName = financeHeadName;
	}
	public String getBuHeadName() {
		return buHeadName;
	}
	public void setBuHeadName(String buHeadName) {
		this.buHeadName = buHeadName;
	}
	public String getItHeadName() {
		return itHeadName;
	}
	public void setItHeadName(String itHeadName) {
		this.itHeadName = itHeadName;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public Integer getPurchaseRequestTypeId() {
		return purchaseRequestTypeId;
	}
	public void setPurchaseRequestTypeId(Integer purchaseRequestTypeId) {
		this.purchaseRequestTypeId = purchaseRequestTypeId;
	}
	public String getPurchaseRequestTypeName() {
		return purchaseRequestTypeName;
	}
	public void setPurchaseRequestTypeName(String purchaseRequestTypeName) {
		this.purchaseRequestTypeName = purchaseRequestTypeName;
	}
	public String getLicenceType() {
		return licenceType;
	}
	public void setLicenceType(String licenceType) {
		this.licenceType = licenceType;
	}
	public String getSoftwareLicenseOrSubscriptionId() {
		return softwareLicenseOrSubscriptionId;
	}
	public void setSoftwareLicenseOrSubscriptionId(
			String softwareLicenseOrSubscriptionId) {
		this.softwareLicenseOrSubscriptionId = softwareLicenseOrSubscriptionId;
	}
	public String getRmComment() {
		return rmComment;
	}
	public void setRmComment(String rmComment) {
		this.rmComment = rmComment;
	}
	public String getSysAdminComment() {
		return sysAdminComment;
	}
	public void setSysAdminComment(String sysAdminComment) {
		this.sysAdminComment = sysAdminComment;
	}
	public String getFinanceExComment() {
		return financeExComment;
	}
	public void setFinanceExComment(String financeExComment) {
		this.financeExComment = financeExComment;
	}
	public String getBuHeadComment() {
		return buHeadComment;
	}
	public void setBuHeadComment(String buHeadComment) {
		this.buHeadComment = buHeadComment;
	}
	public List<QuoteDetailsForPOApprovalBean> getQuoteList() {
		return quoteList;
	}
	public void setQuoteList(List<QuoteDetailsForPOApprovalBean> quoteList) {
		this.quoteList = quoteList;
	}
	public List<String> getGeneratedAssetId() {
		return generatedAssetId;
	}
	public void setGeneratedAssetId(List<String> generatedAssetId) {
		this.generatedAssetId = generatedAssetId;
	}
	public Short getFkBuId() {
		return fkBuId;
	}
	public void setFkBuId(Short fkBuId) {
		this.fkBuId = fkBuId;
	}
	public String getBuUnitName() {
		return buUnitName;
	}
	public void setBuUnitName(String buUnitName) {
		this.buUnitName = buUnitName;
	}
	@Override
	public String toString() {
		return "PurchaseAssetRequestDetailOutputBean [purchaseRequestId="
				+ purchaseRequestId + ", requestorId=" + requestorId
				+ ", requestorName=" + requestorName + ", assetType="
				+ assetType + ", assetId=" + assetId
				+ ", softwareTitleAndVersion=" + softwareTitleAndVersion
				+ ", numberOfLincenses=" + numberOfLincenses
				+ ", projectComponentId=" + projectComponentId
				+ ", componentName=" + componentName + ", createdDate="
				+ createdDate + ", assetRequiredBy=" + assetRequiredBy
				+ ", modifiedOn=" + modifiedOn + ", modifiedBy=" + modifiedBy
				+ ", duration=" + duration + ", briefDescription="
				+ briefDescription + ", purchaseRequestStatus="
				+ purchaseRequestStatus + ", statusInfoString="
				+ statusInfoString + ", locationId=" + locationId
				+ ", locationName=" + locationName + ", clientId=" + clientId
				+ ", clientName=" + clientName + ", rmId=" + rmId
				+ ", reportingManager=" + reportingManager
				+ ", isClientReimbursing=" + isClientReimbursing
				+ ", specifications=" + specifications + ", internalFlag="
				+ internalFlag + ", projectId=" + projectId + ", quoteFlag="
				+ quoteFlag + ", sysAdminName=" + sysAdminName
				+ ", financeHeadName=" + financeHeadName + ", buHeadName="
				+ buHeadName + ", itHeadName=" + itHeadName + ", priority="
				+ priority + ", purchaseRequestTypeId=" + purchaseRequestTypeId
				+ ", purchaseRequestTypeName=" + purchaseRequestTypeName
				+ ", licenceType=" + licenceType
				+ ", softwareLicenseOrSubscriptionId="
				+ softwareLicenseOrSubscriptionId + ", rmComment=" + rmComment
				+ ", sysAdminComment=" + sysAdminComment
				+ ", financeExComment=" + financeExComment + ", buHeadComment="
				+ buHeadComment + ", fkBuId=" + fkBuId + ", buUnitName="
				+ buUnitName + ", generatedAssetId=" + generatedAssetId
				+ ", quoteList=" + quoteList + ", fileBeanList=" + fileBeanList
				+ "]";
	}
}	