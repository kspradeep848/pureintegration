package com.thbs.mis.purchase.bean;

public class ClientAndProjectDetailsInputBean 
{
	private Integer clientId;
	private Integer projectId;
	private Integer componentId;
	private String legacyUrl;
	
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public Integer getComponentId() {
		return componentId;
	}
	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}
	public String getLegacyUrl() {
		return legacyUrl;
	}
	public void setLegacyUrl(String legacyUrl) {
		this.legacyUrl = legacyUrl;
	}
	@Override
	public String toString() {
		return "ClientAndProjectDetailsInputBean [clientId=" + clientId
				+ ", projectId=" + projectId + ", componentId=" + componentId
				+ ", legacyUrl=" + legacyUrl + "]";
	}
	
}
