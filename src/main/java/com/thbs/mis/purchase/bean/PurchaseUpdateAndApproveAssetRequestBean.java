package com.thbs.mis.purchase.bean;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseUpdateAndApproveAssetRequestBean {
	
	@NumberFormat(style = Style.NUMBER, pattern = "purchaseRequestId should be numeric value")
	@Min(1)
	@NotNull(message = "purchaseRequestId field can not be null")
	private Integer purchaseRequestId;
	
	@NumberFormat(style = Style.NUMBER, pattern = "assetId should be numeric value")
	@Min(1)
	@NotNull(message = "assetId field can not be null")
	private Integer assetId;
	
	@NumberFormat(style = Style.NUMBER, pattern = "assetId should be numeric value")
	@Min(1)
	@NotNull(message = "assetId field can not be null")
	private Integer assetDetailId;
	
	@NumberFormat(style = Style.NUMBER, pattern = "numberOfLicenses should be numeric value")
	@NotNull(message = "numberOfLicenses field can not be null")
	@Min(1)
	private Integer numberOfLicenses;
	
	@NumberFormat(style = Style.NUMBER, pattern = "projectComponent should be numeric value")
	@NotNull(message = "projectComponent field can not be null")
	@Min(1)
	private Integer projectComponent;
	
	@JsonSerialize(using = DateSerializer.class)
    @NotNull(message = "assetRequiredBy field can not be null.")
	private Date assetRequiredBy;
	
	@NumberFormat(style = Style.NUMBER, pattern = "duration should be numeric value")
	@NotNull(message = "duration field can not be null")
	@Min(1)
	private Integer duration;
	
	@NotBlank(message = "description should not be blank.")
	@NotEmpty(message = "description Description should not be empty.")
	@NotNull(message = "description Description should not be null.")
	private String description;
	
	@NumberFormat(style = Style.NUMBER, pattern = "modifiedBy should be numeric value")
	@NotNull(message = "modifiedBy field can not be null")
	@Min(1)
	private Integer modifiedBy;
	
	@NumberFormat(style = Style.NUMBER, pattern = "clientId should be numeric value")
	@Min(1)
	@NotNull(message = "clientId field can not be null")
	private Integer clientId;
	
	public Integer getPurchaseRequestId() {
		return purchaseRequestId;
	}
	public void setPurchaseRequestId(Integer purchaseRequestId) {
		this.purchaseRequestId = purchaseRequestId;
	}
	
	public Integer getNumberOfLicenses() {
		return numberOfLicenses;
	}
	public void setNumberOfLicenses(Integer numberOfLicenses) {
		this.numberOfLicenses = numberOfLicenses;
	}
	public Integer getProjectComponent() {
		return projectComponent;
	}
	public void setProjectComponent(Integer projectComponent) {
		this.projectComponent = projectComponent;
	}
	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getAssetRequiredBy() {
		return assetRequiredBy;
	}
	public void setAssetRequiredBy(Date assetRequiredBy) {
		this.assetRequiredBy = assetRequiredBy;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	public Integer getAssetId() {
		return assetId;
	}
	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}
	public Integer getAssetDetailId() {
		return assetDetailId;
	}
	public void setAssetDetailId(Integer assetDetailId) {
		this.assetDetailId = assetDetailId;
	}
	
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@Override
	public String toString() {
		return "PurchaseUpdateAndApproveAssetRequestBean [purchaseRequestId="
				+ purchaseRequestId + ", assetId=" + assetId
				+ ", assetDetailId=" + assetDetailId + ", numberOfLicenses="
				+ numberOfLicenses + ", projectComponent=" + projectComponent
				+ ", assetRequiredBy=" + assetRequiredBy + ", duration="
				+ duration + ", description=" + description + ", modifiedBy="
				+ modifiedBy + ", clientId=" + clientId + "]";
	}
	
}
