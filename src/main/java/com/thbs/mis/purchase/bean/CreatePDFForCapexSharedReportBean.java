package com.thbs.mis.purchase.bean;

import java.util.Date;

public class CreatePDFForCapexSharedReportBean {
	
	private Date createdDate;
	private String capexNumber;
	private String costCenter;
	private String buName;
	private String buProjectName;
	private String productDetails;
	private Double cost;
	private String locationName;
	private String purpose;
	private String year;
	private String componentNumber;
	private Integer numberOfUnits;
	private String itBuName;
	private String itBuProjectName;
	private String itComponentNumber;
	private String buHeadName;
	private String buItHeadName;
	
	//Added by Shyam for BU Contribution
	private Double buContribution;
	private String currencyType;
	//Added by Shyam 
	private String financeHeadName;
	
	public String getBuItHeadName() {
		return buItHeadName;
	}
	public void setBuItHeadName(String buItHeadName) {
		this.buItHeadName = buItHeadName;
	}
	public String getBuHeadName() {
		return buHeadName;
	}
	public void setBuHeadName(String buHeadName) {
		this.buHeadName = buHeadName;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getCapexNumber() {
		return capexNumber;
	}
	public void setCapexNumber(String capexNumber) {
		this.capexNumber = capexNumber;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getBuName() {
		return buName;
	}
	public void setBuName(String buName) {
		this.buName = buName;
	}
	public String getBuProjectName() {
		return buProjectName;
	}
	public void setBuProjectName(String buProjectName) {
		this.buProjectName = buProjectName;
	}
	public String getProductDetails() {
		return productDetails;
	}
	public void setProductDetails(String productDetails) {
		this.productDetails = productDetails;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getComponentNumber() {
		return componentNumber;
	}
	public void setComponentNumber(String componentNumber) {
		this.componentNumber = componentNumber;
	}
	public Integer getNumberOfUnits() {
		return numberOfUnits;
	}
	public void setNumberOfUnits(Integer numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}
	public String getItBuName() {
		return itBuName;
	}
	public void setItBuName(String itBuName) {
		this.itBuName = itBuName;
	}
	public String getItBuProjectName() {
		return itBuProjectName;
	}
	public void setItBuProjectName(String itBuProjectName) {
		this.itBuProjectName = itBuProjectName;
	}
	public String getItComponentNumber() {
		return itComponentNumber;
	}
	public void setItComponentNumber(String itComponentNumber) {
		this.itComponentNumber = itComponentNumber;
	}
	public Double getBuContribution() {
		return buContribution;
	}
	public void setBuContribution(Double buContribution) {
		this.buContribution = buContribution;
	}
	public String getFinanceHeadName() {
		return financeHeadName;
	}
	public void setFinanceHeadName(String financeHeadName) {
		this.financeHeadName = financeHeadName;
	}
	public String getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}
	@Override
	public String toString() {
		return "CreatePDFForCapexSharedReportBean [createdDate=" + createdDate
				+ ", capexNumber=" + capexNumber + ", costCenter=" + costCenter
				+ ", buName=" + buName + ", buProjectName=" + buProjectName
				+ ", productDetails=" + productDetails + ", cost=" + cost
				+ ", locationName=" + locationName + ", purpose=" + purpose
				+ ", year=" + year + ", componentNumber=" + componentNumber
				+ ", numberOfUnits=" + numberOfUnits + ", itBuName=" + itBuName
				+ ", itBuProjectName=" + itBuProjectName
				+ ", itComponentNumber=" + itComponentNumber + ", buHeadName="
				+ buHeadName + ", buItHeadName=" + buItHeadName
				+ ", buContribution=" + buContribution + ", currencyType="
				+ currencyType + ", financeHeadName=" + financeHeadName + "]";
	}
	
	
}
