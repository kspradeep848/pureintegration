package com.thbs.mis.purchase.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

//Added by Prathibha for Reject Asset request by System Admin

public class RejectAssetReqBySysAdminInputBean {

	/*****************Columns from purchase_dat_asset_request table will be updated******************/
	
	@NotNull
	@Min(1)
	private Integer pkPurchaseRequestId;
	
    @Employee_Id_Not_Found
	private Integer modifiedBy;
	
	@NotEmpty
	private String sysAdminComment;
	
	
	public Integer getPkPurchaseRequestId() {
		return pkPurchaseRequestId;
	}


	public void setPkPurchaseRequestId(Integer pkPurchaseRequestId) {
		this.pkPurchaseRequestId = pkPurchaseRequestId;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}


	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	

	public String getSysAdminComment() {
		return sysAdminComment;
	}


	public void setSysAdminComment(String sysAdminComment) {
		this.sysAdminComment = sysAdminComment;
	}


	@Override
	public String toString() {
		return "RejectAssetReqBySysAdminInputBean [pkPurchaseRequestId="
				+ pkPurchaseRequestId + ", modifiedBy=" + modifiedBy
				+ ", sysAdminComment=" + sysAdminComment + "]";
	}


	/*public Integer getSysAdminId() {
		return sysAdminId;
	}


	public void setSysAdminId(Integer sysAdminId) {
		this.sysAdminId = sysAdminId;
	}*/


	
	


	/*public Date getSysAdminUpdatedOn() {
		return sysAdminUpdatedOn;
	}


	public void setSysAdminUpdatedOn(Date sysAdminUpdatedOn) {
		this.sysAdminUpdatedOn = sysAdminUpdatedOn;
	}
*/

	
	
}
