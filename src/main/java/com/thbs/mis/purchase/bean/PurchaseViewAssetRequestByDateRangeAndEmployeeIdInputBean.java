package com.thbs.mis.purchase.bean;

import java.util.Date;

public class PurchaseViewAssetRequestByDateRangeAndEmployeeIdInputBean 
{
	private Date fromDate;
	private Date toDate;
	private Integer requestorId;
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Integer getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Integer requestorId) {
		this.requestorId = requestorId;
	}
	@Override
	public String toString() {
		return "PurchaseViewAssetRequestByDateRangeAndEmployeeIdInputBean [fromDate="
				+ fromDate
				+ ", toDate="
				+ toDate
				+ ", requestorId="
				+ requestorId + "]";
	}
	
}
