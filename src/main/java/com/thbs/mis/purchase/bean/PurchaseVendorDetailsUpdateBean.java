package com.thbs.mis.purchase.bean;

import java.math.BigInteger;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class PurchaseVendorDetailsUpdateBean {

	@NumberFormat(style = Style.NUMBER, pattern = "pkSupplierId should be numeric value")
	@NotNull(message = "pkSupplierId can not be null")
	@Min(1)
	private Integer pkSupplierId;
	
	@NotBlank(message = "vendorName should not be blank.")
	@NotEmpty(message = "vendorName should not be empty.")
	@NotNull(message = "vendorName should not be null.")
	private String vendorName;
	
	@NotBlank(message = "vendorAddress should not be blank.")
	@NotEmpty(message = "vendorAddress should not be empty.")
	@NotNull(message = "vendorAddress should not be null.")
	private String vendorAddress;
	
	
	@NumberFormat(style = Style.NUMBER, pattern = "priceQuotedByVendor should be numeric value")
	@NotNull(message = "priceQuotedByVendor can not be null")
	@Min(1)
	private double priceQuotedByVendor;
	
	@NotBlank(message = "vendorContactNo should not be blank.")
	@NotEmpty(message = "vendorContactNo should not be empty.")
	@NotNull(message = "vendorContactNo should not be null.")
	private String vendorContactNo;
	
	@NumberFormat(style = Style.NUMBER, pattern = "deliveryTime should be numeric value")
	@NotNull(message = "deliveryTime can not be null")
	@Min(1)
	private int deliveryTime;
	
	@NotBlank(message = "contactPerson should not be blank.")
	@NotEmpty(message = "contactPerson should not be empty.")
	@NotNull(message = "contactPerson should not be null.")
	private String contactPerson;
	
	@NotBlank(message = "emailId should not be blank.")
	@NotEmpty(message = "emailId should not be empty.")
	@NotNull(message = "emailId should not be null.")
	private String emailId;
	
	
	private int modifiedby;


	public Integer getPkSupplierId() {
		return pkSupplierId;
	}


	public void setPkSupplierId(Integer pkSupplierId) {
		this.pkSupplierId = pkSupplierId;
	}


	public String getVendorName() {
		return vendorName;
	}


	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}


	public String getVendorAddress() {
		return vendorAddress;
	}


	public void setVendorAddress(String vendorAddress) {
		this.vendorAddress = vendorAddress;
	}


	public double getPriceQuotedByVendor() {
		return priceQuotedByVendor;
	}


	public void setPriceQuotedByVendor(double priceQuotedByVendor) {
		this.priceQuotedByVendor = priceQuotedByVendor;
	}


	public String getVendorContactNo() {
		return vendorContactNo;
	}


	public void setVendorContactNo(String vendorContactNo) {
		this.vendorContactNo = vendorContactNo;
	}


	public int getDeliveryTime() {
		return deliveryTime;
	}


	public void setDeliveryTime(int deliveryTime) {
		this.deliveryTime = deliveryTime;
	}


	public int getModifiedby() {
		return modifiedby;
	}


	public void setModifiedby(int modifiedby) {
		this.modifiedby = modifiedby;
	}
	
	
	public String getContactPerson() {
		return contactPerson;
	}


	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}


	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	@Override
	public String toString() {
		return "PurchaseVendorDetailsUpdateBean [pkSupplierId=" + pkSupplierId
				+ ", vendorName=" + vendorName + ", vendorAddress="
				+ vendorAddress + ", priceQuotedByVendor="
				+ priceQuotedByVendor + ", vendorContactNo=" + vendorContactNo
				+ ", deliveryTime=" + deliveryTime + ", contactPerson="
				+ contactPerson + ", emailId=" + emailId + ", modifiedby="
				+ modifiedby + "]";
	}
	
	
	
}
