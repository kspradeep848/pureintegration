package com.thbs.mis.purchase.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurcahseQuotesOutPutBean {

	
	private Integer pkQuoteId;
	private Double buContribution;
	private String costBorneBy;
	private Date createdDate;
	private Date deliveryDate;
	private Double individualLicenseCost;
	private Date modifiedDate;
	private Double totalCost;
	private Integer fkPurchaseRequestId;
	private Integer fkSupplierAddress;
	private String quoteStatus;
	private Integer modifiedBy;
	private int createdBy;
	private String vendorName;
	private String vendorContactNo;
	private String assetDescriptionEnteredBySysadmin;
	
	public String getVendorContactNo() {
		return vendorContactNo;
	}
	public void setVendorContactNo(String vendorContactNo) {
		this.vendorContactNo = vendorContactNo;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public Integer getPkQuoteId() {
		return pkQuoteId;
	}
	public void setPkQuoteId(Integer pkQuoteId) {
		this.pkQuoteId = pkQuoteId;
	}
	public Double getBuContribution() {
		return buContribution;
	}
	public void setBuContribution(Double buContribution) {
		this.buContribution = buContribution;
	}
	public String getCostBorneBy() {
		return costBorneBy;
	}
	public void setCostBorneBy(String costBorneBy) {
		this.costBorneBy = costBorneBy;
	}
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Double getIndividualLicenseCost() {
		return individualLicenseCost;
	}
	public void setIndividualLicenseCost(Double individualLicenseCost) {
		this.individualLicenseCost = individualLicenseCost;
	}
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	public Integer getFkPurchaseRequestId() {
		return fkPurchaseRequestId;
	}
	public void setFkPurchaseRequestId(Integer fkPurchaseRequestId) {
		this.fkPurchaseRequestId = fkPurchaseRequestId;
	}
	public Integer getFkSupplierAddress() {
		return fkSupplierAddress;
	}
	public void setFkSupplierAddress(Integer fkSupplierAddress) {
		this.fkSupplierAddress = fkSupplierAddress;
	}
	public String getQuoteStatus() {
		return quoteStatus;
	}
	public void setQuoteStatus(String quoteStatus) {
		this.quoteStatus = quoteStatus;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	public String getAssetDescriptionEnteredBySysadmin() {
		return assetDescriptionEnteredBySysadmin;
	}
	public void setAssetDescriptionEnteredBySysadmin(
			String assetDescriptionEnteredBySysadmin) {
		this.assetDescriptionEnteredBySysadmin = assetDescriptionEnteredBySysadmin;
	}
	@Override
	public String toString() {
		return "PurcahseQuotesOutPutBean [pkQuoteId=" + pkQuoteId
				+ ", buContribution=" + buContribution + ", costBorneBy="
				+ costBorneBy + ", createdDate=" + createdDate
				+ ", deliveryDate=" + deliveryDate + ", individualLicenseCost="
				+ individualLicenseCost + ", modifiedDate=" + modifiedDate
				+ ", totalCost=" + totalCost + ", fkPurchaseRequestId="
				+ fkPurchaseRequestId + ", fkSupplierAddress="
				+ fkSupplierAddress + ", quoteStatus=" + quoteStatus
				+ ", modifiedBy=" + modifiedBy + ", createdBy=" + createdBy
				+ ", vendorName=" + vendorName + ", vendorContactNo="
				+ vendorContactNo + ", assetDescriptionEnteredBySysadmin="
				+ assetDescriptionEnteredBySysadmin + "]";
	}
		
}
