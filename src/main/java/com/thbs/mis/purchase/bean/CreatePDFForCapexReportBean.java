package com.thbs.mis.purchase.bean;

import java.util.Date;

public class CreatePDFForCapexReportBean {

	private Date createdDate;
	private String capexNumber;
	private String costCenter;
	private String buName;
	private String projectName;
	private String productDetails;
	private Double cost;
	private String locationName;
	private String purpose;
	private String currencyType;
	//private String year;
	private String componentNumber;
	private Integer numberOfUnits;
	private String buHeadName;
	private String itHeadName;
	private String financeHeadName;


	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getCapexNumber() {
		return capexNumber;
	}
	public void setCapexNumber(String capexNumber) {
		this.capexNumber = capexNumber;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getBuName() {
		return buName;
	}
	public void setBuName(String buName) {
		this.buName = buName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProductDetails() {
		return productDetails;
	}
	public void setProductDetails(String productDetails) {
		this.productDetails = productDetails;
	}


	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {

		this.cost = cost;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getComponentNumber() {
		return componentNumber;
	}
	public void setComponentNumber(String componentNumber) {
		this.componentNumber = componentNumber;
	}


	public Integer getNumberOfUnits() {
		return numberOfUnits;
	}

	public void setNumberOfUnits(Integer numberOfUnits) {

		this.numberOfUnits = numberOfUnits;
	}
	public String getBuHeadName() {
		return buHeadName;
	}
	public void setBuHeadName(String buHeadName) {
		this.buHeadName = buHeadName;
	}
	public String getItHeadName() {
		return itHeadName;
	}
	public void setItHeadName(String itHeadName) {
		this.itHeadName = itHeadName;
	}
	public String getFinanceHeadName() {
		return financeHeadName;
	}
	public void setFinanceHeadName(String financeHeadName) {
		this.financeHeadName = financeHeadName;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	@Override
	public String toString() {
		return "CreatePDFForCapexReportBean [createdDate=" + createdDate
				+ ", capexNumber=" + capexNumber + ", costCenter=" + costCenter
				+ ", buName=" + buName + ", projectName=" + projectName
				+ ", productDetails=" + productDetails + ", cost=" + cost
				+ ", locationName=" + locationName + ", purpose=" + purpose
				+ ", currencyType=" + currencyType + ", componentNumber="
				+ componentNumber + ", numberOfUnits=" + numberOfUnits
				+ ", buHeadName=" + buHeadName + ", itHeadName=" + itHeadName
				+ ", financeHeadName=" + financeHeadName + "]";
	}
	
	
}