package com.thbs.mis.purchase.bean;

import java.math.BigInteger;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseDatSupplierDetailsOutPutBean {

	
	private Integer pkSupplierId;
	private String contactPerson;
	private Integer createdBy;
	private Date createdDate;
	private String emailId;
	private String isAvailable;
	private Integer modifiedBy;
	private Date modifiedOn;
	private double priceQuotedByVendor;
	private String vendorAddress;
	private String vendorContactNo;
	private String vendorName;
	private Integer fkAssetId;
	
	private Integer deliveryTime;
	
	
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public void setFkAssetId(Integer fkAssetId) {
		this.fkAssetId = fkAssetId;
	}
	public Integer getPkSupplierId() {
		return pkSupplierId;
	}
	public void setPkSupplierId(Integer pkSupplierId) {
		this.pkSupplierId = pkSupplierId;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getIsAvailable() {
		return isAvailable;
	}
	public void setIsAvailable(String isAvailable) {
		this.isAvailable = isAvailable;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public double getPriceQuotedByVendor() {
		return priceQuotedByVendor;
	}
	public void setPriceQuotedByVendor(double priceQuotedByVendor) {
		this.priceQuotedByVendor = priceQuotedByVendor;
	}
	public String getVendorAddress() {
		return vendorAddress;
	}
	public void setVendorAddress(String vendorAddress) {
		this.vendorAddress = vendorAddress;
	}
	public String getVendorContactNo() {
		return vendorContactNo;
	}
	public void setVendorContactNo(String vendorContactNo) {
		this.vendorContactNo = vendorContactNo;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public int getFkAssetId() {
		return fkAssetId;
	}
	public void setFkAssetId(int fkAssetId) {
		this.fkAssetId = fkAssetId;
	}

	public Integer getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(Integer deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	
	@Override
	public String toString() {
		return "PurchaseDatSupplierDetailsOutPutBean [pkSupplierId="
				+ pkSupplierId + ", contactPerson=" + contactPerson
				+ ", createdBy=" + createdBy + ", createdDate=" + createdDate
				+ ", emailId=" + emailId + ", isAvailable=" + isAvailable
				+ ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn
				+ ", priceQuotedByVendor=" + priceQuotedByVendor
				+ ", vendorAddress=" + vendorAddress + ", vendorContactNo="
				+ vendorContactNo + ", vendorName=" + vendorName
				+ ", fkAssetId=" + fkAssetId + ", deliveryTime=" + deliveryTime
				+ "]";
	}
	
	
}
