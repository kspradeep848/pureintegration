package com.thbs.mis.purchase.bean;

import java.math.BigInteger;

public class AddquoteUpdateVendordetailInputBean {
	
	
	private Integer supplierId;
	private String vendorName;
	private String vendorAddress;
	private String vendorContactNo;
	
	
	public Integer getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorAddress() {
		return vendorAddress;
	}
	public void setVendorAddress(String vendorAddress) {
		this.vendorAddress = vendorAddress;
	}
	public String getVendorContactNo() {
		return vendorContactNo;
	}
	public void setVendorContactNo(String vendorContactNo) {
		this.vendorContactNo = vendorContactNo;
	}
	@Override
	public String toString() {
		return "AddquoteUpdateVendordetailInputBean [supplierId=" + supplierId
				+ ", vendorName=" + vendorName + ", vendorAddress="
				+ vendorAddress + ", vendorContactNo=" + vendorContactNo + "]";
	}
	
	
}
