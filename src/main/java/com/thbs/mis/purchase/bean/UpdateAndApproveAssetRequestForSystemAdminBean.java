package com.thbs.mis.purchase.bean;



public class UpdateAndApproveAssetRequestForSystemAdminBean {
	
	private String isClientReimbursing;	
	private Integer clientId;	
	private Integer projectComponentId;	
	private Integer purchaseRequestId; 	
	private Integer modifiedBy;	
	private Integer projectId;
	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}
	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	public Integer getProjectComponentId() {
		return projectComponentId;
	}
	public void setProjectComponentId(Integer projectComponentId) {
		this.projectComponentId = projectComponentId;
	}
	public Integer getPurchaseRequestId() {
		return purchaseRequestId;
	}
	public void setPurchaseRequestId(Integer purchaseRequestId) {
		this.purchaseRequestId = purchaseRequestId;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	@Override
	public String toString() {
		return "UpdateAndApproveAssetRequestForSystemAdminBean [isClientReimbursing="
				+ isClientReimbursing
				+ ", clientId="
				+ clientId
				+ ", projectComponentId="
				+ projectComponentId
				+ ", purchaseRequestId="
				+ purchaseRequestId
				+ ", modifiedBy="
				+ modifiedBy + ", projectId=" + projectId + "]";
	}
	
}