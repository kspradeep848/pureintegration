package com.thbs.mis.purchase.bean;

import java.util.Date;

public class PurchasePOPdfGenerationBean {
	private String poNumber;
	private Date createdDate;
	private String thbsAddress;
	private String vendorAddress;
	private String thbsRepresentative;
	private String vendorRepresentative;
	private String thbsPhoneNumber;
	private String vendorPhoneNumber;
	private String referenceQuoteNumber;
	private String description;
	private Integer quantity;
	private Double unitPrice;
	private Double amount;
	private Double grandTotal;
	private String amountInWords;
	private String vendorName;
	private String poCreatorName;
	private String currencyType;
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getThbsAddress() {
		return thbsAddress;
	}
	public void setThbsAddress(String thbsAddress) {
		this.thbsAddress = thbsAddress;
	}
	public String getVendorAddress() {
		return vendorAddress;
	}
	public void setVendorAddress(String vendorAddress) {
		this.vendorAddress = vendorAddress;
	}
	public String getThbsRepresentative() {
		return thbsRepresentative;
	}
	public void setThbsRepresentative(String thbsRepresentative) {
		this.thbsRepresentative = thbsRepresentative;
	}
	public String getVendorRepresentative() {
		return vendorRepresentative;
	}
	public void setVendorRepresentative(String vendorRepresentative) {
		this.vendorRepresentative = vendorRepresentative;
	}
	public String getThbsPhoneNumber() {
		return thbsPhoneNumber;
	}
	public void setThbsPhoneNumber(String thbsPhoneNumber) {
		this.thbsPhoneNumber = thbsPhoneNumber;
	}
	public String getVendorPhoneNumber() {
		return vendorPhoneNumber;
	}
	public void setVendorPhoneNumber(String vendorPhoneNumber) {
		this.vendorPhoneNumber = vendorPhoneNumber;
	}
	public String getReferenceQuoteNumber() {
		return referenceQuoteNumber;
	}
	public void setReferenceQuoteNumber(String referenceQuoteNumber) {
		this.referenceQuoteNumber = referenceQuoteNumber;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}
	public String getAmountInWords() {
		return amountInWords;
	}
	public void setAmountInWords(String amountInWords) {
		this.amountInWords = amountInWords;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getPoCreatorName() {
		return poCreatorName;
	}
	public void setPoCreatorName(String poCreatorName) {
		this.poCreatorName = poCreatorName;
	}
	public String getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}
	@Override
	public String toString() {
		return "PurchasePOPdfGenerationBean [poNumber=" + poNumber
				+ ", createdDate=" + createdDate + ", thbsAddress="
				+ thbsAddress + ", vendorAddress=" + vendorAddress
				+ ", thbsRepresentative=" + thbsRepresentative
				+ ", vendorRepresentative=" + vendorRepresentative
				+ ", thbsPhoneNumber=" + thbsPhoneNumber
				+ ", vendorPhoneNumber=" + vendorPhoneNumber
				+ ", referenceQuoteNumber=" + referenceQuoteNumber
				+ ", description=" + description + ", quantity=" + quantity
				+ ", unitPrice=" + unitPrice + ", amount=" + amount
				+ ", grandTotal=" + grandTotal + ", amountInWords="
				+ amountInWords + ", vendorName=" + vendorName
				+ ", poCreatorName=" + poCreatorName + ", currencyType="
				+ currencyType + "]";
	}
	
}
