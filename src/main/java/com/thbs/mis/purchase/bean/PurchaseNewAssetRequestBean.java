package com.thbs.mis.purchase.bean;

import java.util.Date;

public class PurchaseNewAssetRequestBean 
{
	private Integer employeeId;
	private Integer fkAssetId;
	private Integer fkAssetDetailsId; //yet not confirmed
	private Integer noOfUnits;
	private String specifications; //not compulsory for s/w
	private String briefDescription;
	private Date assetRequiredBy;
	private Integer duration;
	private String isClientReimbursing;
	private Integer clientId;
	private Integer reportingMgrId;
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public Integer getFkAssetId() {
		return fkAssetId;
	}
	public void setFkAssetId(Integer fkAssetId) {
		this.fkAssetId = fkAssetId;
	}
	public Integer getFkAssetDetailsId() {
		return fkAssetDetailsId;
	}
	public void setFkAssetDetailsId(Integer fkAssetDetailsId) {
		this.fkAssetDetailsId = fkAssetDetailsId;
	}
	public Integer getNoOfUnits() {
		return noOfUnits;
	}
	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	public String getSpecifications() {
		return specifications;
	}
	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}
	public String getBriefDescription() {
		return briefDescription;
	}
	public void setBriefDescription(String briefDescription) {
		this.briefDescription = briefDescription;
	}
	public Date getAssetRequiredBy() {
		return assetRequiredBy;
	}
	public void setAssetRequiredBy(Date assetRequiredBy) {
		this.assetRequiredBy = assetRequiredBy;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}
	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	public Integer getReportingMgrId() {
		return reportingMgrId;
	}
	public void setReportingMgrId(Integer reportingMgrId) {
		this.reportingMgrId = reportingMgrId;
	}
	@Override
	public String toString() {
		return "PurchaseNewAssetRequestBean [employeeId=" + employeeId
				+ ", fkAssetId=" + fkAssetId + ", fkAssetDetailsId="
				+ fkAssetDetailsId + ", noOfUnits=" + noOfUnits
				+ ", specifications=" + specifications + ", briefDescription="
				+ briefDescription + ", assetRequiredBy=" + assetRequiredBy
				+ ", duration=" + duration + ", isClientReimbursing="
				+ isClientReimbursing + ", clientId=" + clientId
				+ ", reportingMgrId=" + reportingMgrId + "]";
	}
	
	
}
