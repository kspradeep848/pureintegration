/**
 * 
 */
package com.thbs.mis.purchase.bean;

import java.util.List;

/**
 * @author annapoorani_n
 *
 */
public class PurchaseAssetAndVendorInfoBean {
	
	private Integer assetId;
	
	private String assetType;
	
	private String licenceType;
	
	private String assetVersionAndModel;
	
	List<PurchaseDatSupplierDetailsOutPutBean> vendorDetails;

	public Integer getAssetId() {
		return assetId;
	}

	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public String getLicenceType() {
		return licenceType;
	}

	public void setLicenceType(String licenceType) {
		this.licenceType = licenceType;
	}

	public List<PurchaseDatSupplierDetailsOutPutBean> getVendorDetails() {
		return vendorDetails;
	}

	public void setVendorDetails(List<PurchaseDatSupplierDetailsOutPutBean> vendorDetails) {
		this.vendorDetails = vendorDetails;
	}
		

	public String getAssetVersionAndModel() {
		return assetVersionAndModel;
	}

	public void setAssetVersionAndModel(String assetVersionAndModel) {
		this.assetVersionAndModel = assetVersionAndModel;
	}

	@Override
	public String toString() {
		return "PurchaseAssetAndVendorInfoBean [assetId=" + assetId
				+ ", assetType=" + assetType + ", licenceType=" + licenceType
				+ ", assetVersionAndModel=" + assetVersionAndModel
				+ ", vendorDetails=" + vendorDetails + "]";
	}	
	

}
