package com.thbs.mis.purchase.bean;

public class PurchaseApproveCapexOutputBean {
	
	private String capexNumber;
	private String poNumber;
	private String castCenter;
	private String projectName; 
	private String softwareTitleAndVersion;
	private String specifications;
	private String assetType;
	private Short locationId;
	private String location;
	private String isClientReimbursing;
	private String purpose;
	private Double individualLicenseCost;
	private Double totalCost;
	private String costBorneBy;
	private Double buContribution;
	public String getCapexNumber() {
		return capexNumber;
	}
	public void setCapexNumber(String capexNumber) {
		this.capexNumber = capexNumber;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getCastCenter() {
		return castCenter;
	}
	public void setCastCenter(String castCenter) {
		this.castCenter = castCenter;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getSoftwareTitleAndVersion() {
		return softwareTitleAndVersion;
	}
	public void setSoftwareTitleAndVersion(String softwareTitleAndVersion) {
		this.softwareTitleAndVersion = softwareTitleAndVersion;
	}
	public String getSpecifications() {
		return specifications;
	}
	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public Short getLocationId() {
		return locationId;
	}
	public void setLocationId(Short locationId) {
		this.locationId = locationId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}
	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public Double getIndividualLicenseCost() {
		return individualLicenseCost;
	}
	public void setIndividualLicenseCost(Double individualLicenseCost) {
		this.individualLicenseCost = individualLicenseCost;
	}
	public Double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	public String getCostBorneBy() {
		return costBorneBy;
	}
	public void setCostBorneBy(String costBorneBy) {
		this.costBorneBy = costBorneBy;
	}
	public Double getBuContribution() {
		return buContribution;
	}
	public void setBuContribution(Double buContribution) {
		this.buContribution = buContribution;
	}
	@Override
	public String toString() {
		return "PurchaseApproveCapexOutputBean [capexNumber=" + capexNumber
				+ ", poNumber=" + poNumber + ", castCenter=" + castCenter
				+ ", projectName=" + projectName + ", softwareTitleAndVersion="
				+ softwareTitleAndVersion + ", specifications="
				+ specifications + ", assetType=" + assetType + ", locationId="
				+ locationId + ", location=" + location
				+ ", isClientReimbursing=" + isClientReimbursing + ", purpose="
				+ purpose + ", individualLicenseCost=" + individualLicenseCost
				+ ", totalCost=" + totalCost + ", costBorneBy=" + costBorneBy
				+ ", buContribution=" + buContribution + "]";
	}
}
