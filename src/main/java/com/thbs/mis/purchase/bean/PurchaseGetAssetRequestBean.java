package com.thbs.mis.purchase.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseGetAssetRequestBean {

	private Integer pkPurchaseRequestId;
	
	private String assetRequestType;
	
	private String assetType;
	
	private String priority;
	
	private String assetNameVersionAndModel;
	
	private Integer noOfUnitsOrLicence;
	
	private Integer componentId;
	
	private String componentName;
	
	private Date assetRequiredBy;
	
	private Integer duration;
	
	private String specification;
	
	private String breifDiscription;
	
	private String isClientReimbursing;

	private String assetRequestStatus;
	
	private Integer assetRequestStatusId;
	
	private Integer assetId;
	
	private Integer clientId;
	
	private String clientName;
	
	private Byte internalFlag;
	
	private String buName;
	
	private String reportingManager;

	private Integer rmId;
	
	private Integer buId;
	
	private Date modifiedDate;
	
	private String systemAdminCommets;
	
	//Added by Shyam for reject comments (RM, BU Head)
	private String rmRejectComments;
	
	private String buHeadRejectComments;
	
	private String financeRejectComments;
	//EOA
	
	private List<PurchaseQuotesOutputBean> listOfQuotes;
	
	
	public String getSystemAdminCommets() {
		return systemAdminCommets;
	}

	public void setSystemAdminCommets(String systemAdminCommets) {
		this.systemAdminCommets = systemAdminCommets;
	}

	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public List<PurchaseQuotesOutputBean> getListOfQuotes() {
		return listOfQuotes;
	}

	public void setListOfQuotes(List<PurchaseQuotesOutputBean> listOfQuotes) {
		this.listOfQuotes = listOfQuotes;
	}
	
	public Integer getAssetRequestStatusId() {
		return assetRequestStatusId;
	}

	public void setAssetRequestStatusId(Integer assetRequestStatusId) {
		this.assetRequestStatusId = assetRequestStatusId;
	}

	public Integer getRmId() {
		return rmId;
	}

	public void setRmId(Integer rmId) {
		this.rmId = rmId;
	}

	public Integer getBuId() {
		return buId;
	}

	public void setBuId(Integer buId) {
		this.buId = buId;
	}

	public String getAssetNameVersionAndModel() {
		return assetNameVersionAndModel;
	}

	public void setAssetNameVersionAndModel(String assetNameVersionAndModel) {
		this.assetNameVersionAndModel = assetNameVersionAndModel;
	}

	public Integer getNoOfUnitsOrLicence() {
		return noOfUnitsOrLicence;
	}

	public void setNoOfUnitsOrLicence(Integer noOfUnitsOrLicence) {
		this.noOfUnitsOrLicence = noOfUnitsOrLicence;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getAssetRequestStatus() {
		return assetRequestStatus;
	}

	public void setAssetRequestStatus(String assetRequestStatus) {
		this.assetRequestStatus = assetRequestStatus;
	}

	public Integer getAssetId() {
		return assetId;
	}

	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public Byte getInternalFlag() {
		return internalFlag;
	}

	public void setInternalFlag(Byte internalFlag) {
		this.internalFlag = internalFlag;
	}

	public Integer getPkPurchaseRequestId() {
		return pkPurchaseRequestId;
	}

	public void setPkPurchaseRequestId(Integer pkPurchaseRequestId) {
		this.pkPurchaseRequestId = pkPurchaseRequestId;
	}

	public String getAssetRequestType() {
		return assetRequestType;
	}

	public void setAssetRequestType(String assetRequestType) {
		this.assetRequestType = assetRequestType;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getAssetRequiredBy() {
		return assetRequiredBy;
	}

	public void setAssetRequiredBy(Date assetRequiredBy) {
		this.assetRequiredBy = assetRequiredBy;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	public String getBreifDiscription() {
		return breifDiscription;
	}

	public void setBreifDiscription(String breifDiscription) {
		this.breifDiscription = breifDiscription;
	}

	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}

	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}

	public String getBuName() {
		return buName;
	}

	public void setBuName(String buName) {
		this.buName = buName;
	}

	public String getReportingManager() {
		return reportingManager;
	}

	public void setReportingManager(String reportingManager) {
		this.reportingManager = reportingManager;
	}

	public String getRmRejectComments() {
		return rmRejectComments;
	}

	public void setRmRejectComments(String rmRejectComments) {
		this.rmRejectComments = rmRejectComments;
	}

	public String getBuHeadRejectComments() {
		return buHeadRejectComments;
	}

	public void setBuHeadRejectComments(String buHeadRejectComments) {
		this.buHeadRejectComments = buHeadRejectComments;
	}

	public String getFinanceRejectComments() {
		return financeRejectComments;
	}

	public void setFinanceRejectComments(String financeRejectComments) {
		this.financeRejectComments = financeRejectComments;
	}

	@Override
	public String toString() {
		return "PurchaseGetAssetRequestBean [pkPurchaseRequestId="
				+ pkPurchaseRequestId + ", assetRequestType="
				+ assetRequestType + ", assetType=" + assetType + ", priority="
				+ priority + ", assetNameVersionAndModel="
				+ assetNameVersionAndModel + ", noOfUnitsOrLicence="
				+ noOfUnitsOrLicence + ", componentId=" + componentId
				+ ", componentName=" + componentName + ", assetRequiredBy="
				+ assetRequiredBy + ", duration=" + duration
				+ ", specification=" + specification + ", breifDiscription="
				+ breifDiscription + ", isClientReimbursing="
				+ isClientReimbursing + ", assetRequestStatus="
				+ assetRequestStatus + ", assetRequestStatusId="
				+ assetRequestStatusId + ", assetId=" + assetId + ", clientId="
				+ clientId + ", clientName=" + clientName + ", internalFlag="
				+ internalFlag + ", buName=" + buName + ", reportingManager="
				+ reportingManager + ", rmId=" + rmId + ", buId=" + buId
				+ ", modifiedDate=" + modifiedDate + ", systemAdminCommets="
				+ systemAdminCommets + ", rmRejectComments=" + rmRejectComments
				+ ", buHeadRejectComments=" + buHeadRejectComments
				+ ", financeRejectComments=" + financeRejectComments
				+ ", listOfQuotes=" + listOfQuotes + "]";
	}

	
		
}
