package com.thbs.mis.purchase.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class PurchaseCancelPoBean {
	
	@NumberFormat(style = Style.NUMBER, pattern = "purchaseRequestId should be numeric value")
	@Min(1)
	@NotNull(message = "purchaseRequestId field can not be null")
	private Integer purchaseRequestId;
	
    @Employee_Id_Not_Found
	@NumberFormat(style = Style.NUMBER, pattern = "loggedInUser should be numeric value")
	@Min(1)
	@NotNull(message = "loggedInUser field can not be null")
	private Integer loggedInUser;
	

	@NotBlank(message = "financeExComment should not be blank.")
	@NotEmpty(message = "financeExComment Description should not be empty.")
	@NotNull(message = "financeExComment Description should not be null.")
	private String financeExComment;


	public Integer getPurchaseRequestId() {
		return purchaseRequestId;
	}


	public void setPurchaseRequestId(Integer purchaseRequestId) {
		this.purchaseRequestId = purchaseRequestId;
	}


	public Integer getLoggedInUser() {
		return loggedInUser;
	}


	public void setLoggedInUser(Integer loggedInUser) {
		this.loggedInUser = loggedInUser;
	}


	public String getFinanceExComment() {
		return financeExComment;
	}


	public void setFinanceExComment(String financeExComment) {
		this.financeExComment = financeExComment;
	}


	@Override
	public String toString() {
		return "PurchaseCancelPoBean [purchaseRequestId=" + purchaseRequestId
				+ ", loggedInUser=" + loggedInUser + ", financeExComment="
				+ financeExComment + "]";
	}

}
