package com.thbs.mis.purchase.bean;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class PurchaseFetchAssetsByAssetTypeAndLicenseTypeInputBean {

	
	@NotBlank(message = "assetType should not be blank.")
	@NotEmpty(message = "assetType should not be empty.")
	@NotNull(message = "assetType should not be null.")
	private String assetType;
	
	@NotBlank(message = "licenseType should not be blank.")
	@NotEmpty(message = "licenseType should not be empty.")
	@NotNull(message = "licenseType should not be null.")
	private String licenseType;

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public String getLicenseType() {
		return licenseType;
	}

	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}

	@Override
	public String toString() {
		return "PurchaseFetchAssetsByAssetTypeAndLicenseTypeInputBean [assetType="
				+ assetType + ", licenseType=" + licenseType + "]";
	}
	
	
}
