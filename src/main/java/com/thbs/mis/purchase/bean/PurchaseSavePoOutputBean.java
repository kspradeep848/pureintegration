package com.thbs.mis.purchase.bean;

public class PurchaseSavePoOutputBean {

	private String firstAssetId;
	
	private String lastAssetId;
	
	private String softwareLicenseId;
	
	private String message;
	
	

	public String getFirstAssetId() {
		return firstAssetId;
	}

	public void setFirstAssetId(String firstAssetId) {
		this.firstAssetId = firstAssetId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getLastAssetId() {
		return lastAssetId;
	}

	public void setLastAssetId(String lastAssetId) {
		this.lastAssetId = lastAssetId;
	}

	public String getSoftwareLicenseId() {
		return softwareLicenseId;
	}

	public void setSoftwareLicenseId(String softwareLicenseId) {
		this.softwareLicenseId = softwareLicenseId;
	}

	@Override
	public String toString() {
		return "PurchaseSavePoOutputBean [firstAssetId=" + firstAssetId
				+ ", lastAssetId=" + lastAssetId + ", softwareLicenseId="
				+ softwareLicenseId + ", message=" + message + "]";
	}

}
