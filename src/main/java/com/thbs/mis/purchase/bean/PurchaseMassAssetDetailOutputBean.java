package com.thbs.mis.purchase.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseMassAssetDetailOutputBean {
	
	private Integer pkAssetDetailId;
	private String softwareLicenseOrSubscriptionId;
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date assetAvailableFromDate;
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date assetAvailableToDate;
	private String assetDescription;
	public Integer getPkAssetDetailId() {
		return pkAssetDetailId;
	}
	public void setPkAssetDetailId(Integer pkAssetDetailId) {
		this.pkAssetDetailId = pkAssetDetailId;
	}
	public String getSoftwareLicenseOrSubscriptionId() {
		return softwareLicenseOrSubscriptionId;
	}
	public void setSoftwareLicenseOrSubscriptionId(
			String softwareLicenseOrSubscriptionId) {
		this.softwareLicenseOrSubscriptionId = softwareLicenseOrSubscriptionId;
	}
	public Date getAssetAvailableFromDate() {
		return assetAvailableFromDate;
	}
	public void setAssetAvailableFromDate(Date assetAvailableFromDate) {
		this.assetAvailableFromDate = assetAvailableFromDate;
	}
	public Date getAssetAvailableToDate() {
		return assetAvailableToDate;
	}
	public void setAssetAvailableToDate(Date assetAvailableToDate) {
		this.assetAvailableToDate = assetAvailableToDate;
	}
	public String getAssetDescription() {
		return assetDescription;
	}
	public void setAssetDescription(String assetDescription) {
		this.assetDescription = assetDescription;
	}
	@Override
	public String toString() {
		return "PurchaseMassAssetDetailOutputBean [pkAssetDetailId="
				+ pkAssetDetailId + ", softwareLicenseOrSubscriptionId="
				+ softwareLicenseOrSubscriptionId + ", assetAvailableFromDate="
				+ assetAvailableFromDate + ", assetAvailableToDate="
				+ assetAvailableToDate + ", assetDescription="
				+ assetDescription + "]";
	}
	
}
