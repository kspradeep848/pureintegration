package com.thbs.mis.purchase.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseQuotesOutputBean {

	private Integer quoteId;
	
	private String costBorneBy;
	
	private Double individualLicenseCost;
	
	private Double totalCost;
	
	private Double buContribution;
	
	private Date deliveryDate;
	
	private String quoteStatus;
	
	private String vendorName;
	
	private String vendorAddress;
	
	private String vendorContactNumber;
	
	private String vendorQuoteReferenceNumber;
	
	private Integer supplierAddressId;

	private Integer currencyType;
	
	private String assetDescriptionEnteredBySysadmin;

	public Integer getSupplierAddressId() {
		return supplierAddressId;
	}

	public void setSupplierAddressId(Integer supplierAddressId) {
		this.supplierAddressId = supplierAddressId;
	}

	public String getVendorQuoteReferenceNumber() {
		return vendorQuoteReferenceNumber;
	}

	public void setVendorQuoteReferenceNumber(String vendorQuoteReferenceNumber) {
		this.vendorQuoteReferenceNumber = vendorQuoteReferenceNumber;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	
	public String getVendorAddress() {
		return vendorAddress;
	}

	public void setVendorAddress(String vendorAddress) {
		this.vendorAddress = vendorAddress;
	}

	public String getVendorContactNumber() {
		return vendorContactNumber;
	}

	public void setVendorContactNumber(String vendorContactNumber) {
		this.vendorContactNumber = vendorContactNumber;
	}

	public Integer getQuoteId() {
		return quoteId;
	}

	public void setQuoteId(Integer quoteId) {
		this.quoteId = quoteId;
	}

	public String getCostBorneBy() {
		return costBorneBy;
	}

	public void setCostBorneBy(String costBorneBy) {
		this.costBorneBy = costBorneBy;
	}

	public Double getIndividualLicenseCost() {
		return individualLicenseCost;
	}

	public void setIndividualLicenseCost(Double individualLicenseCost) {
		this.individualLicenseCost = individualLicenseCost;
	}

	public Double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}

	public Double getBuContribution() {
		return buContribution;
	}

	public void setBuContribution(Double buContribution) {
		this.buContribution = buContribution;
	}

	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getQuoteStatus() {
		return quoteStatus;
	}

	public void setQuoteStatus(String quoteStatus) {
		this.quoteStatus = quoteStatus;
	}

	public Integer getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}

	public String getAssetDescriptionEnteredBySysadmin() {
		return assetDescriptionEnteredBySysadmin;
	}

	public void setAssetDescriptionEnteredBySysadmin(
			String assetDescriptionEnteredBySysadmin) {
		this.assetDescriptionEnteredBySysadmin = assetDescriptionEnteredBySysadmin;
	}

	@Override
	public String toString() {
		return "PurchaseQuotesOutputBean [quoteId=" + quoteId
				+ ", costBorneBy=" + costBorneBy + ", individualLicenseCost="
				+ individualLicenseCost + ", totalCost=" + totalCost
				+ ", buContribution=" + buContribution + ", deliveryDate="
				+ deliveryDate + ", quoteStatus=" + quoteStatus
				+ ", vendorName=" + vendorName + ", vendorAddress="
				+ vendorAddress + ", vendorContactNumber="
				+ vendorContactNumber + ", vendorQuoteReferenceNumber="
				+ vendorQuoteReferenceNumber + ", supplierAddressId="
				+ supplierAddressId + ", currencyType=" + currencyType
				+ ", assetDescriptionEnteredBySysadmin="
				+ assetDescriptionEnteredBySysadmin + "]";
	}

}
