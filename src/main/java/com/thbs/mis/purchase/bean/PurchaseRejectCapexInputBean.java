package com.thbs.mis.purchase.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class PurchaseRejectCapexInputBean {

	@NumberFormat(style = Style.NUMBER, pattern = "pkPurchaseRequestId should be numeric value")
	@NotNull(message = "pkPurchaseRequestId can not be null")
	@Min(1)
	private Integer pkPurchaseRequestId;
	
	
	@NumberFormat(style = Style.NUMBER, pattern = "loggedInUser should be numeric value")
	@NotNull(message = "loggedInUser can not be null")
	@Min(1)
	private Integer loggedInUser;
	
	@NotBlank(message = "closureComment should not be blank.")
	@NotEmpty(message = "closureComment should not be empty.")
	@NotNull(message = "closureComment should not be null.")
	private String closureComment;

	public Integer getPkPurchaseRequestId() {
		return pkPurchaseRequestId;
	}

	public void setPkPurchaseRequestId(Integer pkPurchaseRequestId) {
		this.pkPurchaseRequestId = pkPurchaseRequestId;
	}

	

	public String getClosureComment() {
		return closureComment;
	}

	public void setClosureComment(String closureComment) {
		this.closureComment = closureComment;
	}

	public Integer getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Integer loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	@Override
	public String toString() {
		return "PurchaseRejectCapexInputBean [pkPurchaseRequestId="
				+ pkPurchaseRequestId + ", loggedInUser=" + loggedInUser
				+ ", closureComment=" + closureComment + "]";
	}

	
	
}
