package com.thbs.mis.purchase.bean;

import java.util.Date;

public class PurchaseUpdateCapexInputBean {
	
	//private Integer capexId;
	private Integer assetRequestId;
	private Integer loggedInUser;
	private Integer previousAgreedQuoteId;
	private Integer updatedAgreedQuoteId;
	private Double buContribution;
	private String costBorneBy;
	private Date deliveryDate;
	private Double individualLicenseCost;
	private Double totalCost;
	private Integer supplierId;
	private Integer currencyType;
	private String vendorQuoteReferenceNo;
	private String assetDescriptionEnteredBySysadmin;
	
	public Integer getAssetRequestId() {
		return assetRequestId;
	}
	public void setAssetRequestId(Integer assetRequestId) {
		this.assetRequestId = assetRequestId;
	}
	/*public Integer getCapexId() {
		return capexId;
	}
	public void setCapexId(Integer capexId) {
		this.capexId = capexId;
	}*/
	public Integer getLoggedInUser() {
		return loggedInUser;
	}
	public void setLoggedInUser(Integer loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	public Integer getPreviousAgreedQuoteId() {
		return previousAgreedQuoteId;
	}
	public void setPreviousAgreedQuoteId(Integer previousAgreedQuoteId) {
		this.previousAgreedQuoteId = previousAgreedQuoteId;
	}
	public Integer getUpdatedAgreedQuoteId() {
		return updatedAgreedQuoteId;
	}
	public void setUpdatedAgreedQuoteId(Integer updatedAgreedQuoteId) {
		this.updatedAgreedQuoteId = updatedAgreedQuoteId;
	}
	public Double getBuContribution() {
		return buContribution;
	}
	public void setBuContribution(Double buContribution) {
		this.buContribution = buContribution;
	}
	public String getCostBorneBy() {
		return costBorneBy;
	}
	public void setCostBorneBy(String costBorneBy) {
		this.costBorneBy = costBorneBy;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Double getIndividualLicenseCost() {
		return individualLicenseCost;
	}
	public void setIndividualLicenseCost(Double individualLicenseCost) {
		this.individualLicenseCost = individualLicenseCost;
	}
	public Double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	public Integer getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}
	public String getVendorQuoteReferenceNo() {
		return vendorQuoteReferenceNo;
	}
	public void setVendorQuoteReferenceNo(String vendorQuoteReferenceNo) {
		this.vendorQuoteReferenceNo = vendorQuoteReferenceNo;
	}
	public Integer getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}
	public String getAssetDescriptionEnteredBySysadmin() {
		return assetDescriptionEnteredBySysadmin;
	}
	public void setAssetDescriptionEnteredBySysadmin(
			String assetDescriptionEnteredBySysadmin) {
		this.assetDescriptionEnteredBySysadmin = assetDescriptionEnteredBySysadmin;
	}
	@Override
	public String toString() {
		return "PurchaseUpdateCapexInputBean [assetRequestId=" + assetRequestId
				+ ", loggedInUser=" + loggedInUser + ", previousAgreedQuoteId="
				+ previousAgreedQuoteId + ", updatedAgreedQuoteId="
				+ updatedAgreedQuoteId + ", buContribution=" + buContribution
				+ ", costBorneBy=" + costBorneBy + ", deliveryDate="
				+ deliveryDate + ", individualLicenseCost="
				+ individualLicenseCost + ", totalCost=" + totalCost
				+ ", supplierId=" + supplierId + ", currencyType="
				+ currencyType + ", vendorQuoteReferenceNo="
				+ vendorQuoteReferenceNo
				+ ", assetDescriptionEnteredBySysadmin="
				+ assetDescriptionEnteredBySysadmin + "]";
	}
	

}
