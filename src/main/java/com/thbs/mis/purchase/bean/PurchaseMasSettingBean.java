package com.thbs.mis.purchase.bean;


public class PurchaseMasSettingBean {

	private Integer updatedBy;
	private Integer systemAdminId;
	private String settingType;
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getSystemAdminId() {
		return systemAdminId;
	}
	public void setSystemAdminId(Integer systemAdminId) {
		this.systemAdminId = systemAdminId;
	}
	public String getSettingType() {
		return settingType;
	}
	public void setSettingType(String settingType) {
		this.settingType = settingType;
	}
	@Override
	public String toString() {
		return "PurchaseMasSettingBean [updatedBy=" + updatedBy
				+ ", systemAdminId=" + systemAdminId + ", settingType="
				+ settingType + "]";
	}

}
