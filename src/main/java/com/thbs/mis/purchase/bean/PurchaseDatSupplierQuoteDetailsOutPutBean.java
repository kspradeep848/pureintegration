//Added by Prathibha for Fetch Vendor quote  details based on Asset Id

package com.thbs.mis.purchase.bean;

import java.math.BigInteger;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseDatSupplierQuoteDetailsOutPutBean {
	
	/********************Supplier details**************/
	
	
	private String vendorAddress;
	private String vendorContactNo;
	
	private String vendorName;
	private int fkAssetId;
	
	public String getVendorAddress() {
		return vendorAddress;
	}
	public void setVendorAddress(String vendorAddress) {
		this.vendorAddress = vendorAddress;
	}
	public String getVendorContactNo() {
		return vendorContactNo;
	}
	public void setVendorContactNo(String vendorContactNo) {
		this.vendorContactNo = vendorContactNo;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public int getFkAssetId() {
		return fkAssetId;
	}
	public void setFkAssetId(int fkAssetId) {
		this.fkAssetId = fkAssetId;
	}
	
	/**********************************Quote details**********************************/
	
	
	private int pkQuoteId;
	/*private double buContribution;
	private String costBorneBy;
	private int quoteCreatedBy;
	private Date quoteCreatedDate;*/
	private Date deliveryDate;
	private String fkQuoteStatus;
	private double individualLicenseCost;
	/*private int quoteModifiedBy;
	private Date modifiedDate;
	private double totalCost;*/
	private int fkPurchaseRequestId;
	
	public int getPkQuoteId() {
		return pkQuoteId;
	}
	public void setPkQuoteId(int pkQuoteId) {
		this.pkQuoteId = pkQuoteId;
	}
	
	
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getFkQuoteStatus() {
		return fkQuoteStatus;
	}
	public void setFkQuoteStatus(String fkQuoteStatus) {
		this.fkQuoteStatus = fkQuoteStatus;
	}
	public double getIndividualLicenseCost() {
		return individualLicenseCost;
	}
	public void setIndividualLicenseCost(double individualLicenseCost) {
		this.individualLicenseCost = individualLicenseCost;
	}
	
	public int getFkPurchaseRequestId() {
		return fkPurchaseRequestId;
	}
	public void setFkPurchaseRequestId(int fkPurchaseRequestId) {
		this.fkPurchaseRequestId = fkPurchaseRequestId;
	}
	@Override
	public String toString() {
		return "PurchaseDatSupplierQuoteDetailsOutPutBean [vendorAddress="
				+ vendorAddress + ", vendorContactNo=" + vendorContactNo
				+ ", vendorName=" + vendorName + ", fkAssetId=" + fkAssetId
				+ ", pkQuoteId=" + pkQuoteId + ", deliveryDate=" + deliveryDate
				+ ", fkQuoteStatus=" + fkQuoteStatus
				+ ", individualLicenseCost=" + individualLicenseCost
				+ ", fkPurchaseRequestId=" + fkPurchaseRequestId + "]";
	}
	
	

}
