package com.thbs.mis.purchase.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseGetCapexRequestBean {

	private Integer pkPurchaseRequestId;


	private String briefDescription;

	
	private Date createdDate;

	private String isClientReimbursing;

	private String isSoftwareOrHardwarePurchased;

	private String location;

	private String projectName;

	private Integer purchaseRequestStatus;
	
	private String capexNumber;
	
	private String buName;
	
	private String assetNameAndVersion;
	
	private Double approvedQuoteFinalcost;
	
	private PurchaseQuotesOutputBean quote;
	
	private String priority;
	
	private String systemAdminComments;
    private String requestorName;
	private String reportingManager;
	private String sysAdminName;
	private String financeHeadName;
	private String buHeadName;
	private String itHeadName;
	private String rmComment;
	private String sysAdminComment;
	private String financeExComment;
	private String buHeadComment;
	private Integer fkCurrencyType;
	

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getSystemAdminComments() {
		return systemAdminComments;
	}

	public void setSystemAdminComments(String systemAdminComments) {
		this.systemAdminComments = systemAdminComments;
	}

	public PurchaseQuotesOutputBean getQuote() {
		return quote;
	}

	public void setQuote(PurchaseQuotesOutputBean quote) {
		this.quote = quote;
	}

	public Integer getPkPurchaseRequestId() {
		return pkPurchaseRequestId;
	}

	public void setPkPurchaseRequestId(Integer pkPurchaseRequestId) {
		this.pkPurchaseRequestId = pkPurchaseRequestId;
	}

	
	public String getBriefDescription() {
		return briefDescription;
	}

	public void setBriefDescription(String briefDescription) {
		this.briefDescription = briefDescription;
	}
	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}

	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}

	public String getIsSoftwareOrHardwarePurchased() {
		return isSoftwareOrHardwarePurchased;
	}

	public void setIsSoftwareOrHardwarePurchased(
			String isSoftwareOrHardwarePurchased) {
		this.isSoftwareOrHardwarePurchased = isSoftwareOrHardwarePurchased;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}



	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Integer getPurchaseRequestStatus() {
		return purchaseRequestStatus;
	}

	public void setPurchaseRequestStatus(Integer purchaseRequestStatus) {
		this.purchaseRequestStatus = purchaseRequestStatus;
	}

	public String getCapexNumber() {
		return capexNumber;
	}

	public void setCapexNumber(String capexNumber) {
		this.capexNumber = capexNumber;
	}

	public String getBuName() {
		return buName;
	}

	public void setBuName(String buName) {
		this.buName = buName;
	}

	public String getAssetNameAndVersion() {
		return assetNameAndVersion;
	}

	public void setAssetNameAndVersion(String assetNameAndVersion) {
		this.assetNameAndVersion = assetNameAndVersion;
	}

	public Double getApprovedQuoteFinalcost() {
		return approvedQuoteFinalcost;
	}

	public void setApprovedQuoteFinalcost(Double approvedQuoteFinalcost) {
		this.approvedQuoteFinalcost = approvedQuoteFinalcost;
	}
	

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getReportingManager() {
		return reportingManager;
	}

	public void setReportingManager(String reportingManager) {
		this.reportingManager = reportingManager;
	}

	public String getSysAdminName() {
		return sysAdminName;
	}

	public void setSysAdminName(String sysAdminName) {
		this.sysAdminName = sysAdminName;
	}

	public String getFinanceHeadName() {
		return financeHeadName;
	}

	public void setFinanceHeadName(String financeHeadName) {
		this.financeHeadName = financeHeadName;
	}

	public String getBuHeadName() {
		return buHeadName;
	}

	public void setBuHeadName(String buHeadName) {
		this.buHeadName = buHeadName;
	}

	public String getItHeadName() {
		return itHeadName;
	}

	public void setItHeadName(String itHeadName) {
		this.itHeadName = itHeadName;
	}
	public String getRmComment() {
		return rmComment;
	}

	public void setRmComment(String rmComment) {
		this.rmComment = rmComment;
	}

	public String getSysAdminComment() {
		return sysAdminComment;
	}

	public void setSysAdminComment(String sysAdminComment) {
		this.sysAdminComment = sysAdminComment;
	}

	public String getFinanceExComment() {
		return financeExComment;
	}

	public void setFinanceExComment(String financeExComment) {
		this.financeExComment = financeExComment;
	}

	public String getBuHeadComment() {
		return buHeadComment;
	}

	public void setBuHeadComment(String buHeadComment) {
		this.buHeadComment = buHeadComment;
	}

	public Integer getFkCurrencyType() {
		return fkCurrencyType;
	}

	public void setFkCurrencyType(Integer fkCurrencyType) {
		this.fkCurrencyType = fkCurrencyType;
	}

	@Override
	public String toString() {
		return "PurchaseGetCapexRequestBean [pkPurchaseRequestId="
				+ pkPurchaseRequestId + ", briefDescription="
				+ briefDescription + ", createdDate=" + createdDate
				+ ", isClientReimbursing=" + isClientReimbursing
				+ ", isSoftwareOrHardwarePurchased="
				+ isSoftwareOrHardwarePurchased + ", location=" + location
				+ ", projectName=" + projectName + ", purchaseRequestStatus="
				+ purchaseRequestStatus + ", capexNumber=" + capexNumber
				+ ", buName=" + buName + ", assetNameAndVersion="
				+ assetNameAndVersion + ", approvedQuoteFinalcost="
				+ approvedQuoteFinalcost + ", quote=" + quote + ", priority="
				+ priority + ", systemAdminComments=" + systemAdminComments
				+ ", requestorName=" + requestorName + ", reportingManager="
				+ reportingManager + ", sysAdminName=" + sysAdminName
				+ ", financeHeadName=" + financeHeadName + ", buHeadName="
				+ buHeadName + ", itHeadName=" + itHeadName + ", rmComment="
				+ rmComment + ", sysAdminComment=" + sysAdminComment
				+ ", financeExComment=" + financeExComment + ", buHeadComment="
				+ buHeadComment + ", fkCurrencyType=" + fkCurrencyType + "]";
	}

	
}
