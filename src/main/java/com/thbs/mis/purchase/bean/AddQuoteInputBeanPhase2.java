package com.thbs.mis.purchase.bean;

import java.util.Date;

public class AddQuoteInputBeanPhase2 {

		private Double buContribution;
		private String costBorneBy;
		private Date deliveryDate;
		private double individualLicenseCost;
		private double totalCost;
		private Integer fkSupplierAddress;
		private String isSetDefault;
		private String vendorQuoteReferenceNo;
		private String vendorsContactNumber;
		private Integer currencyType;
		private String assetDescriptionEnteredBySysadmin;
		
		public Double getBuContribution() {
			return buContribution;
		}
		public void setBuContribution(Double buContribution) {
			this.buContribution = buContribution;
		}
		public String getCostBorneBy() {
			return costBorneBy;
		}
		public void setCostBorneBy(String costBorneBy) {
			this.costBorneBy = costBorneBy;
		}
		public Date getDeliveryDate() {
			return deliveryDate;
		}
		public void setDeliveryDate(Date deliveryDate) {
			this.deliveryDate = deliveryDate;
		}
		public double getIndividualLicenseCost() {
			return individualLicenseCost;
		}
		public void setIndividualLicenseCost(double individualLicenseCost) {
			this.individualLicenseCost = individualLicenseCost;
		}
		public double getTotalCost() {
			return totalCost;
		}
		public void setTotalCost(double totalCost) {
			this.totalCost = totalCost;
		}
		public Integer getFkSupplierAddress() {
			return fkSupplierAddress;
		}
		public void setFkSupplierAddress(Integer fkSupplierAddress) {
			this.fkSupplierAddress = fkSupplierAddress;
		}
		public String getIsSetDefault() {
			return isSetDefault;
		}
		public void setIsSetDefault(String isSetDefault) {
			this.isSetDefault = isSetDefault;
		}
		public String getVendorQuoteReferenceNo() {
			return vendorQuoteReferenceNo;
		}
		public void setVendorQuoteReferenceNo(String vendorQuoteReferenceNo) {
			this.vendorQuoteReferenceNo = vendorQuoteReferenceNo;
		}
		public Integer getCurrencyType() {
			return currencyType;
		}
		public void setCurrencyType(Integer currencyType) {
			this.currencyType = currencyType;
		}		
		public String getVendorsContactNumber() {
			return vendorsContactNumber;
		}
		public void setVendorsContactNumber(String vendorsContactNumber) {
			this.vendorsContactNumber = vendorsContactNumber;
		}
		public String getAssetDescriptionEnteredBySysadmin() {
			return assetDescriptionEnteredBySysadmin;
		}
		public void setAssetDescriptionEnteredBySysadmin(
				String assetDescriptionEnteredBySysadmin) {
			this.assetDescriptionEnteredBySysadmin = assetDescriptionEnteredBySysadmin;
		}
		@Override
		public String toString() {
			return "AddQuoteInputBeanPhase2 [buContribution=" + buContribution
					+ ", costBorneBy=" + costBorneBy + ", deliveryDate="
					+ deliveryDate + ", individualLicenseCost="
					+ individualLicenseCost + ", totalCost=" + totalCost
					+ ", fkSupplierAddress=" + fkSupplierAddress
					+ ", isSetDefault=" + isSetDefault
					+ ", vendorQuoteReferenceNo=" + vendorQuoteReferenceNo
					+ ", vendorsContactNumber=" + vendorsContactNumber
					+ ", currencyType=" + currencyType
					+ ", assetDescriptionEnteredBySysadmin="
					+ assetDescriptionEnteredBySysadmin + "]";
		}
					

}
