package com.thbs.mis.purchase.bean;

import java.util.Date;

public class FetchQuotesOutputBean {

	
	private Integer pkQuoteId;
	private Double buContribution;
	private String costBorneBy;
	private Date createdDate;
	private Date deliveryDate;
	private Double individualLicenseCost;
	private Date modifiedDate;
	private Double totalCost;
	public Integer getPkQuoteId() {
		return pkQuoteId;
	}
	public void setPkQuoteId(Integer pkQuoteId) {
		this.pkQuoteId = pkQuoteId;
	}
	public Double getBuContribution() {
		return buContribution;
	}
	public void setBuContribution(Double buContribution) {
		this.buContribution = buContribution;
	}
	public String getCostBorneBy() {
		return costBorneBy;
	}
	public void setCostBorneBy(String costBorneBy) {
		this.costBorneBy = costBorneBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Double getIndividualLicenseCost() {
		return individualLicenseCost;
	}
	public void setIndividualLicenseCost(Double individualLicenseCost) {
		this.individualLicenseCost = individualLicenseCost;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	
	
	
	private Integer pkPurchaseRequestId;

	private Integer employeeId;
	
	private Integer noOfUnits;
	
	private String specifications;
	
	private String briefDescription;
	
	private Integer projectComponentId;
	
	private Date reqCreatedDate;
	
	private Date assetRequiredBy;
	
	private Integer duration;
	
	private String isClientReimbursing;
	
	private String isSoftwareOrHardwarePurchased;

	private short locationId;
	
	private Integer modifiedBy;
	
	private Date modifiedOn;
	private String rmComment;

	private Integer rmToApprove;

	private Date rmUpdatedOn;

	private String supportingDocumentsByClient;
	private String sysAdminComment;
	private Integer sysAdminId;
	private Date sysAdminUpdatedOn;
	private Integer assetQuotesEnteredBy;
	private String quotesUploaded;
	private String supportingDocumentsBySysAdmin;
	
	private String buHeadApprovalNeeded;
	private String priority;
	
	private String capexUploaded;

	private String department;
	
	
	private String poCreationRequired;
	
	
	private String poNoMappedByFinance;
	
	
	private String poUploaded;
	
	
	private String projectName;
	
	
	private String financeEx_comment;
	
	
	private Integer financeEx_updated_by;
	
	
	private Date financeEx_updated_on;
	
	
	private String closureComment;
	
	
	private short fkBuHeadId;
	
	
	private Integer clientId;
	
	
	private Integer fkAssetQuot;
	
	
	private Integer fkAssetId;
	
	
	private Integer fkAssetDetailId;
	
	
	private Integer purchaseRequestStatus;
	
	
	private Integer fkRequestTypeId;
	public Integer getPkPurchaseRequestId() {
		return pkPurchaseRequestId;
	}
	public void setPkPurchaseRequestId(Integer pkPurchaseRequestId) {
		this.pkPurchaseRequestId = pkPurchaseRequestId;
	}
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public Integer getNoOfUnits() {
		return noOfUnits;
	}
	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	public String getSpecifications() {
		return specifications;
	}
	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}
	public String getBriefDescription() {
		return briefDescription;
	}
	public void setBriefDescription(String briefDescription) {
		this.briefDescription = briefDescription;
	}
	public Integer getProjectComponentId() {
		return projectComponentId;
	}
	public void setProjectComponentId(Integer projectComponentId) {
		this.projectComponentId = projectComponentId;
	}
	public Date getReqCreatedDate() {
		return reqCreatedDate;
	}
	public void setReqCreatedDate(Date reqCreatedDate) {
		this.reqCreatedDate = reqCreatedDate;
	}
	public Date getAssetRequiredBy() {
		return assetRequiredBy;
	}
	public void setAssetRequiredBy(Date assetRequiredBy) {
		this.assetRequiredBy = assetRequiredBy;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}
	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}
	public String getIsSoftwareOrHardwarePurchased() {
		return isSoftwareOrHardwarePurchased;
	}
	public void setIsSoftwareOrHardwarePurchased(
			String isSoftwareOrHardwarePurchased) {
		this.isSoftwareOrHardwarePurchased = isSoftwareOrHardwarePurchased;
	}
	public short getLocationId() {
		return locationId;
	}
	public void setLocationId(short locationId) {
		this.locationId = locationId;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public String getRmComment() {
		return rmComment;
	}
	public void setRmComment(String rmComment) {
		this.rmComment = rmComment;
	}
	public Integer getRmToApprove() {
		return rmToApprove;
	}
	public void setRmToApprove(Integer rmToApprove) {
		this.rmToApprove = rmToApprove;
	}
	public Date getRmUpdatedOn() {
		return rmUpdatedOn;
	}
	public void setRmUpdatedOn(Date rmUpdatedOn) {
		this.rmUpdatedOn = rmUpdatedOn;
	}
	public String getSupportingDocumentsByClient() {
		return supportingDocumentsByClient;
	}
	public void setSupportingDocumentsByClient(String supportingDocumentsByClient) {
		this.supportingDocumentsByClient = supportingDocumentsByClient;
	}
	public String getSysAdminComment() {
		return sysAdminComment;
	}
	public void setSysAdminComment(String sysAdminComment) {
		this.sysAdminComment = sysAdminComment;
	}
	public Integer getSysAdminId() {
		return sysAdminId;
	}
	public void setSysAdminId(Integer sysAdminId) {
		this.sysAdminId = sysAdminId;
	}
	public Date getSysAdminUpdatedOn() {
		return sysAdminUpdatedOn;
	}
	public void setSysAdminUpdatedOn(Date sysAdminUpdatedOn) {
		this.sysAdminUpdatedOn = sysAdminUpdatedOn;
	}
	public Integer getAssetQuotesEnteredBy() {
		return assetQuotesEnteredBy;
	}
	public void setAssetQuotesEnteredBy(Integer assetQuotesEnteredBy) {
		this.assetQuotesEnteredBy = assetQuotesEnteredBy;
	}
	public String getQuotesUploaded() {
		return quotesUploaded;
	}
	public void setQuotesUploaded(String quotesUploaded) {
		this.quotesUploaded = quotesUploaded;
	}
	public String getSupportingDocumentsBySysAdmin() {
		return supportingDocumentsBySysAdmin;
	}
	public void setSupportingDocumentsBySysAdmin(
			String supportingDocumentsBySysAdmin) {
		this.supportingDocumentsBySysAdmin = supportingDocumentsBySysAdmin;
	}
	public String getBuHeadApprovalNeeded() {
		return buHeadApprovalNeeded;
	}
	public void setBuHeadApprovalNeeded(String buHeadApprovalNeeded) {
		this.buHeadApprovalNeeded = buHeadApprovalNeeded;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getCapexUploaded() {
		return capexUploaded;
	}
	public void setCapexUploaded(String capexUploaded) {
		this.capexUploaded = capexUploaded;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getPoCreationRequired() {
		return poCreationRequired;
	}
	public void setPoCreationRequired(String poCreationRequired) {
		this.poCreationRequired = poCreationRequired;
	}
	public String getPoNoMappedByFinance() {
		return poNoMappedByFinance;
	}
	public void setPoNoMappedByFinance(String poNoMappedByFinance) {
		this.poNoMappedByFinance = poNoMappedByFinance;
	}
	public String getPoUploaded() {
		return poUploaded;
	}
	public void setPoUploaded(String poUploaded) {
		this.poUploaded = poUploaded;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getFinanceEx_comment() {
		return financeEx_comment;
	}
	public void setFinanceEx_comment(String financeEx_comment) {
		this.financeEx_comment = financeEx_comment;
	}
	public Integer getFinanceEx_updated_by() {
		return financeEx_updated_by;
	}
	public void setFinanceEx_updated_by(Integer financeEx_updated_by) {
		this.financeEx_updated_by = financeEx_updated_by;
	}
	public Date getFinanceEx_updated_on() {
		return financeEx_updated_on;
	}
	public void setFinanceEx_updated_on(Date financeEx_updated_on) {
		this.financeEx_updated_on = financeEx_updated_on;
	}
	public String getClosureComment() {
		return closureComment;
	}
	public void setClosureComment(String closureComment) {
		this.closureComment = closureComment;
	}
	public short getFkBuHeadId() {
		return fkBuHeadId;
	}
	public void setFkBuHeadId(short fkBuHeadId) {
		this.fkBuHeadId = fkBuHeadId;
	}
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	public Integer getFkAssetQuot() {
		return fkAssetQuot;
	}
	public void setFkAssetQuot(Integer fkAssetQuot) {
		this.fkAssetQuot = fkAssetQuot;
	}
	public Integer getFkAssetId() {
		return fkAssetId;
	}
	public void setFkAssetId(Integer fkAssetId) {
		this.fkAssetId = fkAssetId;
	}
	public Integer getFkAssetDetailId() {
		return fkAssetDetailId;
	}
	public void setFkAssetDetailId(Integer fkAssetDetailId) {
		this.fkAssetDetailId = fkAssetDetailId;
	}
	public Integer getPurchaseRequestStatus() {
		return purchaseRequestStatus;
	}
	public void setPurchaseRequestStatus(Integer purchaseRequestStatus) {
		this.purchaseRequestStatus = purchaseRequestStatus;
	}
	public Integer getFkRequestTypeId() {
		return fkRequestTypeId;
	}
	public void setFkRequestTypeId(Integer fkRequestTypeId) {
		this.fkRequestTypeId = fkRequestTypeId;
	}
	@Override
	public String toString() {
		return "FetchQuotesOutputBean [pkQuoteId=" + pkQuoteId
				+ ", buContribution=" + buContribution + ", costBorneBy="
				+ costBorneBy + ", createdDate=" + createdDate
				+ ", deliveryDate=" + deliveryDate + ", individualLicenseCost="
				+ individualLicenseCost + ", modifiedDate=" + modifiedDate
				+ ", totalCost=" + totalCost + ", pkPurchaseRequestId="
				+ pkPurchaseRequestId + ", employeeId=" + employeeId
				+ ", noOfUnits=" + noOfUnits + ", specifications="
				+ specifications + ", briefDescription=" + briefDescription
				+ ", projectComponentId=" + projectComponentId
				+ ", reqCreatedDate=" + reqCreatedDate + ", assetRequiredBy="
				+ assetRequiredBy + ", duration=" + duration
				+ ", isClientReimbursing=" + isClientReimbursing
				+ ", isSoftwareOrHardwarePurchased="
				+ isSoftwareOrHardwarePurchased + ", locationId=" + locationId
				+ ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn
				+ ", rmComment=" + rmComment + ", rmToApprove=" + rmToApprove
				+ ", rmUpdatedOn=" + rmUpdatedOn
				+ ", supportingDocumentsByClient="
				+ supportingDocumentsByClient + ", sysAdminComment="
				+ sysAdminComment + ", sysAdminId=" + sysAdminId
				+ ", sysAdminUpdatedOn=" + sysAdminUpdatedOn
				+ ", assetQuotesEnteredBy=" + assetQuotesEnteredBy
				+ ", quotesUploaded=" + quotesUploaded
				+ ", supportingDocumentsBySysAdmin="
				+ supportingDocumentsBySysAdmin + ", buHeadApprovalNeeded="
				+ buHeadApprovalNeeded + ", priority=" + priority
				+ ", capexUploaded=" + capexUploaded + ", department="
				+ department + ", poCreationRequired=" + poCreationRequired
				+ ", poNoMappedByFinance=" + poNoMappedByFinance
				+ ", poUploaded=" + poUploaded + ", projectName=" + projectName
				+ ", financeEx_comment=" + financeEx_comment
				+ ", financeEx_updated_by=" + financeEx_updated_by
				+ ", financeEx_updated_on=" + financeEx_updated_on
				+ ", closureComment=" + closureComment + ", fkBuHeadId="
				+ fkBuHeadId + ", clientId=" + clientId + ", fkAssetQuot="
				+ fkAssetQuot + ", fkAssetId=" + fkAssetId
				+ ", fkAssetDetailId=" + fkAssetDetailId
				+ ", purchaseRequestStatus=" + purchaseRequestStatus
				+ ", fkRequestTypeId=" + fkRequestTypeId + "]";
	}
	
	
	
	
	
	
	
	
	
}
