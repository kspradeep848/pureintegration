package com.thbs.mis.purchase.bean;

import java.util.Date;
import java.util.List;

public class PurchaseAddNewAssetRequestInputBean {
	
    private Integer requestorId;
    private Integer purchaseRequestTypeId;
	private Integer fkAssetId;
	private Integer noOfUnits;
	private String specifications; 
	private String briefDescription;
	private Date assetRequiredDate;
	private Integer duration;
	private String isClientReimbursing;
	private Integer rmToApprove;
	private Short fkBuId;
	private Short locationId;
	private String licenceType;
	private String softwareLicenseOrSubscriptionId;
	private String priority;
	private List<AddQuoteInputBeanPhase2> quoteList;
	
	public Integer getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Integer requestorId) {
		this.requestorId = requestorId;
	}
	public Integer getFkAssetId() {
		return fkAssetId;
	}
	public void setFkAssetId(Integer fkAssetId) {
		this.fkAssetId = fkAssetId;
	}
	public Integer getNoOfUnits() {
		return noOfUnits;
	}
	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	public String getSpecifications() {
		return specifications;
	}
	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}
	public String getBriefDescription() {
		return briefDescription;
	}
	public void setBriefDescription(String briefDescription) {
		this.briefDescription = briefDescription;
	}
	public Date getAssetRequiredDate() {
		return assetRequiredDate;
	}
	public void setAssetRequiredDate(Date assetRequiredDate) {
		this.assetRequiredDate = assetRequiredDate;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}
	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}
	public Integer getRmToApprove() {
		return rmToApprove;
	}
	public void setRmToApprove(Integer rmToApprove) {
		this.rmToApprove = rmToApprove;
	}
	public Short getFkBuId() {
		return fkBuId;
	}
	public void setFkBuId(Short fkBuId) {
		this.fkBuId = fkBuId;
	}
	public Short getLocationId() {
		return locationId;
	}
	public void setLocationId(Short locationId) {
		this.locationId = locationId;
	}
	
	public Integer getPurchaseRequestTypeId() {
		return purchaseRequestTypeId;
	}
	public void setPurchaseRequestTypeId(Integer purchaseRequestTypeId) {
		this.purchaseRequestTypeId = purchaseRequestTypeId;
	}
	
	public String getLicenceType() {
		return licenceType;
	}
	public void setLicenceType(String licenceType) {
		this.licenceType = licenceType;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getSoftwareLicenseOrSubscriptionId() {
		return softwareLicenseOrSubscriptionId;
	}
	public void setSoftwareLicenseOrSubscriptionId(
			String softwareLicenseOrSubscriptionId) {
		this.softwareLicenseOrSubscriptionId = softwareLicenseOrSubscriptionId;
	}
	public List<AddQuoteInputBeanPhase2> getQuoteList() {
		return quoteList;
	}
	public void setQuoteList(List<AddQuoteInputBeanPhase2> quoteList) {
		this.quoteList = quoteList;
	}
	@Override
	public String toString() {
		return "PurchaseAddNewAssetRequestInputBean [requestorId="
				+ requestorId + ", purchaseRequestTypeId="
				+ purchaseRequestTypeId + ", fkAssetId=" + fkAssetId
				+ ", noOfUnits=" + noOfUnits + ", specifications="
				+ specifications + ", briefDescription=" + briefDescription
				+ ", assetRequiredDate=" + assetRequiredDate + ", duration="
				+ duration + ", isClientReimbursing=" + isClientReimbursing
				+ ", rmToApprove=" + rmToApprove + ", fkBuId=" + fkBuId
				+ ", locationId=" + locationId + ", licenceType=" + licenceType
				+ ", softwareLicenseOrSubscriptionId="
				+ softwareLicenseOrSubscriptionId + ", priority=" + priority
				+ ", quoteList=" + quoteList + "]";
	}
	
	
	
}