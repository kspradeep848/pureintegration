package com.thbs.mis.purchase.bean;

public class ClientAndProjectDetailsFromCompIdBean 
{
	private Integer clientId;
	private String clientName;
	private Integer projectId;
	private String projectName;
	private Integer componentId;
	private String componentName;
	private Byte internalProjectFlag;
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Integer getComponentId() {
		return componentId;
	}
	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public Byte getInternalProjectFlag() {
		return internalProjectFlag;
	}
	public void setInternalProjectFlag(Byte internalProjectFlag) {
		this.internalProjectFlag = internalProjectFlag;
	}
	@Override
	public String toString() {
		return "ClientAndProjectDetailsFromCompIdBean [clientId=" + clientId
				+ ", clientName=" + clientName + ", projectId=" + projectId
				+ ", projectName=" + projectName + ", componentId="
				+ componentId + ", componentName=" + componentName
				+ ", internalProjectFlag=" + internalProjectFlag + "]";
	}
	
}
