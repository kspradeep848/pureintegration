package com.thbs.mis.purchase.bean;


public class PurchasePOCreatePhase2Bean {

	private Integer assetReqId;
	
	private Integer createdBy;
	
	private Boolean isPoNeedToCreate;
	
	private String assetPriority;
	
	private Integer projectComponentId;

	private Integer clientId;

	private String isClientReimbursing;

	private Integer projectId;

	public Integer getAssetReqId() {
		return assetReqId;
	}

	public void setAssetReqId(Integer assetReqId) {
		this.assetReqId = assetReqId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Boolean getIsPoNeedToCreate() {
		return isPoNeedToCreate;
	}

	public void setIsPoNeedToCreate(Boolean isPoNeedToCreate) {
		this.isPoNeedToCreate = isPoNeedToCreate;
	}

	public String getAssetPriority() {
		return assetPriority;
	}

	public void setAssetPriority(String assetPriority) {
		this.assetPriority = assetPriority;
	}

	public Integer getProjectComponentId() {
		return projectComponentId;
	}

	public void setProjectComponentId(Integer projectComponentId) {
		this.projectComponentId = projectComponentId;
	}

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}

	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	@Override
	public String toString() {
		return "PurchasePOCreatePhase2Bean [assetReqId=" + assetReqId
				+ ", createdBy=" + createdBy + ", isPoNeedToCreate="
				+ isPoNeedToCreate + ", assetPriority=" + assetPriority
				+ ", projectComponentId=" + projectComponentId + ", clientId="
				+ clientId + ", isClientReimbursing=" + isClientReimbursing
				+ ", projectId=" + projectId + "]";
	}
	
	
}
