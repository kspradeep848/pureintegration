package com.thbs.mis.purchase.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseAssetRequestBean {

	private Integer assetRequestId;
	private Date raisedOn;
	private String assetDetail;
	private String businessUnit;
	private String requesterEmpName;
	private Integer requesterEmpId;
	private String Status;
	private Integer StatusId;
	private Integer fkRequestTypeId;
	
	public Integer getStatusId() {
		return StatusId;
	}

	public void setStatusId(Integer statusId) {
		StatusId = statusId;
	}

	public Integer getAssetRequestId() {
		return assetRequestId;
	}

	public void setAssetRequestId(Integer assetRequestId) {
		this.assetRequestId = assetRequestId;
	}
	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getRaisedOn() {
		return raisedOn;
	}

	public void setRaisedOn(Date raisedOn) {
		this.raisedOn = raisedOn;
	}

	public String getAssetDetail() {
		return assetDetail;
	}

	public void setAssetDetail(String assetDetail) {
		this.assetDetail = assetDetail;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}


	public String getRequesterEmpName() {
		return requesterEmpName;
	}

	public void setRequesterEmpName(String requesterEmpName) {
		this.requesterEmpName = requesterEmpName;
	}

	public Integer getRequesterEmpId() {
		return requesterEmpId;
	}

	public void setRequesterEmpId(Integer requesterEmpId) {
		this.requesterEmpId = requesterEmpId;
	}

	public Integer getFkRequestTypeId() {
		return fkRequestTypeId;
	}

	public void setFkRequestTypeId(Integer fkRequestTypeId) {
		this.fkRequestTypeId = fkRequestTypeId;
	}

	@Override
	public String toString() {
		return "PurchaseAssetRequestBean [assetRequestId=" + assetRequestId
				+ ", raisedOn=" + raisedOn + ", assetDetail=" + assetDetail
				+ ", businessUnit=" + businessUnit + ", requesterEmpName="
				+ requesterEmpName + ", requesterEmpId=" + requesterEmpId
				+ ", Status=" + Status + ", StatusId=" + StatusId
				+ ", fkRequestTypeId=" + fkRequestTypeId + "]";
	}
	
	
	
}
