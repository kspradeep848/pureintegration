package com.thbs.mis.purchase.bean;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class PurchaseAddVendorForExistingAssetBean {
	
	@NumberFormat(style = Style.NUMBER, pattern = "pkAssetId should be numeric value")
	@Min(1)
	@NotNull(message = "pkAssetId field can not be null")	
	private Integer pkAssetId;
	
	@NumberFormat(style = Style.NUMBER, pattern = "createdBy should be numeric value")
	@NotNull(message = "createdBy can not be null")
	@Min(1)
	private Integer createdBy;
	
	private List<PurchaseVendorDetailsBean> vendorDetails;

	public Integer getPkAssetId() {
		return pkAssetId;
	}

	public void setPkAssetId(Integer pkAssetId) {
		this.pkAssetId = pkAssetId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public List<PurchaseVendorDetailsBean> getVendorDetails() {
		return vendorDetails;
	}

	public void setVendorDetails(List<PurchaseVendorDetailsBean> vendorDetails) {
		this.vendorDetails = vendorDetails;
	}

	@Override
	public String toString() {
		return "PurchaseAddVendorForExistingAssetBeen [pkAssetId=" + pkAssetId
				+ ", createdBy=" + createdBy + ", vendorDetails="
				+ vendorDetails + "]";
	}
	

}
