package com.thbs.mis.purchase.bean;

import java.util.Date;

public class PurchaseUpdateNewAssetRequestInputBean {
			
	private Integer pkPurchaseRequestId;
	private Integer purchaseRequestTypeId;
	private Integer fkAssetId;
	private String softwareLicenseOrSubscriptionId;
	private Integer noOfUnits;	
	private String briefDescription;
	private String isClientReimbursing;
	private Integer rmToApprove;
	private Date assetRequiredDate;
	private Integer duration;
	private String specifications; 
	private Short fkBuId;
	private Short locationId;
	private Integer modifiedBy;
	private String licenceType;	
	private String priority;
	
    //private Integer requestorId;
	//private Integer projectId;	
	//private Integer fkAssetDetailsId;	
	//private Integer clientId;		
	//private Integer projectComponentId;

	public Integer getPurchaseRequestTypeId() {
		return purchaseRequestTypeId;
	}
	public void setPurchaseRequestTypeId(Integer purchaseRequestTypeId) {
		this.purchaseRequestTypeId = purchaseRequestTypeId;
	}
	public Integer getFkAssetId() {
		return fkAssetId;
	}
	public void setFkAssetId(Integer fkAssetId) {
		this.fkAssetId = fkAssetId;
	}
	public Integer getNoOfUnits() {
		return noOfUnits;
	}
	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	public String getSpecifications() {
		return specifications;
	}
	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}
	public String getBriefDescription() {
		return briefDescription;
	}
	public void setBriefDescription(String briefDescription) {
		this.briefDescription = briefDescription;
	}
	public Date getAssetRequiredDate() {
		return assetRequiredDate;
	}
	public void setAssetRequiredDate(Date assetRequiredDate) {
		this.assetRequiredDate = assetRequiredDate;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}
	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}
	
	public Integer getRmToApprove() {
		return rmToApprove;
	}
	public void setRmToApprove(Integer rmToApprove) {
		this.rmToApprove = rmToApprove;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	public Short getFkBuId() {
		return fkBuId;
	}
	public void setFkBuId(Short fkBuId) {
		this.fkBuId = fkBuId;
	}
	public Short getLocationId() {
		return locationId;
	}
	public void setLocationId(Short locationId) {
		this.locationId = locationId;
	}
	
	public Integer getPkPurchaseRequestId() {
		return pkPurchaseRequestId;
	}
	public void setPkPurchaseRequestId(Integer pkPurchaseRequestId) {
		this.pkPurchaseRequestId = pkPurchaseRequestId;
	}
	
	public String getLicenceType() {
		return licenceType;
	}
	public void setLicenceType(String licenceType) {
		this.licenceType = licenceType;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	
	public String getSoftwareLicenseOrSubscriptionId() {
		return softwareLicenseOrSubscriptionId;
	}
	public void setSoftwareLicenseOrSubscriptionId(
			String softwareLicenseOrSubscriptionId) {
		this.softwareLicenseOrSubscriptionId = softwareLicenseOrSubscriptionId;
	}
	
	
	@Override
	public String toString() {
		return "PurchaseUpdateNewAssetRequestInputBean [pkPurchaseRequestId="
				+ pkPurchaseRequestId + ", purchaseRequestTypeId="
				+ purchaseRequestTypeId + ", fkAssetId=" + fkAssetId
				+ ", softwareLicenseOrSubscriptionId="
				+ softwareLicenseOrSubscriptionId + ", noOfUnits=" + noOfUnits
				+ ", briefDescription=" + briefDescription
				+ ", isClientReimbursing=" + isClientReimbursing
				+ ", rmToApprove=" + rmToApprove + ", assetRequiredDate="
				+ assetRequiredDate + ", duration=" + duration
				+ ", specifications=" + specifications + ", fkBuId=" + fkBuId
				+ ", locationId=" + locationId + ", modifiedBy=" + modifiedBy
				+ ", licenceType=" + licenceType + ", priority=" + priority
				+ "]";
	}
	
}
