package com.thbs.mis.purchase.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class PurchaseOrderRequestOutputBean {
	
	private Integer assetRequestId;
	private String poDetails;
	private Date createdOn;
	private Short buId;
	private String buName;
	private Integer projectId;
	private String projectName;
	private Integer statusId;
	private String statusInfoString;
	private Integer poId;
	private String poNumber;
	public Integer getAssetRequestId() {
		return assetRequestId;
	}
	public void setAssetRequestId(Integer assetRequestId) {
		this.assetRequestId = assetRequestId;
	}
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Short getBuId() {
		return buId;
	}
	public void setBuId(Short buId) {
		this.buId = buId;
	}
	public String getBuName() {
		return buName;
	}
	public void setBuName(String buName) {
		this.buName = buName;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public String getStatusInfoString() {
		return statusInfoString;
	}
	public void setStatusInfoString(String statusInfoString) {
		this.statusInfoString = statusInfoString;
	}
	public Integer getPoId() {
		return poId;
	}
	public void setPoId(Integer poId) {
		this.poId = poId;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getPoDetails() {
		return poDetails;
	}
	public void setPoDetails(String poDetails) {
		this.poDetails = poDetails;
	}
	@Override
	public String toString() {
		return "PurchaseOrderRequestOutputBean [assetRequestId="
				+ assetRequestId + ", poDetails=" + poDetails + ", createdOn="
				+ createdOn + ", buId=" + buId + ", buName=" + buName
				+ ", projectId=" + projectId + ", projectName=" + projectName
				+ ", statusId=" + statusId + ", statusInfoString="
				+ statusInfoString + ", poId=" + poId + ", poNumber="
				+ poNumber + "]";
	}

}
