package com.thbs.mis.purchase.bean;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class PurchaseViewCapexRequestsByDateRangeAndBuIdOutputBean {

	
	private Integer purchaseRequestId;
	
	private Integer projectId;
	
	private Integer capexId;
	
	@JsonSerialize(using= CustomTimeSerializer.class)
	private Date createdOn;
	
	private Integer purchaseRequestStatusId;
	
	private String purchaseRequestStatusName;
	
	private Integer empId;
	
	private String empName;
	
	private String assetNameAndVersionOrModel;
	
	private String projectName;

	public Integer getPurchaseRequestId() {
		return purchaseRequestId;
	}

	public void setPurchaseRequestId(Integer purchaseRequestId) {
		this.purchaseRequestId = purchaseRequestId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getCapexId() {
		return capexId;
	}

	public void setCapexId(Integer capexId) {
		this.capexId = capexId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getPurchaseRequestStatusId() {
		return purchaseRequestStatusId;
	}

	public void setPurchaseRequestStatusId(Integer purchaseRequestStatusId) {
		this.purchaseRequestStatusId = purchaseRequestStatusId;
	}

	public String getPurchaseRequestStatusName() {
		return purchaseRequestStatusName;
	}

	public void setPurchaseRequestStatusName(String purchaseRequestStatusName) {
		this.purchaseRequestStatusName = purchaseRequestStatusName;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getAssetNameAndVersionOrModel() {
		return assetNameAndVersionOrModel;
	}

	public void setAssetNameAndVersionOrModel(String assetNameAndVersionOrModel) {
		this.assetNameAndVersionOrModel = assetNameAndVersionOrModel;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	@Override
	public String toString() {
		return "PurchaseViewCapexRequestsByDateRangeAndBuIdOutputBean [purchaseRequestId="
				+ purchaseRequestId
				+ ", projectId="
				+ projectId
				+ ", capexId="
				+ capexId
				+ ", createdOn="
				+ createdOn
				+ ", purchaseRequestStatusId="
				+ purchaseRequestStatusId
				+ ", purchaseRequestStatusName="
				+ purchaseRequestStatusName
				+ ", empId="
				+ empId
				+ ", empName="
				+ empName
				+ ", assetNameAndVersionOrModel="
				+ assetNameAndVersionOrModel
				+ ", projectName=" + projectName + "]";
	}
	
	
	
}
