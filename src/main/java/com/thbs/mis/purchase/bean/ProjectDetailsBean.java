package com.thbs.mis.purchase.bean;

public class ProjectDetailsBean {

	private Integer componentId;
	private String componentName;
	private int internalCompFlag;
	private Integer projectId;
	private String projectName;
	
	public Integer getComponentId() {
		return componentId;
	}
	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public int getInternalCompFlag() {
		return internalCompFlag;
	}
	public void setInternalCompFlag(int internalCompFlag) {
		this.internalCompFlag = internalCompFlag;
	}
	@Override
	public String toString() {
		return "ProjectDetailsBean [componentId=" + componentId
				+ ", componentName=" + componentName + ", internalCompFlag="
				+ internalCompFlag + ", projectId=" + projectId
				+ ", projectName=" + projectName + "]";
	}
	
}
