package com.thbs.mis.purchase.bean;

import java.util.Date;

public class PurchaseOrderDetailsSaveBean {

	private Integer assetReqId;
	
	private String isSoftwareOrHardwarePurchased;
	
	private String softwareLicOrSubscriptionId;
	
	private Integer systemAdminId;
	
	private Date assetAvailableFromDate;

	private Date assetAvailableToDate;
	
	private String sysAdminClosureComments;

	private String generatedAssetId;
	
	

	public String getGeneratedAssetId() {
		return generatedAssetId;
	}

	public void setGeneratedAssetId(String generatedAssetId) {
		this.generatedAssetId = generatedAssetId;
	}

	public String getSysAdminClosureComments() {
		return sysAdminClosureComments;
	}

	public void setSysAdminClosureComments(String sysAdminClosureComments) {
		this.sysAdminClosureComments = sysAdminClosureComments;
	}

	public Integer getAssetReqId() {
		return assetReqId;
	}

	public void setAssetReqId(Integer assetReqId) {
		this.assetReqId = assetReqId;
	}

	public String getIsSoftwareOrHardwarePurchased() {
		return isSoftwareOrHardwarePurchased;
	}

	public void setIsSoftwareOrHardwarePurchased(
			String isSoftwareOrHardwarePurchased) {
		this.isSoftwareOrHardwarePurchased = isSoftwareOrHardwarePurchased;
	}

	public String getSoftwareLicOrSubscriptionId() {
		return softwareLicOrSubscriptionId;
	}

	public void setSoftwareLicOrSubscriptionId(String softwareLicOrSubscriptionId) {
		this.softwareLicOrSubscriptionId = softwareLicOrSubscriptionId;
	}

	
	public Date getAssetAvailableFromDate() {
		return assetAvailableFromDate;
	}

	public void setAssetAvailableFromDate(Date assetAvailableFromDate) {
		this.assetAvailableFromDate = assetAvailableFromDate;
	}

	public Date getAssetAvailableToDate() {
		return assetAvailableToDate;
	}

	public void setAssetAvailableToDate(Date assetAvailableToDate) {
		this.assetAvailableToDate = assetAvailableToDate;
	}	
	
	public Integer getSystemAdminId() {
		return systemAdminId;
	}

	public void setSystemAdminId(Integer systemAdminId) {
		this.systemAdminId = systemAdminId;
	}

	@Override
	public String toString() {
		return "PurchaseOrderDetailsSaveBean [assetReqId=" + assetReqId
				+ ", isSoftwareOrHardwarePurchased="
				+ isSoftwareOrHardwarePurchased
				+ ", softwareLicOrSubscriptionId="
				+ softwareLicOrSubscriptionId + ", systemAdminId="
				+ systemAdminId + ", assetAvailableFromDate="
				+ assetAvailableFromDate + ", assetAvailableToDate="
				+ assetAvailableToDate + ", sysAdminClosureComments="
				+ sysAdminClosureComments + ", generatedAssetId="
				+ generatedAssetId + "]";
	}


	
	
}
