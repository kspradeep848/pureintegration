
//Added by Prathibha

package com.thbs.mis.purchase.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;





public class PurchaseMasAssetDetailOutputBean {
	
	
	
	private Integer pkAssetDetailId;
	
	
	private Date assetAvailableFromDate;
	private Date assetAvailableToDate;
	private Integer assetDetailCreatedBy;
	private Date assetDetailCreatedDate;
	private Integer assetDetailModifiedBy;
	private Date assetDetailModifiedDate;
	private String softwareLicenseOrSubscriptionId;
	private Integer fkMasAssetsId;
	private String assetDescription;
	
	
	
	private Integer pkAssetId;
	private String assetType;
	private String assetNameAndVersionOrModel;
	private String isAssetAvailable;
	private Integer createdBy;
	private Date assetCreatedDate;
	private Integer assetModifiedBy;
	private Date assetModifiedDate;
	
	
	
	
	public Integer getPkAssetDetailId() {
		return pkAssetDetailId;
	}
	public void setPkAssetDetailId(Integer pkAssetDetailId) {
		this.pkAssetDetailId = pkAssetDetailId;
	}
	public String getAssetDescription() {
		return assetDescription;
	}
	public void setAssetDescription(String assetDescription) {
		this.assetDescription = assetDescription;
	}
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getAssetAvailableFromDate() {
		return assetAvailableFromDate;
	}
	public void setAssetAvailableFromDate(Date assetAvailableFromDate) {
		this.assetAvailableFromDate = assetAvailableFromDate;
	}
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getAssetAvailableToDate() {
		return assetAvailableToDate;
	}
	public void setAssetAvailableToDate(Date assetAvailableToDate) {
		this.assetAvailableToDate = assetAvailableToDate;
	}
	
	public String getSoftwareLicenseOrSubscriptionId() {
		return softwareLicenseOrSubscriptionId;
	}
	public void setSoftwareLicenseOrSubscriptionId(
			String softwareLicenseOrSubscriptionId) {
		this.softwareLicenseOrSubscriptionId = softwareLicenseOrSubscriptionId;
	}
	public Integer getFkMasAssetsId() {
		return fkMasAssetsId;
	}
	public void setFkMasAssetsId(Integer fkMasAssetsId) {
		this.fkMasAssetsId = fkMasAssetsId;
	}
	
	
	
	
	


	public Integer getPkAssetId() {
		return pkAssetId;
	}
	public void setPkAssetId(Integer pkAssetId) {
		this.pkAssetId = pkAssetId;
	}
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public String getAssetNameAndVersionOrModel() {
		return assetNameAndVersionOrModel;
	}
	public void setAssetNameAndVersionOrModel(String assetNameAndVersionOrModel) {
		this.assetNameAndVersionOrModel = assetNameAndVersionOrModel;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	public Integer getAssetDetailCreatedBy() {
		return assetDetailCreatedBy;
	}
	public void setAssetDetailCreatedBy(Integer assetDetailCreatedBy) {
		this.assetDetailCreatedBy = assetDetailCreatedBy;
	}
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getAssetDetailCreatedDate() {
		return assetDetailCreatedDate;
	}
	public void setAssetDetailCreatedDate(Date assetDetailCreatedDate) {
		this.assetDetailCreatedDate = assetDetailCreatedDate;
	}
	public Integer getAssetDetailModifiedBy() {
		return assetDetailModifiedBy;
	}
	public void setAssetDetailModifiedBy(Integer assetDetailModifiedBy) {
		this.assetDetailModifiedBy = assetDetailModifiedBy;
	}
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getAssetDetailModifiedDate() {
		return assetDetailModifiedDate;
	}
	public void setAssetDetailModifiedDate(Date assetDetailModifiedDate) {
		this.assetDetailModifiedDate = assetDetailModifiedDate;
	}
	public String getIsAssetAvailable() {
		return isAssetAvailable;
	}
	public void setIsAssetAvailable(String isAssetAvailable) {
		this.isAssetAvailable = isAssetAvailable;
	}
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getAssetCreatedDate() {
		return assetCreatedDate;
	}
	public void setAssetCreatedDate(Date assetCreatedDate) {
		this.assetCreatedDate = assetCreatedDate;
	}
	public Integer getAssetModifiedBy() {
		return assetModifiedBy;
	}
	public void setAssetModifiedBy(Integer assetModifiedBy) {
		this.assetModifiedBy = assetModifiedBy;
	}
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getAssetModifiedDate() {
		return assetModifiedDate;
	}
	public void setAssetModifiedDate(Date assetModifiedDate) {
		this.assetModifiedDate = assetModifiedDate;
	}
	@Override
	public String toString() {
		return "PurchaseMasAssetDetailOutputBean [pkAssetDetailId="
				+ pkAssetDetailId + ", assetAvailableFromDate="
				+ assetAvailableFromDate + ", assetAvailableToDate="
				+ assetAvailableToDate + ", assetDetailCreatedBy="
				+ assetDetailCreatedBy + ", assetDetailCreatedDate="
				+ assetDetailCreatedDate + ", assetDetailModifiedBy="
				+ assetDetailModifiedBy + ", assetDetailModifiedDate="
				+ assetDetailModifiedDate
				+ ", softwareLicenseOrSubscriptionId="
				+ softwareLicenseOrSubscriptionId + ", fkMasAssetsId="
				+ fkMasAssetsId + ", assetDescription=" + assetDescription
				+ ", pkAssetId=" + pkAssetId + ", assetType=" + assetType
				+ ", assetNameAndVersionOrModel=" + assetNameAndVersionOrModel
				+ ", isAssetAvailable=" + isAssetAvailable + ", createdBy="
				+ createdBy + ", assetCreatedDate=" + assetCreatedDate
				+ ", assetModifiedBy=" + assetModifiedBy
				+ ", assetModifiedDate=" + assetModifiedDate + "]";
	}
	
}
