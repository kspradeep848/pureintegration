package com.thbs.mis.purchase.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "purchase_asset_increment")
@NamedQuery(name = "PurchaseDatAssetIncrementBO.findAll", query = "SELECT p FROM PurchaseDatAssetIncrementBO p")
public class PurchaseDatAssetIncrementBO  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="pk_id")
	private Integer pkId;
	
	@Column(name="asset_id")
	private Integer assetId;
	
	@Column(name="location_id")
	private Short locationId;
	
	@Column(name="com_asset_name")
	private String commonAssetName;
	
	@Column(name="last_asset_id")
	private Integer lastAssetId;

	public Integer getPkId() {
		return pkId;
	}

	public void setPkId(Integer pkId) {
		this.pkId = pkId;
	}

	public Integer getAssetId() {
		return assetId;
	}

	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}

	public Short getLocationId() {
		return locationId;
	}

	public void setLocationId(Short locationId) {
		this.locationId = locationId;
	}

	public String getCommonAssetName() {
		return commonAssetName;
	}

	public void setCommonAssetName(String commonAssetName) {
		this.commonAssetName = commonAssetName;
	}

	public Integer getLastAssetId() {
		return lastAssetId;
	}

	public void setLastAssetId(Integer lastAssetId) {
		this.lastAssetId = lastAssetId;
	}

	@Override
	public String toString() {
		return "PurchaseDatAssetIncrementBO [pkId=" + pkId + ", assetId="
				+ assetId + ", locationId=" + locationId + ", commonAssetName="
				+ commonAssetName + ", lastAssetId=" + lastAssetId + "]";
	}
	
	
}
