package com.thbs.mis.purchase.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToOne;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



/**
 * The persistent class for the purchase_mas_asset_detail database table.
 * 
 */
@Entity
@Table(name = "purchase_mas_asset_detail")
@NamedStoredProcedureQuery(name = "ADD_ASSET_DEATILS", procedureName = "ADD_ASSET_DEATILS", parameters = {
		  @StoredProcedureParameter(mode = ParameterMode.IN, name = "assetDescription", type = String.class),
		  @StoredProcedureParameter(mode = ParameterMode.IN, name = "fromDate", type = Date.class),
		  @StoredProcedureParameter(mode = ParameterMode.IN, name = "toDate", type = Date.class),
		  @StoredProcedureParameter(mode = ParameterMode.IN, name = "assetId", type = Integer.class),
		  @StoredProcedureParameter(mode = ParameterMode.IN, name = "createdBy", type = Integer.class),
		  @StoredProcedureParameter(mode = ParameterMode.IN, name = "locationId", type = Integer.class),
		  @StoredProcedureParameter(mode = ParameterMode.IN, name = "noOfUnits", type = Integer.class),
		  @StoredProcedureParameter(mode = ParameterMode.IN, name = "purchaseReqId", type = Integer.class),
		  @StoredProcedureParameter(mode = ParameterMode.OUT, name = "firstAssetID", type = String.class),
		  @StoredProcedureParameter(mode = ParameterMode.OUT, name = "lastAssetID", type = String.class)})

@NamedQuery(name = "PurchaseMasAssetDetailsBO.findAll", query = "SELECT p FROM PurchaseMasAssetDetailsBO p")
public class PurchaseMasAssetDetailsBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_asset_detail_id")
	private Integer pkAssetDetailId;
	
	@Column(name = "asset_description")
	private String assetDescription;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "asset_available_from_date")
	private Date assetAvailableFromDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "asset_available_to_date")
	private Date assetAvailableToDate;

	@Column(name = "created_by")
	private Integer createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private Integer modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	@Column(name = "software_license_or_subscription_id")
	private String softwareLicenseOrSubscriptionId;

	@Column(name = "fk_mas_assets_id")
	private Integer fkMasAssetsId;
	
	@Column(name = "fklocation_id")
	private Integer fklocationId;
	
	@Column(name = "fk_purchase_request_id")
	private Integer fkPurchaseRequestId;

	@OneToOne
	@JoinColumn(name = "fk_mas_assets_id", unique = true, nullable = true, insertable = false, updatable = false)
	private PurchaseMasAssetsBO purchaseMasAssetsBO;

	public Integer getPkAssetDetailId() {
		return pkAssetDetailId;
	}

	
	public Integer getFklocationId() {
		return fklocationId;
	}


	public void setFklocationId(Integer fklocationId) {
		this.fklocationId = fklocationId;
	}


	public Integer getFkPurchaseRequestId() {
		return fkPurchaseRequestId;
	}


	public void setFkPurchaseRequestId(Integer fkPurchaseRequestId) {
		this.fkPurchaseRequestId = fkPurchaseRequestId;
	}


	public void setPkAssetDetailId(Integer pkAssetDetailId) {
		this.pkAssetDetailId = pkAssetDetailId;
	}

	public Date getAssetAvailableFromDate() {
		return assetAvailableFromDate;
	}

	public void setAssetAvailableFromDate(Date assetAvailableFromDate) {
		this.assetAvailableFromDate = assetAvailableFromDate;
	}

	public Date getAssetAvailableToDate() {
		return assetAvailableToDate;
	}

	public void setAssetAvailableToDate(Date assetAvailableToDate) {
		this.assetAvailableToDate = assetAvailableToDate;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getSoftwareLicenseOrSubscriptionId() {
		return softwareLicenseOrSubscriptionId;
	}

	public void setSoftwareLicenseOrSubscriptionId(
			String softwareLicenseOrSubscriptionId) {
		this.softwareLicenseOrSubscriptionId = softwareLicenseOrSubscriptionId;
	}

	public Integer getFkMasAssetsId() {
		return fkMasAssetsId;
	}

	public void setFkMasAssetsId(Integer fkMasAssetsId) {
		this.fkMasAssetsId = fkMasAssetsId;
	}

	public PurchaseMasAssetsBO getPurchaseMasAssetsBO() {
		return purchaseMasAssetsBO;
	}

	public void setPurchaseMasAssetsBO(PurchaseMasAssetsBO purchaseMasAssetsBO) {
		this.purchaseMasAssetsBO = purchaseMasAssetsBO;
	}

	public String getAssetDescription() {
		return assetDescription;
	}

	public void setAssetDescription(String assetDescription) {
		this.assetDescription = assetDescription;
	}


	@Override
	public String toString() {
		return "PurchaseMasAssetDetailsBO [pkAssetDetailId=" + pkAssetDetailId
				+ ", assetDescription=" + assetDescription
				+ ", assetAvailableFromDate=" + assetAvailableFromDate
				+ ", assetAvailableToDate=" + assetAvailableToDate
				+ ", createdBy=" + createdBy + ", createdDate=" + createdDate
				+ ", modifiedBy=" + modifiedBy + ", modifiedDate="
				+ modifiedDate + ", softwareLicenseOrSubscriptionId="
				+ softwareLicenseOrSubscriptionId + ", fkMasAssetsId="
				+ fkMasAssetsId + ", fklocationId=" + fklocationId
				+ ", fkPurchaseRequestId=" + fkPurchaseRequestId
				+ ", purchaseMasAssetsBO=" + purchaseMasAssetsBO + "]";
	}

	

}