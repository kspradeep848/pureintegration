package com.thbs.mis.purchase.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the purchase_dat_request_type database table.
 * 
 */
@Entity
@Table(name="purchase_dat_request_type")
@NamedQuery(name="PurchaseDatRequestType.findAll", query="SELECT p FROM PurchaseDatRequestTypeBO p")
public class PurchaseDatRequestTypeBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="purchase_request_type_id")
	private int purchaseRequestTypeId;

	@Column(name="purchase_request_type_name")
	private String purchaseRequestTypeName;

	public PurchaseDatRequestTypeBO() {
	}

	public int getPurchaseRequestTypeId() {
		return this.purchaseRequestTypeId;
	}

	public void setPurchaseRequestTypeId(int purchaseRequestTypeId) {
		this.purchaseRequestTypeId = purchaseRequestTypeId;
	}

	public String getPurchaseRequestTypeName() {
		return this.purchaseRequestTypeName;
	}

	public void setPurchaseRequestTypeName(String purchaseRequestTypeName) {
		this.purchaseRequestTypeName = purchaseRequestTypeName;
	}

	@Override
	public String toString() {
		return "PurchaseDatRequestTypeBO [purchaseRequestTypeId="
				+ purchaseRequestTypeId + ", purchaseRequestTypeName="
				+ purchaseRequestTypeName + "]";
	}

}