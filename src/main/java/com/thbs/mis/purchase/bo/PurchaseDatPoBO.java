package com.thbs.mis.purchase.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;

/**
 * The persistent class for the purchase_dat_po database table.
 * 
 */
@Entity
@Table(name = "purchase_dat_po")
@NamedQuery(name = "PurchaseDatPoBO.findAll", query = "SELECT p FROM PurchaseDatPoBO p")
public class PurchaseDatPoBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_poId")
	private Integer pkPoId;

	@Column(name = "modified_by")
	private Integer modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_on")
	private Date modifiedOn;

	@Column(name = "po_no")
	private String poNo;

	@Column(name = "po_uploaded")
	private String poUploaded;

	@Column(name = "prepared_by")
	private Integer preparedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "prepared_on")
	private Date preparedOn;

	@OneToOne
	@JoinColumn(name = "prepared_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailPreparedBy;

	@OneToOne
	@JoinColumn(name = "modified_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailModifiedBy;

	public PurchaseDatPoBO() {
	}

	public Integer getPkPoId() {
		return pkPoId;
	}



	public void setPkPoId(Integer pkPoId) {
		this.pkPoId = pkPoId;
	}



	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public String getPoUploaded() {
		return poUploaded;
	}

	public void setPoUploaded(String poUploaded) {
		this.poUploaded = poUploaded;
	}

	public Integer getPreparedBy() {
		return preparedBy;
	}

	public void setPreparedBy(Integer preparedBy) {
		this.preparedBy = preparedBy;
	}

	public Date getPreparedOn() {
		return preparedOn;
	}

	public void setPreparedOn(Date preparedOn) {
		this.preparedOn = preparedOn;
	}

	public DatEmpDetailBO getDatEmpDetailPreparedBy() {
		return datEmpDetailPreparedBy;
	}

	public void setDatEmpDetailPreparedBy(DatEmpDetailBO datEmpDetailPreparedBy) {
		this.datEmpDetailPreparedBy = datEmpDetailPreparedBy;
	}

	public DatEmpDetailBO getDatEmpDetailModifiedBy() {
		return datEmpDetailModifiedBy;
	}

	public void setDatEmpDetailModifiedBy(DatEmpDetailBO datEmpDetailModifiedBy) {
		this.datEmpDetailModifiedBy = datEmpDetailModifiedBy;
	}

	@Override
	public String toString() {
		return "PurchaseDatPoBO [pk_poId=" + pkPoId + ", modifiedBy="
				+ modifiedBy + ", modifiedOn=" + modifiedOn + ", poNo=" + poNo
				+ ", poUploaded=" + poUploaded + ", preparedBy=" + preparedBy
				+ ", preparedOn=" + preparedOn + "]";
	}

}
