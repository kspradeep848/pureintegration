package com.thbs.mis.purchase.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasBuUnitBO;

/**
 * The persistent class for the purchase_dat_bu database table.
 * 
 */
@Entity
@Table(name = "purchase_dat_bu")
@NamedQuery(name = "PurchaseDatBuBO.findAll", query = "SELECT p FROM PurchaseDatBuBO p")
public class PurchaseDatBuBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_purchase_dat_bu_id")
	private short pkPurchaseDatBuId;

	@Column(name = "fk_purchase_quote_id")
	private Integer fkPurchaseQuoteId;

	@Column(name = "bu_head_comment")
	private String buHeadComment;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	@Column(name = "bu_head_id")
	private short buHeadId;

	@Column(name = "created_by")
	private Integer createdBy;

	@Column(name = "modified_by")
	private Integer modifiedBy;
	
	@Column(name = "fk_mas_bu_id")
	private short fkMasBuId;

	@Column(name = "fk_purchase_dat_bu_status")
	private String fkPurchaseDatBuStatus;

	@OneToOne
	@JoinColumn(name = "fk_mas_bu_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasBuUnitBO masBuUnitBO;

	@OneToOne
	@JoinColumn(name = "created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO fkCreatedBy;

	@OneToOne
	@JoinColumn(name = "modified_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO fkModifiedBy;
	

	@OneToOne
	@JoinColumn(name = "fk_purchase_quote_id", unique = true, nullable = true, insertable = false, updatable = false)
	private PurchaseDatAssetQuoteBO purchaseDatAssetQuoteBO;
	
	public PurchaseDatBuBO() {
	}

	public short getPkPurchaseDatBuId() {
		return this.pkPurchaseDatBuId;
	}

	public void setPkPurchaseDatBuId(short pkPurchaseDatBuId) {
		this.pkPurchaseDatBuId = pkPurchaseDatBuId;
	}

	public String getBuHeadComment() {
		return this.buHeadComment;
	}

	public void setBuHeadComment(String buHeadComment) {
		this.buHeadComment = buHeadComment;
	}

	public short getBuHeadId() {
		return this.buHeadId;
	}

	public void setBuHeadId(short buHeadId) {
		this.buHeadId = buHeadId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public short getFkMasBuId() {
		return this.fkMasBuId;
	}

	public void setFkMasBuId(short fkMasBuId) {
		this.fkMasBuId = fkMasBuId;
	}

	public String getFkPurchaseDatBuStatus() {
		return this.fkPurchaseDatBuStatus;
	}

	public void setFkPurchaseDatBuStatus(String fkPurchaseDatBuStatus) {
		this.fkPurchaseDatBuStatus = fkPurchaseDatBuStatus;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;

	}

	public MasBuUnitBO getMasBuUnitBO() {
		return masBuUnitBO;
	}

	public void setMasBuUnitBO(MasBuUnitBO masBuUnitBO) {
		this.masBuUnitBO = masBuUnitBO;
	}
	public DatEmpDetailBO getFkCreatedBy() {
		return fkCreatedBy;
	}

	public void setFkCreatedBy(DatEmpDetailBO fkCreatedBy) {
		this.fkCreatedBy = fkCreatedBy;
	}

	public DatEmpDetailBO getFkModifiedBy() {
		return fkModifiedBy;
	}

	public void setFkModifiedBy(DatEmpDetailBO fkModifiedBy) {
		this.fkModifiedBy = fkModifiedBy;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;

	}

	public Integer getFkPurchaseQuoteId() {
		return fkPurchaseQuoteId;
	}

	public void setFkPurchaseQuoteId(Integer fkPurchaseQuoteId) {
		this.fkPurchaseQuoteId = fkPurchaseQuoteId;
	}

	/*public PurchaseDatBuBO getPurchaseDatBuBO() {
		return purchaseDatBuBO;
	}

	public void setPurchaseDatBuBO(PurchaseDatBuBO purchaseDatBuBO) {
		this.purchaseDatBuBO = purchaseDatBuBO;
	}*/

	/*public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}*/


	
	public PurchaseDatAssetQuoteBO getPurchaseDatAssetQuoteBO() {
		return purchaseDatAssetQuoteBO;
	}

	public void setPurchaseDatAssetQuoteBO(
			PurchaseDatAssetQuoteBO purchaseDatAssetQuoteBO) {
		this.purchaseDatAssetQuoteBO = purchaseDatAssetQuoteBO;
	}

	@Override
	public String toString() {
		return "PurchaseDatBuBO [pkPurchaseDatBuId=" + pkPurchaseDatBuId
				+ ", fkPurchaseQuoteId=" + fkPurchaseQuoteId
				+ ", buHeadComment=" + buHeadComment + ", createdDate="
				+ createdDate + ", modifiedDate=" + modifiedDate
				+ ", buHeadId=" + buHeadId + ", createdBy=" + createdBy
				+ ", modifiedBy=" + modifiedBy + ", fkMasBuId=" + fkMasBuId
				+ ", fkPurchaseDatBuStatus=" + fkPurchaseDatBuStatus
				+ ", masBuUnitBO=" + masBuUnitBO + ", fkCreatedBy="
				+ fkCreatedBy + ", fkModifiedBy=" + fkModifiedBy
				+ ", purchaseDatAssetQuoteBO=" + purchaseDatAssetQuoteBO + "]";
	}
	
}