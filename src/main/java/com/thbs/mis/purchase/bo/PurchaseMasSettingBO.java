package com.thbs.mis.purchase.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the purchase_mas_settings database table.
 * 
 */
@Entity
@Table(name = "purchase_mas_settings")
@NamedQuery(name = "PurchaseMasSettingBO.findAll", query = "SELECT p FROM PurchaseMasSettingBO p")
public class PurchaseMasSettingBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_purchase_setting_id")
	private int pkPurchaseSettingId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "created_by")
	private Integer createdBy;

	@Column(name = "system_admin_id")
	private Integer systemAdminId;

	@Column(name = "setting_type")
	private String settingType;

	@Column(name = "modified_by")
	private Integer modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	public PurchaseMasSettingBO() {
	}

	public int getPkPurchaseSettingId() {
		return this.pkPurchaseSettingId;
	}

	public void setPkPurchaseSettingId(int pkPurchaseSettingId) {
		this.pkPurchaseSettingId = pkPurchaseSettingId;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getSettingType() {
		return this.settingType;
	}

	public void setSettingType(String settingType) {
		this.settingType = settingType;
	}

	public Integer getSystemAdminId() {
		return systemAdminId;
	}

	public void setSystemAdminId(Integer systemAdminId) {
		this.systemAdminId = systemAdminId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Override
	public String toString() {
		return "PurchaseMasSettingBO [pkPurchaseSettingId="
				+ pkPurchaseSettingId + ", createdDate=" + createdDate
				+ ", createdBy=" + createdBy + ", systemAdminId="
				+ systemAdminId + ", settingType=" + settingType
				+ ", modifiedBy=" + modifiedBy + ", modifiedDate="
				+ modifiedDate + "]";
	}

}
