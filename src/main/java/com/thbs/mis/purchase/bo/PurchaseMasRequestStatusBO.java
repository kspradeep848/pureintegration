package com.thbs.mis.purchase.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the purchase_mas_request_status database table.
 * 
 */
@Entity
@Table(name="purchase_mas_request_status")
@NamedQuery(name="PurchaseMasRequestStatusBO.findAll", query="SELECT p FROM PurchaseMasRequestStatusBO p")
public class PurchaseMasRequestStatusBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="purchase_request_status_id")
	private int purchaseRequestStatusId;

	@Column(name="purchase_request_status_name")
	private String purchaseRequestStatusName;

	public PurchaseMasRequestStatusBO() {
	}

	public int getPurchaseRequestStatusId() {
		return this.purchaseRequestStatusId;
	}

	public void setPurchaseRequestStatusId(int purchaseRequestStatusId) {
		this.purchaseRequestStatusId = purchaseRequestStatusId;
	}

	public String getPurchaseRequestStatusName() {
		return this.purchaseRequestStatusName;
	}

	public void setPurchaseRequestStatusName(String purchaseRequestStatusName) {
		this.purchaseRequestStatusName = purchaseRequestStatusName;
	}

	@Override
	public String toString() {
		return "PurchaseMasRequestStatusBO [purchaseRequestStatusId="
				+ purchaseRequestStatusId + ", purchaseRequestStatusName="
				+ purchaseRequestStatusName + "]";
	}

}