package com.thbs.mis.purchase.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the purchase_mas_assets database table.
 * 
 */
@Entity
@Table(name = "purchase_mas_assets")
@NamedQuery(name = "PurchaseMasAssetsBO.findAll", query = "SELECT p FROM PurchaseMasAssetsBO p")
public class PurchaseMasAssetsBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_asset_id")
	private Integer pkAssetId;

	@Column(name = "asset_name_and_version_or_model")
	private String assetNameAndVersionOrModel;

	@Column(name = "is_available")
	private String isAvailable;

	@Column(name = "created_by")
	private Integer createdBy;

	@Column(name = "asset_type")
	private String assetType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private Integer modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;	
	
	@Column(name = "licence_type")
	private String licenseType;
	
	public PurchaseMasAssetsBO() {
	}
	

	public Integer getPkAssetId() {
		return pkAssetId;
	}

	public void setPkAssetId(Integer pkAssetId) {
		this.pkAssetId = pkAssetId;
	}

	public String getAssetNameAndVersionOrModel() {
		return this.assetNameAndVersionOrModel;
	}

	public void setAssetNameAndVersionOrModel(String assetNameAndVersionOrModel) {
		this.assetNameAndVersionOrModel = assetNameAndVersionOrModel;
	}

	public String getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(String isAvailable) {
		this.isAvailable = isAvailable;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getLicenseType() {
		return licenseType;
	}


	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}

	@Override
	public String toString() {
		return "PurchaseMasAssetsBO [pkAssetId=" + pkAssetId
				+ ", assetNameAndVersionOrModel=" + assetNameAndVersionOrModel
				+ ", isAvailable=" + isAvailable + ", createdBy=" + createdBy
				+ ", assetType=" + assetType + ", createdDate=" + createdDate
				+ ", modifiedBy=" + modifiedBy + ", modifiedDate="
				+ modifiedDate + ", licenseType=" + licenseType + "]";
	}

}
