package com.thbs.mis.purchase.bo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;

/**
 * The persistent class for the purchase_dat_supplier_details database table.
 * 
 */
@Entity
@Table(name = "purchase_dat_supplier_details")
@NamedQuery(name = "PurchaseDatSupplierDetailBO.findAll", query = "SELECT p FROM PurchaseDatSupplierDetailBO p")
public class PurchaseDatSupplierDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_supplier_id")
	private Integer pkSupplierId;

	@Column(name = "vendor_name")
	private String vendorName;

	@Column(name = "vendor_address")
	private String vendorAddress;

	@Column(name = "contact_person")
	private String contactPerson;

	@Column(name = "vendor_contact_no")
	private String vendorContactNo;

	@Column(name = "email_id")
	private String emailId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_on")
	private Date modifiedOn;

	@Column(name = "modified_by")
	private Integer modifiedBy;

	@Column(name = "price_quoted_by_vendor")
	private Double priceQuotedByVendor;

	@Column(name = "fk_asset_id")
	private Integer fkAssetId;

	@Column(name = "is_available")
	private String isAvailable;

	@Column(name = "delivery_time")
	private Integer deliveryTime;

	@Column(name = "created_by")
	private Integer createdBy;

	@OneToOne
	@JoinColumn(name = "created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailCreatedBy;

	@OneToOne
	@JoinColumn(name = "modified_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailModifyBy;

	@OneToOne
	@JoinColumn(name = "fk_asset_id", unique = true, nullable = true, insertable = false, updatable = false)
	private PurchaseMasAssetsBO purchaseMasAssetsBO;

	public PurchaseDatSupplierDetailBO() {
	}

	public Integer getPkSupplierId() {
		return pkSupplierId;
	}

	public void setPkSupplierId(Integer pkSupplierId) {
		this.pkSupplierId = pkSupplierId;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Double getPriceQuotedByVendor() {
		return priceQuotedByVendor;
	}

	public void setPriceQuotedByVendor(Double priceQuotedByVendor) {
		this.priceQuotedByVendor = priceQuotedByVendor;
	}

	public String getVendorAddress() {
		return vendorAddress;
	}

	public void setVendorAddress(String vendorAddress) {
		this.vendorAddress = vendorAddress;
	}

	public String getVendorContactNo() {
		return vendorContactNo;
	}

	public void setVendorContactNo(String vendorContactNo) {
		this.vendorContactNo = vendorContactNo;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public DatEmpDetailBO getDatEmpDetailCreatedBy() {
		return datEmpDetailCreatedBy;
	}

	public void setDatEmpDetailCreatedBy(DatEmpDetailBO datEmpDetailCreatedBy) {
		this.datEmpDetailCreatedBy = datEmpDetailCreatedBy;
	}

	public DatEmpDetailBO getDatEmpDetailModifyBy() {
		return datEmpDetailModifyBy;
	}

	public void setDatEmpDetailModifyBy(DatEmpDetailBO datEmpDetailModifyBy) {
		this.datEmpDetailModifyBy = datEmpDetailModifyBy;
	}

	public Integer getFkAssetId() {
		return fkAssetId;
	}

	public void setFkAssetId(Integer fkAssetId) {
		this.fkAssetId = fkAssetId;
	}

	public PurchaseMasAssetsBO getPurchaseMasAssetsBO() {
		return purchaseMasAssetsBO;
	}

	public void setPurchaseMasAssetsBO(PurchaseMasAssetsBO purchaseMasAssetsBO) {
		this.purchaseMasAssetsBO = purchaseMasAssetsBO;
	}

	public String getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(String isAvailable) {
		this.isAvailable = isAvailable;
	}

	public Integer getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(Integer deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public String toString() {
		return "PurchaseDatSupplierDetailBO [pkSupplierId=" + pkSupplierId
				+ ", vendorName=" + vendorName + ", vendorAddress="
				+ vendorAddress + ", contactPerson=" + contactPerson
				+ ", vendorContactNo=" + vendorContactNo + ", emailId="
				+ emailId + ", createdDate=" + createdDate + ", modifiedOn="
				+ modifiedOn + ", modifiedBy=" + modifiedBy
				+ ", priceQuotedByVendor=" + priceQuotedByVendor
				+ ", fkAssetId=" + fkAssetId + ", isAvailable=" + isAvailable
				+ ", deliveryTime=" + deliveryTime + ", createdBy=" + createdBy
				+ ", datEmpDetailCreatedBy=" + datEmpDetailCreatedBy
				+ ", datEmpDetailModifyBy=" + datEmpDetailModifyBy +
				 "]";
	}

}