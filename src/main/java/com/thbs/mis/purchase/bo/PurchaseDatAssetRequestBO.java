package com.thbs.mis.purchase.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.bo.MasLocationParentBO;



/**
 * The persistent class for the purchase_dat_asset_request database table.
 * 
 */
@Entity
@Table(name = "purchase_dat_asset_request")
@NamedQuery(name = "PurchaseDatAssetRequestBO.findAll", query = "SELECT p FROM PurchaseDatAssetRequestBO p")
public class PurchaseDatAssetRequestBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_purchase_requestId")
	private Integer pkPurchaseRequestId;

	@Column(name = "brief_description")
	private String briefDescription;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "asset_required_by")
	private Date assetRequiredBy;

	@Column(name = "fk_bu_id")
	private short fkBuId;
	
	@Column(name = "duration")
	private Integer duration;

	@Column(name = "is_client_reimbursing")
	private String isClientReimbursing;

	@Column(name = "is_software_or_hardware_purchased")
	private String isSoftwareOrHardwarePurchased;

	@Column(name = "modified_by")
	private Integer modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_on")
	private Date modifiedOn;

	@Column(name = "rm_comment")
	private String rmComment;

	@Column(name = "rm_to_approve")
	private Integer rmToApprove;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "rm_updated_on")
	private Date rmUpdatedOn;

	@Column(name = "supporting_documents_by_client")
	private String supportingDocumentsByClient;

	@Column(name = "sys_admin_comment")
	private String sysAdminComment;

	@Column(name = "sys_admin_id")
	private Integer sysAdminId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "sys_admin_updated_on")
	private Date sysAdminUpdatedOn;

	@Column(name = "quotes_uploaded")
	private String quotesUploaded;

	@Column(name = "supporting_documents_by_sysadmin")
	private String supportingDocumentsBySysAdmin;

	@Column(name = "bu_head_approval_needed")
	private String buHeadApprovalNeeded;

	@Column(name = "client_id")
	private Integer clientId;

	@Column(name = "po_creation_required")
	private String poCreationRequired;

	@Column(name = "closure_comment")
	private String closureComment;	

	@Column(name = "financeEx_comment")
	private String financeEx_comment;

	@Column(name = "financeEx_updated_by")
	private Integer financeEx_updated_by;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "financeEx_updated_on")
	private Date financeEx_updated_on;

	@Column(name = "location_id")
	private short locationId;

	@Column(name = "no_of_units")
	private Integer noOfUnits;

	@Column(name = "priority")
	private String priority;

	@Column(name = "project_component_id")
	private Integer projectComponentId;

	@Column(name = "requestor_id")
	private Integer requestorId;

	private String specifications;

	@Column(name = "fk_asset_id")
	private Integer fkAssetId;

	@Column(name = "purchase_request_status")
	private Integer purchaseRequestStatus;

	@Column(name = "fk_request_type_id")
	private Integer fkRequestTypeId;

	@Column(name = "fk_purchase_po_id")
	private Integer fkPurchasePoId;

	@Column(name = "fk_purchase_capex_id")
	private Integer fkPurchaseCapexId;

	@Column(name = "project_id")
	private Integer projectId;
	

	@OneToOne
	@JoinColumn(name = "fk_bu_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasBuUnitBO masBuUnitBO;

	@OneToOne
	@JoinColumn(name = "fk_purchase_capex_id", unique = true, nullable = true, insertable = false, updatable = false)
	private PurchaseDatCapexBO purchaseDatCapexBO;

	@OneToOne
	@JoinColumn(name = "fk_purchase_po_id", unique = true, nullable = true, insertable = false, updatable = false)
	private PurchaseDatPoBO purchaseDatPoBO;

	@OneToOne
	@JoinColumn(name = "fk_asset_id", unique = true, nullable = true, insertable = false, updatable = false)
	private PurchaseMasAssetsBO purchaseMasAssetsBO;

	@OneToOne
	@JoinColumn(name = "requestor_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO;

	@OneToOne
	@JoinColumn(name = "purchase_request_status", unique = true, nullable = true, insertable = false, updatable = false)
	private PurchaseMasRequestStatusBO purchaseMasRequestStatusBO;

	@OneToOne
	@JoinColumn(name = "fk_request_type_id", unique = true, nullable = true, insertable = false, updatable = false)
	private PurchaseDatRequestTypeBO purchaseDatRequestTypeBO;

	@OneToOne
	@JoinColumn(name = "location_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasLocationParentBO masLocationParentBO;
	
	public PurchaseDatAssetRequestBO() {
	}

	public void setFkBuId(short fkBuId) {
		this.fkBuId = fkBuId;
	}
	public Integer getPkPurchaseRequestId() {
		return pkPurchaseRequestId;
	}

	public void setPkPurchaseRequestId(Integer pkPurchaseRequestId) {
		this.pkPurchaseRequestId = pkPurchaseRequestId;
	}

	public Date getAssetRequiredBy() {
		return assetRequiredBy;
	}

	public void setAssetRequiredBy(Date assetRequiredBy) {
		this.assetRequiredBy = assetRequiredBy;
	}

	public String getBriefDescription() {
		return briefDescription;
	}

	public void setBriefDescription(String briefDescription) {
		this.briefDescription = briefDescription;
	}

	public String getBuHeadApprovalNeeded() {
		return buHeadApprovalNeeded;
	}

	public void setBuHeadApprovalNeeded(String buHeadApprovalNeeded) {
		this.buHeadApprovalNeeded = buHeadApprovalNeeded;
	}

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public String getClosureComment() {
		return closureComment;
	}

	public void setClosureComment(String closureComment) {
		this.closureComment = closureComment;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;

	}

	public String getSupportingDocumentsBySysAdmin() {
		return supportingDocumentsBySysAdmin;
	}

	public void setSupportingDocumentsBySysAdmin(
			String supportingDocumentsBySysAdmin) {
		this.supportingDocumentsBySysAdmin = supportingDocumentsBySysAdmin;
	}

	
	public String getFinanceEx_comment() {
		return financeEx_comment;
	}

	public void setFinanceEx_comment(String financeEx_comment) {
		this.financeEx_comment = financeEx_comment;
	}

	public Integer getFinanceEx_updated_by() {
		return financeEx_updated_by;
	}

	public void setFinanceEx_updated_by(Integer financeEx_updated_by) {
		this.financeEx_updated_by = financeEx_updated_by;
	}

	public Date getFinanceEx_updated_on() {
		return financeEx_updated_on;
	}

	public void setFinanceEx_updated_on(Date financeEx_updated_on) {
		this.financeEx_updated_on = financeEx_updated_on;
	}

	public String getIsClientReimbursing() {
		return isClientReimbursing;
	}

	public void setIsClientReimbursing(String isClientReimbursing) {
		this.isClientReimbursing = isClientReimbursing;
	}

	public String getIsSoftwareOrHardwarePurchased() {
		return isSoftwareOrHardwarePurchased;

	}

	public void setIsSoftwareOrHardwarePurchased(
			String isSoftwareOrHardwarePurchased) {
		this.isSoftwareOrHardwarePurchased = isSoftwareOrHardwarePurchased;
	}

	public short getLocationId() {
		return locationId;
	}

	public void setLocationId(short locationId) {
		this.locationId = locationId;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Integer getNoOfUnits() {
		return noOfUnits;
	}

	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}

	public String getPoCreationRequired() {
		return poCreationRequired;
	}

	public void setPoCreationRequired(String poCreationRequired) {
		this.poCreationRequired = poCreationRequired;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public Integer getProjectComponentId() {
		return projectComponentId;
	}

	public void setProjectComponentId(Integer projectComponentId) {
		this.projectComponentId = projectComponentId;
	}

	public String getQuotesUploaded() {
		return quotesUploaded;
	}

	public void setQuotesUploaded(String quotesUploaded) {
		this.quotesUploaded = quotesUploaded;
	}

	public String getRmComment() {
		return rmComment;
	}

	public void setRmComment(String rmComment) {
		this.rmComment = rmComment;
	}

	public Integer getRmToApprove() {
		return rmToApprove;
	}

	public void setRmToApprove(Integer rmToApprove) {
		this.rmToApprove = rmToApprove;
	}

	public Date getRmUpdatedOn() {
		return rmUpdatedOn;
	}

	public void setRmUpdatedOn(Date rmUpdatedOn) {
		this.rmUpdatedOn = rmUpdatedOn;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	public String getSupportingDocumentsByClient() {
		return supportingDocumentsByClient;
	}

	public void setSupportingDocumentsByClient(
			String supportingDocumentsByClient) {
		this.supportingDocumentsByClient = supportingDocumentsByClient;
	}

	
	public String getSysAdminComment() {
		return sysAdminComment;
	}

	public void setSysAdminComment(String sysAdminComment) {
		this.sysAdminComment = sysAdminComment;
	}

	public Integer getSysAdminId() {
		return sysAdminId;
	}

	public void setSysAdminId(Integer sysAdminId) {
		this.sysAdminId = sysAdminId;
	}

	public Date getSysAdminUpdatedOn() {
		return sysAdminUpdatedOn;
	}

	public void setSysAdminUpdatedOn(Date sysAdminUpdatedOn) {
		this.sysAdminUpdatedOn = sysAdminUpdatedOn;
	}

	public Integer getFkAssetId() {
		return fkAssetId;
	}

	public void setFkAssetId(Integer fkAssetId) {
		this.fkAssetId = fkAssetId;
	}

	public Integer getPurchaseRequestStatus() {
		return purchaseRequestStatus;
	}

	public void setPurchaseRequestStatus(Integer purchaseRequestStatus) {
		this.purchaseRequestStatus = purchaseRequestStatus;
	}

	public Integer getFkRequestTypeId() {
		return fkRequestTypeId;
	}

	public void setFkRequestTypeId(Integer fkRequestTypeId) {
		this.fkRequestTypeId = fkRequestTypeId;

	}

	public Integer getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(Integer requestorId) {
		this.requestorId = requestorId;
	}

	public PurchaseDatCapexBO getPurchaseDatCapexBO() {
		return purchaseDatCapexBO;
	}

	public void setPurchaseDatCapexBO(PurchaseDatCapexBO purchaseDatCapexBO) {
		this.purchaseDatCapexBO = purchaseDatCapexBO;
	}

	public PurchaseDatPoBO getPurchaseDatPoBO() {
		return purchaseDatPoBO;
	}

	public void setPurchaseDatPoBO(PurchaseDatPoBO purchaseDatPoBO) {
		this.purchaseDatPoBO = purchaseDatPoBO;
	}

	public MasBuUnitBO getMasBuUnitBO() {
		return masBuUnitBO;
	}

	public void setMasBuUnitBO(MasBuUnitBO masBuUnitBO) {
		this.masBuUnitBO = masBuUnitBO;
	}

	public short getFkBuId() {
		return fkBuId;
	}

	public Integer getFkPurchasePoId() {
		return fkPurchasePoId;
	}

	public void setFkPurchasePoId(Integer fkPurchasePoId) {
		this.fkPurchasePoId = fkPurchasePoId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getFkPurchaseCapexId() {
		return fkPurchaseCapexId;
	}

	public void setFkPurchaseCapexId(Integer fkPurchaseCapexId) {
		this.fkPurchaseCapexId = fkPurchaseCapexId;
	}

	public PurchaseMasAssetsBO getPurchaseMasAssetsBO() {
		return purchaseMasAssetsBO;
	}

	public void setPurchaseMasAssetsBO(PurchaseMasAssetsBO purchaseMasAssetsBO) {
		this.purchaseMasAssetsBO = purchaseMasAssetsBO;
	}

	public DatEmpDetailBO getDatEmpDetailBO() {
		return datEmpDetailBO;
	}

	public void setDatEmpDetailBO(DatEmpDetailBO datEmpDetailBO) {
		this.datEmpDetailBO = datEmpDetailBO;
	}

	

	public PurchaseMasRequestStatusBO getPurchaseMasRequestStatusBO() {
		return purchaseMasRequestStatusBO;
	}

	public void setPurchaseMasRequestStatusBO(
			PurchaseMasRequestStatusBO purchaseMasRequestStatusBO) {
		this.purchaseMasRequestStatusBO = purchaseMasRequestStatusBO;
	}

	public PurchaseDatRequestTypeBO getPurchaseDatRequestTypeBO() {
		return purchaseDatRequestTypeBO;
	}

	public void setPurchaseDatRequestTypeBO(
			PurchaseDatRequestTypeBO purchaseDatRequestTypeBO) {
		this.purchaseDatRequestTypeBO = purchaseDatRequestTypeBO;
	}

	public MasLocationParentBO getMasLocationParentBO() {
		return masLocationParentBO;
	}

	public void setMasLocationParentBO(MasLocationParentBO masLocationParentBO) {
		this.masLocationParentBO = masLocationParentBO;
	}

	@Override
	public String toString() {
		return "PurchaseDatAssetRequestBO [pkPurchaseRequestId="
				+ pkPurchaseRequestId + ", briefDescription="
				+ briefDescription + ", createdDate=" + createdDate
				+ ", assetRequiredBy=" + assetRequiredBy + ", fkBuId=" + fkBuId
				+ ", duration=" + duration + ", isClientReimbursing="
				+ isClientReimbursing + ", isSoftwareOrHardwarePurchased="
				+ isSoftwareOrHardwarePurchased + ", modifiedBy=" + modifiedBy
				+ ", modifiedOn=" + modifiedOn + ", rmComment=" + rmComment
				+ ", rmToApprove=" + rmToApprove + ", rmUpdatedOn="
				+ rmUpdatedOn + ", supportingDocumentsByClient="
				+ supportingDocumentsByClient + ", sysAdminComment="
				+ sysAdminComment + ", sysAdminId=" + sysAdminId
				+ ", sysAdminUpdatedOn=" + sysAdminUpdatedOn
				+ ", quotesUploaded=" + quotesUploaded
				+ ", supportingDocumentsBySysAdmin="
				+ supportingDocumentsBySysAdmin + ", buHeadApprovalNeeded="
				+ buHeadApprovalNeeded + ", clientId=" + clientId
				+ ", poCreationRequired=" + poCreationRequired
				+ ", closureComment=" + closureComment + ", financeEx_comment="
				+ financeEx_comment + ", financeEx_updated_by="
				+ financeEx_updated_by + ", financeEx_updated_on="
				+ financeEx_updated_on + ", locationId=" + locationId
				+ ", noOfUnits=" + noOfUnits + ", priority=" + priority
				+ ", projectComponentId=" + projectComponentId
				+ ", requestorId=" + requestorId + ", specifications="
				+ specifications + ", fkAssetId=" + fkAssetId
				+ ", purchaseRequestStatus=" + purchaseRequestStatus
				+ ", fkRequestTypeId=" + fkRequestTypeId + ", fkPurchasePoId="
				+ fkPurchasePoId + ", fkPurchaseCapexId=" + fkPurchaseCapexId
				+ ", projectId=" + projectId + ", masBuUnitBO=" + masBuUnitBO
				+ ", purchaseDatCapexBO=" + purchaseDatCapexBO
				+ ", purchaseDatPoBO=" + purchaseDatPoBO
				+ ", purchaseMasAssetsBO=" + purchaseMasAssetsBO
				+ ", datEmpDetailBO=" + datEmpDetailBO
				+ ", purchaseMasRequestStatusBO=" + purchaseMasRequestStatusBO
				+ ", purchaseDatRequestTypeBO=" + purchaseDatRequestTypeBO
				+ ", masLocationParentBO=" + masLocationParentBO + "]";
	}
	

	

}
