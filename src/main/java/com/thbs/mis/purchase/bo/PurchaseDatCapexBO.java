package com.thbs.mis.purchase.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;

/**
 * The persistent class for the purchase_dat_capex database table.
 * 
 */
@Entity
@Table(name = "purchase_dat_capex")
@NamedQuery(name = "PurchaseDatCapexBO.findAll", query = "SELECT p FROM PurchaseDatCapexBO p")
public class PurchaseDatCapexBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_capexId")
	private Integer pkCapexId;

	@Column(name = "capex_no")
	private String capexNo;

	@Column(name = "capex_uploaded")
	private String capexUploaded;

	@Column(name = "created_by")
	private Integer createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "modified_by")
	private Integer modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_on")
	private Date modifiedOn;

	@OneToOne
	@JoinColumn(name = "created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailPreparedBy;

	@OneToOne
	@JoinColumn(name = "modified_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailModifiedBy;

	public PurchaseDatCapexBO() {
	}

	

	public Integer getPkCapexId() {
		return pkCapexId;
	}



	public void setPkCapexId(Integer pkCapexId) {
		this.pkCapexId = pkCapexId;
	}



	public String getCapexNo() {
		return this.capexNo;
	}

	public void setCapexNo(String capexNo) {
		this.capexNo = capexNo;
	}

	public String getCapexUploaded() {
		return this.capexUploaded;
	}

	public void setCapexUploaded(String capexUploaded) {
		this.capexUploaded = capexUploaded;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public DatEmpDetailBO getDatEmpDetailPreparedBy() {
		return datEmpDetailPreparedBy;
	}

	public void setDatEmpDetailPreparedBy(DatEmpDetailBO datEmpDetailPreparedBy) {
		this.datEmpDetailPreparedBy = datEmpDetailPreparedBy;
	}

	public DatEmpDetailBO getDatEmpDetailModifiedBy() {
		return datEmpDetailModifiedBy;
	}

	public void setDatEmpDetailModifiedBy(DatEmpDetailBO datEmpDetailModifiedBy) {
		this.datEmpDetailModifiedBy = datEmpDetailModifiedBy;
	}

	@Override
	public String toString() {
		return "PurchaseDatCapexBO [pk_capexId=" + pkCapexId + ", capexNo="
				+ capexNo + ", capexUploaded=" + capexUploaded + ", createdBy="
				+ createdBy + ", createdOn=" + createdOn + ", modifiedBy="
				+ modifiedBy + ", modifiedOn=" + modifiedOn + "]";
	}

}
