package com.thbs.mis.purchase.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasCurrencytypeBO;

/**
 * The persistent class for the purchase_dat_asset_quotes database table.
 * 
 */
@Entity
@Table(name = "purchase_dat_asset_quotes")
@NamedQuery(name = "PurchaseDatAssetQuoteBO.findAll", query = "SELECT p FROM PurchaseDatAssetQuoteBO p")
public class PurchaseDatAssetQuoteBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_quote_id")
	private Integer pkQuoteId;

	@Column(name = "bu_contribution")
	private Double buContribution;

	@Column(name = "cost_borne_by")
	private String costBorneBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "delivery_date")
	private Date deliveryDate;

	@Column(name = "individual_license_cost")
	private Double individualLicenseCost;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	@Column(name = "total_cost")
	private Double totalCost;
	
	@Column(name = "fk_purchase_request_id")
	private Integer fkPurchaseRequestId;
	
	@Column(name = "fk_supplier_address")
	private Integer fkSupplierAddress;
	
	@Column(name = "fk_quote_status")
	private String quoteStatus;

	@Column(name = "modified_by")
	private Integer modifiedBy;
	
	@Column(name = "created_by")
	private int createdBy;

	@Column(name = "vendor_quote_reference_no")
	private String vendorQuoteReferenceNo;
	
	@Column(name = "fk_currency_type")
	private Integer fkCurrencyType;
	
	@Column(name = "asset_description_entered_by_sysadmin")
	private String assetDescriptionEnteredBySysadmin;
	
	@OneToOne
	@JoinColumn(name = "fk_purchase_request_id", unique = true, nullable = true, insertable = false, updatable = false)
	private PurchaseDatAssetRequestBO purchaseDatAssetRequestBO;

	@OneToOne
	@JoinColumn(name = "fk_supplier_address", unique = true, nullable = true, insertable = false, updatable = false)
	private PurchaseDatSupplierDetailBO purchaseDatSupplierDetailBO;

	@OneToOne
	@JoinColumn(name = "created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailCreatedBy;

	@OneToOne
	@JoinColumn(name = "modified_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailModifiedBy;
	
	@OneToOne
	@JoinColumn(name = "fk_currency_type", unique = true, nullable = true, insertable = false, updatable = false)
	private MasCurrencytypeBO masCurrencyType;
	
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public PurchaseDatAssetQuoteBO() {
	}

	public Integer getPkQuoteId() {
		return pkQuoteId;
	}

	public void setPkQuoteId(Integer pkQuoteId) {
		this.pkQuoteId = pkQuoteId;
	}

	public Double getBuContribution() {
		return buContribution;
	}

	public void setBuContribution(Double buContribution) {
		this.buContribution = buContribution;
	}

	public String getCostBorneBy() {
		return costBorneBy;
	}

	public void setCostBorneBy(String costBorneBy) {
		this.costBorneBy = costBorneBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Double getIndividualLicenseCost() {
		return individualLicenseCost;
	}

	public void setIndividualLicenseCost(Double individualLicenseCost) {
		this.individualLicenseCost = individualLicenseCost;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}

	public PurchaseDatAssetRequestBO getPurchaseDatAssetRequestBO() {
		return purchaseDatAssetRequestBO;
	}

	public void setPurchaseDatAssetRequestBO(
			PurchaseDatAssetRequestBO purchaseDatAssetRequestBO) {
		this.purchaseDatAssetRequestBO = purchaseDatAssetRequestBO;
	}

	public PurchaseDatSupplierDetailBO getPurchaseDatSupplierDetailBO() {
		return purchaseDatSupplierDetailBO;
	}

	public void setPurchaseDatSupplierDetailBO(
			PurchaseDatSupplierDetailBO purchaseDatSupplierDetailBO) {
		this.purchaseDatSupplierDetailBO = purchaseDatSupplierDetailBO;
	}

	public DatEmpDetailBO getDatEmpDetailCreatedBy() {
		return datEmpDetailCreatedBy;
	}

	public void setDatEmpDetailCreatedBy(DatEmpDetailBO datEmpDetailCreatedBy) {
		this.datEmpDetailCreatedBy = datEmpDetailCreatedBy;
	}

	public DatEmpDetailBO getDatEmpDetailModifiedBy() {
		return datEmpDetailModifiedBy;
	}

	public void setDatEmpDetailModifiedBy(DatEmpDetailBO datEmpDetailModifiedBy) {
		this.datEmpDetailModifiedBy = datEmpDetailModifiedBy;
	}

	public Integer getFkPurchaseRequestId() {
		return fkPurchaseRequestId;
	}

	public void setFkPurchaseRequestId(Integer fkPurchaseRequestId) {
		this.fkPurchaseRequestId = fkPurchaseRequestId;
	}

	public Integer getFkSupplierAddress() {
		return fkSupplierAddress;
	}

	public void setFkSupplierAddress(Integer fkSupplierAddress) {
		this.fkSupplierAddress = fkSupplierAddress;
	}

	public String getQuoteStatus() {
		return quoteStatus;
	}

	public void setQuoteStatus(String quoteStatus) {
		this.quoteStatus = quoteStatus;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public String getVendorQuoteReferenceNo() {
		return vendorQuoteReferenceNo;
	}

	public void setVendorQuoteReferenceNo(String vendorQuoteReferenceNo) {
		this.vendorQuoteReferenceNo = vendorQuoteReferenceNo;
	}
	public Integer getFkCurrencyType() {
		return fkCurrencyType;
	}

	public void setFkCurrencyType(Integer fkCurrencyType) {
		this.fkCurrencyType = fkCurrencyType;
	}

	public MasCurrencytypeBO getMasCurrencyType() {
		return masCurrencyType;
	}

	public void setMasCurrencyType(MasCurrencytypeBO masCurrencyType) {
		this.masCurrencyType = masCurrencyType;
	}
	
	public String getAssetDescriptionEnteredBySysadmin() {
		return assetDescriptionEnteredBySysadmin;
	}

	public void setAssetDescriptionEnteredBySysadmin(
			String assetDescriptionEnteredBySysadmin) {
		this.assetDescriptionEnteredBySysadmin = assetDescriptionEnteredBySysadmin;
	}

	@Override
	public String toString() {
		return "PurchaseDatAssetQuoteBO [pkQuoteId=" + pkQuoteId
				+ ", buContribution=" + buContribution + ", costBorneBy="
				+ costBorneBy + ", createdDate=" + createdDate
				+ ", deliveryDate=" + deliveryDate + ", individualLicenseCost="
				+ individualLicenseCost + ", modifiedDate=" + modifiedDate
				+ ", totalCost=" + totalCost + ", fkPurchaseRequestId="
				+ fkPurchaseRequestId + ", fkSupplierAddress="
				+ fkSupplierAddress + ", quoteStatus=" + quoteStatus
				+ ", modifiedBy=" + modifiedBy + ", createdBy=" + createdBy
				+ ", vendorQuoteReferenceNo=" + vendorQuoteReferenceNo
				+ ", fkCurrencyType=" + fkCurrencyType
				+ ", assetDescriptionEnteredBySysadmin="
				+ assetDescriptionEnteredBySysadmin
				+ ", purchaseDatAssetRequestBO=" + purchaseDatAssetRequestBO
				+ ", purchaseDatSupplierDetailBO="
				+ purchaseDatSupplierDetailBO + ", datEmpDetailCreatedBy="
				+ datEmpDetailCreatedBy + ", datEmpDetailModifiedBy="
				+ datEmpDetailModifiedBy + ", masCurrencyType="
				+ masCurrencyType + "]";
	}
}