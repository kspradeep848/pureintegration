package com.thbs.mis.purchase.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.purchase.bean.ViewAllAssetRequestBean;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;

public class AssetRequestSpecification {

	public static Specification<PurchaseDatAssetRequestBO> getAllAssetRequests(ViewAllAssetRequestBean assetReq){
		
		
	
		
		
		return new Specification<PurchaseDatAssetRequestBO>() {
			
			@Override
			public Predicate toPredicate(Root<PurchaseDatAssetRequestBO> root,
					CriteriaQuery<?> cq, CriteriaBuilder cb) {
				
				Calendar calStartDate = Calendar.getInstance();
				Calendar calEndDate = Calendar.getInstance();
				
				final Collection<Predicate> predicates = new ArrayList<>();

				if (assetReq.getAssetId() != null) {
					final Predicate assetIdPredicate = cb.equal(
							root.get("fkAssetId"), assetReq.getAssetId());
					predicates.add(assetIdPredicate);
				}
				
				if (assetReq.getBuId() != null) {
				
						final Predicate buIdPredicate =cb.equal(
								root.get("fkBuId"), assetReq.getBuId());
						predicates.add(buIdPredicate);
					
				}
				
				if (assetReq.getStartDate() != null && assetReq.getEndDate() != null) {
					
					calStartDate.setTime(assetReq.getStartDate());
					calStartDate.set(Calendar.HOUR_OF_DAY,0);
					calStartDate.set(Calendar.MINUTE,0);
					calStartDate.set(Calendar.SECOND,0);
					calEndDate.setTime(assetReq.getEndDate());
					calEndDate.set(Calendar.HOUR_OF_DAY,23);
					calEndDate.set(Calendar.MINUTE,59);
					calEndDate.set(Calendar.SECOND,59);
				
				final Predicate dateRangePredicate =  cb.and(cb.greaterThanOrEqualTo(root.get("createdDate"),
						calStartDate.getTime()), cb.lessThanOrEqualTo(
						root.get("createdDate"), calEndDate.getTime()));
					
				
				//final Predicate dateRangePredicate = cb.between(root.<Date>get("createdDate"), assetReq.getStartDate(), assetReq.getEndDate());
				
				
					predicates.add(dateRangePredicate);
				
			}

				
				if (assetReq.getStatusId() != null) {
					final Predicate statusIdPredicate = cb.equal(
							root.get("purchaseRequestStatus"), assetReq.getStatusId());
					predicates.add(statusIdPredicate);
				}
				//query.orderBy(cb.desc(root.get("fkGsrSlabId")));
				//query.orderBy(cb.asc(root.get("fkGsrDatEmpId")));

				return cb.and(predicates.toArray(new Predicate[predicates
						.size()]));
				
				
			
			}
		};
		
	}
}
