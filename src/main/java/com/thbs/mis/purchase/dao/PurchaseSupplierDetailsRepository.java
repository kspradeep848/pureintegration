package com.thbs.mis.purchase.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.purchase.bo.PurchaseDatSupplierDetailBO;
import com.thbs.mis.purchase.bo.PurchaseMasAssetsBO;

@Repository
public interface PurchaseSupplierDetailsRepository extends GenericRepository<PurchaseDatSupplierDetailBO, Long> {

     public PurchaseDatSupplierDetailBO findByPkSupplierIdAndFkAssetId(Integer supplierId,Integer assetId);
     
     public PurchaseDatSupplierDetailBO findByPkSupplierId(Integer supplierId);
     
    // public List<PurchaseDatSupplierDetailBO> findByFkAssetId(Integer assetId);
     
     @Query(" SELECT a FROM PurchaseDatSupplierDetailBO a "
 			+ " WHERE a.fkAssetId = :fkAssetId "
 			+ " AND  a.isAvailable = 'YES' ")
 	public List<PurchaseDatSupplierDetailBO> getVendorsDetails(@Param("fkAssetId") Integer fkAssetId);

	
}
