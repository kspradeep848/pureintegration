package com.thbs.mis.purchase.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.purchase.bo.PurchaseDatAssetQuoteBO;

public interface PurchaseAssetQuoteRepository extends GenericRepository<PurchaseDatAssetQuoteBO, Integer>{

	public List<PurchaseDatAssetQuoteBO> findByFkPurchaseRequestId(Integer assetRequestId);
	
	
	//Added by Smrithi
	@Query( "SELECT q FROM PurchaseDatAssetQuoteBO q WHERE q.fkPurchaseRequestId = ? AND q.quoteStatus = 'AGREE' ")
	public PurchaseDatAssetQuoteBO getCapexDetailsByPurchaseRequestID(int fkPurchaseRequestId);
	//EOA by Smrithi
	
	
	@Query("SELECT q FROM PurchaseDatAssetQuoteBO q WHERE q.fkPurchaseRequestId = :requestId AND "
			+ "q.quoteStatus like 'AGREE' ")
	public PurchaseDatAssetQuoteBO getAgreeQuoteDetails(@Param("requestId") Integer requestId);
	
	//Added by Prathibha for update quote repository
    public PurchaseDatAssetQuoteBO findByPkQuoteId(Integer quoteId);  
   
	
    /******************************************************************/
 	
 	@Query("SELECT asstQoute FROM PurchaseDatAssetQuoteBO asstQoute WHERE "
 			+ "asstQoute.purchaseDatSupplierDetailBO.fkAssetId = :assetId")
 	public List<PurchaseDatAssetQuoteBO> getSupplierQuoteDetailsByAssetId(@Param("assetId") Integer assetId);
 	
 	public PurchaseDatAssetQuoteBO findByFkPurchaseRequestIdAndQuoteStatus(Integer assetReqId, String quoteStatus);
}
