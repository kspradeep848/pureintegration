package com.thbs.mis.purchase.dao;
import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.purchase.bo.PurchaseDatSupplierDetailBO;
//Added by Prathibha for Fetch Vendor details and thier quote details based on Asset Id

@Repository
public interface PurchaseDatSupplierDetailRepository extends GenericRepository<PurchaseDatSupplierDetailBO, Long> {

	
	public List<PurchaseDatSupplierDetailBO> findByFkAssetId(@Param("assetId") Integer assetId);
	
	public List<PurchaseDatSupplierDetailBO> findByFkAssetIdAndIsAvailable(@Param("assetId") Integer assetId,@Param("isAvailable") String isAvailable);
	
	public PurchaseDatSupplierDetailBO findByPkSupplierId(Integer suuplierId);
	
	/*@Query("SELECT p from PurchaseDatSupplierDetailBO p ")         
	public List<PurchaseDatSupplierDetailBO> getSupplierQuoteDetailsByAssetId(Integer assetId);*/

}
