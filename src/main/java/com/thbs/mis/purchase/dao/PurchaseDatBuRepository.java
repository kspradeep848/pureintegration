package com.thbs.mis.purchase.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.purchase.bo.PurchaseDatBuBO;

public interface PurchaseDatBuRepository extends GenericRepository<PurchaseDatBuBO, Integer>{

	@Query("SELECT b FROM PurchaseDatBuBO b JOIN PurchaseDatAssetQuoteBO q ON q.pkQuoteId = b.fkPurchaseQuoteId"
			+ " WHERE q.quoteStatus like 'AGREE' "
			+ " AND q.fkPurchaseRequestId = :assetRequestId ")
	public List<PurchaseDatBuBO> getAllPurchaseDatBuRecords(@Param("assetRequestId") Integer assetRequestId);
	

	//Added by Smrithi
	@Query("SELECT b FROM PurchaseDatBuBO b WHERE b.fkMasBuId in (:buList) "
			+ " AND b.createdDate BETWEEN :createdStartDate  AND :createdEndDate ")
	public List<PurchaseDatBuBO> getListOfPurchaseDatBU(@Param("buList") List<Short> buList,
			@Param("createdStartDate") Date createdStartDate,  @Param("createdEndDate") Date createdEndDate);
	//EOA by Smrithi

	
	
	@Query("SELECT b FROM PurchaseDatBuBO b JOIN PurchaseDatAssetQuoteBO q ON q.pkQuoteId = b.fkPurchaseQuoteId"
			+ " WHERE q.quoteStatus like 'AGREE' "
			+ " AND q.fkPurchaseRequestId = :assetRequestId ")
	public PurchaseDatBuBO getPurchaseDatBuObject(@Param("assetRequestId") Integer assetRequestId);
	
	public List<PurchaseDatBuBO> findByFkPurchaseQuoteId(Integer pkQuoteId);
	
	//Added by Kamal Anand for Notifications
	public PurchaseDatBuBO findByBuHeadIdAndFkPurchaseQuoteIdAndFkPurchaseDatBuStatus(short buHeadId,int assetQuoteId,String status);
	//End of Addition by Kamal Anand for Notifications
	
}
