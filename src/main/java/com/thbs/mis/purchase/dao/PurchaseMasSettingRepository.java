package com.thbs.mis.purchase.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.purchase.bo.PurchaseMasSettingBO;

@Repository
public interface PurchaseMasSettingRepository extends GenericRepository<PurchaseMasSettingBO, Integer>
{

	public PurchaseMasSettingBO findBySystemAdminId(Integer sysAdminID);
	
}
