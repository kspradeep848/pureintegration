package com.thbs.mis.purchase.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.purchase.bo.PurchaseMasAssetDetailsBO;

//Added by Prathibha 3610

@Repository
public interface PurchaseMasAssetDetailsRepository extends GenericRepository <PurchaseMasAssetDetailsBO,Long>

{
	
	/*public PurchaseMasAssetDetailsBO findBySoftwareLicenseOrSubscriptionId(@Param("subscriptionId")String subscriptionId);*/
	
	@Query("SELECT d FROM PurchaseMasAssetDetailsBO d WHERE d.purchaseMasAssetsBO.assetType like :assetType ")
	public List<PurchaseMasAssetDetailsBO> getAssetDetailsFromAssetType(@Param("assetType") String assetType);
	
	/*@Deprecated
	public PurchaseMasAssetDetailsBO findByFkMasAssetsId(@Param("assetId") Integer assestId);*/
	
	public List<PurchaseMasAssetDetailsBO> findByFkMasAssetsId(Integer assestId);
	

	//Get asset details based on Subcription Id
	
	@Query("SELECT asdet FROM PurchaseMasAssetDetailsBO asdet WHERE "
			+ "asdet.softwareLicenseOrSubscriptionId =:subscriptionId ")
	public  PurchaseMasAssetDetailsBO getAssetDetBySoftwareLicenseOrSubscriptionId(@Param("subscriptionId")String subscriptionId);
	
	
	public PurchaseMasAssetDetailsBO findByPkAssetDetailId(Integer assetDetailId);
	

	public List<PurchaseMasAssetDetailsBO> findByFkPurchaseRequestIdAndFklocationIdAndFkMasAssetsId(Integer requestId, Integer locationId, Integer assetId);
	

	//public PurchaseMasAssetDetailsBO findByFkMasAssetsIdAndSoftwareLicenseOrSubscriptionId(Integer assetId, String subscriptionId);
	public PurchaseMasAssetDetailsBO findByFkPurchaseRequestId(Integer assetDetailId);
	
	@Query("SELECT asdet FROM PurchaseMasAssetDetailsBO asdet WHERE "
			+ "asdet.softwareLicenseOrSubscriptionId =:subscriptionId  group by asdet.softwareLicenseOrSubscriptionId")
	public PurchaseMasAssetDetailsBO getAssetDetailsFromSoftwareLicenseOrSubscriptionId(@Param("subscriptionId")String subscriptionId);
	
	@Query("SELECT d FROM PurchaseMasAssetDetailsBO d WHERE d.fkMasAssetsId = :assetId "
			//+ "AND d.softwareLicenseOrSubscriptionId is not null "
			+ " GROUP BY d.softwareLicenseOrSubscriptionId ")
	public List<PurchaseMasAssetDetailsBO> getSoftwareOrLicenseIdFromAssetId(@Param("assetId") Integer assetId);
	
	
}
