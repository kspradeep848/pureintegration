package com.thbs.mis.purchase.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.purchase.bo.PurchaseDatCapexBO;

@Repository
public interface PurchaseCapexRepository extends GenericRepository<PurchaseDatCapexBO, Long>{

	public PurchaseDatCapexBO findTop1ByOrderByPkCapexIdDesc();
	
	public PurchaseDatCapexBO findByPkCapexId(Integer capexId);
}
