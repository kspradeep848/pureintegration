package com.thbs.mis.purchase.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.purchase.bo.PurchaseDatAssetIncrementBO;

public interface PurchaseAssetIncrementRepository extends GenericRepository<PurchaseDatAssetIncrementBO, Long>{


	@Query("Select increment From PurchaseDatAssetIncrementBO increment WHERE increment.assetId = :assetId AND increment.locationId = :locationId")
	public PurchaseDatAssetIncrementBO getLastAssetId(@Param(value = "assetId") Integer assetId,@Param(value = "locationId") Short locationId);
}
