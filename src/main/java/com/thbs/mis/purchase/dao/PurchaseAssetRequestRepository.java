package com.thbs.mis.purchase.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;

@Repository
public interface PurchaseAssetRequestRepository extends GenericRepository<PurchaseDatAssetRequestBO, Long>,  JpaSpecificationExecutor<PurchaseDatAssetRequestBO>{
	
	//Added by Smrithi
	
	public List<PurchaseDatAssetRequestBO> findByFkAssetId(Integer  fkAssetId);
	
	public PurchaseDatAssetRequestBO findByPkPurchaseRequestId(Integer pkPurchaseRequestId);
	
	@Query(" SELECT r FROM PurchaseDatAssetRequestBO r "
			+ " WHERE r.fkBuId = :fkBuId "
			+ " OR r.createdDate BETWEEN :startDate AND :endDate ")
	public List<PurchaseDatAssetRequestBO> getcapexRequestsBasedOnDateRangeAndBuId(@Param("fkBuId") Short fkBuId , 
			@Param("startDate") Date startDate, 
			@Param("endDate") Date endDate );
	

	/*@Query( "SELECT new com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO "
			+ " ( buname.buUnitName, "
			+ "capex.capexNo, "
			+ "asset.assetNameAndVersionOrModel, "
			+ "parentLoc.locationParentName, "
			+ "request.isClientReimbursing, "
			+ "request.briefDescription, "
			+ "quotes.individualLicenseCost, "
			+ "quotes.totalCost, "
			+ "quotes.costBorneBy, "
			+ "quotes.buContribution )"
			+ "from PurchaseDatAssetRequestBO request join MasBuUnitBO buname on request.fkBuId = buname.pkBuUnitId "
			+ "join PurchaseDatCapexBO capex on request.fkPurchaseCapexId = capex.pk_capexId "
			+ "join PurchaseMasAssetsBO asset on request.fkAssetId = asset.pkAssetId "
			+ "join MasLocationParentBO parentLoc on request.locationId = parentLoc.pkLocationParentId "
			+ "join PurchaseDatAssetQuoteBO quotes on request.pkPurchaseRequestId = quotes.fkPurchaseRequestId "
			+ "where request.pkPurchaseRequestId = ? ")
	public PurchaseDatAssetRequestBO getCapexDetailsByPurchaseRequestID( int pkPurchaseRequestId); */
	
	//EOA by Smrithi
	
	//Added by Shyam
	
	@Query("SELECT r FROM PurchaseDatAssetRequestBO r "
			+ " WHERE r.createdDate BETWEEN :fromDate AND :toDate "
			+ " AND r.requestorId = :requestorId ")
	public List<PurchaseDatAssetRequestBO> getAssetRequestDetailsByDateRangeAndRequestorId(@Param("fromDate") Date 
			fromDate, @Param("toDate") Date toDate, @Param("requestorId") Integer requestorId);
	
	

	@Query("SELECT r FROM PurchaseDatAssetRequestBO r "
			+ " WHERE r.createdDate BETWEEN :fromDate AND :toDate "
			+ " AND r.rmToApprove = :managerId ")
	public List<PurchaseDatAssetRequestBO> getAssetRequestDetailsByDateRangeAndReporteesIds(@Param("fromDate") Date fromDate , 
			@Param("toDate") Date toDate, @Param("managerId") Integer managerId);
	
	public PurchaseDatAssetRequestBO findByFkPurchaseCapexId(Integer capexId);
	
	//EOA by Shyam
	
	//Added by pratibha TR
	@Query("SELECT req "
            + " FROM PurchaseDatAssetRequestBO req "
            + " WHERE req.createdDate BETWEEN :fromDate AND :toDate ")
	public List<PurchaseDatAssetRequestBO> fetchOrderDetailsBasedOnDateAndStatus(@Param("fromDate") Date fromDate , 
			@Param("toDate") Date toDate);
	
	//EOA by pratibha TR
	
	//Added by Kamal Anand for Purchase Module Notifications
	public List<PurchaseDatAssetRequestBO> findByRmToApproveAndPurchaseRequestStatus(int rmId,int purchaseRequestStatus);
	
	public List<PurchaseDatAssetRequestBO> findByRequestorIdAndPurchaseRequestStatus(int requestorId,int purchaseRequestStatus);
	
	public List<PurchaseDatAssetRequestBO> findByPurchaseRequestStatus(int purchaseRequestStatus);
	
	public List<PurchaseDatAssetRequestBO> findByFkBuIdAndPurchaseRequestStatus(short buId,int purchaseRequestStatus);
	
	public List<PurchaseDatAssetRequestBO> findByPurchaseRequestStatusAndRmToApprove(int purchaseRequestStatus,int rmId);      
	//End of Addition by Kamal Anand for Purchase Module Notifications
	
	
}
