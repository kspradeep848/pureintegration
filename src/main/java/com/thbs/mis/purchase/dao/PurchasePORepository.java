package com.thbs.mis.purchase.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.purchase.bo.PurchaseDatPoBO;

@Repository
public interface PurchasePORepository extends GenericRepository<PurchaseDatPoBO, Integer>{

	
	public PurchaseDatPoBO findTop1ByOrderByPkPoIdDesc();
	
	public PurchaseDatPoBO findByPkPoId(Integer poId);
}
