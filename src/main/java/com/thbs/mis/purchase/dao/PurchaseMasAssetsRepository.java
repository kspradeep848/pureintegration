package com.thbs.mis.purchase.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.purchase.bo.PurchaseMasAssetsBO;

@Repository
public interface PurchaseMasAssetsRepository extends GenericRepository<PurchaseMasAssetsBO, Long>
{
  public PurchaseMasAssetsBO findByPkAssetId(Integer assetId);
  
  public List<PurchaseMasAssetsBO> findAll();
  
  public List<PurchaseMasAssetsBO> findByAssetType(String assetType);
  
  public PurchaseMasAssetsBO findTop1ByAssetTypeOrderByPkAssetId(String assetType);
  
  //Added by Smrithi
  @Query(" SELECT assets FROM PurchaseMasAssetsBO assets "
			+ " WHERE assets.assetType = :assetType "
			+ " AND assets.licenseType = :licenseType "
			+ " AND  assets.isAvailable = 'YES' ")
	public List<PurchaseMasAssetsBO> fetchAssetsBasedOnAssetTypeAndLicenseType(@Param("assetType") String assetType , @Param("licenseType") String licenseType );
  
  @Query(" SELECT assets FROM PurchaseMasAssetsBO assets "
			+ " WHERE assets.assetNameAndVersionOrModel = :assetNameAndVersionOrModel AND assets.isAvailable = 'YES' ")
 public PurchaseMasAssetsBO getAssetName(@Param("assetNameAndVersionOrModel") String assetNameAndVersionOrModel);
  
  //EOA by Smrithi
	
  
 // public PurchaseMasAssetsBO findTop1ByAssetTypeAndAssetNameAndVersionOrModelLikeOrderByPkAssetIdDesc(String assetType, String pattren);

 /* @Query("select top 1 masAsset from PurchaseMasAssetsBO masAsset where masAsset.assetType= ?1 and"
  		+ " masAsset.assetNameAndVersionOrModel like '%?2%' order by masAsset.pkAssetId Desc")
  public PurchaseMasAssetsBO findLastReqAssetId(String assetType, String pattren);
*/
}
