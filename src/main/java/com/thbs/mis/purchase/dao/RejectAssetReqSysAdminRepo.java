package com.thbs.mis.purchase.dao;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;

public interface RejectAssetReqSysAdminRepo extends GenericRepository<PurchaseDatAssetRequestBO, Long>
{

	public PurchaseDatAssetRequestBO findByPkPurchaseRequestId(Integer pkPurchaseRequestId);
	
}
