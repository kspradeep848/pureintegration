package com.thbs.mis.notificationframework.expensenotification.service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.expense.bo.ExpDatReportBO;
import com.thbs.mis.expense.dao.ExpDatReportRepository;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.notificationframework.expensenotification.constants.ExpenseNotificationConstants;
import com.thbs.mis.notificationframework.gsrnotification.service.GSRNotificationService;
import com.thbs.mis.notificationframework.notificationsettings.bo.DatNotificationsBO;
import com.thbs.mis.notificationframework.notificationsettings.dao.NotificationRepository;

@Service
public class ExpenseNotificationService {

	/*
	 * Individual Notification Messages from ClientMessages.properties file
	 */

	@Value("${ui.message.awaitingApprovalToApproverScreen}")
	String awaitingApprovalToApproverScreenIndividualMsg;

	@Value("${ui.message.recallAlertToEmpScreen}")
	String recallAlertToEmpScreenIndividualMsg;

	@Value("${ui.message.rejectAlertFromRmToEmpScreen}")
	String rejectAlertFromRmToEmpScreenIndividualMsg;

	@Value("${ui.message.approvedAlertFromRmToEmpScreen}")
	String approvedAlertFromRmToEmpScreenIndividualMsg;

	@Value("${ui.message.approvedAlertFromRmToReimbursorScreen}")
	String approvedAlertFromRmToReimbursorScreenIndividualMsg;

	@Value("${ui.message.partiallyApprovedAlertFromRmToEmpScreen}")
	String partiallyApprovedAlertFromRmToEmpScreenIndividualMsg;

	@Value("${ui.message.partiallyApprovedAlertFromRmToReimbursorScreen}")
	String partiallyApprovedAlertFromRmToReimbursorScreenIndividualMsg;

	@Value("${ui.message.rejectedAlertFromRmToReimbursorScreen}")
	String rejectedAlertFromRmToReimbursorScreenIndividualMsg;
		
	@Value("${ui.message.forwardAlertFromRmToEmpScreen}")
	String forwardAlertFromRmToEmpScreenIndividualMsg;

	@Value("${ui.message.forwardAlertFromRmToSkipLevelApproverScreen}")
	String forwardAlertFromRmToSkipLevelApproverScreenIndividualMsg;

	@Value("${ui.message.partialForwardAlertFromRmToEmpScreen}")
	String partialForwardAlertFromRmToEmpScreenIndividualMsg;

	@Value("${ui.message.partialForwardAlertFromRmToSkipLevelApproverScreen}")
	String partialForwardAlertFromRmToSkipLevelApproverScreenIndividualMsg;

	@Value("${ui.message.reimbursementAlertFromReimbursorToEmpScreen}")
	String reimbursementAlertFromReimbursorToEmpScreenIndividualMsg;

	@Value("${ui.message.reimbursementAlertFromReimbursorToRmScreen}")
	String reimbursementAlertFromReimbursorToRmScreenIndividualMsg;

	@Value("${ui.message.partialReimbursementAlertFromReimbursorToEmpScreen}")
	String partialReimbursementAlertFromReimbursorToEmpScreenIndividualMsg;

	@Value("${ui.message.partialReimbursementAlertFromReimbursorToRmScreen}")
	String partialReimbursementAlertFromReimbursorToRmScreenIndividualMsg;

	@Value("${ui.message.rejectedReimbursementAlertFromReimbursorToEmpScreen}")
	String rejectedReimbursementAlertFromReimbursorToEmpScreenIndividualMsg;

	@Value("${ui.message.rejectedReimbursementAlertFromReimbursorToRmScreen}")
	String rejectedReimbursementAlertFromReimbursorToRmScreenIndividualMsg;

	@Value("${ui.message.cancelledReimbursementAlertFromReimbursorToEmpScreen}")
	String cancelledReimbursementAlertFromReimbursorToEmpScreenIndividualMsg;

	@Value("${ui.message.cancelledReimbursementAlertFromReimbursorToRmScreen}")
	String cancelledReimbursementAlertFromReimbursorToRmScreenIndividualMsg;

	@Value("${ui.message.reimbursedAlertFromReimbursorToEmpScreen}")
	String reimbursedAlertFromReimbursorToEmpScreenIndividualMsg;

	@Value("${ui.message.reimbursedAlertFromReimbursorToRmScreen}")
	String reimbursedAlertFromReimbursorToRmScreenIndividualMsg;

	@Value("${ui.message.recallToApproverScreen}")
	String recallToApproverScreenIndividualMsg;
	
	@Value("${ui.message.empRejectedToApproverScreen}")
	String empRejectedToApproverScreenIndividualMsg;
	
	@Value("${ui.message.empRejectedToEmpScreen}")
	String empRejectedToEmpScreenIndividualMsg;

	/*
	 * Consolidated Notification Messages from ClientMessages.properties file
	 */

	@Value("${ui.message.awaitingApprovalToApproverConsolidated}")
	String awaitingApprovalToApproverConsolidatedMsg;

	@Value("${ui.message.recallAlertToEmpConsolidated}")
	String recallAlertToEmpConsolidatedMsg;

	@Value("${ui.message.recallToApproverConsolidated}")
	String recallToApproverConsolidatedMsg;

	@Value("${ui.message.approvedAlertFromRmToEmpConsolidated}")
	String approvedAlertFromRmToEmpConsolidatedMsg;

	@Value("${ui.message.approvedAlertFromRmToReimbursorConsolidated}")
	String approvedAlertFromRmToReimbursorConsolidatedMsg;

	@Value("${ui.message.partiallyApprovedAlertFromRmToReimbursorConsolidated}")
	String partiallyApprovedAlertFromRmToReimbursorConsolidatedMsg;
	
	@Value("${ui.message.rejectedAlertFromRmToReimbursorConsolidated}")
	String rejectedAlertFromRmToReimbursorConsolidatedMsg;

	@Value("${ui.message.partiallyApprovedAlertFromRmToEmpConsolidated}")
	String partiallyApprovedAlertFromRmToEmpConsolidatedMsg;

	@Value("${ui.message.rejectAlertFromRmToEmpConsolidated}")
	String rejectAlertFromRmToEmpConsolidatedMsg;

	@Value("${ui.message.forwardAlertFromRmToEmpConsolidated}")
	String forwardAlertFromRmToEmpConsolidatedMsg;

	@Value("${ui.message.forwardAlertFromRmToSkipLevelApproverConsolidated}")
	String forwardAlertFromRmToSkipLevelApproverConsolidatedMsg;

	@Value("${ui.message.partialForwardAlertFromRmToEmpConsolidated}")
	String partialForwardAlertFromRmToEmpConsolidatedMsg;

	@Value("${ui.message.partialForwardAlertFromRmToSkipLevelApproverConsolidated}")
	String partialForwardAlertFromRmToSkipLevelApproverConsolidatedMsg;

	@Value("${ui.message.reimbursementAlertFromReimbursorToEmpConsolidated}")
	String reimbursementAlertFromReimbursorToEmpConsolidatedMsg;

	@Value("${ui.message.reimbursementAlertFromReimbursorToRmConsolidated}")
	String reimbursementAlertFromReimbursorToRmConsolidatedMsg;

	@Value("${ui.message.partialReimbursementAlertFromReimbursorToEmpConsolidated}")
	String partialReimbursementAlertFromReimbursorToEmpConsolidatedMsg;

	@Value("${ui.message.partialReimbursementAlertFromReimbursorToRmConsolidated}")
	String partialReimbursementAlertFromReimbursorToRmConsolidatedMsg;

	@Value("${ui.message.rejectedReimbursementAlertFromReimbursorToEmpConsolidated}")
	String rejectedReimbursementAlertFromReimbursorToEmpConsolidatedMsg;

	@Value("${ui.message.rejectedReimbursementAlertFromReimbursorToRmConsolidated}")
	String rejectedReimbursementAlertFromReimbursorToRmConsolidatedMsg;

	@Value("${ui.message.cancelledReimbursementAlertFromReimbursorToEmpConsolidated}")
	String cancelledReimbursementAlertFromReimbursorToEmpConsolidatedMsg;

	@Value("${ui.message.cancelledReimbursementAlertFromReimbursorToRmConsolidated}")
	String cancelledReimbursementAlertFromReimbursorToRmConsolidatedMsg;

	@Value("${ui.message.reimbursedAlertFromReimbursorToEmpConsolidated}")
	String reimbursedAlertFromReimbursorToEmpConsolidatedMsg;

	@Value("${ui.message.reimbursedAlertFromReimbursorToRmConsolidated}")
	String reimbursedAlertFromReimbursorToRmConsolidatedMsg;
	
	@Value("${ui.message.empRejectedToApproverScreenConsolidated}")
	String empRejectedToApproverScreenConsolidatedMsg;
	
	@Value("${ui.message.empRejectedToEmpConsolidated}")
	String empRejectedToEmpConsolidatedMsg;
	
	/*
	 * Autowiring Repositories
	 */

	@Autowired
	private ExpDatReportRepository expDatReportRepository;

	@Autowired
	GSRNotificationService gsrNotificationService;
	
	@Autowired
	private NotificationRepository notificationRepository;

	@Autowired
	DatEmployeeProfessionalRepository datEmployeeProfessionalRepository;
	/*
	 * Expense Notification Keys from notificationframework.properties file
	 */

	@Value("${EXP_EMP_AWAITING_RM}")
	String expenseReportAwaitAlertRmKeys;

	@Value("${EXP_RECALL_EMP}")
	String expenseReportRecallAlertEmpKeys;

	@Value("${EXP_RM_APPROVED_EMP}")
	String expenseReportApprovedAlertEmpKeys;

	@Value("${EXP_RM_APPROVED_FIN}")
	String expenseReportApprovedAlertFinKeys;

	@Value("${EXP_RM_PARTIAL_APPROVED_FIN}")
	String expenseReportPartialApprovedAlertEmpKeys;

	@Value("${EXP_RM_PARTIAL_APPROVED_FIN}")
	String expenseReportPartialApprovedAlertFinKeys;

	@Value("${EXP_RM_REJECTED_EMP}")
	String expenseReportRejectedAlertEmpKeys;

	@Value("${EXP_RM_FORWARD_EMP}")
	String expenseReportForwardAlertEmpKeys;

	@Value("${EXP_RM_FORWARD_SKIP_RM}")
	String expenseReportForwardAlertSkipLevelKeys;

	@Value("${EXP_RM_PARTIAL_FORWARD_EMP}")
	String expenseReportPartialForwardAlertEmpKeys;

	@Value("${EXP_RM_PARTIAL_FORWARD_EMP_SKIP_RM}")
	String expenseReportPartialForwardAlertSkipLevelKeys;

	@Value("${EXP_FIN_REIMBURSE_INIT_EMP}")
	String expenseReportReimbursmentInitiatedToEmpKeys;

	@Value("${EXP_FIN_REIMBURSE_INIT_RM}")
	String expenseReportReimbursmentInitiatedToRmKeys;

	@Value("${EXP_FIN_REIMBURSE_PARTIAL_INIT_EMP}")
	String expenseReportPartialyReimbursmentToEmpKeys;

	@Value("${EXP_FIN_REIMBURSE_PARTIAL_INIT_RM}")
	String expenseReportPartialyReimbursmentToRmKeys;

	@Value("${EXP_FIN_REIMBURSE_REJECTED_EMP}")
	String expenseReportRejectedReimbursmentToEmpKeys;

	@Value("${EXP_FIN_REIMBURSE_REJECTED_RM}")
	String expenseReportRejectedReimbursmentToRmKeys;

	@Value("${EXP_FIN_REIMBURSE_CANCELLED_EMP}")
	String expenseReportCancelledReimbursmentToEmpKeys;

	@Value("${EXP_FIN_REIMBURSE_CANCELLED_RM}")
	String expenseReportCancelledReimbursmentToRmKeys;

	@Value("${EXP_FIN_REIMBURSE_EMP}")
	String expenseReportReimbursedToEmpKeys;

	@Value("${EXP_FIN_REIMBURSE_RM}")
	String expenseReportReimbursedToRmKeys;

	@Value("${EXP_EMP_RECALL_RM}")
	String expenseReportRecallAlertRmKeys;

	@Value("${EXP_EMP_REJECTED_RM}")
	String expenseEmpRejectrdAlertRmKeys;
	
	@Value("${EXP_EMP_REJECTED_EMP}")
	String expenseEmpRejectrdAlertEmpKeys;
	
	@Value("${EXP_RM_REJECTED_FIN}")
	String expenseReportRejectedAlertFinKeys;
	
	@Value("${expense.menu.id}")
	private String expenseMenuId;

	/**
	 * 
	 * <Description setAwaitingApprovalToRm:> This method is used to awaiting
	 * approval alert message to RM
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setAwaitingApprovalToRm(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportAwaitAlertRmKey = expenseReportAwaitAlertRmKeys;
		String msgIdentifierKey = expenseReportAwaitAlertRmKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByApproverMgrAndFkStatusId(empId,
					ExpenseNotificationConstants.AWAITING_APPROVAL);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(awaitingApprovalToApproverScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()),
								String.valueOf(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()).replaceAll(",", ""),
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportAwaitAlertRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportAwaitAlertRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportAwaitAlertRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(awaitingApprovalToApproverScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()),
								String.valueOf(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()).replaceAll(",", ""),
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						isDuplicatesFound.setNoificationMessage(notificationMessage);
						isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(awaitingApprovalToApproverConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportAwaitAlertRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportAwaitAlertRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportAwaitAlertRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(awaitingApprovalToApproverConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						isDuplicatesFound.setNoificationMessage(notificationMessage);
						isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				}
			} else {
				String menuURL = expenseReportAwaitAlertRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportAwaitAlertRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}
	}

	/**
	 * 
	 * <Description setReportRecallToEmpScreen:> This method is used to report
	 * recall alert message to Emp
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportRecallToEmpScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportRecallAlertEmpKey = expenseReportRecallAlertEmpKeys;
		String msgIdentifierKey = expenseReportRecallAlertEmpKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkEmpIdIntAndFkStatusId(empId,
					ExpenseNotificationConstants.RECALLED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(recallAlertToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportRecallAlertEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportRecallAlertEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportRecallAlertEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(recallAlertToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						isDuplicatesFound.setNoificationMessage(notificationMessage);
						isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(recallAlertToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportRecallAlertEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportRecallAlertEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportRecallAlertEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(recallAlertToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						isDuplicatesFound.setNoificationMessage(notificationMessage);
						isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				}

			} else {
				String menuURL = expenseReportRecallAlertEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportRecallAlertEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}
	}

	/**
	 * 
	 * <Description setReportRecallToRmScreen:> This method is used to report
	 * recall alert message to RM
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportRecallToRmScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportRecallAlertRmKey = expenseReportRecallAlertRmKeys;
		String msgIdentifierKey = expenseReportRecallAlertRmKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByApproverMgrAndFkStatusId(empId,
					ExpenseNotificationConstants.RECALLED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(recallToApproverScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()),
								String.valueOf(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()).replaceAll(",", ""),
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportRecallAlertRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportRecallAlertRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportRecallAlertRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(recallToApproverScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()),
										String.valueOf(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()).replaceAll(",", ""),
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						isDuplicatesFound.setNoificationMessage(notificationMessage);
						isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(recallToApproverConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportRecallAlertRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportRecallAlertRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportRecallAlertRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(recallToApproverConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						isDuplicatesFound.setNoificationMessage(notificationMessage);
						isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				}
			} else {
				String menuURL = expenseReportRecallAlertRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportRecallAlertRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}
	}

	/**
	 * 
	 * <Description setReportApprovedAlertToEmpScreen:> This method is used to
	 * report approved alert message to Emp
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportApprovedAlertToEmpScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportApprovedAlertEmpKey = expenseReportApprovedAlertEmpKeys;
		String msgIdentifierKey = expenseReportApprovedAlertEmpKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkEmpIdIntAndFkStatusId(empId,
					ExpenseNotificationConstants.APPROVED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(approvedAlertFromRmToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportApprovedAlertEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportApprovedAlertEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportApprovedAlertEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(approvedAlertFromRmToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						isDuplicatesFound.setNoificationMessage(notificationMessage);
						isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(approvedAlertFromRmToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportApprovedAlertEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportApprovedAlertEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportApprovedAlertEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(approvedAlertFromRmToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						isDuplicatesFound.setNoificationMessage(notificationMessage);
						isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportApprovedAlertEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportApprovedAlertEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}

	}

	/**
	 * 
	 * <Description setReportApprovedAlertToReimbursorScreen:> This method is
	 * used to report approved alert message to Reimbusor
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportApprovedAlertToReimbursorScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportApprovedAlertFinKey = expenseReportApprovedAlertFinKeys;
		String msgIdentifierKey = expenseReportApprovedAlertFinKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			DatEmpProfessionalDetailBO datEmpProfessionalDetailBO=datEmployeeProfessionalRepository.findByFkMainEmpDetailId(empId);
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkFinReimburserAndFkStatusId(datEmpProfessionalDetailBO.getFkEmpOrganizationId(),
					ExpenseNotificationConstants.APPROVED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String approverName="";
						if(lstExpReportDataExpenseBO.get(0).getFkForwardRmId()==null)
							approverName=gsrNotificationService
							.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverMgr());
						else
							approverName=gsrNotificationService
							.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkForwardRmId());
						String notificationMessage = MessageFormat.format(
								approvedAlertFromRmToReimbursorScreenIndividualMsg,
								approverName,
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()));
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportApprovedAlertFinKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportApprovedAlertFinKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportApprovedAlertFinKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String approverName="";
						if(lstExpReportDataExpenseBO.get(0).getFkForwardRmId()==null)
						approverName=gsrNotificationService
								.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverMgr());
						else
							approverName=gsrNotificationService
							.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkForwardRmId());
						String notificationMessage = MessageFormat.format(
								approvedAlertFromRmToReimbursorScreenIndividualMsg,
								approverName,
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()));
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								approvedAlertFromRmToReimbursorConsolidatedMsg, lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportApprovedAlertFinKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportApprovedAlertFinKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportApprovedAlertFinKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								approvedAlertFromRmToReimbursorConsolidatedMsg, lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportApprovedAlertFinKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportApprovedAlertFinKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}

	}

	/**
	 * 
	 * <Description setReportPartiallyApprovedAlertToReimbursorScreen:> This
	 * method is used to report partial approved alert message to Reimbusor
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportPartiallyApprovedAlertToReimbursorScreen(int empId) throws DataAccessException {

		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportPartialApprovedAlertFinKey = expenseReportPartialApprovedAlertFinKeys;
		String msgIdentifierKey = expenseReportPartialApprovedAlertFinKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			DatEmpProfessionalDetailBO datEmpProfessionalDetailBO=datEmployeeProfessionalRepository.findByFkMainEmpDetailId(empId);
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkFinReimburserAndFkStatusId(datEmpProfessionalDetailBO.getFkEmpOrganizationId(),
					ExpenseNotificationConstants.PARTIAL_APPROVED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String approverName="";
						if(lstExpReportDataExpenseBO.get(0).getFkForwardRmId()==null)
						approverName=gsrNotificationService
								.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverMgr());
						else
							approverName=gsrNotificationService
							.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkForwardRmId());
						String notificationMessage = MessageFormat.format(
								partiallyApprovedAlertFromRmToReimbursorScreenIndividualMsg,
								approverName,
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()));
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportPartialApprovedAlertFinKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportPartialApprovedAlertFinKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportPartialApprovedAlertFinKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String approverName="";
						if(lstExpReportDataExpenseBO.get(0).getFkForwardRmId()==null)
						approverName=gsrNotificationService
								.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverMgr());
						else
							approverName=gsrNotificationService
							.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkForwardRmId());
						String notificationMessage = MessageFormat.format(
								partiallyApprovedAlertFromRmToReimbursorScreenIndividualMsg,
								approverName,
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()));
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								partiallyApprovedAlertFromRmToReimbursorConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportPartialApprovedAlertFinKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportPartialApprovedAlertFinKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportPartialApprovedAlertFinKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								partiallyApprovedAlertFromRmToReimbursorConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				}
			} else {
				String menuURL = expenseReportPartialApprovedAlertFinKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportPartialApprovedAlertFinKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}

	}

	/**
	 * 
	 * <Description setReportPartiallyApprovedAlertToEmpScreen:> This method is
	 * used to report partial approved alert message to emp
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportPartiallyApprovedAlertToEmpScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportPartialApprovedAlertEmpKey = expenseReportPartialApprovedAlertEmpKeys;
		String msgIdentifierKey = expenseReportPartialApprovedAlertEmpKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkEmpIdIntAndFkStatusId(empId,
					ExpenseNotificationConstants.PARTIAL_APPROVED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(
								partiallyApprovedAlertFromRmToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportPartialApprovedAlertEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportPartialApprovedAlertEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportPartialApprovedAlertEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								partiallyApprovedAlertFromRmToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								partiallyApprovedAlertFromRmToEmpConsolidatedMsg, lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportPartialApprovedAlertEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportPartialApprovedAlertEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportPartialApprovedAlertEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								partiallyApprovedAlertFromRmToEmpConsolidatedMsg, lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportPartialApprovedAlertEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportPartialApprovedAlertEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}

	}

	/**
	 * 
	 * <Description setReportRejectedAlertToEmpScreen:> This method is used to
	 * report reject alert message to emp
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportRejectedAlertToEmpScreen(int empId) throws DataAccessException {

		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportRejectedAlertEmpKey = expenseReportRejectedAlertEmpKeys;
		String msgIdentifierKey = expenseReportRejectedAlertEmpKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkEmpIdIntAndFkStatusId(empId,
					ExpenseNotificationConstants.REJECTED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(rejectAlertFromRmToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportRejectedAlertEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportRejectedAlertEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportRejectedAlertEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(rejectAlertFromRmToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
/*						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(rejectAlertFromRmToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportRejectedAlertEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportRejectedAlertEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportRejectedAlertEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(rejectAlertFromRmToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportRejectedAlertEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportRejectedAlertEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}

	}

	/**
	 * 
	 * <Description setReportForwardAlertToEmpScreen:> This method is used to
	 * report forward alert message to emp
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportForwardAlertToEmpScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		List<Integer> skipLevelRmIds=null;
		String expenseReportForwardAlertEmpKey = expenseReportForwardAlertEmpKeys;
		String msgIdentifierKey = expenseReportForwardAlertEmpKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			skipLevelRmIds=expDatReportRepository.groupBySkipLevelManager(empId,ExpenseNotificationConstants.FORWARDED);
			for (Integer skipLevelRmId : skipLevelRmIds) {
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkEmpIdIntAndFkStatusIdAndFkForwardRmId(empId,
					ExpenseNotificationConstants.FORWARDED,skipLevelRmId);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId + "_" + skipLevelRmId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(forwardAlertFromRmToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle(),gsrNotificationService.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkForwardRmId()));
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportForwardAlertEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportForwardAlertEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportForwardAlertEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(forwardAlertFromRmToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle(),gsrNotificationService.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkForwardRmId()));
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						/*isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId + "_" + skipLevelRmId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(forwardAlertFromRmToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size(),gsrNotificationService.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkForwardRmId()));
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportForwardAlertEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportForwardAlertEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportForwardAlertEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(forwardAlertFromRmToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size(),gsrNotificationService.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkForwardRmId()));
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportForwardAlertEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportForwardAlertEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} 
		}	catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}

	}

	/**
	 * 
	 * <Description setReportForwardAlertToSkipLevelScreen:> This method is used
	 * to report forward alert message to skip level Rm
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportForwardAlertToSkipLevelScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportForwardAlertSkipLevelKey = expenseReportForwardAlertSkipLevelKeys;
		String msgIdentifierKey = expenseReportForwardAlertSkipLevelKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkForwardRmIdAndFkStatusId(empId,
					ExpenseNotificationConstants.FORWARDED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(
								forwardAlertFromRmToSkipLevelApproverScreenIndividualMsg,
								gsrNotificationService.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverMgr()),
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()));
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportForwardAlertSkipLevelKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportForwardAlertSkipLevelKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportForwardAlertSkipLevelKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								forwardAlertFromRmToSkipLevelApproverScreenIndividualMsg,
								gsrNotificationService.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverMgr()),
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()));
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								forwardAlertFromRmToSkipLevelApproverConsolidatedMsg, lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportForwardAlertSkipLevelKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportForwardAlertSkipLevelKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportForwardAlertSkipLevelKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								forwardAlertFromRmToSkipLevelApproverConsolidatedMsg, lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportForwardAlertSkipLevelKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportForwardAlertSkipLevelKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}
	}

	/**
	 * 
	 * <Description setReportPartialForwardAlertToEmpScreen:> This method is
	 * used to report partial forward alert message to emp
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportPartialForwardAlertToEmpScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportPartialForwardAlertEmpKey = expenseReportPartialForwardAlertEmpKeys;
		String msgIdentifierKey = expenseReportPartialForwardAlertEmpKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		List<Integer> skipLeveleRmIds = null;
		int updateCount = 0;
		try {
			skipLeveleRmIds = expDatReportRepository.groupBySkipLevelManager(empId,
					ExpenseNotificationConstants.PARTIAL_FORWARDED);
			for (Integer skipLeveleRmId : skipLeveleRmIds) {
				lstExpReportDataExpenseBO = expDatReportRepository.findByFkEmpIdIntAndFkStatusIdAndFkForwardRmId(empId,
						 ExpenseNotificationConstants.PARTIAL_FORWARDED,skipLeveleRmId);
				if (lstExpReportDataExpenseBO.size() > 0) {
					if (lstExpReportDataExpenseBO.size() == 1) {
						String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId + "_" + skipLeveleRmId;
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
						isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
						if (isDuplicatesFound == null) {
							String notificationMessage = MessageFormat.format(
									partialForwardAlertFromRmToEmpScreenIndividualMsg,
									lstExpReportDataExpenseBO.get(0).getReportTitle(),gsrNotificationService.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkForwardRmId()));
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							notificationDetails.setEmpId(empId);
							notificationDetails.setNoificationMessage(notificationMessage);
							notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails.setNotificationcreatedDate(new Date());
							notificationDetails.setClearFlagModifiedDate(new Date());
							notificationDetails.setClearFlagcreatedDate(new Date());
							notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
							notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
							notificationDetails.setMenuUrl(expenseReportPartialForwardAlertEmpKey.split(",")[0]);
							notificationDetails.setIsAction(expenseReportPartialForwardAlertEmpKey.split(",")[1]);
							notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							notificationDetails.setNotificationExpiryTime(
									Integer.parseInt(expenseReportPartialForwardAlertEmpKey.split(",")[3]));
							try {
								notificationRepository.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException("Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat.format(
									partialForwardAlertFromRmToEmpScreenIndividualMsg,
									lstExpReportDataExpenseBO.get(0).getReportTitle(),gsrNotificationService.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkForwardRmId()));
							if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound.setNoificationMessage(notificationMessage);
								isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
								isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound.setNotificationModifiedDate(new Date());
							/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO
									.get(0).getSubmissionDate().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
							else
								isDuplicatesFound.setNotificationcreatedDate(
										lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
							isDuplicatesFound.setNotificationcreatedDate(new Date());
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException("Failed to Save : ", e);
							}
						}
					} else {
						String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId + "_" + skipLeveleRmId;
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
						isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
						if (isDuplicatesFound == null) {
							/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
									.max(Date::compareTo).get();*/
							String notificationMessage = MessageFormat.format(
									partialForwardAlertFromRmToEmpConsolidatedMsg, lstExpReportDataExpenseBO.size(),gsrNotificationService.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkForwardRmId()));
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							notificationDetails.setEmpId(empId);
							notificationDetails.setNoificationMessage(notificationMessage);
							notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails.setNotificationcreatedDate(new Date());
							notificationDetails.setClearFlagModifiedDate(new Date());
							notificationDetails.setClearFlagcreatedDate(new Date());
							notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
							notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
							notificationDetails.setMenuUrl(expenseReportPartialForwardAlertEmpKey.split(",")[0]);
							notificationDetails.setIsAction(expenseReportPartialForwardAlertEmpKey.split(",")[1]);
							notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							notificationDetails.setNotificationExpiryTime(
									Integer.parseInt(expenseReportPartialForwardAlertEmpKey.split(",")[3]));
							try {
								notificationRepository.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException("Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat.format(
									partialForwardAlertFromRmToEmpConsolidatedMsg, lstExpReportDataExpenseBO.size(),gsrNotificationService.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkForwardRmId()));
							if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound.setNoificationMessage(notificationMessage);
								isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							}
							/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
									.max(Date::compareTo).get();
							isDuplicatesFound.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
							else
								isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
							isDuplicatesFound.setNotificationModifiedDate(new Date());
							isDuplicatesFound.setNotificationcreatedDate(new Date());
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException("Failed to Save : ", e);
							}
						}

					}
				} else {
					String menuURL = expenseReportPartialForwardAlertEmpKey.split(",")[0];
					try {
						notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
					}
					if (notificationIdList.size() != 0) {
						try {
							updateCount = notificationRepository
									.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
							if (updateCount == 0)
								throw new DataAccessException(
										"Some exception occured while updating notification table");
						} catch (Exception e) {
							throw new DataAccessException(
									"Error Occurred while updating Expiry in Notification Table by notification id.");
						}
					}
				}
				if (fullMsgIdentifierKeys.size() > 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(fullMsgIdentifierKeys,
										empId, expenseReportPartialForwardAlertEmpKey.split(",")[0]);
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
					}
				}
			}
		}catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}

	}

	/**
	 * 
	 * <Description setReportPartialForwardAlertToSkipLevelScreen:> This method
	 * is used to report partial forward alert message to Skip Level Rm
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportPartialForwardAlertToSkipLevelScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportPartialForwardAlertSkipLevelKey = expenseReportPartialForwardAlertSkipLevelKeys;
		String msgIdentifierKey = expenseReportPartialForwardAlertSkipLevelKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkForwardRmIdAndFkStatusId(empId,
					ExpenseNotificationConstants.PARTIAL_FORWARDED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(
								partialForwardAlertFromRmToSkipLevelApproverScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverMgr()),
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()));
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportPartialForwardAlertSkipLevelKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportPartialForwardAlertSkipLevelKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportPartialForwardAlertSkipLevelKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								partialForwardAlertFromRmToSkipLevelApproverScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverMgr()),
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()));
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {

					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								partialForwardAlertFromRmToSkipLevelApproverConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportPartialForwardAlertSkipLevelKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportPartialForwardAlertSkipLevelKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportPartialForwardAlertSkipLevelKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								partialForwardAlertFromRmToSkipLevelApproverConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportPartialForwardAlertSkipLevelKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportPartialForwardAlertSkipLevelKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}

	}

	/**
	 * 
	 * <Description setReportReimbursmentIntiatedToEmpScreen:> This method is
	 * used to report reimbursment initiated alert message to emp
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportReimbursmentIntiatedToEmpScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportReimbursmentInitiatedToEmpKey = expenseReportReimbursmentInitiatedToEmpKeys;
		String msgIdentifierKey = expenseReportReimbursmentInitiatedToEmpKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkEmpIdIntAndFkStatusId(empId,
					ExpenseNotificationConstants.REIMBURSEMENT_INITIATED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(
								reimbursementAlertFromReimbursorToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverFin()));
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportReimbursmentInitiatedToEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportReimbursmentInitiatedToEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportReimbursmentInitiatedToEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								reimbursementAlertFromReimbursorToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverFin()));
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {

					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								reimbursementAlertFromReimbursorToEmpConsolidatedMsg, lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportReimbursmentInitiatedToEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportReimbursmentInitiatedToEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportReimbursmentInitiatedToEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								reimbursementAlertFromReimbursorToEmpConsolidatedMsg, lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportReimbursmentInitiatedToEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportReimbursmentInitiatedToEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}
	}

	/**
	 * 
	 * <Description setReportReimbursmentIntiatedToRmScreen:> This method is
	 * used to report reimbursment initiated alert message to Rm
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportReimbursmentIntiatedToRmScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportReimbursmentInitiatedToRmKey = expenseReportReimbursmentInitiatedToRmKeys;
		String msgIdentifierKey = expenseReportReimbursmentInitiatedToRmKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByApproverMgrAndFkStatusId(empId,
					ExpenseNotificationConstants.REIMBURSEMENT_INITIATED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(
								reimbursementAlertFromReimbursorToRmScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverFin()));
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportReimbursmentInitiatedToRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportReimbursmentInitiatedToRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportReimbursmentInitiatedToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								reimbursementAlertFromReimbursorToRmScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverFin()));
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								reimbursementAlertFromReimbursorToRmConsolidatedMsg, lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportReimbursmentInitiatedToRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportReimbursmentInitiatedToRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportReimbursmentInitiatedToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								reimbursementAlertFromReimbursorToRmConsolidatedMsg, lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportReimbursmentInitiatedToRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportReimbursmentInitiatedToRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}

	}

	/**
	 * 
	 * <Description setReportReimbursmentPartialyIntiatedToEmpScreen:> This
	 * method is used to report reimbursment partially initiated alert message
	 * to emp
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportReimbursmentPartialyIntiatedToEmpScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportPartialyReimbursmentToEmpKey = expenseReportPartialyReimbursmentToEmpKeys;
		String msgIdentifierKey = expenseReportPartialyReimbursmentToEmpKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkEmpIdIntAndFkStatusId(empId,
					ExpenseNotificationConstants.PARTIAL_REIMBURSEMENT_INITIATED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(
								partialReimbursementAlertFromReimbursorToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverFin()));
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportPartialyReimbursmentToEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportPartialyReimbursmentToEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportPartialyReimbursmentToEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								partialReimbursementAlertFromReimbursorToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverFin()));
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								partialReimbursementAlertFromReimbursorToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportPartialyReimbursmentToEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportPartialyReimbursmentToEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportPartialyReimbursmentToEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								partialReimbursementAlertFromReimbursorToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportPartialyReimbursmentToEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportPartialyReimbursmentToEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}

	}

	/**
	 * 
	 * <Description setReportReimbursmentPartialyIntiatedToRMScreen:> This
	 * method is used to report reimbursment partially initiated alert message
	 * to Rm
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportReimbursmentPartialyIntiatedToRMScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportPartialyReimbursmentToRmKey = expenseReportPartialyReimbursmentToRmKeys;
		String msgIdentifierKey = expenseReportPartialyReimbursmentToRmKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByApproverMgrAndFkStatusId(empId,
					ExpenseNotificationConstants.PARTIAL_REIMBURSEMENT_INITIATED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(
								partialReimbursementAlertFromReimbursorToRmScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverFin()));
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportPartialyReimbursmentToRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportPartialyReimbursmentToRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportPartialyReimbursmentToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								partialReimbursementAlertFromReimbursorToRmScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverFin()));
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								partialReimbursementAlertFromReimbursorToRmConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportPartialyReimbursmentToRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportPartialyReimbursmentToRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportPartialyReimbursmentToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								partialReimbursementAlertFromReimbursorToRmConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportPartialyReimbursmentToRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportPartialyReimbursmentToRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}
	}

	/**
	 * 
	 * <Description setReportReimbursmentRejectedToEmpScreen:> This method is
	 * used to report reimbursment rejected alert message to Emp
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportReimbursmentRejectedToEmpScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportRejectedReimbursmentToEmpKey = expenseReportRejectedReimbursmentToEmpKeys;
		String msgIdentifierKey = expenseReportRejectedReimbursmentToEmpKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkEmpIdIntAndFkStatusId(empId,
					ExpenseNotificationConstants.REJECTED_BY_REIMBURSOR);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(
								rejectedReimbursementAlertFromReimbursorToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportRejectedReimbursmentToEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportRejectedReimbursmentToEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportRejectedReimbursmentToEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								rejectedReimbursementAlertFromReimbursorToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {

					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								rejectedReimbursementAlertFromReimbursorToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportRejectedReimbursmentToEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportRejectedReimbursmentToEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportRejectedReimbursmentToEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								rejectedReimbursementAlertFromReimbursorToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportRejectedReimbursmentToEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportRejectedReimbursmentToEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}
	}

	/**
	 * 
	 * <Description setReportReimbursmentRejectedToRMScreen:> This method is
	 * used to report reimbursment rejected alert message to RM
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportReimbursmentRejectedToRMScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportRejectedReimbursmentToRmKey = expenseReportRejectedReimbursmentToRmKeys;
		String msgIdentifierKey = expenseReportRejectedReimbursmentToRmKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByApproverMgrAndFkStatusId(empId,
					ExpenseNotificationConstants.REJECTED_BY_REIMBURSOR);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(
								rejectedReimbursementAlertFromReimbursorToRmScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportRejectedReimbursmentToRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportRejectedReimbursmentToRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportRejectedReimbursmentToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								rejectedReimbursementAlertFromReimbursorToRmScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								rejectedReimbursementAlertFromReimbursorToRmConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportRejectedReimbursmentToRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportRejectedReimbursmentToRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportRejectedReimbursmentToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								rejectedReimbursementAlertFromReimbursorToRmConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportRejectedReimbursmentToRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportRejectedReimbursmentToRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}

	}

	/**
	 * 
	 * <Description setReportReimbursmentCancelledToEmpScreen:> This method is
	 * used to report reimbursment cancelled alert message to Emp
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportReimbursmentCancelledToEmpScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportCancelledReimbursmentToEmpKey = expenseReportCancelledReimbursmentToEmpKeys;
		String msgIdentifierKey = expenseReportCancelledReimbursmentToEmpKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkEmpIdIntAndFkStatusId(empId,
					ExpenseNotificationConstants.CANCELLED_BY_REIMBURSOR);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(
								cancelledReimbursementAlertFromReimbursorToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportCancelledReimbursmentToEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportCancelledReimbursmentToEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportCancelledReimbursmentToEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								cancelledReimbursementAlertFromReimbursorToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {

					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								cancelledReimbursementAlertFromReimbursorToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportCancelledReimbursmentToEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportCancelledReimbursmentToEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportCancelledReimbursmentToEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								cancelledReimbursementAlertFromReimbursorToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportCancelledReimbursmentToEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportCancelledReimbursmentToEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}

	}

	/**
	 * 
	 * <Description setReportReimbursmentCancelledToRMScreen:> This method is
	 * used to report reimbursment cancelled alert message to Rm
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportReimbursmentCancelledToRMScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportCancelledReimbursmentToRmKey = expenseReportCancelledReimbursmentToRmKeys;
		String msgIdentifierKey = expenseReportCancelledReimbursmentToRmKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByApproverMgrAndFkStatusId(empId,
					ExpenseNotificationConstants.CANCELLED_BY_REIMBURSOR);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(
								cancelledReimbursementAlertFromReimbursorToRmScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportCancelledReimbursmentToRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportCancelledReimbursmentToRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportCancelledReimbursmentToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								cancelledReimbursementAlertFromReimbursorToRmScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								cancelledReimbursementAlertFromReimbursorToRmConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportCancelledReimbursmentToRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportCancelledReimbursmentToRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportCancelledReimbursmentToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								cancelledReimbursementAlertFromReimbursorToRmConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
/*						Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);
*/						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportCancelledReimbursmentToRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportCancelledReimbursmentToRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}

		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}
	}

	/**
	 * 
	 * <Description setReportReimbursedToEmpScreen:> This method is used to
	 * report reimbursed alert message to Emp
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportReimbursedToEmpScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportReimbursedToEmpKey = expenseReportReimbursedToEmpKeys;
		String msgIdentifierKey = expenseReportReimbursedToEmpKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkEmpIdIntAndFkStatusId(empId,
					ExpenseNotificationConstants.REIMBURSED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(
								reimbursedAlertFromReimbursorToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportReimbursedToEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportReimbursedToEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportReimbursedToEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								reimbursedAlertFromReimbursorToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								reimbursedAlertFromReimbursorToEmpConsolidatedMsg, lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportReimbursedToEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportReimbursedToEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportReimbursedToEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								reimbursedAlertFromReimbursorToEmpConsolidatedMsg, lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportReimbursedToEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportReimbursedToEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}
	}

	/**
	 * 
	 * <Description setReportReimbursedToRMScreen:> This method is used to
	 * report reimbursed alert message to Rm
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportReimbursedToRMScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportReimbursedToRmKey = expenseReportReimbursedToRmKeys;
		String msgIdentifierKey = expenseReportReimbursedToRmKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByApproverMgrAndFkStatusId(empId,
					ExpenseNotificationConstants.REIMBURSED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(
								reimbursedAlertFromReimbursorToRmScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportReimbursedToRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportReimbursedToRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportReimbursedToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								reimbursedAlertFromReimbursorToRmScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								reimbursedAlertFromReimbursorToRmConsolidatedMsg, lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportReimbursedToRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportReimbursedToRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportReimbursedToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								reimbursedAlertFromReimbursorToRmConsolidatedMsg, lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportReimbursedToRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportReimbursedToRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}
	}
	
	/**
	 * 
	 * <Description setRejectByEmpToRmScreen:> This method is used to set reject report
	 *  alert message to Rm
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setRejectByEmpToRmScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseEmpRejectrdAlertRmKey = expenseEmpRejectrdAlertRmKeys;
		String msgIdentifierKey = expenseEmpRejectrdAlertRmKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByApproverMgrAndFkStatusId(empId,
					ExpenseNotificationConstants.EMP_REJECTED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(empRejectedToApproverScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()),
								String.valueOf(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()).replaceAll(",", ""),
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseEmpRejectrdAlertRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseEmpRejectrdAlertRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseEmpRejectrdAlertRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(empRejectedToApproverScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()),
										String.valueOf(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()).replaceAll(",", ""),
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(empRejectedToApproverScreenConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseEmpRejectrdAlertRmKey.split(",")[0]);
						notificationDetails.setIsAction(expenseEmpRejectrdAlertRmKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseEmpRejectrdAlertRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(empRejectedToApproverScreenConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				}
			} else {
				String menuURL = expenseEmpRejectrdAlertRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseEmpRejectrdAlertRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}
	}

	/**
	* 
	 * <Description setRejectByEmpToEmp:> This method is used to set reject report
	 *  alert message to Emp
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setRejectByEmpToEmp(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseEmpRejectrdAlertEmpKey = expenseEmpRejectrdAlertEmpKeys;
		String msgIdentifierKey = expenseEmpRejectrdAlertEmpKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkEmpIdIntAndFkStatusId(empId,
					ExpenseNotificationConstants.EMP_REJECTED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat.format(empRejectedToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseEmpRejectrdAlertEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseEmpRejectrdAlertEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseEmpRejectrdAlertEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(empRejectedToEmpScreenIndividualMsg,
								lstExpReportDataExpenseBO.get(0).getReportTitle());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(empRejectedToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseEmpRejectrdAlertEmpKey.split(",")[0]);
						notificationDetails.setIsAction(expenseEmpRejectrdAlertEmpKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseEmpRejectrdAlertEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(empRejectedToEmpConsolidatedMsg,
								lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				}

			} else {
				String menuURL = expenseEmpRejectrdAlertEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseEmpRejectrdAlertEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}
	}
	
	/**
	 * 
	 * <Description setReportRejecteddAlertToReimbursorScreen:> This method is
	 * used to report rejected alert message to Reimbusor
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */

	public void setReportRejecteddAlertToReimbursorScreen(int empId) throws DataAccessException {
		List<ExpDatReportBO> lstExpReportDataExpenseBO = null;
		String expenseReportRejectedAlertFinKey = expenseReportRejectedAlertFinKeys;
		String msgIdentifierKey = expenseReportRejectedAlertFinKey.split(",")[2];
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			DatEmpProfessionalDetailBO datEmpProfessionalDetailBO=datEmployeeProfessionalRepository.findByFkMainEmpDetailId(empId);
			lstExpReportDataExpenseBO = expDatReportRepository.findByFkFinReimburserAndFkStatusId(datEmpProfessionalDetailBO.getFkEmpOrganizationId(),
					ExpenseNotificationConstants.REJECTED);
			if (lstExpReportDataExpenseBO.size() > 0) {
				if (lstExpReportDataExpenseBO.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						String approverName="";
						if(lstExpReportDataExpenseBO.get(0).getFkForwardRmId()== null)
							approverName=gsrNotificationService
							.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverMgr());
						
						else
							approverName=gsrNotificationService
							.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkForwardRmId());
						String notificationMessage = MessageFormat.format(
								rejectedAlertFromRmToReimbursorScreenIndividualMsg,
								approverName,
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()));
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportRejectedAlertFinKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportRejectedAlertFinKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportRejectedAlertFinKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String approverName="";
						if(lstExpReportDataExpenseBO.get(0).getFkForwardRmId()==null)
							approverName=gsrNotificationService
							.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getApproverMgr());
						else
							approverName=gsrNotificationService
							.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkForwardRmId());
							
						String notificationMessage = MessageFormat.format(
								rejectedAlertFromRmToReimbursorScreenIndividualMsg,
								approverName,
								lstExpReportDataExpenseBO.get(0).getReportTitle(), gsrNotificationService
										.getEmployeeNameByEmpId(lstExpReportDataExpenseBO.get(0).getFkEmpIdInt()));
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						/*if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= lstExpReportDataExpenseBO.get(0)
								.getSubmissionDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(lstExpReportDataExpenseBO.get(0).getSubmissionDate());*/
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();*/
						String notificationMessage = MessageFormat.format(
								rejectedAlertFromRmToReimbursorConsolidatedMsg, lstExpReportDataExpenseBO.size());
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails.setNoificationMessage(notificationMessage);
						notificationDetails.setClearFlag(ExpenseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(new Date());
						notificationDetails.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails.setModuleName(ExpenseNotificationConstants.MODULE_NAME);
						notificationDetails.setFkModuleId(Short.parseShort(expenseMenuId));
						notificationDetails.setMenuUrl(expenseReportRejectedAlertFinKey.split(",")[0]);
						notificationDetails.setIsAction(expenseReportRejectedAlertFinKey.split(",")[1]);
						notificationDetails.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(
								Integer.parseInt(expenseReportRejectedAlertFinKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								rejectedAlertFromRmToReimbursorConsolidatedMsg, lstExpReportDataExpenseBO.size());
						if (!isDuplicatesFound.getNoificationMessage().equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound.setNoificationMessage(notificationMessage);
							isDuplicatesFound.setNoificationIsExpiry(ExpenseNotificationConstants.IS_EXPIRY_NO);
						}
						/*Date maxDate = lstExpReportDataExpenseBO.stream().map(ExpDatReportBO::getSubmissionDate)
								.max(Date::compareTo).get();
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate().getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(maxDate);*/
						isDuplicatesFound.setNotificationModifiedDate(new Date());
						isDuplicatesFound.setNotificationcreatedDate(new Date());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ", e);
						}
					}

				}
			} else {
				String menuURL = expenseReportRejectedAlertFinKey.split(",")[0];
				try {
					notificationIdList = notificationRepository.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException("Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
							fullMsgIdentifierKeys, empId, expenseReportRejectedAlertFinKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error in setting  alert to Emp Notifications");
		}

	}
}
	
