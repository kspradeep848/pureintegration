package com.thbs.mis.notificationframework.expensenotification.constants;

public class ExpenseNotificationConstants {
	public final static String CLEAR_FLAG_NO = "NO";
	public static final String MODULE_NAME = "EXPENSE";
	public static final String IS_EXPIRY_NO = "NOTEXPIRED";
	public static final short AWAITING_APPROVAL=2;
	public static final short RECALLED=3;
	public static final short REJECTED=6;
	public static final short REIMBURSEMENT_INITIATED=9;
	public static final short PARTIAL_REIMBURSEMENT_INITIATED=16;
	public static final short REJECTED_BY_REIMBURSOR=15;
	public static final short CANCELLED_BY_REIMBURSOR=11;
	public static final short REIMBURSED=12;
	public static final short FORWARDED=13;
	public static final short PARTIAL_FORWARDED=14;
	public static final short APPROVED=5;
	public static final short PARTIAL_APPROVED=7;
	public static final short EMP_REJECTED=8;
}
