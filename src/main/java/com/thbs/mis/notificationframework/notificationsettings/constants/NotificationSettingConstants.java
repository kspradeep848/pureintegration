package com.thbs.mis.notificationframework.notificationsettings.constants;

public class NotificationSettingConstants {

	public final static String  CLEAR_ALL_FLAG_NO="NO";
	public final static String  CLEAR_ALL_FLAG_YES="YES";
	public final static String  SETTING_TYPE="ENABLE";
	public static final String IS_EXPIRY_NO = "NOTEXPIRED";
	
	public static final String CLEAR_FLAG_YES = "YES";
	public static final String CLEAR_FLAG_NO = "NO";

}
