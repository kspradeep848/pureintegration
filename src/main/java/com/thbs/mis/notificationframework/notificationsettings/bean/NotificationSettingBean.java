package com.thbs.mis.notificationframework.notificationsettings.bean;

public class NotificationSettingBean {
	private String status;
	private int pkNotificationId;
private int empId;
private String settingType;
private String moduleName;
private int previousCount;


public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public int getEmpId() {
	return empId;
}
public void setEmpId(int empId) {
	this.empId = empId;
}
public String getSettingType() {
	return settingType;
}
public void setSettingType(String settingType) {
	this.settingType = settingType;
}


public String getModuleName() {
	return moduleName;
}
public void setModuleName(String moduleName) {
	this.moduleName = moduleName;
}
public int getPkNotificationId() {
	return pkNotificationId;
}
public void setPkNotificationId(int pkNotificationId) {
	this.pkNotificationId = pkNotificationId;
}

public int getPreviousCount() {
	return previousCount;
}
public void setPreviousCount(int previousCount) {
	this.previousCount = previousCount;
}

@Override
public String toString() {
	return "NotificationSettingBean [status=" + status + ", pkNotificationId="
			+ pkNotificationId + ", empId=" + empId + ", settingType="
			+ settingType + ", moduleName=" + moduleName + ", previousCount="
			+ previousCount + "]";
}

}
