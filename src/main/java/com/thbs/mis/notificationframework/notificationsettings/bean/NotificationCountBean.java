package com.thbs.mis.notificationframework.notificationsettings.bean;

public class NotificationCountBean {

	private int currentCount;
	private int previousCount;
	public int getCurrentCount() {
		return currentCount;
	}
	public void setCurrentCount(int currentCount) {
		this.currentCount = currentCount;
	}
	public int getPreviousCount() {
		return previousCount;
	}
	public void setPreviousCount(int previousCount) {
		this.previousCount = previousCount;
	}
	@Override
	public String toString() {
		return "NotificationCountBean [currentCount=" + currentCount
				+ ", previousCount=" + previousCount + "]";
	}
	
	
}
