package com.thbs.mis.notificationframework.notificationsettings.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.notificationframework.notificationsettings.bo.MasNotificationSettingBO;



@Repository
public interface MasNotificationSettingRepository  extends GenericRepository<MasNotificationSettingBO, Integer>
{

	@Query("select n from MasNotificationSettingBO n where n.fkEmployeeId=?")
	public List<MasNotificationSettingBO> findByEmpId(int empId);
	
	public MasNotificationSettingBO findByFkEmployeeId(int empId);
	public MasNotificationSettingBO findByFkEmployeeIdIsNull(int empId);


}
