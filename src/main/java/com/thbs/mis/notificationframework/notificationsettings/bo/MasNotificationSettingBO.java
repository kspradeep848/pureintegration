package com.thbs.mis.notificationframework.notificationsettings.bo;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the mas_notification_settings database table.
 * 
 */
@Entity
@Table(name="mas_notification_settings")
@NamedQuery(name="MasNotificationSettingBO.findAll", query="SELECT m FROM MasNotificationSettingBO m")
public class MasNotificationSettingBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_seting_type_id")
	private int pkSetingTypeId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="clear_all_created_date")
	private Date clearAllCreatedDate;

	@Column(name="clear_all_flag")
	private String clearAllFlag;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="clear_all_modified_date")
	private Date clearAllModifiedDate;

	@Column(name="notification_type")
	private String notificationType;

	
	@Column(name="fk_employee_id")
	private int fkEmployeeId;
	
	@Column(name="previous_count")
	private int previousCount;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="disabled_date")
	private Date disabledDate;

	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_accessed_date")
	private Date lastAccessedDate;


/*	@OneToOne
	@JoinColumn(name="fk_employee_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO;*/
	public MasNotificationSettingBO() {
	}

	public int getPkSetingTypeId() {
		return this.pkSetingTypeId;
	}

	public Date getLastAccessedDate() {
		return lastAccessedDate;
	}

	public void setLastAccessedDate(Date lastAccessedDate) {
		this.lastAccessedDate = lastAccessedDate;
	}

	public void setPkSetingTypeId(int pkSetingTypeId) {
		this.pkSetingTypeId = pkSetingTypeId;
	}

	public Date getClearAllCreatedDate() {
		return this.clearAllCreatedDate;
	}

	public void setClearAllCreatedDate(Date clearAllCreatedDate) {
		this.clearAllCreatedDate = clearAllCreatedDate;
	}



	public String getClearAllFlag() {
		return clearAllFlag;
	}

	public void setClearAllFlag(String clearAllFlag) {
		this.clearAllFlag = clearAllFlag;
	}

	public Date getClearAllModifiedDate() {
		return this.clearAllModifiedDate;
	}

	public void setClearAllModifiedDate(Date clearAllModifiedDate) {
		this.clearAllModifiedDate = clearAllModifiedDate;
	}

	public String getNotificationType() {
		return this.notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public int getFkEmployeeId() {
		return fkEmployeeId;
	}

	public void setFkEmployeeId(int fkEmployeeId) {
		this.fkEmployeeId = fkEmployeeId;
	}

	public int getPreviousCount() {
		return previousCount;
	}

	public void setPreviousCount(int previousCount) {
		this.previousCount = previousCount;
	}

	public Date getDisabledDate() {
		return disabledDate;
	}

	public void setDisabledDate(Date disabledDate) {
		this.disabledDate = disabledDate;
	}

	@Override
	public String toString() {
		return "MasNotificationSettingBO [pkSetingTypeId=" + pkSetingTypeId + ", clearAllCreatedDate="
				+ clearAllCreatedDate + ", clearAllFlag=" + clearAllFlag + ", clearAllModifiedDate="
				+ clearAllModifiedDate + ", notificationType=" + notificationType + ", fkEmployeeId=" + fkEmployeeId
				+ ", previousCount=" + previousCount + ", disabledDate=" + disabledDate + ", lastAccessedDate="
				+ lastAccessedDate + "]";
	}

	

}