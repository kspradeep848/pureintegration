/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrURIConstants.java                              */
/*                                                                   */
/*  Author      :  THBS                                              */
/*                                                                   */
/*  Date        : October 28, 2016                                   */
/*                                                                   */
/*  Description : This is used to create URI for all the services.   */
/*		 									                         */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* October 28, 2016     THBS     1.0        Initial version created  */
/*********************************************************************/

package com.thbs.mis.notificationframework.notificationsettings.constants;

/**
 * 
 * @author THBS
 * @Description: This is used to create URI for all the services.
 */
public class NotificationSettingsURIConstants {
	
	public static final String GET_ALL_MODULE_NOTFICATIONS = "/notifications/all";
	public static final String GET_INDIVIDUAL_MODULE_NOTIFICATION = "/notifications/individualmodule";

	public static final String GET_ALL_MODULE_NOTFICATIONS_COUNT = "/notifications/all/count";
	public static final String GET_INDIVIDUAL_MODULE_NOTFICATIONS_COUNT = "/notifications/module/count";

	public static final String NOTIFICATION_TYPE_SETTINGS_STATUS = "/notificationsettings/check/status";

	public static final String NOTIFICATION_TYPE_SETTINGS_CREATE = "/notificationsettings/notificationtype/create";
	public static final String NOTIFICATION_TYPE_SETTINGS_UPDATE = "/notificationsettings/notificationtype/update";

	public static final String CLEAR_ALL_FLAG_CREATE = "/notifications/clearallflag/createorupdate";

	public static final String DELETE_NOTIFICATION = "/notifications/delete";
	public static final String INSERT_COUNT = "/notifications/insert/previouscount";


}
