package com.thbs.mis.notificationframework.notificationsettings.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.MasMenuBO;

/**
 * The persistent class for the dat_notifications_training database table.
 * 
 */
@Entity
@Table(name = "dat_notifications")
@NamedQuery(name = "DatNotificationsBO.findAll", query = "SELECT d FROM DatNotificationsBO d")
public class DatNotificationsBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_notification_id")
	private int pkNotificationId;

	@Column(name = "notification_dat_emp_id")
	private int empId;
	
	@Column(name = "noification_message")
	private String noificationMessage;
	
	@Column(name = "noification_clear_flag")
	private String clearFlag;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "notification_created_date")
	private Date notificationcreatedDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "notification_modified_date")
	private Date notificationModifiedDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "noification_clear_flag_created_date")
	private Date clearFlagcreatedDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "noification_clear_flag_modified_date")
	private Date clearFlagModifiedDate;

	@Column(name = "noification_module_name")
	private String moduleName;

	@Column(name = "noification_menu_url_key")
	private String menuUrl;

	@Column(name = "fk_module_id")
	private short fkModuleId;
	
	@Column(name = "noification_message_identifier")
	private String noificationMessageIdentifier;

	@Column(name = "noification_is_expiry")
	private String noificationIsExpiry;

	@Column(name = "notification_expiry_time")
	private Integer notificationExpiryTime;
	
	@Column(name = "is_action")
	private String isAction;

	@OneToOne
	@JoinColumn(name = "fk_module_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasMenuBO masMenu;

	public String getIsAction() {
		return isAction;
	}

	public void setIsAction(String isAction) {
		this.isAction = isAction;
	}

	public DatNotificationsBO() {
	}

	public int getPkNotificationId() {
		return pkNotificationId;
	}

	public void setPkNotificationId(int pkNotificationId) {
		this.pkNotificationId = pkNotificationId;
	}

	public String getClearFlag() {
		return this.clearFlag;
	}

	public void setClearFlag(String clearFlag) {
		this.clearFlag = clearFlag;
	}

	public int getEmpId() {
		return this.empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getNoificationMessage() {
		return this.noificationMessage;
	}

	public void setNoificationMessage(String noificationMessage) {
		this.noificationMessage = noificationMessage;
	}

	public Date getNotificationcreatedDate() {
		return notificationcreatedDate;
	}

	public void setNotificationcreatedDate(Date notificationcreatedDate) {
		this.notificationcreatedDate = notificationcreatedDate;
	}

	public Date getClearFlagcreatedDate() {
		return clearFlagcreatedDate;
	}

	public void setClearFlagcreatedDate(Date clearFlagcreatedDate) {
		this.clearFlagcreatedDate = clearFlagcreatedDate;
	}

	public Date getClearFlagModifiedDate() {
		return clearFlagModifiedDate;
	}

	public void setClearFlagModifiedDate(Date clearFlagModifiedDate) {
		this.clearFlagModifiedDate = clearFlagModifiedDate;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public Date getNotificationModifiedDate() {
		return notificationModifiedDate;
	}

	public void setNotificationModifiedDate(Date notificationModifiedDate) {
		this.notificationModifiedDate = notificationModifiedDate;
	}

	public short getFkModuleId() {
		return fkModuleId;
	}

	public void setFkModuleId(short fkModuleId) {
		this.fkModuleId = fkModuleId;
	}

	public MasMenuBO getMasMenu() {
		return masMenu;
	}

	public void setMasMenu(MasMenuBO masMenu) {
		this.masMenu = masMenu;
	}

	public String getMenuUrl() {
		return menuUrl;
	}

	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}

	public String getNoificationMessageIdentifier() {
		return noificationMessageIdentifier;
	}

	public void setNoificationMessageIdentifier(
			String noificationMessageIdentifier) {
		this.noificationMessageIdentifier = noificationMessageIdentifier;
	}

	public String getNoificationIsExpiry() {
		return noificationIsExpiry;
	}

	public void setNoificationIsExpiry(String noificationIsExpiry) {
		this.noificationIsExpiry = noificationIsExpiry;
	}

	public Integer getNotificationExpiryTime() {
		return notificationExpiryTime;
	}

	public void setNotificationExpiryTime(Integer notificationExpiryTime) {
		this.notificationExpiryTime = notificationExpiryTime;
	}

	@Override
	public String toString() {
		return "DatNotificationsBO [pkNotificationId=" + pkNotificationId
				+ ", empId=" + empId + ", noificationMessage="
				+ noificationMessage + ", clearFlag=" + clearFlag
				+ ", notificationcreatedDate=" + notificationcreatedDate
				+ ", notificationModifiedDate=" + notificationModifiedDate
				+ ", clearFlagcreatedDate=" + clearFlagcreatedDate
				+ ", clearFlagModifiedDate=" + clearFlagModifiedDate
				+ ", moduleName=" + moduleName + ", menuUrl=" + menuUrl
				+ ", fkModuleId=" + fkModuleId
				+ ", noificationMessageIdentifier="
				+ noificationMessageIdentifier + ", noificationIsExpiry="
				+ noificationIsExpiry + ", notificationExpiryTime="
				+ notificationExpiryTime + ", isAction=" + isAction
				+ ", masMenu=" + masMenu + "]";
	}

}