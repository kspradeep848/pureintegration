package com.thbs.mis.notificationframework.notificationsettings.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.notificationsettings.bean.NotificationSettingBean;
import com.thbs.mis.notificationframework.notificationsettings.bo.DatNotificationsBO;
import com.thbs.mis.notificationframework.notificationsettings.bo.MasNotificationSettingBO;
import com.thbs.mis.notificationframework.notificationsettings.constants.NotificationSettingConstants;
import com.thbs.mis.notificationframework.notificationsettings.dao.MasNotificationSettingRepository;
import com.thbs.mis.notificationframework.notificationsettings.dao.NotificationRepository;
import com.thbs.mis.notificationframework.trainingnotification.bean.TrainingModuleNotificationBean;

@Service
public class NotificationSettingsService {

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(NotificationSettingsService.class);

	@Autowired
	private MasNotificationSettingRepository masNotificationSettingRepository;

	/*
	 * @Autowired private TrainingNotificationRepository notificationRepository;
	 */

	@Autowired
	private NotificationRepository notificationRepository;

	public MasNotificationSettingBO createnotificationSetting(int empId)
			throws DataAccessException {
		MasNotificationSettingBO masNotificationSettingBO = new MasNotificationSettingBO();
		LOG.startUsecase("notificationSetting inside");
		try {

			masNotificationSettingBO
					.setNotificationType(NotificationSettingConstants.SETTING_TYPE);
			masNotificationSettingBO
					.setClearAllFlag(NotificationSettingConstants.CLEAR_ALL_FLAG_NO);
			masNotificationSettingBO.setFkEmployeeId(empId);

			masNotificationSettingRepository.save(masNotificationSettingBO);

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to sve to notificationSetting : ", e);

		}
		LOG.endUsecase("notificationSetting inside");
		return masNotificationSettingBO;

	}

	public List<MasNotificationSettingBO> updateNotificationSetting(int empId,
			String settingType) throws DataAccessException {
		List<MasNotificationSettingBO> masNotificationSettingBO = new ArrayList<MasNotificationSettingBO>();
		MasNotificationSettingBO bo;
		masNotificationSettingBO = masNotificationSettingRepository
				.findByEmpId(empId);
		LOG.startUsecase("notificationSetting inside");
		try {

			if (masNotificationSettingBO.size() != 0) {
				MasNotificationSettingBO updateMasNotificationSettingBO = new MasNotificationSettingBO();
				updateMasNotificationSettingBO = masNotificationSettingRepository
						.findByFkEmployeeId(empId);
				if (settingType.equalsIgnoreCase("DISABLE"))
					;
				{
					updateMasNotificationSettingBO
							.setNotificationType(settingType);
					updateMasNotificationSettingBO.setDisabledDate(new Date());
					masNotificationSettingRepository
							.save(updateMasNotificationSettingBO);
				}
				if (settingType.equalsIgnoreCase("ENABLE")) {
					updateMasNotificationSettingBO
							.setNotificationType(settingType);
					masNotificationSettingRepository
							.save(updateMasNotificationSettingBO);
				}

			} else {
				bo = new MasNotificationSettingBO();

				bo.setNotificationType(NotificationSettingConstants.SETTING_TYPE);
				bo.setClearAllFlag(NotificationSettingConstants.CLEAR_ALL_FLAG_NO);
				bo.setFkEmployeeId(empId);
				masNotificationSettingRepository.save(bo);
			}

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to sve to notificationSetting : ", e);

		}
		return masNotificationSettingBO;

	}

	public NotificationSettingBean notificationSettingStatus(int empId)
			throws DataAccessException

	{
		NotificationSettingBean bean = new NotificationSettingBean();
		MasNotificationSettingBO boOutput;
		try {
			boOutput = masNotificationSettingRepository
					.findByFkEmployeeId(empId);
			if (boOutput != null) {
				bean.setStatus("TRUE");
				bean.setSettingType(boOutput.getNotificationType());
			} else {
				bean.setStatus("FALSE");
				;
			}
			System.out.println("boOutput" + boOutput);

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to sve to notificationSetting : ", e);

		}
		return bean;

	}

	public MasNotificationSettingBO clearAllFlagCreateAndUpdate(int empId)
			throws DataAccessException {
		MasNotificationSettingBO masNotificationSettingBO;
		LOG.startUsecase("notificationSetting inside");
		try {
			masNotificationSettingBO = masNotificationSettingRepository
					.findByFkEmployeeId(empId);

			if (masNotificationSettingBO.getClearAllCreatedDate() == null) {
				masNotificationSettingBO
						.setClearAllFlag(NotificationSettingConstants.CLEAR_ALL_FLAG_YES);
				masNotificationSettingBO.setClearAllCreatedDate(new Date());

			} else {
				masNotificationSettingBO
						.setClearAllFlag(NotificationSettingConstants.CLEAR_ALL_FLAG_YES);
				masNotificationSettingBO.setClearAllModifiedDate(new Date());
			}

			masNotificationSettingRepository.save(masNotificationSettingBO);

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to sve to notificationSetting : ", e);

		}
		return masNotificationSettingBO;

	}

	public DatNotificationsBO deleteNotificationByClearFlag(int notificationId)
			throws DataAccessException {
		DatNotificationsBO datNotificationsBO;
		try {
			datNotificationsBO = notificationRepository
					.findByPkNotificationId(notificationId);
			datNotificationsBO.setClearFlag("YES");
			datNotificationsBO.setClearFlagModifiedDate(new Date());
			notificationRepository.save(datNotificationsBO);
		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to sve to notificationSetting : ", e);

		}
		return datNotificationsBO;
	}

	/*
	 * Get All Module notifications from newly created dat notification table
	 */
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	public List<Map> getAllModuleNotifications(int empId)
			throws DataAccessException {
		List<TrainingModuleNotificationBean> trainingModuleNotificationsOutputBean = new ArrayList<TrainingModuleNotificationBean>();
		List<DatNotificationsBO> NotificationsBoOutput = new ArrayList<DatNotificationsBO>();
		List<Map> filteredOutputBeanByDate = new ArrayList<Map>();
		Map<String, List<TrainingModuleNotificationBean>> filterMap = new HashMap<String, List<TrainingModuleNotificationBean>>();
		MasNotificationSettingBO notificationSettingoutput;
		TrainingModuleNotificationBean tempBean;
		List<Date> dateList = new ArrayList<Date>();
		try {
			notificationSettingoutput = masNotificationSettingRepository
					.findByFkEmployeeId(empId);

		} catch (Exception e) {
			e.printStackTrace();
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}
		if (notificationSettingoutput != null) {
			if (notificationSettingoutput.getNotificationType()
					.equalsIgnoreCase("ENABLE")
					&& notificationSettingoutput.getClearAllFlag()
							.equalsIgnoreCase("NO")) {
				LOG.info("Entering into getAllModuleNotifications getClearAllFlag is NO ---------> ");
				try {

					if (notificationSettingoutput.getDisabledDate() == null)
						NotificationsBoOutput = notificationRepository
								.findByEmpIdAndNoificationIsExpiryOrderByNotificationcreatedDateDesc(
										empId,
										NotificationSettingConstants.IS_EXPIRY_NO);
					else
						NotificationsBoOutput = notificationRepository
								.findByEmpIdAndNotificationcreatedDateBetweenAndNoificationIsExpiryOrderByNotificationcreatedDateDesc(
										empId,
										notificationSettingoutput
												.getDisabledDate(),
										new Date(),
										NotificationSettingConstants.IS_EXPIRY_NO);

					/*
					 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
					 * 
					 * tempBean = new TrainingModuleNotificationBean(); if
					 * (NotificationsBoOutput.get(i).getClearFlag()
					 * .equalsIgnoreCase("NO")) {
					 * tempBean.setPkTrainingNotificationId
					 * (NotificationsBoOutput .get(i).getPkNotificationId());
					 * tempBean.setEmpId(NotificationsBoOutput.get(i)
					 * .getEmpId());
					 * tempBean.setNotificationMessage(NotificationsBoOutput
					 * .get(i).getNoificationMessage());
					 * 
					 * System.out.println(" date _______>  1" +
					 * NotificationsBoOutput.get(i)
					 * .getNotificationcreatedDate());
					 * 
					 * Date date = NotificationsBoOutput.get(i)
					 * .getNotificationcreatedDate(); String
					 * notificationcreatedDatedate = dateFormat .format(date);
					 * System.out.println("notificationcreatedDatedate" +
					 * notificationcreatedDatedate);
					 * System.out.println((dateFormat.format(date)));
					 * 
					 * tempBean.setNotificationCreatedDateString(
					 * notificationcreatedDatedate);
					 * tempBean.setNotificationCreatedDate(date);
					 * tempBean.setClearFlag(NotificationsBoOutput.get(i)
					 * .getClearFlag());
					 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
					 * .get(i).getClearFlagcreatedDate());
					 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
					 * .get(i).getClearFlagModifiedDate());
					 * 
					 * tempBean.setModule(NotificationsBoOutput.get(i)
					 * .getModuleName());
					 * tempBean.setModuleId(NotificationsBoOutput.get(i)
					 * .getMasMenu().getPkMenuId());
					 * tempBean.setMenuUrl(NotificationsBoOutput.get(i)
					 * .getMenuUrl());
					 * 
					 * trainingModuleNotificationsOutputBean.add(tempBean);
					 * 
					 * }
					 * 
					 * }
					 */

				} catch (Exception e) {
					throw new DataAccessException("Error while Retrieving : ",
							e);
				}
			}

			if (notificationSettingoutput.getNotificationType()
					.equalsIgnoreCase("ENABLE")
					&& notificationSettingoutput.getClearAllFlag()
							.equalsIgnoreCase("YES")) {
				try {
					if (notificationSettingoutput.getClearAllCreatedDate() != null)
						dateList.add(notificationSettingoutput
								.getClearAllCreatedDate());
					if (notificationSettingoutput.getClearAllModifiedDate() != null)
						dateList.add(notificationSettingoutput
								.getClearAllModifiedDate());
					if (notificationSettingoutput.getDisabledDate() != null)
						dateList.add(notificationSettingoutput
								.getDisabledDate());

					Collections.sort(dateList);
					Collections.reverse(dateList);
					NotificationsBoOutput = notificationRepository
							.findByEmpIdAndNotificationcreatedDateBetweenAndNoificationIsExpiryOrderByNotificationcreatedDateDesc(
									empId, dateList.get(0), new Date(),
									NotificationSettingConstants.IS_EXPIRY_NO);

				} catch (Exception e) {
					throw new DataAccessException("Error while Retrieving : ",
							e);
				}
			}

			if (notificationSettingoutput.getNotificationType()
					.equalsIgnoreCase("DISABLE")) {
				try {
					NotificationsBoOutput = notificationRepository
							.findByEmpIdAndNotificationcreatedDateLessThanEqualAndNoificationIsExpiryOrderByNotificationcreatedDateDesc(
									empId,
									notificationSettingoutput.getDisabledDate(),
									NotificationSettingConstants.IS_EXPIRY_NO);
					dateList.clear();
					if (notificationSettingoutput.getClearAllFlag()
							.equalsIgnoreCase("YES")) {
						if (notificationSettingoutput.getClearAllCreatedDate() != null)
							dateList.add(notificationSettingoutput
									.getClearAllCreatedDate());
						if (notificationSettingoutput.getClearAllModifiedDate() != null)
							dateList.add(notificationSettingoutput
									.getClearAllModifiedDate());

						Collections.sort(dateList);
						Collections.reverse(dateList);

						if (dateList.get(0).getTime() > notificationSettingoutput
								.getDisabledDate().getTime()) {
							NotificationsBoOutput.clear();
						}
					}
				} catch (Exception e) {
					throw new DataAccessException("Error while Retrieving : ",
							e);
				}
			}

			/*
			 * if (notificationSettingoutput.getNotificationType()
			 * .equalsIgnoreCase("ENABLE") &&
			 * notificationSettingoutput.getClearAllFlag()
			 * .equalsIgnoreCase("YES") &&
			 * notificationSettingoutput.getClearAllModifiedDate() == null) {
			 * 
			 * Date startDate = notificationSettingoutput
			 * .getClearAllCreatedDate(); Date endDate = new Date(); LOG.info(
			 * "Entering into getAllModuleNotifications getClearAllFlag is YES ---------> "
			 * ); LOG.info(
			 * "getClearAllModifiedDate is considered to  startDate ---------> "
			 * + startDate); LOG.info("end Date is current date ---------> " +
			 * endDate);
			 * 
			 * LOG.info(
			 * "Inside Get Training Module notifications with  Clear all Flag created Date "
			 * ); try { NotificationsBoOutput = notificationRepository
			 * .findByEmpIdAndNotificationcreatedDateBetweenAndNoificationIsExpiry
			 * ( empId, startDate, endDate,
			 * NotificationSettingConstants.IS_EXPIRY_NO); LOG.info(
			 * "Inside Get Training Module notifications with  Clear all Flag created Date trainingModuleNotificationsBoOutput "
			 * + NotificationsBoOutput);
			 * 
			 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
			 * 
			 * tempBean = new TrainingModuleNotificationBean(); if
			 * (NotificationsBoOutput.get(i).getClearFlag()
			 * .equalsIgnoreCase("NO")) {
			 * tempBean.setPkTrainingNotificationId(NotificationsBoOutput
			 * .get(i).getPkNotificationId());
			 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
			 * tempBean.setNotificationMessage(NotificationsBoOutput
			 * .get(i).getNoificationMessage());
			 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
			 * .get(i).getNotificationcreatedDate());
			 * System.out.println(" date _______>  2" +
			 * NotificationsBoOutput.get(i) .getNotificationcreatedDate()); Date
			 * date = NotificationsBoOutput.get(i)
			 * .getNotificationcreatedDate(); String notificationcreatedDatedate
			 * = dateFormat .format(date);
			 * System.out.println("notificationcreatedDatedate" +
			 * notificationcreatedDatedate);
			 * System.out.println((dateFormat.format(date)));
			 * 
			 * tempBean.setNotificationCreatedDateString(notificationcreatedDatedate
			 * ); tempBean.setClearFlag(NotificationsBoOutput.get(i)
			 * .getClearFlag());
			 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
			 * .get(i).getClearFlagcreatedDate());
			 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
			 * .get(i).getClearFlagModifiedDate());
			 * 
			 * tempBean.setModule(NotificationsBoOutput.get(i)
			 * .getModuleName());
			 * tempBean.setModuleId(NotificationsBoOutput.get(i)
			 * .getMasMenu().getPkMenuId());
			 * tempBean.setMenuUrl(NotificationsBoOutput.get(i) .getMenuUrl());
			 * 
			 * trainingModuleNotificationsOutputBean.add(tempBean); }
			 * 
			 * }
			 * 
			 * System.out.println("2" +
			 * trainingModuleNotificationsOutputBean.size());
			 * 
			 * } catch (Exception e) { throw new
			 * DataAccessException("Error while Retrieving : ", e); } }
			 */
			/*
			 * if (notificationSettingoutput.getNotificationType()
			 * .equalsIgnoreCase("ENABLE") &&
			 * notificationSettingoutput.getClearAllFlag()
			 * .equalsIgnoreCase("YES") &&
			 * notificationSettingoutput.getClearAllModifiedDate() != null) {
			 * LOG.info(
			 * "Inside Get get All Module Notificationss with  Clear all Flag Modified Date "
			 * );
			 * 
			 * Date startDate = notificationSettingoutput
			 * .getClearAllModifiedDate(); Date endDate = new Date(); LOG.info(
			 * "Entering into getAllModuleNotifications getClearAllFlag is YES ---------> "
			 * ); LOG.info(
			 * "getClearAllModifiedDate is considered to  startDate ---------> "
			 * + startDate); LOG.info("end Date is current date ---------> " +
			 * endDate); try { NotificationsBoOutput = notificationRepository
			 * .findByEmpIdAndNotificationcreatedDateBetweenAndNoificationIsExpiry
			 * ( empId, startDate, endDate,
			 * NotificationSettingConstants.IS_EXPIRY_NO); LOG.info(
			 * "Inside Get Training Module notifications with  Clear all Flag Modified Date trainingModuleNotificationsBoOutput "
			 * + NotificationsBoOutput);
			 * 
			 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
			 * 
			 * tempBean = new TrainingModuleNotificationBean(); if
			 * (NotificationsBoOutput.get(i).getClearFlag()
			 * .equalsIgnoreCase("NO")) {
			 * tempBean.setPkTrainingNotificationId(NotificationsBoOutput
			 * .get(i).getPkNotificationId());
			 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
			 * tempBean.setNotificationMessage(NotificationsBoOutput
			 * .get(i).getNoificationMessage());
			 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
			 * .get(i).getNotificationcreatedDate());
			 * System.out.println(" date _______> 3" +
			 * NotificationsBoOutput.get(i) .getNotificationcreatedDate()); Date
			 * date = NotificationsBoOutput.get(i)
			 * .getNotificationcreatedDate(); String notificationcreatedDatedate
			 * = dateFormat .format(date);
			 * System.out.println("notificationcreatedDatedate" +
			 * notificationcreatedDatedate);
			 * System.out.println((dateFormat.format(date)));
			 * 
			 * tempBean.setNotificationCreatedDateString(notificationcreatedDatedate
			 * ); tempBean.setClearFlag(NotificationsBoOutput.get(i)
			 * .getClearFlag());
			 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
			 * .get(i).getClearFlagcreatedDate());
			 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
			 * .get(i).getClearFlagModifiedDate());
			 * 
			 * tempBean.setModule(NotificationsBoOutput.get(i)
			 * .getModuleName());
			 * tempBean.setModuleId(NotificationsBoOutput.get(i)
			 * .getMasMenu().getPkMenuId());
			 * tempBean.setMenuUrl(NotificationsBoOutput.get(i) .getMenuUrl());
			 * 
			 * trainingModuleNotificationsOutputBean.add(tempBean); }
			 * 
			 * } System.out.println("3" +
			 * trainingModuleNotificationsOutputBean.size());
			 * 
			 * } catch (Exception e) { throw new
			 * DataAccessException("Error while Retrieving : ", e); }
			 * 
			 * }
			 */
			// When it is disable below code will be executed
			/*
			 * if (notificationSettingoutput.getNotificationType()
			 * .equalsIgnoreCase("DISABLE")) { LOG.info(
			 * "Inside Get get All Module Notificationss with  disabled Date ");
			 * MasNotificationSettingBO masBo; // getting first notification
			 * created Date as a startdate from // table try {
			 * NotificationsBoOutput = notificationRepository
			 * .findByEmpIdOrderByEmpIdAsc(empId);
			 * System.out.println("NotificationsBoOutput" +
			 * NotificationsBoOutput);
			 * 
			 * }
			 * 
			 * catch (Exception e) { throw new
			 * DataAccessException("Error while Retrieving : ", e); } // getting
			 * disabled date as a enddate from table
			 * 
			 * try { masBo = masNotificationSettingRepository
			 * .findByFkEmployeeId(empId); System.out.println("masBo" + masBo);
			 * 
			 * }
			 * 
			 * catch (Exception e) { throw new
			 * DataAccessException("Error while Retrieving : ", e); }
			 * 
			 * Date startDate = NotificationsBoOutput.get(0)
			 * .getNotificationcreatedDate(); Date endDate =
			 * masBo.getDisabledDate(); LOG.info(
			 * " getNotificationcreatedDate is considered to  startDate ---------> "
			 * + startDate); LOG.info("disabled Date is from table ---------> "
			 * + endDate); try { NotificationsBoOutput = notificationRepository
			 * .
			 * findByEmpIdAndNotificationcreatedDateBetweenAndNoificationIsExpiry
			 * ( empId, startDate, endDate,
			 * NotificationSettingConstants.IS_EXPIRY_NO); LOG.info(
			 * "Inside Get Training Module notifications with  Clear all Flag Modified Date trainingModuleNotificationsBoOutput "
			 * + NotificationsBoOutput);
			 * 
			 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
			 * 
			 * tempBean = new TrainingModuleNotificationBean(); if
			 * (NotificationsBoOutput.get(i).getClearFlag()
			 * .equalsIgnoreCase("NO")) {
			 * tempBean.setPkTrainingNotificationId(NotificationsBoOutput
			 * .get(i).getPkNotificationId());
			 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
			 * tempBean.setNotificationMessage(NotificationsBoOutput
			 * .get(i).getNoificationMessage());
			 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
			 * .get(i).getNotificationcreatedDate());
			 * System.out.println(" date _______> 3" +
			 * NotificationsBoOutput.get(i) .getNotificationcreatedDate()); Date
			 * date = NotificationsBoOutput.get(i)
			 * .getNotificationcreatedDate(); String notificationcreatedDatedate
			 * = dateFormat .format(date);
			 * System.out.println("notificationcreatedDatedate" +
			 * notificationcreatedDatedate);
			 * System.out.println((dateFormat.format(date)));
			 * 
			 * tempBean.setNotificationCreatedDateString(notificationcreatedDatedate
			 * ); tempBean.setClearFlag(NotificationsBoOutput.get(i)
			 * .getClearFlag());
			 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
			 * .get(i).getClearFlagcreatedDate());
			 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
			 * .get(i).getClearFlagModifiedDate());
			 * 
			 * tempBean.setModule(NotificationsBoOutput.get(i)
			 * .getModuleName());
			 * tempBean.setModuleId(NotificationsBoOutput.get(i)
			 * .getMasMenu().getPkMenuId());
			 * tempBean.setMenuUrl(NotificationsBoOutput.get(i) .getMenuUrl());
			 * 
			 * trainingModuleNotificationsOutputBean.add(tempBean); }
			 * 
			 * } System.out.println("3" +
			 * trainingModuleNotificationsOutputBean.size());
			 * 
			 * } catch (Exception e) { throw new
			 * DataAccessException("Error while Retrieving : ", e); }
			 * 
			 * }
			 */
		}

		if (NotificationsBoOutput.size() > 0) {
			for (int i = 0; i < NotificationsBoOutput.size(); i++) {

				tempBean = new TrainingModuleNotificationBean();
				if (NotificationsBoOutput.get(i).getClearFlag()
						.equalsIgnoreCase("NO")) {
					tempBean.setPkTrainingNotificationId(NotificationsBoOutput
							.get(i).getPkNotificationId());
					tempBean.setEmpId(NotificationsBoOutput.get(i).getEmpId());
					tempBean.setNotificationMessage(NotificationsBoOutput
							.get(i).getNoificationMessage());

					Date date = NotificationsBoOutput.get(i)
							.getNotificationcreatedDate();
					
					String notificationcreatedDatedate = dateFormat
							.format(date);

					tempBean.setNotificationCreatedDateString(notificationcreatedDatedate);
					tempBean.setNotificationCreatedDate(date);
					tempBean.setClearFlag(NotificationsBoOutput.get(i)
							.getClearFlag());
					tempBean.setClearFlagCreatedDate(NotificationsBoOutput.get(
							i).getClearFlagcreatedDate());
					tempBean.setClearFlagModifiedDate(NotificationsBoOutput
							.get(i).getClearFlagModifiedDate());

					tempBean.setModule(NotificationsBoOutput.get(i)
							.getModuleName());
					tempBean.setModuleId(NotificationsBoOutput.get(i)
							.getMasMenu().getPkMenuId());
					tempBean.setMenuUrl(NotificationsBoOutput.get(i)
							.getMenuUrl());

					trainingModuleNotificationsOutputBean.add(tempBean);

				}
			}

		}
		
		class DateComparator implements
		Comparator<TrainingModuleNotificationBean>{
		  public int compare(TrainingModuleNotificationBean o1, TrainingModuleNotificationBean o2) { return
				  o2.getNotificationCreatedDateString().compareTo(o1.getNotificationCreatedDateString()); }
	  }
		LOG.info("################ trainingModuleNotificationsOutputBean:"+trainingModuleNotificationsOutputBean.size());
		if (trainingModuleNotificationsOutputBean.size() > 0) {
			for (TrainingModuleNotificationBean bean : trainingModuleNotificationsOutputBean) {
				LOG.info("NOtification id:"+bean.getPkTrainingNotificationId());
				// dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
				// Date date = bean.getNotificationCreatedDate();
				String dateString = bean.getNotificationCreatedDateString().split(" ")[0];
				/*
				 * TimeZone tz = TimeZone.getTimeZone("UTC"); SimpleDateFormat
				 * dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
				 * dateFormat.setTimeZone(tz);
				 */
				// System.out.println("GMT TIme"+(dateFormat.format(date)));

				if (!filterMap.containsKey(dateString)) {
					List<TrainingModuleNotificationBean> finalBean = new ArrayList<TrainingModuleNotificationBean>();
					finalBean.add(bean);
					filterMap.put(dateString, finalBean);

					// continue;
				} else if (filterMap.containsKey(dateString)) {
					List<TrainingModuleNotificationBean> finalBean = new ArrayList<TrainingModuleNotificationBean>();
					finalBean.add(bean);
					filterMap.get(dateString).addAll(finalBean);

				}
			}
		}
		
		
		
		  Map<String, List<TrainingModuleNotificationBean>> treeMap = new
		  TreeMap<String, List<TrainingModuleNotificationBean>>( new
		  Comparator<String>() {
		  
		  @Override public int compare(String o1, String o2) { return
		  o2.compareTo(o1); }
		  
		  }); treeMap.putAll(filterMap);
		  
		  
		  
		  /*for (Map.Entry<String, List<TrainingModuleNotificationBean>> entry : treeMap
					.entrySet()) {
			  
			  List<TrainingModuleNotificationBean> notificationList = entry.getValue();
			  
			//  if(notificationList.size() > 1){
				//entry.getValue().clear();
				//  Collections.sort(notificationList,new DateComparator());
				 entry.getValue().sort(new DateComparator());
				  treeMap.put(entry.getKey(),notificationList);
				  Collections.sort(notificationList,new DateComparator());
				  entry.setValue(notificationList);
				 // treeMap.get(entry.getKey()).addAll(notificationList);
				  
			  } else 
				  continue;
			  
		  }*/
		  
		 
		 
		// filteredOutputBeanByDate.add(treeMap);
		/*
		 * System.out.println("filteredOutputBeanByDate" +
		 * filteredOutputBeanByDate);
		 */
		/*Map<Date, List<TrainingModuleNotificationBean>> finalMap = new HashMap<Date, List<TrainingModuleNotificationBean>>();
		for (Map.Entry<String, List<TrainingModuleNotificationBean>> entry : filterMap
				.entrySet()) {
			Date date = null;
			try {
				date = dateFormat.parse(entry.getKey());
			} catch (ParseException e) {
				e.printStackTrace();
			}

			finalMap.put(date, entry.getValue());

		}*/

		/*Map<Date, List<TrainingModuleNotificationBean>> sorted = finalMap
				.entrySet()
				.stream()
				.sorted(Map.Entry.comparingByKey())
				.collect(
						Collectors.toMap(e -> e.getKey(), e -> e.getValue(), (
								e1, e2) -> e2, HashMap::new));

		filterMap.clear();
		for (Map.Entry<Date, List<TrainingModuleNotificationBean>> entry : sorted
				.entrySet()) {
			String date = null;
			date = dateFormat.format(entry.getKey());

			filterMap.put(date, entry.getValue());

		}*/

		filteredOutputBeanByDate.add(treeMap);

		return filteredOutputBeanByDate;

	}

	public List<Map> getIndividualModuleNotification(int empId,
			String moduleName) throws DataAccessException {
		Map<String, List<TrainingModuleNotificationBean>> filterMap = new HashMap<String, List<TrainingModuleNotificationBean>>();

		List<DatNotificationsBO> NotificationsBoOutput = new ArrayList<DatNotificationsBO>();
		List<Map> filteredOutputBeanByDate = new ArrayList<Map>();
		List<TrainingModuleNotificationBean> trainingModuleNotificationsOutputBean = new ArrayList<TrainingModuleNotificationBean>();
		List<Date> dateList = new ArrayList<Date>();
		MasNotificationSettingBO notificationSettingoutput;
		TrainingModuleNotificationBean tempBean;
		try {
			notificationSettingoutput = masNotificationSettingRepository
					.findByFkEmployeeId(empId);

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}
		LOG.info("getIndividualModuleNotification outside enable and No");

		if (notificationSettingoutput.getNotificationType().equalsIgnoreCase(
				"ENABLE")
				&& notificationSettingoutput.getClearAllFlag()
						.equalsIgnoreCase("NO")) {
			LOG.info("getIndividualModuleNotification inside enable and No");
			try {
				if (notificationSettingoutput.getDisabledDate() == null)
					NotificationsBoOutput = notificationRepository
							.findByEmpIdAndModuleNameAndNoificationIsExpiryOrderByNotificationcreatedDateDesc(
									empId, moduleName,
									NotificationSettingConstants.IS_EXPIRY_NO);
				else
					NotificationsBoOutput = notificationRepository
							.findByEmpIdAndModuleNameAndNotificationcreatedDateBetweenAndNoificationIsExpiryOrderByNotificationcreatedDateDesc(
									empId,
									moduleName,
									notificationSettingoutput.getDisabledDate(),
									new Date(),
									NotificationSettingConstants.IS_EXPIRY_NO);

				/*
				 * NotificationsBoOutput = notificationRepository
				 * .findByEmpIdAndModuleNameAndNoificationIsExpiry(empId,
				 * moduleName, NotificationSettingConstants.IS_EXPIRY_NO);
				 * LOG.info("trainingModuleNotificationsBoOutput" +
				 * NotificationsBoOutput);
				 * LOG.info("trainingModuleNotificationsBoOutput size" +
				 * NotificationsBoOutput.size());
				 * 
				 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
				 * System.out.println("1"); tempBean = new
				 * TrainingModuleNotificationBean(); if
				 * (NotificationsBoOutput.get(i).getClearFlag()
				 * .equalsIgnoreCase("NO")) {
				 * tempBean.setPkTrainingNotificationId(NotificationsBoOutput
				 * .get(i).getPkNotificationId());
				 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
				 * tempBean.setNotificationMessage(NotificationsBoOutput
				 * .get(i).getNoificationMessage());
				 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
				 * .get(i).getNotificationcreatedDate()); Date date =
				 * NotificationsBoOutput.get(i) .getNotificationcreatedDate();
				 * String notificationcreatedDatedate = dateFormat
				 * .format(date); tempBean.setNotificationCreatedDateString(
				 * notificationcreatedDatedate);
				 * 
				 * tempBean.setClearFlag(NotificationsBoOutput.get(i)
				 * .getClearFlag());
				 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
				 * .get(i).getClearFlagcreatedDate());
				 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
				 * .get(i).getClearFlagModifiedDate());
				 * 
				 * tempBean.setModule(NotificationsBoOutput.get(i)
				 * .getModuleName());
				 * tempBean.setModuleId(NotificationsBoOutput.get(i)
				 * .getMasMenu().getPkMenuId());
				 * tempBean.setMenuUrl(NotificationsBoOutput.get(i)
				 * .getMenuUrl());
				 * 
				 * trainingModuleNotificationsOutputBean.add(tempBean);
				 * LOG.info("trainingModuleNotificationsOutputBean inside*****"
				 * + trainingModuleNotificationsOutputBean); }
				 * 
				 * } System.out.println("***** size" +
				 * trainingModuleNotificationsOutputBean.size());
				 */} catch (Exception e) {
				throw new DataAccessException("Error while Retrieving : ", e);
			}
		}
		/*
		 * if
		 * (notificationSettingoutput.getClearAllFlag().equalsIgnoreCase("YES")
		 * && notificationSettingoutput.getClearAllModifiedDate() == null) {
		 * System.out.println("2"); Date startDate =
		 * notificationSettingoutput.getClearAllCreatedDate(); Date endDate =
		 * new Date(); LOG.info(
		 * "Inside Get Training Module notifications with  Clear all Flag created Date "
		 * ); try { NotificationsBoOutput = notificationRepository .
		 * findByEmpIdAndModuleNameAndNotificationcreatedDateBetweenAndNoificationIsExpiry
		 * ( empId, moduleName, startDate, endDate,
		 * NotificationSettingConstants.IS_EXPIRY_NO); LOG.info(
		 * "Inside Get Training Module notifications with  Clear all Flag created Date trainingModuleNotificationsBoOutput "
		 * + NotificationsBoOutput);
		 * 
		 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
		 * 
		 * tempBean = new TrainingModuleNotificationBean(); if
		 * (NotificationsBoOutput.get(i).getClearFlag() .equalsIgnoreCase("NO"))
		 * { tempBean.setPkTrainingNotificationId(NotificationsBoOutput
		 * .get(i).getPkNotificationId());
		 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
		 * tempBean.setNotificationMessage(NotificationsBoOutput
		 * .get(i).getNoificationMessage());
		 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
		 * .get(i).getNotificationcreatedDate()); Date date =
		 * NotificationsBoOutput.get(i) .getNotificationcreatedDate(); String
		 * notificationcreatedDatedate = dateFormat .format(date);
		 * tempBean.setNotificationCreatedDateString
		 * (notificationcreatedDatedate);
		 * tempBean.setClearFlag(NotificationsBoOutput.get(i) .getClearFlag());
		 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
		 * .get(i).getClearFlagcreatedDate());
		 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
		 * .get(i).getClearFlagModifiedDate());
		 * 
		 * tempBean.setModule(NotificationsBoOutput.get(i) .getModuleName());
		 * tempBean.setModuleId(NotificationsBoOutput.get(i)
		 * .getMasMenu().getPkMenuId());
		 * tempBean.setMenuUrl(NotificationsBoOutput.get(i) .getMenuUrl());
		 * 
		 * trainingModuleNotificationsOutputBean.add(tempBean); }
		 * 
		 * }
		 * 
		 * } catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); } }
		 */
		/*
		 * if
		 * (notificationSettingoutput.getClearAllFlag().equalsIgnoreCase("YES")
		 * && notificationSettingoutput.getClearAllModifiedDate() != null) {
		 * System.out.println("3"); Date startDate = notificationSettingoutput
		 * .getClearAllModifiedDate(); Date endDate = new Date();
		 * LOG.info("startDate" + startDate);
		 * 
		 * LOG.info("endDate" + endDate);
		 * 
		 * LOG.info(
		 * "Inside Get Training Module notifications with  Clear all Flag Modified Date "
		 * ); try { NotificationsBoOutput = notificationRepository .
		 * findByEmpIdAndModuleNameAndNotificationcreatedDateBetweenAndNoificationIsExpiry
		 * ( empId, moduleName, startDate, endDate,
		 * NotificationSettingConstants.IS_EXPIRY_NO); LOG.info(
		 * "Inside Get Training Module notifications with  Clear all Flag Modified Date trainingModuleNotificationsBoOutput "
		 * + NotificationsBoOutput);
		 * 
		 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
		 * 
		 * tempBean = new TrainingModuleNotificationBean(); if
		 * (NotificationsBoOutput.get(i).getClearFlag() .equalsIgnoreCase("NO"))
		 * { tempBean.setPkTrainingNotificationId(NotificationsBoOutput
		 * .get(i).getPkNotificationId());
		 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
		 * tempBean.setNotificationMessage(NotificationsBoOutput
		 * .get(i).getNoificationMessage());
		 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
		 * .get(i).getNotificationcreatedDate()); Date date =
		 * NotificationsBoOutput.get(i) .getNotificationcreatedDate(); String
		 * notificationcreatedDatedate = dateFormat .format(date);
		 * tempBean.setNotificationCreatedDateString
		 * (notificationcreatedDatedate);
		 * tempBean.setClearFlag(NotificationsBoOutput.get(i) .getClearFlag());
		 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
		 * .get(i).getClearFlagcreatedDate());
		 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
		 * .get(i).getClearFlagModifiedDate());
		 * 
		 * tempBean.setModule(NotificationsBoOutput.get(i) .getModuleName());
		 * tempBean.setModuleId(NotificationsBoOutput.get(i)
		 * .getMasMenu().getPkMenuId());
		 * tempBean.setMenuUrl(NotificationsBoOutput.get(i) .getMenuUrl());
		 * 
		 * trainingModuleNotificationsOutputBean.add(tempBean); }
		 * 
		 * }
		 * 
		 * } catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); }
		 * 
		 * }
		 */
		// When it is disable below code will be executed
		/*
		 * if (notificationSettingoutput.getNotificationType().equalsIgnoreCase(
		 * "DISABLE")) {
		 * LOG.info("Inside Get get All Module Notificationss with  disabled Date "
		 * ); MasNotificationSettingBO masBo; // getting first notification
		 * created Date as a startdate from table try { NotificationsBoOutput =
		 * notificationRepository .findByEmpIdOrderByEmpIdAsc(empId);
		 * System.out.println("NotificationsBoOutput" + NotificationsBoOutput);
		 * 
		 * }
		 * 
		 * catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); } // getting
		 * disabled date as a enddate from table
		 * 
		 * try { masBo = masNotificationSettingRepository
		 * .findByFkEmployeeId(empId); System.out.println("masBo" + masBo);
		 * 
		 * }
		 * 
		 * catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); }
		 * 
		 * Date startDate = NotificationsBoOutput.get(0)
		 * .getNotificationcreatedDate(); Date endDate =
		 * masBo.getDisabledDate(); LOG.info(
		 * " getNotificationcreatedDate is considered to  startDate ---------> "
		 * + startDate); LOG.info("disabled Date is from table ---------> " +
		 * endDate); try { NotificationsBoOutput = notificationRepository
		 * .findByEmpIdAndNotificationcreatedDateBetween(empId, startDate,
		 * endDate); LOG.info(
		 * "Inside Get  notifications with  disabled DateNotificationsBoOutput "
		 * + NotificationsBoOutput);
		 * 
		 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
		 * 
		 * tempBean = new TrainingModuleNotificationBean(); if
		 * (NotificationsBoOutput.get(i).getClearFlag() .equalsIgnoreCase("NO"))
		 * { tempBean.setPkTrainingNotificationId(NotificationsBoOutput
		 * .get(i).getPkNotificationId());
		 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
		 * tempBean.setNotificationMessage(NotificationsBoOutput
		 * .get(i).getNoificationMessage());
		 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
		 * .get(i).getNotificationcreatedDate());
		 * System.out.println(" date _______> 3" + NotificationsBoOutput.get(i)
		 * .getNotificationcreatedDate()); Date date =
		 * NotificationsBoOutput.get(i) .getNotificationcreatedDate(); String
		 * notificationcreatedDatedate = dateFormat .format(date);
		 * System.out.println("notificationcreatedDatedate" +
		 * notificationcreatedDatedate);
		 * System.out.println((dateFormat.format(date)));
		 * 
		 * tempBean.setNotificationCreatedDateString(notificationcreatedDatedate)
		 * ; tempBean.setClearFlag(NotificationsBoOutput.get(i)
		 * .getClearFlag());
		 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
		 * .get(i).getClearFlagcreatedDate());
		 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
		 * .get(i).getClearFlagModifiedDate());
		 * 
		 * tempBean.setModule(NotificationsBoOutput.get(i) .getModuleName());
		 * tempBean.setModuleId(NotificationsBoOutput.get(i)
		 * .getMasMenu().getPkMenuId());
		 * tempBean.setMenuUrl(NotificationsBoOutput.get(i) .getMenuUrl());
		 * 
		 * trainingModuleNotificationsOutputBean.add(tempBean); }
		 * 
		 * } System.out.println("3" +
		 * trainingModuleNotificationsOutputBean.size());
		 * 
		 * } catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); }
		 * 
		 * }
		 */
		if (notificationSettingoutput.getNotificationType().equalsIgnoreCase(
				"ENABLE")
				&& notificationSettingoutput.getClearAllFlag()
						.equalsIgnoreCase("YES")) {
			try {
				if (notificationSettingoutput.getClearAllCreatedDate() != null)
					dateList.add(notificationSettingoutput
							.getClearAllCreatedDate());
				if (notificationSettingoutput.getClearAllModifiedDate() != null)
					dateList.add(notificationSettingoutput
							.getClearAllModifiedDate());
				if (notificationSettingoutput.getDisabledDate() != null)
					dateList.add(notificationSettingoutput.getDisabledDate());

				Collections.sort(dateList);
				Collections.reverse(dateList);

				NotificationsBoOutput = notificationRepository
						.findByEmpIdAndModuleNameAndNotificationcreatedDateBetweenAndNoificationIsExpiryOrderByNotificationcreatedDateDesc(
								empId, moduleName, dateList.get(0), new Date(),
								NotificationSettingConstants.IS_EXPIRY_NO);

			} catch (Exception e) {
				throw new DataAccessException("Error while Retrieving : ", e);
			}
		}
		if (notificationSettingoutput.getNotificationType()
				.equalsIgnoreCase("DISABLE")) {
			try {
				NotificationsBoOutput = notificationRepository
						.findByEmpIdAndModuleNameAndNotificationcreatedDateLessThanEqualAndNoificationIsExpiryOrderByNotificationcreatedDateDesc(
								empId,moduleName,
								notificationSettingoutput.getDisabledDate(),
								NotificationSettingConstants.IS_EXPIRY_NO);
				dateList.clear();
				if (notificationSettingoutput.getClearAllFlag()
						.equalsIgnoreCase("YES")) {
					if (notificationSettingoutput.getClearAllCreatedDate() != null)
						dateList.add(notificationSettingoutput
								.getClearAllCreatedDate());
					if (notificationSettingoutput.getClearAllModifiedDate() != null)
						dateList.add(notificationSettingoutput
								.getClearAllModifiedDate());

					Collections.sort(dateList);
					Collections.reverse(dateList);

					if (dateList.get(0).getTime() > notificationSettingoutput
							.getDisabledDate().getTime()) {
						NotificationsBoOutput.clear();
					}
				}
			} catch (Exception e) {
				throw new DataAccessException("Error while Retrieving : ",
						e);
			}
		}
		LOG.info("trainingModuleNotificationsOutputBean size *****"
				+ trainingModuleNotificationsOutputBean.size());

		if (NotificationsBoOutput.size() > 0) {
			for (int i = 0; i < NotificationsBoOutput.size(); i++) {

				tempBean = new TrainingModuleNotificationBean();
				if (NotificationsBoOutput.get(i).getClearFlag()
						.equalsIgnoreCase("NO")) {
					tempBean.setPkTrainingNotificationId(NotificationsBoOutput
							.get(i).getPkNotificationId());
					tempBean.setEmpId(NotificationsBoOutput.get(i).getEmpId());
					tempBean.setNotificationMessage(NotificationsBoOutput
							.get(i).getNoificationMessage());

					Date date = NotificationsBoOutput.get(i)
							.getNotificationcreatedDate();
					String notificationcreatedDatedate = dateFormat
							.format(date).split(" ")[0];

					tempBean.setNotificationCreatedDateString(notificationcreatedDatedate);
					tempBean.setNotificationCreatedDate(date);
					tempBean.setClearFlag(NotificationsBoOutput.get(i)
							.getClearFlag());
					tempBean.setClearFlagCreatedDate(NotificationsBoOutput.get(
							i).getClearFlagcreatedDate());
					tempBean.setClearFlagModifiedDate(NotificationsBoOutput
							.get(i).getClearFlagModifiedDate());

					tempBean.setModule(NotificationsBoOutput.get(i)
							.getModuleName());
					tempBean.setModuleId(NotificationsBoOutput.get(i)
							.getMasMenu().getPkMenuId());
					tempBean.setMenuUrl(NotificationsBoOutput.get(i)
							.getMenuUrl());

					trainingModuleNotificationsOutputBean.add(tempBean);

				}
			}

		}

		if (trainingModuleNotificationsOutputBean.size() > 0) {
			for (TrainingModuleNotificationBean bean : trainingModuleNotificationsOutputBean) {

				String dateString = bean.getNotificationCreatedDateString();

				if (!filterMap.containsKey(dateString)) {
					List<TrainingModuleNotificationBean> finalBean = new ArrayList<TrainingModuleNotificationBean>();
					finalBean.add(bean);

					filterMap.put(dateString, finalBean);

					// continue;
				} else if (filterMap.containsKey(dateString)) {
					List<TrainingModuleNotificationBean> finalBean = new ArrayList<TrainingModuleNotificationBean>();
					finalBean.add(bean);
					filterMap.get(dateString).addAll(finalBean);

				}
			}
		}
		
		 Map<String, List<TrainingModuleNotificationBean>> treeMap = new
		 TreeMap<String, List<TrainingModuleNotificationBean>>( new
		  Comparator<String>() {
		  
		  @Override public int compare(String o1, String o2) { return
		  o2.compareTo(o1); }
		  
		  }); treeMap.putAll(filterMap);
		 
		/*
		 * filteredOutputBeanByDate.add(treeMap);
		 * System.out.println("filteredOutputBeanByDate" +
		 * filteredOutputBeanByDate);
		 */

		/*Map<Date, List<TrainingModuleNotificationBean>> finalMap = new HashMap<Date, List<TrainingModuleNotificationBean>>();
		for (Map.Entry<String, List<TrainingModuleNotificationBean>> entry : filterMap
				.entrySet()) {
			Date date = null;
			try {
				date = dateFormat.parse(entry.getKey());
			} catch (ParseException e) {
				e.printStackTrace();
			}

			finalMap.put(date, entry.getValue());

		}*/

		/*Map<Date, List<TrainingModuleNotificationBean>> sorted = finalMap
				.entrySet()
				.stream()
				.sorted(Map.Entry.comparingByKey())
				.collect(
						Collectors.toMap(e -> e.getKey(), e -> e.getValue(), (
								e1, e2) -> e2, HashMap::new));

		filterMap.clear();
		for (Map.Entry<Date, List<TrainingModuleNotificationBean>> entry : sorted
				.entrySet()) {
			String date = null;
			date = dateFormat.format(entry.getKey());
			filterMap.put(date, entry.getValue());
		}*/
		filteredOutputBeanByDate.add(treeMap);
		return filteredOutputBeanByDate;
	}

	public int getAllModuleNotificationsPreviousCount(int empId)
			throws DataAccessException {
		MasNotificationSettingBO notificationSettingoutput;
		int previousCount;
		try {
			notificationSettingoutput = masNotificationSettingRepository
					.findByFkEmployeeId(empId);

			previousCount = notificationSettingoutput.getPreviousCount();
		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}
		return previousCount;
	}

	public List getAllModuleNotificationsCount(int empId)
			throws DataAccessException {
		List<TrainingModuleNotificationBean> trainingModuleNotificationsOutputBean = new ArrayList<TrainingModuleNotificationBean>();
		List<DatNotificationsBO> NotificationsBoOutput = new ArrayList<DatNotificationsBO>();
		List<Date> dateList = new ArrayList<Date>();
		MasNotificationSettingBO notificationSettingoutput;
		TrainingModuleNotificationBean tempBean;
		try {
			notificationSettingoutput = masNotificationSettingRepository
					.findByFkEmployeeId(empId);

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}
		if (notificationSettingoutput.getNotificationType().equalsIgnoreCase(
				"ENABLE")
				&& notificationSettingoutput.getClearAllFlag()
						.equalsIgnoreCase("NO")) {
			try {

				if (notificationSettingoutput.getDisabledDate() == null)
					NotificationsBoOutput = notificationRepository
							.findByEmpIdAndNoificationIsExpiry(empId,
									NotificationSettingConstants.IS_EXPIRY_NO);
				else
					NotificationsBoOutput = notificationRepository
							.findByEmpIdAndNotificationcreatedDateBetweenAndNoificationIsExpiry(
									empId,
									notificationSettingoutput.getDisabledDate(),
									new Date(),
									NotificationSettingConstants.IS_EXPIRY_NO);
				/*
				 * NotificationsBoOutput = notificationRepository
				 * .findByEmpIdAndNoificationIsExpiry(empId,
				 * NotificationSettingConstants.IS_EXPIRY_NO);
				 * 
				 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
				 * 
				 * tempBean = new TrainingModuleNotificationBean(); if
				 * (NotificationsBoOutput.get(i).getClearFlag()
				 * .equalsIgnoreCase("NO")) {
				 * tempBean.setPkTrainingNotificationId(NotificationsBoOutput
				 * .get(i).getPkNotificationId());
				 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
				 * tempBean.setNotificationMessage(NotificationsBoOutput
				 * .get(i).getNoificationMessage());
				 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
				 * .get(i).getNotificationcreatedDate());
				 * tempBean.setClearFlag(NotificationsBoOutput.get(i)
				 * .getClearFlag());
				 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
				 * .get(i).getClearFlagcreatedDate());
				 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
				 * .get(i).getClearFlagModifiedDate());
				 * 
				 * tempBean.setModule(NotificationsBoOutput.get(i)
				 * .getModuleName());
				 * 
				 * trainingModuleNotificationsOutputBean.add(tempBean); }
				 * 
				 * }
				 */} catch (Exception e) {
				throw new DataAccessException("Error while Retrieving : ", e);
			}
		}
		/*
		 * if (notificationSettingoutput.getNotificationType().equalsIgnoreCase(
		 * "ENABLE") && notificationSettingoutput.getClearAllFlag()
		 * .equalsIgnoreCase("YES") &&
		 * notificationSettingoutput.getClearAllModifiedDate() == null) {
		 * 
		 * Date startDate = notificationSettingoutput.getClearAllCreatedDate();
		 * Date endDate = new Date(); LOG.info(
		 * "Inside Get Training Module notifications with  Clear all Flag created Date "
		 * ); try { NotificationsBoOutput = notificationRepository
		 * .findByEmpIdAndNotificationcreatedDateBetweenAndNoificationIsExpiry(
		 * empId, startDate, endDate,
		 * NotificationSettingConstants.IS_EXPIRY_NO); LOG.info(
		 * "Inside Get Training Module notifications with  Clear all Flag created Date trainingModuleNotificationsBoOutput "
		 * + NotificationsBoOutput);
		 * 
		 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
		 * 
		 * tempBean = new TrainingModuleNotificationBean(); if
		 * (NotificationsBoOutput.get(i).getClearFlag() .equalsIgnoreCase("NO"))
		 * { tempBean.setPkTrainingNotificationId(NotificationsBoOutput
		 * .get(i).getPkNotificationId());
		 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
		 * tempBean.setNotificationMessage(NotificationsBoOutput
		 * .get(i).getNoificationMessage());
		 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
		 * .get(i).getNotificationcreatedDate());
		 * tempBean.setClearFlag(NotificationsBoOutput.get(i) .getClearFlag());
		 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
		 * .get(i).getClearFlagcreatedDate());
		 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
		 * .get(i).getClearFlagModifiedDate());
		 * 
		 * tempBean.setModule(NotificationsBoOutput.get(i) .getModuleName());
		 * 
		 * trainingModuleNotificationsOutputBean.add(tempBean); }
		 * 
		 * }
		 * 
		 * System.out.println("2" +
		 * trainingModuleNotificationsOutputBean.size());
		 * 
		 * } catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); } }
		 */
		/*
		 * if (notificationSettingoutput.getNotificationType().equalsIgnoreCase(
		 * "ENABLE") && notificationSettingoutput.getClearAllFlag()
		 * .equalsIgnoreCase("YES") &&
		 * notificationSettingoutput.getClearAllModifiedDate() != null) {
		 * 
		 * Date startDate = notificationSettingoutput
		 * .getClearAllModifiedDate(); Date endDate = new Date(); LOG.info(
		 * "Inside Get Training Module notifications with  Clear all Flag Modified Date "
		 * ); try { NotificationsBoOutput = notificationRepository
		 * .findByEmpIdAndNotificationcreatedDateBetweenAndNoificationIsExpiry(
		 * empId, startDate, endDate,
		 * NotificationSettingConstants.IS_EXPIRY_NO); LOG.info(
		 * "Inside Get Training Module notifications with  Clear all Flag Modified Date trainingModuleNotificationsBoOutput "
		 * + NotificationsBoOutput);
		 * 
		 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
		 * 
		 * tempBean = new TrainingModuleNotificationBean(); if
		 * (NotificationsBoOutput.get(i).getClearFlag() .equalsIgnoreCase("NO"))
		 * { tempBean.setPkTrainingNotificationId(NotificationsBoOutput
		 * .get(i).getPkNotificationId());
		 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
		 * tempBean.setNotificationMessage(NotificationsBoOutput
		 * .get(i).getNoificationMessage());
		 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
		 * .get(i).getNotificationcreatedDate());
		 * tempBean.setClearFlag(NotificationsBoOutput.get(i) .getClearFlag());
		 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
		 * .get(i).getClearFlagcreatedDate());
		 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
		 * .get(i).getClearFlagModifiedDate());
		 * 
		 * tempBean.setModule(NotificationsBoOutput.get(i) .getModuleName());
		 * 
		 * trainingModuleNotificationsOutputBean.add(tempBean); }
		 * 
		 * } System.out.println("3" +
		 * trainingModuleNotificationsOutputBean.size());
		 * 
		 * } catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); }
		 * 
		 * }
		 */
		// When it is disable below code will be executed
		/*
		 * if (notificationSettingoutput.getNotificationType().equalsIgnoreCase(
		 * "DISABLE")) {
		 * LOG.info("Inside Get get All Module Notificationss with  disabled Date "
		 * ); // getting first notification created Date as a startdate from
		 * table try { NotificationsBoOutput = notificationRepository
		 * .findByEmpIdOrderByEmpIdAsc(empId);
		 * System.out.println("NotificationsBoOutput" + NotificationsBoOutput);
		 * 
		 * }
		 * 
		 * catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); }
		 * MasNotificationSettingBO masBo; // getting disabled date as a enddate
		 * from table
		 * 
		 * try { masBo = masNotificationSettingRepository
		 * .findByFkEmployeeId(empId); System.out.println("masBo" + masBo);
		 * 
		 * }
		 * 
		 * catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); }
		 * 
		 * Date startDate = NotificationsBoOutput.get(0)
		 * .getNotificationcreatedDate(); Date endDate =
		 * masBo.getDisabledDate(); LOG.info(
		 * " getNotificationcreatedDate is considered to  startDate ---------> "
		 * + startDate); LOG.info("disabled Date is from table ---------> " +
		 * endDate); try { NotificationsBoOutput = notificationRepository
		 * .findByEmpIdAndNotificationcreatedDateBetweenAndNoificationIsExpiry(
		 * empId, startDate, endDate,
		 * NotificationSettingConstants.IS_EXPIRY_NO); LOG.info(
		 * "Inside Get Training Module notifications with  Clear all Flag Modified Date trainingModuleNotificationsBoOutput "
		 * + NotificationsBoOutput);
		 * 
		 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
		 * 
		 * tempBean = new TrainingModuleNotificationBean(); if
		 * (NotificationsBoOutput.get(i).getClearFlag() .equalsIgnoreCase("NO"))
		 * { tempBean.setPkTrainingNotificationId(NotificationsBoOutput
		 * .get(i).getPkNotificationId());
		 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
		 * tempBean.setNotificationMessage(NotificationsBoOutput
		 * .get(i).getNoificationMessage());
		 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
		 * .get(i).getNotificationcreatedDate());
		 * System.out.println(" date _______> 3" + NotificationsBoOutput.get(i)
		 * .getNotificationcreatedDate()); Date date =
		 * NotificationsBoOutput.get(i) .getNotificationcreatedDate(); String
		 * notificationcreatedDatedate = dateFormat .format(date);
		 * System.out.println("notificationcreatedDatedate" +
		 * notificationcreatedDatedate);
		 * System.out.println((dateFormat.format(date)));
		 * 
		 * tempBean.setNotificationCreatedDateString(notificationcreatedDatedate)
		 * ; tempBean.setClearFlag(NotificationsBoOutput.get(i)
		 * .getClearFlag());
		 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
		 * .get(i).getClearFlagcreatedDate());
		 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
		 * .get(i).getClearFlagModifiedDate());
		 * 
		 * tempBean.setModule(NotificationsBoOutput.get(i) .getModuleName());
		 * tempBean.setModuleId(NotificationsBoOutput.get(i)
		 * .getMasMenu().getPkMenuId());
		 * tempBean.setMenuUrl(NotificationsBoOutput.get(i) .getMenuUrl());
		 * 
		 * trainingModuleNotificationsOutputBean.add(tempBean); }
		 * 
		 * } System.out.println("3" +
		 * trainingModuleNotificationsOutputBean.size());
		 * 
		 * } catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); }
		 * 
		 * }
		 */

		if (notificationSettingoutput.getNotificationType().equalsIgnoreCase(
				"ENABLE")
				&& notificationSettingoutput.getClearAllFlag()
						.equalsIgnoreCase("YES")) {
			try {
				if (notificationSettingoutput.getClearAllCreatedDate() != null)
					dateList.add(notificationSettingoutput
							.getClearAllCreatedDate());
				if (notificationSettingoutput.getClearAllModifiedDate() != null)
					dateList.add(notificationSettingoutput
							.getClearAllModifiedDate());
				if (notificationSettingoutput.getDisabledDate() != null)
					dateList.add(notificationSettingoutput.getDisabledDate());

				Collections.sort(dateList);
				Collections.reverse(dateList);
				NotificationsBoOutput = notificationRepository
						.findByEmpIdAndNotificationcreatedDateBetweenAndNoificationIsExpiry(
								empId, dateList.get(0), new Date(),
								NotificationSettingConstants.IS_EXPIRY_NO);

			} catch (Exception e) {
				throw new DataAccessException("Error while Retrieving : ", e);
			}
		}

		if (NotificationsBoOutput.size() > 0) {
			for (int i = 0; i < NotificationsBoOutput.size(); i++) {

				tempBean = new TrainingModuleNotificationBean();
				if (NotificationsBoOutput.get(i).getClearFlag()
						.equalsIgnoreCase("NO")) {
					tempBean.setPkTrainingNotificationId(NotificationsBoOutput
							.get(i).getPkNotificationId());
					tempBean.setEmpId(NotificationsBoOutput.get(i).getEmpId());
					tempBean.setNotificationMessage(NotificationsBoOutput
							.get(i).getNoificationMessage());

					Date date = NotificationsBoOutput.get(i)
							.getNotificationcreatedDate();
					String notificationcreatedDatedate = dateFormat
							.format(date);

					tempBean.setNotificationCreatedDateString(notificationcreatedDatedate);
					tempBean.setNotificationCreatedDate(date);
					tempBean.setClearFlag(NotificationsBoOutput.get(i)
							.getClearFlag());
					tempBean.setClearFlagCreatedDate(NotificationsBoOutput.get(
							i).getClearFlagcreatedDate());
					tempBean.setClearFlagModifiedDate(NotificationsBoOutput
							.get(i).getClearFlagModifiedDate());

					tempBean.setModule(NotificationsBoOutput.get(i)
							.getModuleName());
					tempBean.setModuleId(NotificationsBoOutput.get(i)
							.getMasMenu().getPkMenuId());
					tempBean.setMenuUrl(NotificationsBoOutput.get(i)
							.getMenuUrl());

					trainingModuleNotificationsOutputBean.add(tempBean);

				}
			}

		}

		return trainingModuleNotificationsOutputBean;

	}

	public List<TrainingModuleNotificationBean> getIndiviualModuleNotificationsCount(
			int empId, String moduleName) throws DataAccessException {
		List<TrainingModuleNotificationBean> trainingModuleNotificationsOutputBean = new ArrayList<TrainingModuleNotificationBean>();
		List<DatNotificationsBO> NotificationsBoOutput = new ArrayList<DatNotificationsBO>();
		List<Date> dateList = new ArrayList<Date>();
		MasNotificationSettingBO notificationSettingoutput;
		TrainingModuleNotificationBean tempBean;
		try {
			notificationSettingoutput = masNotificationSettingRepository
					.findByFkEmployeeId(empId);

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}
		if (notificationSettingoutput.getNotificationType().equalsIgnoreCase(
				"ENABLE")
				&& notificationSettingoutput.getClearAllFlag()
						.equalsIgnoreCase("NO")) {
			try {

				if (notificationSettingoutput.getDisabledDate() == null)
					NotificationsBoOutput = notificationRepository
							.findByEmpIdAndModuleNameAndNoificationIsExpiry(
									empId, moduleName,
									NotificationSettingConstants.IS_EXPIRY_NO);
				else
					NotificationsBoOutput = notificationRepository
							.findByEmpIdAndModuleNameAndNotificationcreatedDateBetweenAndNoificationIsExpiry(
									empId,
									moduleName,
									notificationSettingoutput.getDisabledDate(),
									new Date(),
									NotificationSettingConstants.IS_EXPIRY_NO);
				/*
				 * NotificationsBoOutput = notificationRepository
				 * .findByEmpIdAndModuleNameAndNoificationIsExpiry(empId,
				 * moduleName, NotificationSettingConstants.IS_EXPIRY_NO);
				 * 
				 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
				 * 
				 * tempBean = new TrainingModuleNotificationBean(); if
				 * (NotificationsBoOutput.get(i).getClearFlag()
				 * .equalsIgnoreCase("NO")) {
				 * tempBean.setPkTrainingNotificationId(NotificationsBoOutput
				 * .get(i).getPkNotificationId());
				 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
				 * tempBean.setNotificationMessage(NotificationsBoOutput
				 * .get(i).getNoificationMessage());
				 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
				 * .get(i).getNotificationcreatedDate());
				 * tempBean.setClearFlag(NotificationsBoOutput.get(i)
				 * .getClearFlag());
				 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
				 * .get(i).getClearFlagcreatedDate());
				 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
				 * .get(i).getClearFlagModifiedDate());
				 * trainingModuleNotificationsOutputBean.add(tempBean); }
				 * 
				 * }
				 */
			} catch (Exception e) {
				throw new DataAccessException("Error while Retrieving : ", e);
			}
		}
		/*
		 * if
		 * (notificationSettingoutput.getClearAllFlag().equalsIgnoreCase("YES")
		 * && notificationSettingoutput.getClearAllModifiedDate() == null) {
		 * 
		 * Date startDate = notificationSettingoutput.getClearAllCreatedDate();
		 * Date endDate = new Date(); LOG.info(
		 * "Inside Get Training Module notifications with  Clear all Flag created Date "
		 * ); try { NotificationsBoOutput = notificationRepository .
		 * findByEmpIdAndModuleNameAndNotificationcreatedDateBetweenAndNoificationIsExpiry
		 * ( empId, moduleName, startDate, endDate,
		 * NotificationSettingConstants.IS_EXPIRY_NO); LOG.info(
		 * "Inside Get Training Module notifications with  Clear all Flag created Date trainingModuleNotificationsBoOutput "
		 * + NotificationsBoOutput);
		 * 
		 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
		 * 
		 * tempBean = new TrainingModuleNotificationBean(); if
		 * (NotificationsBoOutput.get(i).getClearFlag() .equalsIgnoreCase("NO"))
		 * { tempBean.setPkTrainingNotificationId(NotificationsBoOutput
		 * .get(i).getPkNotificationId());
		 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
		 * tempBean.setNotificationMessage(NotificationsBoOutput
		 * .get(i).getNoificationMessage());
		 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
		 * .get(i).getNotificationcreatedDate());
		 * tempBean.setClearFlag(NotificationsBoOutput.get(i) .getClearFlag());
		 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
		 * .get(i).getClearFlagcreatedDate());
		 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
		 * .get(i).getClearFlagModifiedDate());
		 * trainingModuleNotificationsOutputBean.add(tempBean); }
		 * 
		 * }
		 * 
		 * } catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); } }
		 */
		/*
		 * if
		 * (notificationSettingoutput.getClearAllFlag().equalsIgnoreCase("YES")
		 * && notificationSettingoutput.getClearAllModifiedDate() != null) {
		 * 
		 * Date startDate = notificationSettingoutput
		 * .getClearAllModifiedDate(); Date endDate = new Date(); LOG.info(
		 * "Inside Get Training Module notifications with  Clear all Flag Modified Date "
		 * ); try { NotificationsBoOutput = notificationRepository .
		 * findByEmpIdAndModuleNameAndNotificationcreatedDateBetweenAndNoificationIsExpiry
		 * ( empId, moduleName, startDate, endDate,
		 * NotificationSettingConstants.IS_EXPIRY_NO); LOG.info(
		 * "Inside Get Training Module notifications with  Clear all Flag Modified Date trainingModuleNotificationsBoOutput "
		 * + NotificationsBoOutput);
		 * 
		 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
		 * 
		 * tempBean = new TrainingModuleNotificationBean(); if
		 * (NotificationsBoOutput.get(i).getClearFlag() .equalsIgnoreCase("NO"))
		 * { tempBean.setPkTrainingNotificationId(NotificationsBoOutput
		 * .get(i).getPkNotificationId());
		 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
		 * tempBean.setNotificationMessage(NotificationsBoOutput
		 * .get(i).getNoificationMessage());
		 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
		 * .get(i).getNotificationcreatedDate());
		 * tempBean.setClearFlag(NotificationsBoOutput.get(i) .getClearFlag());
		 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
		 * .get(i).getClearFlagcreatedDate());
		 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
		 * .get(i).getClearFlagModifiedDate());
		 * trainingModuleNotificationsOutputBean.add(tempBean); }
		 * 
		 * }
		 * 
		 * } catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); }
		 * 
		 * }
		 */
		// When it is disable below code will be executed
		/*
		 * if (notificationSettingoutput.getNotificationType().equalsIgnoreCase(
		 * "DISABLE")) {
		 * LOG.info("Inside Get get All Module Notificationss with  disabled Date "
		 * ); // getting first notification created Date as a startdate from
		 * table try { NotificationsBoOutput = notificationRepository
		 * .findByEmpIdOrderByEmpIdAsc(empId);
		 * System.out.println("NotificationsBoOutput" + NotificationsBoOutput);
		 * 
		 * }
		 * 
		 * catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); }
		 * MasNotificationSettingBO masBo; // getting disabled date as a enddate
		 * from table
		 * 
		 * try { masBo = masNotificationSettingRepository
		 * .findByFkEmployeeId(empId); System.out.println("masBo" + masBo);
		 * 
		 * }
		 * 
		 * catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); }
		 * 
		 * Date startDate = NotificationsBoOutput.get(0)
		 * .getNotificationcreatedDate(); Date endDate =
		 * masBo.getDisabledDate(); LOG.info(
		 * " getNotificationcreatedDate is considered to  startDate ---------> "
		 * + startDate); LOG.info("disabled Date is from table ---------> " +
		 * endDate); try { NotificationsBoOutput = notificationRepository
		 * .findByEmpIdAndNotificationcreatedDateBetweenAndNoificationIsExpiry(
		 * empId, startDate, endDate,
		 * NotificationSettingConstants.IS_EXPIRY_NO); LOG.info(
		 * "Inside Get Training Module notifications with  Clear all Flag Modified Date trainingModuleNotificationsBoOutput "
		 * + NotificationsBoOutput);
		 * 
		 * for (int i = 0; i < NotificationsBoOutput.size(); i++) {
		 * 
		 * tempBean = new TrainingModuleNotificationBean(); if
		 * (NotificationsBoOutput.get(i).getClearFlag() .equalsIgnoreCase("NO"))
		 * { tempBean.setPkTrainingNotificationId(NotificationsBoOutput
		 * .get(i).getPkNotificationId());
		 * tempBean.setEmpId(NotificationsBoOutput.get(i) .getEmpId());
		 * tempBean.setNotificationMessage(NotificationsBoOutput
		 * .get(i).getNoificationMessage());
		 * tempBean.setNotificationCreatedDate(NotificationsBoOutput
		 * .get(i).getNotificationcreatedDate());
		 * System.out.println(" date _______> 3" + NotificationsBoOutput.get(i)
		 * .getNotificationcreatedDate()); Date date =
		 * NotificationsBoOutput.get(i) .getNotificationcreatedDate(); String
		 * notificationcreatedDatedate = dateFormat .format(date);
		 * System.out.println("notificationcreatedDatedate" +
		 * notificationcreatedDatedate);
		 * System.out.println((dateFormat.format(date)));
		 * 
		 * tempBean.setNotificationCreatedDateString(notificationcreatedDatedate)
		 * ; tempBean.setClearFlag(NotificationsBoOutput.get(i)
		 * .getClearFlag());
		 * tempBean.setClearFlagCreatedDate(NotificationsBoOutput
		 * .get(i).getClearFlagcreatedDate());
		 * tempBean.setClearFlagModifiedDate(NotificationsBoOutput
		 * .get(i).getClearFlagModifiedDate());
		 * 
		 * tempBean.setModule(NotificationsBoOutput.get(i) .getModuleName());
		 * tempBean.setModuleId(NotificationsBoOutput.get(i)
		 * .getMasMenu().getPkMenuId());
		 * tempBean.setMenuUrl(NotificationsBoOutput.get(i) .getMenuUrl());
		 * 
		 * trainingModuleNotificationsOutputBean.add(tempBean); }
		 * 
		 * } System.out.println("3" +
		 * trainingModuleNotificationsOutputBean.size());
		 * 
		 * } catch (Exception e) { throw new
		 * DataAccessException("Error while Retrieving : ", e); }
		 * 
		 * }
		 */
		if (notificationSettingoutput.getNotificationType().equalsIgnoreCase(
				"ENABLE")
				&& notificationSettingoutput.getClearAllFlag()
						.equalsIgnoreCase("YES")) {
			try {
				if (notificationSettingoutput.getClearAllCreatedDate() != null)
					dateList.add(notificationSettingoutput
							.getClearAllCreatedDate());
				if (notificationSettingoutput.getClearAllModifiedDate() != null)
					dateList.add(notificationSettingoutput
							.getClearAllModifiedDate());
				if (notificationSettingoutput.getDisabledDate() != null)
					dateList.add(notificationSettingoutput.getDisabledDate());

				Collections.sort(dateList);
				Collections.reverse(dateList);

				NotificationsBoOutput = notificationRepository
						.findByEmpIdAndModuleNameAndNotificationcreatedDateBetweenAndNoificationIsExpiry(
								empId, moduleName, dateList.get(0), new Date(),
								NotificationSettingConstants.IS_EXPIRY_NO);

			} catch (Exception e) {
				throw new DataAccessException("Error while Retrieving : ", e);
			}
		}

		if (NotificationsBoOutput.size() > 0) {
			for (int i = 0; i < NotificationsBoOutput.size(); i++) {

				tempBean = new TrainingModuleNotificationBean();
				if (NotificationsBoOutput.get(i).getClearFlag()
						.equalsIgnoreCase("NO")) {
					tempBean.setPkTrainingNotificationId(NotificationsBoOutput
							.get(i).getPkNotificationId());
					tempBean.setEmpId(NotificationsBoOutput.get(i).getEmpId());
					tempBean.setNotificationMessage(NotificationsBoOutput
							.get(i).getNoificationMessage());

					Date date = NotificationsBoOutput.get(i)
							.getNotificationcreatedDate();
					String notificationcreatedDatedate = dateFormat
							.format(date);

					tempBean.setNotificationCreatedDateString(notificationcreatedDatedate);
					tempBean.setNotificationCreatedDate(date);
					tempBean.setClearFlag(NotificationsBoOutput.get(i)
							.getClearFlag());
					tempBean.setClearFlagCreatedDate(NotificationsBoOutput.get(
							i).getClearFlagcreatedDate());
					tempBean.setClearFlagModifiedDate(NotificationsBoOutput
							.get(i).getClearFlagModifiedDate());

					tempBean.setModule(NotificationsBoOutput.get(i)
							.getModuleName());
					tempBean.setModuleId(NotificationsBoOutput.get(i)
							.getMasMenu().getPkMenuId());
					tempBean.setMenuUrl(NotificationsBoOutput.get(i)
							.getMenuUrl());

					trainingModuleNotificationsOutputBean.add(tempBean);

				}
			}

		}
		return trainingModuleNotificationsOutputBean;

	}

	public MasNotificationSettingBO insertNotificationPreviousCount(int empId,
			int previousCount) throws DataAccessException {
		MasNotificationSettingBO masNotificationSettingBO;
		try {
			masNotificationSettingBO = masNotificationSettingRepository
					.findByFkEmployeeId(empId);
			if (masNotificationSettingBO != null) {
				masNotificationSettingBO.setPreviousCount(previousCount);
				masNotificationSettingRepository.save(masNotificationSettingBO);
			}

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to sve to notificationSetting : ", e);

		}
		return masNotificationSettingBO;
	}
}
