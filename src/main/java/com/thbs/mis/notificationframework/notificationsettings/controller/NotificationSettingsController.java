package com.thbs.mis.notificationframework.notificationsettings.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.notificationsettings.bean.NotificationCountBean;
import com.thbs.mis.notificationframework.notificationsettings.bean.NotificationSettingBean;
import com.thbs.mis.notificationframework.notificationsettings.bo.DatNotificationsBO;
import com.thbs.mis.notificationframework.notificationsettings.bo.MasNotificationSettingBO;
import com.thbs.mis.notificationframework.notificationsettings.constants.NotificationSettingsURIConstants;
import com.thbs.mis.notificationframework.notificationsettings.service.NotificationSettingsService;
import com.thbs.mis.notificationframework.trainingnotification.bean.TrainingModuleNotificationBean;

@Controller
public class NotificationSettingsController {


	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(NotificationSettingsController.class);

	@Autowired
private NotificationSettingsService notificationSettingsService;
	
/*	Fetch the notifications for all modules by emp id;
*/	
	@RequestMapping(value = NotificationSettingsURIConstants.GET_ALL_MODULE_NOTFICATIONS, method = RequestMethod.POST)
	public @ResponseBody List<Map>  getALlModuleNotifications(
			@RequestBody NotificationSettingBean notificationSettingBean)
			throws BusinessException, DataAccessException, Exception {
		List outputBean;
		LOG.startUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen ");
		outputBean = notificationSettingsService
				.getAllModuleNotifications(notificationSettingBean
						.getEmpId());

		LOG.endUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen"
				+ outputBean);

		return outputBean;

	}
	
	/*	Fetch the notifications for individual  modules by emp id and module name;
	*/
	@RequestMapping(value = NotificationSettingsURIConstants.GET_INDIVIDUAL_MODULE_NOTIFICATION, method = RequestMethod.POST)
	public @ResponseBody List<Map> getIndividualModuleNotification(
			@RequestBody NotificationSettingBean notificationBean)
			throws BusinessException, DataAccessException, Exception {

		LOG.startUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen ");
		List outputBean;
		LOG.startUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen ");
		outputBean = notificationSettingsService
				.getIndividualModuleNotification(notificationBean.getEmpId(),
						notificationBean.getModuleName());
		LOG.endUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen"
				+ outputBean);

		return outputBean;

	}
	/*	Count  the notifications for all modules
	*/
	@RequestMapping(value = NotificationSettingsURIConstants.GET_ALL_MODULE_NOTFICATIONS_COUNT, method = RequestMethod.POST)
	public @ResponseBody NotificationCountBean  getALlModuleNotificationsCount(
			@RequestBody NotificationSettingBean notificationSettingBean)
			throws BusinessException, DataAccessException, Exception {
		NotificationCountBean notificationCountBean= new NotificationCountBean();
		List outputBean;
		int currentCount;
		int previousCount;
		String outputForCount;
		MasNotificationSettingBO notificationSettingoutput;

		LOG.startUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen ");
		outputBean = notificationSettingsService
				.getAllModuleNotificationsCount(notificationSettingBean
						.getEmpId());
		previousCount=notificationSettingsService.getAllModuleNotificationsPreviousCount(notificationSettingBean
						.getEmpId());
		LOG.endUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen"
				+ outputBean);
		
		notificationCountBean.setCurrentCount(outputBean.size());
		notificationCountBean.setPreviousCount(previousCount);
		return notificationCountBean;

	}
	/*	Count  the notifications for Individual modules
	*/
	@RequestMapping(value = NotificationSettingsURIConstants.GET_INDIVIDUAL_MODULE_NOTFICATIONS_COUNT, method = RequestMethod.POST)
	public @ResponseBody String  getIndividualModuleNotificationsCount(
			@RequestBody NotificationSettingBean notificationSettingBean)
			throws BusinessException, DataAccessException, Exception {
		List<TrainingModuleNotificationBean>  outputBean;
		int count;
		String outputForCount;
		LOG.startUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen ");
		outputBean = notificationSettingsService
				.getIndiviualModuleNotificationsCount(notificationSettingBean
						.getEmpId(),notificationSettingBean.getModuleName());

		LOG.endUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen"
				+ outputBean);
count=outputBean.size();
outputForCount=String.valueOf(count);
		return outputForCount;

	}
	@RequestMapping(value = NotificationSettingsURIConstants.NOTIFICATION_TYPE_SETTINGS_CREATE, method = RequestMethod.POST)
		public @ResponseBody MasNotificationSettingBO notificationSettingCreate(
				@RequestBody NotificationSettingBean notificationSettingBean)
				throws BusinessException, DataAccessException, Exception {
		MasNotificationSettingBO outputBean;
			LOG.startUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen ");
			outputBean = notificationSettingsService
					.createnotificationSetting(notificationSettingBean
							.getEmpId() );

			LOG.endUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen"+outputBean);

			return outputBean;

		}
	
	@RequestMapping(value = NotificationSettingsURIConstants.NOTIFICATION_TYPE_SETTINGS_UPDATE, method = RequestMethod.PUT)
	public @ResponseBody List<MasNotificationSettingBO> notificationSettingUpdate(
			@RequestBody NotificationSettingBean notificationSettingBean)
			throws BusinessException, DataAccessException, Exception {
	List<MasNotificationSettingBO> outputBean;
		LOG.startUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen ");
		outputBean = notificationSettingsService
				.updateNotificationSetting(notificationSettingBean
						.getEmpId(),notificationSettingBean.getSettingType() );

		LOG.endUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen"
				+ outputBean);

		return outputBean;

	}
	
	@RequestMapping(value = NotificationSettingsURIConstants.NOTIFICATION_TYPE_SETTINGS_STATUS, method = RequestMethod.POST)
	public @ResponseBody NotificationSettingBean notificationSettingStatus(
			@RequestBody NotificationSettingBean notificationSettingBean)
			throws BusinessException, DataAccessException, Exception {
		NotificationSettingBean output = new NotificationSettingBean();
		//MasNotificationSettingBO outputBean;
		LOG.startUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen ");
		output = notificationSettingsService
				.notificationSettingStatus(notificationSettingBean
						.getEmpId() );

		LOG.endUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen"
				+ 		output);

		return 		output;

	}

	@RequestMapping(value = NotificationSettingsURIConstants.CLEAR_ALL_FLAG_CREATE, method = RequestMethod.PUT)
	public @ResponseBody MasNotificationSettingBO clearAllFlagCreateAndUpdate(
			@RequestBody NotificationSettingBean notificationSettingBean)
			throws BusinessException, DataAccessException, Exception {
	MasNotificationSettingBO outputBean;
		LOG.startUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen ");
		outputBean = notificationSettingsService
				.clearAllFlagCreateAndUpdate(notificationSettingBean
						.getEmpId());

		LOG.endUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen"
				+ outputBean);

		return outputBean;

	}
	
	@RequestMapping(value = NotificationSettingsURIConstants.DELETE_NOTIFICATION, method = RequestMethod.POST)
	public @ResponseBody String deleteNotificationByClearFlag(
			@RequestBody NotificationSettingBean notificationSettingBean)
			throws BusinessException, DataAccessException, Exception {
	DatNotificationsBO outputBean;
		LOG.startUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen ");
		outputBean = notificationSettingsService
				.deleteNotificationByClearFlag(notificationSettingBean.getPkNotificationId());

		LOG.endUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen"
				+ outputBean);

		return "Success";

	}
	@RequestMapping(value = NotificationSettingsURIConstants.INSERT_COUNT, method = RequestMethod.POST)
	public @ResponseBody String insertNotificationPreviousCount(
			@RequestBody NotificationSettingBean notificationSettingBean)
			throws BusinessException, DataAccessException, Exception {
	MasNotificationSettingBO masNotificationSettingBO;
		LOG.startUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen ");
		masNotificationSettingBO = notificationSettingsService
				.insertNotificationPreviousCount(notificationSettingBean.getEmpId(),notificationSettingBean.getPreviousCount() );

		LOG.endUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen"
				+ masNotificationSettingBO);

		return "Success";

	}
	
}
