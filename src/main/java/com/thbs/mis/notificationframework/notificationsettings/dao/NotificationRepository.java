package com.thbs.mis.notificationframework.notificationsettings.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.notificationframework.notificationsettings.bo.DatNotificationsBO;

@Repository
public interface NotificationRepository extends
		GenericRepository<DatNotificationsBO, Integer> {

	public List<DatNotificationsBO> findByEmpIdAndModuleNameAndNoificationIsExpiry(
			int empId, String moduleName, String isExpiry);

	public List<DatNotificationsBO> findByEmpId(int empId);

	public List<DatNotificationsBO> findByEmpIdAndNoificationIsExpiryOrderByNotificationcreatedDateDesc(
			int empId, String isExpiry);
	
	public List<DatNotificationsBO> findByEmpIdAndNoificationIsExpiry(
			int empId, String isExpiry);

	public List<DatNotificationsBO> findByEmpIdAndModuleNameAndNotificationcreatedDateBetweenAndNoificationIsExpiry(
			int empId, String moduleName, Date startDate, Date endDate,
			String isExpiry);

	public List<DatNotificationsBO> findByEmpIdAndNotificationcreatedDateBetween(
			int empId, Date startDate, Date endDate);

	public List<DatNotificationsBO> findByEmpIdAndNotificationcreatedDateBetweenAndNoificationIsExpiry(
			int empId, Date startDate, Date endDate, String isExpiry);

	public List<DatNotificationsBO> findByEmpIdAndNotificationcreatedDateLessThanEqualAndNoificationIsExpiry(
			int empId, Date createdDate, String isExpiry);

	public DatNotificationsBO findByEmpIdAndNoificationMessage(int empId,
			String msg);

	public DatNotificationsBO findByPkNotificationId(int notificationId);

	public List<DatNotificationsBO> findByEmpIdOrderByEmpIdAsc(int empId);

	public DatNotificationsBO findByEmpIdAndNoificationMessageIdentifier(
			int empId, String msgIdentifier);

	@Query("SELECT n.pkNotificationId from DatNotificationsBO n where n.empId = ? and n.menuUrl = ?")
	public List<Integer> getNotificationIdByEmpIdAndMenuURL(int empId,
			String menuURL);

	@Transactional
	@Modifying
	@Query("UPDATE DatNotificationsBO n SET n.noificationIsExpiry = 'EXPIRED' where n.pkNotificationId IN (:notificationIdList)")
	public int updateNotificationIsExpiryByNotificationIdList(
			@Param("notificationIdList") List<Integer> notificationIdList);

	@Transactional
	@Modifying
	@Query("UPDATE DatNotificationsBO n SET n.noificationIsExpiry = 'EXPIRED' where n.empId = :empId AND n.menuUrl = :menuUrl AND n.noificationMessageIdentifier NOT IN(:notificationIdentifierList)")
	public int updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
			@Param("notificationIdentifierList") List<String> notificationIdentifierList,
			@Param("empId") int empId, @Param("menuUrl") String menuUrl);

	@Query("SELECT n FROM DatNotificationsBO n where n.empId = :empId AND n.fkModuleId in :moduleIdList AND"
			+ " n.clearFlagcreatedDate >=(SELECT s.lastAccessedDate from MasNotificationSettingBO s where s.fkEmployeeId = :empId)")
	public List<DatNotificationsBO> getPushNotificationsForEmpByModuleId(
			@Param("empId") int empId,
			@Param("moduleIdList") List<Short> moduleIdList);
	
	public List<DatNotificationsBO> findByEmpIdAndNotificationcreatedDateLessThanEqualAndNoificationIsExpiryOrderByNotificationcreatedDateDesc(
			int empId, Date createdDate, String isExpiry);
	
	public List<DatNotificationsBO> findByEmpIdAndNotificationcreatedDateBetweenAndNoificationIsExpiryOrderByNotificationcreatedDateDesc(
			int empId, Date startDate, Date endDate, String isExpiry);
	
	public List<DatNotificationsBO> findByEmpIdAndModuleNameAndNoificationIsExpiryOrderByNotificationcreatedDateDesc(
			int empId, String moduleName, String isExpiry);
	
	public List<DatNotificationsBO> findByEmpIdAndModuleNameAndNotificationcreatedDateBetweenAndNoificationIsExpiryOrderByNotificationcreatedDateDesc(
			int empId, String moduleName, Date startDate, Date endDate,
			String isExpiry);
	
	public List<DatNotificationsBO> findByEmpIdAndModuleNameAndNotificationcreatedDateLessThanEqualAndNoificationIsExpiryOrderByNotificationcreatedDateDesc(
			int empId, String moduleName, Date createdDate, String isExpiry);
	

}
