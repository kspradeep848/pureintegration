package com.thbs.mis.notificationframework.projectnotification.constants;


public class ProjectNotificationConstants {

	public static final String MODULE_NAME = "PROJECT";
	public static final Short MENU_ID = 63;
	public static final String IS_EXPIRY_NO = "NOTEXPIRED";
	public final static String CLEAR_FLAG_NO = "NO";
	
	public static final String FIN_ROLES = "FINANCE_HEAD,FINANCE_MANAGER,FINANCE_SENIOR_EXECUTIVE";

}
