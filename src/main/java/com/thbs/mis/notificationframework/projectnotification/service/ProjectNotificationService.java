package com.thbs.mis.notificationframework.projectnotification.service;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.gsrnotification.service.GSRNotificationService;
import com.thbs.mis.notificationframework.notificationsettings.bo.DatNotificationsBO;
import com.thbs.mis.notificationframework.notificationsettings.dao.NotificationRepository;
import com.thbs.mis.notificationframework.projectnotification.constants.ProjectNotificationConstants;
import com.thbs.mis.project.bo.DatMilestoneBO;
import com.thbs.mis.project.bo.DatSowUploadHistoryBO;
import com.thbs.mis.project.bo.DatComponentBO;
import com.thbs.mis.project.bo.DatWorkorderBO;
import com.thbs.mis.project.bo.MasClientBO;
import com.thbs.mis.project.bo.MasProjectBO;
import com.thbs.mis.project.constants.ProjectConstants;
import com.thbs.mis.project.dao.DatWorkorderRepository;
import com.thbs.mis.project.dao.DatMilestoneRepository;
import com.thbs.mis.project.dao.DatSowUploadHistoryRepository;
import com.thbs.mis.project.dao.MasClientRepository;
import com.thbs.mis.project.dao.ProjDatProjectMappedToProjectCoordinatorRepository;

@Service
public class ProjectNotificationService {
	/*
	 * Individual Notification Messages from ClientMessages.properties file
	 */

	@Autowired
	private GSRNotificationService gsrNotificationService;

	@Autowired
	private NotificationRepository notificationRepository;
	
	@Value("${ui.message.clientCreationToFinanceIndividual}")
	private String clientCreationToFin;

	@Value("${ui.message.clientUpdationToFinanceIndividual}")
	private String clientUpdationToFin;

	@Value("${ui.message.projectCreationToPC/DM/DLIndividual}")
	private String projectCreationToDmDlPc;

	@Value("${ui.message.projectUpdationToPC/DM/DLIndividual}")
	private String projectUpdationToDmDlPc;

	@Value("${ui.message.workOrderCreationToPCIndividual}")
	private String workOrderCreationToPCIndividual;

	@Value("${ui.message.workOrderUpdationToPCIndividual}")
	private String workOrderUpdationToPCIndividual;

	@Value("${ui.message.componentCreationToPCIndividual}")
	private String componentCreationToPCIndividual;

	@Value("${ui.message.componentUpdationToPCIndividual}")
	private String componentUpdationToPCIndividual;

	@Value("${ui.message.sowAmountUpdationUpdationToDM/DLIndividual}")
	private String sowAmountUpdationUpdationToDMDL;

	@Value("${ui.message.clientBudgetCreationToPCIndividual}")
	private String clientBudgetCreationToPCIndividual;

	@Value("${ui.message.clientBudgetUpdationToPCIndividual}")
	private String clientBudgetUpdationToPCIndividual;

	@Value("${ui.message.thbsBudgetCreationToPCIndividual}")
	private String thbsBudgetCreationToPCIndividual;

	@Value("${ui.message.thbsBudgetUpdationToPCIndividual}")
	private String thbsBudgetUpdationToPCIndividual;
	
	@Value("${ui.message.workOrderStatusUpdateToPCDMDL}")
	private String workOrderStatusUpdateToPCDMDL;
	
	@Value("${ui.message.milestoneCreationToPCIndividual}")
	private String milestoneCreationToPCIndividual;

	@Value("${ui.message.milestoneUpdationToPCIndividual}")
	private String milestoneUpdationToPCIndividual;

	@Value("${ui.message.internalComponentClosureToAllPc}")
	private String internalComponentClosureToAllPc;

	@Value("${ui.message.fixedTnMComponentClosureToAllPc}")
	private String fixedTnMComponentClosureToAllPc;

	
	/*
	 * Project Notification Keys from notificationframework.properties file
	 */

	@Value("${PRJ_CLI_CRE_FIN}")
	private String clientCreationKey;

	@Value("${PRJ_CLI_UPD_FIN}")
	private String clientUpdationKey;

	@Value("${PRJ_CRE_PC_DM_DL}")
	private String projectCreationKey;

	@Value("${PRJ_UPD_PC_DM_DL}")
	private String projectUpdationKey; 
	
	@Value("${PRJ_COMP_CLS_PC}")
	private String compClosureKey;

	@Value("${PRJ_WO_CRE_PC}")
	private String workorderCreationKey;

	@Value("${PRJ_WO_UPD_PC}")
	private String workorderUpdationKey;

	@Value("${PRJ_CLI_BUDGET_CRE_PC}")
	private String clientBudgetCreationKey;

	@Value("${PRJ_CLI_BDGT_UPD_PC}")
	private String clientBudgetUpdationKey;

	@Value("${PRJ_THBS_BDGT_CRE_PC}")
	private String thbsBudgetCreationKey;

	@Value("${PRJ_THBS_BDGT_UPD_PC}")
	private String thbsBudgetUpdationKey;

	@Value("${PRJ_SOW_AMT_UPD_DM_DL}")
	private String sowAmountUpdationKey;

	@Value("${PRJ_MILE_CRE_PC}")
	private String milestoneCreationKey;
	
	@Value("${PRJ_MILE_UPD_PC}")
	private String milestoneUpdationKey;
	
	@Value("${PRJ_COMP_CRE_PC}")
	private String componentCreationKey;

	@Value("${PRJ_COMP_UPD_PC}")
	private String componentUpdationKey;
	
	@Value("${PRJ_WO_STAT_PC_DM_DL}")
	private String woStatusUpdationKey;




	@Autowired
	private ProjDatProjectMappedToProjectCoordinatorRepository projDatProjectMappedToProjectCoordinatorRepository;
	
	@Autowired
	private DatMilestoneRepository milestoneRepository;
	
	@Autowired
	private DatWorkorderRepository datWorkorderRepository;
	
	@Autowired
	private DatSowUploadHistoryRepository datSowUploadHistoryRepository;
	
	@Autowired
	private MasClientRepository masClientRepository;
	
	@Autowired
	private EmpDetailRepository empDetailRepository;
	
	@Autowired
	private EmployeeProfessionalRepository employeeProfessionalRepository;
	
	private static final AppLog LOG = LogFactory
			.getLog(ProjectNotificationService.class);

	// Added By Rinta
	/**
	 * <Description sendClientCreatedNotificationToFinance> This method is used
	 * to create notification for finance team after adding a client
	 * 
	 * @param clientId
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	
	
	public void sendClientCreatedNotificationToFinance(Short clientId)
			throws DataAccessException, CommonCustomException {
		LOG.startUsecase("Entering into sendClientCreatedNotificationToFinance() service");
		String msgIdentifierKey = clientCreationKey.split(",")[2];
		List<DatNotificationsBO> datNotificationsBOList = new ArrayList<DatNotificationsBO>();
		Set<Integer> finSet = new HashSet<Integer>();
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBO;
		Date today = new Date();
		MasClientBO masClientBO;

		try {
			masClientBO = masClientRepository.findByPkClientId(clientId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Some error occured while adding client.");
		}

		if (masClientBO != null) {
			Integer empId = masClientBO.getCreatedBy();

			DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
			String createdDate = dateFormatter.format(masClientBO
					.getCreatedOn());
			DateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
			String createdtime = timeFormatter.format(masClientBO
					.getCreatedOn());

			String notificationMessage = MessageFormat.format(
					clientCreationToFin,
					gsrNotificationService.getEmployeeNameByEmpId(empId)
							+ " - " + empId, masClientBO.getClientName(),
					createdDate, createdtime);

			List<String> finRoleNames = Arrays
					.asList(ProjectNotificationConstants.FIN_ROLES.split(","));
			try {
				datEmpProfessionalDetailBO = employeeProfessionalRepository
						.findByFkMainEmpDetailId(empId);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException(
						"Some error occured while fetching getails of loggedInUser!!");
			}

			if (datEmpProfessionalDetailBO != null) {
				finSet.add(empId);
				try {
					finSet.addAll(empDetailRepository
							.getAllEmpIdByRoleKeyNames(finRoleNames,
									datEmpProfessionalDetailBO
											.getFkEmpOrganizationId()));
				} catch (Exception e) {
					// e.printStackTrace();
					throw new DataAccessException("unable to send notification");
				}
				finSet.remove(empId);

				for (Integer finId : finSet) {

					DatNotificationsBO notificationDetails = new DatNotificationsBO();

					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + "_" + finId + "_"
							+ masClientBO.getPkClientId();

					notificationDetails.setEmpId(finId);
					notificationDetails
							.setNoificationMessage(notificationMessage);
					notificationDetails
							.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);
					notificationDetails.setNotificationcreatedDate(masClientBO
							.getCreatedOn());
					notificationDetails.setClearFlagModifiedDate(today);
					notificationDetails.setClearFlagcreatedDate(today);
					notificationDetails
							.setModuleName(ProjectNotificationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(ProjectNotificationConstants.MENU_ID);
					notificationDetails
							.setMenuUrl(clientCreationKey.split(",")[0]);
					notificationDetails.setIsAction(clientCreationKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(ProjectNotificationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(clientCreationKey.split(",")[3]));

					datNotificationsBOList.add(notificationDetails);
				}

				if (datNotificationsBOList != null
						&& !datNotificationsBOList.isEmpty()) {
					try {
						notificationRepository.save(datNotificationsBOList);
					} catch (Exception e) {
						// e.printStackTrace();
						throw new DataAccessException("Failed to Save : ", e);
					}
				}

			} else
				throw new CommonCustomException(
						"Error occured while fetching details about loggedInUser.");
		} else
			throw new CommonCustomException(
					"Error occured while adding client.");

		LOG.endUsecase("Exiting from sendClientCreatedNotificationToFinance() service");
	}

	/**
	 * <Description sendClientUpdatedNotificationToFinance> This method is used
	 * to create notification for finance team after updating client details
	 * 
	 * @param clientId
	 * @param clientName
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	public void sendClientUpdatedNotificationToFinance(Short clientId,
			String clientName) throws DataAccessException,
			CommonCustomException {
		LOG.startUsecase("Entering into sendClientUpdatedNotificationToFinance() service");
		String msgIdentifierKey = clientUpdationKey.split(",")[2];
		List<DatNotificationsBO> datNotificationsBOList = new ArrayList<DatNotificationsBO>();
		Set<Integer> finSet = new HashSet<Integer>();
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBO;
		DatNotificationsBO isDuplicatesFound;
		Date today = new Date();
		MasClientBO masClientBO;
		
		System.out.println("clientId : "+clientId);
		
		try {
			masClientBO = masClientRepository.findByPkClientId(clientId);
		} catch (Exception e) {
			 e.printStackTrace();
			throw new DataAccessException(
					"Some error occured while updating client.");
		}

		System.out.println(masClientBO);

		if (masClientBO != null) {
			Integer empId = masClientBO.getCreatedBy();

			DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
			String createdDate = dateFormatter.format(masClientBO
					.getCreatedOn());
			DateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
			String createdtime = timeFormatter.format(masClientBO
					.getCreatedOn());

			String notificationMessage = MessageFormat.format(
					clientUpdationToFin,
					gsrNotificationService.getEmployeeNameByEmpId(empId)
							+ " - " + empId, clientName, createdDate,
					createdtime);

			List<String> finRoleNames = Arrays
					.asList(ProjectNotificationConstants.FIN_ROLES.split(","));
			try {
				datEmpProfessionalDetailBO = employeeProfessionalRepository
						.findByFkMainEmpDetailId(empId);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException(
						"Some error occured while fetching details of loggedInUser!!");
			}

			if (datEmpProfessionalDetailBO != null) {
				finSet.add(empId);
				try {
					finSet.addAll(empDetailRepository
							.getAllEmpIdByRoleKeyNames(finRoleNames,
									datEmpProfessionalDetailBO
											.getFkEmpOrganizationId()));
				} catch (Exception e) {
					// e.printStackTrace();
					throw new DataAccessException("Unable to send notification");
				}
				finSet.remove(empId);

				for (Integer finId : finSet) {

					DatNotificationsBO notificationDetails = new DatNotificationsBO();

					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + "_" + finId + "_"
							+ masClientBO.getPkClientId();

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							finId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {

						notificationDetails.setEmpId(finId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(masClientBO
										.getCreatedOn());
						notificationDetails.setClearFlagModifiedDate(today);
						notificationDetails.setClearFlagcreatedDate(today);
						notificationDetails
								.setModuleName(ProjectNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ProjectNotificationConstants.MENU_ID);
						notificationDetails.setMenuUrl(clientUpdationKey
								.split(",")[0]);
						notificationDetails.setIsAction(clientUpdationKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ProjectNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(clientUpdationKey.split(",")[3]));

						datNotificationsBOList.add(notificationDetails);

					} else {

						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound.setNotificationModifiedDate(today);

						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= today.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(today);

						datNotificationsBOList.add(isDuplicatesFound);
					}
				}

				if (datNotificationsBOList != null
						&& !datNotificationsBOList.isEmpty()) {
					System.out.println(datNotificationsBOList);
					try {
						notificationRepository.save(datNotificationsBOList);
					} catch (Exception e) {
						e.printStackTrace();
						throw new DataAccessException("Failed to Save : ", e);
					}
				}
			} else
				throw new CommonCustomException(
						"Error occured while fetching details about loggedInUser.");
		} else
			throw new CommonCustomException(
					"Error occured while updating client.");

		LOG.endUsecase("Exiting from sendClientUpdatedNotificationToFinance() service");
	}

	// EOA By Rinta
	
	
	public void sendProjectCreatedNotificationToDMDLPC(MasProjectBO masProjectBO) throws DataAccessException
	{
		LOG.startUsecase("Entering into sendProjectCreatedNotificationToDMDLPC() service");
		String msgIdentifierKey = projectCreationKey.split(",")[2];
		List<DatNotificationsBO> datNotificationsBOList = new ArrayList<DatNotificationsBO>();
		Set<Integer> pcSet = new HashSet<Integer>();
		Date today = new Date();

		if (masProjectBO != null) {
			Integer empId = masProjectBO.getCreatedBy();
			String notificationMessage = MessageFormat.format(
					projectCreationToDmDlPc,
					gsrNotificationService.getEmployeeNameByEmpId(empId)
							+ " - " + empId, masProjectBO.getProjectName(),
					masProjectBO.getCreatedOn());

			pcSet.add(empId);
			try {
				pcSet = projDatProjectMappedToProjectCoordinatorRepository
						.getByProjectId(masProjectBO.getPkProjectId());
			} catch (Exception e) {
			//	e.printStackTrace();
				throw new DataAccessException("unable to send notification");
			}
			pcSet.add(masProjectBO.getFkDeliveryMgrId());
			pcSet.add(masProjectBO.getFkDomainMgrId());
			pcSet.remove(empId);

			for (Integer pcId : pcSet) {

				DatNotificationsBO notificationDetails = new DatNotificationsBO();

				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId
						+ "_" + pcId + "_" + masProjectBO.getPkProjectId();

				notificationDetails.setEmpId(pcId);
				notificationDetails.setNoificationMessage(notificationMessage);
				notificationDetails
						.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);
				notificationDetails.setNotificationcreatedDate(masProjectBO
						.getCreatedOn());
				notificationDetails.setClearFlagModifiedDate(today);
				notificationDetails.setClearFlagcreatedDate(today);
				notificationDetails
						.setModuleName(ProjectNotificationConstants.MODULE_NAME);
				notificationDetails
						.setFkModuleId(ProjectNotificationConstants.MENU_ID);
				notificationDetails
						.setMenuUrl(projectCreationKey.split(",")[0]);
				notificationDetails
						.setIsAction(projectCreationKey.split(",")[1]);
				notificationDetails
						.setNoificationMessageIdentifier(fullmsgIdentifierKey);
				notificationDetails
						.setNoificationIsExpiry(ProjectNotificationConstants.IS_EXPIRY_NO);
				notificationDetails.setNotificationExpiryTime(Integer
						.parseInt(projectCreationKey.split(",")[3]));

				datNotificationsBOList.add(notificationDetails);
			}
			try {
				notificationRepository.save(datNotificationsBOList);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException("Failed to Save : ", e);
			}
		}
		LOG.endUsecase("Exiting from sendProjectCreatedNotificationToDMDLPC() service");

	}

	public void sendProjectUpdatedNotificationToDMDLPC(MasProjectBO masProjectBO)
			throws DataAccessException {
		LOG.startUsecase("Entering into sendProjectUpdatedNotificationToDMDLPC() service");
		String msgIdentifierKey = projectUpdationKey.split(",")[2];
		List<DatNotificationsBO> datNotificationsBOList = new ArrayList<DatNotificationsBO>();
		Set<Integer> pcSet = new HashSet<Integer>();
		Date today = new Date();
		DatNotificationsBO isDuplicatesFound = null;

		if (masProjectBO != null) {
			Integer empId = masProjectBO.getUpdatedBy();
			String notificationMessage = MessageFormat.format(
					projectUpdationToDmDlPc,
					gsrNotificationService.getEmployeeNameByEmpId(empId)
							+ " - " + empId, masProjectBO.getProjectName(),
					masProjectBO.getUpdatedOn());

			pcSet.add(empId);
			try {
				pcSet.addAll(projDatProjectMappedToProjectCoordinatorRepository
						.getByProjectId(masProjectBO.getPkProjectId()));
			} catch (Exception e) {
			//	e.printStackTrace();
				throw new DataAccessException("unable to send notification");
			}
			pcSet.add(masProjectBO.getFkDeliveryMgrId());
			pcSet.add(masProjectBO.getFkDomainMgrId());
			pcSet.remove(empId);

			for (Integer pcId : pcSet) {

				DatNotificationsBO notificationDetails = new DatNotificationsBO();

				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId
						+ "_" + pcId + "_" + masProjectBO.getPkProjectId();

				isDuplicatesFound = gsrNotificationService.checkDuplicates(
						pcId, fullmsgIdentifierKey);
				if (isDuplicatesFound == null) {

					notificationDetails.setEmpId(pcId);
					notificationDetails
							.setNoificationMessage(notificationMessage);
					notificationDetails
							.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);
					notificationDetails.setNotificationcreatedDate(masProjectBO
							.getUpdatedOn());
					notificationDetails.setClearFlagModifiedDate(today);
					notificationDetails.setClearFlagcreatedDate(today);
					notificationDetails
							.setModuleName(ProjectNotificationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(ProjectNotificationConstants.MENU_ID);
					notificationDetails.setMenuUrl(projectUpdationKey
							.split(",")[0]);
					notificationDetails.setIsAction(projectUpdationKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(ProjectNotificationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(projectUpdationKey.split(",")[3]));

					datNotificationsBOList.add(notificationDetails);

				} else {

					isDuplicatesFound
							.setNoificationMessage(notificationMessage);
					isDuplicatesFound.setNotificationModifiedDate(new Date());

					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= masProjectBO.getUpdatedOn().getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound
								.setNotificationcreatedDate(masProjectBO
										.getUpdatedOn());

					datNotificationsBOList.add(isDuplicatesFound);

				}
			}

			try {
				notificationRepository.save(datNotificationsBOList);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException("Failed to Save : ", e);
			}

		}
		LOG.endUsecase("Exiting from sendProjectCreatedNotificationToDMDLPC() service");

	}

	public void sendWorkorderCreatedNotificationToAllPC(
			DatWorkorderBO datWorkorderBO) throws DataAccessException {
		LOG.startUsecase("Entering into sendWorkorderCreatedNotificationToDMDLPC() service");
		String msgIdentifierKey = workorderCreationKey.split(",")[2];
		List<DatNotificationsBO> datNotificationsBOList = new ArrayList<DatNotificationsBO>();
		Set<Integer> pcSet = new HashSet<Integer>();
		Date today = new Date();

		if (datWorkorderBO != null) {
			Integer empId = datWorkorderBO.getWorkorderCreatedBy();
			String notificationMessage = MessageFormat.format(
					workOrderCreationToPCIndividual,
					gsrNotificationService.getEmployeeNameByEmpId(empId)
							+ " - " + empId,
					datWorkorderBO.getWorkorderNumber(),
					datWorkorderBO.getWorkorderCreatedOn());

			pcSet.add(empId);
			try {
				pcSet.addAll(projDatProjectMappedToProjectCoordinatorRepository
						.getAllPcRelatedToProject(datWorkorderBO
								.getPkWorkorderId()));
			} catch (Exception e) {
			//	e.printStackTrace();
				throw new DataAccessException("unable to send notification");
			}
			pcSet.remove(empId);

			for (Integer pcId : pcSet) {

				DatNotificationsBO notificationDetails = new DatNotificationsBO();

				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId
						+ "_" + pcId + "_" + datWorkorderBO.getPkWorkorderId();

				notificationDetails.setEmpId(pcId);
				notificationDetails.setNoificationMessage(notificationMessage);
				notificationDetails
						.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);
				notificationDetails.setNotificationcreatedDate(datWorkorderBO
						.getWorkorderCreatedOn());
				notificationDetails.setClearFlagModifiedDate(today);
				notificationDetails.setClearFlagcreatedDate(today);
				notificationDetails
						.setModuleName(ProjectNotificationConstants.MODULE_NAME);
				notificationDetails
						.setFkModuleId(ProjectNotificationConstants.MENU_ID);
				notificationDetails
						.setMenuUrl(workorderCreationKey.split(",")[0]);
				notificationDetails
						.setIsAction(workorderCreationKey.split(",")[1]);
				notificationDetails
						.setNoificationMessageIdentifier(fullmsgIdentifierKey);
				notificationDetails
						.setNoificationIsExpiry(ProjectNotificationConstants.IS_EXPIRY_NO);
				notificationDetails.setNotificationExpiryTime(Integer
						.parseInt(workorderCreationKey.split(",")[3]));
				datNotificationsBOList.add(notificationDetails);
			}

			try {
				notificationRepository.save(datNotificationsBOList);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException("Failed to Save : ", e);
			}
		}
		LOG.endUsecase("Exiting from sendWorkorderCreatedNotificationToDMDLPC() service");

	}

	public void sendWorkorderUpdatedNotificationToAllPC(
			DatWorkorderBO datWorkorderBO) throws DataAccessException {
		LOG.startUsecase("Entering into sendWorkorderUpdatedNotificationToAllPC() service");
		String msgIdentifierKey = workorderUpdationKey.split(",")[2];
		List<DatNotificationsBO> datNotificationsBOList = new ArrayList<DatNotificationsBO>();
		Set<Integer> pcSet = new HashSet<Integer>();
		DatNotificationsBO isDuplicatesFound = null;
		Date today = new Date();

		if (datWorkorderBO != null) {
			Integer empId = datWorkorderBO.getWorkorderUpdatedBy();
			String notificationMessage = MessageFormat.format(
					workOrderUpdationToPCIndividual,
					gsrNotificationService.getEmployeeNameByEmpId(empId)
							+ " - " + empId,
					datWorkorderBO.getWorkorderNumber(),
					datWorkorderBO.getWorkorderUpdatedOn());

			pcSet.add(empId);
			try {
				pcSet.addAll(projDatProjectMappedToProjectCoordinatorRepository
						.getAllPcRelatedToProject(datWorkorderBO
								.getPkWorkorderId()));
			} catch (Exception e) {
	//			e.printStackTrace();
				throw new DataAccessException("unable to send notification");
			}
			pcSet.remove(empId);

			for (Integer pcId : pcSet) {

				DatNotificationsBO notificationDetails = new DatNotificationsBO();

				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId
						+ "_" + pcId + "_" + datWorkorderBO.getPkWorkorderId();

				isDuplicatesFound = gsrNotificationService.checkDuplicates(
						pcId, fullmsgIdentifierKey);
				if (isDuplicatesFound == null) {

					notificationDetails.setEmpId(pcId);
					notificationDetails
							.setNoificationMessage(notificationMessage);
					notificationDetails
							.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);
					notificationDetails
							.setNotificationcreatedDate(datWorkorderBO
									.getWorkorderUpdatedOn());
					notificationDetails.setClearFlagModifiedDate(today);
					notificationDetails.setClearFlagcreatedDate(today);
					notificationDetails
							.setModuleName(ProjectNotificationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(ProjectNotificationConstants.MENU_ID);
					notificationDetails.setMenuUrl(workorderUpdationKey
							.split(",")[0]);
					notificationDetails.setIsAction(workorderUpdationKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(ProjectNotificationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(workorderUpdationKey.split(",")[3]));

					datNotificationsBOList.add(notificationDetails);

				} else {

					isDuplicatesFound
							.setNoificationMessage(notificationMessage);
					isDuplicatesFound.setNotificationModifiedDate(new Date());

					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= datWorkorderBO
							.getWorkorderUpdatedOn().getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound
								.setNotificationcreatedDate(datWorkorderBO
										.getWorkorderUpdatedOn());

					datNotificationsBOList.add(isDuplicatesFound);

				}

			}
			try {
				notificationRepository.save(datNotificationsBOList);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException("Unable to send notification.");
			}
		}
		LOG.endUsecase("Exiting from sendWorkorderUpdatedNotificationToAllPC() service");

	}

	public void sendComponentCreatedNotificationToAllPC(
			DatComponentBO datComponentBO) throws DataAccessException {
		LOG.startUsecase("Entering into sendWorkorderCreatedNotificationToDMDLPC() service");
		String msgIdentifierKey = componentCreationKey.split(",")[2];
		List<DatNotificationsBO> datNotificationsBOList = new ArrayList<DatNotificationsBO>();
		Set<Integer> pcSet = new HashSet<Integer>();
		Date today = new Date();

		if (datComponentBO != null) {
			Integer empId = datComponentBO.getCreatedBy();
			String notificationMessage = MessageFormat.format(
					componentCreationToPCIndividual,
					gsrNotificationService.getEmployeeNameByEmpId(empId)
							+ " - " + empId, datComponentBO.getComponentName(),
					datComponentBO.getCreatedOn());

			pcSet.add(empId);
			try {
				pcSet.addAll(projDatProjectMappedToProjectCoordinatorRepository
						.getAllPcRelatedToProject(datComponentBO
								.getFkWorkorderId()));
			} catch (Exception e) {
		//		e.printStackTrace();
				throw new DataAccessException("unable to send notification");
			}
			pcSet.remove(empId);

			for (Integer pcId : pcSet) {

				DatNotificationsBO notificationDetails = new DatNotificationsBO();

				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId
						+ "_" + pcId + "_" + datComponentBO.getPkComponentId();

				notificationDetails.setEmpId(pcId);
				notificationDetails.setNoificationMessage(notificationMessage);
				notificationDetails
						.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);
				notificationDetails.setNotificationcreatedDate(datComponentBO
						.getCreatedOn());
				notificationDetails.setClearFlagModifiedDate(today);
				notificationDetails.setClearFlagcreatedDate(today);
				notificationDetails
						.setModuleName(ProjectNotificationConstants.MODULE_NAME);
				notificationDetails
						.setFkModuleId(ProjectNotificationConstants.MENU_ID);
				notificationDetails
						.setMenuUrl(componentCreationKey.split(",")[0]);
				notificationDetails
						.setIsAction(componentCreationKey.split(",")[1]);
				notificationDetails
						.setNoificationMessageIdentifier(fullmsgIdentifierKey);
				notificationDetails
						.setNoificationIsExpiry(ProjectNotificationConstants.IS_EXPIRY_NO);
				notificationDetails.setNotificationExpiryTime(Integer
						.parseInt(componentCreationKey.split(",")[3]));

				datNotificationsBOList.add(notificationDetails);

			}

			try {
				notificationRepository.save(datNotificationsBOList);
			} catch (Exception e) {
		//		e.printStackTrace();
				throw new DataAccessException("Failed to Save : ", e);
			}
		}
		LOG.endUsecase("Exiting from sendWorkorderCreatedNotificationToDMDLPC() service");

	}

	public void sendComponentUpdatedNotificationToAllPC(
			DatComponentBO datComponentBO) throws DataAccessException,CommonCustomException{
		LOG.startUsecase("Entering into sendWorkorderCreatedNotificationToDMDLPC() service");
		String msgIdentifierKey = componentUpdationKey.split(",")[2];
		List<DatNotificationsBO> datNotificationsBOList = new ArrayList<DatNotificationsBO>();
		Set<Integer> pcSet = new HashSet<Integer>();
		DatNotificationsBO isDuplicatesFound = null;
		Date today = new Date();

		if (datComponentBO != null) {
			Integer empId = datComponentBO.getUpdatedBy();
			String notificationMessage = MessageFormat.format(
					componentUpdationToPCIndividual,
					gsrNotificationService.getEmployeeNameByEmpId(empId)
							+ " - " + empId, datComponentBO.getComponentName(),
							today);

			pcSet.add(empId);
			try {
				pcSet.addAll(projDatProjectMappedToProjectCoordinatorRepository
						.getAllPcRelatedToProject(datComponentBO
								.getFkWorkorderId()));
			} catch (Exception e) {
	//			e.printStackTrace();
				throw new DataAccessException("unable to send notification");
			}
			pcSet.remove(empId);

			for (Integer pcId : pcSet) {

				DatNotificationsBO notificationDetails = new DatNotificationsBO();

				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId
						+ "_" + pcId + "_" + datComponentBO.getPkComponentId();

				try{
				isDuplicatesFound = gsrNotificationService.checkDuplicates(
						pcId, fullmsgIdentifierKey);
				}catch(Exception e)
				{
				throw new CommonCustomException(e.getMessage());
				}
				LOG.info("isDuplicatesFound::::::::::======>"+isDuplicatesFound);
				System.out.println(isDuplicatesFound);
				if (isDuplicatesFound == null) {

					notificationDetails.setEmpId(pcId);
					notificationDetails
							.setNoificationMessage(notificationMessage);
					
					notificationDetails
							.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);
					LOG.info("setClearFlag+============>"+notificationDetails.getClearFlag());
					notificationDetails
							.setNotificationcreatedDate(today);
					notificationDetails.setClearFlagModifiedDate(today);
					notificationDetails.setClearFlagcreatedDate(today);
					notificationDetails
							.setModuleName(ProjectNotificationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(ProjectNotificationConstants.MENU_ID);
					notificationDetails.setMenuUrl(componentUpdationKey
							.split(",")[0]);
					notificationDetails.setIsAction(componentUpdationKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(ProjectNotificationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(componentUpdationKey.split(",")[3]));

					datNotificationsBOList.add(notificationDetails);

				} else {

					isDuplicatesFound
							.setNoificationMessage(notificationMessage);
					isDuplicatesFound.setNotificationModifiedDate(new Date());
					isDuplicatesFound.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);
					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= today
							.getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound
								.setNotificationcreatedDate(today);

					datNotificationsBOList.add(isDuplicatesFound);

				}
			}

			try {
				notificationRepository.save(datNotificationsBOList);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException("Failed to Save : ", e);
			}
		}
		LOG.endUsecase("Exiting from sendWorkorderCreatedNotificationToDMDLPC() service");

	}

	
	public void sendWoStatusUpdatedNotificationToAllPC(String oldStatusName, byte status,
			DatWorkorderBO datWorkorderBO) throws DataAccessException,
			CommonCustomException {
		LOG.startUsecase("Entering into sendWorkorderCreatedNotificationToDMDLPC() service");
		String msgIdentifierKey = woStatusUpdationKey.split(",")[2];
		List<DatNotificationsBO> datNotificationsBOList = new ArrayList<DatNotificationsBO>();
		Set<Integer> pcSet = new HashSet<Integer>();
		DatNotificationsBO isDuplicatesFound = null;
		DatWorkorderBO datWoBO;
		Date today = new Date();

		if (datWorkorderBO != null) {

			try {
				datWoBO = datWorkorderRepository
						.findByPkWorkorderId(datWorkorderBO.getPkWorkorderId());
			} catch (Exception e) {
				throw new DataAccessException("Unable to send notification");
			}

			String notificationMessage = MessageFormat
					.format(workOrderStatusUpdateToPCDMDL, datWorkorderBO
							.getWorkorderNumber(), datWorkorderBO
							.getMasProject().getProjectName(),
							oldStatusName
									+ " to "
									+ datWoBO.getProjMasWoStatus()
									.getWorkorderStatus(), datWoBO
									.getWorkorderUpdatedOn());
			if (status == ProjectConstants.WO_ACTIVE
					&& datWorkorderBO.getFkWorkorderStatusId() == ProjectConstants.WO_CLOSED) {
				pcSet.add(datWorkorderBO.getMasProject().getFkDeliveryMgrId());
				pcSet.add(datWorkorderBO.getMasProject().getFkDomainMgrId());
				// pcSet.add(arg0);
			}
			try {
				pcSet.addAll(projDatProjectMappedToProjectCoordinatorRepository
						.getAllPcRelatedToProject(datWorkorderBO
								.getPkWorkorderId()));
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException("unable to send notification");
			}

			for (Integer pcId : pcSet) {

				DatNotificationsBO notificationDetails = new DatNotificationsBO();

				String fullmsgIdentifierKey = msgIdentifierKey + "_" + pcId
						+ "_" + datWorkorderBO.getPkWorkorderId();

				try {
					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							pcId, fullmsgIdentifierKey);
				} catch (Exception e) {
					throw new CommonCustomException(e.getMessage());
				}
				LOG.info("isDuplicatesFound::::::::::======>"
						+ isDuplicatesFound);
				System.out.println(isDuplicatesFound);
				if (isDuplicatesFound == null) {

					notificationDetails.setEmpId(pcId);
					notificationDetails
							.setNoificationMessage(notificationMessage);
					notificationDetails
							.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);

					LOG.info("setClearFlag+============>"
							+ notificationDetails.getClearFlag());
					notificationDetails
							.setNotificationcreatedDate(datWorkorderBO
									.getWorkorderUpdatedOn());
					notificationDetails.setClearFlagModifiedDate(today);
					notificationDetails.setClearFlagcreatedDate(today);
					notificationDetails
							.setModuleName(ProjectNotificationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(ProjectNotificationConstants.MENU_ID);
					notificationDetails.setMenuUrl(woStatusUpdationKey
							.split(",")[0]);
					notificationDetails.setIsAction(woStatusUpdationKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(ProjectNotificationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(woStatusUpdationKey.split(",")[3]));
					System.out.println(notificationDetails);
					datNotificationsBOList.add(notificationDetails);

				} else {

					isDuplicatesFound
							.setNoificationMessage(notificationMessage);
					isDuplicatesFound.setNotificationModifiedDate(new Date());
					isDuplicatesFound
							.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);
					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= datWorkorderBO
							.getWorkorderUpdatedOn().getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound
								.setNotificationcreatedDate(datWorkorderBO
										.getWorkorderUpdatedOn());
					System.out.println(notificationDetails);
					datNotificationsBOList.add(isDuplicatesFound);

					try {
						notificationRepository.save(datNotificationsBOList);
					} catch (Exception e) {
						// e.printStackTrace();
						throw new DataAccessException("Failed to Save : ", e);
					}
				}

			}
		}
		LOG.endUsecase("Exiting from sendWorkorderCreatedNotificationToDMDLPC() service");

	}
	
			
	// Added By Rinta
	/**
	 * <Description sendMileStoneCreatedNotificationToPC> This method is used to
	 * create notification for PC after creating a milestone
	 * 
	 * @param milestoneId
	 * @throws DataAccessException
	 */
	public void sendMileStoneCreatedNotificationToPC(Integer milestoneId)
			throws DataAccessException {
		LOG.startUsecase("Entering into sendMileStoneCreatedNotificationToPC() service");
		String msgIdentifierKey = milestoneCreationKey.split(",")[2];
		List<DatNotificationsBO> datNotificationsBOList = new ArrayList<DatNotificationsBO>();
		Set<Integer> pcSet = new HashSet<Integer>();
		Date today = new Date();
		DatMilestoneBO milestoneBO = null;

		try {
			milestoneBO = milestoneRepository.findByPkMilestoneId(milestoneId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Something went wrong while creating Milestone");
		}

		if (milestoneBO != null) {
			System.out.println(milestoneBO);
			Integer empId = milestoneBO.getCreatedBy();

			DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
			String createdDate = dateFormatter.format(milestoneBO
					.getCreatedOn());
			DateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
			String createdtime = timeFormatter.format(milestoneBO
					.getCreatedOn());

			String notificationMessage = MessageFormat.format(
					milestoneCreationToPCIndividual,
					gsrNotificationService.getEmployeeNameByEmpId(empId)
							+ " - " + empId, milestoneBO.getMilestoneName(),
					milestoneBO.getDatComponent().getComponentName(),
					createdDate, createdtime);

			pcSet.add(empId);
			try {
				pcSet.addAll(projDatProjectMappedToProjectCoordinatorRepository
						.getAllPcRelatedToProject(milestoneBO.getDatComponent()
								.getFkWorkorderId()));
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException(
						"unable to send milestone create notification");
			}
			pcSet.remove(empId);

			for (Integer pcId : pcSet) {

				DatNotificationsBO notificationDetails = new DatNotificationsBO();

				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId
						+ "_" + pcId + "_" + milestoneBO.getPkMilestoneId();

				notificationDetails.setEmpId(pcId);
				notificationDetails.setNoificationMessage(notificationMessage);
				notificationDetails
						.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);
				notificationDetails.setNotificationcreatedDate(milestoneBO
						.getCreatedOn());
				notificationDetails.setClearFlagModifiedDate(today);
				notificationDetails.setClearFlagcreatedDate(today);
				notificationDetails
						.setModuleName(ProjectNotificationConstants.MODULE_NAME);
				notificationDetails
						.setFkModuleId(ProjectNotificationConstants.MENU_ID);
				notificationDetails
						.setMenuUrl(milestoneCreationKey.split(",")[0]);
				notificationDetails
						.setIsAction(milestoneCreationKey.split(",")[1]);
				notificationDetails
						.setNoificationMessageIdentifier(fullmsgIdentifierKey);
				notificationDetails
						.setNoificationIsExpiry(ProjectNotificationConstants.IS_EXPIRY_NO);
				notificationDetails.setNotificationExpiryTime(Integer
						.parseInt(milestoneCreationKey.split(",")[3]));

				datNotificationsBOList.add(notificationDetails);
			}

			if (datNotificationsBOList != null
					&& !datNotificationsBOList.isEmpty()) {
				try {
					notificationRepository.save(datNotificationsBOList);
				} catch (Exception e) {
					// e.printStackTrace();
					throw new DataAccessException("Failed to Save : ", e);
				}
			}
		}
		LOG.endUsecase("Exiting from sendMileStoneCreatedNotificationToPC() service");

	}

	/**
	 * <Description sendMilestoneUpdatedNotificationToPC> This method is used to
	 * create notification for PC after updating a milestone
	 * 
	 * @param milestoneId
	 * @param milestoneName
	 * @throws DataAccessException
	 */
	public void sendMilestoneUpdatedNotificationToPC(Integer milestoneId,
			String milestoneName) throws DataAccessException {
		LOG.startUsecase("Entering into sendProjectUpdatedNotificationToDMDLPC() service");
		String msgIdentifierKey = milestoneUpdationKey.split(",")[2];
		List<DatNotificationsBO> datNotificationsBOList = new ArrayList<DatNotificationsBO>();
		DatNotificationsBO isDuplicatesFound = null;
		Set<Integer> pcSet = new HashSet<Integer>();
		DatMilestoneBO milestoneBO = null;
		Date today = new Date();

		try {
			milestoneBO = milestoneRepository.findByPkMilestoneId(milestoneId);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new DataAccessException(
					"Something went wrong while creating Milestone");
		}

		if (milestoneBO != null) {
			Integer empId = milestoneBO.getUpdatedBy();

			DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
			String createdDate = dateFormatter.format(milestoneBO
					.getCreatedOn());
			DateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
			String createdtime = timeFormatter.format(milestoneBO
					.getCreatedOn());

			String notificationMessage = MessageFormat.format(
					milestoneUpdationToPCIndividual,
					gsrNotificationService.getEmployeeNameByEmpId(empId)
							+ " - " + empId, milestoneName, milestoneBO
							.getDatComponent().getComponentName(), createdDate,
					createdtime);

			pcSet.add(empId);
			try {
				pcSet.addAll(projDatProjectMappedToProjectCoordinatorRepository
						.getAllPcRelatedToProject(milestoneBO.getDatComponent()
								.getFkWorkorderId()));
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException(
						"unable to send milestone update notification");
			}
			pcSet.remove(empId);

			for (Integer pcId : pcSet) {

				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId
						+ "_" + pcId + "_" + milestoneBO.getPkMilestoneId();

				isDuplicatesFound = gsrNotificationService.checkDuplicates(
						pcId, fullmsgIdentifierKey);

				if (isDuplicatesFound == null) {

					DatNotificationsBO notificationDetails = new DatNotificationsBO();

					notificationDetails.setEmpId(pcId);
					notificationDetails
							.setNoificationMessage(notificationMessage);
					
					notificationDetails
							.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);
				
					notificationDetails.setNotificationcreatedDate(milestoneBO
							.getUpdatedOn());
					notificationDetails.setClearFlagModifiedDate(today);
					notificationDetails.setClearFlagcreatedDate(today);
					notificationDetails
							.setModuleName(ProjectNotificationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(ProjectNotificationConstants.MENU_ID);
					
					notificationDetails.setMenuUrl(milestoneUpdationKey
							.split(",")[0]);
					notificationDetails.setIsAction(milestoneUpdationKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(ProjectNotificationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(milestoneUpdationKey.split(",")[3]));

					datNotificationsBOList.add(notificationDetails);
				} else {

					isDuplicatesFound
							.setNoificationMessage(notificationMessage);

					isDuplicatesFound.setNotificationModifiedDate(today);
					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= today.getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound.setNotificationcreatedDate(today);

					datNotificationsBOList.add(isDuplicatesFound);
				}
			}

				
			if (datNotificationsBOList != null
					&& !datNotificationsBOList.isEmpty()) {
				try {
					notificationRepository.save(datNotificationsBOList);
				} catch (Exception e) {
					// e.printStackTrace();
					throw new DataAccessException("Failed to Save : ", e);
				}
			}
		}
		LOG.endUsecase("Exiting from sendProjectCreatedNotificationToDMDLPC() service");
	}

	/**
	 * <Description sendSowUploadNotificationToDMDL> This method is used to
	 * create notification for DM and DL after uploading sow file
	 * 
	 * @param woId
	 * @param version
	 * @throws DataAccessException
	 */
	public void sendSowUploadNotificationToDMDL(Integer woId, Short version)
			throws DataAccessException {
		LOG.startUsecase("Entering into sendSowUploadNotificationToDMDL() service");
		String msgIdentifierKey = sowAmountUpdationKey.split(",")[2];
		List<DatNotificationsBO> datNotificationsBOList = new ArrayList<DatNotificationsBO>();
		Set<Integer> dmDl = new HashSet<Integer>();
		Date today = new Date();
		DatWorkorderBO datWorkorderBO;
		DatSowUploadHistoryBO datSowUploadHistoryBO;
		DatNotificationsBO isDuplicatesFound = null;

		if (woId != null && version != null) {

			try {
				datSowUploadHistoryBO = datSowUploadHistoryRepository
						.findByFkWorkorderIdAndVersionId(woId, version);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException(
						"Some error occured while uploading sow docs.");
			}

			try {
				datWorkorderBO = datWorkorderRepository
						.findByPkWorkorderId(woId);
			} catch (Exception e1) {
				// e1.printStackTrace();
				throw new DataAccessException(
						"Some error occured while uploading sow docs.");
			}

			if (datWorkorderBO != null && datSowUploadHistoryBO != null) {
				Integer empId = datSowUploadHistoryBO.getCreatedBy();

				DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
				String createdDate = dateFormatter.format(datSowUploadHistoryBO
						.getCreatedOn());
				DateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
				String createdtime = timeFormatter.format(datSowUploadHistoryBO
						.getCreatedOn());

				String notificationMessage = MessageFormat.format(
						sowAmountUpdationUpdationToDMDL,
						gsrNotificationService.getEmployeeNameByEmpId(empId)
								+ " - " + empId,
						datWorkorderBO.getWorkorderNumber(), createdDate,
						createdtime);

				dmDl.add(empId);
				dmDl.add(datWorkorderBO.getMasProject().getFkDeliveryMgrId());
				dmDl.add(datWorkorderBO.getMasProject().getFkDomainMgrId());
				dmDl.remove(empId);
				System.out.println(dmDl);

				for (Integer id : dmDl) {

					DatNotificationsBO notificationDetails = new DatNotificationsBO();
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + "_" + id + "_" + woId;

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							id, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {

						notificationDetails.setEmpId(id);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(datWorkorderBO
										.getWorkorderUpdatedOn());
						notificationDetails.setClearFlagModifiedDate(today);
						notificationDetails.setClearFlagcreatedDate(today);
						notificationDetails
								.setModuleName(ProjectNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ProjectNotificationConstants.MENU_ID);
						notificationDetails.setMenuUrl(sowAmountUpdationKey
								.split(",")[0]);
						notificationDetails.setIsAction(sowAmountUpdationKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ProjectNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(sowAmountUpdationKey.split(",")[3]));

						datNotificationsBOList.add(notificationDetails);

					} else {

						isDuplicatesFound
								.setNoificationMessage(notificationMessage);

						isDuplicatesFound.setNotificationModifiedDate(today);
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= today.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound.setNotificationcreatedDate(today);

						datNotificationsBOList.add(isDuplicatesFound);
					}
				}
				if (datNotificationsBOList != null
						&& !datNotificationsBOList.isEmpty()) {
					try {
						notificationRepository.save(datNotificationsBOList);
					} catch (Exception e2) {
						// e2.printStackTrace();
						throw new DataAccessException("Failed to Save : ", e2);
					}
				}
			}
		}
		LOG.endUsecase("Exiting from sendSowUploadNotificationToDMDL() service");
	}
	// EOA By Rinta
	
	
	public void sendComponentClosureNotificationToAllPC(DatComponentBO datComponentBO)
			throws DataAccessException {
		LOG.startUsecase("Entering into sendComponentClosureNotificationToAllPC() service");
		String msgIdentifierKey = compClosureKey.split(",")[2];
		List<DatNotificationsBO> datNotificationsBOList = new ArrayList<DatNotificationsBO>();
		Set<Integer> pcSet = new HashSet<Integer>();
		Date today = new Date();
		DatNotificationsBO isDuplicatesFound = null;

		if (datComponentBO != null) {
			
			String notificationMessage;
			Integer empId = datComponentBO.getUpdatedBy();
			if(datComponentBO.getFkComponentTypeId()==ProjectConstants.COMPONENT_TYPE_ID_OTHERS)
			{
			 notificationMessage = MessageFormat.format(
					internalComponentClosureToAllPc,
					gsrNotificationService.getEmployeeNameByEmpId(empId)
							+ " - " + empId, datComponentBO.getComponentName(),
							datComponentBO.getDatWorkorder().getWorkorderNumber(),today);
			}
			else
			{
				notificationMessage = MessageFormat.format(
						fixedTnMComponentClosureToAllPc,datComponentBO.getComponentName(),
						today);
			}
			pcSet.add(empId);
			try {
				pcSet.addAll(projDatProjectMappedToProjectCoordinatorRepository.getAllPcRelatedToProject(
						datComponentBO.getFkWorkorderId()));
					
			} catch (Exception e) {
			//	e.printStackTrace();
				throw new DataAccessException("unable to send notification");
			}
			pcSet.remove(empId);

			for (Integer pcId : pcSet) {
				String fullmsgIdentifierKey;
				DatNotificationsBO notificationDetails = new DatNotificationsBO();
				if(datComponentBO.getFkComponentTypeId()==ProjectConstants.COMPONENT_TYPE_ID_OTHERS)
				{
				 fullmsgIdentifierKey = msgIdentifierKey + "_" + empId
						+ "_" + pcId + "_" + datComponentBO.getPkComponentId();
				}else
				{
					fullmsgIdentifierKey = msgIdentifierKey+"_"+datComponentBO.getPkComponentId();
				}
				isDuplicatesFound = gsrNotificationService.checkDuplicates(
						pcId, fullmsgIdentifierKey);
				if (isDuplicatesFound == null) {

					notificationDetails.setEmpId(pcId);
					notificationDetails
							.setNoificationMessage(notificationMessage);
					notificationDetails
							.setClearFlag(ProjectNotificationConstants.CLEAR_FLAG_NO);
					notificationDetails.setNotificationcreatedDate(today);
					notificationDetails.setClearFlagModifiedDate(today);
					notificationDetails.setClearFlagcreatedDate(today);
					notificationDetails
							.setModuleName(ProjectNotificationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(ProjectNotificationConstants.MENU_ID);
					notificationDetails.setMenuUrl(compClosureKey
							.split(",")[0]);
					notificationDetails.setIsAction(compClosureKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(ProjectNotificationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(compClosureKey.split(",")[3]));

					datNotificationsBOList.add(notificationDetails);

				} else {

					isDuplicatesFound
							.setNoificationMessage(notificationMessage);
					isDuplicatesFound.setNotificationModifiedDate(new Date());

					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= datComponentBO.getUpdatedOn().getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound
								.setNotificationcreatedDate(datComponentBO
										.getUpdatedOn());

					datNotificationsBOList.add(isDuplicatesFound);

				}
			}

			try {
				notificationRepository.save(datNotificationsBOList);
			} catch (Exception e) {
				// e.printStackTrace();
				throw new DataAccessException("Failed to Save : ", e);
			}

		}
		LOG.endUsecase("Exiting from sendComponentClosureNotificationToAllPC() service");

	}
	
	
}
