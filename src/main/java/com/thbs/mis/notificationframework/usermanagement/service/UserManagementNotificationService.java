package com.thbs.mis.notificationframework.usermanagement.service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.gsrnotification.service.GSRNotificationService;
import com.thbs.mis.notificationframework.notificationsettings.bo.DatNotificationsBO;
import com.thbs.mis.notificationframework.notificationsettings.dao.NotificationRepository;
import com.thbs.mis.notificationframework.usermanagement.constants.UserManagementNotificationConstants;

@Service
public class UserManagementNotificationService {

	private static final AppLog LOG = LogFactory
			.getLog(UserManagementNotificationService.class);

	/*
	 * Individual Notification Messages from ClientMessages.properties file
	 */
	@Value("${ui.message.empProfileCreatedToSysAdminScreen}")
	String empProfileCreatedToSysAdminIndividualMsg;

	/*
	 * Consolidated Notification Messages from ClientMessages.properties file
	 */
	@Value("${ui.message.empProfileCreatedToSysAdminScreenConsolidated}")
	String empProfileCreatedToSysAdminConsolidatedMsg;

	/*
	 * Purchase Notification Keys from notificationframework.properties file
	 */
	@Value("${UM_HR_CRE_EMP_SYSADMIN}")
	String hrCreEmpProfToSysAdminKeys;

	/*
	 * Autowiring Repositories
	 */
	@Autowired
	private GSRNotificationService gsrNotificationService;

	@Autowired
	private NotificationRepository notificationRepository;

	@Autowired
	private DatEmployeeProfessionalRepository empProfessionalRepository;

	/**
	 * @Description : This method is used to set Profile Created Notification to
	 *              System Admin Screen
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setProfileCreatedNotificationToSysAdminScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setProfileCreatedNotificationToSysAdminScreen() method");
		String hrCreEmpProfToSysAdminKey = hrCreEmpProfToSysAdminKeys;
		String msgIdentifierKey = hrCreEmpProfToSysAdminKey.split(",")[2];
		List<DatEmpProfessionalDetailBO> profDetailsList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			profDetailsList = empProfessionalRepository
					.findByEmpProfessionalEmailIdIsNull();

			if (profDetailsList.size() > 0) {
				if (profDetailsList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								empProfileCreatedToSysAdminIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(profDetailsList
												.get(0).getFkMainEmpDetailId())
										+ "-"
										+ profDetailsList.get(0)
												.getFkMainEmpDetailId());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(UserManagementNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(profDetailsList
										.get(0).getDatEmpDetailBO_main_emp_id()
										.getProfileCreatedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(UserManagementNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(UserManagementNotificationConstants.USER_MANAGEMENT_MENU_ID);
						notificationDetails
								.setMenuUrl(hrCreEmpProfToSysAdminKey
										.split(",")[0]);
						notificationDetails
								.setIsAction(hrCreEmpProfToSysAdminKey
										.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(UserManagementNotificationConstants.IS_EXPIRY_NO);
						notificationDetails
								.setNotificationExpiryTime(Integer
										.parseInt(hrCreEmpProfToSysAdminKey
												.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								empProfileCreatedToSysAdminIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(profDetailsList
												.get(0).getFkMainEmpDetailId())
										+ "-"
										+ profDetailsList.get(0)
												.getFkMainEmpDetailId());
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(UserManagementNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(UserManagementNotificationConstants.CLEAR_FLAG_NO);

						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= profDetailsList.get(0)
								.getDatEmpDetailBO_main_emp_id()
								.getProfileCreatedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(profDetailsList
											.get(0)
											.getDatEmpDetailBO_main_emp_id()
											.getProfileCreatedDate());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					List<DatEmpDetailBO> empDetail = new ArrayList<DatEmpDetailBO>();

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						profDetailsList.forEach(detail -> {
							empDetail.add(detail
									.getDatEmpDetailBO_main_emp_id());
						});

						Date maxDate = empDetail.stream()
								.map(DatEmpDetailBO::getProfileCreatedDate)
								.max(Date::compareTo).get();

						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								empProfileCreatedToSysAdminConsolidatedMsg,
								profDetailsList.size());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(UserManagementNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(maxDate);
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(UserManagementNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(UserManagementNotificationConstants.USER_MANAGEMENT_MENU_ID);
						notificationDetails
								.setMenuUrl(hrCreEmpProfToSysAdminKey
										.split(",")[0]);
						notificationDetails
								.setIsAction(hrCreEmpProfToSysAdminKey
										.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(UserManagementNotificationConstants.IS_EXPIRY_NO);
						notificationDetails
								.setNotificationExpiryTime(Integer
										.parseInt(hrCreEmpProfToSysAdminKey
												.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								empProfileCreatedToSysAdminConsolidatedMsg,
								profDetailsList.size());
						Date maxDate = empDetail.stream()
								.map(DatEmpDetailBO::getProfileCreatedDate)
								.max(Date::compareTo).get();
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(UserManagementNotificationConstants.IS_EXPIRY_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(maxDate);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = hrCreEmpProfToSysAdminKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									hrCreEmpProfToSysAdminKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in setting User Management Profile Created Notifications");
		}
		LOG.endUsecase("Exiting setProfileCreatedNotificationToSysAdminScreen() method");
	}

}
