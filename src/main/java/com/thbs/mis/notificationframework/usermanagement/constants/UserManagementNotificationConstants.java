package com.thbs.mis.notificationframework.usermanagement.constants;

public class UserManagementNotificationConstants {

	public static final String MODULE_NAME = "USER MANAGEMENT";
	public static final short USER_MANAGEMENT_MENU_ID = 36;
	public final static String CLEAR_FLAG_NO = "NO";
	public static final String IS_EXPIRY_NO = "NOTEXPIRED";

}
