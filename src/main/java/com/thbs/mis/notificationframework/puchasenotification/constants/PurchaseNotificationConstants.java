package com.thbs.mis.notificationframework.puchasenotification.constants;

public class PurchaseNotificationConstants {

	public static final String MODULE_NAME = "PURCHASE";
	public static final short PURCHASE_MENU_ID = 21;
	public final static String CLEAR_FLAG_NO = "NO";
	public static final String IS_EXPIRY_NO = "NOTEXPIRED";
	public static final String ASSET_QUOTE_STATUS_AGREE = "AGREE";
	public static final String COST_BORNE_BY_IT = "IT";
	public static final String COST_BORNE_BY_BU = "BU";
	public static final String COST_BORNE_BY_SHARED = "SHARED";
	public static final String DISAGREE_QUOTE = "DISAGREE";

	public static final int WAITING_FOR_RM_APPROVAL = 1;
	public static final int APPROVED_BY_RM = 2;
	public static final int PO_CREATED_BY_SYSADMIN = 8;
	public static final int CAPEX_APPROVED_BY_BUHEAD = 6;
	public static final int PO_APPROVED_BY_FINANCE = 9;
	public static final int CAPEX_REJECTED_BY_BUHEAD = 7;
	public static final int CAPEX_CREATED_BY_SYSADMIN = 4;

}
