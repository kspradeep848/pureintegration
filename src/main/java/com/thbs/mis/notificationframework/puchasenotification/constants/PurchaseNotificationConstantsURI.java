package com.thbs.mis.notificationframework.puchasenotification.constants;

public class PurchaseNotificationConstantsURI {
	
	public static final String SET_PURCHASE_NOTIFICATIONS = "/notifications/purchase";

}
