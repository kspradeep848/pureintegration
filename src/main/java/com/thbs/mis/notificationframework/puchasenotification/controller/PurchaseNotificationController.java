package com.thbs.mis.notificationframework.puchasenotification.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.gsrnotification.controller.GSRNotificationController;
import com.thbs.mis.notificationframework.puchasenotification.bean.PurchaseNotificationBean;
import com.thbs.mis.notificationframework.puchasenotification.constants.PurchaseNotificationConstantsURI;
import com.thbs.mis.notificationframework.puchasenotification.service.PurchaseNotificationService;

@Controller
public class PurchaseNotificationController {

	private static final AppLog LOG = LogFactory
			.getLog(GSRNotificationController.class);

	@Autowired
	PurchaseNotificationService purchaseNotificationService;

	@RequestMapping(value = PurchaseNotificationConstantsURI.SET_PURCHASE_NOTIFICATIONS, method = RequestMethod.POST)
	public @ResponseBody String setPurchaseNotifications(
			@RequestBody PurchaseNotificationBean purchaseNotificationBean)
			throws BusinessException {

		LOG.startUsecase("Entering setPurchaseNotifications() in Controller");
		String sucess = "";
		try {
			sucess = purchaseNotificationService
					.setAllPurchaseNotifications(purchaseNotificationBean
							.getEmpId());
		} catch (Exception e) {
			throw new BusinessException(
					"Exception thrown from setPurchaseNotifications controller");
		}
		LOG.endUsecase("Exiting setPurchaseNotifications() in Controller");
		return sucess;

	}

}
