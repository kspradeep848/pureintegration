package com.thbs.mis.notificationframework.puchasenotification.bean;

import java.util.Date;

public class PurchaseNotificationBean {

	private int empId;
	private String moduleName;
	private Date sDate;
	private Date eDate;

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public Date getsDate() {
		return sDate;
	}

	public void setsDate(Date sDate) {
		this.sDate = sDate;
	}

	public Date geteDate() {
		return eDate;
	}

	public void seteDate(Date eDate) {
		this.eDate = eDate;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	@Override
	public String toString() {
		return "PurchaseNotificationBean [empId=" + empId + ", moduleName="
				+ moduleName + ", sDate=" + sDate + ", eDate=" + eDate + "]";
	}

}
