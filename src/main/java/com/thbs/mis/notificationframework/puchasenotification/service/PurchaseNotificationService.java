package com.thbs.mis.notificationframework.puchasenotification.service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.dao.BusinessUnitRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.gsrnotification.constants.GsrNotificationConstants;
import com.thbs.mis.notificationframework.gsrnotification.service.GSRNotificationService;
import com.thbs.mis.notificationframework.notificationsettings.bo.DatNotificationsBO;
import com.thbs.mis.notificationframework.notificationsettings.dao.NotificationRepository;
import com.thbs.mis.notificationframework.puchasenotification.constants.PurchaseNotificationConstants;
import com.thbs.mis.purchase.bo.PurchaseDatAssetQuoteBO;
import com.thbs.mis.purchase.bo.PurchaseDatAssetRequestBO;
import com.thbs.mis.purchase.bo.PurchaseDatBuBO;
import com.thbs.mis.purchase.dao.PurchaseAssetQuoteRepository;
import com.thbs.mis.purchase.dao.PurchaseAssetRequestRepository;
import com.thbs.mis.purchase.dao.PurchaseDatBuRepository;

@Service
public class PurchaseNotificationService {

	private static final AppLog LOG = LogFactory
			.getLog(GSRNotificationService.class);

	/*
	 * Individual Notification Messages from ClientMessages.properties file
	 */
	@Value("${ui.message.assetRequestByEmployeeToRMScreen}")
	String assetReqByEmpToRMScreenIndividualMsg;

	@Value("${ui.message.assetRequestApprovedByRMToEmployeeScreen}")
	String assetReqApprovedByRMToEmpScreenIndividualMsg;

	@Value("${ui.message.assetRequestApprovedByRMToSysAdminScreen}")
	String assetReqApprovedByRMToSysAdminScreenIndividualMsg;

	@Value("${ui.message.poCreatedbySysAdminToFinanceHeadScreen}")
	String poCreatedBySysAdminToFinanceHeadIndividualMsg;

	@Value("${ui.message.capexApprovedByBUToEmployeeScreen}")
	String capexApprovedByBUHeadToEmpScreenIndividualMsg;

	@Value("${ui.message.poApprovedByFinancetoEmployeeScreen}")
	String poApprovedByFinanceToEmpScreenIndividualMsg;

	@Value("${ui.message.poApprovedByFinancetoSysAdminScreen}")
	String poApprovedByFinanceToSysAdminScreenIndividualMsg;

	@Value("${ui.message.capexCreatedBySysAdminToBUHeadScreen}")
	String capexCreatedBySysAdminToBUHeadIndividualMsg;

	@Value("${ui.message.capexApprovedByBUToSysAdminScreen}")
	String capexApprovedByBUToSysAdminIndividualMsg;

	@Value("${ui.message.capexRejectedByBUToEmployeeScreen}")
	String capexRejectedByBUToEmployeeIndividualMsg;

	@Value("${ui.message.capexRejectedByBUToRMScreen}")
	String capexRejectedByBUToRmScreenIndividualMsg;

	@Value("${ui.message.capexUpdatedBySysAdminToBUHeadScreen}")
	String capexUpdatedBySysAdminToBUHeadIndividualMsg;

	@Value("${ui.message.capexRejectedByBUToBUHeadScreen}")
	String capexRejectedByBUToBUHeadIndividualMsg;

	/*
	 * Consolidated Notification Messages from ClientMessages.properties file
	 */
	@Value("${ui.message.assetRequestByEmployeToRMScreenConsolidated}")
	String assetReqByEmpToRMScreenConsolidatedMsg;

	@Value("${ui.message.assetRequestApprovedByRMToSysAdminScreenConsolidated}")
	String assetReqApprovedByRMToSysAdminScreenConsolidatedMsg;

	@Value("${ui.message.poCreatedbySysAdminToFinanceHeadScreenConsolidated}")
	String poCreatedBySysAdminToFinanceHeadConsolidatedMsg;

	@Value("${ui.message.poApprovedByFInanceToSysAdminScreen}")
	String poApprovedByFinancetoSysAdminScreenConsolidatedMsg;

	@Value("${ui.message.capexCreatedBySysAdminToBUHeadConsolidated}")
	String capexCreatedBySysAdminToBUHeadConsolidatedmsg;

	@Value("${ui.message.capexApprovedByBUToSysAdminConsolidated}")
	String capexApprovedByBUToSysAdminConsolidatedMsg;

	@Value("${ui.message.capexRejectedByBUToRMScreenConsolidated}")
	String capexRejectedByBUToSysAdminConsolidatedMsg;

	@Value("${ui.message.capexRejectedByBUToRMScreenConsolidated}")
	String capexRejectedByBUToRmConsolidatedMsg;

	/*
	 * Purchase Notification Keys from notificationframework.properties file
	 */
	@Value("${PUR_EMP_CRE_ASSET_REQ}")
	String empAssetRequestKeys;

	@Value("${PUR_RM_APPROVE_ASSET_REQ_EMP}")
	String rmApproveRequestEmpKeys;

	@Value("${PUR_RM_APPROVE_ASSET_REQ_SYSADMIN}")
	String rmApproveReqSysAdminKeys;

	@Value("${PUR_SYSADMIN_CRE_PO}")
	String sysAdminCrePOKeys;

	@Value("${PUR_FINANCE_APPROVE_PO_SYSADMIN}")
	String poApprovedByFinanceToSysAdminKeys;

	@Value("${PUR_BU_CAPEX_APPROVE_EMP}")
	String capexApproveByBUToEmpkeys;

	@Value("${PUR_FINANCE_APPROVE_PO_EMP}")
	String poApprovedByFinanceToEmpKeys;

	@Value("${PUR_SYSADMIN_CRE_CAPEX}")
	String sysAdminCreateCapexKeys;

	@Value("${PUR_BU_CAPEX_APPROVE_SYSADMIN}")
	String buApproveCapexSysAdminKeys;

	@Value("${PUR_BU_CAPEX_REJ_EMP}")
	String buRejectCapexEmployeeKeys;

	@Value("${PUR_BU_CAPEX_REJ_RM}")
	String buRejectCapexRMKeys;

	@Value("${PUR_SYSADMIN_UPDATE_CAPEX}")
	String sysAdminUpdCapexKeys;

	@Value("${PUR_BU_CAPEX_REJ_SYSADMIN}")
	String buRejectCapexSysAdminKeys;

	@Value("${PUR_ITBU_REJCAPEX_BU}")
	String itBuRejectCapexToBuKeys;

	@Value("${system.admin.procurement.roleId}")
	String sysAdminProcurementRoleId;

	@Value("${finance.head.role.Id}")
	String financeHeadRoleId;

	@Value("${bu.head.role.Id}")
	String buHeadRoleId;

	@Value("${it.bu.id}")
	String itBUId;

	/*
	 * Autowiring Repositories
	 */
	@Autowired
	private PurchaseAssetRequestRepository assetRequestRepository;

	@Autowired
	private GSRNotificationService gsrNotificationService;

	@Autowired
	private NotificationRepository notificationRepository;

	@Autowired
	private EmpDetailRepository empDetailRepository;

	@Autowired
	private PurchaseAssetQuoteRepository assetQuoteRepository;

	@Autowired
	private BusinessUnitRepository buRepository;

	@Autowired
	private PurchaseDatBuRepository purchaseBuRepository;

	/**
	 * 
	 * <Description setAllPurchaseNotifications:> This method is used to set all
	 * Purchase Module Notifications
	 * 
	 * @param empId
	 * @return String
	 * @throws DataAccessException
	 */
	public String setAllPurchaseNotifications(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setAllPurchaseNotifications() service");

		DatEmpDetailBO empDetail = null;

		empDetail = empDetailRepository.findByPkEmpId(empId);

		/*
		 * Set Asset Request Notification by Employee to RM Screen
		 */
		setAssetRequestNotificationByEmpToRMScreen(empId); // Working

		/*
		 * Set Asset Request Approved Notification to Employee Screen
		 */
		setAssetRequestApprovedNotificationToEmpScreen(empId); // Working

		if (empDetail.getFkEmpRoleId() == (short) Integer
				.parseInt(sysAdminProcurementRoleId)) {
			/*
			 * Set Asset Request Approved Notification to System Admin Screen
			 */
			setAssetRequestApprovedNotificationToSysAdminScreen(empId); // Working

			/*
			 * Set PO Approved Notification to System Admin Screen
			 */
			setPOApprovedNotificationtoSysAdminScreen(empId); // Working

			/*
			 * Set Capex Approved Notification to System Admin Screen
			 */
			setCapexApprovedNotificationToSysAdminScreen(empId);
		}

		if (empDetail.getFkEmpRoleId() == (short) Integer
				.parseInt(financeHeadRoleId)) {
			/*
			 * Set PO created notification to Finance Head Screen
			 */
			setPOCreatedNotificationToFinanceHeadScreen(empId); // Working
		}

		if (empDetail.getFkEmpRoleId() == (short) Integer
				.parseInt(buHeadRoleId)) {
			/*
			 * Set Capex Created Notification to BU Head Screen
			 */
			setCapexCreatedNotificationToBUHeadScreen(empId);
		}

		/*
		 * Set Capex Approved Notification to Employee Screen
		 */
		setCapexApprovedNotificationToEmployeeScreen(empId); // Working

		/*
		 * Set PO Approved Notification to Employee Screen
		 */
		setPOApprovedNotificationToEmployeeScreen(empId); // Working

		/*
		 * Set Capex Rejected Notification to Employee Screen
		 */
		setCapexRejectedNotificationToEmployeeScreen(empId);

		/*
		 * Set Capex Rejected Notification to RM Screen
		 */
		setCapexRejectedNotificationToRMScreen(empId);

		setCapexUpdatedNotificationToBUHeadScreen(empId);

		LOG.endUsecase("Exiting setAllPurchaseNotifications() service");

		return "Success";
	}

	/**
	 * 
	 * <Description setAssetRequestNotificationByEmpToRMScreen:> This method is
	 * used to set Asset Request Notification by Employee to Reporting Manager
	 * Screen on Reporting Manager login
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setAssetRequestNotificationByEmpToRMScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setAssetRequestNotificationByEmpToRMScreen() method");
		String empAssetRequestKey = empAssetRequestKeys;
		String msgIdentifierKey = empAssetRequestKey.split(",")[2];
		List<PurchaseDatAssetRequestBO> assetRequestList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			assetRequestList = assetRequestRepository
					.findByRmToApproveAndPurchaseRequestStatus(
							empId,
							PurchaseNotificationConstants.WAITING_FOR_RM_APPROVAL);

			if (assetRequestList.size() > 0) {
				if (assetRequestList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(assetReqByEmpToRMScreenIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(assetRequestList
														.get(0)
														.getRequestorId()),
										assetRequestList
												.get(0)
												.getPurchaseMasAssetsBO()
												.getAssetNameAndVersionOrModel());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(assetRequestList
										.get(0).getCreatedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
						notificationDetails.setMenuUrl(empAssetRequestKey
								.split(",")[0]);
						notificationDetails.setIsAction(empAssetRequestKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(empAssetRequestKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(assetReqByEmpToRMScreenIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(assetRequestList
														.get(0)
														.getRequestorId()),
										assetRequestList
												.get(0)
												.getPurchaseMasAssetsBO()
												.getAssetNameAndVersionOrModel());
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);

						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= assetRequestList.get(0)
								.getCreatedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(assetRequestList
											.get(0).getCreatedDate());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						Date maxDate = assetRequestList.stream()
								.map(PurchaseDatAssetRequestBO::getCreatedDate)
								.max(Date::compareTo).get();
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								assetReqByEmpToRMScreenConsolidatedMsg,
								assetRequestList.size());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(maxDate);
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
						notificationDetails.setMenuUrl(empAssetRequestKey
								.split(",")[0]);
						notificationDetails.setIsAction(empAssetRequestKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(empAssetRequestKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								assetReqByEmpToRMScreenConsolidatedMsg,
								assetRequestList.size());
						Date maxDate = assetRequestList.stream()
								.map(PurchaseDatAssetRequestBO::getCreatedDate)
								.max(Date::compareTo).get();
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(maxDate);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = empAssetRequestKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									empAssetRequestKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setAssetRequestNotificationByEmpToRMScreen() method");
	}

	/**
	 * 
	 * <Description setAssetRequestApprovedNotificationToEmpScreen:> This method
	 * is used to set Approved Asset Request Notification to Employee Screen on
	 * Employee Login
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setAssetRequestApprovedNotificationToEmpScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setAssetRequestApprovedNotificationToEmpScreen() method");
		String rmApproveRequestEmpKey = rmApproveRequestEmpKeys;
		String msgIdentifierKey = rmApproveRequestEmpKey.split(",")[2];
		List<PurchaseDatAssetRequestBO> assetRequestList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			assetRequestList = assetRequestRepository
					.findByRequestorIdAndPurchaseRequestStatus(empId,
							PurchaseNotificationConstants.APPROVED_BY_RM);

			if (assetRequestList.size() > 0) {

				for (PurchaseDatAssetRequestBO assetRequest : assetRequestList) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + "_"
							+ assetRequest.getPkPurchaseRequestId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								assetReqApprovedByRMToEmpScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(assetRequest
												.getRmToApprove()),
								assetRequest.getPurchaseMasAssetsBO()
										.getAssetNameAndVersionOrModel());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(assetRequest
										.getModifiedOn());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
						notificationDetails.setMenuUrl(rmApproveRequestEmpKey
								.split(",")[0]);
						notificationDetails.setIsAction(rmApproveRequestEmpKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails
								.setNotificationExpiryTime(Integer
										.parseInt(rmApproveRequestEmpKey
												.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								assetReqApprovedByRMToEmpScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(assetRequest
												.getRmToApprove()),
								assetRequest.getPurchaseMasAssetsBO()
										.getAssetNameAndVersionOrModel());

						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= assetRequest.getModifiedOn()
								.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(assetRequest
											.getModifiedOn());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = rmApproveRequestEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									rmApproveRequestEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setAssetRequestApprovedNotificationToEmpScreen() method");
	}

	/**
	 * 
	 * <Description setAssetRequestApprovedNotificationToSysAdminScreen:> This
	 * method is used to set Approved Asset Request notification to System Admin
	 * when System admin logs in.
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setAssetRequestApprovedNotificationToSysAdminScreen(int empId)
			throws DataAccessException {
		LOG.startUsecase("Entering setAssetRequestApprovedNotificationToEmpScreen() method");
		String rmApproveReqSysAdminKey = rmApproveReqSysAdminKeys;
		String msgIdentifierKey = rmApproveReqSysAdminKey.split(",")[2];
		List<PurchaseDatAssetRequestBO> assetRequestList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			assetRequestList = assetRequestRepository
					.findByPurchaseRequestStatus(PurchaseNotificationConstants.APPROVED_BY_RM);

			if (assetRequestList.size() > 0) {
				if (assetRequestList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(assetReqApprovedByRMToSysAdminScreenIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(assetRequestList
														.get(0)
														.getRmToApprove()),
										assetRequestList
												.get(0)
												.getPurchaseMasAssetsBO()
												.getAssetNameAndVersionOrModel());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(assetRequestList
										.get(0).getModifiedOn());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
						notificationDetails.setMenuUrl(rmApproveReqSysAdminKey
								.split(",")[0]);
						notificationDetails.setIsAction(rmApproveReqSysAdminKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails
								.setNotificationExpiryTime(Integer
										.parseInt(rmApproveReqSysAdminKey
												.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(assetReqApprovedByRMToSysAdminScreenIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(assetRequestList
														.get(0)
														.getRmToApprove()),
										assetRequestList
												.get(0)
												.getPurchaseMasAssetsBO()
												.getAssetNameAndVersionOrModel());
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= assetRequestList.get(0)
								.getModifiedOn().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(assetRequestList
											.get(0).getModifiedOn());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(assetReqApprovedByRMToSysAdminScreenConsolidatedMsg,
										assetRequestList.size());
						Date maxDate = assetRequestList.stream()
								.map(PurchaseDatAssetRequestBO::getModifiedOn)
								.max(Date::compareTo).get();
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(maxDate);
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
						notificationDetails.setMenuUrl(rmApproveReqSysAdminKey
								.split(",")[0]);
						notificationDetails.setIsAction(rmApproveReqSysAdminKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails
								.setNotificationExpiryTime(Integer
										.parseInt(rmApproveReqSysAdminKey
												.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(assetReqApprovedByRMToSysAdminScreenConsolidatedMsg,
										assetRequestList.size());
						Date maxDate = assetRequestList.stream()
								.map(PurchaseDatAssetRequestBO::getModifiedOn)
								.max(Date::compareTo).get();
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(maxDate);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = rmApproveReqSysAdminKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									rmApproveReqSysAdminKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}

		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setAssetRequestApprovedNotificationToEmpScreen() method");
	}

	/**
	 * 
	 * <Description setPOCreatedNotificationToFinanceHeadScreen:> This method is
	 * used to PO Created Notification to Finance Head Screen when Finance Head
	 * Logs in
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setPOCreatedNotificationToFinanceHeadScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setPOCreatedNotificationToFinanceHeadScreen() method");
		String sysAdminCrePOKey = sysAdminCrePOKeys;
		String msgIdentifierKey = sysAdminCrePOKey.split(",")[2];
		List<PurchaseDatAssetRequestBO> assetRequestList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			assetRequestList = assetRequestRepository
					.findByPurchaseRequestStatus(PurchaseNotificationConstants.PO_CREATED_BY_SYSADMIN);

			if (assetRequestList.size() > 0) {
				if (assetRequestList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(poCreatedBySysAdminToFinanceHeadIndividualMsg,
										assetRequestList
												.get(0)
												.getPurchaseMasAssetsBO()
												.getAssetNameAndVersionOrModel(),
										gsrNotificationService
												.getEmployeeNameByEmpId(assetRequestList
														.get(0)
														.getRequestorId()));
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(assetRequestList
										.get(0).getModifiedOn());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
						notificationDetails.setMenuUrl(sysAdminCrePOKey
								.split(",")[0]);
						notificationDetails.setIsAction(sysAdminCrePOKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(sysAdminCrePOKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(poCreatedBySysAdminToFinanceHeadIndividualMsg,
										assetRequestList
												.get(0)
												.getPurchaseMasAssetsBO()
												.getAssetNameAndVersionOrModel(),
										gsrNotificationService
												.getEmployeeNameByEmpId(assetRequestList
														.get(0)
														.getRequestorId()));
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= assetRequestList.get(0)
								.getModifiedOn().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(assetRequestList
											.get(0).getModifiedOn());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(poCreatedBySysAdminToFinanceHeadConsolidatedMsg,
										assetRequestList.size());
						Date maxDate = assetRequestList.stream()
								.map(PurchaseDatAssetRequestBO::getModifiedOn)
								.max(Date::compareTo).get();
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(maxDate);
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
						notificationDetails.setMenuUrl(sysAdminCrePOKey
								.split(",")[0]);
						notificationDetails.setIsAction(sysAdminCrePOKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(sysAdminCrePOKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(poCreatedBySysAdminToFinanceHeadConsolidatedMsg,
										assetRequestList.size());
						Date maxDate = assetRequestList.stream()
								.map(PurchaseDatAssetRequestBO::getModifiedOn)
								.max(Date::compareTo).get();
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(maxDate);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = sysAdminCrePOKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									sysAdminCrePOKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setPOCreatedNotificationToFinanceHeadScreen() method");
	}

	/**
	 * 
	 * <Description setCapexApprovedNotificationToEmployeeScreen:> This method
	 * is used to set Capex Approved Notification to Employee Screen when
	 * Employee logs In
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setCapexApprovedNotificationToEmployeeScreen(int empId)
			throws DataAccessException {
		LOG.startUsecase("Entering setCapexApprovedNotificationToEmployeeScreen() method");
		String capexApproveByBUToEmpkey = capexApproveByBUToEmpkeys;
		String msgIdentifierKey = capexApproveByBUToEmpkey.split(",")[2];
		List<PurchaseDatAssetRequestBO> assetRequestList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			assetRequestList = assetRequestRepository
					.findByRequestorIdAndPurchaseRequestStatus(
							empId,
							PurchaseNotificationConstants.CAPEX_APPROVED_BY_BUHEAD);

			if (assetRequestList.size() > 0) {
				for (PurchaseDatAssetRequestBO assetRequest : assetRequestList) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + "_"
							+ assetRequest.getPkPurchaseRequestId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								capexApprovedByBUHeadToEmpScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(assetRequest
												.getMasBuUnitBO()
												.getFkBuHeadEmpId()),
								assetRequest.getPurchaseMasAssetsBO()
										.getAssetNameAndVersionOrModel());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(assetRequest
										.getModifiedOn());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
						notificationDetails.setMenuUrl(capexApproveByBUToEmpkey
								.split(",")[0]);
						notificationDetails
								.setIsAction(capexApproveByBUToEmpkey
										.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails
								.setNotificationExpiryTime(Integer
										.parseInt(capexApproveByBUToEmpkey
												.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								capexApprovedByBUHeadToEmpScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(assetRequest
												.getMasBuUnitBO()
												.getFkBuHeadEmpId()),
								assetRequest.getPurchaseMasAssetsBO()
										.getAssetNameAndVersionOrModel());

						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= assetRequest.getModifiedOn()
								.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(assetRequest
											.getModifiedOn());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = capexApproveByBUToEmpkey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									capexApproveByBUToEmpkey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setCapexApprovedNotificationToEmployeeScreen() method");
	}

	/**
	 * 
	 * <Description setPOApprovedNotificationToEmployeeScreen:> This method is
	 * used to set PO Approved Notification to Employee Screen when Employee
	 * Logs In
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setPOApprovedNotificationToEmployeeScreen(int empId)
			throws DataAccessException {
		LOG.startUsecase("Entering setPOApprovedNotificationToEmployeeScreen() method");
		String poApprovedByFinanceToEmpKey = poApprovedByFinanceToEmpKeys;
		String msgIdentifierKey = poApprovedByFinanceToEmpKey.split(",")[2];
		List<PurchaseDatAssetRequestBO> assetRequestList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			assetRequestList = assetRequestRepository
					.findByRequestorIdAndPurchaseRequestStatus(
							empId,
							PurchaseNotificationConstants.PO_APPROVED_BY_FINANCE);

			if (assetRequestList.size() > 0) {
				for (PurchaseDatAssetRequestBO assetRequest : assetRequestList) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + "_"
							+ assetRequest.getPkPurchaseRequestId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								poApprovedByFinanceToEmpScreenIndividualMsg,
								assetRequest.getPurchaseMasAssetsBO()
										.getAssetNameAndVersionOrModel());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(assetRequest
										.getFinanceEx_updated_on());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
						notificationDetails
								.setMenuUrl(poApprovedByFinanceToEmpKey
										.split(",")[0]);
						notificationDetails
								.setIsAction(poApprovedByFinanceToEmpKey
										.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(poApprovedByFinanceToEmpKey
										.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								poApprovedByFinanceToEmpScreenIndividualMsg,
								assetRequest.getPurchaseMasAssetsBO()
										.getAssetNameAndVersionOrModel());

						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= assetRequest
								.getFinanceEx_updated_on().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(assetRequest
											.getFinanceEx_updated_on());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = poApprovedByFinanceToEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									poApprovedByFinanceToEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setPOApprovedNotificationToEmployeeScreen() method");
	}

	/**
	 * 
	 * <Description setPOApprovedNotificationtoSysAdminScreen:> This method is
	 * used to set PO Approved Notification to System Admin Screen on System
	 * Admin Login
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setPOApprovedNotificationtoSysAdminScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setPOApprovedNotificationtoSysAdminScreen() method");
		String poApprovedByFinanceToSysAdminKey = poApprovedByFinanceToSysAdminKeys;
		String msgIdentifierKey = poApprovedByFinanceToSysAdminKey.split(",")[2];
		List<PurchaseDatAssetRequestBO> assetRequestList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {

			assetRequestList = assetRequestRepository
					.findByPurchaseRequestStatus(PurchaseNotificationConstants.PO_APPROVED_BY_FINANCE);

			if (assetRequestList.size() > 0) {
				if (assetRequestList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(poApprovedByFinanceToSysAdminScreenIndividualMsg,
										assetRequestList
												.get(0)
												.getPurchaseMasAssetsBO()
												.getAssetNameAndVersionOrModel(),
										gsrNotificationService
												.getEmployeeNameByEmpId(assetRequestList
														.get(0)
														.getRequestorId()));
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(assetRequestList
										.get(0).getFinanceEx_updated_on());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
						notificationDetails
								.setMenuUrl(poApprovedByFinanceToSysAdminKey
										.split(",")[0]);
						notificationDetails
								.setIsAction(poApprovedByFinanceToSysAdminKey
										.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(poApprovedByFinanceToSysAdminKey
										.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(poApprovedByFinanceToSysAdminScreenIndividualMsg,
										assetRequestList
												.get(0)
												.getPurchaseMasAssetsBO()
												.getAssetNameAndVersionOrModel(),
										gsrNotificationService
												.getEmployeeNameByEmpId(assetRequestList
														.get(0)
														.getRequestorId()));
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= assetRequestList.get(0)
								.getFinanceEx_updated_on().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(assetRequestList
											.get(0).getCreatedDate());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						Date maxDate = assetRequestList
								.stream()
								.map(PurchaseDatAssetRequestBO::getFinanceEx_updated_on)
								.max(Date::compareTo).get();

						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(poApprovedByFinancetoSysAdminScreenConsolidatedMsg,
										assetRequestList.size());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(maxDate);
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
						notificationDetails
								.setMenuUrl(poApprovedByFinanceToSysAdminKey
										.split(",")[0]);
						notificationDetails
								.setIsAction(poApprovedByFinanceToSysAdminKey
										.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(poApprovedByFinanceToSysAdminKey
										.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(poApprovedByFinancetoSysAdminScreenConsolidatedMsg,
										assetRequestList.size());
						Date maxDate = assetRequestList
								.stream()
								.map(PurchaseDatAssetRequestBO::getFinanceEx_updated_on)
								.max(Date::compareTo).get();
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(maxDate);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = poApprovedByFinanceToSysAdminKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys,
									empId,
									poApprovedByFinanceToSysAdminKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setPOApprovedNotificationtoSysAdminScreen() method");
	}

	/**
	 * 
	 * <Description setCapexCreatedNotificationToBUHeadScreen:> This method is
	 * used to set Capex Created Notification to BU Head Screen on BU Head login
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setCapexCreatedNotificationToBUHeadScreen(int assetReqId)
			throws DataAccessException {
		LOG.startUsecase("Entering setCapexCreatedNotificationToBUHeadScreen() method");
		PurchaseDatAssetQuoteBO assetQuotes;
		List<PurchaseDatAssetRequestBO> assetRequestList;
		MasBuUnitBO buDetail;
		MasBuUnitBO itBuDetail;
		try {
			assetQuotes = assetQuoteRepository
					.findByFkPurchaseRequestIdAndQuoteStatus(
							assetReqId,
							PurchaseNotificationConstants.ASSET_QUOTE_STATUS_AGREE);

			if (assetQuotes != null) {
				if (assetQuotes.getPurchaseDatAssetRequestBO()
						.getPurchaseDatCapexBO().getModifiedOn() == null) {
					if (assetQuotes.getCostBorneBy().equalsIgnoreCase(
							PurchaseNotificationConstants.COST_BORNE_BY_IT)) {
						assetRequestList = assetRequestRepository
								.findByPurchaseRequestStatus(PurchaseNotificationConstants.CAPEX_CREATED_BY_SYSADMIN);

						buDetail = buRepository
								.findByPkBuUnitId((short) Integer
										.parseInt(itBUId));

						setNotificationToBUHead(buDetail.getFkBuHeadEmpId(),
								assetRequestList);
					}
					if (assetQuotes.getCostBorneBy().equalsIgnoreCase(
							PurchaseNotificationConstants.COST_BORNE_BY_BU)) {
						assetRequestList = assetRequestRepository
								.findByFkBuIdAndPurchaseRequestStatus(
										assetQuotes
												.getPurchaseDatAssetRequestBO()
												.getFkBuId(),
										PurchaseNotificationConstants.CAPEX_CREATED_BY_SYSADMIN);

						buDetail = buRepository.findByPkBuUnitId(assetQuotes
								.getPurchaseDatAssetRequestBO().getFkBuId());
						setNotificationToBUHead(buDetail.getFkBuHeadEmpId(),
								assetRequestList);

					}
					if (assetQuotes.getCostBorneBy().equalsIgnoreCase(
							PurchaseNotificationConstants.COST_BORNE_BY_SHARED)) {
						assetRequestList = assetRequestRepository
								.findByPurchaseRequestStatus(PurchaseNotificationConstants.CAPEX_CREATED_BY_SYSADMIN);

						itBuDetail = buRepository
								.findByPkBuUnitId((short) Integer
										.parseInt(itBUId));

						setNotificationToBUHead(itBuDetail.getFkBuHeadEmpId(),
								assetRequestList);
						assetRequestList.clear();

						if (itBuDetail.getPkBuUnitId() != assetQuotes
								.getPurchaseDatAssetRequestBO().getFkBuId()) {
							assetRequestList = assetRequestRepository
									.findByFkBuIdAndPurchaseRequestStatus(
											assetQuotes
													.getPurchaseDatAssetRequestBO()
													.getFkBuId(),
											PurchaseNotificationConstants.CAPEX_CREATED_BY_SYSADMIN);

							buDetail = buRepository
									.findByPkBuUnitId(assetQuotes
											.getPurchaseDatAssetRequestBO()
											.getFkBuId());

							setNotificationToBUHead(
									buDetail.getFkBuHeadEmpId(),
									assetRequestList);
						}
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setCapexCreatedNotificationToBUHeadScreen() method");
	}

	/**
	 * <Description setCapexCreatedNotificationToBUHeadScreen:> This method is
	 * used to set Capex Created Notification to BU Head Screen
	 * 
	 * @param buEmpId
	 * @param assetRequestList
	 * @throws DataAccessException
	 */
	public void setNotificationToBUHead(int buEmpId,
			List<PurchaseDatAssetRequestBO> assetRequestList)
			throws DataAccessException {
		LOG.startUsecase("Entering setNotificationToBUHead() method");
		String sysAdminCreateCapexKey = sysAdminCreateCapexKeys;
		String msgIdentifierKey = sysAdminCreateCapexKey.split(",")[2];
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			if (assetRequestList.size() > 0) {
				if (assetRequestList.size() == 1) {
					LOG.info("inside size 1");
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ buEmpId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							buEmpId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(capexCreatedBySysAdminToBUHeadIndividualMsg,
										assetRequestList
												.get(0)
												.getPurchaseMasAssetsBO()
												.getAssetNameAndVersionOrModel(),
										gsrNotificationService
												.getEmployeeNameByEmpId(assetRequestList
														.get(0)
														.getRequestorId()));
						notificationDetails.setEmpId(buEmpId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(assetRequestList
										.get(0).getModifiedOn());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
						notificationDetails.setMenuUrl(sysAdminCreateCapexKey
								.split(",")[0]);
						notificationDetails.setIsAction(sysAdminCreateCapexKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
						notificationDetails
								.setNotificationExpiryTime(Integer
										.parseInt(sysAdminCreateCapexKey
												.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(capexCreatedBySysAdminToBUHeadIndividualMsg,
										assetRequestList
												.get(0)
												.getPurchaseMasAssetsBO()
												.getAssetNameAndVersionOrModel(),
										gsrNotificationService
												.getEmployeeNameByEmpId(assetRequestList
														.get(0)
														.getRequestorId()));
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= assetRequestList.get(0)
								.getModifiedOn().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(assetRequestList
											.get(0).getModifiedOn());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ buEmpId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							buEmpId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								capexCreatedBySysAdminToBUHeadConsolidatedmsg,
								assetRequestList.size());
						Date maxDate = assetRequestList.stream()
								.map(PurchaseDatAssetRequestBO::getModifiedOn)
								.max(Date::compareTo).get();
						notificationDetails.setEmpId(buEmpId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(maxDate);
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
						notificationDetails.setMenuUrl(sysAdminCreateCapexKey
								.split(",")[0]);
						notificationDetails.setIsAction(sysAdminCreateCapexKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails
								.setNotificationExpiryTime(Integer
										.parseInt(sysAdminCreateCapexKey
												.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								capexCreatedBySysAdminToBUHeadConsolidatedmsg,
								assetRequestList.size());
						Date maxDate = assetRequestList.stream()
								.map(PurchaseDatAssetRequestBO::getModifiedOn)
								.max(Date::compareTo).get();
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(maxDate);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = sysAdminCreateCapexKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(buEmpId,
									menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, buEmpId,
									sysAdminCreateCapexKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setNotificationToBUHead() method");
	}

	/**
	 * 
	 * <Description setCapexApprovedNotificationToSysAdminScreen:> This method
	 * is used to set Capex Approved Notification to System Admin Screen when
	 * System Admin logs in.
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setCapexApprovedNotificationToSysAdminScreen(int empId)
			throws DataAccessException {
		LOG.startUsecase("Entering setCapexApprovedNotificationToSysAdminScreen() method");
		String buApproveCapexSysAdminKey = buApproveCapexSysAdminKeys;
		String msgIdentifierKey = buApproveCapexSysAdminKey.split(",")[2];
		List<PurchaseDatAssetRequestBO> assetRequestList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		Map<Short, List<PurchaseDatAssetRequestBO>> assetRequestsMapByBuId = new HashMap<Short, List<PurchaseDatAssetRequestBO>>();
		int updateCount = 0;
		try {
			assetRequestList = assetRequestRepository
					.findByPurchaseRequestStatus(PurchaseNotificationConstants.CAPEX_APPROVED_BY_BUHEAD);

			if (assetRequestList.size() > 0) {
				for (PurchaseDatAssetRequestBO assetRequest : assetRequestList) {
					if (!assetRequestsMapByBuId.containsKey(assetRequest
							.getFkBuId())) {
						List<PurchaseDatAssetRequestBO> finalBean = new ArrayList<PurchaseDatAssetRequestBO>();
						finalBean.add(assetRequest);
						assetRequestsMapByBuId.put(assetRequest.getFkBuId(),
								finalBean);
						continue;
					}
					if (assetRequestsMapByBuId.containsKey(assetRequest
							.getFkBuId())) {
						List<PurchaseDatAssetRequestBO> finalBean = new ArrayList<PurchaseDatAssetRequestBO>();
						finalBean.add(assetRequest);
						assetRequestsMapByBuId.get(assetRequest.getFkBuId())
								.addAll(finalBean);
					}
				}
				for (Map.Entry<Short, List<PurchaseDatAssetRequestBO>> entry : assetRequestsMapByBuId
						.entrySet()) {
					List<PurchaseDatAssetRequestBO> assetRequests = (List<PurchaseDatAssetRequestBO>) entry
							.getValue();

					if (assetRequests.size() == 1) {
						String fullmsgIdentifierKey = msgIdentifierKey + "_"
								+ empId;
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

						isDuplicatesFound = gsrNotificationService
								.checkDuplicates(empId, fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(capexApprovedByBUToSysAdminIndividualMsg,
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getMasBuUnitBO()
															.getFkBuHeadEmpId()),
											assetRequests
													.get(0)
													.getPurchaseMasAssetsBO()
													.getAssetNameAndVersionOrModel(),
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getRequestorId()));
							notificationDetails.setEmpId(empId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(assetRequests
											.get(0).getModifiedOn());
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
							notificationDetails
									.setMenuUrl(buApproveCapexSysAdminKey
											.split(",")[0]);
							notificationDetails
									.setIsAction(buApproveCapexSysAdminKey
											.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(buApproveCapexSysAdminKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat
									.format(capexApprovedByBUToSysAdminIndividualMsg,
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getMasBuUnitBO()
															.getFkBuHeadEmpId()),
											assetRequests
													.get(0)
													.getPurchaseMasAssetsBO()
													.getAssetNameAndVersionOrModel(),
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getRequestorId()));
							if (!isDuplicatesFound.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
								isDuplicatesFound
										.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= assetRequests.get(0)
									.getModifiedOn().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(assetRequests
												.get(0).getModifiedOn());
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					} else {
						String fullmsgIdentifierKey = msgIdentifierKey + "_"
								+ empId;
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

						isDuplicatesFound = gsrNotificationService
								.checkDuplicates(empId, fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(capexApprovedByBUToSysAdminConsolidatedMsg,
											assetRequests.size(),
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getMasBuUnitBO()
															.getFkBuHeadEmpId()));
							Date maxDate = assetRequestList
									.stream()
									.map(PurchaseDatAssetRequestBO::getModifiedOn)
									.max(Date::compareTo).get();
							notificationDetails.setEmpId(empId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(maxDate);
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
							notificationDetails
									.setMenuUrl(buApproveCapexSysAdminKey
											.split(",")[0]);
							notificationDetails
									.setIsAction(buApproveCapexSysAdminKey
											.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(buApproveCapexSysAdminKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat
									.format(capexApprovedByBUToSysAdminConsolidatedMsg,
											assetRequests.size(),
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getMasBuUnitBO()
															.getFkBuHeadEmpId()));
							Date maxDate = assetRequestList
									.stream()
									.map(PurchaseDatAssetRequestBO::getModifiedOn)
									.max(Date::compareTo).get();
							if (!isDuplicatesFound.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
								isDuplicatesFound
										.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= maxDate.getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(maxDate);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					}
				}
			} else {
				String menuURL = buApproveCapexSysAdminKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									buApproveCapexSysAdminKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setCapexApprovedNotificationToSysAdminScreen() method");
	}

	/**
	 * 
	 * <Description setCapexApprovedNotificationToSysAdminScreen:> This method
	 * is used to set Capex Rejected Notification to Employee Screen when
	 * Employee logs in.
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setCapexRejectedNotificationToEmployeeScreen(int empId)
			throws DataAccessException {
		LOG.startUsecase("Entering setCapexRejectedNotificationToEmployeeScreen() method");
		String buRejectCapexEmployeeKey = buRejectCapexEmployeeKeys;
		String msgIdentifierKey = buRejectCapexEmployeeKey.split(",")[2];
		List<PurchaseDatAssetRequestBO> assetRequestList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			assetRequestList = assetRequestRepository
					.findByRequestorIdAndPurchaseRequestStatus(
							empId,
							PurchaseNotificationConstants.CAPEX_REJECTED_BY_BUHEAD);

			if (assetRequestList.size() > 0) {
				for (PurchaseDatAssetRequestBO assetRequest : assetRequestList) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + "_"
							+ assetRequest.getPkPurchaseRequestId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								capexRejectedByBUToEmployeeIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(assetRequest
												.getModifiedBy()), assetRequest
										.getPurchaseMasAssetsBO()
										.getAssetNameAndVersionOrModel());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(assetRequest
										.getModifiedOn());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
						notificationDetails.setMenuUrl(buRejectCapexEmployeeKey
								.split(",")[0]);
						notificationDetails
								.setIsAction(buRejectCapexEmployeeKey
										.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails
								.setNotificationExpiryTime(Integer
										.parseInt(buRejectCapexEmployeeKey
												.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								capexRejectedByBUToEmployeeIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(assetRequest
												.getModifiedBy()), assetRequest
										.getPurchaseMasAssetsBO()
										.getAssetNameAndVersionOrModel());

						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= assetRequest.getModifiedOn()
								.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(assetRequest
											.getModifiedOn());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = buRejectCapexEmployeeKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									buRejectCapexEmployeeKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setCapexRejectedNotificationToEmployeeScreen() method");
	}

	/**
	 * 
	 * <Description setCapexRejectedNotificationToRMScreen:> This method is used
	 * to set Capex Rejected Notification to RM Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setCapexRejectedNotificationToRMScreen(int empId)
			throws DataAccessException {
		LOG.startUsecase("Entering setCapexRejectedNotificationToRMScreen() method");
		String buRejectCapexRMKey = buRejectCapexRMKeys;
		String msgIdentifierKey = buRejectCapexRMKey.split(",")[2];
		List<PurchaseDatAssetRequestBO> assetRequestList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		Map<Integer, List<PurchaseDatAssetRequestBO>> assetRequestsMapByBuId = new HashMap<Integer, List<PurchaseDatAssetRequestBO>>();
		int updateCount = 0;
		try {

			assetRequestList = assetRequestRepository
					.findByPurchaseRequestStatusAndRmToApprove(
							PurchaseNotificationConstants.CAPEX_REJECTED_BY_BUHEAD,
							empId);

			if (assetRequestList.size() > 0) {
				for (PurchaseDatAssetRequestBO assetRequest : assetRequestList) {
					if (!assetRequestsMapByBuId.containsKey(assetRequest
							.getModifiedBy())) {
						List<PurchaseDatAssetRequestBO> finalBean = new ArrayList<PurchaseDatAssetRequestBO>();
						finalBean.add(assetRequest);
						assetRequestsMapByBuId.put(
								assetRequest.getModifiedBy(), finalBean);
						continue;
					}
					if (assetRequestsMapByBuId.containsKey(assetRequest
							.getModifiedBy())) {
						List<PurchaseDatAssetRequestBO> finalBean = new ArrayList<PurchaseDatAssetRequestBO>();
						finalBean.add(assetRequest);
						assetRequestsMapByBuId
								.get(assetRequest.getModifiedBy()).addAll(
										finalBean);
					}
				}
				for (Map.Entry<Integer, List<PurchaseDatAssetRequestBO>> entry : assetRequestsMapByBuId
						.entrySet()) {
					List<PurchaseDatAssetRequestBO> assetRequests = (List<PurchaseDatAssetRequestBO>) entry
							.getValue();

					if (assetRequests.size() == 1) {
						String fullmsgIdentifierKey = msgIdentifierKey + "_"
								+ empId;
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

						isDuplicatesFound = gsrNotificationService
								.checkDuplicates(empId, fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(capexRejectedByBUToRmScreenIndividualMsg,
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getMasBuUnitBO()
															.getFkBuHeadEmpId()),
											assetRequests
													.get(0)
													.getPurchaseMasAssetsBO()
													.getAssetNameAndVersionOrModel(),
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getRequestorId()));
							notificationDetails.setEmpId(empId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(assetRequests
											.get(0).getModifiedOn());
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
							notificationDetails.setMenuUrl(buRejectCapexRMKey
									.split(",")[0]);
							notificationDetails.setIsAction(buRejectCapexRMKey
									.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(buRejectCapexRMKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat
									.format(capexRejectedByBUToRmScreenIndividualMsg,
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getMasBuUnitBO()
															.getFkBuHeadEmpId()),
											assetRequests
													.get(0)
													.getPurchaseMasAssetsBO()
													.getAssetNameAndVersionOrModel(),
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getRequestorId()));
							if (!isDuplicatesFound.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
								isDuplicatesFound
										.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= assetRequests.get(0)
									.getModifiedOn().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(assetRequests
												.get(0).getModifiedOn());
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					} else {
						String fullmsgIdentifierKey = msgIdentifierKey + "_"
								+ empId;
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

						isDuplicatesFound = gsrNotificationService
								.checkDuplicates(empId, fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(capexRejectedByBUToRmConsolidatedMsg,
											assetRequests.size(),
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getMasBuUnitBO()
															.getFkBuHeadEmpId()));
							Date maxDate = assetRequestList
									.stream()
									.map(PurchaseDatAssetRequestBO::getModifiedOn)
									.max(Date::compareTo).get();
							notificationDetails.setEmpId(empId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(maxDate);
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
							notificationDetails.setMenuUrl(buRejectCapexRMKey
									.split(",")[0]);
							notificationDetails.setIsAction(buRejectCapexRMKey
									.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(buRejectCapexRMKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat
									.format(capexRejectedByBUToRmConsolidatedMsg,
											assetRequests.size(),
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getMasBuUnitBO()
															.getFkBuHeadEmpId()));
							Date maxDate = assetRequestList
									.stream()
									.map(PurchaseDatAssetRequestBO::getModifiedOn)
									.max(Date::compareTo).get();
							if (!isDuplicatesFound.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
								isDuplicatesFound
										.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= maxDate.getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(maxDate);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					}
				}
			} else {
				String menuURL = buRejectCapexRMKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									buRejectCapexRMKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setCapexRejectedNotificationToRMScreen() method");
	}

	/**
	 * 
	 * <Description setCapexRejectedNotificationToRMScreen:> This method is used
	 * to set Capex Rejected Notification to System Admin Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setCapexRejectedNotificationToSysAdminScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setCapexRejectedNotificationToSysAdminScreen() method");
		String buRejectCapexSysAdminKey = buRejectCapexSysAdminKeys;
		String msgIdentifierKey = buRejectCapexSysAdminKey.split(",")[2];
		List<PurchaseDatAssetRequestBO> assetRequestList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		Map<Integer, List<PurchaseDatAssetRequestBO>> assetRequestsMapByBuId = new HashMap<Integer, List<PurchaseDatAssetRequestBO>>();
		int updateCount = 0;
		try {

			assetRequestList = assetRequestRepository
					.findByPurchaseRequestStatus(PurchaseNotificationConstants.CAPEX_REJECTED_BY_BUHEAD);

			if (assetRequestList.size() > 0) {
				for (PurchaseDatAssetRequestBO assetRequest : assetRequestList) {
					if (!assetRequestsMapByBuId.containsKey(assetRequest
							.getModifiedBy())) {
						List<PurchaseDatAssetRequestBO> finalBean = new ArrayList<PurchaseDatAssetRequestBO>();
						finalBean.add(assetRequest);
						assetRequestsMapByBuId.put(
								assetRequest.getModifiedBy(), finalBean);
						continue;
					}
					if (assetRequestsMapByBuId.containsKey(assetRequest
							.getModifiedBy())) {
						List<PurchaseDatAssetRequestBO> finalBean = new ArrayList<PurchaseDatAssetRequestBO>();
						finalBean.add(assetRequest);
						assetRequestsMapByBuId
								.get(assetRequest.getModifiedBy()).addAll(
										finalBean);
					}
				}
				for (Map.Entry<Integer, List<PurchaseDatAssetRequestBO>> entry : assetRequestsMapByBuId
						.entrySet()) {
					List<PurchaseDatAssetRequestBO> assetRequests = (List<PurchaseDatAssetRequestBO>) entry
							.getValue();

					if (assetRequests.size() == 1) {
						String fullmsgIdentifierKey = msgIdentifierKey + "_"
								+ empId;
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

						isDuplicatesFound = gsrNotificationService
								.checkDuplicates(empId, fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(capexRejectedByBUToRmScreenIndividualMsg,
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getMasBuUnitBO()
															.getFkBuHeadEmpId()),
											assetRequests
													.get(0)
													.getPurchaseMasAssetsBO()
													.getAssetNameAndVersionOrModel(),
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getRequestorId()));
							notificationDetails.setEmpId(empId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(assetRequests
											.get(0).getModifiedOn());
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
							notificationDetails
									.setMenuUrl(buRejectCapexSysAdminKey
											.split(",")[0]);
							notificationDetails
									.setIsAction(buRejectCapexSysAdminKey
											.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(buRejectCapexSysAdminKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat
									.format(capexRejectedByBUToRmScreenIndividualMsg,
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getMasBuUnitBO()
															.getFkBuHeadEmpId()),
											assetRequests
													.get(0)
													.getPurchaseMasAssetsBO()
													.getAssetNameAndVersionOrModel(),
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getRequestorId()));
							if (!isDuplicatesFound.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
								isDuplicatesFound
										.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= assetRequests.get(0)
									.getModifiedOn().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(assetRequests
												.get(0).getModifiedOn());
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					} else {
						String fullmsgIdentifierKey = msgIdentifierKey + "_"
								+ empId;
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

						isDuplicatesFound = gsrNotificationService
								.checkDuplicates(empId, fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(capexRejectedByBUToRmConsolidatedMsg,
											assetRequests.size(),
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getMasBuUnitBO()
															.getFkBuHeadEmpId()));
							Date maxDate = assetRequestList
									.stream()
									.map(PurchaseDatAssetRequestBO::getModifiedOn)
									.max(Date::compareTo).get();
							notificationDetails.setEmpId(empId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(maxDate);
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
							notificationDetails
									.setMenuUrl(buRejectCapexSysAdminKey
											.split(",")[0]);
							notificationDetails
									.setIsAction(buRejectCapexSysAdminKey
											.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(buRejectCapexSysAdminKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat
									.format(capexRejectedByBUToRmConsolidatedMsg,
											assetRequests.size(),
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequests
															.get(0)
															.getMasBuUnitBO()
															.getFkBuHeadEmpId()));
							Date maxDate = assetRequestList
									.stream()
									.map(PurchaseDatAssetRequestBO::getModifiedOn)
									.max(Date::compareTo).get();
							if (!isDuplicatesFound.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
								isDuplicatesFound
										.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= maxDate.getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(maxDate);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					}
				}
			} else {
				String menuURL = buRejectCapexSysAdminKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									buRejectCapexSysAdminKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setCapexRejectedNotificationToSysAdminScreen() method");
	}

	/**
	 * 
	 * <Description setCapexRejectedNotificationToRMScreen:> This method is used
	 * to set Capex Updated Notification to BU Head Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setCapexUpdatedNotificationToBUHeadScreen(int assetReqId)
			throws DataAccessException {

		LOG.startUsecase("Entering setCapexUpdatedNotificationToBUHeadScreen() method");
		PurchaseDatAssetQuoteBO assetQuotes;
		List<PurchaseDatAssetRequestBO> assetRequestList = new ArrayList<PurchaseDatAssetRequestBO>();
		MasBuUnitBO buDetail;
		MasBuUnitBO itBuDetail;
		try {
			assetQuotes = assetQuoteRepository
					.findByFkPurchaseRequestIdAndQuoteStatus(
							assetReqId,
							PurchaseNotificationConstants.ASSET_QUOTE_STATUS_AGREE);

			assetRequestList.add(assetQuotes.getPurchaseDatAssetRequestBO());

			if (assetQuotes != null) {
				if (assetQuotes.getPurchaseDatAssetRequestBO()
						.getPurchaseDatCapexBO().getModifiedOn() != null) {
					if (assetQuotes.getCostBorneBy().equalsIgnoreCase(
							PurchaseNotificationConstants.COST_BORNE_BY_IT)) {

						buDetail = buRepository
								.findByPkBuUnitId((short) Integer
										.parseInt(itBUId));

						setUpdatedNotificationToBUHead(buDetail.getFkBuHeadEmpId(),
								assetRequestList);
					}
					if (assetQuotes.getCostBorneBy().equalsIgnoreCase(
							PurchaseNotificationConstants.COST_BORNE_BY_BU)) {

						buDetail = buRepository.findByPkBuUnitId(assetQuotes
								.getPurchaseDatAssetRequestBO().getFkBuId());
						setUpdatedNotificationToBUHead(buDetail.getFkBuHeadEmpId(),
								assetRequestList);

					}
					if (assetQuotes.getCostBorneBy().equalsIgnoreCase(
							PurchaseNotificationConstants.COST_BORNE_BY_SHARED)) {

						itBuDetail = buRepository
								.findByPkBuUnitId((short) Integer
										.parseInt(itBUId));

						setUpdatedNotificationToBUHead(itBuDetail.getFkBuHeadEmpId(),
								assetRequestList);
						assetRequestList.clear();

						if (itBuDetail.getPkBuUnitId() != assetQuotes
								.getPurchaseDatAssetRequestBO().getFkBuId()) {

							buDetail = buRepository
									.findByPkBuUnitId(assetQuotes
											.getPurchaseDatAssetRequestBO()
											.getFkBuId());

							setUpdatedNotificationToBUHead(
									buDetail.getFkBuHeadEmpId(),
									assetRequestList);
						}
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setCapexUpdatedNotificationToBUHeadScreen() method");
	}

	/**
	 * <Description setCapexCreatedNotificationToBUHeadScreen:> This method is
	 * used to set Capex Updated Notification to BU Head Screen
	 * 
	 * @param buEmpId
	 * @param assetRequestList
	 * @throws DataAccessException
	 */
	public void setUpdatedNotificationToBUHead(int buEmpId,
			List<PurchaseDatAssetRequestBO> assetRequestList)
			throws DataAccessException { 
		LOG.startUsecase("Entering setUpdatedNotificationToBUHead() method");
		String sysAdminUpdCapexKey = sysAdminUpdCapexKeys;
		String msgIdentifierKey = sysAdminUpdCapexKey.split(",")[2];
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			if (assetRequestList.size() > 0) {
				String fullmsgIdentifierKey = msgIdentifierKey + "_" + buEmpId;
				fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

				isDuplicatesFound = gsrNotificationService.checkDuplicates(
						buEmpId, fullmsgIdentifierKey);

				if (isDuplicatesFound == null) {
					DatNotificationsBO notificationDetails = new DatNotificationsBO();
					String notificationMessage = MessageFormat.format(
							capexUpdatedBySysAdminToBUHeadIndividualMsg,
							assetRequestList.get(0).getPurchaseMasAssetsBO()
									.getAssetNameAndVersionOrModel(),
							gsrNotificationService
									.getEmployeeNameByEmpId(assetRequestList
											.get(0).getRequestorId()));
					notificationDetails.setEmpId(buEmpId);
					notificationDetails
							.setNoificationMessage(notificationMessage);
					notificationDetails
							.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
					notificationDetails
							.setNotificationcreatedDate(assetRequestList.get(0)
									.getModifiedOn());
					notificationDetails.setClearFlagModifiedDate(new Date());
					notificationDetails.setClearFlagcreatedDate(new Date());
					notificationDetails
							.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
					notificationDetails.setMenuUrl(sysAdminUpdCapexKey
							.split(",")[0]);
					notificationDetails.setIsAction(sysAdminUpdCapexKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(sysAdminUpdCapexKey.split(",")[3]));
					try {
						notificationRepository.save(notificationDetails);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				} else {
					String notificationMessage = MessageFormat.format(
							capexUpdatedBySysAdminToBUHeadIndividualMsg,
							assetRequestList.get(0).getPurchaseMasAssetsBO()
									.getAssetNameAndVersionOrModel(),
							gsrNotificationService
									.getEmployeeNameByEmpId(assetRequestList
											.get(0).getRequestorId()));
					if (!isDuplicatesFound.getNoificationMessage()
							.equalsIgnoreCase(notificationMessage)) {
						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound
								.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
						isDuplicatesFound
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
					}
					isDuplicatesFound.setNotificationModifiedDate(new Date());
					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= assetRequestList.get(0)
							.getModifiedOn().getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound
								.setNotificationcreatedDate(assetRequestList
										.get(0).getModifiedOn());
					try {
						notificationRepository.save(isDuplicatesFound);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				}
			} else {
				String menuURL = sysAdminUpdCapexKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(buEmpId,
									menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, buEmpId,
									sysAdminUpdCapexKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setUpdatedNotificationToBUHead() method");
	}

	/**
	 * <Description setCapexCreatedNotificationToBUHeadScreen:> This method is
	 * used to set Capex Rejected Notification to BU Head Screen
	 * 
	 * @param assetReqId
	 * @throws DataAccessException
	 */
	public void setCapexRejectedNotificationToBuHeadScreen(int assetReqId)
			throws DataAccessException {

		LOG.startUsecase("Entering setCapexRejectedNotificationToBuHeadScreen() method");
		PurchaseDatAssetQuoteBO assetQuotes;
		List<PurchaseDatAssetRequestBO> assetRequestList = new ArrayList<PurchaseDatAssetRequestBO>();
		PurchaseDatBuBO datBuDetail;
		MasBuUnitBO buDetail;
		MasBuUnitBO itBuDetail;
		String itBuRejectCapexToBuKey = itBuRejectCapexToBuKeys;
		String msgIdentifierKey = itBuRejectCapexToBuKey.split(",")[2];
		try {
			assetQuotes = assetQuoteRepository
					.findByFkPurchaseRequestIdAndQuoteStatus(
							assetReqId,
							PurchaseNotificationConstants.ASSET_QUOTE_STATUS_AGREE);

			assetRequestList.add(assetQuotes.getPurchaseDatAssetRequestBO());

			if (assetQuotes != null) {
				if (assetQuotes.getCostBorneBy().equalsIgnoreCase(
						PurchaseNotificationConstants.COST_BORNE_BY_SHARED)) {

					itBuDetail = buRepository.findByPkBuUnitId((short) Integer
							.parseInt(itBUId));

					buDetail = buRepository.findByPkBuUnitId(assetQuotes
							.getPurchaseDatAssetRequestBO().getFkBuId());

					datBuDetail = purchaseBuRepository
							.findByBuHeadIdAndFkPurchaseQuoteIdAndFkPurchaseDatBuStatus(
									(short) itBuDetail.getFkBuHeadEmpId(),
									assetQuotes.getPkQuoteId(),
									PurchaseNotificationConstants.DISAGREE_QUOTE);

					if (datBuDetail != null) {
						if (itBuDetail.getFkBuHeadEmpId() != buDetail
								.getFkBuHeadEmpId()) {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_"
									+ buDetail.getFkBuHeadEmpId()
									+ "_"
									+ assetRequestList.get(0)
											.getPkPurchaseRequestId();

							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(capexRejectedByBUToBUHeadIndividualMsg,
											assetRequestList
													.get(0)
													.getPurchaseMasAssetsBO()
													.getAssetNameAndVersionOrModel(),
											gsrNotificationService
													.getEmployeeNameByEmpId(assetRequestList
															.get(0)
															.getRequestorId()));
							notificationDetails.setEmpId(buDetail
									.getFkBuHeadEmpId());
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(assetRequestList
											.get(0).getModifiedOn());
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(PurchaseNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(PurchaseNotificationConstants.PURCHASE_MENU_ID);
							notificationDetails
									.setMenuUrl(itBuRejectCapexToBuKey
											.split(",")[0]);
							notificationDetails
									.setIsAction(itBuRejectCapexToBuKey
											.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(PurchaseNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(itBuRejectCapexToBuKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Purchase Module Notifications");
		}
		LOG.endUsecase("Exiting setCapexRejectedNotificationToBuHeadScreen() method");
	}
}
