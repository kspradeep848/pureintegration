package com.thbs.mis.notificationframework.confirmationnotification.constants;

public class ConfirmationConstants {

	public static final String MODULE_NAME = "CONFIRMATION";
	public static final short MENU_ID = 31;
	public final static String CLEAR_FLAG_NO = "NO";
	public static final String IS_EXPIRY_NO = "NOTEXPIRED";

	public static final String CONFIRMED_BY_RM = "1";
	public static final int PENDING_WITH_MANAGER = 2;
	public static final int PENDING_WITH_HR = 3;
	public static final int EXTENDED_PROBATION = 4;
	public static final int CONFIRMED = 5;
}
