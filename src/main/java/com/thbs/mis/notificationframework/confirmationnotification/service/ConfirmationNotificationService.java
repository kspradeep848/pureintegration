package com.thbs.mis.notificationframework.confirmationnotification.service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO;
import com.thbs.mis.confirmation.dao.ConfirmationCommonRepository;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.confirmationnotification.constants.ConfirmationConstants;
import com.thbs.mis.notificationframework.gsrnotification.service.GSRNotificationService;
import com.thbs.mis.notificationframework.notificationsettings.bo.DatNotificationsBO;
import com.thbs.mis.notificationframework.notificationsettings.dao.NotificationRepository;
import com.thbs.mis.notificationframework.puchasenotification.constants.PurchaseNotificationConstants;

@Service
public class ConfirmationNotificationService {

	private static final AppLog LOG = LogFactory
			.getLog(GSRNotificationService.class);

	/*
	 * Individual Notification Messages from ClientMessages.properties file
	 */
	@Value("${ui.message.feedbackProvidedByRmToEmpScreen}")
	String feedbackProvidedByRMToEmpScreenIndividualMsg;

	@Value("${ui.message.feedbackProvidedByRmToHRScreen}")
	String feedbackProvidedByRMToHRScreenIndividualMsg;

	@Value("${ui.message.feedbackProvidedByDelRmToEmpScreen}")
	String feedbackProvidedByDelRmToEmpScreenIndividualMsg;

	@Value("${ui.message.feedbackProvidedByDelRmToRmScreen}")
	String feedbackProvidedByDelRmToRmScreenIndividualMsg;

	@Value("${ui.message.feedbackProvidedByDelRmToHRScreen}")
	String feedbackProvidedByDelRmToHRScreenIndividualMsg;

	@Value("${ui.message.confirmationExtendedByHrToEmpScreen}")
	String confirmationExtendedByHrToEmpScreenIndividualMsg;

	@Value("${ui.message.confirmationExtendedByHrToRmScreen}")
	String confirmationExtendedByHrToRmScreenIndividualMsg;

	@Value("${ui.message.confirmationExtendedByHrToDelRmScreen}")
	String confirmationExtendedByHrToDelRmScreenIndividualMsg;

	@Value("${ui.message.confirmationDelegatedByHrToEmpScreen}")
	String confirmationDelegatedByHrToEmpScreenIndividualMsg;

	@Value("${ui.message.confirmationDelegatedByHrToRMScreen}")
	String confirmationDelegatedByHrToRmScreenIndividualMsg;

	@Value("${ui.message.confirmationDelegatedByHrToDelRmScreen}")
	String confirmationDelegatedByHrToDelRmScreenIndividualMsg;

	@Value("${ui.message.confirmedByHrToEmpScreen}")
	String confirmedByHrToEmpScreenIndividualMsg;

	@Value("${ui.message.confirmedByHrToRmScreen}")
	String confirmedByHrToRmScreenIndividualMsg;

	@Value("${ui.message.confirmedByHrToDelRmScreen}")
	String confirmedByHrToDelRmScreenIndividualMsg;

	@Value("${ui.message.feedbackProvidedByRmToDelRmScreen}")
	String feedbackProvidedByRmToDelRmScreenIndividualMsg;

	/*
	 * Consolidated Notification Messages from ClientMessages.properties file
	 */
	@Value("${ui.message.feedbackProvidedByRmToHRConsolidated}")
	String feedbackProvidedByRmToHRConsolidatedMsg;

	@Value("${ui.message.feedbackProvidedByDelRmtoRmConsolidated}")
	String feedbackProvidedByDelRmToRmConsolidatedMsg;

	@Value("${ui.message.feedbackProvidedByDelRmtoHRConsolidated}")
	String feedbackProvidedByDelRmToHRConsolidatedMsg;

	@Value("${ui.message.confirmationExtendedByHrToRmConsolidated}")
	String confirmationExtendedByHrToRmConsolidatedMsg;

	@Value("${ui.message.confirmationExtendedByHrToDelRmConsolidated}")
	String confirmationExtendedByHrToDelRmConsolidatedMsg;

	@Value("${ui.message.confirmationDelegatedByHrToRmConsolidated}")
	String confirmationDelegatedByHrToRmConsolidatedMsg;

	@Value("${ui.message.confirmationDelegatedByHrToDelRmConsolidated}")
	String confirmationDelegatedByHrToDelRmConsolidatedMsg;

	@Value("${ui.message.confirmedByHrToRmConsolidated}")
	String confirmedByHrToRmConsolidatedMsg;

	@Value("${ui.message.confirmedByHrToDelRmConsolidated}")
	String confirmedByHrToDelRmConsolidatedMsg;

	@Value("${ui.message.feedbackProvidedByRmToDelRmConsolidated}")
	String feedbackProvidedByRmToDelRmConsolidatedMsg;

	/*
	 * Purchase Notification Keys from notificationframework.properties file
	 */
	@Value("${CONF_RM_FEEDBACK_EMP}")
	String rmFeedbackToEmpKeys;

	@Value("${CONF_RM_FEEDBACK_HR}")
	String rmFeedbackToHRKeys;

	@Value("${CONF_DELRM_FEEDBACK_EMP}")
	String delRmFeedbackEmpKeys;

	@Value("${CONF_DELRM_FEEDBACK_RM}")
	String delRmFeedbackRmKeys;

	@Value("${CONF_DELRM_FEEDBACK_HR}")
	String delRmFeedbackHRKeys;

	@Value("${CONF_EXTENDED_HR_EMP}")
	String confExtendByHrToEmpKeys;

	@Value("${CONF_EXTENDED_HR_RM}")
	String confExtendByHrToRmKeys;

	@Value("${CONF_EXTENDED_HR_DELMGR}")
	String confExtendByHrToDelRmKeys;

	@Value("${CONF_DELEGATED_HR_EMP}")
	String confDelegatedByHrToEmpKeys;

	@Value("${CONF_DELEGATED_HR_RM}")
	String confDelegatedByHrToRmKeys;

	@Value("${CONF_DELEGATED_HR_DELRM}")
	String confDelegatedByHrToDelRmKeys;

	@Value("${CONF_HR_CONFIRM_EMP}")
	String confirmedByHrToEmpKeys;

	@Value("${CONF_HR_CONFIRM_RM}")
	String confirmedByHrToRmKeys;

	@Value("${CONF_HR_CONFIRM_DELRM}")
	String confirmedByHrToDelRmKeys;

	@Value("${CONF_RM_FEEDBACK_DELRM}")
	String rmFeedbackToDelRmKeys;

	/*
	 * Autowiring Repositories
	 */
	@Autowired
	private ConfirmationCommonRepository confProbRepository;

	@Autowired
	private NotificationRepository notificationRepository;

	@Autowired
	GSRNotificationService gsrNotificationService;

	/**
	 * 
	 * <Description setFeedbackProvidedNotificationByRMToEmpScreen:> This method
	 * is used to set Feedback Provided Notification to Employee Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setFeedbackProvidedNotificationByRMToEmpScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setFeedbackProvidedNotificationByRMToEmpScreen()");
		String rmFeedbackToEmpKey = rmFeedbackToEmpKeys;
		String msgIdentifierKey = rmFeedbackToEmpKey.split(",")[2];
		ConfDatProbationConfirmationBO confDetail;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			confDetail = confProbRepository
					.findByFkEmpIdAndFkConfStatusIdAndIsConfirmedByMgr(empId,
							ConfirmationConstants.PENDING_WITH_HR,
							ConfirmationConstants.CONFIRMED_BY_RM);

			if (confDetail != null) {
				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
				fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

				isDuplicatesFound = gsrNotificationService.checkDuplicates(
						empId, fullmsgIdentifierKey);

				if (isDuplicatesFound == null) {
					DatNotificationsBO notificationDetails = new DatNotificationsBO();
					notificationDetails.setEmpId(empId);
					notificationDetails
							.setNoificationMessage(feedbackProvidedByRMToEmpScreenIndividualMsg);
					notificationDetails
							.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
					notificationDetails.setNotificationcreatedDate(confDetail
							.getUpdatedOn());
					notificationDetails.setClearFlagModifiedDate(new Date());
					notificationDetails.setClearFlagcreatedDate(new Date());
					notificationDetails
							.setModuleName(ConfirmationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(ConfirmationConstants.MENU_ID);
					notificationDetails.setMenuUrl(rmFeedbackToEmpKey
							.split(",")[0]);
					notificationDetails.setIsAction(rmFeedbackToEmpKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(rmFeedbackToEmpKey.split(",")[3]));
					try {
						notificationRepository.save(notificationDetails);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				} else {
					isDuplicatesFound
							.setNoificationMessage(feedbackProvidedByRMToEmpScreenIndividualMsg);

					isDuplicatesFound.setNotificationModifiedDate(new Date());
					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= confDetail.getUpdatedOn().getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound.setNotificationcreatedDate(confDetail
								.getUpdatedOn());
					try {
						notificationRepository.save(isDuplicatesFound);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				}
			} else {
				String menuURL = rmFeedbackToEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									rmFeedbackToEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in setting Feedback Provided by RM to Emp Notifications");
		}
		LOG.endUsecase("Exiting setFeedbackProvidedNotificationByRMToEmpScreen()");
	}

	/**
	 * 
	 * <Description setFeedbackProvidedNotificationByRMToHRScreen:> This method
	 * is used to set Feedback Provided By RM to HR Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setFeedbackProvidedNotificationByRMToHRScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setFeedbackProvidedNotificationByRMToEmpScreen()");
		String rmFeedbackToHRKey = rmFeedbackToHRKeys;
		String msgIdentifierKey = rmFeedbackToHRKey.split(",")[2];
		List<ConfDatProbationConfirmationBO> confProbDetailList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		Map<Integer, List<ConfDatProbationConfirmationBO>> confDetailsMapByRmId = new HashMap<Integer, List<ConfDatProbationConfirmationBO>>();
		int updateCount = 0;
		try {
			confProbDetailList = confProbRepository
					.findByFkConfStatusIdAndIsConfirmedByMgr(
							ConfirmationConstants.PENDING_WITH_HR,
							ConfirmationConstants.CONFIRMED_BY_RM);

			if (confProbDetailList.size() > 0) {

				for (ConfDatProbationConfirmationBO confDetail : confProbDetailList) {
					if (!confDetailsMapByRmId.containsKey(confDetail
							.getFkMgrId())) {
						List<ConfDatProbationConfirmationBO> finalBean = new ArrayList<ConfDatProbationConfirmationBO>();
						finalBean.add(confDetail);
						confDetailsMapByRmId.put(confDetail.getFkMgrId(),
								finalBean);
						continue;
					}
					if (confDetailsMapByRmId.containsKey(confDetail
							.getFkMgrId())) {
						List<ConfDatProbationConfirmationBO> finalBean = new ArrayList<ConfDatProbationConfirmationBO>();
						finalBean.add(confDetail);
						confDetailsMapByRmId.get(confDetail.getFkMgrId())
								.addAll(finalBean);
					}
				}

				for (Map.Entry<Integer, List<ConfDatProbationConfirmationBO>> entry : confDetailsMapByRmId
						.entrySet()) {
					List<ConfDatProbationConfirmationBO> confDetails = (List<ConfDatProbationConfirmationBO>) entry
							.getValue();

					if (confDetails.size() == 1) {
						String fullmsgIdentifierKey = msgIdentifierKey + "_"
								+ empId + "_" +confDetails.get(0).getFkMgrId();
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

						isDuplicatesFound = gsrNotificationService
								.checkDuplicates(empId, fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(feedbackProvidedByRMToHRScreenIndividualMsg,
											gsrNotificationService
													.getEmployeeNameByEmpId(confDetails
															.get(0)
															.getFkMgrId()),
											gsrNotificationService
													.getEmployeeNameByEmpId(confDetails
															.get(0)
															.getFkEmpId()));
							notificationDetails.setEmpId(empId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(confDetails
											.get(0).getUpdatedOn());
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(ConfirmationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(ConfirmationConstants.MENU_ID);
							notificationDetails.setMenuUrl(rmFeedbackToHRKey
									.split(",")[0]);
							notificationDetails.setIsAction(rmFeedbackToHRKey
									.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(rmFeedbackToHRKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat
									.format(feedbackProvidedByRMToHRScreenIndividualMsg,
											gsrNotificationService
													.getEmployeeNameByEmpId(confDetails
															.get(0)
															.getFkMgrId()),
											gsrNotificationService
													.getEmployeeNameByEmpId(confDetails
															.get(0)
															.getFkEmpId()));
							if (!isDuplicatesFound.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
								isDuplicatesFound
										.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= confDetails.get(0)
									.getUpdatedOn().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(confDetails
												.get(0).getUpdatedOn());
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					} else {
						String fullmsgIdentifierKey = msgIdentifierKey + "_"
								+ empId + "_"+ confDetails.get(0).getFkMgrId();
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

						isDuplicatesFound = gsrNotificationService
								.checkDuplicates(empId, fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat.format(
									feedbackProvidedByRmToHRConsolidatedMsg,
									gsrNotificationService
											.getEmployeeNameByEmpId(confDetails
													.get(0).getFkMgrId()),
									confDetails.size());

							Date maxDate = confDetails
									.stream()
									.map(ConfDatProbationConfirmationBO::getUpdatedOn)
									.max(Date::compareTo).get();
							notificationDetails.setEmpId(empId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(maxDate);
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(ConfirmationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(ConfirmationConstants.MENU_ID);
							notificationDetails.setMenuUrl(rmFeedbackToHRKey
									.split(",")[0]);
							notificationDetails.setIsAction(rmFeedbackToHRKey
									.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(rmFeedbackToHRKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat.format(
									feedbackProvidedByRmToHRConsolidatedMsg,
									gsrNotificationService
											.getEmployeeNameByEmpId(confDetails
													.get(0).getFkMgrId()),
									confDetails.size());
							Date maxDate = confDetails
									.stream()
									.map(ConfDatProbationConfirmationBO::getUpdatedOn)
									.max(Date::compareTo).get();
							if (!isDuplicatesFound.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
								isDuplicatesFound
										.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= maxDate.getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(maxDate);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					}
				}
			} else {
				String menuURL = rmFeedbackToHRKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									rmFeedbackToHRKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in setting Feedback Provided by RM to HR Notifications");
		}
		LOG.endUsecase("Exiting setFeedbackProvidedNotificationByRMToEmpScreen()");
	}

	/**
	 * 
	 * <Description setFeedbackProvidedByDelMgrToEmpScreen:> This method is used
	 * to set Feedback Provided by Delegated Manager Notification to Employee
	 * Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setFeedbackProvidedByDelMgrToEmpScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setFeedbackProvidedByDelMgrToEmpScreen()");
		String delRmFeedbackEmpKey = delRmFeedbackEmpKeys;
		String msgIdentifierKey = delRmFeedbackEmpKey.split(",")[2];
		ConfDatProbationConfirmationBO confDetail;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			confDetail = confProbRepository
					.findByFkEmpIdAndFkConfStatusIdAndIsConfirmedByMgrAndDelegatedToMgrIdIsNotNull(
							empId, ConfirmationConstants.PENDING_WITH_HR,
							ConfirmationConstants.CONFIRMED_BY_RM);

			if (confDetail != null) {

				if (confDetail.getDelegatedToMgrId().equals(
						confDetail.getConfirmedByMgrId())) {

					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						String notificationMessage = MessageFormat
								.format(feedbackProvidedByDelRmToEmpScreenIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetail
														.getDelegatedToMgrId()));
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(confDetail
										.getUpdatedOn());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(ConfirmationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ConfirmationConstants.MENU_ID);
						notificationDetails.setMenuUrl(delRmFeedbackEmpKey
								.split(",")[0]);
						notificationDetails.setIsAction(delRmFeedbackEmpKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(delRmFeedbackEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(feedbackProvidedByDelRmToEmpScreenIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetail
														.getDelegatedToMgrId()));
						isDuplicatesFound
								.setNoificationMessage(notificationMessage);

						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= confDetail.getUpdatedOn()
								.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(confDetail
											.getUpdatedOn());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					String menuURL = delRmFeedbackEmpKey.split(",")[0];
					try {
						notificationIdList = notificationRepository
								.getNotificationIdByEmpIdAndMenuURL(empId,
										menuURL);
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
					}
					if (notificationIdList.size() != 0) {
						try {
							updateCount = notificationRepository
									.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
							if (updateCount == 0)
								throw new DataAccessException(
										"Some exception occured while updating notification table");
						} catch (Exception e) {
							throw new DataAccessException(
									"Error Occurred while updating Expiry in Notification Table by notification id.");
						}
					}
				}
				if (fullMsgIdentifierKeys.size() > 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
										fullMsgIdentifierKeys, empId,
										delRmFeedbackEmpKey.split(",")[0]);
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in setting Feedback Provided by RM to Emp Notifications");
		}
		LOG.endUsecase("Exiting setFeedbackProvidedByDelMgrToEmpScreen()");
	}

	/**
	 * 
	 * <Description setFeedbackProvidedByDelRmToRmScreen:> This method is used
	 * to set Feedback provided by Delegated Mgr Notification to RM Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setFeedbackProvidedByDelRmToRmScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setFeedbackProvidedByDelRmToRmScreen() method");
		String delRmFeedbackRmKey = delRmFeedbackRmKeys;
		String msgIdentifierKey = delRmFeedbackRmKey.split(",")[2];
		List<ConfDatProbationConfirmationBO> confDetailsList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			confDetailsList = confProbRepository
					.findByFkMgrIdAndFkConfStatusIdAndIsConfirmedByMgrAndDelegatedToMgrIdIsNotNull(
							empId, ConfirmationConstants.PENDING_WITH_HR,
							ConfirmationConstants.CONFIRMED_BY_RM);

			if (confDetailsList.size() > 0) {

				for (ConfDatProbationConfirmationBO confDetail : confDetailsList) {
					if (!confDetail.getDelegatedToMgrId().equals(
							confDetail.getConfirmedByMgrId())) {
						confDetailsList.remove(confDetail);
					}
				}

				if (confDetailsList.size() == 1) {

					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								feedbackProvidedByDelRmToRmScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(confDetailsList
												.get(0).getDelegatedToMgrId()),
								gsrNotificationService
										.getEmployeeNameByEmpId(confDetailsList
												.get(0).getFkEmpId()));
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(confDetailsList
										.get(0).getUpdatedOn());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(ConfirmationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ConfirmationConstants.MENU_ID);
						notificationDetails.setMenuUrl(delRmFeedbackRmKey
								.split(",")[0]);
						notificationDetails.setIsAction(delRmFeedbackRmKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(delRmFeedbackRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								feedbackProvidedByDelRmToRmScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(confDetailsList
												.get(0).getDelegatedToMgrId()),
								gsrNotificationService
										.getEmployeeNameByEmpId(confDetailsList
												.get(0).getFkEmpId()));
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= confDetailsList.get(0)
								.getUpdatedOn().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(confDetailsList
											.get(0).getUpdatedOn());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}

				} else {

					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						Date maxDate = confDetailsList
								.stream()
								.map(ConfDatProbationConfirmationBO::getUpdatedOn)
								.max(Date::compareTo).get();
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								feedbackProvidedByDelRmToRmConsolidatedMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(confDetailsList
												.get(0).getDelegatedToMgrId()),
								confDetailsList.size());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(maxDate);
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(ConfirmationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ConfirmationConstants.MENU_ID);
						notificationDetails.setMenuUrl(delRmFeedbackRmKey
								.split(",")[0]);
						notificationDetails.setIsAction(delRmFeedbackRmKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(delRmFeedbackRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								feedbackProvidedByDelRmToRmConsolidatedMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(confDetailsList
												.get(0).getDelegatedToMgrId()),
								confDetailsList.size());
						Date maxDate = confDetailsList
								.stream()
								.map(ConfDatProbationConfirmationBO::getUpdatedOn)
								.max(Date::compareTo).get();
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(maxDate);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = delRmFeedbackRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									delRmFeedbackRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Confirmation Module Notifications");
		}
		LOG.endUsecase("Exiting setFeedbackProvidedByDelRmToRmScreen() method");
	}

	/**
	 * 
	 * <Description setFeedbackProvidedByDelRmToHrScreen:> This method is used
	 * to set Feedback Provided by Delegated RM Notification to HR Screen
	 * 
	 * @param hrEmpId
	 * @throws DataAccessException
	 */
	public void setFeedbackProvidedByDelRmToHrScreen(int hrEmpId)
			throws DataAccessException {

		LOG.startUsecase("Entering setFeedbackProvidedByDelRmToHrScreen()");
		String delRmFeedbackHRKey = delRmFeedbackHRKeys;
		String msgIdentifierKey = delRmFeedbackHRKey.split(",")[2];
		List<ConfDatProbationConfirmationBO> confProbDetailList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		Map<Integer, List<ConfDatProbationConfirmationBO>> confDetailsMapByRmId = new HashMap<Integer, List<ConfDatProbationConfirmationBO>>();
		int updateCount = 0;
		try {
			confProbDetailList = confProbRepository
					.findByFkConfStatusIdAndIsConfirmedByMgrAndDelegatedToMgrIdIsNotNull(
							ConfirmationConstants.PENDING_WITH_HR,
							ConfirmationConstants.CONFIRMED_BY_RM);

			if (confProbDetailList.size() > 0) {

				for (ConfDatProbationConfirmationBO confDetail : confProbDetailList) {
					if (!confDetail.getDelegatedToMgrId().equals(
							confDetail.getConfirmedByMgrId())) {
						confProbDetailList.remove(confDetail);
					}
				}

				for (ConfDatProbationConfirmationBO confDetail : confProbDetailList) {
					if (!confDetailsMapByRmId.containsKey(confDetail
							.getFkMgrId())) {
						List<ConfDatProbationConfirmationBO> finalBean = new ArrayList<ConfDatProbationConfirmationBO>();
						finalBean.add(confDetail);
						confDetailsMapByRmId.put(confDetail.getFkMgrId(),
								finalBean);
						continue;
					}
					if (confDetailsMapByRmId.containsKey(confDetail
							.getFkMgrId())) {
						List<ConfDatProbationConfirmationBO> finalBean = new ArrayList<ConfDatProbationConfirmationBO>();
						finalBean.add(confDetail);
						confDetailsMapByRmId.get(confDetail.getFkMgrId())
								.addAll(finalBean);
					}
				}

				for (Map.Entry<Integer, List<ConfDatProbationConfirmationBO>> entry : confDetailsMapByRmId
						.entrySet()) {
					List<ConfDatProbationConfirmationBO> confDetails = (List<ConfDatProbationConfirmationBO>) entry
							.getValue();

					if (confDetails.size() == 1) {
						String fullmsgIdentifierKey = msgIdentifierKey + "_"
								+ hrEmpId + "_"
								+ confDetails.get(0).getDelegatedToMgrId();
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

						isDuplicatesFound = gsrNotificationService
								.checkDuplicates(hrEmpId, fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(feedbackProvidedByDelRmToHRScreenIndividualMsg,
											gsrNotificationService
													.getEmployeeNameByEmpId(confDetails
															.get(0)
															.getDelegatedToMgrId()),
											gsrNotificationService
													.getEmployeeNameByEmpId(confDetails
															.get(0)
															.getFkEmpId()));
							notificationDetails.setEmpId(hrEmpId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(confDetails
											.get(0).getUpdatedOn());
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(ConfirmationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(ConfirmationConstants.MENU_ID);
							notificationDetails.setMenuUrl(delRmFeedbackHRKey
									.split(",")[0]);
							notificationDetails.setIsAction(delRmFeedbackHRKey
									.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(delRmFeedbackHRKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat
									.format(feedbackProvidedByDelRmToHRScreenIndividualMsg,
											gsrNotificationService
													.getEmployeeNameByEmpId(confDetails
															.get(0)
															.getDelegatedToMgrId()),
											gsrNotificationService
													.getEmployeeNameByEmpId(confDetails
															.get(0)
															.getFkEmpId()));
							if (!isDuplicatesFound.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
								isDuplicatesFound
										.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= confDetails.get(0)
									.getUpdatedOn().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(confDetails
												.get(0).getUpdatedOn());
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					} else {
						String fullmsgIdentifierKey = msgIdentifierKey + "_"
								+ hrEmpId + "_"
								+ confDetails.get(0).getDelegatedToMgrId();
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

						isDuplicatesFound = gsrNotificationService
								.checkDuplicates(hrEmpId, fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat.format(
									feedbackProvidedByDelRmToHRConsolidatedMsg,
									gsrNotificationService
											.getEmployeeNameByEmpId(confDetails
													.get(0)
													.getDelegatedToMgrId()),
									confDetails.size());

							Date maxDate = confDetails
									.stream()
									.map(ConfDatProbationConfirmationBO::getUpdatedOn)
									.max(Date::compareTo).get();
							notificationDetails.setEmpId(hrEmpId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(maxDate);
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(ConfirmationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(ConfirmationConstants.MENU_ID);
							notificationDetails.setMenuUrl(delRmFeedbackHRKey
									.split(",")[0]);
							notificationDetails.setIsAction(delRmFeedbackHRKey
									.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(delRmFeedbackHRKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat.format(
									feedbackProvidedByDelRmToHRConsolidatedMsg,
									gsrNotificationService
											.getEmployeeNameByEmpId(confDetails
													.get(0)
													.getDelegatedToMgrId()),
									confDetails.size());
							Date maxDate = confDetails
									.stream()
									.map(ConfDatProbationConfirmationBO::getUpdatedOn)
									.max(Date::compareTo).get();
							if (!isDuplicatesFound.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
								isDuplicatesFound
										.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= maxDate.getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(maxDate);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					}
				}
			} else {
				String menuURL = delRmFeedbackHRKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(hrEmpId,
									menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, hrEmpId,
									delRmFeedbackHRKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in setting Feedback Provided by Delegation RM to HR Notifications");
		}
		LOG.endUsecase("Exiting setFeedbackProvidedByDelRmToHrScreen()");
	}

	/**
	 * 
	 * <Description setConfirmationExtendedNotificationToEmpScreen:> This method
	 * is used to set Confirmation Extended Notification to Employee Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setConfirmationExtendedNotificationToEmpScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setConfirmationExtendedNotificationToEmpScreen()");
		String confExtendByHrToEmpKey = confExtendByHrToEmpKeys;
		String msgIdentifierKey = confExtendByHrToEmpKey.split(",")[2];
		ConfDatProbationConfirmationBO confDetail;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			confDetail = confProbRepository.findByFkEmpIdAndFkConfStatusId(
					empId, ConfirmationConstants.EXTENDED_PROBATION);

			if (confDetail != null) {
				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
				fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

				isDuplicatesFound = gsrNotificationService.checkDuplicates(
						empId, fullmsgIdentifierKey);

				if (isDuplicatesFound == null) {
					DatNotificationsBO notificationDetails = new DatNotificationsBO();
					notificationDetails.setEmpId(empId);
					notificationDetails
							.setNoificationMessage(confirmationExtendedByHrToEmpScreenIndividualMsg);
					notificationDetails
							.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
					notificationDetails.setNotificationcreatedDate(confDetail
							.getUpdatedOn());
					notificationDetails.setClearFlagModifiedDate(new Date());
					notificationDetails.setClearFlagcreatedDate(new Date());
					notificationDetails
							.setModuleName(ConfirmationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(ConfirmationConstants.MENU_ID);
					notificationDetails.setMenuUrl(confExtendByHrToEmpKey
							.split(",")[0]);
					notificationDetails.setIsAction(confExtendByHrToEmpKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(confExtendByHrToEmpKey.split(",")[3]));
					try {
						notificationRepository.save(notificationDetails);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				} else {
					isDuplicatesFound
							.setNoificationMessage(confirmationExtendedByHrToEmpScreenIndividualMsg);

					isDuplicatesFound.setNotificationModifiedDate(new Date());
					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= confDetail.getUpdatedOn().getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound.setNotificationcreatedDate(confDetail
								.getUpdatedOn());
					try {
						notificationRepository.save(isDuplicatesFound);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				}
			} else {
				String menuURL = confExtendByHrToEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									confExtendByHrToEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in setting Confirmation Extended by HR to Emp Notifications");
		}
		LOG.endUsecase("Exiting setConfirmationExtendedNotificationToEmpScreen()");
	}

	/**
	 * 
	 * <Description setConfirmationExtendedNotificationToRmScreen:> This method
	 * is used to set Confirmation extended by HR notification to RM Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setConfirmationExtendedByHrNotificationToRmScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setConfirmationExtendedByHrNotificationToRmScreen() method");
		String confExtendByHrToRmKey = confExtendByHrToRmKeys;
		String msgIdentifierKey = confExtendByHrToRmKey.split(",")[2];
		List<ConfDatProbationConfirmationBO> confDetailsList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			confDetailsList = confProbRepository
					.findByFkMgrIdAndFkConfStatusId(empId,
							ConfirmationConstants.EXTENDED_PROBATION);

			if (confDetailsList.size() > 0) {
				if (confDetailsList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(confirmationExtendedByHrToRmScreenIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetailsList
														.get(0).getFkEmpId()));
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(confDetailsList
										.get(0).getUpdatedOn());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(ConfirmationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ConfirmationConstants.MENU_ID);
						notificationDetails.setMenuUrl(confExtendByHrToRmKey
								.split(",")[0]);
						notificationDetails.setIsAction(confExtendByHrToRmKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(confExtendByHrToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(confirmationExtendedByHrToRmScreenIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetailsList
														.get(0).getFkEmpId()));
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= confDetailsList.get(0)
								.getUpdatedOn().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(confDetailsList
											.get(0).getUpdatedOn());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						Date maxDate = confDetailsList
								.stream()
								.map(ConfDatProbationConfirmationBO::getUpdatedOn)
								.max(Date::compareTo).get();
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								confirmationExtendedByHrToRmConsolidatedMsg,
								confDetailsList.size());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(maxDate);
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(ConfirmationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ConfirmationConstants.MENU_ID);
						notificationDetails.setMenuUrl(confExtendByHrToRmKey
								.split(",")[0]);
						notificationDetails.setIsAction(confExtendByHrToRmKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(confExtendByHrToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								confirmationExtendedByHrToRmConsolidatedMsg,
								confDetailsList.size());
						Date maxDate = confDetailsList
								.stream()
								.map(ConfDatProbationConfirmationBO::getUpdatedOn)
								.max(Date::compareTo).get();
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(maxDate);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = confExtendByHrToRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									confExtendByHrToRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in setting Confirmation Extended Notification to RM Screen");
		}
		LOG.endUsecase("Exiting setConfirmationExtendedByHrNotificationToRmScreen() method");
	}

	/**
	 * 
	 * <Description setConfirmationExtendedByHrNotificationToDelRmScreen:> This
	 * method is used to set Confirmation Extended by HR notification to
	 * Delegated RM Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setConfirmationExtendedByHrNotificationToDelRmScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setConfirmationExtendedByHrNotificationToDelRmScreen() method");
		String confExtendByHrToDelRmKey = confExtendByHrToDelRmKeys;
		String msgIdentifierKey = confExtendByHrToDelRmKey.split(",")[2];
		List<ConfDatProbationConfirmationBO> confDetailsList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			confDetailsList = confProbRepository
					.findByDelegatedToMgrIdAndFkConfStatusId(empId,
							ConfirmationConstants.EXTENDED_PROBATION);

			if (confDetailsList.size() > 0) {
				if (confDetailsList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(confirmationExtendedByHrToDelRmScreenIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetailsList
														.get(0).getFkEmpId()));
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(confDetailsList
										.get(0).getUpdatedOn());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(ConfirmationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ConfirmationConstants.MENU_ID);
						notificationDetails.setMenuUrl(confExtendByHrToDelRmKey
								.split(",")[0]);
						notificationDetails
								.setIsAction(confExtendByHrToDelRmKey
										.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
						notificationDetails
								.setNotificationExpiryTime(Integer
										.parseInt(confExtendByHrToDelRmKey
												.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(confirmationExtendedByHrToDelRmScreenIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetailsList
														.get(0).getFkEmpId()));
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= confDetailsList.get(0)
								.getUpdatedOn().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(confDetailsList
											.get(0).getUpdatedOn());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						Date maxDate = confDetailsList
								.stream()
								.map(ConfDatProbationConfirmationBO::getUpdatedOn)
								.max(Date::compareTo).get();
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								confirmationExtendedByHrToDelRmConsolidatedMsg,
								confDetailsList.size());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(maxDate);
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(ConfirmationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ConfirmationConstants.MENU_ID);
						notificationDetails.setMenuUrl(confExtendByHrToDelRmKey
								.split(",")[0]);
						notificationDetails
								.setIsAction(confExtendByHrToDelRmKey
										.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
						notificationDetails
								.setNotificationExpiryTime(Integer
										.parseInt(confExtendByHrToDelRmKey
												.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								confirmationExtendedByHrToDelRmConsolidatedMsg,
								confDetailsList.size());
						Date maxDate = confDetailsList
								.stream()
								.map(ConfDatProbationConfirmationBO::getUpdatedOn)
								.max(Date::compareTo).get();
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(maxDate);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = confExtendByHrToDelRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									confExtendByHrToDelRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in setting Confirmation Extended Notification to Delegated RM Screen");
		}
		LOG.endUsecase("Exiting setConfirmationExtendedByHrNotificationToDelRmScreen() method");
	}

	/**
	 * 
	 * <Description setConfirmationDelegatedByHrNotificationToEmpScreen:> This
	 * method is used to set Confirmation Delegated Notification by HR to
	 * Employee Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setConfirmationDelegatedByHrNotificationToEmpScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setConfirmationDelegatedByHrNotificationToEmpScreen()");
		String confDelegatedByHrToEmpKey = confDelegatedByHrToEmpKeys;
		String msgIdentifierKey = confDelegatedByHrToEmpKey.split(",")[2];
		ConfDatProbationConfirmationBO confDetail;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			confDetail = confProbRepository
					.findByFkEmpIdAndFkConfStatusIdAndDelegatedToMgrIdIsNotNull(
							empId, ConfirmationConstants.PENDING_WITH_MANAGER);

			if (confDetail != null) {
				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
				fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

				isDuplicatesFound = gsrNotificationService.checkDuplicates(
						empId, fullmsgIdentifierKey);

				if (isDuplicatesFound == null) {
					String notificationMessage = MessageFormat.format(
							confirmationDelegatedByHrToEmpScreenIndividualMsg,
							gsrNotificationService
									.getEmployeeNameByEmpId(confDetail
											.getFkMgrId()),
							gsrNotificationService
									.getEmployeeNameByEmpId(confDetail
											.getDelegatedToMgrId()));

					DatNotificationsBO notificationDetails = new DatNotificationsBO();
					notificationDetails.setEmpId(empId);
					notificationDetails
							.setNoificationMessage(notificationMessage);
					notificationDetails
							.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
					notificationDetails.setNotificationcreatedDate(confDetail
							.getUpdatedOn());
					notificationDetails.setClearFlagModifiedDate(new Date());
					notificationDetails.setClearFlagcreatedDate(new Date());
					notificationDetails
							.setModuleName(ConfirmationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(ConfirmationConstants.MENU_ID);
					notificationDetails.setMenuUrl(confDelegatedByHrToEmpKey
							.split(",")[0]);
					notificationDetails.setIsAction(confDelegatedByHrToEmpKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(confDelegatedByHrToEmpKey.split(",")[3]));
					try {
						notificationRepository.save(notificationDetails);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				} else {
					String notificationMessage = MessageFormat.format(
							confirmationDelegatedByHrToEmpScreenIndividualMsg,
							gsrNotificationService
									.getEmployeeNameByEmpId(confDetail
											.getFkMgrId()),
							gsrNotificationService
									.getEmployeeNameByEmpId(confDetail
											.getDelegatedToMgrId()));
					isDuplicatesFound
							.setNoificationMessage(notificationMessage);

					isDuplicatesFound.setNotificationModifiedDate(new Date());
					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= confDetail.getUpdatedOn().getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound.setNotificationcreatedDate(confDetail
								.getUpdatedOn());
					try {
						notificationRepository.save(isDuplicatesFound);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				}
			} else {
				String menuURL = confDelegatedByHrToEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									confDelegatedByHrToEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in setting Confirmation Delegated by HR to Emp Notifications");
		}
		LOG.endUsecase("Exiting setConfirmationDelegatedByHrNotificationToEmpScreen()");
	}

	/**
	 * 
	 * <Description setConfirmationDelegatedByHrNotificationToRmScreen:> This
	 * method is used to set Confirmation Delegated Notification To RM Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setConfirmationDelegatedByHrNotificationToRmScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setConfirmationDelegatedByHrNotificationToRmScreen() method");
		String confDelegatedByHrToRmKey = confDelegatedByHrToRmKeys;
		String msgIdentifierKey = confDelegatedByHrToRmKey.split(",")[2];
		List<ConfDatProbationConfirmationBO> confDetailsList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			confDetailsList = confProbRepository
					.findByFkMgrIdAndFkConfStatusIdAndDelegatedToMgrIdIsNotNull(
							empId, ConfirmationConstants.PENDING_WITH_MANAGER);

			if (confDetailsList.size() > 0) {
				if (confDetailsList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(confirmationDelegatedByHrToRmScreenIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetailsList
														.get(0).getFkEmpId()),
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetailsList
														.get(0)
														.getDelegatedToMgrId()));
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(confDetailsList
										.get(0).getUpdatedOn());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(ConfirmationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ConfirmationConstants.MENU_ID);
						notificationDetails.setMenuUrl(confDelegatedByHrToRmKey
								.split(",")[0]);
						notificationDetails
								.setIsAction(confDelegatedByHrToRmKey
										.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
						notificationDetails
								.setNotificationExpiryTime(Integer
										.parseInt(confDelegatedByHrToRmKey
												.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(confirmationDelegatedByHrToRmScreenIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetailsList
														.get(0).getFkEmpId()),
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetailsList
														.get(0)
														.getDelegatedToMgrId()));
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= confDetailsList.get(0)
								.getUpdatedOn().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(confDetailsList
											.get(0).getUpdatedOn());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						Date maxDate = confDetailsList
								.stream()
								.map(ConfDatProbationConfirmationBO::getUpdatedOn)
								.max(Date::compareTo).get();
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								confirmationDelegatedByHrToRmConsolidatedMsg,
								confDetailsList.size(), gsrNotificationService
										.getEmployeeNameByEmpId(confDetailsList
												.get(0).getDelegatedToMgrId()));
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(maxDate);
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(ConfirmationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ConfirmationConstants.MENU_ID);
						notificationDetails.setMenuUrl(confDelegatedByHrToRmKey
								.split(",")[0]);
						notificationDetails
								.setIsAction(confDelegatedByHrToRmKey
										.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
						notificationDetails
								.setNotificationExpiryTime(Integer
										.parseInt(confDelegatedByHrToRmKey
												.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								confirmationDelegatedByHrToRmConsolidatedMsg,
								confDetailsList.size(), gsrNotificationService
										.getEmployeeNameByEmpId(confDetailsList
												.get(0).getDelegatedToMgrId()));
						Date maxDate = confDetailsList
								.stream()
								.map(ConfDatProbationConfirmationBO::getUpdatedOn)
								.max(Date::compareTo).get();
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(maxDate);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = confDelegatedByHrToRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									confDelegatedByHrToRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in setting Confirmation Delegated Notification to RM Screen");
		}
		LOG.endUsecase("Exiting setConfirmationDelegatedByHrNotificationToRmScreen() method");
	}

	/**
	 * 
	 * <Description setConfirmationDelegatedByHrNotificationToDelRmScreen:> This
	 * method is used to set Confirmation Delegated Notification to Delegated RM
	 * Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setConfirmationDelegatedByHrNotificationToDelRmScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setConfirmationDelegatedByHrNotificationToDelRmScreen() method");
		String confDelegatedByHrToDelRmKey = confDelegatedByHrToDelRmKeys;
		String msgIdentifierKey = confDelegatedByHrToDelRmKey.split(",")[2];
		List<ConfDatProbationConfirmationBO> confDetailsList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			confDetailsList = confProbRepository
					.findByDelegatedToMgrIdAndFkConfStatusId(empId,
							ConfirmationConstants.PENDING_WITH_MANAGER);

			if (confDetailsList.size() > 0) {
				if (confDetailsList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(confirmationDelegatedByHrToDelRmScreenIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetailsList
														.get(0).getFkEmpId()),
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetailsList
														.get(0).getFkMgrId()));
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(confDetailsList
										.get(0).getUpdatedOn());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(ConfirmationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ConfirmationConstants.MENU_ID);
						notificationDetails
								.setMenuUrl(confDelegatedByHrToDelRmKey
										.split(",")[0]);
						notificationDetails
								.setIsAction(confDelegatedByHrToDelRmKey
										.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(confDelegatedByHrToDelRmKey
										.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(confirmationDelegatedByHrToDelRmScreenIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetailsList
														.get(0).getFkEmpId()),
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetailsList
														.get(0).getFkMgrId()));
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= confDetailsList.get(0)
								.getUpdatedOn().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(confDetailsList
											.get(0).getUpdatedOn());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId;
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						Date maxDate = confDetailsList
								.stream()
								.map(ConfDatProbationConfirmationBO::getUpdatedOn)
								.max(Date::compareTo).get();
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(confirmationDelegatedByHrToDelRmConsolidatedMsg,
										confDetailsList.size(),
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetailsList
														.get(0).getFkMgrId()));
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(maxDate);
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(ConfirmationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ConfirmationConstants.MENU_ID);
						notificationDetails
								.setMenuUrl(confDelegatedByHrToDelRmKey
										.split(",")[0]);
						notificationDetails
								.setIsAction(confDelegatedByHrToDelRmKey
										.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(confDelegatedByHrToDelRmKey
										.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(confirmationDelegatedByHrToDelRmConsolidatedMsg,
										confDetailsList.size(),
										gsrNotificationService
												.getEmployeeNameByEmpId(confDetailsList
														.get(0).getFkMgrId()));
						Date maxDate = confDetailsList
								.stream()
								.map(ConfDatProbationConfirmationBO::getUpdatedOn)
								.max(Date::compareTo).get();
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(maxDate);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = confDelegatedByHrToDelRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									confDelegatedByHrToDelRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in setting Confirmation Delegated Notification to Delegated RM Screen");
		}
		LOG.endUsecase("Exiting setConfirmationDelegatedByHrNotificationToDelRmScreen() method");
	}

	/**
	 * 
	 * <Description setEmpConfirmedNotificationToEmpScreen:> This method is used
	 * to set Confirmed By HR Notification to Employee Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setEmpConfirmedNotificationToEmpScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setEmpConfirmedNotificationToEmpScreen()");
		String confirmedByHrToEmpKey = confirmedByHrToEmpKeys;
		String msgIdentifierKey = confirmedByHrToEmpKey.split(",")[2];
		ConfDatProbationConfirmationBO confDetail;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			confDetail = confProbRepository.findByFkEmpIdAndFkConfStatusId(
					empId, ConfirmationConstants.CONFIRMED);

			if (confDetail != null) {
				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId;
				fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

				isDuplicatesFound = gsrNotificationService.checkDuplicates(
						empId, fullmsgIdentifierKey);

				if (isDuplicatesFound == null) {
					DatNotificationsBO notificationDetails = new DatNotificationsBO();
					notificationDetails.setEmpId(empId);
					notificationDetails
							.setNoificationMessage(confirmedByHrToEmpScreenIndividualMsg);
					notificationDetails
							.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
					notificationDetails.setNotificationcreatedDate(confDetail
							.getUpdatedOn());
					notificationDetails.setClearFlagModifiedDate(new Date());
					notificationDetails.setClearFlagcreatedDate(new Date());
					notificationDetails
							.setModuleName(ConfirmationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(ConfirmationConstants.MENU_ID);
					notificationDetails.setMenuUrl(confirmedByHrToEmpKey
							.split(",")[0]);
					notificationDetails.setIsAction(confirmedByHrToEmpKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(confirmedByHrToEmpKey.split(",")[3]));
					try {
						notificationRepository.save(notificationDetails);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				} else {
					isDuplicatesFound
							.setNoificationMessage(confirmedByHrToEmpScreenIndividualMsg);

					isDuplicatesFound.setNotificationModifiedDate(new Date());
					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= confDetail.getUpdatedOn().getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound.setNotificationcreatedDate(confDetail
								.getUpdatedOn());
					try {
						notificationRepository.save(isDuplicatesFound);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				}
			} else {
				String menuURL = confirmedByHrToEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									confirmedByHrToEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in setting Confirmed by HR to Emp Notifications");
		}
		LOG.endUsecase("Exiting setEmpConfirmedNotificationToEmpScreen()");
	}

	/**
	 * 
	 * <Description setEmpConfirmedNotificationToRmScreen:> This method is used
	 * to Employee Confirmed Notification to RM Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setEmpConfirmedNotificationToRmScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setEmpConfirmedNotificationToRmScreen() method");
		String confirmedByHrToRmKey = confirmedByHrToRmKeys;
		String msgIdentifierKey = confirmedByHrToRmKey.split(",")[2];
		List<ConfDatProbationConfirmationBO> confDetailsList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			confDetailsList = confProbRepository
					.findByFkMgrIdAndFkConfStatusId(empId,
							ConfirmationConstants.CONFIRMED);

			if (confDetailsList.size() > 0) {
				for (ConfDatProbationConfirmationBO confDetail : confDetailsList) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + "_" + confDetail.getFkEmpId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								confirmedByHrToRmScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(confDetail
												.getFkEmpId()));
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(confDetail
										.getUpdatedOn());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(ConfirmationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ConfirmationConstants.MENU_ID);
						notificationDetails.setMenuUrl(confirmedByHrToRmKey
								.split(",")[0]);
						notificationDetails.setIsAction(confirmedByHrToRmKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(confirmedByHrToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = confirmedByHrToRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									confirmedByHrToRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in setting Emp Confirmed Notification to RM Screen");
		}
		LOG.endUsecase("Exiting setEmpConfirmedNotificationToRmScreen() method");
	}

	/**
	 * 
	 * <Description setEmpConfirmedNotificationToDelRmScreen:> This method is
	 * used to set Employee Confirmed notification to Delegated RM Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setEmpConfirmedNotificationToDelRmScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setEmpConfirmedNotificationToDelRmScreen() method");
		String confirmedByHrToDelRmKey = confirmedByHrToDelRmKeys;
		String msgIdentifierKey = confirmedByHrToDelRmKey.split(",")[2];
		List<ConfDatProbationConfirmationBO> confDetailsList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		int updateCount = 0;
		try {
			confDetailsList = confProbRepository
					.findByDelegatedToMgrIdAndFkConfStatusId(empId,
							ConfirmationConstants.CONFIRMED);

			if (confDetailsList.size() > 0) {
				for (ConfDatProbationConfirmationBO confDetail : confDetailsList) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + confDetail.getFkEmpId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								confirmedByHrToDelRmScreenIndividualMsg,
								gsrNotificationService
										.getEmployeeNameByEmpId(confDetail
												.getFkEmpId()));
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(confDetail
										.getUpdatedOn());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(ConfirmationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(ConfirmationConstants.MENU_ID);
						notificationDetails.setMenuUrl(confirmedByHrToDelRmKey
								.split(",")[0]);
						notificationDetails.setIsAction(confirmedByHrToDelRmKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
						notificationDetails
								.setNotificationExpiryTime(Integer
										.parseInt(confirmedByHrToDelRmKey
												.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = confirmedByHrToDelRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									confirmedByHrToDelRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in setting Emp Confirmed Notification to Del RM Screen");
		}
		LOG.endUsecase("Exiting setEmpConfirmedNotificationToDELRmScreen() method");
	}

	/**
	 * 
	 * <Description setFeedbackProvidedByRmNotificationtoDelRmScreen:> This
	 * method is used to set Feedback Provided by RM to Delegated RM Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setFeedbackProvidedByRmNotificationtoDelRmScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setFeedbackProvidedByRmNotificationtoDelRmScreen() method");
		String rmFeedbackToDelRmKey = rmFeedbackToDelRmKeys;
		String msgIdentifierKey = rmFeedbackToDelRmKey.split(",")[2];
		List<ConfDatProbationConfirmationBO> confDetailsList;
		List<Integer> notificationIdList = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		Map<Integer, List<ConfDatProbationConfirmationBO>> confDetailsMapByDelRmId = new HashMap<Integer, List<ConfDatProbationConfirmationBO>>();
		int updateCount = 0;
		try {
			confDetailsList = confProbRepository
					.findByDelegatedToMgrIdAndFkConfStatusIdAndIsConfirmedByMgrAndDelegatedToMgrIdIsNotNull(
							empId, ConfirmationConstants.PENDING_WITH_HR,
							ConfirmationConstants.CONFIRMED_BY_RM);

			if (confDetailsList.size() > 0) {

				for (ConfDatProbationConfirmationBO confDetail : confDetailsList) {
					if (confDetail.getDelegatedToMgrId().equals(
							confDetail.getConfirmedByMgrId())) {
						confDetailsList.remove(confDetail);
					}
				}

				for (ConfDatProbationConfirmationBO confDetail : confDetailsList) {
					if (!confDetailsMapByDelRmId.containsKey(confDetail
							.getDelegatedToMgrId())) {
						List<ConfDatProbationConfirmationBO> finalBean = new ArrayList<ConfDatProbationConfirmationBO>();
						finalBean.add(confDetail);
						confDetailsMapByDelRmId.put(
								confDetail.getDelegatedToMgrId(), finalBean);
						continue;
					}
					if (confDetailsMapByDelRmId.containsKey(confDetail
							.getDelegatedToMgrId())) {
						List<ConfDatProbationConfirmationBO> finalBean = new ArrayList<ConfDatProbationConfirmationBO>();
						finalBean.add(confDetail);
						confDetailsMapByDelRmId.get(
								confDetail.getDelegatedToMgrId()).addAll(
								finalBean);
					}
				}

				for (Map.Entry<Integer, List<ConfDatProbationConfirmationBO>> entry : confDetailsMapByDelRmId
						.entrySet()) {
					List<ConfDatProbationConfirmationBO> confDetails = (List<ConfDatProbationConfirmationBO>) entry
							.getValue();

					if (confDetails.size() == 1) {

						String fullmsgIdentifierKey = msgIdentifierKey + "_"
								+ empId;
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

						isDuplicatesFound = gsrNotificationService
								.checkDuplicates(empId, fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(feedbackProvidedByRmToDelRmScreenIndividualMsg,
											gsrNotificationService
													.getEmployeeNameByEmpId(confDetails
															.get(0)
															.getDelegatedToMgrId()),
											gsrNotificationService
													.getEmployeeNameByEmpId(confDetails
															.get(0)
															.getFkEmpId()));
							notificationDetails.setEmpId(empId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(confDetailsList
											.get(0).getUpdatedOn());
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(ConfirmationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(ConfirmationConstants.MENU_ID);
							notificationDetails.setMenuUrl(rmFeedbackToDelRmKey
									.split(",")[0]);
							notificationDetails
									.setIsAction(rmFeedbackToDelRmKey
											.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(rmFeedbackToDelRmKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat
									.format(feedbackProvidedByRmToDelRmScreenIndividualMsg,
											gsrNotificationService
													.getEmployeeNameByEmpId(confDetails
															.get(0)
															.getDelegatedToMgrId()),
											gsrNotificationService
													.getEmployeeNameByEmpId(confDetails
															.get(0)
															.getFkEmpId()));
							if (!isDuplicatesFound.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
								isDuplicatesFound
										.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= confDetails.get(0)
									.getUpdatedOn().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(confDetails
												.get(0).getUpdatedOn());
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}

					} else {

						String fullmsgIdentifierKey = msgIdentifierKey + "_"
								+ empId;
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

						isDuplicatesFound = gsrNotificationService
								.checkDuplicates(empId, fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							Date maxDate = confDetails
									.stream()
									.map(ConfDatProbationConfirmationBO::getUpdatedOn)
									.max(Date::compareTo).get();
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat.format(
									feedbackProvidedByRmToDelRmConsolidatedMsg,
									gsrNotificationService
											.getEmployeeNameByEmpId(confDetails
													.get(0)
													.getDelegatedToMgrId()),
									confDetails.size());
							notificationDetails.setEmpId(empId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(maxDate);
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(ConfirmationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(ConfirmationConstants.MENU_ID);
							notificationDetails.setMenuUrl(rmFeedbackToDelRmKey
									.split(",")[0]);
							notificationDetails
									.setIsAction(rmFeedbackToDelRmKey
											.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(rmFeedbackToDelRmKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat.format(
									feedbackProvidedByRmToDelRmConsolidatedMsg,
									gsrNotificationService
											.getEmployeeNameByEmpId(confDetails
													.get(0)
													.getDelegatedToMgrId()),
									confDetails.size());
							Date maxDate = confDetails
									.stream()
									.map(ConfDatProbationConfirmationBO::getUpdatedOn)
									.max(Date::compareTo).get();
							if (!isDuplicatesFound.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
								isDuplicatesFound
										.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= maxDate.getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(maxDate);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					}
				}
			} else {
				String menuURL = rmFeedbackToDelRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									rmFeedbackToDelRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Confirmation Module Notifications");
		}
		LOG.endUsecase("Exiting setFeedbackProvidedByRmNotificationtoDelRmScreen() method");
	}
}
