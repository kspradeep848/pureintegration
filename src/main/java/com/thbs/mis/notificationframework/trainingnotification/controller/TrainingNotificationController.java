package com.thbs.mis.notificationframework.trainingnotification.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.trainingnotification.bean.TrainingModuleNotificationBean;
import com.thbs.mis.notificationframework.trainingnotification.bean.TrainingNotificationBean;
import com.thbs.mis.notificationframework.trainingnotification.constants.TrainingNotificationURIConstants;
import com.thbs.mis.notificationframework.trainingnotification.service.TrainingNotificationService;

@Controller
public class TrainingNotificationController {/*
	private static final AppLog LOG = LogFactory
			.getLog(TrainingNotificationController.class);

	@Autowired
	private TrainingNotificationService trainingNotificationService;

	
	 * Fetching the records from the training notification table
	 

	@RequestMapping(value = TrainingNotificationURIConstants.TRAINING_MODULE_NOTIFICATION, method = RequestMethod.POST)
	public @ResponseBody List<TrainingModuleNotificationBean> getTrainingModuleNotification(
			@RequestBody TrainingNotificationBean trainingNotificationBean)
			throws BusinessException, DataAccessException, Exception {

		List<TrainingModuleNotificationBean> outputBean = new ArrayList<TrainingModuleNotificationBean>();
		LOG.startUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen ");
		outputBean = trainingNotificationService
				.getTrainingModuleNotifications(
						trainingNotificationBean.getEmpId(),
						trainingNotificationBean.getModuleName());

		LOG.endUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen"
				+ outputBean);

		return outputBean;

	}

	 inserting into tables for Training Request (By Manager) 
	@RequestMapping(value = TrainingNotificationURIConstants.TRAINING_REQUEST_NOTIFICATION, method = RequestMethod.POST)
	public @ResponseBody String createTrainingNotificationsForTrainingCoordinator(
			@RequestBody TrainingNotificationBean trainingNotificationBean)
			throws BusinessException, DataAccessException, Exception {

		LOG.startUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen ");

		String output = trainingNotificationService
				.createTrainingNotificationsForTrainingCoordinator(
						trainingNotificationBean.getEmpId(),
						trainingNotificationBean.getsDate(),
						trainingNotificationBean.geteDate());

		LOG.endUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen"
				+ output);

		return output;

	}

	
	 * inserting into tables for Approve the Training Request (By Trainig
	 * Co-ordinator)
	 
	@RequestMapping(value = TrainingNotificationURIConstants.TRAINING_REQUEST_APPROVED_NOTIFICATION_FOR_RM, method = RequestMethod.POST)
	public @ResponseBody String createTrainingNotificationsForRM(
			@RequestBody TrainingNotificationBean trainingNotificationBean)
			throws BusinessException, DataAccessException, Exception {

		LOG.startUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen ");

		String output = trainingNotificationService
				.createTrainingNotificationsForRM(
						trainingNotificationBean.getRmId(),
						trainingNotificationBean.getsDate(),
						trainingNotificationBean.geteDate());

		LOG.endUsecase("getTrainingRequestByRMIdForTrainingCoordinatorScreen"
				+ output);

		return output;

	}

	
	 * inserting into tables for Create the Training Program(By Trainig
	 * Co-ordinator)
	 
	@RequestMapping(value = TrainingNotificationURIConstants.CREATE_TRAINING_PROGRAM_NOTIFICATION_BY_TC_TO_RMSCREEN, method = RequestMethod.POST)
	public @ResponseBody String createTrainingProgramNotificationsByTcToRmScreen(
			@RequestBody TrainingNotificationBean trainingNotificationBean)
			throws BusinessException, DataAccessException, Exception {

		LOG.startUsecase("createTrainingProgramNotificationsByTcToRmScreen ");

		String output = trainingNotificationService
				.createTrainingProgramNotificationsByTcToRmScreen(trainingNotificationBean
						.getRmId());

		LOG.endUsecase("createTrainingProgramNotificationsByTcToRmScreen"
				+ output);

		return output;

	}

	 Invite Reportees(By Manager) 

	@RequestMapping(value = TrainingNotificationURIConstants.INVITE_NOTIFICATION_BY_RM_TO_EMPLOYEESCREEN, method = RequestMethod.POST)
	public @ResponseBody String inviteTrainingProgramNotificationsByRMToEmployeeScreen(
			@RequestBody TrainingNotificationBean trainingNotificationBean)
			throws BusinessException, DataAccessException, Exception {

		LOG.startUsecase("createTrainingProgramNotificationsByTcToRmScreen ");

		String output = trainingNotificationService
				.inviteTrainingProgramNotificationsByRMToEmployeeScreen(trainingNotificationBean
						.getEmpId());

		LOG.endUsecase("createTrainingProgramNotificationsByTcToRmScreen"
				+ output);

		return output;

	}
	
	@RequestMapping(value = TrainingNotificationURIConstants.SET_TRAINING_NOTIFICATIONS, method = RequestMethod.POST)
	public @ResponseBody String setTrainingNotifications(@RequestBody TrainingNotificationBean trainingNotificationBean) throws BusinessException
	{
		LOG.startUsecase("setTrainingNotifications");
		String success="";
		try{
			success = trainingNotificationService.setTrainingNotifications(trainingNotificationBean.getEmpId());
		} catch(Exception e) {
			throw new BusinessException("Exception thrown from setTrainingNotificaitons controller");
		}
		LOG.endUsecase("setTrainingNotifications");
		
		return success;
	}
*/}
