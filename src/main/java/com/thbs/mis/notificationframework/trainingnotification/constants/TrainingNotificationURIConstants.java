/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  TrainingNotificationURIConstants.java                              */
/*                                                                   */
/*  Author      :  THBS                                              */
/*                                                                   */
/*  Date        : October 28, 2016                                   */
/*                                                                   */
/*  Description : This is used to create URI for all the services.   */
/*		 									                         */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* October 28, 2016     THBS     1.0        Initial version created  */
/*********************************************************************/

package com.thbs.mis.notificationframework.trainingnotification.constants;

/**
 * 
 * @author THBS
 * @Description: This is used to create URI for all the services.
 */
public class TrainingNotificationURIConstants {
	

	public static final String TRAINING_REQUEST_NOTIFICATION = "/notifications/training/request";
	public static final String TRAINING_REQUEST_APPROVED_NOTIFICATION_FOR_RM = "/notifications/training/request/approve";
	public static final String CREATE_TRAINING_PROGRAM_NOTIFICATION_BY_TC_TO_RMSCREEN = "/notifications/training/program";
	public static final String INVITE_NOTIFICATION_BY_RM_TO_EMPLOYEESCREEN = "/notifications/training/invite";

	
	public static final String TRAINING_MODULE_NOTIFICATION = "/notifications/trainingmodule";
	
	
	public static final String SET_TRAINING_NOTIFICATIONS = "/notifications/setTrainings";

}
