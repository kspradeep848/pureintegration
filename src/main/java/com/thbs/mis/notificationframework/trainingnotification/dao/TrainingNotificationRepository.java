package com.thbs.mis.notificationframework.trainingnotification.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.notificationframework.notificationsettings.bo.DatNotificationsBO;

@Repository
public interface TrainingNotificationRepository extends GenericRepository<DatNotificationsBO, Integer>{

	public List<DatNotificationsBO> findByEmpIdAndModuleName(int empId,String moduleName);
	public List<DatNotificationsBO> findByEmpId(int empId);

	public List<DatNotificationsBO> findByEmpIdAndModuleNameAndNotificationcreatedDateBetween(int empId, String moduleName,Date startDate, Date endDate);
	public List<DatNotificationsBO> findByEmpIdAndNotificationcreatedDateBetween(int empId,Date startDate, Date endDate);

	//public List<DatNotificationsBO> findByEmpIdAndNoificationMessage(int empId,String msg);

	public DatNotificationsBO findByEmpIdAndNoificationMessage(int empId,String msg);
	
	public DatNotificationsBO findByPkNotificationId(int notificationId);

}
