package com.thbs.mis.notificationframework.trainingnotification.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;



public class TrainingModuleNotificationBean {

	private int pkTrainingNotificationId;
	private int empId;

	@Temporal(TemporalType.TIMESTAMP)
	private Date notificationCreatedDate;
	private String notificationMessage;
	private String clearFlag;
	private Date clearFlagCreatedDate;
	private Date clearFlagModifiedDate;
	private String noNotificationMessage;
	private String actionURL;
	private String notificationCreatedDateString;
	private String module;
	private short moduleId;
	private String menuUrl;
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public Date getNotificationCreatedDate() {
		return notificationCreatedDate;
	}
	public void setNotificationCreatedDate(Date notificationCreatedDate) {
		this.notificationCreatedDate = notificationCreatedDate;
	}
	public String getNotificationMessage() {
		return notificationMessage;
	}
	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}
	public String getClearFlag() {
		return clearFlag;
	}
	public void setClearFlag(String clearFlag) {
		this.clearFlag = clearFlag;
	}
	public Date getClearFlagCreatedDate() {
		return clearFlagCreatedDate;
	}
	public void setClearFlagCreatedDate(Date clearFlagCreatedDate) {
		this.clearFlagCreatedDate = clearFlagCreatedDate;
	}
	public Date getClearFlagModifiedDate() {
		return clearFlagModifiedDate;
	}
	public void setClearFlagModifiedDate(Date clearFlagModifiedDate) {
		this.clearFlagModifiedDate = clearFlagModifiedDate;
	}
	
	public String getNoNotificationMessage() {
		return noNotificationMessage;
	}
	public void setNoNotificationMessage(String noNotificationMessage) {
		this.noNotificationMessage = noNotificationMessage;
	}
	
	public int getPkTrainingNotificationId() {
		return pkTrainingNotificationId;
	}
	public void setPkTrainingNotificationId(int pkTrainingNotificationId) {
		this.pkTrainingNotificationId = pkTrainingNotificationId;
	}
	
	public String getActionURL() {
		return actionURL;
	}
	public void setActionURL(String actionURL) {
		this.actionURL = actionURL;
	}
	
	public String getNotificationCreatedDateString() {
		return notificationCreatedDateString;
	}
	public void setNotificationCreatedDateString(
			String notificationCreatedDateString) {
		this.notificationCreatedDateString = notificationCreatedDateString;
	}
	
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	
	public short getModuleId() {
		return moduleId;
	}
	public void setModuleId(short moduleId) {
		this.moduleId = moduleId;
	}
	
	public String getMenuUrl() {
		return menuUrl;
	}
	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}
	
	@Override
	public String toString() {
		return "TrainingModuleNotificationBean [pkTrainingNotificationId="
				+ pkTrainingNotificationId + ", empId=" + empId
				+ ", notificationCreatedDate=" + notificationCreatedDate
				+ ", notificationMessage=" + notificationMessage
				+ ", clearFlag=" + clearFlag + ", clearFlagCreatedDate="
				+ clearFlagCreatedDate + ", clearFlagModifiedDate="
				+ clearFlagModifiedDate + ", noNotificationMessage="
				+ noNotificationMessage + ", actionURL=" + actionURL
				+ ", notificationCreatedDateString="
				+ notificationCreatedDateString + ", module=" + module
				+ ", moduleId=" + moduleId + ", menuUrl=" + menuUrl + "]";
	}

	
	
}
