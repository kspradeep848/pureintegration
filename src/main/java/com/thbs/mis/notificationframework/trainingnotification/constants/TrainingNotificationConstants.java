package com.thbs.mis.notificationframework.trainingnotification.constants;

public class TrainingNotificationConstants {

	public final static String  IS_PROG_CREATED="YES";
	public final static String  IS_FEEDBACK_CREATED="YES";

	public final static String  ATTENDEDTRAININGYES="YES";
	public final static String  ATTENDEDTRAININGNO="NO";
	
	public static final String MODULE_NAME="TRAINING";
	public static final short MENU_ID=1;
	
	public final static byte NEW_TRAINING_REQUEST_STATUS = 1;
	public final static byte APPROVED_TRAINING_REQUEST_STATUS = 3;
	public final static byte REJECTED_TRAINING_REQUEST_STATUS = 5;
	public final static byte CANCELED_TRAINING_REQUEST_STATUS = 4;
	public final static byte ONHOLD_TRAINING_REQUEST_STATUS = 2;
	
	public static final byte NOMINATION_POOL_INTERESTED_STATUS = 1;

}
