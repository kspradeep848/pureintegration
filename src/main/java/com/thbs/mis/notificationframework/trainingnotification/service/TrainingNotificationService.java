package com.thbs.mis.notificationframework.trainingnotification.service;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.confirmationnotification.constants.ConfirmationConstants;
import com.thbs.mis.notificationframework.gsrnotification.constants.GsrNotificationConstants;
import com.thbs.mis.notificationframework.gsrnotification.service.GSRNotificationService;
import com.thbs.mis.notificationframework.notificationsettings.bo.DatNotificationsBO;
import com.thbs.mis.notificationframework.notificationsettings.dao.MasNotificationSettingRepository;
import com.thbs.mis.notificationframework.notificationsettings.dao.NotificationRepository;
import com.thbs.mis.notificationframework.puchasenotification.constants.PurchaseNotificationConstants;
import com.thbs.mis.notificationframework.trainingnotification.constants.TrainingNotificationConstants;
import com.thbs.mis.notificationframework.trainingnotification.dao.TrainingNotificationRepository;
import com.thbs.mis.training.bo.TrainingDatAttendeeFeedbackBO;
import com.thbs.mis.training.bo.TrainingDatNominationPoolDetailBO;
import com.thbs.mis.training.bo.TrainingDatProgrammeDetailBO;
import com.thbs.mis.training.bo.TrainingDatRequestDetailBO;
import com.thbs.mis.training.dao.TrainingFeedBackRepository;
import com.thbs.mis.training.dao.TrainingNominationPoolRepository;
import com.thbs.mis.training.dao.TrainingNominationRepository;
import com.thbs.mis.training.dao.TrainingProgrammeRepository;
import com.thbs.mis.training.dao.TrainingRequestRepository;

@Service
public class TrainingNotificationService {

	private static final AppLog LOG = LogFactory
			.getLog(TrainingNotificationService.class);

	/*
	 * Individual Notification messages from ClientMessages.properties file
	 */
	@Value("${ui.mesage.trainingRequestForTrainingCoordinator}")
	String trainingRequest;

	@Value("${ui.mesage.trainingRequestForReportingManager}")
	String trainingApprovedRequestForReportingManager;

	@Value("${ui.mesage.trainingRequestForReportingManagerReject}")
	String trainingRequestForReportingManagerReject;

	@Value("${ui.mesage.trainingRequestForReportingManagerOnHold}")
	String trainingRequestForReportingManagerOnHold;

	@Value("${ui.mesage.trainingCompleteMsgForReportingManager}")
	String trainingCompleteMsgForReportingManager;

	@Value("${ui.mesage.trainingInviteMsgForEmployee}")
	String trainingInviteMsgForEmploye;

	@Value("${ui.mesage.trainingCompleteMsgForEmployee}")
	String trainingCompleteMsgForEmployee;

	@Value("${ui.mesage.trainingCompleteMsgForRM}")
	String trainingCompleteMsgForRM;
	
	@Value("${ui.message.postApplicationFeedbackToTCScreen}")
	String postApplicationFeedbackToTCScreen;

	@Value("${ui.mesage.trainingFeedbackMsgForTC}")
	String trainingFeedbackMsgForTC;
	
	@Value("${ui.message.trainingCreatedForRequestToRequestor}")
	String trainingCreatedForRequestToRmIndividualMsg;
	
	@Value("${ui.message.attendeefeedbackToRmScreen}")
	String attendeeFeedbackToRmIndividualMsg;
	
	@Value("${ui.message.attendeefeedbackToTCScreen}")
	String attendeeFeedbackToTCIndividualMsg;
	
	@Value("${ui.message.trainingCreatedByTCToEmpScreen}")
	String trainingCreatedByTCToEmpIndividualMsg;
	
	@Value("${ui.message.trainingCreatedByTCToRmScreen}")
	String trainingCreatedByTCToRMIndividualMsg;
	
	@Value("${ui.message.empInterestedProgToRmScreen}")
	String empInterestedInProgToRmIndividualMsg;
	
	@Value("${ui.message.rmApproveEmpRequestToEmpScreen}")
	String rmApproveEmpRequestToEmpIndividualMsg;
	
	@Value("${ui.message.rmInviteEmpToProgToEmpScreen}")
	String rmInviteEmpToProgIndividualMsg;
	
	@Value("${ui.message.tcNominateEmpToProgToEmpScreen}")
	String tcinviteEmpToProgIndividualMsg;
	
	/*
	 * Consolidated Notifications from ClientMessages.properties file
	 */
	@Value("${ui.message.attendeefeedbackToRmConsolidatedMsg}")
	String attendeeFeedbackToRmConsolidatedMsg;
	
	@Value("${ui.message.attendeefeedbackToTCConsolidatedMsg}")
	String attendeeFeedbackToTCConsolidatedMsg;
	
	@Value("${ui.message.empInterestedProgToRmConsolidatedMsg}")
	String empInterestedInProgToRmConsolidatedMsg;

	/*
	 * Training Notification Keys from notificationframework.properties file
	 */
	@Value("${TRAIN_RM_CRE_REQ}")
	String rmCreTrainingReqKeys;

	@Value("${TRAIN_TC_APPROVE_REQ}")
	String tcApproveReqKeys;

	@Value("${TRAIN_TC_CANCEL_REQ}")
	String tcCancelReqKeys;

	@Value("${TRAIN_TC_REJ_REQ}")
	String tcRejectReqKeys;

	@Value("${TRAIN_TC_ONHOLD_REQ}")
	String tcOnholdReqKeys;
	
	@Value("${TRAIN_TC_CRE_PROG_REQ}")
	String tcCreProgReqKeys;
	
	@Value("${TRAIN_EMP_FEEDBACK_RM}")
	String empFeedbackToRmKeys;
	
	@Value("${TRAIN_RM_POST_FEEDBACK_TC}")
	String rmPostAppFeedbackKeys;
	
	@Value("${TRAIN_EMP_FEEDBACK_TC}")
	String empFeedbackToTCKeys;
	
	@Value("${TRAIN_TC_CRE_PROG_EMP}")
	String tcCreProgKeys;
	
	@Value("${TRAIN_TC_CRE_PROG_RM}")
	String tcCreProgRmKeys;
	
	@Value("${TRAIN_EMP_INTERESTED_PROG_RM}")
	String empInterestedProgRmKeys;
	
	@Value("${TRAIN_RM_APPROVE_EMP_REQ_EMP}")
	String rmApproveEmpReqKeys;
	
	@Value("${TRAIN_RM_INVITE_EMP_PROG_EMP}")
	String rminviteEmpProgKeys;
	
	@Value("${TRAIN_TC_INVITE_EMP_PROG_EMP}")
	String tcinviteEmpProgKeys;

	@Value("${training.coordinator.role.Id}")
	String tcRoleIds;

	/*
	 * Autowiring Repositories
	 */
	@Autowired
	private TrainingRequestRepository trainingRequestRepository;
	@Autowired
	private TrainingNominationRepository trainingNominationDetailsRepository;
	@Autowired
	private TrainingNotificationRepository traNotificationRepository;

	@Autowired
	private EmployeePersonalDetailsRepository personalDetailsRepository;

	@Autowired
	private TrainingProgrammeRepository programRepository;
	@Autowired
	private MasNotificationSettingRepository masNotificationSettingRepository;
	@Autowired
	private EmpDetailRepository empDetailRepository;
	
	@Autowired
	private TrainingFeedBackRepository attendeeFeedbackRepository;

	@Autowired
	GSRNotificationService gsrNotificationService;

	@Autowired
	NotificationRepository notificationRepository;
	
	@Autowired
	private DatEmployeeProfessionalRepository empProfRepository;
	
	@Autowired
	private TrainingNominationPoolRepository nominationPoolRepository;

	DateFormat dateFormatter = DateFormat.getDateTimeInstance();

	/*
	 * Get Training Module notifications from newly created dat training
	 * notification table
	 */

	/*public List<TrainingModuleNotificationBean> getTrainingModuleNotifications(
			int empId, String moduleName) throws DataAccessException {
		List<TrainingModuleNotificationBean> trainingModuleNotificationsOutputBean = new ArrayList<TrainingModuleNotificationBean>();
		List<DatNotificationsBO> trainingModuleNotificationsBoOutput = new ArrayList<DatNotificationsBO>();
		;
		MasNotificationSettingBO notificationSettingoutput;
		TrainingModuleNotificationBean tempBean;
		try {
			notificationSettingoutput = masNotificationSettingRepository
					.findByFkEmployeeId(empId);

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}
		if (notificationSettingoutput.getNotificationType().equalsIgnoreCase(
				"ENABLE")
				&& notificationSettingoutput.getClearAllFlag()
						.equalsIgnoreCase("NO")) {
			try {
				trainingModuleNotificationsBoOutput = traNotificationRepository
						.findByEmpIdAndModuleName(empId, moduleName);

				for (int i = 0; i < trainingModuleNotificationsBoOutput.size(); i++) {

					tempBean = new TrainingModuleNotificationBean();
					if (trainingModuleNotificationsBoOutput.get(i)
							.getClearFlag().equalsIgnoreCase("NO")) {
						tempBean.setPkTrainingNotificationId(trainingModuleNotificationsBoOutput
								.get(i).getPkNotificationId());
						tempBean.setEmpId(trainingModuleNotificationsBoOutput
								.get(i).getEmpId());
						tempBean.setNotificationMessage(trainingModuleNotificationsBoOutput
								.get(i).getNoificationMessage());
						tempBean.setNotificationCreatedDate(trainingModuleNotificationsBoOutput
								.get(i).getNotificationcreatedDate());
						tempBean.setClearFlag(trainingModuleNotificationsBoOutput
								.get(i).getClearFlag());
						tempBean.setClearFlagCreatedDate(trainingModuleNotificationsBoOutput
								.get(i).getClearFlagcreatedDate());
						tempBean.setClearFlagModifiedDate(trainingModuleNotificationsBoOutput
								.get(i).getClearFlagModifiedDate());
						trainingModuleNotificationsOutputBean.add(tempBean);
					}

				}

			} catch (Exception e) {
				throw new DataAccessException("Error while Retrieving : ", e);
			}
		}
		if (notificationSettingoutput.getClearAllFlag().equalsIgnoreCase("YES")
				&& notificationSettingoutput.getClearAllModifiedDate() == null) {

			Date startDate = notificationSettingoutput.getClearAllCreatedDate();
			Date endDate = new Date();
			LOG.info("Inside Get Training Module notifications with  Clear all Flag created Date ");
			try {
				trainingModuleNotificationsBoOutput = traNotificationRepository
						.findByEmpIdAndModuleNameAndNotificationcreatedDateBetween(
								empId, moduleName, startDate, endDate);
				LOG.info("Inside Get Training Module notifications with  Clear all Flag created Date trainingModuleNotificationsBoOutput "
						+ trainingModuleNotificationsBoOutput);

				for (int i = 0; i < trainingModuleNotificationsBoOutput.size(); i++) {

					tempBean = new TrainingModuleNotificationBean();
					if (trainingModuleNotificationsBoOutput.get(i)
							.getClearFlag().equalsIgnoreCase("NO")) {
						tempBean.setPkTrainingNotificationId(trainingModuleNotificationsBoOutput
								.get(i).getPkNotificationId());
						tempBean.setEmpId(trainingModuleNotificationsBoOutput
								.get(i).getEmpId());
						tempBean.setNotificationMessage(trainingModuleNotificationsBoOutput
								.get(i).getNoificationMessage());
						tempBean.setNotificationCreatedDate(trainingModuleNotificationsBoOutput
								.get(i).getNotificationcreatedDate());
						tempBean.setClearFlag(trainingModuleNotificationsBoOutput
								.get(i).getClearFlag());
						tempBean.setClearFlagCreatedDate(trainingModuleNotificationsBoOutput
								.get(i).getClearFlagcreatedDate());
						tempBean.setClearFlagModifiedDate(trainingModuleNotificationsBoOutput
								.get(i).getClearFlagModifiedDate());

						trainingModuleNotificationsOutputBean.add(tempBean);
					}

				}

			} catch (Exception e) {
				throw new DataAccessException("Error while Retrieving : ", e);
			}
		}
		if (notificationSettingoutput.getClearAllFlag().equalsIgnoreCase("YES")
				&& notificationSettingoutput.getClearAllModifiedDate() != null) {

			Date startDate = notificationSettingoutput
					.getClearAllModifiedDate();
			Date endDate = new Date();
			LOG.info("Inside Get Training Module notifications with  Clear all Flag Modified Date ");
			try {
				trainingModuleNotificationsBoOutput = traNotificationRepository
						.findByEmpIdAndModuleNameAndNotificationcreatedDateBetween(
								empId, moduleName, startDate, endDate);
				LOG.info("Inside Get Training Module notifications with  Clear all Flag Modified Date trainingModuleNotificationsBoOutput "
						+ trainingModuleNotificationsBoOutput);

				for (int i = 0; i < trainingModuleNotificationsBoOutput.size(); i++) {

					tempBean = new TrainingModuleNotificationBean();
					if (trainingModuleNotificationsBoOutput.get(i)
							.getClearFlag().equalsIgnoreCase("NO")) {
						tempBean.setPkTrainingNotificationId(trainingModuleNotificationsBoOutput
								.get(i).getPkNotificationId());
						tempBean.setEmpId(trainingModuleNotificationsBoOutput
								.get(i).getEmpId());
						tempBean.setNotificationMessage(trainingModuleNotificationsBoOutput
								.get(i).getNoificationMessage());
						tempBean.setNotificationCreatedDate(trainingModuleNotificationsBoOutput
								.get(i).getNotificationcreatedDate());
						tempBean.setClearFlag(trainingModuleNotificationsBoOutput
								.get(i).getClearFlag());
						tempBean.setClearFlagCreatedDate(trainingModuleNotificationsBoOutput
								.get(i).getClearFlagcreatedDate());
						tempBean.setClearFlagModifiedDate(trainingModuleNotificationsBoOutput
								.get(i).getClearFlagModifiedDate());

						trainingModuleNotificationsOutputBean.add(tempBean);
					}

				}

			} catch (Exception e) {
				throw new DataAccessException("Error while Retrieving : ", e);
			}

		}

		return trainingModuleNotificationsOutputBean;

	}

	*//**
	 * Training Request (By Manager)
	 * 
	 * This service is to display the notifications into training co-ordinator
	 * screen. First it will get the requested training from dat training
	 * request detail table based on status and program is not created after it
	 * will store into the dat notification training table.
	 * 
	 * @throws ParseException
	 * 
	 * 
	 *//*

	public String createTrainingNotificationsForTrainingCoordinator(int empId,
			Date sDate, Date eDate) throws DataAccessException, ParseException {

		System.out.println("sDate" + sDate);
		System.out.println("eDate" + eDate);
		DatNotificationsBO checkDuplicates;
		LOG.startUsecase("Entering getAndInsertTrainingRequestByRMIdForTrainingCoordinatorScreen");
		List<TrainingDatRequestDetailBO> trainingDatRequestDetailOutput = new ArrayList<TrainingDatRequestDetailBO>();
		DatNotificationsBO trainingNotificationBO;
		String Sucess = "";

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.add(Calendar.DAY_OF_MONTH, +1);
		Date curDate = gregorianCalendar.getTime();
		System.out.println("curDate" + curDate);
		final String currentDate = dateFormatter.format(curDate);
		System.out.println("after parse currentDate" + currentDate);

		gregorianCalendar.setTime(dateFormatter.parse(currentDate));
		gregorianCalendar.add(Calendar.DAY_OF_MONTH, -Integer.valueOf(5));
		Date endDate = gregorianCalendar.getTime();
		System.out.println("endDate " + endDate);

		byte newStatus = 1;
		try {
			trainingDatRequestDetailOutput = trainingRequestRepository
					.findByStatusIsprogRequestDateBetween(newStatus, "NO",
							sDate, eDate);
			LOG.info("trainingDatRequestDetailOutput"
					+ trainingDatRequestDetailOutput);

			for (int i = 0; i < trainingDatRequestDetailOutput.size(); i++) {

				String rmId = String.valueOf(trainingDatRequestDetailOutput
						.get(i).getFkTrainingRequesterEmpId());

				DatEmpPersonalDetailBO rmNameBO = personalDetailsRepository
						.findByFkEmpDetailId(trainingDatRequestDetailOutput
								.get(i).getFkTrainingRequesterEmpId());
				trainingNotificationBO = new DatNotificationsBO();

				String trainingNotificationMessage = MessageFormat.format(
						trainingRequest, rmNameBO.getEmpFirstName(), rmId);
				try {
					checkDuplicates = traNotificationRepository
							.findByEmpIdAndNoificationMessage(empId,
									trainingNotificationMessage);

					System.out.println("checkDuplicates" + checkDuplicates);
				} catch (Exception e) {
					throw new DataAccessException("Failed to Save : ", e);

				}
				if (checkDuplicates == null) {
					System.out.println("inside  if");
					trainingNotificationBO.setEmpId(empId);
					trainingNotificationBO
							.setNoificationMessage(trainingNotificationMessage);
					trainingNotificationBO
							.setNotificationcreatedDate(new Date());
					trainingNotificationBO.setClearFlagcreatedDate(new Date());
					trainingNotificationBO.setClearFlagModifiedDate(new Date());
					trainingNotificationBO.setClearFlag("NO");
					trainingNotificationBO.setModuleName("TRAINING");
					try {

						traNotificationRepository.save(trainingNotificationBO);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				} else if (String.valueOf(checkDuplicates.getEmpId())
						.equalsIgnoreCase(String.valueOf(empId))
						&& checkDuplicates.getNoificationMessage()
								.equalsIgnoreCase(trainingNotificationMessage)) {
					System.out.println("inside else if");
					checkDuplicates.setEmpId(empId);
					checkDuplicates
							.setNoificationMessage(trainingNotificationMessage);
					checkDuplicates.setNotificationModifiedDate(new Date());
					checkDuplicates.setClearFlagcreatedDate(new Date());
					checkDuplicates.setClearFlagModifiedDate(new Date());
					checkDuplicates.setClearFlag("NO");
					checkDuplicates.setModuleName("TRAINING");
					try {

						traNotificationRepository.save(checkDuplicates);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				}
			}

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}
		LOG.endUsecase("Exitingg  getAndInsertTrainingRequestByRMIdForTrainingCoordinatorScreen");

		Sucess = "Notification Successfull ";

		return Sucess;

	}

	
	 * RM SCREEN NOTIFICATION
	 * 
	 * Approve the Training Request (By Trainig Co-ordinator)
	 public String createTrainingNotificationsForRM(int reportingManagerId,
			Date sDate, Date eDate) throws DataAccessException, ParseException {
		String Sucess = "Sucess";
		
		 * Approve the Training Request (By Trainig Co-ordinator) ForRMScreen
		 
		approveTheTrainingRequestByTrainigCoordinatorForRMScreen(
				reportingManagerId, sDate, eDate);
		
		 * Reject the Training Request (By Trainig Co-ordinator) ForRMScreen
		 
		rejectTheTrainingRequestByTrainigCoordinatorForRMScreen(
				reportingManagerId, sDate, eDate);

		
		 * On Hold the Training Request (By Trainig Co-ordinator) ForRMScreen
		 
		onHoldTheTrainingRequestByTrainigCoordinatorForRMScreen(
				reportingManagerId, sDate, eDate);

		
		 * Cancel the Training Request (By Trainig Co-ordinator) ForRMScreen
		 
		cancelTheTrainingRequestByTrainigCoordinatorForRMScreen(
				reportingManagerId, sDate, eDate);

		 Complete the Training Program(By Trainig Co-ordinator) for RM Screen 

		completeTheTrainingProgramByTrainigCoordinatorforRMScreen(reportingManagerId);
		return Sucess;

	}

	
	 * Approve the Training Request (By Trainig Co-ordinator) ForRMScreen
	 
	public void approveTheTrainingRequestByTrainigCoordinatorForRMScreen(
			int reportingManagerId, Date sDate, Date eDate)
			throws DataAccessException {
		byte rejectStatus = 3;
		List<TrainingDatRequestDetailBO> trainingDatRequestDetailOutput = new ArrayList<TrainingDatRequestDetailBO>();
		DatNotificationsBO trainingNotificationBO;
		String Sucess = "";
		DatNotificationsBO checkDuplicates;

		try {
			trainingDatRequestDetailOutput = trainingRequestRepository
					.findByStatusRequesterempIdAndtrainingModifiedDate(
							rejectStatus, reportingManagerId, sDate, eDate);
			LOG.info("trainingDatRequestDetailOutput"
					+ trainingDatRequestDetailOutput);

			for (int i = 0; i < trainingDatRequestDetailOutput.size(); i++) {

				String msg = MessageFormat.format(
						trainingApprovedRequestForReportingManager,
						trainingDatRequestDetailOutput.get(i)
								.getMasTrainingTopicName()
								.getTrainingTopicName());
				try {
					checkDuplicates = traNotificationRepository
							.findByEmpIdAndNoificationMessage(
									reportingManagerId, msg);
				} catch (Exception e) {
					throw new DataAccessException("Failed to Save : ", e);

				}

				if (checkDuplicates == null) {
					System.out.println(" Insert");

					trainingNotificationBO = new DatNotificationsBO();
					trainingNotificationBO.setEmpId(reportingManagerId);
					trainingNotificationBO.setNoificationMessage(msg);
					trainingNotificationBO
							.setNotificationcreatedDate(new Date());
					trainingNotificationBO.setClearFlagcreatedDate(new Date());
					trainingNotificationBO.setClearFlagModifiedDate(new Date());
					trainingNotificationBO.setClearFlag("NO");
					trainingNotificationBO.setModuleName("TRAINING");
					try {

						traNotificationRepository.save(trainingNotificationBO);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				} else if (String.valueOf(checkDuplicates.getEmpId())
						.equalsIgnoreCase(String.valueOf(reportingManagerId))
						&& checkDuplicates.getNoificationMessage()
								.equalsIgnoreCase(msg)) {
					System.out.println("update");

					checkDuplicates.setEmpId(reportingManagerId);
					checkDuplicates.setNoificationMessage(msg);
					checkDuplicates.setNotificationModifiedDate(new Date());
					checkDuplicates.setClearFlagcreatedDate(new Date());
					checkDuplicates.setClearFlagModifiedDate(new Date());
					checkDuplicates.setClearFlag("NO");
					checkDuplicates.setModuleName("TRAINING");

					try {

						traNotificationRepository.save(checkDuplicates);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				}

			}
			Sucess = "Notification Successfull ";

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}

	}

	
	 * Reject the Training Request (By Trainig Co-ordinator) ForRMScreen
	 
	public void rejectTheTrainingRequestByTrainigCoordinatorForRMScreen(
			int reportingManagerId, Date sDate, Date eDate)
			throws DataAccessException {
		byte rejectStatus = 5;
		List<TrainingDatRequestDetailBO> trainingDatRequestDetailOutput = new ArrayList<TrainingDatRequestDetailBO>();
		DatNotificationsBO trainingNotificationBO;
		String Sucess = "";
		DatNotificationsBO checkDuplicates;

		try {
			trainingDatRequestDetailOutput = trainingRequestRepository
					.findByStatusRequesterempIdAndtrainingModifiedDate(
							rejectStatus, reportingManagerId, sDate, eDate);
			LOG.info("trainingDatRequestDetailOutput"
					+ trainingDatRequestDetailOutput);

			for (int i = 0; i < trainingDatRequestDetailOutput.size(); i++) {

				String msg = MessageFormat.format(
						trainingRequestForReportingManagerReject,
						trainingDatRequestDetailOutput.get(i)
								.getMasTrainingTopicName()
								.getTrainingTopicName());
				try {
					checkDuplicates = traNotificationRepository
							.findByEmpIdAndNoificationMessage(
									reportingManagerId, msg);
				} catch (Exception e) {
					throw new DataAccessException("Failed to Save : ", e);

				}

				if (checkDuplicates == null) {
					System.out.println(" Insert");

					trainingNotificationBO = new DatNotificationsBO();
					trainingNotificationBO.setEmpId(reportingManagerId);
					trainingNotificationBO.setNoificationMessage(msg);
					trainingNotificationBO
							.setNotificationcreatedDate(new Date());
					trainingNotificationBO.setClearFlagcreatedDate(new Date());
					trainingNotificationBO.setClearFlagModifiedDate(new Date());
					trainingNotificationBO.setClearFlag("NO");
					trainingNotificationBO.setModuleName("TRAINING");
					try {

						traNotificationRepository.save(trainingNotificationBO);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				} else if (String.valueOf(checkDuplicates.getEmpId())
						.equalsIgnoreCase(String.valueOf(reportingManagerId))
						&& checkDuplicates.getNoificationMessage()
								.equalsIgnoreCase(msg)) {
					System.out.println("update");

					checkDuplicates.setEmpId(reportingManagerId);
					checkDuplicates.setNoificationMessage(msg);
					checkDuplicates.setNotificationModifiedDate(new Date());
					checkDuplicates.setClearFlagcreatedDate(new Date());
					checkDuplicates.setClearFlagModifiedDate(new Date());
					checkDuplicates.setClearFlag("NO");
					checkDuplicates.setModuleName("TRAINING");

					try {

						traNotificationRepository.save(checkDuplicates);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				}

			}
			Sucess = "Notification Successfull ";

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}

	}

	
	 * On Hold the Training Request (By Trainig Co-ordinator) ForRMScreen
	 
	public void onHoldTheTrainingRequestByTrainigCoordinatorForRMScreen(
			int reportingManagerId, Date sDate, Date eDate)
			throws DataAccessException {
		byte rejectStatus = 2;
		List<TrainingDatRequestDetailBO> trainingDatRequestDetailOutput = new ArrayList<TrainingDatRequestDetailBO>();
		DatNotificationsBO trainingNotificationBO;
		String Sucess = "";
		DatNotificationsBO checkDuplicates;

		try {
			trainingDatRequestDetailOutput = trainingRequestRepository
					.findByStatusRequesterempIdAndtrainingModifiedDate(
							rejectStatus, reportingManagerId, sDate, eDate);
			LOG.info("trainingDatRequestDetailOutput"
					+ trainingDatRequestDetailOutput);

			for (int i = 0; i < trainingDatRequestDetailOutput.size(); i++) {

				String msg = MessageFormat.format(
						trainingRequestForReportingManagerOnHold,
						trainingDatRequestDetailOutput.get(i)
								.getMasTrainingTopicName()
								.getTrainingTopicName());
				try {
					checkDuplicates = traNotificationRepository
							.findByEmpIdAndNoificationMessage(
									reportingManagerId, msg);
				} catch (Exception e) {
					throw new DataAccessException("Failed to Save : ", e);

				}

				if (checkDuplicates == null) {
					System.out.println(" Insert");

					trainingNotificationBO = new DatNotificationsBO();
					trainingNotificationBO.setEmpId(reportingManagerId);
					trainingNotificationBO.setNoificationMessage(msg);
					trainingNotificationBO
							.setNotificationcreatedDate(new Date());
					trainingNotificationBO.setClearFlagcreatedDate(new Date());
					trainingNotificationBO.setClearFlagModifiedDate(new Date());
					trainingNotificationBO.setClearFlag("NO");
					trainingNotificationBO.setModuleName("TRAINING");
					try {

						traNotificationRepository.save(trainingNotificationBO);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				} else if (String.valueOf(checkDuplicates.getEmpId())
						.equalsIgnoreCase(String.valueOf(reportingManagerId))
						&& checkDuplicates.getNoificationMessage()
								.equalsIgnoreCase(msg)) {
					System.out.println("update");

					checkDuplicates.setEmpId(reportingManagerId);
					checkDuplicates.setNoificationMessage(msg);
					checkDuplicates.setNotificationModifiedDate(new Date());
					checkDuplicates.setClearFlagcreatedDate(new Date());
					checkDuplicates.setClearFlagModifiedDate(new Date());
					checkDuplicates.setClearFlag("NO");
					checkDuplicates.setModuleName("TRAINING");

					try {

						traNotificationRepository.save(checkDuplicates);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				}

			}
			Sucess = "Notification Successfull ";

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}

	}

	
	 * Cancel the Training Request (By Trainig Co-ordinator) ForRMScreen
	 
	public void cancelTheTrainingRequestByTrainigCoordinatorForRMScreen(
			int reportingManagerId, Date sDate, Date eDate)
			throws DataAccessException {
		byte rejectStatus = 4;
		List<TrainingDatRequestDetailBO> trainingDatRequestDetailOutput = new ArrayList<TrainingDatRequestDetailBO>();
		DatNotificationsBO trainingNotificationBO;
		String Sucess = "";
		DatNotificationsBO checkDuplicates;

		try {
			trainingDatRequestDetailOutput = trainingRequestRepository
					.findByStatusRequesterempIdAndtrainingModifiedDate(
							rejectStatus, reportingManagerId, sDate, eDate);
			LOG.info("trainingDatRequestDetailOutput"
					+ trainingDatRequestDetailOutput);

			for (int i = 0; i < trainingDatRequestDetailOutput.size(); i++) {

				String msg = MessageFormat.format(
						trainingRequestForReportingManagerCancel,
						trainingDatRequestDetailOutput.get(i)
								.getMasTrainingTopicName()
								.getTrainingTopicName());
				try {
					checkDuplicates = traNotificationRepository
							.findByEmpIdAndNoificationMessage(
									reportingManagerId, msg);
				} catch (Exception e) {
					throw new DataAccessException("Failed to Save : ", e);

				}

				if (checkDuplicates == null) {
					System.out.println(" Insert");

					trainingNotificationBO = new DatNotificationsBO();
					trainingNotificationBO.setEmpId(reportingManagerId);
					trainingNotificationBO.setNoificationMessage(msg);
					trainingNotificationBO
							.setNotificationcreatedDate(new Date());
					trainingNotificationBO.setClearFlagcreatedDate(new Date());
					trainingNotificationBO.setClearFlagModifiedDate(new Date());
					trainingNotificationBO.setClearFlag("NO");
					trainingNotificationBO.setModuleName("TRAINING");
					try {

						traNotificationRepository.save(trainingNotificationBO);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				} else if (String.valueOf(checkDuplicates.getEmpId())
						.equalsIgnoreCase(String.valueOf(reportingManagerId))
						&& checkDuplicates.getNoificationMessage()
								.equalsIgnoreCase(msg)) {
					System.out.println("update");

					checkDuplicates.setEmpId(reportingManagerId);
					checkDuplicates.setNoificationMessage(msg);
					checkDuplicates.setNotificationModifiedDate(new Date());
					checkDuplicates.setClearFlagcreatedDate(new Date());
					checkDuplicates.setClearFlagModifiedDate(new Date());
					checkDuplicates.setClearFlag("NO");
					checkDuplicates.setModuleName("TRAINING");

					try {

						traNotificationRepository.save(checkDuplicates);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				}

			}
			Sucess = "Notification Successfull ";

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}

	}

	 Complete the Training Program(By Trainig Co-ordinator) for RM Screen 

	public void completeTheTrainingProgramByTrainigCoordinatorforRMScreen(
			int reportingManagerId) throws DataAccessException {
		List<TrainingDatRequestDetailBO> trainingDatRequestDetailOutput = new ArrayList<TrainingDatRequestDetailBO>();
		List<TrainingDatProgrammeDetailBO> trainingDatProgrammeDetailBOOutput = new ArrayList<TrainingDatProgrammeDetailBO>();
		DatNotificationsBO trainingNotificationBO;
		String Sucess = "";
		DatNotificationsBO checkDuplicates;
		try {

			trainingDatProgrammeDetailBOOutput = programRepository
					.findByprogramNotificationOnRmIDAndstatus(reportingManagerId);
			LOG.info("trainingDatProgrammeDetailBOOutput"
					+ trainingDatProgrammeDetailBOOutput);

			for (int i = 0; i < trainingDatRequestDetailOutput.size(); i++) {

				String msg = MessageFormat.format(trainingCompleteMsgForRM,
						trainingDatRequestDetailOutput.get(i)
								.getMasTrainingTopicName()
								.getTrainingTopicName());
				try {
					checkDuplicates = traNotificationRepository
							.findByEmpIdAndNoificationMessage(
									reportingManagerId, msg);
				} catch (Exception e) {
					throw new DataAccessException("Failed to Save : ", e);

				}

				if (checkDuplicates == null) {
					System.out.println(" Insert");

					trainingNotificationBO = new DatNotificationsBO();
					trainingNotificationBO.setEmpId(reportingManagerId);
					trainingNotificationBO.setNoificationMessage(msg);
					trainingNotificationBO
							.setNotificationcreatedDate(new Date());
					trainingNotificationBO.setClearFlagcreatedDate(new Date());
					trainingNotificationBO.setClearFlagModifiedDate(new Date());
					trainingNotificationBO.setClearFlag("NO");
					trainingNotificationBO.setModuleName("TRAINING");

					try {

						traNotificationRepository.save(trainingNotificationBO);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				} else if (String.valueOf(checkDuplicates.getEmpId())
						.equalsIgnoreCase(String.valueOf(reportingManagerId))
						&& checkDuplicates.getNoificationMessage()
								.equalsIgnoreCase(msg)) {
					System.out.println("update");

					checkDuplicates.setEmpId(reportingManagerId);
					checkDuplicates.setNoificationMessage(msg);
					checkDuplicates.setNotificationModifiedDate(new Date());
					checkDuplicates.setClearFlagcreatedDate(new Date());
					checkDuplicates.setClearFlagModifiedDate(new Date());
					checkDuplicates.setClearFlag("NO");
					checkDuplicates.setModuleName("TRAINING");

					try {

						traNotificationRepository.save(checkDuplicates);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				}

			}
			Sucess = "Notification Successfull ";

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}
	}

	 Create the Training Program(By Trainig Co-ordinator) 
	public String createTrainingProgramNotificationsByTcToRmScreen(
			int reportingManagerId) throws DataAccessException, ParseException {
		List<TrainingDatRequestDetailBO> trainingDatRequestDetailOutput = new ArrayList<TrainingDatRequestDetailBO>();
		DatNotificationsBO trainingNotificationBO;
		String Sucess = "";
		DatNotificationsBO checkDuplicates;

		try {
			trainingDatRequestDetailOutput = trainingRequestRepository
					.findByFkTrainingRequesterEmpIdAndIsTrainingProgramCreated(
							reportingManagerId,
							TrainingNotificationConstants.IS_PROG_CREATED);
			LOG.info("createTrainingProgramNotificationsByTcToRmScreen trainingDatRequestDetailOutput"
					+ trainingDatRequestDetailOutput);
			try {
				checkDuplicates = traNotificationRepository
						.findByEmpIdAndNoificationMessage(reportingManagerId,
								trainingCompleteMsgForReportingManager);
			} catch (Exception e) {
				throw new DataAccessException("Failed to Save : ", e);

			}

			for (int i = 0; i < trainingDatRequestDetailOutput.size(); i++) {

				if (checkDuplicates == null) {
					System.out.println(" Insert");

					trainingNotificationBO = new DatNotificationsBO();
					trainingNotificationBO.setEmpId(reportingManagerId);
					trainingNotificationBO
							.setNoificationMessage(trainingCompleteMsgForReportingManager);
					trainingNotificationBO
							.setNotificationcreatedDate(new Date());
					trainingNotificationBO.setClearFlagcreatedDate(new Date());
					trainingNotificationBO.setClearFlagModifiedDate(new Date());
					trainingNotificationBO.setClearFlag("NO");
					trainingNotificationBO.setModuleName("TRAINING");

					try {

						traNotificationRepository.save(trainingNotificationBO);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				} else if (String.valueOf(checkDuplicates.getEmpId())
						.equalsIgnoreCase(String.valueOf(reportingManagerId))
						&& checkDuplicates.getNoificationMessage()
								.equalsIgnoreCase(
										trainingCompleteMsgForReportingManager)) {
					System.out.println("update");

					checkDuplicates.setEmpId(reportingManagerId);
					checkDuplicates
							.setNoificationMessage(trainingCompleteMsgForReportingManager);
					checkDuplicates.setNotificationModifiedDate(new Date());
					checkDuplicates.setClearFlagcreatedDate(new Date());
					checkDuplicates.setClearFlagModifiedDate(new Date());
					checkDuplicates.setClearFlag("NO");
					checkDuplicates.setModuleName("TRAINING");

					try {

						traNotificationRepository.save(checkDuplicates);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				}

			}
			Sucess = "Notification Successfull ";

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}

		return Sucess;

	}

	
	 * EMPLOYEE SCREEN NOTIFICATION
	 * 
	 * Invite Reportees(By Manager)
	 
	List<TrainingDatNominationDetailBO> trainingDatNominationDetailBO = new ArrayList<TrainingDatNominationDetailBO>();
	DatNotificationsBO trainingNotificationBO;
	DatNotificationsBO checkDuplicates;

	public String inviteTrainingProgramNotificationsByRMToEmployeeScreen(
			int employeeId) throws DataAccessException, ParseException {
		
		 * List<TrainingDatNominationDetailBO> trainingDatNominationDetailBO =
		 * new ArrayList<TrainingDatNominationDetailBO>(); DatNotificationsBO
		 * trainingNotificationBO; DatNotificationsBO checkDuplicates;
		 

		try {
			trainingDatNominationDetailBO = trainingNominationDetailsRepository
					.findByNominatedEmpId(employeeId);
			LOG.info("trainingDatNominationDetailBO"
					+ trainingDatNominationDetailBO);

			for (int i = 0; i < trainingDatNominationDetailBO.size(); i++) {

				String msg = MessageFormat.format(trainingInviteMsgForEmploye,
						trainingDatNominationDetailBO.get(i)
								.getTrainingDatProgrammeDetail()
								.getTrainingDatRequestDetail()
								.getMasTrainingTopicName()
								.getTrainingTopicName());
				System.out.println("msg =" + msg);
				try {

					checkDuplicates = traNotificationRepository
							.findByEmpIdAndNoificationMessage(employeeId, msg);
				} catch (Exception e) {
					throw new DataAccessException("Failed to Save : ", e);

				}

				if (checkDuplicates == null) {
					System.out.println(" Insert");
					trainingNotificationBO = new DatNotificationsBO();
					trainingNotificationBO.setEmpId(employeeId);
					trainingNotificationBO.setNoificationMessage(msg);
					trainingNotificationBO
							.setNotificationcreatedDate(new Date());
					trainingNotificationBO.setClearFlagcreatedDate(new Date());
					trainingNotificationBO.setClearFlagModifiedDate(new Date());
					trainingNotificationBO.setClearFlag("NO");
					trainingNotificationBO.setModuleName("TRAINING");

					try {

						traNotificationRepository.save(trainingNotificationBO);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				} else if (String.valueOf(checkDuplicates.getEmpId())
						.equalsIgnoreCase(String.valueOf(employeeId))
						&& checkDuplicates.getNoificationMessage()
								.equalsIgnoreCase(msg)) {
					System.out.println("update");

					checkDuplicates.setEmpId(employeeId);
					checkDuplicates.setNoificationMessage(msg);
					checkDuplicates.setNotificationModifiedDate(new Date());
					checkDuplicates.setClearFlagcreatedDate(new Date());
					checkDuplicates.setClearFlagModifiedDate(new Date());
					checkDuplicates.setClearFlag("NO");
					checkDuplicates.setModuleName("TRAINING");

					try {

						traNotificationRepository.save(checkDuplicates);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				}

			}

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}
		
		 * Complete the Training Program(By Trainig Co-ordinator) to employee
		 * screen
		 
		CompletetheTrainingProgramByTrainigCoordinatortoemployeescreen(
				employeeId, TrainingNotificationConstants.ATTENDEDTRAININGNO);

		 Provide Feedback(By Employee) To RM SCREEN 
		provideFeedbackByEmployeeToRMSCREEN(employeeId,
				TrainingNotificationConstants.IS_FEEDBACK_CREATED);
		 Provide Feedback(By Employee) To Training Co-ordinator SCREEN 
		provideFeedbackByEmployeeToTrainingCoordinatorSCREEN(employeeId);
		return "Success";

	}

	
	 * Complete the Training Program(By Trainig Co-ordinator) to employee screen
	 
	public void CompletetheTrainingProgramByTrainigCoordinatortoemployeescreen(
			int employeeId, String AttendedTraining) throws DataAccessException {
		try {
			trainingDatNominationDetailBO = trainingNominationDetailsRepository
					.findByNominatedEmpIdForFeedbackAndTrainingProgrmCompleted(
							employeeId, AttendedTraining);
			LOG.info("trainingDatNominationDetailBO"
					+ trainingDatNominationDetailBO);

			for (int i = 0; i < trainingDatNominationDetailBO.size(); i++) {

				String msg = MessageFormat.format(
						trainingCompleteMsgForEmployee,
						trainingDatNominationDetailBO.get(i)
								.getTrainingDatProgrammeDetail()
								.getTrainingDatRequestDetail()
								.getMasTrainingTopicName()
								.getTrainingTopicName());
				System.out.println("msg =" + msg);
				try {

					checkDuplicates = traNotificationRepository
							.findByEmpIdAndNoificationMessage(employeeId, msg);
				} catch (Exception e) {
					throw new DataAccessException("Failed to Save : ", e);

				}

				if (checkDuplicates == null) {
					System.out.println(" Insert");
					trainingNotificationBO = new DatNotificationsBO();
					trainingNotificationBO.setEmpId(employeeId);
					trainingNotificationBO.setNoificationMessage(msg);
					trainingNotificationBO
							.setNotificationcreatedDate(new Date());
					trainingNotificationBO.setClearFlagcreatedDate(new Date());
					trainingNotificationBO.setClearFlagModifiedDate(new Date());
					trainingNotificationBO.setClearFlag("NO");
					trainingNotificationBO.setModuleName("TRAINING");

					try {

						traNotificationRepository.save(trainingNotificationBO);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				} else if (String.valueOf(checkDuplicates.getEmpId())
						.equalsIgnoreCase(String.valueOf(employeeId))
						&& checkDuplicates.getNoificationMessage()
								.equalsIgnoreCase(msg)) {
					System.out.println("update");

					checkDuplicates.setEmpId(employeeId);
					checkDuplicates.setNoificationMessage(msg);
					checkDuplicates.setNotificationModifiedDate(new Date());
					checkDuplicates.setClearFlagcreatedDate(new Date());
					checkDuplicates.setClearFlagModifiedDate(new Date());
					checkDuplicates.setClearFlag("NO");
					checkDuplicates.setModuleName("TRAINING");

					try {

						traNotificationRepository.save(checkDuplicates);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				}

			}

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}
	}

	 Provide Feedback(By Employee) To RM SCREEN 

	public void provideFeedbackByEmployeeToRMSCREEN(int employeeId,
			String isFeedbackCreated) throws DataAccessException {
		try {
			trainingDatNominationDetailBO = trainingNominationDetailsRepository
					.findByFkDatNominatedEmpIdAndIsFeedbackProvided(employeeId,
							isFeedbackCreated);
			LOG.info("*****************************trainingDatNominationDetailBO"
					+ trainingDatNominationDetailBO);

			for (int i = 0; i < trainingDatNominationDetailBO.size(); i++) {

				DatEmpPersonalDetailBO rmNameBO = personalDetailsRepository
						.findByFkEmpDetailId(trainingDatNominationDetailBO.get(
								i).getFkDatNominatedEmpId());
				String msg = MessageFormat.format(trainingFeedbackMsgForRM,
						rmNameBO.getEmpFirstName(),
						trainingDatNominationDetailBO.get(i)
								.getTrainingDatProgrammeDetail()
								.getTrainingDatRequestDetail()
								.getMasTrainingTopicName()
								.getTrainingTopicName());
				System.out.println("msg " + msg);
				LOG.info("notification msg" + msg);
				int rmID = trainingDatNominationDetailBO.get(i)
						.getFkNominatedByEmpId();
				try {

					checkDuplicates = traNotificationRepository
							.findByEmpIdAndNoificationMessage(rmID, msg);
				} catch (Exception e) {
					throw new DataAccessException("Failed to Save : ", e);

				}
				if (checkDuplicates == null) {
					System.out.println(" Insert");
					trainingNotificationBO = new DatNotificationsBO();
					trainingNotificationBO.setEmpId(rmID);
					trainingNotificationBO.setNoificationMessage(msg);
					trainingNotificationBO
							.setNotificationcreatedDate(new Date());
					trainingNotificationBO.setClearFlagcreatedDate(new Date());
					trainingNotificationBO.setClearFlagModifiedDate(new Date());
					trainingNotificationBO.setClearFlag("NO");
					trainingNotificationBO.setModuleName("TRAINING");

					try {

						traNotificationRepository.save(trainingNotificationBO);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				} else if (String.valueOf(checkDuplicates.getEmpId())
						.equalsIgnoreCase(String.valueOf(rmID))
						&& checkDuplicates.getNoificationMessage()
								.equalsIgnoreCase(msg)) {
					System.out.println("update");

					checkDuplicates.setEmpId(rmID);
					checkDuplicates.setNoificationMessage(msg);
					checkDuplicates.setNotificationModifiedDate(new Date());
					checkDuplicates.setClearFlagcreatedDate(new Date());
					checkDuplicates.setClearFlagModifiedDate(new Date());
					checkDuplicates.setClearFlag("NO");
					checkDuplicates.setModuleName("TRAINING");

					try {

						traNotificationRepository.save(checkDuplicates);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
				}

			}

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}
	}

	 Provide Feedback(By Employee) To Training Co-ordinator SCREEN 
	public void provideFeedbackByEmployeeToTrainingCoordinatorSCREEN(
			int employeeId) throws DataAccessException {
		try {
			trainingDatNominationDetailBO = trainingNominationDetailsRepository
					.findByFkDatNominatedEmpIdAndIsFeedbackProvided(employeeId,
							TrainingNotificationConstants.IS_FEEDBACK_CREATED);
			LOG.info("*****************************trainingDatNominationDetailBO"
					+ trainingDatNominationDetailBO);

			for (int i = 0; i < trainingDatNominationDetailBO.size(); i++) {

				List<DatEmpDetailBO> empDetailBo;
				DatEmpPersonalDetailBO rmNameBO = personalDetailsRepository
						.findByFkEmpDetailId(trainingDatNominationDetailBO.get(
								i).getFkNominatedByEmpId());
				String msg = MessageFormat.format(trainingFeedbackMsgForTC,
						rmNameBO.getEmpFirstName(),
						trainingDatNominationDetailBO.get(i)
								.getTrainingDatProgrammeDetail()
								.getTrainingDatRequestDetail()
								.getMasTrainingTopicName()
								.getTrainingTopicName());
				System.out.println("msg " + msg);
				LOG.info("notification msg" + msg);
				List<Short> roles = new ArrayList<Short>();
				roles.add((short) 21);
				roles.add((short) 20);
				try {

					empDetailBo = empDetailRepository
							.findByFkEmpRoleIdIn(roles);
					LOG.info("empDetailBo" + empDetailBo);
				} catch (Exception e) {
					throw new DataAccessException("Failed to Save : ", e);

				}

				for (int j = 0; j < empDetailBo.size(); j++) {
					int trainingCoordinatorID = empDetailBo.get(j).getPkEmpId();
					System.out.println("trainingCoordinatorID"
							+ trainingCoordinatorID);
					try {

						checkDuplicates = traNotificationRepository
								.findByEmpIdAndNoificationMessage(
										trainingCoordinatorID, msg);
						System.out.println("checkDuplicates" + checkDuplicates);

					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}

					if (checkDuplicates == null) {
						System.out.println(" Insert");

						trainingNotificationBO = new DatNotificationsBO();

						trainingNotificationBO.setEmpId(empDetailBo.get(j)
								.getPkEmpId());
						trainingNotificationBO.setNoificationMessage(msg);
						trainingNotificationBO
								.setNotificationcreatedDate(new Date());
						trainingNotificationBO
								.setClearFlagcreatedDate(new Date());
						trainingNotificationBO
								.setClearFlagModifiedDate(new Date());
						trainingNotificationBO.setClearFlag("NO");
						trainingNotificationBO.setModuleName("TRAINING");

						try {

							traNotificationRepository
									.save(trainingNotificationBO);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
					} else if (String.valueOf(checkDuplicates.getEmpId())
							.equalsIgnoreCase(
									String.valueOf(trainingCoordinatorID))
							&& checkDuplicates.getNoificationMessage()
									.equalsIgnoreCase(msg)) {
						System.out.println("update");

						checkDuplicates.setEmpId(empDetailBo.get(j)
								.getPkEmpId());
						checkDuplicates.setNoificationMessage(msg);
						checkDuplicates.setNotificationModifiedDate(new Date());
						checkDuplicates.setClearFlagcreatedDate(new Date());
						checkDuplicates.setClearFlagModifiedDate(new Date());
						checkDuplicates.setClearFlag("NO");
						checkDuplicates.setModuleName("TRAINING");

						try {

							traNotificationRepository.save(checkDuplicates);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}

					}

				}

			}

		} catch (Exception e) {
			throw new DataAccessException(
					"Failed to retrieve training request list : ", e);
		}

	}

	*//**
	 * 
	 * <Description setTrainingNotifications:> This method is used to set
	 * Training Module Notifications
	 * 
	 * @param empId
	 * @return String
	 * @throws DataAccessException
	 *//*
	public String setTrainingNotifications(int empId)
			throws DataAccessException {
		LOG.startUsecase("Entering setTrainingNotifications() service");

		DatEmpDetailBO empDetail = null;

		empDetail = empDetailRepository.findByPkEmpId(empId);

		List<Short> tcRoleIdList = Stream.of(tcRoleIds.split(","))
				.map(Short::parseShort).collect(Collectors.toList());

		if (tcRoleIdList.contains(empDetail.getFkEmpRoleId())) {
			
			 * Set Training Request By RM Notification to Training Coordinator
			 
			setTrainingRequestNotificationToTCScreen(empId);
		}

		
		 * Set Approved Training Request Notification to RM Screen
		 
		setApproveTrainingRequestNotificationToRmScreen(empId);

		
		 * Set Rejected Training Request Notification to RM Screen
		 
		setRejectTrainingRequestNotificationToRmScreen(empId);

		
		 * Set On Hold Training Request Notification to RM Screen
		 
		setOnHoldTrainingRequestNotificationToRmScreen(empId);
		
		
		 * SetCancelled Training Request Notification to RM Screen 
		 
		setCancelledTrainingRequestNotificationToRmScreen(empId);

		LOG.endUsecase("Exiting setTrainingNotifications() service");
		return "Success";
	}*/

	/**
	 * 
	 * <Description setTrainingRequestNotificationToTCScreen:> This method is
	 * used to set Training Request Notification to Training Coordinator 
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setTrainingRequestNotificationToTCScreen(int empId)
			throws DataAccessException {
		LOG.startUsecase("Entering setTrainingRequestNotificationToTCScreen() service");
		String rmCreTrainingReqKey = rmCreTrainingReqKeys;
		String msgIdentifierKey = rmCreTrainingReqKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		List<TrainingDatRequestDetailBO> reqDetails;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		int updateCount = 0;
		try {
			reqDetails = trainingRequestRepository
					.findByfkTrainingRequesterEmpIdNotNullAndFkTrainingRequestStatusId(TrainingNotificationConstants.NEW_TRAINING_REQUEST_STATUS);

			if (reqDetails.size() > 0) {
				for (TrainingDatRequestDetailBO trainingDatRequestDetailBO : reqDetails) {
					String fullmsgIdentifierKey = msgIdentifierKey
							+ "_"
							+ empId
							+ "_"
							+ trainingDatRequestDetailBO
									.getPkTrainingRequestId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(trainingRequest,
										gsrNotificationService
												.getEmployeeNameByEmpId(trainingDatRequestDetailBO
														.getFkTrainingRequesterEmpId()),
										trainingDatRequestDetailBO
												.getFkTrainingRequesterEmpId()
												.toString());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(trainingDatRequestDetailBO
										.getTrainingRequestedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(TrainingNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(TrainingNotificationConstants.MENU_ID);
						notificationDetails.setMenuUrl(rmCreTrainingReqKey
								.split(",")[0]);
						notificationDetails.setIsAction(rmCreTrainingReqKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(rmCreTrainingReqKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(trainingRequest,
										gsrNotificationService
												.getEmployeeNameByEmpId(trainingDatRequestDetailBO
														.getFkTrainingRequesterEmpId()),
										trainingDatRequestDetailBO
												.getFkTrainingRequesterEmpId()
												.toString());

						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= trainingDatRequestDetailBO
								.getTrainingRequestedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(trainingDatRequestDetailBO
											.getTrainingRequestedDate());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = rmCreTrainingReqKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									rmCreTrainingReqKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setTrainingRequestNotificationToTCScreen() service");
	}

	/**
	 * 
	 * <Description setApproveTrainingRequestNotificationToRmScreen:> This
	 * method is used to set Approved Training Request Notification to Requestor
	 * Screen 
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setApproveTrainingRequestNotificationToRmScreen(int empId)
			throws DataAccessException {
		LOG.startUsecase("Entering setApproveTrainingRequestNotificationToRmScreen() service");
		String tcApproveReqKey = tcApproveReqKeys;
		String msgIdentifierKey = tcApproveReqKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		List<TrainingDatRequestDetailBO> reqDetails;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		int updateCount = 0;
		try {
			reqDetails = trainingRequestRepository
					.findByFkTrainingRequesterEmpIdAndFkTrainingRequestStatusId(
							empId,
							TrainingNotificationConstants.APPROVED_TRAINING_REQUEST_STATUS);

			if (reqDetails.size() > 0) {
				for (TrainingDatRequestDetailBO trainingDatRequestDetailBO : reqDetails) {
					String fullmsgIdentifierKey = msgIdentifierKey
							+ "_"
							+ empId
							+ "_"
							+ trainingDatRequestDetailBO
									.getPkTrainingRequestId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								trainingApprovedRequestForReportingManager,
								trainingDatRequestDetailBO
										.getMasTrainingTopicName()
										.getTrainingTopicName());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(trainingDatRequestDetailBO
										.getTrainingModifiedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(TrainingNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(TrainingNotificationConstants.MENU_ID);
						notificationDetails.setMenuUrl(tcApproveReqKey
								.split(",")[0]);
						notificationDetails.setIsAction(tcApproveReqKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(tcApproveReqKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								trainingApprovedRequestForReportingManager,
								trainingDatRequestDetailBO
										.getMasTrainingTopicName()
										.getTrainingTopicName());

						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= trainingDatRequestDetailBO
								.getTrainingModifiedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(trainingDatRequestDetailBO
											.getTrainingModifiedDate());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = tcApproveReqKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									tcApproveReqKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setApproveTrainingRequestNotificationToRmScreen() service");
	}

	/**
	 * 
	 * <Description setApproveTrainingRequestNotificationToRmScreen:> This
	 * method is used to set Rejected Training Request Notification to Requestor
	 * Screen 
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setRejectTrainingRequestNotificationToRmScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setRejectTrainingRequestNotificationToRmScreen() service");
		String tcRejectReqKey = tcRejectReqKeys;
		String msgIdentifierKey = tcRejectReqKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		List<TrainingDatRequestDetailBO> reqDetails;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		int updateCount = 0;
		try {
			reqDetails = trainingRequestRepository
					.findByFkTrainingRequesterEmpIdAndFkTrainingRequestStatusId(
							empId,
							TrainingNotificationConstants.REJECTED_TRAINING_REQUEST_STATUS);

			if (reqDetails.size() > 0) {
				for (TrainingDatRequestDetailBO trainingDatRequestDetailBO : reqDetails) {
					String fullmsgIdentifierKey = msgIdentifierKey
							+ "_"
							+ empId
							+ "_"
							+ trainingDatRequestDetailBO
									.getPkTrainingRequestId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								trainingRequestForReportingManagerReject,
								trainingDatRequestDetailBO
										.getMasTrainingTopicName()
										.getTrainingTopicName());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(trainingDatRequestDetailBO
										.getTrainingModifiedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(TrainingNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(TrainingNotificationConstants.MENU_ID);
						notificationDetails.setMenuUrl(tcRejectReqKey
								.split(",")[0]);
						notificationDetails.setIsAction(tcRejectReqKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(tcRejectReqKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								trainingRequestForReportingManagerReject,
								trainingDatRequestDetailBO
										.getMasTrainingTopicName()
										.getTrainingTopicName());

						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= trainingDatRequestDetailBO
								.getTrainingModifiedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(trainingDatRequestDetailBO
											.getTrainingModifiedDate());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = tcRejectReqKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									tcRejectReqKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setRejectTrainingRequestNotificationToRmScreen() service");
	}

	/**
	 * 
	 * <Description setApproveTrainingRequestNotificationToRmScreen:> This
	 * method is used to set On Hold Training Request Notification to Requestor
	 * Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setOnHoldTrainingRequestNotificationToRmScreen(int empId)
			throws DataAccessException {
		LOG.startUsecase("Entering setOnHoldTrainingRequestNotificationToRmScreen() service");
		String tcOnholdReqKey = tcOnholdReqKeys;
		String msgIdentifierKey = tcOnholdReqKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		List<TrainingDatRequestDetailBO> reqDetails;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		int updateCount = 0;
		try {
			reqDetails = trainingRequestRepository
					.findByFkTrainingRequesterEmpIdAndFkTrainingRequestStatusId(
							empId,
							TrainingNotificationConstants.ONHOLD_TRAINING_REQUEST_STATUS);

			if (reqDetails.size() > 0) {
				for (TrainingDatRequestDetailBO trainingDatRequestDetailBO : reqDetails) {
					String fullmsgIdentifierKey = msgIdentifierKey
							+ "_"
							+ empId
							+ "_"
							+ trainingDatRequestDetailBO
									.getPkTrainingRequestId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								trainingRequestForReportingManagerOnHold,
								trainingDatRequestDetailBO
										.getMasTrainingTopicName()
										.getTrainingTopicName());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(trainingDatRequestDetailBO
										.getTrainingModifiedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(TrainingNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(TrainingNotificationConstants.MENU_ID);
						notificationDetails.setMenuUrl(tcOnholdReqKey
								.split(",")[0]);
						notificationDetails.setIsAction(tcOnholdReqKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(tcOnholdReqKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								trainingRequestForReportingManagerOnHold,
								trainingDatRequestDetailBO
										.getMasTrainingTopicName()
										.getTrainingTopicName());

						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= trainingDatRequestDetailBO
								.getTrainingModifiedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(trainingDatRequestDetailBO
											.getTrainingModifiedDate());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = tcOnholdReqKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									tcOnholdReqKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setOnHoldTrainingRequestNotificationToRmScreen() service");
	}
	
	/**
	 * 
	 * <Description setApproveTrainingRequestNotificationToRmScreen:> This
	 * method is used to set Cancelled Training Request Notification to Requestor
	 * Screen when Requestor Logs In
	 * 
	 * @param empId
	 * @throws DataAccessException
	 *//*
	public void setCancelledTrainingRequestNotificationToRmScreen(int empId) throws DataAccessException{

		LOG.startUsecase("Entering setCancelledTrainingRequestNotificationToRmScreen() service");
		String tcCancelReqKey = tcCancelReqKeys;
		String msgIdentifierKey = tcCancelReqKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		List<TrainingDatRequestDetailBO> reqDetails;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		int updateCount = 0;
		try {
			reqDetails = trainingRequestRepository
					.findByFkTrainingRequesterEmpIdAndFkTrainingRequestStatusId(
							empId,
							TrainingNotificationConstants.CANCELED_TRAINING_REQUEST_STATUS);

			if (reqDetails.size() > 0) {
				for (TrainingDatRequestDetailBO trainingDatRequestDetailBO : reqDetails) {
					String fullmsgIdentifierKey = msgIdentifierKey
							+ "_"
							+ empId
							+ "_"
							+ trainingDatRequestDetailBO
									.getPkTrainingRequestId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								trainingRequestForReportingManagerCancel,
								trainingDatRequestDetailBO
										.getMasTrainingTopicName()
										.getTrainingTopicName());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(trainingDatRequestDetailBO
										.getTrainingModifiedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(TrainingNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(TrainingNotificationConstants.MENU_ID);
						notificationDetails.setMenuUrl(tcCancelReqKey
								.split(",")[0]);
						notificationDetails.setIsAction(tcCancelReqKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(tcCancelReqKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								trainingRequestForReportingManagerCancel,
								trainingDatRequestDetailBO
										.getMasTrainingTopicName()
										.getTrainingTopicName());

						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= trainingDatRequestDetailBO
								.getTrainingModifiedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(trainingDatRequestDetailBO
											.getTrainingModifiedDate());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = tcCancelReqKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									tcCancelReqKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setCancelledTrainingRequestNotificationToRmScreen() service");
	}*/
	
	/**
	 * 
	 * <Description setProgramCreatedForRequestNotificationToRequestorScreen:>
	 * This method is used to set training program created notification
	 * 
	 * @param empId
	 * @param progId
	 * @throws DataAccessException
	 */
	public void setProgramCreatedForRequestNotificationToRequestorScreen(
			int empId, int progId) throws DataAccessException {

		LOG.startUsecase("Entering setProgramCreatedForRequestNotificationToRequestorScreen() service");
		String tcCreProgReqKey = tcCreProgReqKeys;
		String msgIdentifierKey = tcCreProgReqKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		TrainingDatProgrammeDetailBO reqDetails;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		int updateCount = 0;
		try {
			reqDetails = programRepository.findByPkTrainingProgrammeId(progId);

			if (reqDetails != null) {
				String fullmsgIdentifierKey = msgIdentifierKey
						+ "_"
						+ empId
						+ "_"
						+ reqDetails.getTrainingDatRequestDetail()
								.getPkTrainingRequestId();
				fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
				isDuplicatesFound = gsrNotificationService.checkDuplicates(
						empId, fullmsgIdentifierKey);
				if (isDuplicatesFound == null) {
					DatNotificationsBO notificationDetails = new DatNotificationsBO();
					String notificationMessage = MessageFormat.format(
							trainingCreatedForRequestToRmIndividualMsg,
							reqDetails.getTrainingDatRequestDetail()
									.getMasTrainingTopicName()
									.getTrainingTopicName());
					notificationDetails.setEmpId(empId);
					notificationDetails
							.setNoificationMessage(notificationMessage);
					notificationDetails
							.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
					notificationDetails.setNotificationcreatedDate(reqDetails
							.getProgrammeCreatedDate());
					notificationDetails.setClearFlagModifiedDate(new Date());
					notificationDetails.setClearFlagcreatedDate(new Date());
					notificationDetails
							.setModuleName(TrainingNotificationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(TrainingNotificationConstants.MENU_ID);
					notificationDetails
							.setMenuUrl(tcCreProgReqKey.split(",")[0]);
					notificationDetails
							.setIsAction(tcCreProgReqKey.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(tcCreProgReqKey.split(",")[3]));
					try {
						notificationRepository.save(notificationDetails);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				} else {
					String notificationMessage = MessageFormat.format(
							trainingCreatedForRequestToRmIndividualMsg,
							reqDetails.getTrainingDatRequestDetail()
									.getMasTrainingTopicName()
									.getTrainingTopicName());

					isDuplicatesFound
							.setNoificationMessage(notificationMessage);
					isDuplicatesFound.setNotificationModifiedDate(new Date());
					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= reqDetails.getProgrammeCreatedDate()
							.getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound.setNotificationcreatedDate(reqDetails
								.getProgrammeCreatedDate());
					try {
						notificationRepository.save(isDuplicatesFound);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				}
			} else {
				String menuURL = tcCreProgReqKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									tcCreProgReqKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setProgramCreatedForRequestNotificationToRequestorScreen() service");
	}
	
	/**
	 * 
	 * <Description setPostApplicationFeedbackProvidedNotificationToTCScreen:>
	 * This method is used to set Post Application feedback provided
	 * notification to Training Co-ordinator screen
	 * 
	 * @param empId
	 * @param progId
	 * @throws DataAccessException
	 */
	public void setPostApplicationFeedbackProvidedNotificationToTCScreen(
			int empId, int progId) throws DataAccessException {

		LOG.startUsecase("Entering setPostApplicationFeedbackProvidedNotificationToTCScreen() service");
		String rmPostAppFeedbackKey = rmPostAppFeedbackKeys;
		String msgIdentifierKey = rmPostAppFeedbackKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		TrainingDatProgrammeDetailBO reqDetails;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		int updateCount = 0;
		try {
			reqDetails = programRepository.findByPkTrainingProgrammeId(progId);

			if (reqDetails != null) {
				String fullmsgIdentifierKey = msgIdentifierKey
						+ "_"
						+ empId
						+ "_"
						+ reqDetails.getTrainingDatRequestDetail()
								.getPkTrainingRequestId();
				fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
				isDuplicatesFound = gsrNotificationService.checkDuplicates(
						empId, fullmsgIdentifierKey);
				if (isDuplicatesFound == null) {
					DatNotificationsBO notificationDetails = new DatNotificationsBO();
					String notificationMessage = MessageFormat.format(
							postApplicationFeedbackToTCScreen,
							gsrNotificationService
									.getEmployeeNameByEmpId(reqDetails
											.getTrainingDatRequestDetail()
											.getFkTrainingRequesterEmpId()),
							reqDetails.getTrainingDatRequestDetail()
									.getMasTrainingTopicName()
									.getTrainingTopicName());
					notificationDetails.setEmpId(empId);
					notificationDetails
							.setNoificationMessage(notificationMessage);
					notificationDetails
							.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
					notificationDetails.setNotificationcreatedDate(new Date());
					notificationDetails.setClearFlagModifiedDate(new Date());
					notificationDetails.setClearFlagcreatedDate(new Date());
					notificationDetails
							.setModuleName(TrainingNotificationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(TrainingNotificationConstants.MENU_ID);
					notificationDetails.setMenuUrl(rmPostAppFeedbackKey
							.split(",")[0]);
					notificationDetails.setIsAction(rmPostAppFeedbackKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(rmPostAppFeedbackKey.split(",")[3]));
					try {
						notificationRepository.save(notificationDetails);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				} else {
					String notificationMessage = MessageFormat.format(
							postApplicationFeedbackToTCScreen,
							gsrNotificationService
									.getEmployeeNameByEmpId(reqDetails
											.getTrainingDatRequestDetail()
											.getFkTrainingRequesterEmpId()),
							reqDetails.getTrainingDatRequestDetail()
									.getMasTrainingTopicName()
									.getTrainingTopicName());

					isDuplicatesFound
							.setNoificationMessage(notificationMessage);
					isDuplicatesFound.setNotificationModifiedDate(new Date());
					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= new Date().getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound
								.setNotificationcreatedDate(new Date());
					try {
						notificationRepository.save(isDuplicatesFound);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				}
			} else {
				String menuURL = rmPostAppFeedbackKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									rmPostAppFeedbackKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setPostApplicationFeedbackProvidedNotificationToTCScreen() service");
	}
	
	/**
	 * 
	 * <Description setTrainingFeedbackProvidedNotificationToRmScreen:> This
	 * method is used to set Attendee feedback notification to RM Screen
	 * 
	 * @param empId
	 * @param progId
	 * @throws DataAccessException
	 */
	public void setTrainingFeedbackProvidedNotificationToRmScreen(int empId,
			int progId) throws DataAccessException {

		LOG.startUsecase("Entering setTrainingFeedbackProvidedNotificationToRmScreen() service");
		String empFeedbackToRmKey = empFeedbackToRmKeys;
		String msgIdentifierKey = empFeedbackToRmKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		List<TrainingDatAttendeeFeedbackBO> feedbackDetailsList;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		int updateCount = 0;
		try {

			feedbackDetailsList = attendeeFeedbackRepository
					.getFeedbackAttendeesByProgIdForRm(progId,empId);

			if (feedbackDetailsList.size() > 0) {
				if (feedbackDetailsList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey
							+ "_"
							+ empId
							+ "_"
							+ feedbackDetailsList.get(0)
									.getTrainingDatProgrammeDetail()
									.getFkTrainingRequestId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(attendeeFeedbackToRmIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(feedbackDetailsList
														.get(0)
														.getFkAttendeeEmpId()),
										feedbackDetailsList
												.get(0)
												.getTrainingDatProgrammeDetail()
												.getTrainingDatRequestDetail()
												.getMasTrainingTopicName()
												.getTrainingTopicName());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(feedbackDetailsList
										.get(0)
										.getAttendeeFeedbackCreatedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(TrainingNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(TrainingNotificationConstants.MENU_ID);
						notificationDetails.setMenuUrl(empFeedbackToRmKey
								.split(",")[0]);
						notificationDetails.setIsAction(empFeedbackToRmKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(empFeedbackToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(attendeeFeedbackToRmIndividualMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(feedbackDetailsList
														.get(0)
														.getFkAttendeeEmpId()),
										feedbackDetailsList
												.get(0)
												.getTrainingDatProgrammeDetail()
												.getTrainingDatRequestDetail()
												.getMasTrainingTopicName()
												.getTrainingTopicName());
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= feedbackDetailsList.get(0)
								.getAttendeeFeedbackCreatedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(feedbackDetailsList
											.get(0)
											.getAttendeeFeedbackCreatedDate());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey
							+ "_"
							+ empId
							+ "_"
							+ feedbackDetailsList.get(0)
									.getTrainingDatProgrammeDetail()
									.getFkTrainingRequestId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						Date maxDate = feedbackDetailsList
								.stream()
								.map(TrainingDatAttendeeFeedbackBO::getAttendeeFeedbackCreatedDate)
								.max(Date::compareTo).get();
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								attendeeFeedbackToRmConsolidatedMsg,
								feedbackDetailsList.size(), feedbackDetailsList
										.get(0).getTrainingDatProgrammeDetail()
										.getTrainingDatRequestDetail()
										.getMasTrainingTopicName()
										.getTrainingTopicName());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(maxDate);
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(TrainingNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(TrainingNotificationConstants.MENU_ID);
						notificationDetails.setMenuUrl(empFeedbackToRmKey
								.split(",")[0]);
						notificationDetails.setIsAction(empFeedbackToRmKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(empFeedbackToRmKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								attendeeFeedbackToRmConsolidatedMsg,
								feedbackDetailsList.size(), feedbackDetailsList
										.get(0).getTrainingDatProgrammeDetail()
										.getTrainingDatRequestDetail()
										.getMasTrainingTopicName()
										.getTrainingTopicName());

						Date maxDate = feedbackDetailsList
								.stream()
								.map(TrainingDatAttendeeFeedbackBO::getAttendeeFeedbackCreatedDate)
								.max(Date::compareTo).get();
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(maxDate);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = empFeedbackToRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									empFeedbackToRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setTrainingFeedbackProvidedNotificationToRmScreen() service");
	}
	
	/**
	 * 
	 * <Description setTrainingFeedbackProvidedNotificationToTCScreen:> This
	 * method is used to set Attendee feedback provided notification to Training
	 * Co-ordinator screen
	 * 
	 * @param empId
	 * @param progId
	 * @throws DataAccessException
	 */
	public void setTrainingFeedbackProvidedNotificationToTCScreen(int empId,
			int progId) throws DataAccessException {

		LOG.startUsecase("Entering setTrainingFeedbackProvidedNotificationToTCScreen() service");
		String empFeedbackToTCKey = empFeedbackToTCKeys;
		String msgIdentifierKey = empFeedbackToTCKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		List<TrainingDatAttendeeFeedbackBO> feedbackDetailsList;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		int updateCount = 0;
		try {

			feedbackDetailsList = attendeeFeedbackRepository
					.getFeedbackAttendeesByProgId(progId);

			if (feedbackDetailsList.size() > 0) {
				if (feedbackDetailsList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey
							+ "_"
							+ empId
							+ "_"
							+ feedbackDetailsList.get(0)
									.getTrainingDatProgrammeDetail()
									.getFkTrainingRequestId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat
								.format(attendeeFeedbackToRmConsolidatedMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(feedbackDetailsList
														.get(0)
														.getFkAttendeeEmpId()),
														gsrNotificationService
														.getEmployeeNameByEmpId(empId),
										feedbackDetailsList
												.get(0)
												.getTrainingDatProgrammeDetail()
												.getTrainingDatRequestDetail()
												.getMasTrainingTopicName()
												.getTrainingTopicName());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(feedbackDetailsList
										.get(0)
										.getAttendeeFeedbackCreatedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(TrainingNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(TrainingNotificationConstants.MENU_ID);
						notificationDetails.setMenuUrl(empFeedbackToTCKey
								.split(",")[0]);
						notificationDetails.setIsAction(empFeedbackToTCKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(empFeedbackToTCKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat
								.format(attendeeFeedbackToRmConsolidatedMsg,
										gsrNotificationService
												.getEmployeeNameByEmpId(feedbackDetailsList
														.get(0)
														.getFkAttendeeEmpId()),
														gsrNotificationService
														.getEmployeeNameByEmpId(empId),
										feedbackDetailsList
												.get(0)
												.getTrainingDatProgrammeDetail()
												.getTrainingDatRequestDetail()
												.getMasTrainingTopicName()
												.getTrainingTopicName());
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= feedbackDetailsList.get(0)
								.getAttendeeFeedbackCreatedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(feedbackDetailsList
											.get(0)
											.getAttendeeFeedbackCreatedDate());
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					String fullmsgIdentifierKey = msgIdentifierKey
							+ "_"
							+ empId
							+ "_"
							+ feedbackDetailsList.get(0)
									.getTrainingDatProgrammeDetail()
									.getFkTrainingRequestId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

					isDuplicatesFound = gsrNotificationService.checkDuplicates(
							empId, fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						Date maxDate = feedbackDetailsList
								.stream()
								.map(TrainingDatAttendeeFeedbackBO::getAttendeeFeedbackCreatedDate)
								.max(Date::compareTo).get();
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								attendeeFeedbackToTCConsolidatedMsg,
								feedbackDetailsList.size(), feedbackDetailsList
										.get(0).getTrainingDatProgrammeDetail()
										.getTrainingDatRequestDetail()
										.getMasTrainingTopicName()
										.getTrainingTopicName());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(maxDate);
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(TrainingNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(TrainingNotificationConstants.MENU_ID);
						notificationDetails.setMenuUrl(empFeedbackToTCKey
								.split(",")[0]);
						notificationDetails.setIsAction(empFeedbackToTCKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(empFeedbackToTCKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								attendeeFeedbackToTCConsolidatedMsg,
								feedbackDetailsList.size(), feedbackDetailsList
										.get(0).getTrainingDatProgrammeDetail()
										.getTrainingDatRequestDetail()
										.getMasTrainingTopicName()
										.getTrainingTopicName());

						Date maxDate = feedbackDetailsList
								.stream()
								.map(TrainingDatAttendeeFeedbackBO::getAttendeeFeedbackCreatedDate)
								.max(Date::compareTo).get();
						if (!isDuplicatesFound.getNoificationMessage()
								.equalsIgnoreCase(notificationMessage)) {
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							isDuplicatesFound
									.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						}
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= maxDate.getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(maxDate);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = empFeedbackToTCKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									empFeedbackToTCKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setTrainingFeedbackProvidedNotificationToTCScreen() service");
	}
	
	/**
	 * 
	 * <Description setTcCreateProgramNotificationToEmpScreen:> This method is
	 * used to set Training Co-ordinator create Training program notification to
	 * Employee Screen
	 * 
	 * @param progId
	 * @throws DataAccessException
	 */
	public void setTcCreateProgramNotificationToEmpScreen(int progId)
			throws DataAccessException {

		LOG.startUsecase("Entering setTcCreateProgramNotificationToEmpScreen() service");
		String tcCreProgKey = tcCreProgKeys;
		String msgIdentifierKey = tcCreProgKey.split(",")[2];
		TrainingDatProgrammeDetailBO progDetail = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		List<String> participantsLevelList = new ArrayList<String>();
		List<Integer> empIdList = new ArrayList<Integer>();
		List<DatNotificationsBO> notificationsList = new ArrayList<DatNotificationsBO>();
		try {
			progDetail = programRepository.findByPkTrainingProgrammeId(progId);
			if (progDetail != null) {
				participantsLevelList = Arrays.asList(progDetail
						.getTrainingDatRequestDetail().getLevelOfParticipants()
						.split(","));

				if (participantsLevelList.size() > 0) {
					empIdList = empProfRepository
							.getEmpIdByRoleNameList(participantsLevelList);
					if (empIdList.size() > 0) {
						for (Integer empId : empIdList) {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_" + empId + "_"
									+ progDetail.getPkTrainingProgrammeId();

							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat.format(
									trainingCreatedByTCToEmpIndividualMsg,
									progDetail.getTrainingDatRequestDetail()
											.getMasTrainingTopicName()
											.getTrainingTopicName());
							notificationDetails.setEmpId(empId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(progDetail
											.getProgrammeCreatedDate());
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(TrainingNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(TrainingNotificationConstants.MENU_ID);
							notificationDetails.setMenuUrl(tcCreProgKey
									.split(",")[0]);
							notificationDetails.setIsAction(tcCreProgKey
									.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(tcCreProgKey.split(",")[3]));
							notificationsList.add(notificationDetails);
						}
						try {
							notificationRepository.save(notificationsList);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setTcCreateProgramNotificationToEmpScreen() service");
	}
	
	/**
	 * 
	 * <Description setTcCreateProgramNotificationToRmScreen:> This method is
	 * used to set Training co-ordinator create training program notification to
	 * RM Screen
	 * 
	 * @param progId
	 * @throws DataAccessException
	 */
	public void setTcCreateProgramNotificationToRmScreen(int progId)
			throws DataAccessException {

		LOG.startUsecase("Entering setTcCreateProgramNotificationToRmScreen() service");
		String tcCreProgRmKey = tcCreProgRmKeys;
		String msgIdentifierKey = tcCreProgRmKey.split(",")[2];
		TrainingDatProgrammeDetailBO progDetail = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		List<String> participantsLevelList = new ArrayList<String>();
		List<Integer> empIdList = new ArrayList<Integer>();
		List<DatNotificationsBO> notificationsList = new ArrayList<DatNotificationsBO>();
		try {
			progDetail = programRepository.findByPkTrainingProgrammeId(progId);
			if (progDetail != null) {
				participantsLevelList = Arrays.asList(progDetail
						.getTrainingDatRequestDetail().getLevelOfParticipants()
						.split(","));

				if (participantsLevelList.size() > 0) {
					empIdList = empProfRepository
							.getRmIdByRoleNameList(participantsLevelList);
					if (empIdList.size() > 0) {
						for (Integer empId : empIdList) {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_" + empId + "_"
									+ progDetail.getPkTrainingProgrammeId();

							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat.format(
									trainingCreatedByTCToRMIndividualMsg,
									progDetail.getTrainingDatRequestDetail()
											.getMasTrainingTopicName()
											.getTrainingTopicName());
							notificationDetails.setEmpId(empId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(progDetail
											.getProgrammeCreatedDate());
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(TrainingNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(TrainingNotificationConstants.MENU_ID);
							notificationDetails.setMenuUrl(tcCreProgRmKey
									.split(",")[0]);
							notificationDetails.setIsAction(tcCreProgRmKey
									.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(tcCreProgRmKey.split(",")[3]));
							notificationsList.add(notificationDetails);
						}
						try {
							notificationRepository.save(notificationsList);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setTcCreateProgramNotificationToRmScreen() service");
	}
	
	/**
	 * 
	 * <Description setEmpInterestedInProgNotificationToRmScreen:> This method
	 * is used to set Employee Interested in Training Program notification to RM
	 * Screen
	 * 
	 * @param empId
	 * @throws DataAccessException
	 */
	public void setEmpInterestedInProgNotificationToRmScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering setEmpInterestedInProgNotificationToRmScreen() service");
		String empInterestedProgRmKey = empInterestedProgRmKeys;
		String msgIdentifierKey = empInterestedProgRmKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		List<TrainingDatNominationPoolDetailBO> nominationReqList;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		Map<Integer, List<TrainingDatNominationPoolDetailBO>> nomDetailsMapByProgId = new HashMap<Integer, List<TrainingDatNominationPoolDetailBO>>();
		int updateCount = 0;
		try {

			nominationReqList = nominationPoolRepository
					.findByFkApproverEmpIdAndNominationStatus(
							empId,
							TrainingNotificationConstants.NOMINATION_POOL_INTERESTED_STATUS);

			if (nominationReqList.size() > 0) {

				for (TrainingDatNominationPoolDetailBO nomDetail : nominationReqList) {
					if (!nomDetailsMapByProgId.containsKey(nomDetail
							.getFkTrainingProgrammeId())) {
						List<TrainingDatNominationPoolDetailBO> finalBean = new ArrayList<TrainingDatNominationPoolDetailBO>();
						finalBean.add(nomDetail);
						nomDetailsMapByProgId
								.put(nomDetail.getFkTrainingProgrammeId(),
										finalBean);
						continue;
					}
					if (nomDetailsMapByProgId.containsKey(nomDetail
							.getFkTrainingProgrammeId())) {
						List<TrainingDatNominationPoolDetailBO> finalBean = new ArrayList<TrainingDatNominationPoolDetailBO>();
						finalBean.add(nomDetail);
						nomDetailsMapByProgId.get(
								nomDetail.getFkTrainingProgrammeId()).addAll(
								finalBean);
					}
				}

				for (Map.Entry<Integer, List<TrainingDatNominationPoolDetailBO>> entry : nomDetailsMapByProgId
						.entrySet()) {
					List<TrainingDatNominationPoolDetailBO> nomDetails = (List<TrainingDatNominationPoolDetailBO>) entry
							.getValue();

					if (nomDetails.size() == 1) {
						String fullmsgIdentifierKey = msgIdentifierKey + "_"
								+ empId + "_"
								+ nomDetails.get(0).getFkTrainingProgrammeId();
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

						isDuplicatesFound = gsrNotificationService
								.checkDuplicates(empId, fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat.format(
									empInterestedInProgToRmIndividualMsg,
									gsrNotificationService
											.getEmployeeNameByEmpId(nomDetails
													.get(0)
													.getFkNominatedEmpId()),
									nomDetails.get(0)
											.getTrainingDatProgrammeDetail()
											.getTrainingDatRequestDetail()
											.getMasTrainingTopicName()
											.getTrainingTopicName());
							notificationDetails.setEmpId(empId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(nomDetails.get(
											0).getNominationPoolCreatedDate());
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(TrainingNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(TrainingNotificationConstants.MENU_ID);
							notificationDetails
									.setMenuUrl(empInterestedProgRmKey
											.split(",")[0]);
							notificationDetails
									.setIsAction(empInterestedProgRmKey
											.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(empInterestedProgRmKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat.format(
									empInterestedInProgToRmIndividualMsg,
									gsrNotificationService
											.getEmployeeNameByEmpId(nomDetails
													.get(0)
													.getFkNominatedEmpId()),
									nomDetails.get(0)
											.getTrainingDatProgrammeDetail()
											.getTrainingDatRequestDetail()
											.getMasTrainingTopicName()
											.getTrainingTopicName());
							if (!isDuplicatesFound.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNoificationIsExpiry(ConfirmationConstants.IS_EXPIRY_NO);
								isDuplicatesFound
										.setClearFlag(ConfirmationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= nomDetails.get(0)
									.getNominationPoolCreatedDate().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(nomDetails
												.get(0)
												.getNominationPoolCreatedDate());
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					} else {
						String fullmsgIdentifierKey = msgIdentifierKey + "_"
								+ empId + "_"
								+ nomDetails.get(0).getFkTrainingProgrammeId();
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

						isDuplicatesFound = gsrNotificationService
								.checkDuplicates(empId, fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							Date maxDate = nomDetails
									.stream()
									.map(TrainingDatNominationPoolDetailBO::getNominationPoolCreatedDate)
									.max(Date::compareTo).get();
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat.format(
									empInterestedInProgToRmConsolidatedMsg,
									nomDetails.size(), nomDetails.get(0)
											.getTrainingDatProgrammeDetail()
											.getTrainingDatRequestDetail()
											.getMasTrainingTopicName()
											.getTrainingTopicName());
							notificationDetails.setEmpId(empId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(maxDate);
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(TrainingNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(TrainingNotificationConstants.MENU_ID);
							notificationDetails
									.setMenuUrl(empInterestedProgRmKey
											.split(",")[0]);
							notificationDetails
									.setIsAction(empInterestedProgRmKey
											.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(empInterestedProgRmKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat.format(
									empInterestedInProgToRmConsolidatedMsg,
									nomDetails.size(), nomDetails.get(0)
											.getTrainingDatProgrammeDetail()
											.getTrainingDatRequestDetail()
											.getMasTrainingTopicName()
											.getTrainingTopicName());

							Date maxDate = nomDetails
									.stream()
									.map(TrainingDatNominationPoolDetailBO::getNominationPoolCreatedDate)
									.max(Date::compareTo).get();
							if (!isDuplicatesFound.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)) {
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								isDuplicatesFound
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
							}
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= maxDate.getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(maxDate);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					}
				}
			} else {
				String menuURL = empInterestedProgRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									empInterestedProgRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setEmpInterestedInProgNotificationToRmScreen() service");
	}
	
	/**
	 * 
	 * <Description setRmApprovedEmpRequestToEmpScreen:> This method is used to
	 * set RM approve Employee Request Notification to Employee Screen
	 * 
	 * @param empId
	 * @param progId
	 * @throws DataAccessException
	 */
	public void setRmApprovedEmpRequestToEmpScreen(int empId,int progId) throws DataAccessException {

		LOG.startUsecase("Entering setRmApprovedEmpRequestToEmpScreen() service");
		String rmApproveEmpReqKey = rmApproveEmpReqKeys;
		String msgIdentifierKey = rmApproveEmpReqKey.split(",")[2];
		TrainingDatProgrammeDetailBO progDetail = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		List<DatNotificationsBO> notificationsList = new ArrayList<DatNotificationsBO>();
		try {
			progDetail = programRepository.findByPkTrainingProgrammeId(progId);
			if (progDetail != null) {

				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId
						+ "_" + progDetail.getPkTrainingProgrammeId();

				fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

				DatNotificationsBO notificationDetails = new DatNotificationsBO();
				String notificationMessage = MessageFormat.format(
						rmApproveEmpRequestToEmpIndividualMsg, progDetail
								.getTrainingDatRequestDetail()
								.getMasTrainingTopicName()
								.getTrainingTopicName());
				notificationDetails.setEmpId(empId);
				notificationDetails.setNoificationMessage(notificationMessage);
				notificationDetails
						.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
				notificationDetails.setNotificationcreatedDate(new Date());
				notificationDetails.setClearFlagModifiedDate(new Date());
				notificationDetails.setClearFlagcreatedDate(new Date());
				notificationDetails
						.setModuleName(TrainingNotificationConstants.MODULE_NAME);
				notificationDetails
						.setFkModuleId(TrainingNotificationConstants.MENU_ID);
				notificationDetails.setMenuUrl(rmApproveEmpReqKey.split(",")[0]);
				notificationDetails.setIsAction(rmApproveEmpReqKey.split(",")[1]);
				notificationDetails
						.setNoificationMessageIdentifier(fullmsgIdentifierKey);
				notificationDetails
						.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
				notificationDetails.setNotificationExpiryTime(Integer
						.parseInt(rmApproveEmpReqKey.split(",")[3]));
				notificationsList.add(notificationDetails);

				try {
					notificationRepository.save(notificationsList);
				} catch (Exception e) {
					throw new DataAccessException("Failed to Save : ", e);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setRmApprovedEmpRequestToEmpScreen() service");
	}
	
	/**
	 * 
	 * <Description setRmInviteEmpForProgNotificationToEmpScreen:> This method
	 * is used to set RM invite Employee for a training program notification to
	 * Employee Screen
	 * 
	 * @param empId
	 * @param progId
	 * @throws DataAccessException
	 */
	public void setRmInviteEmpForProgNotificationToEmpScreen(int empId,int progId) throws DataAccessException {

		LOG.startUsecase("Entering setRmInviteEmpForProgNotificationToEmpScreen() service");
		String rminviteEmpProgKey = rminviteEmpProgKeys;
		String msgIdentifierKey = rminviteEmpProgKey.split(",")[2];
		TrainingDatProgrammeDetailBO progDetail = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		List<DatNotificationsBO> notificationsList = new ArrayList<DatNotificationsBO>();
		try {
			progDetail = programRepository.findByPkTrainingProgrammeId(progId);
			if (progDetail != null) {

				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId
						+ "_" + progDetail.getPkTrainingProgrammeId();

				fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

				DatNotificationsBO notificationDetails = new DatNotificationsBO();
				String notificationMessage = MessageFormat.format(
						rmInviteEmpToProgIndividualMsg, progDetail
								.getTrainingDatRequestDetail()
								.getMasTrainingTopicName()
								.getTrainingTopicName());
				notificationDetails.setEmpId(empId);
				notificationDetails.setNoificationMessage(notificationMessage);
				notificationDetails
						.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
				notificationDetails.setNotificationcreatedDate(new Date());
				notificationDetails.setClearFlagModifiedDate(new Date());
				notificationDetails.setClearFlagcreatedDate(new Date());
				notificationDetails
						.setModuleName(TrainingNotificationConstants.MODULE_NAME);
				notificationDetails
						.setFkModuleId(TrainingNotificationConstants.MENU_ID);
				notificationDetails.setMenuUrl(rminviteEmpProgKey.split(",")[0]);
				notificationDetails.setIsAction(rminviteEmpProgKey.split(",")[1]);
				notificationDetails
						.setNoificationMessageIdentifier(fullmsgIdentifierKey);
				notificationDetails
						.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
				notificationDetails.setNotificationExpiryTime(Integer
						.parseInt(rminviteEmpProgKey.split(",")[3]));
				notificationsList.add(notificationDetails);

				try {
					notificationRepository.save(notificationsList);
				} catch (Exception e) {
					throw new DataAccessException("Failed to Save : ", e);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setRmInviteEmpForProgNotificationToEmpScreen() service");
	}
	
	/**
	 * 
	 * <Description setRmInviteEmpForProgNotificationToEmpScreen:> This method
	 * is used to set Training Co-ordinator invite Employee for a training
	 * program notification to Employee Screen
	 * 
	 * @param empId
	 * @param progId
	 * @throws DataAccessException
	 */
	public void setTCInviteEmpForProgNotificationToEmpScreen(int empId,int progId) throws DataAccessException{

		LOG.startUsecase("Entering setTCInviteEmpForProgNotificationToEmpScreen() service");
		String tcinviteEmpProgKey = tcinviteEmpProgKeys;
		String msgIdentifierKey = tcinviteEmpProgKey.split(",")[2];
		TrainingDatProgrammeDetailBO progDetail = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		List<DatNotificationsBO> notificationsList = new ArrayList<DatNotificationsBO>();
		try {
			progDetail = programRepository.findByPkTrainingProgrammeId(progId);
			if (progDetail != null) {

				String fullmsgIdentifierKey = msgIdentifierKey + "_" + empId
						+ "_" + progDetail.getPkTrainingProgrammeId();

				fullMsgIdentifierKeys.add(fullmsgIdentifierKey);

				DatNotificationsBO notificationDetails = new DatNotificationsBO();
				String notificationMessage = MessageFormat.format(
						tcinviteEmpToProgIndividualMsg, progDetail
								.getTrainingDatRequestDetail()
								.getMasTrainingTopicName()
								.getTrainingTopicName());
				notificationDetails.setEmpId(empId);
				notificationDetails.setNoificationMessage(notificationMessage);
				notificationDetails
						.setClearFlag(PurchaseNotificationConstants.CLEAR_FLAG_NO);
				notificationDetails.setNotificationcreatedDate(new Date());
				notificationDetails.setClearFlagModifiedDate(new Date());
				notificationDetails.setClearFlagcreatedDate(new Date());
				notificationDetails
						.setModuleName(TrainingNotificationConstants.MODULE_NAME);
				notificationDetails
						.setFkModuleId(TrainingNotificationConstants.MENU_ID);
				notificationDetails.setMenuUrl(tcinviteEmpProgKey.split(",")[0]);
				notificationDetails.setIsAction(tcinviteEmpProgKey.split(",")[1]);
				notificationDetails
						.setNoificationMessageIdentifier(fullmsgIdentifierKey);
				notificationDetails
						.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
				notificationDetails.setNotificationExpiryTime(Integer
						.parseInt(tcinviteEmpProgKey.split(",")[3]));
				notificationsList.add(notificationDetails);

				try {
					notificationRepository.save(notificationsList);
				} catch (Exception e) {
					throw new DataAccessException("Failed to Save : ", e);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Error in getting Training Module Notifications");
		}
		LOG.endUsecase("Exiting setTCInviteEmpForProgNotificationToEmpScreen() service");
	}
}
