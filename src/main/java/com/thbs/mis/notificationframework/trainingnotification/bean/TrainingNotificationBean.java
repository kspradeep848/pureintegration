package com.thbs.mis.notificationframework.trainingnotification.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.thbs.mis.framework.util.TimeDeserializer;

public class TrainingNotificationBean {
	private int empId;
	private int rmId;
	private String moduleName;
	private String notificationMessage;
	private String trainingTopicName;
	private String roleName;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd HH:mm:SSSS")
	@JsonDeserialize(using = TimeDeserializer.class)
	private Date sDate;
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd HH:mm:SSSS")
	@JsonDeserialize(using = TimeDeserializer.class)
	private Date eDate;

	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public int getRmId() {
		return rmId;
	}
	public void setRmId(int rmId) {
		this.rmId = rmId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getNotificationMessage() {
		return notificationMessage;
	}
	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}
	public String getTrainingTopicName() {
		return trainingTopicName;
	}
	public void setTrainingTopicName(String trainingTopicName) {
		this.trainingTopicName = trainingTopicName;
	}
	
	public Date getsDate() {
		return sDate;
	}
	public void setsDate(Date sDate) {
		this.sDate = sDate;
	}
	public Date geteDate() {
		return eDate;
	}
	public void seteDate(Date eDate) {
		this.eDate = eDate;
	}
	
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	@Override
	public String toString() {
		return "TrainingNotificationBean [empId=" + empId + ", rmId=" + rmId
				+ ", moduleName=" + moduleName + ", notificationMessage="
				+ notificationMessage + ", trainingTopicName="
				+ trainingTopicName + "]";
	}

}
