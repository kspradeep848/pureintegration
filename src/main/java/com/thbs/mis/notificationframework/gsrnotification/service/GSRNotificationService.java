package com.thbs.mis.notificationframework.gsrnotification.service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO;
import com.thbs.mis.gsr.bo.GsrDatEmpGoalBO;
import com.thbs.mis.gsr.dao.GSREmployeegoalsRepository;
import com.thbs.mis.gsr.dao.GSRFinalRatingRepository;
import com.thbs.mis.notificationframework.gsrnotification.constants.GsrNotificationConstants;
import com.thbs.mis.notificationframework.notificationsettings.bo.DatNotificationsBO;
import com.thbs.mis.notificationframework.notificationsettings.constants.NotificationSettingConstants;
import com.thbs.mis.notificationframework.notificationsettings.dao.NotificationRepository;

@Service
public class GSRNotificationService {

	private static final AppLog LOG = LogFactory
			.getLog(GSRNotificationService.class);

	/*
	 * Individual View Messages
	 */
	/*
	 * Goal Set (By Employee)
	 */
	@Value("${ui.mesage.goalSetByEmployeeToRMScreen}")
	String goalSetByEmployeeToRMScreen;
	/*
	 * Goal Set (By Manager)
	 */
	@Value("${ui.mesage.goalSetByRMToEmployeeScreen}")
	String goalSetByRMToEmployeeScreen;
	/*
	 * Goal Update(By Manager)
	 */
	@Value("${ui.mesage.goalUpdateByRMToEmployeeScreen}")
	String goalUpdateByRMToEmployeeScreen;
	/*
	 * Goal Reject (By Manager)
	 */
	@Value("${ui.mesage.goalRejectByRMToEmployeeScreen}")
	String goalRejectByRMToEmployeeScreen;
	/*
	 * Rated(By Manager)
	 */
	@Value("${ui.mesage.goalRatedByRMToEmployeeScreen}")
	String goalRatedByRMToEmployeeScreen;
	/*
	 * Force Close(By HR/Manager/Skip level Manager) to Employee Screen
	 */
	@Value("${ui.mesage.forceCloseByRMToEmployeeScreen}")
	String forceCloseByRMToEmployeeScreen;

	/*
	 * Force Close(By HR/Manager/Skip level Manager) to RM Screen
	 */
	@Value("${ui.mesage.forceCloseByHROrManagerToRmScreen}")
	String forceCloseByHROrManagerToRmScreen;

	/*
	 * 
	 * Pre-Normalised Rating Set.(By Manager)
	 */
	@Value("${ui.mesage.setPreNormalisedRatingByRMToEmployeeScreen}")
	String setPreNormalisedRatingByRMToEmployeeScreen;
	/*
	 * Pre-Normalised Rating Update.(By Manager)
	 */
	@Value("${ui.mesage.updatePreNormalisedRatingByRMToEmployeeScreen}")
	String updatePreNormalisedRatingByRMToEmployeeScreen;

	/*
	 * Dis-Agree Rating .(By Employee)
	 */
	@Value("${ui.mesage.disagreeRatingByEmployeeToRMScreen}")
	String disagreeRatingByEmployeeToRMScreen;

	@Value("${ui.mesage.disagreeRatingByEmployeeToSkipLevelManagerScreen}")
	String disagreeRatingByEmployeeToSkipLevelManagerScreen;

	@Value("${ui.mesage.disagreeRatingByEmployeeToHRScreen}")
	String disagreeRatingByEmployeeToHRScreen;

	/*
	 * Goal Set by Skip Level RM
	 */
	@Value("${ui.message.goalSetBySkipLevelRmToEmployeeScreen}")
	String goalSetBySkipLevelRmToEmpScreen;

	/*
	 * Goal Set by HR
	 */
	@Value("${ui.message.goalSetByHRToEmployeeScreen}")
	String goalSetByHRToEmpScreen;

	@Value("${ui.message.goalSetByBUHeadToEmpScreen}")
	String goalSetByBUHeadToEmpScreen;

	/*
	 * Consolidated View Messages
	 */
	/*
	 * Goal Set (By Employee) ConsolidatedMsg
	 */
	@Value("${ui.mesage.goalSetByEmployeeToRMScreenConsolidatedMsg}")
	String goalSetByEmployeeToRMScreenConsolidatedMsg;

	/*
	 * 
	 * Goal Set (By Manager) ConsolidatedMsg
	 */
	@Value("${ui.mesage.goalSetByRMToEmployeeScreenConsolidatedMsg}")
	String goalSetByRMToEmployeeScreenConsolidatedMsg;

	/*
	 * 
	 * Goal Update (By Manager) ConsolidatedMsg
	 */
	@Value("${ui.mesage.goalUpdateByRMToEmployeeScreenConsolidatedMsg}")
	String goalUpdateByRMToEmployeeScreenConsolidatedMsg;

	/*
	 * 
	 * Goal Reject (By Manager) ConsolidatedMsg
	 */
	@Value("${ui.mesage.goalRejectByRMToEmployeeScreenConsolidatedMsg}")
	String goalRejectByRMToEmployeeScreenConsolidatedMsg;

	/*
	 * Rated(By Manager)
	 */
	@Value("${ui.mesage.goalRatedByRMToEmployeeScreenConsolidatedMsg}")
	String goalRatedByRMToEmployeeScreenConsolidatedMsg;

	/*
	 * ConsolidatedMsg Force Close(By HR/Manager/Skip level Manager) to RM
	 * Screen
	 */
	@Value("${ui.mesage.forceCloseByHROrManagerToRmScreenConsolidatedMsg}")
	String forceCloseByHROrManagerToRmScreenConsolidatedMsg;
	/*
	 * Dis-Agree Rating .(By Employee) for consolidated screens
	 */
	@Value("${ui.mesage.disagreeRatingByEmployeeToRMScreenConsolidatedMsg}")
	String disagreeRatingByEmployeeToRMScreenConsolidatedMsg;

	@Value("${ui.mesage.disagreeRatingByEmployeeToSkipLevelManagerScreenConsolidatedMsg}")
	String disagreeRatingByEmployeeToSkipLevelManagerScreenConsolidatedMsg;

	@Value("${ui.mesage.disagreeRatingByEmployeeToHRScreenConsolidatedMsg}")
	String disagreeRatingByEmployeeToHRScreenConsolidatedMsg;

	/*
	 * Goal Set by Skip Level Rm to Employee Screen
	 */
	@Value("${ui.message.goalSetBySkipLevelRmToEmpScreenConsolidatedMsg}")
	String goalSetBySkipLevelRmToEmpConsolidatedMsg;

	/*
	 * Goal Set by HR to Employee Screen
	 */
	@Value("${ui.message.goalSetByHrToEmpScreenConsolidatedMsg}")
	String goalSetByHRToEmpScreenConsolidatedMsg;

	/*
	 * Goal Set by BU Head to Employee Screen
	 */
	@Value("${ui.mesage.goalSetByBUToEmpScreenConsolidatedMsg}")
	String goalSetByBUHeadToEmpScreenConsolidatedMsg;

	@Value("${hr.role.id}")
	String hrRoleIds;
	/*
	 * NOtification Keys
	 */
	@Value("${GSR_EMP_CRE_GOAL}")
	String empCreGoalKeys;

	@Value("${GSR_RM_CRE_GOAL}")
	String rmCreGoalKeys;

	@Value("${GSR_RM_UPD_GOAL}")
	String rmUpdateGoalKeys;

	@Value("${GSR_RM_REJ_GOAL}")
	String rmRejectGoalKeys;

	@Value("${GSR_RM_RATE_GOAL}")
	String rmRateGoalKeys;

	@Value("${GSR_RM_SET_PRENORM}")
	String rmSetPrenormRatingKeys;

	@Value("${GSR_RM_UPD_PRENORM}")
	String rmUpdPrenormRatingKeys;

	@Value("${GSR_FORCE_CLOSE_EMP}")
	String forceCloseEmpKeys;

	@Value("${GSR_FORCE_CLOSE_RM}")
	String forceCloseRmKeys;

	@Value("${GSR_DIS_AGREE_RM}")
	String disAgreeByRmKeys;

	@Value("${GSR_DIS_AGREE_SKIP_RM}")
	String disAgreeSkiplvlRmKeys;

	@Value("${GSR_DIS_AGREE_HR}")
	String disAgreeHrKeys;

	@Value("${GSR_SKIP_RM_CRE_GOAL}")
	String skipLevelRmCreGoalKeys;

	@Value("${GSR_HR_CRE_GOAL}")
	String hrCreGoalKeys;

	@Value("${GSR_BUHEAD_CRE_GOAL}")
	String buCreGoalKeys;

	/*
	 * Auto wiring the repository interfaces
	 */
	@Autowired
	private NotificationRepository notificationRepository;
	@Autowired
	private GSREmployeegoalsRepository gsrEmployeegoalsRepository;
	@Autowired
	private GSRFinalRatingRepository gsrFinalRatingRepository;
	@Autowired
	private EmployeePersonalDetailsRepository personalDetailsRepository;

	@Autowired
	private EmployeeProfessionalRepository professionalDetailsRepository;
	@Autowired
	private EmpDetailRepository empDetailRepository;

	/* Goal Set (By Employee) */

	public String setGoalNotificationByEmployee(int empId)
			throws DataAccessException {

		LOG.startUsecase("setGoalNotificationByEmployee");

		DatEmpDetailBO empDetail = null;

		List<Short> hrRoleIdList = Stream.of(hrRoleIds.split(","))
				.map(Short::parseShort).collect(Collectors.toList());

		empDetail = empDetailRepository.findByPkEmpId(empId);

		/* Goal Set (By Employee) To RM Screen for individual view */
		// setGoalNotificationByEmployeeToRmScreen(empId);

		/* Goal Set (By Manager) to EMployee Screen for individual view */
		// goalSetByManagerToEmployeeScreen(empId);
		/* Goal Update (By Manager) to EMployee Screenfor individual view */
		// goalUpdateByManagerToEmployeeScreen(empId);
		/* Goal Reject (By Manager) to EMployee Screen for individual view */

		// goalRejectByManagerToEmployeeScreen(empId);
		/* Goal Rated (By Manager) to EMployee Screen for individual view */
		// goalRatedByManagerToEmployeeScreen(empId);

		/*
		 * Force Close(By HR/Manager/Skip level Manager)
		 */
		goalForceClosedByByHROrRMOrSkipRMToEmployeeScreen(empId);

		/* Force Close(By HR/Manager/Skip level Manager) to RM Screen */
		goalForceCloseByByManagerOrHROrSLRMToRMScreen(empId);

		/* Pre-Normalised Rating Set.(By Manager) to employee screen */
		preNormalisedRatingSetByManagerToEMployeeScreen(empId);

		/* Pre-Normalised Rating Update.(By Manager) to Employee screen */
		preNormalisedRatingUpdateByManagerToEMployeeScreen(empId);

		// Individual View
		/* Dis-Agree Rating .(By Employee) to RM screen */
		// disAgreeRatingByEmployeeToRMScreen(empId);

		/* Dis-Agree Rating .(By Employee) to SkipLevelRM screen */
		// disAgreeRatingByEmployeeToSkipLevelRMScreen(empId);

		/* Dis-Agree Rating .(By Employee) to HRD screen */
		// disAgreeRatingByEmployeeToHRScreen(empId);

		/* Consolidated View Services */

		/* Goal Set (By Employee) To RM Screen for Consolidated view */
		setGoalNotificationByEmployeeToRmScreenForConsolidatedView(empId);

		/* Goal Set (By Manager) to EMployee Screen for Consolidated view */
		goalSetByManagerToEmployeeScreenConsolidatedMsg(empId);

		/* Goal update (By Manager) to EMployee Screen for Consolidated view */
		goalUpdateByManagerToEmployeeScreenForConsolidatedMsg(empId);

		/* Goal Reject (By Manager) to EMployee Screen for Consolidated view */
		goalRejectByManagerToEmployeeScreenConsolidatedMsg(empId);

		/* Goal Rated (By Manager) to EMployee Screen for Consolidated view */
		goalRatedByManagerToEmployeeScreenConsolidatedMsg(empId);

		/* Force Close(By HR/Manager/Skip level Manager ) to Employee Screen */
		// goalForceCloseByByManagerOrHROrSLRMToEmployeeScreen(empId);

		// consolidated view
		/* Dis-Agree Rating .(By Employee) to RM screen ConsolidatedMsg */
		disAgreeRatingByEmployeeToRMScreenConsolidatedMsg(empId);
		/*
		 * Dis-Agree Rating .(By Employee) to SkipLevel RM screen
		 * ConsolidatedMsg
		 */
		disAgreeRatingByEmployeeToSkipLevelRMScreenConsolidatedMsg(empId);

		if (hrRoleIdList.contains(empDetail.getFkEmpRoleId())) {
			/* Dis-Agree Rating .(By Employee) to HR screen ConsolidatedMsg */
			disAgreeRatingByEmployeeToHRScreenConsolidatedMsg(empId);
		}

		goalSetBySkipLevelManagerToEmpScreen(empId);

		goalSetByHRToEmpScreen(empId);

		goalSetByBUHeadToEmpScreen(empId);

		LOG.endUsecase("setGoalNotificationByEmployee");
		return "Success";

	}

	/*
	 * Individual View methods Goal Set (By Employee) To RM Screen * inputs are
	 * Emp id and goal created by id is also an employee id and status is
	 * waiting for manager approval (1)
	 */
	public void setGoalNotificationByEmployeeToRmScreen(int empId)
			throws DataAccessException {

		DatNotificationsBO datNotificationsBO;
		DatNotificationsBO checkDuplicates;
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBOList;
		DatEmpPersonalDetailBO personalDetails;
		String notificationMessage = "";
		int employeeId = empId;
		int createdByEmpID = empId;
		try {
			gsrDatEmpGoalBOList = gsrEmployeegoalsRepository
					.findByGsrGoalCreatedByAndFkGsrDatEmpIdAndFkGsrGoalStatus(
							createdByEmpID,
							employeeId,
							GsrNotificationConstants.WAITINING_FOR_MANAGER_APPROVAL);
			LOG.info("setGoalNotificationByEmployeeToRmScreen gsrDatEmpGoalBOList"
					+ gsrDatEmpGoalBOList);
			if (gsrDatEmpGoalBOList.size() != 0) {
				for (int i = 0; i < gsrDatEmpGoalBOList.size(); i++) {
					personalDetails = personalDetailsRepository
							.findByFkEmpDetailId(gsrDatEmpGoalBOList.get(i)
									.getGsrGoalCreatedBy());
					String reporteeName = personalDetails.getEmpFirstName()
							+ " " + personalDetails.getEmpLastName();
					String goalName = gsrDatEmpGoalBOList.get(i)
							.getGsrDatEmpGoalName();
					String slabName = gsrDatEmpGoalBOList.get(i)
							.getGsrSlabBO_slab_id().getGsrSlabName();

					notificationMessage = MessageFormat.format(
							goalSetByEmployeeToRMScreen, reporteeName,
							goalName, slabName);
					int reportingManagerId = gsrDatEmpGoalBOList.get(i)
							.getFkGsrDatMgrId();
					try {
						checkDuplicates = notificationRepository
								.findByEmpIdAndNoificationMessage(
										reportingManagerId, notificationMessage);

					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
					if (checkDuplicates == null) {
						LOG.info(" Inserting the notification messages into notification table ");

						datNotificationsBO = new DatNotificationsBO();
						datNotificationsBO.setEmpId(reportingManagerId);
						datNotificationsBO
								.setNoificationMessage(notificationMessage);
						datNotificationsBO
								.setNotificationcreatedDate(new Date());
						datNotificationsBO.setClearFlagcreatedDate(new Date());
						datNotificationsBO.setClearFlagModifiedDate(new Date());
						datNotificationsBO
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						datNotificationsBO
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						datNotificationsBO
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
						datNotificationsBO
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {

							notificationRepository.save(datNotificationsBO);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
						LOG.info("Succesfully Inserted setGoalNotificationByEmployeeToRmScreen");

					} else if (String.valueOf(checkDuplicates.getEmpId())
							.equalsIgnoreCase(
									String.valueOf(reportingManagerId))
							&& checkDuplicates.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)
							&& checkDuplicates.getClearFlag() == "NO") {
						LOG.info(" Updating the notification messages into notification table ");

						checkDuplicates.setEmpId(reportingManagerId);
						checkDuplicates
								.setNoificationMessage(notificationMessage);
						checkDuplicates.setNotificationModifiedDate(new Date());
						checkDuplicates.setClearFlagcreatedDate(new Date());

						checkDuplicates
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						checkDuplicates
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						checkDuplicates
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						checkDuplicates
								.setMenuUrl(GsrNotificationConstants.MENU_URL);

						try {

							notificationRepository.save(checkDuplicates);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Updated setGoalNotificationByEmployeeToRmScreen");
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
	}

	/*
	 * Goal Set (By Manager) to Employee Screen If employee id is reporting
	 * manager role, Emp id is taken has manager id and goal created by id and
	 * status is goal set (2)
	 */

	public void goalSetByManagerToEmployeeScreen(int rmId)
			throws DataAccessException {
		DatNotificationsBO datNotificationsBO;
		DatNotificationsBO checkDuplicates;
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBOList;
		String notificationMessage = "";
		int MgrId = rmId;
		int GoalCreatedBy = rmId;
		try {
			gsrDatEmpGoalBOList = gsrEmployeegoalsRepository
					.findByFkGsrDatMgrIdAndGsrGoalCreatedByAndFkGsrGoalStatus(
							MgrId, GoalCreatedBy,
							GsrNotificationConstants.GOAL_SET);
			LOG.info("goalSetByManagerToEmployeeScreen gsrDatEmpGoalBOList"
					+ gsrDatEmpGoalBOList);
			if (gsrDatEmpGoalBOList.size() != 0) {
				for (int i = 0; i < gsrDatEmpGoalBOList.size(); i++) {

					String goalName = gsrDatEmpGoalBOList.get(i)
							.getGsrDatEmpGoalName();
					String slabName = gsrDatEmpGoalBOList.get(i)
							.getGsrSlabBO_slab_id().getGsrSlabName();
					notificationMessage = MessageFormat.format(
							goalSetByRMToEmployeeScreen, goalName, slabName);
					int employeeId = gsrDatEmpGoalBOList.get(i)
							.getFkGsrDatEmpId();
					try {
						checkDuplicates = notificationRepository
								.findByEmpIdAndNoificationMessage(employeeId,
										notificationMessage);

					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
					if (checkDuplicates == null) {
						LOG.info(" Inserting the notification messages into notification table ");

						datNotificationsBO = new DatNotificationsBO();
						datNotificationsBO.setEmpId(employeeId);
						datNotificationsBO
								.setNoificationMessage(notificationMessage);
						datNotificationsBO
								.setNotificationcreatedDate(new Date());
						datNotificationsBO.setClearFlagcreatedDate(new Date());
						datNotificationsBO.setClearFlagModifiedDate(new Date());
						datNotificationsBO
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						datNotificationsBO
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						datNotificationsBO
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						datNotificationsBO
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {

							notificationRepository.save(datNotificationsBO);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Inserted goalSetByManagerToEmployeeScreen");

					} else if (String.valueOf(checkDuplicates.getEmpId())
							.equalsIgnoreCase(String.valueOf(employeeId))
							&& checkDuplicates.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)
							&& checkDuplicates.getClearFlag() == "NO") {
						LOG.info(" Updating the notification messages into notification table ");

						checkDuplicates.setEmpId(employeeId);
						checkDuplicates
								.setNoificationMessage(notificationMessage);
						checkDuplicates.setNotificationModifiedDate(new Date());
						checkDuplicates.setClearFlagcreatedDate(new Date());

						checkDuplicates
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						checkDuplicates
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						checkDuplicates
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						checkDuplicates
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {

							notificationRepository.save(checkDuplicates);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Updated goalSetByManagerToEmployeeScreen");

					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
	}

	/*
	 * Goal Update (By Manager) to Employee Screen If employee id is reporting
	 * manager role, Emp id is taken has goal modified by id and status is goal
	 * set (2)
	 */

	public void goalUpdateByManagerToEmployeeScreen(int rmId)
			throws DataAccessException {
		DatNotificationsBO datNotificationsBO;
		DatNotificationsBO checkDuplicates;
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBOList;
		String notificationMessage = "";
		int MgrId = rmId;
		int createdBy = rmId;
		try {
			gsrDatEmpGoalBOList = gsrEmployeegoalsRepository
					.findByGsrGoalModifiedByAndGsrGoalCreatedByAndFkGsrGoalStatus(
							MgrId, createdBy, GsrNotificationConstants.GOAL_SET);
			LOG.info("gsrDatEmpGoalBOList" + gsrDatEmpGoalBOList);
			if (gsrDatEmpGoalBOList.size() != 0) {
				for (int i = 0; i < gsrDatEmpGoalBOList.size(); i++) {

					String goalName = gsrDatEmpGoalBOList.get(i)
							.getGsrDatEmpGoalName();
					String slabName = gsrDatEmpGoalBOList.get(i)
							.getGsrSlabBO_slab_id().getGsrSlabName();

					notificationMessage = MessageFormat.format(
							goalUpdateByRMToEmployeeScreen, goalName, slabName);
					int employeeId = gsrDatEmpGoalBOList.get(i)
							.getFkGsrDatEmpId();
					try {
						checkDuplicates = notificationRepository
								.findByEmpIdAndNoificationMessage(employeeId,
										notificationMessage);

					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
					if (checkDuplicates == null) {
						LOG.info(" Inserting the notification messages into notification table ");

						datNotificationsBO = new DatNotificationsBO();
						datNotificationsBO.setEmpId(employeeId);
						datNotificationsBO
								.setNoificationMessage(notificationMessage);
						datNotificationsBO
								.setNotificationcreatedDate(new Date());
						datNotificationsBO.setClearFlagcreatedDate(new Date());
						datNotificationsBO.setClearFlagModifiedDate(new Date());
						datNotificationsBO
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						datNotificationsBO
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						datNotificationsBO
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						datNotificationsBO
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {

							notificationRepository.save(datNotificationsBO);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Inserted goalUpdateByManagerToEmployeeScreen");

					} else if (String.valueOf(checkDuplicates.getEmpId())
							.equalsIgnoreCase(String.valueOf(employeeId))
							&& checkDuplicates.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)
							&& checkDuplicates.getClearFlag() == "NO") {
						LOG.info(" Updating the notification messages into notification table ");

						checkDuplicates.setEmpId(employeeId);
						checkDuplicates
								.setNoificationMessage(notificationMessage);
						checkDuplicates.setNotificationModifiedDate(new Date());
						checkDuplicates.setClearFlagcreatedDate(new Date());

						checkDuplicates
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						checkDuplicates
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						checkDuplicates
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						checkDuplicates
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {

							notificationRepository.save(checkDuplicates);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Updated goalUpdateByManagerToEmployeeScreen");

					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
	}

	/*
	 * Goal Reject (By Manager) to Employee Screen Emp id is taken has goal
	 * created by id and status is goal reject (6)
	 */

	public void goalRejectByManagerToEmployeeScreen(int employeeId)
			throws DataAccessException {
		DatNotificationsBO datNotificationsBO;
		DatNotificationsBO checkDuplicates;
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBOList;
		String notificationMessage = "";
		int createdBy = employeeId;
		try {
			gsrDatEmpGoalBOList = gsrEmployeegoalsRepository
					.findByFkGsrDatEmpIdAndGsrGoalCreatedByAndFkGsrGoalStatus(
							employeeId, createdBy,
							GsrNotificationConstants.REJECTED);
			LOG.info("gsrDatEmpGoalBOList" + gsrDatEmpGoalBOList);
			if (gsrDatEmpGoalBOList.size() != 0) {
				for (int i = 0; i < gsrDatEmpGoalBOList.size(); i++) {

					String goalName = gsrDatEmpGoalBOList.get(i)
							.getGsrDatEmpGoalName();
					String slabName = gsrDatEmpGoalBOList.get(i)
							.getGsrSlabBO_slab_id().getGsrSlabName();

					notificationMessage = MessageFormat.format(
							goalRejectByRMToEmployeeScreen, goalName, slabName);

					try {
						checkDuplicates = notificationRepository
								.findByEmpIdAndNoificationMessage(employeeId,
										notificationMessage);

					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
					if (checkDuplicates == null) {
						LOG.info(" Inserting the notification messages into notification table ");

						datNotificationsBO = new DatNotificationsBO();
						datNotificationsBO.setEmpId(employeeId);
						datNotificationsBO
								.setNoificationMessage(notificationMessage);
						datNotificationsBO
								.setNotificationcreatedDate(new Date());
						datNotificationsBO.setClearFlagcreatedDate(new Date());
						datNotificationsBO.setClearFlagModifiedDate(new Date());
						datNotificationsBO
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						datNotificationsBO
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						datNotificationsBO
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						datNotificationsBO
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {

							notificationRepository.save(datNotificationsBO);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Inserted goalRejectByManagerToEmployeeScreen");

					} else if (String.valueOf(checkDuplicates.getEmpId())
							.equalsIgnoreCase(String.valueOf(employeeId))
							&& checkDuplicates.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)
							&& checkDuplicates.getClearFlag() == "NO") {
						LOG.info(" Updating the notification messages into notification table ");

						checkDuplicates.setEmpId(employeeId);
						checkDuplicates
								.setNoificationMessage(notificationMessage);
						checkDuplicates.setNotificationModifiedDate(new Date());
						checkDuplicates.setClearFlagcreatedDate(new Date());

						checkDuplicates
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						checkDuplicates
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						checkDuplicates
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						checkDuplicates
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {

							notificationRepository.save(checkDuplicates);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Updated goalRejectByManagerToEmployeeScreen");
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
	}

	/*
	 * Goal Rated (By Manager) to Employee Screen Emp id is taken has goal
	 * created by id and status is goal rated (4)
	 */

	public void goalRatedByManagerToEmployeeScreen(int employeeId)
			throws DataAccessException {
		DatNotificationsBO datNotificationsBO;
		DatNotificationsBO checkDuplicates;
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBOList;
		String notificationMessage = "";

		try {
			gsrDatEmpGoalBOList = gsrEmployeegoalsRepository
					.findByFkGsrDatEmpIdAndFkGsrGoalStatus(employeeId,
							GsrNotificationConstants.RATED);
			LOG.info("gsrDatEmpGoalBOList" + gsrDatEmpGoalBOList);
			if (gsrDatEmpGoalBOList.size() != 0) {
				for (int i = 0; i < gsrDatEmpGoalBOList.size(); i++) {

					String goalName = gsrDatEmpGoalBOList.get(i)
							.getGsrDatEmpGoalName();
					String slabName = gsrDatEmpGoalBOList.get(i)
							.getGsrSlabBO_slab_id().getGsrSlabName();

					notificationMessage = MessageFormat.format(
							goalRatedByRMToEmployeeScreen, goalName, slabName);

					try {
						checkDuplicates = notificationRepository
								.findByEmpIdAndNoificationMessage(employeeId,
										notificationMessage);

					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
					if (checkDuplicates == null) {
						LOG.info(" Inserting the notification messages into notification table ");

						datNotificationsBO = new DatNotificationsBO();
						datNotificationsBO.setEmpId(employeeId);
						datNotificationsBO
								.setNoificationMessage(notificationMessage);
						datNotificationsBO
								.setNotificationcreatedDate(new Date());
						datNotificationsBO.setClearFlagcreatedDate(new Date());
						datNotificationsBO.setClearFlagModifiedDate(new Date());
						datNotificationsBO
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						datNotificationsBO
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						datNotificationsBO
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						datNotificationsBO
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {

							notificationRepository.save(datNotificationsBO);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Inserted goalRatedByManagerToEmployeeScreen");

					} else if (String.valueOf(checkDuplicates.getEmpId())
							.equalsIgnoreCase(String.valueOf(employeeId))
							&& checkDuplicates.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)
							&& checkDuplicates.getClearFlag() == "NO") {
						LOG.info(" Updating the notification messages into notification table ");

						checkDuplicates.setEmpId(employeeId);
						checkDuplicates
								.setNoificationMessage(notificationMessage);
						checkDuplicates.setNotificationModifiedDate(new Date());
						checkDuplicates.setClearFlagcreatedDate(new Date());

						checkDuplicates
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						checkDuplicates
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						checkDuplicates
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						checkDuplicates
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {

							notificationRepository.save(checkDuplicates);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Updated goalRatedByManagerToEmployeeScreen");

					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
	}

	/*
	 * Goal Force closed by By HR/Manager/Skip level Manager to Employee Screen
	 * , inputs are Emp id and status is goal rated (7)
	 */

	public void goalForceClosedByByHROrRMOrSkipRMToEmployeeScreen(int employeeId)
			throws DataAccessException {

		LOG.startUsecase("Entering goalForceClosedByByHROrRMOrSkipRMToEmployeeScreen() service");
		DatEmpProfessionalDetailBO professionalDetailBO;
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBOList;
		String forceCloseEmpKey = forceCloseEmpKeys;
		String msgIdentifierKey = forceCloseEmpKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		Map<Short, List<GsrDatEmpGoalBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpGoalBO>>();
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		int updateCount = 0;
		try {
			gsrDatEmpGoalBOList = gsrEmployeegoalsRepository
					.findByFkGsrDatEmpIdAndFkGsrGoalStatus(employeeId,
							GsrNotificationConstants.FORCE_CLOSE);

			if (gsrDatEmpGoalBOList.size() > 0) {
				for (GsrDatEmpGoalBO empGoal : gsrDatEmpGoalBOList) {
					if (!empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
						List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
						finalBean.add(empGoal);
						empGoalsMap.put(empGoal.getFkGsrSlabId(), finalBean);
						continue;
					}
					if (empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
						List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
						finalBean.add(empGoal);
						empGoalsMap.get(empGoal.getFkGsrSlabId()).addAll(
								finalBean);
					}
				}
				for (Map.Entry<Short, List<GsrDatEmpGoalBO>> entry : empGoalsMap
						.entrySet()) {
					List<GsrDatEmpGoalBO> empGoalList = (List<GsrDatEmpGoalBO>) entry
							.getValue();
					int hrOrRmOrSkipLevelRMId = empGoalList.get(0)
							.getGsrGoalClosedBy();
					String hrOrRmOrSkipLevelRMName = getEmployeeNameByEmpId(hrOrRmOrSkipLevelRMId);
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ employeeId + "_"
							+ empGoalList.get(0).getFkGsrSlabId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = checkDuplicates(employeeId,
							fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						Date maxDate = empGoalList.stream()
								.map(GsrDatEmpGoalBO::getGsrGoalModifiedDate)
								.max(Date::compareTo).get();

						String notificationMessage = MessageFormat.format(
								forceCloseByRMToEmployeeScreen, empGoalList
										.get(0).getGsrSlabBO_slab_id()
										.getGsrSlabShortName(),
								hrOrRmOrSkipLevelRMName);
						notificationDetails.setEmpId(employeeId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails.setNotificationcreatedDate(maxDate);
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
						notificationDetails.setMenuUrl(forceCloseEmpKey
								.split(",")[0]);
						notificationDetails.setIsAction(forceCloseEmpKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(forceCloseEmpKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						Date maxDate = empGoalList.stream()
								.map(GsrDatEmpGoalBO::getGsrGoalModifiedDate)
								.max(Date::compareTo).get();
						String notificationMessage = MessageFormat.format(
								forceCloseByRMToEmployeeScreen, empGoalList
										.get(0).getGsrSlabBO_slab_id()
										.getGsrSlabShortName(),
								hrOrRmOrSkipLevelRMName);
						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (maxDate.getTime() > isDuplicatesFound
								.getNotificationcreatedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(maxDate);
						if (isDuplicatesFound.getClearFlag().equals(
								NotificationSettingConstants.CLEAR_FLAG_YES))
							if (!isDuplicatesFound.getNoificationMessage()
									.trim().equals(notificationMessage.trim()))
								isDuplicatesFound
										.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				}
			} else {
				String menuURL = forceCloseEmpKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(employeeId,
									menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, employeeId,
									forceCloseEmpKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
		LOG.endUsecase("Exiting goalForceClosedByByHROrRMOrSkipRMToEmployeeScreen() service");
	}

	/*
	 * Goal Set (By Employee) To RM Screen for Consolidated View inputs are rmId
	 * and status is waiting for manager approval (1)
	 */
	public void setGoalNotificationByEmployeeToRmScreenForConsolidatedView(
			int empId) throws DataAccessException {

		LOG.startUsecase("Entering setGoalNotificationByEmployeeToRmScreenForConsolidatedView()");
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBOList = null;
		String gsrEmpCreGoalKey = empCreGoalKeys;
		String msgIdentifierKey = gsrEmpCreGoalKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		Map<Short, List<GsrDatEmpGoalBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpGoalBO>>();
		int updateCount = 0;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		try {
			gsrDatEmpGoalBOList = gsrEmployeegoalsRepository
					.findByGsrGoalCreatedByNotAndFkGsrDatMgrIdAndFkGsrGoalStatus(
							empId,
							empId,
							GsrNotificationConstants.WAITINING_FOR_MANAGER_APPROVAL);
			if (gsrDatEmpGoalBOList.size() > 0) {
				if (gsrDatEmpGoalBOList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + "_"
							+ gsrDatEmpGoalBOList.get(0).getFkGsrSlabId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = checkDuplicates(empId,
							fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								goalSetByEmployeeToRMScreen,
								getEmployeeNameByEmpId(gsrDatEmpGoalBOList.get(
										0).getFkGsrDatEmpId()),
								gsrDatEmpGoalBOList.get(0)
										.getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(gsrDatEmpGoalBOList
										.get(0).getGsrGoalCreatedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
						notificationDetails.setMenuUrl(gsrEmpCreGoalKey
								.split(",")[0]);
						notificationDetails.setIsAction(gsrEmpCreGoalKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(gsrEmpCreGoalKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								goalSetByEmployeeToRMScreen,
								getEmployeeNameByEmpId(gsrDatEmpGoalBOList.get(
										0).getFkGsrDatEmpId()),
								gsrDatEmpGoalBOList.get(0)
										.getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());

						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= gsrDatEmpGoalBOList.get(0)
								.getGsrGoalCreatedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(gsrDatEmpGoalBOList
											.get(0).getGsrGoalCreatedDate());
						if (isDuplicatesFound.getClearFlag().equals(
								NotificationSettingConstants.CLEAR_FLAG_YES))
							if (!isDuplicatesFound.getNoificationMessage()
									.trim().equals(notificationMessage.trim()))
								isDuplicatesFound
										.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					for (GsrDatEmpGoalBO empGoal : gsrDatEmpGoalBOList) {
						if (!empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap
									.put(empGoal.getFkGsrSlabId(), finalBean);
							continue;
						}
						if (empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap.get(empGoal.getFkGsrSlabId()).addAll(
									finalBean);
						}
					}
					for (Map.Entry<Short, List<GsrDatEmpGoalBO>> entry : empGoalsMap
							.entrySet()) {
						List<GsrDatEmpGoalBO> empGoalList = (List<GsrDatEmpGoalBO>) entry
								.getValue();

						if (empGoalList.size() == 1) {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_" + empId + "_"
									+ empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empId,
									fullmsgIdentifierKey);

							if (isDuplicatesFound == null) {
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalSetByEmployeeToRMScreen,
												getEmployeeNameByEmpId(empGoalList
														.get(0)
														.getFkGsrDatEmpId()),
												gsrDatEmpGoalBOList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empId);
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(empGoalList
												.get(0).getGsrGoalCreatedDate());
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails.setMenuUrl(gsrEmpCreGoalKey
										.split(",")[0]);
								notificationDetails
										.setIsAction(gsrEmpCreGoalKey
												.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(gsrEmpCreGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								String notificationMessage = MessageFormat
										.format(goalSetByEmployeeToRMScreen,
												getEmployeeNameByEmpId(empGoalList
														.get(0)
														.getFkGsrDatEmpId()),
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());

								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (isDuplicatesFound
										.getNotificationcreatedDate().getTime() >= empGoalList
										.get(0).getGsrGoalCreatedDate()
										.getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(isDuplicatesFound
													.getNotificationcreatedDate());
								else
									isDuplicatesFound
											.setNotificationcreatedDate(empGoalList
													.get(0)
													.getGsrGoalCreatedDate());
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						} else {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_" + empId + "_"
									+ empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empId,
									fullmsgIdentifierKey);

							if (isDuplicatesFound == null) {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalCreatedDate)
										.max(Date::compareTo).get();
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalSetByEmployeeToRMScreenConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empGoalList.get(0)
										.getFkGsrDatMgrId());
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(maxDate);
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails.setMenuUrl(gsrEmpCreGoalKey
										.split(",")[0]);
								notificationDetails
										.setIsAction(gsrEmpCreGoalKey
												.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(gsrEmpCreGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalCreatedDate)
										.max(Date::compareTo).get();
								String notificationMessage = MessageFormat
										.format(goalSetByEmployeeToRMScreenConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (maxDate.getTime() > isDuplicatesFound
										.getNotificationcreatedDate().getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(maxDate);
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						}
					}
				}
			} else {
				String menuURL = gsrEmpCreGoalKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									gsrEmpCreGoalKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
		LOG.endUsecase("Entering setGoalNotificationByEmployeeToRmScreenForConsolidatedView()");
	}

	/**
	 * 
	 * <Description checkDuplicates:> Check for duplicates in the notification
	 * table
	 * 
	 * @param empId
	 * @param msgIdentifier
	 * @return boolean
	 * @throws DataAccessException
	 */
	public DatNotificationsBO checkDuplicates(int empId, String msgIdentifier)
			throws DataAccessException {
		LOG.startUsecase("Entering checkDuplicates()");
		DatNotificationsBO notificationDetails = null;
		try {
			notificationDetails = notificationRepository
					.findByEmpIdAndNoificationMessageIdentifier(empId,
							msgIdentifier);
		} catch (Exception e) {
			throw new DataAccessException("error in checking duplicates ");
		}
		LOG.endUsecase("Exiting checkDuplicates()");
		return notificationDetails;
	}

	public String getEmployeeNameByEmpId(int empId) throws DataAccessException {
		LOG.startUsecase("Entering getEmployeeNameByEmpId()");
		DatEmpPersonalDetailBO personalDetails = null;
		String empName = "";
		try {
			personalDetails = personalDetailsRepository
					.findByFkEmpDetailId(empId);
			empName = personalDetails.getEmpFirstName() + " "
					+ personalDetails.getEmpLastName();
		} catch (Exception e) {
			throw new DataAccessException("error in getting employee name");
		}
		LOG.endUsecase("Exiting getEmployeeNameByEmpId()");
		return empName;
	}

	/*
	 * consolidated notifications for Goal Set (By Manager) to Employee Screen
	 * By using Emp id to get manager id inputs are empId , manager id goal
	 * created by id and status is goal set (2)
	 */

	public void goalSetByManagerToEmployeeScreenConsolidatedMsg(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering goalSetByManagerToEmployeeScreenConsolidatedMsg() service");
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBOList = null;
		String rmCreGoalKey = rmCreGoalKeys;
		String msgIdentifierKey = rmCreGoalKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		Map<Short, List<GsrDatEmpGoalBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpGoalBO>>();
		int updateCount = 0;
		DatEmpProfessionalDetailBO profDetail = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		try {

			profDetail = professionalDetailsRepository
					.findByFkMainEmpDetailId(empId);

			gsrDatEmpGoalBOList = gsrEmployeegoalsRepository
					.findByGsrGoalCreatedByAndFkGsrDatEmpIdAndFkGsrGoalStatus(
							profDetail.getFkEmpReportingMgrId(), empId,
							GsrNotificationConstants.GOAL_SET);

			if (gsrDatEmpGoalBOList.size() != 0) {
				if (gsrDatEmpGoalBOList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + "_"
							+ gsrDatEmpGoalBOList.get(0).getFkGsrSlabId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = checkDuplicates(empId,
							fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								goalSetByRMToEmployeeScreen,
								gsrDatEmpGoalBOList.get(0)
										.getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(gsrDatEmpGoalBOList
										.get(0).getGsrGoalCreatedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
						notificationDetails
								.setMenuUrl(rmCreGoalKey.split(",")[0]);
						notificationDetails
								.setIsAction(rmCreGoalKey.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(rmCreGoalKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								goalSetByRMToEmployeeScreen,
								gsrDatEmpGoalBOList.get(0)
										.getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= gsrDatEmpGoalBOList.get(0)
								.getGsrGoalCreatedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(gsrDatEmpGoalBOList
											.get(0).getGsrGoalCreatedDate());
						if (isDuplicatesFound.getClearFlag().equals(
								NotificationSettingConstants.CLEAR_FLAG_YES))
							if (!isDuplicatesFound.getNoificationMessage()
									.trim().equals(notificationMessage.trim()))
								isDuplicatesFound
										.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					for (GsrDatEmpGoalBO empGoal : gsrDatEmpGoalBOList) {
						if (!empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap
									.put(empGoal.getFkGsrSlabId(), finalBean);
							continue;
						}
						if (empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap.get(empGoal.getFkGsrSlabId()).addAll(
									finalBean);
						}
					}
					for (Map.Entry<Short, List<GsrDatEmpGoalBO>> entry : empGoalsMap
							.entrySet()) {
						List<GsrDatEmpGoalBO> empGoalList = (List<GsrDatEmpGoalBO>) entry
								.getValue();
						if (empGoalList.size() == 1) {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_" + empId + "_"
									+ empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empId,
									fullmsgIdentifierKey);

							if (isDuplicatesFound == null) {
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalSetByRMToEmployeeScreen,
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empId);
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(empGoalList
												.get(0).getGsrGoalCreatedDate());
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails.setMenuUrl(rmCreGoalKey
										.split(",")[0]);
								notificationDetails.setIsAction(rmCreGoalKey
										.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(rmCreGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								String notificationMessage = MessageFormat
										.format(goalSetByRMToEmployeeScreen,
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (isDuplicatesFound
										.getNotificationcreatedDate().getTime() >= empGoalList
										.get(0).getGsrGoalCreatedDate()
										.getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(isDuplicatesFound
													.getNotificationcreatedDate());
								else
									isDuplicatesFound
											.setNotificationcreatedDate(empGoalList
													.get(0)
													.getGsrGoalCreatedDate());
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						} else {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_" + empId + "_"
									+ empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empId,
									fullmsgIdentifierKey);

							if (isDuplicatesFound == null) {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalCreatedDate)
										.max(Date::compareTo).get();
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalSetByRMToEmployeeScreenConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empId);
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(maxDate);
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails.setMenuUrl(rmCreGoalKey
										.split(",")[0]);
								notificationDetails.setIsAction(rmCreGoalKey
										.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(rmCreGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalCreatedDate)
										.max(Date::compareTo).get();
								String notificationMessage = MessageFormat
										.format(goalSetByRMToEmployeeScreenConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (maxDate.getTime() > isDuplicatesFound
										.getNotificationcreatedDate().getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(maxDate);
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						}
					}
				}
			} else {
				String menuURL = rmCreGoalKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									rmCreGoalKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
		LOG.endUsecase("Exiting goalSetByManagerToEmployeeScreenConsolidatedMsg() service");

	}

	/*
	 * Consolidated notification Goal Update (By Manager) to Employee Screen If
	 * employee id is reporting manager role, Emp id is taken has goal modified
	 * by id and status is goal set (2)
	 */

	public void goalUpdateByManagerToEmployeeScreenForConsolidatedMsg(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering goalUpdateByManagerToEmployeeScreenForConsolidatedMsg() service");
		DatEmpProfessionalDetailBO professionalDetailBO;
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBOList;
		String rmUpdateGoalKey = rmUpdateGoalKeys;
		String msgIdentifierKey = rmUpdateGoalKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		Map<Short, List<GsrDatEmpGoalBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpGoalBO>>();
		int updateCount = 0;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		int rmId = 0;
		System.out.println("rm updategoal key:" + rmUpdateGoalKeys);
		try {
			try {
				professionalDetailBO = professionalDetailsRepository
						.findByFkMainEmpDetailId(empId);
			} catch (Exception e) {
				throw new DataAccessException(
						"Exception in professionalDetailBO ");
			}
			if (professionalDetailBO != null)
				rmId = professionalDetailBO.getFkEmpReportingMgrId();
			else
				throw new DataAccessException(
						"Unable to get Professional Details");

			gsrDatEmpGoalBOList = gsrEmployeegoalsRepository
					.findByFkGsrDatEmpIdAndGsrGoalCreatedByAndGsrGoalModifiedByAndFkGsrGoalStatus(
							empId, empId, rmId,
							GsrNotificationConstants.GOAL_SET);

			if (gsrDatEmpGoalBOList.size() > 0) {
				if (gsrDatEmpGoalBOList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + "_"
							+ gsrDatEmpGoalBOList.get(0).getFkGsrSlabId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = checkDuplicates(empId,
							fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								goalUpdateByRMToEmployeeScreen,
								gsrDatEmpGoalBOList.get(0)
										.getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(gsrDatEmpGoalBOList
										.get(0).getGsrGoalModifiedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
						notificationDetails.setMenuUrl(rmUpdateGoalKey
								.split(",")[0]);
						notificationDetails.setIsAction(rmUpdateGoalKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(rmUpdateGoalKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								goalUpdateByRMToEmployeeScreen,
								gsrDatEmpGoalBOList.get(0)
										.getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= gsrDatEmpGoalBOList.get(0)
								.getGsrGoalModifiedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(gsrDatEmpGoalBOList
											.get(0).getGsrGoalModifiedDate());
						if (isDuplicatesFound.getClearFlag().equals(
								NotificationSettingConstants.CLEAR_FLAG_YES))
							if (!isDuplicatesFound.getNoificationMessage()
									.trim().equals(notificationMessage.trim()))
								isDuplicatesFound
										.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					for (GsrDatEmpGoalBO empGoal : gsrDatEmpGoalBOList) {
						if (!empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap
									.put(empGoal.getFkGsrSlabId(), finalBean);
							continue;
						}
						if (empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap.get(empGoal.getFkGsrSlabId()).addAll(
									finalBean);
						}
					}
					for (Map.Entry<Short, List<GsrDatEmpGoalBO>> entry : empGoalsMap
							.entrySet()) {
						List<GsrDatEmpGoalBO> empGoalList = (List<GsrDatEmpGoalBO>) entry
								.getValue();

						if (empGoalList.size() == 1) {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_" + empId + "_"
									+ empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empId,
									fullmsgIdentifierKey);

							if (isDuplicatesFound == null) {
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalUpdateByRMToEmployeeScreen,
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empId);
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(empGoalList
												.get(0)
												.getGsrGoalModifiedDate());
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails.setMenuUrl(rmUpdateGoalKey
										.split(",")[0]);
								notificationDetails.setIsAction(rmUpdateGoalKey
										.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(rmUpdateGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								String notificationMessage = MessageFormat
										.format(goalUpdateByRMToEmployeeScreen,
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());

								isDuplicatesFound
										.setNoificationMessage(notificationMessage);

								if (isDuplicatesFound
										.getNotificationcreatedDate().getTime() >= empGoalList
										.get(0).getGsrGoalModifiedDate()
										.getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(isDuplicatesFound
													.getNotificationcreatedDate());
								else
									isDuplicatesFound
											.setNotificationcreatedDate(empGoalList
													.get(0)
													.getGsrGoalCreatedDate());
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						} else {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_" + empId + "_"
									+ empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empId,
									fullmsgIdentifierKey);

							if (isDuplicatesFound == null) {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalModifiedDate)
										.max(Date::compareTo).get();
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalUpdateByRMToEmployeeScreenConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empId);
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(maxDate);
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails.setMenuUrl(rmUpdateGoalKey
										.split(",")[0]);
								notificationDetails.setIsAction(rmUpdateGoalKey
										.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(rmUpdateGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalModifiedDate)
										.max(Date::compareTo).get();
								String notificationMessage = MessageFormat
										.format(goalUpdateByRMToEmployeeScreenConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (maxDate.getTime() > isDuplicatesFound
										.getNotificationcreatedDate().getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(maxDate);
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						}
					}
				}
			} else {
				String menuURL = rmUpdateGoalKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									rmUpdateGoalKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
		LOG.endUsecase("Exiting goalUpdateByManagerToEmployeeScreenForConsolidatedMsg() service");
	}

	/*
	 * Consolidated Notification Goal Reject (By Manager) to Employee Screen Emp
	 * id is taken has goal created by id and status is goal reject (6)
	 */

	public void goalRejectByManagerToEmployeeScreenConsolidatedMsg(
			int employeeId) throws DataAccessException {

		LOG.startUsecase("Entering goalRejectByManagerToEmployeeScreenConsolidatedMsg() service");
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBOList;
		String rmRejectGoalKey = rmRejectGoalKeys;
		String msgIdentifierKey = rmRejectGoalKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		Map<Short, List<GsrDatEmpGoalBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpGoalBO>>();
		Map<Integer, List<GsrDatEmpGoalBO>> goalsByEmpIdMap = new HashMap<Integer, List<GsrDatEmpGoalBO>>();
		int updateCount = 0;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		try {
			gsrDatEmpGoalBOList = gsrEmployeegoalsRepository
					.findByFkGsrDatEmpIdAndFkGsrGoalStatus(employeeId,
							GsrNotificationConstants.REJECTED);

			if (gsrDatEmpGoalBOList.size() > 0) {
				if (gsrDatEmpGoalBOList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ gsrDatEmpGoalBOList.get(0).getFkGsrDatEmpId()
							+ "_" + gsrDatEmpGoalBOList.get(0).getFkGsrSlabId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = checkDuplicates(gsrDatEmpGoalBOList
							.get(0).getFkGsrDatEmpId(), fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								goalRejectByRMToEmployeeScreen,
								gsrDatEmpGoalBOList.get(0)
										.getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						notificationDetails.setEmpId(gsrDatEmpGoalBOList.get(0)
								.getFkGsrDatEmpId());
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(gsrDatEmpGoalBOList
										.get(0).getGsrGoalModifiedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
						notificationDetails.setMenuUrl(rmRejectGoalKey
								.split(",")[0]);
						notificationDetails.setIsAction(rmRejectGoalKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(rmRejectGoalKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								goalRejectByRMToEmployeeScreen,
								gsrDatEmpGoalBOList.get(0)
										.getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= gsrDatEmpGoalBOList.get(0)
								.getGsrGoalModifiedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(gsrDatEmpGoalBOList
											.get(0).getGsrGoalModifiedDate());
						if (isDuplicatesFound.getClearFlag().equals(
								NotificationSettingConstants.CLEAR_FLAG_YES))
							if (!isDuplicatesFound.getNoificationMessage()
									.trim().equals(notificationMessage.trim()))
								isDuplicatesFound
										.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					for (GsrDatEmpGoalBO empGoal : gsrDatEmpGoalBOList) {
						if (!empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap
									.put(empGoal.getFkGsrSlabId(), finalBean);
							continue;
						}
						if (empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap.get(empGoal.getFkGsrSlabId()).addAll(
									finalBean);
						}
					}
					/*
					 * for (Map.Entry<Short,List<GsrDatEmpGoalBO>> entry:
					 * empGoalsMap.entrySet()) { List<GsrDatEmpGoalBO>
					 * empGoalList = (List<GsrDatEmpGoalBO>)entry.getValue();
					 * for(GsrDatEmpGoalBO empGoal : empGoalList) { if
					 * (!goalsByEmpIdMap
					 * .containsKey(empGoal.getFkGsrDatEmpId())) {
					 * List<GsrDatEmpGoalBO> finalBean = new
					 * ArrayList<GsrDatEmpGoalBO>(); finalBean.add(empGoal);
					 * goalsByEmpIdMap.put(empGoal.getFkGsrDatEmpId(),
					 * finalBean); continue; } if
					 * (goalsByEmpIdMap.containsKey(empGoal.getFkGsrDatEmpId()))
					 * {
					 * 
					 * List<GsrDatEmpGoalBO> finalBean = new
					 * ArrayList<GsrDatEmpGoalBO>(); finalBean.add(empGoal);
					 * goalsByEmpIdMap
					 * .get(empGoal.getFkGsrDatEmpId()).addAll(finalBean); } } }
					 */
					for (Map.Entry<Short, List<GsrDatEmpGoalBO>> entry : empGoalsMap
							.entrySet()) {
						List<GsrDatEmpGoalBO> empGoalList = (List<GsrDatEmpGoalBO>) entry
								.getValue();
						if (empGoalList.size() == 1) {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_"
									+ empGoalList.get(0).getFkGsrDatEmpId()
									+ "_" + empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empGoalList
									.get(0).getFkGsrDatEmpId(),
									fullmsgIdentifierKey);
							if (isDuplicatesFound == null) {
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalRejectByRMToEmployeeScreen,
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empGoalList.get(0)
										.getFkGsrDatEmpId());
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(empGoalList
												.get(0)
												.getGsrGoalModifiedDate());
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails.setMenuUrl(rmRejectGoalKey
										.split(",")[0]);
								notificationDetails.setIsAction(rmRejectGoalKey
										.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(rmRejectGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								String notificationMessage = MessageFormat
										.format(goalRejectByRMToEmployeeScreen,
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());

								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (isDuplicatesFound
										.getNotificationcreatedDate().getTime() >= empGoalList
										.get(0).getGsrGoalModifiedDate()
										.getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(isDuplicatesFound
													.getNotificationcreatedDate());
								else
									isDuplicatesFound
											.setNotificationcreatedDate(empGoalList
													.get(0)
													.getGsrGoalCreatedDate());
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						} else {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_"
									+ empGoalList.get(0).getFkGsrDatEmpId()
									+ "_" + empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(employeeId,
									fullmsgIdentifierKey);

							if (isDuplicatesFound == null) {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalModifiedDate)
										.max(Date::compareTo).get();
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalRejectByRMToEmployeeScreenConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empGoalList.get(0)
										.getFkGsrDatEmpId());
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(maxDate);
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails.setMenuUrl(rmRejectGoalKey
										.split(",")[0]);
								notificationDetails.setIsAction(rmRejectGoalKey
										.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(rmRejectGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalModifiedDate)
										.max(Date::compareTo).get();
								String notificationMessage = MessageFormat
										.format(goalRejectByRMToEmployeeScreenConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (maxDate.getTime() > isDuplicatesFound
										.getNotificationcreatedDate().getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(maxDate);
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						}
					}
				}
			} else {
				String menuURL = rmRejectGoalKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(employeeId,
									menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, employeeId,
									rmRejectGoalKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
		LOG.endUsecase("Exiting goalRejectByManagerToEmployeeScreenConsolidatedMsg() service");
	}

	/*
	 * Goal Rated (By Manager) to Employee Screen Emp id is taken has goal
	 * created by id and status is goal rated (4)
	 */

	public void goalRatedByManagerToEmployeeScreenConsolidatedMsg(int employeeId)
			throws DataAccessException {

		LOG.startUsecase("Entering goalRatedByManagerToEmployeeScreenConsolidatedMsg() service");
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBOList;
		String rmRateGoalKey = rmRateGoalKeys;
		String msgIdentifierKey = rmRateGoalKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		Map<Short, List<GsrDatEmpGoalBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpGoalBO>>();
		Map<Integer, List<GsrDatEmpGoalBO>> goalsByEmpIdMap = new HashMap<Integer, List<GsrDatEmpGoalBO>>();
		int updateCount = 0;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		try {
			gsrDatEmpGoalBOList = gsrEmployeegoalsRepository
					.findByFkGsrDatEmpIdAndFkGsrGoalStatus(employeeId,
							GsrNotificationConstants.RATED);

			if (gsrDatEmpGoalBOList.size() > 0) {
				if (gsrDatEmpGoalBOList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ gsrDatEmpGoalBOList.get(0).getFkGsrDatEmpId()
							+ "_" + gsrDatEmpGoalBOList.get(0).getFkGsrSlabId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = checkDuplicates(gsrDatEmpGoalBOList
							.get(0).getFkGsrDatEmpId(), fullmsgIdentifierKey);
					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								goalRatedByRMToEmployeeScreen,
								gsrDatEmpGoalBOList.get(0)
										.getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						notificationDetails.setEmpId(gsrDatEmpGoalBOList.get(0)
								.getFkGsrDatEmpId());
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(gsrDatEmpGoalBOList
										.get(0).getGsrGoalModifiedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
						notificationDetails
								.setMenuUrl(rmRateGoalKey.split(",")[0]);
						notificationDetails.setIsAction(rmRateGoalKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(rmRateGoalKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								goalRatedByRMToEmployeeScreen,
								gsrDatEmpGoalBOList.get(0)
										.getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= gsrDatEmpGoalBOList.get(0)
								.getGsrGoalModifiedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(gsrDatEmpGoalBOList
											.get(0).getGsrGoalModifiedDate());
						if (isDuplicatesFound.getClearFlag().equals(
								NotificationSettingConstants.CLEAR_FLAG_YES))
							if (!isDuplicatesFound.getNoificationMessage()
									.trim().equals(notificationMessage.trim()))
								isDuplicatesFound
										.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					for (GsrDatEmpGoalBO empGoal : gsrDatEmpGoalBOList) {
						if (!empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap
									.put(empGoal.getFkGsrSlabId(), finalBean);
							continue;
						}
						if (empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap.get(empGoal.getFkGsrSlabId()).addAll(
									finalBean);
						}
					}
					for (Map.Entry<Short, List<GsrDatEmpGoalBO>> entry : empGoalsMap
							.entrySet()) {
						List<GsrDatEmpGoalBO> empGoalList = (List<GsrDatEmpGoalBO>) entry
								.getValue();
						for (GsrDatEmpGoalBO empGoal : empGoalList) {
							if (!goalsByEmpIdMap.containsKey(empGoal
									.getFkGsrDatEmpId())) {
								List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
								finalBean.add(empGoal);
								goalsByEmpIdMap.put(empGoal.getFkGsrDatEmpId(),
										finalBean);
								continue;
							}
							if (goalsByEmpIdMap.containsKey(empGoal
									.getFkGsrDatEmpId())) {
								List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
								finalBean.add(empGoal);
								goalsByEmpIdMap.get(empGoal.getFkGsrDatEmpId())
										.addAll(finalBean);
							}
						}
					}
					for (Map.Entry<Integer, List<GsrDatEmpGoalBO>> entry : goalsByEmpIdMap
							.entrySet()) {
						List<GsrDatEmpGoalBO> empGoalList = (List<GsrDatEmpGoalBO>) entry
								.getValue();
						if (empGoalList.size() == 1) {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_"
									+ empGoalList.get(0).getFkGsrDatEmpId()
									+ "_" + empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empGoalList
									.get(0).getFkGsrDatEmpId(),
									fullmsgIdentifierKey);
							if (isDuplicatesFound == null) {
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalRatedByRMToEmployeeScreen,
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empGoalList.get(0)
										.getFkGsrDatEmpId());
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(empGoalList
												.get(0)
												.getGsrGoalModifiedDate());
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails.setMenuUrl(rmRateGoalKey
										.split(",")[0]);
								notificationDetails.setIsAction(rmRateGoalKey
										.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(rmRateGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								String notificationMessage = MessageFormat
										.format(goalRatedByRMToEmployeeScreen,
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());

								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (isDuplicatesFound
										.getNotificationcreatedDate().getTime() >= empGoalList
										.get(0).getGsrGoalModifiedDate()
										.getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(isDuplicatesFound
													.getNotificationcreatedDate());
								else
									isDuplicatesFound
											.setNotificationcreatedDate(empGoalList
													.get(0)
													.getGsrGoalCreatedDate());
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						} else {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_"
									+ empGoalList.get(0).getFkGsrDatEmpId()
									+ "_" + empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empGoalList
									.get(0).getFkGsrDatEmpId(),
									fullmsgIdentifierKey);
							if (isDuplicatesFound == null) {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalModifiedDate)
										.max(Date::compareTo).get();
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalRatedByRMToEmployeeScreenConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empGoalList.get(0)
										.getFkGsrDatEmpId());
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(maxDate);
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails.setMenuUrl(rmRateGoalKey
										.split(",")[0]);
								notificationDetails.setIsAction(rmRateGoalKey
										.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(rmRateGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalModifiedDate)
										.max(Date::compareTo).get();
								String notificationMessage = MessageFormat
										.format(goalRatedByRMToEmployeeScreenConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (maxDate.getTime() > isDuplicatesFound
										.getNotificationcreatedDate().getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(maxDate);
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						}
					}
				}
			} else {
				String menuURL = rmRateGoalKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(employeeId,
									menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, employeeId,
									rmRateGoalKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
		LOG.endUsecase("Exiting goalRatedByManagerToEmployeeScreenConsolidatedMsg() service");
	}

	/*
	 * Force Close(By HR/Manager/Skip level Manager) display to Employee Screen
	 * by force close Status and Employee Id
	 */
	public void goalForceCloseByByManagerOrHROrSLRMToEmployeeScreen(
			int employeeId) throws DataAccessException {
		DatNotificationsBO datNotificationsBO;
		DatNotificationsBO checkDuplicates;
		List<GsrDatEmpFinalRatingBO> gsrDatEmpFinalRatingBOList;
		String notificationMessage = "";
		DatEmpPersonalDetailBO personalDetailBO;
		try {
			gsrDatEmpFinalRatingBOList = gsrFinalRatingRepository
					.findByFkGsrFinalRatingEmpIdAndFinalRatingStatus(
							employeeId, GsrNotificationConstants.FORCE_CLOSE);
			LOG.info("gsrDatEmpGoalBOList" + gsrDatEmpFinalRatingBOList);
			if (gsrDatEmpFinalRatingBOList.size() != 0) {
				for (int i = 0; i < gsrDatEmpFinalRatingBOList.size(); i++) {

					String slabName = gsrDatEmpFinalRatingBOList.get(i)
							.getGsrSlabBO_slab_id().getGsrSlabName();
					int managerOrHROrSkipManagerId = gsrDatEmpFinalRatingBOList
							.get(i).getClosedBy();
					try {
						personalDetailBO = personalDetailsRepository
								.findByFkEmpDetailId(managerOrHROrSkipManagerId);
					} catch (Exception e) {
						throw new DataAccessException(
								"Error occured in getting managerOrHROrSkipManager Name ");
					}
					String managerOrHROrSkipManagerName = personalDetailBO
							.getEmpFirstName()
							+ " "
							+ personalDetailBO.getEmpLastName();
					notificationMessage = MessageFormat.format(
							forceCloseByRMToEmployeeScreen, slabName,
							managerOrHROrSkipManagerName);

					try {
						checkDuplicates = notificationRepository
								.findByEmpIdAndNoificationMessage(employeeId,
										notificationMessage);

					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
					if (checkDuplicates == null) {
						LOG.info(" Inserting the notification messages into notification table ");

						datNotificationsBO = new DatNotificationsBO();
						datNotificationsBO.setEmpId(employeeId);
						datNotificationsBO
								.setNoificationMessage(notificationMessage);
						datNotificationsBO
								.setNotificationcreatedDate(new Date());
						datNotificationsBO.setClearFlagcreatedDate(new Date());
						datNotificationsBO.setClearFlagModifiedDate(new Date());
						datNotificationsBO
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						datNotificationsBO
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						datNotificationsBO
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						datNotificationsBO
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {
							notificationRepository.save(datNotificationsBO);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Inserted goalForceCloseByByManagerOrHROrSLRMToEmployeeScreen");

					} else if (String.valueOf(checkDuplicates.getEmpId())
							.equalsIgnoreCase(String.valueOf(employeeId))
							&& checkDuplicates.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)
							&& checkDuplicates.getClearFlag() == "NO") {
						LOG.info(" Updating the notification messages into notification table ");

						checkDuplicates.setEmpId(employeeId);
						checkDuplicates
								.setNoificationMessage(notificationMessage);
						checkDuplicates.setNotificationModifiedDate(new Date());
						checkDuplicates.setClearFlagcreatedDate(new Date());

						checkDuplicates
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						checkDuplicates
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						checkDuplicates
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						checkDuplicates
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {

							notificationRepository.save(checkDuplicates);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Updated goalForceCloseByByManagerOrHROrSLRMToEmployeeScreen");
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
	}

	/*
	 * Force Close(By HR/Manager/Skip level Manager) display to RM Screen by
	 * force close Status and Employee Id
	 */
	public void goalForceCloseByByManagerOrHROrSLRMToRMScreen(int employeeId)
			throws DataAccessException {

		LOG.startUsecase("Entering goalForceCloseByByManagerOrHROrSLRMToRMScreen() service");
		List<GsrDatEmpFinalRatingBO> gsrDatEmpFinalRatingBOList;
		Map<Short, List<GsrDatEmpFinalRatingBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpFinalRatingBO>>();
		Map<Integer, List<GsrDatEmpFinalRatingBO>> empFinalMap = new HashMap<Integer, List<GsrDatEmpFinalRatingBO>>();
		String forceCloseRmKey = forceCloseRmKeys;
		String msgIdentifierKey = forceCloseRmKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			gsrDatEmpFinalRatingBOList = gsrFinalRatingRepository
					.findByFkGsrFinalRatingMgrIdAndFinalRatingStatus(
							employeeId,
							GsrNotificationConstants.FINAL_RATING_RESOLVED);
			if (gsrDatEmpFinalRatingBOList.size() > 0) {
				for (GsrDatEmpFinalRatingBO empGoal : gsrDatEmpFinalRatingBOList) {
					if (!empGoalsMap.containsKey(empGoal
							.getFkGsrFinalRatingSlabId())) {
						List<GsrDatEmpFinalRatingBO> finalBean = new ArrayList<GsrDatEmpFinalRatingBO>();
						finalBean.add(empGoal);
						empGoalsMap.put(empGoal.getFkGsrFinalRatingSlabId(),
								finalBean);
						continue;
					}
					if (empGoalsMap.containsKey(empGoal
							.getFkGsrFinalRatingSlabId())) {
						List<GsrDatEmpFinalRatingBO> finalBean = new ArrayList<GsrDatEmpFinalRatingBO>();
						finalBean.add(empGoal);
						empGoalsMap.get(empGoal.getFkGsrFinalRatingSlabId())
								.addAll(finalBean);
					}
				}

				/*
				 * for(Map.Entry<Short,List<GsrDatEmpFinalRatingBO>> entry:
				 * empGoalsMap.entrySet()){ List<GsrDatEmpFinalRatingBO>
				 * empGoalList = (List<GsrDatEmpFinalRatingBO>)entry.getValue();
				 * for(GsrDatEmpFinalRatingBO finalRatingDetail : empGoalList){
				 * if
				 * (!empGoalsMap.containsKey(finalRatingDetail.getClosedBy())) {
				 * List<GsrDatEmpFinalRatingBO> finalBean = new
				 * ArrayList<GsrDatEmpFinalRatingBO>();
				 * finalBean.add(finalRatingDetail);
				 * empFinalMap.put(finalRatingDetail.getClosedBy(), finalBean);
				 * continue; } if
				 * (empGoalsMap.containsKey(finalRatingDetail.getClosedBy())) {
				 * List<GsrDatEmpFinalRatingBO> finalBean = new
				 * ArrayList<GsrDatEmpFinalRatingBO>();
				 * finalBean.add(finalRatingDetail);
				 * empFinalMap.get(finalRatingDetail
				 * .getClosedBy()).addAll(finalBean); } } }
				 */
				for (Map.Entry<Short, List<GsrDatEmpFinalRatingBO>> entry : empGoalsMap
						.entrySet()) {
					List<GsrDatEmpFinalRatingBO> empGoalList = (List<GsrDatEmpFinalRatingBO>) entry
							.getValue();
					if (empGoalList.size() == 1) {
						String fullmsgIdentifierKey = msgIdentifierKey
								+ "_"
								+ employeeId
								+ "_"
								+ empGoalList.get(0)
										.getFkGsrFinalRatingSlabId() + "_"
								+ empGoalList.get(0).getClosedBy();
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
						isDuplicatesFound = checkDuplicates(employeeId,
								fullmsgIdentifierKey);
						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat.format(
									forceCloseByHROrManagerToRmScreen,
									getEmployeeNameByEmpId(empGoalList.get(0)
											.getFkGsrFinalRatingEmpId()),
									empGoalList.get(0).getGsrSlabBO_slab_id()
											.getGsrSlabShortName(),
									getEmployeeNameByEmpId(empGoalList.get(0)
											.getClosedBy()));
							notificationDetails.setEmpId(employeeId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(empGoalList
											.get(0)
											.getRatingAcceptOrRejectDate());
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(GsrNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
							notificationDetails.setMenuUrl(forceCloseRmKey
									.split(",")[0]);
							notificationDetails.setIsAction(forceCloseRmKey
									.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(forceCloseRmKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat.format(
									forceCloseByHROrManagerToRmScreen,
									getEmployeeNameByEmpId(empGoalList.get(0)
											.getFkGsrFinalRatingEmpId()),
									empGoalList.get(0).getGsrSlabBO_slab_id()
											.getGsrSlabShortName(),
									getEmployeeNameByEmpId(empGoalList.get(0)
											.getClosedBy()));
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);

							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= empGoalList.get(0)
									.getRatingAcceptOrRejectDate().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(empGoalList
												.get(0)
												.getRatingAcceptOrRejectDate());
							if (isDuplicatesFound
									.getClearFlag()
									.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
								if (!isDuplicatesFound.getNoificationMessage()
										.trim()
										.equals(notificationMessage.trim()))
									isDuplicatesFound
											.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					} else {
						String fullmsgIdentifierKey = msgIdentifierKey
								+ "_"
								+ employeeId
								+ "_"
								+ empGoalList.get(0)
										.getFkGsrFinalRatingSlabId() + "_"
								+ empGoalList.get(0).getClosedBy();
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
						isDuplicatesFound = checkDuplicates(employeeId,
								fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							Date maxDate = empGoalList
									.stream()
									.map(GsrDatEmpFinalRatingBO::getRatingAcceptOrRejectDate)
									.max(Date::compareTo).get();
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(forceCloseByHROrManagerToRmScreenConsolidatedMsg,
											empGoalList.size(), empGoalList
													.get(0)
													.getGsrSlabBO_slab_id()
													.getGsrSlabShortName(),
											getEmployeeNameByEmpId(empGoalList
													.get(0).getClosedBy()));
							notificationDetails.setEmpId(employeeId);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(maxDate);
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(GsrNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
							notificationDetails.setMenuUrl(forceCloseRmKey
									.split(",")[0]);
							notificationDetails.setIsAction(forceCloseRmKey
									.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(forceCloseRmKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							Date maxDate = empGoalList
									.stream()
									.map(GsrDatEmpFinalRatingBO::getRatingAcceptOrRejectDate)
									.max(Date::compareTo).get();
							String notificationMessage = MessageFormat
									.format(forceCloseByHROrManagerToRmScreenConsolidatedMsg,
											empGoalList.size(), empGoalList
													.get(0)
													.getGsrSlabBO_slab_id()
													.getGsrSlabShortName(),
											getEmployeeNameByEmpId(empGoalList
													.get(0).getClosedBy()));
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (maxDate.getTime() > isDuplicatesFound
									.getNotificationcreatedDate().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(maxDate);
							if (isDuplicatesFound
									.getClearFlag()
									.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
								if (!isDuplicatesFound.getNoificationMessage()
										.trim()
										.equals(notificationMessage.trim()))
									isDuplicatesFound
											.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					}
				}
			} else {
				String menuURL = forceCloseRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(employeeId,
									menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, employeeId,
									forceCloseRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}

		LOG.endUsecase("Exiting goalForceCloseByByManagerOrHROrSLRMToRMScreen() service");

	}

	/* Pre-Normalised Rating Set.(By Manager) to Employee screen */
	public void preNormalisedRatingSetByManagerToEMployeeScreen(int employeeId)
			throws DataAccessException {

		LOG.startUsecase("Entering preNormalisedRatingSetByManagerToEMployeeScreen() service");
		List<GsrDatEmpFinalRatingBO> gsrDatEmpFinalRatingBO;
		String rmSetPrenormRatingKey = rmSetPrenormRatingKeys;
		String msgIdentifierKey = rmSetPrenormRatingKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		List<Integer> notificationIdList = null;
		int updateCount = 0;
		try {
			gsrDatEmpFinalRatingBO = gsrFinalRatingRepository
					.findByFkGsrFinalRatingEmpIdAndFinalRatingStatusAndIsOverriddenFlag(
							employeeId, GsrNotificationConstants.FINAL_RATED,
							GsrNotificationConstants.NOT_OVERRIDEN);

			if (gsrDatEmpFinalRatingBO.size() > 0) {
				String fullmsgIdentifierKey = msgIdentifierKey
						+ "_"
						+ employeeId
						+ "_"
						+ gsrDatEmpFinalRatingBO.get(0)
								.getFkGsrFinalRatingSlabId();
				fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
				isDuplicatesFound = checkDuplicates(employeeId,
						fullmsgIdentifierKey);

				if (isDuplicatesFound == null) {
					DatNotificationsBO notificationDetails = new DatNotificationsBO();
					String notificationMessage = MessageFormat.format(
							setPreNormalisedRatingByRMToEmployeeScreen,
							gsrDatEmpFinalRatingBO.get(0)
									.getGsrSlabBO_slab_id()
									.getGsrSlabShortName());
					notificationDetails.setEmpId(employeeId);
					notificationDetails
							.setNoificationMessage(notificationMessage);
					notificationDetails
							.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
					notificationDetails
							.setNotificationcreatedDate(gsrDatEmpFinalRatingBO
									.get(0).getCreatedDate());
					notificationDetails.setClearFlagModifiedDate(new Date());
					notificationDetails.setClearFlagcreatedDate(new Date());
					notificationDetails
							.setModuleName(GsrNotificationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
					notificationDetails.setMenuUrl(rmSetPrenormRatingKey
							.split(",")[0]);
					notificationDetails.setIsAction(rmSetPrenormRatingKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(rmSetPrenormRatingKey.split(",")[3]));
					try {
						notificationRepository.save(notificationDetails);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				} else {
					String notificationMessage = MessageFormat.format(
							setPreNormalisedRatingByRMToEmployeeScreen,
							gsrDatEmpFinalRatingBO.get(0)
									.getGsrSlabBO_slab_id()
									.getGsrSlabShortName());
					isDuplicatesFound
							.setNoificationMessage(notificationMessage);
					isDuplicatesFound.setNotificationModifiedDate(new Date());
					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= gsrDatEmpFinalRatingBO.get(0)
							.getCreatedDate().getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound
								.setNotificationcreatedDate(gsrDatEmpFinalRatingBO
										.get(0).getCreatedDate());
					if (isDuplicatesFound.getClearFlag().equals(
							NotificationSettingConstants.CLEAR_FLAG_YES))
						if (!isDuplicatesFound.getNoificationMessage().trim()
								.equals(notificationMessage.trim()))
							isDuplicatesFound
									.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
					try {
						notificationRepository.save(isDuplicatesFound);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				}
			} else {
				String menuURL = rmSetPrenormRatingKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(employeeId,
									menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, employeeId,
									rmSetPrenormRatingKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
		LOG.endUsecase("Exiting preNormalisedRatingSetByManagerToEMployeeScreen() service");
	}

	/* Pre-Normalised Rating Update.(By Manager) to Employee screen */
	public void preNormalisedRatingUpdateByManagerToEMployeeScreen(
			int employeeId) throws DataAccessException {

		LOG.startUsecase("Entering preNormalisedRatingUpdateByManagerToEMployeeScreen() service");
		List<GsrDatEmpFinalRatingBO> gsrDatEmpFinalRatingBO;
		String rmUpdPrenormRatingKey = rmUpdPrenormRatingKeys;
		String msgIdentifierKey = rmUpdPrenormRatingKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		List<Integer> notificationIdList = null;
		DatEmpProfessionalDetailBO ProfessionalDetailBO;
		int updateCount = 0;
		try {
			try {
				ProfessionalDetailBO = professionalDetailsRepository
						.findByFkMainEmpDetailId(employeeId);
			} catch (Exception e) {
				throw new DataAccessException(
						"error in get ProfessionalDetailBO");
			}
			int overridenBy = ProfessionalDetailBO.getFkEmpReportingMgrId();

			gsrDatEmpFinalRatingBO = gsrFinalRatingRepository
					.findByFkGsrFinalRatingEmpIdAndFinalRatingStatusAndIsOverriddenFlagAndOverriddenBy(
							employeeId, GsrNotificationConstants.FINAL_RATED,
							GsrNotificationConstants.OVERRIDEN, overridenBy);

			if (gsrDatEmpFinalRatingBO.size() > 0) {
				String fullmsgIdentifierKey = msgIdentifierKey
						+ "_"
						+ employeeId
						+ "_"
						+ gsrDatEmpFinalRatingBO.get(0)
								.getFkGsrFinalRatingSlabId();
				fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
				isDuplicatesFound = checkDuplicates(employeeId,
						fullmsgIdentifierKey);

				if (isDuplicatesFound == null) {
					DatNotificationsBO notificationDetails = new DatNotificationsBO();
					String notificationMessage = MessageFormat.format(
							updatePreNormalisedRatingByRMToEmployeeScreen,
							gsrDatEmpFinalRatingBO.get(0)
									.getGsrSlabBO_slab_id()
									.getGsrSlabShortName());
					notificationDetails.setEmpId(employeeId);
					notificationDetails
							.setNoificationMessage(notificationMessage);
					notificationDetails
							.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
					notificationDetails
							.setNotificationcreatedDate(gsrDatEmpFinalRatingBO
									.get(0).getModifiedDate());
					notificationDetails.setClearFlagModifiedDate(new Date());
					notificationDetails.setClearFlagcreatedDate(new Date());
					notificationDetails
							.setModuleName(GsrNotificationConstants.MODULE_NAME);
					notificationDetails
							.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
					notificationDetails.setMenuUrl(rmUpdPrenormRatingKey
							.split(",")[0]);
					notificationDetails.setIsAction(rmUpdPrenormRatingKey
							.split(",")[1]);
					notificationDetails
							.setNoificationMessageIdentifier(fullmsgIdentifierKey);
					notificationDetails
							.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
					notificationDetails.setNotificationExpiryTime(Integer
							.parseInt(rmUpdPrenormRatingKey.split(",")[3]));
					try {
						notificationRepository.save(notificationDetails);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				} else {
					String notificationMessage = MessageFormat.format(
							updatePreNormalisedRatingByRMToEmployeeScreen,
							gsrDatEmpFinalRatingBO.get(0)
									.getGsrSlabBO_slab_id()
									.getGsrSlabShortName());
					isDuplicatesFound
							.setNoificationMessage(notificationMessage);
					isDuplicatesFound.setNotificationModifiedDate(new Date());
					if (isDuplicatesFound.getNotificationcreatedDate()
							.getTime() >= gsrDatEmpFinalRatingBO.get(0)
							.getModifiedDate().getTime())
						isDuplicatesFound
								.setNotificationcreatedDate(isDuplicatesFound
										.getNotificationcreatedDate());
					else
						isDuplicatesFound
								.setNotificationcreatedDate(gsrDatEmpFinalRatingBO
										.get(0).getModifiedDate());
					if (isDuplicatesFound.getClearFlag().equals(
							NotificationSettingConstants.CLEAR_FLAG_YES))
						if (!isDuplicatesFound.getNoificationMessage().trim()
								.equals(notificationMessage.trim()))
							isDuplicatesFound
									.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
					try {
						notificationRepository.save(isDuplicatesFound);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);
					}
				}
			} else {
				String menuURL = rmUpdPrenormRatingKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(employeeId,
									menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, employeeId,
									rmUpdPrenormRatingKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
		LOG.endUsecase("Exiting preNormalisedRatingUpdateByManagerToEMployeeScreen() service");

	}

	/* Dis-Agree Rating .(By Employee) to RM screen */
	public void disAgreeRatingByEmployeeToRMScreen(int employeeId)
			throws DataAccessException {
		DatNotificationsBO datNotificationsBO;
		DatNotificationsBO checkDuplicates;
		List<GsrDatEmpFinalRatingBO> gsrDatEmpFinalRatingBO;
		String notificationMessage = "";
		DatEmpPersonalDetailBO personalDetailBO;
		try {
			personalDetailBO = personalDetailsRepository
					.findByFkEmpDetailId(employeeId);
		} catch (Exception e) {
			throw new DataAccessException("error in get ProfessionalDetailBO");
		}
		String reporteeName = personalDetailBO.getEmpFirstName() + " "
				+ personalDetailBO.getEmpLastName();
		LOG.info("reporteeName" + reporteeName);
		try {
			gsrDatEmpFinalRatingBO = gsrFinalRatingRepository
					.findByFkGsrFinalRatingEmpIdAndFinalRatingStatus(
							employeeId, GsrNotificationConstants.DIS_AGREE);
			LOG.info("disAgreeRatingByEmployeeToRMScreen gsrDatEmpFinalRatingBO "
					+ gsrDatEmpFinalRatingBO);
			if (gsrDatEmpFinalRatingBO.size() != 0) {
				for (int i = 0; i < gsrDatEmpFinalRatingBO.size(); i++) {

					String finalRating = String.valueOf(gsrDatEmpFinalRatingBO
							.get(i).getGsrFinalRating());
					String slabName = gsrDatEmpFinalRatingBO.get(i)
							.getGsrSlabBO_slab_id().getGsrSlabName();

					notificationMessage = MessageFormat.format(
							disagreeRatingByEmployeeToRMScreen, reporteeName,
							finalRating, slabName);
					int rmId = gsrDatEmpFinalRatingBO.get(i)
							.getFkGsrFinalRatingMgrId();
					try {
						checkDuplicates = notificationRepository
								.findByEmpIdAndNoificationMessage(rmId,
										notificationMessage);

					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
					if (checkDuplicates == null) {
						LOG.info(" Inserting the notification messages into notification table ");

						datNotificationsBO = new DatNotificationsBO();
						datNotificationsBO.setEmpId(rmId);
						datNotificationsBO
								.setNoificationMessage(notificationMessage);
						datNotificationsBO
								.setNotificationcreatedDate(new Date());
						datNotificationsBO.setClearFlagcreatedDate(new Date());
						datNotificationsBO.setClearFlagModifiedDate(new Date());
						datNotificationsBO
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						datNotificationsBO
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						datNotificationsBO
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						datNotificationsBO
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {

							notificationRepository.save(datNotificationsBO);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Inserted disAgreeRatingByEmployeeToRMScreen");

					} else if (String.valueOf(checkDuplicates.getEmpId())
							.equalsIgnoreCase(String.valueOf(rmId))
							&& checkDuplicates.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)
							&& checkDuplicates.getClearFlag() == "NO") {
						LOG.info(" Updating the notification messages into notification table ");

						checkDuplicates.setEmpId(rmId);
						checkDuplicates
								.setNoificationMessage(notificationMessage);
						checkDuplicates.setNotificationModifiedDate(new Date());
						checkDuplicates.setClearFlagcreatedDate(new Date());

						checkDuplicates
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						checkDuplicates
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						checkDuplicates
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						checkDuplicates
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {

							notificationRepository.save(checkDuplicates);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Updated disAgreeRatingByEmployeeToRMScreen");

					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
	}

	/* Dis-Agree Rating .(By Employee) to SkipLevelRM screen */
	public void disAgreeRatingByEmployeeToSkipLevelRMScreen(int employeeId)
			throws DataAccessException {
		DatNotificationsBO datNotificationsBO;
		DatNotificationsBO checkDuplicates;
		List<GsrDatEmpFinalRatingBO> gsrDatEmpFinalRatingBO;
		String notificationMessage = "";
		DatEmpPersonalDetailBO personalDetailBO;
		DatEmpProfessionalDetailBO professionalDetailBO;
		try {
			personalDetailBO = personalDetailsRepository
					.findByFkEmpDetailId(employeeId);
		} catch (Exception e) {
			throw new DataAccessException("error in get ProfessionalDetailBO");
		}
		String reporteeName = personalDetailBO.getEmpFirstName() + " "
				+ personalDetailBO.getEmpLastName();

		LOG.info("disAgreeRatingByEmployeeToSkipLevelRMScreen reporteeName"
				+ reporteeName);
		try {
			gsrDatEmpFinalRatingBO = gsrFinalRatingRepository
					.findByFkGsrFinalRatingEmpIdAndFinalRatingStatus(
							employeeId, GsrNotificationConstants.DIS_AGREE);
			LOG.info("disAgreeRatingByEmployeeToSkipLevelRMScreen  gsrDatEmpFinalRatingBO "
					+ gsrDatEmpFinalRatingBO);
			if (gsrDatEmpFinalRatingBO.size() != 0) {
				for (int i = 0; i < gsrDatEmpFinalRatingBO.size(); i++) {

					try {
						professionalDetailBO = professionalDetailsRepository
								.findByFkMainEmpDetailId(gsrDatEmpFinalRatingBO
										.get(i).getFkGsrFinalRatingMgrId());
					} catch (Exception e) {
						throw new DataAccessException(
								"error in get ProfessionalDetailBO");
					}
					int skipLevelRMID = professionalDetailBO
							.getFkEmpReportingMgrId();
					int managerID = gsrDatEmpFinalRatingBO.get(i)
							.getFkGsrFinalRatingMgrId();
					try {
						personalDetailBO = personalDetailsRepository
								.findByFkEmpDetailId(managerID);
					} catch (Exception e) {
						throw new DataAccessException(
								"error in get ProfessionalDetailBO");
					}
					String reportingManagerName = personalDetailBO
							.getEmpFirstName()
							+ " "
							+ personalDetailBO.getEmpLastName();
					String finalRating = String.valueOf(gsrDatEmpFinalRatingBO
							.get(i).getGsrFinalRating());
					String slabName = gsrDatEmpFinalRatingBO.get(i)
							.getGsrSlabBO_slab_id().getGsrSlabName();

					notificationMessage = MessageFormat.format(
							disagreeRatingByEmployeeToSkipLevelManagerScreen,
							reporteeName, reportingManagerName, finalRating,
							slabName);

					try {
						checkDuplicates = notificationRepository
								.findByEmpIdAndNoificationMessage(
										skipLevelRMID, notificationMessage);

					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
					if (checkDuplicates == null) {
						LOG.info(" Inserting the notification messages into notification table ");

						datNotificationsBO = new DatNotificationsBO();
						datNotificationsBO.setEmpId(skipLevelRMID);
						datNotificationsBO
								.setNoificationMessage(notificationMessage);
						datNotificationsBO
								.setNotificationcreatedDate(new Date());
						datNotificationsBO.setClearFlagcreatedDate(new Date());
						datNotificationsBO.setClearFlagModifiedDate(new Date());
						datNotificationsBO
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						datNotificationsBO
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						datNotificationsBO
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						datNotificationsBO
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {

							notificationRepository.save(datNotificationsBO);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Inserted disAgreeRatingByEmployeeToSkipLevelRMScreen");

					} else if (String.valueOf(checkDuplicates.getEmpId())
							.equalsIgnoreCase(String.valueOf(skipLevelRMID))
							&& checkDuplicates.getNoificationMessage()
									.equalsIgnoreCase(notificationMessage)
							&& checkDuplicates.getClearFlag() == "NO") {
						LOG.info(" Updating the notification messages into notification table ");

						checkDuplicates.setEmpId(skipLevelRMID);
						checkDuplicates
								.setNoificationMessage(notificationMessage);
						checkDuplicates.setNotificationModifiedDate(new Date());
						checkDuplicates.setClearFlagcreatedDate(new Date());

						checkDuplicates
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						checkDuplicates
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						checkDuplicates
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

						checkDuplicates
								.setMenuUrl(GsrNotificationConstants.MENU_URL);
						try {

							notificationRepository.save(checkDuplicates);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						LOG.info("Succesfully Updated disAgreeRatingByEmployeeToSkipLevelRMScreen");

					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
	}

	/* Dis-Agree Rating .(By Employee) to HR screen */
	public void disAgreeRatingByEmployeeToHRScreen(int employeeId)
			throws DataAccessException {
		DatNotificationsBO datNotificationsBO;
		DatNotificationsBO checkDuplicates;
		List<GsrDatEmpFinalRatingBO> gsrDatEmpFinalRatingBO;
		String notificationMessage = "";
		DatEmpPersonalDetailBO personalDetailBO;
		try {
			personalDetailBO = personalDetailsRepository
					.findByFkEmpDetailId(employeeId);
		} catch (Exception e) {
			throw new DataAccessException("error in get ProfessionalDetailBO");
		}
		String reporteeName = personalDetailBO.getEmpFirstName() + " "
				+ personalDetailBO.getEmpLastName();

		LOG.info("disAgreeRatingByEmployeeToSkipLevelRMScreen reporteeName"
				+ reporteeName);
		try {
			gsrDatEmpFinalRatingBO = gsrFinalRatingRepository
					.findByFkGsrFinalRatingEmpIdAndFinalRatingStatus(
							employeeId, GsrNotificationConstants.DIS_AGREE);
			LOG.info("disAgreeRatingByEmployeeToSkipLevelRMScreen  gsrDatEmpFinalRatingBO "
					+ gsrDatEmpFinalRatingBO);
			if (gsrDatEmpFinalRatingBO.size() != 0) {
				for (int i = 0; i < gsrDatEmpFinalRatingBO.size(); i++) {
					List<DatEmpDetailBO> empDetailBo;

					List<Short> roles = new ArrayList<Short>();
					// 8,9,10,11 are HRD roles
					roles.add((short) 8);
					roles.add((short) 9);
					roles.add((short) 10);
					roles.add((short) 11);

					try {

						empDetailBo = empDetailRepository
								.findByFkEmpRoleIdIn(roles);
						LOG.info("empDetailBo" + empDetailBo);
					} catch (Exception e) {
						throw new DataAccessException("Failed to Save : ", e);

					}
					int managerID = gsrDatEmpFinalRatingBO.get(i)
							.getFkGsrFinalRatingMgrId();
					try {
						personalDetailBO = personalDetailsRepository
								.findByFkEmpDetailId(managerID);
					} catch (Exception e) {
						throw new DataAccessException(
								"error in get ProfessionalDetailBO");
					}
					String reportingManagerName = personalDetailBO
							.getEmpFirstName()
							+ " "
							+ personalDetailBO.getEmpLastName();
					for (int j = 0; j < empDetailBo.size(); j++) {
						String finalRating = String
								.valueOf(gsrDatEmpFinalRatingBO.get(i)
										.getGsrFinalRating());
						String slabName = gsrDatEmpFinalRatingBO.get(i)
								.getGsrSlabBO_slab_id().getGsrSlabName();

						notificationMessage = MessageFormat.format(
								disagreeRatingByEmployeeToHRScreen,
								reporteeName, reportingManagerName,
								finalRating, slabName);
						int hrEmployeeId = empDetailBo.get(j).getPkEmpId();
						try {
							checkDuplicates = notificationRepository
									.findByEmpIdAndNoificationMessage(
											hrEmployeeId, notificationMessage);

						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);

						}
						if (checkDuplicates == null) {
							LOG.info(" Inserting the notification messages into notification table ");

							datNotificationsBO = new DatNotificationsBO();
							datNotificationsBO.setEmpId(hrEmployeeId);
							datNotificationsBO
									.setNoificationMessage(notificationMessage);
							datNotificationsBO
									.setNotificationcreatedDate(new Date());
							datNotificationsBO
									.setClearFlagcreatedDate(new Date());
							datNotificationsBO
									.setClearFlagModifiedDate(new Date());
							datNotificationsBO
									.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
							datNotificationsBO
									.setModuleName(GsrNotificationConstants.MODULE_NAME);
							datNotificationsBO
									.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

							datNotificationsBO
									.setMenuUrl(GsrNotificationConstants.MENU_URL);
							try {

								notificationRepository.save(datNotificationsBO);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);

							}
							LOG.info("Succesfully Inserted disAgreeRatingByEmployeeToSkipLevelRMScreen");

						} else if (String.valueOf(checkDuplicates.getEmpId())
								.equalsIgnoreCase(String.valueOf(hrEmployeeId))
								&& checkDuplicates.getNoificationMessage()
										.equalsIgnoreCase(notificationMessage)
								&& checkDuplicates.getClearFlag() == "NO") {
							LOG.info(" Updating the notification messages into notification table ");

							checkDuplicates.setEmpId(hrEmployeeId);
							checkDuplicates
									.setNoificationMessage(notificationMessage);
							checkDuplicates
									.setNotificationModifiedDate(new Date());
							checkDuplicates.setClearFlagcreatedDate(new Date());
							checkDuplicates
									.setClearFlagModifiedDate(new Date());
							checkDuplicates
									.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
							checkDuplicates
									.setModuleName(GsrNotificationConstants.MODULE_NAME);
							checkDuplicates
									.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);

							checkDuplicates
									.setMenuUrl(GsrNotificationConstants.MENU_URL);
							try {

								notificationRepository.save(checkDuplicates);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);

							}
							LOG.info("Succesfully Updated disAgreeRatingByEmployeeToSkipLevelRMScreen");

						}
					}
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
	}

	/* Dis-Agree Rating .(By Employee) to RM screen ConsolidatedMsg */
	public void disAgreeRatingByEmployeeToRMScreenConsolidatedMsg(int rmID)
			throws DataAccessException {

		LOG.startUsecase("Entering disAgreeRatingByEmployeeToRMScreenConsolidatedMsg() service");
		List<GsrDatEmpFinalRatingBO> gsrDatEmpFinalRatingBOList;
		Map<Short, List<GsrDatEmpFinalRatingBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpFinalRatingBO>>();
		String disAgreeByRmKey = disAgreeByRmKeys;
		String msgIdentifierKey = disAgreeByRmKey.split(",")[2];
		int updateCount = 0;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;

		try {

			gsrDatEmpFinalRatingBOList = gsrFinalRatingRepository
					.findByFkGsrFinalRatingMgrIdAndFinalRatingStatus(rmID,
							GsrNotificationConstants.DIS_AGREE);

			if (gsrDatEmpFinalRatingBOList.size() > 0) {
				for (GsrDatEmpFinalRatingBO empGoal : gsrDatEmpFinalRatingBOList) {
					if (!empGoalsMap.containsKey(empGoal
							.getFkGsrFinalRatingSlabId())) {
						List<GsrDatEmpFinalRatingBO> finalBean = new ArrayList<GsrDatEmpFinalRatingBO>();
						finalBean.add(empGoal);
						empGoalsMap.put(empGoal.getFkGsrFinalRatingSlabId(),
								finalBean);
						continue;
					}
					if (empGoalsMap.containsKey(empGoal
							.getFkGsrFinalRatingSlabId())) {
						List<GsrDatEmpFinalRatingBO> finalBean = new ArrayList<GsrDatEmpFinalRatingBO>();
						finalBean.add(empGoal);
						empGoalsMap.get(empGoal.getFkGsrFinalRatingSlabId())
								.addAll(finalBean);
					}
				}
				for (Map.Entry<Short, List<GsrDatEmpFinalRatingBO>> entry : empGoalsMap
						.entrySet()) {
					List<GsrDatEmpFinalRatingBO> empGoalList = (List<GsrDatEmpFinalRatingBO>) entry
							.getValue();

					if (empGoalList.size() == 1) {
						String fullmsgIdentifierKey = msgIdentifierKey
								+ "_"
								+ rmID
								+ "_"
								+ empGoalList.get(0)
										.getFkGsrFinalRatingSlabId();
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
						isDuplicatesFound = checkDuplicates(rmID,
								fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat.format(
									disagreeRatingByEmployeeToRMScreen,
									getEmployeeNameByEmpId(empGoalList.get(0)
											.getFkGsrFinalRatingEmpId()),
									empGoalList.get(0).getGsrFinalRating(),
									empGoalList.get(0).getGsrSlabBO_slab_id()
											.getGsrSlabShortName());
							notificationDetails.setEmpId(rmID);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(empGoalList
											.get(0)
											.getRatingAcceptOrRejectDate());
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(GsrNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
							notificationDetails.setMenuUrl(disAgreeByRmKey
									.split(",")[0]);
							notificationDetails.setIsAction(disAgreeByRmKey
									.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(disAgreeByRmKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat.format(
									disagreeRatingByEmployeeToRMScreen,
									getEmployeeNameByEmpId(empGoalList.get(0)
											.getFkGsrFinalRatingEmpId()),
									empGoalList.get(0).getGsrFinalRating(),
									empGoalList.get(0).getGsrSlabBO_slab_id()
											.getGsrSlabShortName());
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= empGoalList.get(0)
									.getRatingAcceptOrRejectDate().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(empGoalList
												.get(0)
												.getRatingAcceptOrRejectDate());
							if (isDuplicatesFound
									.getClearFlag()
									.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
								if (!isDuplicatesFound.getNoificationMessage()
										.trim()
										.equals(notificationMessage.trim()))
									isDuplicatesFound
											.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					} else {
						String fullmsgIdentifierKey = msgIdentifierKey
								+ "_"
								+ rmID
								+ "_"
								+ empGoalList.get(0)
										.getFkGsrFinalRatingSlabId();
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
						isDuplicatesFound = checkDuplicates(rmID,
								fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							Date maxDate = empGoalList
									.stream()
									.map(GsrDatEmpFinalRatingBO::getRatingAcceptOrRejectDate)
									.max(Date::compareTo).get();
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(disagreeRatingByEmployeeToRMScreenConsolidatedMsg,
											empGoalList.size(), empGoalList
													.get(0)
													.getGsrSlabBO_slab_id()
													.getGsrSlabShortName());

							notificationDetails.setEmpId(rmID);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(maxDate);
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(GsrNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
							notificationDetails.setMenuUrl(disAgreeByRmKey
									.split(",")[0]);
							notificationDetails.setIsAction(disAgreeByRmKey
									.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(disAgreeByRmKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							Date maxDate = empGoalList
									.stream()
									.map(GsrDatEmpFinalRatingBO::getRatingAcceptOrRejectDate)
									.max(Date::compareTo).get();
							String notificationMessage = MessageFormat
									.format(disagreeRatingByEmployeeToRMScreenConsolidatedMsg,
											empGoalList.size(), empGoalList
													.get(0)
													.getGsrSlabBO_slab_id()
													.getGsrSlabShortName());
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (maxDate.getTime() > isDuplicatesFound
									.getNotificationcreatedDate().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(maxDate);
							if (isDuplicatesFound
									.getClearFlag()
									.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
								if (!isDuplicatesFound.getNoificationMessage()
										.trim()
										.equals(notificationMessage.trim()))
									isDuplicatesFound
											.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					}
				}
			} else {
				String menuURL = disAgreeByRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(rmID, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, rmID,
									disAgreeByRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
		LOG.endUsecase("Exiting disAgreeRatingByEmployeeToRMScreenConsolidatedMsg() service");

	}

	/* Dis-Agree Rating .(By Employee) to SkipLevel RM screen ConsolidatedMsg */
	public void disAgreeRatingByEmployeeToSkipLevelRMScreenConsolidatedMsg(
			int rmID) throws DataAccessException {

		LOG.startUsecase("Entering disAgreeRatingByEmployeeToSkipLevelRMScreenConsolidatedMsg() service");
		List<GsrDatEmpFinalRatingBO> gsrDatEmpFinalRatingBOList;
		Map<Short, List<GsrDatEmpFinalRatingBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpFinalRatingBO>>();
		Map<Integer, List<GsrDatEmpFinalRatingBO>> empGoalsFinalMap = new HashMap<Integer, List<GsrDatEmpFinalRatingBO>>();
		String disAgreeSkiplvlRmKey = disAgreeSkiplvlRmKeys;
		String msgIdentifierKey = disAgreeSkiplvlRmKey.split(",")[2];
		int updateCount = 0;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;

		try {
			gsrDatEmpFinalRatingBOList = gsrFinalRatingRepository
					.findBySkipLevelMgrIdAndFinalRatingStatus(rmID,
							GsrNotificationConstants.DIS_AGREE);

			if (gsrDatEmpFinalRatingBOList.size() > 0) {
				for (GsrDatEmpFinalRatingBO empGoal : gsrDatEmpFinalRatingBOList) {
					if (!empGoalsMap.containsKey(empGoal
							.getFkGsrFinalRatingSlabId())) {
						List<GsrDatEmpFinalRatingBO> finalBean = new ArrayList<GsrDatEmpFinalRatingBO>();
						finalBean.add(empGoal);
						empGoalsMap.put(empGoal.getFkGsrFinalRatingSlabId(),
								finalBean);
						continue;
					}
					if (empGoalsMap.containsKey(empGoal
							.getFkGsrFinalRatingSlabId())) {
						List<GsrDatEmpFinalRatingBO> finalBean = new ArrayList<GsrDatEmpFinalRatingBO>();
						finalBean.add(empGoal);
						empGoalsMap.get(empGoal.getFkGsrFinalRatingSlabId())
								.addAll(finalBean);
					}
				}
				/*
				 * for(Map.Entry<Short,List<GsrDatEmpFinalRatingBO>> entry:
				 * empGoalsMap.entrySet()){ List<GsrDatEmpFinalRatingBO>
				 * empGoalList = (List<GsrDatEmpFinalRatingBO>)entry.getValue();
				 * 
				 * for(GsrDatEmpFinalRatingBO empFinalRating : empGoalList){ if
				 * (
				 * !empGoalsMap.containsKey(empFinalRating.getFkGsrFinalRatingEmpId
				 * ())) { List<GsrDatEmpFinalRatingBO> finalBean = new
				 * ArrayList<GsrDatEmpFinalRatingBO>();
				 * finalBean.add(empFinalRating);
				 * empGoalsFinalMap.put(empFinalRating
				 * .getFkGsrFinalRatingEmpId(), finalBean); continue; } if
				 * (empGoalsMap
				 * .containsKey(empFinalRating.getFkGsrFinalRatingEmpId())) {
				 * List<GsrDatEmpFinalRatingBO> finalBean = new
				 * ArrayList<GsrDatEmpFinalRatingBO>();
				 * finalBean.add(empFinalRating);
				 * empGoalsFinalMap.get(empFinalRating
				 * .getFkGsrFinalRatingEmpId()).addAll(finalBean); } } }
				 */
				for (Map.Entry<Short, List<GsrDatEmpFinalRatingBO>> entry : empGoalsMap
						.entrySet()) {
					List<GsrDatEmpFinalRatingBO> empGoalList = (List<GsrDatEmpFinalRatingBO>) entry
							.getValue();

					if (empGoalList.size() == 1) {
						String fullmsgIdentifierKey = msgIdentifierKey
								+ "_"
								+ rmID
								+ "_"
								+ empGoalList.get(0)
										.getFkGsrFinalRatingSlabId();
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
						isDuplicatesFound = checkDuplicates(rmID,
								fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(disagreeRatingByEmployeeToSkipLevelManagerScreen,
											getEmployeeNameByEmpId(empGoalList
													.get(0)
													.getFkGsrFinalRatingEmpId()),
											getEmployeeNameByEmpId(empGoalList
													.get(0)
													.getFkGsrFinalRatingMgrId()),
											empGoalList.get(0)
													.getGsrFinalRating(),
											empGoalList.get(0)
													.getGsrSlabBO_slab_id()
													.getGsrSlabShortName());
							notificationDetails.setEmpId(rmID);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(empGoalList
											.get(0)
											.getRatingAcceptOrRejectDate());
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(GsrNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
							notificationDetails.setMenuUrl(disAgreeSkiplvlRmKey
									.split(",")[0]);
							notificationDetails
									.setIsAction(disAgreeSkiplvlRmKey
											.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(disAgreeSkiplvlRmKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat
									.format(disagreeRatingByEmployeeToSkipLevelManagerScreen,
											getEmployeeNameByEmpId(empGoalList
													.get(0)
													.getFkGsrFinalRatingEmpId()),
											getEmployeeNameByEmpId(empGoalList
													.get(0)
													.getFkGsrFinalRatingMgrId()),
											empGoalList.get(0)
													.getGsrFinalRating(),
											empGoalList.get(0)
													.getGsrSlabBO_slab_id()
													.getGsrSlabShortName());
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= empGoalList.get(0)
									.getRatingAcceptOrRejectDate().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(empGoalList
												.get(0)
												.getRatingAcceptOrRejectDate());
							if (isDuplicatesFound
									.getClearFlag()
									.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
								if (!isDuplicatesFound.getNoificationMessage()
										.trim()
										.equals(notificationMessage.trim()))
									isDuplicatesFound
											.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					} else {
						String fullmsgIdentifierKey = msgIdentifierKey
								+ "_"
								+ rmID
								+ "_"
								+ empGoalList.get(0)
										.getFkGsrFinalRatingSlabId();
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
						isDuplicatesFound = checkDuplicates(rmID,
								fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							Date maxDate = empGoalList
									.stream()
									.map(GsrDatEmpFinalRatingBO::getRatingAcceptOrRejectDate)
									.max(Date::compareTo).get();
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(disagreeRatingByEmployeeToSkipLevelManagerScreenConsolidatedMsg,
											empGoalList.size(),
											getEmployeeNameByEmpId(empGoalList
													.get(0)
													.getFkGsrFinalRatingMgrId()),
											empGoalList.get(0)
													.getGsrSlabBO_slab_id()
													.getGsrSlabShortName());
							notificationDetails.setEmpId(rmID);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(maxDate);
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(GsrNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
							notificationDetails.setMenuUrl(disAgreeSkiplvlRmKey
									.split(",")[0]);
							notificationDetails
									.setIsAction(disAgreeSkiplvlRmKey
											.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(disAgreeSkiplvlRmKey
													.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							Date maxDate = empGoalList
									.stream()
									.map(GsrDatEmpFinalRatingBO::getRatingAcceptOrRejectDate)
									.max(Date::compareTo).get();
							String notificationMessage = MessageFormat
									.format(disagreeRatingByEmployeeToSkipLevelManagerScreenConsolidatedMsg,
											empGoalList.size(),
											getEmployeeNameByEmpId(empGoalList
													.get(0)
													.getFkGsrFinalRatingMgrId()),
											empGoalList.get(0)
													.getGsrSlabBO_slab_id()
													.getGsrSlabShortName());
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (maxDate.getTime() > isDuplicatesFound
									.getNotificationcreatedDate().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(maxDate);
							if (isDuplicatesFound
									.getClearFlag()
									.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
								if (!isDuplicatesFound.getNoificationMessage()
										.trim()
										.equals(notificationMessage.trim()))
									isDuplicatesFound
											.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					}
				}
			} else {
				String menuURL = disAgreeSkiplvlRmKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(rmID, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, rmID,
									disAgreeSkiplvlRmKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
		LOG.endUsecase("Exiting disAgreeRatingByEmployeeToSkipLevelRMScreenConsolidatedMsg() service");

	}

	/* Dis-Agree Rating .(By Employee) to SkipLevel RM screen ConsolidatedMsg */
	public void disAgreeRatingByEmployeeToHRScreenConsolidatedMsg(int rmID)
			throws DataAccessException {

		LOG.startUsecase("Entering disAgreeRatingByEmployeeToHRScreenConsolidatedMsg() service");
		List<GsrDatEmpFinalRatingBO> gsrDatEmpFinalRatingBOList;
		Map<Short, List<GsrDatEmpFinalRatingBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpFinalRatingBO>>();
		Map<Integer, List<GsrDatEmpFinalRatingBO>> empGoalsFinalMap = new HashMap<Integer, List<GsrDatEmpFinalRatingBO>>();
		String disAgreeHrKey = disAgreeHrKeys;
		String msgIdentifierKey = disAgreeHrKey.split(",")[2];
		int updateCount = 0;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;

		try {
			gsrDatEmpFinalRatingBOList = gsrFinalRatingRepository
					.findByFinalRatingStatusOrderByFkGsrFinalRatingSlabIdAscFkGsrFinalRatingMgrIdAsc(GsrNotificationConstants.DIS_AGREE);

			if (gsrDatEmpFinalRatingBOList.size() > 0) {
				for (GsrDatEmpFinalRatingBO empGoal : gsrDatEmpFinalRatingBOList) {
					if (!empGoalsMap.containsKey(empGoal
							.getFkGsrFinalRatingSlabId())) {
						List<GsrDatEmpFinalRatingBO> finalBean = new ArrayList<GsrDatEmpFinalRatingBO>();
						finalBean.add(empGoal);
						empGoalsMap.put(empGoal.getFkGsrFinalRatingSlabId(),
								finalBean);
						continue;
					}
					if (empGoalsMap.containsKey(empGoal
							.getFkGsrFinalRatingSlabId())) {
						List<GsrDatEmpFinalRatingBO> finalBean = new ArrayList<GsrDatEmpFinalRatingBO>();
						finalBean.add(empGoal);
						empGoalsMap.get(empGoal.getFkGsrFinalRatingSlabId())
								.addAll(finalBean);
					}
				}
				/*
				 * for(Map.Entry<Short,List<GsrDatEmpFinalRatingBO>> entry:
				 * empGoalsMap.entrySet()){ List<GsrDatEmpFinalRatingBO>
				 * empGoalList = (List<GsrDatEmpFinalRatingBO>)entry.getValue();
				 * 
				 * for(GsrDatEmpFinalRatingBO empFinalRating : empGoalList){ if
				 * (!empGoalsFinalMap.containsKey(empFinalRating.
				 * getFkGsrFinalRatingMgrId())) { List<GsrDatEmpFinalRatingBO>
				 * finalBean = new ArrayList<GsrDatEmpFinalRatingBO>();
				 * finalBean.add(empFinalRating);
				 * empGoalsFinalMap.put(empFinalRating
				 * .getFkGsrFinalRatingMgrId(), finalBean); continue; } if
				 * (empGoalsFinalMap
				 * .containsKey(empFinalRating.getFkGsrFinalRatingMgrId())) {
				 * List<GsrDatEmpFinalRatingBO> finalBean = new
				 * ArrayList<GsrDatEmpFinalRatingBO>();
				 * finalBean.add(empFinalRating);
				 * empGoalsFinalMap.get(empFinalRating
				 * .getFkGsrFinalRatingMgrId()).addAll(finalBean); } } }
				 */
				for (Map.Entry<Short, List<GsrDatEmpFinalRatingBO>> entry : empGoalsMap
						.entrySet()) {
					List<GsrDatEmpFinalRatingBO> empGoalList = (List<GsrDatEmpFinalRatingBO>) entry
							.getValue();

					if (empGoalList.size() == 1) {
						String fullmsgIdentifierKey = msgIdentifierKey
								+ "_"
								+ rmID
								+ "_"
								+ empGoalList.get(0)
										.getFkGsrFinalRatingSlabId() + "_"
								+ empGoalList.get(0).getFkGsrFinalRatingMgrId();
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
						isDuplicatesFound = checkDuplicates(rmID,
								fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat.format(
									disagreeRatingByEmployeeToHRScreen,
									getEmployeeNameByEmpId(empGoalList.get(0)
											.getFkGsrFinalRatingEmpId()),
									getEmployeeNameByEmpId(empGoalList.get(0)
											.getFkGsrFinalRatingMgrId()),
									empGoalList.get(0).getGsrFinalRating(),
									empGoalList.get(0).getGsrSlabBO_slab_id()
											.getGsrSlabShortName());
							notificationDetails.setEmpId(rmID);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(empGoalList
											.get(0)
											.getRatingAcceptOrRejectDate());
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(GsrNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
							notificationDetails.setMenuUrl(disAgreeHrKey
									.split(",")[0]);
							notificationDetails.setIsAction(disAgreeHrKey
									.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(disAgreeHrKey.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							String notificationMessage = MessageFormat.format(
									disagreeRatingByEmployeeToHRScreen,
									getEmployeeNameByEmpId(empGoalList.get(0)
											.getFkGsrFinalRatingEmpId()),
									getEmployeeNameByEmpId(empGoalList.get(0)
											.getFkGsrFinalRatingMgrId()),
									empGoalList.get(0).getGsrFinalRating(),
									empGoalList.get(0).getGsrSlabBO_slab_id()
											.getGsrSlabShortName());
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (isDuplicatesFound.getNotificationcreatedDate()
									.getTime() >= empGoalList.get(0)
									.getRatingAcceptOrRejectDate().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(isDuplicatesFound
												.getNotificationcreatedDate());
							else
								isDuplicatesFound
										.setNotificationcreatedDate(empGoalList
												.get(0)
												.getRatingAcceptOrRejectDate());
							if (isDuplicatesFound
									.getClearFlag()
									.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
								if (!isDuplicatesFound.getNoificationMessage()
										.trim()
										.equals(notificationMessage.trim()))
									isDuplicatesFound
											.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					} else {
						String fullmsgIdentifierKey = msgIdentifierKey
								+ "_"
								+ rmID
								+ "_"
								+ empGoalList.get(0)
										.getFkGsrFinalRatingSlabId() + "_"
								+ empGoalList.get(0).getFkGsrFinalRatingMgrId();
						fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
						isDuplicatesFound = checkDuplicates(rmID,
								fullmsgIdentifierKey);

						if (isDuplicatesFound == null) {
							Date maxDate = empGoalList
									.stream()
									.map(GsrDatEmpFinalRatingBO::getRatingAcceptOrRejectDate)
									.max(Date::compareTo).get();
							DatNotificationsBO notificationDetails = new DatNotificationsBO();
							String notificationMessage = MessageFormat
									.format(disagreeRatingByEmployeeToHRScreenConsolidatedMsg,
											empGoalList.size(),
											getEmployeeNameByEmpId(empGoalList
													.get(0)
													.getFkGsrFinalRatingMgrId()),
											empGoalList.get(0)
													.getGsrSlabBO_slab_id()
													.getGsrSlabShortName());
							notificationDetails.setEmpId(rmID);
							notificationDetails
									.setNoificationMessage(notificationMessage);
							notificationDetails
									.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
							notificationDetails
									.setNotificationcreatedDate(maxDate);
							notificationDetails
									.setClearFlagModifiedDate(new Date());
							notificationDetails
									.setClearFlagcreatedDate(new Date());
							notificationDetails
									.setModuleName(GsrNotificationConstants.MODULE_NAME);
							notificationDetails
									.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
							notificationDetails.setMenuUrl(disAgreeHrKey
									.split(",")[0]);
							notificationDetails.setIsAction(disAgreeHrKey
									.split(",")[1]);
							notificationDetails
									.setNoificationMessageIdentifier(fullmsgIdentifierKey);
							notificationDetails
									.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
							notificationDetails
									.setNotificationExpiryTime(Integer
											.parseInt(disAgreeHrKey.split(",")[3]));
							try {
								notificationRepository
										.save(notificationDetails);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						} else {
							Date maxDate = empGoalList
									.stream()
									.map(GsrDatEmpFinalRatingBO::getRatingAcceptOrRejectDate)
									.max(Date::compareTo).get();
							String notificationMessage = MessageFormat
									.format(disagreeRatingByEmployeeToHRScreenConsolidatedMsg,
											empGoalList.size(),
											getEmployeeNameByEmpId(empGoalList
													.get(0)
													.getFkGsrFinalRatingMgrId()),
											empGoalList.get(0)
													.getGsrSlabBO_slab_id()
													.getGsrSlabShortName());
							isDuplicatesFound
									.setNoificationMessage(notificationMessage);
							isDuplicatesFound
									.setNotificationModifiedDate(new Date());
							if (maxDate.getTime() > isDuplicatesFound
									.getNotificationcreatedDate().getTime())
								isDuplicatesFound
										.setNotificationcreatedDate(maxDate);
							if (isDuplicatesFound
									.getClearFlag()
									.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
								if (!isDuplicatesFound.getNoificationMessage()
										.trim()
										.equals(notificationMessage.trim()))
									isDuplicatesFound
											.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
							try {
								notificationRepository.save(isDuplicatesFound);
							} catch (Exception e) {
								throw new DataAccessException(
										"Failed to Save : ", e);
							}
						}
					}
				}
			} else {
				String menuURL = disAgreeHrKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(rmID, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
						if (updateCount == 0)
							throw new DataAccessException(
									"Some exception occured while updating notification table");
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, rmID,
									disAgreeHrKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
		LOG.endUsecase("Exiting disAgreeRatingByEmployeeToHRScreenConsolidatedMsg() service");
	}

	/**
	 * @Description : This method is used to set Goal Set by Skip level Manager
	 *              notification to Employee Screen
	 * @param empId
	 * @throws DataAccessException
	 */
	public void goalSetBySkipLevelManagerToEmpScreen(int empId)
			throws DataAccessException {

		LOG.startUsecase("Entering goalSetBySkipLevelManagerToEmpScreen() service");
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBOList = null;
		String skipLevelRmCreGoalKey = skipLevelRmCreGoalKeys;
		String msgIdentifierKey = skipLevelRmCreGoalKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		Map<Short, List<GsrDatEmpGoalBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpGoalBO>>();
		int updateCount = 0;
		DatEmpProfessionalDetailBO profDetail = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		try {

			profDetail = professionalDetailsRepository
					.getSkipLevelRmDetailsByEmpId(empId);

			gsrDatEmpGoalBOList = gsrEmployeegoalsRepository
					.findByGsrGoalCreatedByAndFkGsrDatEmpIdAndFkGsrGoalStatus(
							profDetail.getFkEmpReportingMgrId(), empId,
							GsrNotificationConstants.GOAL_SET);

			if (gsrDatEmpGoalBOList.size() != 0) {
				if (gsrDatEmpGoalBOList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + "_"
							+ gsrDatEmpGoalBOList.get(0).getFkGsrSlabId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = checkDuplicates(empId,
							fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								goalSetBySkipLevelRmToEmpScreen,
								gsrDatEmpGoalBOList.get(0)
										.getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(gsrDatEmpGoalBOList
										.get(0).getGsrGoalCreatedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
						notificationDetails.setMenuUrl(skipLevelRmCreGoalKey
								.split(",")[0]);
						notificationDetails.setIsAction(skipLevelRmCreGoalKey
								.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(skipLevelRmCreGoalKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								goalSetBySkipLevelRmToEmpScreen,
								gsrDatEmpGoalBOList.get(0)
										.getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= gsrDatEmpGoalBOList.get(0)
								.getGsrGoalCreatedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(gsrDatEmpGoalBOList
											.get(0).getGsrGoalCreatedDate());
						if (isDuplicatesFound.getClearFlag().equals(
								NotificationSettingConstants.CLEAR_FLAG_YES))
							if (!isDuplicatesFound.getNoificationMessage()
									.trim().equals(notificationMessage.trim()))
								isDuplicatesFound
										.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					for (GsrDatEmpGoalBO empGoal : gsrDatEmpGoalBOList) {
						if (!empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap
									.put(empGoal.getFkGsrSlabId(), finalBean);
							continue;
						}
						if (empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap.get(empGoal.getFkGsrSlabId()).addAll(
									finalBean);
						}
					}
					for (Map.Entry<Short, List<GsrDatEmpGoalBO>> entry : empGoalsMap
							.entrySet()) {
						List<GsrDatEmpGoalBO> empGoalList = (List<GsrDatEmpGoalBO>) entry
								.getValue();
						if (empGoalList.size() == 1) {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_" + empId + "_"
									+ empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empId,
									fullmsgIdentifierKey);

							if (isDuplicatesFound == null) {
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalSetByRMToEmployeeScreen,
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empId);
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(empGoalList
												.get(0).getGsrGoalCreatedDate());
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails
										.setMenuUrl(skipLevelRmCreGoalKey
												.split(",")[0]);
								notificationDetails
										.setIsAction(skipLevelRmCreGoalKey
												.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(skipLevelRmCreGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								String notificationMessage = MessageFormat
										.format(goalSetByRMToEmployeeScreen,
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (isDuplicatesFound
										.getNotificationcreatedDate().getTime() >= empGoalList
										.get(0).getGsrGoalCreatedDate()
										.getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(isDuplicatesFound
													.getNotificationcreatedDate());
								else
									isDuplicatesFound
											.setNotificationcreatedDate(empGoalList
													.get(0)
													.getGsrGoalCreatedDate());
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						} else {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_" + empId + "_"
									+ empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empId,
									fullmsgIdentifierKey);

							if (isDuplicatesFound == null) {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalCreatedDate)
										.max(Date::compareTo).get();
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalSetBySkipLevelRmToEmpConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empId);
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(maxDate);
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails
										.setMenuUrl(skipLevelRmCreGoalKey
												.split(",")[0]);
								notificationDetails
										.setIsAction(skipLevelRmCreGoalKey
												.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(skipLevelRmCreGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalCreatedDate)
										.max(Date::compareTo).get();
								String notificationMessage = MessageFormat
										.format(goalSetBySkipLevelRmToEmpConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (maxDate.getTime() > isDuplicatesFound
										.getNotificationcreatedDate().getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(maxDate);
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						}
					}
				}
			} else {
				String menuURL = skipLevelRmCreGoalKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									skipLevelRmCreGoalKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
		LOG.endUsecase("Exiting goalSetBySkipLevelManagerToEmpScreen() service");
	}

	/**
	 * @Description : This method is ued to set Goal Set By HR notification to
	 *              Employee Screen
	 * @param empId
	 * @throws DataAccessException
	 */
	public void goalSetByHRToEmpScreen(int empId) throws DataAccessException {

		LOG.startUsecase("Entering goalSetByHRToEmpScreen() service");
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBOList = null;
		String hrCreGoalKey = hrCreGoalKeys;
		String msgIdentifierKey = hrCreGoalKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		Map<Short, List<GsrDatEmpGoalBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpGoalBO>>();
		int updateCount = 0;
		DatEmpProfessionalDetailBO profDetail = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		List<Integer> empDetail;
		try {
			List<Short> hrRoleIdList = Stream.of(hrRoleIds.split(","))
					.map(Short::parseShort).collect(Collectors.toList());

			empDetail = empDetailRepository.findByFkEmpRoleIds(hrRoleIdList);

			gsrDatEmpGoalBOList = gsrEmployeegoalsRepository
					.findByGsrGoalCreatedByInAndFkGsrDatEmpIdAndFkGsrGoalStatus(
							empDetail, empId, GsrNotificationConstants.GOAL_SET);

			if (gsrDatEmpGoalBOList.size() != 0) {
				if (gsrDatEmpGoalBOList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + "_"
							+ gsrDatEmpGoalBOList.get(0).getFkGsrSlabId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = checkDuplicates(empId,
							fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								goalSetByHRToEmpScreen, gsrDatEmpGoalBOList
										.get(0).getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(gsrDatEmpGoalBOList
										.get(0).getGsrGoalCreatedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
						notificationDetails
								.setMenuUrl(hrCreGoalKey.split(",")[0]);
						notificationDetails
								.setIsAction(hrCreGoalKey.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(hrCreGoalKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								goalSetByHRToEmpScreen, gsrDatEmpGoalBOList
										.get(0).getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= gsrDatEmpGoalBOList.get(0)
								.getGsrGoalCreatedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(gsrDatEmpGoalBOList
											.get(0).getGsrGoalCreatedDate());
						if (isDuplicatesFound.getClearFlag().equals(
								NotificationSettingConstants.CLEAR_FLAG_YES))
							if (!isDuplicatesFound.getNoificationMessage()
									.trim().equals(notificationMessage.trim()))
								isDuplicatesFound
										.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					for (GsrDatEmpGoalBO empGoal : gsrDatEmpGoalBOList) {
						if (!empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap
									.put(empGoal.getFkGsrSlabId(), finalBean);
							continue;
						}
						if (empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap.get(empGoal.getFkGsrSlabId()).addAll(
									finalBean);
						}
					}
					for (Map.Entry<Short, List<GsrDatEmpGoalBO>> entry : empGoalsMap
							.entrySet()) {
						List<GsrDatEmpGoalBO> empGoalList = (List<GsrDatEmpGoalBO>) entry
								.getValue();
						if (empGoalList.size() == 1) {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_" + empId + "_"
									+ empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empId,
									fullmsgIdentifierKey);

							if (isDuplicatesFound == null) {
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalSetByHRToEmpScreen,
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empId);
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(empGoalList
												.get(0).getGsrGoalCreatedDate());
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails.setMenuUrl(hrCreGoalKey
										.split(",")[0]);
								notificationDetails.setIsAction(hrCreGoalKey
										.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(hrCreGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								String notificationMessage = MessageFormat
										.format(goalSetByHRToEmpScreen,
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (isDuplicatesFound
										.getNotificationcreatedDate().getTime() >= empGoalList
										.get(0).getGsrGoalCreatedDate()
										.getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(isDuplicatesFound
													.getNotificationcreatedDate());
								else
									isDuplicatesFound
											.setNotificationcreatedDate(empGoalList
													.get(0)
													.getGsrGoalCreatedDate());
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						} else {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_" + empId + "_"
									+ empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empId,
									fullmsgIdentifierKey);

							if (isDuplicatesFound == null) {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalCreatedDate)
										.max(Date::compareTo).get();
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalSetByHRToEmpScreenConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empId);
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(maxDate);
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails.setMenuUrl(hrCreGoalKey
										.split(",")[0]);
								notificationDetails.setIsAction(hrCreGoalKey
										.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(hrCreGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalCreatedDate)
										.max(Date::compareTo).get();
								String notificationMessage = MessageFormat
										.format(goalSetByHRToEmpScreenConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (maxDate.getTime() > isDuplicatesFound
										.getNotificationcreatedDate().getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(maxDate);
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						}
					}
				}
			} else {
				String menuURL = hrCreGoalKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									hrCreGoalKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
		LOG.endUsecase("Exiting goalSetByHRToEmpScreen() service");
	}

	/**
	 * @Description : This method is used to set Goal Set by BU Head
	 *              Notification to Employee Screen
	 * @param empId
	 * @throws DataAccessException
	 */
	public void goalSetByBUHeadToEmpScreen(int empId)
			throws DataAccessException {
		LOG.startUsecase("Entering goalSetByManagerToEmployeeScreenConsolidatedMsg() service");
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBOList = null;
		String buCreGoalKey = buCreGoalKeys;
		String msgIdentifierKey = buCreGoalKey.split(",")[2];
		DatNotificationsBO isDuplicatesFound = null;
		List<Integer> notificationIdList = null;
		Map<Short, List<GsrDatEmpGoalBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpGoalBO>>();
		int updateCount = 0;
		DatEmpProfessionalDetailBO profDetail = null;
		List<String> fullMsgIdentifierKeys = new ArrayList<String>();
		try {

			profDetail = professionalDetailsRepository
					.findByFkMainEmpDetailId(empId);

			gsrDatEmpGoalBOList = gsrEmployeegoalsRepository
					.findByGsrGoalCreatedByAndFkGsrDatEmpIdAndFkGsrGoalStatus(
							profDetail.getMasBuUnitBO().getFkBuHeadEmpId(),
							empId, GsrNotificationConstants.GOAL_SET);

			if (gsrDatEmpGoalBOList.size() != 0) {
				if (gsrDatEmpGoalBOList.size() == 1) {
					String fullmsgIdentifierKey = msgIdentifierKey + "_"
							+ empId + "_"
							+ gsrDatEmpGoalBOList.get(0).getFkGsrSlabId();
					fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
					isDuplicatesFound = checkDuplicates(empId,
							fullmsgIdentifierKey);

					if (isDuplicatesFound == null) {
						DatNotificationsBO notificationDetails = new DatNotificationsBO();
						String notificationMessage = MessageFormat.format(
								goalSetByBUHeadToEmpScreen, gsrDatEmpGoalBOList
										.get(0).getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						notificationDetails.setEmpId(empId);
						notificationDetails
								.setNoificationMessage(notificationMessage);
						notificationDetails
								.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
						notificationDetails
								.setNotificationcreatedDate(gsrDatEmpGoalBOList
										.get(0).getGsrGoalCreatedDate());
						notificationDetails
								.setClearFlagModifiedDate(new Date());
						notificationDetails.setClearFlagcreatedDate(new Date());
						notificationDetails
								.setModuleName(GsrNotificationConstants.MODULE_NAME);
						notificationDetails
								.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
						notificationDetails
								.setMenuUrl(buCreGoalKey.split(",")[0]);
						notificationDetails
								.setIsAction(buCreGoalKey.split(",")[1]);
						notificationDetails
								.setNoificationMessageIdentifier(fullmsgIdentifierKey);
						notificationDetails
								.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
						notificationDetails.setNotificationExpiryTime(Integer
								.parseInt(buCreGoalKey.split(",")[3]));
						try {
							notificationRepository.save(notificationDetails);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					} else {
						String notificationMessage = MessageFormat.format(
								goalSetByBUHeadToEmpScreen, gsrDatEmpGoalBOList
										.get(0).getGsrDatEmpGoalName(),
								gsrDatEmpGoalBOList.get(0)
										.getGsrSlabBO_slab_id()
										.getGsrSlabShortName());
						isDuplicatesFound
								.setNoificationMessage(notificationMessage);
						isDuplicatesFound
								.setNotificationModifiedDate(new Date());
						if (isDuplicatesFound.getNotificationcreatedDate()
								.getTime() >= gsrDatEmpGoalBOList.get(0)
								.getGsrGoalCreatedDate().getTime())
							isDuplicatesFound
									.setNotificationcreatedDate(isDuplicatesFound
											.getNotificationcreatedDate());
						else
							isDuplicatesFound
									.setNotificationcreatedDate(gsrDatEmpGoalBOList
											.get(0).getGsrGoalCreatedDate());
						if (isDuplicatesFound.getClearFlag().equals(
								NotificationSettingConstants.CLEAR_FLAG_YES))
							if (!isDuplicatesFound.getNoificationMessage()
									.trim().equals(notificationMessage.trim()))
								isDuplicatesFound
										.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
						try {
							notificationRepository.save(isDuplicatesFound);
						} catch (Exception e) {
							throw new DataAccessException("Failed to Save : ",
									e);
						}
					}
				} else {
					for (GsrDatEmpGoalBO empGoal : gsrDatEmpGoalBOList) {
						if (!empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap
									.put(empGoal.getFkGsrSlabId(), finalBean);
							continue;
						}
						if (empGoalsMap.containsKey(empGoal.getFkGsrSlabId())) {
							List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
							finalBean.add(empGoal);
							empGoalsMap.get(empGoal.getFkGsrSlabId()).addAll(
									finalBean);
						}
					}
					for (Map.Entry<Short, List<GsrDatEmpGoalBO>> entry : empGoalsMap
							.entrySet()) {
						List<GsrDatEmpGoalBO> empGoalList = (List<GsrDatEmpGoalBO>) entry
								.getValue();
						if (empGoalList.size() == 1) {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_" + empId + "_"
									+ empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empId,
									fullmsgIdentifierKey);

							if (isDuplicatesFound == null) {
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalSetByBUHeadToEmpScreen,
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empId);
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(empGoalList
												.get(0).getGsrGoalCreatedDate());
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails.setMenuUrl(buCreGoalKey
										.split(",")[0]);
								notificationDetails.setIsAction(buCreGoalKey
										.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(buCreGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								String notificationMessage = MessageFormat
										.format(goalSetByBUHeadToEmpScreen,
												empGoalList.get(0)
														.getGsrDatEmpGoalName(),
												empGoalList.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (isDuplicatesFound
										.getNotificationcreatedDate().getTime() >= empGoalList
										.get(0).getGsrGoalCreatedDate()
										.getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(isDuplicatesFound
													.getNotificationcreatedDate());
								else
									isDuplicatesFound
											.setNotificationcreatedDate(empGoalList
													.get(0)
													.getGsrGoalCreatedDate());
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						} else {
							String fullmsgIdentifierKey = msgIdentifierKey
									+ "_" + empId + "_"
									+ empGoalList.get(0).getFkGsrSlabId();
							fullMsgIdentifierKeys.add(fullmsgIdentifierKey);
							isDuplicatesFound = checkDuplicates(empId,
									fullmsgIdentifierKey);

							if (isDuplicatesFound == null) {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalCreatedDate)
										.max(Date::compareTo).get();
								DatNotificationsBO notificationDetails = new DatNotificationsBO();
								String notificationMessage = MessageFormat
										.format(goalSetByBUHeadToEmpScreenConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								notificationDetails.setEmpId(empId);
								notificationDetails
										.setNoificationMessage(notificationMessage);
								notificationDetails
										.setClearFlag(GsrNotificationConstants.CLEAR_FLAG_NO);
								notificationDetails
										.setNotificationcreatedDate(maxDate);
								notificationDetails
										.setClearFlagModifiedDate(new Date());
								notificationDetails
										.setClearFlagcreatedDate(new Date());
								notificationDetails
										.setModuleName(GsrNotificationConstants.MODULE_NAME);
								notificationDetails
										.setFkModuleId(GsrNotificationConstants.GSR_MENU_ID);
								notificationDetails.setMenuUrl(buCreGoalKey
										.split(",")[0]);
								notificationDetails.setIsAction(buCreGoalKey
										.split(",")[1]);
								notificationDetails
										.setNoificationMessageIdentifier(fullmsgIdentifierKey);
								notificationDetails
										.setNoificationIsExpiry(GsrNotificationConstants.IS_EXPIRY_NO);
								notificationDetails
										.setNotificationExpiryTime(Integer
												.parseInt(buCreGoalKey
														.split(",")[3]));
								try {
									notificationRepository
											.save(notificationDetails);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							} else {
								Date maxDate = empGoalList
										.stream()
										.map(GsrDatEmpGoalBO::getGsrGoalCreatedDate)
										.max(Date::compareTo).get();
								String notificationMessage = MessageFormat
										.format(goalSetByBUHeadToEmpScreenConsolidatedMsg,
												empGoalList.size(), empGoalList
														.get(0)
														.getGsrSlabBO_slab_id()
														.getGsrSlabShortName());
								isDuplicatesFound
										.setNoificationMessage(notificationMessage);
								isDuplicatesFound
										.setNotificationModifiedDate(new Date());
								if (maxDate.getTime() > isDuplicatesFound
										.getNotificationcreatedDate().getTime())
									isDuplicatesFound
											.setNotificationcreatedDate(maxDate);
								if (isDuplicatesFound
										.getClearFlag()
										.equals(NotificationSettingConstants.CLEAR_FLAG_YES))
									if (!isDuplicatesFound
											.getNoificationMessage().trim()
											.equals(notificationMessage.trim()))
										isDuplicatesFound
												.setClearFlag(NotificationSettingConstants.CLEAR_FLAG_NO);
								try {
									notificationRepository
											.save(isDuplicatesFound);
								} catch (Exception e) {
									throw new DataAccessException(
											"Failed to Save : ", e);
								}
							}
						}
					}
				}
			} else {
				String menuURL = buCreGoalKey.split(",")[0];
				try {
					notificationIdList = notificationRepository
							.getNotificationIdByEmpIdAndMenuURL(empId, menuURL);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while getting notification by EmpId and menu url from Notification Table.");
				}
				if (notificationIdList.size() != 0) {
					try {
						updateCount = notificationRepository
								.updateNotificationIsExpiryByNotificationIdList(notificationIdList);
					} catch (Exception e) {
						throw new DataAccessException(
								"Error Occurred while updating Expiry in Notification Table by notification id.");
					}
				}
			}
			if (fullMsgIdentifierKeys.size() > 0) {
				try {
					updateCount = notificationRepository
							.updateNotificationIsExpiryByNotificationIdentifierListAndEmpId(
									fullMsgIdentifierKeys, empId,
									buCreGoalKey.split(",")[0]);
				} catch (Exception e) {
					throw new DataAccessException(
							"Error Occurred while updating Expiry in Notification Table by message identifier keys.");
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"error in getting gsr module notifications ");
		}
		LOG.endUsecase("Exiting goalSetByManagerToEmployeeScreenConsolidatedMsg() service");
	}
}
