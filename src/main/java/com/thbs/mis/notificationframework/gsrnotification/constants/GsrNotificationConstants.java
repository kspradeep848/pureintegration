package com.thbs.mis.notificationframework.gsrnotification.constants;

public class GsrNotificationConstants {

	public static final String MODULE_NAME="GSR";
	public static final short GSR_MENU_ID=12;

	public static final String MENU_URL="gsr/12/dashboard/employee";

	public final static String  CLEAR_FLAG_NO="NO";
	

	public static final byte  WAITINING_FOR_MANAGER_APPROVAL=1;
	public static final byte  GOAL_SET=2;
	public static final byte  RATED=4;
	public static final byte  REJECTED=6;
	public static final byte  FORCE_CLOSE=7;

	
	public static final byte  FINAL_RATED=5;
	public static final byte  DIS_AGREE=3;

	public final static String  NOT_OVERRIDEN="NOT_OVERRIDEN";
	public final static String  OVERRIDEN="OVERRIDEN";
	
	public static final String IS_EXPIRY_NO = "NOTEXPIRED"; 
	
	public static final byte FINAL_RATING_RESOLVED = 4;


}
