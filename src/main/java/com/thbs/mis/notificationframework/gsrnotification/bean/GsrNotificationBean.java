package com.thbs.mis.notificationframework.gsrnotification.bean;

import java.util.Date;


public class GsrNotificationBean {

	private int empId;
	private String moduleName;
	private Date sDate;
	private Date eDate;

	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getModuleName() {
		return moduleName;
	}

	public Date getsDate() {
		return sDate;
	}
	public void setsDate(Date sDate) {
		this.sDate = sDate;
	}
	public Date geteDate() {
		return eDate;
	}
	public void seteDate(Date eDate) {
		this.eDate = eDate;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	@Override
	public String toString() {
		return "GsrNotificationBean [empId=" + empId + ", moduleName="
				+ moduleName + ", sDate=" + sDate + ", eDate=" + eDate + "]";
	}
	
	
}
