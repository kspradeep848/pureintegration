package com.thbs.mis.notificationframework.gsrnotification.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.gsrnotification.bean.GsrModuleNotificationBean;
import com.thbs.mis.notificationframework.gsrnotification.bean.GsrNotificationBean;
import com.thbs.mis.notificationframework.gsrnotification.constants.GsrNotificationConstantsURI;
import com.thbs.mis.notificationframework.gsrnotification.service.GSRNotificationService;

@Controller
public class GSRNotificationController {

	private static final AppLog LOG = LogFactory
			.getLog(GSRNotificationController.class);
	
	@Autowired
	private GSRNotificationService gsrNotificationService;
	
	
	/*Goal Set (By Employee)*/
	@RequestMapping(value = GsrNotificationConstantsURI.GSR_MODULE_SET_GOAL_NOTIFICATION_BY_EMPLOYEE_FOR_RM_SCREEN, method = RequestMethod.POST)
	public @ResponseBody String setGoalNotificationByEmployeeToRmScreen(@RequestBody  GsrNotificationBean gsrNotificationBean) throws BusinessException, ParseException
	{
		LOG.startUsecase("getGsrModuleNotifications");
		List<GsrModuleNotificationBean> gsrModuleNotificationsOutputBean=new ArrayList<GsrModuleNotificationBean>();
		String Sucess="";
		
	/*	
		Date edate=new Date();
Calendar calendar=Calendar.getInstance();
calendar.setTime(edate);
calendar.set(Calendar.SECOND,(calendar.get(Calendar.SECOND)-25));

System.out.println(calendar.getTime());	
		
		Date sDate=calendar.getTime();
		System.out.println("sDate"+sDate);
		System.out.println("eDate"+edate);*/

		try{
			//Sucess=gsrNotificationService.setGoalNotificationByEmployee(gsrNotificationBean.getEmpId(),sDate,edate);
		
			Sucess=gsrNotificationService.setGoalNotificationByEmployee(gsrNotificationBean.getEmpId());

		}
		catch(Exception e)
		{
			throw new BusinessException("Exception thrown from getGsrModuleNotifications controller");
		}
		Sucess="Success";
		LOG.endUsecase("getGsrModuleNotifications");
		return Sucess;

	}
	
}
