package com.thbs.mis.example.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.example.bo.EmployeeBO;
import com.thbs.mis.example.constants.EmpRestURIConstants;
import com.thbs.mis.example.service.EmployeeSrvc;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

/**
 * Handles requests for the Employee service.
 */
@Api(value = "Feed Back Related Operations", 
description = "Operations Related to the Employee", 
basePath = "http://localhost:8080/", consumes = "Rest Service", 
position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "Employee Related Services")

@Controller
public class EmployeeController
{

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(EmployeeController.class);

	@Autowired
	private EmployeeSrvc empSrvc;

	/*
	 * // Map to store employees, ideally we should use database
	 * Map<Long, EmployeeBO> empData = new HashMap<Long, EmployeeBO>();
	 * 
	 * @RequestMapping(value = EmpRestURIConstants.DUMMY_EMP, method =
	 * RequestMethod.GET) public @ResponseBody EmployeeBO
	 * getDummyEmployee() {
	 * 
	 * LOG.info("Start getDummyEmployee"); EmployeeBO emp = new
	 * EmployeeBO(); emp.setId(new Long(9999)); emp.setName("Dummy");
	 * emp.setCreatedDate(new Date()); empData.put(new Long(9999), emp);
	 * return emp; }
	 * 
	 * @RequestMapping(value = EmpRestURIConstants.GET_EMP, method =
	 * RequestMethod.GET) public @ResponseBody EmployeeBO getEmployee(
	 * 
	 * @PathVariable("id") int empId) {
	 * LOG.info("Start getEmployee. ID=" + empId);
	 * 
	 * return empData.get(empId); }
	 */

	/**
	 * 
	 * <Description getAllEmployees:> TODO
	 * 
	 * @return
	 * @throws BusinessException
	 */
	
	@ApiOperation(value = "Get all Employees."
			+ " .", httpMethod = "GET",

	notes = "This Service has been implemented to fetch all Employees."
			+ "<br>The Response will be the detailed of the Employee Details.",

	nickname = "Employee Details",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeeBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeBO.class),
			@ApiResponse(code = 201, message = "Employee details retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Employee details resource", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Employee details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = EmpRestURIConstants.GET_ALL_EMP, method = RequestMethod.GET)
	public @ResponseBody List<EmployeeBO> getAllEmployees()
			throws BusinessException
	{

		LOG.startUsecase("Get ALL Employee");
		List<EmployeeBO> emps;
		try
		{
			emps = empSrvc.getAllEmployees();
		} catch (DataAccessException e)
		{
			throw new BusinessException("Exception occured", e);
		}

		LOG.endUsecase("Get ALL Employee");
		return emps;
	}

   /**
    * 	
    * @param emp
    * @return EmployeeBO
    * @throws BusinessException
    */
	
	@ApiOperation(value = "Create a new Employee."
			+ "This service will be called from the front-end when the user want create an Employee.", httpMethod = "POST", notes = "This Service has been implemented to create an Employee."
			+ "<br>The Details will be stored in database."
			+ "<br>The Response will be the detailed of the Employee Details."
			+ "<br><b>Inputs :</b><br> Employee Details. <br><br>",

	nickname = "Employee Details",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeeBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Employee resource", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Employee resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = EmpRestURIConstants.CREATE_EMP, method = RequestMethod.POST)
	public @ResponseBody EmployeeBO createEmployee(
			@ApiParam(name = "Employee Details", example = "1", value = "Employee details.") 
			@RequestBody EmployeeBO emp) throws BusinessException
	{
		LOG.startUsecase("Create Employee");
		emp.setCreatedDate(new Date());

		boolean res;
		try
		{
			res = empSrvc.createEmployee(emp);

			LOG.debug("Employee Addition Success :" + res);
		} catch (DataAccessException e)
		{
			throw new BusinessException("Exception occured", e);
		}

		LOG.endUsecase("Create Employee");
		return emp;
	}

	/*
	 * @RequestMapping(value = EmpRestURIConstants.DELETE_EMP, method =
	 * RequestMethod.PUT) public @ResponseBody EmployeeBO
	 * deleteEmployee(
	 * 
	 * @PathVariable("id") int empId) {
	 * LOG.info("Start deleteEmployee."); EmployeeBO emp =
	 * empData.get(empId); empData.remove(empId); return emp; }
	 */

}
