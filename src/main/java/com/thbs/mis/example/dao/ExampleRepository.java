/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  ExampleRepository.java                                		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  16-Jun-2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.example.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.example.bo.EmployeeBO;

/**
 * <Description ExampleRepository:> TODO
 * @author THBS
 * @version 1.0
 * @see 
 */
@Repository
@Transactional(readOnly = true)
public interface ExampleRepository extends GenericRepository<EmployeeBO,Long>
{

}
