/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmployeeSrvc.java                                		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  16-Jun-2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbs.mis.example.bo.EmployeeBO;
import com.thbs.mis.example.dao.ExampleRepository;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.CollectionsUtil;

/**
 * <Description EmployeeSrvc:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
@Service
public class EmployeeSrvc
{
	@Autowired
	private ExampleRepository employeeRepos;

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(EmployeeSrvc.class);

	public EmployeeSrvc()
	{
		super();
	}

	/**
	 * 
	 * <Description createEmployee:> TODO
	 * 
	 * @param empBO
	 * @return
	 * @throws DataAccessException
	 */
	public boolean createEmployee(EmployeeBO empBO)
			throws DataAccessException
	{

		LOG.debug("About to save data");

		try
		{
			employeeRepos.save(empBO);
		} catch (Exception e)
		{
			throw new DataAccessException("Exception Occured : ", e);

		}

		return true;

	}

	public List<EmployeeBO> getAllEmployees() throws DataAccessException
	{
		List<EmployeeBO> empList = null;

		try
		{
			empList = CollectionsUtil
					.getIterableAsList(employeeRepos.findAll());

			
		} catch (Exception e)
		{
			throw new DataAccessException("Exception Occured : ", e);

		}

		return empList;
	}
}
