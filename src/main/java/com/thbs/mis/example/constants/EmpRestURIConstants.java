package com.thbs.mis.example.constants;

public class EmpRestURIConstants {

	public static final String DUMMY_EMP = "/rest/emp/dummy";
	public static final String GET_EMP = "/rest/emp/{id}";
	public static final String GET_ALL_EMP = "/rest/emps";
	public static final String CREATE_EMP = "/rest/emp/create";
	public static final String DELETE_EMP = "/rest/emp/delete/{id}";
	
	public static final String GET_ALL_EMPLOYEE = "/rest/employeeinfo";
	public static final String GET_ALL_REPORTEE = "/rest/reportee";
	public static final String EMP_LOGIN = "/rest/login";
	public static final String MAIL_SEND_FOR_GENERAL = "/rest/general/mail";
	public static final String TOKEN_REVOKE = "/tokens/revoke/{tokenId:.*}";
	
}