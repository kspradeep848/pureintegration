package com.thbs.mis.confirmation.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class ConfirmationUpdateHRFeedbackInputBean {

			
	@NumberFormat(style = Style.NUMBER, pattern = "empId should be numeric value")
	@NotNull(message = "empId can not be null")
	@Min(1)
	private Integer empId;
	
	@NumberFormat(style = Style.NUMBER, pattern = "pkFeedbackId should be numeric value")
	@NotNull(message = "pkFeedbackId can not be null")
	private Integer pkFeedbackId;
	
	@NotBlank(message = "workObjectives should not be blank.")
	@NotEmpty(message = "workObjectives should not be empty.")
	@NotNull(message = "workObjectives should not be null.")
	private String workObjectives;
	
	@NotBlank(message = "feedback should not be blank.")
	@NotEmpty(message = "feedback should not be empty.")
	@NotNull(message = "feedback should not be null.")
	private String feedback;
	
	@NotBlank(message = "areasOfImprovements should not be blank.")
	@NotEmpty(message = "areasOfImprovements should not be empty.")
	@NotNull(message = "areasOfImprovements should not be null.")
	private String areasOfImprovements;
	
	@NumberFormat(style = Style.NUMBER, pattern = "attitude should be numeric value")
	@NotNull(message = "attitude can not be null")
	@Min(1)
	private Integer attitude;
	
	@NumberFormat(style = Style.NUMBER, pattern = "communication should be numeric value")
	@NotNull(message = "communication can not be null")
	@Min(1)
	private Integer communication;
	
	@NumberFormat(style = Style.NUMBER, pattern = "willingnessToLearn should be numeric value")
	@NotNull(message = "willingnessToLearn can not be null")
	@Min(1)
	private Integer willingnessToLearn;
	
	@NumberFormat(style = Style.NUMBER, pattern = "selfConfidence should be numeric value")
	@NotNull(message = "selfConfidence can not be null")
	@Min(1)
	private Integer selfConfidence;
	
	@NumberFormat(style = Style.NUMBER, pattern = "flexibility should be numeric value")
	@NotNull(message = "flexibility can not be null")
	@Min(1)
	private Integer flexibility;
	
	@NumberFormat(style = Style.NUMBER, pattern = "proactiveness should be numeric value")
	@NotNull(message = "proactiveness can not be null")
	@Min(1)
	private Integer proactiveness;
	
	@NumberFormat(style = Style.NUMBER, pattern = "technicalExpertise should be numeric value")
	@NotNull(message = "technicalExpertise can not be null")
	@Min(1)
	private Integer technicalExpertise;
	
	@NumberFormat(style = Style.NUMBER, pattern = "dependability should be numeric value")
	@NotNull(message = "dependability can not be null")
	@Min(1)
	private Integer dependability;
	
	@NumberFormat(style = Style.NUMBER, pattern = "loggedInUser should be numeric value")
	@NotNull(message = "loggedInUser can not be null")
	@Min(1)
	private Integer loggedInUser;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Integer getPkFeedbackId() {
		return pkFeedbackId;
	}

	public void setPkFeedbackId(Integer pkFeedbackId) {
		this.pkFeedbackId = pkFeedbackId;
	}

	public String getWorkObjectives() {
		return workObjectives;
	}

	public void setWorkObjectives(String workObjectives) {
		this.workObjectives = workObjectives;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getAreasOfImprovements() {
		return areasOfImprovements;
	}

	public void setAreasOfImprovements(String areasOfImprovements) {
		this.areasOfImprovements = areasOfImprovements;
	}

	public Integer getAttitude() {
		return attitude;
	}

	public void setAttitude(Integer attitude) {
		this.attitude = attitude;
	}

	public Integer getCommunication() {
		return communication;
	}

	public void setCommunication(Integer communication) {
		this.communication = communication;
	}

	public Integer getWillingnessToLearn() {
		return willingnessToLearn;
	}

	public void setWillingnessToLearn(Integer willingnessToLearn) {
		this.willingnessToLearn = willingnessToLearn;
	}

	public Integer getSelfConfidence() {
		return selfConfidence;
	}

	public void setSelfConfidence(Integer selfConfidence) {
		this.selfConfidence = selfConfidence;
	}

	public Integer getFlexibility() {
		return flexibility;
	}

	public void setFlexibility(Integer flexibility) {
		this.flexibility = flexibility;
	}

	public Integer getProactiveness() {
		return proactiveness;
	}

	public void setProactiveness(Integer proactiveness) {
		this.proactiveness = proactiveness;
	}

	public Integer getTechnicalExpertise() {
		return technicalExpertise;
	}

	public void setTechnicalExpertise(Integer technicalExpertise) {
		this.technicalExpertise = technicalExpertise;
	}

	public Integer getDependability() {
		return dependability;
	}

	public void setDependability(Integer dependability) {
		this.dependability = dependability;
	}

	public Integer getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Integer loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	@Override
	public String toString() {
		return "ConfirmationUpdateHRFeedbackInputBean [empId=" + empId
				+ ", pkFeedbackId=" + pkFeedbackId + ", workObjectives="
				+ workObjectives + ", feedback=" + feedback
				+ ", areasOfImprovements=" + areasOfImprovements
				+ ", attitude=" + attitude + ", communication=" + communication
				+ ", willingnessToLearn=" + willingnessToLearn
				+ ", selfConfidence=" + selfConfidence + ", flexibility="
				+ flexibility + ", proactiveness=" + proactiveness
				+ ", technicalExpertise=" + technicalExpertise
				+ ", dependability=" + dependability + ", loggedInUser="
				+ loggedInUser + "]";
	}

	
	
}
