package com.thbs.mis.confirmation.bean;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class GenerateConfExtLetterOutputBean {

	
	private String filePath;
		
	private String fileName;

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String toString() {
		return "GenerateConfExtLetterOutputBean [filePath=" + filePath + ", fileName=" + fileName + "]";
	}


	
	
}
