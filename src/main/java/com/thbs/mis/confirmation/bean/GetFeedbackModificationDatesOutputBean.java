package com.thbs.mis.confirmation.bean;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class GetFeedbackModificationDatesOutputBean {

	
	private Integer pkHistoryId;
	
	@JsonSerialize(using=CustomTimeSerializer.class)
	private Date updatedOn;
	
	public Integer getPkHistoryId() {
		return pkHistoryId;
	}
	public void setPkHistoryId(Integer pkHistoryId) {
		this.pkHistoryId = pkHistoryId;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Override
	public String toString() {
		return "GetFeedbackModificationDatesOutputBean [pkHistoryId=" + pkHistoryId + ", updatedOn=" + updatedOn + "]";
	}
	
	
}
