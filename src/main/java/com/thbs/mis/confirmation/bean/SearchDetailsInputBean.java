package com.thbs.mis.confirmation.bean;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;
public class SearchDetailsInputBean {
	
	private Integer empId;
	
	private Integer mgrId;
	
	
	private List<Integer> confStatusIdList;
	
	private Short buUnitId;
	
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date confStartDate;
	
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date confEndDate;
	
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public Integer getMgrId() {
		return mgrId;
	}
	public void setMgrId(Integer mgrId) {
		this.mgrId = mgrId;
	}
	
	public List<Integer> getConfStatusIdList() {
		return confStatusIdList;
	}
	public void setConfStatusIdList(List<Integer> confStatusIdList) {
		this.confStatusIdList = confStatusIdList;
	}
	public Short getBuUnitId() {
		return buUnitId;
	}
	public void setBuUnitId(Short buUnitId) {
		this.buUnitId = buUnitId;
	}
	public Date getConfStartDate() {
		return confStartDate;
	}
	public void setConfStartDate(Date confStartDate) {
		this.confStartDate = confStartDate;
	}
	public Date getConfEndDate() {
		return confEndDate;
	}
	public void setConfEndDate(Date confEndDate) {
		this.confEndDate = confEndDate;
	}
	@Override
	public String toString() {
		return "SearchDetailsInputBean [empId=" + empId + ", mgrId=" + mgrId
				+ ", confStatusIdList=" + confStatusIdList + ", buUnitId="
				+ buUnitId + ", confStartDate=" + confStartDate
				+ ", confEndDate=" + confEndDate + "]";
	}
	
}