package com.thbs.mis.confirmation.bean;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class ConfirmStatusByHrInputBean {

	@NumberFormat(style = Style.NUMBER, pattern = "fkEmpId should be numeric value")
	@NotNull(message = "fkEmpId can not be null")
	@Min(1)
	private Integer fkEmpId;
	
	
	@NumberFormat(style = Style.NUMBER, pattern = "fkEmpLevel should be numeric value")
	@NotNull(message = "fkEmpLevel can not be null")
	@Min(1)
	private Short fkEmpLevel;
	
	@NumberFormat(style = Style.NUMBER, pattern = "fkEmpDesignation should be numeric value")
	@NotNull(message = "fkEmpDesignation can not be null")
	@Min(1)
	private Short fkEmpDesignation;
	
	@NumberFormat(style = Style.NUMBER, pattern = "loggedInUser should be numeric value")
	@NotNull(message = "loggedInUser can not be null")
	@Min(1)
	private Integer loggedInUser;
	
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date actualDateOFConf;
	
	
	public Date getActualDateOFConf() {
		return actualDateOFConf;
	}
	public void setActualDateOFConf(Date actualDateOFConf) {
		this.actualDateOFConf = actualDateOFConf;
	}
	public Integer getLoggedInUser() {
		return loggedInUser;
	}
	public void setLoggedInUser(Integer loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	public Integer getFkEmpId() {
		return fkEmpId;
	}
	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}
	
	public Short getFkEmpLevel() {
		return fkEmpLevel;
	}
	public void setFkEmpLevel(Short fkEmpLevel) {
		this.fkEmpLevel = fkEmpLevel;
	}
	public Short getFkEmpDesignation() {
		return fkEmpDesignation;
	}
	public void setFkEmpDesignation(Short fkEmpDesignation) {
		this.fkEmpDesignation = fkEmpDesignation;
	}
	@Override
	public String toString() {
		return "ConfirmStatusByHrInputBean [fkEmpId=" + fkEmpId
				+ ", fkEmpLevel=" + fkEmpLevel + ", fkEmpDesignation="
				+ fkEmpDesignation + ", loggedInUser=" + loggedInUser
				+ ", actualDateOFConf=" + actualDateOFConf + "]";
	}
	
	
}
