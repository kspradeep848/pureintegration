package com.thbs.mis.confirmation.bean;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class NotConfirmedByHrInputBean {

	@NumberFormat(style = Style.NUMBER, pattern = "fkEmpId should be numeric value")
	@NotNull(message = "fkEmpId can not be null")
	@Min(1)
	private Integer empId;

	@NumberFormat(style = Style.NUMBER, pattern = "loggedInUser should be numeric value")
	@NotNull(message = "loggedInUser can not be null")
	@Min(1)
	private Integer loggedInUserId;

	private String hrReason;

	public String getHrReason() {
		return hrReason;
	}

	public void setHrReason(String hrReason) {
		this.hrReason = hrReason;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Integer getLoggedInUserId() {
		return loggedInUserId;
	}

	public void setLoggedInUserId(Integer loggedInUserId) {
		this.loggedInUserId = loggedInUserId;
	}

	@Override
	public String toString() {
		return "NotConfirmedByHrInputBean [empId=" + empId
				+ ", loggedInUserId=" + loggedInUserId + ", hrReason="
				+ hrReason + "]";
	}

}
