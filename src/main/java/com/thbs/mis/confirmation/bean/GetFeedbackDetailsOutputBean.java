package com.thbs.mis.confirmation.bean;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class GetFeedbackDetailsOutputBean {

	private Integer pkFeedbackId;	
	private String workObjectives;
	private String feedback;
	private String areasOfImprovements;
	private Integer attitude;
	private Integer communication;
	private Integer willingnessToLearn;
	private Integer selfConfidence;
	private Integer flexibility;
	private Integer proactiveness;
	private Integer technicalExpertise;
	private Integer dependability;
	private Integer UpdatedBy;
	public Integer getPkFeedbackId() {
		return pkFeedbackId;
	}
	public void setPkFeedbackId(Integer pkFeedbackId) {
		this.pkFeedbackId = pkFeedbackId;
	}
	public String getWorkObjectives() {
		return workObjectives;
	}
	public void setWorkObjectives(String workObjectives) {
		this.workObjectives = workObjectives;
	}
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	public String getAreasOfImprovements() {
		return areasOfImprovements;
	}
	public void setAreasOfImprovements(String areasOfImprovements) {
		this.areasOfImprovements = areasOfImprovements;
	}
	public Integer getAttitude() {
		return attitude;
	}
	public void setAttitude(Integer attitude) {
		this.attitude = attitude;
	}
	public Integer getCommunication() {
		return communication;
	}
	public void setCommunication(Integer communication) {
		this.communication = communication;
	}
	public Integer getWillingnessToLearn() {
		return willingnessToLearn;
	}
	public void setWillingnessToLearn(Integer willingnessToLearn) {
		this.willingnessToLearn = willingnessToLearn;
	}
	public Integer getSelfConfidence() {
		return selfConfidence;
	}
	public void setSelfConfidence(Integer selfConfidence) {
		this.selfConfidence = selfConfidence;
	}
	public Integer getFlexibility() {
		return flexibility;
	}
	public void setFlexibility(Integer flexibility) {
		this.flexibility = flexibility;
	}
	public Integer getProactiveness() {
		return proactiveness;
	}
	public void setProactiveness(Integer proactiveness) {
		this.proactiveness = proactiveness;
	}
	public Integer getTechnicalExpertise() {
		return technicalExpertise;
	}
	public void setTechnicalExpertise(Integer technicalExpertise) {
		this.technicalExpertise = technicalExpertise;
	}
	public Integer getDependability() {
		return dependability;
	}
	public void setDependability(Integer dependability) {
		this.dependability = dependability;
	}
	public Integer getUpdatedBy() {
		return UpdatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		UpdatedBy = updatedBy;
	}
	@Override
	public String toString() {
		return "GetFeedbackDetailsOutputBean [pkFeedbackId=" + pkFeedbackId
				+ ", workObjectives=" + workObjectives + ", feedback="
				+ feedback + ", areasOfImprovements=" + areasOfImprovements
				+ ", attitude=" + attitude + ", communication=" + communication
				+ ", willingnessToLearn=" + willingnessToLearn
				+ ", selfConfidence=" + selfConfidence + ", flexibility="
				+ flexibility + ", proactiveness=" + proactiveness
				+ ", technicalExpertise=" + technicalExpertise
				+ ", dependability=" + dependability + ", UpdatedBy="
				+ UpdatedBy + "]";
	}
	
	
	
			
}
