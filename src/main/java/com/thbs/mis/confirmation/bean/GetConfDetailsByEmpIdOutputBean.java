package com.thbs.mis.confirmation.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class GetConfDetailsByEmpIdOutputBean {

	private String fkEmpIdWithName;
	private Integer fkConfStatusId;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date dateOfConfirmation;
	private String statusDesc;
	
	public String getFkEmpIdWithName() {
		return fkEmpIdWithName;
	}


	public void setFkEmpIdWithName(String fkEmpIdWithName) {
		this.fkEmpIdWithName = fkEmpIdWithName;
	}


	public Integer getFkConfStatusId() {
		return fkConfStatusId;
	}


	public void setFkConfStatusId(Integer fkConfStatusId) {
		this.fkConfStatusId = fkConfStatusId;
	}


	public Date getDateOfConfirmation() {
		return dateOfConfirmation;
	}


	public void setDateOfConfirmation(Date dateOfConfirmation) {
		this.dateOfConfirmation = dateOfConfirmation;
	}


	public String getStatusDesc() {
		return statusDesc;
	}


	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}


	@Override
	public String toString() {
		return "GetConfDetailsByEmpIdOutputBean [fkEmpIdWithName="
				+ fkEmpIdWithName + ", fkConfStatusId=" + fkConfStatusId
				+ ", dateOfConfirmation=" + dateOfConfirmation
				+ ", statusDesc=" + statusDesc + "]";
	}
	
}
