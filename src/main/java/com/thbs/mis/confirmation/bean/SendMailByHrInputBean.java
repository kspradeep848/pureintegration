package com.thbs.mis.confirmation.bean;

public class SendMailByHrInputBean {

	private Integer empId;
	
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	@Override
	public String toString() {
		return "SendMailByHrInputBean [empId=" + empId + "]";
	}
	
	
}
