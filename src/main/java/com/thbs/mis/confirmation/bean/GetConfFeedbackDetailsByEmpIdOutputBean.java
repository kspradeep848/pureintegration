package com.thbs.mis.confirmation.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

import com.thbs.mis.confirmation.bean.GetFeedbackDetailsOutputBean;


public class GetConfFeedbackDetailsByEmpIdOutputBean {

	 private Integer fkEmpId;
	 private String empName;
	 private Integer fkMgrId;
	 private String reportingManagerName;
	 private Integer delegatedByHrId;
	 private String delegatedByHrName;
	 private Integer delegatedToMgrId;
	 private String delegatedManagerName;
	 private String delegatedComments;
	 @JsonSerialize(using=CustomTimeSerializer.class)
	 private Date delegatedDate;
	 private Short empLevel;
	 private String empLevelName;
	 private Short designation;
	 private String designationName;
	 @JsonSerialize(using=CustomTimeSerializer.class)
	 private Date DateOfJoining;
	 @JsonSerialize(using=CustomTimeSerializer.class)
	 private Date tentativeConfirmationDate;
	 @JsonSerialize(using=CustomTimeSerializer.class)
	 private Date actualDateOfConfirmation;
	 private Integer confirmationStatus;
	 private Integer confExtendedByHrId;
	 private String confExtendedByHrName;
	 private Integer confExtendedInMonths;
	 private String reasonForExtension;
	 private String isConfirmedByHr;
	 private Integer confirmedByHrId;
	 private String confirmedByHrName;
	 private String confirmationLetterSent;
	 private String extensionLetterSent;
	 private String isConfirmedByMgr;
	 private Integer confirmedByMgrId;
	 private String confirmedByMgrName;
	 private String mgrDenialReason;
	 private Integer updatedBy;
	 @JsonSerialize(using=CustomTimeSerializer.class)
	 private Date updatedOn;
	 
	 private GetFeedbackDetailsOutputBean managerFeedback;
	 
	public Integer getFkEmpId() {
		return fkEmpId;
	}
	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public Integer getFkMgrId() {
		return fkMgrId;
	}
	public void setFkMgrId(Integer fkMgrId) {
		this.fkMgrId = fkMgrId;
	}
	public String getReportingManagerName() {
		return reportingManagerName;
	}
	public void setReportingManagerName(String reportingManagerName) {
		this.reportingManagerName = reportingManagerName;
	}
	public Integer getDelegatedByHrId() {
		return delegatedByHrId;
	}
	public void setDelegatedByHrId(Integer delegatedByHrId) {
		this.delegatedByHrId = delegatedByHrId;
	}
	public String getDelegatedByHrName() {
		return delegatedByHrName;
	}
	public void setDelegatedByHrName(String delegatedByHrName) {
		this.delegatedByHrName = delegatedByHrName;
	}
	public Integer getDelegatedToMgrId() {
		return delegatedToMgrId;
	}
	public void setDelegatedToMgrId(Integer delegatedToMgrId) {
		this.delegatedToMgrId = delegatedToMgrId;
	}
	public String getDelegatedManagerName() {
		return delegatedManagerName;
	}
	public void setDelegatedManagerName(String delegatedManagerName) {
		this.delegatedManagerName = delegatedManagerName;
	}
	public String getDelegatedComments() {
		return delegatedComments;
	}
	public void setDelegatedComments(String delegatedComments) {
		this.delegatedComments = delegatedComments;
	}
	public Date getDelegatedDate() {
		return delegatedDate;
	}
	public void setDelegatedDate(Date delegatedDate) {
		this.delegatedDate = delegatedDate;
	}
	public Short getEmpLevel() {
		return empLevel;
	}
	public void setEmpLevel(Short empLevel) {
		this.empLevel = empLevel;
	}
	public String getEmpLevelName() {
		return empLevelName;
	}
	public void setEmpLevelName(String empLevelName) {
		this.empLevelName = empLevelName;
	}
	public Short getDesignation() {
		return designation;
	}
	public void setDesignation(Short designation) {
		this.designation = designation;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public Date getDateOfJoining() {
		return DateOfJoining;
	}
	public void setDateOfJoining(Date DateOfJoining) {
		this.DateOfJoining = DateOfJoining;
	}
	public Date getTentativeConfirmationDate() {
		return tentativeConfirmationDate;
	}
	public void setTentativeConfirmationDate(Date tentativeConfirmationDate) {
		this.tentativeConfirmationDate = tentativeConfirmationDate;
	}
	public Date getActualDateOfConfirmation() {
		return actualDateOfConfirmation;
	}
	public void setActualDateOfConfirmation(Date actualDateOfConfirmation) {
		this.actualDateOfConfirmation = actualDateOfConfirmation;
	}
	public Integer getConfirmationStatus() {
		return confirmationStatus;
	}
	public void setConfirmationStatus(Integer confirmationStatus) {
		this.confirmationStatus = confirmationStatus;
	}
	public Integer getConfExtendedByHrId() {
		return confExtendedByHrId;
	}
	public void setConfExtendedByHrId(Integer confExtendedByHrId) {
		this.confExtendedByHrId = confExtendedByHrId;
	}
	public String getConfExtendedByHrName() {
		return confExtendedByHrName;
	}
	public void setConfExtendedByHrName(String confExtendedByHrName) {
		this.confExtendedByHrName = confExtendedByHrName;
	}
	public Integer getConfExtendedInMonths() {
		return confExtendedInMonths;
	}
	public void setConfExtendedInMonths(Integer confExtendedInMonths) {
		this.confExtendedInMonths = confExtendedInMonths;
	}
	public String getReasonForExtension() {
		return reasonForExtension;
	}
	public void setReasonForExtension(String reasonForExtension) {
		this.reasonForExtension = reasonForExtension;
	}
	public String getIsConfirmedByHr() {
		return isConfirmedByHr;
	}
	public void setIsConfirmedByHr(String isConfirmedByHr) {
		this.isConfirmedByHr = isConfirmedByHr;
	}
	public Integer getConfirmedByHrId() {
		return confirmedByHrId;
	}
	public void setConfirmedByHrId(Integer confirmedByHrId) {
		this.confirmedByHrId = confirmedByHrId;
	}
	public String getConfirmedByHrName() {
		return confirmedByHrName;
	}
	public void setConfirmedByHrName(String confirmedByHrName) {
		this.confirmedByHrName = confirmedByHrName;
	}
	public String getConfirmationLetterSent() {
		return confirmationLetterSent;
	}
	public void setConfirmationLetterSent(String confirmationLetterSent) {
		this.confirmationLetterSent = confirmationLetterSent;
	}
	public String getExtensionLetterSent() {
		return extensionLetterSent;
	}
	public void setExtensionLetterSent(String extensionLetterSent) {
		this.extensionLetterSent = extensionLetterSent;
	}
	public String getIsConfirmedByMgr() {
		return isConfirmedByMgr;
	}
	public void setIsConfirmedByMgr(String isConfirmedByMgr) {
		this.isConfirmedByMgr = isConfirmedByMgr;
	}
	public Integer getConfirmedByMgrId() {
		return confirmedByMgrId;
	}
	public void setConfirmedByMgrId(Integer confirmedByMgrId) {
		this.confirmedByMgrId = confirmedByMgrId;
	}
	public String getConfirmedByMgrName() {
		return confirmedByMgrName;
	}
	public void setConfirmedByMgrName(String confirmedByMgrName) {
		this.confirmedByMgrName = confirmedByMgrName;
	}
	public String getMgrDenialReason() {
		return mgrDenialReason;
	}
	public void setMgrDenialReason(String mgrDenialReason) {
		this.mgrDenialReason = mgrDenialReason;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	
	public GetFeedbackDetailsOutputBean getManagerFeedback() {
		return managerFeedback;
	}
	public void setManagerFeedback(GetFeedbackDetailsOutputBean managerFeedback) {
		this.managerFeedback = managerFeedback;
	}
	
	@Override
	public String toString() {
		return "GetConfFeedbackDetailsByEmpIdOutputBean [fkEmpId=" + fkEmpId + ", empName=" + empName + ", fkMgrId="
				+ fkMgrId + ", reportingManagerName=" + reportingManagerName + ", delegatedByHrId=" + delegatedByHrId
				+ ", delegatedByHrName=" + delegatedByHrName + ", delegatedToMgrId=" + delegatedToMgrId
				+ ", delegatedManagerName=" + delegatedManagerName + ", delegatedComments=" + delegatedComments
				+ ", delegatedDate=" + delegatedDate + ", empLevel=" + empLevel + ", empLevelName=" + empLevelName
				+ ", designation=" + designation + ", designationName=" + designationName + ", DateOfJoining="
				+ DateOfJoining + ", tentativeConfirmationDate=" + tentativeConfirmationDate
				+ ", actualDateOfConfirmation=" + actualDateOfConfirmation + ", confirmationStatus="
				+ confirmationStatus + ", confExtendedByHrId=" + confExtendedByHrId + ", confExtendedByHrName="
				+ confExtendedByHrName + ", confExtendedInMonths=" + confExtendedInMonths + ", reasonForExtension="
				+ reasonForExtension + ", isConfirmedByHr=" + isConfirmedByHr + ", confirmedByHrId=" + confirmedByHrId
				+ ", confirmedByHrName=" + confirmedByHrName + ", confirmationLetterSent=" + confirmationLetterSent
				+ ", extensionLetterSent=" + extensionLetterSent + ", isConfirmedByMgr=" + isConfirmedByMgr
				+ ", confirmedByMgrId=" + confirmedByMgrId + ", confirmedByMgrName=" + confirmedByMgrName
				+ ", mgrDenialReason=" + mgrDenialReason + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn
				+ ", managerFeedback=" + managerFeedback + "]";
	}

	
	
}
