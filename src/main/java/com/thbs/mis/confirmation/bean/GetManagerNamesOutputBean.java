package com.thbs.mis.confirmation.bean;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class GetManagerNamesOutputBean {

	
	private String mgrName;
	
	
	private Integer mgrId;


	public String getMgrName() {
		return mgrName;
	}


	public void setMgrName(String mgrName) {
		this.mgrName = mgrName;
	}


	public Integer getMgrId() {
		return mgrId;
	}


	public void setMgrId(Integer mgrId) {
		this.mgrId = mgrId;
	}


	@Override
	public String toString() {
		return "GetManagerNamesOutputBean [mgrName=" + mgrName + ", mgrId=" + mgrId + "]";
	}

	
	
	
}
