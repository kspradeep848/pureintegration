package com.thbs.mis.confirmation.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class ExtendProbationByHrInputBean {

	
	@NumberFormat(style = Style.NUMBER, pattern = "confExtendedInMonths should be numeric value")
	@NotNull(message = "confExtendedInMonths can not be null")
	@Min(1)
	private Integer confExtendedInMonths;
	
	@NotBlank(message = "reasonForExtension should not be blank.")
	@NotEmpty(message = "reasonForExtension should not be empty.")
	@NotNull(message = "reasonForExtension should not be null.")
	private String reasonForExtension;
	
	@NumberFormat(style = Style.NUMBER, pattern = "confExtendedByHrId should be numeric value")
	@NotNull(message = "confExtendedByHrId can not be null")
	@Min(1)
	private Integer confExtendedByHrId;
	
	@NumberFormat(style = Style.NUMBER, pattern = "fkEmpId should be numeric value")
	@NotNull(message = "fkEmpId can not be null")
	@Min(1)
	private Integer fkEmpId;
	
	public Integer getConfExtendedInMonths() {
		return confExtendedInMonths;
	}
	public void setConfExtendedInMonths(Integer confExtendedInMonths) {
		this.confExtendedInMonths = confExtendedInMonths;
	}
	public String getReasonForExtension() {
		return reasonForExtension;
	}
	public void setReasonForExtension(String reasonForExtension) {
		this.reasonForExtension = reasonForExtension;
	}
	public Integer getConfExtendedByHrId() {
		return confExtendedByHrId;
	}
	public void setConfExtendedByHrId(Integer confExtendedByHrId) {
		this.confExtendedByHrId = confExtendedByHrId;
	}
	public Integer getFkEmpId() {
		return fkEmpId;
	}
	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}
	@Override
	public String toString() {
		return "ExtendProbationByHrInputBean [confExtendedInMonths="
				+ confExtendedInMonths + ", reasonForExtension="
				+ reasonForExtension + ", confExtendedByHrId="
				+ confExtendedByHrId + ", fkEmpId=" + fkEmpId + "]";
	}
	
}
