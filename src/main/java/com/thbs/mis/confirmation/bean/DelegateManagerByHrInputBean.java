package com.thbs.mis.confirmation.bean;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class DelegateManagerByHrInputBean {

	@NumberFormat(style = Style.NUMBER, pattern = "fkEmpId should be numeric value")
	@NotNull(message = "fkEmpId can not be null")
	@Min(1)
	private Integer fkEmpId;
	
	@NotBlank(message = "delegatedComments should not be blank.")
	@NotEmpty(message = "delegatedComments should not be empty.")
	@NotNull(message = "delegatedComments should not be null.")
	private String delegatedComments;
	
	@NumberFormat(style = Style.NUMBER, pattern = "delegatedByHrId should be numeric value")
	@NotNull(message = "delegatedByHrId can not be null")
	@Min(1)
	private Integer delegatedByHrId;
	
	@NumberFormat(style = Style.NUMBER, pattern = "delegatedToMgrId should be numeric value")
	@NotNull(message = "delegatedToMgrId can not be null")
	@Min(1)
	private Integer delegatedToMgrId;
	
	
	public Integer getFkEmpId() {
		return fkEmpId;
	}
	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}
	public String getDelegatedComments() {
		return delegatedComments;
	}
	public void setDelegatedComments(String delegatedComments) {
		this.delegatedComments = delegatedComments;
	}
	public Integer getDelegatedByHrId() {
		return delegatedByHrId;
	}
	public void setDelegatedByHrId(Integer delegatedByHrId) {
		this.delegatedByHrId = delegatedByHrId;
	}
	public Integer getDelegatedToMgrId() {
		return delegatedToMgrId;
	}
	public void setDelegatedToMgrId(Integer delegatedToMgrId) {
		this.delegatedToMgrId = delegatedToMgrId;
	}
	@Override
	public String toString() {
		return "DelegateManagerByHrInputBean [fkEmpId=" + fkEmpId
				+ ", delegatedComments=" + delegatedComments
				+ ", delegatedByHrId=" + delegatedByHrId
				+ ", delegatedToMgrId=" + delegatedToMgrId + "]";
	}
	
	
}
