package com.thbs.mis.confirmation.bean;

public class GenerateExtensionLetterBean {

	
	private String empName;
	private String empFname;
	private Integer empId;
	private String desg;
	private String level;
	private String extMonths;
	private String finalDate;
	private String curDate;
	private String country;
	private String footer1;
	private String footer2;
	private String logo;
	
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpFname() {
		return empFname;
	}
	public void setEmpFname(String empFname) {
		this.empFname = empFname;
	}
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getDesg() {
		return desg;
	}
	public void setDesg(String desg) {
		this.desg = desg;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getExtMonths() {
		return extMonths;
	}
	public void setExtMonths(String extMonths) {
		this.extMonths = extMonths;
	}
	public String getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
	public String getCurDate() {
		return curDate;
	}
	public void setCurDate(String curDate) {
		this.curDate = curDate;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getFooter1() {
		return footer1;
	}
	public void setFooter1(String footer1) {
		this.footer1 = footer1;
	}
	public String getFooter2() {
		return footer2;
	}
	public void setFooter2(String footer2) {
		this.footer2 = footer2;
	}
	
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	@Override
	public String toString() {
		return "GenerateExtensionLetterBean [empName=" + empName + ", empFname=" + empFname + ", empId=" + empId
				+ ", desg=" + desg + ", level=" + level + ", extMonths=" + extMonths + ", finalDate=" + finalDate
				+ ", curDate=" + curDate + ", country=" + country + ", footer1=" + footer1 + ", footer2=" + footer2
				+ ", logo=" + logo + "]";
	}
	
	
}
