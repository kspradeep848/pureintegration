package com.thbs.mis.confirmation.bean;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;
public class SearchDetailsOutputBean {
	
		private Integer empId;
		private String empName;
		private Integer mgrId;
		private String mgrName;
		private Integer pkFeedbackId;
		private Integer pkConfId;
		private Integer pkEmpLevelId;
		private String empLevelName;
		private Short pkEmpDesignationId;
		private String empDesignationName;
		@JsonSerialize(using=CustomTimeSerializer.class)
		private Date empDateOfJoining;
		@JsonSerialize(using=CustomTimeSerializer.class)
		private Date empDateOfConfirmation;
		private Integer fkConfStatusId;
		private Short buUnitId;
		private Integer delegatedToMgrId;
		private String delegatedMgrName;
		
		
		public Integer getEmpId() {
			return empId;
		}
		public void setEmpId(Integer empId) {
			this.empId = empId;
		}
		public String getEmpName() {
			return empName;
		}
		public void setEmpName(String empName) {
			this.empName = empName;
		}
		public Integer getMgrId() {
			return mgrId;
		}
		public void setMgrId(Integer mgrId) {
			this.mgrId = mgrId;
		}
		public String getMgrName() {
			return mgrName;
		}
		public void setMgrName(String mgrName) {
			this.mgrName = mgrName;
		}
		public Integer getPkFeedbackId() {
			return pkFeedbackId;
		}
		public void setPkFeedbackId(Integer pkFeedbackId) {
			this.pkFeedbackId = pkFeedbackId;
		}
		public Integer getPkConfId() {
			return pkConfId;
		}
		public void setPkConfId(Integer pkConfId) {
			this.pkConfId = pkConfId;
		}
		public Integer getPkEmpLevelId() {
			return pkEmpLevelId;
		}
		public void setPkEmpLevelId(Integer pkEmpLevelId) {
			this.pkEmpLevelId = pkEmpLevelId;
		}
		public String getEmpLevelName() {
			return empLevelName;
		}
		public void setEmpLevelName(String empLevelName) {
			this.empLevelName = empLevelName;
		}
		public Short getPkEmpDesignationId() {
			return pkEmpDesignationId;
		}
		public void setPkEmpDesignationId(Short pkEmpDesignationId) {
			this.pkEmpDesignationId = pkEmpDesignationId;
		}
		public String getEmpDesignationName() {
			return empDesignationName;
		}
		public void setEmpDesignationName(String empDesignationName) {
			this.empDesignationName = empDesignationName;
		}
		public Date getEmpDateOfJoining() {
			return empDateOfJoining;
		}
		public void setEmpDateOfJoining(Date empDateOfJoining) {
			this.empDateOfJoining = empDateOfJoining;
		}
		public Date getEmpDateOfConfirmation() {
			return empDateOfConfirmation;
		}
		public void setEmpDateOfConfirmation(Date empDateOfConfirmation) {
			this.empDateOfConfirmation = empDateOfConfirmation;
		}
		public Integer getFkConfStatusId() {
			return fkConfStatusId;
		}
		public void setFkConfStatusId(Integer fkConfStatusId) {
			this.fkConfStatusId = fkConfStatusId;
		}
		public Short getBuUnitId() {
			return buUnitId;
		}
		public void setBuUnitId(Short buUnitId) {
			this.buUnitId = buUnitId;
		}
		public Integer getDelegatedToMgrId() {
			return delegatedToMgrId;
		}
		public void setDelegatedToMgrId(Integer delegatedToMgrId) {
			this.delegatedToMgrId = delegatedToMgrId;
		}
		public String getDelegatedMgrName() {
			return delegatedMgrName;
		}
		public void setDelegatedMgrName(String delegatedMgrName) {
			this.delegatedMgrName = delegatedMgrName;
		}
		@Override
		public String toString() {
			return "SearchDetailsOutputBean [empId=" + empId + ", empName="
					+ empName + ", mgrId=" + mgrId + ", mgrName=" + mgrName
					+ ", pkFeedbackId=" + pkFeedbackId + ", pkConfId="
					+ pkConfId + ", pkEmpLevelId=" + pkEmpLevelId
					+ ", empLevelName=" + empLevelName
					+ ", pkEmpDesignationId=" + pkEmpDesignationId
					+ ", empDesignationName=" + empDesignationName
					+ ", empDateOfJoining=" + empDateOfJoining
					+ ", empDateOfConfirmation=" + empDateOfConfirmation
					+ ", fkConfStatusId=" + fkConfStatusId + ", buUnitId="
					+ buUnitId + ", delegatedToMgrId=" + delegatedToMgrId
					+ ", delegatedMgrName=" + delegatedMgrName + "]";
		}
		
}
