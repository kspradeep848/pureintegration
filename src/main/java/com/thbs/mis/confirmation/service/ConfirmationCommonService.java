package com.thbs.mis.confirmation.service;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bean.EmployeeNameWithRMIdBean;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.dao.BusinessUnitRepository;
import com.thbs.mis.common.dao.DesignationRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.common.dao.LevelRepository;
import com.thbs.mis.confirmation.bean.GetConfDetailsByEmpIdOutputBean;
import com.thbs.mis.confirmation.bean.GetConfFeedbackDetailsByEmpIdOutputBean;
import com.thbs.mis.confirmation.bean.GetFeedbackDetailsOutputBean;
import com.thbs.mis.confirmation.bean.GetManagerNamesOutputBean;
import com.thbs.mis.confirmation.bean.SearchDetailsInputBean;
import com.thbs.mis.confirmation.bean.SearchDetailsOutputBean;
import com.thbs.mis.confirmation.bo.ConfDatFeedbackBO;
import com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO;
import com.thbs.mis.confirmation.controller.ConfirmationByManagerController;
import com.thbs.mis.confirmation.dao.ConfirmationCommonRepository;
import com.thbs.mis.confirmation.dao.ConfirmationFeedbackRepository;
import com.thbs.mis.confirmation.dao.ConfirmationSpecification;
import com.thbs.mis.confirmation.dao.ConfirmationStatusRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.DateTimeUtil;
import com.thbs.mis.gsr.service.GSRGoalService;
@Service
public class ConfirmationCommonService {
	
	private static final AppLog LOG = LogFactory
			.getLog(ConfirmationByManagerController.class);
	@Autowired
	private ConfirmationCommonRepository confCommonRepo;
	
	@Autowired
	private ConfirmationFeedbackRepository confFeedbackRepo;
	
	@Autowired
	private EmployeeProfessionalRepository profDetailRepo;
	
	@Autowired
	private EmployeePersonalDetailsRepository personalDetailRepo;
	
	@Autowired
	private EmpDetailRepository empDetailRepo;
	
	@Autowired
	private BusinessUnitRepository businessUnitRepo;
	
	@Autowired
	private DesignationRepository designationRepos;
	
	@Autowired
	private LevelRepository levelRepos;
	
	@Autowired
	private GSRGoalService gsrGoalService;
	
	@Autowired
	private ConfirmationStatusRepository statusRepository;
	
	@Value("${confirmation.confirmed.status}")
	Integer confirmedStatus;
	
	@Value("${employee.not.include}")
	private String empIds;
	
	@Value("${emptype.contract.id}")
	private byte contractEmp;
	
	@Value("${emptype.fte.id}")
	private byte fteEmp;
	
	@Value("${confirmation.notApplicable.empIds}")
	private Integer confNotApplicableEmpIds;
	
	@Value("${nonbc.id}")
	private String nonbcId;
	
	@Value("${uknonbc.id}")
	private String uknonbcId;
	
	@Value("${confirmation.underProbation.status}")
	String underProbation;
	
//Added by Smrithi
	/**
	 * @author THBS
	 * @return Confirmation status of the employee id.
	 * @throws CommonCustomException 
	 * @Description: This method is used to get the Confirmation status of the employee id.
	 */
	public GetConfDetailsByEmpIdOutputBean getConfDetailsByEmpId(Integer empId) throws CommonCustomException {
		
		LOG.startUsecase("Get Confirmation status by empId");
		
		ConfDatProbationConfirmationBO probConfBO = new ConfDatProbationConfirmationBO();
		GetConfDetailsByEmpIdOutputBean outputBean = new GetConfDetailsByEmpIdOutputBean();
		DatEmpProfessionalDetailBO profDetailBO = new DatEmpProfessionalDetailBO();
		DatEmpPersonalDetailBO personalDetailBO = new DatEmpPersonalDetailBO();
		DatEmpDetailBO detailBO = new DatEmpDetailBO();
		
		Calendar c = Calendar.getInstance();
		int noOFMonths = 0;
		
		try{
			detailBO = empDetailRepo.findByPkEmpId(empId);
		}catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		try{
			profDetailBO = profDetailRepo.findByFkMainEmpDetailId(empId);
		}catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		try{
			personalDetailBO = personalDetailRepo.findByFkEmpDetailId(empId);
		}catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		
		if(detailBO.getEmpType() == (byte)2){
			outputBean.setFkEmpIdWithName(detailBO.getPkEmpId() + " - " + personalDetailBO.getEmpFirstName() + " " +personalDetailBO.getEmpLastName());
			LOG.info("profDetailBO:"+profDetailBO.toString());
			outputBean.setFkConfStatusId(null);
			outputBean.setStatusDesc("Contract");
			outputBean.setDateOfConfirmation(profDetailBO.getEmpContractEndDate());
			return outputBean;
		} 
		if(detailBO.getEmpType() == (byte)3){
			outputBean.setFkEmpIdWithName(detailBO.getPkEmpId() + " - " + personalDetailBO.getEmpFirstName() + " " +personalDetailBO.getEmpLastName());
			outputBean.setFkConfStatusId(null);
			outputBean.setStatusDesc("FTE");
			outputBean.setDateOfConfirmation(profDetailBO.getEmpContractEndDate());
			return outputBean;
		}
		
		try{
			probConfBO = confCommonRepo.findByFkEmpId(empId);
		}catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		if(probConfBO == null){
			throw new CommonCustomException("Invalid employee id");
		}else{
			
			c.setTime(profDetailBO.getEmpDateOfJoining()); 
			if(profDetailBO.getProbationMonths() != null)
				noOFMonths = profDetailBO.getProbationMonths();
			if(probConfBO.getConfExtendedInMonths() != null)
				noOFMonths += probConfBO.getConfExtendedInMonths();
			c.add(Calendar.MONTH, noOFMonths);
			Date dateOfConf = c.getTime();
			LOG.info("detailBO.getEmpType():"+detailBO.getEmpType());
				
			outputBean.setFkEmpIdWithName(probConfBO.getFkEmpId() + " - " + personalDetailBO.getEmpFirstName() + " " +personalDetailBO.getEmpLastName());
			outputBean.setFkConfStatusId(probConfBO.getFkConfStatusId());
			if(probConfBO.getFkConfStatusId() == confirmedStatus)
			{
				outputBean.setDateOfConfirmation(profDetailBO.getEmpDateOfConfirmation());
			}else
			{
				outputBean.setDateOfConfirmation(probConfBO.getTentativeConfirmationDate());
			}
			outputBean.setStatusDesc(probConfBO.getConfMasStatusBO().getStatusDesc());
			return outputBean;
			}
		
		
		
	}
	
	public List<SearchDetailsOutputBean> searchDetails(
			SearchDetailsInputBean searchDetailsInputBean) throws CommonCustomException {
		
		LOG.startUsecase("Search employee details");
		String regexName = "[a-zA-Z\\s]*$";
		
		List<SearchDetailsOutputBean> outputBeanList = new ArrayList<SearchDetailsOutputBean>();
		List<ConfDatProbationConfirmationBO> probConfList = new ArrayList<ConfDatProbationConfirmationBO>();
		DatEmpProfessionalDetailBO profDetailBO = new DatEmpProfessionalDetailBO();
		List<DatEmpProfessionalDetailBO> profDetailsList = new ArrayList<DatEmpProfessionalDetailBO>();
		List<ConfDatProbationConfirmationBO> confDetailsList = new ArrayList<ConfDatProbationConfirmationBO>();
		List<Integer> empIdList = new ArrayList<Integer>();
		
		
		
		
	if(searchDetailsInputBean.getConfStatusIdList() != null)
		{
		
		if(statusRepository.findByPkStatusIdIn(searchDetailsInputBean.getConfStatusIdList()) == null)
			throw new CommonCustomException("Invalid input for Confirmation Status Id.");
			/*if(searchDetailsInputBean.getConfStatusId() >= 6 || searchDetailsInputBean.getConfStatusId() < 1 )
			{
			throw new CommonCustomException("Invalid input for Status Id. Please enter values from 1 to 5");
			}*/
		}
		
		if(searchDetailsInputBean.getEmpId() != null)
		{	
			try{
				
			profDetailBO = profDetailRepo.findByFkMainEmpDetailId(searchDetailsInputBean.getEmpId());
				}
		catch(Exception ex)
			{
			throw new CommonCustomException("Unable to get employee details from backend.");
			}
		
			if(profDetailBO == null || searchDetailsInputBean.getEmpId() < 1)
			{
			throw new CommonCustomException("Invalid empId");
			}
		}
			DatEmpProfessionalDetailBO profDetailBO1 = new DatEmpProfessionalDetailBO();
		if(searchDetailsInputBean.getMgrId() != null)
		{
			
			try{
			
			profDetailBO1 = profDetailRepo.findByFkMainEmpDetailId(searchDetailsInputBean.getMgrId());
				}
		catch(Exception ex)
			{
			throw new CommonCustomException("Unable to get employee details from backend.");
			}
		
			if(profDetailBO1 == null || searchDetailsInputBean.getMgrId() < 1)
			{
			throw new CommonCustomException("Invalid mgrId");
			} 
		}
		MasBuUnitBO masUnitBO = new MasBuUnitBO();
		if(searchDetailsInputBean.getBuUnitId() != null )
		{
			try{
				masUnitBO = businessUnitRepo.findByPkBuUnitId(searchDetailsInputBean.getBuUnitId());
				}
			catch(Exception e)
			{
			throw new CommonCustomException("Unable to get employee details from backend.");
			}
		
			if(masUnitBO == null || searchDetailsInputBean.getBuUnitId() < 1)
			{
			throw new CommonCustomException("Invalid BU Unit Id");
			}
		}
		try{
			
			if(searchDetailsInputBean.getBuUnitId() != null || searchDetailsInputBean.getMgrId() != null)
			{
				Specification<DatEmpProfessionalDetailBO> empDetailsSpecification = ConfirmationSpecification.getBuOrRmId(searchDetailsInputBean);
				
				
				profDetailsList = profDetailRepo.findAll(empDetailsSpecification);
				
				if(!profDetailsList.isEmpty())
				{
					empIdList = profDetailsList.stream()
							.map(DatEmpProfessionalDetailBO::getFkMainEmpDetailId)
							.collect(Collectors.toList());
				} else {
					empIdList.clear();
				}
				
			}
			
			if(searchDetailsInputBean.getConfStatusIdList() != null || searchDetailsInputBean.getConfStartDate() != null || searchDetailsInputBean.getConfEndDate() != null)
			{
				LOG.startUsecase("empIdList:"+empIdList.size());
				
				if(searchDetailsInputBean.getConfStartDate() != null && searchDetailsInputBean.getConfEndDate() != null)
				{
					if (searchDetailsInputBean.getConfStartDate().after(
							searchDetailsInputBean.getConfEndDate())) 
					{
						throw new CommonCustomException(
								"Conf StartDate should be less than Conf endDate.");
			
					}
					else {
					Date startDate = DateTimeUtil
							.dateWithMinTimeofDay(searchDetailsInputBean.getConfStartDate());
					searchDetailsInputBean.setConfStartDate(startDate);
					System.out.println("++++SD@@@@@@@@"+startDate);
					
							Date endDate = DateTimeUtil
							.dateWithMaxTimeofDay(searchDetailsInputBean.getConfEndDate());
							System.out.println("@@@@ED++++"+endDate);
							searchDetailsInputBean.setConfEndDate(endDate);
						
							
					}
				}
				
				Specification<ConfDatProbationConfirmationBO> confDetailsSpecification = ConfirmationSpecification.getStatusOrEmpId(searchDetailsInputBean,empIdList);
				confDetailsList = confCommonRepo.findAll(confDetailsSpecification);
				LOG.info("@@@@@@@@@@@@@@@@@ confDetailsList:"+confDetailsList.size());
				if(!confDetailsList.isEmpty()){
					empIdList.clear();
					for(ConfDatProbationConfirmationBO confDetail : confDetailsList){
						empIdList.add(confDetail.getFkEmpId());
					}
				} else {
					empIdList.clear();
				}
				
			
				
			}
			
			LOG.startUsecase("confDetailsList:"+confDetailsList.size());
			
			if(searchDetailsInputBean.getEmpId() != null)
			{
				empIdList.clear();
				empIdList.add(searchDetailsInputBean.getEmpId());
			}
			
			LOG.startUsecase("empIdList:"+empIdList);
			if(!empIdList.isEmpty())
			{
				/*System.out.println("@@@@@Coming inside if loop");
				if(searchDetailsInputBean.getConfStartDate() != null && searchDetailsInputBean.getConfEndDate() != null)
				{
					if (searchDetailsInputBean.getConfStartDate().equals(
							searchDetailsInputBean.getConfEndDate())) 
					{
						throw new CommonCustomException("Conf startDate and Conf endDate are same.");
			
					}
			
					else 
					Date startDate = DateTimeUtil
							.dateWithMinTimeofDay(searchDetailsInputBean.getConfStartDate());
					System.out.println("++++SD@@@@@@@@"+startDate);
					
							Date endDate = DateTimeUtil
							.dateWithMaxTimeofDay(searchDetailsInputBean.getConfEndDate());
							System.out.println("@@@@ED++++"+endDate);

					if (searchDetailsInputBean.getConfStartDate().after(
							searchDetailsInputBean.getConfEndDate())) 
					{
						throw new CommonCustomException(
								"Conf StartDate should be less than Conf endDate.");
			
					} else {
						
						probConfList = confCommonRepo.searchDetailsByDateRange(empIdList, startDate, endDate);
						
					}
				} else {
					System.out.println("!!!!!!!!!!!!!!Coming here ");
					probConfList = confCommonRepo.searchDetails(empIdList);
				}*/
				probConfList = confCommonRepo.searchDetails(empIdList);
				
			}
			
			/*if(empIdList.isEmpty() && searchDetailsInputBean.getBuUnitId() == null && searchDetailsInputBean.getMgrId() == null && searchDetailsInputBean.getConfStatusId() == null )
			{
				System.out.println("Comming inside first if");
				if(searchDetailsInputBean.getConfStartDate() != null && searchDetailsInputBean.getConfEndDate() != null)
				{
					System.out.println("inside date not null");
					if (searchDetailsInputBean.getConfStartDate().equals(
							searchDetailsInputBean.getConfEndDate())) 
					{
						System.out.println("-------------");
						throw new CommonCustomException("Conf startDate and Conf endDate are same.");
			
					}
			
					Date startDate = DateTimeUtil
							.dateWithMinTimeofDay(searchDetailsInputBean.getConfStartDate());
					System.out.println("++++SD@@@@@@@@"+startDate);
					
							Date endDate = DateTimeUtil
							.dateWithMaxTimeofDay(searchDetailsInputBean.getConfEndDate());
							System.out.println("@@@@ED++++"+endDate);

					
					if (searchDetailsInputBean.getConfStartDate().after(
							searchDetailsInputBean.getConfEndDate())) 
					{
						System.out.println("+++++++++++++++++++++++++++++++");
						throw new CommonCustomException(
								"Conf StartDate should be less than Conf endDate.");
			
					} else {
						System.out.println("startDate:"+startDate);
						System.out.println("endDate:"+endDate);
						probConfList = confCommonRepo.searchByDateRange( startDate, endDate);
						
						empIdList = probConfList.stream()
								.map(ConfDatProbationConfirmationBO::getFkEmpId)
								.collect(Collectors.toList());
						
						//probConfList = confCommonRepo.searchDetailsByDateRange(empIdList, searchDetailsInputBean.getConfStartDate(), searchDetailsInputBean.getConfEndDate());
						
					}
				} else {
					probConfList = confCommonRepo.searchDetails(empIdList);
				}
				
			} */
			
		}catch(Exception e){
			throw new CommonCustomException("No details found.");
			
		}
		
		if(!(probConfList == null))
		{
		for (ConfDatProbationConfirmationBO confDatProbationConfirmationBO : probConfList)
			{
			SearchDetailsOutputBean outputBean = new SearchDetailsOutputBean();
			
			outputBean.setEmpId(confDatProbationConfirmationBO.getFkEmpId() );
			outputBean.setEmpName(gsrGoalService.getEmpNameByEmpId(confDatProbationConfirmationBO.getFkEmpId()));
			outputBean.setPkEmpLevelId((int)confDatProbationConfirmationBO.getPkEmpLevelId());
			outputBean.setEmpLevelName(confDatProbationConfirmationBO.getEmpLevelName());
			outputBean.setPkEmpDesignationId(confDatProbationConfirmationBO.getFkEmpDesignation());
			outputBean.setEmpDesignationName(confDatProbationConfirmationBO.getEmpDesignationName());
			outputBean.setEmpDateOfJoining(confDatProbationConfirmationBO.getEmpDateOfJoining());
			outputBean.setEmpDateOfConfirmation(confDatProbationConfirmationBO.getTentativeConfirmationDate());
			outputBean.setPkConfId(confDatProbationConfirmationBO.getPkConfId());
			outputBean.setBuUnitId(confDatProbationConfirmationBO.getBuUnitId());
			outputBean.setMgrId(confDatProbationConfirmationBO.getFkEmpReportingMgrId());
			if(confDatProbationConfirmationBO.getPkFeedbackId()!= null)
				outputBean.setPkFeedbackId(confDatProbationConfirmationBO.getPkFeedbackId());
			outputBean.setMgrName(gsrGoalService.getEmpNameByEmpId(confDatProbationConfirmationBO.getFkEmpReportingMgrId()));
			if(confDatProbationConfirmationBO.getDelegatedToMgrId() != null){
				outputBean.setDelegatedToMgrId(confDatProbationConfirmationBO.getDelegatedToMgrId());
				outputBean.setDelegatedMgrName(gsrGoalService.getEmpNameByEmpId(confDatProbationConfirmationBO.getDelegatedToMgrId()));
			}
			if(confDatProbationConfirmationBO.getEmpType() == 1){
				if(confDatProbationConfirmationBO.getFkConfStatusId() == null && outputBean.getEmpDateOfConfirmation() == null)
					outputBean.setFkConfStatusId(confirmedStatus);
				else
					outputBean.setFkConfStatusId(confDatProbationConfirmationBO.getFkConfStatusId());
			}
			if(confDatProbationConfirmationBO.getEmpType() == 2)
				outputBean.setFkConfStatusId(6);
			if(confDatProbationConfirmationBO.getEmpType() == 3)
				outputBean.setFkConfStatusId(7);
			outputBeanList.add(outputBean);
			}
		}else{
			//throw new CommonCustomException("No details found");
			return outputBeanList;
		}
		return outputBeanList;
	}
	
	public List<GetManagerNamesOutputBean> getManagerNames(Integer empId) throws CommonCustomException {
		ConfDatProbationConfirmationBO probConfBO = new ConfDatProbationConfirmationBO();
		DatEmpProfessionalDetailBO profDetailBO = new DatEmpProfessionalDetailBO();
		DatEmpPersonalDetailBO personalDetailBO = new DatEmpPersonalDetailBO();
		DatEmpPersonalDetailBO personalDetailBO1 = new DatEmpPersonalDetailBO();
		GetManagerNamesOutputBean outputBean = new GetManagerNamesOutputBean();
		List<GetManagerNamesOutputBean> outputBeanList = new ArrayList<GetManagerNamesOutputBean>();
		List<Integer> notInclude = new ArrayList<Integer>();
		String[] empIdArray = empIds.split(",");
		for (String s : empIdArray)
			notInclude.add(Integer.parseInt(s));
		try{
			probConfBO = confCommonRepo.findByFkEmpId(empId);
		}catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		
		if(probConfBO == null){
			throw new CommonCustomException("Invalid employee id");
		}else{
			try{
				profDetailBO = profDetailRepo.getSkipLevelMgrDetailsByEmpId(empId);
			}catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB");
			}
			try{
				personalDetailBO = personalDetailRepo.findByFkEmpDetailId(profDetailBO.getFkMainEmpDetailId());
				personalDetailBO1 = personalDetailRepo.findByFkEmpDetailId(profDetailBO.getFkEmpReportingMgrId());
			}catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB");
			}
			if(!notInclude.contains(profDetailBO.getFkMainEmpDetailId()) ){
			/*	outputBean.setMgrId(null);
				outputBean.setMgrName(null);
				outputBeanList.add(outputBean);*/
				outputBean.setMgrId(profDetailBO.getFkMainEmpDetailId());
				outputBean.setMgrName( personalDetailBO.getEmpFirstName() + " " +personalDetailBO.getEmpLastName());
				outputBeanList.add(outputBean);
			}/*else{
				outputBean.setMgrId(profDetailBO.getFkMainEmpDetailId());
				outputBean.setMgrName( personalDetailBO.getEmpFirstName() + " " +personalDetailBO.getEmpLastName());
				outputBeanList.add(outputBean);
			}*/
				GetManagerNamesOutputBean outputBean1 = new GetManagerNamesOutputBean();
				if(! notInclude.contains(profDetailBO.getFkEmpReportingMgrId()) ){
					/*outputBean.setMgrId(null);
					outputBean.setMgrName(null);
					outputBeanList.add(outputBean);*/
					outputBean1.setMgrId(profDetailBO.getFkEmpReportingMgrId());
					outputBean1.setMgrName( personalDetailBO1.getEmpFirstName() + " " +personalDetailBO1.getEmpLastName());
					outputBeanList.add(outputBean1);
				}/*else{
				outputBean1.setMgrId(profDetailBO.getFkEmpReportingMgrId());
				outputBean1.setMgrName( personalDetailBO1.getEmpFirstName() + " " +personalDetailBO1.getEmpLastName());
				outputBeanList.add(outputBean1);
				}*/
	}
		return outputBeanList;
	}
	
	public List<EmployeeNameWithRMIdBean> getDirectAndIndirectReporteesDetails(
			Integer rmId) {
		
		//DatEmpProfessionalDetailBO 
		 List<EmployeeNameWithRMIdBean>  outputList = new ArrayList<EmployeeNameWithRMIdBean>();
		List<Integer> empIdList = confCommonRepo.getDirectAndIndirectReporteesByRmId(rmId);
		LOG.info("empIdList:"+empIdList);
		
		
		
		// outputList = confCommonRepo.getDirectAndIndirectReporteesByRmId(rmId);
		for(Integer empId : empIdList){
		EmployeeNameWithRMIdBean employeeNameWithRMIdBeanObject = new EmployeeNameWithRMIdBean();
			employeeNameWithRMIdBeanObject.setEmpId(empId);
					
			employeeNameWithRMIdBeanObject
					.setEmpFirstName(gsrGoalService.getEmpNameByEmpId(empId));
			
			outputList.add(employeeNameWithRMIdBeanObject);
		}
		return outputList;
	}
	
	
	//EOA by Smrithi 
	
	//Added by Harsh
	
	
	/**
	 * @author THBS
	 * @return Confirmation and feedback details of the employee id.
	 * @throws CommonCustomException 
	 * @Description: This method is used to get the Confirmation details of the employee id.
	 */
	public GetConfFeedbackDetailsByEmpIdOutputBean getConfFeedbackDetailsByEmpId(Integer empId) throws CommonCustomException {
		
		LOG.startUsecase("Get Confirmation Details");
		
		ConfDatProbationConfirmationBO probConfBO = new ConfDatProbationConfirmationBO();
		GetConfFeedbackDetailsByEmpIdOutputBean outputBean = new GetConfFeedbackDetailsByEmpIdOutputBean();
		GetFeedbackDetailsOutputBean feedbackBean = new GetFeedbackDetailsOutputBean();
		DatEmpProfessionalDetailBO profDetailBO = new DatEmpProfessionalDetailBO();
		DatEmpPersonalDetailBO personalDetailBO = new DatEmpPersonalDetailBO();
		ConfDatFeedbackBO feedbackBO = new ConfDatFeedbackBO();
		//Added by Kamal Anand
		DatEmpDetailBO detailBO = new DatEmpDetailBO();
		
		try{
			detailBO = empDetailRepo.findByPkEmpId(empId);
		}catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		
		
		try{
			probConfBO = confCommonRepo.findByFkEmpId(empId);
		}catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		//End of Addition by Kamal Anand
		
		feedbackBO = confFeedbackRepo.findByFkEmpId(empId);
		
		
		/*if(probConfBO == null){
			throw new CommonCustomException("Invalid employee id");
		}else{*/
			try{
				profDetailBO = profDetailRepo.findByFkMainEmpDetailId(empId);
			}catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB");
			}
			try{
				personalDetailBO = personalDetailRepo.findByFkEmpDetailId(empId);
			}catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB");
			}
						
			/*outputBean.setFkEmpId(probConfBO.getFkEmpId());
			outputBean.setEmpName(gsrGoalService.getEmpNameByEmpId(empId));
			outputBean.setFkMgrId(probConfBO.getFkMgrId());
			outputBean.setReportingManagerName(gsrGoalService.getEmpNameByEmpId(probConfBO.getFkMgrId().intValue()));*/
			if(probConfBO != null && probConfBO.getConfirmedByMgrId()!=null) {
			outputBean.setConfirmedByMgrId(probConfBO.getConfirmedByMgrId());
			outputBean.setConfirmedByMgrName(gsrGoalService.getEmpNameByEmpId(probConfBO.getConfirmedByMgrId().intValue()));
			}
			
			if(probConfBO != null &&probConfBO.getDelegatedByHrId() != null && probConfBO.getDelegatedToMgrId() != null) {
				outputBean.setDelegatedByHrId(probConfBO.getDelegatedByHrId());
				outputBean.setDelegatedByHrName(gsrGoalService.getEmpNameByEmpId(probConfBO.getDelegatedByHrId().intValue()));
				outputBean.setDelegatedToMgrId(probConfBO.getDelegatedToMgrId());
				outputBean.setDelegatedManagerName(gsrGoalService.getEmpNameByEmpId(probConfBO.getDelegatedToMgrId().intValue()));
				outputBean.setDelegatedComments(probConfBO.getDelegatedComments());
				outputBean.setDelegatedDate(probConfBO.getDelegatedDate());
				
			}
			
			if(probConfBO != null) {
			outputBean.setTentativeConfirmationDate(probConfBO.getTentativeConfirmationDate());
			outputBean.setActualDateOfConfirmation(probConfBO.getActualDateOfConfirmation());	
			}
			
			outputBean.setEmpLevel(profDetailBO.getFkEmpLevel());
			outputBean.setEmpLevelName(profDetailBO.getMasEmpLevelBO().getEmpLevelName());
			outputBean.setDesignation(profDetailBO.getFkEmpDesignation());
			outputBean.setDesignationName(profDetailBO.getMasEmpDesignationBO().getEmpDesignationName());
			outputBean.setDateOfJoining(profDetailBO.getEmpDateOfJoining());
			
			
			//Added by Kamal Anand
			if(detailBO.getEmpType() == contractEmp)
			{
				outputBean.setConfirmationStatus(6);
				outputBean.setFkEmpId(empId);
				outputBean.setEmpName(gsrGoalService.getEmpNameByEmpId(empId));
				outputBean.setFkMgrId(profDetailBO.getFkEmpReportingMgrId());
				outputBean.setReportingManagerName(gsrGoalService.getEmpNameByEmpId(profDetailBO.getFkEmpReportingMgrId().intValue()));
			}
			else if(detailBO.getEmpType() == fteEmp)
			{
				outputBean.setConfirmationStatus(7);
				outputBean.setFkEmpId(empId);
				outputBean.setEmpName(gsrGoalService.getEmpNameByEmpId(empId));
				outputBean.setFkMgrId(profDetailBO.getFkEmpReportingMgrId());
				outputBean.setReportingManagerName(gsrGoalService.getEmpNameByEmpId(profDetailBO.getFkEmpReportingMgrId().intValue()));
			}
			else if(empId < confNotApplicableEmpIds){
				
				outputBean.setConfirmationStatus(confirmedStatus);
				outputBean.setFkEmpId(empId);
				outputBean.setEmpName(gsrGoalService.getEmpNameByEmpId(empId));
				outputBean.setFkMgrId(profDetailBO.getFkEmpReportingMgrId());
				outputBean.setReportingManagerName(gsrGoalService.getEmpNameByEmpId(profDetailBO.getFkEmpReportingMgrId().intValue()));
				outputBean.setActualDateOfConfirmation(profDetailBO.getEmpDateOfJoining());
				
				
				//setting feedback to null
				feedbackBean.setPkFeedbackId(null);
				feedbackBean.setAttitude(null);
				feedbackBean.setCommunication(null);
				feedbackBean.setDependability(null);
				feedbackBean.setFlexibility(null);
				feedbackBean.setProactiveness(null);
				feedbackBean.setSelfConfidence(null);
				feedbackBean.setTechnicalExpertise(null);
				feedbackBean.setWillingnessToLearn(null);
				feedbackBean.setFeedback(null);
				feedbackBean.setWorkObjectives(null);
				feedbackBean.setAreasOfImprovements(null);
				feedbackBean.setUpdatedBy(null);
				outputBean.setManagerFeedback(null);
				
				
			
			} else if(profDetailBO.getFkEmpBootCampId().intValue() != Short.parseShort(nonbcId) && profDetailBO.getFkEmpBootCampId().shortValue() != Short.parseShort(uknonbcId)) {
				if(profDetailBO.getEmpConfirmationGraduateFlag() == 1) {
					outputBean.setConfirmationStatus(confirmedStatus);
					outputBean.setFkEmpId(empId);
					outputBean.setEmpName(gsrGoalService.getEmpNameByEmpId(empId));
					outputBean.setFkMgrId(profDetailBO.getFkEmpReportingMgrId());
					outputBean.setReportingManagerName(gsrGoalService.getEmpNameByEmpId(profDetailBO.getFkEmpReportingMgrId().intValue()));
					outputBean.setActualDateOfConfirmation(profDetailBO.getEmpDateOfJoining());
				} else {
					outputBean.setConfirmationStatus(Integer.parseInt(underProbation));
					outputBean.setFkEmpId(empId);
					outputBean.setEmpName(gsrGoalService.getEmpNameByEmpId(empId));
					outputBean.setFkMgrId(profDetailBO.getFkEmpReportingMgrId());
					outputBean.setReportingManagerName(gsrGoalService.getEmpNameByEmpId(profDetailBO.getFkEmpReportingMgrId().intValue()));
					outputBean.setActualDateOfConfirmation(profDetailBO.getEmpDateOfJoining());
				}
				outputBean.setManagerFeedback(null);
			}
			else
			{
				outputBean.setConfirmationStatus(probConfBO.getFkConfStatusId());
				outputBean.setFkEmpId(probConfBO.getFkEmpId());
				outputBean.setEmpName(gsrGoalService.getEmpNameByEmpId(empId));
				outputBean.setFkMgrId(probConfBO.getFkMgrId());
				outputBean.setReportingManagerName(gsrGoalService.getEmpNameByEmpId(probConfBO.getFkMgrId().intValue()));
				outputBean.setIsConfirmedByHr(probConfBO.getIsConfirmedByHr());
				outputBean.setMgrDenialReason(probConfBO.getMgrDenialReason());
				outputBean.setUpdatedBy(probConfBO.getUpdatedBy());
				outputBean.setUpdatedOn(probConfBO.getUpdatedOn());
				outputBean.setConfirmationLetterSent(probConfBO.getConfirmationLetterSent());
				outputBean.setExtensionLetterSent(probConfBO.getExtensionLetterSent());
				outputBean.setReasonForExtension(probConfBO.getReasonForExtension());
				
			}
			//End of Addition by Kamal Anand
			if(probConfBO != null && probConfBO.getConfExtendedByHrId() != null) {
				outputBean.setConfExtendedByHrId(probConfBO.getConfExtendedByHrId());
				outputBean.setConfExtendedByHrName(gsrGoalService.getEmpNameByEmpId(probConfBO.getConfExtendedByHrId().intValue()));
				outputBean.setConfExtendedInMonths(probConfBO.getConfExtendedInMonths());
				outputBean.setReasonForExtension(probConfBO.getReasonForExtension());
			}
			
			if(probConfBO != null && probConfBO.getConfirmedByHrId() != null) {
				outputBean.setConfirmedByHrId(probConfBO.getConfirmedByHrId());
				outputBean.setConfirmedByHrName(gsrGoalService.getEmpNameByEmpId(probConfBO.getConfirmedByHrId().intValue()));
			}
			
			
			if(probConfBO != null && probConfBO.getIsConfirmedByMgr() != null && probConfBO.getConfirmedByMgrId() != null) {
				outputBean.setIsConfirmedByMgr(probConfBO.getIsConfirmedByMgr());
				outputBean.setConfirmedByMgrId(probConfBO.getConfirmedByMgrId());
				outputBean.setConfirmedByMgrName(gsrGoalService.getEmpNameByEmpId(probConfBO.getConfirmedByMgrId().intValue()));
			}
			
			if(feedbackBO != null)
			{			
				feedbackBean.setPkFeedbackId(feedbackBO.getPkFeedbackId());
				feedbackBean.setAttitude(feedbackBO.getAttitude());
				feedbackBean.setCommunication(feedbackBO.getCommunication());
				feedbackBean.setDependability(feedbackBO.getDependability());
				feedbackBean.setFlexibility(feedbackBO.getFlexibility());
				feedbackBean.setProactiveness(feedbackBO.getProactiveness());
				feedbackBean.setSelfConfidence(feedbackBO.getSelfConfidence());
				feedbackBean.setTechnicalExpertise(feedbackBO.getTechnicalExpertise());
				feedbackBean.setWillingnessToLearn(feedbackBO.getWillingnessToLearn());
				feedbackBean.setFeedback(feedbackBO.getFeedback());
				feedbackBean.setWorkObjectives(feedbackBO.getWorkObjectives());
				feedbackBean.setAreasOfImprovements(feedbackBO.getAreasOfImprovements());
				feedbackBean.setUpdatedBy(feedbackBO.getUpdatedBy());
				outputBean.setManagerFeedback(feedbackBean);
			}
	//	}
		
		return outputBean;
	}

	
	
	//EOA by Harsh
}