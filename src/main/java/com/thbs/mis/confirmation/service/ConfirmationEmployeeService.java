package com.thbs.mis.confirmation.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.MasCountryBO;
import com.thbs.mis.common.dao.BusinessUnitRepository;
import com.thbs.mis.common.dao.CountryRepository;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.DesignationRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.common.dao.LevelRepository;
import com.thbs.mis.confirmation.bean.GenerateConfExtLetterOutputBean;
import com.thbs.mis.confirmation.bean.GenerateConfirmationLetterBean;
import com.thbs.mis.confirmation.bean.GenerateExtensionLetterBean;
import com.thbs.mis.confirmation.bean.GetConfFeedbackDetailsByHistoryIdOutputBean;
import com.thbs.mis.confirmation.bean.GetFeedbackHistoryByIdOutputBean;
import com.thbs.mis.confirmation.bean.GetFeedbackModificationDatesOutputBean;
import com.thbs.mis.confirmation.bo.ConfDatFeedbackBO;
import com.thbs.mis.confirmation.bo.ConfDatFeedbackHistoryBO;
import com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO;
import com.thbs.mis.confirmation.dao.ConfirmationCommonRepository;
import com.thbs.mis.confirmation.dao.ConfirmationFeedbackHistoryRepository;
import com.thbs.mis.confirmation.dao.ConfirmationFeedbackRepository;
import com.thbs.mis.confirmation.dao.ConfirmationProbationHistoryRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.service.GSRGoalService;

@Service
public class ConfirmationEmployeeService {
	private static final AppLog LOG = LogFactory.getLog(ConfirmationEmployeeService.class);

	@Autowired
	private ConfirmationCommonRepository confCommonRepo;
	
	@Autowired
	private ConfirmationFeedbackHistoryRepository confFeedbackHistoryRepo;
	
	@Autowired
	private EmployeeProfessionalRepository profDetailRepo;
	
	@Autowired
	private DatEmployeeProfessionalRepository professionalRepo;
	
	@Autowired
	private EmployeePersonalDetailsRepository personalDetailRepo;
	
	@Autowired
	private EmpDetailRepository empDetailRepo;
	
	@Autowired
	private ConfirmationByHRService hrService;
	
	@Autowired
	private BusinessUnitRepository businessUnitRepo;
	
	@Autowired
	private DesignationRepository designationRepos;
	
	@Autowired
	private LevelRepository levelRepos;
	
	@Autowired
	private GSRGoalService gsrGoalService;
	
	@Autowired
	private ConfirmationFeedbackRepository confFeedbackRepo;
	
	@Autowired
	private CountryRepository countryRepo;
	
	@Autowired
	private ConfirmationProbationHistoryRepository confProbHistoryRepo;
	
	@Value("${conf.ext.letter.basepath}")
	private String confExtLetterBasePath;
	
	@Value("${conf.letter.basepath}")
	private String confLetterBasePath;
	
	@Value("${ext.letter.basepath}")
	private String extLetterBasePath;
	
	
	@Value("${confirmation.footer1}")
	private String confFooter1;
	
	@Value("${confirmation.footer2}")
	private String confFooter2;
	
	@Value("${confirmation.confirmed.status}")
	private Integer confirmedStatus;
	
	@Value("${confirmation.extended.probation.status}")
	private Integer extendedProbationStatus;
	
	@Value("${torry.harris.logo}")
	private String torryHarrisLogo;
	
	@Value("${conf.template.filepath}")
	private String confTemplateFilepath;
	
	@Value("${ext.template.filepath}")
	private String extTemplateFilepath;
	
	
	/**
	 * @author THBS
	 * @return Employee Feedback History.
	 * @throws CommonCustomException 
	 * @Description: This method is used to get employee Feedback History by History Id.
	 */
	public GetConfFeedbackDetailsByHistoryIdOutputBean getFeedbackHistoryById(Integer historyId) throws CommonCustomException {
		
		LOG.startUsecase("Get Employee Feedback History");
		
		GetFeedbackHistoryByIdOutputBean feedbackBean = new GetFeedbackHistoryByIdOutputBean();
	
		ConfDatFeedbackHistoryBO feedbackBO = new ConfDatFeedbackHistoryBO();
		ConfDatProbationConfirmationBO probConfBO = new ConfDatProbationConfirmationBO();
		GetConfFeedbackDetailsByHistoryIdOutputBean outputBean = new GetConfFeedbackDetailsByHistoryIdOutputBean();
		DatEmpProfessionalDetailBO profDetailBO = new DatEmpProfessionalDetailBO();
		DatEmpPersonalDetailBO personalDetailBO = new DatEmpPersonalDetailBO();
		
	
		try{
			feedbackBO = confFeedbackHistoryRepo.findByPkHistoryId(historyId);
		}catch(Exception e){
			throw new CommonCustomException("Feedback history Id not found.");
		}
		
		Integer empId = feedbackBO.getFkEmpId();
		
		try{
			probConfBO = confCommonRepo.findByFkEmpId(empId);
		}catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		
		
		if(probConfBO == null){
			throw new CommonCustomException("Invalid employee id");
		}else{
			try{
				profDetailBO = profDetailRepo.findByFkMainEmpDetailId(empId);
			}catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB");
			}
			try{
				personalDetailBO = personalDetailRepo.findByFkEmpDetailId(empId);
			}catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB");
			}
						
			outputBean.setFkEmpId(feedbackBO.getFkEmpId());
			outputBean.setEmpName(gsrGoalService.getEmpNameByEmpId(empId));
			outputBean.setFkMgrId(feedbackBO.getFkMgrId());
			outputBean.setReportingManagerName(gsrGoalService.getEmpNameByEmpId(feedbackBO.getFkMgrId().intValue()));
			if(feedbackBO.getConfirmedByMgrId()!=null) {
			outputBean.setConfirmedByMgrId(feedbackBO.getConfirmedByMgrId());
			outputBean.setConfirmedByMgrName(gsrGoalService.getEmpNameByEmpId(feedbackBO.getConfirmedByMgrId().intValue()));
			}
			
			if(feedbackBO.getDelegatedByHrId() != null && feedbackBO.getDelegatedToMgrId() != null) {
				outputBean.setDelegatedByHrId(feedbackBO.getDelegatedByHrId());
				outputBean.setDelegatedByHrName(gsrGoalService.getEmpNameByEmpId(feedbackBO.getDelegatedByHrId().intValue()));
				outputBean.setDelegatedToMgrId(feedbackBO.getDelegatedToMgrId());
				outputBean.setDelegatedManagerName(gsrGoalService.getEmpNameByEmpId(feedbackBO.getDelegatedToMgrId().intValue()));
				outputBean.setDelegatedComments(feedbackBO.getDelegatedComments());
				outputBean.setDelegatedDate(feedbackBO.getDelegatedDate());
			}
			
						
			outputBean.setEmpLevel(profDetailBO.getFkEmpLevel());
			outputBean.setEmpLevelName(profDetailBO.getMasEmpLevelBO().getEmpLevelName());
			outputBean.setDesignation(profDetailBO.getFkEmpDesignation());
			outputBean.setDesignationName(profDetailBO.getMasEmpDesignationBO().getEmpDesignationName());
			outputBean.setDateOfJoining(profDetailBO.getEmpDateOfJoining());
			outputBean.setTentativeConfirmationDate(feedbackBO.getTentativeConfirmationDate());
			outputBean.setActualDateOfConfirmation(feedbackBO.getActualDateOfConfirmation());
			outputBean.setConfirmationStatus(feedbackBO.getFkConfStatusId());
			
			if(feedbackBO.getConfExtendedByHrId() != null) {
				outputBean.setConfExtendedByHrId(feedbackBO.getConfExtendedByHrId());
				outputBean.setConfExtendedByHrName(gsrGoalService.getEmpNameByEmpId(feedbackBO.getConfExtendedByHrId().intValue()));
				outputBean.setConfExtendedInMonths(feedbackBO.getConfExtendedInMonths());
				outputBean.setReasonForExtension(feedbackBO.getReasonForExtension());
			}
			
			outputBean.setIsConfirmedByHr(feedbackBO.getIsConfirmedByHr());
			if(feedbackBO.getConfirmedByHrId() != null) {
				outputBean.setConfirmedByHrId(feedbackBO.getConfirmedByHrId());
				outputBean.setConfirmedByHrName(gsrGoalService.getEmpNameByEmpId(feedbackBO.getConfirmedByHrId().intValue()));
			}
			outputBean.setConfirmationLetterSent(probConfBO.getConfirmationLetterSent());
			outputBean.setExtensionLetterSent(probConfBO.getExtensionLetterSent());
			
			if(feedbackBO.getIsConfirmedByMgr() != null && feedbackBO.getConfirmedByMgrId() != null) {
				outputBean.setIsConfirmedByMgr(feedbackBO.getIsConfirmedByMgr());
				outputBean.setConfirmedByMgrId(feedbackBO.getConfirmedByMgrId());
				outputBean.setConfirmedByMgrName(gsrGoalService.getEmpNameByEmpId(feedbackBO.getConfirmedByMgrId().intValue()));
			}
			
			outputBean.setMgrDenialReason(feedbackBO.getMgrDenialReason());
			outputBean.setUpdatedBy(feedbackBO.getUpdatedBy());
			outputBean.setUpdatedOn(feedbackBO.getUpdatedOn());
			
								 
			feedbackBean.setPkHistoryId(feedbackBO.getPkHistoryId());		
			feedbackBean.setFkFeedbackId(feedbackBO.getFkFeedbackId());
			feedbackBean.setAttitude(feedbackBO.getAttitude());
			feedbackBean.setCommunication(feedbackBO.getCommunication());
			feedbackBean.setDependability(feedbackBO.getDependability());
			feedbackBean.setFlexibility(feedbackBO.getFlexibility());
			feedbackBean.setProactiveness(feedbackBO.getProactiveness());
			feedbackBean.setSelfConfidence(feedbackBO.getSelfConfidence());
			feedbackBean.setTechnicalExpertise(feedbackBO.getTechnicalExpertise());
			feedbackBean.setWillingnessToLearn(feedbackBO.getWillingnessToLearn());
			feedbackBean.setFeedback(feedbackBO.getFeedback());
			feedbackBean.setWorkObjectives(feedbackBO.getWorkObjectives());
			feedbackBean.setAreasOfImprovements(feedbackBO.getAreasOfImprovements());
			feedbackBean.setUpdatedBy(feedbackBO.getUpdatedBy());
			feedbackBean.setUpdatedOn(feedbackBO.getUpdatedOn());
			outputBean.setManagerFeedback(feedbackBean);
						
			
		}
		
		return outputBean;
	}
	
	
	public List<GetFeedbackModificationDatesOutputBean> getFeedbackDatesByEmpId(Integer empId) throws CommonCustomException {
				
		List<GetFeedbackModificationDatesOutputBean> outputBeanList = new ArrayList<GetFeedbackModificationDatesOutputBean>();
		List<ConfDatFeedbackHistoryBO> feedbackHistoryList = new ArrayList<ConfDatFeedbackHistoryBO>();
		
		try{
			
			if(confFeedbackRepo.findByFkEmpId(empId) != null) {
				feedbackHistoryList = confFeedbackHistoryRepo.findByFkEmpIdOrderByUpdatedOnDesc(empId);
			} else {
				outputBeanList.add(new GetFeedbackModificationDatesOutputBean());
			}
			
		}catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		
		if(feedbackHistoryList != null){
			
			
			try {
				if(confFeedbackRepo.findByFkEmpId(empId) != null) {
					GetFeedbackModificationDatesOutputBean outputBean = new GetFeedbackModificationDatesOutputBean();
					ConfDatFeedbackBO feedbackBO = new ConfDatFeedbackBO();
					
					outputBean.setPkHistoryId(null);
					outputBean.setUpdatedOn(null);
					feedbackBO = confFeedbackRepo.findByFkEmpId(empId);
					
					outputBean.setUpdatedOn(feedbackBO.getUpdatedOn());
					outputBeanList.add(outputBean);
				}
				
			}catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB");
			}
			
						
			for (ConfDatFeedbackHistoryBO feedbackHistoryBO : feedbackHistoryList)
			{
				GetFeedbackModificationDatesOutputBean outputBean = new GetFeedbackModificationDatesOutputBean();
				
				outputBean.setPkHistoryId(feedbackHistoryBO.getPkHistoryId());
				outputBean.setUpdatedOn(feedbackHistoryBO.getUpdatedOn());
				
				outputBeanList.add(outputBean);
			}
		} /*else {
			outputBeanList.add(new GetFeedbackModificationDatesOutputBean());
		}*/
		
		
		
		return outputBeanList;
	}
	
	
	public byte[]  generatePDF(String jasperReportName, ByteArrayOutputStream outputStream, 
			@SuppressWarnings("rawtypes") List reportList) 
	 {
	       	JRPdfExporter exporter = new JRPdfExporter();
	        try {
	            	            
	            JasperReport jasperReport = JasperCompileManager.compileReport(jasperReportName);
	            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,null, new JRBeanCollectionDataSource(reportList));
	            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);   
	            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
	            exporter.exportReport();
	            LOG.startUsecase("Successfully generated PDF file");
	        } catch (Exception e) {
	          LOG.endUsecase("Exception while generating file "+e);
	          
	        } finally {
	        }
	        return outputStream.toByteArray();
	 }
	
	
	public GenerateConfExtLetterOutputBean generateConfExtLetter(Integer empId) 
			throws CommonCustomException
	{
	
	ConfDatProbationConfirmationBO probationConfBO = new ConfDatProbationConfirmationBO();
	DatEmpProfessionalDetailBO profDetailBO1 = new DatEmpProfessionalDetailBO();
	GenerateConfExtLetterOutputBean outputBean = new GenerateConfExtLetterOutputBean();
	MasCountryBO masCountryBO = new MasCountryBO();
	DatEmpPersonalDetailBO personalDetailBO = new DatEmpPersonalDetailBO();
	SimpleDateFormat sdf = new SimpleDateFormat("MMMMM dd, yyyy");
		
	try{
		probationConfBO = confCommonRepo.findByFkEmpId(empId);
	}catch(Exception e){
		throw new CommonCustomException("Failed to fetch details from DB");
	}
	
	if(probationConfBO == null){
		throw new CommonCustomException("Invalid employee id");
	}else{
		try{
			profDetailBO1 =  professionalRepo.findByFkMainEmpDetailId(empId);
			LOG.info("@@@@@@@ profDetailBO.getMasEmpDesignationBO().getEmpDesignationName() : "+profDetailBO1.getMasEmpDesignationBO().getEmpDesignationName());
			LOG.info("@@@@@@@ profDetailBO.getMasEmpLevelBO().getEmpLevelName():"+profDetailBO1.getMasEmpLevelBO().getEmpLevelName());
		}catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		try{
			personalDetailBO = personalDetailRepo.findByFkEmpDetailId(empId);
		}catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		
		
		try{
			masCountryBO = countryRepo.findByPkCountryId(profDetailBO1.getMasLocationParentBO().getFkCountryId());
		}catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		
		String countryName = masCountryBO.getCountryName();
		
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		String confExtLetterFileName = "";
		
		String filePath = null;        
	    FileOutputStream fos = null;
		
		if(probationConfBO.getFkConfStatusId() == confirmedStatus){
			//PDF file creation for Confirmation Letter.
			List<GenerateConfirmationLetterBean> list = new ArrayList<GenerateConfirmationLetterBean>();
			GenerateConfirmationLetterBean createPDFBean = new GenerateConfirmationLetterBean();
			String actualdoc ="";
			 confExtLetterFileName = "Confirmation_Letter_" + empId.toString() + ".pdf" ;
			
			if(probationConfBO.getActualDateOfConfirmation() == null) {
				throw new CommonCustomException("Actual Confimation Date is not set.");
			}
			else {
			actualdoc = "After review of your performance during the probation period,"
					+ " we are pleased to confirm your position as " + designationRepos.findByPkEmpDesignationId(probationConfBO.getPostDesignationLevel()).getEmpDesignationName() + " with effect from " + sdf.format(probationConfBO.getActualDateOfConfirmation()).toString() ;
			}
			createPDFBean.setCurDate(sdf.format(new Date()).toString());
			createPDFBean.setActualDoC(actualdoc);
			createPDFBean.setLogo(torryHarrisLogo);
			createPDFBean.setEmpName(personalDetailBO.getEmpFirstName() + " " + personalDetailBO.getEmpLastName());
			createPDFBean.setEmpFname(personalDetailBO.getEmpFirstName());
			createPDFBean.setEmpId(empId);
			createPDFBean.setDesg(designationRepos.findByPkEmpDesignationId(probationConfBO.getPostDesignationLevel()).getEmpDesignationName());
			createPDFBean.setLevel(levelRepos.findByPkEmpLevelId(probationConfBO.getPostConfirmationLevel()).getEmpLevelName());
			createPDFBean.setCountry(countryName);
			createPDFBean.setFooter1(confFooter1);
			createPDFBean.setFooter2(confFooter2);
			list.add(createPDFBean);
			
			 try {
				 
				
			    	byte[] bytes = generatePDF(confTemplateFilepath, outputStream, list);
			        if(bytes.length>1){
			        	System.out.println("-------------------");
			        	
			        	filePath = confLetterBasePath + empId.toString(); 
			        	File someFile = new File(filePath);
			        	
						if (!someFile.exists()) {
							LOG.startUsecase("Entering if (!someFile.exists())");
							someFile.mkdir();
							LOG.startUsecase("Exiting if (!someFile.exists())");
						}
						
						
						filePath = filePath + "/" +  confExtLetterFileName;
						someFile = new File(filePath);
						
			        	System.out.println("++++++++++++++++++++++++++++");
			        	if(someFile.exists())
			        	{
			        		if(!someFile.delete())
			        			throw new CommonCustomException("Failed to delete the existing Confirmation Letter in server.");
			        	}
			        	
			        	fos = new FileOutputStream(someFile);
			  	        fos.write(bytes);
			  	        fos.flush();
			  	        fos.close();
			  	        
			            System.out.println("<<<<<<<<<<<<Confirmation Letter Generated>>>>>>>>");
			        }
			    }catch(FileNotFoundException e) {
			       e.printStackTrace();
			    }catch(IOException e){
			       e.printStackTrace();
			    }finally{
			    	if(fos!=null){
			    		try {
			    			fos.close();
			    		} catch (IOException e) {
			            	e.printStackTrace();
			            }
			        }
			    }
			
		}
		else if(probationConfBO.getFkConfStatusId() == extendedProbationStatus){
			//PDF file creation for Extension Letter.
			List<GenerateExtensionLetterBean> list = new ArrayList<GenerateExtensionLetterBean>();
			GenerateExtensionLetterBean createPDFBean = new GenerateExtensionLetterBean();
			String finalDate ="";
			String extMonths ="";
			confExtLetterFileName = "Extension_Letter_" + empId.toString() + ".pdf";
			//Date updatedTentDate = hrService.extendProbationByHr(probationConfBO.getTentativeConfirmationDate());
			
			if(probationConfBO.getTentativeConfirmationDate() == null || probationConfBO.getConfExtendedInMonths() == null) {
				throw new CommonCustomException("Tentative Confimation Date is not set.");
			}
			else {
			
				/*String[] numToword = { "-", "One", "Two", "Three", "Four",
						"Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve" };*/
				
			finalDate = "Your next review will be conducted on " + sdf.format(probationConfBO.getTentativeConfirmationDate()).toString() ;
			extMonths = "After review, we would like to inform you that your probation has been extended by " + probationConfBO.getConfExtendedInMonths().toString() + " months.";
			LOG.info("tent conf date!!!!!"+probationConfBO.getTentativeConfirmationDate());
			}
			
			createPDFBean.setCurDate(sdf.format(new Date()).toString());
			createPDFBean.setExtMonths(extMonths);
			createPDFBean.setFinalDate(finalDate);
			createPDFBean.setLogo(torryHarrisLogo);
			createPDFBean.setEmpName(personalDetailBO.getEmpFirstName() + " " + personalDetailBO.getEmpLastName());
			createPDFBean.setEmpFname(personalDetailBO.getEmpFirstName());
			createPDFBean.setEmpId(empId);
			createPDFBean.setDesg(designationRepos.findByPkEmpDesignationId(profDetailBO1.getFkEmpDesignation()).getEmpDesignationName());
			createPDFBean.setLevel(levelRepos.findByPkEmpLevelId(profDetailBO1.getFkEmpLevel()).getEmpLevelName());
			createPDFBean.setCountry(countryName);
			createPDFBean.setFooter1(confFooter1);
			createPDFBean.setFooter2(confFooter2);
			list.add(createPDFBean);
			
			 try {
				 LOG.startUsecase("Entering generatePDF");
			    	byte[] bytes = generatePDF(extTemplateFilepath, outputStream, list);
			        if(bytes.length>1){
			        	System.out.println("-------------------");
			        	
			        	filePath = extLetterBasePath + empId.toString(); 
			        	File someFile = new File(filePath);
			        	
						if (!someFile.exists()) {
							LOG.startUsecase("Entering if (!someFile.exists())");
							someFile.mkdir();
							LOG.startUsecase("Exiting if (!someFile.exists())");
						}
						
						
						filePath = filePath + "/" +  confExtLetterFileName;
						someFile = new File(filePath);
						
			        	System.out.println("++++++++++++++++++++++++++++");
			        	if(someFile.exists())
			        	{
			        		if(!someFile.delete())
			        			throw new CommonCustomException("Failed to delete the existing Extension Letter in server.");
			        	}
			        	
			        	fos = new FileOutputStream(someFile);
			  	        fos.write(bytes);
			  	        fos.flush();
			  	        fos.close();
			  	        
			            System.out.println("<<<<<<<<<<<<Extension Letter Generated>>>>>>>>");
			        }
			    }catch(FileNotFoundException e) {
			       e.printStackTrace();
			    }catch(IOException e){
			       e.printStackTrace();
			    }finally{
			    	if(fos!=null){
			    		try {
			    			fos.close();
			    		} catch (IOException e) {
			            	e.printStackTrace();
			            }
			        }
			    }
			
		}
		else {
			throw new CommonCustomException("Operation not allowed. Employee status should be either Confirmed or Extended Probation.");
		}
		
	    
	    outputBean.setFilePath(filePath);
	    outputBean.setFileName(confExtLetterFileName);
	    return outputBean;
	} 
}
	
	//EOA by Harsh
}
