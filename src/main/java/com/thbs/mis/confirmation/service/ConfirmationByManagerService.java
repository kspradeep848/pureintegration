package com.thbs.mis.confirmation.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.confirmation.bean.ConfirmationSubmitFeedbackInputBean;
import com.thbs.mis.confirmation.bean.ConfirmationUpdateFeedbackInputBean;
import com.thbs.mis.confirmation.bo.ConfDatFeedbackBO;
import com.thbs.mis.confirmation.bo.ConfDatFeedbackHistoryBO;
import com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO;
import com.thbs.mis.confirmation.dao.ConfirmationCommonRepository;
import com.thbs.mis.confirmation.dao.ConfirmationFeedbackHistoryRepository;
import com.thbs.mis.confirmation.dao.ConfirmationFeedbackRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.service.GSRGoalService;
import com.thbs.mis.notificationframework.confirmationnotification.service.ConfirmationNotificationService;


@Service
@Transactional
public class ConfirmationByManagerService {

	private static final AppLog LOG = LogFactory
			.getLog(ConfirmationByManagerService.class);

	
	
		@Autowired
		private ConfirmationCommonRepository confCommonRepo;
		
		@Autowired
		private ConfirmationFeedbackRepository confFeedbackRepo;

		@Autowired
		private ConfirmationFeedbackHistoryRepository confFeedbackHistoryRepo;
		
		@Autowired
		private EmpDetailRepository empDetailRepo;
		
		@Autowired
		private EmployeeProfessionalRepository profDetailRepo;
		
		@Autowired
		private GSRGoalService goalService;
		
		@Value("${confirmation.confirmed.status}")
		private Integer confirmedStatus;
		
		@Value("${confirmation.extended.probation.status}")
		private Integer extendedProbationStatus;
		
		@Value("${confirmation.pendingWithHr.status}")
		private Integer pendingWithHrStatus;

		
		@Value("${confirmation.active.feedback.days}")
		private Integer activeFeedbackDays;
		
		@Autowired
		private ConfirmationNotificationService confNotificationService;
		
		@Autowired
		private EmpDetailRepository empDetailRepository;
		
		@Value("${hr.role.id}")
		private String hrRoleIds;
		
	//Added by Harsh
		
		public boolean submitFeedbackByManager(
				ConfirmationSubmitFeedbackInputBean confirmationSubmitFeedbackInputBean) throws CommonCustomException {
			
			boolean flag = false;
			boolean hasAccess = false;
			boolean hasAccessforExtend = false;
			
			ConfDatProbationConfirmationBO probationConfirmationBO = new ConfDatProbationConfirmationBO();
			ConfDatFeedbackBO confDatFeedbackBO = new ConfDatFeedbackBO();
			ConfDatFeedbackBO feedbackBO = new ConfDatFeedbackBO();
			LOG.startUsecase("Manager Feedback");
			try{				
				probationConfirmationBO = confCommonRepo.findByFkEmpId(confirmationSubmitFeedbackInputBean.getEmpId());
			}
			catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB");
			}
			
			if(probationConfirmationBO == null){
				throw new CommonCustomException("Invalid employee id.");
			}
			if(probationConfirmationBO.getFkConfStatusId() == confirmedStatus)
			{
				throw new CommonCustomException("The employee is already confirmed. Cannot submit Feedback.");
			}
			
			if(probationConfirmationBO.getFkConfStatusId() == pendingWithHrStatus)
			{
				throw new CommonCustomException("The employee Feedback is already submitted. Update Feedback if you provided it.");
			}
			
			else{
					
				DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
				try
				{
					datEmpDetailBO = empDetailRepo.findByPkEmpId(confirmationSubmitFeedbackInputBean.getLoggedInUser());
				}
				catch(Exception ex){
					throw new CommonCustomException("Unable to get employee details from backend.");
				}
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(probationConfirmationBO.getTentativeConfirmationDate());
				cal.add(Calendar.DAY_OF_MONTH, - activeFeedbackDays);
				Date confDateMinus21Days = cal.getTime();
				
				
				 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				 String validFeedbackDate = df.format(confDateMinus21Days);
				 String tentconfdate = df.format(probationConfirmationBO.getTentativeConfirmationDate());
				 String currentDate = df.format(new Date());
				 Date currDate = new Date();
				
				String errorMsg="Error in submitting feedback.";
				
					
				if(probationConfirmationBO.getFkConfStatusId() == extendedProbationStatus)
				{
					if(confirmationSubmitFeedbackInputBean.getLoggedInUser().intValue() == probationConfirmationBO.getFkMgrId().intValue() )
					{
						System.out.println("Entering first if!!!!");
						if(validFeedbackDate.equals(currentDate) || currDate.after(confDateMinus21Days))
						{
							hasAccessforExtend = true;
							System.out.println("Enterinf date check !!!!!!!!!!!!");
							System.out.println("validFeedbackDate"+validFeedbackDate);
							System.out.println("confDateMinus21Days"+confDateMinus21Days);
							if(hasAccessforExtend){
								try{
									confDatFeedbackBO= confFeedbackRepo.findByFkEmpId(confirmationSubmitFeedbackInputBean.getEmpId());
								}catch(Exception e){
									throw new CommonCustomException("Failed to fetch details from DB");
								}
								if(confDatFeedbackBO != null ){
									
									System.out.println("Entering if  feedback is present ");
									
								confDatFeedbackBO.setPkFeedbackId(confDatFeedbackBO.getPkFeedbackId());	
								confDatFeedbackBO.setAttitude(confirmationSubmitFeedbackInputBean.getAttitude());
								confDatFeedbackBO.setCommunication(confirmationSubmitFeedbackInputBean.getCommunication());
								confDatFeedbackBO.setDependability(confirmationSubmitFeedbackInputBean.getDependability());
								confDatFeedbackBO.setFlexibility(confirmationSubmitFeedbackInputBean.getFlexibility());
								confDatFeedbackBO.setProactiveness(confirmationSubmitFeedbackInputBean.getProactiveness());
								confDatFeedbackBO.setSelfConfidence(confirmationSubmitFeedbackInputBean.getSelfConfidence());
								confDatFeedbackBO.setTechnicalExpertise(confirmationSubmitFeedbackInputBean.getTechnicalExpertise());
								confDatFeedbackBO.setWillingnessToLearn(confirmationSubmitFeedbackInputBean.getWillingnessToLearn());
								confDatFeedbackBO.setFeedback(confirmationSubmitFeedbackInputBean.getFeedback());
								confDatFeedbackBO.setWorkObjectives(confirmationSubmitFeedbackInputBean.getWorkObjectives());
								confDatFeedbackBO.setAreasOfImprovements(confirmationSubmitFeedbackInputBean.getAreasOfImprovements());
								confDatFeedbackBO.setFkConfId(probationConfirmationBO.getPkConfId());
								confDatFeedbackBO.setFkEmpId(confirmationSubmitFeedbackInputBean.getEmpId());
								confDatFeedbackBO.setUpdatedBy(confirmationSubmitFeedbackInputBean.getLoggedInUser());
								confDatFeedbackBO.setUpdatedOn(new Date());
								confFeedbackRepo.save(confDatFeedbackBO);
								
								probationConfirmationBO.setConfirmedByMgrId(confirmationSubmitFeedbackInputBean.getLoggedInUser());
								probationConfirmationBO.setIsConfirmedByMgr(confirmationSubmitFeedbackInputBean.getIsConfirmedByMgr());
								probationConfirmationBO.setFkConfStatusId(pendingWithHrStatus);
								probationConfirmationBO.setUpdatedBy(confirmationSubmitFeedbackInputBean.getLoggedInUser());
								probationConfirmationBO.setUpdatedOn(new Date());
								probationConfirmationBO.setMgrDenialReason(null);
								probationConfirmationBO.setDelegatedByHrId(null);
								probationConfirmationBO.setDelegatedComments(null);
								probationConfirmationBO.setDelegatedDate(null);
								probationConfirmationBO.setDelegatedToMgrId(null);
								probationConfirmationBO.setConfExtendedByHrId(null);
								probationConfirmationBO.setConfExtendedInMonths(null);
								probationConfirmationBO.setReasonForExtension(null);
								LOG.startUsecase("Printing BO !!!!!!!"+probationConfirmationBO.toString());
								confCommonRepo.save(probationConfirmationBO);
								flag = true;
							
								}
						}
						else {
							errorMsg = "Feedback can only be provided within a period of "+activeFeedbackDays+" days before the Date of Confirmation";
						}
					} else{
						throw new CommonCustomException( "Feedback can only be provided within a period of "+activeFeedbackDays+" days before the Date of Confirmation");
					}
				} else{
					throw new CommonCustomException("Access denied because you are not an authorized person.");
				}
				}
				
				
				if(!hasAccessforExtend) {
				
					if(confirmationSubmitFeedbackInputBean.getLoggedInUser().intValue() == probationConfirmationBO.getFkMgrId().intValue() )
					{
						if(validFeedbackDate.equals(currentDate) || currDate.after(confDateMinus21Days))
						{
														
							hasAccess = true;
							
						}
						else {
							errorMsg = "Feedback can only be provided within a period of "+activeFeedbackDays+" days before the Date of Confirmation";
						}
					}
						
					else if(confirmationSubmitFeedbackInputBean.getLoggedInUser().equals(probationConfirmationBO.getDelegatedToMgrId()))
					{
						
						Calendar cal2 = Calendar.getInstance();
						cal2.setTime(probationConfirmationBO.getDelegatedDate());
						int workDays = 2;
						int dow = cal2.get (Calendar.DAY_OF_WEEK);
						if(dow == Calendar.THURSDAY || dow == Calendar.FRIDAY) {
							workDays =4;
						}
						cal2.add(Calendar.DAY_OF_MONTH, workDays);
						Date deligatedDatePlus2Days = cal2.getTime();
						
						String validDelFeedbackDate = df.format(deligatedDatePlus2Days);
						
						if(currentDate.equals(validDelFeedbackDate) || deligatedDatePlus2Days.after(currDate))
						{
							hasAccess = true;
						}
						else {
							
							errorMsg = "Access denied to Submit Feedback since the deadline of 2 working days has passed.";
						}
					}
					else {
						
						errorMsg = "Access denied to Submit Feedback since you are not an authorized person.";
					}
				
				
				
				
				if(hasAccess){
					
					if(confirmationSubmitFeedbackInputBean.getIsConfirmedByMgr().equals("0") ) {
						if(confirmationSubmitFeedbackInputBean.getMgrDenialReason() != null ) {	
							
							probationConfirmationBO.setMgrDenialReason(confirmationSubmitFeedbackInputBean.getMgrDenialReason());
						}					
						else {
							throw new CommonCustomException("Manager Denial Reason is required.");
						}
					}
					try{
						feedbackBO= confFeedbackRepo.findByFkEmpId(confirmationSubmitFeedbackInputBean.getEmpId());
					}catch(Exception e){
						throw new CommonCustomException("Failed to fetch details from DB");
					}
					if(feedbackBO != null ){
						throw new CommonCustomException("Feedback is already provided. Update Feedback if you provided it ");
					}else{
					confDatFeedbackBO.setAttitude(confirmationSubmitFeedbackInputBean.getAttitude());
					confDatFeedbackBO.setCommunication(confirmationSubmitFeedbackInputBean.getCommunication());
					confDatFeedbackBO.setDependability(confirmationSubmitFeedbackInputBean.getDependability());
					confDatFeedbackBO.setFlexibility(confirmationSubmitFeedbackInputBean.getFlexibility());
					confDatFeedbackBO.setProactiveness(confirmationSubmitFeedbackInputBean.getProactiveness());
					confDatFeedbackBO.setSelfConfidence(confirmationSubmitFeedbackInputBean.getSelfConfidence());
					confDatFeedbackBO.setTechnicalExpertise(confirmationSubmitFeedbackInputBean.getTechnicalExpertise());
					confDatFeedbackBO.setWillingnessToLearn(confirmationSubmitFeedbackInputBean.getWillingnessToLearn());
					confDatFeedbackBO.setFeedback(confirmationSubmitFeedbackInputBean.getFeedback());
					confDatFeedbackBO.setWorkObjectives(confirmationSubmitFeedbackInputBean.getWorkObjectives());
					confDatFeedbackBO.setAreasOfImprovements(confirmationSubmitFeedbackInputBean.getAreasOfImprovements());
					confDatFeedbackBO.setFkConfId(probationConfirmationBO.getPkConfId());
					confDatFeedbackBO.setFkEmpId(confirmationSubmitFeedbackInputBean.getEmpId());
					confDatFeedbackBO.setUpdatedBy(confirmationSubmitFeedbackInputBean.getLoggedInUser());
					confDatFeedbackBO.setUpdatedOn(new Date());
					confFeedbackRepo.save(confDatFeedbackBO);
					
					probationConfirmationBO.setConfirmedByMgrId(confirmationSubmitFeedbackInputBean.getLoggedInUser());
					probationConfirmationBO.setIsConfirmedByMgr(confirmationSubmitFeedbackInputBean.getIsConfirmedByMgr());
					probationConfirmationBO.setFkConfStatusId(pendingWithHrStatus);
					probationConfirmationBO.setUpdatedBy(confirmationSubmitFeedbackInputBean.getLoggedInUser());
					probationConfirmationBO.setUpdatedOn(new Date());
					flag = true;
					}
					//Added by Harsh for Notifications
					
					try
					{
						
						if(confirmationSubmitFeedbackInputBean.getLoggedInUser().intValue() == probationConfirmationBO.getFkMgrId().intValue() )
						{
							confNotificationService.setFeedbackProvidedNotificationByRMToEmpScreen(confirmationSubmitFeedbackInputBean.getEmpId());
							if(probationConfirmationBO.getDelegatedToMgrId() != null)
								confNotificationService.setFeedbackProvidedByRmNotificationtoDelRmScreen(probationConfirmationBO.getDelegatedToMgrId());
							List<Short> hrRoleIdList = Stream.of(hrRoleIds.split(",")).map(Short::parseShort).collect(Collectors.toList());
							if(hrRoleIdList != null)
							{
								hrRoleIdList.forEach(roleId -> {
									List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId(roleId);
									if(empDetail != null)
									{
										empDetail.forEach(emp -> {
											try {
												confNotificationService.setFeedbackProvidedNotificationByRMToHRScreen(emp.getPkEmpId());
											} catch (Exception e) {
												LOG.info("Unable to set feedback provided by RM notification."+e);
											}
										});
									}
								});
							}
						}
						else if(confirmationSubmitFeedbackInputBean.getLoggedInUser().equals(probationConfirmationBO.getDelegatedToMgrId()))
						{
							confNotificationService.setFeedbackProvidedByDelMgrToEmpScreen(confirmationSubmitFeedbackInputBean.getEmpId());
							confNotificationService.setFeedbackProvidedByDelRmToRmScreen(probationConfirmationBO.getFkMgrId());
							List<Short> hrRoleIdList = Stream.of(hrRoleIds.split(",")).map(Short::parseShort).collect(Collectors.toList());
							if(hrRoleIdList != null)
							{
								hrRoleIdList.forEach(roleId -> {
									List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId(roleId);
									if(empDetail != null)
									{
										empDetail.forEach(emp -> {
											try {
												confNotificationService.setFeedbackProvidedByDelRmToHrScreen(emp.getPkEmpId());
											} catch (Exception e) {
												LOG.info("Unable to set feedback provided by Delegated RM notification."+e);
											}
										});
									}
								});
							}
						}
					}	
					
					catch(Exception ex){
						LOG.info("Unable to set feedback provided by Manager notification."+ex);
					}
					
					//EOA by Harsh for Notifications
				}else{
					throw new CommonCustomException(errorMsg);
				}
				}
			}
			LOG.endUsecase("Manager Feedback ends");
			return flag;
			 
			
		}
		
		
		public boolean updateFeedbackByManager(
				ConfirmationUpdateFeedbackInputBean confirmationUpdateFeedbackInputBean) throws CommonCustomException {
			
			boolean flag = false;
			boolean hasAccess = false;
			
			ConfDatProbationConfirmationBO probationConfirmationBO = new ConfDatProbationConfirmationBO();
			ConfDatFeedbackBO confDatFeedbackBO = new ConfDatFeedbackBO();
			DatEmpProfessionalDetailBO profDetailBO = new DatEmpProfessionalDetailBO();
			ConfDatFeedbackHistoryBO confDatFeedbackHistoryBO = new ConfDatFeedbackHistoryBO();
			LOG.startUsecase("Update Manager Feedback");
			
			try{				
				confDatFeedbackBO = confFeedbackRepo.findByPkFeedbackId(confirmationUpdateFeedbackInputBean.getPkFeedbackId().intValue());
			}
			catch(Exception e){
				throw new CommonCustomException("Failed to fetch feedback details from DB");
			}
			
			if(confDatFeedbackBO == null){
				throw new CommonCustomException("Feedback Id Not Found in DB");
			}
			
			try{				
				probationConfirmationBO = confCommonRepo.findByFkEmpId(confirmationUpdateFeedbackInputBean.getEmpId());
			}
			catch(Exception e){
				throw new CommonCustomException("Failed to fetch confirmation details from DB");
			}
			
			if(probationConfirmationBO == null){
				throw new CommonCustomException("Invalid employee id.");
			}
			
			try{
				profDetailBO = profDetailRepo.findByFkMainEmpDetailId(confirmationUpdateFeedbackInputBean.getEmpId());
			}catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from DB");
			}
			
			if(probationConfirmationBO.getFkConfStatusId() == confirmedStatus)
			{
				throw new CommonCustomException("The employee is already confirmed. Cannot Update Feedback.");
			}
			
			
			
			else{
					
				DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
				try
				{
					datEmpDetailBO = empDetailRepo.findByPkEmpId(confirmationUpdateFeedbackInputBean.getLoggedInUser());
				}
				catch(Exception ex){
					throw new CommonCustomException("Unable to get employee details from backend.");
				}
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(probationConfirmationBO.getTentativeConfirmationDate());
				cal.add(Calendar.DAY_OF_MONTH, - activeFeedbackDays);
				Date confDateMinus21Days = cal.getTime();
				
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				String validFeedbackDate = df.format(confDateMinus21Days);
				String tentconfdate = df.format(probationConfirmationBO.getTentativeConfirmationDate());
				String currentDate = df.format(new Date());
				Date currDate = new Date();						
								
				String errorMsg="Error in submitting feedback.";
				String loggedInUserName = goalService.getEmpNameByEmpId(confDatFeedbackBO.getUpdatedBy());
				System.out.println("!!!!!!loggedInUserName@@@@@@@"+loggedInUserName);
				System.out.println("++++++confDatFeedbackBO.getUpdatedBy()"+confDatFeedbackBO.getUpdatedBy());
				if(probationConfirmationBO.getUpdatedBy() == null || probationConfirmationBO.getUpdatedBy().intValue() == confirmationUpdateFeedbackInputBean.getLoggedInUser().intValue() )
				{
					if(confirmationUpdateFeedbackInputBean.getLoggedInUser().intValue() == probationConfirmationBO.getFkMgrId().intValue() )
					{
						if(validFeedbackDate.equals(currentDate) || currDate.after(confDateMinus21Days))
						{
						
							hasAccess = true;
													
						}
						else {
							
							errorMsg = "Feedback can only be updated within a period of "+activeFeedbackDays+" days before the Date of Confirmation";
						}
					}
					else if(probationConfirmationBO.getDelegatedToMgrId() != null && confirmationUpdateFeedbackInputBean.getLoggedInUser().intValue() == probationConfirmationBO.getDelegatedToMgrId().intValue())
					{
						
						Calendar cal2 = Calendar.getInstance();
						cal2.setTime(probationConfirmationBO.getDelegatedDate());
						int workDays = 2;
						int dow = cal2.get (Calendar.DAY_OF_WEEK);
						if(dow == Calendar.THURSDAY || dow == Calendar.FRIDAY) {
							workDays =4;
						}
						cal2.add(Calendar.DAY_OF_MONTH, workDays);
						
						Date deligatedDatePlus2Days = cal2.getTime();
						String validDelFeedbackDate = df.format(deligatedDatePlus2Days);
						
						if(currentDate.equals(validDelFeedbackDate) || deligatedDatePlus2Days.after(currDate))
						{
							hasAccess = true;
						}
						else {
							
							errorMsg = "Access denied to update Feedback since the deadline of 2 days has passed.";
						}
					}
					else {
						
						errorMsg = "Access denied to update Feedback since you are not an authorized person.";
						System.out.println("--------confirmationUpdateFeedbackInputBean.getLoggedInUser()++++++++"+confirmationUpdateFeedbackInputBean.getLoggedInUser());
						System.out.println("##########probationConfirmationBO.getDelegatedToMgrId():"+probationConfirmationBO.getDelegatedToMgrId());
					}
				}
				else {
					
					errorMsg = "Cannot update Feedback since "+loggedInUserName+" has already reviewed it and provided feedback.";
				}
				
				if(probationConfirmationBO.getUpdatedCount() != null && probationConfirmationBO.getUpdatedCount() > 2) {
					throw new CommonCustomException("Cannot update feedback more that three times for an employee.");
				}
				if(hasAccess){
				
					// Insert to History table 
					if(probationConfirmationBO.getActualDateOfConfirmation() != null) {
						confDatFeedbackHistoryBO.setActualDateOfConfirmation(probationConfirmationBO.getActualDateOfConfirmation());
					}
					
					if(probationConfirmationBO.getConfExtendedInMonths() != null) {
						confDatFeedbackHistoryBO.setConfExtendedInMonths(probationConfirmationBO.getConfExtendedInMonths());
						confDatFeedbackHistoryBO.setReasonForExtension(probationConfirmationBO.getReasonForExtension());
						confDatFeedbackHistoryBO.setConfExtendedByHrId(probationConfirmationBO.getConfExtendedByHrId());
					}
					
					if(probationConfirmationBO.getDelegatedToMgrId() != null) {
						confDatFeedbackHistoryBO.setDelegatedByHrId(probationConfirmationBO.getDelegatedByHrId());
						confDatFeedbackHistoryBO.setDelegatedComments(probationConfirmationBO.getDelegatedComments());
						confDatFeedbackHistoryBO.setDelegatedDate(probationConfirmationBO.getDelegatedDate());
						confDatFeedbackHistoryBO.setDelegatedToMgrId(probationConfirmationBO.getDelegatedToMgrId());
					}
					
					if(probationConfirmationBO.getIsConfirmedByHr() != null) {
						if(probationConfirmationBO.getIsConfirmedByHr().equals("1")) {
							
							confDatFeedbackHistoryBO.setConfirmedByHrId(probationConfirmationBO.getConfirmedByHrId());
						}
						confDatFeedbackHistoryBO.setIsConfirmedByHr(probationConfirmationBO.getIsConfirmedByHr());
					}
					
					if(probationConfirmationBO.getIsConfirmedByMgr() != null) {
						if(probationConfirmationBO.getIsConfirmedByMgr().equals("0")) {
							confDatFeedbackHistoryBO.setMgrDenialReason(probationConfirmationBO.getMgrDenialReason());
							LOG.info("++++++++++mgr denial reason :"+probationConfirmationBO.getMgrDenialReason());
						}
						else {
							confDatFeedbackHistoryBO.setConfirmedByMgrId(probationConfirmationBO.getConfirmedByMgrId());
						}
					confDatFeedbackHistoryBO.setIsConfirmedByMgr(probationConfirmationBO.getIsConfirmedByMgr());					
										
					}
					
					confDatFeedbackHistoryBO.setFkConfId(probationConfirmationBO.getPkConfId());
					confDatFeedbackHistoryBO.setFkConfStatusId(probationConfirmationBO.getFkConfStatusId());
					confDatFeedbackHistoryBO.setFkEmpId(probationConfirmationBO.getFkEmpId());
					confDatFeedbackHistoryBO.setFkMgrId(probationConfirmationBO.getFkMgrId());
					confDatFeedbackHistoryBO.setTentativeConfirmationDate(probationConfirmationBO.getTentativeConfirmationDate());
										
					confDatFeedbackHistoryBO.setLevel(profDetailBO.getFkEmpLevel());
					confDatFeedbackHistoryBO.setDesignation(profDetailBO.getFkEmpDesignation());
					
					confDatFeedbackHistoryBO.setFkFeedbackId(confDatFeedbackBO.getPkFeedbackId());					
					confDatFeedbackHistoryBO.setAreasOfImprovements(confDatFeedbackBO.getAreasOfImprovements());
					confDatFeedbackHistoryBO.setAttitude(confDatFeedbackBO.getAttitude());
					confDatFeedbackHistoryBO.setCommunication(confDatFeedbackBO.getCommunication());
					confDatFeedbackHistoryBO.setDependability(confDatFeedbackBO.getDependability());
					confDatFeedbackHistoryBO.setFeedback(confDatFeedbackBO.getFeedback());					
					confDatFeedbackHistoryBO.setFlexibility(confDatFeedbackBO.getFlexibility());					
					confDatFeedbackHistoryBO.setProactiveness(confDatFeedbackBO.getProactiveness());					
					confDatFeedbackHistoryBO.setSelfConfidence(confDatFeedbackBO.getSelfConfidence());					
					confDatFeedbackHistoryBO.setTechnicalExpertise(confDatFeedbackBO.getTechnicalExpertise());
					confDatFeedbackHistoryBO.setWillingnessToLearn(confDatFeedbackBO.getWillingnessToLearn());
					confDatFeedbackHistoryBO.setWorkObjectives(confDatFeedbackBO.getWorkObjectives());
					confDatFeedbackHistoryBO.setUpdatedBy(confDatFeedbackBO.getUpdatedBy());
					confDatFeedbackHistoryBO.setUpdatedOn(confDatFeedbackBO.getUpdatedOn());
					confDatFeedbackHistoryBO.setStatus("updated");
					confFeedbackHistoryRepo.save(confDatFeedbackHistoryBO);	
					
					// EOA for History Table
					
					if(probationConfirmationBO.getUpdatedCount() != null && probationConfirmationBO.getUpdatedCount() > 2) {
						throw new CommonCustomException("Cannot update feedback more that three times for an employee.");
					}
					
					if(confirmationUpdateFeedbackInputBean.getIsConfirmedByMgr().equals("0")  && confirmationUpdateFeedbackInputBean.getMgrDenialReason() != null) {
						probationConfirmationBO.setMgrDenialReason(confirmationUpdateFeedbackInputBean.getMgrDenialReason());
					}
					else if(confirmationUpdateFeedbackInputBean.getIsConfirmedByMgr().equals("1") && confirmationUpdateFeedbackInputBean.getConfirmedByMgrId() != null){
						probationConfirmationBO.setConfirmedByMgrId(confirmationUpdateFeedbackInputBean.getConfirmedByMgrId());
					}
					else {
						throw new CommonCustomException("Please provide all the required inputs.");
					}
					
					
					confDatFeedbackBO.setAttitude(confirmationUpdateFeedbackInputBean.getAttitude());
					confDatFeedbackBO.setCommunication(confirmationUpdateFeedbackInputBean.getCommunication());
					confDatFeedbackBO.setDependability(confirmationUpdateFeedbackInputBean.getDependability());
					confDatFeedbackBO.setFlexibility(confirmationUpdateFeedbackInputBean.getFlexibility());
					confDatFeedbackBO.setProactiveness(confirmationUpdateFeedbackInputBean.getProactiveness());
					confDatFeedbackBO.setSelfConfidence(confirmationUpdateFeedbackInputBean.getSelfConfidence());
					confDatFeedbackBO.setTechnicalExpertise(confirmationUpdateFeedbackInputBean.getTechnicalExpertise());
					confDatFeedbackBO.setWillingnessToLearn(confirmationUpdateFeedbackInputBean.getWillingnessToLearn());
					confDatFeedbackBO.setFeedback(confirmationUpdateFeedbackInputBean.getFeedback());
					confDatFeedbackBO.setWorkObjectives(confirmationUpdateFeedbackInputBean.getWorkObjectives());
					confDatFeedbackBO.setAreasOfImprovements(confirmationUpdateFeedbackInputBean.getAreasOfImprovements());
					confDatFeedbackBO.setFkConfId(probationConfirmationBO.getPkConfId());
					confDatFeedbackBO.setFkEmpId(confirmationUpdateFeedbackInputBean.getEmpId());
					confDatFeedbackBO.setUpdatedBy(confirmationUpdateFeedbackInputBean.getLoggedInUser());
					confDatFeedbackBO.setUpdatedOn(new Date());
							
					probationConfirmationBO.setIsConfirmedByMgr(confirmationUpdateFeedbackInputBean.getIsConfirmedByMgr());
					probationConfirmationBO.setFkConfStatusId(pendingWithHrStatus);
					probationConfirmationBO.setUpdatedBy(confirmationUpdateFeedbackInputBean.getLoggedInUser());
					probationConfirmationBO.setUpdatedOn(new Date());
					if(probationConfirmationBO.getUpdatedCount() != null) {
						probationConfirmationBO.setUpdatedCount(probationConfirmationBO.getUpdatedCount()+1);
					}
					else {
						probationConfirmationBO.setUpdatedCount(1);
					}
					flag = true;
					
				}else{
					throw new CommonCustomException(errorMsg);
				}
				
			}
			LOG.endUsecase("Manager Update Feedback Ends");
			return flag;
		
		}
		//EOA by Harsh
}
