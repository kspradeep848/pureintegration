package com.thbs.mis.confirmation.service;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.log.Log;
import com.thbs.mis.common.bean.EmployeeNameWithRMIdBean;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.confirmation.bean.ConfirmStatusByHrInputBean;
import com.thbs.mis.confirmation.bean.ConfirmationSubmitHRFeedbackInputBean;
import com.thbs.mis.confirmation.bean.ConfirmationUpdateHRFeedbackInputBean;
import com.thbs.mis.confirmation.bean.DelegateManagerByHrInputBean;
import com.thbs.mis.confirmation.bean.ExtendProbationByHrInputBean;
import com.thbs.mis.confirmation.bean.GenerateConfExtLetterOutputBean;
import com.thbs.mis.confirmation.bean.NotConfirmedByHrInputBean;
import com.thbs.mis.confirmation.bo.ConfDatFeedbackBO;
import com.thbs.mis.confirmation.bo.ConfDatFeedbackHistoryBO;
import com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO;
import com.thbs.mis.confirmation.dao.ConfirmationCommonRepository;
import com.thbs.mis.confirmation.dao.ConfirmationFeedbackHistoryRepository;
import com.thbs.mis.confirmation.dao.ConfirmationFeedbackRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.mail.EmailServiceUtil;
import com.thbs.mis.framework.util.DateTimeUtil;
import com.thbs.mis.gsr.service.GSRGoalService;
import com.thbs.mis.notificationframework.confirmationnotification.service.ConfirmationNotificationService;


@Service
@Transactional
public class ConfirmationByHRService {

	private static final AppLog LOG = LogFactory
			.getLog(ConfirmationByHRService.class);
	
	@Autowired
	private ConfirmationCommonRepository confCommonRepo;
	
	@Autowired
	private ConfirmationFeedbackRepository confFeedbackRepo;
	
	@Autowired
	private EmpDetailRepository empDetailRepo;
	
	@Autowired
	private EmployeeProfessionalRepository profDetailRepository;
	
	@Autowired
	private DatEmployeeProfessionalRepository profDetailRepo;
	
	@Autowired
	private EmployeeProfessionalRepository employeeProfessionalRepository;
	
	@Autowired
	private GSRGoalService goalService;
	
	@Autowired
	private EmailServiceUtil misEmailService;
	
	@Autowired
	private ConfirmationNotificationService confNotificationService;
	
	@Autowired
	private ConfirmationEmployeeService confEmpService;
		
	@Autowired
	private EmpDetailRepository empDetailRepository;
	
	@Autowired
	private ConfirmationFeedbackHistoryRepository confFeedbackHistoryRepo;
	
	@Autowired
	private CommonService commonService;

	
	@Value("${confirmation.confirmed.status}")
	private Integer confirmedStatus;
	
	@Value("${confirmation.extended.probation.status}")
	private Integer extendedProbationStatus;
	
	@Value("${confirmation.pendingWithHr.status}")
	private Integer pendingWithHrStatus;
	
	@Value("${confirmation.pendingWithManager.status}")
	private Integer pendingWithManagerStatus;
	
	@Value("${confirmation.underProbation.status}")
	private Integer underProbationStatus;
	
	@Value("${hr.mail.id}")
	private String hrMailId;
	
	@Value("${hr.role.id}")
	private String hrRoleIds;
	
	@Value("${confirmation.template}")
	private File confirmationLetter;
	
	@Value("${extension.template}")
	private File extensionLetter;

	@Value("${conf.ext.letter.basepath}")
	private File confLetterBasePath;
	
	@Value("${employee.not.include}")
	private String empIds;
	
	@Value("${conf.notconfirmed.status}")
	private Integer notConfirmedStatus; 
	

	//Added by Smrithi
	
	public ConfDatProbationConfirmationBO extendProbationByHr(
			ExtendProbationByHrInputBean extendProbationByHrInputBean) throws CommonCustomException {

		ConfDatProbationConfirmationBO probationConfirmationBO = new ConfDatProbationConfirmationBO();
		DatEmpProfessionalDetailBO profDetailBO = new DatEmpProfessionalDetailBO();
		ConfDatFeedbackBO feedbackBO = new ConfDatFeedbackBO();
		ConfDatFeedbackHistoryBO confDatFeedbackHistoryBO = new ConfDatFeedbackHistoryBO();
		List<Short> hrRoleIdList = Stream.of(hrRoleIds.split(","))
                .map(Short::parseShort)
                .collect(Collectors.toList());
		
		try{
			profDetailBO = profDetailRepo.findByFkMainEmpDetailId(extendProbationByHrInputBean.getFkEmpId());
			
		}
		catch(Exception ex){
			throw new CommonCustomException("Unable to get employee details from backend.");
		}
		Integer probMonths;
		if( profDetailBO.getProbationMonths() != null) 
		{
			probMonths = profDetailBO.getProbationMonths() + extendProbationByHrInputBean.getConfExtendedInMonths();
		}else {
			probMonths = extendProbationByHrInputBean.getConfExtendedInMonths();
		}
		try{
			probationConfirmationBO = confCommonRepo.findByFkEmpId(extendProbationByHrInputBean.getFkEmpId());
			
		}catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		
		if(probationConfirmationBO == null){
			throw new CommonCustomException("Invalid employee id.");
		}
		 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String tentconfdate = df.format(probationConfirmationBO.getTentativeConfirmationDate());
		 String currDate = df.format(new Date());
		if(probationConfirmationBO.getFkConfStatusId() == confirmedStatus)
		{
			throw new CommonCustomException("The employee is already confirmed. Hence probation cannot be extended.");
		}else if (probationConfirmationBO.getFkConfStatusId() == extendedProbationStatus){
			throw new CommonCustomException("Sorry the employee is already in extended probation period.");
		}else{
			probationConfirmationBO.setFkEmpId(extendProbationByHrInputBean.getFkEmpId());
			
			DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
			try{
				
				datEmpDetailBO = empDetailRepo.findByPkEmpId(extendProbationByHrInputBean.getConfExtendedByHrId());
			}
			catch(Exception ex){
				throw new CommonCustomException("Unable to get employee details from backend.");
			}
			try{
				feedbackBO = confFeedbackRepo.findByFkEmpId(extendProbationByHrInputBean.getFkEmpId());
			}catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from feedback table");
			}
			
			if(hrRoleIdList.contains(datEmpDetailBO.getFkEmpRoleId()) )
			{
				probationConfirmationBO.setConfExtendedByHrId(extendProbationByHrInputBean.getConfExtendedByHrId());
			}else{
				throw new CommonCustomException("Access denied to extend probation since you are not an authorized person.");
			}
			if(extendProbationByHrInputBean.getConfExtendedByHrId().intValue() == extendProbationByHrInputBean.getFkEmpId().intValue()){
				throw new CommonCustomException("Access denied since you cannot extend your own confirmation.");
			}
			if(extendProbationByHrInputBean.getConfExtendedInMonths() >= 4){
				throw new CommonCustomException("Probation can be extended only for 3 months.");
			}
			/*if (    probationConfirmationBO.getFkConfStatusId() == pendingWithHrStatus ||
					probationConfirmationBO.getFkConfStatusId() == pendingWithManagerStatus || 
					probationConfirmationBO.getFkConfStatusId() == notConfirmedStatus ){*/
			 if (tentconfdate.equals(currDate) || new Date().after(probationConfirmationBO.getTentativeConfirmationDate()))
				{
				if (feedbackBO == null)
					{
					throw new CommonCustomException("Sorry feedback not provided, hence extension cannot be done");
					}
				else
					{
					
					
					
					probationConfirmationBO.setConfExtendedInMonths(extendProbationByHrInputBean.getConfExtendedInMonths());
					probationConfirmationBO.setFkConfStatusId(extendedProbationStatus);
					probationConfirmationBO.setReasonForExtension(extendProbationByHrInputBean.getReasonForExtension());
					probationConfirmationBO.setUpdatedBy(extendProbationByHrInputBean.getConfExtendedByHrId());
					probationConfirmationBO.setUpdatedOn(new Date());
					probationConfirmationBO.setIsConfirmedByHr("0");
					
					profDetailBO.setProfessionalDetailModifiedDate(new Date());
					profDetailBO.setProfessionalDetailModifiedBy(extendProbationByHrInputBean.getConfExtendedByHrId());
					//do calc for prob months field n update it.
					profDetailBO.setProbationMonths(probMonths);
					
					Calendar cal = Calendar.getInstance();
					cal.setTime(probationConfirmationBO.getTentativeConfirmationDate());
					cal.add(Calendar.MONTH, +probationConfirmationBO.getConfExtendedInMonths());
					Date calcTentativeConfDate = cal.getTime();
					System.out.println("++++++++++++date+++++++++++++++"+calcTentativeConfDate);
					probationConfirmationBO.setTentativeConfirmationDate(calcTentativeConfDate);
					
					try {
						sendMailByHr(extendProbationByHrInputBean.getFkEmpId());
						System.out.println("Coming here after extending!!!!!!!!!!!");
					} catch (Exception e) {
						LOG.info("Error in sending email."+e);
					}
					
					//Added by Harsh for Notifications
					try {
						confNotificationService.setConfirmationExtendedNotificationToEmpScreen(extendProbationByHrInputBean.getFkEmpId());
						confNotificationService.setConfirmationExtendedByHrNotificationToRmScreen(probationConfirmationBO.getFkMgrId());
						if(probationConfirmationBO.getDelegatedToMgrId()!=null) {
							confNotificationService.setConfirmationExtendedByHrNotificationToDelRmScreen(probationConfirmationBO.getDelegatedToMgrId());
						}
						
					} catch (Exception e) {
						LOG.info("Unable to set Extend Probation By HR notification."+e);
					}
					//EOA by Harsh for Notifications
					// Insert to History table 
					if(probationConfirmationBO.getActualDateOfConfirmation() != null) {
						confDatFeedbackHistoryBO.setActualDateOfConfirmation(probationConfirmationBO.getActualDateOfConfirmation());
					}
					
					if(probationConfirmationBO.getConfExtendedInMonths() != null) {
						confDatFeedbackHistoryBO.setConfExtendedInMonths(probationConfirmationBO.getConfExtendedInMonths());
						confDatFeedbackHistoryBO.setReasonForExtension(probationConfirmationBO.getReasonForExtension());
						confDatFeedbackHistoryBO.setConfExtendedByHrId(probationConfirmationBO.getConfExtendedByHrId());
					}
					
					if(probationConfirmationBO.getDelegatedToMgrId() != null) {
						confDatFeedbackHistoryBO.setDelegatedByHrId(probationConfirmationBO.getDelegatedByHrId());
						confDatFeedbackHistoryBO.setDelegatedComments(probationConfirmationBO.getDelegatedComments());
						confDatFeedbackHistoryBO.setDelegatedDate(probationConfirmationBO.getDelegatedDate());
						confDatFeedbackHistoryBO.setDelegatedToMgrId(probationConfirmationBO.getDelegatedToMgrId());
					}
					
					if(probationConfirmationBO.getIsConfirmedByHr() != null) {
						if(probationConfirmationBO.getIsConfirmedByHr().equals("1") ){
							
							confDatFeedbackHistoryBO.setConfirmedByHrId(probationConfirmationBO.getConfirmedByHrId());
						}
						confDatFeedbackHistoryBO.setIsConfirmedByHr(probationConfirmationBO.getIsConfirmedByHr());
					}
					
					if(probationConfirmationBO.getIsConfirmedByMgr() != null) {
						if(probationConfirmationBO.getIsConfirmedByMgr().equals("0") ){
							confDatFeedbackHistoryBO.setMgrDenialReason(probationConfirmationBO.getMgrDenialReason());
						}
						else {
							confDatFeedbackHistoryBO.setConfirmedByMgrId(probationConfirmationBO.getConfirmedByMgrId());
						}
					confDatFeedbackHistoryBO.setIsConfirmedByMgr(probationConfirmationBO.getIsConfirmedByMgr());					
										
					}
					
					confDatFeedbackHistoryBO.setFkConfId(probationConfirmationBO.getPkConfId());
					confDatFeedbackHistoryBO.setFkConfStatusId(probationConfirmationBO.getFkConfStatusId());
					confDatFeedbackHistoryBO.setFkEmpId(probationConfirmationBO.getFkEmpId());
					confDatFeedbackHistoryBO.setFkMgrId(probationConfirmationBO.getFkMgrId());
					confDatFeedbackHistoryBO.setTentativeConfirmationDate(probationConfirmationBO.getTentativeConfirmationDate());
										
					confDatFeedbackHistoryBO.setLevel(profDetailBO.getFkEmpLevel());
					confDatFeedbackHistoryBO.setDesignation(profDetailBO.getFkEmpDesignation());
					
					//confDatFeedbackHistoryBO.setFkFeedbackId(feedbackBO.getPkFeedbackId());
					
					if(probationConfirmationBO.getUpdatedBy() != null)
						confDatFeedbackHistoryBO.setUpdatedBy(probationConfirmationBO.getUpdatedBy());
					if(probationConfirmationBO.getUpdatedOn() != null )
						confDatFeedbackHistoryBO.setUpdatedOn(probationConfirmationBO.getUpdatedOn());
					
					confDatFeedbackHistoryBO.setStatus("updated");
					
					
					confDatFeedbackHistoryBO.setFkConfId(probationConfirmationBO.getPkConfId());
					confDatFeedbackHistoryBO.setFkConfStatusId(probationConfirmationBO.getFkConfStatusId());
					confDatFeedbackHistoryBO.setFkEmpId(probationConfirmationBO.getFkEmpId());
					confDatFeedbackHistoryBO.setFkMgrId(probationConfirmationBO.getFkMgrId());
					confDatFeedbackHistoryBO.setTentativeConfirmationDate(probationConfirmationBO.getTentativeConfirmationDate());
										
					confDatFeedbackHistoryBO.setLevel(profDetailBO.getFkEmpLevel());
					confDatFeedbackHistoryBO.setDesignation(profDetailBO.getFkEmpDesignation());
					
					confDatFeedbackHistoryBO.setFkFeedbackId(feedbackBO.getPkFeedbackId());					
					confDatFeedbackHistoryBO.setAreasOfImprovements(feedbackBO.getAreasOfImprovements());
					confDatFeedbackHistoryBO.setAttitude(feedbackBO.getAttitude());
					confDatFeedbackHistoryBO.setCommunication(feedbackBO.getCommunication());
					confDatFeedbackHistoryBO.setDependability(feedbackBO.getDependability());
					confDatFeedbackHistoryBO.setFeedback(feedbackBO.getFeedback());					
					confDatFeedbackHistoryBO.setFlexibility(feedbackBO.getFlexibility());					
					confDatFeedbackHistoryBO.setProactiveness(feedbackBO.getProactiveness());					
					confDatFeedbackHistoryBO.setSelfConfidence(feedbackBO.getSelfConfidence());					
					confDatFeedbackHistoryBO.setTechnicalExpertise(feedbackBO.getTechnicalExpertise());
					confDatFeedbackHistoryBO.setWillingnessToLearn(feedbackBO.getWillingnessToLearn());
					confDatFeedbackHistoryBO.setWorkObjectives(feedbackBO.getWorkObjectives());
					confDatFeedbackHistoryBO.setUpdatedBy(feedbackBO.getUpdatedBy());
					confDatFeedbackHistoryBO.setUpdatedOn(feedbackBO.getUpdatedOn());
					confDatFeedbackHistoryBO.setStatus("updated");
					confFeedbackHistoryRepo.save(confDatFeedbackHistoryBO);	
					
					
					// EOA for History Table
				
					//Reset Fields for Next Feedback Cycle
					probationConfirmationBO.setDelegatedByHrId(null);
					probationConfirmationBO.setDelegatedToMgrId(null);
					probationConfirmationBO.setDelegatedDate(null);
					probationConfirmationBO.setDelegatedComments(null);
					probationConfirmationBO.setUpdatedCount(0);
					probationConfirmationBO.setTentativeConfirmationDate(calcTentativeConfDate);
					probationConfirmationBO.setUpdatedBy(null);		
					probationConfirmationBO.setConfirmationLetterSent(null);
					probationConfirmationBO.setExtensionLetterSent(null);
					
					
					
				/*	//Added by Harsh for Notifications
					try {
						confNotificationService.setConfirmationExtendedNotificationToEmpScreen(extendProbationByHrInputBean.getFkEmpId());
						confNotificationService.setConfirmationExtendedByHrNotificationToRmScreen(probationConfirmationBO.getFkMgrId());
						if(probationConfirmationBO.getDelegatedToMgrId()!=null) {
							confNotificationService.setConfirmationExtendedByHrNotificationToDelRmScreen(probationConfirmationBO.getDelegatedToMgrId());
						}
						
					} catch (Exception e) {
						LOG.info("Unable to set Extend Probation By HR notification."+e);
					}
					//EOA by Harsh for Notifications
*/					
					
					}
				
				}
			else
					{
						throw new CommonCustomException("Sorry, Extension can only be done on or after tentative date of confirmation.");
					}
			
			/*}	else{
				throw new CommonCustomException("In order to extend an employee he/she should be in status 'pending with manager' or 'pending with HR'  or 'not confirmed' ");
			}*/
		}
		
		return probationConfirmationBO;
		
		
	}

	public boolean confirmStatusByHr(
			ConfirmStatusByHrInputBean confirmStatusByHrInputBean) throws CommonCustomException {
		ConfDatProbationConfirmationBO probationConfirmationBO = new ConfDatProbationConfirmationBO();
		ConfDatFeedbackBO feedbackBO = new ConfDatFeedbackBO();
		ConfDatFeedbackHistoryBO confDatFeedbackHistoryBO = new ConfDatFeedbackHistoryBO();
		DatEmpProfessionalDetailBO saveProfDetail = new DatEmpProfessionalDetailBO();
		List<Short> hrRoleIdList = Stream.of(hrRoleIds.split(","))
                .map(Short::parseShort)
                .collect(Collectors.toList());
		LOG.startUsecase("Confirm Service");
		try{
			probationConfirmationBO = confCommonRepo.findByFkEmpId(confirmStatusByHrInputBean.getFkEmpId());
			
		}catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}if(probationConfirmationBO == null){
			throw new CommonCustomException("Invalid employee id.");
		}
		 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String tentconfdate = df.format(probationConfirmationBO.getTentativeConfirmationDate());
		 String currDate = df.format(new Date());
		if(probationConfirmationBO.getFkConfStatusId() == confirmedStatus ){
			throw new CommonCustomException("The employee is already confirmed.");
		}
		/*else if (probationConfirmationBO.getFkConfStatusId() == underProbationStatus){
			throw new CommonCustomException("The employee cannot be confirmed because no feedback is provided.");
		}*/
		else if (probationConfirmationBO.getFkConfStatusId() == extendedProbationStatus ||
				probationConfirmationBO.getFkConfStatusId() == pendingWithHrStatus ||
				probationConfirmationBO.getFkConfStatusId() == pendingWithManagerStatus || 
				probationConfirmationBO.getFkConfStatusId() == notConfirmedStatus ){
			if (tentconfdate.equals(currDate) || new Date().after(probationConfirmationBO.getTentativeConfirmationDate()))
			{
				
				try
				{
					feedbackBO = confFeedbackRepo.findByFkEmpId(confirmStatusByHrInputBean.getFkEmpId());
				}
				catch(Exception e)
				{
					throw new CommonCustomException("Failed to fetch details from feedback table");
				}
				if (feedbackBO == null)
					{
				throw new CommonCustomException("Manager Feedback not provided, hence confirmation cannot be done");
					}
				else
					{
					
			
					
						DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
						try{
				
							datEmpDetailBO = empDetailRepo.findByPkEmpId(confirmStatusByHrInputBean.getLoggedInUser());
						    }
						catch(Exception ex)
						{
							throw new CommonCustomException("Unable to get employee details from backend.");
						}
			
						if(hrRoleIdList.contains(datEmpDetailBO.getFkEmpRoleId()) )
						{
							//probationConfirmationBO.setConfirmedByHrId(confirmStatusByHrInputBean.getLoggedInUser());
						}else{
							throw new CommonCustomException("Access denied since you are not an authorized person.");
						}
						DatEmpProfessionalDetailBO profDetailBO = new DatEmpProfessionalDetailBO();
						try{
							profDetailBO = profDetailRepo.findByFkMainEmpDetailId(confirmStatusByHrInputBean.getFkEmpId());
						}
						catch(Exception ex){
							throw new CommonCustomException("Unable to get employee details from backend.");
						}
						 Short preConfLevel = profDetailBO.getFkEmpLevel();
						 Short preDesignationLevel = profDetailBO.getFkEmpDesignation();
						Short pk = 2;
						if(confirmStatusByHrInputBean.getFkEmpLevel() < preConfLevel){
							confFeedbackRepo.delete(feedbackBO);
							throw new CommonCustomException("Enter a level equal or above the pre-confirmation level");
							
						}
						if(confirmStatusByHrInputBean.getLoggedInUser().intValue() == confirmStatusByHrInputBean.getFkEmpId().intValue()){
							throw new CommonCustomException("Access denied since you can provide your own feedback.");
						}
						if(profDetailBO.getFkEmpLevel() == 1)
						{
							if(confirmStatusByHrInputBean.getFkEmpLevel() != 2)
							{
							
								throw new CommonCustomException("Sorry, The employee default status will be D1 only. ");
							}
						
						}else{
								pk = confirmStatusByHrInputBean.getFkEmpLevel();
								//profDetailBO.setFkEmpLevel(confirmStatusByHrInputBean.getFkEmpLevel());
						}
						//probationConfirmationBO.setFkEmpId(confirmStatusByHrInputBean.getFkEmpId());
						
						if(confirmStatusByHrInputBean.getActualDateOFConf().after(probationConfirmationBO.getTentativeConfirmationDate()) || confirmStatusByHrInputBean.getActualDateOFConf().equals(probationConfirmationBO.getTentativeConfirmationDate()))
						{
							
							 Calendar aDate = Calendar.getInstance();
							    aDate.setTime(confirmStatusByHrInputBean.getActualDateOFConf());

							    Calendar aTime = Calendar.getInstance();
							    aTime.setTime(new Date());

							    Calendar aDateTime = Calendar.getInstance();
							    aDateTime.set(Calendar.DAY_OF_MONTH, aDate.get(Calendar.DAY_OF_MONTH));
							    aDateTime.set(Calendar.MONTH, aDate.get(Calendar.MONTH));
							    aDateTime.set(Calendar.YEAR, aDate.get(Calendar.YEAR));
							    aDateTime.set(Calendar.HOUR, aTime.get(Calendar.HOUR));
							    aDateTime.set(Calendar.MINUTE, aTime.get(Calendar.MINUTE));
							probationConfirmationBO.setActualDateOfConfirmation(aDateTime.getTime());
							profDetailBO.setEmpDateOfConfirmation(confirmStatusByHrInputBean.getActualDateOFConf());
						}else{
							throw new CommonCustomException("Actual Conf Date can only be after or same as tentative date of confirmation.");
						}
						
						profDetailBO.setFkEmpDesignation(confirmStatusByHrInputBean.getFkEmpDesignation());
						profDetailBO.setProfessionalDetailModifiedBy(confirmStatusByHrInputBean.getLoggedInUser());
						profDetailBO.setProfessionalDetailModifiedDate(new Date());
						probationConfirmationBO.setUpdatedBy(confirmStatusByHrInputBean.getLoggedInUser());
						probationConfirmationBO.setUpdatedOn(new Date());
						probationConfirmationBO.setFkConfStatusId(confirmedStatus);
						probationConfirmationBO.setPreConfirmationLevel(preConfLevel);
						probationConfirmationBO.setIsConfirmedByHr("1");
						probationConfirmationBO.setConfirmedByHrId(confirmStatusByHrInputBean.getLoggedInUser());
						probationConfirmationBO.setPreDesignationLevel(preDesignationLevel);
						probationConfirmationBO.setPostDesignationLevel(confirmStatusByHrInputBean.getFkEmpDesignation());
						//probationConfirmationBO.setPostConfirmationLevel(confirmStatusByHrInputBean.getFkEmpLevel());
						//Added by Harsh
						profDetailBO.setFkEmpLevel(pk);
						probationConfirmationBO.setPostConfirmationLevel(pk);
						saveProfDetail = profDetailRepo.save(profDetailBO);
						confCommonRepo.save(probationConfirmationBO);
						LOG.info("saveProfDetail:"+saveProfDetail.toString());
						//EOA by Harsh
					
						// Insert to History table 
						if(probationConfirmationBO.getActualDateOfConfirmation() != null) {
							confDatFeedbackHistoryBO.setActualDateOfConfirmation(probationConfirmationBO.getActualDateOfConfirmation());
						}
						
						if(probationConfirmationBO.getConfExtendedInMonths() != null) {
							confDatFeedbackHistoryBO.setConfExtendedInMonths(probationConfirmationBO.getConfExtendedInMonths());
							confDatFeedbackHistoryBO.setReasonForExtension(probationConfirmationBO.getReasonForExtension());
							confDatFeedbackHistoryBO.setConfExtendedByHrId(probationConfirmationBO.getConfExtendedByHrId());
						}
						
						if(probationConfirmationBO.getDelegatedToMgrId() != null) {
							confDatFeedbackHistoryBO.setDelegatedByHrId(probationConfirmationBO.getDelegatedByHrId());
							confDatFeedbackHistoryBO.setDelegatedComments(probationConfirmationBO.getDelegatedComments());
							confDatFeedbackHistoryBO.setDelegatedDate(probationConfirmationBO.getDelegatedDate());
							confDatFeedbackHistoryBO.setDelegatedToMgrId(probationConfirmationBO.getDelegatedToMgrId());
						}
						
						if(probationConfirmationBO.getIsConfirmedByHr() != null) {
							if(probationConfirmationBO.getIsConfirmedByHr().equals("1") ){
								
								confDatFeedbackHistoryBO.setConfirmedByHrId(probationConfirmationBO.getConfirmedByHrId());
							}
							confDatFeedbackHistoryBO.setIsConfirmedByHr(probationConfirmationBO.getIsConfirmedByHr());
						}
						
						if(probationConfirmationBO.getIsConfirmedByMgr() != null) {
							if(probationConfirmationBO.getIsConfirmedByMgr().equals("0") ){
								confDatFeedbackHistoryBO.setMgrDenialReason(probationConfirmationBO.getMgrDenialReason());
							}
							else {
								confDatFeedbackHistoryBO.setConfirmedByMgrId(probationConfirmationBO.getConfirmedByMgrId());
							}
						confDatFeedbackHistoryBO.setIsConfirmedByMgr(probationConfirmationBO.getIsConfirmedByMgr());					
											
						}
						
						confDatFeedbackHistoryBO.setFkConfId(probationConfirmationBO.getPkConfId());
						confDatFeedbackHistoryBO.setFkConfStatusId(probationConfirmationBO.getFkConfStatusId());
						confDatFeedbackHistoryBO.setFkEmpId(probationConfirmationBO.getFkEmpId());
						confDatFeedbackHistoryBO.setFkMgrId(probationConfirmationBO.getFkMgrId());
						confDatFeedbackHistoryBO.setTentativeConfirmationDate(probationConfirmationBO.getTentativeConfirmationDate());
											
						confDatFeedbackHistoryBO.setLevel(profDetailBO.getFkEmpLevel());
						confDatFeedbackHistoryBO.setDesignation(profDetailBO.getFkEmpDesignation());
						
						//confDatFeedbackHistoryBO.setFkFeedbackId(feedbackBO.getPkFeedbackId());
						
						if(probationConfirmationBO.getUpdatedBy() != null)
							confDatFeedbackHistoryBO.setUpdatedBy(probationConfirmationBO.getUpdatedBy());
						if(probationConfirmationBO.getUpdatedOn() != null )
							confDatFeedbackHistoryBO.setUpdatedOn(probationConfirmationBO.getUpdatedOn());
						
						confDatFeedbackHistoryBO.setStatus("updated");
						
						
						confDatFeedbackHistoryBO.setFkConfId(probationConfirmationBO.getPkConfId());
						confDatFeedbackHistoryBO.setFkConfStatusId(probationConfirmationBO.getFkConfStatusId());
						confDatFeedbackHistoryBO.setFkEmpId(probationConfirmationBO.getFkEmpId());
						confDatFeedbackHistoryBO.setFkMgrId(probationConfirmationBO.getFkMgrId());
						confDatFeedbackHistoryBO.setTentativeConfirmationDate(probationConfirmationBO.getTentativeConfirmationDate());
											
						confDatFeedbackHistoryBO.setLevel(profDetailBO.getFkEmpLevel());
						confDatFeedbackHistoryBO.setDesignation(profDetailBO.getFkEmpDesignation());
						
						confDatFeedbackHistoryBO.setFkFeedbackId(feedbackBO.getPkFeedbackId());					
						confDatFeedbackHistoryBO.setAreasOfImprovements(feedbackBO.getAreasOfImprovements());
						confDatFeedbackHistoryBO.setAttitude(feedbackBO.getAttitude());
						confDatFeedbackHistoryBO.setCommunication(feedbackBO.getCommunication());
						confDatFeedbackHistoryBO.setDependability(feedbackBO.getDependability());
						confDatFeedbackHistoryBO.setFeedback(feedbackBO.getFeedback());					
						confDatFeedbackHistoryBO.setFlexibility(feedbackBO.getFlexibility());					
						confDatFeedbackHistoryBO.setProactiveness(feedbackBO.getProactiveness());					
						confDatFeedbackHistoryBO.setSelfConfidence(feedbackBO.getSelfConfidence());					
						confDatFeedbackHistoryBO.setTechnicalExpertise(feedbackBO.getTechnicalExpertise());
						confDatFeedbackHistoryBO.setWillingnessToLearn(feedbackBO.getWillingnessToLearn());
						confDatFeedbackHistoryBO.setWorkObjectives(feedbackBO.getWorkObjectives());
						confDatFeedbackHistoryBO.setUpdatedBy(feedbackBO.getUpdatedBy());
						confDatFeedbackHistoryBO.setUpdatedOn(feedbackBO.getUpdatedOn());
						confDatFeedbackHistoryBO.setStatus("updated");
						confFeedbackHistoryRepo.save(confDatFeedbackHistoryBO);	
						
						
						// EOA for History Table
						
						
						
						
						//Added by Harsh for Notifications
						try {
							confNotificationService.setEmpConfirmedNotificationToEmpScreen(confirmStatusByHrInputBean.getFkEmpId());
							confNotificationService.setEmpConfirmedNotificationToRmScreen(probationConfirmationBO.getFkMgrId());
							if(probationConfirmationBO.getDelegatedToMgrId()!=null) {
								confNotificationService.setEmpConfirmedNotificationToDelRmScreen(probationConfirmationBO.getDelegatedToMgrId());
							}
							
							
							if(hrRoleIdList != null)
							{
								for(Short roleId : hrRoleIdList) {
									List<DatEmpDetailBO> empDetail = empDetailRepository.findByFkEmpRoleId(roleId);
									if(empDetail != null)
									{
										for(DatEmpDetailBO emp : empDetail) {
											try {
												confNotificationService.setFeedbackProvidedNotificationByRMToHRScreen(emp.getPkEmpId());
												if(probationConfirmationBO.getDelegatedToMgrId()!=null && feedbackBO.getUpdatedBy() == probationConfirmationBO.getDelegatedToMgrId()) {
													confNotificationService.setFeedbackProvidedByDelRmToHrScreen(emp.getPkEmpId());
												}	
											} catch (Exception e) {
												LOG.info("Unable to set feedback provided by RM notification."+e);
											}
										}
									}
								}
							}
							
							
						} catch (Exception e) {
							LOG.info("Unable to set Confirmation By HR notification."+e);
						}
						//EOA by Harsh for Notifications
						LOG.endUsecase("Confirm Service");
						try {
							if(saveProfDetail != null)
								sendMailByHr(confirmStatusByHrInputBean.getFkEmpId());
						} catch (Exception e) {
							LOG.startUsecase("Error in sending email."+e);
						}
					}
				}else
				{
			throw new CommonCustomException("Sorry, Confirmation can only be done on or after tentative date of confirmation.");
				}
			
		}
		return true;
	}

	public ConfDatProbationConfirmationBO delegateManager(
			DelegateManagerByHrInputBean delegateManagerByHrInputBean) throws CommonCustomException {
		ConfDatProbationConfirmationBO probationConfirmationBO = new ConfDatProbationConfirmationBO();
		DatEmpProfessionalDetailBO profDetailBO = new DatEmpProfessionalDetailBO();
		ConfDatFeedbackBO feedbackBO = new ConfDatFeedbackBO();
		List<EmployeeNameWithRMIdBean> empDetailBO = new ArrayList<EmployeeNameWithRMIdBean>();
		
		List<Short> hrRoleIdList = Stream.of(hrRoleIds.split(","))
                .map(Short::parseShort)
                .collect(Collectors.toList());
		 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 List<Integer> notInclude = new ArrayList<Integer>();
			String[] empIdArray = empIds.split(",");
			for (String s : empIdArray)
				notInclude.add(Integer.parseInt(s));
		 
		try
		{
			probationConfirmationBO = confCommonRepo.findByFkEmpId(delegateManagerByHrInputBean.getFkEmpId());
			
		}catch(Exception e)
		{
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		
		
		if(probationConfirmationBO == null)
		{
			throw new CommonCustomException("Invalid employee id.");
			
	    }
		if(probationConfirmationBO.getFkConfStatusId() == confirmedStatus){
			throw new CommonCustomException("The employee is in confirmed status. So no need of delegation.");
		}
		
		String tentconfdate = df.format(probationConfirmationBO.getTentativeConfirmationDate());
		 String currDate = df.format(new Date());
		if (!tentconfdate.equals(currDate)){
			
		if((new Date()).after(probationConfirmationBO.getTentativeConfirmationDate()))

		{
		
			try
			{
				feedbackBO = confFeedbackRepo.findByFkEmpId(delegateManagerByHrInputBean.getFkEmpId());
			}
			catch(Exception e)
			{
				throw new CommonCustomException("Failed to fetch details from feedback table");
			}
			if (feedbackBO == null)
				{
			
						if(probationConfirmationBO.getDelegatedToMgrId() != null)
						{
						if (probationConfirmationBO.getDelegatedToMgrId().intValue() == delegateManagerByHrInputBean.getDelegatedToMgrId().intValue())
							{
							
							throw new CommonCustomException("Sorry, It's already delegated. Please enter another ID to delegate.");
							}
						}
						DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
						try
						{
							
							datEmpDetailBO = empDetailRepo.findByPkEmpId(delegateManagerByHrInputBean.getDelegatedByHrId());
						}
						catch(Exception ex)
						{
							throw new CommonCustomException("Unable to get employee details from backend.");
						}
						
						if(hrRoleIdList.contains(datEmpDetailBO.getFkEmpRoleId()) )
						{
							probationConfirmationBO.setDelegatedByHrId(delegateManagerByHrInputBean.getDelegatedByHrId());
						}
						else
						{
							throw new CommonCustomException("Access denied since you are not an authorized person.");
						}
							try{
								//profDetailBO = profDetailRepository.getSkipLevelMgrDetailsByEmpId(delegateManagerByHrInputBean.getFkEmpId());
								empDetailBO = commonService.getAllReportingManagersByEmpId(delegateManagerByHrInputBean.getFkEmpId());
								
							
							}catch(Exception e){
								throw new CommonCustomException("Failed to fetch details from DB");
							}
							try{
								profDetailBO = profDetailRepository.findByFkMainEmpDetailId(delegateManagerByHrInputBean.getFkEmpId());
								//empDetailBO = commonService.getAllReportingManagersByEmpId(delegateManagerByHrInputBean.getFkEmpId());
								
							
							}catch(Exception e){
								throw new CommonCustomException("Failed to fetch details from DB");
							}
					/*		int skiplevelMgr = 0;
							int skiplevelMgrsMgr = 0;
							
							if(!notInclude.contains(profDetailBO.getFkMainEmpDetailId()) )
								{ skiplevelMgr =profDetailBO.getFkMainEmpDetailId();}
							if(! notInclude.contains(profDetailBO.getFkEmpReportingMgrId()) )
							{ skiplevelMgrsMgr =profDetailBO.getFkEmpReportingMgrId();}*/
							/*System.out.println("skiplevelmgr"+skiplevelMgr);
							System.out.println("nextmgr"+skiplevelMgrsMgr);*/
							//if(delegateManagerByHrInputBean.getDelegatedToMgrId().intValue() == skiplevelMgr || delegateManagerByHrInputBean.getDelegatedToMgrId().intValue() == skiplevelMgrsMgr)
							LOG.info("empDetailBO before if condition:"+empDetailBO.toString());
							boolean found = empDetailBO.stream().filter(o -> o.getEmpId() == delegateManagerByHrInputBean.getDelegatedToMgrId()).findFirst().isPresent();
							if(found){
								if(profDetailBO.getFkEmpReportingMgrId().intValue() == delegateManagerByHrInputBean.getDelegatedToMgrId().intValue()){
									throw new CommonCustomException("Delegation cannot be done to the RM .Please choose another RM");
								}else
							probationConfirmationBO.setDelegatedToMgrId(delegateManagerByHrInputBean.getDelegatedToMgrId());
							}else{
								throw new CommonCustomException("Sorry invalid Manager Id to delegate");
							}
							probationConfirmationBO.setDelegatedComments(delegateManagerByHrInputBean.getDelegatedComments());
							probationConfirmationBO.setDelegatedDate(new Date());
							probationConfirmationBO.setUpdatedBy(delegateManagerByHrInputBean.getDelegatedByHrId());
							probationConfirmationBO.setUpdatedOn(new Date());
							confCommonRepo.save(probationConfirmationBO);
							
							
							try {
								confNotificationService.setConfirmationDelegatedByHrNotificationToEmpScreen(delegateManagerByHrInputBean.getFkEmpId());
								confNotificationService.setConfirmationDelegatedByHrNotificationToRmScreen(probationConfirmationBO.getFkMgrId());
								confNotificationService.setConfirmationDelegatedByHrNotificationToDelRmScreen(delegateManagerByHrInputBean.getDelegatedToMgrId());
								
								
							} catch (Exception e) {
								LOG.info("Unable to set Delegate Manager by HR notification."+e);
							}
							
						
					}
					else{
						throw new CommonCustomException("Sorry delegation cannot be done after the feedback is provided.");
					
						}
			}
		else
		{
			throw new CommonCustomException("Sorry, Delegation can be done only after the tentative date of confirmation. ");
		}
		
	}
		else
		{
			throw new CommonCustomException("Sorry, Delegation can be done only after the tentative date of confirmation.");
		}
		
		return probationConfirmationBO;
	}
	
	public boolean sendMailByHr(Integer empId) throws CommonCustomException {
		
		boolean isUpdated = false;
				
		DatEmpProfessionalDetailBO empDetails = null;
		DatEmpProfessionalDetailBO rmDetails = null;
		DatEmpProfessionalDetailBO delRmDetails = null;
		DatEmpProfessionalDetailBO profDetails = null;
		ConfDatProbationConfirmationBO probationConfirmationBO = new ConfDatProbationConfirmationBO();
		GenerateConfExtLetterOutputBean confExtLetterBean = new GenerateConfExtLetterOutputBean();
		
		List<String> to = new ArrayList<String>();
		List<String> cc = new ArrayList<String>();
		cc.add(hrMailId);
		LOG.startUsecase("Send mail by HR");				
		try
		{
			probationConfirmationBO = confCommonRepo.findByFkEmpId(empId);
		}
		catch(Exception e)
		{
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		
		if(probationConfirmationBO == null)
		{
			throw new CommonCustomException("Invalid employee id.");
		}
		
					
		try
		{
			confExtLetterBean = confEmpService.generateConfExtLetter(empId);
		}
		catch(Exception e)
		{
			throw new CommonCustomException("Error while generating Confirmation Letter PDF.");
		}
					
		empDetails = getEmpProfessionalDetailsByEmpId(empId);
		to.add(empDetails.getEmpProfessionalEmailId());
		
		if (empDetails != null) {
			rmDetails = getEmpProfessionalDetailsByEmpId(empDetails.getFkEmpReportingMgrId());
			if (rmDetails != null)
				cc.add(rmDetails.getEmpProfessionalEmailId());
									
		}
		
		if (probationConfirmationBO.getDelegatedToMgrId() != null) {
			delRmDetails = getEmpProfessionalDetailsByEmpId(probationConfirmationBO.getDelegatedToMgrId());
			if (delRmDetails != null)
				cc.add(delRmDetails.getEmpProfessionalEmailId());
									
		}
		
				
		String filePath = confExtLetterBean.getFilePath();
		String fileName = confExtLetterBean.getFileName();
		String empName = goalService.getEmpNameByEmpId(empDetails.getFkMainEmpDetailId());
		String mgrName = goalService.getEmpNameByEmpId(rmDetails.getFkMainEmpDetailId());
		String subject= "ConfExtLetter";
		
		if(probationConfirmationBO.getFkConfStatusId() == confirmedStatus)
		{
			subject = "EMPLOYMENT CONFIRMED";
		}
		else if(probationConfirmationBO.getFkConfStatusId() == extendedProbationStatus)
		{
			subject = "PROBATION PERIOD EXTENDED";
		}
		
		try {
			
			if(filePath != null) {
				misEmailService.sendMailWithAttchment( to, cc, subject, filePath, fileName, empName, mgrName, probationConfirmationBO.getFkConfStatusId());
				isUpdated = true;
				
			}
			else {
				throw new CommonCustomException("Confirmation / Extension Letter generation failed.");
			}
			
				
		} catch (Exception e) {
				e.printStackTrace();
		}
		
		if(isUpdated) {
			if(probationConfirmationBO.getFkConfStatusId() == confirmedStatus)
			{
				probationConfirmationBO.setConfirmationLetterSent("1");
			}
			if(probationConfirmationBO.getFkConfStatusId() == extendedProbationStatus)
			{
				probationConfirmationBO.setExtensionLetterSent("1");
			}
			
		}
		LOG.endUsecase("Send mail by HR");
		return isUpdated;
	}

		
	
	public DatEmpProfessionalDetailBO getEmpProfessionalDetailsByEmpId(int empId) {
		DatEmpProfessionalDetailBO profDetails = null;
		try {
			profDetails = employeeProfessionalRepository
					.findByFkMainEmpDetailId(empId);
			if (profDetails != null) {
				return profDetails;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
		
	//EOA by Smrithi

	
	
	
	
	//Added by Harsh
	public boolean submitFeedbackByHr(
			ConfirmationSubmitHRFeedbackInputBean confirmationSubmitHRFeedbackInputBean) throws CommonCustomException {
		
		boolean flag = false;
		boolean hasAccess = false;
		
		ConfDatProbationConfirmationBO probationConfirmationBO = new ConfDatProbationConfirmationBO();
		ConfDatFeedbackBO confDatFeedbackBO = new ConfDatFeedbackBO();
		ConfDatFeedbackBO feedbackBO = new ConfDatFeedbackBO();
		
		
		List<Short> hrRoleIdList = Stream.of(hrRoleIds.split(","))
                .map(Short::parseShort)
                .collect(Collectors.toList());
		
		
		LOG.startUsecase("HR Feedback");
		try{				
			probationConfirmationBO = confCommonRepo.findByFkEmpId(confirmationSubmitHRFeedbackInputBean.getEmpId());
		}
		catch(Exception e){
			throw new CommonCustomException("Failed to fetch details from DB");
		}
		
		if(probationConfirmationBO == null){
			throw new CommonCustomException("Invalid employee id.");
		}
		if(confirmationSubmitHRFeedbackInputBean.getEmpId().intValue() == confirmationSubmitHRFeedbackInputBean.getLoggedInUser().intValue()){
			throw new CommonCustomException("Access denied for submitting feedeback. ");
		}
		if(probationConfirmationBO.getFkConfStatusId() == confirmedStatus)
		{
			throw new CommonCustomException("The employee is already confirmed. Cannot submit Feedback.");
		}
		
		else{
				
			DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
			try
			{
				datEmpDetailBO = empDetailRepo.findByPkEmpId(confirmationSubmitHRFeedbackInputBean.getLoggedInUser());
			}
			catch(Exception ex){
				throw new CommonCustomException("Unable to get employee details from backend.");
			}
			
			try{
				feedbackBO = confFeedbackRepo.findByFkEmpId(confirmationSubmitHRFeedbackInputBean.getEmpId());
			}catch(Exception e){
				throw new CommonCustomException("Failed to fetch details from feedback table");
			}						
			Date currentDate = new Date();
			
			String errorMsg="Error in submitting feedback.";
			
			if(currentDate.after(probationConfirmationBO.getTentativeConfirmationDate()))
			{
				
				if(hrRoleIdList.contains(datEmpDetailBO.getFkEmpRoleId()) )
				{
					hasAccess = true;
				}
				else {
					
					errorMsg = "Access denied to Submit Feedback since you are not an authorized person.";
				}
			
			}
			else {
				errorMsg = "Feedback can only be provided after the Date of Confirmation";
			}
			
			if(feedbackBO != null){
				throw new CommonCustomException("Feedback is already submitted");
			}else{
			if(hasAccess){
				
				confDatFeedbackBO.setAttitude(confirmationSubmitHRFeedbackInputBean.getAttitude());
				confDatFeedbackBO.setCommunication(confirmationSubmitHRFeedbackInputBean.getCommunication());
				confDatFeedbackBO.setDependability(confirmationSubmitHRFeedbackInputBean.getDependability());
				confDatFeedbackBO.setFlexibility(confirmationSubmitHRFeedbackInputBean.getFlexibility());
				confDatFeedbackBO.setProactiveness(confirmationSubmitHRFeedbackInputBean.getProactiveness());
				confDatFeedbackBO.setSelfConfidence(confirmationSubmitHRFeedbackInputBean.getSelfConfidence());
				confDatFeedbackBO.setTechnicalExpertise(confirmationSubmitHRFeedbackInputBean.getTechnicalExpertise());
				confDatFeedbackBO.setWillingnessToLearn(confirmationSubmitHRFeedbackInputBean.getWillingnessToLearn());
				confDatFeedbackBO.setFeedback(confirmationSubmitHRFeedbackInputBean.getFeedback());
				confDatFeedbackBO.setWorkObjectives(confirmationSubmitHRFeedbackInputBean.getWorkObjectives());
				confDatFeedbackBO.setAreasOfImprovements(confirmationSubmitHRFeedbackInputBean.getAreasOfImprovements());
				confDatFeedbackBO.setFkConfId(probationConfirmationBO.getPkConfId());
				confDatFeedbackBO.setFkEmpId(confirmationSubmitHRFeedbackInputBean.getEmpId());
				confDatFeedbackBO.setUpdatedBy(confirmationSubmitHRFeedbackInputBean.getLoggedInUser());
				confDatFeedbackBO.setUpdatedOn(new Date());
				confFeedbackRepo.save(confDatFeedbackBO);
							
				flag = true;
				
			}else{
				throw new CommonCustomException(errorMsg);
			}
			}
		}
		LOG.endUsecase("HR Feedback ends");
		return flag;
		 
		
	}
	
	
	public boolean updateFeedbackByHr(
			ConfirmationUpdateHRFeedbackInputBean confirmationUpdateHRFeedbackInputBean) throws CommonCustomException {
		
		boolean flag = false;
		boolean hasAccess = false;
		
		ConfDatProbationConfirmationBO probationConfirmationBO = new ConfDatProbationConfirmationBO();
		ConfDatFeedbackBO confDatFeedbackBO = new ConfDatFeedbackBO();
				
		List<Short> hrRoleIdList = Stream.of(hrRoleIds.split(","))
                .map(Short::parseShort)
                .collect(Collectors.toList());
		
		LOG.startUsecase("Update HR Feedback");
		
		try{				
			confDatFeedbackBO = confFeedbackRepo.findByPkFeedbackId(confirmationUpdateHRFeedbackInputBean.getPkFeedbackId().intValue());
		}
		catch(Exception e){
			throw new CommonCustomException("Failed to fetch feedback details from DB");
		}
		
				
		if(confDatFeedbackBO == null){
			throw new CommonCustomException("Feedback Id Not Found in DB");
		}
		
		try{				
			probationConfirmationBO = confCommonRepo.findByFkEmpId(confirmationUpdateHRFeedbackInputBean.getEmpId());
		}
		catch(Exception e){
			throw new CommonCustomException("Failed to fetch confirmation details from DB");
		}
		
		if(probationConfirmationBO == null){
			throw new CommonCustomException("Invalid employee id.");
		}
		
		if(probationConfirmationBO.getFkConfStatusId() == confirmedStatus)
		{
			throw new CommonCustomException("The employee is already confirmed. Cannot Update Feedback.");
		}
		
		else{
				
			DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
			try
			{
				datEmpDetailBO = empDetailRepo.findByPkEmpId(confirmationUpdateHRFeedbackInputBean.getLoggedInUser());
			}
			catch(Exception ex){
				throw new CommonCustomException("Unable to get employee details from backend.");
			}
			
						
			Date currentDate = new Date();
			
			String errorMsg="Error in submitting feedback.";
			
			if(currentDate.after(probationConfirmationBO.getTentativeConfirmationDate()))
			{
				
					if(hrRoleIdList.contains(datEmpDetailBO.getFkEmpRoleId()) )
					{
						hasAccess = true;
					}
					else {
						
						errorMsg = "Access denied to Submit Feedback since you are not an authorized person.";
					}
				
				
			}
			else {
				
				errorMsg = "Feedback can only be provided after the Date of Confirmation";
			}
			
			if(hasAccess){
				
				confDatFeedbackBO.setAttitude(confirmationUpdateHRFeedbackInputBean.getAttitude());
				confDatFeedbackBO.setCommunication(confirmationUpdateHRFeedbackInputBean.getCommunication());
				confDatFeedbackBO.setDependability(confirmationUpdateHRFeedbackInputBean.getDependability());
				confDatFeedbackBO.setFlexibility(confirmationUpdateHRFeedbackInputBean.getFlexibility());
				confDatFeedbackBO.setProactiveness(confirmationUpdateHRFeedbackInputBean.getProactiveness());
				confDatFeedbackBO.setSelfConfidence(confirmationUpdateHRFeedbackInputBean.getSelfConfidence());
				confDatFeedbackBO.setTechnicalExpertise(confirmationUpdateHRFeedbackInputBean.getTechnicalExpertise());
				confDatFeedbackBO.setWillingnessToLearn(confirmationUpdateHRFeedbackInputBean.getWillingnessToLearn());
				confDatFeedbackBO.setFeedback(confirmationUpdateHRFeedbackInputBean.getFeedback());
				confDatFeedbackBO.setWorkObjectives(confirmationUpdateHRFeedbackInputBean.getWorkObjectives());
				confDatFeedbackBO.setAreasOfImprovements(confirmationUpdateHRFeedbackInputBean.getAreasOfImprovements());
				confDatFeedbackBO.setFkConfId(probationConfirmationBO.getPkConfId());
				confDatFeedbackBO.setFkEmpId(confirmationUpdateHRFeedbackInputBean.getEmpId());
				confDatFeedbackBO.setUpdatedBy(confirmationUpdateHRFeedbackInputBean.getLoggedInUser());
				confDatFeedbackBO.setUpdatedOn(new Date());
							
				flag = true;
				
			}else{
				throw new CommonCustomException(errorMsg);
			}
			
		}
		LOG.endUsecase("HR Update Feedback Ends");
		return flag;
	
	}

	
	
	//EOA by Harsh
	
	// Added by Kamal Anand
	public boolean notConfirmByHr(
			NotConfirmedByHrInputBean notConfirmedByHrInputBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering Not Confirm by HR service");
		ConfDatProbationConfirmationBO probationConfirmationBO = new ConfDatProbationConfirmationBO();
		ConfDatFeedbackBO feedbackBO = new ConfDatFeedbackBO();
		ConfDatFeedbackHistoryBO confDatFeedbackHistoryBO = new ConfDatFeedbackHistoryBO();
		DatEmpDetailBO datEmpDetailBO = new DatEmpDetailBO();
		boolean isNotConfirmed = false;
		try {
			List<Short> hrRoleIdList = Stream.of(hrRoleIds.split(","))
					.map(Short::parseShort).collect(Collectors.toList());

			try {
				probationConfirmationBO = confCommonRepo
						.findByFkEmpId(notConfirmedByHrInputBean.getEmpId());

			} catch (Exception e) {
				throw new DataAccessException("Failed to fetch details from DB");
			}
			if (probationConfirmationBO == null) {
				throw new CommonCustomException("Invalid employee id.");
			}

			if (probationConfirmationBO.getFkConfStatusId() == confirmedStatus) {
				throw new CommonCustomException(
						"The employee is already confirmed.");
			}
			if(probationConfirmationBO.getFkConfStatusId() == notConfirmedStatus){
				throw new CommonCustomException(
						"The employee is already not confirmed.");
			}

			try {
				feedbackBO = confFeedbackRepo
						.findByFkEmpId(notConfirmedByHrInputBean.getEmpId());
			} catch (Exception e) {
				throw new DataAccessException(
						"Failed to fetch details from feedback table");
			}
				
			if(feedbackBO == null)
				throw new CommonCustomException("Manager Feedback not provided, hence Not Confirmed cannot be done");
			

			Date currentDate = DateTimeUtil.dateWithMinTimeofDay(new Date());
			Date tentiativeConfDate = DateTimeUtil
					.dateWithMinTimeofDay(probationConfirmationBO
							.getTentativeConfirmationDate());

			if (tentiativeConfDate.equals(currentDate)
					|| currentDate.after(probationConfirmationBO
							.getTentativeConfirmationDate())) {
				try {
					datEmpDetailBO = empDetailRepo
							.findByPkEmpId(notConfirmedByHrInputBean
									.getLoggedInUserId());
				} catch (Exception ex) {
					throw new DataAccessException(
							"Unable to get employee details from backend.");
				}

				if (hrRoleIdList.contains(datEmpDetailBO.getFkEmpRoleId())) {
					DatEmpProfessionalDetailBO profDetailBO = new DatEmpProfessionalDetailBO();
					try {
						profDetailBO = profDetailRepo
								.findByFkMainEmpDetailId(notConfirmedByHrInputBean
										.getEmpId());
					} catch (Exception ex) {
						throw new CommonCustomException(
								"Unable to get employee details from backend.");
					}

					if (currentDate.after(tentiativeConfDate)
							|| tentiativeConfDate.equals(currentDate)) {

					/*	if (probationConfirmationBO
								.getActualDateOfConfirmation() != null) {
							confDatFeedbackHistoryBO
									.setActualDateOfConfirmation(probationConfirmationBO
											.getActualDateOfConfirmation());
						}

						if (probationConfirmationBO.getConfExtendedInMonths() != null) {
							confDatFeedbackHistoryBO
									.setConfExtendedInMonths(probationConfirmationBO
											.getConfExtendedInMonths());
							confDatFeedbackHistoryBO
									.setReasonForExtension(probationConfirmationBO
											.getReasonForExtension());
							confDatFeedbackHistoryBO
									.setConfExtendedByHrId(probationConfirmationBO
											.getConfExtendedByHrId());
						}

						if (probationConfirmationBO.getDelegatedToMgrId() != null) {
							confDatFeedbackHistoryBO
									.setDelegatedByHrId(probationConfirmationBO
											.getDelegatedByHrId());
							confDatFeedbackHistoryBO
									.setDelegatedComments(probationConfirmationBO
											.getDelegatedComments());
							confDatFeedbackHistoryBO
									.setDelegatedDate(probationConfirmationBO
											.getDelegatedDate());
							confDatFeedbackHistoryBO
									.setDelegatedToMgrId(probationConfirmationBO
											.getDelegatedToMgrId());
						}

						if (probationConfirmationBO.getIsConfirmedByMgr() != null) {
							if (probationConfirmationBO.getIsConfirmedByMgr()
									.equals("0")) {
								confDatFeedbackHistoryBO
										.setMgrDenialReason(probationConfirmationBO
												.getMgrDenialReason());
							} else {
								confDatFeedbackHistoryBO
										.setConfirmedByMgrId(probationConfirmationBO
												.getConfirmedByMgrId());
							}
							confDatFeedbackHistoryBO
									.setIsConfirmedByMgr(probationConfirmationBO
											.getIsConfirmedByMgr());

						}

						confDatFeedbackHistoryBO
								.setFkConfId(probationConfirmationBO
										.getPkConfId());
						confDatFeedbackHistoryBO
								.setFkConfStatusId(probationConfirmationBO
										.getFkConfStatusId());
						confDatFeedbackHistoryBO
								.setFkEmpId(probationConfirmationBO
										.getFkEmpId());
						confDatFeedbackHistoryBO
								.setFkMgrId(probationConfirmationBO
										.getFkMgrId());
						confDatFeedbackHistoryBO
								.setTentativeConfirmationDate(probationConfirmationBO
										.getTentativeConfirmationDate());

						confDatFeedbackHistoryBO.setLevel(profDetailBO
								.getFkEmpLevel());
						confDatFeedbackHistoryBO.setDesignation(profDetailBO
								.getFkEmpDesignation());

						if (probationConfirmationBO.getUpdatedBy() != null)
							confDatFeedbackHistoryBO
									.setUpdatedBy(probationConfirmationBO
											.getUpdatedBy());
						if (probationConfirmationBO.getUpdatedOn() != null)
							confDatFeedbackHistoryBO
									.setUpdatedOn(probationConfirmationBO
											.getUpdatedOn());

						confDatFeedbackHistoryBO.setStatus("updated");

						confDatFeedbackHistoryBO
								.setFkConfId(probationConfirmationBO
										.getPkConfId());
						confDatFeedbackHistoryBO
								.setFkConfStatusId(probationConfirmationBO
										.getFkConfStatusId());
						confDatFeedbackHistoryBO
								.setFkEmpId(probationConfirmationBO
										.getFkEmpId());
						confDatFeedbackHistoryBO
								.setFkMgrId(probationConfirmationBO
										.getFkMgrId());
						confDatFeedbackHistoryBO
								.setTentativeConfirmationDate(probationConfirmationBO
										.getTentativeConfirmationDate());

						confDatFeedbackHistoryBO.setLevel(profDetailBO
								.getFkEmpLevel());
						confDatFeedbackHistoryBO.setDesignation(profDetailBO
								.getFkEmpDesignation());

						if (feedbackBO != null) {
							confDatFeedbackHistoryBO.setFkFeedbackId(feedbackBO
									.getPkFeedbackId());
							confDatFeedbackHistoryBO
									.setAreasOfImprovements(feedbackBO
											.getAreasOfImprovements());
							confDatFeedbackHistoryBO.setAttitude(feedbackBO
									.getAttitude());
							confDatFeedbackHistoryBO
									.setCommunication(feedbackBO
											.getCommunication());
							confDatFeedbackHistoryBO
									.setDependability(feedbackBO
											.getDependability());
							confDatFeedbackHistoryBO.setFeedback(feedbackBO
									.getFeedback());
							confDatFeedbackHistoryBO.setFlexibility(feedbackBO
									.getFlexibility());
							confDatFeedbackHistoryBO
									.setProactiveness(feedbackBO
											.getProactiveness());
							confDatFeedbackHistoryBO
									.setSelfConfidence(feedbackBO
											.getSelfConfidence());
							confDatFeedbackHistoryBO
									.setTechnicalExpertise(feedbackBO
											.getTechnicalExpertise());
							confDatFeedbackHistoryBO
									.setWillingnessToLearn(feedbackBO
											.getWillingnessToLearn());
							confDatFeedbackHistoryBO
									.setWorkObjectives(feedbackBO
											.getWorkObjectives());
							confDatFeedbackHistoryBO.setUpdatedBy(feedbackBO
									.getUpdatedBy());
							confDatFeedbackHistoryBO.setUpdatedOn(feedbackBO
									.getUpdatedOn());
						}
						confDatFeedbackHistoryBO.setStatus("updated");
						confFeedbackHistoryRepo.save(confDatFeedbackHistoryBO);*/

						// EOA for History Table
						
						/*Calendar aDate = Calendar.getInstance();
						aDate.setTime(actualConfDate);

						Calendar aTime = Calendar.getInstance();
						aTime.setTime(new Date());

						Calendar aDateTime = Calendar.getInstance();
						aDateTime.set(Calendar.DAY_OF_MONTH,
								aDate.get(Calendar.DAY_OF_MONTH));
						aDateTime
								.set(Calendar.MONTH, aDate.get(Calendar.MONTH));
						aDateTime.set(Calendar.YEAR, aDate.get(Calendar.YEAR));
						aDateTime.set(Calendar.HOUR, aTime.get(Calendar.HOUR));
						aDateTime.set(Calendar.MINUTE,
								aTime.get(Calendar.MINUTE));
						probationConfirmationBO
								.setTentativeConfirmationDate(aDateTime
										.getTime());*/

						probationConfirmationBO
								.setFkConfStatusId(notConfirmedStatus);
						probationConfirmationBO.setUpdatedOn(new Date());
						probationConfirmationBO
								.setUpdatedBy(notConfirmedByHrInputBean
										.getLoggedInUserId());
						probationConfirmationBO
								.setReasonForExtension(notConfirmedByHrInputBean
										.getHrReason());
						confCommonRepo.save(probationConfirmationBO);

						/*profDetailBO.setEmpDateOfConfirmation(aDateTime
								.getTime());

						employeeProfessionalRepository.save(profDetailBO);*/

						isNotConfirmed = true;
						
					} else {
						throw new CommonCustomException(
								"Actual Conf Date can only be after or same as tentative date of confirmation.");
					}
				} else {
					throw new CommonCustomException(
							"Access denied since you are not an authorized person.");
				}

			} else {
				throw new CommonCustomException(
						"Date Of Confirmation must be Current Date or Greater than current date");
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("Exiting Not Confirm by HR service");
		return isNotConfirmed;
	}
	// EOA by Kamal Anand

}
