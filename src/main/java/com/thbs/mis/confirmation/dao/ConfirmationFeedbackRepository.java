package com.thbs.mis.confirmation.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.confirmation.bo.ConfDatFeedbackBO;

@Repository
public interface ConfirmationFeedbackRepository extends GenericRepository<ConfDatFeedbackBO, Long>,  JpaSpecificationExecutor<ConfDatFeedbackBO> {

	//Added by Smrithi
	public ConfDatFeedbackBO findByFkEmpId(Integer empId);
	//EOA by Smrithi
	
	//Added by Harsh
	public ConfDatFeedbackBO findByPkFeedbackId(Integer feedbackId);
	//EOA by Harsh
	

}
