package com.thbs.mis.confirmation.dao;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.confirmation.bo.ConfDatConfirmationHistoryBO;
import com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO;
@Repository
public interface ConfirmationProbationHistoryRepository extends GenericRepository<ConfDatConfirmationHistoryBO, Long>,  JpaSpecificationExecutor<ConfDatConfirmationHistoryBO>
{
	public ConfDatConfirmationHistoryBO findByFkEmpId(Integer empId);
	
}