package com.thbs.mis.confirmation.dao;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bean.EmployeeNameWithRMIdBean;
import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO;
import com.thbs.mis.gsr.bo.GsrDatEmpGoalBO;
@Repository
public interface ConfirmationCommonRepository extends GenericRepository<ConfDatProbationConfirmationBO, Long>,  JpaSpecificationExecutor<ConfDatProbationConfirmationBO>
{
	//Added by Smrithi
	public ConfDatProbationConfirmationBO findByFkEmpId(Integer empId);
	

	/* @Query( " SELECT new com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO "
			    + " ( conf.fkEmpId , prof.empDateOfJoining, conf.actualDateOfConfirmation , "
				+ " des.empDesignationName, level.empLevelName, conf.fkConfStatusId , "
				+ " prof.fkEmpLevel, prof.fkEmpDesignation , "
				+ " personal.empFirstName , personal.empLastName , conf.pkConfId , "
				+ " prof.fkEmpBuUnit , conf.delegatedToMgrId ,  prof.fkEmpReportingMgrId , feedback.pkFeedbackId ) "
				+ " from ConfDatProbationConfirmationBO conf "
				+ " join DatEmpDetailBO detail on conf.fkEmpId = detail.pkEmpId "
				+ " join DatEmpProfessionalDetailBO prof on detail.pkEmpId = prof.fkMainEmpDetailId "
				+ " join  DatEmpPersonalDetailBO personal on detail.pkEmpId = personal.fkEmpDetailId "
				+ " join MasEmpLevelBO level on level.pkEmpLevelId = prof.fkEmpLevel "
				+ " join MasEmpDesignationBO des on des.pkEmpDesignationId = prof.fkEmpDesignation "
				+ " join  DatEmpPersonalDetailBO mgrName on mgrName.fkEmpDetailId = conf.fkMgrId "
				+ " left join ConfDatFeedbackBO feedback on feedback.fkConfId = conf.pkConfId "
	 		    + " where conf.tentativeConfirmationDate  >= :startDate and "
	 		    + " conf.tentativeConfirmationDate  <= :endDate " ) 
	public List<ConfDatProbationConfirmationBO> searchByDateRange(@Param("startDate")Date startDate, @Param("endDate") Date endDate);

	*/
/*	@Query(" SELECT new com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO "
			+ " ( conf.fkEmpId , prof.empDateOfJoining, conf.actualDateOfConfirmation , "
			+ " des.empDesignationName, level.empLevelName, conf.fkConfStatusId , "
			+ " prof.fkEmpLevel, prof.fkEmpDesignation , "
			+ " personal.empFirstName , personal.empLastName , conf.pkConfId , "
			+ " prof.fkEmpBuUnit , conf.delegatedToMgrId , prof.fkEmpReportingMgrId , feedback.pkFeedbackId ) "
			+ " from ConfDatProbationConfirmationBO conf "
			+ " join DatEmpDetailBO detail on conf.fkEmpId = detail.pkEmpId "
			+ " join DatEmpProfessionalDetailBO prof on detail.pkEmpId = prof.fkMainEmpDetailId "
			+ " join  DatEmpPersonalDetailBO personal on detail.pkEmpId = personal.fkEmpDetailId "
			+ " join MasEmpLevelBO level on level.pkEmpLevelId = prof.fkEmpLevel "
			+ " join MasEmpDesignationBO des on des.pkEmpDesignationId = prof.fkEmpDesignation "
			+ " join  DatEmpPersonalDetailBO mgrName on mgrName.fkEmpDetailId = conf.fkMgrId "
			+ " left join ConfDatFeedbackBO feedback on feedback.fkConfId = conf.pkConfId "
			+ " where conf.fkEmpId in (:empIdList) ")*/
	//@Query(name = "Confirmation.empConfDetails")
	 @Query("SELECT new com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO(prof.fkMainEmpDetailId,prof.empDateOfJoining,"
	 		+ "conf.tentativeConfirmationDate,des.empDesignationName,lvl.empLevelName,"
	 		+ "conf.fkConfStatusId,lvl.pkEmpLevelId,des.pkEmpDesignationId,conf.pkConfId,"
	 		+ "prof.fkEmpBuUnit,conf.delegatedToMgrId,prof.fkEmpReportingMgrId,feedback.pkFeedbackId,det.empType) "
	 		+ "FROM DatEmpProfessionalDetailBO prof "
	 		+ "join DatEmpPersonalDetailBO per on per.fkEmpDetailId = prof.fkMainEmpDetailId "
	 		+ "join DatEmpDetailBO det on det.pkEmpId = prof.fkMainEmpDetailId "
	 		+ "left join ConfDatFeedbackBO feedback on feedback.fkEmpId = prof.fkMainEmpDetailId "
	 		+ "left join ConfDatProbationConfirmationBO conf on conf.fkEmpId = prof.fkMainEmpDetailId "
	 		+ "join MasEmpLevelBO lvl on prof.fkEmpLevel = lvl.pkEmpLevelId "
	 		+ "join MasEmpDesignationBO des on prof.fkEmpDesignation = des.pkEmpDesignationId "
	 		+ "where prof.fkMainEmpDetailId in :empIdList")
public List<ConfDatProbationConfirmationBO> searchDetails(@Param("empIdList") List<Integer> empIdList);
		
	

	/*@Query(" SELECT new com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO "
			+ " ( conf.fkEmpId , prof.empDateOfJoining, conf.actualDateOfConfirmation , "
			+ " des.empDesignationName, level.empLevelName, conf.fkConfStatusId , "
			+ " prof.fkEmpLevel, prof.fkEmpDesignation , "
			+ " personal.empFirstName , personal.empLastName , conf.pkConfId , "
			+ " prof.fkEmpBuUnit , conf.delegatedToMgrId , prof.fkEmpReportingMgrId , feedback.pkFeedbackId ) "
			+ " from ConfDatProbationConfirmationBO conf "
			+ " join DatEmpDetailBO detail on conf.fkEmpId = detail.pkEmpId "
			+ " join DatEmpProfessionalDetailBO prof on detail.pkEmpId = prof.fkMainEmpDetailId "
			+ " join  DatEmpPersonalDetailBO personal on detail.pkEmpId = personal.fkEmpDetailId "
			+ " join MasEmpLevelBO level on level.pkEmpLevelId = prof.fkEmpLevel "
			+ " join MasEmpDesignationBO des on des.pkEmpDesignationId = prof.fkEmpDesignation "
			+ " join  DatEmpPersonalDetailBO mgrName on mgrName.fkEmpDetailId = conf.fkMgrId "
			+ " left join ConfDatFeedbackBO feedback on feedback.fkConfId = conf.pkConfId "
			+ " where conf.fkEmpId in (:empIdList) "
			+ " and conf.tentativeConfirmationDate >= :startDate and "
			+ " conf.tentativeConfirmationDate <= :endDate " )
	public List<ConfDatProbationConfirmationBO> searchDetailsByDateRange(@Param("empIdList") List<Integer> empIdList,@Param("startDate")Date startDate,@Param("endDate") Date endDate);
	*/
	 
	 @Query(nativeQuery = true, value ="SELECT fk_emp_id FROM conf_dat_probation_confirmation where delegated_to_mgr_id = :rmId"
	 		+ " UNION "
	 		+ "select fk_main_emp_detail_id from dat_emp_professional_detail where fk_emp_reporting_mgr_id = :rmId " )
	 public List<Integer> getDirectAndIndirectReporteesByRmId (@Param ("rmId") Integer rmId);
	 
	//EOA by Smrithi
	
	//Added by Kamal Anand for Notifications
	public ConfDatProbationConfirmationBO findByFkEmpIdAndFkConfStatusIdAndIsConfirmedByMgr(int empId,int statusId,String isConfirmedByMgr);
	
	public List<ConfDatProbationConfirmationBO> findByFkConfStatusIdAndIsConfirmedByMgr(int statusId,String isConfirmedByMgr);
	public ConfDatProbationConfirmationBO findByFkEmpIdAndFkConfStatusIdAndIsConfirmedByMgrAndDelegatedToMgrIdIsNotNull(int empId,int statusId,String isConfirmedByMgr);
	
	public List<ConfDatProbationConfirmationBO> findByFkMgrIdAndFkConfStatusIdAndIsConfirmedByMgrAndDelegatedToMgrIdIsNotNull(int rmId,int statusId,String isConfirmedByMgr);
	
	public List<ConfDatProbationConfirmationBO> findByFkConfStatusIdAndIsConfirmedByMgrAndDelegatedToMgrIdIsNotNull(int statusId,String isConfirmedByMgr);
	
	public ConfDatProbationConfirmationBO findByFkEmpIdAndFkConfStatusId(int empId,int statusId);
	
	public List<ConfDatProbationConfirmationBO> findByFkMgrIdAndFkConfStatusId(int rmId,int statusId);
	
	public List<ConfDatProbationConfirmationBO> findByDelegatedToMgrIdAndFkConfStatusId(int delegatedRmId,int statusId);
	
	//public ConfDatProbationConfirmationBO findByFkEmpIdAndFkConfStatusIdAndDelegatedToMgrIdIsNotNull(int empId,int statusId);
	
	public List<ConfDatProbationConfirmationBO> findByFkMgrIdAndFkConfStatusIdAndDelegatedToMgrIdIsNotNull(int rmId,int statusId);
	
	public ConfDatProbationConfirmationBO findByFkEmpIdAndFkConfStatusIdAndDelegatedToMgrIdIsNotNull(int empId,int statusId);
	
	//public List<ConfDatProbationConfirmationBO> findByFkMgrIdAndFkConfStatusIdAndDelegatedToMgrIdIsNotNull(int rmId,int statusId);
	
	public List<ConfDatProbationConfirmationBO> findByDelegatedToMgrIdAndFkConfStatusIdAndIsConfirmedByMgrAndDelegatedToMgrIdIsNotNull(int rmId,int statusId,String isConfirmedByMgr);
	
	
	//EOA by Kamal anand for Notifications
	
}