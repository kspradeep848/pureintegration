package com.thbs.mis.confirmation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO;
import com.thbs.mis.confirmation.bo.ConfMasStatusBO;

public interface ConfirmationStatusRepository extends
		GenericRepository<ConfMasStatusBO, Long>,
		JpaSpecificationExecutor<ConfDatProbationConfirmationBO> {
	
	public List<ConfMasStatusBO> findByPkStatusIdIn(List<Integer> confStatusIdList);

}
