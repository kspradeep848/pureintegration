package com.thbs.mis.confirmation.dao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.confirmation.bean.SearchDetailsInputBean;
import com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO;
public class ConfirmationSpecification {
	/**
	 * 
	 * @param searchBean
	 * @return Specification<DatEmpProfessionalDetailBO>
	 * @Description : This specification is used to get details based of
	 *              RmId or BuId
	 */
	public static Specification<DatEmpProfessionalDetailBO> getBuOrRmId(
			SearchDetailsInputBean searchBean) {
		
		return new Specification<DatEmpProfessionalDetailBO>() {
			
			final Collection<Predicate> predicates = new ArrayList<>();
			
			
			@Override
			public Predicate toPredicate(Root<DatEmpProfessionalDetailBO> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				
				if(searchBean.getBuUnitId() != null){
					final Predicate buPredicate = cb.equal(
							root.get("fkEmpBuUnit"), searchBean.getBuUnitId());
					predicates.add(buPredicate);
				}
				
				
				if(searchBean.getMgrId() != null){
					final Predicate rmPredicate = cb.equal(
							root.get("fkEmpReportingMgrId"),searchBean.getMgrId());
					predicates.add(rmPredicate);
				}
				
				return cb.and(predicates.toArray(new Predicate[predicates
				                       						.size()]));
				
			}
			
		};
	}
	
	
	
	/**
	 * 
	 * @param searchBean
	 * @return Specification<ConfDatProbationConfirmationBO>
	 * @Description : This specification is used to get details based of
	 *              statusId or empId
	 */
	public static Specification<ConfDatProbationConfirmationBO> getStatusOrEmpId(
			SearchDetailsInputBean searchBean,List<Integer> empIdList) {
		
		return new Specification<ConfDatProbationConfirmationBO>() {
			
			final Collection<Predicate> predicates = new ArrayList<>();
			
			
			@Override
			public Predicate toPredicate(Root<ConfDatProbationConfirmationBO> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				
				
					if(!empIdList.isEmpty()){
					final Predicate empIdListPredicate = root.get(
							"fkEmpId").in(empIdList);
					predicates.add(empIdListPredicate);
					}
				
				if(searchBean.getConfStatusIdList() != null){
					
					final Predicate statusIdPredicate = root.get("fkConfStatusId").in(
							searchBean.getConfStatusIdList());
					
					predicates.add(statusIdPredicate);
				}
				
				if (searchBean.getConfStartDate() != null && searchBean.getConfEndDate() != null) {
					/*Calendar calStartDate = Calendar.getInstance();
					Calendar calEndDate = Calendar.getInstance();
					calStartDate.setTime(searchBean.getConfStartDate());
					calStartDate.set(Calendar.HOUR_OF_DAY,0);
					calStartDate.set(Calendar.MINUTE,0);
					calStartDate.set(Calendar.SECOND,0);
					calEndDate.setTime(searchBean.getConfEndDate());
					calEndDate.set(Calendar.HOUR_OF_DAY,23);
					calEndDate.set(Calendar.MINUTE,59);
					calEndDate.set(Calendar.SECOND,59);*/
				
				final Predicate dateRangePredicate =  cb.and(cb.greaterThanOrEqualTo(root.get("tentativeConfirmationDate"),
						searchBean.getConfStartDate()), cb.lessThanOrEqualTo(
						root.get("tentativeConfirmationDate"), searchBean.getConfEndDate()));
					
				
				//final Predicate dateRangePredicate = cb.between(root.<Date>get("createdDate"), assetReq.getStartDate(), assetReq.getEndDate());
				
				
					predicates.add(dateRangePredicate);
				
			}
				
				return cb.and(predicates.toArray(new Predicate[predicates
				                       						.size()]));
				
			}
			
		};
	}
	
}