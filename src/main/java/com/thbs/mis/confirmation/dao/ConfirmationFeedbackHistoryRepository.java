package com.thbs.mis.confirmation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.confirmation.bo.ConfDatFeedbackHistoryBO;


@Repository
public interface ConfirmationFeedbackHistoryRepository extends GenericRepository<ConfDatFeedbackHistoryBO, Long>,  JpaSpecificationExecutor<ConfDatFeedbackHistoryBO> {

	
	//Added by Harsh
	public ConfDatFeedbackHistoryBO findByPkHistoryId(Integer historyId);
	public List<ConfDatFeedbackHistoryBO> findByFkEmpIdOrderByUpdatedOnDesc(Integer empId);
	//EOA by Harsh
	

}
