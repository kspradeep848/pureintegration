package com.thbs.mis.confirmation.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.text.ParseException;

import javax.validation.Valid;
import javax.validation.Validator;

import org.apache.commons.httpclient.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.confirmation.bean.ConfirmStatusByHrInputBean;
import com.thbs.mis.confirmation.bean.ConfirmationSubmitHRFeedbackInputBean;
import com.thbs.mis.confirmation.bean.ConfirmationUpdateHRFeedbackInputBean;
import com.thbs.mis.confirmation.bean.DelegateManagerByHrInputBean;
import com.thbs.mis.confirmation.bean.ExtendProbationByHrInputBean;
import com.thbs.mis.confirmation.bean.NotConfirmedByHrInputBean;
import com.thbs.mis.confirmation.constants.ConfirmationManagementURIConstants;
import com.thbs.mis.confirmation.service.ConfirmationByHRService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.ConfigException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

@Controller
public class ConfirmationByHRController {

	private static final AppLog LOG = LogFactory
			.getLog(ConfirmationByHRController.class);
	
	@Autowired
	private ConfirmationByHRService confirmationByHRService;
	
	//Added by Smrithi
	/**
	 * @param ConfirmStatusByHrInputBean
	 * @return
	 * @throws CommonCustomException
	 * @Description: This method is used to confirm the status  by HR.
	 */
	@ApiOperation(tags = "Confirmation Module", value = " Confirm Status By Hr "
			+ "This service will be called from the front-end when the HR wants to confirm the status ", httpMethod = "PUT", notes = "This Service has been implemented when the HR wants to confirm the status based on  ConfirmStatusByHrInputBean "
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) conf_dat_probation_confirmation -> It contains all confiramtion probation details"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> ConfirmStatusByHrInputBean <br><br>"
			+ "<br> fkEmpId : fkEmpId  in the format of integer <br>"
			+ "<br> dateOfConfirmation : dateOfConfirmation in the format of Date <br>"
			+ "<br> fkEmpLevel : fkEmpLevel in the format of Short <br>"
			+ "<br> fkEmpDesignation : fkEmpDesignation in the format of Short <br> ",

	nickname = "Confirmation Module Confirm Status by HR", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Confirm Status by HR", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ConfirmationManagementURIConstants.CONFIRM_STATUS_BY_HR, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> confirmByHr(
			@ApiParam(name = "Confirmation Module Confirm Status by HR", example = "1", value = "{"
					+ "<br> fkEmpId : 1, <br>"
					+ "<br> dateOfConfirmation : 12-12-2017, <br>"
					+ "<br> fkEmpLevel : 2, <br>"
					+ " <br> fkEmpDesignation : 27 <br>" + "}") @Valid @RequestBody ConfirmStatusByHrInputBean confirmStatusByHrInputBean)
			throws CommonCustomException {
		
		confirmationByHRService.confirmStatusByHr(confirmStatusByHrInputBean);
		
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"The employee is confirmed successfully and mail sent successfully ."));
	}
	
	/**
	 * @param ExtendProbationByHrInputBean
	 * @return
	 * @throws CommonCustomException
	 * @throws ParseException 
	 * @Description: This method is used to extend the probation period by HR.
	 */
	@ApiOperation(tags = "Confirmation Module", value = " Extend Probation Period By Hr"
			+ "This service will be called from the front-end when the HR wants to extend the probation period ", httpMethod = "PUT", notes = "This Service has been implemented to extend the probation period by HR based on  ExtendProbationByHrInputBean "
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) conf_dat_probation_confirmation -> It contains all confiramtion probation details"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> ExtendProbationByHrInputBean <br><br>"
			+ "<br> confExtendedInMonths : confExtendedInMonths  in the format of integer <br>"
			+ "<br> fkConfStatusId : fkConfStatusId in the format of integer <br>"
			+ "<br> confExtendedByHrId : confExtendedByHrId in the format of integer <br> ",

	nickname = "Confirmation Module Extend probation period by HR", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Extend Probation period by HR", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ConfirmationManagementURIConstants.EXTEND_PROBATION_BY_HR, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> extendByHr(
			@ApiParam(name = "Confirmation Module Extend probation period by HR", example = "1", value = "{"
					+ "<br> confExtendedInMonths : 1, <br>"
					+ "<br> fkConfStatusId : 1, <br>"
					+ "<br> confExtendedByHrId : 1806,  <br>" + "}") @Valid @RequestBody ExtendProbationByHrInputBean extendProbationByHrInputBean)
			throws CommonCustomException, ParseException {
		//try{
		confirmationByHRService.extendProbationByHr(extendProbationByHrInputBean);
		/*}catch(Exception e){
			throw new CommonCustomException(e.getMessage());
		}*/
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"Probation period is extended successfully."));
	}
	
	/**
	 * @param DelegateManagerByHrInputBean
	 * @return
	 * @throws CommonCustomException
	 * @Description: This method is used for delegating to another manager by HR.
	 */
	@ApiOperation(tags = "Confirmation Module", value = "Delegate Manager By Hr"
			+ "This service will be called from the front-end when the HR wants to delegate to another manager. ", httpMethod = "PUT", notes = "This Service has been implemented to delegate to another manager by HR based on DelegateManagerByHrInputBean "
			+ "<br>The Details will be fetched from database."
			+ "<br> <b> Table : </b> </br>"
			+ "1) conf_dat_probation_confirmation -> It contains all confiramtion probation details"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> ExtendProbationByHrInputBean <br><br>"
			+ "<br> fkEmpId : fkEmpId  in the format of integer <br>"
			+ "<br> delegatedComments : delegatedComments in the format of String <br>"
			+ "<br> delegatedByHrId : delegatedByHrId in the format of integer <br> "
			+ "<br> delegatedToMgrId : delegatedToMgrId in the format of integer <br> ",

	nickname = "Confirmation Module Delegate Manager by HR", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = " Delegate Manager by HR", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ConfirmationManagementURIConstants.DELEGATE_MANAGER_BY_HR, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> delegateManager(
			@ApiParam(name = "Confirmation Module Delegate Manager by HR", example = "1", value = "{"
					+ "<br> fkEmpId : 3806, <br>"
					+ "<br> delegatedComments : Currrent RM is not available hence it's delegated to you, <br>"
					+ "<br> delegatedByHrId : 1806,  <br>"
					+ "<br> delegatedToMgrId : 2188  <br>" + "}") @Valid @RequestBody DelegateManagerByHrInputBean delegateManagerByHrInputBean)
			throws CommonCustomException {
		//try{
		confirmationByHRService.delegateManager(delegateManagerByHrInputBean);
		/*}catch(Exception e){
			throw new CommonCustomException(e.getMessage());
		}*/
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"Delegation done successfully."));
	}
	
	
	
	/**
	 * 
	 * @param SendMailByHrInputBean
	 * @return
	 * @throws BusinessException
	 * @throws ConfigException
	 * @throws ParseException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 * @Description: This method is used to send mail for confirmed or extended 
	 *               employees.
	 */
	@ApiOperation(tags = "Confirmation Module", value = "Send mail by HR "
			+ "This service will be called from the front-end when the HR wants to send mail for confirmed or extended employees.", httpMethod = "GET", notes = "This Service has been implemented  when the HR wants to  send mail for confirmed or extended employees based on SendMailByHrInputBean ."
			+ "<br> <b> Table : </b> </br>"
			+"1) conf_dat_probation_confirmation -> It contains all confiramtion probation details"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> SendMailByHrInputBean <br><br>"
			+ "<br> empId : empId in the format of integer <br>"
			+ "<br> statusId : statusId in the format of integer <br>",

	nickname = "Send mail by HR", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Send mail by HR ", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ConfirmationManagementURIConstants.SEND_MAIL_BY_HR, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> sendMailByHr(@PathVariable Integer empId)
			throws BusinessException, DataAccessException, ParseException,
			ConfigException, CommonCustomException {
		LOG.startUsecase("Send mail by HR.");
		boolean isUpdated = false;
		
		isUpdated = confirmationByHRService.sendMailByHr(empId);
		LOG.endUsecase("Send mail by HR.");
		if (isUpdated) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Mail sent successfully ."));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Sorry, Mail could not be sent."));
		}
	}
	
	
	
	
	//EOA by Smrithi
	
	
	// Added by Harsh

			@Autowired
			private Validator validator;

			/**
			 * @param ConfirmationSubmitHRFeedbackInputBean
			 * @return
			 * @throws CommonCustomException
			 * @Description: This method is used to Submit Feedback by HR.
			 */
			@ApiOperation(tags = "Confirmation Module", value = " Submit Feedback by HR"
					+ "This service will be called from the front-end when the HR wants to Submit Feedback for employee ", httpMethod = "POST", notes = "This Service has been implemented to submit Feedback by HR based on ConfirmationSubmitHRFeedbackInputBean "
					+ "<br>The Details will be saved to database."
					+ "<br> <b> Table : </b> </br>"
					+ "1) conf_dat_probation_confirmation -> It contains all confiramtion probation details"
					+ "<br>"
					+ "<br><b>Inputs :</b><br> ConfirmationSubmitHRFeedbackInputBean <br><br>"
					+ "<br> empId :  empId in the format of Integer <br>"
					+ "<br> attitude : attitude in the format of String <br>"
					+ "<br> communication : communication in the format of String <br>"
					+ "<br> willingnessToLearn : willingnessToLearn in the format of String  <br>"
					+ "<br> selfConfidence : selfConfidence in the format of String  <br>"
					+ "<br> flexibility : flexibility in the format of String  <br>"
					+ "<br> proactiveness : proactiveness in the format of String  <br>"
					+ "<br> technicalExpertise : technicalExpertise in the format of String  <br>"
					+ "<br> dependability : dependability in the format of String  <br>"
					+ "<br> workObjectives : workObjectives in the format of String  <br>"
					+ "<br> feedback : feedback in the format of String  <br>"
					+ "<br> areasOfImprovements : areasOfImprovements in the format of String  <br>"
					+ "<br> loggedInUser : loggedInUser in the format of Integer <br>",	nickname = "Confirmation Module Submit Feedback by HR", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
			@ApiResponses(value = {
					@ApiResponse(code = 200, message = "Request Executed Successfully"),
					@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Submit Feedback by HR", response = URI.class)),
					@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
					@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
			@RequestMapping(value = ConfirmationManagementURIConstants.SUBMIT_FEEDBACK_BY_HR, method = RequestMethod.POST)
			public @ResponseBody ResponseEntity<MISResponse> submitFeedbackByHr(
					@ApiParam(name = "Confirmation Module Submit Feedback by HR", example = "1", value = "{"
							+ "<br> empId :  3951 <br>"
							+ "<br> attitude : 4 <br>"
							+ "<br> communication : 3 <br>"
							+ "<br> willingnessToLearn : 4  <br>"
							+ "<br> selfConfidence : 5  <br>"
							+ "<br> flexibility : 3  <br>"
							+ "<br> proactiveness : 3  <br>"
							+ "<br> technicalExpertise : 4  <br>"
							+ "<br> dependability : 4  <br>"
							+ "<br> workObjectives : Achieved the goals set during probation  <br>"
							+ "<br> feedback : Good performance  <br>"
							+ "<br> areasOfImprovements : Need to be more flexible  <br>"
							+ "<br> loggedInUser : 3005 <br>" + "}") @Valid @RequestBody ConfirmationSubmitHRFeedbackInputBean confirmationSubmitHRFeedbackInputBean)
					throws CommonCustomException {
				LOG.startUsecase("HR Feedback Controller Begins");
				try{
				confirmationByHRService.submitFeedbackByHr(confirmationSubmitHRFeedbackInputBean);
				}catch(Exception e){
					throw new CommonCustomException(e.getMessage());
				}
				LOG.endUsecase("HR Feedback Controller Ends");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
								"HR Feedback is submitted successfully."));
			}
			
			
			
			/**
			 * @param ConfirmationUpdateHRFeedbackInputBean
			 * @return
			 * @throws CommonCustomException
			 * @Description: This method is used to Update Feedback provided by HR.
			 */
			@ApiOperation(tags = "Confirmation Module", value = " Update Feedback by HR"
					+ "This service will be called from the front-end when the HR wants to Update Feedback for employee ", httpMethod = "POST", notes = "This Service has been implemented to update Feedback by HR based on ConfirmationUpdateHRFeedbackInputBean "
					+ "<br>The Details will be updated in database."
					+ "<br> <b> Table : </b> </br>"
					+ "1) conf_dat_feedback -> It contains HR Feedback for all employees"
					+ "<br>"
					+ "<br><b>Inputs :</b><br> ConfirmationUpdateHRFeedbackInputBean <br><br>"
					+ "<br> empId :  empId in the format of Integer <br>"
					+ "<br> pkFeedbackId :  feedbackId in the format of Integer <br>"
					+ "<br> attitude : attitude in the format of String <br>"
					+ "<br> communication : communication in the format of String <br>"
					+ "<br> willingnessToLearn : willingnessToLearn in the format of String  <br>"
					+ "<br> selfConfidence : selfConfidence in the format of String  <br>"
					+ "<br> flexibility : flexibility in the format of String  <br>"
					+ "<br> proactiveness : proactiveness in the format of String  <br>"
					+ "<br> technicalExpertise : technicalExpertise in the format of String  <br>"
					+ "<br> dependability : dependability in the format of String  <br>"
					+ "<br> workObjectives : workObjectives in the format of String  <br>"
					+ "<br> feedback : feedback in the format of String  <br>"
					+ "<br> areasOfImprovements : areasOfImprovements in the format of String  <br>"
					+ "<br> loggedInUser : loggedInUser in the format of Integer <br>",	nickname = "Confirmation Module Submit Feedback by HR", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
			@ApiResponses(value = {
					@ApiResponse(code = 200, message = "Request Executed Successfully"),
					@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Submit Feedback by HR", response = URI.class)),
					@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
					@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
			@RequestMapping(value = ConfirmationManagementURIConstants.UPDATE_FEEDBACK_BY_HR, method = RequestMethod.PUT)
			public @ResponseBody ResponseEntity<MISResponse> updateFeedbackByHr(
					@ApiParam(name = "Confirmation Module Submit Feedback by HR", example = "1", value = "{"
							+ "<br> empId :  3951 <br>"
							+ "<br> pkFeedbackId :  1 <br>"
							+ "<br> attitude : 4 <br>"
							+ "<br> communication : 3 <br>"
							+ "<br> willingnessToLearn : 4  <br>"
							+ "<br> selfConfidence : 5  <br>"
							+ "<br> flexibility : 3  <br>"
							+ "<br> proactiveness : 3  <br>"
							+ "<br> technicalExpertise : 4  <br>"
							+ "<br> dependability : 4  <br>"
							+ "<br> workObjectives : Achieved the goals set during probation  <br>"
							+ "<br> feedback : Good performance  <br>"
							+ "<br> areasOfImprovements : Need to be more flexible  <br>"
							+ "<br> loggedInUser : 3005 <br>" + "}") @Valid @RequestBody ConfirmationUpdateHRFeedbackInputBean confirmationUpdateHRFeedbackInputBean)
					throws CommonCustomException {
				LOG.startUsecase("HR Feedback Controller Begins"); 
				try{
				confirmationByHRService.updateFeedbackByHr(confirmationUpdateHRFeedbackInputBean);
				}catch(Exception e){
					throw new CommonCustomException(e.getMessage());
				}
				LOG.endUsecase("HR Feedback Controller Ends");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
								"HR Feedback is updated successfully."));
			}
	//EOA by Harsh
			
	//Added by Kamal Anand	
	@RequestMapping(value = ConfirmationManagementURIConstants.NOT_CONFIRM_STATUS_BY_HR, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> notConfirmByHr(
			@Valid @RequestBody NotConfirmedByHrInputBean notConfirmedByHrInputBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering notConfirmByHr controller");
		boolean isNotConfirmed = false;

		try {
			isNotConfirmed = confirmationByHRService
					.notConfirmByHr(notConfirmedByHrInputBean);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		if (isNotConfirmed)
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"The employee is not confirmed ."));
		else
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Not able to change confirmation status to not confirmed."));
	}
	//EOA by Kamal Anand
}
