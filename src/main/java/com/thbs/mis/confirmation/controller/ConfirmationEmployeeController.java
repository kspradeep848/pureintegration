package com.thbs.mis.confirmation.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thbs.mis.confirmation.bean.GenerateConfExtLetterOutputBean;
import com.thbs.mis.confirmation.bean.GetConfFeedbackDetailsByHistoryIdOutputBean;
import com.thbs.mis.confirmation.bean.GetFeedbackModificationDatesOutputBean;
import com.thbs.mis.confirmation.constants.ConfirmationManagementURIConstants;
import com.thbs.mis.confirmation.service.ConfirmationEmployeeService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@Controller
public class ConfirmationEmployeeController {

	private static final AppLog LOG = LogFactory.getLog(ConfirmationEmployeeController.class);

	@Autowired
	private ConfirmationEmployeeService confEmpService;

	

	//Added by Harsh
	/**
	 * 
	 * @return Employee Feedback History By History id.
	 * @throws CommonCustomException 
	 * @Description: This method is used to Get Employee Feedback History for a given Date.
	 * 
	 */
	@ApiOperation(tags = "Confirmation", value = "Get Employee Feedback History"
			+ "This service will be called from the front-end when the user want to view the Employee Feedback History.", httpMethod = "GET", notes = "This Service has been implemented to view the Employee Feedback History."
			+ "<br> <b> Table : </b> </br>"
			+ "1)conf_dat_feedback_history -> It contains all the Feedback modifiaction history for an employee."
			+ "<br>"
			+ "<br><b>Inputs : <br>"
			+ "<br> historyId : historyId in the format of integer <br>",
	nickname = "Get Employee Feedback History", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get Employee Feedback History", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ConfirmationManagementURIConstants.GET_FEEDBACK_HISTORY, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getFeedbackHistory
	(@PathVariable Integer historyId) throws CommonCustomException
	  {
		LOG.startUsecase("Get Employee Feedback History");
		GetConfFeedbackDetailsByHistoryIdOutputBean outputBean = new GetConfFeedbackDetailsByHistoryIdOutputBean();
		List<GetConfFeedbackDetailsByHistoryIdOutputBean> outputBeanList = new ArrayList<GetConfFeedbackDetailsByHistoryIdOutputBean>();
		try{
		outputBean = confEmpService.getFeedbackHistoryById(historyId);
		outputBeanList.add(outputBean);
		}
		catch(CommonCustomException e){
			throw new CommonCustomException(e.getMessage());
		}
		if(outputBean != null){
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Feedback History displayed successfully.",outputBeanList));
		}else{
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE, "No Feedback History to display.",outputBeanList));
		}
		
	}
	
	
	/**
	 * 
	 * @return Employee Feedback Modification Dates.
	 * @throws CommonCustomException 
	 * @Description: This method is used to Get Employee Feedback Modification Dates List.
	 * 
	 */
	@ApiOperation(tags = "Confirmation", value = "Get Employee Feedback Modification Dates"
			+ "This service will be called from the front-end when the user want to view the Employee Feedback Modification Dates.", httpMethod = "GET", notes = "This Service has been implemented to view the Employee Feedback Modification Dates."
			+ "<br> <b> Table : </b> </br>"
			+ "1)conf_dat_feedback_history -> It contains all the Feedback modifiaction history for an employee."
			+ "<br>"
			+ "<br><b>Inputs : <br>"
			+ "<br> empId : empId in the format of integer <br>",
	nickname = "Get Employee Feedback Modification Dates", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get Employee Feedback Modification Dates", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ConfirmationManagementURIConstants.GET_FEEDBACK_DATES, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getFeedbackDates
	(@PathVariable Integer empId) throws CommonCustomException
	  {
		LOG.startUsecase("Get Employee Feedback Modification Dates");
		
		List<GetFeedbackModificationDatesOutputBean> outputBeanList = new ArrayList<GetFeedbackModificationDatesOutputBean>();
		try{
		outputBeanList = confEmpService.getFeedbackDatesByEmpId(empId);
		
		}
		catch(CommonCustomException e){
			throw new CommonCustomException(e.getMessage());
		}
		if(outputBeanList != null){
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Feedback Modification Dates displayed successfully.",outputBeanList));
		}else{
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE, "No Feedback Modification Dates to display.",outputBeanList));
		}
		
	}
	
	
	
	
	/**
	 * 
	 * @return Generate Confirmation Extension Letter.
	 * @throws CommonCustomException 
	 * @Description: This method is used to Generate Confirmation Extension Letter.
	 * 
	 */
	@ApiOperation(tags = "Confirmation", value = "Generate Confirmation Extension Letter"
			+ "This service will be called from the front-end when the user want to Generate Confirmation / Extension Letter.", httpMethod = "GET", notes = "This Service has been implemented to Generate Confirmation Extension Letter."
			+ "<br> <b> Table : </b> </br>"
			+ "1)conf_dat_confirmation_probation -> It contains all the confirmation detail for an employee."
			+ "<br>"
			+ "<br><b>Inputs : <br>"
			+ "<br> empId : empId in the format of integer <br>",
	nickname = "Generate Confirmation Extension Letter", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Generate Confirmation Extension Letter", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ConfirmationManagementURIConstants.GENERATE_CONF_EXT_LETTER, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> generateConfExtLetter
	(@PathVariable Integer empId) throws CommonCustomException
	  {
		LOG.startUsecase("Generate Confirmation Extension Letter");
		GenerateConfExtLetterOutputBean outputBean = new GenerateConfExtLetterOutputBean();
		List<GenerateConfExtLetterOutputBean> outputBeanList = new ArrayList<GenerateConfExtLetterOutputBean>();
		try{
			outputBean = confEmpService.generateConfExtLetter(empId);
		
		}
		catch(CommonCustomException e){
			throw new CommonCustomException(e.getMessage());
		}
		
		outputBeanList.add(outputBean);
		
		if(outputBean.getFilePath() != null){
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Confirmation/Extension Letter generated successfully.",outputBeanList));
		}else{
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE, "Confirmation/Extension Letter generation failed.",outputBeanList));
		}
		
	}
	
	
	//EOA by Harsh
	
	//EOA by Harsh
	
}
