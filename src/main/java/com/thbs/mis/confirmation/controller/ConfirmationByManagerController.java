package com.thbs.mis.confirmation.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.util.Set;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.Validator;

import org.apache.commons.httpclient.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

import com.thbs.mis.confirmation.bean.ConfirmationSubmitFeedbackInputBean;
import com.thbs.mis.confirmation.bean.ConfirmationUpdateFeedbackInputBean;
import com.thbs.mis.confirmation.bean.ExtendProbationByHrInputBean;
import com.thbs.mis.confirmation.constants.ConfirmationManagementURIConstants;
import com.thbs.mis.confirmation.service.ConfirmationByManagerService;

@Controller
public class ConfirmationByManagerController {

	private static final AppLog LOG = LogFactory
			.getLog(ConfirmationByManagerController.class);

	@Autowired
	private ConfirmationByManagerService confirmationByManagerService;


	// Added by Harsh

		@Autowired
		private Validator validator;

		/**
		 * @param ConfirmationSubmitFeedbackInputBean
		 * @return
		 * @throws CommonCustomException
		 * @Description: This method is used to Submit Feedback by Manager.
		 */
		@ApiOperation(tags = "Confirmation Module", value = " Submit Feedback by Manager"
				+ "This service will be called from the front-end when the Manager wants to Submit Feedback for employee ", httpMethod = "POST", notes = "This Service has been implemented to submit Feedback by manager based on ConfirmationSubmitFeedbackInputBean "
				+ "<br>The Details will be saved to database."
				+ "<br> <b> Table : </b> </br>"
				+ "1) conf_dat_probation_confirmation -> It contains all confiramtion probation details"
				+ "<br>"
				+ "<br><b>Inputs :</b><br> ConfirmationSubmitFeedbackInputBean <br><br>"
				+ "<br> empId :  empId in the format of Integer <br>"
				+ "<br> attitude : attitude in the format of String <br>"
				+ "<br> communication : communication in the format of String <br>"
				+ "<br> willingnessToLearn : willingnessToLearn in the format of String  <br>"
				+ "<br> selfConfidence : selfConfidence in the format of String  <br>"
				+ "<br> flexibility : flexibility in the format of String  <br>"
				+ "<br> proactiveness : proactiveness in the format of String  <br>"
				+ "<br> technicalExpertise : technicalExpertise in the format of String  <br>"
				+ "<br> dependability : dependability in the format of String  <br>"
				+ "<br> workObjectives : workObjectives in the format of String  <br>"
				+ "<br> feedback : feedback in the format of String  <br>"
				+ "<br> areasOfImprovements : areasOfImprovements in the format of String  <br>"
				+ "<br> isConfirmedByMgr : isConfirmedByMgr in the format of String  <br>"
				+ "<br> confirmedByMgrId : confirmedByMgrId in the format of Integer  <br>"
				+ "<br> mgrDenialReason : mgrDenialReason in the format of String  <br>"
				+ "<br> loggedInUser : loggedInUser in the format of Integer <br>",	nickname = "Confirmation Module Submit Feedback by Manager", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully"),
				@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Submit Feedback by Manager", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = ConfirmationManagementURIConstants.SUBMIT_FEEDBACK_BY_MANAGER, method = RequestMethod.POST)
		public @ResponseBody ResponseEntity<MISResponse> submitFeedbackByManager(
				@ApiParam(name = "Confirmation Module Submit Feedback by Manager", example = "1", value = "{"
						+ "<br> empId :  3951 <br>"
						+ "<br> attitude : 4 <br>"
						+ "<br> communication : 3 <br>"
						+ "<br> willingnessToLearn : 4  <br>"
						+ "<br> selfConfidence : 5  <br>"
						+ "<br> flexibility : 3  <br>"
						+ "<br> proactiveness : 3  <br>"
						+ "<br> technicalExpertise : 4  <br>"
						+ "<br> dependability : 4  <br>"
						+ "<br> workObjectives : Achieved the goals set during probation  <br>"
						+ "<br> feedback : Good performance  <br>"
						+ "<br> areasOfImprovements : Need to be more flexible  <br>"
						+ "<br> isConfirmedByMgr : 1 <br>"
						+ "<br> confirmedByMgrId : 3005  <br>"
						+ "<br> mgrDenialReason : NULL  <br>"
						+ "<br> loggedInUser : 3005 <br>" + "}") @Valid @RequestBody ConfirmationSubmitFeedbackInputBean confirmationSubmitFeedbackInputBean)
				throws CommonCustomException {
			LOG.startUsecase("Manager Feedback Controller Begins");
			try{
			confirmationByManagerService.submitFeedbackByManager(confirmationSubmitFeedbackInputBean);
			}catch(Exception e){
				throw new CommonCustomException(e.getMessage());
			}
			LOG.endUsecase("Manager Feedback Controller Ends");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"Manager Feedback is submitted successfully."));
		}
		
		
		
		/**
		 * @param ConfirmationUpdateFeedbackInputBean
		 * @return
		 * @throws CommonCustomException
		 * @Description: This method is used to Update Feedback provided by Manager.
		 */
		@ApiOperation(tags = "Confirmation Module", value = " Update Feedback by Manager"
				+ "This service will be called from the front-end when the Manager wants to Update Feedback for employee ", httpMethod = "POST", notes = "This Service has been implemented to update Feedback by manager based on ConfirmationUpdateFeedbackInputBean "
				+ "<br>The Details will be updated in database."
				+ "<br> <b> Table : </b> </br>"
				+ "1) conf_dat_feedback -> It contains Manager Feedback for all employees"
				+ "<br>"
				+ "<br><b>Inputs :</b><br> ConfirmationUpdateFeedbackInputBean <br><br>"
				+ "<br> empId :  empId in the format of Integer <br>"
				+ "<br> pkFeedbackId :  feedbackId in the format of Integer <br>"
				+ "<br> attitude : attitude in the format of String <br>"
				+ "<br> communication : communication in the format of String <br>"
				+ "<br> willingnessToLearn : willingnessToLearn in the format of String  <br>"
				+ "<br> selfConfidence : selfConfidence in the format of String  <br>"
				+ "<br> flexibility : flexibility in the format of String  <br>"
				+ "<br> proactiveness : proactiveness in the format of String  <br>"
				+ "<br> technicalExpertise : technicalExpertise in the format of String  <br>"
				+ "<br> dependability : dependability in the format of String  <br>"
				+ "<br> workObjectives : workObjectives in the format of String  <br>"
				+ "<br> feedback : feedback in the format of String  <br>"
				+ "<br> areasOfImprovements : areasOfImprovements in the format of String  <br>"
				+ "<br> isConfirmedByMgr : isConfirmedByMgr in the format of String  <br>"
				+ "<br> confirmedByMgrId : confirmedByMgrId in the format of Integer  <br>"
				+ "<br> mgrDenialReason : mgrDenialReason in the format of String  <br>"
				+ "<br> loggedInUser : loggedInUser in the format of Integer <br>",	nickname = "Confirmation Module Submit Feedback by Manager", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
		@ApiResponses(value = {
				@ApiResponse(code = 200, message = "Request Executed Successfully"),
				@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Submit Feedback by Manager", response = URI.class)),
				@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
				@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		@RequestMapping(value = ConfirmationManagementURIConstants.UPDATE_FEEDBACK_BY_MANAGER, method = RequestMethod.PUT)
		public @ResponseBody ResponseEntity<MISResponse> updateFeedbackByManager(
				@ApiParam(name = "Confirmation Module Submit Feedback by Manager", example = "1", value = "{"
						+ "<br> empId :  3951 <br>"
						+ "<br> pkFeedbackId :  1 <br>"
						+ "<br> attitude : 4 <br>"
						+ "<br> communication : 3 <br>"
						+ "<br> willingnessToLearn : 4  <br>"
						+ "<br> selfConfidence : 5  <br>"
						+ "<br> flexibility : 3  <br>"
						+ "<br> proactiveness : 3  <br>"
						+ "<br> technicalExpertise : 4  <br>"
						+ "<br> dependability : 4  <br>"
						+ "<br> workObjectives : Achieved the goals set during probation  <br>"
						+ "<br> feedback : Good performance  <br>"
						+ "<br> areasOfImprovements : Need to be more flexible  <br>"
						+ "<br> isConfirmedByMgr : 1 <br>"
						+ "<br> confirmedByMgrId : 3005  <br>"
						+ "<br> mgrDenialReason : NULL  <br>"
						+ "<br> loggedInUser : 3005 <br>" + "}") @Valid @RequestBody ConfirmationUpdateFeedbackInputBean confirmationUpdateFeedbackInputBean)
				throws CommonCustomException {
			LOG.startUsecase("Manager Feedback Controller Begins"); 
			try{
			confirmationByManagerService.updateFeedbackByManager(confirmationUpdateFeedbackInputBean);
			}catch(Exception e){
				throw new CommonCustomException(e.getMessage());
			}
			LOG.endUsecase("Manager Feedback Controller Ends");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"Manager Feedback is updated successfully."));
		}
	//EOA by Harsh	
}