package com.thbs.mis.confirmation.controller;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.common.bean.EmployeeNameWithRMIdBean;
import com.thbs.mis.confirmation.bean.GetConfDetailsByEmpIdOutputBean;
import com.thbs.mis.confirmation.bean.GetConfFeedbackDetailsByEmpIdOutputBean;
import com.thbs.mis.confirmation.bean.GetFeedbackDetailsOutputBean;
import com.thbs.mis.confirmation.bean.GetManagerNamesOutputBean;
import com.thbs.mis.confirmation.bean.SearchDetailsInputBean;
import com.thbs.mis.confirmation.bean.SearchDetailsOutputBean;
import com.thbs.mis.confirmation.bo.ConfDatProbationConfirmationBO;
import com.thbs.mis.confirmation.constants.ConfirmationManagementURIConstants;
import com.thbs.mis.confirmation.service.ConfirmationCommonService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bo.GsrWorkflowPermissionBO; 

@Controller
public class ConfirmationCommonController {
	private static final AppLog LOG = LogFactory
			.getLog(ConfirmationByManagerController.class);
	@Autowired
	private ConfirmationCommonService confCommonService;
	
//Added by Smrithi
	/**
	 * 
	 * @return Confirmation status of the employee id.
	 * @throws CommonCustomException 
	 * @Description: This method is used to get the Confirmation status of the employee id.
	 * 
	 */

	@ApiOperation(tags = "Confirmation", value = "Get Confirmation status by empId"
			+ "This service will be called from the front-end when the user want to view the Confirmation status of the employee id.", httpMethod = "GET", notes = "This Service has been implemented to view the Confirmation status of the employee id"
			+ "<br> <b> Table : </b> </br>"
			+ "1)conf_dat_probation_confirmation -> It contains all the details of probation confirmation."
			+ "<br>"
			+ "<br><b>Inputs : <br>"
			+ "<br> empId : empId in the format of integer <br>",
	nickname = "Get Confirmation status by empId", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ConfDatProbationConfirmationBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrWorkflowPermissionBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get Confirmation status by empId", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ConfirmationManagementURIConstants.GET_CONFIRMATION_STATUS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getConfirmationDetails
	(@PathVariable Integer empId) throws CommonCustomException
	  {
		LOG.startUsecase("Get Confirmation status by empId");
		GetConfDetailsByEmpIdOutputBean outputBean = new GetConfDetailsByEmpIdOutputBean();
		List<GetConfDetailsByEmpIdOutputBean> outputBeanList = new ArrayList<GetConfDetailsByEmpIdOutputBean>();
		try{
		outputBean = confCommonService.getConfDetailsByEmpId(empId);
		outputBeanList.add(outputBean);
		}
		catch(CommonCustomException e){
			throw new CommonCustomException(e.getMessage());
		}
		if(outputBean != null){
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Confirmation details are  displayed successfully.",outputBeanList));
		}else{
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE, "No Confirmation details to display.",outputBeanList));
		}
		
	}
	
	/**
 	 * @param SearchDetailsInputBean
 	 * @return
 	 * @throws CommonCustomException
 	 * @Description: This method is used to reject capex records by BU Head.
 	 */
 	@ApiOperation(tags = "Search employee details ", value = "Search employee details. "
 			+ "This service will be called from the front-end to search the employee details by various filters. ", httpMethod = "POST", notes = "This Service has been implemented to search the employee details based on SearchDetailsInputBean "
 			+ "<br>The Details will be fetched from database."
 			+ "<br> <b> Table : </b> </br>"
 			+ "1)conf_dat_probation_confirmation -> It contains all the details of probation confirmation."
 			+ "<br>"
 			+ "<br><b>Inputs :</b><br> SearchDetailsInputBean <br><br>"
 			+ "<br> empId : empId  in the format of integer <br>"
 			+ "<br> mgrId : mgrId  in the format of integer <br>"
 			+ "<br> confStatusId : confStatusId in the format of integer <br>"
 			+ "<br> empName : empName in the format of String <br>"
 			+ "<br> mgrName : mgrName in the format of String <br>"
 			+ "<br> buUnitId : buUnitId in the format of Short <br>"
 			+ "<br> confStartDate : confStartDate in the format of Date <br>"
 			+ "<br> confEndDate : confEndDate in the format of Date <br>",
 
 	nickname = "Confirmation Module Search employee details ", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
 	@ApiResponses(value = {
 			@ApiResponse(code = 200, message = "Request Executed Successfully"),
 			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Search employee details ", response = URI.class)),
 			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
 			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
 	@RequestMapping(value = ConfirmationManagementURIConstants.SEARCH_DETAILS, method = RequestMethod.POST)
 	public @ResponseBody ResponseEntity<MISResponse> searchDetails(
 			 @Valid @RequestBody SearchDetailsInputBean searchDetailsInputBean)
 			throws CommonCustomException 
 	{
 		LOG.startUsecase("Search employee details");
 		List<SearchDetailsOutputBean> outputList = new ArrayList<SearchDetailsOutputBean>();
 		SearchDetailsOutputBean searchDetailsOutput = new SearchDetailsOutputBean();
 		outputList = confCommonService
 				.searchDetails(searchDetailsInputBean);
 
 		/*if(searchDetailsOutput != null)
 			outputList.add(searchDetailsOutput);*/
 		
 		if(!outputList.isEmpty())
 		{
 			return ResponseEntity.status(HttpStatus.OK.value()).body(
 				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
 						"Details are displayed successfully.", outputList));
 		}else{
 			return ResponseEntity.status(HttpStatus.OK.value()).body(
 					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
 							"No details Found", outputList));
 		}
 	}
	
 	
 
	/**
	 * 
	 * @return Names of the skip level manager and skip's level manager's manager.
	 * @throws CommonCustomException 
	 * @Description: This method is used to get the Manager names of the skip level manager and skip's level manager's manager.
	 * 
	 */
	@ApiOperation(tags = "Confirmation", value = "Get Manager Names "
			+ "This service will be called from the front-end when the user want to view the skip level manager and skip's level manager's manager of the employee id.", httpMethod = "GET", notes = "This Service has been implemented to view the skip level manager and skip's level manager's manager of the employee id"
			+ "<br> <b> Table : </b> </br>"
			+ "1)conf_dat_probation_confirmation -> It contains all the details of probation confirmation."
			+ "<br>"
			+ "<br><b>Inputs : <br>"
			+ "<br> empId : empId in the format of integer <br>",
	nickname = "GetGet Manager Names ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ConfDatProbationConfirmationBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrWorkflowPermissionBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get Manager Names ", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ConfirmationManagementURIConstants.GET_MANAGER_NAMES, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getManagerNames
	(@PathVariable Integer empId) throws CommonCustomException
	  {
		LOG.startUsecase("Get Manager Names ");
		//GetManagerNamesOutputBean outputBean = new ArrayList<>();
		List<GetManagerNamesOutputBean> outputBeanList = new ArrayList<GetManagerNamesOutputBean>();
		outputBeanList = confCommonService.getManagerNames(empId);
		//outputBeanList.add(outputBeanList);
		if(outputBeanList != null){
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Manager names are displayed successfully.",outputBeanList));
		}else{
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE, "No Manager names to display.",outputBeanList));
		}
		
	}
	
	
	/**
	 * 
	 * @throws CommonCustomException 
	 * @Description: This method is used to Get direct and indirect reportees of RM by rmId.
	 * 
	 */

	@ApiOperation(tags = "Confirmation", value = "Get direct and indirect reportees of RM by rmId."
			+ "This service will be called from the front-end when the user want to view the direct and indirect reportees of RM.", httpMethod = "GET", notes = "This Service has been implemented to view the direct and indirect reportees of RM."
			+ "<br> <b> Table : </b> </br>"
			+ "1)conf_dat_probation_confirmation -> It contains all the details of probation confirmation."
			+ "2) dat_emp_professional_detail -> It contains all the professional details of an employee. "
			+ "<br>"
			+ "<br><b>Inputs : <br>"
			+ "<br> rmId : empId in the format of integer <br>",
	nickname = "Get direct and indirect reportees of RM by rmId.", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = ConfDatProbationConfirmationBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrWorkflowPermissionBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get Confirmation status by empId", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ConfirmationManagementURIConstants.GET_DIRECT_AND_INDIRECT_REPORTEES, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getDirectAndIndirectReporteesDetails
	(@PathVariable Integer rmId) throws CommonCustomException
	  {
		LOG.startUsecase("Get Direct And Indirect Reportees Details");
		List<EmployeeNameWithRMIdBean> outputList = new ArrayList<EmployeeNameWithRMIdBean>();
		outputList = confCommonService.getDirectAndIndirectReporteesDetails(rmId);
//outputBeanList.add(outputBean);
		if(outputList != null){
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "All direct and indirect reportees have been retrieved successfully.",outputList));
		}else{
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE, "No details to display.",outputList));
		}
		
	}
	
	
	//EOA by Smrithi
	
	//Added by Harsh
	/**
	 * 
	 * @return Confirmation and Feedback details of the employee id.
	 * @throws CommonCustomException 
	 * @Description: This method is used to get the Confirmation details of the employee id.
	 * 
	 */
	@ApiOperation(tags = "Confirmation", value = "Get Confirmation and Feedback details"
			+ "This service will be called from the front-end when the user want to view the Confirmation details of the employee id.", httpMethod = "GET", notes = "This Service has been implemented to view the Confirmation and Feedback details of the employee id"
			+ "<br> <b> Table : </b> </br>"
			+ "1)conf_dat_probation_confirmation -> It contains all the details of probation confirmation."
			+ "<br> 2)conf_dat_feedback -> It contains all the Feedback details of employees."
			+ "<br>"
			+ "<br><b>Inputs : <br>"
			+ "<br> empId : empId in the format of integer <br>",
	nickname = "Get Confirmation details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Get Confirmation details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = ConfirmationManagementURIConstants.GET_CONFIRMATION_DETAIL, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getConfirmationFeedbackDetails
	(@PathVariable Integer empId) throws CommonCustomException
	  {
		LOG.startUsecase("Get Confirmation details");
		GetConfFeedbackDetailsByEmpIdOutputBean outputBean = new GetConfFeedbackDetailsByEmpIdOutputBean();
		List<GetConfFeedbackDetailsByEmpIdOutputBean> outputBeanList = new ArrayList<GetConfFeedbackDetailsByEmpIdOutputBean>();
		try{
		outputBean = confCommonService.getConfFeedbackDetailsByEmpId(empId);
		outputBeanList.add(outputBean);
		}
		catch(CommonCustomException e){
			throw new CommonCustomException(e.getMessage());
		}
		if(outputBean != null){
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Confirmation and feedback details are displayed successfully.",outputBeanList));
		}else{
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE, "No Confirmation details to display.",outputBeanList));
		}
		
	}
	
	
	//EOA by Harsh
}