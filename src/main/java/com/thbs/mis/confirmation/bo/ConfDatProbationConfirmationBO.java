package com.thbs.mis.confirmation.bo;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.MasEmpLevelBO;

/**
 * The persistent class for the conf_dat_probation_confirmation database table.
 * 
 */
@Entity
@Table(name="conf_dat_probation_confirmation")
@NamedQuery(name="ConfDatProbationConfirmationBO.findAll", query="SELECT c FROM ConfDatProbationConfirmationBO c")

public class ConfDatProbationConfirmationBO implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="pk_conf_id")
	private Integer pkConfId;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="actual_date_of_confirmation")
	private Date actualDateOfConfirmation;
	@Column(name="conf_extended_in_months")
	private Integer confExtendedInMonths;
	@Column(name="confirmation_letter_sent")
	private String confirmationLetterSent;
	@Column(name="delegated_comments")
	private String delegatedComments;
	@Column(name="extension_letter_sent")
	private String extensionLetterSent;
	@Column(name="is_confirmed_by_hr")
	private String isConfirmedByHr;
	@Column(name="is_confirmed_by_mgr")
	private String isConfirmedByMgr;
	@Column(name="mgr_denial_reason")
	private String mgrDenialReason;
	
	@Column(name="reason_for_extension")
	private String reasonForExtension;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;
	
	@Column(name="fk_emp_id")
	private Integer fkEmpId;
	
	@Column(name="fk_mgr_id")
	private Integer fkMgrId;
	
	@Column(name="fk_conf_status_id")
	private Integer fkConfStatusId;
	
	@Column(name="conf_extended_by_hr_id")
	private Integer confExtendedByHrId;
	
	@Column(name="confirmed_by_mgr_id")
	private Integer confirmedByMgrId;
	
	@Column(name="confirmed_by_hr_id")
	private Integer confirmedByHrId;
	
	@Column(name="delegated_by_hr_id")
	private Integer delegatedByHrId;
	
	@Column(name="delegated_to_mgr_id")
	private Integer delegatedToMgrId;
	
	@Column(name="pre_confirmation_level")
	private Short preConfirmationLevel;
	
	@Column(name="updated_by")
	private Integer updatedBy;
	
	@Column(name="updated_count")
	private Integer updatedCount;
	
	@Column(name="post_confirmation_level")
	private Short postConfirmationLevel;
	
	@Column(name="pre_conf_designation")
	private Short preDesignationLevel;
	
	@Column(name="post_conf_designation")
	private Short postDesignationLevel;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="tentative_confirmation_date")
	private Date tentativeConfirmationDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="delegated_date")
	private Date delegatedDate;
		
	@OneToOne
	@JoinColumn(name="confirmed_by_hr_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOConfirmedByHrId;
	@OneToOne
	@JoinColumn(name="confirmed_by_mgr_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOConfirmedByMgrId;
	@OneToOne
	@JoinColumn(name="conf_extended_by_hr_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOExtendedByHrId;
	
		
	@OneToOne
	@JoinColumn(name="delegated_by_hr_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBODelegatedByHrId;
	
	@OneToOne
	@JoinColumn(name="delegated_to_mgr_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBODelegatedToMgrId;
	
	@OneToOne
	@JoinColumn(name="fk_conf_status_id",unique = true, nullable = true, insertable = false, updatable = false)
	private ConfMasStatusBO confMasStatusBO;
	@OneToOne
	@JoinColumn(name="fk_emp_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOFkEmpId;
	@OneToOne
	@JoinColumn(name="fk_mgr_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOFkMgrId;
	@OneToOne
	@JoinColumn(name="pre_confirmation_level",unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpLevelBO masEmpLevelBO;
	
	@OneToOne
	@JoinColumn(name="post_confirmation_level",unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpLevelBO masEmpLevelBOPostConfirmationDate;
	
	@OneToOne
	@JoinColumn(name="updated_by",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOUpdatedBy;
	public Integer getPkConfId() {
		return pkConfId;
	}
	public void setPkConfId(Integer pkConfId) {
		this.pkConfId = pkConfId;
	}
	public Date getActualDateOfConfirmation() {
		return actualDateOfConfirmation;
	}
	public void setActualDateOfConfirmation(Date actualDateOfConfirmation) {
		this.actualDateOfConfirmation = actualDateOfConfirmation;
	}
	public Integer getConfExtendedInMonths() {
		return confExtendedInMonths;
	}
	public void setConfExtendedInMonths(Integer confExtendedInMonths) {
		this.confExtendedInMonths = confExtendedInMonths;
	}
	public String getConfirmationLetterSent() {
		return confirmationLetterSent;
	}
	public void setConfirmationLetterSent(String confirmationLetterSent) {
		this.confirmationLetterSent = confirmationLetterSent;
	}
	public String getDelegatedComments() {
		return delegatedComments;
	}
	public void setDelegatedComments(String delegatedComments) {
		this.delegatedComments = delegatedComments;
	}
	public String getExtensionLetterSent() {
		return extensionLetterSent;
	}
	public void setExtensionLetterSent(String extensionLetterSent) {
		this.extensionLetterSent = extensionLetterSent;
	}
	public String getIsConfirmedByHr() {
		return isConfirmedByHr;
	}
	public void setIsConfirmedByHr(String isConfirmedByHr) {
		this.isConfirmedByHr = isConfirmedByHr;
	}
	public Short getPkEmpLevelId() {
		return pkEmpLevelId;
	}
	public void setPkEmpLevelId(Short pkEmpLevelId) {
		this.pkEmpLevelId = pkEmpLevelId;
	}
	public String getIsConfirmedByMgr() {
		return isConfirmedByMgr;
	}
	public void setIsConfirmedByMgr(String isConfirmedByMgr) {
		this.isConfirmedByMgr = isConfirmedByMgr;
	}
	public String getMgrDenialReason() {
		return mgrDenialReason;
	}
	public void setMgrDenialReason(String mgrDenialReason) {
		this.mgrDenialReason = mgrDenialReason;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Integer getFkEmpId() {
		return fkEmpId;
	}
	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}
	public Integer getFkMgrId() {
		return fkMgrId;
	}
	public void setFkMgrId(Integer fkMgrId) {
		this.fkMgrId = fkMgrId;
	}
	public Integer getFkConfStatusId() {
		return fkConfStatusId;
	}
	public void setFkConfStatusId(Integer fkConfStatusId) {
		this.fkConfStatusId = fkConfStatusId;
	}
	public Integer getConfExtendedByHrId() {
		return confExtendedByHrId;
	}
	public void setConfExtendedByHrId(Integer confExtendedByHrId) {
		this.confExtendedByHrId = confExtendedByHrId;
	}
	public Integer getConfirmedByMgrId() {
		return confirmedByMgrId;
	}
	public void setConfirmedByMgrId(Integer confirmedByMgrId) {
		this.confirmedByMgrId = confirmedByMgrId;
	}
	public Integer getConfirmedByHrId() {
		return confirmedByHrId;
	}
	public void setConfirmedByHrId(Integer confirmedByHrId) {
		this.confirmedByHrId = confirmedByHrId;
	}
	public Integer getDelegatedByHrId() {
		return delegatedByHrId;
	}
	public void setDelegatedByHrId(Integer delegatedByHrId) {
		this.delegatedByHrId = delegatedByHrId;
	}
	public Integer getDelegatedToMgrId() {
		return delegatedToMgrId;
	}
	public void setDelegatedToMgrId(Integer delegatedToMgrId) {
		this.delegatedToMgrId = delegatedToMgrId;
	}
	public Short getPreConfirmationLevel() {
		return preConfirmationLevel;
	}
	public void setPreConfirmationLevel(Short preConfirmationLevel) {
		this.preConfirmationLevel = preConfirmationLevel;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public DatEmpDetailBO getDatEmpDetailBOConfirmedByHrId() {
		return datEmpDetailBOConfirmedByHrId;
	}
	public void setDatEmpDetailBOConfirmedByHrId(
			DatEmpDetailBO datEmpDetailBOConfirmedByHrId) {
		this.datEmpDetailBOConfirmedByHrId = datEmpDetailBOConfirmedByHrId;
	}
	public DatEmpDetailBO getDatEmpDetailBOConfirmedByMgrId() {
		return datEmpDetailBOConfirmedByMgrId;
	}
	public void setDatEmpDetailBOConfirmedByMgrId(
			DatEmpDetailBO datEmpDetailBOConfirmedByMgrId) {
		this.datEmpDetailBOConfirmedByMgrId = datEmpDetailBOConfirmedByMgrId;
	}
	public DatEmpDetailBO getDatEmpDetailBOExtendedByHrId() {
		return datEmpDetailBOExtendedByHrId;
	}
	public void setDatEmpDetailBOExtendedByHrId(
			DatEmpDetailBO datEmpDetailBOExtendedByHrId) {
		this.datEmpDetailBOExtendedByHrId = datEmpDetailBOExtendedByHrId;
	}
	public DatEmpDetailBO getDatEmpDetailBODelegatedByHrId() {
		return datEmpDetailBODelegatedByHrId;
	}
	public void setDatEmpDetailBODelegatedByHrId(
			DatEmpDetailBO datEmpDetailBODelegatedByHrId) {
		this.datEmpDetailBODelegatedByHrId = datEmpDetailBODelegatedByHrId;
	}
	public DatEmpDetailBO getDatEmpDetailBODelegatedToMgrId() {
		return datEmpDetailBODelegatedToMgrId;
	}
	public void setDatEmpDetailBODelegatedToMgrId(
			DatEmpDetailBO datEmpDetailBODelegatedToMgrId) {
		this.datEmpDetailBODelegatedToMgrId = datEmpDetailBODelegatedToMgrId;
	}
	public ConfMasStatusBO getConfMasStatusBO() {
		return confMasStatusBO;
	}
	public void setConfMasStatusBO(ConfMasStatusBO confMasStatusBO) {
		this.confMasStatusBO = confMasStatusBO;
	}
	public DatEmpDetailBO getDatEmpDetailBOFkEmpId() {
		return datEmpDetailBOFkEmpId;
	}
	public void setDatEmpDetailBOFkEmpId(DatEmpDetailBO datEmpDetailBOFkEmpId) {
		this.datEmpDetailBOFkEmpId = datEmpDetailBOFkEmpId;
	}
	public DatEmpDetailBO getDatEmpDetailBOFkMgrId() {
		return datEmpDetailBOFkMgrId;
	}
	public void setDatEmpDetailBOFkMgrId(DatEmpDetailBO datEmpDetailBOFkMgrId) {
		this.datEmpDetailBOFkMgrId = datEmpDetailBOFkMgrId;
	}
	public MasEmpLevelBO getMasEmpLevelBO() {
		return masEmpLevelBO;
	}
	public void setMasEmpLevelBO(MasEmpLevelBO masEmpLevelBO) {
		this.masEmpLevelBO = masEmpLevelBO;
	}
	public DatEmpDetailBO getDatEmpDetailBOUpdatedBy() {
		return datEmpDetailBOUpdatedBy;
	}
	public void setDatEmpDetailBOUpdatedBy(DatEmpDetailBO datEmpDetailBOUpdatedBy) {
		this.datEmpDetailBOUpdatedBy = datEmpDetailBOUpdatedBy;
	}
	public String getReasonForExtension() {
		return reasonForExtension;
	}
	public void setReasonForExtension(String reasonForExtension) {
		this.reasonForExtension = reasonForExtension;
	}
	public Short getPostConfirmationLevel() {
		return postConfirmationLevel;
	}
	public void setPostConfirmationLevel(Short postConfirmationLevel) {
		this.postConfirmationLevel = postConfirmationLevel;
	}
	public Date getTentativeConfirmationDate() {
		return tentativeConfirmationDate;
	}
	public void setTentativeConfirmationDate(Date tentativeConfirmationDate) {
		this.tentativeConfirmationDate = tentativeConfirmationDate;
	}
	public Date getDelegatedDate() {
		return delegatedDate;
	}
	public void setDelegatedDate(Date delegatedDate) {
		this.delegatedDate = delegatedDate;
	}
	public MasEmpLevelBO getMasEmpLevelBOPostConfirmationDate() {
		return masEmpLevelBOPostConfirmationDate;
	}
	public void setMasEmpLevelBOPostConfirmationDate(
			MasEmpLevelBO masEmpLevelBOPostConfirmationDate) {
		this.masEmpLevelBOPostConfirmationDate = masEmpLevelBOPostConfirmationDate;
	}
	
	
	//Added by Smrithi
	
	@Transient 
	private Date empDateOfJoining;
	
	@Transient 
	private String empDesignationName;
	
	@Transient 
	private String empLevelName;
	
	@Transient 
	private String empFirstName;
	
	@Transient 
	private String empLastName;
	
	@Transient 
	private Short fkEmpDesignation;
	
	@Transient
	private Short buUnitId;
	
	@Transient
	private Integer fkEmpReportingMgrId;
	
	@Transient
	private Integer pkFeedbackId;
	
	@Transient
	private Short pkEmpLevelId;
	
	@Transient
	private Byte empType;
	
	
	public ConfDatProbationConfirmationBO(Integer fkEmpId, Date empDateOfJoining,
			Date tentativeConfirmationDate,String empDesignationName ,String empLevelName,
			Integer fkConfStatusId,Short pkEmpLevelId, Short fkEmpDesignation,
			Integer pkConfId, Short buUnitId,
			Integer delegatedToMgrId , Integer fkEmpReportingMgrId , Integer pkFeedbackId ,Byte empType) {
		this.fkEmpId = fkEmpId;
		this.empDateOfJoining = empDateOfJoining;
		this.tentativeConfirmationDate = tentativeConfirmationDate;
		this.empDesignationName = empDesignationName;
		this.empLevelName = empLevelName;
		this.fkConfStatusId = fkConfStatusId;
		this.pkEmpLevelId = pkEmpLevelId;
		this.fkEmpDesignation = fkEmpDesignation;
		this.pkConfId = pkConfId;
		this.buUnitId = buUnitId;
		this.delegatedToMgrId = delegatedToMgrId;
		this.fkEmpReportingMgrId = fkEmpReportingMgrId;
		this.pkFeedbackId = pkFeedbackId;
		this.empType = empType;
	}
	
	public Byte getEmpType() {
		return empType;
	}
	public void setEmpType(Byte empType) {
		this.empType = empType;
	}
	//EOA by Smrithi
	public ConfDatProbationConfirmationBO() {
	}
	public Date getEmpDateOfJoining() {
		return empDateOfJoining;
	}
	public void setEmpDateOfJoining(Date empDateOfJoining) {
		this.empDateOfJoining = empDateOfJoining;
	}
	public String getEmpDesignationName() {
		return empDesignationName;
	}
	public void setEmpDesignationName(String empDesignationName) {
		this.empDesignationName = empDesignationName;
	}
	public String getEmpLevelName() {
		return empLevelName;
	}
	public void setEmpLevelName(String empLevelName) {
		this.empLevelName = empLevelName;
	}
	public String getEmpFirstName() {
		return empFirstName;
	}
	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}
	public String getEmpLastName() {
		return empLastName;
	}
	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}
	/*public Integer getFkEmpLevel() {
		return level;
	}
	public void setFkEmpLevel(Integer level) {
		this.level = level;
	}*/
	public Short getFkEmpDesignation() {
		return fkEmpDesignation;
	}
	public void setFkEmpDesignation(Short fkEmpDesignation) {
		this.fkEmpDesignation = fkEmpDesignation;
	}
	
	public Short getBuUnitId() {
		return buUnitId;
	}
	public void setBuUnitId(Short buUnitId) {
		this.buUnitId = buUnitId;
	}
	public Integer getUpdatedCount() {
		return updatedCount;
	}
	public void setUpdatedCount(Integer updatedCount) {
		this.updatedCount = updatedCount;
	}
	
	public Integer getFkEmpReportingMgrId() {
		return fkEmpReportingMgrId;
	}
	public void setFkEmpReportingMgrId(Integer fkEmpReportingMgrId) {
		this.fkEmpReportingMgrId = fkEmpReportingMgrId;
	}
	
	public Short getPreDesignationLevel() {
		return preDesignationLevel;
	}
	public void setPreDesignationLevel(Short preDesignationLevel) {
		this.preDesignationLevel = preDesignationLevel;
	}
	public Short getPostDesignationLevel() {
		return postDesignationLevel;
	}
	public void setPostDesignationLevel(Short postDesignationLevel) {
		this.postDesignationLevel = postDesignationLevel;
	}
	
	public Integer getPkFeedbackId() {
		return pkFeedbackId;
	}
	public void setPkFeedbackId(Integer pkFeedbackId) {
		this.pkFeedbackId = pkFeedbackId;
	}
	@Override
	public String toString() {
		return "ConfDatProbationConfirmationBO [pkConfId=" + pkConfId
				+ ", actualDateOfConfirmation=" + actualDateOfConfirmation
				+ ", confExtendedInMonths=" + confExtendedInMonths
				+ ", confirmationLetterSent=" + confirmationLetterSent
				+ ", delegatedComments=" + delegatedComments
				+ ", extensionLetterSent=" + extensionLetterSent
				+ ", isConfirmedByHr=" + isConfirmedByHr
				+ ", isConfirmedByMgr=" + isConfirmedByMgr
				+ ", mgrDenialReason=" + mgrDenialReason
				+ ", reasonForExtension=" + reasonForExtension + ", updatedOn="
				+ updatedOn + ", fkEmpId=" + fkEmpId + ", fkMgrId=" + fkMgrId
				+ ", fkConfStatusId=" + fkConfStatusId
				+ ", confExtendedByHrId=" + confExtendedByHrId
				+ ", confirmedByMgrId=" + confirmedByMgrId
				+ ", confirmedByHrId=" + confirmedByHrId + ", delegatedByHrId="
				+ delegatedByHrId + ", delegatedToMgrId=" + delegatedToMgrId
				+ ", preConfirmationLevel=" + preConfirmationLevel
				+ ", updatedBy=" + updatedBy + ", updatedCount=" + updatedCount
				+ ", postConfirmationLevel=" + postConfirmationLevel
				+ ", preDesignationLevel=" + preDesignationLevel
				+ ", postDesignationLevel=" + postDesignationLevel
				+ ", tentativeConfirmationDate=" + tentativeConfirmationDate
				+ ", delegatedDate=" + delegatedDate
				+ ", datEmpDetailBOConfirmedByHrId="
				+ datEmpDetailBOConfirmedByHrId
				+ ", datEmpDetailBOConfirmedByMgrId="
				+ datEmpDetailBOConfirmedByMgrId
				+ ", datEmpDetailBOExtendedByHrId="
				+ datEmpDetailBOExtendedByHrId
				+ ", datEmpDetailBODelegatedByHrId="
				+ datEmpDetailBODelegatedByHrId
				+ ", datEmpDetailBODelegatedToMgrId="
				+ datEmpDetailBODelegatedToMgrId + ", confMasStatusBO="
				+ confMasStatusBO + ", datEmpDetailBOFkEmpId="
				+ datEmpDetailBOFkEmpId + ", datEmpDetailBOFkMgrId="
				+ datEmpDetailBOFkMgrId + ", masEmpLevelBO=" + masEmpLevelBO
				+ ", masEmpLevelBOPostConfirmationDate="
				+ masEmpLevelBOPostConfirmationDate
				+ ", datEmpDetailBOUpdatedBy=" + datEmpDetailBOUpdatedBy
				+ ", empDateOfJoining=" + empDateOfJoining
				+ ", empDesignationName=" + empDesignationName
				+ ", empLevelName=" + empLevelName + ", empFirstName="
				+ empFirstName + ", empLastName=" + empLastName
				+ ", fkEmpDesignation=" + fkEmpDesignation + ", buUnitId="
				+ buUnitId + ", fkEmpReportingMgrId=" + fkEmpReportingMgrId
				+ ", pkFeedbackId=" + pkFeedbackId + ", pkEmpLevelId="
				+ pkEmpLevelId + ", empType=" + empType + "]";
	}
	
}