package com.thbs.mis.confirmation.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TemporalType;
import javax.persistence.Temporal;



import com.thbs.mis.common.bo.DatEmpDetailBO;


/**
 * The persistent class for the conf_dat_feedback database table.
 * 
 */
@Entity
@Table(name="conf_dat_feedback")
@NamedQuery(name="ConfDatFeedbackBO.findAll", query="SELECT c FROM ConfDatFeedbackBO c")
public class ConfDatFeedbackBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="pk_feedback_id")
	private Integer pkFeedbackId;

	@Column(name="areas_of_improvements")
	private String areasOfImprovements;

	private Integer attitude;

	private Integer communication;

	private Integer dependability;

	private String feedback;

	private Integer flexibility;

	private Integer proactiveness;

	@Column(name="self_confidence")
	private Integer selfConfidence;

	@Column(name="technical_expertise")
	private Integer technicalExpertise;

	@Column(name="willingness_to_learn")
	private Integer willingnessToLearn;

	@Column(name="work_objectives")
	private String workObjectives;

	@Column(name="fk_conf_id")
	private Integer fkConfId;
	
	@Column(name="fk_emp_id")
	private Integer fkEmpId;
	
	@Column(name="updated_by")
	private int updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;
	
	@OneToOne
	@JoinColumn(name="fk_conf_id",unique = true, nullable = true, insertable = false, updatable = false)
	private ConfDatProbationConfirmationBO confDatProbationConfirmationBO;

	@OneToOne
	@JoinColumn(name="updated_by",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOUpdatedBy;
	
	@OneToOne
	@JoinColumn(name="fk_emp_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO;

	public ConfDatProbationConfirmationBO getConfDatProbationConfirmationBO() {
		return confDatProbationConfirmationBO;
	}

	public void setConfDatProbationConfirmationBO(
			ConfDatProbationConfirmationBO confDatProbationConfirmationBO) {
		this.confDatProbationConfirmationBO = confDatProbationConfirmationBO;
	}

	public DatEmpDetailBO getDatEmpDetailBOUpdatedBy() {
		return datEmpDetailBOUpdatedBy;
	}

	public void setDatEmpDetailBOUpdatedBy(DatEmpDetailBO datEmpDetailBOUpdatedBy) {
		this.datEmpDetailBOUpdatedBy = datEmpDetailBOUpdatedBy;
	}

	public DatEmpDetailBO getDatEmpDetailBO() {
		return datEmpDetailBO;
	}

	public void setDatEmpDetailBO(DatEmpDetailBO datEmpDetailBO) {
		this.datEmpDetailBO = datEmpDetailBO;
	}

	public Integer getPkFeedbackId() {
		return pkFeedbackId;
	}

	public void setPkFeedbackId(Integer pkFeedbackId) {
		this.pkFeedbackId = pkFeedbackId;
	}

	public String getAreasOfImprovements() {
		return areasOfImprovements;
	}

	public void setAreasOfImprovements(String areasOfImprovements) {
		this.areasOfImprovements = areasOfImprovements;
	}

	public Integer getAttitude() {
		return attitude;
	}

	public void setAttitude(Integer attitude) {
		this.attitude = attitude;
	}

	public Integer getCommunication() {
		return communication;
	}

	public void setCommunication(Integer communication) {
		this.communication = communication;
	}

	public Integer getDependability() {
		return dependability;
	}

	public void setDependability(Integer dependability) {
		this.dependability = dependability;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public Integer getFlexibility() {
		return flexibility;
	}

	public void setFlexibility(Integer flexibility) {
		this.flexibility = flexibility;
	}

	public Integer getProactiveness() {
		return proactiveness;
	}

	public void setProactiveness(Integer proactiveness) {
		this.proactiveness = proactiveness;
	}

	public Integer getSelfConfidence() {
		return selfConfidence;
	}

	public void setSelfConfidence(Integer selfConfidence) {
		this.selfConfidence = selfConfidence;
	}

	public Integer getTechnicalExpertise() {
		return technicalExpertise;
	}

	public void setTechnicalExpertise(Integer technicalExpertise) {
		this.technicalExpertise = technicalExpertise;
	}

	public Integer getWillingnessToLearn() {
		return willingnessToLearn;
	}

	public void setWillingnessToLearn(Integer willingnessToLearn) {
		this.willingnessToLearn = willingnessToLearn;
	}

	public String getWorkObjectives() {
		return workObjectives;
	}

	public void setWorkObjectives(String workObjectives) {
		this.workObjectives = workObjectives;
	}

	public Integer getFkConfId() {
		return fkConfId;
	}

	public void setFkConfId(Integer fkConfId) {
		this.fkConfId = fkConfId;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public int getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Override
	public String toString() {
		return "ConfDatFeedbackBO [pkFeedbackId=" + pkFeedbackId
				+ ", areasOfImprovements=" + areasOfImprovements
				+ ", attitude=" + attitude + ", communication=" + communication
				+ ", dependability=" + dependability + ", feedback=" + feedback
				+ ", flexibility=" + flexibility + ", proactiveness="
				+ proactiveness + ", selfConfidence=" + selfConfidence
				+ ", technicalExpertise=" + technicalExpertise
				+ ", willingnessToLearn=" + willingnessToLearn
				+ ", workObjectives=" + workObjectives + ", fkConfId="
				+ fkConfId + ", fkEmpId=" + fkEmpId + ", updatedBy="
				+ updatedBy + ", updatedOn=" + updatedOn
				+ ", confDatProbationConfirmationBO="
				+ confDatProbationConfirmationBO + ", datEmpDetailBOUpdatedBy="
				+ datEmpDetailBOUpdatedBy + ", datEmpDetailBO="
				+ datEmpDetailBO + "]";
	}


}