package com.thbs.mis.confirmation.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;

	/**
	 * The persistent class for the conf_dat_feedback_history database table.
	 * 
	 */
	@Entity
	@Table(name="conf_dat_feedback_history")
	@NamedQuery(name="ConfDatFeedbackHistoryBO.findAll", query="SELECT c FROM ConfDatFeedbackHistoryBO c")
	public class ConfDatFeedbackHistoryBO implements Serializable {
		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name="pk_history_id")
		private int pkHistoryId;
		
		@Column(name="fk_conf_id")
		private int fkConfId;

		@Column(name="fk_emp_id")
		private int fkEmpId;
		
		@Column(name="fk_feedback_id")
		private int fkFeedbackId;
		
		@Column(name="fk_mgr_id")
		private Integer fkMgrId;
		
		@Column(name="fk_conf_status_id")
		private Integer fkConfStatusId;

		@Column(name="areas_of_improvements")
		private String areasOfImprovements;
		
		private Integer attitude;

		private Integer communication;

		private Integer dependability;

		private String feedback;

		private Integer flexibility;

		private Integer proactiveness;

		@Column(name="self_confidence")
		private Integer selfConfidence;

		@Column(name="technical_expertise")
		private Integer technicalExpertise;

		@Column(name="updated_by")
		private int updatedBy;

		@Temporal(TemporalType.TIMESTAMP)
		@Column(name="updated_on")
		private Date updatedOn;

		@Column(name="willingness_to_learn")
		private Integer willingnessToLearn;

		@Column(name="work_objectives")
		private String workObjectives;
				
		private String status;
					
		@Temporal(TemporalType.TIMESTAMP)
		@Column(name="actual_date_of_confirmation")
		private Date actualDateOfConfirmation;
		
		@Temporal(TemporalType.TIMESTAMP)
		@Column(name="tentative_confirmation_date")
		private Date tentativeConfirmationDate;
		
		@Column(name="delegated_by_hr_id")
		private Integer delegatedByHrId;
		
		@Column(name="delegated_to_mgr_id")
		private Integer delegatedToMgrId;
		
		@Temporal(TemporalType.TIMESTAMP)
		@Column(name="delegated_date")
		private Date delegatedDate;
										
		@Column(name="delegated_comments")
		private String delegatedComments;
		
		@Column(name="conf_extended_in_months")
		private Integer confExtendedInMonths;
		
		@Column(name="conf_extended_by_hr_id")
		private Integer confExtendedByHrId;
	
		@Column(name="is_confirmed_by_hr")
		private String isConfirmedByHr;
		
		@Column(name="is_confirmed_by_mgr")
		private String isConfirmedByMgr;
		
		@Column(name="confirmed_by_mgr_id")
		private Integer confirmedByMgrId;
		
		@Column(name="confirmed_by_hr_id")
		private Integer confirmedByHrId;
		
		@Column(name="mgr_denial_reason")
		private String mgrDenialReason;
		
		@Column(name="reason_for_extension")
		private String reasonForExtension;
									
		private Short level;
				
		private Short designation;
		
		
		public int getPkHistoryId() {
			return pkHistoryId;
		}


		public void setPkHistoryId(int pkHistoryId) {
			this.pkHistoryId = pkHistoryId;
		}


		public int getFkConfId() {
			return fkConfId;
		}


		public void setFkConfId(int fkConfId) {
			this.fkConfId = fkConfId;
		}


		public int getFkEmpId() {
			return fkEmpId;
		}


		public void setFkEmpId(int fkEmpId) {
			this.fkEmpId = fkEmpId;
		}


		public int getFkFeedbackId() {
			return fkFeedbackId;
		}


		public void setFkFeedbackId(int fkFeedbackId) {
			this.fkFeedbackId = fkFeedbackId;
		}


		public Integer getFkMgrId() {
			return fkMgrId;
		}


		public void setFkMgrId(Integer fkMgrId) {
			this.fkMgrId = fkMgrId;
		}


		public Integer getFkConfStatusId() {
			return fkConfStatusId;
		}


		public void setFkConfStatusId(Integer fkConfStatusId) {
			this.fkConfStatusId = fkConfStatusId;
		}


		public String getAreasOfImprovements() {
			return areasOfImprovements;
		}


		public void setAreasOfImprovements(String areasOfImprovements) {
			this.areasOfImprovements = areasOfImprovements;
		}


		public Integer getAttitude() {
			return attitude;
		}


		public void setAttitude(Integer attitude) {
			this.attitude = attitude;
		}


		public Integer getCommunication() {
			return communication;
		}


		public void setCommunication(Integer communication) {
			this.communication = communication;
		}


		public Integer getDependability() {
			return dependability;
		}


		public void setDependability(Integer dependability) {
			this.dependability = dependability;
		}


		public String getFeedback() {
			return feedback;
		}


		public void setFeedback(String feedback) {
			this.feedback = feedback;
		}


		public Integer getFlexibility() {
			return flexibility;
		}


		public void setFlexibility(Integer flexibility) {
			this.flexibility = flexibility;
		}


		public Integer getProactiveness() {
			return proactiveness;
		}


		public void setProactiveness(Integer proactiveness) {
			this.proactiveness = proactiveness;
		}


		public Integer getSelfConfidence() {
			return selfConfidence;
		}


		public void setSelfConfidence(Integer selfConfidence) {
			this.selfConfidence = selfConfidence;
		}


		public Integer getTechnicalExpertise() {
			return technicalExpertise;
		}


		public void setTechnicalExpertise(Integer technicalExpertise) {
			this.technicalExpertise = technicalExpertise;
		}


		public int getUpdatedBy() {
			return updatedBy;
		}


		public void setUpdatedBy(int updatedBy) {
			this.updatedBy = updatedBy;
		}


		public Date getUpdatedOn() {
			return updatedOn;
		}


		public void setUpdatedOn(Date updatedOn) {
			this.updatedOn = updatedOn;
		}


		public Integer getWillingnessToLearn() {
			return willingnessToLearn;
		}


		public void setWillingnessToLearn(Integer willingnessToLearn) {
			this.willingnessToLearn = willingnessToLearn;
		}


		public String getWorkObjectives() {
			return workObjectives;
		}


		public void setWorkObjectives(String workObjectives) {
			this.workObjectives = workObjectives;
		}


		public String getStatus() {
			return status;
		}


		public void setStatus(String status) {
			this.status = status;
		}


		public Date getActualDateOfConfirmation() {
			return actualDateOfConfirmation;
		}


		public void setActualDateOfConfirmation(Date actualDateOfConfirmation) {
			this.actualDateOfConfirmation = actualDateOfConfirmation;
		}


		public Date getTentativeConfirmationDate() {
			return tentativeConfirmationDate;
		}


		public void setTentativeConfirmationDate(Date tentativeConfirmationDate) {
			this.tentativeConfirmationDate = tentativeConfirmationDate;
		}


		public Integer getDelegatedByHrId() {
			return delegatedByHrId;
		}


		public void setDelegatedByHrId(Integer delegatedByHrId) {
			this.delegatedByHrId = delegatedByHrId;
		}


		public Integer getDelegatedToMgrId() {
			return delegatedToMgrId;
		}


		public void setDelegatedToMgrId(Integer delegatedToMgrId) {
			this.delegatedToMgrId = delegatedToMgrId;
		}


		public Date getDelegatedDate() {
			return delegatedDate;
		}


		public void setDelegatedDate(Date delegatedDate) {
			this.delegatedDate = delegatedDate;
		}


		public String getDelegatedComments() {
			return delegatedComments;
		}


		public void setDelegatedComments(String delegatedComments) {
			this.delegatedComments = delegatedComments;
		}


		public Integer getConfExtendedInMonths() {
			return confExtendedInMonths;
		}


		public void setConfExtendedInMonths(Integer confExtendedInMonths) {
			this.confExtendedInMonths = confExtendedInMonths;
		}


		public Integer getConfExtendedByHrId() {
			return confExtendedByHrId;
		}


		public void setConfExtendedByHrId(Integer confExtendedByHrId) {
			this.confExtendedByHrId = confExtendedByHrId;
		}


		public String getIsConfirmedByHr() {
			return isConfirmedByHr;
		}


		public void setIsConfirmedByHr(String isConfirmedByHr) {
			this.isConfirmedByHr = isConfirmedByHr;
		}


		public String getIsConfirmedByMgr() {
			return isConfirmedByMgr;
		}


		public void setIsConfirmedByMgr(String isConfirmedByMgr) {
			this.isConfirmedByMgr = isConfirmedByMgr;
		}


		public Integer getConfirmedByMgrId() {
			return confirmedByMgrId;
		}


		public void setConfirmedByMgrId(Integer confirmedByMgrId) {
			this.confirmedByMgrId = confirmedByMgrId;
		}


		public Integer getConfirmedByHrId() {
			return confirmedByHrId;
		}


		public void setConfirmedByHrId(Integer confirmedByHrId) {
			this.confirmedByHrId = confirmedByHrId;
		}


		public String getMgrDenialReason() {
			return mgrDenialReason;
		}


		public void setMgrDenialReason(String mgrDenialReason) {
			this.mgrDenialReason = mgrDenialReason;
		}


		public String getReasonForExtension() {
			return reasonForExtension;
		}


		public void setReasonForExtension(String reasonForExtension) {
			this.reasonForExtension = reasonForExtension;
		}


		public Short getLevel() {
			return level;
		}


		public void setLevel(Short level) {
			this.level = level;
		}


		public Short getDesignation() {
			return designation;
		}


		public void setDesignation(Short designation) {
			this.designation = designation;
		}


		@Override
		public String toString() {
			return "ConfDatFeedbackHistoryBO [pkHistoryId=" + pkHistoryId
					+ ", fkConfId=" + fkConfId + ", fkEmpId=" + fkEmpId
					+ ", fkFeedbackId=" + fkFeedbackId + ", fkMgrId=" + fkMgrId
					+ ", fkConfStatusId=" + fkConfStatusId
					+ ", areasOfImprovements=" + areasOfImprovements
					+ ", attitude=" + attitude + ", communication="
					+ communication + ", dependability=" + dependability
					+ ", feedback=" + feedback + ", flexibility=" + flexibility
					+ ", proactiveness=" + proactiveness + ", selfConfidence="
					+ selfConfidence + ", technicalExpertise="
					+ technicalExpertise + ", updatedBy=" + updatedBy
					+ ", updatedOn=" + updatedOn + ", willingnessToLearn="
					+ willingnessToLearn + ", workObjectives=" + workObjectives
					+ ", status=" + status + ", actualDateOfConfirmation="
					+ actualDateOfConfirmation + ", tentativeConfirmationDate="
					+ tentativeConfirmationDate + ", delegatedByHrId="
					+ delegatedByHrId + ", delegatedToMgrId="
					+ delegatedToMgrId + ", delegatedDate=" + delegatedDate
					+ ", delegatedComments=" + delegatedComments
					+ ", confExtendedInMonths=" + confExtendedInMonths
					+ ", confExtendedByHrId=" + confExtendedByHrId
					+ ", isConfirmedByHr=" + isConfirmedByHr
					+ ", isConfirmedByMgr=" + isConfirmedByMgr
					+ ", confirmedByMgrId=" + confirmedByMgrId
					+ ", confirmedByHrId=" + confirmedByHrId
					+ ", mgrDenialReason=" + mgrDenialReason
					+ ", reasonForExtension=" + reasonForExtension + ", level="
					+ level + ", designation=" + designation + "]";
		}

				
		
		
		
}
