package com.thbs.mis.confirmation.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the conf_mas_status database table.
 * 
 */
@Entity
@Table(name="conf_mas_status")
@NamedQuery(name="ConfMasStatusBO.findAll", query="SELECT c FROM ConfMasStatusBO c")
public class ConfMasStatusBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_status_id")
	private int pkStatusId;

	@Column(name="status_desc")
	private String statusDesc;

	
	public ConfMasStatusBO() {
	}

	public int getPkStatusId() {
		return this.pkStatusId;
	}

	public void setPkStatusId(int pkStatusId) {
		this.pkStatusId = pkStatusId;
	}

	public String getStatusDesc() {
		return this.statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	@Override
	public String toString() {
		return "ConfMasStatusBO [pkStatusId=" + pkStatusId + ", statusDesc=" + statusDesc + "]";
	}

		

}