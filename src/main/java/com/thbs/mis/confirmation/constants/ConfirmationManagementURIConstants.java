package com.thbs.mis.confirmation.constants;

public class ConfirmationManagementURIConstants {

	//Added by Smrithi
	public static final String GET_CONFIRMATION_STATUS = "/conf/status/details/{empId}";
	public static final String EXTEND_PROBATION_BY_HR = "/conf/extend/probation/hr";
	public static final String CONFIRM_STATUS_BY_HR = "/conf/confirm/status/hr";
	public static final String SEARCH_DETAILS = "/conf/employee/search";
	public static final String DELEGATE_MANAGER_BY_HR = "/conf/delegate/manager";
	public static final String GET_MANAGER_NAMES = "/conf/rm/names/{empId}";
	public static final String SEND_MAIL_BY_HR = "/conf/send/mail/hr/{empId}";
	public static final String GET_DIRECT_AND_INDIRECT_REPORTEES = "/conf/direct/indirect/reportees/{rmId}";
	//EOA by Smrithi

	//Added by Harsh J
	public static final String SUBMIT_FEEDBACK_BY_MANAGER = "/conf/mgr/feedback/submit";
	public static final String UPDATE_FEEDBACK_BY_MANAGER = "/conf/mgr/feedback/update";
	public static final String SUBMIT_FEEDBACK_BY_HR = "/conf/hr/feedback/submit";
	public static final String UPDATE_FEEDBACK_BY_HR = "/conf/hr/feedback/update";
	public static final String GET_CONFIRMATION_DETAIL = "/conf/confirmation/detail/{empId}";
	public static final String GET_FEEDBACK_HISTORY = "/conf/feedback/history/{historyId}";
	public static final String GET_FEEDBACK_DATES = "/conf/feedback/dates/{empId}";
	public static final String GENERATE_CONF_EXT_LETTER = "/conf/generate/letter/{empId}";
	
	//EOA by Harsh
	
	//Added by Kamal Anand
	public static final String NOT_CONFIRM_STATUS_BY_HR = "/conf/notconfirm/status/hr";
	//EOA by Kamal Anand


}
