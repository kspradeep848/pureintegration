/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRCreateGoalRepository.java                      */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the Repository methods    */
/*                 used for the GSRGoalService                       */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.gsr.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrDatEmpGoalBO;

/**
 * <Description TGSRCreateGoalRepository:> This class contains all the DAO
 * related methods used for the GSR...........GoalRepository repository for Data
 * Base operation.
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
@Repository
public interface GSRCreateGoalRepository extends
		GenericRepository<GsrDatEmpGoalBO, Long> {
	public List<GsrDatEmpGoalBO> findAll();

	public GsrDatEmpGoalBO findByPkGsrDatEmpGoalId(int PkGsrDatEmpGoalId);

	// Added by Mani for GSR module service committed on 30-11-2016

	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpIdAndFkGsrSlabId(
			int fkMainEmpDetailId, short pkGsrSlabId);

	// End of addition by Mani for GSR module service committed on 30-11-2016

	// Added by Balaji 15-12-16

	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpIdAndGsrGoalStartDateGreaterThanEqualAndGsrGoalEndDateLessThanEqual(
			Integer fkGsrDatEmpId, Date gsrGoalStartDate, Date gsrGoalEndDate);
	
	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpId(Integer fkGsrDatEmpId);
	
	public List<GsrDatEmpGoalBO> findByFkGsrDatMgrId(Integer fkGsrDatEmpId);
	
	public List<GsrDatEmpGoalBO> findByFkGsrDatMgrIdAndFkGsrSlabId(Integer fkGsrDatEmpId, short pkGsrSlabId);

	// End of Addition by Balaji

	@Query("SELECT new com.thbs.mis.gsr.bo.GsrDatEmpGoalBO "
			+ "( slab.gsrSlabName ," + " gsrGoal.fkGsrDatEmpId, "
			+ "gsrGoal.fkGsrDatMgrId, " + " gsrGoal.fkGsrGoalStatus, "
			+ "gsrGoal.fkGsrSlabId, " + " gsrGoal.gsrDatEmpGoalName, "
			+ "SUM(gsrGoal.gsrGoalWeightage) ," 
			+ "gsrGoal.fkEmpGoalDomainMgrId,"
			+ "gsrGoal.fkEmpGoalBuId"+ " ) "
			+ "from GsrDatEmpGoalBO  gsrGoal " + " JOIN GsrSlabBO slab "
			+ " ON slab.pkGsrSlabId=gsrGoal.fkGsrSlabId"
			+ " where gsrGoal.fkGsrDatEmpId = ? AND "
//			+ " gsrGoal.fkGsrDatMgrId = ? AND "
			+ " YEAR(gsrGoal.gsrGoalStartDate) =  ? "
			+ " group by  gsrGoal.fkGsrSlabId,gsrGoal.fkGsrDatMgrId ")
	public List<GsrDatEmpGoalBO> getCurrentYearGoalStatusByEmpId(int empId,
			int Year);
	
	
	@Query("SELECT new com.thbs.mis.gsr.bo.GsrDatEmpGoalBO "
			+ "( slab.gsrSlabName ," + " gsrGoal.fkGsrDatEmpId, "
			+ "gsrGoal.fkGsrDatMgrId, " + " gsrGoal.fkGsrGoalStatus, "
			+ "gsrGoal.fkGsrSlabId, " + " gsrGoal.gsrDatEmpGoalName, "
			+ "SUM(gsrGoal.gsrGoalWeightage) ," 
			+ "gsrGoal.fkEmpGoalDomainMgrId,"
			+ "gsrGoal.fkEmpGoalBuId"+ " ) "
			+ "from GsrDatEmpGoalBO  gsrGoal " + " JOIN GsrSlabBO slab "
			+ " ON slab.pkGsrSlabId=gsrGoal.fkGsrSlabId"
			+ " where gsrGoal.fkEmpGoalBuId = ? AND "
			+ " YEAR(gsrGoal.gsrGoalStartDate) =  ? "
			+ " group by  gsrGoal.fkGsrSlabId,gsrGoal.fkGsrDatEmpId ")
	public List<GsrDatEmpGoalBO> getCurrentYearGoalStatusByBuId(short buId,
			int Year);
	
	@Query("SELECT new com.thbs.mis.gsr.bo.GsrDatEmpGoalBO "
			+ "( slab.gsrSlabName ," + " gsrGoal.fkGsrDatEmpId, "
			+ "gsrGoal.fkGsrDatMgrId, " + " gsrGoal.fkGsrGoalStatus, "
			+ "gsrGoal.fkGsrSlabId, " + " gsrGoal.gsrDatEmpGoalName, "
			+ "SUM(gsrGoal.gsrGoalWeightage) ," 
			+ "gsrGoal.fkEmpGoalDomainMgrId,"
			+ "gsrGoal.fkEmpGoalBuId"+ " ) "
			+ "from GsrDatEmpGoalBO  gsrGoal " + " JOIN GsrSlabBO slab "
			+ " ON slab.pkGsrSlabId=gsrGoal.fkGsrSlabId"
			+ " where gsrGoal.fkEmpGoalDomainMgrId = ? AND "
			+ " YEAR(gsrGoal.gsrGoalStartDate) =  ? "
			+ " group by gsrGoal.fkGsrSlabId,gsrGoal.fkGsrDatEmpId ")
	public List<GsrDatEmpGoalBO> getCurrentYearGoalStatusByDmId(int dmId,
			int Year);
	
	@Query("SELECT new com.thbs.mis.gsr.bo.GsrDatEmpGoalBO "
			+ "( slab.gsrSlabName ," + " gsrGoal.fkGsrDatEmpId, "
			+ "gsrGoal.fkGsrDatMgrId, " + " gsrGoal.fkGsrGoalStatus, "
			+ "gsrGoal.fkGsrSlabId, " + " gsrGoal.gsrDatEmpGoalName, "
			+ "SUM(gsrGoal.gsrGoalWeightage) ," 
			+ "gsrGoal.fkEmpGoalDomainMgrId,"
			+ "gsrGoal.fkEmpGoalBuId"+ " ) "
			+ "from GsrDatEmpGoalBO  gsrGoal " + " JOIN GsrSlabBO slab "
			+ " ON slab.pkGsrSlabId=gsrGoal.fkGsrSlabId"
			+ " where gsrGoal.fkGsrDatMgrId = ? AND "
			+ " YEAR(gsrGoal.gsrGoalStartDate) =  ? "
			+ " group by  gsrGoal.fkGsrDatEmpId,gsrGoal.fkGsrSlabId ")
	public List<GsrDatEmpGoalBO> getCurrentYearGoalStatusByRmId(int rmId,
			int Year);
	@Query("SELECT new com.thbs.mis.gsr.bo.GsrDatEmpGoalBO "
			+ "( slab.gsrSlabName ," + " gsrGoal.fkGsrDatEmpId, "
			+ "gsrGoal.fkGsrDatMgrId, " + " gsrGoal.fkGsrGoalStatus, "
			+ "gsrGoal.fkGsrSlabId, " + " gsrGoal.gsrDatEmpGoalName, "
			+ "SUM(gsrGoal.gsrGoalWeightage) ," 
			+ "gsrGoal.fkEmpGoalDomainMgrId,"
			+ "gsrGoal.fkEmpGoalBuId"+ " ) "
			+ "from GsrDatEmpGoalBO  gsrGoal " + " JOIN GsrSlabBO slab "
			+ " ON slab.pkGsrSlabId=gsrGoal.fkGsrSlabId"
			+ " where gsrGoal.fkGsrDatEmpId = ? AND "
			+ " gsrGoal.fkGsrDatMgrId = ? AND "
			+ " gsrGoal.fkGsrSlabId = ? AND "
			+ " YEAR(gsrGoal.gsrGoalStartDate) =  ? "
			+ " group by  gsrGoal.fkGsrSlabId ")
	public List<GsrDatEmpGoalBO> getCurrentYearGoalStatusByEmpIdWithRmIdAndSlabId(int empId,int rmId,Short slabId,
			int Year);
	
	@Query("SELECT new com.thbs.mis.gsr.bo.GsrDatEmpGoalBO "
			+ "( slab.gsrSlabName ," + " gsrGoal.fkGsrDatEmpId, "
			+ "gsrGoal.fkGsrDatMgrId, " + " gsrGoal.fkGsrGoalStatus, "
			+ "gsrGoal.fkGsrSlabId, " + " gsrGoal.gsrDatEmpGoalName, "
			+ "SUM(gsrGoal.gsrGoalWeightage) ," 
			+ "gsrGoal.fkEmpGoalDomainMgrId,"
			+ "gsrGoal.fkEmpGoalBuId"+ " ) "
			+ "from GsrDatEmpGoalBO  gsrGoal " + " JOIN GsrSlabBO slab "
			+ " ON slab.pkGsrSlabId=gsrGoal.fkGsrSlabId"
			+ " where gsrGoal.fkGsrDatEmpId = ? AND "
//			+ " gsrGoal.fkGsrDatMgrId = ? AND "
			+ " gsrGoal.fkGsrSlabId = ?  "
//			+ " YEAR(gsrGoal.gsrGoalStartDate) =  ? "
			+ " AND gsrGoal.fkGsrGoalStatus != 6 " //Added by Shyam for Issue #139
			+ " group by  gsrGoal.fkGsrSlabId, gsrGoal.fkGsrDatMgrId")
	public List<GsrDatEmpGoalBO> getCurrentYearGoalStatusByEmpIdAndSlabId(
			Integer empId, Short slabId);
	
	//added by Smrithi
	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpIdAndFkGsrSlabIdAndEmpGoalKpiWeightageIdAndPkGsrDatEmpGoalIdNot(Integer empId,Short slabId,Short kpiWeightageId,Integer goalId);
	//EOA by Smrithi 

	//added by Rajesh 14/7/17
	public GsrDatEmpGoalBO findByFkGsrDatEmpIdAndFkGsrSlabIdAndEmpGoalKpiWeightageIdAndPkGsrDatEmpGoalId(Integer empId,Short slabId,Short kpiWeightageId,Integer goalId);
	//EOA
	@Query("SELECT new com.thbs.mis.gsr.bo.GsrDatEmpGoalBO "
			+ "( slab.gsrSlabName ," + " gsrGoal.fkGsrDatEmpId, "
			+ "gsrGoal.fkGsrDatMgrId, " + " gsrGoal.fkGsrGoalStatus, "
			+ "gsrGoal.fkGsrSlabId, " + " gsrGoal.gsrDatEmpGoalName, "
			+ "SUM(gsrGoal.gsrGoalWeightage) ," 
			+ "gsrGoal.fkEmpGoalDomainMgrId,"
			+ "gsrGoal.fkEmpGoalBuId"+ " ) "
			+ "from GsrDatEmpGoalBO  gsrGoal " + " JOIN GsrSlabBO slab "
			+ " ON slab.pkGsrSlabId=gsrGoal.fkGsrSlabId"
			+ " where "
//			+ "gsrGoal.fkEmpGoalDomainMgrId = ? AND "
			+ " YEAR(gsrGoal.gsrGoalStartDate) =  ? "
			+ " group by gsrGoal.fkGsrSlabId,gsrGoal.fkGsrDatEmpId ")
	public List<GsrDatEmpGoalBO> getCurrentYearGoalStatusForAllEmployees(
			int Year);
	
	public List<GsrDatEmpGoalBO> findByFkEmpGoalBuIdAndAndFkGsrSlabId(Short buId,Short slabId);
	
	public List<GsrDatEmpGoalBO> findByFkEmpGoalDomainMgrIdAndFkGsrSlabId(Integer fkEmpGoalDomainMgrId, short pkGsrSlabId);
	
	public List<GsrDatEmpGoalBO> findByFkGsrSlabId(short pkGsrSlabId);
}
