package com.thbs.mis.gsr.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bean.GsrWorflowPermissionBean;
import com.thbs.mis.gsr.bo.GsrTrainingRequestBO;
import com.thbs.mis.gsr.bo.GsrWorkflowPermissionBO;

@Repository
public interface GSRWorflowPermissionRepository extends GenericRepository<GsrWorkflowPermissionBO,Long> {
	
	public GsrWorkflowPermissionBO findByFkGsrWorkflowPermissionEmpId(int fkGsrWorkflowPermissionEmpId);
	 

}
