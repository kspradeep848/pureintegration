/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRRequestSpec.java                               */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  October 28, 2016                                  */
/*                                                                   */
/*  Description :  This class contains all the methods               */
/*                 for the GSRRequestSpecRepository                  */			 									 
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/*October 28, 2016     THBS     1.0        Initial version created   */
/*********************************************************************/

package com.thbs.mis.gsr.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.training.bo.TrainingDatProgrammeDetailBO;

/**
 * 
 * @author THBS
 * @version 1.0
 *
 */
public class GsrRequestSpec implements Specification<TrainingDatProgrammeDetailBO>
{
	private  TrainingDatProgrammeDetailBO criteria;
	private static final AppLog LOG = LogFactory
			.getLog(GsrRequestSpec.class);
	
	@Override
	public Predicate toPredicate(Root<TrainingDatProgrammeDetailBO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
		
		if(criteria.getFkProgrammeStatusId()>0)
		{
			System.out.println("inside 1");
			return builder.equal(root.get(criteria.getFkProgrammeStatusId()+""), criteria.getFkProgrammeStatusId()); 
		}
		else if(criteria.getTrainingStartDateTime() !=null && criteria.getTrainingEndDateTime()!=null)
		{
			System.out.println("inside 2");
			return null;
			//return builder.between(root.get(criteria.getTrainingStartdate()+""), criteria.getTrainingEnddate(), criteria.getTrainingStartdate());
			
		}
		else if(criteria.getTrainingStartDateTime() !=null && criteria.getTrainingEndDateTime()!=null && criteria.getFkProgrammeStatusId() > 0)
		{
			System.out.println("inside 3");
			//return builder.equal(root.get(criteria.getFkStatusId()+""), criteria.getFkStatusId());
			return null;
		}
		return null;
	}
	
}
