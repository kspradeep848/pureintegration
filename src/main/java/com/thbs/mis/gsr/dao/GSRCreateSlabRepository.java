/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrAcceptRatingRepository.java                    */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the GSRCreateSlabRepository              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrSlabBO;

@Repository
public interface GSRCreateSlabRepository extends GenericRepository<GsrSlabBO, Long>
{
	List<GsrSlabBO> findByGsrSlabStartDateGreaterThanEqualAndGsrSlabEndDateLessThanEqual(Date startDate,Date endDate);
	List<GsrSlabBO> findAll();
	
	//Added by Shyam for creation of new goal
	@Query("SELECT s FROM GsrSlabBO s WHERE ?1 BETWEEN s.gsrSlabStartDate AND s.gsrSlabEndDate "
			+ " AND ?2 BETWEEN s.gsrSlabStartDate AND s.gsrSlabEndDate ")
	public GsrSlabBO getSlabObjectFromStartAndEndDate(Date startDate,Date endDate);
	
	public GsrSlabBO findByPkGsrSlabId(short pkGsrSlabId);
	//End of addition by Shyam for creation of new goal
}
