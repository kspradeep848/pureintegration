/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrNormalisedRatingOrgWideRepository.java                            */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the methods for the           */
/*                 GSRCommonController                               */
/*  Created By  : Manikandan_Chinnasamy                              */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.dao;

import java.util.List;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrNormalisedRatingOrgWideBO;

public interface GsrNormalisedRatingOrgWideRepository extends GenericRepository<GsrNormalisedRatingOrgWideBO,Long>
{
	List<GsrNormalisedRatingOrgWideBO> findByGsrNormalisedRatingYear(String gsrNormalisedRatingYear);
}