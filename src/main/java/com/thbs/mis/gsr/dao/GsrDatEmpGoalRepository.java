/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrDatEmpGoalRepository.java                      */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the GsrDatEmpGoalRepository              */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrDatEmpGoalBO;

@Repository
public interface GsrDatEmpGoalRepository extends GenericRepository<GsrDatEmpGoalBO, Long> {

	public final static String APPROVE_GOAL_STATUS = "UPDATE GsrDatEmpGoalBO m SET m.fkGsrGoalStatus= ? where m.pkGsrDatEmpGoalId= ?";

	@Transactional
	@Modifying
	@Query(APPROVE_GOAL_STATUS)
	public int approveGoal(Byte status, Integer empGoalId);

	public GsrDatEmpGoalBO findByPkGsrDatEmpGoalId(Integer pkDatgoalId);

	public final static String ENTER_FEEDBACK = "UPDATE GsrDatEmpGoalBO n SET n.gsrGoalMgrFeedback= ?, n.gsrGoalSelfRating= ?, n.gsrGoalMgrRating= ? WHERE n.pkGsrDatEmpGoalId= ?";

	@Transactional
	@Modifying
	@Query(ENTER_FEEDBACK)
	public int enterRating(String feedback, Float selfRating, Float mgrRating,
			Integer empGoalId);

	public final static String DELETE_GOAL = "DELETE from GsrDatEmpGoalBO n WHERE n.pkGsrDatEmpGoalId= ?";

	@Transactional
	@Modifying
	@Query(DELETE_GOAL)
	public int delete_Goal(Integer empGoalId);
 	
	public GsrDatEmpGoalBO findByPkGsrDatEmpGoalIdAndGsrGoalStartDateGreaterThanEqualAndGsrGoalEndDateLessThanEqual(
			int empGoalId, Date goalStartDate, Date goalEndDate);

	public Iterable<GsrDatEmpGoalBO> findAll();

	public GsrDatEmpGoalBO findByPkGsrDatEmpGoalIdAndGsrGoalStartDateAndGsrGoalEndDate(
			Integer getpkGsrDatEmpGoalId, Date getgsrGoalStartDate,
			Date getgsrGoalEndDate);

	// Added by prathibha on 22/3/2017
	@Query("SELECT new com.thbs.mis.gsr.bo.GsrDatEmpGoalBO( "
			+ "gdempdtl.pkGsrDatEmpGoalId, gdempdtl.gsrDatEmpGoalName,gdempdtl.fkGsrDatEmpId, "
			+ "gdempdtl.gsrGoalStartDate,gdempdtl.gsrGoalEndDate, "
			+ "gdempdtl.fkGsrSlabId,gdempdtl.gsrGoalWeightage,gdempdtl.gsrGoalDescription,gdempdtl.gsrGoalMeasurement, "
			+ "gdempdtl.gsrGoalAccomplishment,gdempdtl.gsrGoalSelfRating,gdempdtl.fkGsrGoalStatus, "
			+ "gsrslab.pkGsrSlabId,gsrslab.gsrSlabName,gsrslab.gsrSlabYear, "
			+ "personldet.empFirstName,personldet.empLastName, "
			+ "gsrglstatus.gsrGoalStatusName) FROM GsrDatEmpGoalBO gdempdtl JOIN GsrSlabBO gsrslab ON "
			+ "gsrslab.pkGsrSlabId = gdempdtl.fkGsrSlabId JOIN DatEmpPersonalDetailBO personldet ON "
			+ "personldet.fkEmpDetailId = gdempdtl.fkGsrDatEmpId JOIN GsrMasGoalStatusBO gsrglstatus ON "
			+ "gsrglstatus.pkGsrGoalStatusId = gdempdtl.fkGsrGoalStatus WHERE "
			+ "gdempdtl.fkEmpGoalDomainMgrId=?1 AND gdempdtl.fkGsrSlabId=?2 ")
	public List<GsrDatEmpGoalBO> viewDMeReporeetsGoal(Integer domainManagerId,short slabId);

	//Added by Prathibha for view reportees goals by BU Head
	@Query("SELECT new com.thbs.mis.gsr.bo.GsrDatEmpGoalBO( "
	 		+ "gsrdatgl.pkGsrDatEmpGoalId,gsrdatgl.gsrDatEmpGoalName, "
			+ "gsrdatgl.fkGsrDatEmpId,gsrdatgl.gsrGoalStartDate, "
			+ "gsrdatgl.gsrGoalEndDate,gsrdatgl.fkGsrSlabId, "
			+ "gsrdatgl.gsrGoalWeightage,gsrdatgl.gsrGoalDescription, "
			+ "gsrdatgl.gsrGoalMeasurement,gsrdatgl.gsrGoalAccomplishment, "
			+ "gsrdatgl.gsrGoalSelfRating,gsrdatgl.fkGsrGoalStatus,gsrdatgl.fkEmpGoalBuId, "
			+ "gsrslab.pkGsrSlabId,gsrslab.gsrSlabName,gsrslab.gsrSlabYear, "
			+ "persndet.empFirstName,persndet.empLastName, "
			+ "gsrglstatus.gsrGoalStatusName, "
			+ "masbu.fkBuHeadEmpId) FROM "
			+ "GsrDatEmpGoalBO gsrdatgl JOIN GsrSlabBO gsrslab ON gsrslab.pkGsrSlabId=gsrdatgl.fkGsrSlabId JOIN "
			+ "DatEmpPersonalDetailBO persndet ON persndet.fkEmpDetailId=gsrdatgl.fkGsrDatEmpId JOIN "
			+ "GsrMasGoalStatusBO gsrglstatus ON gsrglstatus.pkGsrGoalStatusId=gsrdatgl.fkGsrGoalStatus JOIN "
			+ "MasBuUnitBO masbu ON masbu.pkBuUnitId=gsrdatgl.fkEmpGoalBuId WHERE "
			+ "masbu.fkBuHeadEmpId=?1 AND gsrdatgl.fkGsrSlabId=?2 ")	
	 public List<GsrDatEmpGoalBO> viewRepoGoalsByBUHead (Integer buHeadid,short slabId);
	 //EOC by Prathibha for view reportees goals by BU Head
		 
		
		 

	@Query(nativeQuery = true, value = "SELECT (SELECT count(*) FROM gsr_dat_emp_goal g "
			+ " WHERE g.fk_gsr_dat_emp_id =:employeeId  AND g.fk_gsr_slab_id =:slabId  AND g.fk_gsr_goal_status != 6) "
			+ " - (SELECT count(*) FROM gsr_dat_emp_goal e WHERE e.fk_gsr_dat_emp_id =:employeeId "
			+ " AND e.fk_gsr_slab_id =:slabId AND e.fk_gsr_goal_status = 4)")
	public int getNumberOfGoalsClosedOfEmployee(
			@Param("employeeId") Integer employeeId,
			@Param("slabId") short slabId);

	@Query("SELECT g FROM GsrDatEmpGoalBO g WHERE g.fkGsrDatEmpId =?1 AND g.fkGsrSlabId =?2 AND g.fkGsrGoalStatus !=6 ")
	public List<GsrDatEmpGoalBO> getAllGoalsOfAnEmployee(int employeeId,
			short slabId);

	@Modifying
	@Query("UPDATE GsrDatEmpGoalBO g SET g.gsrGoalMgrFeedback =?1, g.gsrGoalMgrRating =?2, g.fkGsrGoalStatus =?4 WHERE g.pkGsrDatEmpGoalId =?3 ")
	public int updateGsrGoalAsRated(String mgrFeedback, Float mgrRating,
			Integer goalId, byte goalStatus);

	@Query(nativeQuery = true, value = "SELECT sum(gsr_goal_weightage) FROM gsr_dat_emp_goal g WHERE g.fk_gsr_dat_emp_id = :empID AND g.fk_gsr_slab_id = :slabId AND g.fk_gsr_dat_mgr_id = :mgrId "
			+ " AND g.fk_gsr_goal_status != 6 ")
	public Integer getSumOfWeightage(@Param("empID") Integer empID,
			@Param("slabId") Short slabId,@Param("mgrId") Integer mgrId);

	//Added by Shyam for updation of existing goals
	@Query("SELECT g FROM GsrDatEmpGoalBO g "
			+ " JOIN GsrKpiWeightageBO k ON k.pkGsrKpiWeightageId = g.empGoalKpiWeightageId "
			+ " WHERE g.fkGsrDatEmpId =?1 AND g.fkGsrSlabId =?2 AND g.fkGsrGoalStatus != 6 "
			+ " AND g.fkGsrDatMgrId =?3 AND g.empGoalKpiWeightageId >0")
	public List<GsrDatEmpGoalBO> getAllKpiGoalsOfEmployeeGoalOfASlab(Integer employeeId,  short slabId, Integer managerId);
	
	
	@Query("SELECT g FROM GsrDatEmpGoalBO g WHERE g.fkGsrDatEmpId =?1 AND g.fkGsrSlabId =?2 "
			+ " AND g.fkGsrGoalStatus !=6 AND g.empGoalKpiWeightageId=0 AND g.fkGsrDatMgrId =?3")
	public List<GsrDatEmpGoalBO> getAllOtherGoalsOfEmployeeOfASlab(Integer employeeId, short slabId, Integer managerId);
	//End of addition by Shyam for updation of existing of goals
	
	public List<GsrDatEmpGoalBO> findByfkGsrDatEmpIdAndFkGsrSlabId(Integer empId,short slabId);
	
	
	//Addition by Shyam for creation of new goals
	@Query("SELECT g FROM GsrDatEmpGoalBO g WHERE g.fkGsrDatEmpId = :empId AND g.fkGsrSlabId = :slabId "
			+ " AND g.fkGsrDatMgrId = :mgrId AND g.fkGsrGoalStatus != 6")
	public List<GsrDatEmpGoalBO> getAllGoalsEnteredByEmpOfASlabUnderAMgr(@Param("empId") int empId, 
			@Param("slabId") short slabId, @Param("mgrId") int mgrId);
	
	@Query("SELECT g FROM GsrDatEmpGoalBO g WHERE g.fkGsrDatEmpId = :empId AND g.fkGsrSlabId = :slabId "
			+ "AND g.fkGsrGoalStatus != 6")
	public List<GsrDatEmpGoalBO> getAllEneteredEmpGoalsOfASlab(@Param("empId") int empId, 
			@Param("slabId") short slabId);
	
	//End of addition by Shyam for creation of new goals
	
	// added by Smrithi
	//Fetch past goals by employee id 
	@Query("SELECT distinct new com.thbs.mis.gsr.bo.GsrDatEmpGoalBO "
			+ "( slab.gsrSlabName ,"
			+ " gsrGoal.fkGsrDatEmpId, "
			+ "slab.gsrSlabShortName,"
			+ "gsrGoal.fkGsrDatMgrId, "
			+ " gsrGoal.fkGsrGoalStatus, "
			+ "gsrGoal.pkGsrDatEmpGoalId,"
			+ "gsrGoal.gsrGoalAccomplishment,"
			+ "gsrGoal.gsrGoalDescription,"
			+ "gsrGoal.gsrGoalMeasurement,"
			+ "gsrGoal.fkGsrSlabId, "
			+ " gsrGoal.gsrDatEmpGoalName, "
			+ "gsrGoal.gsrGoalWeightage ,"
			+ "gsrGoal.fkEmpGoalDomainMgrId,"
			+ "gsrGoal.fkEmpGoalBuId,"
			+ " gsrGoal.empGoalKpiWeightageId,"
			+ "w"
			+ " ) "
			+ "from GsrDatEmpGoalBO  gsrGoal "
			+ " JOIN GsrSlabBO slab "
			+ " ON slab.pkGsrSlabId = gsrGoal.fkGsrSlabId "
			+ " LEFT JOIN GsrKpiWeightageBO w on gsrGoal.empGoalKpiWeightageId = w.pkGsrKpiWeightageId "
			+ " where gsrGoal.fkGsrDatEmpId = ? AND "
			+ " slab.gsrSlabYear IN ((YEAR(now()) - 1) , YEAR(now()))"
			+ " order by gsrGoal.empGoalKpiWeightageId ")
	public List<GsrDatEmpGoalBO> fetchGoalsByEmpId(int empId);
	
	
	
	//Fetch past goals by employee id List 
	@Query("SELECT distinct new com.thbs.mis.gsr.bo.GsrDatEmpGoalBO "
			+ "( slab.gsrSlabName ,"
			+ " gsrGoal.fkGsrDatEmpId, "
			+ "slab.gsrSlabShortName,"
			+ "gsrGoal.fkGsrDatMgrId, "
			+ " gsrGoal.fkGsrGoalStatus, "
			+ "gsrGoal.pkGsrDatEmpGoalId,"
			+ "gsrGoal.gsrGoalAccomplishment,"
			+ "gsrGoal.gsrGoalDescription,"
			+ "gsrGoal.gsrGoalMeasurement,"
			+ "gsrGoal.fkGsrSlabId, "
			+ " gsrGoal.gsrDatEmpGoalName, "
			+ "gsrGoal.gsrGoalWeightage ,"
			+ "gsrGoal.fkEmpGoalDomainMgrId,"
			+ "gsrGoal.fkEmpGoalBuId,"
			+ " gsrGoal.empGoalKpiWeightageId,"
			+ "w"
			+ " ) "
			+ "from GsrDatEmpGoalBO  gsrGoal "
			+ " JOIN GsrSlabBO slab "
			+ " ON slab.pkGsrSlabId = gsrGoal.fkGsrSlabId "
			+ " LEFT JOIN GsrKpiWeightageBO w on gsrGoal.empGoalKpiWeightageId = w.pkGsrKpiWeightageId "
			+ " where gsrGoal.fkGsrDatEmpId IN (:empIdList) AND "
			+ " slab.gsrSlabYear IN ((YEAR(now()) - 1) , YEAR(now()))"
			+ " order by gsrGoal.empGoalKpiWeightageId ")
	public List<GsrDatEmpGoalBO> fetchGoalsByEmpIdList(@Param("empIdList") List<Integer> empId);
	
	
	//Fetch past goals by reporting manager id 
	@Query("SELECT distinct new com.thbs.mis.gsr.bo.GsrDatEmpGoalBO "
			+ "( slab.gsrSlabName ,"
			+ " gsrGoal.fkGsrDatEmpId, "
			+ "slab.gsrSlabShortName,"
			+ "gsrGoal.fkGsrDatMgrId, "
			+ " gsrGoal.fkGsrGoalStatus, "
			+ "gsrGoal.pkGsrDatEmpGoalId,"
			+ "gsrGoal.gsrGoalAccomplishment,"
			+ "gsrGoal.gsrGoalDescription,"
			+ "gsrGoal.gsrGoalMeasurement,"
			+ "gsrGoal.fkGsrSlabId, "
			+ " gsrGoal.gsrDatEmpGoalName, "
			+ "gsrGoal.gsrGoalWeightage ,"
			+ "gsrGoal.fkEmpGoalDomainMgrId,"
			+ "gsrGoal.fkEmpGoalBuId,"
			+ " gsrGoal.empGoalKpiWeightageId,"
			+ "w"
			+ " ) "
			+ "from GsrDatEmpGoalBO  gsrGoal "
			+ " JOIN GsrSlabBO slab "
			+ " ON slab.pkGsrSlabId=gsrGoal.fkGsrSlabId "
			+ " LEFT JOIN GsrKpiWeightageBO w on gsrGoal.empGoalKpiWeightageId = w.pkGsrKpiWeightageId "
			+ " where gsrGoal.fkGsrDatMgrId = ? AND  "
			+ " slab.gsrSlabYear IN ((YEAR(now()) - 1) , YEAR(now()))"
			)
	public List<GsrDatEmpGoalBO> fetchGoalsByMgrId(Integer mgrId);

	@Transactional
	@Modifying
	@Query("UPDATE GsrDatEmpGoalBO g SET g.fkGsrGoalStatus = 9, g.gsrGoalModifiedDate = now(), g.gsrGoalModifiedBy = :modifiedBy"
			+ " WHERE g.fkGsrSlabId =:slabId and g.fkGsrDatEmpId=:empId and g.fkGsrDatEmpId not in :empIds and g.fkGsrGoalStatus in(5,7)")
			public Integer updateReopenGoal(@Param("empId") Integer empId,@Param("slabId") Short slabId,@Param("modifiedBy") Integer modifiedBy,@Param("empIds") List<Integer> empIds);
	// EOA by Smrithi
	
	
	// Added by pratibha for enter feedback service
		@Query("SELECT g FROM GsrDatEmpGoalBO g WHERE g.fkGsrDatEmpId =?1 AND g.fkGsrSlabId =?2 AND g.fkGsrDatMgrId =?3 "
				+ "AND g.fkGsrGoalStatus != 6")
		public List<GsrDatEmpGoalBO> getAllGoalsOfAnEmployeeUnderMgr(Integer employeeId, Short slabId, Integer managerId);
		
		@Query(nativeQuery = true, value = "SELECT (SELECT count(*) FROM gsr_dat_emp_goal g "
				+ " WHERE g.fk_gsr_dat_emp_id =:employeeId  AND g.fk_gsr_slab_id =:slabId  AND g.fk_gsr_goal_status != 6 "
				+ " AND g.fk_gsr_dat_mgr_id =:managerId ) "
				+ " - (SELECT count(*) FROM gsr_dat_emp_goal e WHERE e.fk_gsr_dat_emp_id =:employeeId "
				+ " AND e.fk_gsr_slab_id =:slabId AND e.fk_gsr_goal_status in (4,5,7,9) AND e.fk_gsr_dat_mgr_id =:managerId) ")
		public int getNumberOfGoalsClosedOfEmployee(@Param("employeeId") Integer employeeId, @Param("slabId") short slabId,
				@Param("managerId") Integer managerId);
		
		@Query("SELECT g FROM GsrDatEmpGoalBO g WHERE g.empGoalKpiWeightageId = :kpiId AND g.fkGsrGoalStatus != 6 "
				+ " AND g.fkGsrSlabId = :slabId AND g.fkGsrDatEmpId = :employeeId AND g.pkGsrDatEmpGoalId != :goalId")
		public List<GsrDatEmpGoalBO> getGoalHavingKpiIdAndSlabIdAndMgrFeedbackStatus(@Param("kpiId") short kpiId, 
				@Param("slabId") short slabId, @Param("employeeId") Integer employeeId, @Param("goalId") int goalId);
		
		
		public List<GsrDatEmpGoalBO> findByFkGsrSlabIdAndFkGsrDatEmpIdAndFkGsrDatMgrIdAndEmpGoalKpiWeightageIdIsNotNull(short slabId,Integer empId,Integer mgrId);
		public List<GsrDatEmpGoalBO> findByFkGsrSlabIdAndFkGsrDatEmpIdAndFkGsrDatMgrIdAndEmpGoalKpiWeightageIdIsNull(short slabId,Integer empId,Integer mgrId);
		
		public List<GsrDatEmpGoalBO> findByFkGsrSlabIdAndFkGsrDatEmpIdAndEmpGoalKpiWeightageIdIsNotNull(short slabId,Integer empId);
		public List<GsrDatEmpGoalBO> findByFkGsrSlabIdAndFkGsrDatEmpIdAndEmpGoalKpiWeightageIdIsNull(short slabId,Integer empId);
		
		// EOA by pratibha
		
		//Added by Shyam for creation of goal by BUHead, Skip Level Mgr, Domain Mgr and HR.
		@Query("SELECT g FROM GsrDatEmpGoalBO g WHERE g.fkGsrSlabId = :slabId AND g.fkGsrDatEmpId = :employeeId "
				+ " AND g.fkGsrGoalStatus not in (4,6,8)")
		public List<GsrDatEmpGoalBO> getAllEnteredGoalsOfASlab(@Param("slabId") short slabId, @Param("employeeId") int employeeId);
		
		
		@Query("SELECT g FROM GsrDatEmpGoalBO g WHERE g.fkGsrSlabId = :slabId AND g.fkGsrDatEmpId = :employeeId "
				+ " AND g.fkGsrGoalStatus != 6 ")
		public List<GsrDatEmpGoalBO> getAllEnteredGsrGoalsOfASlab(@Param("slabId") short slabId, @Param("employeeId") int employeeId);
		//End of addition by Shyam
	
}
