/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRDefaultGoalsRepository.java                    */
/*                                                                   */
/*  Author      : THBS                                               */
/*                                                                   */
/*  Date        :  November 04, 2016                                 */
/*                                                                   */
/*  Description :  This class contains all the methods               */
/*                 for the GSRDefaultGoalsRepository                 */				 									 
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 04, 2016     THBS     1.0        Initial version created */
/*********************************************************************/

package com.thbs.mis.gsr.dao;

import java.util.List;

import org.hibernate.annotations.ParamDef;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrKpiGoalBO;

/**
 * 
 * @author THBS
 * @version:1.0
 * @Description: This is GSRDefaultGoalsRepository.
 *
 */
@Repository
public interface GSRDefaultGoalsRepository extends GenericRepository<GsrKpiGoalBO, Long> {
	public List<GsrKpiGoalBO> findAll();
	
	
	
	
	@Query(nativeQuery=true, value ="SELECT * FROM gsr_kpi_weightage wtg  "
			+"inner join gsr_kpi_goal goal on "
			+" wtg.fk_gsr_kpi_goal_id =goal.pk_gsr_kpi_goal_id"	
		+" WHERE wtg.fk_emp_level_id = (SELECT fk_emp_level from dat_emp_professional_detail where fk_main_emp_detail_id=?1) "
			+" AND wtg.fk_bu_id=(SELECT fk_emp_bu_unit from dat_emp_professional_detail where fk_main_emp_detail_id=?1) "
		+" AND goal.gsr_kpi_goal_active_flag ='ENABLED' order by goal.pk_gsr_kpi_goal_id ")
	
	/*@Query("Select GsrKpiGoalBO goal join GsrKpiWeightageBO wtg on goal.pk_gsr_kpi_goal_id = wtg.fk_gsr_kpi_goal_id "
			+"join DatEmpProfessionalDetailBO empProfDetail on empProfDetail.fkMainEmpDetailId=:empId "
			+" WHERE wtg.fkEmpLevelId = empProfDetail.fkEmpLevel AND wtg.gsrKpiBuId=empProfDetail.fkEmpBuUnit "
			+" AND goal.gsrkpigoalactiveflag ='ENABLED' order by goal.pkGsrKpiGoalId ")*/
	public List<GsrKpiGoalBO>  getDefaultGoalsByEmpId(@Param ("empId") Integer empId);
	
	
	//public List<GsrKpiGoalBO>  getDefaultGoalsByGsrKpiWeightageBO(@Param ("empId") Integer empId);
	
	public GsrKpiGoalBO findByPkGsrKpiGoalId(short goalID);
	
	
	
	
	
}
