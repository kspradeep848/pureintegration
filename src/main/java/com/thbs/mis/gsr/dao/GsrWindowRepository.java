package com.thbs.mis.gsr.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrWindowBO;

@Repository
public interface GsrWindowRepository extends GenericRepository<GsrWindowBO, Long>, JpaSpecificationExecutor<GsrWindowBO>{

}
