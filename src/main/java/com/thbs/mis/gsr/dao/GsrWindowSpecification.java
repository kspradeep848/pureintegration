package com.thbs.mis.gsr.dao;

import java.util.Date;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.gsr.bo.GsrWindowBO;

public class GsrWindowSpecification {

	public Specification<GsrWindowBO> getGsrWindowEndDate(Short sladId){
	return (root, query, cb) -> {

		return cb.and((cb.lessThanOrEqualTo(root.get("gsrWindowStartDate"),
				new Date())), (cb.greaterThanOrEqualTo(
				root.get("gsrWindowEndDate"), new Date())),(cb.equal(root.get("fkGsrWorkFlowSlabId"), sladId)));
	};
	}
}
