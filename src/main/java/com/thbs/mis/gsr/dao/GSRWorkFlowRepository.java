/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRWorkflowRepository.java                        */
/*                                                                   */
/*  Author      :  THBS                                              */
/*                                                                   */
/*  Date        :  October 28, 2016                                  */
/*                                                                   */
/*  Description :   This class contains all the methods              */
/*                 for the GSRWorkflowRepository                     */			 									
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* October 28, 2016     THBS     1.0        Initial version created  */
/*********************************************************************/


package com.thbs.mis.gsr.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrWorkflowPermissionBO;

/**
 * 
 * @author THBS
 * @version:1.0
 * @Description: This is GSRWorkflowRepository.
 */
@Repository
public interface GSRWorkFlowRepository extends GenericRepository<GsrWorkflowPermissionBO,Long>{
	public List<GsrWorkflowPermissionBO> findAll();
	
	public List<GsrWorkflowPermissionBO> findByGsrSlabPkGsrSlabIdAndDatEmployeePkEmpId(short slabId, Integer empId);
	
	
}
