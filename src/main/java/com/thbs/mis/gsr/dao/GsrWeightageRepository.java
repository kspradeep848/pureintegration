package com.thbs.mis.gsr.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrKpiWeightageBO;

@Repository
public interface GsrWeightageRepository extends GenericRepository<GsrKpiWeightageBO,Long>{

	//Added by Shyam for creation of new goal
	// Update by Annu as per new KPi requirements based on employee level and BU
	@Query("SELECT w FROM GsrKpiWeightageBO w JOIN DatEmpProfessionalDetailBO prof "
			+ " ON prof.fkEmpBuUnit = w.gsrKpiBuId JOIN GsrKpiGoalBO k "
			+ " ON k.pkGsrKpiGoalId = w.fkGsrKpiGoalId WHERE prof.fkMainEmpDetailId = :employeeId "
			+ " AND prof.fkEmpLevel = w.fkEmpLevelId AND k.gsrkpigoalactiveflag like '%ENABLED%' ") 
	public List<GsrKpiWeightageBO> getAllKpiGoalsWithWeightageForEmpLevel(@Param("employeeId") int employeeId);
	//End of addition by Shyam for creation of new goal 
	
	//Added by Smrithi
	
	@Query("SELECT w FROM GsrKpiWeightageBO w JOIN DatEmpProfessionalDetailBO prof "
			+ " ON prof.fkEmpBuUnit = w.gsrKpiBuId JOIN GsrKpiGoalBO k "
			+ " ON k.pkGsrKpiGoalId = w.fkGsrKpiGoalId WHERE prof.fkMainEmpDetailId IN :employeeId "
			+ " AND prof.fkEmpLevel = w.fkEmpLevelId AND k.gsrkpigoalactiveflag like '%ENABLED%' ") 
	public List<GsrKpiWeightageBO> getAllKpiGoalsWithWeightageForEmpLevelList(@Param("employeeId") List<Integer> employeeId);
	
	//EOA by Smrithi
	
	public GsrKpiWeightageBO findByPkGsrKpiWeightageId(short pkGsrKpiWeightageId);
	
	
	
	@Query("SELECT new com.thbs.mis.gsr.bo.GsrKpiWeightageBO("
			+ " kpi.fkEmpLevelId,"
			+ " kpi.gsrKpiBuId,"
			+ " kpi.pkGsrKpiWeightageId,"
			+ " kpi.gsrKpiGoalWeightage)"
			+ " FROM GsrKpiWeightageBO  kpi"
			+ "  WHERE  kpi.fkEmpLevelId = ?1"
			+ " AND kpi.gsrKpiBuId = ?2"
			+ " AND kpi.fkGsrKpiGoalId =(SELECT kpi2.fkGsrKpiGoalId FROM GsrKpiWeightageBO  kpi2 WHERE"
			+ " kpi2.pkGsrKpiWeightageId = ?3)")
	public GsrKpiWeightageBO getWeightage(Short levelId,byte buId,Short weighatgeId);
	
	@Query("SELECT new com.thbs.mis.gsr.bo.GsrKpiWeightageBO("
			+ " kpi.fkEmpLevelId,"
			+ " kpi.gsrKpiBuId,"
			+ " kpi.pkGsrKpiWeightageId,"
			+ " kpi.gsrKpiGoalWeightage,"
			+ " kpi.fkGsrKpiGoalId)"
			+ " FROM GsrKpiWeightageBO  kpi"
			+ "  WHERE  kpi.fkEmpLevelId = ?1"
			+ " AND kpi.gsrKpiBuId = ?2")
			/*+ " AND kpi.fkGsrKpiGoalId =(SELECT kpi2.fkGsrKpiGoalId FROM GsrKpiWeightageBO  kpi2 WHERE"
			+ " kpi2.pkGsrKpiWeightageId = ?3)")*/
	public List<GsrKpiWeightageBO> getAllWeightage(Short levelId,byte buId);
	
	

}
