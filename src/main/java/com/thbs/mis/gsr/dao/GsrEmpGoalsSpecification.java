/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrEmpGoalsSpecification.java                     */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  March 29, 2017                                    */
/*                                                                   */
/*  Description :  This class contains specification for Employee    */
/*                 goals.   	 				                     */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 02, 2016     THBS     1.0        Initial version created */
/*********************************************************************/
package com.thbs.mis.gsr.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.gsr.bean.HrViewEmpGoalsInputBean;
import com.thbs.mis.gsr.bean.HrViewTeamGoalsInputBean;
import com.thbs.mis.gsr.bean.ViewEmployeeGoalsInputBean;
import com.thbs.mis.gsr.bean.ViewTeamGoalsInputBean;
import com.thbs.mis.gsr.bo.GsrDatEmpGoalBO;

public class GsrEmpGoalsSpecification {

	/**
	 * 
	 * @param goalBean
	 * @return Specification<GsrDatEmpGoalBO>
	 * @Description : This specification is used to get Reportees Goals based of
	 *              a Reporting manager based on EmpId or Goal Status or Slab Id
	 */
	public static Specification<GsrDatEmpGoalBO> getReporteesGoals(
			ViewEmployeeGoalsInputBean goalBean) {
		return new Specification<GsrDatEmpGoalBO>() {
			@Override
			public Predicate toPredicate(Root<GsrDatEmpGoalBO> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				final Collection<Predicate> predicates = new ArrayList<>();

				final Predicate reportingMgrPredicate = cb.equal(
						root.get("fkGsrDatMgrId"), goalBean.getMgrId());
				predicates.add(reportingMgrPredicate);

				if (goalBean.getEmpIdList() != null) {
					if (goalBean.getEmpIdList().size() > 0) {
						final Predicate empListPredicate = root.get(
								"fkGsrDatEmpId").in(goalBean.getEmpIdList());
						predicates.add(empListPredicate);
					}
				}
				if (goalBean.getGoalStatus() != null) {
					final Predicate goalStatusPredicate = cb.equal(
							root.get("fkGsrGoalStatus"),
							goalBean.getGoalStatus());
					predicates.add(goalStatusPredicate);
				}
				if (goalBean.getSlabId() != null) {
					final Predicate slabIdPredicate = cb.equal(
							root.get("fkGsrSlabId"), goalBean.getSlabId());
					predicates.add(slabIdPredicate);
				}

				query.orderBy(cb.desc(root.get("fkGsrSlabId")),
						cb.asc(root.get("fkGsrDatEmpId")));

				return cb.and(predicates.toArray(new Predicate[predicates
						.size()]));
			}
		};
	}

	/**
	 * 
	 * @param goalBean
	 * @return Specification<GsrDatEmpGoalBO>
	 * @Description : this specification is used to get Employee Goals based on
	 *              Employee Id or Goal Status or Slab Id
	 */
	public static Specification<GsrDatEmpGoalBO> getGoalsByHR(
			HrViewEmpGoalsInputBean goalBean) {
		return new Specification<GsrDatEmpGoalBO>() {
			@Override
			public Predicate toPredicate(Root<GsrDatEmpGoalBO> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				final Collection<Predicate> predicates = new ArrayList<>();

				if (goalBean.getEmpIdList() != null) {
					if (goalBean.getEmpIdList().size() > 0) {
						final Predicate empListPredicate = root.get(
								"fkGsrDatEmpId").in(goalBean.getEmpIdList());
						predicates.add(empListPredicate);
					}
				}
				if (goalBean.getGoalStatus() != null) {
					final Predicate goalStatusPredicate = cb.equal(
							root.get("fkGsrGoalStatus"),
							goalBean.getGoalStatus());
					predicates.add(goalStatusPredicate);
				}
				if (goalBean.getSlabId() != null) {
					final Predicate slabIdPredicate = cb.equal(
							root.get("fkGsrSlabId"), goalBean.getSlabId());
					predicates.add(slabIdPredicate);
				}

				
				query.orderBy(cb.desc(root.get("fkGsrSlabId")),
						cb.asc(root.get("fkGsrDatEmpId")));

				return cb.and(predicates.toArray(new Predicate[predicates
						.size()]));
			}
		};
	}

	/**
	 * 
	 * @param teamGoalBean
	 * @return Specification<GsrDatEmpGoalBO>
	 * @Description : This specification is used to get Team Goals based on
	 *              Reporting Manager Id and Employee Id or slab Id
	 */
	public static Specification<GsrDatEmpGoalBO> getTeamGoalsByRm(
			ViewTeamGoalsInputBean teamGoalBean) {
		return new Specification<GsrDatEmpGoalBO>() {

			@Override
			public Predicate toPredicate(Root<GsrDatEmpGoalBO> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {

				final Collection<Predicate> predicates = new ArrayList<>();

				if (teamGoalBean.getMgrId() != null) {
					final Predicate reportingMgrPredicate = cb.equal(
							root.get("fkGsrDatMgrId"), teamGoalBean.getMgrId());
					predicates.add(reportingMgrPredicate);
				}

				if (teamGoalBean.getSlabId() != null) {
					if (teamGoalBean.getSlabId().size() > 0) {
						final Predicate slabIdListPredicate = root.get(
								"fkGsrSlabId").in(teamGoalBean.getSlabId());
						predicates.add(slabIdListPredicate);
					}
				}

				if (teamGoalBean.getEmpIdList() != null) {
					if (teamGoalBean.getEmpIdList().size() > 0) {
						final Predicate empListPredicate = root.get(
								"fkGsrDatEmpId")
								.in(teamGoalBean.getEmpIdList());
						predicates.add(empListPredicate);
					}
				}

				List<Order> order = new ArrayList<Order>();
				order.add(cb.desc(root.get("fkGsrSlabId")));
				order.add(cb.asc(root.get("fkGsrDatEmpId")));
				query.orderBy(order);
				/*query.orderBy(cb.desc(root.get("fkGsrSlabId")));
				query.orderBy(cb.asc(root.get("fkGsrDatEmpId")));*/

				return cb.and(predicates.toArray(new Predicate[predicates
						.size()]));

			}
		};
	}

	/**
	 * 
	 * @param teamGoalBean
	 * @return Specification<GsrDatEmpGoalBO>
	 * @Description : This specification is used to get Employee Goals by
	 *              Employee Id or Slab Id
	 */
	public static Specification<GsrDatEmpGoalBO> getTeamGoalsByHr(
			HrViewTeamGoalsInputBean teamGoalBean) {
		return new Specification<GsrDatEmpGoalBO>() {

			@Override
			public Predicate toPredicate(Root<GsrDatEmpGoalBO> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {

				final Collection<Predicate> predicates = new ArrayList<>();

				if (teamGoalBean.getMgrId() != null) {
					final Predicate reportingMgrPredicate = cb.equal(
							root.get("fkGsrDatMgrId"), teamGoalBean.getMgrId());
					predicates.add(reportingMgrPredicate);
				}

				if (teamGoalBean.getSlabId() != null) {
					if (teamGoalBean.getSlabId().size() > 0) {
						final Predicate slabIdListPredicate = root.get(
								"fkGsrSlabId").in(teamGoalBean.getSlabId());
						predicates.add(slabIdListPredicate);
					}
				}

				if (teamGoalBean.getEmpIdList() != null) {
					if (teamGoalBean.getEmpIdList().size() > 0) {
						final Predicate empListPredicate = root.get(
								"fkGsrDatEmpId")
								.in(teamGoalBean.getEmpIdList());
						predicates.add(empListPredicate);
					}
				}

				List<Order> order = new ArrayList<Order>();
				order.add(cb.desc(root.get("fkGsrSlabId")));
				order.add(cb.asc(root.get("fkGsrDatEmpId")));
				query.orderBy(order);
				/*query.orderBy(cb.desc(root.get("fkGsrSlabId")));
				query.orderBy(cb.asc(root.get("fkGsrDatEmpId")));*/

				return cb.and(predicates.toArray(new Predicate[predicates
						.size()]));
			}
		};
	}
}
