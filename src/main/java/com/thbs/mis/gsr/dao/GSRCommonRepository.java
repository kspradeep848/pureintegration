/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrCommonRepository.java                          */
/*                                                                   */
/*  Author      :  THBS                                              */
/*                                                                   */
/*  Date        : October 28, 2016                                   */
/*                                                                   */
/*  Description : This class contains all the methods                */
/*                 for the GSRCommonRepository                       */		 									
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* October 28, 2016     THBS     1.0        Initial version created  */
/*********************************************************************/

package com.thbs.mis.gsr.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.bo.MasTrainingTopicNameBO;
import com.thbs.mis.common.dao.GenericRepository;


/**
 * 
 * @author THBS
 * @version:1.0
 * @Description: This is GSRCommonRepository.
 */
@Repository
public interface GSRCommonRepository extends GenericRepository<MasTrainingTopicNameBO,Long>
{
	public List<MasTrainingTopicNameBO> findAll();
	
	
}

  