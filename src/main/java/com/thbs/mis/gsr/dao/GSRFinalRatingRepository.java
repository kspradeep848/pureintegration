/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRFinalRatingRepository.java                     */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  13-Nov-2016                                       */
/*                                                                   */
/*  Description :  Contains all the repository methods for the Final */
/*                 GSR rating functionalities.                       */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 13-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO;

@Repository
public interface GSRFinalRatingRepository extends GenericRepository<GsrDatEmpFinalRatingBO, Long> {

    //Added by Kamal Anand
    public GsrDatEmpFinalRatingBO findByFkGsrFinalRatingSlabIdAndFkGsrFinalRatingEmpId(short slabId, int empId);
    //End of Addition by Kamal Anand
    
    //Added by Shyam Lama
    @Query("SELECT new com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO(rating.fkGsrFinalRatingSlabId, rating.fkGsrFinalRatingEmpId,"
    		+ " rating.gsrFinalRating, rating.pkGsrFinalRatingId, per.empFirstName, per.empLastName, rating.finalRatingStatus) "
    		+ " FROM GsrDatEmpFinalRatingBO rating "
    		+ " JOIN DatEmpProfessionalDetailBO prof ON prof.fkMainEmpDetailId = rating.fkGsrFinalRatingEmpId "
    		+ " JOIN DatEmpPersonalDetailBO per ON per.fkEmpDetailId = rating.fkGsrFinalRatingEmpId "
    		+ " WHERE rating.fkGsrFinalRatingSlabId =?1 AND prof.fkEmpDomainMgrId =?2 AND rating.finalRatingStatus in (3,4) "
    		+ " ORDER BY rating.fkGsrFinalRatingEmpId ")
    public List<GsrDatEmpFinalRatingBO> getDisagreeAndResolvedRatingDetailsForASlabUnderDM(short slabId, int domainMgr);
    
    
    
    @Query("SELECT  new com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO(rating.fkGsrFinalRatingSlabId, rating.fkGsrFinalRatingEmpId, "
    		+ " emp.empFirstName, emp.empLastName, rating.fkGsrFinalRatingMgrId, mgr.empFirstName, mgr.empLastName, rating.pkGsrFinalRatingId, "
    		+ " rating.finalRatingStatus, rating.gsrFinalRating) FROM GsrDatEmpFinalRatingBO rating "
    		+ " JOIN DatEmpProfessionalDetailBO prof ON prof.fkMainEmpDetailId = rating.fkGsrFinalRatingEmpId "
    		+ " JOIN DatEmpPersonalDetailBO emp ON emp.fkEmpDetailId = rating.fkGsrFinalRatingEmpId "
    		+ " JOIN DatEmpPersonalDetailBO mgr ON mgr.fkEmpDetailId = rating.fkGsrFinalRatingMgrId "
    		+ " WHERE rating.fkGsrFinalRatingSlabId =?1 AND prof.fkEmpBuUnit =?2 AND rating.finalRatingStatus in (3,4) "
    		+ " ORDER BY rating.fkGsrFinalRatingMgrId, rating.fkGsrFinalRatingEmpId")
    public List<GsrDatEmpFinalRatingBO> getDisagreeOrResolvedRationgDetailsForASlabUnderBUHead(short slabId, short businessUnitId);
    
    
    @Query("SELECT new com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO(rating.fkGsrFinalRatingSlabId, rating.fkGsrFinalRatingEmpId,"
    		+ " rating.gsrFinalRating, rating.pkGsrFinalRatingId, per.empFirstName, per.empLastName, rating.finalRatingStatus) "
    		+ " FROM GsrDatEmpFinalRatingBO rating "
    		+ " JOIN DatEmpProfessionalDetailBO prof ON prof.fkMainEmpDetailId = rating.fkGsrFinalRatingEmpId "
    		+ " JOIN DatEmpPersonalDetailBO per ON per.fkEmpDetailId = rating.fkGsrFinalRatingEmpId "
    		+ " WHERE rating.fkGsrFinalRatingSlabId = :slabId AND rating.skipLevelMgrId = :skipLevelMgrId AND rating.finalRatingStatus in (3,4) "
    		+ " ORDER BY rating.fkGsrFinalRatingEmpId ")
    public List<GsrDatEmpFinalRatingBO> getDisagreeAndResolvedRatingDetailsForASlabUnderSkipLevelMgr(@Param("slabId") short slabId, 
    		@Param("skipLevelMgrId") int skipLevelMgrId);
    //End of addition by Shyam Lama
    
    //added by Smrithi 
    @Transactional
	@Modifying
	@Query("UPDATE GsrDatEmpFinalRatingBO g SET g.finalRatingStatus = 6, g.modifiedDate = now()"
			+ " WHERE g.fkGsrFinalRatingSlabId =:slabId and g.fkGsrFinalRatingEmpId=:empId and g.fkGsrFinalRatingEmpId not in :empIds and g.finalRatingStatus not in(6)")
			public Integer updateReopenGoalForFinalRating(@Param("empId") Integer empId,@Param("slabId") Short slabId,@Param("empIds") List<Integer> empIds);
    //EOA by Smrithi
    
    
    //Added by Pradeep K S
    public List<GsrDatEmpFinalRatingBO> findByFkGsrFinalRatingEmpIdAndFinalRatingStatusAndIsOverriddenFlag( int empId, byte status,String isOverriddenFlag);
   
    public List<GsrDatEmpFinalRatingBO> findByFkGsrFinalRatingEmpIdAndFinalRatingStatusAndIsOverriddenFlagAndOverriddenBy( int empId, byte status,String isOverriddenFlag, int overriddenBy );

    public List<GsrDatEmpFinalRatingBO> findByFkGsrFinalRatingEmpIdAndFinalRatingStatus( int empId, byte status);
    public List<GsrDatEmpFinalRatingBO> findByFkGsrFinalRatingMgrIdAndFinalRatingStatus( int rmID, byte status);

    @Query("SELECT goal from GsrDatEmpFinalRatingBO goal where goal.finalRatingStatus = 4 and goal.fkGsrFinalRatingEmpId IN"
			+ "(SELECT p.fkMainEmpDetailId from DatEmpProfessionalDetailBO p where p.fkEmpReportingMgrId = ?)")
	public List<GsrDatEmpFinalRatingBO> getReporteesForceCloseGoals(int empId);
    
    @Query("SELECT goal from GsrDatEmpFinalRatingBO goal where goal.finalRatingStatus = 3 and goal.fkGsrFinalRatingEmpId IN"
			+ "(SELECT p.fkMainEmpDetailId from DatEmpProfessionalDetailBO p where p.fkEmpReportingMgrId = ?)")
    public List<GsrDatEmpFinalRatingBO> getDisagreeRatingGoalsForRm(int rmId);
    
   /* @Query("SELECT goal from GsrDatEmpFinalRatingBO goal where goal.finalRatingStatus = 3 and goal.fkGsrFinalRatingEmpId IN"
			+ "(SELECT p.fkMainEmpDetailId from DatEmpProfessionalDetailBO p where p.fkEmpDomainMgrId = ?)")
    public List<GsrDatEmpFinalRatingBO> getDisagreeRatingGoalsForSkipLevelRm(int skipLvlRmId);*/
    
    public List<GsrDatEmpFinalRatingBO> findBySkipLevelMgrIdAndFinalRatingStatus(int skipLvlRmId,byte status);
    
    public List<GsrDatEmpFinalRatingBO> findByFinalRatingStatusOrderByFkGsrFinalRatingSlabIdAscFkGsrFinalRatingMgrIdAsc(byte status);
    
    //End of Addition by  Pradeep K S
    
    public List<GsrDatEmpFinalRatingBO> findByFkGsrFinalRatingEmpIdAndFkGsrFinalRatingMgrId(Integer empId, Integer rmId);
}
