/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRSlabRepository.java                            */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  October 28, 2016                                  */
/*                                                                   */
/*  Description :   This class contains all the methods              */
/*                 for the GSRSlabRepository                         */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/*October 28, 2016     THBS     1.0        Initial version created   */
/*********************************************************************/

package com.thbs.mis.gsr.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrSlabBO;

/**
 * 
 * @author THBS
 * @version 1.0
 *
 */
@Repository
public interface GsrSlabRepository extends GenericRepository<GsrSlabBO, Long>,
		JpaSpecificationExecutor<GsrSlabBO> {

	public List<GsrSlabBO> findAll();
	
	//Added by Rajesh Kumar for Slabs in Desc order
	public List<GsrSlabBO> findAllByOrderByPkGsrSlabIdDesc();

	// Added by Mani for GSR module service committed on 30-11-2016
	
	GsrSlabBO findByPkGsrSlabId(Short pkGsrSlabId);
	
	List<GsrSlabBO> findByGsrSlabYear(String gsrSlabYear);
	
	// End of addition by Mani for GSR module service committed on 30-11-2016
	
	//Added by Kamal Anand
	
	@Query(nativeQuery = true, value ="SELECT * FROM gsr_slab  where :currentDate between gsr_slab_start_date and gsr_slab_end_date "
			+ "UNION "
			+ "SELECT * FROM gsr_slab where :previousDate between gsr_slab_start_date and gsr_slab_end_date")
	List<GsrSlabBO> getCurrentAndPreviousSlab(@Param("currentDate") Date currentDate,@Param("previousDate") Date previousDate);
	
	
	@Query("SELECT new com.thbs.mis.gsr.bo.GsrSlabBO(slab.pkGsrSlabId, slab.gsrSlabEndDate,slab.gsrSlabName,slab.gsrSlabStartDate,slab.gsrSlabYear,slab.gsrSlabShortName,"
			+ "(select gsrFinalRating from GsrDatEmpFinalRatingBO rat where rat.fkGsrFinalRatingSlabId = slab.pkGsrSlabId "
			+ "and rat.fkGsrFinalRatingEmpId = ?)as finalRating) from  GsrSlabBO slab where (YEAR(now()) - slab.gsrSlabYear<=3) AND slab.gsrSlabEnabled = 'ENABLED'")
	List<GsrSlabBO> getPreviousSlabAndPreNormalizedRating(int empId);
	
	List<GsrSlabBO> findByGsrSlabYearAndGsrSlabEnabledOrderByPkGsrSlabIdDesc(String gsrSlabYear,String enabled);
	
	List<GsrSlabBO> findByGsrSlabYearOrderByPkGsrSlabIdDesc(String gsrSlabYear);
	
	//End of Addition by Kamal Anand
	
	//Added by Shyam for new gsr goal creation
	@Query("SELECT s FROM GsrSlabBO s WHERE :startDate BETWEEN s.gsrSlabStartDate AND s.gsrSlabEndDate "
			+ " AND :endDate BETWEEN s.gsrSlabStartDate AND s.gsrSlabEndDate")
	public GsrSlabBO getGsrSlabBOByStartAndEndDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
	//End of addition by Shyam for new gsr goal creation
	
	List<GsrSlabBO> findByGsrSlabYearOrderByPkGsrSlabIdAsc(String gsrSlabYear);

}
