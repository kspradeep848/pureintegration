/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRForceGoalsRepository.java                      */
/*                                                                   */
/*  Author      :  THBS                                              */
/*                                                                   */
/*  Date        :  November 03, 2016                                 */
/*                                                                   */
/*  Description :   This class contains all the methods              */
/*                 for the GSRForceGoalsRepository                   */			 									 
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 03, 2016     THBS     1.0        Initial version created */
/*********************************************************************/


package com.thbs.mis.gsr.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrDatEmpGoalBO;

/**
 * 
 * @author THBS
 * @version:1.0
 * @Description: This is GSRForceCloseGoalsRepository.
 */
@Repository
public interface GSRForceCloseGoalsRepository extends GenericRepository<GsrDatEmpGoalBO, Long>{

	@Transactional
    @Modifying
    @Query(force_closegoals_status)
  
    public int updateNominationStatusToZero(int empId);
    
 
    public final static String force_closegoals_status=("UPDATE GsrDatEmpGoalBO n SET n.fkGsrGoalStatus=7 where  n.fkGsrDatEmpId= ?");
	
    public List<GsrDatEmpGoalBO> findByFkGsrDatEmpId(Integer  empId);
	
}
