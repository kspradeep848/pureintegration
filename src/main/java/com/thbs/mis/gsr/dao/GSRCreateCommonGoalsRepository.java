/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrAcceptRatingRepository.java                    */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the GSRAdminConfigRepository             */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrKpiGoalBO;

@Repository
public interface GSRCreateCommonGoalsRepository extends
		GenericRepository<GsrKpiGoalBO, Long>
{

	public final static String DELETE_KPI_GOAL_STATUS = "UPDATE GsrKpiGoalBO n SET n.gsrkpigoalactiveflag='DISABLED' where  n.pkGsrKpiGoalId= ?";

	@Transactional
	@Modifying
	@Query(DELETE_KPI_GOAL_STATUS)
	public int deleteKPIGoals(Short id);
	
	
	public GsrKpiGoalBO findByPkGsrKpiGoalId(short pkGoalId);

}
