/*********************************************************************/
/*                  FILE HEADER                                      */

/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrViewGoalsSpecifiacation.java                            */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  November 28, 2016                                  */
/*                                                                   */
/*  Description :   This class contains all the methods              */
/*                 for the GSRViewPastGoalRepository                         */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/*November 28, 2016     THBS     1.0        Initial version created   */
/*********************************************************************/

package com.thbs.mis.gsr.dao;

import java.util.Date;
import java.util.List;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrDatEmpGoalBO;

public interface GsrViewPastGoalsRepo extends GenericRepository <GsrDatEmpGoalBO, Long> {
	
	/*public Iterable<GsrDatEmpGoalBO> findByFkGsrDatEmpIdAndGsrGoalStartDateBetween(
			Integer fkGsrDatEmpId, Date gsrGoalStartDate, Date gsrGoalEndDate);*/
	
	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpIdAndGsrGoalStartDateGreaterThanEqualAndGsrGoalEndDateLessThanEqual (
			
			Integer fkGsrDatEmpId, Date gsrGoalStartDate, Date gsrGoalEndDate); 
	
	//public Iterable<GsrDatEmpGoalBO> findAll();

	
	
	
	
}
