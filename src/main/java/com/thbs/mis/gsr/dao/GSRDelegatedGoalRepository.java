/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRDelegatedGoalsRepository.java                  */
/*                                                                   */
/*  Author      :  THBS                                              */
/*                                                                   */
/*  Date        :  November 03, 2016                                 */
/*                                                                   */
/*  Description :  This class contains all the methods               */
/*                 for the GSRDelegatedGoalsRepository               */			 									 
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 03, 2016     THBS     1.0        Initial version created */
/*********************************************************************/

package com.thbs.mis.gsr.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrDatEmpDelegatedGoalBO;

/**
 * 
 * @author THBS
 * @version:1.0
 * @Description: This is GSRDelegatedtGoalRepository.
 */
@Repository
public interface GSRDelegatedGoalRepository extends GenericRepository<GsrDatEmpDelegatedGoalBO, Long>
{
	/*public List<GsrDatEmpDelegatedGoalBO> findOne();*/
	
    
    @Transactional
    @Modifying
    
    @Query(update_approve_status)
    public int updateNominationStatusToZero(int empID);
    
 
    public final static String update_approve_status= "UPDATE GsrDatEmpDelegatedGoalBO n SET n.delegationStatus=?1 ,  n.acceptedOrRejectedDate= now() where  n.fkGsrDelegatedEmpId= ?2";
    
    public Iterable<GsrDatEmpDelegatedGoalBO> findAll();
    public GsrDatEmpDelegatedGoalBO findByFkGsrDatEmpGoalId( Integer pkGsrDelegatedGoalId);
    
    public GsrDatEmpDelegatedGoalBO findByFkGsrDelegatedEmpId(int empId);
	public GsrDatEmpDelegatedGoalBO findByPkGsrDelegatedGoalId(
			Integer pkgsrdelegatedgoalid);
	
	//Added by Pratibha for creation of delegated goal
	@Query("SELECT d FROM GsrDatEmpDelegatedGoalBO d WHERE d.fkGsrDatEmpGoalId = :goalId")
	public GsrDatEmpDelegatedGoalBO checkDelegatedGoalByGsrGoalId(@Param("goalId") Integer goalId);
	//End of addition by Pratibha for creation of delegated goal
	
	public void deleteByPkGsrDelegatedGoalIdIn(List<Integer> listOfGoalId);
}
