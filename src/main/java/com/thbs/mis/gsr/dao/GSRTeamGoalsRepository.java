/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRTeamGoalsRepository.java                       */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  November 04, 2016                                 */
/*                                                                   */
/*  Description : This class contains all the methods                */
/*                 for the GSRTeamGoalsRepository                    */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 04, 2016     THBS     1.0        Initial version created */
/*********************************************************************/


package com.thbs.mis.gsr.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrDatEmpGoalBO;

/**
 * 
 * @author THBS
 * @version:1.0
 * @Description: This is GSRTeamGoalsRepository.
 */
@Deprecated
@Repository
public interface GSRTeamGoalsRepository extends GenericRepository<GsrDatEmpGoalBO,Long>{
	public  List<GsrDatEmpGoalBO> findByFkGsrDatMgrIdAndFkGsrGoalStatusAndGsrGoalStartDateGreaterThanEqualAndGsrGoalEndDateLessThanEqual(int mgrId, byte status,Date startdate, Date enddate);
}
