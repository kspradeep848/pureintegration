package com.thbs.mis.gsr.dao;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.gsr.bean.GsrTrainingRequestSearchBean;
import com.thbs.mis.gsr.bo.GsrTrainingRequestBO;

public class GsrTrainingRequestSpecifications { 
	
	public GsrTrainingRequestSpecifications(){} 
	
	public Specification<GsrTrainingRequestBO> getTrainingRequestDatesByDateRangeOrTopicId(GsrTrainingRequestSearchBean dateRangeBean) {
	        return (root, query, cb) -> {
	        	if(dateRangeBean.getStartDate()!=null && dateRangeBean.getEndDate()!=null){
	        		 return cb.between(root.get("gsrTrainingRequestedDate"), dateRangeBean.getStartDate(), dateRangeBean.getEndDate());
	        	}else{
	        		return cb.equal(root.join("masTrainingTopic").get("pkTrainingTopicNameId"), dateRangeBean.getFkGsrTrainingTopicId());
	        	}
/*	         	 return cb.or(cb.between(root.get("gsrTrainingRequestedDate"), dateRangeBean.getStartDate(), dateRangeBean.getEndDate()),
	         			  cb.equal(root.join("masTrainingTopic").get("pkTrainingTopicNameId"), dateRangeBean.getFkGsrTrainingTopicId()));*/
  
	        };
	    }

}
