/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRDisagreeRatingRepository.java                  */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  November 08, 2016                                 */
/*                                                                   */
/*  Description : This class contains all the methods                */
/*                 for the GSRDisagreeratingRepository               */		 									
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 08, 2016     THBS     1.0        Initial version created */
/*********************************************************************/

package com.thbs.mis.gsr.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO;

/**
 * 
 * @author THBS
 * @version:1.0
 * @Description: This is GSRDefaultGoalsRepository.
 */
@Repository
public interface GSRDisagreeRatingRepository extends GenericRepository<GsrDatEmpFinalRatingBO, Long>{
	
	//public final static String disagree_rating_status = ("Update gsr_dat_emp_final_rating set rating_accept_or_reject_date=now(), final_rating_status=3, skip_level_mgr_id= (SELECT fk_emp_domain_mgr_id FROM dat_emp_professional_detail where fk_main_emp_detail_id=?1 where fk_gsr_final_rating_emp_id=?1 and fk_gsr_final_rating_slab_id=?2");
	
	@Transactional
	@Modifying
	
	@Query(nativeQuery = true,value="Update gsr_dat_emp_final_rating set rating_accept_or_reject_date=now(), "
			+ " final_rating_status=3, skip_level_mgr_id = (SELECT fk_emp_reporting_mgr_id FROM dat_emp_professional_detail "
			+ " where fk_main_emp_detail_id = (SELECT fk_emp_reporting_mgr_id FROM dat_emp_professional_detail where "
			+ " fk_main_emp_detail_id =?1) ) where final_rating_status=5 and fk_gsr_final_rating_emp_id=?1 "
			+ " and fk_gsr_final_rating_slab_id=?2")
	public int updateFinalRatingStatusToRejected(int empId, short slabId);

	
}
