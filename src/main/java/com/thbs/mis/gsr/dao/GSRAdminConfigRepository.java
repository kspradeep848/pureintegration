/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRAdminConfigRepository.java                     */
/*                                                                   */
/*  Author      :  THBS			                                     */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the GSRAdminConfigRepository             */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date              Who           Version         Comments     	 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016       THBS            1.0     Initial version created */
/*********************************************************************/
package com.thbs.mis.gsr.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrAdminConfigurationBO;

/**
 * <Description GSRAdminConfigRepository:> This class contains all the 
 * DAO related methods used for the GSR Admin Configuration Repository for Data Base operation.
 * @author THBS
 * @version 1.0
 * @see 
 */
@Repository
public interface GSRAdminConfigRepository extends GenericRepository<GsrAdminConfigurationBO,Long> {
	public List<GsrAdminConfigurationBO> findAll();
	
	
	//Updated by Shyam for a new goal creation
	public GsrAdminConfigurationBO findByFkGsrSlabId(Integer fkGsrSlabId);
	//Updated by Shyam for a new goal creation
	public GsrAdminConfigurationBO findByPkGsrAdminConfigId(Integer adminConfigId);
}
