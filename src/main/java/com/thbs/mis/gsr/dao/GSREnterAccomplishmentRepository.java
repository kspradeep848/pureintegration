/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSREnterAccomplishmentRepository.java             */
/*                                                                   */
/*  Author      :  THBS			                                     */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the GSREnterAccomplishmentRepository     */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date              Who           Version         Comments     	 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016       THBS            1.0     Initial version created */
/*********************************************************************/
package com.thbs.mis.gsr.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrDatEmpGoalBO;

/**
 * <Description GSREnterAccomplishmentRepository:> This class contains all the 
 * DAO related methods used for the GSR Enter Accomplishment Repository for Data Base operation.
 * @author THBS
 * @version 1.0
 * @see 
 */
@Repository
public interface GSREnterAccomplishmentRepository extends GenericRepository<GsrDatEmpGoalBO,Long> {
public Iterable<GsrDatEmpGoalBO> findAll();
	
	public GsrDatEmpGoalBO findByFkGsrDatEmpIdAndGsrGoalStartDateAndGsrGoalEndDate(Integer id, Date sdate, Date endDate);

    public final static String UPDATE_GSR_ENTER_ACCOMPLISHMENT = "UPDATE GsrDatEmpGoalBO n SET n.gsrGoalAccomplishment=?1 , n.gsrGoalSelfRating=?2 where n.fkGsrDatEmpId= ?3";
    
    
    @Transactional
    @Modifying
    @Query(UPDATE_GSR_ENTER_ACCOMPLISHMENT)
    public int updateGsrEnterAccomplishment(String gsrGoalAccomplishment,Float gsrGoalSelfRating,Integer pkGsrDatEmpGoalId);


public GsrDatEmpGoalBO findByPkGsrDatEmpGoalId(Integer pkGsrDatEmpGoalId);
}
