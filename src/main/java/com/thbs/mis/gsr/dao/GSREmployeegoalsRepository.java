/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSREmployeeGoalsRepository.java                   */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  November 02, 2016                                 */
/*                                                                   */
/*  Description :  This class contains all the methods               */
/*                 for the GSREmployeeGoalsRepository                */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 02, 2016     THBS     1.0        Initial version created */
/*********************************************************************/

package com.thbs.mis.gsr.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bean.DelegationResultFromDbBean;
import com.thbs.mis.gsr.bo.GsrDatEmpGoalBO;

/**
 * 
 * @author THBS
 * @version:1.0
 * @Description: This is GSREmployeeGoalsRepository.
 */
@Repository
public interface GSREmployeegoalsRepository extends
		GenericRepository<GsrDatEmpGoalBO, Long>,
		PagingAndSortingRepository<GsrDatEmpGoalBO, Long> {
	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpIdAndGsrGoalStartDateGreaterThanEqualAndGsrGoalEndDateLessThanEqual(
			Integer id, Date startdate, Date end);

	public Integer countByFkGsrSlabIdAndFkGsrDatEmpIdAndFkGsrGoalStatusNotIn(Short slabId, Integer empId, Byte status);
	
	public Integer countByFkGsrSlabIdAndFkGsrDatMgrIdAndFkGsrGoalStatusNotIn(Short slabId, Integer rmId, Byte status);
	
	public Integer countByFkGsrSlabIdAndFkEmpGoalDomainMgrIdAndFkGsrGoalStatusNotIn(Short slabId, Integer dmId, Byte status);
	
	public Integer countByFkGsrSlabIdAndFkEmpGoalBuIdAndFkGsrGoalStatusNotIn(Short slabId, Short buId, Byte status);
	
	public Integer countByFkGsrSlabIdIn(Short[] slabId);
	
	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpIdAndFkGsrDelegatedGoalIdIsNotNull(Integer empId);

	// Added by Kamal Anand
	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpIdAndGsrGoalStartDateGreaterThanEqualAndGsrGoalEndDateLessThanEqualOrderByFkGsrSlabIdDescPkGsrDatEmpGoalIdAscEmpGoalKpiWeightageIdAsc(
			Integer id, Date startdate, Date end);

	@Query("SELECT new com.thbs.mis.gsr.bo.GsrDatEmpGoalBO(goal.pkGsrDatEmpGoalId,goal.fkGsrDatEmpId,goal.fkGsrDatMgrId,"
			+ "concat(per.empFirstName, ' ',per.empLastName) as mgrName,"
			+ "goal.gsrGoalStartDate,goal.gsrGoalEndDate,goal.fkGsrSlabId,sl.gsrSlabName,sl.gsrSlabShortName,"
			+ "sl.gsrSlabYear,goal.gsrGoalWeightage,goal.gsrGoalDescription,goal.gsrGoalMeasurement,goal.gsrGoalAccomplishment,"
			+ "goal.gsrGoalMgrFeedback,goal.gsrGoalSelfRating,goal.gsrGoalMgrRating,goal.fkGsrGoalStatus,"
			+ "sta.gsrGoalStatusName,fin.gsrFinalRating,"
			+ "fin.isOverriddenFlag,fin.overriddenBy,concat(ove.empFirstName, ' ',ove.empLastName) as overriddenByName,fin.modifiedDate) "
			+ "FROM GsrDatEmpGoalBO goal "
			+ "join GsrSlabBO sl on sl.pkGsrSlabId=goal.fkGsrSlabId "
			+ "join GsrMasGoalStatusBO sta on sta.pkGsrGoalStatusId = goal.fkGsrGoalStatus "
			+ "join DatEmpPersonalDetailBO per on per.fkEmpDetailId = goal.fkGsrDatMgrId "
			+ "left join GsrDatEmpFinalRatingBO fin on fin.pkGsrFinalRatingId = goal.fkGsrDatEmpId and fin.fkGsrFinalRatingSlabId = goal.fkGsrSlabId "
			+ "left join DatEmpPersonalDetailBO ove on ove.fkEmpDetailId = fin.overriddenBy "
			+ "where goal.fkGsrDatEmpId = ? and goal.gsrGoalStartDate >= ? and goal.gsrGoalEndDate<= ? order by goal.pkGsrDatEmpGoalId asc")
	public Page<GsrDatEmpGoalBO> getEmpGoals(Integer empId, Date startDate, Date endDate,
			Pageable pageable);	
	
	@Query(name = "GsrDatEmpGoalBO.getEmpGoalsForDelegation")
	public List<DelegationResultFromDbBean> getEmpGoalsForDelegation(
			@Param("empIds") List<Integer> empIdList,
			@Param("delMgrId") Integer delegationMgrId);
	
	@Query(name = "GsrDatEmpGoalBO.getEmpGoalsForDelegationHR")
	public List<DelegationResultFromDbBean> getEmpGoalsForDelegationHR(
			@Param("empIds") List<Integer> empIdList); //Added by Rajesh Kumar for HR Delegate Goals
	
	@Query("SELECT distinct e.fkGsrDatEmpId FROM GsrDatEmpGoalBO e where e.fkGsrDatMgrId = ?")
	public List<Integer> getReporteesEmpIdForRm(Integer mgrId);
	
	// End of Addition by Kamal Anand
	public GsrDatEmpGoalBO findByPkGsrDatEmpGoalId(Integer list);
	
	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpIdAndFkGsrSlabId(Integer empId,Short slabId);
	
	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpIdAndFkGsrDatMgrIdAndFkGsrSlabId(Integer empId,Integer mgrId,Short slabId);
	
	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpIdAndFkGsrDatMgrIdAndFkGsrSlabIdNotIn(Integer empId,Integer mgrId,Short slabId);
	
	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpIdInAndFkGsrDatMgrIdAndFkGsrSlabId(List<Integer> empIds,Integer mgrId,Short slabId);
	
	@Query("SELECT g FROM GsrDatEmpGoalBO g WHERE g.pkGsrDatEmpGoalId = :goalId AND g.fkGsrGoalStatus !=6")
	public GsrDatEmpGoalBO getNonRejectedGoalDetailsByGoalId(@Param("goalId") Integer goalId);
	
	//Added by Rajesh Kumar
	@Query( " SELECT new com.thbs.mis.gsr.bo.GsrDatEmpGoalBO(empGoal.pkGsrDatEmpGoalId,empGoal.fkGsrDatEmpId,empGoal.fkGsrDatMgrId,"
			+ " empGoal.fkGsrGoalStatus,empGoal.fkGsrSlabId,empGoal.gsrDatEmpGoalName,empGoal.gsrGoalAccomplishment,empGoal.gsrGoalClosedBy,"
			+ " empGoal.gsrGoalCreatedBy,empGoal.gsrGoalCreatedDate,empGoal.gsrGoalDescription,empGoal.gsrGoalEndDate,"
			+ " empGoal.gsrGoalMeasurement,empGoal.gsrGoalMgrFeedback,empGoal.gsrGoalMgrRating,empGoal.gsrGoalModifiedBy,"
			+ " empGoal.gsrGoalModifiedDate,empGoal.gsrGoalSelfRating,empGoal.gsrGoalStartDate,empGoal.gsrGoalWeightage,"
			+ " empGoal.empGoalKpiWeightageId,empGoal.fkEmpGoalDomainMgrId,empGoal.fkEmpGoalBuId,empGoal.fkGsrDelegatedGoalId,delegate.pkGsrDelegatedGoalId,"
			+ " delegate.fkMgrIdDelegatedTo,delegate.delegationStatus )"
			+ " FROM GsrDatEmpGoalBO empGoal"
			+ " JOIN GsrDatEmpDelegatedGoalBO delegate "
			+ " ON empGoal.fkGsrDelegatedGoalId = delegate.pkGsrDelegatedGoalId "
			+ " where empGoal.fkGsrDatEmpId = :empId"
			+ " and delegate.delegationStatus = 2"
			+ " and empGoal.gsrGoalStartDate >=:empStartDate"
			+ " and empGoal.gsrGoalEndDate <= :empEndDate"
			+ " and delegate.fkMgrIdDelegatedTo = :delegatedTo "
			+ " order by empGoal.fkGsrSlabId Asc,"
			+ " empGoal.empGoalKpiWeightageId Asc,"
			+ " empGoal.pkGsrDatEmpGoalId Asc")
	public List<GsrDatEmpGoalBO> getDelegatedGoals (@Param("empId") Integer empId, @Param("empStartDate") Date empStartDate , @Param("empEndDate") Date empEndDate, @Param("delegatedTo") Integer delegatedTo);
	//EOA by Rajesh Kumar
	
	//Added by Shyam for proper count of goals in employee dashboard issue #84
	@Query("SELECT count(g) FROM GsrDatEmpGoalBO g WHERE g.fkGsrDatEmpId = :empId AND g.fkGsrSlabId = :currentSlab "
			+ " AND g.fkGsrGoalStatus != 6 AND g.empGoalKpiWeightageId is null ")
	public int getAllEnteredProjectGoalsOfEmpUnderAslab(@Param("currentSlab") Short currentSlab, 
				@Param("empId") Integer empId);
	
	@Query(nativeQuery=true, value = "SELECT count(*) FROM (SELECT * FROM gsr_dat_emp_goal "
			+ " WHERE fk_gsr_slab_id = :currentSlab AND fk_gsr_dat_emp_id = :empId " 
			+ " AND  fk_emp_goal_kpi_weightage_id > 0 AND fk_gsr_goal_status != 6"
			+ " GROUP BY fk_gsr_slab_id, fk_gsr_dat_emp_id, fk_emp_goal_kpi_weightage_id ) as a")
	public int getAllEnteredKpiGoalsOfEmpUnderAslab(@Param("currentSlab") Short currentSlab, 
			@Param("empId") Integer empId);
	//End of addition by Shyam for proper count of goals in employee dashboard issue #84
	
	
	// Added by Pradeep K S for NOtifications  
/*	(Individual View)*/
	public List<GsrDatEmpGoalBO> findByGsrGoalCreatedByAndFkGsrDatEmpIdAndFkGsrGoalStatus(int createdByEmpID, int employeeId, byte status);
	public List<GsrDatEmpGoalBO> findByFkGsrDatMgrIdAndGsrGoalCreatedByAndFkGsrGoalStatus(int rmId,int createdEmpId, byte status);
	public List<GsrDatEmpGoalBO> findByGsrGoalModifiedByAndGsrGoalCreatedByAndFkGsrGoalStatus(int modifiedByEmpID,int createdBy, byte status);
	public List<GsrDatEmpGoalBO> findByGsrGoalCreatedByAndFkGsrGoalStatus(int createdByEmpID, byte status);
	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpIdAndFkGsrGoalStatus(int empID, byte status);
	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpIdAndGsrGoalCreatedByAndFkGsrGoalStatus(int empId,int createdByEmpID, byte status);

	
	/* (Consolidated View)*/
	public List<GsrDatEmpGoalBO> findByFkGsrDatMgrIdAndFkGsrGoalStatusAndGsrGoalCreatedDateBetween(int createdByEmpID,byte status, Date sDate, Date eDate);

	public List<GsrDatEmpGoalBO> findByFkGsrDatMgrIdAndFkGsrGoalStatusAndGsrGoalModifiedBy(int createdByEmpID,byte status,int modifiedBy);
	
	public List<GsrDatEmpGoalBO>  findByFkGsrDatEmpIdAndFkGsrDatMgrIdAndGsrGoalCreatedByAndFkGsrGoalStatus(int empId, int rmId, int createdById, byte status);

	public List<GsrDatEmpGoalBO> findByFkGsrDatEmpIdAndGsrGoalCreatedByAndGsrGoalModifiedByAndFkGsrGoalStatus(int empId,int createdByEmpId, int modifiedByEmpID, byte status);
	
	public List<GsrDatEmpGoalBO> findByGsrGoalCreatedByNotAndFkGsrDatMgrIdAndFkGsrGoalStatus(int createdBy,int mgrId,byte status);
	
	public List<GsrDatEmpGoalBO> findByGsrGoalCreatedByNotAndFkGsrDatEmpIdAndFkGsrGoalStatus(int createdBy, int empId,byte status);
	
	public List<GsrDatEmpGoalBO> findByGsrGoalCreatedByInAndFkGsrDatEmpIdAndFkGsrGoalStatus(List<Integer> createdBy,int empId,byte status);
	
	
	
}
