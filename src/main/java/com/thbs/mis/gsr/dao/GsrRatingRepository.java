package com.thbs.mis.gsr.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO;

@Repository
public interface GsrRatingRepository extends
		GenericRepository<GsrDatEmpFinalRatingBO, Long>,
		JpaSpecificationExecutor<GsrDatEmpFinalRatingBO> {

	public Set<GsrDatEmpFinalRatingBO> findByFkGsrFinalRatingEmpId(int empId);
	
	public List<GsrDatEmpFinalRatingBO> findByFkGsrFinalRatingEmpIdAndFkGsrFinalRatingSlabId(int empId, short slabId);

	@Query("SELECT new com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO(final_rating.gsrFinalRating, slab) "		
			+ " from GsrDatEmpFinalRatingBO final_rating "
			+ " INNER JOIN GsrSlabBO slab ON final_rating.fkGsrFinalRatingSlabId = slab.pkGsrSlabId "
			+ " WHERE final_rating.fkGsrFinalRatingEmpId IN (?1) AND final_rating.fkGsrFinalRatingSlabId IN (?2)")
	public List<GsrDatEmpFinalRatingBO> getAllEmpRatingsForRm(List<Integer> listOfEmp, List<Short> slabs);
	
	public List<GsrDatEmpFinalRatingBO> findByFkGsrFinalRatingEmpIdAndFkGsrFinalRatingSlabIdAndFkGsrFinalRatingMgrId(
			Integer fkGsrDatEmpId, Short fkGsrSlabId, Integer fkGsrDatMgrId);
	
	@Query("SELECT new com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO(final_rating.gsrFinalRating, slab) "		
			+ " from GsrDatEmpFinalRatingBO final_rating "
			+ " INNER JOIN GsrSlabBO slab ON final_rating.fkGsrFinalRatingSlabId = slab.pkGsrSlabId "
			+ " WHERE final_rating.fkGsrFinalRatingSlabId IN (?1)")
	public List<GsrDatEmpFinalRatingBO> getAllEmpRatingsForHr(List<Short> slabs);

}
