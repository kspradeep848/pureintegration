/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRRequestRepository.java                         */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  October 28, 2016                                  */
/*                                                                   */
/*  Description :   This class contains all the methods              */
/*                 for the GSRRequestRepository                      */		 									
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/*October 28, 2016     THBS     1.0        Initial version created   */
/*********************************************************************/

package com.thbs.mis.gsr.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.training.bo.TrainingDatRequestDetailBO;


/**
 * <Description ExampleRepository:> TODO
 * @author THBS
 * @version 1.0
 * @see 
 */
@Repository
@Transactional(readOnly = true)
public interface GsrRequestRepository extends GenericRepository<TrainingDatRequestDetailBO,Long>
{
	//View Training Request by Training Manager on Criteria base
	
	
	
	
	

}
