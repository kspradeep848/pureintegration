/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrAcceptRatingRepository.java                    */
/*                                                                   */
/*  Author      :  THBS			                                     */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the GsrAcceptRatingRepository            */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date              Who           Version         Comments     	 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016       THBS            1.0     Initial version created */
/*********************************************************************/
package com.thbs.mis.gsr.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO;

/**
 * <Description GsrAcceptRatingRepository:> This class contains all the 
 * DAO related methods used for the Gsr Accept Rating Repository for Data Base operation.
 * @author THBS
 * @version 1.0
 * @see 
 */
@Repository
public interface GsrAcceptRatingRepository extends GenericRepository<GsrDatEmpFinalRatingBO,Long>{
	public List<GsrDatEmpFinalRatingBO> findAll();
	public GsrDatEmpFinalRatingBO findByPkGsrFinalRatingId(Integer pkGsrFinalRatingId);
}
