package com.thbs.mis.gsr.dao;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO;
import com.thbs.mis.gsr.bo.GsrSlabBO;
import com.thbs.mis.gsr.constants.GsrConstants;

public class GsrRatingSpecification {

	public Specification<GsrDatEmpFinalRatingBO> getEmployeesPerformanceRating(
			Integer empId, List<Short> slabId) {
		return (root, query, cb) -> {

			return cb.and(cb.equal(root.get("fkGsrFinalRatingEmpId"), empId),
					root.get("fkGsrFinalRatingSlabId").in(slabId));

		};
	}

	

	public Specification<GsrDatEmpFinalRatingBO> getAllEmpRatingsForRm(
			List<Integer> rmId, Short slabId) {
		return (root, query, cb) -> {
			
			return cb.and(cb.equal(root.get("fkGsrFinalRatingSlabId"), slabId),
					root.get("fkGsrFinalRatingEmpId").in(rmId));

		};

	}

	public Specification<GsrDatEmpFinalRatingBO> getAllEmpRatingsForHr(
			Short slabId) {
		return (root, query, cb) -> {
			
			return cb.equal(root.get("fkGsrFinalRatingSlabId"), slabId);

		};

	}

	public Specification<GsrDatEmpFinalRatingBO> getAllEmpRatings(
			List<Integer> rmId) {
		return (root, query, cb) -> {

			return root.get("fkGsrFinalRatingEmpId").in(rmId);

		};

	}

	public static Specification<GsrSlabBO> getCurrentSlab() {
		return (root, query, cb) -> {

			
		
			/*return cb.and((cb.lessThanOrEqualTo(root.get("gsrSlabStartDate"),
					new Date())), (cb.greaterThanOrEqualTo(
					root.get("gsrSlabEndDate"), new Date())),cb.equal(root.get("gsrSlabEnabled"),"ENABLED"));*/
			
			return cb.and((cb.between(cb.currentDate(),root.get("gsrSlabStartDate"), root.get("gsrSlabEndDate"))),cb.equal(root.get("gsrSlabEnabled"),"ENABLED"));
		};
	}
	
	
	public Specification<GsrDatEmpFinalRatingBO> getAllEmpGsrDisagreeForBu(List<Integer> empId, List<Short> slabId){
		return (root, query, cb) -> {
			
	
			return cb.and(root.get("fkGsrFinalRatingSlabId").in(slabId),
					root.get("fkGsrFinalRatingEmpId").in(empId),root.get("finalRatingStatus").in(GsrConstants.STATUS_FINAL_RATING_REJECTED,GsrConstants.STATUS_FINAL_RATING_RESOLVED));
			
		};
		
	}
	

	public Specification<GsrDatEmpFinalRatingBO> getAllEmpGsrDisagree(List<Short> slabId){
		return (root, query, cb) -> {
			return cb.and(root.get("fkGsrFinalRatingSlabId").in(slabId),root.get("finalRatingStatus").in(GsrConstants.STATUS_FINAL_RATING_REJECTED,GsrConstants.STATUS_FINAL_RATING_RESOLVED));
			
		};
		
	}
}
