package com.thbs.mis.gsr.dao;

import java.util.List;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrMasGoalStatusBO;

public interface GsrGoalStatusRepository extends GenericRepository<GsrMasGoalStatusBO, Long>
{

	public List<GsrMasGoalStatusBO> findAll();
	
	//Added by Kamal Anand
	public GsrMasGoalStatusBO findByPkGsrGoalStatusId(Byte goalStatusId);
	//End of Addition by Kamal Anand
}
