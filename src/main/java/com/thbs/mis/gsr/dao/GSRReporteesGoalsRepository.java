/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRReporteesGoalsRepository.java                  */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  November 07, 2016                                 */
/*                                                                   */
/*  Description :   This class contains all the methods              */
/*                 for the GSRReporteesGoalsRepository               */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 07, 2016     THBS     1.0        Initial version created */
/*********************************************************************/

package com.thbs.mis.gsr.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrDatEmpGoalBO;

/**
 * 
 * @author THBS
 * @version:1.0
 * @Description: This is GSRReporteesGoalsRepository.
 */
@Repository
public interface GSRReporteesGoalsRepository extends
		GenericRepository<GsrDatEmpGoalBO, Long>,JpaSpecificationExecutor<GsrDatEmpGoalBO>
{
	public List<GsrDatEmpGoalBO> findByFkGsrDatMgrId(int mgrId);

	// Added by Kamal Anand
	public List<GsrDatEmpGoalBO> findByFkGsrDatMgrIdOrderByFkGsrSlabIdDescFkGsrDatEmpIdAscEmpGoalKpiWeightageIdAscPkGsrDatEmpGoalIdAsc(
			int mgrId);
	
	public List<GsrDatEmpGoalBO> findAll();
	// End of Addition by Kamal Anand
}
