package com.thbs.mis.gsr.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.gsr.bo.GsrTrainingRequestBO;

@Repository
public interface GSRTrainingRequestRepository extends GenericRepository<GsrTrainingRequestBO,Long>, JpaSpecificationExecutor<GsrTrainingRequestBO>{
	
	public List<GsrTrainingRequestBO> findAll();
	
	public List<GsrTrainingRequestBO> findByGsrTrainingRequestedDateBetween
		(Date startDate,Date endDate);


}
