package com.thbs.mis.gsr.bean;

import java.util.List;

public class PreNormalizedRatingBean {

	private String year;
	private List<EmpPreNormalizedRatingOutputBean> slabs;

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public List<EmpPreNormalizedRatingOutputBean> getSlabs() {
		return slabs;
	}

	public void setSlabs(List<EmpPreNormalizedRatingOutputBean> slabs) {
		this.slabs = slabs;
	}

	@Override
	public String toString() {
		return "PreNormalizedRatingBean [year=" + year + ", slabs=" + slabs
				+ "]";
	}

}
