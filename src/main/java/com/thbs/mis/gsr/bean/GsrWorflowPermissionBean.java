/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrWorflowPermissionBean.java                     */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  12-Dec-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 12-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.gsr.bean;

import java.util.Date;
import java.util.List;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_Slab;
import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class GsrWorflowPermissionBean {

	private Integer pkGsrWorkflowPermissionId;

	@NumberFormat(style = Style.NUMBER, pattern = "Employee should be numeric value")
	@Min(1)
	@Employee_Id_Not_Found
	@NotNull(message = "fkGsrWorkflowPermissionEmpId field can not be null")
	private Integer fkGsrWorkflowPermissionEmpId;

	@NumberFormat(style = Style.NUMBER, pattern = "Slab Id should be numeric value")
	@Min(1)
	@NotNull(message = "Slab ID field can not be null")
	@GSR_Id_Not_Found_Slab
	private short fkGsrWorkflowPermissionSlabId;

	private String enableGoalsCreation;

	private String enableEnterAccomplishments;

	private String enableFeedback;

	@NumberFormat(style = Style.NUMBER, pattern = "Requested by Employee Id should be numeric value")
	@Min(1)
	@Employee_Id_Not_Found
	@NotNull(message = "WorkflowPermissionRequestedBy field cannot be null")
	private Integer gsrWorkflowPermissionRequestedBy;

	@NotNull(message = "WorkflowPermissionRequestedDate field cannot be null")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date gsrWorkflowPermissionRequestedDate;

	@NotNull(message = "WorkflowPermissionStartDate field cannot be null")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date gsrWorkflowPermissionStartDate;

	@NotNull(message = "PermissionEndDate field cannot be null")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date gsrWorkflowPermissionEndDate;

	@NumberFormat(style = Style.NUMBER, pattern = "Permission type should be numeric value")
	@Min(1)
	@Max(3)
	@NotNull(message = "PermissionType field cannot be null")
	private Integer permissionType;
	
	private String empName;

	public Integer getPkGsrWorkflowPermissionId() {
		return pkGsrWorkflowPermissionId;
	}

	public void setPkGsrWorkflowPermissionId(Integer pkGsrWorkflowPermissionId) {
		this.pkGsrWorkflowPermissionId = pkGsrWorkflowPermissionId;
	}

	public Integer getFkGsrWorkflowPermissionEmpId() {
		return fkGsrWorkflowPermissionEmpId;
	}

	public void setFkGsrWorkflowPermissionEmpId(Integer fkGsrWorkflowPermissionEmpId) {
		this.fkGsrWorkflowPermissionEmpId = fkGsrWorkflowPermissionEmpId;
	}

	public short getFkGsrWorkflowPermissionSlabId() {
		return fkGsrWorkflowPermissionSlabId;
	}

	public void setFkGsrWorkflowPermissionSlabId(short fkGsrWorkflowPermissionSlabId) {
		this.fkGsrWorkflowPermissionSlabId = fkGsrWorkflowPermissionSlabId;
	}

	public String getEnableGoalsCreation() {
		return enableGoalsCreation;
	}

	public void setEnableGoalsCreation(String enableGoalsCreation) {
		this.enableGoalsCreation = enableGoalsCreation;
	}

	public String getEnableEnterAccomplishments() {
		return enableEnterAccomplishments;
	}

	public void setEnableEnterAccomplishments(String enableEnterAccomplishments) {
		this.enableEnterAccomplishments = enableEnterAccomplishments;
	}

	public String getEnableFeedback() {
		return enableFeedback;
	}

	public void setEnableFeedback(String enableFeedback) {
		this.enableFeedback = enableFeedback;
	}

	public Integer getGsrWorkflowPermissionRequestedBy() {
		return gsrWorkflowPermissionRequestedBy;
	}

	public void setGsrWorkflowPermissionRequestedBy(
			Integer gsrWorkflowPermissionRequestedBy) {
		this.gsrWorkflowPermissionRequestedBy = gsrWorkflowPermissionRequestedBy;
	}

	public Date getGsrWorkflowPermissionRequestedDate() {
		return gsrWorkflowPermissionRequestedDate;
	}

	public void setGsrWorkflowPermissionRequestedDate(
			Date gsrWorkflowPermissionRequestedDate) {
		this.gsrWorkflowPermissionRequestedDate = gsrWorkflowPermissionRequestedDate;
	}

	public Date getGsrWorkflowPermissionStartDate() {
		return gsrWorkflowPermissionStartDate;
	}

	public void setGsrWorkflowPermissionStartDate(
			Date gsrWorkflowPermissionStartDate) {
		this.gsrWorkflowPermissionStartDate = gsrWorkflowPermissionStartDate;
	}

	public Date getGsrWorkflowPermissionEndDate() {
		return gsrWorkflowPermissionEndDate;
	}

	public void setGsrWorkflowPermissionEndDate(Date gsrWorkflowPermissionEndDate) {
		this.gsrWorkflowPermissionEndDate = gsrWorkflowPermissionEndDate;
	}

	public Integer getPermissionType() {
		return permissionType;
	}

	public void setPermissionType(Integer permissionType) {
		this.permissionType = permissionType;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	@Override
	public String toString() {
		return "GsrWorflowPermissionBean [pkGsrWorkflowPermissionId="
				+ pkGsrWorkflowPermissionId + ", fkGsrWorkflowPermissionEmpId="
				+ fkGsrWorkflowPermissionEmpId
				+ ", fkGsrWorkflowPermissionSlabId="
				+ fkGsrWorkflowPermissionSlabId + ", enableGoalsCreation="
				+ enableGoalsCreation + ", enableEnterAccomplishments="
				+ enableEnterAccomplishments + ", enableFeedback="
				+ enableFeedback + ", gsrWorkflowPermissionRequestedBy="
				+ gsrWorkflowPermissionRequestedBy
				+ ", gsrWorkflowPermissionRequestedDate="
				+ gsrWorkflowPermissionRequestedDate
				+ ", gsrWorkflowPermissionStartDate="
				+ gsrWorkflowPermissionStartDate
				+ ", gsrWorkflowPermissionEndDate="
				+ gsrWorkflowPermissionEndDate + ", permissionType="
				+ permissionType + ", empName=" + empName + "]";
	}
	

	
	
}
