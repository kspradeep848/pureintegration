package com.thbs.mis.gsr.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.thbs.mis.framework.customValidation.annotation.common.MIS_Id_Not_Found_Employee;
import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_Slab;

public class BUHeadreporteesgoalinputbean {
	
	@NotNull(message="BU Head Emp Id cannot be null")
	@Min(1)
	@MIS_Id_Not_Found_Employee
	private Integer buHeadEmpid;
	
	@NotNull(message="GSR slabId  cannot be null")
	@Min(1)
	@GSR_Id_Not_Found_Slab
	private Short slabId;

	public Integer getBuHeadEmpid() {
		return buHeadEmpid;
	}

	public void setBuHeadEmpid(Integer buHeadEmpid) {
		this.buHeadEmpid = buHeadEmpid;
	}

	public Short getSlabId() {
		return slabId;
	}

	public void setSlabId(Short slabId) {
		this.slabId = slabId;
	}

	@Override
	public String toString() {
		return "BUHeadreporteesgoalinputbean [buHeadEmpid=" + buHeadEmpid
				+ ", slabId=" + slabId + "]";
	}
	
	
	
	
}
