/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  ViewEmployeeGoalsInputBean.java                   */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  March 29, 2017                                    */
/*                                                                   */
/*  Description :  This class contains input fields required for     */
/*                 view GSR Employee goal details	 		         */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 02, 2016     THBS     1.0        Initial version created */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

import java.util.List;

import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_GoalStatus;
import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_Slab;
import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class ViewEmployeeGoalsInputBean
{
	@Employee_Id_Not_Found
	private Integer mgrId;
	
	private List<Integer> empIdList;
	
	@GSR_Id_Not_Found_GoalStatus
	private Byte goalStatus;
	
	@GSR_Id_Not_Found_Slab
	private Short slabId;

	public Integer getMgrId()
	{
		return mgrId;
	}

	public void setMgrId(Integer mgrId)
	{
		this.mgrId = mgrId;
	}

	public List<Integer> getEmpIdList()
	{
		return empIdList;
	}

	public void setEmpIdList(List<Integer> empIdList)
	{
		this.empIdList = empIdList;
	}

	public Byte getGoalStatus()
	{
		return goalStatus;
	}

	public void setGoalStatus(Byte goalStatus)
	{
		this.goalStatus = goalStatus;
	}

	public Short getSlabId()
	{
		return slabId;
	}

	public void setSlabId(Short slabId)
	{
		this.slabId = slabId;
	}

	@Override
	public String toString()
	{
		return "ViewEmployeeGoalsInputBean [mgrId=" + mgrId
				+ ", empIdList=" + empIdList + ", goalStatus="
				+ goalStatus + ", slabId=" + slabId + "]";
	}
}
