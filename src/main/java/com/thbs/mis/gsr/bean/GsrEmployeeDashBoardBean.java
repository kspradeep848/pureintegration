package com.thbs.mis.gsr.bean;

import java.util.Date;

public class GsrEmployeeDashBoardBean {

	private Integer mgrId;
	private String mgrName;
	private Integer TotalGoalSetForCurrentSlab;
	private Date gsrWindowEndDate;
	private String workFlowName;
	
	
	public Integer getMgrId() {
		return mgrId;
	}
	public void setMgrId(Integer mgrId) {
		this.mgrId = mgrId;
	}
	public String getMgrName() {
		return mgrName;
	}
	public void setMgrName(String mgrName) {
		this.mgrName = mgrName;
	}
	
	public Integer getTotalGoalSetForCurrentSlab() {
		return TotalGoalSetForCurrentSlab;
	}
	public void setTotalGoalSetForCurrentSlab(Integer totalGoalSetForCurrentSlab) {
		TotalGoalSetForCurrentSlab = totalGoalSetForCurrentSlab;
	}
	public Date getGsrWindowEndDate() {
		return gsrWindowEndDate;
	}
	public void setGsrWindowEndDate(Date gsrWindowEndDate) {
		this.gsrWindowEndDate = gsrWindowEndDate;
	}
	public String getWorkFlowName() {
		return workFlowName;
	}
	public void setWorkFlowName(String workFlowName) {
		this.workFlowName = workFlowName;
	}
	@Override
	public String toString() {
		return "EmployeeDashBoardBean [mgrId=" + mgrId + ", mgrName=" + mgrName
				+ ", TotalGoalSetForCurrentSlab=" + TotalGoalSetForCurrentSlab
				+ ", gsrWindowEndDate=" + gsrWindowEndDate + ", workFlowName="
				+ workFlowName + "]";
	}
	
	
	
}
