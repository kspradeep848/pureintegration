package com.thbs.mis.gsr.bean;

public class GsrGoalSetStatusAllEmployeeBean {

	Integer employeeCount;
	Short slabId;
	Integer statusGoalClosedCount;
	Integer statusNotGoalClosedCount;
	Integer statusGoalSetCount;
	Integer statusNotGoalSetCount;
	String slabDuration;
	String slabShortName;
	String status;


	public Integer getEmployeeCount() {
		return employeeCount;
	}

	public void setEmployeeCount(Integer employeeCount) {
		this.employeeCount = employeeCount;
	}

	public Short getSlabId() {
		return slabId;
	}

	public void setSlabId(Short slabId) {
		this.slabId = slabId;
	}

	public Integer getStatusGoalClosedCount() {
		return statusGoalClosedCount;
	}

	public void setStatusGoalClosedCount(Integer statusGoalClosedCount) {
		this.statusGoalClosedCount = statusGoalClosedCount;
	}

	public Integer getStatusNotGoalClosedCount() {
		return statusNotGoalClosedCount;
	}

	public void setStatusNotGoalClosedCount(Integer statusNotGoalClosedCount) {
		this.statusNotGoalClosedCount = statusNotGoalClosedCount;
	}

	public Integer getStatusGoalSetCount() {
		return statusGoalSetCount;
	}

	public void setStatusGoalSetCount(Integer statusGoalSetCount) {
		this.statusGoalSetCount = statusGoalSetCount;
	}

	public Integer getStatusNotGoalSetCount() {
		return statusNotGoalSetCount;
	}

	public void setStatusNotGoalSetCount(Integer statusNotGoalSetCount) {
		this.statusNotGoalSetCount = statusNotGoalSetCount;
	}

	public String getSlabDuration() {
		return slabDuration;
	}

	public void setSlabDuration(String slabDuration) {
		this.slabDuration = slabDuration;
	}

	public String getSlabShortName() {
		return slabShortName;
	}

	public void setSlabShortName(String slabShortName) {
		this.slabShortName = slabShortName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


}
