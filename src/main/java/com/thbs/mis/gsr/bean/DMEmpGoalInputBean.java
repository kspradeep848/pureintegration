package com.thbs.mis.gsr.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.thbs.mis.framework.customValidation.annotation.common.MIS_Id_Not_Found_Employee;
import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_Slab;



public class DMEmpGoalInputBean {
  
	    @NotNull(message="Domain manger id cannot be null")
	    @Min(1)
	    @MIS_Id_Not_Found_Employee
		private Integer domainMgrId;
		
	
	    @NotNull(message="Slab ID cannot be null")
	    @Min(1)
	    @GSR_Id_Not_Found_Slab
	    private short slabID;
	    

		public short getSlabID() {
			return slabID;
		}


		public void setSlabID(short slabID) {
			this.slabID = slabID;
		}


		public Integer getDomainMgrId() {
			return domainMgrId;
		}


		public void setDomainMgrId(Integer domainMgrId) {
			this.domainMgrId = domainMgrId;
		}


		@Override
		public String toString() {
			return "DMEmpGoalInputBean [domainMgrId=" + domainMgrId
					+ ", slabID=" + slabID + "]";
		}
		
		

		
}
