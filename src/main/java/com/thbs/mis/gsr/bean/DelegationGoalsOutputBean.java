package com.thbs.mis.gsr.bean;

import java.util.Date;

public class DelegationGoalsOutputBean {

	private Integer goalId;
	private String goalName;
	private Date goalStartDate;
	private Date goalEndDate;
	private Byte goalWeightage;
	private String goalDescription;
	private String goalMeasurement;
	private String goalAccomplishment;
	private String mgrFeedback;
	private Float goalSelfRating;
	private Float goalMgrRating;
	private Byte goalStatusId;
	private String goalStatusName;
	private Short kpiGoalId;
	private Short kpiGoalWeightage;
	private Integer delegatedGoalId;
	private Integer delegatedFrom;
	private String delegatedFromName;
	private Integer delegatedTo;
	private String delegatedToName;
	private Byte delegatedStatus;
	private Date delegatedDate;
	private Date acceptedOrRejectedDate;
	private Integer delegatedBy;
	private String delegatedByName;
	private String delegationStatusName;

	public String getDelegationStatusName() {
		return delegationStatusName;
	}

	public void setDelegationStatusName(String delegationStatusName) {
		this.delegationStatusName = delegationStatusName;
	}

	public Integer getGoalId() {
		return goalId;
	}

	public void setGoalId(Integer goalId) {
		this.goalId = goalId;
	}

	public String getGoalName() {
		return goalName;
	}

	public void setGoalName(String goalName) {
		this.goalName = goalName;
	}

	public Date getGoalStartDate() {
		return goalStartDate;
	}

	public void setGoalStartDate(Date goalStartDate) {
		this.goalStartDate = goalStartDate;
	}

	public Date getGoalEndDate() {
		return goalEndDate;
	}

	public void setGoalEndDate(Date goalEndDate) {
		this.goalEndDate = goalEndDate;
	}

	public Byte getGoalWeightage() {
		return goalWeightage;
	}

	public void setGoalWeightage(Byte goalWeightage) {
		this.goalWeightage = goalWeightage;
	}

	public String getGoalDescription() {
		return goalDescription;
	}

	public void setGoalDescription(String goalDescription) {
		this.goalDescription = goalDescription;
	}

	public String getGoalMeasurement() {
		return goalMeasurement;
	}

	public void setGoalMeasurement(String goalMeasurement) {
		this.goalMeasurement = goalMeasurement;
	}

	public String getGoalAccomplishment() {
		return goalAccomplishment;
	}

	public void setGoalAccomplishment(String goalAccomplishment) {
		this.goalAccomplishment = goalAccomplishment;
	}

	public String getMgrFeedback() {
		return mgrFeedback;
	}

	public void setMgrFeedback(String mgrFeedback) {
		this.mgrFeedback = mgrFeedback;
	}

	public Float getGoalSelfRating() {
		return goalSelfRating;
	}

	public void setGoalSelfRating(Float goalSelfRating) {
		this.goalSelfRating = goalSelfRating;
	}

	public Float getGoalMgrRating() {
		return goalMgrRating;
	}

	public void setGoalMgrRating(Float goalMgrRating) {
		this.goalMgrRating = goalMgrRating;
	}

	public Byte getGoalStatusId() {
		return goalStatusId;
	}

	public void setGoalStatusId(Byte goalStatusId) {
		this.goalStatusId = goalStatusId;
	}

	public String getGoalStatusName() {
		return goalStatusName;
	}

	public void setGoalStatusName(String goalStatusName) {
		this.goalStatusName = goalStatusName;
	}

	public Short getKpiGoalId() {
		return kpiGoalId;
	}

	public void setKpiGoalId(Short kpiGoalId) {
		this.kpiGoalId = kpiGoalId;
	}

	public Short getKpiGoalWeightage() {
		return kpiGoalWeightage;
	}

	public void setKpiGoalWeightage(Short kpiGoalWeightage) {
		this.kpiGoalWeightage = kpiGoalWeightage;
	}

	public Integer getDelegatedGoalId() {
		return delegatedGoalId;
	}

	public void setDelegatedGoalId(Integer delegatedGoalId) {
		this.delegatedGoalId = delegatedGoalId;
	}

	public Integer getDelegatedFrom() {
		return delegatedFrom;
	}

	public void setDelegatedFrom(Integer delegatedFrom) {
		this.delegatedFrom = delegatedFrom;
	}

	public String getDelegatedFromName() {
		return delegatedFromName;
	}

	public void setDelegatedFromName(String delegatedFromName) {
		this.delegatedFromName = delegatedFromName;
	}

	public Integer getDelegatedTo() {
		return delegatedTo;
	}

	public void setDelegatedTo(Integer delegatedTo) {
		this.delegatedTo = delegatedTo;
	}

	public String getDelegatedToName() {
		return delegatedToName;
	}

	public void setDelegatedToName(String delegatedToName) {
		this.delegatedToName = delegatedToName;
	}

	public Byte getDelegatedStatus() {
		return delegatedStatus;
	}

	public void setDelegatedStatus(Byte delegatedStatus) {
		this.delegatedStatus = delegatedStatus;
	}

	public Date getDelegatedDate() {
		return delegatedDate;
	}

	public void setDelegatedDate(Date delegatedDate) {
		this.delegatedDate = delegatedDate;
	}

	public Date getAcceptedOrRejectedDate() {
		return acceptedOrRejectedDate;
	}

	public void setAcceptedOrRejectedDate(Date acceptedOrRejectedDate) {
		this.acceptedOrRejectedDate = acceptedOrRejectedDate;
	}

	public Integer getDelegatedBy() {
		return delegatedBy;
	}

	public void setDelegatedBy(Integer delegatedBy) {
		this.delegatedBy = delegatedBy;
	}

	public String getDelegatedByName() {
		return delegatedByName;
	}

	public void setDelegatedByName(String delegatedByName) {
		this.delegatedByName = delegatedByName;
	}

	@Override
	public String toString() {
		return "DelegationGoalsOutputBean [goalId=" + goalId + ", goalName="
				+ goalName + ", goalStartDate=" + goalStartDate
				+ ", goalEndDate=" + goalEndDate + ", goalWeightage="
				+ goalWeightage + ", goalDescription=" + goalDescription
				+ ", goalMeasurement=" + goalMeasurement
				+ ", goalAccomplishment=" + goalAccomplishment
				+ ", mgrFeedback=" + mgrFeedback + ", goalSelfRating="
				+ goalSelfRating + ", goalMgrRating=" + goalMgrRating
				+ ", goalStatusId=" + goalStatusId + ", goalStatusName="
				+ goalStatusName + ", kpiGoalId=" + kpiGoalId
				+ ", kpiGoalWeightage=" + kpiGoalWeightage
				+ ", delegatedGoalId=" + delegatedGoalId + ", delegatedFrom="
				+ delegatedFrom + ", delegatedFromName=" + delegatedFromName
				+ ", delegatedTo=" + delegatedTo + ", delegatedToName="
				+ delegatedToName + ", delegatedStatus=" + delegatedStatus
				+ ", delegatedDate=" + delegatedDate
				+ ", acceptedOrRejectedDate=" + acceptedOrRejectedDate
				+ ", delegatedBy=" + delegatedBy + ", delegatedByName="
				+ delegatedByName + ", delegationStatusName="
				+ delegationStatusName + "]";
	}

}
