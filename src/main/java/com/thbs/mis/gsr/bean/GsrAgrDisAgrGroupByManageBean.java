package com.thbs.mis.gsr.bean;


public class GsrAgrDisAgrGroupByManageBean {

	private int mgrId;
	private String mgrName;
	int noOfEmpDisAgree;

	public int getMgrId() {
		return mgrId;
	}

	public void setMgrId(int mgrId) {
		this.mgrId = mgrId;
	}

	public String getMgrName() {
		return mgrName;
	}

	public void setMgrName(String mgrName) {
		this.mgrName = mgrName;
	}

	public int getNoOfEmpDisAgree() {
		return noOfEmpDisAgree;
	}

	public void setNoOfEmpDisAgree(int noOfEmpDisAgree) {
		this.noOfEmpDisAgree = noOfEmpDisAgree;
	}

	@Override
	public String toString() {
		return "GsrAgrDisAgrGroupByManageBean [mgrId=" + mgrId + ", mgrName="
				+ mgrName + ", noOfEmpDisAgree=" + noOfEmpDisAgree + "]";
	}

	
}
