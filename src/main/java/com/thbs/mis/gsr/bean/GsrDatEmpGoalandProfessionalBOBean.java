package com.thbs.mis.gsr.bean;

import java.util.Date;

public class GsrDatEmpGoalandProfessionalBOBean {
	

	private Integer fkMainEmpDetailId;
	

	public Integer getFkMainEmpDetailId() {
		return fkMainEmpDetailId;
	}
	public void setFkMainEmpDetailId(Integer fkMainEmpDetailId) {
		this.fkMainEmpDetailId = fkMainEmpDetailId;
	}
	
	
	private Byte fkGsrGoalStatus;
	private Date gsrGoalStartDate;
	private Date gsrGoalEndDate;
	


	public Byte getFkGsrGoalStatus() {
		return fkGsrGoalStatus;
	}
	public void setFkGsrGoalStatus(Byte fkGsrGoalStatus) {
		this.fkGsrGoalStatus = fkGsrGoalStatus;
	}
	public Date getGsrGoalStartDate() {
		return gsrGoalStartDate;
	}
	public void setGsrGoalStartDate(Date gsrGoalStartDate) {
		this.gsrGoalStartDate = gsrGoalStartDate;
	}
	public Date getGsrGoalEndDate() {
		return gsrGoalEndDate;
	}
	public void setGsrGoalEndDate(Date gsrGoalEndDate) {
		this.gsrGoalEndDate = gsrGoalEndDate;
	}
	
	
	
	@Override
	public String toString() {
		return "GsrDatEmpGoalandProfessionalBOBean [fkMainEmpDetailId="
				+ fkMainEmpDetailId + ", fkGsrGoalStatus=" + fkGsrGoalStatus
				+ ", gsrGoalStartDate=" + gsrGoalStartDate
				+ ", gsrGoalEndDate=" + gsrGoalEndDate + "]";
	}
	
	
	
	
	
		
	}
	
	
	
	
	
	
	
	


