package com.thbs.mis.gsr.bean;


import java.util.Date;


import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_Slab;
import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;



public class DatEmpProfDetailandGsrEmpGoalBean  {
	
	//@NotNull(message="Domain manger id cannot be null")
	@Employee_Id_Not_Found
	private Integer fkEmpDomainMgrId;
	
	@Employee_Id_Not_Found
	 private Integer fkMainEmpDetailId;
	 
	/*@NotNull(message="Goal Status cannot be null")
	@Min(1)
	@Max(7)*/
	private Byte fkGsrGoalStatus;
	
	@NotNull(message="Slab id cannot be null")
    @GSR_Id_Not_Found_Slab
	private Short fkGsrSlabId;
	
	
	/*@NotNull(message="Goal end date cannot be null")*/
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date gsrGoalEndDate;
	
	
	
	/*@NotNull(message="Goal start date cannot be null")
	@DateTimeFormat(style = "yyyy-MM-dd")*/
	private Date gsrGoalStartDate;
	

	
	
	public Integer getFkMainEmpDetailId() {
		return fkMainEmpDetailId;
	}
	public void setFkMainEmpDetailId(Integer fkMainEmpDetailId) {
		this.fkMainEmpDetailId = fkMainEmpDetailId;
	}
	public Integer getFkEmpDomainMgrId() {
		return fkEmpDomainMgrId;
	}
	public void setFkEmpDomainMgrId(Integer fkEmpDomainMgrId) {
		this.fkEmpDomainMgrId = fkEmpDomainMgrId;
	}
	
	public Byte getFkGsrGoalStatus() {
		return fkGsrGoalStatus;
	}
	public void setFkGsrGoalStatus(Byte fkGsrGoalStatus) {
		this.fkGsrGoalStatus = fkGsrGoalStatus;
	}
	
	public Date getGsrGoalEndDate() {
		return gsrGoalEndDate;
	}
	public void setGsrGoalEndDate(Date gsrGoalEndDate) {
		this.gsrGoalEndDate = gsrGoalEndDate;
	}
	
	public Date getGsrGoalStartDate() {
		return gsrGoalStartDate;
	}
	public void setGsrGoalStartDate(Date gsrGoalStartDate) {
		this.gsrGoalStartDate = gsrGoalStartDate;
	}
	
	public Short getFkGsrSlabId() {
		return fkGsrSlabId;
	}
	public void setFkGsrSlabId(Short fkGsrSlabId) {
		this.fkGsrSlabId = fkGsrSlabId;
	}
	@Override
	public String toString() {
		return "DatEmpProfDetailandGsrEmpGoalBean [fkEmpDomainMgrId="
				+ fkEmpDomainMgrId + ", fkMainEmpDetailId=" + fkMainEmpDetailId
				+ ", fkGsrGoalStatus=" + fkGsrGoalStatus + ", fkGsrSlabId="
				+ fkGsrSlabId + ", gsrGoalEndDate=" + gsrGoalEndDate
				+ ", gsrGoalStartDate=" + gsrGoalStartDate + "]";
	}
	
	
	
}
