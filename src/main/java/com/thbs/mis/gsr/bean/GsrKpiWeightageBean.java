package com.thbs.mis.gsr.bean;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


public class GsrKpiWeightageBean {
	
	
	private short pkGsrKpiWeightageId;
	
	@NotNull(message="Level id cannod be null")
	@Min(1)
	//@Max(20)
	private short fkEmpLevelId;
	
    @NotNull(message="Goal id cannot be null")
    @Min(1)
	private short fkGsrKpiGoalId;
    
    @NotNull(message="Goal Weightage cannot be null")
    @Max(20)
    @Min(1)
    
	private Byte gsrKpiGoalWeightage;

	public short getFkEmpLevelId() {
		return fkEmpLevelId;
	}

	public void setFkEmpLevelId(short fkEmpLevelId) {
		this.fkEmpLevelId = fkEmpLevelId;
	}

	public short getFkGsrKpiGoalId() {
		return fkGsrKpiGoalId;
	}

	public void setFkGsrKpiGoalId(short fkGsrKpiGoalId) {
		this.fkGsrKpiGoalId = fkGsrKpiGoalId;
	}

	public Byte getGsrKpiGoalWeightage() {
		return gsrKpiGoalWeightage;
	}

	public void setGsrKpiGoalWeightage(Byte gsrKpiGoalWeightage) {
		this.gsrKpiGoalWeightage = gsrKpiGoalWeightage;
	}

	

	public short getPkGsrKpiWeightageId() {
		return pkGsrKpiWeightageId;
	}

	public void setPkGsrKpiWeightageId(short pkGsrKpiWeightageId) {
		this.pkGsrKpiWeightageId = pkGsrKpiWeightageId;
	}

	@Override
	public String toString() {
		return "GsrKpiWeightageBean [pkGsrKpiWeightageId="
				+ pkGsrKpiWeightageId + ", fkEmpLevelId=" + fkEmpLevelId
				+ ", fkGsrKpiGoalId=" + fkGsrKpiGoalId
				+ ", gsrKpiGoalWeightage=" + gsrKpiGoalWeightage + "]";
	}

}
