package com.thbs.mis.gsr.bean;

public class GsrKpiGoalsBean {
	private short pkGsrKpiWeightageId;
	private short fkEmpLevelId;
	private short fkGsrKpiGoalId;
	private byte gsrKpiGoalWeightage;
	private byte gsrKpiBuId;
	private String gsrKpiGoalName;
	
	@Override
	public String toString() {
		return "GsrKpiGoalsBean [pkGsrKpiWeightageId=" + pkGsrKpiWeightageId
				+ ", fkEmpLevelId=" + fkEmpLevelId + ", fkGsrKpiGoalId="
				+ fkGsrKpiGoalId + ", gsrKpiGoalWeightage="
				+ gsrKpiGoalWeightage + ", gsrKpiBuId=" + gsrKpiBuId
				+ ", gsrKpiGoalName=" + gsrKpiGoalName + "]";
	}
	public String getGsrKpiGoalName() {
		return gsrKpiGoalName;
	}
	public void setGsrKpiGoalName(String gsrKpiGoalName) {
		this.gsrKpiGoalName = gsrKpiGoalName;
	}
	public short getPkGsrKpiWeightageId() {
		return pkGsrKpiWeightageId;
	}
	public void setPkGsrKpiWeightageId(short pkGsrKpiWeightageId) {
		this.pkGsrKpiWeightageId = pkGsrKpiWeightageId;
	}
	public short getFkEmpLevelId() {
		return fkEmpLevelId;
	}
	public void setFkEmpLevelId(short fkEmpLevelId) {
		this.fkEmpLevelId = fkEmpLevelId;
	}
	public short getFkGsrKpiGoalId() {
		return fkGsrKpiGoalId;
	}
	public void setFkGsrKpiGoalId(short fkGsrKpiGoalId) {
		this.fkGsrKpiGoalId = fkGsrKpiGoalId;
	}
	public byte getGsrKpiGoalWeightage() {
		return gsrKpiGoalWeightage;
	}
	public void setGsrKpiGoalWeightage(byte gsrKpiGoalWeightage) {
		this.gsrKpiGoalWeightage = gsrKpiGoalWeightage;
	}
	public byte getGsrKpiBuId() {
		return gsrKpiBuId;
	}
	public void setGsrKpiBuId(byte gsrKpiBuId) {
		this.gsrKpiBuId = gsrKpiBuId;
	}

}
