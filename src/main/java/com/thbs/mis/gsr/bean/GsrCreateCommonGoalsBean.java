/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrCreateCommonGoalsBean.java                     */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	GsrCreateCommonGoalsBean.                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class GsrCreateCommonGoalsBean
{
	private short pkGsrKpiGoalId;
	
	public short getPkGsrKpiGoalId() {
		return pkGsrKpiGoalId;
	}

	public void setPkGsrKpiGoalId(short pkGsrKpiGoalId) {
		this.pkGsrKpiGoalId = pkGsrKpiGoalId;
	}

	@NotBlank(message = "gsrKpiGoalDescription should not be blank.")
	@NotEmpty(message = "gsrKpiGoalDescription should not be empty.")
	@NotNull(message = "gsrKpiGoalDescription should not be null.")
	private String gsrKpiGoalDescription;

	@NotBlank(message = "gsrKpiGoalMeasurement should not be blank.")
	@NotEmpty(message = "gsrKpiGoalMeasurement should not be empty.")
	@NotNull(message = "gsrKpiGoalMeasurement should not be null.")
	private String gsrKpiGoalMeasurement;

	@NotBlank(message = "gsrKpiGoalName should not be blank.")
	@NotEmpty(message = "gsrKpiGoalName should not be empty.")
	@NotNull(message = "gsrKpiGoalName should not be null.")
	private String gsrKpiGoalName;

	public String getGsrKpiGoalDescription()
	{
		return gsrKpiGoalDescription;
	}

	public void setGsrKpiGoalDescription(String gsrKpiGoalDescription)
	{
		this.gsrKpiGoalDescription = gsrKpiGoalDescription;
	}

	public String getGsrKpiGoalMeasurement()
	{
		return gsrKpiGoalMeasurement;
	}

	public void setGsrKpiGoalMeasurement(String gsrKpiGoalMeasurement)
	{
		this.gsrKpiGoalMeasurement = gsrKpiGoalMeasurement;
	}

	public String getGsrKpiGoalName()
	{
		return gsrKpiGoalName;
	}

	public void setGsrKpiGoalName(String gsrKpiGoalName)
	{
		this.gsrKpiGoalName = gsrKpiGoalName;
	}

	@Override
	public String toString() {
		return "GsrCreateCommonGoalsBean [pkGsrKpiGoalId=" + pkGsrKpiGoalId
				+ ", gsrKpiGoalDescription=" + gsrKpiGoalDescription
				+ ", gsrKpiGoalMeasurement=" + gsrKpiGoalMeasurement
				+ ", gsrKpiGoalName=" + gsrKpiGoalName + "]";
	}

	

}
