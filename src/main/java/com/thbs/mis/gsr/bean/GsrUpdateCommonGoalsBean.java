package com.thbs.mis.gsr.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class GsrUpdateCommonGoalsBean {
	
	@Min(1)
	@NotNull(message = "pkGsrKpiGoalId field can not be null.")
	@NumberFormat(style = Style.NUMBER, pattern = "pkGsrKpiGoalId must be an Integer.")
	private Short pkGsrKpiGoalId;
	
	@NotBlank(message = "gsrKpiGoalDescription should not be blank.")
	@NotEmpty(message = "gsrKpiGoalDescription should not be empty.")
	@NotNull(message = "gsrKpiGoalDescription should not be null.")
	private String gsrKpiGoalDescription;

	@NotBlank(message = "gsrKpiGoalMeasurement should not be blank.")
	@NotEmpty(message = "gsrKpiGoalMeasurement should not be empty.")
	@NotNull(message = "gsrKpiGoalMeasurement should not be null.")
	private String gsrKpiGoalMeasurement;

	public Short getPkGsrKpiGoalId() {
		return pkGsrKpiGoalId;
	}

	public void setPkGsrKpiGoalId(Short pkGsrKpiGoalId) {
		this.pkGsrKpiGoalId = pkGsrKpiGoalId;
	}

	public String getGsrKpiGoalDescription() {
		return gsrKpiGoalDescription;
	}

	public void setGsrKpiGoalDescription(String gsrKpiGoalDescription) {
		this.gsrKpiGoalDescription = gsrKpiGoalDescription;
	}

	public String getGsrKpiGoalMeasurement() {
		return gsrKpiGoalMeasurement;
	}

	public void setGsrKpiGoalMeasurement(String gsrKpiGoalMeasurement) {
		this.gsrKpiGoalMeasurement = gsrKpiGoalMeasurement;
	}

	@Override
	public String toString() {
		return "GsrUpdateCommonGoalsBean [pkGsrKpiGoalId=" + pkGsrKpiGoalId
				+ ", gsrKpiGoalDescription=" + gsrKpiGoalDescription
				+ ", gsrKpiGoalMeasurement=" + gsrKpiGoalMeasurement + "]";
	}

	
}
