package com.thbs.mis.gsr.bean;

import java.util.List;

public class GsrReportInputBean {
	 
	String query;
	List<String> headers;
	
	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
	public List<String> getHeaders() {
		return headers;
	}

	public void setHeaders(List<String> headers) {
		this.headers = headers;
	}

	@Override
	public String toString() {
		return "GsrReportInputBean [query=" + query + ", headers=" + headers
				+ "]";
	}
	
}
 
