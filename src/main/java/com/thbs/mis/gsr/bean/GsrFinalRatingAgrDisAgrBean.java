/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrFinalRatingAgrDisAgrBean.java                  */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  13-Nov-2016                                       */
/*                                                                   */
/*  Description :  This is the bean class for all the variable and   */
/*                 the setter and getter methods are used for the    */
/*				   variable related to Final rating functionalities. */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 13-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

public class GsrFinalRatingAgrDisAgrBean {

	// Represents the employee Id.
	Integer empId;
	
	// represents the name of the employee
	String empName;
	
	// represents the employee id of the manager.
	Integer mgrId;
	
	// represents the manager name.. 
	String mgrName;
	// represents the ratings.
	Float rating;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Integer getMgrId() {
		return mgrId;
	}

	public void setMgrId(Integer mgrId) {
		this.mgrId = mgrId;
	}

	public String getMgrName() {
		return mgrName;
	}

	public void setMgrName(String mgrName) {
		this.mgrName = mgrName;
	}

	public Float getRating() {
		return rating;
	}

	public void setRating(Float rating) {
		this.rating = rating;
	}

	/**
	 *	This is the to string representation for all the classes are present in this class. 
	 */
	
	@Override
	public String toString() {
		return "GsrFinalRatingAgrDisAgrBean [empId=" + empId + ", empName="
				+ empName + ", mgrId=" + mgrId + ", mgrName=" + mgrName
				+ ", rating=" + rating + "]";
	}
	
	

}
