package com.thbs.mis.gsr.bean;

import java.util.List;

public class GsrDisagreeBean {

	
	private GsrDisagreeRatingDetailBean gsrDisagreeRatingDetail;
	private List<GsrDisagreeRatingForPrvsSlabsBean> listOfGsrDisagree;
	public GsrDisagreeRatingDetailBean getGsrDisagreeRatingDetail() {
		return gsrDisagreeRatingDetail;
	}
	public void setGsrDisagreeRatingDetail(
			GsrDisagreeRatingDetailBean gsrDisagreeRatingDetail) {
		this.gsrDisagreeRatingDetail = gsrDisagreeRatingDetail;
	}
	public List<GsrDisagreeRatingForPrvsSlabsBean> getListOfGsrDisagree() {
		return listOfGsrDisagree;
	}
	public void setListOfGsrDisagree(
			List<GsrDisagreeRatingForPrvsSlabsBean> listOfGsrDisagree) {
		this.listOfGsrDisagree = listOfGsrDisagree;
	}
	@Override
	public String toString() {
		return "GsrDisagreeBean [gsrDisagreeRatingDetail="
				+ gsrDisagreeRatingDetail + ", listOfGsrDisagree="
				+ listOfGsrDisagree + "]";
	}
	
}
