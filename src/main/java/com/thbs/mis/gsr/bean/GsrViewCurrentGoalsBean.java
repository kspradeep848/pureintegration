/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrViewCurrentGoalsBean.java                		 */
/*                                                                   */
/*  Author      :  THBS			                                     */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Current Goals.                                   */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date              Who           Version         Comments     	 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016       THBS            1.0     Initial version created */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.thbs.mis.framework.util.TimeDeserializer;

/**
 * <Description GsrViewCurrentGoalsBean:> This class contains all the 
 * Bean related methods used for the GsrViewCurrentGoalsBean for Data Base operation.
 * @author THBS
 * @version 1.0
 * @see 
 */
public class GsrViewCurrentGoalsBean {
	@Min(1)
	@NotNull(message = "pkGsrDatEmpGoalId field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "pkGsrDatEmpGoalId must be an Integer")
	private Integer pkGsrDatEmpGoalId;
	@NotNull(message = "Goal Start Date field can not be null")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd HH:mm:SSSS")
	@JsonDeserialize(using = TimeDeserializer.class)
	private Date gsrGoalStartDate;
	@NotNull(message = "Goal End Date field can not be null")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd HH:mm:SSSS")
	@JsonDeserialize(using = TimeDeserializer.class)
	private Date gsrGoalEndDate;
	public Integer getpkGsrDatEmpGoalId() {
		return pkGsrDatEmpGoalId;
	}
	public void setpkGsrDatEmpGoalId(Integer pkGsrDatEmpGoalId) {
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
	}
	public Date getgsrGoalStartDate() {
		return gsrGoalStartDate;
	}
	public void setgsrGoalStartDate(Date gsrGoalStartDate) {
		this.gsrGoalStartDate = gsrGoalStartDate;
	}
	public Date getgsrGoalEndDate() {
		return gsrGoalEndDate;
	}
	public void setgsrGoalEndDate(Date gsrGoalEndDate) {
		this.gsrGoalEndDate = gsrGoalEndDate;
	}
	@Override
	public String toString() {
		return "GsrViewCurrentGoalsBean [pkGsrDatEmpGoalId=" + pkGsrDatEmpGoalId + ", gsrGoalStartDate="
				+ gsrGoalStartDate + ", gsrGoalEndDate=" + gsrGoalEndDate + "]";
	}
	
}
