package com.thbs.mis.gsr.bean;

import java.util.Date;

public class GsrCreateGoalDetailBean {
	private Date startDate;
	private Date endDate;
	private int rmMgrId;
	private int status;
	private int trainingRequestId;
private int pkGsrDatEmpGoalId;
	private String comments;

	public int getTrainingRequestId() {
		return trainingRequestId;
	}

	public void setTrainingRequestId(int trainingRequestId) {
		this.trainingRequestId = trainingRequestId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getRmMgrId() {
		return rmMgrId;
	}

	public void setRmMgrId(int rmMgrId) {
		this.rmMgrId = rmMgrId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getPkGsrDatEmpGoalId()
	{
		return pkGsrDatEmpGoalId;
	}

	public void setPkGsrDatEmpGoalId(int pkGsrDatEmpGoalId)
	{
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
	}

	@Override
	public String toString()
	{
		return "GsrCreateGoalDetailBean [startDate=" + startDate
				+ ", endDate=" + endDate + ", rmMgrId=" + rmMgrId
				+ ", status=" + status + ", trainingRequestId="
				+ trainingRequestId + ", pkGsrDatEmpGoalId="
				+ pkGsrDatEmpGoalId + ", comments=" + comments + "]";
	}

}
