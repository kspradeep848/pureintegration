package com.thbs.mis.gsr.bean;

public class GsrGetAllReportingManagersBean {

	int empId;
	String empName;
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	@Override
	public String toString() {
		return "GsrGetAllReportingManagersBean [empId=" + empId + ", empName="
				+ empName + "]";
	}

	
	
}
