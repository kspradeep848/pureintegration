package com.thbs.mis.gsr.bean;

import javax.validation.constraints.Min;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.common.MIS_Id_Not_Found_BusinessUnit;
import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_Slab;

public class GsrDisagreeAndResolvedFinalRatingUnderBUHeadBean {

	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	@GSR_Id_Not_Found_Slab
	private short slabId;
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	@MIS_Id_Not_Found_BusinessUnit
	private short businessUnitId;

	public short getSlabId() {
		return slabId;
	}

	public void setSlabId(short slabId) {
		this.slabId = slabId;
	}

	public short getBusinessUnitId() {
		return businessUnitId;
	}

	public void setBusinessUnitId(short businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

	@Override
	public String toString() {
		return "GsrDisagreeAndResolvedFinalRatingUnderBUHeadBean [slabId="
				+ slabId + ", businessUnitId=" + businessUnitId + "]";
	}

	
	
}
