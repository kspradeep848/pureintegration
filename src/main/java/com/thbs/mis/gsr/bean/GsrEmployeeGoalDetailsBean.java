/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrEmployeeGoalDetailsBean.java                   */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  November 02, 2016                                 */
/*                                                                   */
/*  Description :  This class contains for the inputs required for   */
/*                   GSR Employee goal details	 				     */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 02, 2016     THBS     1.0        Initial version created */
/*********************************************************************/

package com.thbs.mis.gsr.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

public class GsrEmployeeGoalDetailsBean {

	@Min(1)
	@NotNull(message = "Employee ID should not be null")
	private Integer id;

	@NotNull(message = "Start Date should not be null")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd ")
	private Date empStartDate;

	@NotNull(message = "End Date should not be null")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd ")
	private Date empEndDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEmpStartDate() {
		return empStartDate;
	}

	public void setEmpStartDate(Date empStartDate) {
		this.empStartDate = empStartDate;
	}

	public Date getEmpEndDate() {
		return empEndDate;
	}

	public void setEmpEndDate(Date empEndDate) {
		this.empEndDate = empEndDate;
	}

	@Override
	public String toString() {
		return "GsrEmployeeGoalDetailsBean [id=" + id + ", empStartDate="
				+ empStartDate + ", empEndDate=" + empEndDate + "]";
	}

}
