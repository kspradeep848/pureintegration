/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmployeePerformanceRating.java           		 */
/*                                                                   */
/*  Author      :  Anil kumar B K	                                 */
/*                                                                   */
/*  Date        :  24-Nov-2016                                       */
/*                                                                   */
/*  Description :  Bean class for GSR dash board    			     */
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 24-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.gsr.bean;


public class EmployeePerformanceRating implements Comparable<EmployeePerformanceRating>{

private Float rating;
private Short slab;
private String slabName; 
public Float getRating() {
	return rating;
}
public void setRating(Float rating) {
	this.rating = rating;
}
public Short getSlab() {
	return slab;
}
public void setSlab(Short slab) {
	this.slab = slab;
}

public String getSlabName() {
	return slabName;
}
public void setSlabName(String slabName) {
	this.slabName = slabName;
}

@Override
public String toString() {
	return "EmployeePerformanceRating [rating=" + rating + ", slab=" + slab
			+ ", slabName=" + slabName + "]";
}
@Override
public int compareTo(EmployeePerformanceRating obj) {
	
	if(this.slab == obj.slab)
	return 0;
	else if(this.slab > obj.slab)
	return 1;
	else
	return -1;
}

	
	
}
