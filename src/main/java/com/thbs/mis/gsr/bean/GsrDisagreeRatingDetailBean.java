package com.thbs.mis.gsr.bean;

public class GsrDisagreeRatingDetailBean {

	private String highestDisagreeInQuarter;
	private Integer totalDisagrees;
	public String getHighestDisagreeInQuarter() {
		return highestDisagreeInQuarter;
	}
	public void setHighestDisagreeInQuarter(String highestDisagreeInQuarter) {
		this.highestDisagreeInQuarter = highestDisagreeInQuarter;
	}
	public Integer getTotalDisagrees() {
		return totalDisagrees;
	}
	public void setTotalDisagrees(Integer totalDisagrees) {
		this.totalDisagrees = totalDisagrees;
	}
	@Override
	public String toString() {
		return "GsrDisagreeRatingDetailBean [highestDisagreeInQuarter="
				+ highestDisagreeInQuarter + ", totalDisagrees="
				+ totalDisagrees + "]";
	}
	
	
	
}
