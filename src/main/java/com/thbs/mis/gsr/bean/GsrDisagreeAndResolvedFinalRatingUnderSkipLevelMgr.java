package com.thbs.mis.gsr.bean;

import javax.validation.constraints.Min;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.common.MIS_Id_Not_Found_Employee;
import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_Slab;

public class GsrDisagreeAndResolvedFinalRatingUnderSkipLevelMgr {
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	@GSR_Id_Not_Found_Slab
	private short slabId;
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	@MIS_Id_Not_Found_Employee
	private int skipLevelMgrId;

	public short getSlabId() {
		return slabId;
	}

	public void setSlabId(short slabId) {
		this.slabId = slabId;
	}

	public int getSkipLevelMgrId() {
		return skipLevelMgrId;
	}

	public void setSkipLevelMgrId(int skipLevelMgrId) {
		this.skipLevelMgrId = skipLevelMgrId;
	}

	@Override
	public String toString() {
		return "DisagreeAndResolvedFinalRatingUnderSkipLevelMgr [slabId="
				+ slabId + ", skipLevelMgrId=" + skipLevelMgrId + "]";
	}
	
	

}
