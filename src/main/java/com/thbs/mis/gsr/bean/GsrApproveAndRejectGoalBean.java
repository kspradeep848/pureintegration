package com.thbs.mis.gsr.bean;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class GsrApproveAndRejectGoalBean
{
	@Min(1)
	@NotNull(message = "pkGsrDatEmpGoalId should not be null.")
	@NumberFormat(style = Style.NUMBER)
	private Integer pkGsrDatEmpGoalId;
	
	@Min(1)
	@Max(7)
	@NotNull(message = "fkGsrGoalStatus should not be null.")
	@NumberFormat(style = Style.NUMBER)
	private Byte fkGsrGoalStatus;
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	@NotNull(message = "goalModifiedBy should not be null.")
	private Integer goalModifiedBy;
	
	public Integer getGoalModifiedBy() {
		return goalModifiedBy;
	}
	public void setGoalModifiedBy(Integer goalModifiedBy) {
		this.goalModifiedBy = goalModifiedBy;
	}
	public Integer getPkGsrDatEmpGoalId()
	{
		return pkGsrDatEmpGoalId;
	}
	public void setPkGsrDatEmpGoalId(Integer pkGsrDatEmpGoalId)
	{
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
	}
	public Byte getFkGsrGoalStatus()
	{
		return fkGsrGoalStatus;
	}
	public void setFkGsrGoalStatus(Byte fkGsrGoalStatus)
	{
		this.fkGsrGoalStatus = fkGsrGoalStatus;
	}
	@Override
	public String toString() {
		return "GsrApproveAndRejectGoalBean [pkGsrDatEmpGoalId="
				+ pkGsrDatEmpGoalId + ", fkGsrGoalStatus=" + fkGsrGoalStatus
				+ ", goalModifiedBy=" + goalModifiedBy + "]";
	}
}
