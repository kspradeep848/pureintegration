package com.thbs.mis.gsr.bean;

import java.util.Date;

public class GSRDelegatedEmpGoalBean {
	
	private Integer empId;
	
	private Date slabStartDate;
	
	private Date slabEndDate;
	
	private Integer delegatedTo;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Date getSlabStartDate() {
		return slabStartDate;
	}

	public void setSlabStartDate(Date slabStartDate) {
		this.slabStartDate = slabStartDate;
	}

	public Date getSlabEndDate() {
		return slabEndDate;
	}

	public void setSlabEndDate(Date slabEndDate) {
		this.slabEndDate = slabEndDate;
	}

	public Integer getDelegatedTo() {
		return delegatedTo;
	}

	public void setDelegatedTo(Integer delegatedTo) {
		this.delegatedTo = delegatedTo;
	}

	@Override
	public String toString() {
		return "GSRDelegatedEmpGoalBean [empId=" + empId + ", slabStartDate="
				+ slabStartDate + ", slabEndDate=" + slabEndDate
				+ ", delegatedTo=" + delegatedTo + "]";
	}

}
