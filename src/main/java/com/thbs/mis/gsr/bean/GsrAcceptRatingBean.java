/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrAcceptRatingBean.java                   		 */
/*                                                                   */
/*  Author      :  THBS					                             */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Accept Rating.                                   */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date              Who           Version         Comments     	 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016       THBS            1.0     Initial version created */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

/**
 * <Description GsrAcceptRatingBean:> This class contains all the 
 * Bean related methods used for the GsrAcceptRatingBean for Data Base operation.
 * @author THBS
 * @version 1.0
 * @see 
 */
public class GsrAcceptRatingBean {
	@Min(1)
	@NotNull(message = "FinalRatingID field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "pkGsrFinalRatingId must be an Integer")
	private Integer pkGsrFinalRatingId;
	@Min(1)
	@Max(4)
	@NotNull(message = "FinalRatingStatus field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "finalRatingStatus must be an Integer")
	private Byte finalRatingStatus;
	@NotNull(message = "GSRFinalRating field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern ="gsrFinalRating must be an Integer")
	@DecimalMax(value = "5.0", message = "The decimal value can not be more than 5.0 ")
    @DecimalMin(value = "1.00", message = "The decimal value can not be less than 1.00 digit ")
	private Float gsrFinalRating;
	public Integer getPkGsrFinalRatingId() {
		return pkGsrFinalRatingId;
	}
	public void setPkGsrFinalRatingId(Integer pkGsrFinalRatingId) {
		this.pkGsrFinalRatingId = pkGsrFinalRatingId;
	}
	public Byte getFinalRatingStatus() {
		return finalRatingStatus;
	}
	public void setFinalRatingStatus(Byte finalRatingStatus) {
		this.finalRatingStatus = finalRatingStatus;
	}
	public Float getGsrFinalRating() {
		return gsrFinalRating;
	}
	public void setGsrFinalRating(Float gsrFinalRating) {
		this.gsrFinalRating = gsrFinalRating;
	}
	@Override
	public String toString() {
		return "GsrAcceptRatingBean [pkGsrFinalRatingId=" + pkGsrFinalRatingId
				+ ", finalRatingStatus=" + finalRatingStatus
				+ ", gsrFinalRating=" + gsrFinalRating + "]";
	}
	
		
}
