package com.thbs.mis.gsr.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.thbs.mis.framework.util.DateDeserializer;

public class GsrCreateGoalInputBean {

	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	private int employeeId;
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	private int managerId;
		
	@Length(max = 100, message = "Goal name field can't exceed 100 characters")
	private String goalName;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date goalStartDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date goalEndDate;
	
	@Min(0)
	@NumberFormat(style = Style.NUMBER)
	private short slabId;
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	private  int goalWeightage;
	
	@NotEmpty(message =  "Please add goal description ")	
	@NotNull(message =  "Please add goal description ")	
	private String goalDescription;
	
	@NotEmpty(message =  "Please Add GoalMeasuremente ")	
	@NotNull(message =  "Please Add GoalMeasuremente ")	
	private String goalMeasurement;
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	private int goalCreatedBy;
	
	@Min(0)
	@NumberFormat(style = Style.NUMBER)
	private short gsrKpiWeightageId;
	
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public int getManagerId() {
		return managerId;
	}
	public void setManagerId(int managerId) {
		this.managerId = managerId;
	}
	public String getGoalName() {
		return goalName;
	}
	public void setGoalName(String goalName) {
		this.goalName = goalName;
	}
	
	
	public Date getGoalStartDate() {
		return goalStartDate;
	}
	@JsonDeserialize(using = DateDeserializer.class)
	public void setGoalStartDate(Date goalStartDate) {
		this.goalStartDate = goalStartDate;
	}
	
	public Date getGoalEndDate() {
		return goalEndDate;
	}
	@JsonDeserialize(using = DateDeserializer.class)
	public void setGoalEndDate(Date goalEndDate) {
		this.goalEndDate = goalEndDate;
	}
	public short getSlabId() {
		return slabId;
	}
	public void setSlabId(short slabId) {
		this.slabId = slabId;
	}
	public int getGoalWeightage() {
		return goalWeightage;
	}
	public void setGoalWeightage(int goalWeightage) {
		this.goalWeightage = goalWeightage;
	}
	public String getGoalDescription() {
		return goalDescription;
	}
	public void setGoalDescription(String goalDescription) {
		this.goalDescription = goalDescription;
	}
	public String getGoalMeasurement() {
		return goalMeasurement;
	}
	public void setGoalMeasurement(String goalMeasurement) {
		this.goalMeasurement = goalMeasurement;
	}
	public int getGoalCreatedBy() {
		return goalCreatedBy;
	}
	public void setGoalCreatedBy(int goalCreatedBy) {
		this.goalCreatedBy = goalCreatedBy;
	}
	public short getGsrKpiWeightageId() {
		return gsrKpiWeightageId;
	}
	public void setGsrKpiWeightageId(short gsrKpiWeightageId) {
		this.gsrKpiWeightageId = gsrKpiWeightageId;
	}
	@Override
	public String toString() {
		return "GsrCreateGoalInputBean [employeeId=" + employeeId
				+ ", managerId=" + managerId + ", goalName=" + goalName
				+ ", goalStartDate=" + goalStartDate + ", goalEndDate="
				+ goalEndDate + ", slabId=" + slabId + ", goalWeightage="
				+ goalWeightage + ", goalDescription=" + goalDescription
				+ ", goalMeasurement=" + goalMeasurement + ", goalCreatedBy="
				+ goalCreatedBy + ", gsrKpiWeightageId=" + gsrKpiWeightageId
				+ "]";
	}
	
	
		
}
