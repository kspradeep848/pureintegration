 /*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    : GsrFinalRatingBean.java                            */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the GSREnterAccomplishmentRepository     */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
package com.thbs.mis.gsr.bean;

import javax.validation.constraints.Min;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class GsrFinalRatingBean {

	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	
	private short slabId;
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	private int domainMgrId;
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	private short buId;
	
	private String gsrStartDate;
	
	private String gsrEndDate;

	public short getSlabId() {
		return slabId;
	}

	public void setSlabId(short slabId) {
		this.slabId = slabId;
	}

	public int getDomainMgrId() {
		return domainMgrId;
	}

	public void setDomainMgrId(int domainMgrId) {
		this.domainMgrId = domainMgrId;
	}

	public short getBuId() {
		return buId;
	}

	public void setBuId(short buId) {
		this.buId = buId;
	}

	public String getGsrStartDate() {
		return gsrStartDate;
	}

	public void setGsrStartDate(String gsrStartDate) {
		this.gsrStartDate = gsrStartDate;
	}

	public String getGsrEndDate() {
		return gsrEndDate;
	}

	public void setGsrEndDate(String gsrEndDate) {
		this.gsrEndDate = gsrEndDate;
	}

	@Override
	public String toString() {
		return "GsrFinalRatingBean [slabId=" + slabId + ", domainMgrId="
				+ domainMgrId + ", buId=" + buId + ", gsrStartDate="
				+ gsrStartDate + ", gsrEndDate=" + gsrEndDate + "]";
	}
	
	

}
