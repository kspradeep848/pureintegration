 /*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    : GsrCreateGoalBean.java                             */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains all the DAO related methods   */
/*                 used for the GSREnterAccomplishmentRepository     */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/


package com.thbs.mis.gsr.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class GsrCreateGoalBean {
	
	private int pkGsrDatEmpGoalId;

	@NotNull(message =  "Please Add emp id")
	@Min(1)
	private int fkGsrDatEmpId;

	@Min(1)
	@NotNull(message =  "Please add mgr id ")
	private int fkGsrDatMgrId;

	
	
	@NotNull(message =  "Please add goal status ")
	private byte fkGsrGoalStatus;

	@Min(1)
	@NotNull(message =  "Please add slab id ")
	private short fkGsrSlabId;
	
	@NotEmpty(message =  "Please Add goal Name ")	
	@NotNull(message =  "Please Add goal Name ")	
	@Length(max = 100, message = "goal name field can't exceed 100 chars")
	private String gsrDatEmpGoalName;

/*	@NotNull(message = "Please enter goal aaccomplishment")
	@NotBlank(message = "Please enter goal aaccomplishment")
	@Length(max = 100, message = "goal accomplishment field can't exceed 100 chars")*/
	private String gsrGoalAccomplishment;

	
/*	@Min(1)
	@NotNull(message =  "Please add Goal closed by ")*/
	 
	private int gsrGoalClosedBy;

	@Min(1)
	@NotNull(message =  "Please add crated by ")
	private int gsrGoalCreatedBy;

	
	@NotNull(message =  "Please Add GoalCreatedDate ")
	private Date gsrGoalCreatedDate;

	@NotEmpty(message =  "Please Add GoalDescription ")
	@NotNull(message =  "Please Add GoalDescription ")
	private String gsrGoalDescription;

	@NotNull(message =  "Please Add GoalEndDate ")	
	private Date gsrGoalEndDate;

	@NotEmpty(message =  "Please Add GoalMeasuremente ")	
	@NotNull(message =  "Please Add GoalMeasuremente ")	
	private String gsrGoalMeasurement;

	/*@NotEmpty(message =  "Please Add GoalMgrFeedback ")	
	@NotNull(message =  "Please Add GoalMgrFeedback ")	*/
	private String gsrGoalMgrFeedback;

	
	/*@Min(1)*/
	private float gsrGoalMgrRating;

/*	@Min(1)*/
	private int gsrGoalModifiedBy;

	/*@NotNull(message =  "Please Add GoalModifiedDate ")	*/
	private Date gsrGoalModifiedDate;

	/*@Min(1)*/
	private float gsrGoalSelfRating;

	@NotNull(message =  "Please Add GoalStartDate ")	
	private Date gsrGoalStartDate;

	
	public int getPkGsrDatEmpGoalId() {
		return pkGsrDatEmpGoalId;
	}

	public void setPkGsrDatEmpGoalId(int pkGsrDatEmpGoalId) {
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
	}

	public int getFkGsrDatEmpId() {
		return fkGsrDatEmpId;
	}

	public void setFkGsrDatEmpId(int fkGsrDatEmpId) {
		this.fkGsrDatEmpId = fkGsrDatEmpId;
	}

	public int getFkGsrDatMgrId() {
		return fkGsrDatMgrId;
	}

	public void setFkGsrDatMgrId(int fkGsrDatMgrId) {
		this.fkGsrDatMgrId = fkGsrDatMgrId;
	}

	public byte getFkGsrGoalStatus() {
		return fkGsrGoalStatus;
	}

	public void setFkGsrGoalStatus(byte fkGsrGoalStatus) {
		this.fkGsrGoalStatus = fkGsrGoalStatus;
	}

	public short getFkGsrSlabId() {
		return fkGsrSlabId;
	}

	public void setFkGsrSlabId(short fkGsrSlabId) {
		this.fkGsrSlabId = fkGsrSlabId;
	}

	public String getGsrDatEmpGoalName() {
		return gsrDatEmpGoalName;
	}

	public void setGsrDatEmpGoalName(String gsrDatEmpGoalName) {
		this.gsrDatEmpGoalName = gsrDatEmpGoalName;
	}

	public String getGsrGoalAccomplishment() {
		return gsrGoalAccomplishment;
	}

	public void setGsrGoalAccomplishment(String gsrGoalAccomplishment) {
		this.gsrGoalAccomplishment = gsrGoalAccomplishment;
	}

	public int getGsrGoalClosedBy() {
		return gsrGoalClosedBy;
	}

	public void setGsrGoalClosedBy(int gsrGoalClosedBy) {
		this.gsrGoalClosedBy = gsrGoalClosedBy;
	}

	public int getGsrGoalCreatedBy() {
		return gsrGoalCreatedBy;
	}

	public void setGsrGoalCreatedBy(int gsrGoalCreatedBy) {
		this.gsrGoalCreatedBy = gsrGoalCreatedBy;
	}

	public Date getGsrGoalCreatedDate() {
		return gsrGoalCreatedDate;
	}

	public void setGsrGoalCreatedDate(Date gsrGoalCreatedDate) {
		this.gsrGoalCreatedDate = gsrGoalCreatedDate;
	}

	public String getGsrGoalDescription() {
		return gsrGoalDescription;
	}

	public void setGsrGoalDescription(String gsrGoalDescription) {
		this.gsrGoalDescription = gsrGoalDescription;
	}

	public Date getGsrGoalEndDate() {
		return gsrGoalEndDate;
	}

	public void setGsrGoalEndDate(Date gsrGoalEndDate) {
		this.gsrGoalEndDate = gsrGoalEndDate;
	}

	public String getGsrGoalMeasurement() {
		return gsrGoalMeasurement;
	}

	public void setGsrGoalMeasurement(String gsrGoalMeasurement) {
		this.gsrGoalMeasurement = gsrGoalMeasurement;
	}

	public String getGsrGoalMgrFeedback() {
		return gsrGoalMgrFeedback;
	}

	public void setGsrGoalMgrFeedback(String gsrGoalMgrFeedback) {
		this.gsrGoalMgrFeedback = gsrGoalMgrFeedback;
	}

	public float getGsrGoalMgrRating() {
		return gsrGoalMgrRating;
	}

	public void setGsrGoalMgrRating(float gsrGoalMgrRating) {
		this.gsrGoalMgrRating = gsrGoalMgrRating;
	}

	public int getGsrGoalModifiedBy() {
		return gsrGoalModifiedBy;
	}

	public void setGsrGoalModifiedBy(int gsrGoalModifiedBy) {
		this.gsrGoalModifiedBy = gsrGoalModifiedBy;
	}

	public Date getGsrGoalModifiedDate() {
		return gsrGoalModifiedDate;
	}

	public void setGsrGoalModifiedDate(Date gsrGoalModifiedDate) {
		this.gsrGoalModifiedDate = gsrGoalModifiedDate;
	}

	public float getGsrGoalSelfRating() {
		return gsrGoalSelfRating;
	}

	public void setGsrGoalSelfRating(float gsrGoalSelfRating) {
		this.gsrGoalSelfRating = gsrGoalSelfRating;
	}

	public Date getGsrGoalStartDate() {
		return gsrGoalStartDate;
	}

	public void setGsrGoalStartDate(Date gsrGoalStartDate) {
		this.gsrGoalStartDate = gsrGoalStartDate;
	}

	public byte getGsrGoalWeightage() {
		return gsrGoalWeightage;
	}

	public void setGsrGoalWeightage(byte gsrGoalWeightage) {
		this.gsrGoalWeightage = gsrGoalWeightage;
	}
	@NotNull(message =  "Please Add GoalWeightage ")
	private byte gsrGoalWeightage;

	@Override
	public String toString() {
		return "GsrCreateGoalBean [pkGsrDatEmpGoalId=" + pkGsrDatEmpGoalId
				+ ", fkGsrDatEmpId=" + fkGsrDatEmpId + ", fkGsrDatMgrId="
				+ fkGsrDatMgrId + ", fkGsrGoalStatus=" + fkGsrGoalStatus
				+ ", fkGsrSlabId=" + fkGsrSlabId + ", gsrDatEmpGoalName="
				+ gsrDatEmpGoalName + ", gsrGoalAccomplishment="
				+ gsrGoalAccomplishment + ", gsrGoalClosedBy="
				+ gsrGoalClosedBy + ", gsrGoalCreatedBy=" + gsrGoalCreatedBy
				+ ", gsrGoalCreatedDate=" + gsrGoalCreatedDate
				+ ", gsrGoalDescription=" + gsrGoalDescription
				+ ", gsrGoalEndDate=" + gsrGoalEndDate
				+ ", gsrGoalMeasurement=" + gsrGoalMeasurement
				+ ", gsrGoalMgrFeedback=" + gsrGoalMgrFeedback
				+ ", gsrGoalMgrRating=" + gsrGoalMgrRating
				+ ", gsrGoalModifiedBy=" + gsrGoalModifiedBy
				+ ", gsrGoalModifiedDate=" + gsrGoalModifiedDate
				+ ", gsrGoalSelfRating=" + gsrGoalSelfRating
				+ ", gsrGoalStartDate=" + gsrGoalStartDate
				+ ", gsrGoalWeightage=" + gsrGoalWeightage
				+ ", getPkGsrDatEmpGoalId()=" + getPkGsrDatEmpGoalId()
				+ ", getFkGsrDatEmpId()=" + getFkGsrDatEmpId()
				+ ", getFkGsrDatMgrId()=" + getFkGsrDatMgrId()
				+ ", getFkGsrGoalStatus()=" + getFkGsrGoalStatus()
				+ ", getFkGsrSlabId()=" + getFkGsrSlabId()
				+ ", getGsrDatEmpGoalName()=" + getGsrDatEmpGoalName()
				+ ", getGsrGoalAccomplishment()=" + getGsrGoalAccomplishment()
				+ ", getGsrGoalClosedBy()=" + getGsrGoalClosedBy()
				+ ", getGsrGoalCreatedBy()=" + getGsrGoalCreatedBy()
				+ ", getGsrGoalCreatedDate()=" + getGsrGoalCreatedDate()
				+ ", getGsrGoalDescription()=" + getGsrGoalDescription()
				+ ", getGsrGoalEndDate()=" + getGsrGoalEndDate()
				+ ", getGsrGoalMeasurement()=" + getGsrGoalMeasurement()
				+ ", getGsrGoalMgrFeedback()=" + getGsrGoalMgrFeedback()
				+ ", getGsrGoalMgrRating()=" + getGsrGoalMgrRating()
				+ ", getGsrGoalModifiedBy()=" + getGsrGoalModifiedBy()
				+ ", getGsrGoalModifiedDate()=" + getGsrGoalModifiedDate()
				+ ", getGsrGoalSelfRating()=" + getGsrGoalSelfRating()
				+ ", getGsrGoalStartDate()=" + getGsrGoalStartDate()
				+ ", getGsrGoalWeightage()=" + getGsrGoalWeightage()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

 
 
 
 
 
 
}
