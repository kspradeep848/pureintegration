package com.thbs.mis.gsr.bean;

public class GsrAgreeDisAgreeDetailsBean {

	private Integer employeeId;
	
	private String empName;
	
	private Float rating;
	
	private Integer mgrEmpId;
	
	private String mgrEmpName;
	

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Float getRating() {
		return rating;
	}

	public void setRating(Float rating) {
		this.rating = rating;
	}

	public Integer getMgrEmpId() {
		return mgrEmpId;
	}

	public void setMgrEmpId(Integer mgrEmpId) {
		this.mgrEmpId = mgrEmpId;
	}

	public String getMgrEmpName() {
		return mgrEmpName;
	}

	public void setMgrEmpName(String mgrEmpName) {
		this.mgrEmpName = mgrEmpName;
	}

	@Override
	public String toString() {
		return "GsrAgreeDisAgreeDetailsBean [employeeId=" + employeeId
				+ ", empName=" + empName + ", rating=" + rating + ", mgrEmpId="
				+ mgrEmpId + ", mgrEmpName=" + mgrEmpName + "]";
	}
	
	

}
