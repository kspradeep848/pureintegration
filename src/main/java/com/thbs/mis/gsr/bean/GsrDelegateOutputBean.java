package com.thbs.mis.gsr.bean;

import java.util.Date;

public class GsrDelegateOutputBean {
	
	private Integer delegatedGoalId;
	private Integer empGoalID;
	private Integer delegatedEmpId;
	private Integer delegatedFrom;
	private Integer delegatedTo;
	private byte delegatedStatus;
	private Date delegatedDate;
	private Date acceptOrRejectedDate;
	private Integer delegatedBy;
	private String delegationStatusMessage;
	private String goalName;
	private String delegatedToMgrName;
	
	public String getDelegatedToMgrName() {
		return delegatedToMgrName;
	}
	public void setDelegatedToMgrName(String delegatedToMgrName) {
		this.delegatedToMgrName = delegatedToMgrName;
	}
	public String getGoalName() {
		return goalName;
	}
	public void setGoalName(String goalName) {
		this.goalName = goalName;
	}

	public Integer getDelegatedGoalId() {
		return delegatedGoalId;
	}
	public void setDelegatedGoalId(Integer delegatedGoalId) {
		this.delegatedGoalId = delegatedGoalId;
	}
	public Integer getEmpGoalID() {
		return empGoalID;
	}
	public String getDelegationStatusMessage() {
		return delegationStatusMessage;
	}
	public void setDelegationStatusMessage(String delegationStatusMessage) {
		this.delegationStatusMessage = delegationStatusMessage;
	}
	public void setEmpGoalID(Integer empGoalID) {
		this.empGoalID = empGoalID;
	}
	public Integer getDelegatedEmpId() {
		return delegatedEmpId;
	}
	public void setDelegatedEmpId(Integer delegatedEmpId) {
		this.delegatedEmpId = delegatedEmpId;
	}
	public Integer getDelegatedFrom() {
		return delegatedFrom;
	}
	public void setDelegatedFrom(Integer delegatedFrom) {
		this.delegatedFrom = delegatedFrom;
	}
	public Integer getDelegatedTo() {
		return delegatedTo;
	}
	public void setDelegatedTo(Integer delegatedTo) {
		this.delegatedTo = delegatedTo;
	}
	public byte getDelegatedStatus() {
		return delegatedStatus;
	}
	public void setDelegatedStatus(byte delegatedStatus) {
		this.delegatedStatus = delegatedStatus;
	}
	public Date getDelegatedDate() {
		return delegatedDate;
	}
	public void setDelegatedDate(Date delegatedDate) {
		this.delegatedDate = delegatedDate;
	}
	public Date getAcceptOrRejectedDate() {
		return acceptOrRejectedDate;
	}
	public void setAcceptOrRejectedDate(Date acceptOrRejectedDate) {
		this.acceptOrRejectedDate = acceptOrRejectedDate;
	}
	public Integer getDelegatedBy() {
		return delegatedBy;
	}
	public void setDelegatedBy(Integer delegatedBy) {
		this.delegatedBy = delegatedBy;
	}
	
	@Override
	public String toString() {
		return "GsrDelegateOutputBean [delegatedGoalId=" + delegatedGoalId
				+ ", empGoalID=" + empGoalID + ", delegatedEmpId="
				+ delegatedEmpId + ", delegatedFrom=" + delegatedFrom
				+ ", delegatedTo=" + delegatedTo + ", delegatedStatus="
				+ delegatedStatus + ", delegatedDate=" + delegatedDate
				+ ", acceptOrRejectedDate=" + acceptOrRejectedDate
				+ ", delegatedBy=" + delegatedBy + ", delegationStatusMessage="
				+ delegationStatusMessage + ", goalName=" + goalName
				+ ", delegatedToMgrName=" + delegatedToMgrName + "]";
	}

}
