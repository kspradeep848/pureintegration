/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrEmployeeGoalDetailsBeanPagenation.java         */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  March 28, 2017                                    */
/*                                                                   */
/*  Description :  This class contains for the inputs required for   */
/*                   GSR Employee goal details	 				     */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 02, 2016     THBS     1.0        Initial version created */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class GsrEmployeeGoalDetailsBeanPagenation {

	@Min(1)
	@NotNull(message = "Employee ID should not be null")
	@Employee_Id_Not_Found
	private Integer empId;

	@NotNull(message = "Start Date should not be null")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd ")
	private Date goalStartDate;

	@NotNull(message = "End Date should not be null")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd ")
	private Date goalEndDate;
	
	@Min(0)
	@NumberFormat(style = Style.NUMBER, pattern = "Page Number should be a numeric value")
	private Integer pageNumber;
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER, pattern = "Page Size should be a numeric value")
	private Integer pageSize;

	public Integer getEmpId()
	{
		return empId;
	}

	public void setEmpId(Integer empId)
	{
		this.empId = empId;
	}

	public Date getGoalStartDate()
	{
		return goalStartDate;
	}

	public void setGoalStartDate(Date goalStartDate)
	{
		this.goalStartDate = goalStartDate;
	}

	public Date getGoalEndDate()
	{
		return goalEndDate;
	}

	public void setGoalEndDate(Date goalEndDate)
	{
		this.goalEndDate = goalEndDate;
	}

	public Integer getPageNumber()
	{
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber)
	{
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize()
	{
		return pageSize;
	}

	public void setPageSize(Integer pageSize)
	{
		this.pageSize = pageSize;
	}

	@Override
	public String toString()
	{
		return "GsrEmployeeGoalDetailsBeanPagenation [empId=" + empId
				+ ", goalStartDate=" + goalStartDate + ", goalEndDate="
				+ goalEndDate + ", pageNumber=" + pageNumber
				+ ", pageSize=" + pageSize + "]";
	}

}
