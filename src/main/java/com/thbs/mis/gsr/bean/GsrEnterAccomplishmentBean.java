/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrEnterAccomplishmentBean.java             		 */
/*                                                                   */
/*  Author      :  THBS			                                     */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Enter Accomplishment.                            */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date              Who           Version         Comments     	 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016       THBS            1.0     Initial version created */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

/**
 * <Description GsrEnterAccomplishmentBean:> This class contains all the Bean
 * related methods used for the GsrEnterAccomplishmentBean for Data Base
 * operation.
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
public class GsrEnterAccomplishmentBean {

	@NotNull(message = "Accomplishment field can not be null")
	@NotEmpty(message = "Accomplishment field can not be empty")
	@NotBlank(message = "Accomplishment should not be empty")
	private String gsrGoalAccomplishment;

	private Float gsrGoalSelfRating;

	@Min(1)
	@NotNull(message = "EmpGoalID field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "empGoalid must be an Integer")
	private Integer pkGsrDatEmpGoalId;

	@Min(1)
	@NotNull(message = "EmpID field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "empId must be an Integer")
	private Integer fkGsrDatEmpId;

	// @JsonSerialize(using = DateSerializer.class)
	private Date gsrGoalModifiedDate;
	
	private boolean updateGoalFlag;

	public String getGsrGoalAccomplishment() {
		return gsrGoalAccomplishment;
	}

	public void setGsrGoalAccomplishment(String gsrGoalAccomplishment) {
		this.gsrGoalAccomplishment = gsrGoalAccomplishment;
	}

	public Float getGsrGoalSelfRating() {
		return gsrGoalSelfRating;
	}

	public void setGsrGoalSelfRating(Float gsrGoalSelfRating) {
		this.gsrGoalSelfRating = gsrGoalSelfRating;
	}

	public Integer getPkGsrDatEmpGoalId() {
		return pkGsrDatEmpGoalId;
	}

	public void setPkGsrDatEmpGoalId(Integer pkGsrDatEmpGoalId) {
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
	}

	public Integer getFkGsrDatEmpId() {
		return fkGsrDatEmpId;
	}

	public void setFkGsrDatEmpId(Integer fkGsrDatEmpId) {
		this.fkGsrDatEmpId = fkGsrDatEmpId;
	}

	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getGsrGoalModifiedDate() {
		return gsrGoalModifiedDate;
	}

	public void setGsrGoalModifiedDate(Date gsrGoalModifiedDate) {
		this.gsrGoalModifiedDate = gsrGoalModifiedDate;
	}

	public boolean isUpdateGoalFlag() {
		return updateGoalFlag;
	}

	public void setUpdateGoalFlag(boolean updateGoalFlag) {
		this.updateGoalFlag = updateGoalFlag;
	}

	@Override
	public String toString() {
		return "GsrEnterAccomplishmentBean [gsrGoalAccomplishment="
				+ gsrGoalAccomplishment + ", gsrGoalSelfRating="
				+ gsrGoalSelfRating + ", pkGsrDatEmpGoalId="
				+ pkGsrDatEmpGoalId + ", fkGsrDatEmpId=" + fkGsrDatEmpId
				+ ", gsrGoalModifiedDate=" + gsrGoalModifiedDate
				+ ", updateGoalFlag=" + updateGoalFlag + "]";
	}

}
