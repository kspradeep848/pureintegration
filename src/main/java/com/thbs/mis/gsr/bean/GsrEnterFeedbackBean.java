package com.thbs.mis.gsr.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class GsrEnterFeedbackBean
{
	@NotBlank(message = "gsrGoalMgrFeedback should not be blank.")
	@NotEmpty(message = "gsrGoalMgrFeedback Description should not be empty.")
	@NotNull(message = "gsrGoalMgrFeedback should not be null.")
	private String gsrGoalMgrFeedback;

	
	//@DecimalMax(value = "5.0", message = "The decimal value can not be more than 5.0.")
   // @DecimalMin(value = "1.00", message = "The decimal value can not be less than 1.00.")
	@NumberFormat(style = Style.NUMBER)
	@NotNull(message = "gsrGoalMgrRating should not be null.")
	private Float gsrGoalMgrRating;

	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	@NotNull(message = "pkGsrDatEmpGoalId should not be null.")
	private Integer pkGsrDatEmpGoalId;
	
	@Min(1)
	@Employee_Id_Not_Found
	@NumberFormat(style = Style.NUMBER)
	@NotNull(message = "enteredBy should not be null.")
	private Integer enteredBy;

	public Integer getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(Integer enteredBy) {
		this.enteredBy = enteredBy;
	}

	public String getGsrGoalMgrFeedback()
	{
		return gsrGoalMgrFeedback;
	}

	public void setGsrGoalMgrFeedback(String gsrGoalMgrFeedback)
	{
		this.gsrGoalMgrFeedback = gsrGoalMgrFeedback;
	}

	public Float getGsrGoalMgrRating()
	{
		return gsrGoalMgrRating;
	}

	public void setGsrGoalMgrRating(Float gsrGoalMgrRating)
	{
		this.gsrGoalMgrRating = gsrGoalMgrRating;
	}

	public Integer getPkGsrDatEmpGoalId()
	{
		return pkGsrDatEmpGoalId;
	}

	public void setPkGsrDatEmpGoalId(Integer pkGsrDatEmpGoalId)
	{
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
	}

	@Override
	public String toString() {
		return "GsrEnterFeedbackBean [gsrGoalMgrFeedback=" + gsrGoalMgrFeedback
				+ ", gsrGoalMgrRating=" + gsrGoalMgrRating
				+ ", pkGsrDatEmpGoalId=" + pkGsrDatEmpGoalId + ", enteredBy="
				+ enteredBy + "]";
	}
	
}
