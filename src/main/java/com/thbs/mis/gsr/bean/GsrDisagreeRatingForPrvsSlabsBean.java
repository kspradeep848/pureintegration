
package com.thbs.mis.gsr.bean;

public class GsrDisagreeRatingForPrvsSlabsBean implements Comparable<GsrDisagreeRatingForPrvsSlabsBean>{

	private Short slabId;
	private String slabName;
	private Integer countOfDisagreedRating;
	private String slabShortName;
	private String slabYear;
	
	
	public Short getSlabId() {
		return slabId;
	}
	public void setSlabId(Short slabId) {
		this.slabId = slabId;
	}
	public String getSlabName() {
		return slabName;
	}
	public void setSlabName(String slabName) {
		this.slabName = slabName;
	}
	public Integer getCountOfDisagreedRating() {
		return countOfDisagreedRating;
	}
	public void setCountOfDisagreedRating(Integer countOfDisagreedRating) {
		this.countOfDisagreedRating = countOfDisagreedRating;
	}
	public String getSlabShortName() {
		return slabShortName;
	}
	public void setSlabShortName(String slabShortName) {
		this.slabShortName = slabShortName;
	}
	
	public String getSlabYear() {
		return slabYear;
	}
	public void setSlabYear(String slabYear) {
		this.slabYear = slabYear;
	}
	@Override
	public String toString() {
		return "GsrDisagreeRatingForPrvsSlabsBean [slabId=" + slabId
				+ ", slabName=" + slabName + ", countOfDisagreedRating="
				+ countOfDisagreedRating + ", slabShortName=" + slabShortName
				+ ", slabYear=" + slabYear + "]";
	}
	
	@Override
	public int compareTo(GsrDisagreeRatingForPrvsSlabsBean obj) {
		
		if(this.slabId == obj.slabId)
		return 0;
		else if(this.slabId > obj.slabId)
		return 1;
		else
		return -1;
	}
	
}
