package com.thbs.mis.gsr.bean;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class GsrEnterFeedbackForManagerBean {
	
	@NotBlank(message = "gsrGoalMgrFeedback should not be blank.")
	@NotEmpty(message = "gsrGoalMgrFeedback Description should not be empty.")
	@NotNull(message = "gsrGoalMgrFeedback should not be null.")
	private String gsrGoalMgrFeedback;

		
	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	@NotNull(message = "pkGsrDatEmpGoalId should not be null.")
	private Integer pkGsrDatEmpGoalId;
	
	@Min(1)
	@Employee_Id_Not_Found
	@NumberFormat(style = Style.NUMBER)
	@NotNull(message = "managerId should not be null.")
	private Integer managerId;

	public String getGsrGoalMgrFeedback() {
		return gsrGoalMgrFeedback;
	}

	public void setGsrGoalMgrFeedback(String gsrGoalMgrFeedback) {
		this.gsrGoalMgrFeedback = gsrGoalMgrFeedback;
	}

	public Integer getPkGsrDatEmpGoalId() {
		return pkGsrDatEmpGoalId;
	}

	public void setPkGsrDatEmpGoalId(Integer pkGsrDatEmpGoalId) {
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
	}

	public Integer getManagerId() {
		return managerId;
	}

	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}

	@Override
	public String toString() {
		return "GsrEnterFeedbackForManagerBean [gsrGoalMgrFeedback="
				+ gsrGoalMgrFeedback + ", pkGsrDatEmpGoalId="
				+ pkGsrDatEmpGoalId + ", managerId=" + managerId + "]";
	}
	
	

}
