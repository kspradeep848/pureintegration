package com.thbs.mis.gsr.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.thbs.mis.framework.util.TimeDeserializer;

public class GsrSetWorkflowDatesBean

{
	@NotNull(message = "startDate field can not be null.")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date startDate;

	@NotNull(message = "endDate field can not be null.")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date endDate;

	@Min(1)
	@NotNull(message = "pkGsrWorkflowPermissionId field can not be null.")
	@NumberFormat(style = Style.NUMBER)
	private Integer pkGsrWorkflowPermissionId;

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public Integer getPkGsrWorkflowPermissionId()
	{
		return pkGsrWorkflowPermissionId;
	}

	public void setPkGsrWorkflowPermissionId(
			Integer pkGsrWorkflowPermissionId)
	{
		this.pkGsrWorkflowPermissionId = pkGsrWorkflowPermissionId;
	}

	@Override
	public String toString()
	{
		return "GsrSetWorkflowDatesBean [startDate=" + startDate
				+ ", endDate=" + endDate
				+ ", pkGsrWorkflowPermissionId="
				+ pkGsrWorkflowPermissionId + "]";
	}
}
