package com.thbs.mis.gsr.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

public class GsrDatEmpGoalBOBeanInput implements Serializable {

	@Min(1)
	private Integer fkGsrDatEmpId;

	
	private Date gsrGoalStartDate;

	
	private Date gsrGoalEndDate;

	public Integer getFkGsrDatEmpId() {
		return fkGsrDatEmpId;
	}

	public void setFkGsrDatEmpId(int fkGsrDatEmpId) {
		this.fkGsrDatEmpId = fkGsrDatEmpId;
	}

	public Date getGsrGoalStartDate() {
		return gsrGoalStartDate;
	}

	public void setGsrGoalStartDate(Date gsrGoalStartDate) {
		this.gsrGoalStartDate = gsrGoalStartDate;
	}

	public Date getGsrGoalEndDate() {
		return gsrGoalEndDate;
	}

	public void setGsrGoalEndDate(Date gsrGoalEndDate) {
		this.gsrGoalEndDate = gsrGoalEndDate;
	}

	@Override
	public String toString() {

		return "fkGsrDatEmpId" + fkGsrDatEmpId + "gsrGoalStartDate"
				+ gsrGoalStartDate + "gsrGoalEndDate" + gsrGoalEndDate;

	}
}
