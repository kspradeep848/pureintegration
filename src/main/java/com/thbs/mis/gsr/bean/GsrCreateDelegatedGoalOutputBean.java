package com.thbs.mis.gsr.bean;

import java.util.List;

public class GsrCreateDelegatedGoalOutputBean 
{
	private List<GsrDelegateOutputBean> listofSuccessfulCreatedDelegatedGoal;
	private List<GsrDelegateOutputBean> listOfFailedCreatedDelegatedGoal;
	public List<GsrDelegateOutputBean> getListofSuccessfulCreatedDelegatedGoal() {
		return listofSuccessfulCreatedDelegatedGoal;
	}
	public void setListofSuccessfulCreatedDelegatedGoal(
			List<GsrDelegateOutputBean> listofSuccessfulCreatedDelegatedGoal) {
		this.listofSuccessfulCreatedDelegatedGoal = listofSuccessfulCreatedDelegatedGoal;
	}
	public List<GsrDelegateOutputBean> getListOfFailedCreatedDelegatedGoal() {
		return listOfFailedCreatedDelegatedGoal;
	}
	public void setListOfFailedCreatedDelegatedGoal(
			List<GsrDelegateOutputBean> listOfFailedCreatedDelegatedGoal) {
		this.listOfFailedCreatedDelegatedGoal = listOfFailedCreatedDelegatedGoal;
	}
	@Override
	public String toString() {
		return "GsrCreateDelegatedGoalOutputBean [listofSuccessfulCreatedDelegatedGoal="
				+ listofSuccessfulCreatedDelegatedGoal
				+ ", listOfFailedCreatedDelegatedGoal="
				+ listOfFailedCreatedDelegatedGoal + "]";
	}
	
	
}
