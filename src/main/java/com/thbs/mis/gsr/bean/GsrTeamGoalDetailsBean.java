/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrTeamGoalDetailsBean.java                       */
/*                                                                   */
/*  Author      : THBS                                               */
/*                                                                   */
/*  Date        :  November 04, 2016                                 */
/*                                                                   */
/*  Description :  This class contains for the inputs required for   */
/*                   GSR Team goal details	 				         */			 									
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 04, 2016     THBS     1.0        Initial version created */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;
import org.springframework.validation.annotation.Validated;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;
 
@Deprecated
@Validated
public class GsrTeamGoalDetailsBean {
	
	@Min(1)
	@NotNull(message="Manager ID should not be null")
	@NumberFormat(style = Style.NUMBER,pattern="Manager ID should be numbers")
	@Employee_Id_Not_Found
	private Integer mgrId ;
	
	
	
	@NotNull(message = "Start Date  should not be null")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date startDate;
	
	@NotNull(message = "End Date  should not be null")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(style = "yyyy-MM-dd ")
	private Date endDate;
	
	@Min(1)
	@Max(7)
	@NotNull(message = "Status  should not be null")
	private Integer status;

	public Integer getMgrId() {
		return mgrId;
	}

	public void setMgrId(Integer mgrId) {
		this.mgrId = mgrId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "GsrTeamGoalDetailsBean [mgrId=" + mgrId + ", startDate="
				+ startDate + ", endDate=" + endDate + ", status=" + status
				+ "]";
	}
	

	

	

	
}
