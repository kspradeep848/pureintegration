package com.thbs.mis.gsr.bean;

public class GsrResolvedFinalRatingReporteeListBean {
	
	private int finalRatingId;
	private int reportingMgrId;
	private int reporteeId;
	private String reporteeName;
	private float resolvedFinalRating;

	public int getFinalRatingId() {
		return finalRatingId;
	}
	public void setFinalRatingId(int finalRatingId) {
		this.finalRatingId = finalRatingId;
	}
	public int getReportingMgrId() {
		return reportingMgrId;
	}
	public void setReportingMgrId(int reportingMgrId) {
		this.reportingMgrId = reportingMgrId;
	}
	public int getReporteeId() {
		return reporteeId;
	}
	public void setReporteeId(int reporteeId) {
		this.reporteeId = reporteeId;
	}
	public String getReporteeName() {
		return reporteeName;
	}
	public void setReporteeName(String reporteeName) {
		this.reporteeName = reporteeName;
	}
	public float getResolvedFinalRating() {
		return resolvedFinalRating;
	}
	public void setResolvedFinalRating(float resolvedFinalRating) {
		this.resolvedFinalRating = resolvedFinalRating;
	}
	@Override
	public String toString() {
		return "GsrResolvedFinalRatingReporteeListBean [finalRatingId="
				+ finalRatingId + ", reportingMgrId=" + reportingMgrId
				+ ", reporteeId=" + reporteeId + ", reporteeName="
				+ reporteeName + ", resolvedFinalRating=" + resolvedFinalRating
				+ "]";
	}
	
}
