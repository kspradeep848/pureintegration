package com.thbs.mis.gsr.bean;

import java.util.List;

import com.thbs.mis.common.bean.EmployeeNameAndIdWithHierarchyFlagBean;

public class GsrHierarchyBean {
	
	private Short buId;
	private String buName;
	private Integer hierarchyLevelCounter;
	private List<EmployeeNameAndIdWithHierarchyFlagBean> empHierarchyList;
	public Short getBuId() {
		return buId;
	}
	public void setBuId(Short buId) {
		this.buId = buId;
	}
	public String getBuName() {
		return buName;
	}
	public void setBuName(String buName) {
		this.buName = buName;
	}
	public Integer getHierarchyLevelCounter() {
		return hierarchyLevelCounter;
	}
	public void setHierarchyLevelCounter(Integer hierarchyLevelCounter) {
		this.hierarchyLevelCounter = hierarchyLevelCounter;
	}
	public List<EmployeeNameAndIdWithHierarchyFlagBean> getEmpHierarchyList() {
		return empHierarchyList;
	}
	public void setEmpHierarchyList(
			List<EmployeeNameAndIdWithHierarchyFlagBean> empHierarchyList) {
		this.empHierarchyList = empHierarchyList;
	}
	@Override
	public String toString() {
		return "GsrHierarchyBean [buId=" + buId + ", buName=" + buName
				+ ", hierarchyLevelCounter=" + hierarchyLevelCounter
				+ ", empHierarchyList=" + empHierarchyList + "]";
	}
	

}
