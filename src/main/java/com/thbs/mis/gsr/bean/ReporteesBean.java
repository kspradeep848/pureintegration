package com.thbs.mis.gsr.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class ReporteesBean {

	private Integer empId;
	private String name;
	private Float finalRating;
	private Short slabId;
	private String slabName;
	private String slabShortName;
	private Date acceptedRejectedDate;
	private Integer finalRatingStatusId;
	private String finalRatingStatusName;
	private short buId;
	private String buName;
	private Integer skipLevelMgrId;
	private Integer currentRmId;
	private String skipLevelMgrName;
	private String currentMgrName;
	private Date slabStartDate;
	private Date slabEndDate;
	
	public String getSkipLevelMgrName() {
		return skipLevelMgrName;
	}

	public void setSkipLevelMgrName(String skipLevelMgrName) {
		this.skipLevelMgrName = skipLevelMgrName;
	}

	public String getCurrentMgrName() {
		return currentMgrName;
	}

	public void setCurrentMgrName(String currentMgrName) {
		this.currentMgrName = currentMgrName;
	}

	public Integer getCurrentRmId() {
		return currentRmId;
	}

	public void setCurrentRmId(Integer currentRmId) {
		this.currentRmId = currentRmId;
	}

	private List<GoalDetails> goals;

	public Integer getSkipLevelMgrId() {
		return skipLevelMgrId;
	}

	public void setSkipLevelMgrId(Integer skipLevelMgrId) {
		this.skipLevelMgrId = skipLevelMgrId;
	}

	public short getBuId() {
		return buId;
	}

	public void setBuId(short buId) {
		this.buId = buId;
	}

	public String getBuName() {
		return buName;
	}

	public void setBuName(String buName) {
		this.buName = buName;
	}

	public Integer getFinalRatingStatusId() {
		return finalRatingStatusId;
	}

	public void setFinalRatingStatusId(Integer finalRatingStatusId) {
		this.finalRatingStatusId = finalRatingStatusId;
	}

	public String getFinalRatingStatusName() {
		return finalRatingStatusName;
	}

	public void setFinalRatingStatusName(String finalRatingStatusName) {
		this.finalRatingStatusName = finalRatingStatusName;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getFinalRating() {
		return finalRating;
	}

	public void setFinalRating(Float finalRating) {
		this.finalRating = finalRating;
	}

	public Short getSlabId() {
		return slabId;
	}

	public void setSlabId(Short slabId) {
		this.slabId = slabId;
	}

	public String getSlabName() {
		return slabName;
	}

	public void setSlabName(String slabName) {
		this.slabName = slabName;
	}

	public String getSlabShortName() {
		return slabShortName;
	}

	public void setSlabShortName(String slabShortName) {
		this.slabShortName = slabShortName;
	}

	public Date getAcceptedRejectedDate() {
		return acceptedRejectedDate;
	}

	public void setAcceptedRejectedDate(Date acceptedRejectedDate) {
		this.acceptedRejectedDate = acceptedRejectedDate;
	}

	public List<GoalDetails> getGoals() {
		return goals;
	}

	public void setGoals(List<GoalDetails> goals) {
		this.goals = goals;
	}
	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getSlabStartDate() {
		return slabStartDate;
	}

	public void setSlabStartDate(Date slabStartDate) {
		this.slabStartDate = slabStartDate;
	}
	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getSlabEndDate() {
		return slabEndDate;
	}

	public void setSlabEndDate(Date slabEndDate) {
		this.slabEndDate = slabEndDate;
	}

	@Override
	public String toString() {
		return "ReporteesBean [empId=" + empId + ", name=" + name
				+ ", finalRating=" + finalRating + ", slabId=" + slabId
				+ ", slabName=" + slabName + ", slabShortName=" + slabShortName
				+ ", acceptedRejectedDate=" + acceptedRejectedDate
				+ ", finalRatingStatusId=" + finalRatingStatusId
				+ ", finalRatingStatusName=" + finalRatingStatusName
				+ ", buId=" + buId + ", buName=" + buName + ", skipLevelMgrId="
				+ skipLevelMgrId + ", currentRmId=" + currentRmId
				+ ", skipLevelMgrName=" + skipLevelMgrName
				+ ", currentMgrName=" + currentMgrName + ", slabStartDate="
				+ slabStartDate + ", slabEndDate=" + slabEndDate + ", goals="
				+ goals + "]";
	}

}
