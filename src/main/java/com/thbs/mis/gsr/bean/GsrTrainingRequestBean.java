/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrTrainingRequestBean.java                       */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  12-Dec-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 12-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.gsr.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class GsrTrainingRequestBean {

	private int pkGsrTrainingRequestId;

	@NumberFormat(style = Style.NUMBER)
	@Min(1)
	private Integer fkGsrTrainingTopicId;

	@NumberFormat(style = Style.NUMBER)
	@Min(1)
	private Integer fkTrainingRequestedForEmpId;

	private String otherTopicDescription;

	private String comments;

	@NumberFormat(style = Style.NUMBER)
	@Min(1)
	private int gsrTrainingRequestedBy;

	@NotNull(message = "RequestedDate field can not be null")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date gsrTrainingRequestedDate;

	private Date startDate;

	private Date endDate;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getPkGsrTrainingRequestId() {
		return pkGsrTrainingRequestId;
	}

	public void setPkGsrTrainingRequestId(int pkGsrTrainingRequestId) {
		this.pkGsrTrainingRequestId = pkGsrTrainingRequestId;
	}

	public Integer getFkGsrTrainingTopicId() {
		return fkGsrTrainingTopicId;
	}

	public void setFkGsrTrainingTopicId(Integer fkGsrTrainingTopicId) {
		this.fkGsrTrainingTopicId = fkGsrTrainingTopicId;
	}

	public int getFkTrainingRequestedForEmpId() {
		return fkTrainingRequestedForEmpId;
	}

	public void setFkTrainingRequestedForEmpId(int fkTrainingRequestedForEmpId) {
		this.fkTrainingRequestedForEmpId = fkTrainingRequestedForEmpId;
	}

	public String getOtherTopicDescription() {
		return otherTopicDescription;
	}

	public void setOtherTopicDescription(String otherTopicDescription) {
		this.otherTopicDescription = otherTopicDescription;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getGsrTrainingRequestedBy() {
		return gsrTrainingRequestedBy;
	}

	public void setGsrTrainingRequestedBy(int gsrTrainingRequestedBy) {
		this.gsrTrainingRequestedBy = gsrTrainingRequestedBy;
	}

	public Date getGsrTrainingRequestedDate() {
		return gsrTrainingRequestedDate;
	}

	public void setGsrTrainingRequestedDate(Date gsrTrainingRequestedDate) {
		this.gsrTrainingRequestedDate = gsrTrainingRequestedDate;
	}

	public String toString() {
		return "GsrTrainingRequestBean " + "[pkGsrTrainingRequestId="
				+ pkGsrTrainingRequestId + ", fkGsrTrainingTopicId="
				+ fkGsrTrainingTopicId + ", fkTrainingRequestedForEmpId="
				+ fkTrainingRequestedForEmpId + ", otherTopicDescription="
				+ otherTopicDescription + ", comments=" + comments
				+ ", gsrTrainingRequestedBy=" + gsrTrainingRequestedBy
				+ ", gsrTrainingRequestedDate=" + gsrTrainingRequestedDate
				+ ", getPkGsrTrainingRequestId()="
				+ getPkGsrTrainingRequestId() + ", getFkGsrTrainingTopicId()="
				+ getFkGsrTrainingTopicId()
				+ ", getFkTrainingRequestedForEmpId()="
				+ getFkTrainingRequestedForEmpId()
				+ ", getOtherTopicDescription()=" + getOtherTopicDescription()
				+ ", getComments()=" + getComments()
				+ ", getGsrTrainingRequestedBy()="
				+ getGsrTrainingRequestedBy()
				+ ", getGsrTrainingRequestedDate()="
				+ getGsrTrainingRequestedDate() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

}
