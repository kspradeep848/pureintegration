package com.thbs.mis.gsr.bean;

import java.util.Arrays;

public class GsrReportFilterInputBean {

	String roleName;
	String[] filterIds;
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String[] getFilterIds() {
		return filterIds;
	}
	public void setFilterIds(String[] filterIds) {
		this.filterIds = filterIds;
	}
	@Override
	public String toString() {
		return "GsrReportFilterInputBean [roleName=" + roleName
				+ ", filterIds=" + Arrays.toString(filterIds) + "]";
	}
	
	
}
