/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  HrViewEmpGoalsInputBean.java                      */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  March 29, 2017                                    */
/*                                                                   */
/*  Description :  This class contains input fields required for     */
/*                 view GSR Employee goal details	 		         */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 02, 2016     THBS     1.0        Initial version created */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

import java.util.List;

import com.thbs.mis.framework.customValidation.annotation.common.MIS_Id_Not_Found_BusinessUnit;
import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_GoalStatus;
import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_Slab;

public class HrViewEmpGoalsInputBean
{
	private List<Integer> empIdList;

	@GSR_Id_Not_Found_GoalStatus
	private Byte goalStatus;

	@GSR_Id_Not_Found_Slab
	private Short slabId;

	@MIS_Id_Not_Found_BusinessUnit
	private Short buId;

	public List<Integer> getEmpIdList()
	{
		return empIdList;
	}

	public void setEmpIdList(List<Integer> empIdList)
	{
		this.empIdList = empIdList;
	}

	public Byte getGoalStatus()
	{
		return goalStatus;
	}

	public void setGoalStatus(Byte goalStatus)
	{
		this.goalStatus = goalStatus;
	}

	public Short getSlabId()
	{
		return slabId;
	}

	public void setSlabId(Short slabId)
	{
		this.slabId = slabId;
	}

	public Short getBuId()
	{
		return buId;
	}

	public void setBuId(Short buId)
	{
		this.buId = buId;
	}

	@Override
	public String toString()
	{
		return "HrViewEmpGoalsInputBean [empIdList=" + empIdList
				+ ", goalStatus=" + goalStatus + ", slabId=" + slabId
				+ ", buId=" + buId + "]";
	}

}
