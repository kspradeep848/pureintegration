/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrFetchPastGoalsForEmployeeOutputBean.java       */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  08-May-2017                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	GsrFetchPastGoalsForEmployeeOutputBean.          */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.gsr.bean;

import java.util.List;

public class GsrFetchPastGoalsForEmployeeOutputBean {

	List<GsrKpiGoalsBean> kpiGoals;
	List<ViewEmployeeGoalsOutputBean> pastGoals;

	public List<ViewEmployeeGoalsOutputBean> getPastGoals() {
		return pastGoals;
	}

	public void setPastGoals(List<ViewEmployeeGoalsOutputBean> pastGoals) {
		this.pastGoals = pastGoals;
	}

	public List<GsrKpiGoalsBean> getKpiGoals() {
		return kpiGoals;
	}

	public void setKpiGoals(List<GsrKpiGoalsBean> kpiGoals) {
		this.kpiGoals = kpiGoals;
	}
	
	
	
}
