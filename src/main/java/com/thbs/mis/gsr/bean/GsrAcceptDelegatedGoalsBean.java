/*********************************************************************/
/*                           FILE HEADER                                                                                                   */
/********************************************************************/
/*                                                                                                                                                          */
/*  FileName    :  GsrAcceptDelegatedGoalsBean.java           		                                  */
/*                                                                                                                                                        */
/*  Author      :  THBS                                                                                                                  */
/*                                                                                                                                                       */
/*  Date        :  24-Nov-2016                                                                                                    */
/*                                                                                                                                                      */
/*  Description :  Bean class for GSR Accept Delagated Goals.   			                 */
/*                 			                                                                                                                            */
/*                                                                                                                                                       */
/*********************************************************************/
/* Date            Who     Version      Comments             	                                                           */
/*-------------------------------------------------------------------                                         */
/* 24-Nov-2016    THBS     1.0         Initial version created   	                                        */
/*********************************************************************/

package com.thbs.mis.gsr.bean;

import java.util.List;

public class GsrAcceptDelegatedGoalsBean {
	

	private byte status;
	List<Integer> goalId;
	
	public List<Integer> getGoalId() {
		return goalId;
	}

	public void setGoalId(List<Integer> goalId) {
		this.goalId = goalId;
	}

	
	
	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "GsrAcceptDelegatedGoalsBean [status=" + status + ", goalId="
				+ goalId + "]";
	}

	

	
	
	
}
