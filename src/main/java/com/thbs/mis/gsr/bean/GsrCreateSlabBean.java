/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrCreateCommonGoalsBean.java                     */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	GsrCreateSlabBean                                 */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class GsrCreateSlabBean
{

	@NotBlank(message = "gsrSlabName should not be blank.")
	@NotEmpty(message = "gsrSlabName Description should not be empty.")
	@NotNull(message = "gsrSlabName Description should not be null.")
	@Length(min=3,max=50, message = "gsrSlabName field shoud be minimum 3 chars and can'texceed 50 chars.")
	private String gsrSlabName;
	
	@JsonSerialize(using = DateSerializer.class)
    @NotNull(message = "gsrSlabStartDate field can not be null.")
	private Date gsrSlabStartDate;
	
	@JsonSerialize(using = DateSerializer.class)
	@NotNull(message = "gsrSlabEndDate field can not be null.")
	private Date gsrSlabEndDate;
	
    @NotEmpty(message = "gsrSlabYear Description should not be empty.")
	@NotBlank(message = "gsrSlabYear should not be blank.")
	@NotNull(message = "gsrSlabYear Description should not be null.")
	private String gsrSlabYear;
    
    
    @NotEmpty(message = "gsrSlabShortName Description should not be empty.")
	@NotBlank(message = "gsrSlabShortName should not be blank.")
	@NotNull(message = "gsrSlabShortName Description should not be null.")
    private String gsrSlabShortName;
    
    @NotEmpty(message = "gsrSlabEnabled Description should not be empty.")
   	@NotBlank(message = "gsrSlabEnabled should not be blank.")
   	@NotNull(message = "gsrSlabEnabled Description should not be null.")
    private String gsrSlabEnabled;
	
	public Date getGsrSlabEndDate()
	{
		return gsrSlabEndDate;
	}

	public String getGsrSlabEnabled() {
		return gsrSlabEnabled;
	}

	public void setGsrSlabEnabled(String gsrSlabEnabled) {
		this.gsrSlabEnabled = gsrSlabEnabled;
	}

	public void setGsrSlabEndDate(Date gsrSlabEndDate)
	{
		this.gsrSlabEndDate = gsrSlabEndDate;
	}

	public String getGsrSlabShortName() {
		return gsrSlabShortName;
	}

	public void setGsrSlabShortName(String gsrSlabShortName) {
		this.gsrSlabShortName = gsrSlabShortName;
	}

	public String getGsrSlabName()
	{
		return gsrSlabName;
	}

	public void setGsrSlabName(String gsrSlabName)
	{
		this.gsrSlabName = gsrSlabName;
	}
	
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getGsrSlabStartDate()
	{
		return gsrSlabStartDate;
	}

	public void setGsrSlabStartDate(Date gsrSlabStartDate)
	{
		this.gsrSlabStartDate = gsrSlabStartDate;
	}

	public String getGsrSlabYear()
	{
		return gsrSlabYear;
	}

	public void setGsrSlabYear(String gsrSlabYear)
	{
		this.gsrSlabYear = gsrSlabYear;
	}

	@Override
	public String toString() {
		return "GsrCreateSlabBean [gsrSlabName=" + gsrSlabName
				+ ", gsrSlabStartDate=" + gsrSlabStartDate
				+ ", gsrSlabEndDate=" + gsrSlabEndDate + ", gsrSlabYear="
				+ gsrSlabYear + ", gsrSlabShortName=" + gsrSlabShortName
				+ ", gsrSlabEnabled=" + gsrSlabEnabled + "]";
	}

	
}
