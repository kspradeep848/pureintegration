package com.thbs.mis.gsr.bean;

import java.util.List;

public class DelegationGoalInputBeanHR {

	private List<Integer> empIdList;
	

	public List<Integer> getEmpIdList() {
		return empIdList;
	}

	public void setEmpIdList(List<Integer> empIdList) {
		this.empIdList = empIdList;
	}

	@Override
	public String toString() {
		return "DelegationGoalInputBeanHR [empIdList=" + empIdList + "]";
	}

}
