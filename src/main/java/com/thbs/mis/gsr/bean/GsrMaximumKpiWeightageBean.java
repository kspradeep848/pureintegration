package com.thbs.mis.gsr.bean;

import java.util.List;

public class GsrMaximumKpiWeightageBean {
	
	private List<Integer> empIdList;
	private Short slabId;
	private Integer mgrId;
	public Integer getMgrId() {
		return mgrId;
	}
	public void setMgrId(Integer mgrId) {
		this.mgrId = mgrId;
	}
	public List<Integer> getEmpIdList() {
		return empIdList;
	}
	public void setEmpIdList(List<Integer> empIdList) {
		this.empIdList = empIdList;
	}
	public Short getSlabId() {
		return slabId;
	}
	public void setSlabId(Short slabId) {
		this.slabId = slabId;
	}
	@Override
	public String toString() {
		return "GsrMaximumKpiWeightageBean [empIdList=" + empIdList
				+ ", slabId=" + slabId + ", mgrId=" + mgrId + "]";
	}
	
	

}
