package com.thbs.mis.gsr.bean;

import java.util.List;

public class FetchPastGoalsForEmployeeListInputBean {

	
	private List<Integer> empId;

	public List<Integer> getEmpId() {
		return empId;
	}

	public void setEmpId(List<Integer> empId) {
		this.empId = empId;
	}

	@Override
	public String toString() {
		return "FetchPastGoalsForEmployeeListInputBean [empId=" + empId + "]";
	}
	
	
}
