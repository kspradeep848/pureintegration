package com.thbs.mis.gsr.bean;

import java.util.List;

public class GSRReportGenerateBean {
	List<String[]> reportData;
	List<String> headers;
	String fileType;
	public List<String[]> getReportData() {
		return reportData;
	}
	public void setReportData(List<String[]> reportData) {
		this.reportData = reportData;
	}
	public List<String> getHeaders() {
		return headers;
	}
	public void setHeaders(List<String> headers) {
		this.headers = headers;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	@Override
	public String toString() {
		return "GSRReportGenerateBean [reportData=" + reportData + ", headers="
				+ headers + ", fileType=" + fileType + "]";
	}
	 
	 
	 
	 
	
}
