/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  ViewEmployeeGoalsOutputBean.java                  */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  March 28, 2017                                    */
/*                                                                   */
/*  Description :  This class contains fields required for view      */
/*                   GSR Employee goal details	 				     */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 02, 2016     THBS     1.0        Initial version created */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class ViewEmployeeGoalsOutputBean {

	private int goalId;
	private Integer empId;
	private String empName;
	private String goalName;
	private String goalWeightage;
	private String goalDuration;
	private String goalDescription;
	private String goalMeasurement;
	private String accomplishments;
	private String selfRating;
	private String mgrName;
	private Integer mgrId;
	private Integer currentReportingMgrId;
	private String mgrFeedback;
	private String mgrRating;
	private Short slabId;
	private String slabYear;
	private Byte goalStatusId;
	private String goalStatusName;
	private Integer fkGsrDelegatedGoalId;
	private Integer fkMgrIdDelegatedTo;
	private Integer fkGsrDelegatedFromMgrId;
	private Byte delegationStatus;
	private Integer skipLevelMgrId;
	private Byte kpiGoalWeightage;
	private String slabShortName;
	private Short kpiGoalId;
	private String delegatedMgrName;
	private String gsrGoalModifiedByName;
	private Integer gsrGoalModifiedBy;
	//Added by Shyam
	private boolean rmChangeStatus;
	//EOA by Shyam
	
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date gsrGoalModifiedDate;
	
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date slabStartDate;
	
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date slabEndDate;
	
	
	public String getGsrGoalModifiedByName() {
		return gsrGoalModifiedByName;
	}

	public void setGsrGoalModifiedByName(String gsrGoalModifiedByName) {
		this.gsrGoalModifiedByName = gsrGoalModifiedByName;
	}

	public String getDelegatedMgrName() {
		return delegatedMgrName;
	}

	public void setDelegatedMgrName(String delegatedMgrName) {
		this.delegatedMgrName = delegatedMgrName;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	

	public Date getSlabStartDate() {
		return slabStartDate;
	}

	public void setSlabStartDate(Date slabStartDate) {
		this.slabStartDate = slabStartDate;
	}

	public Date getSlabEndDate() {
		return slabEndDate;
	}

	public void setSlabEndDate(Date slabEndDate) {
		this.slabEndDate = slabEndDate;
	}

	public Integer getGsrGoalModifiedBy() {
		return gsrGoalModifiedBy;
	}

	public void setGsrGoalModifiedBy(Integer gsrGoalModifiedBy) {
		this.gsrGoalModifiedBy = gsrGoalModifiedBy;
	}

	public Date getGsrGoalModifiedDate() {
		return gsrGoalModifiedDate;
	}

	public void setGsrGoalModifiedDate(Date gsrGoalModifiedDate) {
		this.gsrGoalModifiedDate = gsrGoalModifiedDate;
	}

	public Integer getCurrentReportingMgrId() {
		return currentReportingMgrId;
	}

	public void setCurrentReportingMgrId(Integer currentReportingMgrId) {
		this.currentReportingMgrId = currentReportingMgrId;
	}


	//Final rating related data
	private String finalRating;
	private String finalRatingStatus;
	private String isOverridden;
	private Date ratingAcceptedRejectedDate;
	private Integer overridenBy;
	private String overridenByName;
	private Date finalRatingModifiedDate;
	private List<FeedbackListBean> feedbackList;
	private Integer closedBy;
	private String closedByName;
	private Boolean isLastTwoWeeksCheck;
	private Short empGoalKpiWeightageId;//Not used in view gsr goal service, commented by Shyam
	private short buId;
	
	
	public short getBuId() {
		return buId;
	}

	public void setBuId(short buId) {
		this.buId = buId;
	}

	public Integer getFkGsrDelegatedFromMgrId() {
		return fkGsrDelegatedFromMgrId;
	}

	public void setFkGsrDelegatedFromMgrId(Integer fkGsrDelegatedFromMgrId) {
		this.fkGsrDelegatedFromMgrId = fkGsrDelegatedFromMgrId;
	}

	public Byte getDelegationStatus() {
		return delegationStatus;
	}

	public void setDelegationStatus(Byte delegationStatus) {
		this.delegationStatus = delegationStatus;
	}

	public Integer getFkGsrDelegatedGoalId() {
		return fkGsrDelegatedGoalId;
	}

	public void setFkGsrDelegatedGoalId(Integer fkGsrDelegatedGoalId) {
		this.fkGsrDelegatedGoalId = fkGsrDelegatedGoalId;
	}

	public Integer getFkMgrIdDelegatedTo() {
		return fkMgrIdDelegatedTo;
	}

	public void setFkMgrIdDelegatedTo(Integer fkMgrIdDelegatedTo) {
		this.fkMgrIdDelegatedTo = fkMgrIdDelegatedTo;
	}

	public Integer getSkipLevelMgrId() {
		return skipLevelMgrId;
	}

	public void setSkipLevelMgrId(Integer skipLevelMgrId) {
		this.skipLevelMgrId = skipLevelMgrId;
	}

	public Short getEmpGoalKpiWeightageId() {
		return empGoalKpiWeightageId;
	}

	public void setEmpGoalKpiWeightageId(Short empGoalKpiWeightageId) {
		this.empGoalKpiWeightageId = empGoalKpiWeightageId;
	}

	public Boolean getIsLastTwoWeeksCheck() {
		return isLastTwoWeeksCheck;
	}

	public void setIsLastTwoWeeksCheck(Boolean isLastTwoWeeksCheck) {
		this.isLastTwoWeeksCheck = isLastTwoWeeksCheck;
	}

	public Integer getClosedBy() {
		return closedBy;
	}

	public void setClosedBy(Integer closedBy) {
		this.closedBy = closedBy;
	}

	public String getClosedByName() {
		return closedByName;
	}

	public void setClosedByName(String closedByName) {
		this.closedByName = closedByName;
	}

	public List<FeedbackListBean> getFeedbackList() {
		return feedbackList;
	}

	public void setFeedbackList(List<FeedbackListBean> feedbackList) {
		this.feedbackList = feedbackList;
	}

	public String getSlabYear() {
		return slabYear;
	}

	public void setSlabYear(String slabYear) {
		this.slabYear = slabYear;
	}

	public String getSlabShortName() {
		return slabShortName;
	}

	public void setSlabShortName(String slabShortName) {
		this.slabShortName = slabShortName;
	}

	public Short getKpiGoalId() {
		return kpiGoalId;
	}

	public void setKpiGoalId(Short kpiGoalId) {
		this.kpiGoalId = (kpiGoalId == null) ? 0 : kpiGoalId;
	}

	public Byte getKpiGoalWeightage() {
		return kpiGoalWeightage;
	}

	public void setKpiGoalWeightage(Byte kpiGoalWeightage) {
		if (kpiGoalWeightage == null) {
			this.kpiGoalWeightage = 0;
		} else {
			this.kpiGoalWeightage = kpiGoalWeightage;
		}
	}

	public String getGoalName() {
		return goalName;
	}

	public void setGoalName(String goalName) {
		this.goalName = goalName;
	}

	public String getGoalWeightage() {
		return goalWeightage;
	}

	public void setGoalWeightage(String goalWeightage) {
		this.goalWeightage = goalWeightage;
	}

	public String getGoalDuration() {
		return goalDuration;
	}

	public void setGoalDuration(String goalDuration) {
		this.goalDuration = goalDuration;
	}

	public String getMgrName() {
		return mgrName;
	}

	public void setMgrName(String mgrName) {
		this.mgrName = mgrName;
	}

	public Integer getMgrId() {
		return mgrId;
	}

	public void setMgrId(Integer mgrId) {
		this.mgrId = mgrId;
	}

	public String getGoalDescription() {
		return goalDescription;
	}

	public void setGoalDescription(String goalDescription) {
		this.goalDescription = goalDescription;
	}

	public String getGoalMeasurement() {
		return goalMeasurement;
	}

	public void setGoalMeasurement(String goalMeasurement) {
		this.goalMeasurement = goalMeasurement;
	}

	public String getSelfRating() {
		return selfRating;
	}

	public void setSelfRating(String selfRating) {
		this.selfRating = selfRating;
	}

	public String getAccomplishments() {
		return accomplishments;
	}

	public void setAccomplishments(String accomplishments) {
		this.accomplishments = accomplishments;
	}

	public String getMgrFeedback() {
		return mgrFeedback;
	}

	public void setMgrFeedback(String mgrFeedback) {
		this.mgrFeedback = mgrFeedback;
	}

	public String getMgrRating() {
		return mgrRating;
	}

	public void setMgrRating(String mgrRating) {
		this.mgrRating = mgrRating;
	}

	public String getFinalRating() {
		return finalRating;
	}

	public void setFinalRating(String finalRating) {
		this.finalRating = finalRating;
	}

	public Short getSlabId() {
		return slabId;
	}

	public void setSlabId(Short slabId) {
		this.slabId = slabId;
	}

	public String getFinalRatingStatus() {
		return finalRatingStatus;
	}

	public void setFinalRatingStatus(String finalRatingStatus) {
		this.finalRatingStatus = finalRatingStatus;
	}

	public String getIsOverridden() {
		return isOverridden;
	}

	public void setIsOverridden(String isOverridden) {
		this.isOverridden = isOverridden;
	}

	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getRatingAcceptedRejectedDate() {
		return ratingAcceptedRejectedDate;
	}

	public void setRatingAcceptedRejectedDate(Date ratingAcceptedRejectedDate) {
		this.ratingAcceptedRejectedDate = ratingAcceptedRejectedDate;
	}

	public int getGoalStatusId() {
		return goalStatusId;
	}

	public void setGoalStatusId(Byte goalStatusId) {
		this.goalStatusId = goalStatusId;
	}

	public String getGoalStatusName() {
		return goalStatusName;
	}

	public void setGoalStatusName(String goalStatusName) {
		this.goalStatusName = goalStatusName;
	}

	public int getGoalId() {
		return goalId;
	}

	public void setGoalId(int goalId) {
		this.goalId = goalId;
	}

	public Integer getOverridenBy() {
		return overridenBy;
	}

	public void setOverridenBy(Integer overridenBy) {
		this.overridenBy = overridenBy;
	}

	public String getOverridenByName() {
		return overridenByName;
	}

	public void setOverridenByName(String overridenByName) {
		this.overridenByName = overridenByName;
	}

	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getFinalRatingModifiedDate() {
		return finalRatingModifiedDate;
	}

	public void setFinalRatingModifiedDate(Date finalRatingModifiedDate) {
		this.finalRatingModifiedDate = finalRatingModifiedDate;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public boolean isRmChangeStatus() {
		return rmChangeStatus;
	}

	public void setRmChangeStatus(boolean rmChangeStatus) {
		this.rmChangeStatus = rmChangeStatus;
	}

	@Override
	public String toString() {
		return "ViewEmployeeGoalsOutputBean [goalId=" + goalId + ", empId="
				+ empId + ", empName=" + empName + ", goalName=" + goalName
				+ ", goalWeightage=" + goalWeightage + ", goalDuration="
				+ goalDuration + ", goalDescription=" + goalDescription
				+ ", goalMeasurement=" + goalMeasurement + ", accomplishments="
				+ accomplishments + ", selfRating=" + selfRating + ", mgrName="
				+ mgrName + ", mgrId=" + mgrId + ", currentReportingMgrId="
				+ currentReportingMgrId + ", mgrFeedback=" + mgrFeedback
				+ ", mgrRating=" + mgrRating + ", slabId=" + slabId
				+ ", slabYear=" + slabYear + ", goalStatusId=" + goalStatusId
				+ ", goalStatusName=" + goalStatusName
				+ ", fkGsrDelegatedGoalId=" + fkGsrDelegatedGoalId
				+ ", fkMgrIdDelegatedTo=" + fkMgrIdDelegatedTo
				+ ", fkGsrDelegatedFromMgrId=" + fkGsrDelegatedFromMgrId
				+ ", delegationStatus=" + delegationStatus
				+ ", skipLevelMgrId=" + skipLevelMgrId + ", kpiGoalWeightage="
				+ kpiGoalWeightage + ", slabShortName=" + slabShortName
				+ ", kpiGoalId=" + kpiGoalId + ", delegatedMgrName="
				+ delegatedMgrName + ", gsrGoalModifiedByName="
				+ gsrGoalModifiedByName + ", gsrGoalModifiedBy="
				+ gsrGoalModifiedBy + ", rmChangeStatus=" + rmChangeStatus
				+ ", gsrGoalModifiedDate=" + gsrGoalModifiedDate
				+ ", slabStartDate=" + slabStartDate + ", slabEndDate="
				+ slabEndDate + ", finalRating=" + finalRating
				+ ", finalRatingStatus=" + finalRatingStatus
				+ ", isOverridden=" + isOverridden
				+ ", ratingAcceptedRejectedDate=" + ratingAcceptedRejectedDate
				+ ", overridenBy=" + overridenBy + ", overridenByName="
				+ overridenByName + ", finalRatingModifiedDate="
				+ finalRatingModifiedDate + ", feedbackList=" + feedbackList
				+ ", closedBy=" + closedBy + ", closedByName=" + closedByName
				+ ", isLastTwoWeeksCheck=" + isLastTwoWeeksCheck
				+ ", empGoalKpiWeightageId=" + empGoalKpiWeightageId
				+ ", buId=" + buId + "]";
	}

}
