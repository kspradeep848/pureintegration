package com.thbs.mis.gsr.bean;

import java.util.Date;

public class GoalDetails {

	private Integer goalId;
	private String goalName;
	private Date goalStartDate;
	private Date goalEndDate;
	private Integer goalWeightage;
	private String goalDescription;
	private String goalMeasurement;
	private String goalAcomplishment;
	private String mgrFeedback;
	private Float goalSelfRating;
	private Float goalMgrRating;
	private Integer goalStatusId;
	private String goalStatusName;
	private Integer empId;
	private Short slabId;
	private Short kpiGoalId;
	private Byte kpiGoalWeightage;
	private Integer fkGsrDelegatedGoalId;
	private Integer delegatedToMgrId;
	private Integer delegatedFromMgrId;
	private Byte delegationStatus;
	private Integer mgrId;
	private String delegatedToMgrName;
	private String delegatedFromMgrName;

	public String getDelegatedToMgrName() {
		return delegatedToMgrName;
	}

	public void setDelegatedToMgrName(String delegatedToMgrName) {
		this.delegatedToMgrName = delegatedToMgrName;
	}

	public String getDelegatedFromMgrName() {
		return delegatedFromMgrName;
	}

	public void setDelegatedFromMgrName(String delegatedFromMgrName) {
		this.delegatedFromMgrName = delegatedFromMgrName;
	}

	public Integer getDelegatedToMgrId() {
		return delegatedToMgrId;
	}

	public void setDelegatedToMgrId(Integer delegatedToMgrId) {
		this.delegatedToMgrId = delegatedToMgrId;
	}

	public Integer getDelegatedFromMgrId() {
		return delegatedFromMgrId;
	}

	public void setDelegatedFromMgrId(Integer delegatedFromMgrId) {
		this.delegatedFromMgrId = delegatedFromMgrId;
	}

	public Byte getDelegationStatus() {
		return delegationStatus;
	}

	public void setDelegationStatus(Byte delegationStatus) {
		this.delegationStatus = delegationStatus;
	}

	public Integer getFkGsrDelegatedGoalId() {
		return fkGsrDelegatedGoalId;
	}

	public void setFkGsrDelegatedGoalId(Integer fkGsrDelegatedGoalId) {
		this.fkGsrDelegatedGoalId = fkGsrDelegatedGoalId;
	}

	public Integer getGoalId() {
		return goalId;
	}

	public void setGoalId(Integer goalId) {
		
		this.goalId = goalId;
	}

	public String getGoalName() {
		return goalName;
	}

	public void setGoalName(String goalName) {
		this.goalName = goalName;
	}

	public Date getGoalStartDate() {
		return goalStartDate;
	}

	public void setGoalStartDate(Date goalStartDate) {
		this.goalStartDate = goalStartDate;
	}

	public Date getGoalEndDate() {
		return goalEndDate;
	}

	public void setGoalEndDate(Date goalEndDate) {
		this.goalEndDate = goalEndDate;
	}

	public Integer getGoalWeightage() {
		return goalWeightage;
	}

	public void setGoalWeightage(Integer goalWeightage) {
		this.goalWeightage = goalWeightage;
	}

	public String getGoalDescription() {
		return goalDescription;
	}

	public void setGoalDescription(String goalDescription) {
		this.goalDescription = goalDescription;
	}

	public String getGoalMeasurement() {
		return goalMeasurement;
	}

	public void setGoalMeasurement(String goalMeasurement) {
		this.goalMeasurement = goalMeasurement;
	}

	public String getGoalAcomplishment() {
		return goalAcomplishment;
	}

	public void setGoalAcomplishment(String goalAcomplishment) {
		this.goalAcomplishment = goalAcomplishment;
	}

	public String getMgrFeedback() {
		return mgrFeedback;
	}

	public void setMgrFeedback(String mgrFeedback) {
		this.mgrFeedback = mgrFeedback;
	}

	public Float getGoalSelfRating() {
		return goalSelfRating;
	}

	public void setGoalSelfRating(Float goalSelfRating) {
		this.goalSelfRating = goalSelfRating;
	}

	public Float getGoalMgrRating() {
		return goalMgrRating;
	}

	public void setGoalMgrRating(Float goalMgrRating) {
		this.goalMgrRating = goalMgrRating;
	}

	public Integer getGoalStatusId() {
		return goalStatusId;
	}

	public void setGoalStatusId(Integer goalStatusId) {
		this.goalStatusId = goalStatusId;
	}

	public String getGoalStatusName() {
		return goalStatusName;
	}

	public void setGoalStatusName(String goalStatusName) {
		this.goalStatusName = goalStatusName;
	}

	public Short getKpiGoalId() {
		return kpiGoalId;
	}

	public void setKpiGoalId(Short kpiGoalId) {
		this.kpiGoalId = (kpiGoalId == null) ? 0 : kpiGoalId;
	}

	public Byte getKpiGoalWeightage() {
		return kpiGoalWeightage;
	}

	public void setKpiGoalWeightage(Byte kpiGoalWeightage) {
		if (kpiGoalWeightage == null) {
			this.kpiGoalWeightage = 0;
		} else {
			this.kpiGoalWeightage = kpiGoalWeightage;
		}
	}

	public Integer getMgrId() {
		return mgrId;
	}

	public void setMgrId(Integer mgrId) {
		this.mgrId = mgrId;
	}

	@Override
	public String toString() {
		return "GoalDetails [goalId=" + goalId + ", goalName=" + goalName
				+ ", goalStartDate=" + goalStartDate + ", goalEndDate="
				+ goalEndDate + ", goalWeightage=" + goalWeightage
				+ ", goalDescription=" + goalDescription + ", goalMeasurement="
				+ goalMeasurement + ", goalAcomplishment=" + goalAcomplishment
				+ ", mgrFeedback=" + mgrFeedback + ", goalSelfRating="
				+ goalSelfRating + ", goalMgrRating=" + goalMgrRating
				+ ", goalStatusId=" + goalStatusId + ", goalStatusName="
				+ goalStatusName + ", empId=" + empId + ", slabId=" + slabId
				+ ", kpiGoalId=" + kpiGoalId + ", kpiGoalWeightage="
				+ kpiGoalWeightage + ", fkGsrDelegatedGoalId="
				+ fkGsrDelegatedGoalId + ", delegatedToMgrId="
				+ delegatedToMgrId + ", delegatedFromMgrId="
				+ delegatedFromMgrId + ", delegationStatus=" + delegationStatus
				+ ", mgrId=" + mgrId + ", delegatedToMgrName="
				+ delegatedToMgrName + ", delegatedFromMgrName="
				+ delegatedFromMgrName + "]";
	}

}
