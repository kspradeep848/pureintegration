package com.thbs.mis.gsr.bean;

import java.util.List;

public class GsrReporteesBean {

	private GsrReporteesDashBoardBean reporteesWorkFlowDetails;
	private List<GsrEmployeesRatingBean> empRatings;
	public GsrReporteesDashBoardBean getReporteesWorkFlowDetails() {
		return reporteesWorkFlowDetails;
	}
	public void setReporteesWorkFlowDetails(
			GsrReporteesDashBoardBean reporteesWorkFlowDetails) {
		this.reporteesWorkFlowDetails = reporteesWorkFlowDetails;
	}
	public List<GsrEmployeesRatingBean> getEmpRatings() {
		return empRatings;
	}
	public void setEmpRatings(List<GsrEmployeesRatingBean> empRatings) {
		this.empRatings = empRatings;
	}
	@Override
	public String toString() {
		return "GsrReporteesBean [reporteesWorkFlowDetails="
				+ reporteesWorkFlowDetails + ", empRatings=" + empRatings + "]";
	}
	
	
}
