package com.thbs.mis.gsr.bean;

 

import java.util.List;

public class GSRReportOutputBean { 
	List<String> Headers;
	List<List<String>> cells;
	public List<String> getHeaders() {
		return Headers;
	}
	public void setHeaders(List<String> headers) {
		Headers = headers;
	}
	public List<List<String>> getCells() {
		return cells;
	}
	public void setCells(List<List<String>> cells) {
		this.cells = cells;
	}
	@Override
	public String toString() {
		return "GSRReportOutputBean [Headers=" + Headers + ", cells=" + cells
				+ "]";
	}
 
	
	
}

