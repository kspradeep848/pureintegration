package com.thbs.mis.gsr.bean;

import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_Slab;
import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class ForceClosedGoalsInputBean {

	@Employee_Id_Not_Found
	private Integer empId;
	
	@GSR_Id_Not_Found_Slab
	private Short slabId;
	
	@Employee_Id_Not_Found
	private Integer closedByEmpId;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Short getSlabId() {
		return slabId;
	}

	public void setSlabId(Short slabId) {
		this.slabId = slabId;
	}

	public Integer getClosedByEmpId() {
		return closedByEmpId;
	}

	public void setClosedByEmpId(Integer closedByEmpId) {
		this.closedByEmpId = closedByEmpId;
	}

	@Override
	public String toString() {
		return "ForceClosedGoalsInputBean [empId=" + empId + ", slabId="
				+ slabId + ", closedByEmpId=" + closedByEmpId + "]";
	}

}
