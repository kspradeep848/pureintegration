package com.thbs.mis.gsr.bean;

import java.util.List;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class ViewTeamGoalsInputBean {

	@Employee_Id_Not_Found
	private Integer mgrId;

	private List<Short> slabId;

	private List<Integer> empIdList;

	public Integer getMgrId() {
		return mgrId;
	}

	public void setMgrId(Integer mgrId) {
		this.mgrId = mgrId;
	}

	public List<Short> getSlabId() {
		return slabId;
	}

	public void setSlabId(List<Short> slabId) {
		this.slabId = slabId;
	}

	public List<Integer> getEmpIdList() {
		return empIdList;
	}

	public void setEmpIdList(List<Integer> empIdList) {
		this.empIdList = empIdList;
	}

	@Override
	public String toString() {
		return "ViewTeamGoalsInputBean [mgrId=" + mgrId + ", slabId=" + slabId
				+ ", empIdList=" + empIdList + "]";
	}
}
