package com.thbs.mis.gsr.bean;

import java.util.Date;

public class GsrReporteesDashBoardBean {

	private Integer noOfReportees;
	private Integer noOfgoalSetForCurrentSlab;
	private Date gsrWindowEndDate;
	private String workFlowName;
	
	

	public Integer getNoOfReportees() {
		return noOfReportees;
	}
	public void setNoOfReportees(Integer noOfReportees) {
		this.noOfReportees = noOfReportees;
	}
	public Integer getNoOfgoalSetForCurrentSlab() {
		return noOfgoalSetForCurrentSlab;
	}
	public void setNoOfgoalSetForCurrentSlab(Integer noOfgoalSetForCurrentSlab) {
		this.noOfgoalSetForCurrentSlab = noOfgoalSetForCurrentSlab;
	}
	public Date getGsrWindowEndDate() {
		return gsrWindowEndDate;
	}
	public void setGsrWindowEndDate(Date gsrWindowEndDate) {
		this.gsrWindowEndDate = gsrWindowEndDate;
	}
	public String getWorkFlowName() {
		return workFlowName;
	}
	public void setWorkFlowName(String workFlowName) {
		this.workFlowName = workFlowName;
	}
	
	@Override
	public String toString() {
		return "ReporteesDashBoardBean [NoOfReportees=" + noOfReportees
				+ ", NoOfGoalSet=" + noOfgoalSetForCurrentSlab + ", gsrWindowEndDate="
				+ gsrWindowEndDate + ", workFlowName=" + workFlowName + "]";
	}
	
}
