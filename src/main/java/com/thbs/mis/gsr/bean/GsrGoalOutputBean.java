package com.thbs.mis.gsr.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class GsrGoalOutputBean {
	
	private int goalId;
	private String goalName;
	private int employeeId;
	private int reportingMgrId;
	private Date gsrStartDate;
	private Date gsrEndDate;
	private short goalSlabId;
	private byte goalWeightage;
	private String goalDesciption;
	private String goalMeasurement;
	private byte goalStatus;
	private int goalCreatedBy;
	private Date goalCreatedDate;
	private short empGoalKpiWeightageId;
	private short fkEmpGoalBuId;
	private int fkEmpGoalDomainMgrId;
	
	public int getGoalId() {
		return goalId;
	}
	public void setGoalId(int goalId) {
		this.goalId = goalId;
	}
	public String getGoalName() {
		return goalName;
	}
	public void setGoalName(String goalName) {
		this.goalName = goalName;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public int getReportingMgrId() {
		return reportingMgrId;
	}
	public void setReportingMgrId(int reportingMgrId) {
		this.reportingMgrId = reportingMgrId;
	}
	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getGsrStartDate() {
		return gsrStartDate;
	}
	public void setGsrStartDate(Date gsrStartDate) {
		this.gsrStartDate = gsrStartDate;
	}
	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getGsrEndDate() {
		return gsrEndDate;
	}
	public void setGsrEndDate(Date gsrEndDate) {
		this.gsrEndDate = gsrEndDate;
	}
	public short getGoalSlabId() {
		return goalSlabId;
	}
	public void setGoalSlabId(short goalSlabId) {
		this.goalSlabId = goalSlabId;
	}
	public byte getGoalWeightage() {
		return goalWeightage;
	}
	public void setGoalWeightage(byte goalWeightage) {
		this.goalWeightage = goalWeightage;
	}
	public String getGoalDesciption() {
		return goalDesciption;
	}
	public void setGoalDesciption(String goalDesciption) {
		this.goalDesciption = goalDesciption;
	}
	public String getGoalMeasurement() {
		return goalMeasurement;
	}
	public void setGoalMeasurement(String goalMeasurement) {
		this.goalMeasurement = goalMeasurement;
	}
	public byte getGoalStatus() {
		return goalStatus;
	}
	public void setGoalStatus(byte goalStatus) {
		this.goalStatus = goalStatus;
	}
	public int getGoalCreatedBy() {
		return goalCreatedBy;
	}
	public void setGoalCreatedBy(int goalCreatedBy) {
		this.goalCreatedBy = goalCreatedBy;
	}
	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getGoalCreatedDate() {
		return goalCreatedDate;
	}
	public void setGoalCreatedDate(Date goalCreatedDate) {
		this.goalCreatedDate = goalCreatedDate;
	}
	public short getEmpGoalKpiWeightageId() {
		return empGoalKpiWeightageId;
	}
	public void setEmpGoalKpiWeightageId(short empGoalKpiWeightageId) {
		this.empGoalKpiWeightageId = empGoalKpiWeightageId;
	}
	public short getFkEmpGoalBuId() {
		return fkEmpGoalBuId;
	}
	public void setFkEmpGoalBuId(short fkEmpGoalBuId) {
		this.fkEmpGoalBuId = fkEmpGoalBuId;
	}
	public int getFkEmpGoalDomainMgrId() {
		return fkEmpGoalDomainMgrId;
	}
	public void setFkEmpGoalDomainMgrId(int fkEmpGoalDomainMgrId) {
		this.fkEmpGoalDomainMgrId = fkEmpGoalDomainMgrId;
	}
	@Override
	public String toString() {
		return "GsrGoalOutputBean [goalId=" + goalId + ", goalName=" + goalName
				+ ", employeeId=" + employeeId + ", reportingMgrId="
				+ reportingMgrId + ", gsrStartDate=" + gsrStartDate
				+ ", gsrEndDate=" + gsrEndDate + ", goalSlabId=" + goalSlabId
				+ ", goalWeightage=" + goalWeightage + ", goalDesciption="
				+ goalDesciption + ", goalMeasurement=" + goalMeasurement
				+ ", goalStatus=" + goalStatus + ", goalCreatedBy="
				+ goalCreatedBy + ", goalCreatedDate=" + goalCreatedDate
				+ ", empGoalKpiWeightageId=" + empGoalKpiWeightageId
				+ ", fkEmpGoalBuId=" + fkEmpGoalBuId
				+ ", fkEmpGoalDomainMgrId=" + fkEmpGoalDomainMgrId + "]";
	}
	

}
