/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  HrViewEmployeeGoalsOutputBean.java                */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  March 29, 2017                                    */
/*                                                                   */
/*  Description :  This class contains fields required for view      */
/*                   GSR Employee goal details	 				     */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 02, 2016     THBS     1.0        Initial version created */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

import java.util.Date;
import java.util.List;

public class HrViewEmployeeGoalsOutputBean {
	private int goalId;
	private String goalName;
	private String goalStatus;
	private int empId;
	private String empName;
	private int slabId;
	private String slabName;
	private String buName;
	private String slabShortName;
	private Date goalStartDate;
	private Date goalEndDate;
	private Short kpiGoalId;
	private Byte kpiGoalWeightage ;
	private Byte goalWeightage ;
	List<FeedbackListBean> feedbackList;
	private Integer closedBy;
	private String closedByName;

	public Integer getClosedBy() {
		return closedBy;
	}

	public void setClosedBy(Integer closedBy) {
		this.closedBy = closedBy;
	}

	public String getClosedByName() {
		return closedByName;
	}

	public void setClosedByName(String closedByName) {
		this.closedByName = closedByName;
	}

	public List<FeedbackListBean> getFeedbackList() {
		return feedbackList;
	}

	public void setFeedbackList(List<FeedbackListBean> feedbackList) {
		this.feedbackList = feedbackList;
	}

	public Short getKpiGoalId() {
		return kpiGoalId;
	}

	public void setKpiGoalId(Short kpiGoalId) {
		this.kpiGoalId = (kpiGoalId == null) ? 0 : kpiGoalId;
	}

	public Byte getKpiGoalWeightage() {
		return kpiGoalWeightage;
	}

	public void setKpiGoalWeightage(Byte kpiGoalWeightage) {

		if (kpiGoalWeightage == null) {
			this.kpiGoalWeightage = 0;
		} else {
			this.kpiGoalWeightage = kpiGoalWeightage;
		}
	}

	public Date getGoalStartDate() {
		return goalStartDate;
	}

	public void setGoalStartDate(Date goalStartDate) {
		this.goalStartDate = goalStartDate;
	}

	public Date getGoalEndDate() {
		return goalEndDate;
	}

	public void setGoalEndDate(Date goalEndDate) {
		this.goalEndDate = goalEndDate;
	}

	public String getSlabShortName() {
		return slabShortName;
	}

	public void setSlabShortName(String slabShortName) {
		this.slabShortName = slabShortName;
	}

	public int getGoalId() {
		return goalId;
	}

	public void setGoalId(int goalId) {
		this.goalId = goalId;
	}

	public String getGoalName() {
		return goalName;
	}

	public void setGoalName(String goalName) {
		this.goalName = goalName;
	}

	public String getGoalStatus() {
		return goalStatus;
	}

	public void setGoalStatus(String goalStatus) {
		this.goalStatus = goalStatus;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public int getSlabId() {
		return slabId;
	}

	public void setSlabId(int slabId) {
		this.slabId = slabId;
	}

	public String getSlabName() {
		return slabName;
	}

	public void setSlabName(String slabName) {
		this.slabName = slabName;
	}

	public String getBuName() {
		return buName;
	}

	public void setBuName(String buName) {
		this.buName = buName;
	}	

	public Byte getGoalWeightage() {
		return goalWeightage;
	}

	public void setGoalWeightage(Byte goalWeightage) {
		this.goalWeightage = goalWeightage;
	}

	@Override
	public String toString() {
		return "HrViewEmployeeGoalsOutputBean [goalId=" + goalId
				+ ", goalName=" + goalName + ", goalStatus=" + goalStatus
				+ ", empId=" + empId + ", empName=" + empName + ", slabId="
				+ slabId + ", slabName=" + slabName + ", buName=" + buName
				+ ", slabShortName=" + slabShortName + ", goalStartDate="
				+ goalStartDate + ", goalEndDate=" + goalEndDate
				+ ", kpiGoalId=" + kpiGoalId + ", kpiGoalWeightage="
				+ kpiGoalWeightage + ", goalWeightage=" + goalWeightage
				+ ", feedbackList=" + feedbackList + ", closedBy=" + closedBy
				+ ", closedByName=" + closedByName + "]";
	}
	

}
