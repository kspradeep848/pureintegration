package com.thbs.mis.gsr.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class GsrApproveGoalBean {
	
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	@NotNull(message = "goalWeightage should not be null.")
	private Byte goalWeightage;
	
	@NotBlank(message = "goalMeasurement should not be blank.")
	@NotEmpty(message = "goalMeasurement should not be empty.")
	@NotNull(message = "goalMeasurement should not be null.")
	private String goalMeasurement;
	
	@NotBlank(message = "goalDescription should not be blank.")
	@NotEmpty(message = "goalDescription should not be empty.")
	@NotNull(message = "goalDescription should not be null.")
	private String goalDescription;
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	@NotNull(message = "pkGsrDatEmpGoalId should not be null.")
	private Integer pkGsrDatEmpGoalId;
	
	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	@NotNull(message = "goalModifiedBy should not be null.")
	private Integer goalModifiedBy;
	
	public Byte getGoalWeightage() {
		return goalWeightage;
	}
	public void setGoalWeightage(Byte goalWeightage) {
		this.goalWeightage = goalWeightage;
	}
	public String getGoalMeasurement() {
		return goalMeasurement;
	}
	public void setGoalMeasurement(String goalMeasurement) {
		this.goalMeasurement = goalMeasurement;
	}
	public String getGoalDescription() {
		return goalDescription;
	}
	public void setGoalDescription(String goalDescription) {
		this.goalDescription = goalDescription;
	}
	public Integer getPkGsrDatEmpGoalId() {
		return pkGsrDatEmpGoalId;
	}
	public void setPkGsrDatEmpGoalId(Integer pkGsrDatEmpGoalId) {
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
	}
	public Integer getGoalModifiedBy() {
		return goalModifiedBy;
	}
	public void setGoalModifiedBy(Integer goalModifiedBy) {
		this.goalModifiedBy = goalModifiedBy;
	}
	@Override
	public String toString() {
		return "GsrApproveGoalBean [goalWeightage=" + goalWeightage
				+ ", goalMeasurement=" + goalMeasurement + ", goalDescription="
				+ goalDescription + ", pkGsrDatEmpGoalId=" + pkGsrDatEmpGoalId
				+ ", goalModifiedBy=" + goalModifiedBy + "]";
	}
	
}
