package com.thbs.mis.gsr.bean;

import java.util.Date;

public class DMViewEmpGoalsOutputBean {
	
	private Integer pkGsrDatEmpGoalId;
	private String gsrDatEmpGoalName;
	private Integer fkGsrDatEmpId;
	//private Short fkEmpGoalBuId;
	private Date gsrGoalStartDate;
	private Date gsrGoalEndDate;
	private Short fkGsrSlabId;
	private Byte gsrGoalWeightage;
	private String gsrGoalDescription;
	private String gsrGoalMeasurement;
	private String gsrGoalAccomplishment;
	private Float gsrGoalSelfRating;
	private Byte fkGsrGoalStatus;
	private short gsrSlabId;
	private String slabName;
	private String gsrSlabYear;
	private String empFirstName;
	private String empLastName;
	private String gsrGoalStatusName;
	
	
	
	public Integer getPkGsrDatEmpGoalId() {
		return pkGsrDatEmpGoalId;
	}

	public void setPkGsrDatEmpGoalId(Integer pkGsrDatEmpGoalId) {
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
	}


	public String getGsrDatEmpGoalName() {
		return gsrDatEmpGoalName;
	}


	public void setGsrDatEmpGoalName(String gsrDatEmpGoalName) {
		this.gsrDatEmpGoalName = gsrDatEmpGoalName;
	}

	public Integer getFkGsrDatEmpId() {
		return fkGsrDatEmpId;
	}

	public void setFkGsrDatEmpId(Integer fkGsrDatEmpId) {
		this.fkGsrDatEmpId = fkGsrDatEmpId;
	}


	/*public Short getFkEmpGoalBuId() {
		return fkEmpGoalBuId;
	}

	public void setFkEmpGoalBuId(Short fkEmpGoalBuId) {
		this.fkEmpGoalBuId = fkEmpGoalBuId;
	}*/

	public Date getGsrGoalStartDate() {
		return gsrGoalStartDate;
	}

	public void setGsrGoalStartDate(Date gsrGoalStartDate) {
		this.gsrGoalStartDate = gsrGoalStartDate;
	}






	public Date getGsrGoalEndDate() {
		return gsrGoalEndDate;
	}






	public void setGsrGoalEndDate(Date gsrGoalEndDate) {
		this.gsrGoalEndDate = gsrGoalEndDate;
	}






	public Short getFkGsrSlabId() {
		return fkGsrSlabId;
	}






	public void setFkGsrSlabId(Short fkGsrSlabId) {
		this.fkGsrSlabId = fkGsrSlabId;
	}






	public Byte getGsrGoalWeightage() {
		return gsrGoalWeightage;
	}






	public void setGsrGoalWeightage(Byte gsrGoalWeightage) {
		this.gsrGoalWeightage = gsrGoalWeightage;
	}






	public String getGsrGoalDescription() {
		return gsrGoalDescription;
	}






	public void setGsrGoalDescription(String gsrGoalDescription) {
		this.gsrGoalDescription = gsrGoalDescription;
	}






	public String getGsrGoalMeasurement() {
		return gsrGoalMeasurement;
	}






	public void setGsrGoalMeasurement(String gsrGoalMeasurement) {
		this.gsrGoalMeasurement = gsrGoalMeasurement;
	}






	public String getGsrGoalAccomplishment() {
		return gsrGoalAccomplishment;
	}






	public void setGsrGoalAccomplishment(String gsrGoalAccomplishment) {
		this.gsrGoalAccomplishment = gsrGoalAccomplishment;
	}






	public Float getGsrGoalSelfRating() {
		return gsrGoalSelfRating;
	}






	public void setGsrGoalSelfRating(Float gsrGoalSelfRating) {
		this.gsrGoalSelfRating = gsrGoalSelfRating;
	}






	public Byte getFkGsrGoalStatus() {
		return fkGsrGoalStatus;
	}






	public void setFkGsrGoalStatus(Byte fkGsrGoalStatus) {
		this.fkGsrGoalStatus = fkGsrGoalStatus;
	}






	public short getGsrSlabId() {
		return gsrSlabId;
	}






	public void setGsrSlabId(short gsrSlabId) {
		this.gsrSlabId = gsrSlabId;
	}


	public String getSlabName() {
		return slabName;
	}


	public void setSlabName(String slabName) {
		this.slabName = slabName;
	}

	public String getGsrSlabYear() {
		return gsrSlabYear;
	}

	public void setGsrSlabYear(String gsrSlabYear) {
		this.gsrSlabYear = gsrSlabYear;
	}

	public String getEmpFirstName() {
		return empFirstName;
	}

	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}



	public String getEmpLastName() {
		return empLastName;
	}


	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}


	public String getGsrGoalStatusName() {
		return gsrGoalStatusName;
	}


	public void setGsrGoalStatusName(String gsrGoalStatusName) {
		this.gsrGoalStatusName = gsrGoalStatusName;
	}

	@Override
	public String toString() {
		return "DMViewEmpGoalsOutputBean [pkGsrDatEmpGoalId="
				+ pkGsrDatEmpGoalId + ", gsrDatEmpGoalName="
				+ gsrDatEmpGoalName + ", fkGsrDatEmpId=" + fkGsrDatEmpId
				+ ", gsrGoalStartDate="
				+ gsrGoalStartDate + ", gsrGoalEndDate=" + gsrGoalEndDate
				+ ", fkGsrSlabId=" + fkGsrSlabId + ", gsrGoalWeightage="
				+ gsrGoalWeightage + ", gsrGoalDescription="
				+ gsrGoalDescription + ", gsrGoalMeasurement="
				+ gsrGoalMeasurement + ", gsrGoalAccomplishment="
				+ gsrGoalAccomplishment + ", gsrGoalSelfRating="
				+ gsrGoalSelfRating + ", fkGsrGoalStatus=" + fkGsrGoalStatus
				+ ", gsrSlabId=" + gsrSlabId + ", slabName=" + slabName
				+ ", gsrSlabYear=" + gsrSlabYear + ", empFirstName="
				+ empFirstName + ", empLastName=" + empLastName
				+ ", gsrGoalStatusName=" + gsrGoalStatusName + "]";
	}
	
}







