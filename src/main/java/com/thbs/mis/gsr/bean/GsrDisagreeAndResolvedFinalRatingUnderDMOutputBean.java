package com.thbs.mis.gsr.bean;

import java.util.List;

public class GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean {
	
	private short slabId;
	private List<GsrDisagreeFinalRatingReporteeListBean> listOfDisgareeFinalRatingOfASlab;
	private List<GsrResolvedFinalRatingReporteeListBean> listOfResolvedFinalRatingOfASlab;
	
	public short getSlabId() {
		return slabId;
	}
	public void setSlabId(short slabId) {
		this.slabId = slabId;
	}
	public List<GsrDisagreeFinalRatingReporteeListBean> getListOfDisgareeFinalRatingOfASlab() {
		return listOfDisgareeFinalRatingOfASlab;
	}
	public void setListOfDisgareeFinalRatingOfASlab(
			List<GsrDisagreeFinalRatingReporteeListBean> listOfDisgareeFinalRatingOfASlab) {
		this.listOfDisgareeFinalRatingOfASlab = listOfDisgareeFinalRatingOfASlab;
	}
	public List<GsrResolvedFinalRatingReporteeListBean> getListOfResolvedFinalRatingOfASlab() {
		return listOfResolvedFinalRatingOfASlab;
	}
	public void setListOfResolvedFinalRatingOfASlab(
			List<GsrResolvedFinalRatingReporteeListBean> listOfResolvedFinalRatingOfASlab) {
		this.listOfResolvedFinalRatingOfASlab = listOfResolvedFinalRatingOfASlab;
	}
	@Override
	public String toString() {
		return "GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean [slabId="
				+ slabId + ", listOfDisgareeFinalRatingOfASlab="
				+ listOfDisgareeFinalRatingOfASlab
				+ ", listOfResolvedFinalRatingOfASlab="
				+ listOfResolvedFinalRatingOfASlab + "]";
	}
	
	
}
