package com.thbs.mis.gsr.bean;

import java.util.List;

public class EmployeeBean {

	private EmployeeDashBoardBean empWorkFlowDetails;
	private List<EmployeePerformanceRatingBean> empRatings;
	public EmployeeDashBoardBean getEmpWorkFlowDetails() {
		return empWorkFlowDetails;
	}
	public void setEmpWorkFlowDetails(EmployeeDashBoardBean empWorkFlowDetails) {
		this.empWorkFlowDetails = empWorkFlowDetails;
	}
	public List<EmployeePerformanceRatingBean> getEmpRatings() {
		return empRatings;
	}
	public void setEmpRatings(List<EmployeePerformanceRatingBean> empRatings) {
		this.empRatings = empRatings;
	}
	@Override
	public String toString() {
		return "EmployeeBean [empWorkFlowDetails=" + empWorkFlowDetails
				+ ", empRatings=" + empRatings + "]";
	}
	
	
}
