package com.thbs.mis.gsr.bean;

public class GsrDisagreeFinalRatingUnderDMOutputBean {
	
	private short slabId;
	private int employeeId;
	private float finalRating;
	private int finalRatingPkId;
	private String employeeName;
	
	public short getSlabId() {
		return slabId;
	}
	public void setSlabId(short slabId) {
		this.slabId = slabId;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public float getFinalRating() {
		return finalRating;
	}
	public void setFinalRating(float finalRating) {
		this.finalRating = finalRating;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public int getFinalRatingPkId() {
		return finalRatingPkId;
	}
	public void setFinalRatingId(int finalRatingPkId) {
		this.finalRatingPkId = finalRatingPkId;
	}
	@Override
	public String toString() {
		return "GsrDisagreeFinalRatingUnderDMOutputBean [slabId=" + slabId
				+ ", employeeId=" + employeeId + ", finalRating=" + finalRating
				+ ", finalRatingPkId=" + finalRatingPkId + ", employeeName="
				+ employeeName + "]";
	}
}
