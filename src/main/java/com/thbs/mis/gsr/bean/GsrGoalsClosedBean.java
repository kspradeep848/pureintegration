/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrGoalsClosedBean.java                   		  */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  18-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	GsrGoalsClosedBean		.                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

import java.util.Date;
import java.util.List;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class GsrGoalsClosedBean {
	
	@Employee_Id_Not_Found
	@NotNull(message = "EmpID field can not be null")
	private Integer fkGsrDatEmpId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date gsrGoalStartDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd")
	private Date gsrGoalEndDate;
	
	private List<Integer> listOfEmpId;
	
	public Integer getFkGsrDatEmpId() {
		return fkGsrDatEmpId;
	}
	public void setFkGsrDatEmpId(Integer fkGsrDatEmpId) {
		this.fkGsrDatEmpId = fkGsrDatEmpId;
	}
	public Date getGsrGoalStartDate() {
		return gsrGoalStartDate;
	}
	public void setGsrGoalStartDate(Date gsrGoalStartDate) {
		this.gsrGoalStartDate = gsrGoalStartDate;
	}
	public Date getGsrGoalEndDate() {
		return gsrGoalEndDate;
	}
	public void setGsrGoalEndDate(Date gsrGoalEndDate) {
		this.gsrGoalEndDate = gsrGoalEndDate;
	}
	public List<Integer> getListOfEmpId() {
		return listOfEmpId;
	}
	public void setListOfEmpId(List<Integer> listOfEmpId) {
		this.listOfEmpId = listOfEmpId;
	}
	@Override
	public String toString() {
		return "GsrGoalsClosedBean [fkGsrDatEmpId=" + fkGsrDatEmpId
				+ ", gsrGoalStartDate=" + gsrGoalStartDate
				+ ", gsrGoalEndDate=" + gsrGoalEndDate + ", listOfEmpId="
				+ listOfEmpId + "]";
	}	
}
