package com.thbs.mis.gsr.bean;

import java.util.List;

import com.thbs.mis.common.bean.BusinessUnitBean;
import com.thbs.mis.common.bean.EmployeeLevelBean;
import com.thbs.mis.common.bean.EmployeesBean;
import com.thbs.mis.gsr.bo.GsrMasGoalStatusBO;

public class GsrReportInitContentBean {
	List<GsrSlabOutputBean> getAllSlabs;
	
	List<EmployeesBean> allEmployeesList;
	
	List<BusinessUnitBean> allActiveBu;
	
	List<GsrGetAllReportingManagersBean> getAllReportingManagerList;
	 
	List<EmployeeLevelBean> levelList;
	
	List<GsrMasGoalStatusBO> allGsrMasStatus;
	
	
	

	public List<GsrSlabOutputBean> getGetAllSlabs() {
		return getAllSlabs;
	}

	public void setGetAllSlabs(List<GsrSlabOutputBean> getAllSlabs) {
		this.getAllSlabs = getAllSlabs;
	}

	public List<EmployeesBean> getAllEmployeesList() {
		return allEmployeesList;
	}

	public void setAllEmployeesList(List<EmployeesBean> allEmployeesList) {
		this.allEmployeesList = allEmployeesList;
	}

	public List<BusinessUnitBean> getAllActiveBu() {
		return allActiveBu;
	}

	public void setAllActiveBu(List<BusinessUnitBean> allActiveBu) {
		this.allActiveBu = allActiveBu;
	}

	public List<GsrGetAllReportingManagersBean> getGetAllReportingManagerList() {
		return getAllReportingManagerList;
	}

	public void setGetAllReportingManagerList(
			List<GsrGetAllReportingManagersBean> getAllReportingManagerList) {
		this.getAllReportingManagerList = getAllReportingManagerList;
	}

	public List<EmployeeLevelBean> getLevelList() {
		return levelList;
	}

	public void setLevelList(List<EmployeeLevelBean> levelList) {
		this.levelList = levelList;
	}

	public List<GsrMasGoalStatusBO> getAllGsrMasStatus() {
		return allGsrMasStatus;
	}

	public void setAllGsrMasStatus(List<GsrMasGoalStatusBO> allGsrMasStatus) {
		this.allGsrMasStatus = allGsrMasStatus;
	}

	@Override
	public String toString() {
		return "GsrReportInitContentBean [getAllSlabs=" + getAllSlabs
				+ ", allEmployeesList=" + allEmployeesList + ", allActiveBu="
				+ allActiveBu + ", getAllReportingManagerList="
				+ getAllReportingManagerList + ", levelList=" + levelList
				+ ", allGsrMasStatus=" + allGsrMasStatus + "]";
	}
	
	
}
