package com.thbs.mis.gsr.bean;

public class GsrMaximumKpiOutputBean {
private Integer kpiWeightage;
private Integer nonKpiWeightage;
private Integer empId;
private Short slabId;
private Integer kpiWeightageUsed;
private Integer kpiWeightageAvailable;
private Integer nonKpiWeightageUsed;
private Integer nonKpiWeightageAvailable;
private Integer empBuId;
private Integer empLevelId;
private Integer empReportingManager;
public Integer getEmpBuId() {
	return empBuId;
}
public void setEmpBuId(Integer empBuId) {
	this.empBuId = empBuId;
}
public Integer getEmpLevelId() {
	return empLevelId;
}
public void setEmpLevelId(Integer empLevelId) {
	this.empLevelId = empLevelId;
}
public Integer getEmpReportingManager() {
	return empReportingManager;
}
public void setEmpReportingManager(Integer empReportingManager) {
	this.empReportingManager = empReportingManager;
}
public Short getSlabId() {
	return slabId;
}
public void setSlabId(Short slabId) {
	this.slabId = slabId;
}
public Integer getKpiWeightageUsed() {
	return kpiWeightageUsed;
}
public void setKpiWeightageUsed(Integer kpiWeightageUsed) {
	this.kpiWeightageUsed = kpiWeightageUsed;
}
public Integer getKpiWeightageAvailable() {
	return kpiWeightageAvailable;
}
public void setKpiWeightageAvailable(Integer kpiWeightageAvailable) {
	this.kpiWeightageAvailable = kpiWeightageAvailable;
}
public Integer getNonKpiWeightageUsed() {
	return nonKpiWeightageUsed;
}
public void setNonKpiWeightageUsed(Integer nonKpiWeightageUsed) {
	this.nonKpiWeightageUsed = nonKpiWeightageUsed;
}
public Integer getNonKpiWeightageAvailable() {
	return nonKpiWeightageAvailable;
}
public void setNonKpiWeightageAvailable(Integer nonKpiWeightageAvailable) {
	this.nonKpiWeightageAvailable = nonKpiWeightageAvailable;
}
public Integer getEmpId() {
	return empId;
}
public void setEmpId(Integer empId) {
	this.empId = empId;
}

public Integer getKpiWeightage() {
	return kpiWeightage;
}
public void setKpiWeightage(Integer kpiWeightage) {
	this.kpiWeightage = kpiWeightage;
}
public Integer getNonKpiWeightage() {
	return nonKpiWeightage;
}
public void setNonKpiWeightage(Integer nonKpiWeightage) {
	this.nonKpiWeightage = nonKpiWeightage;
}
@Override
public String toString() {
	return "GsrMaximumKpiOutputBean [kpiWeightage=" + kpiWeightage
			+ ", nonKpiWeightage=" + nonKpiWeightage + ", empId=" + empId
			+ ", slabId=" + slabId + ", kpiWeightageUsed=" + kpiWeightageUsed
			+ ", kpiWeightageAvailable=" + kpiWeightageAvailable
			+ ", nonKpiWeightageUsed=" + nonKpiWeightageUsed
			+ ", nonKpiWeightageAvailable=" + nonKpiWeightageAvailable
			+ ", empBuId=" + empBuId + ", empLevelId=" + empLevelId
			+ ", empReportingManager=" + empReportingManager + "]";
}
 


}
