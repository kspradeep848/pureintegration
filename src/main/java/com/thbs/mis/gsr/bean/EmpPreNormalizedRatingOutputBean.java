package com.thbs.mis.gsr.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class EmpPreNormalizedRatingOutputBean {

	private int slabId;
	private String slabName;
	private Float preNormalizedRating;
	private String slabYear;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date slabStartDate;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date slabEndDate;
	private String slabShortName;

	public String getSlabYear() {
		return slabYear;
	}

	public void setSlabYear(String slabYear) {
		this.slabYear = slabYear;
	}

	public int getSlabId() {
		return slabId;
	}

	public void setSlabId(int slabId) {
		this.slabId = slabId;
	}

	public String getSlabName() {
		return slabName;
	}

	public void setSlabName(String slabName) {
		this.slabName = slabName;
	}

	public Float getPreNormalizedRating() {
		return preNormalizedRating;
	}

	public Date getSlabStartDate() {
		return slabStartDate;
	}

	public void setSlabStartDate(Date slabStartDate) {
		this.slabStartDate = slabStartDate;
	}

	public Date getSlabEndDate() {
		return slabEndDate;
	}

	public void setSlabEndDate(Date slabEndDate) {
		this.slabEndDate = slabEndDate;
	}

	public String getSlabShortName() {
		return slabShortName;
	}

	public void setSlabShortName(String slabShortName) {
		this.slabShortName = slabShortName;
	}

	public void setPreNormalizedRating(Float preNormalizedRating) {
		this.preNormalizedRating = preNormalizedRating;
	}

	@Override
	public String toString() {
		return "EmpPreNormalizedRatingOutputBean [slabId=" + slabId
				+ ", slabName=" + slabName + ", preNormalizedRating="
				+ preNormalizedRating + ", slabYear=" + slabYear
				+ ", slabStartDate=" + slabStartDate + ", slabEndDate="
				+ slabEndDate + ", slabShortName=" + slabShortName + "]";
	}

}
