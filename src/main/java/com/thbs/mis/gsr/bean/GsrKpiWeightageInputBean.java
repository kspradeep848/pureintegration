package com.thbs.mis.gsr.bean;

import java.util.List;

public class GsrKpiWeightageInputBean {
	
	private List<Integer> empIdList;
	private short kpiWeitageId;
	public List<Integer> getEmpIdList() {
		return empIdList;
	}
	public void setEmpIdList(List<Integer> empIdList) {
		this.empIdList = empIdList;
	}
	public short getKpiWeitageId() {
		return kpiWeitageId;
	}
	public void setKpiWeitageId(short kpiWeitageId) {
		this.kpiWeitageId = kpiWeitageId;
	}
	@Override
	public String toString() {
		return "GsrKpiWeightageInputBean [empIdList=" + empIdList
				+ ", kpiWeitageId=" + kpiWeitageId + "]";
	}

}
