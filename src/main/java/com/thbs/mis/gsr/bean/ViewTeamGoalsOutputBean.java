package com.thbs.mis.gsr.bean;

import java.util.List;

public class ViewTeamGoalsOutputBean {

	List<ReporteesBean> reportees;

	public List<ReporteesBean> getReportees() {
		return reportees;
	}

	public void setReportees(List<ReporteesBean> reportees) {
		this.reportees = reportees;
	}

	@Override
	public String toString() {
		return "ViewTeamGoalsOutputBean [reportees=" + reportees + "]";
	}

}
