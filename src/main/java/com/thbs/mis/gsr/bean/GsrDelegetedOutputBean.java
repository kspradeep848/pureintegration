package com.thbs.mis.gsr.bean;

public class GsrDelegetedOutputBean {
	
	
	int  pkGsrKpiGoalId;
	String gsrKpiGoalName;
	String gsrKpiGoalDescription;
	String gsrKpiGoalMeasurement;
	String gsrKpiGoalActiveFlag;
	int pkGsrKpiWeightageId;
	int fkGsrKpiGoalId;
	int fkEmpLevelId;
	int gsrKpiGoalWeightage;
	
	

	public int getPkGsrKpiGoalId() {
		return pkGsrKpiGoalId;
	}
	public void setPkGsrKpiGoalId(int pkGsrKpiGoalId) {
		this.pkGsrKpiGoalId = pkGsrKpiGoalId;
	}
	public String getGsrKpiGoalName() {
		return gsrKpiGoalName;
	}
	public void setGsrKpiGoalName(String gsrKpiGoalName) {
		this.gsrKpiGoalName = gsrKpiGoalName;
	}
	public String getGsrKpiGoalDescription() {
		return gsrKpiGoalDescription;
	}
	public void setGsrKpiGoalDescription(String gsrKpiGoalDescription) {
		this.gsrKpiGoalDescription = gsrKpiGoalDescription;
	}
	public String getGsrKpiGoalMeasurement() {
		return gsrKpiGoalMeasurement;
	}
	public void setGsrKpiGoalMeasurement(String gsrKpiGoalMeasurement) {
		this.gsrKpiGoalMeasurement = gsrKpiGoalMeasurement;
	}
	public String getGsrKpiGoalActiveFlag() {
		return gsrKpiGoalActiveFlag;
	}
	public void setGsrKpiGoalActiveFlag(String gsrKpiGoalActiveFlag) {
		this.gsrKpiGoalActiveFlag = gsrKpiGoalActiveFlag;
	}
	public int getPkGsrKpiWeightageId() {
		return pkGsrKpiWeightageId;
	}
	public void setPkGsrKpiWeightageId(int pkGsrKpiWeightageId) {
		this.pkGsrKpiWeightageId = pkGsrKpiWeightageId;
	}
	public int getFkGsrKpiGoalId() {
		return fkGsrKpiGoalId;
	}
	public void setFkGsrKpiGoalId(int fkGsrKpiGoalId) {
		this.fkGsrKpiGoalId = fkGsrKpiGoalId;
	}
	public int getFkEmpLevelId() {
		return fkEmpLevelId;
	}
	public void setFkEmpLevelId(int fkEmpLevelId) {
		this.fkEmpLevelId = fkEmpLevelId;
	}
	public int getGsrKpiGoalWeightage() {
		return gsrKpiGoalWeightage;
	}
	public void setGsrKpiGoalWeightage(int gsrKpiGoalWeightage) {
		this.gsrKpiGoalWeightage = gsrKpiGoalWeightage;
	}
	
	
	@Override
	public String toString() {
		return "GsrDelegetedOutputBean [pkGsrKpiGoalId=" + pkGsrKpiGoalId
				+ ", gsrKpiGoalName=" + gsrKpiGoalName
				+ ", gsrKpiGoalDescription=" + gsrKpiGoalDescription
				+ ", gsrKpiGoalMeasurement=" + gsrKpiGoalMeasurement
				+ ", gsrKpiGoalActiveFlag=" + gsrKpiGoalActiveFlag
				+ ", pkGsrKpiWeightageId=" + pkGsrKpiWeightageId
				+ ", fkGsrKpiGoalId=" + fkGsrKpiGoalId + ", fkEmpLevelId="
				+ fkEmpLevelId + ", gsrKpiGoalWeightage=" + gsrKpiGoalWeightage
				+ "]";
	}
	
	
	
}
