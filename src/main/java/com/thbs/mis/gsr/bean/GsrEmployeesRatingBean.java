/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmployeesRating.java           		             */
/*                                                                   */
/*  Author      :  Anil kumar B K	                                 */
/*                                                                   */
/*  Date        :  24-Nov-2016                                       */
/*                                                                   */
/*  Description :  Bean class for GSR dash board    			     */
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 24-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

public class GsrEmployeesRatingBean implements Comparable<GsrEmployeesRatingBean>{

	private int countOfBelowTwo;
	private int countOfRangeTwoToThree;
	private int countOfRangeThreeToFour;
	private int countOfRangeFourToFive;
	private int slabId;
	private String SlabName;
	private String slabYear;
	private String slabShortName;
	
	public int getSlabId() {
		return slabId;
	}
	public void setSlabId(int slabId) {
		this.slabId = slabId;
	}
	
	public int getCountOfBelowTwo() {
		return countOfBelowTwo;
	}
	public void setCountOfBelowTwo(int countOfBelowTwo) {
		this.countOfBelowTwo = countOfBelowTwo;
	}
	public int getCountOfRangeTwoToThree() {
		return countOfRangeTwoToThree;
	}
	public void setCountOfRangeTwoToThree(int countOfRangeTwoToThree) {
		this.countOfRangeTwoToThree = countOfRangeTwoToThree;
	}
	public int getCountOfRangeThreeToFour() {
		return countOfRangeThreeToFour;
	}
	public void setCountOfRangeThreeToFour(int countOfRangeThreeToFour) {
		this.countOfRangeThreeToFour = countOfRangeThreeToFour;
	}
	public int getCountOfRangeFourToFive() {
		return countOfRangeFourToFive;
	}
	public void setCountOfRangeFourToFive(int countOfRangeFourToFive) {
		this.countOfRangeFourToFive = countOfRangeFourToFive;
	}
	public String getSlabName() {
		return SlabName;
	}
	public void setSlabName(String slabName) {
		SlabName = slabName;
	}
	
	
	
	public String getSlabYear() {
		return slabYear;
	}
	public void setSlabYear(String slabYear) {
		this.slabYear = slabYear;
	}
	public String getSlabShortName() {
		return slabShortName;
	}
	public void setSlabShortName(String slabShortName) {
		this.slabShortName = slabShortName;
	}
	
	
	
	@Override
	public String toString() {
		return "GsrEmployeesRatingBean [countOfBelowTwo=" + countOfBelowTwo
				+ ", countOfRangeTwoToThree=" + countOfRangeTwoToThree
				+ ", countOfRangeThreeToFour=" + countOfRangeThreeToFour
				+ ", countOfRangeFourToFive=" + countOfRangeFourToFive
				+ ", slabId=" + slabId + ", SlabName=" + SlabName
				+ ", slabYear=" + slabYear + ", slabShortName=" + slabShortName
				+ "]";
	}
	@Override
	public int compareTo(GsrEmployeesRatingBean obj) {
		
		if(this.slabId == obj.slabId)
		return 0;
		else if(this.slabId > obj.slabId)
		return 1;
		else
		return -1;
	}
	
}
