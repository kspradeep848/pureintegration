package com.thbs.mis.gsr.bean;

import java.util.List;

public class GsrDisagreeAndResolvedFinalRatingOutputBean {
	
	private short slabId;
	private int reportingMgrId;
	private String reportingMgrName;
	private int numberOfDisagreeUnderMgr;
	private int numberOfResolvedUnderMgr;
	private List<GsrDisagreeFinalRatingReporteeListBean> listOfDisgareeFinalRatingOfASlab;
	private List<GsrResolvedFinalRatingReporteeListBean> listOfResolvedFinalRatingOfASlab;

	public short getSlabId() {
		return slabId;
	}
	public void setSlabId(short slabId) {
		this.slabId = slabId;
	}
	public int getReportingMgrId() {
		return reportingMgrId;
	}
	public void setReportingMgrId(int reportingMgrId) {
		this.reportingMgrId = reportingMgrId;
	}
	public String getReportingMgrName() {
		return reportingMgrName;
	}
	public void setReportingMgrName(String reportingMgrName) {
		this.reportingMgrName = reportingMgrName;
	}
	public int getNumberOfDisagreeUnderMgr() {
		return numberOfDisagreeUnderMgr;
	}
	public void setNumberOfDisagreeUnderMgr(int numberOfDisagreeUnderMgr) {
		this.numberOfDisagreeUnderMgr = numberOfDisagreeUnderMgr;
	}
	public List<GsrDisagreeFinalRatingReporteeListBean> getListOfDisgareeFinalRatingOfASlab() {
		return listOfDisgareeFinalRatingOfASlab;
	}
	public void setListOfDisgareeFinalRatingOfASlab(
			List<GsrDisagreeFinalRatingReporteeListBean> listOfDisgareeFinalRatingOfASlab) {
		this.listOfDisgareeFinalRatingOfASlab = listOfDisgareeFinalRatingOfASlab;
	}
	public List<GsrResolvedFinalRatingReporteeListBean> getListOfResolvedFinalRatingOfASlab() {
		return listOfResolvedFinalRatingOfASlab;
	}
	public void setListOfResolvedFinalRatingOfASlab(
			List<GsrResolvedFinalRatingReporteeListBean> listOfResolvedFinalRatingOfASlab) {
		this.listOfResolvedFinalRatingOfASlab = listOfResolvedFinalRatingOfASlab;
	}
	
	public int getNumberOfResolvedUnderMgr() {
		return numberOfResolvedUnderMgr;
	}
	public void setNumberOfResolvedUnderMgr(int numberOfResolvedUnderMgr) {
		this.numberOfResolvedUnderMgr = numberOfResolvedUnderMgr;
	}

	@Override
	public String toString() {
		return "GsrDisagreeAndResolvedFinalRatingOutputBean [slabId=" + slabId
				+ ", reportingMgrId=" + reportingMgrId + ", reportingMgrName="
				+ reportingMgrName + ", numberOfDisagreeUnderMgr="
				+ numberOfDisagreeUnderMgr + ", numberOfResolvedUnderMgr="
				+ numberOfResolvedUnderMgr
				+ ", listOfDisgareeFinalRatingOfASlab="
				+ listOfDisgareeFinalRatingOfASlab
				+ ", listOfResolvedFinalRatingOfASlab="
				+ listOfResolvedFinalRatingOfASlab + "]";
	}
	
	
}
