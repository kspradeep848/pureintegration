package com.thbs.mis.gsr.bean;

public class GsrKpiWeightageOutputBean {
	
	private Integer empId;
	private short kpiWeightageId;
	private byte kpiWeightage;
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public short getKpiWeightageId() {
		return kpiWeightageId;
	}
	public void setKpiWeightageId(short kpiWeightageId) {
		this.kpiWeightageId = kpiWeightageId;
	}
	public byte getKpiWeightage() {
		return kpiWeightage;
	}
	public void setKpiWeightage(byte kpiWeightage) {
		this.kpiWeightage = kpiWeightage;
	}
	@Override
	public String toString() {
		return "GsrKpiWeightageOutputBean [empId=" + empId
				+ ", kpiWeightageId=" + kpiWeightageId + ", kpiWeightage="
				+ kpiWeightage + "]";
	}
	
}
