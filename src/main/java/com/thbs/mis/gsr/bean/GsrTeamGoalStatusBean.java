package com.thbs.mis.gsr.bean;

public class GsrTeamGoalStatusBean {

	Integer teamCount;
	Short slabId;
	Integer statusGoalClosedCount;
	Integer statusNotGoalClosedCount;
	Integer statusGoalSetCount;
	Integer statusNotGoalSetCount;
	String slabDuration;
	String slabShortName;
	String status;
	Integer statusId;

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the teamCount
	 */
	public Integer getTeamCount() {
		return teamCount;
	}

	/**
	 * @return the slabId
	 */
	public Short getSlabId() {
		return slabId;
	}

	/**
	 * @return the statusGoalClosedCount
	 */
	public Integer getStatusGoalClosedCount() {
		return statusGoalClosedCount;
	}

	/**
	 * @return the statusNotGoalClosedCount
	 */
	public Integer getStatusNotGoalClosedCount() {
		return statusNotGoalClosedCount;
	}

	/**
	 * @return the statusGoalSetCount
	 */
	public Integer getStatusGoalSetCount() {
		return statusGoalSetCount;
	}

	/**
	 * @return the statusNotGoalSetCount
	 */
	public Integer getStatusNotGoalSetCount() {
		return statusNotGoalSetCount;
	}

	/**
	 * @return the slabDuration
	 */
	public String getSlabDuration() {
		return slabDuration;
	}

	/**
	 * @return the slabShortName
	 */
	public String getSlabShortName() {
		return slabShortName;
	}

	/**
	 * @param teamCount
	 *            the teamCount to set
	 */
	public void setTeamCount(Integer teamCount) {
		this.teamCount = teamCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GsrTeamGoalStatusBean [teamCount=" + teamCount + ", slabId="
				+ slabId + ", statusGoalClosedCount=" + statusGoalClosedCount
				+ ", statusNotGoalClosedCount=" + statusNotGoalClosedCount
				+ ", statusGoalSetCount=" + statusGoalSetCount
				+ ", statusNotGoalSetCount=" + statusNotGoalSetCount
				+ ", slabDuration=" + slabDuration + ", slabShortName="
				+ slabShortName + "]";
	}

	/**
	 * @param slabId
	 *            the slabId to set
	 */
	public void setSlabId(Short slabId) {
		this.slabId = slabId;
	}

	/**
	 * @param statusGoalClosedCount
	 *            the statusGoalClosedCount to set
	 */
	public void setStatusGoalClosedCount(Integer statusGoalClosedCount) {
		this.statusGoalClosedCount = statusGoalClosedCount;
	}

	/**
	 * @param statusNotGoalClosedCount
	 *            the statusNotGoalClosedCount to set
	 */
	public void setStatusNotGoalClosedCount(Integer statusNotGoalClosedCount) {
		this.statusNotGoalClosedCount = statusNotGoalClosedCount;
	}

	/**
	 * @param statusGoalSetCount
	 *            the statusGoalSetCount to set
	 */
	public void setStatusGoalSetCount(Integer statusGoalSetCount) {
		this.statusGoalSetCount = statusGoalSetCount;
	}

	/**
	 * @param statusNotGoalSetCount
	 *            the statusNotGoalSetCount to set
	 */
	public void setStatusNotGoalSetCount(Integer statusNotGoalSetCount) {
		this.statusNotGoalSetCount = statusNotGoalSetCount;
	}

	/**
	 * @param slabDuration
	 *            the slabDuration to set
	 */
	public void setSlabDuration(String slabDuration) {
		this.slabDuration = slabDuration;
	}

	/**
	 * @param slabShortName
	 *            the slabShortName to set
	 */
	public void setSlabShortName(String slabShortName) {
		this.slabShortName = slabShortName;
	}
}
