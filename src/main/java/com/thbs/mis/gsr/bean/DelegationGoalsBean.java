package com.thbs.mis.gsr.bean;

import java.util.List;

public class DelegationGoalsBean {

	List<GoalDetailsForDelegationBean> reportees;

	public List<GoalDetailsForDelegationBean> getReportees() {
		return reportees;
	}

	public void setReportees(List<GoalDetailsForDelegationBean> reportees) {
		this.reportees = reportees;
	}

	@Override
	public String toString() {
		return "DelegationGoalsBean [reportees=" + reportees + "]";
	}

}
