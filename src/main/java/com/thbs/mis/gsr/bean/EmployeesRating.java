/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmployeesRating.java           		             */
/*                                                                   */
/*  Author      :  Anil kumar B K	                                 */
/*                                                                   */
/*  Date        :  24-Nov-2016                                       */
/*                                                                   */
/*  Description :  Bean class for GSR dash board    			     */
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 24-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

public class EmployeesRating implements Comparable<EmployeesRating>{

	private int countOfBelowTwo;
	private int countOfUnderThree;
	private int countOfUnderFour;
	private int countOfUnderFive;
	private int slabId;
	private int totalGoalsSet;
	private int ratedFourAndAbove;
	private int ratedTwoPointFiveAndBelow;
	private String SlabName;
	
	public int getSlabId() {
		return slabId;
	}
	public void setSlabId(int slabId) {
		this.slabId = slabId;
	}
	public int getRatedFourAndAbove() {
		return ratedFourAndAbove;
	}
	public void setRatedFourAndAbove(int ratedFourAndAbove) {
		this.ratedFourAndAbove = ratedFourAndAbove;
	}
	public int getRatedTwoPointFiveAndBelow() {
		return ratedTwoPointFiveAndBelow;
	}
	public void setRatedTwoPointFiveAndBelow(int ratedTwoPointFiveAndBelow) {
		this.ratedTwoPointFiveAndBelow = ratedTwoPointFiveAndBelow;
	}
	public int getTotalGoalsSet() {
		return totalGoalsSet;
	}
	public void setTotalGoalsSet(int totalGoalsSet) {
		this.totalGoalsSet = totalGoalsSet;
	}
	public int getCountOfBelowTwo() {
		return countOfBelowTwo;
	}
	public void setCountOfBelowTwo(int countOfBelowTwo) {
		this.countOfBelowTwo = countOfBelowTwo;
	}
	public int getCountOfUnderThree() {
		return countOfUnderThree;
	}
	public void setCountOfUnderThree(int countOfUnderThree) {
		this.countOfUnderThree = countOfUnderThree;
	}
	public int getCountOfUnderFour() {
		return countOfUnderFour;
	}
	public void setCountOfUnderFour(int countOfUnderFour) {
		this.countOfUnderFour = countOfUnderFour;
	}
	public int getCountOfUnderFive() {
		return countOfUnderFive;
	}
	public void setCountOfUnderFive(int countOfUnderFive) {
		this.countOfUnderFive = countOfUnderFive;
	}
	
	public String getSlabName() {
		return SlabName;
	}
	public void setSlabName(String slabName) {
		SlabName = slabName;
	}
	@Override
	public String toString() {
		return "EmployeesRating [countOfBelowTwo=" + countOfBelowTwo
				+ ", countOfUnderThree=" + countOfUnderThree
				+ ", countOfUnderFour=" + countOfUnderFour
				+ ", countOfUnderFive=" + countOfUnderFive + ", slabId="
				+ slabId + ", totalGoalsSet=" + totalGoalsSet
				+ ", ratedFourAndAbove=" + ratedFourAndAbove
				+ ", ratedTwoPointFiveAndBelow=" + ratedTwoPointFiveAndBelow
				+ ", SlabName=" + SlabName + "]";
	}
	
	
	@Override
	public int compareTo(EmployeesRating obj) {
		
		if(this.slabId == obj.slabId)
		return 0;
		else if(this.slabId > obj.slabId)
		return 1;
		else
		return -1;
	}
	
}
