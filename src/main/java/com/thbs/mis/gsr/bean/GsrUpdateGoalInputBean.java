package com.thbs.mis.gsr.bean;

public class GsrUpdateGoalInputBean {
	
	private int goalId;
	private byte goalWeightage;
	private String goalDescription;
	private String goalMeasurement;
	private int loggedInEmployee;
	
	public int getGoalId() {
		return goalId;
	}
	public void setGoalId(int goalId) {
		this.goalId = goalId;
	}
	public byte getGoalWeightage() {
		return goalWeightage;
	}
	public void setGoalWeightage(byte goalWeightage) {
		this.goalWeightage = goalWeightage;
	}
	public String getGoalDescription() {
		return goalDescription;
	}
	public void setGoalDescription(String goalDescription) {
		this.goalDescription = goalDescription;
	}
	public String getGoalMeasurement() {
		return goalMeasurement;
	}
	public void setGoalMeasurement(String goalMeasurement) {
		this.goalMeasurement = goalMeasurement;
	}
	public int getLoggedInEmployee() {
		return loggedInEmployee;
	}
	public void setLoggedInEmployee(int loggedInEmployee) {
		this.loggedInEmployee = loggedInEmployee;
	}
	@Override
	public String toString() {
		return "GsrUpdateGoalInputBean [goalId=" + goalId + ", goalWeightage="
				+ goalWeightage + ", goalDescription=" + goalDescription
				+ ", goalMeasurement=" + goalMeasurement
				+ ", loggedInEmployee=" + loggedInEmployee + "]";
	}

}
