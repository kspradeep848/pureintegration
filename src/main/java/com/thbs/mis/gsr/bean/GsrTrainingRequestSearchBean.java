/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrTrainingRequestSearchBean.java                 */
/*                                                                   */
/*  Author      :  THBS	MIS	                                         */
/*                                                                   */
/*  Date        :  12-Dec-2016                                       */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 12-Dec-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.gsr.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class GsrTrainingRequestSearchBean {

	@NumberFormat(style = Style.NUMBER, pattern = "Training Topic Id should be numeric value")
	@Min(1)
	private Integer fkGsrTrainingTopicId;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd HH:mm:SSSS")
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "yyyy-MM-dd HH:mm:SSSS")
	private Date endDate;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getFkGsrTrainingTopicId() {
		return fkGsrTrainingTopicId;
	}

	public void setFkGsrTrainingTopicId(Integer fkGsrTrainingTopicId) {
		this.fkGsrTrainingTopicId = fkGsrTrainingTopicId;
	}

	@Override
	public String toString() {
		return "GsrTrainingRequestBeanTest [fkGsrTrainingTopicId="
				+ fkGsrTrainingTopicId + ", startDate=" + startDate
				+ ", endDate=" + endDate + "]";
	}

}
