package com.thbs.mis.gsr.bean;

import java.util.Date;

public class DelegationResultFromDbBean {

	private Integer fkGsrDatEmpId;
	private String empName;
	private Short fkGsrSlabId;
	private String gsrSlabName;
	private String slabShortName;
	private Date finalRatingAccRejDate;
	private Byte finalRatingStatus;
	private Integer pkGsrDatEmpGoalId;
	private String gsrDatEmpGoalName;
	private Date gsrGoalStartDate;
	private Date gsrGoalEndDate;
	private Byte gsrGoalWeightage;
	private String gsrGoalDescription;
	private String gsrGoalMeasurement;
	private String gsrGoalAccomplishment;
	private String gsrGoalMgrFeedback;
	private Float gsrGoalSelfRating;
	private Float gsrGoalMgrRating;
	private Byte fkGsrGoalStatus;
	private String gsrGoalStatusName;
	private Short empGoalKpiWeightageId;
	private Short fkGsrKpiGoalId;
	private Byte gsrKpiGoalWeightage;
	private Integer fkGsrDelegatedGoalId;
	private Integer delegatedFromEmpId;
	private String delegatedFromEmpName;
	private Integer delegatedToEmpId;
	private String delegatedToEmpName;
	private Byte delegationStatus;
	private Date delegatedDate;
	private Date delAccRejDate;
	private Integer delegatedByEmpId;
	private String delegatedByEmpName;
	private String finalRatingStatusName;
	private Float finalRating;
	private String delegationStatusName;

	public DelegationResultFromDbBean(Integer fkGsrDatEmpId, String empName,
			Short fkGsrSlabId, String gsrSlabName, String slabShortName,
			Date finalRatingAccRejDate, Byte finalRatingStatus,
			Integer pkGsrDatEmpGoalId, String gsrDatEmpGoalName,
			Date gsrGoalStartDate, Date gsrGoalEndDate, Byte gsrGoalWeightage,
			String gsrGoalDescription, String gsrGoalMeasurement,
			String gsrGoalAccomplishment, String gsrGoalMgrFeedback,
			Float gsrGoalSelfRating, Float gsrGoalMgrRating,
			Byte fkGsrGoalStatus, String gsrGoalStatusName,
			Short empGoalKpiWeightageId, Short fkGsrKpiGoalId,
			Byte gsrKpiGoalWeightage, Integer fkGsrDelegatedGoalId,
			Integer delegatedFromEmpId, String delegatedFromEmpName,
			Integer delegatedToEmpId, String delegatedToEmpName,
			Byte delegationStatus, Date delegatedDate, Date delAccRejDate,
			Integer delegatedByEmpId, String delegatedByEmpName,
			String finalRatingStatusName, Float finalRating,String delegationStatusName) {

		this.fkGsrDatEmpId = fkGsrDatEmpId;
		this.empName = empName;
		this.fkGsrSlabId = fkGsrSlabId;
		this.gsrSlabName = gsrSlabName;
		this.slabShortName = slabShortName;
		this.finalRatingAccRejDate = finalRatingAccRejDate;
		this.finalRatingStatus = finalRatingStatus;
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
		this.gsrDatEmpGoalName = gsrDatEmpGoalName;
		this.gsrGoalStartDate = gsrGoalStartDate;
		this.gsrGoalEndDate = gsrGoalEndDate;
		this.gsrGoalWeightage = gsrGoalWeightage;
		this.gsrGoalDescription = gsrGoalDescription;
		this.gsrGoalMeasurement = gsrGoalMeasurement;
		this.gsrGoalAccomplishment = gsrGoalAccomplishment;
		this.gsrGoalMgrFeedback = gsrGoalMgrFeedback;
		this.gsrGoalSelfRating = gsrGoalSelfRating;
		this.gsrGoalMgrRating = gsrGoalMgrRating;
		this.fkGsrGoalStatus = fkGsrGoalStatus;
		this.gsrGoalStatusName = gsrGoalStatusName;
		this.empGoalKpiWeightageId = empGoalKpiWeightageId;
		this.fkGsrKpiGoalId = fkGsrKpiGoalId;
		this.gsrKpiGoalWeightage = gsrKpiGoalWeightage;
		this.fkGsrDelegatedGoalId = fkGsrDelegatedGoalId;
		this.delegatedFromEmpId = delegatedFromEmpId;
		this.delegatedFromEmpName = delegatedFromEmpName;
		this.delegatedToEmpId = delegatedToEmpId;
		this.delegatedToEmpName = delegatedToEmpName;
		this.delegationStatus = delegationStatus;
		this.delegatedDate = delegatedDate;
		this.delAccRejDate = delAccRejDate;
		this.delegatedByEmpId = delegatedByEmpId;
		this.delegatedByEmpName = delegatedByEmpName;
		this.finalRatingStatusName = finalRatingStatusName;
		this.finalRating = finalRating;
		this.delegationStatusName = delegationStatusName;
	}

	public String getDelegationStatusName() {
		return delegationStatusName;
	}

	public void setDelegationStatusName(String delegationStatusName) {
		this.delegationStatusName = delegationStatusName;
	}

	public String getDelegatedByEmpName() {
		return delegatedByEmpName;
	}

	public void setDelegatedByEmpName(String delegatedByEmpName) {
		this.delegatedByEmpName = delegatedByEmpName;
	}

	public Integer getFkGsrDatEmpId() {
		return fkGsrDatEmpId;
	}

	public void setFkGsrDatEmpId(Integer fkGsrDatEmpId) {
		this.fkGsrDatEmpId = fkGsrDatEmpId;
	}

	public String getEmpName() {
		return empName;
	}

	public String getFinalRatingStatusName() {
		return finalRatingStatusName;
	}

	public void setFinalRatingStatusName(String finalRatingStatusName) {
		this.finalRatingStatusName = finalRatingStatusName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Short getFkGsrSlabId() {
		return fkGsrSlabId;
	}

	public void setFkGsrSlabId(Short fkGsrSlabId) {
		this.fkGsrSlabId = fkGsrSlabId;
	}

	public String getGsrSlabName() {
		return gsrSlabName;
	}

	public void setGsrSlabName(String gsrSlabName) {
		this.gsrSlabName = gsrSlabName;
	}

	public String getSlabShortName() {
		return slabShortName;
	}

	public void setSlabShortName(String slabShortName) {
		this.slabShortName = slabShortName;
	}

	public Date getFinalRatingAccRejDate() {
		return finalRatingAccRejDate;
	}

	public void setFinalRatingAccRejDate(Date finalRatingAccRejDate) {
		this.finalRatingAccRejDate = finalRatingAccRejDate;
	}

	public Byte getFinalRatingStatus() {
		return finalRatingStatus;
	}

	public void setFinalRatingStatus(Byte finalRatingStatus) {
		this.finalRatingStatus = finalRatingStatus;
	}

	public Integer getPkGsrDatEmpGoalId() {
		return pkGsrDatEmpGoalId;
	}

	public void setPkGsrDatEmpGoalId(Integer pkGsrDatEmpGoalId) {
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
	}

	public String getGsrDatEmpGoalName() {
		return gsrDatEmpGoalName;
	}

	public void setGsrDatEmpGoalName(String gsrDatEmpGoalName) {
		this.gsrDatEmpGoalName = gsrDatEmpGoalName;
	}

	public Date getGsrGoalStartDate() {
		return gsrGoalStartDate;
	}

	public void setGsrGoalStartDate(Date gsrGoalStartDate) {
		this.gsrGoalStartDate = gsrGoalStartDate;
	}

	public Date getGsrGoalEndDate() {
		return gsrGoalEndDate;
	}

	public void setGsrGoalEndDate(Date gsrGoalEndDate) {
		this.gsrGoalEndDate = gsrGoalEndDate;
	}

	public Byte getGsrGoalWeightage() {
		return gsrGoalWeightage;
	}

	public void setGsrGoalWeightage(Byte gsrGoalWeightage) {
		this.gsrGoalWeightage = gsrGoalWeightage;
	}

	public String getGsrGoalDescription() {
		return gsrGoalDescription;
	}

	public void setGsrGoalDescription(String gsrGoalDescription) {
		this.gsrGoalDescription = gsrGoalDescription;
	}

	public String getGsrGoalMeasurement() {
		return gsrGoalMeasurement;
	}

	public void setGsrGoalMeasurement(String gsrGoalMeasurement) {
		this.gsrGoalMeasurement = gsrGoalMeasurement;
	}

	public String getGsrGoalAccomplishment() {
		return gsrGoalAccomplishment;
	}

	public void setGsrGoalAccomplishment(String gsrGoalAccomplishment) {
		this.gsrGoalAccomplishment = gsrGoalAccomplishment;
	}

	public String getGsrGoalMgrFeedback() {
		return gsrGoalMgrFeedback;
	}

	public void setGsrGoalMgrFeedback(String gsrGoalMgrFeedback) {
		this.gsrGoalMgrFeedback = gsrGoalMgrFeedback;
	}

	public Float getGsrGoalSelfRating() {
		return gsrGoalSelfRating;
	}

	public void setGsrGoalSelfRating(Float gsrGoalSelfRating) {
		this.gsrGoalSelfRating = gsrGoalSelfRating;
	}

	public Float getGsrGoalMgrRating() {
		return gsrGoalMgrRating;
	}

	public void setGsrGoalMgrRating(Float gsrGoalMgrRating) {
		this.gsrGoalMgrRating = gsrGoalMgrRating;
	}

	public Byte getFkGsrGoalStatus() {
		return fkGsrGoalStatus;
	}

	public void setFkGsrGoalStatus(Byte fkGsrGoalStatus) {
		this.fkGsrGoalStatus = fkGsrGoalStatus;
	}

	public String getGsrGoalStatusName() {
		return gsrGoalStatusName;
	}

	public void setGsrGoalStatusName(String gsrGoalStatusName) {
		this.gsrGoalStatusName = gsrGoalStatusName;
	}

	public Short getEmpGoalKpiWeightageId() {
		return empGoalKpiWeightageId;
	}

	public void setEmpGoalKpiWeightageId(Short empGoalKpiWeightageId) {
		this.empGoalKpiWeightageId = empGoalKpiWeightageId;
	}

	public Short getFkGsrKpiGoalId() {
		return fkGsrKpiGoalId;
	}

	public void setFkGsrKpiGoalId(Short fkGsrKpiGoalId) {
		this.fkGsrKpiGoalId = fkGsrKpiGoalId;
	}

	public Byte getGsrKpiGoalWeightage() {
		return gsrKpiGoalWeightage;
	}

	public void setGsrKpiGoalWeightage(Byte gsrKpiGoalWeightage) {
		this.gsrKpiGoalWeightage = gsrKpiGoalWeightage;
	}

	public Integer getFkGsrDelegatedGoalId() {
		return fkGsrDelegatedGoalId;
	}

	public void setFkGsrDelegatedGoalId(Integer fkGsrDelegatedGoalId) {
		this.fkGsrDelegatedGoalId = fkGsrDelegatedGoalId;
	}

	public Integer getDelegatedFromEmpId() {
		return delegatedFromEmpId;
	}

	public void setDelegatedFromEmpId(Integer delegatedFromEmpId) {
		this.delegatedFromEmpId = delegatedFromEmpId;
	}

	public String getDelegatedFromEmpName() {
		return delegatedFromEmpName;
	}

	public void setDelegatedFromEmpName(String delegatedFromEmpName) {
		this.delegatedFromEmpName = delegatedFromEmpName;
	}

	public Integer getDelegatedToEmpId() {
		return delegatedToEmpId;
	}

	public void setDelegatedToEmpId(Integer delegatedToEmpId) {
		this.delegatedToEmpId = delegatedToEmpId;
	}

	public String getDelegatedToEmpName() {
		return delegatedToEmpName;
	}

	public void setDelegatedToEmpName(String delegatedToEmpName) {
		this.delegatedToEmpName = delegatedToEmpName;
	}

	public Byte getDelegationStatus() {
		return delegationStatus;
	}

	public void setDelegationStatus(Byte delegationStatus) {
		this.delegationStatus = delegationStatus;
	}

	public Date getDelegatedDate() {
		return delegatedDate;
	}

	public void setDelegatedDate(Date delegatedDate) {
		this.delegatedDate = delegatedDate;
	}

	public Date getDelAccRejDate() {
		return delAccRejDate;
	}

	public void setDelAccRejDate(Date delAccRejDate) {
		this.delAccRejDate = delAccRejDate;
	}

	public Integer getDelegatedByEmpId() {
		return delegatedByEmpId;
	}

	public void setDelegatedByEmpId(Integer delegatedByEmpId) {
		this.delegatedByEmpId = delegatedByEmpId;
	}

	public Float getFinalRating() {
		return finalRating;
	}

	public void setFinalRating(Float finalRating) {
		this.finalRating = finalRating;
	}

	@Override
	public String toString() {
		return "DelegationResultFromDbBean [fkGsrDatEmpId=" + fkGsrDatEmpId
				+ ", empName=" + empName + ", fkGsrSlabId=" + fkGsrSlabId
				+ ", gsrSlabName=" + gsrSlabName + ", slabShortName="
				+ slabShortName + ", finalRatingAccRejDate="
				+ finalRatingAccRejDate + ", finalRatingStatus="
				+ finalRatingStatus + ", pkGsrDatEmpGoalId="
				+ pkGsrDatEmpGoalId + ", gsrDatEmpGoalName="
				+ gsrDatEmpGoalName + ", gsrGoalStartDate=" + gsrGoalStartDate
				+ ", gsrGoalEndDate=" + gsrGoalEndDate + ", gsrGoalWeightage="
				+ gsrGoalWeightage + ", gsrGoalDescription="
				+ gsrGoalDescription + ", gsrGoalMeasurement="
				+ gsrGoalMeasurement + ", gsrGoalAccomplishment="
				+ gsrGoalAccomplishment + ", gsrGoalMgrFeedback="
				+ gsrGoalMgrFeedback + ", gsrGoalSelfRating="
				+ gsrGoalSelfRating + ", gsrGoalMgrRating=" + gsrGoalMgrRating
				+ ", fkGsrGoalStatus=" + fkGsrGoalStatus
				+ ", gsrGoalStatusName=" + gsrGoalStatusName
				+ ", empGoalKpiWeightageId=" + empGoalKpiWeightageId
				+ ", fkGsrKpiGoalId=" + fkGsrKpiGoalId
				+ ", gsrKpiGoalWeightage=" + gsrKpiGoalWeightage
				+ ", fkGsrDelegatedGoalId=" + fkGsrDelegatedGoalId
				+ ", delegatedFromEmpId=" + delegatedFromEmpId
				+ ", delegatedFromEmpName=" + delegatedFromEmpName
				+ ", delegatedToEmpId=" + delegatedToEmpId
				+ ", delegatedToEmpName=" + delegatedToEmpName
				+ ", delegationStatus=" + delegationStatus + ", delegatedDate="
				+ delegatedDate + ", delAccRejDate=" + delAccRejDate
				+ ", delegatedByEmpId=" + delegatedByEmpId
				+ ", delegatedByEmpName=" + delegatedByEmpName
				+ ", finalRatingStatusName=" + finalRatingStatusName
				+ ", finalRating=" + finalRating + ", delegationStatusName="
				+ delegationStatusName + "]";
	}

}
