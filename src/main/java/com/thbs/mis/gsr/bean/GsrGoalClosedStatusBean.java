/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRGoalController.java                            */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  October 28, 2016                                  */
/*                                                                   */
/*  Description :  CRUD operations for Goals   
 * Created By   : Manikandan_chinnasamy		                        */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* October 28, 2016     THBS     1.0        Initial version created  */
/*********************************************************************/

package com.thbs.mis.gsr.bean;
/**
 * 
 * @author Thbs
 *
 */
public class GsrGoalClosedStatusBean {

	private Integer empId;

	private Short slabId;

	private Integer reportingManagerId;

	private Integer domainManagerId;

	private String status;

	public Integer getDomainManagerId() {
		return domainManagerId;
	}

	public void setDomainManagerId(Integer domainManagerId) {
		this.domainManagerId = domainManagerId;
	}

	private Short buUnit;

	public Short getSlabId() {
		return slabId;
	}

	public void setSlabId(Short slabId) {
		this.slabId = slabId;
	}

	public Integer getReportingManagerId() {
		return reportingManagerId;
	}

	public void setReportingManagerId(Integer reportingManagerId) {
		this.reportingManagerId = reportingManagerId;
	}

	public Short getBuUnit() {
		return buUnit;
	}

	public void setBuUnit(Short buUnit) {
		this.buUnit = buUnit;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "GsrGoalClosedStatusBean [empId=" + empId + ", slabId=" + slabId
				+ ", reportingManagerId=" + reportingManagerId
				+ ", domainManagerId=" + domainManagerId + ", status=" + status
				+ ", buUnit=" + buUnit + "]";
	}

}
