/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrDelegateGoalBean.java                   		 */
/*                                                                   */
/*  Author      :  THBS			                                     */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	Delegate Goal.                                   */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date              Who           Version         Comments     	 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016       THBS            1.0     Initial version created */
/*********************************************************************/
package com.thbs.mis.gsr.bean;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

/**
 * <Description GsrDelegateGoalBean:> This class contains all the 
 * Bean related methods used for the GsrDelegateGoalBean for Data Base operation.
 * @author THBS
 * @version 1.0
 * @see 
 */
public class GsrDelegateGoalBean {
	
	private List<Integer> goalIdList;
	
	@Min(1)
	@NotNull(message = "delegateToManagerId field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "delegateToManagerId must be an Integer")
	private Integer delegateToManagerId;
	
	@Min(1)
	@NotNull(message = "delegatedBy field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "delegatedBy must be an Integer")
	private Integer delegatedBy;
		
	public Integer getDelegateToManagerId() {
		return delegateToManagerId;
	}
	public void setDelegateToManagerId(Integer delegateToManagerId) {
		this.delegateToManagerId = delegateToManagerId;
	}
	
	
	public List<Integer> getGoalIdList() {
		return goalIdList;
	}
	public void setGoalIdList(List<Integer> goalIdList) {
		this.goalIdList = goalIdList;
	}
	public Integer getDelegatedBy() {
		return delegatedBy;
	}
	public void setDelegatedBy(Integer delegatedBy) {
		this.delegatedBy = delegatedBy;
	}
	@Override
	public String toString() {
		return "GsrDelegateGoalBean [goalIdList=" + goalIdList
				+ ", delegateToManagerId=" + delegateToManagerId
				+ ", delegatedBy=" + delegatedBy + "]";
	}
	
	
	
	
	
	
	
}
