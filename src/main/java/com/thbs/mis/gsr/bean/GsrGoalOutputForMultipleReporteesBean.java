package com.thbs.mis.gsr.bean;


public class GsrGoalOutputForMultipleReporteesBean {
	private Integer employeeId;
	private String employeeNameWithId;
	private String processStatus;
	private String processDesc;
	private GsrGoalOutputBean gsrGoalOutputBean;
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public String getProcessStatus() {
		return processStatus;
	}
	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}
	public String getProcessDesc() {
		return processDesc;
	}
	public void setProcessDesc(String processDesc) {
		this.processDesc = processDesc;
	}
	public GsrGoalOutputBean getGsrGoalOutputBean() {
		return gsrGoalOutputBean;
	}
	public void setGsrGoalOutputBean(GsrGoalOutputBean gsrGoalOutputBean) {
		this.gsrGoalOutputBean = gsrGoalOutputBean;
	}
	
	public String getEmployeeNameWithId() {
		return employeeNameWithId;
	}
	public void setEmployeeNameWithId(String employeeNameWithId) {
		this.employeeNameWithId = employeeNameWithId;
	}
	@Override
	public String toString() {
		return "GsrGoalOutputForMultipleReporteesBean [employeeId="
				+ employeeId + ", employeeNameWithId=" + employeeNameWithId
				+ ", processStatus=" + processStatus + ", processDesc="
				+ processDesc + ", gsrGoalOutputBean=" + gsrGoalOutputBean
				+ "]";
	}	
	
}
