/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrGoalSetStatusBean.java                     */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  18-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the 	 */
/*                 	GsrGoalSetStatusBean.                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.gsr.bean;

import java.util.List;
import java.util.Set;

import javax.validation.constraints.Min;


public class GsrGoalSetStatusBean {

	@Min(1)
//	@MIS_Id_Not_Found_Employee(message="Id is not present in database")
	private Integer empId;
	
	private Integer statusId;
	
	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	@Min(1)
//	@GSR_Id_Not_Found_Slab(message = "Slab id Is not Exists in Database.")
	private Short slabId;
	
	 
	private Set<Integer> reportingManagerId;
	
	@Min(1)
//	@MIS_Id_Not_Found_Employee(message = " Domain Manager Id Is Not Exists In Database.")
	private Integer domainManagerId;

	 
	private Long totalWeightage;
	
	private Long remainingWeightage;
	
	private String slabShortName;

	 

	public String getSlabShortName() {
		return slabShortName;
	}

	public void setSlabShortName(String slabShortName) {
		this.slabShortName = slabShortName;
	}

	public Long getRemainingWeightage() {
		return remainingWeightage;
	}

	public void setRemainingWeightage(Long remainingWeightage) {
		this.remainingWeightage = remainingWeightage;
	}
	private String status;
	
	private String slabName;
	
	private String slabDuration;

	public String getSlabDuration() {
		return slabDuration;
	}

	public void setSlabDuration(String slabDuration) {
		this.slabDuration = slabDuration;
	}

	public String getSlabName() {
		return slabName;
	}

	public void setSlabName(String slabName) {
		this.slabName = slabName;
	}

	public Integer getDomainManagerId() {
		return domainManagerId;
	}

	public void setDomainManagerId(Integer domainManagerId) {
		this.domainManagerId = domainManagerId;
	}
	//@MIS_Id_Not_Found_BusinessUnit(message="Bu Id Is Not Exists In Database.")
	private Short buUnit;

	public Short getSlabId() {
		return slabId;
	}

	public void setSlabId(Short slabId) {
		this.slabId = slabId;
	}

	public Set<Integer> getReportingManagerId() {
		return reportingManagerId;
	}

	public void setReportingManagerId(Set<Integer> reportingManagerId) {
		this.reportingManagerId = reportingManagerId;
	}

	public Short getBuUnit() {
		return buUnit;
	}

	public void setBuUnit(Short buUnit) {
		this.buUnit = buUnit;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Long getTotalWeightage() {
		return totalWeightage;
	}

	public void setTotalWeightage(Long totalWeightage) {
		this.totalWeightage =  totalWeightage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "GsrGoalSetStatusBean [empId=" + empId + ", statusId="
				+ statusId + ", slabId=" + slabId + ", reportingManagerId="
				+ reportingManagerId + ", domainManagerId=" + domainManagerId
				+ ", totalWeightage=" + totalWeightage
				+ ", remainingWeightage=" + remainingWeightage
				+ ", slabShortName=" + slabShortName + ", status=" + status
				+ ", slabName=" + slabName + ", slabDuration=" + slabDuration
				+ ", buUnit=" + buUnit + "]";
	}
	
	 
}
