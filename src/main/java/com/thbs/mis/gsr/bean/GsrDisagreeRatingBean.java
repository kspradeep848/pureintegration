/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrDisagreeRatingBean.java           		     */
/*                                                                   */
/*  Author      :  Smrithi J                                         */
/*                                                                   */
/*  Date        :  06-Apr-2017                                       */
/*                                                                   */
/*  Description :  Bean class for GSR Disagree Rating    			 */
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 24-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/


package com.thbs.mis.gsr.bean;

import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_Slab;
import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class GsrDisagreeRatingBean {

	@Employee_Id_Not_Found
	private Integer empId;
	
	@GSR_Id_Not_Found_Slab
	private Short slabId;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Short getSlabId() {
		return slabId;
	}

	public void setSlabId(Short slabId) {
		this.slabId = slabId;
	}

	@Override
	public String toString() {
		return "GsrDisagreeRatingBean [empId=" + empId + ", slabId=" + slabId
				+ "]";
	}
	
	
	
	
}
