package com.thbs.mis.gsr.bean;

import java.util.List;

public class GsrEmployeeBean {

	private GsrEmployeeDashBoardBean empWorkFlowDetails;
	private List<GsrEmployeePerformanceRatingBean> empRatings;
	public GsrEmployeeDashBoardBean getEmpWorkFlowDetails() {
		return empWorkFlowDetails;
	}
	public void setEmpWorkFlowDetails(GsrEmployeeDashBoardBean empWorkFlowDetails) {
		this.empWorkFlowDetails = empWorkFlowDetails;
	}
	public List<GsrEmployeePerformanceRatingBean> getEmpRatings() {
		return empRatings;
	}
	public void setEmpRatings(List<GsrEmployeePerformanceRatingBean> empRatings) {
		this.empRatings = empRatings;
	}
	@Override
	public String toString() {
		return "EmployeeBean [empWorkFlowDetails=" + empWorkFlowDetails
				+ ", empRatings=" + empRatings + "]";
	}
	
	
}
