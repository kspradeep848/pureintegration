/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmployeePerformanceRating.java           		 */
/*                                                                   */
/*  Author      :  Anil kumar B K	                                 */
/*                                                                   */
/*  Date        :  24-Nov-2016                                       */
/*                                                                   */
/*  Description :  Bean class for GSR dash board    			     */
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 24-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.gsr.bean;


public class EmployeePerformanceRatingBean implements Comparable<EmployeePerformanceRatingBean>{

private Float rating;
private String slab;
private Short slabId;
private Short slabYear;
private String slabName;

public Short getSlabYear() {
	return slabYear;
}
public void setSlabYear(Short slabYear) {
	this.slabYear = slabYear;
}
public Short getSlabId() {
	return slabId;
}
public void setSlabId(Short slabId) {
	this.slabId = slabId;
}
public Float getRating() {
	return rating;
}
public void setRating(Float rating) {
	this.rating = rating;
}

public String getSlab() {
	return slab;
}
public void setSlab(String slab) {
	this.slab = slab;
}
public String getSlabName() {
	return slabName;
}
public void setSlabName(String slabName) {
	this.slabName = slabName;
}




@Override
public String toString() {
	return "EmployeePerformanceRatingBean [rating=" + rating + ", slab=" + slab
			+ ", slabId=" + slabId + ", slabYear=" + slabYear + ", slabName="
			+ slabName 
			+ "]";
}
@Override
public int compareTo(EmployeePerformanceRatingBean obj) {
	
	if(this.slabId == obj.slabId)
	return 0;
	else if(this.slabId > obj.slabId)
	return 1;
	else
	return -1;
}

	
	
}
