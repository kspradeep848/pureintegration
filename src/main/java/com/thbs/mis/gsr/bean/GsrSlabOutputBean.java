package com.thbs.mis.gsr.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class GsrSlabOutputBean {
	
	
	private short pkGsrSlabId;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date gsrSlabEndDate;	
	private String gsrSlabName;
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date gsrSlabStartDate;	
	private String gsrSlabYear;	
	private String gsrSlabShortName;	
	private String gsrSlabEnabled;
	

	public String getGsrSlabEnabled() {
		return gsrSlabEnabled;
	}
	public void setGsrSlabEnabled(String gsrSlabEnabled) {
		this.gsrSlabEnabled = gsrSlabEnabled;
	}
	public short getPkGsrSlabId() {
		return pkGsrSlabId;
	}
	public void setPkGsrSlabId(short pkGsrSlabId) {
		this.pkGsrSlabId = pkGsrSlabId;
	}
	public Date getGsrSlabEndDate() {
		return gsrSlabEndDate;
	}
	public void setGsrSlabEndDate(Date gsrSlabEndDate) {
		this.gsrSlabEndDate = gsrSlabEndDate;
	}
	public String getGsrSlabName() {
		return gsrSlabName;
	}
	public void setGsrSlabName(String gsrSlabName) {
		this.gsrSlabName = gsrSlabName;
	}
	public Date getGsrSlabStartDate() {
		return gsrSlabStartDate;
	}
	public void setGsrSlabStartDate(Date gsrSlabStartDate) {
		this.gsrSlabStartDate = gsrSlabStartDate;
	}
	public String getGsrSlabYear() {
		return gsrSlabYear;
	}
	public void setGsrSlabYear(String gsrSlabYear) {
		this.gsrSlabYear = gsrSlabYear;
	}
	public String getGsrSlabShortName() {
		return gsrSlabShortName;
	}
	public void setGsrSlabShortName(String gsrSlabShortName) {
		this.gsrSlabShortName = gsrSlabShortName;
	}
	@Override
	public String toString() {
		return "GsrSlabOutputBean [pkGsrSlabId=" + pkGsrSlabId
				+ ", gsrSlabEndDate=" + gsrSlabEndDate + ", gsrSlabName="
				+ gsrSlabName + ", gsrSlabStartDate=" + gsrSlabStartDate
				+ ", gsrSlabYear=" + gsrSlabYear + ", gsrSlabShortName="
				+ gsrSlabShortName + ", gsrSlabEnabled=" + gsrSlabEnabled + "]";
	}
	
	
	
}
