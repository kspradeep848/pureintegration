package com.thbs.mis.gsr.bean;

public class GsrEmployeesAvgRatingBean {

	private Integer totalGoalsSet;
	private Integer ratedFourAndAbove;
	private Integer ratedTwoPointFiveAndBelow;
	public Integer getTotalGoalsSet() {
		return totalGoalsSet;
	}
	public void setTotalGoalsSet(Integer totalGoalsSet) {
		this.totalGoalsSet = totalGoalsSet;
	}
	public Integer getRatedFourAndAbove() {
		return ratedFourAndAbove;
	}
	public void setRatedFourAndAbove(Integer ratedFourAndAbove) {
		this.ratedFourAndAbove = ratedFourAndAbove;
	}
	public Integer getRatedTwoPointFiveAndBelow() {
		return ratedTwoPointFiveAndBelow;
	}
	public void setRatedTwoPointFiveAndBelow(Integer ratedTwoPointFiveAndBelow) {
		this.ratedTwoPointFiveAndBelow = ratedTwoPointFiveAndBelow;
	}
	@Override
	public String toString() {
		return "GsrEmployeesAvgRatingBean [totalGoalsSet=" + totalGoalsSet
				+ ", ratedFourAndAbove=" + ratedFourAndAbove
				+ ", ratedTwoPointFiveAndBelow=" + ratedTwoPointFiveAndBelow
				+ "]";
	}
	
	
}
