package com.thbs.mis.gsr.bean;

import java.util.List;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class HrViewTeamGoalsInputBean {

	@Employee_Id_Not_Found
	private Integer mgrId;

	private List<Short> slabId;
	private List<Integer> empIdList;
	private Short buId;

	public Integer getMgrId() {
		return mgrId;
	}

	public void setMgrId(Integer mgrId) {
		this.mgrId = mgrId;
	}

	public List<Short> getSlabId() {
		return slabId;
	}

	public void setSlabId(List<Short> slabId) {
		this.slabId = slabId;
	}

	public List<Integer> getEmpIdList() {
		return empIdList;
	}

	public void setEmpIdList(List<Integer> empIdList) {
		this.empIdList = empIdList;
	}

	public Short getBuId() {
		return buId;
	}

	public void setBuId(Short buId) {
		this.buId = buId;
	}

	@Override
	public String toString() {
		return "HrViewTeamGoalsInputBean [mgrId=" + mgrId + ", slabId="
				+ slabId + ", empIdList=" + empIdList + ", buId=" + buId + "]";
	}
}
