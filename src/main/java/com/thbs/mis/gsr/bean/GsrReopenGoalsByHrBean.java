/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrReopenGoalsByHrBean.java                       */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  March 29, 2017                                    */
/*                                                                   */
/*  Description :  This class contains fields required for           */
/*                   GSR Reopen goals details	 				     */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* May 30, 2017     THBS     1.0        Initial version created      */
/*********************************************************************/

package com.thbs.mis.gsr.bean;

import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Found_Goal;
import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_Slab;
import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class GsrReopenGoalsByHrBean {

	@Employee_Id_Not_Found
	Integer empId;
	
	@GSR_Id_Not_Found_Slab
	Short slabId;
	
	@Employee_Id_Not_Found
	Integer modifiedBy;
	
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public Short getSlabId() {
		return slabId;
	}
	public void setSlabId(Short slabId) {
		this.slabId = slabId;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@Override
	public String toString() {
		return "GsrReopenGoalsByHrBean [empId=" + empId + ", slabId=" + slabId
				+ ", modifiedBy=" + modifiedBy + "]";
	}
	
	
	
	
}
