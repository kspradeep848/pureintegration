package com.thbs.mis.gsr.bean;

import java.util.List;

public class GsrEmployeesRatingForHrBean {

	private GsrEmployeesAvgRatingBean employeesAvgRating;
	private List<GsrEmployeesRatingBean> listOfEmployeesRating;
	public GsrEmployeesAvgRatingBean getEmployeesAvgRating() {
		return employeesAvgRating;
	}
	public void setEmployeesAvgRating(GsrEmployeesAvgRatingBean employeesAvgRating) {
		this.employeesAvgRating = employeesAvgRating;
	}
	public List<GsrEmployeesRatingBean> getListOfEmployeesRating() {
		return listOfEmployeesRating;
	}
	public void setListOfEmployeesRating(
			List<GsrEmployeesRatingBean> listOfEmployeesRating) {
		this.listOfEmployeesRating = listOfEmployeesRating;
	}
	@Override
	public String toString() {
		return "GsrEmployeesRatingForHrBean [employeesAvgRating="
				+ employeesAvgRating + ", listOfEmployeesRating="
				+ listOfEmployeesRating + "]";
	}
	
	
	
}
