package com.thbs.mis.gsr.bean;

import java.util.Date;
import java.util.List;

public class GoalDetailsForDelegationBean {

	private Integer empId;
	private String empName;
	private Float finalRating;
	private Short slabId;
	private String slabName;
	private String slabShortName;
	private Date acceptedOrRejectedDate;
	private Byte finalRatingStatusId;
	private String finalRatingStatusName;
	private List<DelegationGoalsOutputBean> goals;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Float getFinalRating() {
		return finalRating;
	}

	public void setFinalRating(Float finalRating) {
		this.finalRating = finalRating;
	}

	public Short getSlabId() {
		return slabId;
	}

	public void setSlabId(Short slabId) {
		this.slabId = slabId;
	}

	public String getSlabName() {
		return slabName;
	}

	public void setSlabName(String slabName) {
		this.slabName = slabName;
	}

	public String getSlabShortName() {
		return slabShortName;
	}

	public void setSlabShortName(String slabShortName) {
		this.slabShortName = slabShortName;
	}

	public Date getAcceptedOrRejectedDate() {
		return acceptedOrRejectedDate;
	}

	public void setAcceptedOrRejectedDate(Date acceptedOrRejectedDate) {
		this.acceptedOrRejectedDate = acceptedOrRejectedDate;
	}

	public Byte getFinalRatingStatusId() {
		return finalRatingStatusId;
	}

	public void setFinalRatingStatusId(Byte finalRatingStatusId) {
		this.finalRatingStatusId = finalRatingStatusId;
	}

	public String getFinalRatingStatusName() {
		return finalRatingStatusName;
	}

	public void setFinalRatingStatusName(String finalRatingStatusName) {
		this.finalRatingStatusName = finalRatingStatusName;
	}

	public List<DelegationGoalsOutputBean> getGoals() {
		return goals;
	}

	public void setGoals(List<DelegationGoalsOutputBean> goals) {
		this.goals = goals;
	}

	@Override
	public String toString() {
		return "GoalDetailsForDelegationBean [empId=" + empId + ", empName="
				+ empName + ", finalRating=" + finalRating + ", slabId="
				+ slabId + ", slabName=" + slabName + ", slabShortName="
				+ slabShortName + ", acceptedOrRejectedDate="
				+ acceptedOrRejectedDate + ", finalRatingStatusId="
				+ finalRatingStatusId + ", finalRatingStatusName="
				+ finalRatingStatusName + ", goals=" + goals + "]";
	}

}
