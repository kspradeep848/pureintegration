/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  FeedbackListBean.java                             */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  March 28, 2017                                    */
/*                                                                   */
/*  Description :  This class contains fields required for viewing   */
/*                   feedback details	 				             */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* November 02, 2016     THBS     1.0        Initial version created */
/*********************************************************************/

package com.thbs.mis.gsr.bean;

public class FeedbackListBean {

	private Integer mgrId;
	private String mgrName;
	private String mgrFeedback;
	private Float rating;
	private String delegatedMgrName;
	private Byte delegationStatus;
	private Integer fkMgrIdDelegatedTo;

	public String getMgrName() {
		return mgrName;
	}

	public void setMgrName(String mgrName) {
		this.mgrName = mgrName;
	}

	public Integer getMgrId() {
		return mgrId;
	}

	public void setMgrId(Integer mgrId) {
		this.mgrId = mgrId;
	}

	public String getMgrFeedback() {
		return mgrFeedback;
	}

	public void setMgrFeedback(String mgrFeedback) {
		this.mgrFeedback = mgrFeedback;
	}

	public Float getRating() {
		return rating;
	}

	public void setRating(Float rating) {
		this.rating = rating;
	}

	public String getDelegatedMgrName() {
		return delegatedMgrName;
	}

	public void setDelegatedMgrName(String delegatedMgrName) {
		this.delegatedMgrName = delegatedMgrName;
	}

	public Byte getDelegationStatus() {
		return delegationStatus;
	}

	public void setDelegationStatus(Byte delegationStatus) {
		this.delegationStatus = delegationStatus;
	}

	public Integer getFkMgrIdDelegatedTo() {
		return fkMgrIdDelegatedTo;
	}

	public void setFkMgrIdDelegatedTo(Integer fkMgrIdDelegatedTo) {
		this.fkMgrIdDelegatedTo = fkMgrIdDelegatedTo;
	}

	@Override
	public String toString() {
		return "FeedbackListBean [mgrId=" + mgrId + ", mgrName=" + mgrName
				+ ", mgrFeedback=" + mgrFeedback + ", rating=" + rating
				+ ", delegatedMgrName=" + delegatedMgrName
				+ ", delegationStatus=" + delegationStatus
				+ ", fkMgrIdDelegatedTo=" + fkMgrIdDelegatedTo + "]";
	}

}
