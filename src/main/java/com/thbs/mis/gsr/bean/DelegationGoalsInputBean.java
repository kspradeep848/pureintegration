package com.thbs.mis.gsr.bean;

import java.util.List;

public class DelegationGoalsInputBean {

	private List<Integer> empIdList;
	private Integer delegationMgrId;

	public List<Integer> getEmpIdList() {
		return empIdList;
	}

	public void setEmpIdList(List<Integer> empIdList) {
		this.empIdList = empIdList;
	}

	public Integer getDelegationMgrId() {
		return delegationMgrId;
	}

	public void setDelegationMgrId(Integer delegationMgrId) {
		this.delegationMgrId = delegationMgrId;
	}

	@Override
	public String toString() {
		return "DelegationGoalsInputBean [empIdList=" + empIdList
				+ ", delegationMgrId=" + delegationMgrId + "]";
	}
}
