package com.thbs.mis.gsr.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class GsrEnableSettingsBean
{
	@Min(1)
	@NumberFormat(style = Style.NUMBER)
	@NotNull(message = "pkGsrAdminConfigId should not be null.")
	private Integer pkGsrAdminConfigId;
	
	@NotBlank(message = "enableDisableSettings should not be blank.")
	@NotNull(message = "enableDisableSettings should not be null.")
	@NotEmpty(message = "enableDisableSettings should not be empty.")
	private String enableDisableSettings;
	
	public Integer getPkGsrAdminConfigId()
	{
		return pkGsrAdminConfigId;
	}
	public void setPkGsrAdminConfigId(Integer pkGsrAdminConfigId)
	{
		this.pkGsrAdminConfigId = pkGsrAdminConfigId;
	}
	public String getEnableDisableSettings()
	{
		return enableDisableSettings;
	}
	public void setEnableDisableSettings(String enableDisableSettings)
	{
		this.enableDisableSettings = enableDisableSettings;
	}
	@Override
	public String toString()
	{
		return "GsrEnableSettingsBean [pkGsrAdminConfigId="
				+ pkGsrAdminConfigId + ", enableDisableSettings="
				+ enableDisableSettings + "]";
	}
}
