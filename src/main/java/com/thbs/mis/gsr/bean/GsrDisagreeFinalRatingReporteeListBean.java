package com.thbs.mis.gsr.bean;

public class GsrDisagreeFinalRatingReporteeListBean {
	
	private int finalRatingPkId;
	private int reportingMgrId;
	private int reporteeId;
	private String reporteeName;
	private float disagreeFinalRating;
	
	public int getFinalRatingPkId() {
		return finalRatingPkId;
	}
	public void setFinalRatingPkId(int finalRatingPkId) {
		this.finalRatingPkId = finalRatingPkId;
	}
	
	public int getReportingMgrId() {
		return reportingMgrId;
	}
	public void setReportingMgrId(int reportingMgrId) {
		this.reportingMgrId = reportingMgrId;
	}
	public int getReporteeId() {
		return reporteeId;
	}
	public void setReporteeId(int reporteeId) {
		this.reporteeId = reporteeId;
	}
	public String getReporteeName() {
		return reporteeName;
	}
	public void setReporteeName(String reporteeName) {
		this.reporteeName = reporteeName;
	}
	public float getDisagreeFinalRating() {
		return disagreeFinalRating;
	}
	public void setDisagreeFinalRating(float disagreeFinalRating) {
		this.disagreeFinalRating = disagreeFinalRating;
	}
	@Override
	public String toString() {
		return "GsrDisagreeFinalRatingReporteeListBean [finalRatingPkId="
				+ finalRatingPkId + ", reportingMgrId=" + reportingMgrId
				+ ", reporteeId=" + reporteeId + ", reporteeName="
				+ reporteeName + ", disagreeFinalRating=" + disagreeFinalRating
				+ "]";
	}
	
}
