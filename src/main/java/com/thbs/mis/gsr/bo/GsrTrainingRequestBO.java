package com.thbs.mis.gsr.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.MasTrainingTopicNameBO;


/**
 * The persistent class for the gsr_training_request database table.
 * 
 */
@Entity
@Table(name="gsr_training_request")
@NamedQuery(name="GsrTrainingRequestBO.findAll", query="SELECT g FROM GsrTrainingRequestBO g")
public class GsrTrainingRequestBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_gsr_training_request_id")
	private int pkGsrTrainingRequestId;

	private String comments;

	@Column(name="fk_gsr_training_topic_id")
	private int fkGsrTrainingTopicId;

	@Column(name="gsr_training_requested_by")
	private int gsrTrainingRequestedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="gsr_training_requested_date")
	private Date gsrTrainingRequestedDate;

	@Column(name="gsr_training_requested_for_emp_id")
	private int gsrTrainingRequestedForEmpId;

	@Column(name="other_topic_description")
	private String otherTopicDescription;

	@OneToOne
	@JoinColumn(name = "fk_gsr_training_topic_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasTrainingTopicNameBO masTrainingTopic;
	
	public GsrTrainingRequestBO() {
	}

	public int getPkGsrTrainingRequestId() {
		return this.pkGsrTrainingRequestId;
	}

	public void setPkGsrTrainingRequestId(int pkGsrTrainingRequestId) {
		this.pkGsrTrainingRequestId = pkGsrTrainingRequestId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getFkGsrTrainingTopicId() {
		return this.fkGsrTrainingTopicId;
	}

	public void setFkGsrTrainingTopicId(int fkGsrTrainingTopicId) {
		this.fkGsrTrainingTopicId = fkGsrTrainingTopicId;
	}

	public int getGsrTrainingRequestedBy() {
		return this.gsrTrainingRequestedBy;
	}

	public void setGsrTrainingRequestedBy(int gsrTrainingRequestedBy) {
		this.gsrTrainingRequestedBy = gsrTrainingRequestedBy;
	}

	public Date getGsrTrainingRequestedDate() {
		return this.gsrTrainingRequestedDate;
	}

	public void setGsrTrainingRequestedDate(Date gsrTrainingRequestedDate) {
		this.gsrTrainingRequestedDate = gsrTrainingRequestedDate;
	}

	public int getGsrTrainingRequestedForEmpId() {
		return this.gsrTrainingRequestedForEmpId;
	}

	public void setGsrTrainingRequestedForEmpId(int gsrTrainingRequestedForEmpId) {
		this.gsrTrainingRequestedForEmpId = gsrTrainingRequestedForEmpId;
	}

	public String getOtherTopicDescription() {
		return this.otherTopicDescription;
	}

	public void setOtherTopicDescription(String otherTopicDescription) {
		this.otherTopicDescription = otherTopicDescription;
	}

	@Override
	public String toString()
	{
		return "GsrTrainingRequestBO [pkGsrTrainingRequestId="
				+ pkGsrTrainingRequestId + ", comments=" + comments
				+ ", fkGsrTrainingTopicId=" + fkGsrTrainingTopicId
				+ ", gsrTrainingRequestedBy=" + gsrTrainingRequestedBy
				+ ", gsrTrainingRequestedDate="
				+ gsrTrainingRequestedDate
				+ ", gsrTrainingRequestedForEmpId="
				+ gsrTrainingRequestedForEmpId
				+ ", otherTopicDescription=" + otherTopicDescription
				+ ", masTrainingTopic=" + masTrainingTopic + "]";
	}

}