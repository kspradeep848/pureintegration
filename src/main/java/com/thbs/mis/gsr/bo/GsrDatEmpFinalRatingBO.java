package com.thbs.mis.gsr.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.thbs.mis.common.bo.DatEmpDetailBO;

/**
 * The persistent class for the gsr_dat_emp_final_rating database table.
 * 
 */
@Entity
@Table(name = "gsr_dat_emp_final_rating")
@NamedQuery(name = "GsrDatEmpFinalRatingBO.findAll", query = "SELECT g FROM GsrDatEmpFinalRatingBO g")
public class GsrDatEmpFinalRatingBO implements Serializable {
	private static final long serialVersionUID = 1L;

	GsrDatEmpFinalRatingBO( Byte finalRatingStatus){
		this.finalRatingStatus = finalRatingStatus;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_gsr_final_rating_id")
	private int pkGsrFinalRatingId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "final_rating_status")
	private Byte finalRatingStatus;

	@Column(name = "fk_gsr_final_rating_emp_id")
	private int fkGsrFinalRatingEmpId;

	@Column(name = "fk_gsr_final_rating_mgr_id")
	private Integer fkGsrFinalRatingMgrId;

	@Column(name = "fk_gsr_final_rating_slab_id")
	private short fkGsrFinalRatingSlabId;

	@Column(name = "gsr_final_rating")
	private Float gsrFinalRating;

	@Column(name = "is_overridden_flag")
	private String isOverriddenFlag;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	@Column(name = "overridden_by")
	private Integer overriddenBy;

	@Column(name = "overridden_comments")
	private String overriddenComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "rating_accept_or_reject_date")
	private Date ratingAcceptOrRejectDate;

	@Column(name = "skip_level_mgr_id")
	private Integer skipLevelMgrId;
	
	@Column(name = "closed_by")
	private Integer closedBy;

	@OneToOne
	@JoinColumn(name = "fk_gsr_final_rating_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO_final_rating_emp_id;

	@OneToOne
	@JoinColumn(name = "fk_gsr_final_rating_mgr_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO_final_rating_mgr_id;

	@OneToOne
	@JoinColumn(name = "fk_gsr_final_rating_slab_id", unique = true, nullable = true, insertable = false, updatable = false)
	private GsrSlabBO gsrSlabBO_slab_id;

	@OneToOne
	@JoinColumn(name = "overridden_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO_overridden_by;

	@OneToOne
	@JoinColumn(name = "skip_level_mgr_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO_skip_level_mgr_id;
	
	@OneToOne
	@JoinColumn(name = "closed_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO closedByEmpDetailBO;

	@Transient
	private String empFirstName;
	@Transient
	private String empLastName;
	@Transient
	private String mgrFirstName;
	@Transient
	private String mgrLastName;
	
	
	public GsrDatEmpFinalRatingBO(Float gsrFinalRating,
			GsrSlabBO gsrSlabBO_slab_id) {
		super();
		this.gsrFinalRating = gsrFinalRating;
		this.gsrSlabBO_slab_id = gsrSlabBO_slab_id;
	}
	
	public GsrDatEmpFinalRatingBO() {
	}
	
	public GsrDatEmpFinalRatingBO(short slabId, int employeeId, float gsrFinalRating, int finalRatingPkId,
			String empFirstName, String empLastName, Byte finalRatingStatus) 
	{
		this.fkGsrFinalRatingSlabId = slabId;
		this.fkGsrFinalRatingEmpId = employeeId;
		this.gsrFinalRating = gsrFinalRating;
		this.pkGsrFinalRatingId = finalRatingPkId;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.finalRatingStatus = finalRatingStatus;
	}
	
	
	public GsrDatEmpFinalRatingBO(short slabId, int employeeId, String empFirstname, String empLastName, int reportingMgrId,
			String mgrFirstName, String mgrLastName, int finalRatingPkId, Byte finalRatingStatus, Float gsrFinalRating) 
	{
		this.fkGsrFinalRatingSlabId = slabId;
		this.fkGsrFinalRatingEmpId = employeeId;
		this.empFirstName = empFirstname;
		this.empLastName = empLastName;
		this.fkGsrFinalRatingMgrId = reportingMgrId;
		this.mgrFirstName = mgrFirstName;
		this.mgrLastName = mgrLastName;
		this.pkGsrFinalRatingId = finalRatingPkId;
		this.finalRatingStatus = finalRatingStatus;
		this.gsrFinalRating = gsrFinalRating;
	}


	public Integer getClosedBy() {
		return closedBy;
	}

	public void setClosedBy(Integer closedBy) {
		this.closedBy = closedBy;
	}

	public DatEmpDetailBO getClosedByEmpDetailBO() {
		return closedByEmpDetailBO;
	}

	public void setClosedByEmpDetailBO(DatEmpDetailBO closedByEmpDetailBO) {
		this.closedByEmpDetailBO = closedByEmpDetailBO;
	}

	
	/*public DatEmpPersonalDetailBO getOverridenByPersonalDetailBO() {
		return overridenByPersonalDetailBO;
	}

	public void setOverridenByPersonalDetailBO(
			DatEmpPersonalDetailBO overridenByPersonalDetailBO) {
		this.overridenByPersonalDetailBO = overridenByPersonalDetailBO;
	}*/

	public int getPkGsrFinalRatingId() {
		return this.pkGsrFinalRatingId;
	}

	public void setPkGsrFinalRatingId(int pkGsrFinalRatingId) {
		this.pkGsrFinalRatingId = pkGsrFinalRatingId;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Byte getFinalRatingStatus() {
		return this.finalRatingStatus;
	}

	public void setFinalRatingStatus(Byte finalRatingStatus) {
		this.finalRatingStatus = finalRatingStatus;
	}

	public int getFkGsrFinalRatingEmpId() {
		return this.fkGsrFinalRatingEmpId;
	}

	public void setFkGsrFinalRatingEmpId(int fkGsrFinalRatingEmpId) {
		this.fkGsrFinalRatingEmpId = fkGsrFinalRatingEmpId;
	}

	public Integer getFkGsrFinalRatingMgrId() {
		return fkGsrFinalRatingMgrId;
	}

	public void setFkGsrFinalRatingMgrId(Integer fkGsrFinalRatingMgrId) {
		this.fkGsrFinalRatingMgrId = fkGsrFinalRatingMgrId;
	}

	public short getFkGsrFinalRatingSlabId() {
		return this.fkGsrFinalRatingSlabId;
	}

	public void setFkGsrFinalRatingSlabId(short fkGsrFinalRatingSlabId) {
		this.fkGsrFinalRatingSlabId = fkGsrFinalRatingSlabId;
	}

	public Float getGsrFinalRating() {
		return gsrFinalRating;
	}

	public void setGsrFinalRating(Float gsrFinalRating) {
		this.gsrFinalRating = gsrFinalRating;
	}

	public String getIsOverriddenFlag() {
		return this.isOverriddenFlag;
	}

	public void setIsOverriddenFlag(String isOverriddenFlag) {
		this.isOverriddenFlag = isOverriddenFlag;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getOverriddenBy() {
		return this.overriddenBy;
	}

	public void setOverriddenBy(Integer overriddenBy) {
		this.overriddenBy = overriddenBy;
	}

	public String getOverriddenComments() {
		return this.overriddenComments;
	}

	public void setOverriddenComments(String overriddenComments) {
		this.overriddenComments = overriddenComments;
	}

	public Date getRatingAcceptOrRejectDate() {
		return this.ratingAcceptOrRejectDate;
	}

	public void setRatingAcceptOrRejectDate(Date ratingAcceptOrRejectDate) {
		this.ratingAcceptOrRejectDate = ratingAcceptOrRejectDate;
	}

	public Integer getSkipLevelMgrId() {
		return this.skipLevelMgrId;
	}

	public void setSkipLevelMgrId(Integer skipLevelMgrId) {
		this.skipLevelMgrId = skipLevelMgrId;
	}

	public DatEmpDetailBO getDatEmpDetailBO_final_rating_emp_id() {
		return datEmpDetailBO_final_rating_emp_id;
	}

	public void setDatEmpDetailBO_final_rating_emp_id(
			DatEmpDetailBO datEmpDetailBO_final_rating_emp_id) {
		this.datEmpDetailBO_final_rating_emp_id = datEmpDetailBO_final_rating_emp_id;
	}

	public DatEmpDetailBO getDatEmpDetailBO_final_rating_mgr_id() {
		return datEmpDetailBO_final_rating_mgr_id;
	}

	public void setDatEmpDetailBO_final_rating_mgr_id(
			DatEmpDetailBO datEmpDetailBO_final_rating_mgr_id) {
		this.datEmpDetailBO_final_rating_mgr_id = datEmpDetailBO_final_rating_mgr_id;
	}

	public GsrSlabBO getGsrSlabBO_slab_id() {
		return gsrSlabBO_slab_id;
	}

	public void setGsrSlabBO_slab_id(GsrSlabBO gsrSlabBO_slab_id) {
		this.gsrSlabBO_slab_id = gsrSlabBO_slab_id;
	}

	public DatEmpDetailBO getDatEmpDetailBO_overridden_by() {
		return datEmpDetailBO_overridden_by;
	}

	public void setDatEmpDetailBO_overridden_by(
			DatEmpDetailBO datEmpDetailBO_overridden_by) {
		this.datEmpDetailBO_overridden_by = datEmpDetailBO_overridden_by;
	}

	public DatEmpDetailBO getDatEmpDetailBO_skip_level_mgr_id() {
		return datEmpDetailBO_skip_level_mgr_id;
	}

	public void setDatEmpDetailBO_skip_level_mgr_id(
			DatEmpDetailBO datEmpDetailBO_skip_level_mgr_id) {
		this.datEmpDetailBO_skip_level_mgr_id = datEmpDetailBO_skip_level_mgr_id;
	}

	public String getEmpFirstName() {
		return empFirstName;
	}

	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}

	public String getEmpLastName() {
		return empLastName;
	}

	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}

	public String getMgrFirstName() {
		return mgrFirstName;
	}

	public void setMgrFirstName(String mgrFirstName) {
		this.mgrFirstName = mgrFirstName;
	}

	public String getMgrLastName() {
		return mgrLastName;
	}

	public void setMgrLastName(String mgrLastName) {
		this.mgrLastName = mgrLastName;
	}

	@Override
	public String toString() {
		return "GsrDatEmpFinalRatingBO [pkGsrFinalRatingId="
				+ pkGsrFinalRatingId + ", createdDate=" + createdDate
				+ ", finalRatingStatus=" + finalRatingStatus
				+ ", fkGsrFinalRatingEmpId=" + fkGsrFinalRatingEmpId
				+ ", fkGsrFinalRatingMgrId=" + fkGsrFinalRatingMgrId
				+ ", fkGsrFinalRatingSlabId=" + fkGsrFinalRatingSlabId
				+ ", gsrFinalRating=" + gsrFinalRating + ", isOverriddenFlag="
				+ isOverriddenFlag + ", modifiedDate=" + modifiedDate
				+ ", overriddenBy=" + overriddenBy + ", overriddenComments="
				+ overriddenComments + ", ratingAcceptOrRejectDate="
				+ ratingAcceptOrRejectDate + ", skipLevelMgrId="
				+ skipLevelMgrId + ", closedBy=" + closedBy
				+ ", datEmpDetailBO_final_rating_emp_id="
				+ datEmpDetailBO_final_rating_emp_id
				+ ", datEmpDetailBO_final_rating_mgr_id="
				+ datEmpDetailBO_final_rating_mgr_id + ", gsrSlabBO_slab_id="
				+ gsrSlabBO_slab_id + ", datEmpDetailBO_overridden_by="
				+ datEmpDetailBO_overridden_by
				+ ", datEmpDetailBO_skip_level_mgr_id="
				+ datEmpDetailBO_skip_level_mgr_id + ", closedByEmpDetailBO="
				+ closedByEmpDetailBO + ", empFirstName=" + empFirstName
				+ ", empLastName=" + empLastName + ", mgrFirstName="
				+ mgrFirstName + ", mgrLastName=" + mgrLastName + "]";
	}

}