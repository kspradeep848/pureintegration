package com.thbs.mis.gsr.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

/**
 * The persistent class for the gsr_slab database table.
 * 
 */
@Entity
@Table(name = "gsr_slab")
@NamedQuery(name = "GsrSlabBO.findAll", query = "SELECT g FROM GsrSlabBO g")
public class GsrSlabBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_gsr_slab_id")
	private short pkGsrSlabId;

	@JsonSerialize(using = CustomTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "gsr_slab_end_date")
	private Date gsrSlabEndDate;

	@Column(name = "gsr_slab_name")
	private String gsrSlabName;

	@JsonSerialize(using = CustomTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "gsr_slab_start_date")
	private Date gsrSlabStartDate;

	/* @Temporal(TemporalType.TIMESTAMP) */
	@Column(name = "gsr_slab_year")
	private String gsrSlabYear;

	@Column(name = "gsr_slab_short_name")
	private String gsrSlabShortName;

	@Column(name = "gsr_slab_enabled")
	private String gsrSlabEnabled;

	@Transient
	private Float finalRating;

	public GsrSlabBO(short pkGsrSlabId, Date gsrSlabEndDate,
			String gsrSlabName, Date gsrSlabStartDate, String gsrSlabYear,
			String gsrSlabShortName, Float finalRating) {
		super();
		this.pkGsrSlabId = pkGsrSlabId;
		this.gsrSlabEndDate = gsrSlabEndDate;
		this.gsrSlabName = gsrSlabName;
		this.gsrSlabStartDate = gsrSlabStartDate;
		this.gsrSlabYear = gsrSlabYear;
		this.gsrSlabShortName = gsrSlabShortName;
		this.finalRating = finalRating;
	}

	public Float getFinalRating() {
		return finalRating;
	}

	public void setFinalRating(Float finalRating) {
		this.finalRating = finalRating;
	}

	public GsrSlabBO() {
	}

	public short getPkGsrSlabId() {
		return this.pkGsrSlabId;
	}

	public void setPkGsrSlabId(short pkGsrSlabId) {
		this.pkGsrSlabId = pkGsrSlabId;
	}

	public Date getGsrSlabEndDate() {
		return this.gsrSlabEndDate;
	}

	public void setGsrSlabEndDate(Date gsrSlabEndDate) {
		this.gsrSlabEndDate = gsrSlabEndDate;
	}

	public String getGsrSlabName() {
		return this.gsrSlabName;
	}

	public void setGsrSlabName(String gsrSlabName) {
		this.gsrSlabName = gsrSlabName;
	}

	public Date getGsrSlabStartDate() {
		return this.gsrSlabStartDate;
	}

	public void setGsrSlabStartDate(Date gsrSlabStartDate) {
		this.gsrSlabStartDate = gsrSlabStartDate;
	}

	public String getGsrSlabShortName() {
		return gsrSlabShortName;
	}

	public void setGsrSlabShortName(String gsrSlabShortName) {
		this.gsrSlabShortName = gsrSlabShortName;
	}

	public String getGsrSlabYear() {
		return gsrSlabYear;
	}

	public void setGsrSlabYear(String gsrSlabYear) {
		this.gsrSlabYear = gsrSlabYear;
	}

	public String getGsrSlabEnabled() {
		return gsrSlabEnabled;
	}

	public void setGsrSlabEnabled(String gsrSlabEnabled) {
		this.gsrSlabEnabled = gsrSlabEnabled;
	}

	@Override
	public String toString() {
		return "GsrSlabBO [pkGsrSlabId=" + pkGsrSlabId + ", gsrSlabEndDate="
				+ gsrSlabEndDate + ", gsrSlabName=" + gsrSlabName
				+ ", gsrSlabStartDate=" + gsrSlabStartDate + ", gsrSlabYear="
				+ gsrSlabYear + ", gsrSlabShortName=" + gsrSlabShortName
				+ ", gsrSlabEnabled=" + gsrSlabEnabled + ", finalRating="
				+ finalRating + "]";
	}

}