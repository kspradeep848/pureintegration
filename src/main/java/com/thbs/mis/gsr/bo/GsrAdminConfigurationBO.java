package com.thbs.mis.gsr.bo;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the gsr_admin_configuration database table.
 * 
 */
@Entity
@Table(name="gsr_admin_configuration")
@NamedQuery(name="GsrAdminConfigurationBO.findAll", query="SELECT g FROM GsrAdminConfigurationBO g")
public class GsrAdminConfigurationBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_gsr_admin_config_id")
	private int pkGsrAdminConfigId;

	@Column(name="enable_gsr_window")
	private String enableGsrWindow;

	@Column(name="enable_self_rating")
	private String enableSelfRating;

	@Column(name="enable_training_request")
	private String enableTrainingRequest;
	
	@Column(name="fk_gsr_slab_id")
	private int fkGsrSlabId;
	
	@Column(name="enable_org_wide_goal")
	private String enableOrgWideGoal;
	
	@OneToOne
	@JoinColumn(name="fk_gsr_slab_id",unique = true, nullable = true, insertable = false, updatable = false)
	private GsrSlabBO gsrSlabBO;

	public GsrAdminConfigurationBO() {
	}

	public int getPkGsrAdminConfigId() {
		return pkGsrAdminConfigId;
	}

	public void setPkGsrAdminConfigId(int pkGsrAdminConfigId) {
		this.pkGsrAdminConfigId = pkGsrAdminConfigId;
	}

	public String getEnableGsrWindow() {
		return enableGsrWindow;
	}

	public void setEnableGsrWindow(String enableGsrWindow) {
		this.enableGsrWindow = enableGsrWindow;
	}

	public String getEnableSelfRating() {
		return enableSelfRating;
	}

	public void setEnableSelfRating(String enableSelfRating) {
		this.enableSelfRating = enableSelfRating;
	}

	public String getEnableTrainingRequest() {
		return enableTrainingRequest;
	}

	public void setEnableTrainingRequest(String enableTrainingRequest) {
		this.enableTrainingRequest = enableTrainingRequest;
	}

	public int getFkGsrSlabId() {
		return fkGsrSlabId;
	}

	public void setFkGsrSlabId(int fkGsrSlabId) {
		this.fkGsrSlabId = fkGsrSlabId;
	}

	public GsrSlabBO getGsrSlabBO() {
		return gsrSlabBO;
	}

	public void setGsrSlabBO(GsrSlabBO gsrSlabBO) {
		this.gsrSlabBO = gsrSlabBO;
	}

	public String getEnableOrgWideGoal() {
		return enableOrgWideGoal;
	}

	public void setEnableOrgWideGoal(String enableOrgWideGoal) {
		this.enableOrgWideGoal = enableOrgWideGoal;
	}

	@Override
	public String toString() {
		return "GsrAdminConfigurationBO [pkGsrAdminConfigId="
				+ pkGsrAdminConfigId + ", enableGsrWindow=" + enableGsrWindow
				+ ", enableSelfRating=" + enableSelfRating
				+ ", enableTrainingRequest=" + enableTrainingRequest
				+ ", fkGsrSlabId=" + fkGsrSlabId + ", enableOrgWideGoal="
				+ enableOrgWideGoal + ", gsrSlabBO=" + gsrSlabBO + "]";
	}

	
}