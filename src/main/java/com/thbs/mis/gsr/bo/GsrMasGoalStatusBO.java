package com.thbs.mis.gsr.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the gsr_mas_goal_status database table.
 * 
 */
@Entity
@Table(name="gsr_mas_goal_status")
@NamedQuery(name="GsrMasGoalStatusBO.findAll", query="SELECT g FROM GsrMasGoalStatusBO g")
public class GsrMasGoalStatusBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_gsr_goal_status_id")
	private byte pkGsrGoalStatusId;

	@Column(name="gsr_goal_status_name")
	private String gsrGoalStatusName;

	public GsrMasGoalStatusBO() {
	}

	public byte getPkGsrGoalStatusId() {
		return this.pkGsrGoalStatusId;
	}

	public void setPkGsrGoalStatusId(byte pkGsrGoalStatusId) {
		this.pkGsrGoalStatusId = pkGsrGoalStatusId;
	}

	public String getGsrGoalStatusName() {
		return this.gsrGoalStatusName;
	}

	public void setGsrGoalStatusName(String gsrGoalStatusName) {
		this.gsrGoalStatusName = gsrGoalStatusName;
	}

	@Override
	public String toString()
	{
		return "GsrMasGoalStatusBO [pkGsrGoalStatusId="
				+ pkGsrGoalStatusId + ", gsrGoalStatusName="
				+ gsrGoalStatusName + "]";
	}

}