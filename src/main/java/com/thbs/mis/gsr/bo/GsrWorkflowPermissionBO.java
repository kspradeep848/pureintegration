package com.thbs.mis.gsr.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.framework.util.CustomTimeSerializer;


/**
 * The persistent class for the gsr_workflow_permission database table.
 * 
 */
@Entity
@Table(name="gsr_workflow_permission")
@NamedQuery(name="GsrWorkflowPermissionBO.findAll", query="SELECT g FROM GsrWorkflowPermissionBO g")
public class GsrWorkflowPermissionBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_gsr_workflow_permission_id")
	private int pkGsrWorkflowPermissionId;

	@Column(name="enable_enter_accomplishments")
	private String enableEnterAccomplishments;

	@Column(name="enable_feedback")
	private String enableFeedback;

	@Column(name="enable_goals_creation")
	private String enableGoalsCreation;

	@Column(name="fk_gsr_workflow_permission_emp_id")
	private int fkGsrWorkflowPermissionEmpId;

	@Column(name="fk_gsr_workflow_permission_slab_id")
	private short fkGsrWorkflowPermissionSlabId;

	@Column(name="gsr_workflow_permission_requested_by")
	private int gsrWorkflowPermissionRequestedBy;

	@JsonSerialize(using = CustomTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="gsr_workflow_permission_requested_date")
	private Date gsrWorkflowPermissionRequestedDate;
	
	@JsonSerialize(using = CustomTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="gsr_workflow_permission_start_date")
	private Date gsrWorkflowPermissionStartDate;

	@JsonSerialize(using = CustomTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="gsr_workflow_permission_end_date")
	private Date gsrWorkflowPermissionEndDate;
	@OneToOne()
	@JoinColumn(name = "fk_gsr_workflow_permission_slab_id", unique = true, nullable = true, insertable = false, updatable = false)
	private GsrSlabBO gsrSlab;
	
	
	@OneToOne()
	@JoinColumn(name = "fk_gsr_workflow_permission_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmployee;
	
	public GsrWorkflowPermissionBO() {
	}

	public int getPkGsrWorkflowPermissionId() {
		return this.pkGsrWorkflowPermissionId;
	}

	public void setPkGsrWorkflowPermissionId(int pkGsrWorkflowPermissionId) {
		this.pkGsrWorkflowPermissionId = pkGsrWorkflowPermissionId;
	}

	public String getEnableEnterAccomplishments() {
		return this.enableEnterAccomplishments;
	}

	public void setEnableEnterAccomplishments(String enableEnterAccomplishments) {
		this.enableEnterAccomplishments = enableEnterAccomplishments;
	}

	public String getEnableFeedback() {
		return this.enableFeedback;
	}

	public void setEnableFeedback(String enableFeedback) {
		this.enableFeedback = enableFeedback;
	}

	public String getEnableGoalsCreation() {
		return this.enableGoalsCreation;
	}

	public void setEnableGoalsCreation(String enableGoalsCreation) {
		this.enableGoalsCreation = enableGoalsCreation;
	}

	public int getFkGsrWorkflowPermissionEmpId() {
		return this.fkGsrWorkflowPermissionEmpId;
	}

	public void setFkGsrWorkflowPermissionEmpId(int fkGsrWorkflowPermissionEmpId) {
		this.fkGsrWorkflowPermissionEmpId = fkGsrWorkflowPermissionEmpId;
	}

	public short getFkGsrWorkflowPermissionSlabId() {
		return this.fkGsrWorkflowPermissionSlabId;
	}

	public void setFkGsrWorkflowPermissionSlabId(short fkGsrWorkflowPermissionSlabId) {
		this.fkGsrWorkflowPermissionSlabId = fkGsrWorkflowPermissionSlabId;
	}

	public int getGsrWorkflowPermissionRequestedBy() {
		return this.gsrWorkflowPermissionRequestedBy;
	}

	public void setGsrWorkflowPermissionRequestedBy(int gsrWorkflowPermissionRequestedBy) {
		this.gsrWorkflowPermissionRequestedBy = gsrWorkflowPermissionRequestedBy;
	}

	public Date getGsrWorkflowPermissionRequestedDate() {
		return this.gsrWorkflowPermissionRequestedDate;
	}

	public void setGsrWorkflowPermissionRequestedDate(Date gsrWorkflowPermissionRequestedDate) {
		this.gsrWorkflowPermissionRequestedDate = gsrWorkflowPermissionRequestedDate;
	}

	public Date getGsrWorkflowPermissionStartDate()
	{
		return gsrWorkflowPermissionStartDate;
	}

	public void setGsrWorkflowPermissionStartDate(
			Date gsrWorkflowPermissionStartDate)
	{
		this.gsrWorkflowPermissionStartDate = gsrWorkflowPermissionStartDate;
	}

	public Date getGsrWorkflowPermissionEndDate()
	{
		return gsrWorkflowPermissionEndDate;
	}

	public void setGsrWorkflowPermissionEndDate(
			Date gsrWorkflowPermissionEndDate)
	{
		this.gsrWorkflowPermissionEndDate = gsrWorkflowPermissionEndDate;
	}

	public GsrSlabBO getGsrSlab()
	{
		return gsrSlab;
	}

	public void setGsrSlab(GsrSlabBO gsrSlab)
	{
		this.gsrSlab = gsrSlab;
	}

	public DatEmpDetailBO getDatEmployee()
	{
		return datEmployee;
	}

	public void setDatEmployee(DatEmpDetailBO datEmployee)
	{
		this.datEmployee = datEmployee;
	}

	@Override
	public String toString()
	{
		return "GsrWorkflowPermissionBO [pkGsrWorkflowPermissionId="
				+ pkGsrWorkflowPermissionId
				+ ", enableEnterAccomplishments="
				+ enableEnterAccomplishments + ", enableFeedback="
				+ enableFeedback + ", enableGoalsCreation="
				+ enableGoalsCreation
				+ ", fkGsrWorkflowPermissionEmpId="
				+ fkGsrWorkflowPermissionEmpId
				+ ", fkGsrWorkflowPermissionSlabId="
				+ fkGsrWorkflowPermissionSlabId
				+ ", gsrWorkflowPermissionRequestedBy="
				+ gsrWorkflowPermissionRequestedBy
				+ ", gsrWorkflowPermissionRequestedDate="
				+ gsrWorkflowPermissionRequestedDate
				+ ", gsrWorkflowPermissionStartDate="
				+ gsrWorkflowPermissionStartDate
				+ ", gsrWorkflowPermissionEndDate="
				+ gsrWorkflowPermissionEndDate + ", gsrSlab=" + gsrSlab
				+ ", datEmployee=" + datEmployee + "]";
	}

}