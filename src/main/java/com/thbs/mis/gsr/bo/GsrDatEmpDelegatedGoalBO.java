package com.thbs.mis.gsr.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;


/**
 * The persistent class for the gsr_dat_emp_delegated_goal database table.
 * 
 */
@Entity
@Table(name="gsr_dat_emp_delegated_goal")
@NamedQuery(name="GsrDatEmpDelegatedGoalBO.findAll", query="SELECT g FROM GsrDatEmpDelegatedGoalBO g")
public class GsrDatEmpDelegatedGoalBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_gsr_delegated_goal_id")
	private Integer pkGsrDelegatedGoalId;

	@Column(name="delegation_status")
	private Byte delegationStatus;

	@Column(name="fk_gsr_dat_emp_goal_id")
	private Integer fkGsrDatEmpGoalId;

	@Column(name="fk_gsr_delegated_emp_id")
	private Integer fkGsrDelegatedEmpId;

	@Column(name="fk_mgr_id_delegated_from")
	private Integer fkMgrIdDelegatedFrom;

	@Column(name="fk_mgr_id_delegated_to")
	private Integer fkMgrIdDelegatedTo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="delegated_date")
	private Date delegatedDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="accepted_or_rejected_date")
	private Date acceptedOrRejectedDate;
	
	@Column(name="delegated_by")
	private Integer delegatedBy;
	
	public Integer getDelegatedBy() {
		return delegatedBy;
	}

	public void setDelegatedBy(Integer delegatedBy) {
		this.delegatedBy = delegatedBy;
	}

	@OneToOne
	@JoinColumn(name="fk_gsr_dat_emp_goal_id",unique = true, nullable = true, insertable = false, updatable = false)
	private GsrDatEmpGoalBO gsrDatEmpGoalBO;
	
	@OneToOne
	@JoinColumn(name="fk_gsr_delegated_emp_id",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO_delegated_emp_id;
	
	@OneToOne
	@JoinColumn(name="fk_mgr_id_delegated_from",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO_delegated_mgr_id_from;
	
	@OneToOne
	@JoinColumn(name="fk_mgr_id_delegated_to",unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO_delegated_mgr_id_to;
	
	public GsrDatEmpDelegatedGoalBO() {
	}

	public Integer getPkGsrDelegatedGoalId() {
		return this.pkGsrDelegatedGoalId;
	}


	public void setPkGsrDelegatedGoalId(Integer pkGsrDelegatedGoalId) {
		this.pkGsrDelegatedGoalId = pkGsrDelegatedGoalId;
	}

	public Byte getDelegationStatus() {
		return this.delegationStatus;
	}

	public void setDelegationStatus(Byte delegationStatus) {
		this.delegationStatus = delegationStatus;
	}

	public Integer getFkGsrDatEmpGoalId() {
		return this.fkGsrDatEmpGoalId;
	}

	public void setFkGsrDatEmpGoalId(Integer fkGsrDatEmpGoalId) {
		this.fkGsrDatEmpGoalId = fkGsrDatEmpGoalId;
	}

	public Integer getFkGsrDelegatedEmpId() {
		return this.fkGsrDelegatedEmpId;
	}

	public void setFkGsrDelegatedEmpId(Integer fkGsrDelegatedEmpId) {
		this.fkGsrDelegatedEmpId = fkGsrDelegatedEmpId;
	}

	public Integer getFkMgrIdDelegatedFrom() {
		return this.fkMgrIdDelegatedFrom;
	}

	public void setFkMgrIdDelegatedFrom(Integer fkMgrIdDelegatedFrom) {
		this.fkMgrIdDelegatedFrom = fkMgrIdDelegatedFrom;
	}

	public Integer getFkMgrIdDelegatedTo() {
		return this.fkMgrIdDelegatedTo;
	}

	public void setFkMgrIdDelegatedTo(Integer fkMgrIdDelegatedTo) {
		this.fkMgrIdDelegatedTo = fkMgrIdDelegatedTo;
	}

	public Date getDelegatedDate() {
		return delegatedDate;
	}

	public void setDelegatedDate(Date delegatedDate) {
		this.delegatedDate = delegatedDate;
	}

	public Date getAcceptedOrRejectedDate() {
		return acceptedOrRejectedDate;
	}

	public void setAcceptedOrRejectedDate(Date acceptedOrRejectedDate) {
		this.acceptedOrRejectedDate = acceptedOrRejectedDate;
	}

	public GsrDatEmpGoalBO getGsrDatEmpGoalBO() {
		return gsrDatEmpGoalBO;
	}

	public void setGsrDatEmpGoalBO(GsrDatEmpGoalBO gsrDatEmpGoalBO) {
		this.gsrDatEmpGoalBO = gsrDatEmpGoalBO;
	}

	public DatEmpDetailBO getDatEmpDetailBO_delegated_emp_id() {
		return datEmpDetailBO_delegated_emp_id;
	}

	public void setDatEmpDetailBO_delegated_emp_id(
			DatEmpDetailBO datEmpDetailBO_delegated_emp_id) {
		this.datEmpDetailBO_delegated_emp_id = datEmpDetailBO_delegated_emp_id;
	}

	public DatEmpDetailBO getDatEmpDetailBO_delegated_mgr_id_from() {
		return datEmpDetailBO_delegated_mgr_id_from;
	}

	public void setDatEmpDetailBO_delegated_mgr_id_from(
			DatEmpDetailBO datEmpDetailBO_delegated_mgr_id_from) {
		this.datEmpDetailBO_delegated_mgr_id_from = datEmpDetailBO_delegated_mgr_id_from;
	}

	public DatEmpDetailBO getDatEmpDetailBO_delegated_mgr_id_to() {
		return datEmpDetailBO_delegated_mgr_id_to;
	}

	public void setDatEmpDetailBO_delegated_mgr_id_to(
			DatEmpDetailBO datEmpDetailBO_delegated_mgr_id_to) {
		this.datEmpDetailBO_delegated_mgr_id_to = datEmpDetailBO_delegated_mgr_id_to;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "GsrDatEmpDelegatedGoalBO [pkGsrDelegatedGoalId="
				+ pkGsrDelegatedGoalId + ", delegationStatus="
				+ delegationStatus + ", fkGsrDatEmpGoalId=" + fkGsrDatEmpGoalId
				+ ", fkGsrDelegatedEmpId=" + fkGsrDelegatedEmpId
				+ ", fkMgrIdDelegatedFrom=" + fkMgrIdDelegatedFrom
				+ ", fkMgrIdDelegatedTo=" + fkMgrIdDelegatedTo
				+ ", delegatedDate=" + delegatedDate
				+ ", acceptedOrRejectedDate=" + acceptedOrRejectedDate
				+ ", delegatedBy=" + delegatedBy + ", gsrDatEmpGoalBO="
				+ gsrDatEmpGoalBO + ", datEmpDetailBO_delegated_emp_id="
				+ datEmpDetailBO_delegated_emp_id
				+ ", datEmpDetailBO_delegated_mgr_id_from="
				+ datEmpDetailBO_delegated_mgr_id_from
				+ ", datEmpDetailBO_delegated_mgr_id_to="
				+ datEmpDetailBO_delegated_mgr_id_to + "]";
	}

	
}