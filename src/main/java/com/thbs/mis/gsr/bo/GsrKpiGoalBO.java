package com.thbs.mis.gsr.bo;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the gsr_kpi_goal database table.
 * 
 */
@Entity
@Table(name = "gsr_kpi_goal")
@NamedQuery(name = "GsrKpiGoalBO.findAll", query = "SELECT g FROM GsrKpiGoalBO g")
public class GsrKpiGoalBO implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_gsr_kpi_goal_id")
	private short pkGsrKpiGoalId;

	@Column(name = "gsr_kpi_goal_description")
	private String gsrKpiGoalDescription;

	@Column(name = "gsr_kpi_goal_measurement")
	private String gsrKpiGoalMeasurement;

	@Column(name = "gsr_kpi_goal_name")
	private String gsrKpiGoalName;

	@Column(name = "gsr_kpi_goal_active_flag")
	private String gsrkpigoalactiveflag;
	
	/*@OneToOne
	@JoinColumn(name = "pk_gsr_kpi_goal_id", unique = true, nullable = true, insertable = false, updatable = false)
	private GsrKpiWeightageBO gsrKpiWeightageBO;*/
	
	
	
	public GsrKpiGoalBO()
	{
	}

	public short getPkGsrKpiGoalId()
	{
		return this.pkGsrKpiGoalId;
	}

	public void setPkGsrKpiGoalId(short pkGsrKpiGoalId)
	{
		this.pkGsrKpiGoalId = pkGsrKpiGoalId;
	}

	public String getGsrKpiGoalDescription()
	{
		return this.gsrKpiGoalDescription;
	}

	public void setGsrKpiGoalDescription(String gsrKpiGoalDescription)
	{
		this.gsrKpiGoalDescription = gsrKpiGoalDescription;
	}

	public String getGsrKpiGoalMeasurement()
	{
		return this.gsrKpiGoalMeasurement;
	}

	public void setGsrKpiGoalMeasurement(String gsrKpiGoalMeasurement)
	{
		this.gsrKpiGoalMeasurement = gsrKpiGoalMeasurement;
	}

	public String getGsrKpiGoalName()
	{
		return this.gsrKpiGoalName;
	}

	public void setGsrKpiGoalName(String gsrKpiGoalName)
	{
		this.gsrKpiGoalName = gsrKpiGoalName;
	}

	public String getGsrkpigoalactiveflag()
	{
		return gsrkpigoalactiveflag;
	}

	public void setGsrkpigoalactiveflag(String gsrkpigoalactiveflag)
	{
		this.gsrkpigoalactiveflag = gsrkpigoalactiveflag;
	}
	
	
	

	/*public GsrKpiWeightageBO getGsrKpiWeightageBO() {
		return gsrKpiWeightageBO;
	}

	public void setGsrKpiWeightageBO(GsrKpiWeightageBO gsrKpiWeightageBO) {
		this.gsrKpiWeightageBO = gsrKpiWeightageBO;
	}*/

	@Override
	public String toString() {
		return "GsrKpiGoalBO [pkGsrKpiGoalId=" + pkGsrKpiGoalId
				+ ", gsrKpiGoalDescription=" + gsrKpiGoalDescription
				+ ", gsrKpiGoalMeasurement=" + gsrKpiGoalMeasurement
				+ ", gsrKpiGoalName=" + gsrKpiGoalName
				+ ", gsrkpigoalactiveflag=" + gsrkpigoalactiveflag
				+"]";
	}

	
}