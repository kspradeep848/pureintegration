package com.thbs.mis.gsr.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.framework.util.CustomTimeSerializer;
import com.thbs.mis.gsr.bean.DelegationResultFromDbBean;

/**
 * The persistent class for the gsr_dat_emp_goal database table.
 * 
 */
@Entity
@Table(name = "gsr_dat_emp_goal")
@NamedQuery(name = "GsrDatEmpGoalBO.findAll", query = "SELECT g FROM GsrDatEmpGoalBO g")
//Added by Kamal Anand
@SqlResultSetMapping(name = "groupDetailsMapping", classes = { @ConstructorResult(targetClass = DelegationResultFromDbBean.class, columns = {
		@ColumnResult(name = "fk_gsr_dat_emp_id"),
		@ColumnResult(name = " empName"),
		@ColumnResult(name = "fk_gsr_slab_id"),
		@ColumnResult(name = "gsr_slab_name"),
		@ColumnResult(name = "gsr_slab_short_name"),
		@ColumnResult(name = "rating_accept_or_reject_date"),
		@ColumnResult(name = "final_rating_status"),
		@ColumnResult(name = "pk_gsr_dat_emp_goal_id"),
		@ColumnResult(name = "gsr_dat_emp_goal_name"),
		@ColumnResult(name = "gsr_goal_start_date"),
		@ColumnResult(name = "gsr_goal_end_date"),
		@ColumnResult(name = "gsr_goal_weightage"),
		@ColumnResult(name = "gsr_goal_description"),
		@ColumnResult(name = "gsr_goal_measurement"),
		@ColumnResult(name = "gsr_goal_accomplishment"),
		@ColumnResult(name = "gsr_goal_mgr_feedback"),
		@ColumnResult(name = "gsr_goal_self_rating"),
		@ColumnResult(name = "gsr_goal_mgr_rating"),
		@ColumnResult(name = "fk_gsr_goal_status"),
		@ColumnResult(name = "gsr_goal_status_name"),
		@ColumnResult(name = "fk_emp_goal_kpi_weightage_id"),
		@ColumnResult(name = "fk_gsr_kpi_goal_id"),
		@ColumnResult(name = "gsr_kpi_goal_weightage"),
		@ColumnResult(name = "fk_gsr_delegated_goal_id"),
		@ColumnResult(name = "fk_mgr_id_delegated_from"),
		@ColumnResult(name = "delegatedFromName"),
		@ColumnResult(name = "fk_mgr_id_delegated_to"),
		@ColumnResult(name = "delegatedToName"),
		@ColumnResult(name = "delegation_status"),
		@ColumnResult(name = "delegated_date"),
		@ColumnResult(name = "accepted_or_rejected_date"),
		@ColumnResult(name = "delegated_by"),
		@ColumnResult(name = "delegatedByName"),
		@ColumnResult(name = "finalRatingStatusName"),
		@ColumnResult(name = "gsr_final_rating"),
		@ColumnResult(name = "delegationStatusName")}) })

@NamedNativeQueries({
@NamedNativeQuery(name = "GsrDatEmpGoalBO.getEmpGoalsForDelegation", query = "select a.fk_gsr_dat_emp_id,a.empName,a.fk_gsr_slab_id,a.gsr_slab_name,"
		+ "a.gsr_slab_short_name,a.rating_accept_or_reject_date,a.final_rating_status,a.pk_gsr_dat_emp_goal_id,a.gsr_dat_emp_goal_name,a.gsr_goal_start_date,"
		+ "a.gsr_goal_end_date,a.gsr_goal_weightage,a.gsr_goal_description,a.gsr_goal_measurement,a.gsr_goal_accomplishment,a.gsr_goal_mgr_feedback,"
		+ "a.gsr_goal_self_rating,a.gsr_goal_mgr_rating,a.fk_gsr_goal_status,a.gsr_goal_status_name,a.fk_emp_goal_kpi_weightage_id,a.fk_gsr_kpi_goal_id,"
		+ "a.gsr_kpi_goal_weightage,a.fk_gsr_delegated_goal_id,a.fk_mgr_id_delegated_from,a.delegatedFromName,a.fk_mgr_id_delegated_to,"
		+ "a.delegatedToName,a.delegation_status,a.delegated_date,a.accepted_or_rejected_date,a.delegated_by,a.delegatedByName,"
		+ "if(a.final_rating_status=1, 'Not Yet Rated',if(a.final_rating_status=2, 'Accepted',if(a.final_rating_status=3, 'Rejected',"
		+ "if(a.final_rating_status=4, 'Force Closed',if(a.final_rating_status=5, 'Rated', null))))) finalRatingStatusName,a.gsr_final_rating,"
		+ "if(a.delegation_status=1,'Waiting For Acceptance',if(a.delegation_status=2,'Accepted',if(a.delegation_status=3,'Rejected',null))) delegationStatusName from "
		+ "(select g.fk_gsr_dat_emp_id,concat(per.emp_first_name, ' ',per.emp_last_name) empName,g.fk_gsr_slab_id,slab.gsr_slab_name,"
		+ "slab.gsr_slab_short_name,final.rating_accept_or_reject_date,final.final_rating_status,g.pk_gsr_dat_emp_goal_id,g.gsr_dat_emp_goal_name,"
		+ "g.gsr_goal_start_date,g.gsr_goal_end_date,g.gsr_goal_weightage,g.gsr_goal_description,g.gsr_goal_measurement,g.gsr_goal_accomplishment,"
		+ "g.gsr_goal_mgr_feedback,g.gsr_goal_self_rating,g.gsr_goal_mgr_rating,g.fk_gsr_goal_status,stat.gsr_goal_status_name,g.fk_emp_goal_kpi_weightage_id,"
		+ "w.fk_gsr_kpi_goal_id,w.gsr_kpi_goal_weightage,g.fk_gsr_delegated_goal_id,d.fk_mgr_id_delegated_from,"
		+ "concat(p.emp_first_name, ' ',p.emp_last_name) delegatedFromName,d.fk_mgr_id_delegated_to,"
		+ "concat(personal.emp_first_name, ' ',personal.emp_last_name) delegatedToName,d.delegation_status,d.delegated_date,d.accepted_or_rejected_date,"
		+ "concat(pers.emp_first_name, ' ',pers.emp_last_name) delegatedByName,final.gsr_final_rating, "
		+ "d.delegated_by from gsr_dat_emp_goal g "
		+ "left join gsr_dat_emp_delegated_goal d on d.pk_gsr_delegated_goal_id = g.fk_gsr_delegated_goal_id "
		+ "join dat_emp_personal_detail per on per.fk_emp_detail_id = g.fk_gsr_dat_emp_id "
		+ "left join dat_emp_personal_detail p on per.fk_emp_detail_id = d.fk_mgr_id_delegated_from "
		+ "left join dat_emp_personal_detail personal on per.fk_emp_detail_id = d.fk_mgr_id_delegated_to "
		+ "left join dat_emp_personal_detail pers on pers.fk_emp_detail_id = d.delegated_by "
		+ "join gsr_slab slab on slab.pk_gsr_slab_id = g.fk_gsr_slab_id "
		+ "left join gsr_dat_emp_final_rating final on final.fk_gsr_final_rating_emp_id = g.fk_gsr_dat_emp_id "
		+ "and final.fk_gsr_final_rating_slab_id = g.fk_gsr_slab_id "
		+ "join gsr_mas_goal_status stat on stat.pk_gsr_goal_status_id = g.fk_gsr_goal_status "
		+ "left join gsr_kpi_weightage as w on w.pk_gsr_kpi_weightage_id=g.fk_emp_goal_kpi_weightage_id "
		+ "where g.fk_gsr_dat_emp_id in(:empIds) and g.fk_gsr_delegated_goal_id is null and g.fk_gsr_goal_status in(3,8) and g.fk_gsr_dat_mgr_id = :delMgrId "
		+ "UNION "
		+ "select goal.fk_gsr_dat_emp_id,concat(per.emp_first_name, ' ',per.emp_last_name) empName,goal.fk_gsr_slab_id,slab.gsr_slab_name,"
		+ "slab.gsr_slab_short_name,final.rating_accept_or_reject_date,final.final_rating_status,goal.pk_gsr_dat_emp_goal_id,goal.gsr_dat_emp_goal_name,"
		+ "goal.gsr_goal_start_date,goal.gsr_goal_end_date,goal.gsr_goal_weightage,goal.gsr_goal_description,goal.gsr_goal_measurement,"
		+ "goal.gsr_goal_accomplishment,goal.gsr_goal_mgr_feedback,goal.gsr_goal_self_rating,goal.gsr_goal_mgr_rating,goal.fk_gsr_goal_status,"
		+ "stat.gsr_goal_status_name,goal.fk_emp_goal_kpi_weightage_id,w.fk_gsr_kpi_goal_id,w.gsr_kpi_goal_weightage,goal.fk_gsr_delegated_goal_id,"
		+ "del.fk_mgr_id_delegated_from,concat(p.emp_first_name, ' ',p.emp_last_name) delegatedFromName,del.fk_mgr_id_delegated_to,"
		+ "concat(personal.emp_first_name, ' ',personal.emp_last_name) delegatedToName,del.delegation_status,del.delegated_date,del.accepted_or_rejected_date,"
		+ "concat(pers.emp_first_name, ' ',pers.emp_last_name) delegatedByName,final.gsr_final_rating, "
		+ "del.delegated_by from gsr_dat_emp_goal goal "
		+ "join gsr_dat_emp_delegated_goal del on del.pk_gsr_delegated_goal_id = goal.fk_gsr_delegated_goal_id "
		+ "left join dat_emp_personal_detail per on per.fk_emp_detail_id = goal.fk_gsr_dat_emp_id "
		+ "left join dat_emp_personal_detail p on p.fk_emp_detail_id = del.fk_mgr_id_delegated_from "
		+ "left join dat_emp_personal_detail personal on personal.fk_emp_detail_id = del.fk_mgr_id_delegated_to "
		+ "left join dat_emp_personal_detail pers on pers.fk_emp_detail_id = del.delegated_by "
		+ "left join gsr_slab slab on slab.pk_gsr_slab_id = goal.fk_gsr_slab_id "
		+ "left join gsr_dat_emp_final_rating final on final.fk_gsr_final_rating_emp_id = goal.fk_gsr_dat_emp_id "
		+ "and final.fk_gsr_final_rating_slab_id = goal.fk_gsr_slab_id "
		+ "left join gsr_mas_goal_status stat on stat.pk_gsr_goal_status_id = goal.fk_gsr_goal_status "
		+ "left join gsr_kpi_weightage as w on w.pk_gsr_kpi_weightage_id=goal.fk_emp_goal_kpi_weightage_id "
		+ "where del.fk_mgr_id_delegated_from = :delMgrId or del.fk_mgr_id_delegated_to = :delMgrId) as a "
		+ "order by a.fk_gsr_dat_emp_id asc,a.fk_gsr_slab_id desc,a.pk_gsr_dat_emp_goal_id desc", resultSetMapping = "groupDetailsMapping"),
//End of Addition by Kamal Anand
//Added by Rajesh Kumar
@NamedNativeQuery(name = "GsrDatEmpGoalBO.getEmpGoalsForDelegationHR", query = "select a.fk_gsr_dat_emp_id,a.empName,a.fk_gsr_slab_id,a.gsr_slab_name,"
		+ "a.gsr_slab_short_name,a.rating_accept_or_reject_date,a.final_rating_status,a.pk_gsr_dat_emp_goal_id,a.gsr_dat_emp_goal_name,a.gsr_goal_start_date,"
		+ "a.gsr_goal_end_date,a.gsr_goal_weightage,a.gsr_goal_description,a.gsr_goal_measurement,a.gsr_goal_accomplishment,a.gsr_goal_mgr_feedback,"
		+ "a.gsr_goal_self_rating,a.gsr_goal_mgr_rating,a.fk_gsr_goal_status,a.gsr_goal_status_name,a.fk_emp_goal_kpi_weightage_id,a.fk_gsr_kpi_goal_id,"
		+ "a.gsr_kpi_goal_weightage,a.fk_gsr_delegated_goal_id,a.fk_mgr_id_delegated_from,a.delegatedFromName,a.fk_mgr_id_delegated_to,"
		+ "a.delegatedToName,a.delegation_status,a.delegated_date,a.accepted_or_rejected_date,a.delegated_by,a.delegatedByName,"
		+ "if(a.final_rating_status=1, 'Not Yet Rated',if(a.final_rating_status=2, 'Accepted',if(a.final_rating_status=3, 'Rejected',"
		+ "if(a.final_rating_status=4, 'Force Closed',if(a.final_rating_status=5, 'Rated', null))))) finalRatingStatusName,a.gsr_final_rating,"
		+ "if(a.delegation_status=1,'Waiting For Acceptance',if(a.delegation_status=2,'Accepted',if(a.delegation_status=3,'Rejected',null))) delegationStatusName from "
		+ "(select g.fk_gsr_dat_emp_id,concat(per.emp_first_name, ' ',per.emp_last_name) empName,g.fk_gsr_slab_id,slab.gsr_slab_name,"
		+ "slab.gsr_slab_short_name,final.rating_accept_or_reject_date,final.final_rating_status,g.pk_gsr_dat_emp_goal_id,g.gsr_dat_emp_goal_name,"
		+ "g.gsr_goal_start_date,g.gsr_goal_end_date,g.gsr_goal_weightage,g.gsr_goal_description,g.gsr_goal_measurement,g.gsr_goal_accomplishment,"
		+ "g.gsr_goal_mgr_feedback,g.gsr_goal_self_rating,g.gsr_goal_mgr_rating,g.fk_gsr_goal_status,stat.gsr_goal_status_name,g.fk_emp_goal_kpi_weightage_id,"
		+ "w.fk_gsr_kpi_goal_id,w.gsr_kpi_goal_weightage,g.fk_gsr_delegated_goal_id,d.fk_mgr_id_delegated_from,"
		+ "concat(p.emp_first_name, ' ',p.emp_last_name) delegatedFromName,d.fk_mgr_id_delegated_to,"
		+ "concat(personal.emp_first_name, ' ',personal.emp_last_name) delegatedToName,d.delegation_status,d.delegated_date,d.accepted_or_rejected_date,"
		+ "concat(pers.emp_first_name, ' ',pers.emp_last_name) delegatedByName,final.gsr_final_rating, "
		+ "d.delegated_by from gsr_dat_emp_goal g "
		+ "left join gsr_dat_emp_delegated_goal d on d.pk_gsr_delegated_goal_id = g.fk_gsr_delegated_goal_id "
		+ "join dat_emp_personal_detail per on per.fk_emp_detail_id = g.fk_gsr_dat_emp_id "
		+ "left join dat_emp_personal_detail p on per.fk_emp_detail_id = d.fk_mgr_id_delegated_from "
		+ "left join dat_emp_personal_detail personal on per.fk_emp_detail_id = d.fk_mgr_id_delegated_to "
		+ "left join dat_emp_personal_detail pers on pers.fk_emp_detail_id = d.delegated_by "
		+ "join gsr_slab slab on slab.pk_gsr_slab_id = g.fk_gsr_slab_id "
		+ "left join gsr_dat_emp_final_rating final on final.fk_gsr_final_rating_emp_id = g.fk_gsr_dat_emp_id "
		+ "and final.fk_gsr_final_rating_slab_id = g.fk_gsr_slab_id "
		+ "join gsr_mas_goal_status stat on stat.pk_gsr_goal_status_id = g.fk_gsr_goal_status "
		+ "left join gsr_kpi_weightage as w on w.pk_gsr_kpi_weightage_id=g.fk_emp_goal_kpi_weightage_id "
		+ "where g.fk_gsr_dat_emp_id in(:empIds) and g.fk_gsr_delegated_goal_id is null and g.fk_gsr_goal_status in(3,8) "
		+ "UNION "
		+ "select goal.fk_gsr_dat_emp_id,concat(per.emp_first_name, ' ',per.emp_last_name) empName,goal.fk_gsr_slab_id,slab.gsr_slab_name,"
		+ "slab.gsr_slab_short_name,final.rating_accept_or_reject_date,final.final_rating_status,goal.pk_gsr_dat_emp_goal_id,goal.gsr_dat_emp_goal_name,"
		+ "goal.gsr_goal_start_date,goal.gsr_goal_end_date,goal.gsr_goal_weightage,goal.gsr_goal_description,goal.gsr_goal_measurement,"
		+ "goal.gsr_goal_accomplishment,goal.gsr_goal_mgr_feedback,goal.gsr_goal_self_rating,goal.gsr_goal_mgr_rating,goal.fk_gsr_goal_status,"
		+ "stat.gsr_goal_status_name,goal.fk_emp_goal_kpi_weightage_id,w.fk_gsr_kpi_goal_id,w.gsr_kpi_goal_weightage,goal.fk_gsr_delegated_goal_id,"
		+ "del.fk_mgr_id_delegated_from,concat(p.emp_first_name, ' ',p.emp_last_name) delegatedFromName,del.fk_mgr_id_delegated_to,"
		+ "concat(personal.emp_first_name, ' ',personal.emp_last_name) delegatedToName,del.delegation_status,del.delegated_date,del.accepted_or_rejected_date,"
		+ "concat(pers.emp_first_name, ' ',pers.emp_last_name) delegatedByName,final.gsr_final_rating, "
		+ "del.delegated_by from gsr_dat_emp_goal goal "
		+ "join gsr_dat_emp_delegated_goal del on del.pk_gsr_delegated_goal_id = goal.fk_gsr_delegated_goal_id "
		+ "left join dat_emp_personal_detail per on per.fk_emp_detail_id = goal.fk_gsr_dat_emp_id "
		+ "left join dat_emp_personal_detail p on p.fk_emp_detail_id = del.fk_mgr_id_delegated_from "
		+ "left join dat_emp_personal_detail personal on personal.fk_emp_detail_id = del.fk_mgr_id_delegated_to "
		+ "left join dat_emp_personal_detail pers on pers.fk_emp_detail_id = del.delegated_by "
		+ "left join gsr_slab slab on slab.pk_gsr_slab_id = goal.fk_gsr_slab_id "
		+ "left join gsr_dat_emp_final_rating final on final.fk_gsr_final_rating_emp_id = goal.fk_gsr_dat_emp_id "
		+ "and final.fk_gsr_final_rating_slab_id = goal.fk_gsr_slab_id "
		+ "left join gsr_mas_goal_status stat on stat.pk_gsr_goal_status_id = goal.fk_gsr_goal_status "
		+ "left join gsr_kpi_weightage as w on w.pk_gsr_kpi_weightage_id=goal.fk_emp_goal_kpi_weightage_id "
		+ "where del.fk_gsr_delegated_emp_id in(:empIds)) as a "
		+ "order by a.fk_gsr_dat_emp_id asc,a.fk_gsr_slab_id desc,a.pk_gsr_dat_emp_goal_id desc", resultSetMapping = "groupDetailsMapping")})
//End of Addition Rajesh Kumar
public class GsrDatEmpGoalBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_gsr_dat_emp_goal_id")
	private Integer pkGsrDatEmpGoalId;

	@Column(name = "fk_gsr_dat_emp_id")
	private Integer fkGsrDatEmpId;

	@Column(name = "fk_gsr_dat_mgr_id")
	private Integer fkGsrDatMgrId;

	@Column(name = "fk_gsr_goal_status")
	private Byte fkGsrGoalStatus;

	@Column(name = "fk_gsr_slab_id")
	private Short fkGsrSlabId;

	@Column(name = "gsr_dat_emp_goal_name")
	private String gsrDatEmpGoalName;

	@Column(name = "gsr_goal_accomplishment")
	private String gsrGoalAccomplishment;

	@Column(name = "gsr_goal_closed_by")
	private Integer gsrGoalClosedBy;

	@Column(name = "gsr_goal_created_by")
	private Integer gsrGoalCreatedBy;

	@JsonSerialize(using = CustomTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "gsr_goal_created_date")
	private Date gsrGoalCreatedDate;

	@Column(name = "gsr_goal_description")
	private String gsrGoalDescription;

	@JsonSerialize(using = CustomTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "gsr_goal_end_date")
	private Date gsrGoalEndDate;

	@Column(name = "gsr_goal_measurement")
	private String gsrGoalMeasurement;

	@Column(name = "gsr_goal_mgr_feedback")
	private String gsrGoalMgrFeedback;

	@Column(name = "gsr_goal_mgr_rating")
	private Float gsrGoalMgrRating;

	@Column(name = "gsr_goal_modified_by")
	private Integer gsrGoalModifiedBy;

	@JsonSerialize(using = CustomTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "gsr_goal_modified_date")
	private Date gsrGoalModifiedDate;

	@Column(name = "gsr_goal_self_rating")
	private Float gsrGoalSelfRating;

	@JsonSerialize(using = CustomTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "gsr_goal_start_date")
	private Date gsrGoalStartDate;

	@Column(name = "gsr_goal_weightage")
	private Byte gsrGoalWeightage;

	@Column(name = "fk_emp_goal_kpi_weightage_id")
	private Short empGoalKpiWeightageId;

	@Column(name = "fk_emp_goal_domain_mgr_id")
	private Integer fkEmpGoalDomainMgrId;

	@Column(name = "fk_emp_goal_bu_id")
	private short fkEmpGoalBuId;
	
	@Column(name = "fk_gsr_delegated_goal_id")
	private Integer fkGsrDelegatedGoalId;

	@OneToOne
	@JoinColumn(name = "fk_gsr_dat_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOdatemp_id;

	@OneToOne
	@JoinColumn(name = "fk_gsr_dat_mgr_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBOdatmgrid;

	@OneToOne
	@JoinColumn(name = "fk_gsr_goal_status", unique = true, nullable = true, insertable = false, updatable = false)
	private GsrMasGoalStatusBO gsrMasGoalStatusBOgoalstatus;

	@OneToOne
	@JoinColumn(name = "fk_gsr_slab_id", unique = true, nullable = true, insertable = false, updatable = false)
	private GsrSlabBO gsrSlabBO_slab_id;

	@OneToOne
	@JoinColumn(name = "gsr_goal_closed_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO_goal_closed_by;

	@OneToOne
	@JoinColumn(name = "gsr_goal_created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO_goal_created_by;

	@OneToOne
	@JoinColumn(name = "gsr_goal_modified_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailBO_goal_modified_by;

	
	@OneToOne
	@JoinColumn(name = "fk_emp_goal_kpi_weightage_id", unique = true, nullable = true, insertable = false, updatable = false)
	private GsrKpiWeightageBO gsrKpiWeightageDetails;
	
	
	@Transient
	private Float gsrFinalRating;

	@Transient
	private Byte finalRatingStatus;

	@Transient
	private String isOverriddenFlag;

	@Transient
	private Date ratingAcceptOrRejectDate;

	@Transient
	private String gsrGoalStatusName;

	@Transient
	private String gsrSlabYear;

	@Transient
	private String empFirstName;

	@Transient
	private String empLastName;

	@Transient
	private String finalRatingStatusName;

	@Transient
	private String slabName;

	@Transient
	private Long totalWeightage;

	@Transient
	private String slabShortName;

	// Added by prathibha for GSR Services on 6/4/2017
	@Transient
	private String gsrSlabName;

	// Added by prathibha for view reportee goals by BU Head

	@Transient
	private Integer fkBuHeadEmpId;

	@Transient
	private short gsrSlabId;

	@Transient
	private String overriddenByName;

	@Transient
	private Integer overriddenBy;

	@Transient
	private Date modifiedDate;

	@Transient
	private String mgrName;
	
	public String getOverriddenByName() {
		return overriddenByName;
	}

	public Integer getFkGsrDelegatedGoalId() {
		return fkGsrDelegatedGoalId;
	}

	public void setFkGsrDelegatedGoalId(Integer fkGsrDelegatedGoalId) {
		this.fkGsrDelegatedGoalId = fkGsrDelegatedGoalId;
	}

	public void setOverriddenByName(String overriddenByName) {
		this.overriddenByName = overriddenByName;
	}

	public Integer getOverriddenBy() {
		return overriddenBy;
	}

	public void setOverriddenBy(Integer overriddenBy) {
		this.overriddenBy = overriddenBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getMgrName() {
		return mgrName;
	}

	public void setMgrName(String mgrName) {
		this.mgrName = mgrName;
	}

	public short getGsrSlabId() {
		return gsrSlabId;
	}

	public void setGsrSlabId(short gsrSlabId) {
		this.gsrSlabId = gsrSlabId;
	}

	public String getGsrSlabName() {
		return gsrSlabName;
	}

	public void setGsrSlabName(String gsrSlabName) {
		this.gsrSlabName = gsrSlabName;
	}

	public Integer getFkBuHeadEmpId() {
		return fkBuHeadEmpId;
	}

	public void setFkBuHeadEmpId(Integer fkBuHeadEmpId) {
		this.fkBuHeadEmpId = fkBuHeadEmpId;
	}

	public GsrDatEmpGoalBO() {
	}

	public GsrDatEmpGoalBO(Integer pkGsrDatEmpGoalId, String gsrDatEmpGoalName,
			Integer fkGsrDatEmpId, Date gsrGoalStartDate, Date gsrGoalEndDate,
			Short fkGsrSlabId, Byte gsrGoalWeightage,
			String gsrGoalDescription, String gsrGoalMeasurement,
			String gsrGoalAccomplishment, Float gsrGoalSelfRating,
			Byte fkGsrGoalStatus, short gsrSlabId, String slabName,
			String gsrSlabYear, String empFirstName, String empLastName,
			String gsrGoalStatusName) {
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
		this.gsrDatEmpGoalName = gsrDatEmpGoalName;
		this.fkGsrDatEmpId = fkGsrDatEmpId;
		// this.fkEmpGoalBuId=fkEmpGoalBuId;
		this.gsrGoalStartDate = gsrGoalStartDate;
		this.gsrGoalEndDate = gsrGoalEndDate;
		this.fkGsrSlabId = fkGsrSlabId;
		this.gsrGoalWeightage = gsrGoalWeightage;
		this.gsrGoalDescription = gsrGoalDescription;
		this.gsrGoalMeasurement = gsrGoalMeasurement;
		this.gsrGoalAccomplishment = gsrGoalAccomplishment;
		this.gsrGoalSelfRating = gsrGoalSelfRating;
		this.fkGsrGoalStatus = fkGsrGoalStatus;
		this.gsrSlabId = gsrSlabId;
		this.slabName = slabName;
		this.gsrSlabYear = gsrSlabYear;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.gsrGoalStatusName = gsrGoalStatusName;

	}

	public GsrDatEmpGoalBO(Integer pkGsrDatEmpGoalId, Integer fkGsrDatEmpId,
			Integer fkGsrDatMgrId, String mgrName, Date gsrGoalStartDate,
			Date gsrGoalEndDate, Short fkGsrSlabId, String gsrSlabName,
			String slabShortName, String gsrSlabYear, Byte gsrGoalWeightage,
			String gsrGoalDescription, String gsrGoalMeasurement,
			String gsrGoalAccomplishment, String gsrGoalMgrFeedback,
			Float gsrGoalSelfRating, Float gsrGoalMgrRating,
			Byte fkGsrGoalStatus, String gsrGoalStatusName,
			Float gsrFinalRating, String isOverriddenFlag,
			Integer overriddenBy, String overriddenByName, Date modifiedDate) {

		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
		this.fkGsrDatEmpId = fkGsrDatEmpId;
		this.mgrName = mgrName;
		this.gsrGoalStartDate = gsrGoalStartDate;
		this.gsrGoalEndDate = gsrGoalEndDate;
		this.fkGsrSlabId = fkGsrSlabId;
		this.gsrSlabName = gsrSlabName;
		this.slabShortName = slabShortName;
		this.gsrSlabYear = gsrSlabYear;
		this.gsrGoalWeightage = gsrGoalWeightage;
		this.gsrGoalDescription = gsrGoalDescription;
		this.gsrGoalMeasurement = gsrGoalMeasurement;
		this.gsrGoalAccomplishment = gsrGoalAccomplishment;
		this.gsrGoalMgrFeedback = gsrGoalMgrFeedback;
		this.gsrGoalSelfRating = gsrGoalSelfRating;
		this.gsrGoalMgrRating = gsrGoalMgrRating;
		this.fkGsrGoalStatus = fkGsrGoalStatus;
		this.gsrGoalStatusName = gsrGoalStatusName;
		this.gsrFinalRating = gsrFinalRating;
		this.isOverriddenFlag = isOverriddenFlag;
		this.overriddenBy = overriddenBy;
		this.overriddenByName = overriddenByName;
		this.modifiedDate = modifiedDate;
	}

	// Added by prathibha fro VIEW REPORTEE GOALS BY BU HEAD
	
			@Transient
			private Integer buHeadEmpid;
			
			

			public Integer getBuHeadEmpid() {
				return buHeadEmpid;
			}

			public void setBuHeadEmpid(Integer buHeadEmpid) {
				this.buHeadEmpid = buHeadEmpid;
			}
	         
			//test for buhead
			
			 public GsrDatEmpGoalBO (Integer pkGsrDatEmpGoalId, String gsrDatEmpGoalName,
						Integer fkGsrDatEmpId, Date gsrGoalStartDate, Date gsrGoalEndDate,
						Short fkGsrSlabId, Byte gsrGoalWeightage,
						String gsrGoalDescription, String gsrGoalMeasurement,
						String gsrGoalAccomplishment, Float gsrGoalSelfRating,
						Byte fkGsrGoalStatus,short fkEmpGoalBuId,
						short gsrSlabId, String gsrSlabName,
						String gsrSlabYear, String empFirstName, String empLastName,
						String gsrGoalStatusName,Integer buHeadEmpid)
			{
				this.pkGsrDatEmpGoalId=pkGsrDatEmpGoalId;
				this.gsrDatEmpGoalName=gsrDatEmpGoalName;
				this.fkGsrDatEmpId=fkGsrDatEmpId;
				this.gsrGoalStartDate=gsrGoalStartDate;
				this.gsrGoalEndDate=gsrGoalEndDate;
				this.fkGsrSlabId=fkGsrSlabId;
				this.gsrGoalWeightage=gsrGoalWeightage;
				this.gsrGoalDescription=gsrGoalDescription;
				this.gsrGoalMeasurement=gsrGoalMeasurement;
				this.gsrGoalAccomplishment=gsrGoalAccomplishment;
				this.gsrGoalSelfRating=gsrGoalSelfRating;
				this.fkGsrGoalStatus=fkGsrGoalStatus;
				this.fkEmpGoalBuId=fkEmpGoalBuId;
				this.gsrSlabId=gsrSlabId;
				this.gsrSlabName=gsrSlabName;
				this.gsrSlabYear=gsrSlabYear;
				this.empFirstName=empFirstName;
				this.empLastName=empLastName;
				this.gsrGoalStatusName=gsrGoalStatusName;
				this.buHeadEmpid=buHeadEmpid;
			
			} //Test for buhead
	
	
	
	
	public GsrDatEmpGoalBO(Integer pkGsrDatEmpGoalId, Integer fkGsrDatEmpId,
			Integer fkGsrDatMgrId, Byte fkGsrGoalStatus, Short fkGsrSlabId,
			String gsrDatEmpGoalName, String gsrGoalAccomplishment,
			String gsrGoalDescription, String gsrGoalMeasurement,
			String gsrGoalMgrFeedback, Float gsrGoalMgrRating,
			Float gsrGoalSelfRating, Byte gsrGoalWeightage,
			Float gsrFinalRating, Byte finalRatingStatus,
			String isOverriddenFlag, Date ratingAcceptOrRejectDate,
			String gsrSlabYear, String gsrGoalStatusName, String empFirstName,
			String empLastName) {
		super();
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
		this.fkGsrDatEmpId = fkGsrDatEmpId;
		this.fkGsrDatMgrId = fkGsrDatMgrId;
		this.fkGsrGoalStatus = fkGsrGoalStatus;
		this.fkGsrSlabId = fkGsrSlabId;
		this.gsrDatEmpGoalName = gsrDatEmpGoalName;
		this.gsrGoalAccomplishment = gsrGoalAccomplishment;
		this.gsrGoalDescription = gsrGoalDescription;
		this.gsrGoalMeasurement = gsrGoalMeasurement;
		this.gsrGoalMgrFeedback = gsrGoalMgrFeedback;
		this.gsrGoalMgrRating = gsrGoalMgrRating;
		this.gsrGoalSelfRating = gsrGoalSelfRating;
		this.gsrGoalWeightage = gsrGoalWeightage;
		this.gsrFinalRating = gsrFinalRating;
		this.fkGsrSlabId = fkGsrSlabId;
		this.finalRatingStatus = finalRatingStatus;
		this.isOverriddenFlag = isOverriddenFlag;
		this.ratingAcceptOrRejectDate = ratingAcceptOrRejectDate;
		this.gsrFinalRating = gsrFinalRating;
		this.gsrSlabYear = gsrSlabYear;
		this.gsrGoalStatusName = gsrGoalStatusName;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
	}

	public GsrDatEmpGoalBO(Short fkGsrSlabId, Float gsrFinalRating,
			String slabName, String gsrSlabYear, Date gsrGoalStartDate,
			Date gsrGoalEndDate, String slabShortName) {
		super();
		this.fkGsrSlabId = fkGsrSlabId;
		this.gsrFinalRating = gsrFinalRating;
		this.slabName = slabName;
		this.gsrSlabYear = gsrSlabYear;
		this.gsrGoalStartDate = gsrGoalStartDate;
		this.gsrGoalEndDate = gsrGoalEndDate;
		this.slabShortName = slabShortName;
	}

	// Added by prathibha fro VIEW REPORTEE GOALS BY BU HEAD

	public GsrDatEmpGoalBO(String gsrDatEmpGoalName, Integer pkGsrDatEmpGoalId,
			Date gsrGoalStartDate, Date gsrGoalEndDate,
			String gsrGoalDescription, byte gsrGoalWeightage,
			String gsrSlabName, String gsrGoalStatusName, String empFirstName,
			String empLastName, Integer fkBuHeadEmpId) {
		this.gsrDatEmpGoalName = gsrDatEmpGoalName;
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
		this.gsrGoalStartDate = gsrGoalStartDate;
		this.gsrGoalEndDate = gsrGoalEndDate;
		this.gsrGoalDescription = gsrGoalDescription;
		this.gsrGoalWeightage = gsrGoalWeightage;
		this.gsrSlabName = gsrSlabName;
		this.gsrGoalStatusName = gsrGoalStatusName;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.fkBuHeadEmpId = fkBuHeadEmpId;
	}

	public GsrDatEmpGoalBO(String slabName, int fkGsrDatEmpId,
			int fkGsrDatMgrId, byte fkGsrGoalStatus, short fkGsrSlabId,
			String gsrDatEmpGoalName, Long totalWeightage,Integer fkEmpGoalDomainMgrId,short fkEmpGoalBuId) {
		super();
		this.slabName = slabName;
		this.fkGsrDatEmpId = fkGsrDatEmpId;
		this.fkGsrDatMgrId = fkGsrDatMgrId;
		this.fkGsrGoalStatus = fkGsrGoalStatus;
		this.fkGsrSlabId = fkGsrSlabId;
		this.gsrDatEmpGoalName = gsrDatEmpGoalName;
		this.totalWeightage = totalWeightage;
		this.fkEmpGoalDomainMgrId = fkEmpGoalDomainMgrId;
		this.fkEmpGoalBuId = fkEmpGoalBuId;
	}
	
	// added by Smrithi

	@Transient
	private Short fkGsrKpiGoalId;

	public GsrDatEmpGoalBO(String gsrSlabName, Integer fkGsrDatEmpId,
			String slabShortName, Integer fkGsrDatMgrId, Byte fkGsrGoalStatus,
			Integer pkGsrDatEmpGoalId, String gsrGoalAccomplishment,
			String gsrGoalDescription, String gsrGoalMeasurement,
			Short fkGsrSlabId, String gsrDatEmpGoalName, Byte gsrGoalWeightage,
			Integer fkEmpGoalDomainMgrId, Short fkEmpGoalBuId,Short empGoalKpiWeightageId,
			GsrKpiWeightageBO gsrKpiWeightageDetails) {
		super();
		this.slabName = gsrSlabName;
		this.fkGsrDatEmpId = fkGsrDatEmpId;
		this.slabShortName = slabShortName;
		this.fkGsrDatMgrId = fkGsrDatMgrId;
		this.fkGsrGoalStatus = fkGsrGoalStatus;
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
		this.gsrGoalAccomplishment = gsrGoalAccomplishment;
		this.gsrGoalDescription = gsrGoalDescription;
		this.gsrGoalMeasurement = gsrGoalMeasurement;
		this.fkGsrSlabId = fkGsrSlabId;
		this.gsrDatEmpGoalName = gsrDatEmpGoalName;
		this.gsrGoalWeightage = gsrGoalWeightage;
		this.fkEmpGoalDomainMgrId = fkEmpGoalDomainMgrId;
		this.fkEmpGoalBuId = fkEmpGoalBuId;
		this.empGoalKpiWeightageId = empGoalKpiWeightageId;
		this.gsrKpiWeightageDetails = gsrKpiWeightageDetails;
	}

	/*public GsrDatEmpGoalBO(Integer empId,Short slabId,Integer modifiedBy){
		super();
		this.fkGsrDatEmpId= fkGsrDatEmpId;
		this.fkGsrSlabId= fkGsrSlabId;
		this.gsrGoalModifiedBy=gsrGoalModifiedBy;
	}*/
	
	// EOA by Smrithi

	//Added by Rajesh Kumar
		@Transient
		private Integer pkGsrDelegatedGoalId;
		
		@Transient
		private Integer fkMgrIdDelegatedTo;
		
		@Transient
		private Byte delegationStatus;
		
		public GsrDatEmpGoalBO(Integer pkGsrDatEmpGoalId, Integer fkGsrDatEmpId, Integer fkGsrDatMgrId, Byte fkGsrGoalStatus,
				Short fkGsrSlabId, String gsrDatEmpGoalName, String gsrGoalAccomplishment, Integer gsrGoalClosedBy, Integer gsrGoalCreatedBy,
				Date gsrGoalCreatedDate, String gsrGoalDescription, Date gsrGoalEndDate, String gsrGoalMeasurement, String gsrGoalMgrFeedback,
				Float gsrGoalMgrRating, Integer gsrGoalModifiedBy, Date gsrGoalModifiedDate, Float gsrGoalSelfRating, Date gsrGoalStartDate,
				Byte gsrGoalWeightage, Short empGoalKpiWeightageId, Integer fkEmpGoalDomainMgrId, short fkEmpGoalBuId, Integer fkGsrDelegatedGoalId,
				Integer pkGsrDelegatedGoalId, Integer fkMgrIdDelegatedTo, Byte delegationStatus
				){
			super();
			this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
			this.fkGsrDatEmpId = fkGsrDatEmpId;
			this.fkGsrDatMgrId = fkGsrDatMgrId;
			this.fkGsrGoalStatus = fkGsrGoalStatus;
			this.fkGsrSlabId = fkGsrSlabId;
			this.gsrDatEmpGoalName = gsrDatEmpGoalName;
			this.gsrGoalAccomplishment = gsrGoalAccomplishment;
			this.gsrGoalClosedBy = gsrGoalClosedBy;
			this.gsrGoalCreatedBy = gsrGoalCreatedBy;
			this.gsrGoalCreatedDate = gsrGoalCreatedDate;
			this.gsrGoalDescription = gsrGoalDescription;
			this.gsrGoalEndDate = gsrGoalEndDate;
			this.gsrGoalMeasurement = gsrGoalMeasurement;
			this.gsrGoalMgrFeedback = gsrGoalMgrFeedback;
			this.gsrGoalMgrRating = gsrGoalMgrRating;
			this.gsrGoalModifiedBy = gsrGoalModifiedBy;
			this.gsrGoalModifiedDate = gsrGoalModifiedDate;
			this.gsrGoalSelfRating = gsrGoalSelfRating;
			this.gsrGoalStartDate = gsrGoalStartDate;
			this.gsrGoalWeightage = gsrGoalWeightage;
			this.empGoalKpiWeightageId = empGoalKpiWeightageId;
			this.fkEmpGoalDomainMgrId = fkEmpGoalDomainMgrId;
			this.fkEmpGoalBuId = fkEmpGoalBuId;
			this.fkGsrDelegatedGoalId = fkGsrDelegatedGoalId;
			this.pkGsrDelegatedGoalId = pkGsrDelegatedGoalId;
			this.fkMgrIdDelegatedTo = fkMgrIdDelegatedTo;
			this.delegationStatus = delegationStatus;
		}
		//EOA by Rajesh Kumar	
	
		
	public Short getFkGsrKpiGoalId() {
		return fkGsrKpiGoalId;
	}

	public Integer getPkGsrDelegatedGoalId() {
		return pkGsrDelegatedGoalId;
	}

	public void setPkGsrDelegatedGoalId(Integer pkGsrDelegatedGoalId) {
		this.pkGsrDelegatedGoalId = pkGsrDelegatedGoalId;
	}

	public Integer getFkMgrIdDelegatedTo() {
		return fkMgrIdDelegatedTo;
	}

	public void setFkMgrIdDelegatedTo(Integer fkMgrIdDelegatedTo) {
		this.fkMgrIdDelegatedTo = fkMgrIdDelegatedTo;
	}

	public Byte getDelegationStatus() {
		return delegationStatus;
	}

	public void setDelegationStatus(Byte delegationStatus) {
		this.delegationStatus = delegationStatus;
	}

	public void setFkGsrKpiGoalId(Short fkGsrKpiGoalId) {
		this.fkGsrKpiGoalId = fkGsrKpiGoalId;
	}

	public String getSlabShortName() {
		return slabShortName;
	}

	public void setSlabShortName(String slabShortName) {
		this.slabShortName = slabShortName;
	}

	public Long getTotalWeightage() {
		return totalWeightage;
	}

	public void setTotalWeightage(Long totalWeightage) {
		this.totalWeightage = totalWeightage;
	}

	public String getFinalRatingStatusName() {
		return finalRatingStatusName;
	}

	public void setFinalRatingStatusName(String finalRatingStatusName) {
		this.finalRatingStatusName = finalRatingStatusName;
	}

	public String getSlabName() {
		return slabName;
	}

	public void setSlabName(String slabName) {
		this.slabName = slabName;
	}

	public Integer getPkGsrDatEmpGoalId() {
		return this.pkGsrDatEmpGoalId;
	}

	public DatEmpDetailBO getDatEmpDetailBOdatemp_id() {
		return datEmpDetailBOdatemp_id;
	}

	public void setDatEmpDetailBOdatemp_id(
			DatEmpDetailBO datEmpDetailBOdatemp_id) {
		this.datEmpDetailBOdatemp_id = datEmpDetailBOdatemp_id;
	}

	public DatEmpDetailBO getDatEmpDetailBOdatmgrid() {
		return datEmpDetailBOdatmgrid;
	}

	public void setDatEmpDetailBOdatmgrid(DatEmpDetailBO datEmpDetailBOdatmgrid) {
		this.datEmpDetailBOdatmgrid = datEmpDetailBOdatmgrid;
	}

	public GsrSlabBO getGsrSlabBO_slab_id() {
		return gsrSlabBO_slab_id;
	}

	public void setGsrSlabBO_slab_id(GsrSlabBO gsrSlabBO_slab_id) {
		this.gsrSlabBO_slab_id = gsrSlabBO_slab_id;
	}

	public DatEmpDetailBO getDatEmpDetailBO_goal_closed_by() {
		return datEmpDetailBO_goal_closed_by;
	}

	public void setDatEmpDetailBO_goal_closed_by(
			DatEmpDetailBO datEmpDetailBO_goal_closed_by) {
		this.datEmpDetailBO_goal_closed_by = datEmpDetailBO_goal_closed_by;
	}

	public DatEmpDetailBO getDatEmpDetailBO_goal_created_by() {
		return datEmpDetailBO_goal_created_by;
	}

	public void setDatEmpDetailBO_goal_created_by(
			DatEmpDetailBO datEmpDetailBO_goal_created_by) {
		this.datEmpDetailBO_goal_created_by = datEmpDetailBO_goal_created_by;
	}

	public DatEmpDetailBO getDatEmpDetailBO_goal_modified_by() {
		return datEmpDetailBO_goal_modified_by;
	}

	public void setDatEmpDetailBO_goal_modified_by(
			DatEmpDetailBO datEmpDetailBO_goal_modified_by) {
		this.datEmpDetailBO_goal_modified_by = datEmpDetailBO_goal_modified_by;
	}

	public void setPkGsrDatEmpGoalId(Integer pkGsrDatEmpGoalId) {
		this.pkGsrDatEmpGoalId = pkGsrDatEmpGoalId;
	}

	public Integer getFkGsrDatEmpId() {
		return this.fkGsrDatEmpId;
	}

	public void setFkGsrDatEmpId(Integer fkGsrDatEmpId) {
		this.fkGsrDatEmpId = fkGsrDatEmpId;
	}

	public Integer getFkGsrDatMgrId() {
		return this.fkGsrDatMgrId;
	}

	public void setFkGsrDatMgrId(Integer fkGsrDatMgrId) {
		this.fkGsrDatMgrId = fkGsrDatMgrId;
	}

	public Byte getFkGsrGoalStatus() {
		return this.fkGsrGoalStatus;
	}

	public void setFkGsrGoalStatus(Byte fkGsrGoalStatus) {
		this.fkGsrGoalStatus = fkGsrGoalStatus;
	}

	public Short getFkGsrSlabId() {
		return this.fkGsrSlabId;
	}

	public void setFkGsrSlabId(Short fkGsrSlabId) {
		this.fkGsrSlabId = fkGsrSlabId;
	}

	public String getGsrDatEmpGoalName() {
		return this.gsrDatEmpGoalName;
	}

	public void setGsrDatEmpGoalName(String gsrDatEmpGoalName) {
		this.gsrDatEmpGoalName = gsrDatEmpGoalName;
	}

	public String getGsrGoalAccomplishment() {
		return this.gsrGoalAccomplishment;
	}

	public void setGsrGoalAccomplishment(String gsrGoalAccomplishment) {
		this.gsrGoalAccomplishment = gsrGoalAccomplishment;
	}

	public Integer getGsrGoalClosedBy() {
		return this.gsrGoalClosedBy;
	}

	public void setGsrGoalClosedBy(Integer gsrGoalClosedBy) {
		this.gsrGoalClosedBy = gsrGoalClosedBy;
	}

	public Integer getGsrGoalCreatedBy() {
		return this.gsrGoalCreatedBy;
	}

	public void setGsrGoalCreatedBy(Integer gsrGoalCreatedBy) {
		this.gsrGoalCreatedBy = gsrGoalCreatedBy;
	}

	public Date getGsrGoalCreatedDate() {
		return this.gsrGoalCreatedDate;
	}

	public void setGsrGoalCreatedDate(Date gsrGoalCreatedDate) {
		this.gsrGoalCreatedDate = gsrGoalCreatedDate;
	}

	public String getGsrGoalDescription() {
		return this.gsrGoalDescription;
	}

	public void setGsrGoalDescription(String gsrGoalDescription) {
		this.gsrGoalDescription = gsrGoalDescription;
	}

	public Date getGsrGoalEndDate() {
		return this.gsrGoalEndDate;
	}

	public void setGsrGoalEndDate(Date gsrGoalEndDate) {
		this.gsrGoalEndDate = gsrGoalEndDate;
	}

	public String getGsrGoalMeasurement() {
		return this.gsrGoalMeasurement;
	}

	public void setGsrGoalMeasurement(String gsrGoalMeasurement) {
		this.gsrGoalMeasurement = gsrGoalMeasurement;
	}

	public String getGsrGoalMgrFeedback() {
		return this.gsrGoalMgrFeedback;
	}

	public void setGsrGoalMgrFeedback(String gsrGoalMgrFeedback) {
		this.gsrGoalMgrFeedback = gsrGoalMgrFeedback;
	}

	public Float getGsrGoalMgrRating() {
		return this.gsrGoalMgrRating;
	}

	public void setGsrGoalMgrRating(Float gsrGoalMgrRating) {
		this.gsrGoalMgrRating = gsrGoalMgrRating;
	}

	public Integer getGsrGoalModifiedBy() {
		return this.gsrGoalModifiedBy;
	}

	public void setGsrGoalModifiedBy(Integer gsrGoalModifiedBy) {
		this.gsrGoalModifiedBy = gsrGoalModifiedBy;
	}

	public Date getGsrGoalModifiedDate() {
		return this.gsrGoalModifiedDate;
	}

	public void setGsrGoalModifiedDate(Date gsrGoalModifiedDate) {
		this.gsrGoalModifiedDate = gsrGoalModifiedDate;
	}

	public Float getGsrGoalSelfRating() {
		return this.gsrGoalSelfRating;
	}

	public void setGsrGoalSelfRating(Float gsrGoalSelfRating) {
		this.gsrGoalSelfRating = gsrGoalSelfRating;
	}

	public Date getGsrGoalStartDate() {
		return this.gsrGoalStartDate;
	}

	public void setGsrGoalStartDate(Date gsrGoalStartDate) {
		this.gsrGoalStartDate = gsrGoalStartDate;
	}

	public Byte getGsrGoalWeightage() {
		return this.gsrGoalWeightage;
	}

	public void setGsrGoalWeightage(Byte gsrGoalWeightage) {
		this.gsrGoalWeightage = gsrGoalWeightage;
	}

	public Float getGsrFinalRating() {
		return gsrFinalRating;
	}

	public void setGsrFinalRating(Float gsrFinalRating) {
		this.gsrFinalRating = gsrFinalRating;
	}

	public Byte getFinalRatingStatus() {
		return finalRatingStatus;
	}

	public void setFinalRatingStatus(Byte finalRatingStatus) {
		this.finalRatingStatus = finalRatingStatus;
	}

	public String getIsOverriddenFlag() {
		return isOverriddenFlag;
	}

	public void setIsOverriddenFlag(String isOverriddenFlag) {
		this.isOverriddenFlag = isOverriddenFlag;
	}

	public Date getRatingAcceptOrRejectDate() {
		return ratingAcceptOrRejectDate;
	}

	public void setRatingAcceptOrRejectDate(Date ratingAcceptOrRejectDate) {
		this.ratingAcceptOrRejectDate = ratingAcceptOrRejectDate;
	}

	public String getGsrGoalStatusName() {
		return gsrGoalStatusName;
	}

	public void setGsrGoalStatusName(String gsrGoalStatusName) {
		this.gsrGoalStatusName = gsrGoalStatusName;
	}

	public String getGsrSlabYear() {
		return gsrSlabYear;
	}

	public void setGsrSlabYear(String gsrSlabYear) {
		this.gsrSlabYear = gsrSlabYear;
	}

	public String getEmpFirstName() {
		return empFirstName;
	}

	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}

	public String getEmpLastName() {
		return empLastName;
	}

	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}

	public GsrMasGoalStatusBO getGsrMasGoalStatusBOgoalstatus() {
		return gsrMasGoalStatusBOgoalstatus;
	}

	public void setGsrMasGoalStatusBOgoalstatus(
			GsrMasGoalStatusBO gsrMasGoalStatusBOgoalstatus) {
		this.gsrMasGoalStatusBOgoalstatus = gsrMasGoalStatusBOgoalstatus;
	}

	public Short getEmpGoalKpiWeightageId() {
		return (empGoalKpiWeightageId == null ? 0 : empGoalKpiWeightageId);
	}

	public void setEmpGoalKpiWeightageId(Short empGoalKpiWeightageId) {
		Short nonKpiGoalId = 0;
		this.empGoalKpiWeightageId = ((nonKpiGoalId)
				.compareTo(empGoalKpiWeightageId) == 0) ? null
				: empGoalKpiWeightageId;
	}

	public Integer getFkEmpGoalDomainMgrId() {
		return fkEmpGoalDomainMgrId;
	}

	public void setFkEmpGoalDomainMgrId(Integer fkEmpGoalDomainMgrId) {
		this.fkEmpGoalDomainMgrId = fkEmpGoalDomainMgrId;
	}

	public short getFkEmpGoalBuId() {
		return fkEmpGoalBuId;
	}

	public void setFkEmpGoalBuId(short fkEmpGoalBuId) {
		this.fkEmpGoalBuId = fkEmpGoalBuId;
	}
	
	

	public GsrKpiWeightageBO getGsrKpiWeightageDetails() {
		return gsrKpiWeightageDetails;
	}

	public void setGsrKpiWeightageDetails(GsrKpiWeightageBO gsrKpiWeightageDetails) {
		this.gsrKpiWeightageDetails = gsrKpiWeightageDetails;
	}

	@Override
	public String toString() {
		return "GsrDatEmpGoalBO [pkGsrDatEmpGoalId=" + pkGsrDatEmpGoalId
				+ ", fkGsrDatEmpId=" + fkGsrDatEmpId + ", fkGsrDatMgrId="
				+ fkGsrDatMgrId + ", fkGsrGoalStatus=" + fkGsrGoalStatus
				+ ", fkGsrSlabId=" + fkGsrSlabId + ", gsrDatEmpGoalName="
				+ gsrDatEmpGoalName + ", gsrGoalAccomplishment="
				+ gsrGoalAccomplishment + ", gsrGoalClosedBy="
				+ gsrGoalClosedBy + ", gsrGoalCreatedBy=" + gsrGoalCreatedBy
				+ ", gsrGoalCreatedDate=" + gsrGoalCreatedDate
				+ ", gsrGoalDescription=" + gsrGoalDescription
				+ ", gsrGoalEndDate=" + gsrGoalEndDate
				+ ", gsrGoalMeasurement=" + gsrGoalMeasurement
				+ ", gsrGoalMgrFeedback=" + gsrGoalMgrFeedback
				+ ", gsrGoalMgrRating=" + gsrGoalMgrRating
				+ ", gsrGoalModifiedBy=" + gsrGoalModifiedBy
				+ ", gsrGoalModifiedDate=" + gsrGoalModifiedDate
				+ ", gsrGoalSelfRating=" + gsrGoalSelfRating
				+ ", gsrGoalStartDate=" + gsrGoalStartDate
				+ ", gsrGoalWeightage=" + gsrGoalWeightage
				+ ", empGoalKpiWeightageId=" + empGoalKpiWeightageId
				+ ", fkEmpGoalDomainMgrId=" + fkEmpGoalDomainMgrId
				+ ", fkEmpGoalBuId=" + fkEmpGoalBuId
				+ ", fkGsrDelegatedGoalId=" + fkGsrDelegatedGoalId
				+ ", datEmpDetailBOdatemp_id=" + datEmpDetailBOdatemp_id
				+ ", datEmpDetailBOdatmgrid=" + datEmpDetailBOdatmgrid
				+ ", gsrMasGoalStatusBOgoalstatus="
				+ gsrMasGoalStatusBOgoalstatus + ", gsrSlabBO_slab_id="
				+ gsrSlabBO_slab_id + ", datEmpDetailBO_goal_closed_by="
				+ datEmpDetailBO_goal_closed_by
				+ ", datEmpDetailBO_goal_created_by="
				+ datEmpDetailBO_goal_created_by
				+ ", datEmpDetailBO_goal_modified_by="
				+ datEmpDetailBO_goal_modified_by + ", gsrKpiWeightageDetails="
				+ gsrKpiWeightageDetails + ", gsrFinalRating=" + gsrFinalRating
				+ ", finalRatingStatus=" + finalRatingStatus
				+ ", isOverriddenFlag=" + isOverriddenFlag
				+ ", ratingAcceptOrRejectDate=" + ratingAcceptOrRejectDate
				+ ", gsrGoalStatusName=" + gsrGoalStatusName + ", gsrSlabYear="
				+ gsrSlabYear + ", empFirstName=" + empFirstName
				+ ", empLastName=" + empLastName + ", finalRatingStatusName="
				+ finalRatingStatusName + ", slabName=" + slabName
				+ ", totalWeightage=" + totalWeightage + ", slabShortName="
				+ slabShortName + ", gsrSlabName=" + gsrSlabName
				+ ", fkBuHeadEmpId=" + fkBuHeadEmpId + ", gsrSlabId="
				+ gsrSlabId + ", overriddenByName=" + overriddenByName
				+ ", overriddenBy=" + overriddenBy + ", modifiedDate="
				+ modifiedDate + ", mgrName=" + mgrName + ", buHeadEmpid="
				+ buHeadEmpid + ", fkGsrKpiGoalId=" + fkGsrKpiGoalId
				+ ", pkGsrDelegatedGoalId=" + pkGsrDelegatedGoalId
				+ ", fkMgrIdDelegatedTo=" + fkMgrIdDelegatedTo
				+ ", delegationStatus=" + delegationStatus + "]";
	}
	

}
