package com.thbs.mis.gsr.bo;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the gsr_normalised_rating_org_wide database table.
 * 
 */
@Entity
@Table(name="gsr_normalised_rating_org_wide")
@NamedQuery(name="GsrNormalisedRatingOrgWideBO.findAll", query="SELECT g FROM GsrNormalisedRatingOrgWideBO g")
public class GsrNormalisedRatingOrgWideBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_gsr_normalised_rating_id")
	private int pkGsrNormalisedRatingId;

	@Column(name="fk_gsr_normalised_rating_emp_id")
	private int fkGsrNormalisedRatingEmpId;

	@Column(name="gsr_normalised_rating")
	private float gsrNormalisedRating;

	@Column(name="gsr_normalised_rating_created_by")
	private int gsrNormalisedRatingCreatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="gsr_normalised_rating_created_date")
	private Date gsrNormalisedRatingCreatedDate;

	@Column(name="gsr_normalised_rating_modified_by")
	private int gsrNormalisedRatingModifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="gsr_normalised_rating_modified_date")
	private Date gsrNormalisedRatingModifiedDate;

	@Temporal(TemporalType.DATE)
	@Column(name="gsr_normalised_rating_year")
	private Date gsrNormalisedRatingYear;


	
	public GsrNormalisedRatingOrgWideBO() {
	}

	public int getPkGsrNormalisedRatingId() {
		return this.pkGsrNormalisedRatingId;
	}

	public void setPkGsrNormalisedRatingId(int pkGsrNormalisedRatingId) {
		this.pkGsrNormalisedRatingId = pkGsrNormalisedRatingId;
	}

	public int getFkGsrNormalisedRatingEmpId() {
		return this.fkGsrNormalisedRatingEmpId;
	}

	public void setFkGsrNormalisedRatingEmpId(int fkGsrNormalisedRatingEmpId) {
		this.fkGsrNormalisedRatingEmpId = fkGsrNormalisedRatingEmpId;
	}

	public float getGsrNormalisedRating() {
		return this.gsrNormalisedRating;
	}

	public void setGsrNormalisedRating(float gsrNormalisedRating) {
		this.gsrNormalisedRating = gsrNormalisedRating;
	}

	public int getGsrNormalisedRatingCreatedBy() {
		return this.gsrNormalisedRatingCreatedBy;
	}

	public void setGsrNormalisedRatingCreatedBy(int gsrNormalisedRatingCreatedBy) {
		this.gsrNormalisedRatingCreatedBy = gsrNormalisedRatingCreatedBy;
	}

	public Date getGsrNormalisedRatingCreatedDate() {
		return this.gsrNormalisedRatingCreatedDate;
	}

	public void setGsrNormalisedRatingCreatedDate(Date gsrNormalisedRatingCreatedDate) {
		this.gsrNormalisedRatingCreatedDate = gsrNormalisedRatingCreatedDate;
	}

	public int getGsrNormalisedRatingModifiedBy() {
		return this.gsrNormalisedRatingModifiedBy;
	}

	public void setGsrNormalisedRatingModifiedBy(int gsrNormalisedRatingModifiedBy) {
		this.gsrNormalisedRatingModifiedBy = gsrNormalisedRatingModifiedBy;
	}

	public Date getGsrNormalisedRatingModifiedDate() {
		return this.gsrNormalisedRatingModifiedDate;
	}

	public void setGsrNormalisedRatingModifiedDate(Date gsrNormalisedRatingModifiedDate) {
		this.gsrNormalisedRatingModifiedDate = gsrNormalisedRatingModifiedDate;
	}

	public Date getGsrNormalisedRatingYear() {
		return this.gsrNormalisedRatingYear;
	}

	public void setGsrNormalisedRatingYear(Date gsrNormalisedRatingYear) {
		this.gsrNormalisedRatingYear = gsrNormalisedRatingYear;
	}

	@Override
	public String toString()
	{
		return "GsrNormalisedRatingOrgWideBO [pkGsrNormalisedRatingId="
				+ pkGsrNormalisedRatingId
				+ ", fkGsrNormalisedRatingEmpId="
				+ fkGsrNormalisedRatingEmpId + ", gsrNormalisedRating="
				+ gsrNormalisedRating
				+ ", gsrNormalisedRatingCreatedBy="
				+ gsrNormalisedRatingCreatedBy
				+ ", gsrNormalisedRatingCreatedDate="
				+ gsrNormalisedRatingCreatedDate
				+ ", gsrNormalisedRatingModifiedBy="
				+ gsrNormalisedRatingModifiedBy
				+ ", gsrNormalisedRatingModifiedDate="
				+ gsrNormalisedRatingModifiedDate
				+ ", gsrNormalisedRatingYear="
				+ gsrNormalisedRatingYear + "]";
	}

}