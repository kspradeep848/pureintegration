package com.thbs.mis.gsr.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.MasBootCampBO;
import com.thbs.mis.common.bo.MasWorkFlowBO;

import java.util.Date;


/**
 * The persistent class for the gsr_window database table.
 * 
 */
@Entity
@Table(name="gsr_window")
@NamedQuery(name="GsrWindowBO.findAll", query="SELECT g FROM GsrWindowBO g")
public class GsrWindowBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_gsr_window_id")
	private int pkGsrWindowId;

	@Column(name="fk_gsr_work_flow_slab_id")
	private short fkGsrWorkFlowSlabId;

	@Column(name="fk_work_flow_id")
	private int fkWorkFlowId;

	@Column(name="gsr_window_created_by")
	private int gsrWindowCreatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="gsr_window_created_date")
	private Date gsrWindowCreatedDate;

	@Temporal(TemporalType.DATE)
	@Column(name="gsr_window_end_date")
	private Date gsrWindowEndDate;

	@Column(name="gsr_window_modified_by")
	private Short gsrWindowModifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="gsr_window_modified_date")
	private Date gsrWindowModifiedDate;

	@Temporal(TemporalType.DATE)
	@Column(name="gsr_window_start_date")
	private Date gsrWindowStartDate;

	
	@OneToOne
	@JoinColumn(name="fk_work_flow_id",unique = true, nullable = true, insertable = false, updatable = false)
	private MasWorkFlowBO MasWorkFlowBO;
	
	
	public GsrWindowBO() {
	}

	
	
	public MasWorkFlowBO getMasWorkFlowBO() {
		return MasWorkFlowBO;
	}



	public void setMasWorkFlowBO(MasWorkFlowBO masWorkFlowBO) {
		MasWorkFlowBO = masWorkFlowBO;
	}



	public int getPkGsrWindowId() {
		return this.pkGsrWindowId;
	}

	public void setPkGsrWindowId(int pkGsrWindowId) {
		this.pkGsrWindowId = pkGsrWindowId;
	}

	public short getFkGsrWorkFlowSlabId() {
		return this.fkGsrWorkFlowSlabId;
	}

	public void setFkGsrWorkFlowSlabId(short fkGsrWorkFlowSlabId) {
		this.fkGsrWorkFlowSlabId = fkGsrWorkFlowSlabId;
	}

	public int getFkWorkFlowId() {
		return this.fkWorkFlowId;
	}

	public void setFkWorkFlowId(int fkWorkFlowId) {
		this.fkWorkFlowId = fkWorkFlowId;
	}

	public int getGsrWindowCreatedBy() {
		return this.gsrWindowCreatedBy;
	}

	public void setGsrWindowCreatedBy(int gsrWindowCreatedBy) {
		this.gsrWindowCreatedBy = gsrWindowCreatedBy;
	}

	public Date getGsrWindowCreatedDate() {
		return this.gsrWindowCreatedDate;
	}

	public void setGsrWindowCreatedDate(Date gsrWindowCreatedDate) {
		this.gsrWindowCreatedDate = gsrWindowCreatedDate;
	}

	public Date getGsrWindowEndDate() {
		return this.gsrWindowEndDate;
	}

	public void setGsrWindowEndDate(Date gsrWindowEndDate) {
		this.gsrWindowEndDate = gsrWindowEndDate;
	}


	public Short getGsrWindowModifiedBy() {
		return gsrWindowModifiedBy;
	}

	public void setGsrWindowModifiedBy(Short gsrWindowModifiedBy) {
		this.gsrWindowModifiedBy = gsrWindowModifiedBy;
	}

	public Date getGsrWindowModifiedDate() {
		return this.gsrWindowModifiedDate;
	}

	public void setGsrWindowModifiedDate(Date gsrWindowModifiedDate) {
		this.gsrWindowModifiedDate = gsrWindowModifiedDate;
	}

	public Date getGsrWindowStartDate() {
		return this.gsrWindowStartDate;
	}

	public void setGsrWindowStartDate(Date gsrWindowStartDate) {
		this.gsrWindowStartDate = gsrWindowStartDate;
	}

	@Override
	public String toString() {
		return "GsrWindowBO [pkGsrWindowId=" + pkGsrWindowId
				+ ", fkGsrWorkFlowSlabId=" + fkGsrWorkFlowSlabId
				+ ", fkWorkFlowId=" + fkWorkFlowId + ", gsrWindowCreatedBy="
				+ gsrWindowCreatedBy + ", gsrWindowCreatedDate="
				+ gsrWindowCreatedDate + ", gsrWindowEndDate="
				+ gsrWindowEndDate + ", gsrWindowModifiedBy="
				+ gsrWindowModifiedBy + ", gsrWindowModifiedDate="
				+ gsrWindowModifiedDate + ", gsrWindowStartDate="
				+ gsrWindowStartDate + ", MasWorkFlowBO=" + MasWorkFlowBO + "]";
	}

	

}