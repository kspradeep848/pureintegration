package com.thbs.mis.gsr.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.thbs.mis.common.bo.MasEmpLevelBO;


/**
 * The persistent class for the gsr_kpi_weightage database table.
 * 
 */
@Entity
@Table(name="gsr_kpi_weightage")
@NamedQuery(name="GsrKpiWeightageBO.findAll", query="SELECT g FROM GsrKpiWeightageBO g")
public class GsrKpiWeightageBO implements Serializable {
	private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_gsr_kpi_weightage_id")
	private short pkGsrKpiWeightageId;
	
	@Column(name="fk_emp_level_id")
	private short fkEmpLevelId;

	@Column(name="fk_gsr_kpi_goal_id")
	private short fkGsrKpiGoalId;

	@Column(name="gsr_kpi_goal_weightage")
	private byte gsrKpiGoalWeightage;
	
	@Column(name="fk_bu_id")
	private byte gsrKpiBuId;

	public MasEmpLevelBO getEmployeeLevel() {
		return employeeLevel;
	}

	public void setEmployeeLevel(MasEmpLevelBO employeeLevel) {
		this.employeeLevel = employeeLevel;
	}

	public GsrKpiGoalBO getGsrKpiGoal() {
		return gsrKpiGoal;
	}

	public void setGsrKpiGoal(GsrKpiGoalBO gsrKpiGoal) {
		this.gsrKpiGoal = gsrKpiGoal;
	}

	

	@OneToOne()
	@JoinColumn(name = "fk_emp_level_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpLevelBO employeeLevel;
	
	@OneToOne()
	@JoinColumn(name = "fk_gsr_kpi_goal_id", unique = true, nullable = true, insertable = false, updatable = false)
	private GsrKpiGoalBO gsrKpiGoal;
	
	
	@Transient
	private Integer empId;
	
	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public GsrKpiWeightageBO() {
	}

	public short getPkGsrKpiWeightageId() {
		return this.pkGsrKpiWeightageId;
	}

	public void setPkGsrKpiWeightageId(short pkGsrKpiWeightageId) {
		this.pkGsrKpiWeightageId = pkGsrKpiWeightageId;
	}

	public short getFkEmpLevelId() {
		return this.fkEmpLevelId;
	}

	public void setFkEmpLevelId(short fkEmpLevelId) {
		this.fkEmpLevelId = fkEmpLevelId;
	}

	public short getFkGsrKpiGoalId() {
		return this.fkGsrKpiGoalId;
	}

	public void setFkGsrKpiGoalId(short fkGsrKpiGoalId) {
		this.fkGsrKpiGoalId = fkGsrKpiGoalId;
	}

	public byte getGsrKpiGoalWeightage() {
		return this.gsrKpiGoalWeightage;
	}

	public void setGsrKpiGoalWeightage(byte gsrKpiGoalWeightage) {
		this.gsrKpiGoalWeightage = gsrKpiGoalWeightage;
	}
	

	public byte getGsrKpiBuId()
	{
		return gsrKpiBuId;
	}

	public void setGsrKpiBuId(byte gsrKpiBuId)
	{
		this.gsrKpiBuId = gsrKpiBuId;
	}

	

	
	public GsrKpiWeightageBO(short fkEmpLevelId,byte gsrKpiBuId,short pkGsrKpiWeightageId,byte gsrKpiGoalWeightage
		) {
		super();
		this.fkEmpLevelId = fkEmpLevelId;
		this.gsrKpiBuId = gsrKpiBuId;
		this.pkGsrKpiWeightageId = pkGsrKpiWeightageId;
		this.gsrKpiGoalWeightage = gsrKpiGoalWeightage;
	}

	@Override
	public String toString()
	{
		return "GsrKpiWeightageBO [pkGsrKpiWeightageId="
				+ pkGsrKpiWeightageId + ", fkEmpLevelId="
				+ fkEmpLevelId + ", fkGsrKpiGoalId=" + fkGsrKpiGoalId
				+ ", gsrKpiGoalWeightage=" + gsrKpiGoalWeightage
				+ ", gsrKpiBuId=" + gsrKpiBuId + ", employeeLevel="
				+ employeeLevel + ", gsrKpiGoal=" + gsrKpiGoal + "]";
	}
	public GsrKpiWeightageBO(
			short fkEmpLevelId,
			byte gsrKpiBuId,
			short pkGsrKpiWeightageId,
			byte gsrKpiGoalWeightage,
			short fkGsrKpiGoalId
			) {
		super();
		this.pkGsrKpiWeightageId = pkGsrKpiWeightageId;
		this.fkEmpLevelId = fkEmpLevelId;
		this.fkGsrKpiGoalId = fkGsrKpiGoalId;
		this.gsrKpiGoalWeightage = gsrKpiGoalWeightage;
		this.gsrKpiBuId = gsrKpiBuId;
	}

}