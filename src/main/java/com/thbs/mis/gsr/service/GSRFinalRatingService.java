/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRFinalRatingService.java                     	 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  13-Nov-2016                                       */
/*                                                                   */
/*  Description :  Contains all the service related to the Final     */
/*                 rating functionalities.                           */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 13-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bean.GsrDisagreeAndResolvedFinalRatingOutputBean;
import com.thbs.mis.gsr.bean.GsrDisagreeAndResolvedFinalRatingUnderBUHeadBean;
import com.thbs.mis.gsr.bean.GsrDisagreeAndResolvedFinalRatingUnderSkipLevelMgr;
import com.thbs.mis.gsr.bean.GsrDisagreeFinalRatingReporteeListBean;
import com.thbs.mis.gsr.bean.GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean;
import com.thbs.mis.gsr.bean.GsrDisagreeAndResolvedFinalRatingUnderDmBean;
import com.thbs.mis.gsr.bean.GsrResolvedFinalRatingReporteeListBean;
import com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO;
import com.thbs.mis.gsr.dao.GSRFinalRatingRepository;

@Service
public class GSRFinalRatingService {
	
	
	/* This Variable is used to call the methods are present in the Final rating repository */
	@Autowired
	private GSRFinalRatingRepository finalRatingRepository;
	
	/* This Variable is used to call the methods are present in the Employee professional repository.*/
	@Autowired
	private DatEmployeeProfessionalRepository empProfDetailsrepository;
	
	/* This Variable is used to call the methods are present in the Employee professional repository.*/
	@Autowired
	private EmployeePersonalDetailsRepository empPersonalDetailsrepository;

	/**
	 * Instance of <code>Log</code>
	 */
	@SuppressWarnings("unused")
	private static final AppLog LOG = LogFactory.getLog(GSRFinalRatingService.class);
	
	private static final int DISAGREE_FINAL_RATING_STATUS = 3;
	private static final int RESOLVED_FINAL_RATING_STATUS = 4;

	public GSRFinalRatingService() {
		super();
	}

	//Added by Shyam Lama
	
	/**
	 * @Description This method will fetch all disagree reportees which are falls under a domain manager.
	 * @param GsrDisagreeAndResolvedFinalRatingUnderDmBean bean contains the input details are to be used in the service.
	 * @return GsrDisagreeFinalRatingUnderDMOutputBean list of reportees with final rating BO.
	 * @throws DataAccessException throws when the data access exception occurs.
	 * 
	 */
	
	public GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean getDisagreeAndResolvedRatingOfASlabForDomainMgr(
			GsrDisagreeAndResolvedFinalRatingUnderDmBean disagreeAndResolvedFinalRatingBean)
			throws CommonCustomException 
	{
		GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean disagreeAndResolvedFinalRatingOutputBeanList = 
				new GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean();
		List<GsrDisagreeFinalRatingReporteeListBean> disagreeFinalRatingReporteeList = 
				new ArrayList<GsrDisagreeFinalRatingReporteeListBean>();
		List<GsrResolvedFinalRatingReporteeListBean> resolvedFinalRatingReporteeList = 
				new ArrayList<GsrResolvedFinalRatingReporteeListBean>();
		
		List<GsrDatEmpFinalRatingBO> disagreeAndResolvedFinalRatingBOList = new ArrayList<GsrDatEmpFinalRatingBO>();
		try {
			disagreeAndResolvedFinalRatingBOList = finalRatingRepository.
					getDisagreeAndResolvedRatingDetailsForASlabUnderDM( 
						disagreeAndResolvedFinalRatingBean.getSlabId(), 
						disagreeAndResolvedFinalRatingBean.getDomainMgrId());
		}catch(Exception ex){
			throw new CommonCustomException("Failed to retrieve disagree and resolved final rating for "
					+ "a slab under domain manager. " + ex.getMessage());
		}
		
		disagreeAndResolvedFinalRatingOutputBeanList.setSlabId(disagreeAndResolvedFinalRatingBean.getSlabId());
		
		for (GsrDatEmpFinalRatingBO gsrDatEmpFinalRatingBO : disagreeAndResolvedFinalRatingBOList) 
		{
			if(gsrDatEmpFinalRatingBO.getFinalRatingStatus() == DISAGREE_FINAL_RATING_STATUS)
			{
				GsrDisagreeFinalRatingReporteeListBean disagreeFinalRatingBean = 
						new GsrDisagreeFinalRatingReporteeListBean();
				disagreeFinalRatingBean.setFinalRatingPkId(gsrDatEmpFinalRatingBO.getPkGsrFinalRatingId());
				disagreeFinalRatingBean.setReporteeId(gsrDatEmpFinalRatingBO.getFkGsrFinalRatingEmpId());
				disagreeFinalRatingBean.setReporteeName(gsrDatEmpFinalRatingBO.getEmpFirstName() + " " 
						+ gsrDatEmpFinalRatingBO.getEmpLastName());
				disagreeFinalRatingBean.setDisagreeFinalRating(gsrDatEmpFinalRatingBO.getGsrFinalRating());
				disagreeFinalRatingReporteeList.add(disagreeFinalRatingBean);
			}else{
				if(gsrDatEmpFinalRatingBO.getFinalRatingStatus() == RESOLVED_FINAL_RATING_STATUS)
				{
					GsrResolvedFinalRatingReporteeListBean resolvedFinalRatingBean =  
							new GsrResolvedFinalRatingReporteeListBean();
					resolvedFinalRatingBean.setFinalRatingId(gsrDatEmpFinalRatingBO.getPkGsrFinalRatingId());
					resolvedFinalRatingBean.setReporteeId(gsrDatEmpFinalRatingBO.getFkGsrFinalRatingEmpId());
					resolvedFinalRatingBean.setReporteeName(gsrDatEmpFinalRatingBO.getEmpFirstName() + " "
							+ gsrDatEmpFinalRatingBO.getEmpLastName());
					resolvedFinalRatingBean.setResolvedFinalRating(gsrDatEmpFinalRatingBO.getGsrFinalRating());
					resolvedFinalRatingReporteeList.add(resolvedFinalRatingBean);
				}
			}
		}
		
		disagreeAndResolvedFinalRatingOutputBeanList.setListOfDisgareeFinalRatingOfASlab(
				disagreeFinalRatingReporteeList);
		disagreeAndResolvedFinalRatingOutputBeanList.setListOfResolvedFinalRatingOfASlab(
				resolvedFinalRatingReporteeList);
		
		return disagreeAndResolvedFinalRatingOutputBeanList;
	}
		

	
	/**
	 * @Description This method will fetch all disagree/resolved reportees which are falls under a BU.
	 * @param GsrDisagreeAndResolvedFinalRatingUnderBUHeadBean bean contains the input details are to be used in the service.
	 * @return List<GsrDisagreeAndResolvedFinalRatingOutputBean> list of reportees with final rating BO.
	 * @throws DataAccessException throws when the data access exception occurs.
	 * 
	 */
	
	public List<GsrDisagreeAndResolvedFinalRatingOutputBean> getDisagreeAndResolvedRatingOfASlabForBUHead(
			GsrDisagreeAndResolvedFinalRatingUnderBUHeadBean disagreeAndResolvedFinalRatingUnderBUHeadBean) 
					throws DataAccessException
	{
		List<GsrDisagreeAndResolvedFinalRatingOutputBean> disagreeAndResolvedFinalRatingOutputBean = 
				new ArrayList<GsrDisagreeAndResolvedFinalRatingOutputBean>();
		List<GsrDatEmpFinalRatingBO> disagreeAndResolvedFinalRatingList = new ArrayList<GsrDatEmpFinalRatingBO>();
		try{
			disagreeAndResolvedFinalRatingList = finalRatingRepository.
					getDisagreeOrResolvedRationgDetailsForASlabUnderBUHead(
							disagreeAndResolvedFinalRatingUnderBUHeadBean.getSlabId(), 
							disagreeAndResolvedFinalRatingUnderBUHeadBean.getBusinessUnitId());
		}catch(Exception ex){
			throw new DataAccessException("Failed while retriving disagree and resolved final rating for a slab under BU Head",ex);
		}
		
		Set<Integer> reportingManagerIdSet = new TreeSet<Integer>();
		List<GsrDisagreeFinalRatingReporteeListBean> disagreeFinalRatingReporteeList = 
				new ArrayList<GsrDisagreeFinalRatingReporteeListBean>();
		List<GsrResolvedFinalRatingReporteeListBean> resolvedFinalRatingReporteeList = 
				new ArrayList<GsrResolvedFinalRatingReporteeListBean>();
		
		//Collect the list of disagree or resolved reportees for this bean "GsrDisagreeOrResolvedFinalRatingReporteeListBean" List with mgr id
		for (GsrDatEmpFinalRatingBO gsrDatEmpFinalRatingBO : disagreeAndResolvedFinalRatingList) {
			GsrDisagreeFinalRatingReporteeListBean disagreeFinalRatingBean = new 
					GsrDisagreeFinalRatingReporteeListBean();
			GsrResolvedFinalRatingReporteeListBean resolvedFinalRatingBean = new 
					GsrResolvedFinalRatingReporteeListBean();
			//Add reporting manager id to Set Collection Interface 
			reportingManagerIdSet.add(gsrDatEmpFinalRatingBO.getFkGsrFinalRatingMgrId());
			
			//Store only disagree final rating
			if(gsrDatEmpFinalRatingBO.getFinalRatingStatus() == DISAGREE_FINAL_RATING_STATUS) {
				disagreeFinalRatingBean.setFinalRatingPkId(gsrDatEmpFinalRatingBO.getPkGsrFinalRatingId());
				disagreeFinalRatingBean.setReporteeId(gsrDatEmpFinalRatingBO.getFkGsrFinalRatingEmpId());
				disagreeFinalRatingBean.setReporteeName(gsrDatEmpFinalRatingBO.getEmpFirstName() + " " 
						+ gsrDatEmpFinalRatingBO.getEmpLastName());
				disagreeFinalRatingBean.setReportingMgrId(gsrDatEmpFinalRatingBO.getFkGsrFinalRatingMgrId());
				disagreeFinalRatingBean.setDisagreeFinalRating(gsrDatEmpFinalRatingBO.getGsrFinalRating().floatValue());
				disagreeFinalRatingReporteeList.add(disagreeFinalRatingBean);
			}
			//Store only resolved final rating
			if(gsrDatEmpFinalRatingBO.getFinalRatingStatus() == RESOLVED_FINAL_RATING_STATUS){
				resolvedFinalRatingBean.setFinalRatingId(gsrDatEmpFinalRatingBO.getPkGsrFinalRatingId());
				resolvedFinalRatingBean.setReporteeId(gsrDatEmpFinalRatingBO.getFkGsrFinalRatingEmpId());
				resolvedFinalRatingBean.setReporteeName(gsrDatEmpFinalRatingBO.getEmpFirstName() + " " 
						+ gsrDatEmpFinalRatingBO.getEmpLastName());
				resolvedFinalRatingBean.setReportingMgrId(gsrDatEmpFinalRatingBO.getFkGsrFinalRatingMgrId());
				resolvedFinalRatingBean.setResolvedFinalRating(gsrDatEmpFinalRatingBO.getGsrFinalRating().floatValue());
				resolvedFinalRatingReporteeList.add(resolvedFinalRatingBean);
			}
			
		}
		
		//Iterate each reporting manager id from a Set of reporting managers
		for (Integer integerMgrId : reportingManagerIdSet) {
			GsrDisagreeAndResolvedFinalRatingOutputBean finalRatingOutputBean =  new 
					GsrDisagreeAndResolvedFinalRatingOutputBean();
			List<GsrDisagreeFinalRatingReporteeListBean> disagreeFinalRatingOfReporteesList = new 
					ArrayList<GsrDisagreeFinalRatingReporteeListBean>();
			List<GsrResolvedFinalRatingReporteeListBean> resolvedFinalRatingOfReporteesList = new 
					ArrayList<GsrResolvedFinalRatingReporteeListBean>();
			
			//Counter is initiated for number of disagree  and resolved reportees 
			int countOfDisagreeReportees = 0;
			int countOfResolvedReportees = 0;
			
			for (GsrDatEmpFinalRatingBO finalRatingObject : disagreeAndResolvedFinalRatingList) {
				
				if(integerMgrId == finalRatingObject.getFkGsrFinalRatingMgrId()){
					if(countOfDisagreeReportees == 0 && countOfResolvedReportees == 0){//For first record of particular mgr id from disagreeAndResolvedFinalRatingList
						finalRatingOutputBean.setReportingMgrId(finalRatingObject.getFkGsrFinalRatingMgrId());
						finalRatingOutputBean.setReportingMgrName(finalRatingObject.getMgrFirstName() + " " 
								+ finalRatingObject.getMgrLastName());
						finalRatingOutputBean.setSlabId(finalRatingObject.getFkGsrFinalRatingSlabId());
						
						if(finalRatingObject.getFinalRatingStatus() == DISAGREE_FINAL_RATING_STATUS){
							++countOfDisagreeReportees ;
						}else{//For resolved count
							++countOfResolvedReportees ;
						}
					}
				}
			}
		
			//Disagree data add to the disagree list which are matched to the particular mgr id. 
			for (GsrDisagreeFinalRatingReporteeListBean disagreeFinalRatingObject : disagreeFinalRatingReporteeList) {
				if(integerMgrId == disagreeFinalRatingObject.getReportingMgrId()){
					disagreeFinalRatingOfReporteesList.add(disagreeFinalRatingObject);
				}
			}
			//List added to finalRatingOutputBean
			finalRatingOutputBean.setListOfDisgareeFinalRatingOfASlab(disagreeFinalRatingOfReporteesList);
		
			//Resolved data add to the resolved list which are matched to the particular mgr id. 
			for (GsrResolvedFinalRatingReporteeListBean resolvedFinalRatingObject : resolvedFinalRatingReporteeList) {
				if(integerMgrId == resolvedFinalRatingObject.getReportingMgrId()){
					resolvedFinalRatingOfReporteesList.add(resolvedFinalRatingObject);
				}
			}
			//List added to finalRatingOutputBean
			finalRatingOutputBean.setListOfResolvedFinalRatingOfASlab(resolvedFinalRatingOfReporteesList);
			
			finalRatingOutputBean.setNumberOfDisagreeUnderMgr(disagreeFinalRatingOfReporteesList.size());
			finalRatingOutputBean.setNumberOfResolvedUnderMgr(resolvedFinalRatingOfReporteesList.size());
			disagreeAndResolvedFinalRatingOutputBean.add(finalRatingOutputBean);
		}
		
		return disagreeAndResolvedFinalRatingOutputBean;
	}
	
	
	
	/**
	 * @Description This method will fetch all disagree and resolved final rating details of reportees which are falls under a skip level manager.
	 * @param GsrDisagreeAndResolvedFinalRatingUnderSkipLevelMgr bean contains the input details are to be used in the service.
	 * @return GsrDisagreeFinalRatingUnderDMOutputBean list of reportees with final rating BO.
	 * @throws CommonCustomException throws error message.
	 * 
	 */
	
	public GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean getDisagreeAndResolvedRatingOfASlabForSkipLevelMgr(
			GsrDisagreeAndResolvedFinalRatingUnderSkipLevelMgr disagreeAndResolvedFinalRatingBean)
			throws CommonCustomException 
	{
		GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean disagreeAndResolvedFinalRatingOutputBeanList = 
				new GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean();
		List<GsrDisagreeFinalRatingReporteeListBean> disagreeFinalRatingReporteeList = 
				new ArrayList<GsrDisagreeFinalRatingReporteeListBean>();
		List<GsrResolvedFinalRatingReporteeListBean> resolvedFinalRatingReporteeList = 
				new ArrayList<GsrResolvedFinalRatingReporteeListBean>();
		
		List<GsrDatEmpFinalRatingBO> disagreeAndResolvedFinalRatingBOList = new ArrayList<GsrDatEmpFinalRatingBO>();
		try {
			disagreeAndResolvedFinalRatingBOList = finalRatingRepository.
					getDisagreeAndResolvedRatingDetailsForASlabUnderSkipLevelMgr( 
						disagreeAndResolvedFinalRatingBean.getSlabId(), 
						disagreeAndResolvedFinalRatingBean.getSkipLevelMgrId());
		}catch(Exception ex){
			throw new CommonCustomException("Failed to retrieve disagree and resolved final rating for "
					+ "a slab under skip level manager. " + ex.getMessage());
		}
		
		disagreeAndResolvedFinalRatingOutputBeanList.setSlabId(disagreeAndResolvedFinalRatingBean.getSlabId());
		
		for (GsrDatEmpFinalRatingBO gsrDatEmpFinalRatingBO : disagreeAndResolvedFinalRatingBOList) 
		{
			if(gsrDatEmpFinalRatingBO.getFinalRatingStatus() == DISAGREE_FINAL_RATING_STATUS)
			{
				GsrDisagreeFinalRatingReporteeListBean disagreeFinalRatingBean = 
						new GsrDisagreeFinalRatingReporteeListBean();
				disagreeFinalRatingBean.setFinalRatingPkId(gsrDatEmpFinalRatingBO.getPkGsrFinalRatingId());
				disagreeFinalRatingBean.setReporteeId(gsrDatEmpFinalRatingBO.getFkGsrFinalRatingEmpId());
				disagreeFinalRatingBean.setReporteeName(gsrDatEmpFinalRatingBO.getEmpFirstName() + " " 
						+ gsrDatEmpFinalRatingBO.getEmpLastName());
				disagreeFinalRatingBean.setDisagreeFinalRating(gsrDatEmpFinalRatingBO.getGsrFinalRating());
				disagreeFinalRatingReporteeList.add(disagreeFinalRatingBean);
			}else{
				if(gsrDatEmpFinalRatingBO.getFinalRatingStatus() == RESOLVED_FINAL_RATING_STATUS)
				{
					GsrResolvedFinalRatingReporteeListBean resolvedFinalRatingBean =  
							new GsrResolvedFinalRatingReporteeListBean();
					resolvedFinalRatingBean.setFinalRatingId(gsrDatEmpFinalRatingBO.getPkGsrFinalRatingId());
					resolvedFinalRatingBean.setReporteeId(gsrDatEmpFinalRatingBO.getFkGsrFinalRatingEmpId());
					resolvedFinalRatingBean.setReporteeName(gsrDatEmpFinalRatingBO.getEmpFirstName() + " "
							+ gsrDatEmpFinalRatingBO.getEmpLastName());
					resolvedFinalRatingBean.setResolvedFinalRating(gsrDatEmpFinalRatingBO.getGsrFinalRating());
					resolvedFinalRatingReporteeList.add(resolvedFinalRatingBean);
				}
			}
		}
		
		disagreeAndResolvedFinalRatingOutputBeanList.setListOfDisgareeFinalRatingOfASlab(
				disagreeFinalRatingReporteeList);
		disagreeAndResolvedFinalRatingOutputBeanList.setListOfResolvedFinalRatingOfASlab(
				resolvedFinalRatingReporteeList);
		
		return disagreeAndResolvedFinalRatingOutputBeanList;
	}
	
	//End of addition by Shyam Lama
}
