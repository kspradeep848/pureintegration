package com.thbs.mis.gsr.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bean.EmployeeNameAndIdWithHierarchyFlagBean;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.BusinessUnitRepository;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bean.GsrHierarchyBean;

@Service
public class GSRHierarchyViewService {
	
	/**
	 * Instance of <code>Log</code>
	 */
	@SuppressWarnings("unused")
	private static final AppLog LOG = LogFactory.getLog(GSRHierarchyViewService.class);
	
	public GSRHierarchyViewService() {
		super();
	}

	@Autowired
	private DatEmployeeProfessionalRepository datEmpPersonalDetailRepository;
	@Autowired
	private EmpDetailRepository empDetailRepository;
	@Autowired
	private BusinessUnitRepository buRepos;
	
	private final String YES_HAS_REPORTEES = "YES_HAS_REPORTEES";  
	
	private final Short BU_HEAD_ROLE_ID = 6;
	private final Short FINANCE_HEAD_ROLE_ID = 12;
	private final Short CHIEF_EXECUTIVE_OFFICER_ROLE_ID = 26;
	
	@Value("${employee.not.include}")
	private String empIds;
	
	
	
	/**
	 * @Description This method will fetch a GSR hierarchy structure of provided employee id. 
	 * @param Integer employeeId.
	 * @return GsrHierarchyBean.
	 * @throws DataAccessException throws when the data access exception occurs.
	 * 
	 */
	public GsrHierarchyBean getGsrHierarchyStructureOfGivenEmp(Integer employeeId) throws DataAccessException 
	{
		GsrHierarchyBean outputBean = new GsrHierarchyBean();
		List<EmployeeNameAndIdWithHierarchyFlagBean> hierarchyList = new ArrayList<EmployeeNameAndIdWithHierarchyFlagBean>();
		
		//Get the employee object with all details information
		DatEmpProfessionalDetailBO empProfessionalInfo = new DatEmpProfessionalDetailBO();
		empProfessionalInfo = datEmpPersonalDetailRepository.getEmployeeProfessionalDetail(employeeId);
		
		//Set the BU Name & id
		outputBean.setBuId(empProfessionalInfo.getFkEmpBuUnit());
		outputBean.setBuName(empProfessionalInfo.getBusinessUnitName());
		
		if(empProfessionalInfo != null)
		{	
			int hierarchyLevel = 1;
			//Given employee's own details
			EmployeeNameAndIdWithHierarchyFlagBean givenEmpObject = new EmployeeNameAndIdWithHierarchyFlagBean();
			givenEmpObject.setEmployeeId(empProfessionalInfo.getFkMainEmpDetailId());
			givenEmpObject.setEmpFirstName(empProfessionalInfo.getEmpFirstName());
			givenEmpObject.setEmpLastName(empProfessionalInfo.getEmpLastName());
			givenEmpObject.setEmpFullNameWithId(empProfessionalInfo.getEmpFirstName() + " " 
					+ empProfessionalInfo.getEmpLastName() + "-" + empProfessionalInfo.getFkMainEmpDetailId());
			givenEmpObject.setManagerId(empProfessionalInfo.getFkEmpReportingMgrId());
			
			//Find out he/she have any reportees or not, if yes then make a list all reportees details
			List<EmployeeNameAndIdWithHierarchyFlagBean> directReporteesList = new ArrayList<EmployeeNameAndIdWithHierarchyFlagBean>();
			if(empProfessionalInfo.getHasReporteesFlag().equalsIgnoreCase(YES_HAS_REPORTEES))
			{
				List<DatEmpDetailBO> reporteesDetailList = new ArrayList<DatEmpDetailBO>();
				List<Integer> empIDList = new ArrayList<Integer>();
				String[] empIdArray = empIds.split(",");
				for (String s : empIdArray)
				empIDList.add(Integer.parseInt(s));
				reporteesDetailList = empDetailRepository.getAllDirectReporteesWithRMIdForRMId(empProfessionalInfo.getFkMainEmpDetailId(),empIDList);
				
				for (DatEmpDetailBO datEmpDetailBO : reporteesDetailList) {
					EmployeeNameAndIdWithHierarchyFlagBean employeesBean = new EmployeeNameAndIdWithHierarchyFlagBean();
					employeesBean.setEmployeeId(datEmpDetailBO.getPkEmpId());
					employeesBean.setEmpFirstName(datEmpDetailBO.getEmpFirstName());
					employeesBean.setEmpLastName(datEmpDetailBO.getEmpLastName());
					employeesBean.setEmpFullNameWithId(datEmpDetailBO.getEmpFirstName() + " " + datEmpDetailBO.getEmpLastName() 
							+ "-" + datEmpDetailBO.getPkEmpId());
					employeesBean.setManagerId(datEmpDetailBO.getEmpRMId());
					employeesBean.setLastHierarchyOrderFlag(true);
					directReporteesList.add(employeesBean);
				}
				
				givenEmpObject.setLastHierarchyOrderFlag(false);
				++hierarchyLevel;
			}//If given employee do not have any reportees
			else{
				givenEmpObject.setLastHierarchyOrderFlag(true);
			}
			
			//Find out his/her reporting manager details till his/her manager's reporting manager is 1 "Ravi Chander" 
			//or his/her manager's reporting manager's role is BU Head.
			if(empProfessionalInfo.getEmpRoleId() != CHIEF_EXECUTIVE_OFFICER_ROLE_ID && 
					empProfessionalInfo.getEmpRoleId() != BU_HEAD_ROLE_ID && 
					empProfessionalInfo.getEmpRoleId() != FINANCE_HEAD_ROLE_ID )
			{
				//Creating a TreeMap for collection of hierarchy order of reporting manager of given employee 
				TreeMap<Integer, DatEmpProfessionalDetailBO> mgrTreeMap = new TreeMap<Integer, DatEmpProfessionalDetailBO>(); 
				int reportingmgrId = 0;
				for (int index = 1; index >0; index ++) 
				{
					if(index == 1){
						reportingmgrId = empProfessionalInfo.getFkEmpReportingMgrId();
					}
					
					//Find out his/her reporting managers details from empProfessionalInfo Object
					DatEmpProfessionalDetailBO employeeProfDetail = new DatEmpProfessionalDetailBO();
					employeeProfDetail = datEmpPersonalDetailRepository.getEmployeeProfessionalDetail(reportingmgrId);
					
					//Added manager detail into TreeMap
					mgrTreeMap.put(index, employeeProfDetail);
					
					//Check the condition if the employe is BU head or finance head then come out from the for loop
					if(employeeProfDetail.getEmpRoleId() == FINANCE_HEAD_ROLE_ID ||  
							employeeProfDetail.getEmpRoleId() == BU_HEAD_ROLE_ID){
						break;
					}else{
						reportingmgrId = employeeProfDetail.getFkEmpReportingMgrId();
					}
				}
				
				//Order has been converted into descending way
				Map<Integer, DatEmpProfessionalDetailBO> newMap = new TreeMap<Integer, DatEmpProfessionalDetailBO>(Collections.reverseOrder());
				newMap.putAll(mgrTreeMap);
				
				//Transferring From  DatEmpProfessionalDetailBO to EmployeeNameAndIdWithReporteeDetailBean
				for (Map.Entry<Integer, DatEmpProfessionalDetailBO> treeMapEntry : newMap.entrySet())
				{
					EmployeeNameAndIdWithHierarchyFlagBean employeesBeanObject = new EmployeeNameAndIdWithHierarchyFlagBean();
					employeesBeanObject.setEmployeeId(((DatEmpProfessionalDetailBO) treeMapEntry.getValue()).getFkMainEmpDetailId());
					employeesBeanObject.setEmpFirstName(((DatEmpProfessionalDetailBO) treeMapEntry.getValue()).getEmpFirstName());
					employeesBeanObject.setEmpLastName(((DatEmpProfessionalDetailBO) treeMapEntry.getValue()).getEmpLastName());
					employeesBeanObject.setEmpFullNameWithId(((DatEmpProfessionalDetailBO) 
							treeMapEntry.getValue()).getEmpFirstName() + " " 
							+ ((DatEmpProfessionalDetailBO) treeMapEntry.getValue()).getEmpLastName() + "-" 
							+ ((DatEmpProfessionalDetailBO) treeMapEntry.getValue()).getFkMainEmpDetailId());
					employeesBeanObject.setManagerId(((DatEmpProfessionalDetailBO) treeMapEntry.getValue()).getFkEmpReportingMgrId());
					employeesBeanObject.setLastHierarchyOrderFlag(false);
					hierarchyList.add(employeesBeanObject);
					++hierarchyLevel;
				}//End of for loop
				
			}//End of if statement
			//else{//Employee is CEO
			outputBean.setHierarchyLevelCounter(hierarchyLevel);
			hierarchyList.add(givenEmpObject);
			hierarchyList.addAll(directReporteesList);
			//}
		}//End of if(empProfessionalInfo != null)
		outputBean.setEmpHierarchyList(hierarchyList);
		return outputBean;
	}
	
	
}
