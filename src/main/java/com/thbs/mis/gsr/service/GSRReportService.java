package com.thbs.mis.gsr.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import jxl.write.WriteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bean.EmployeesBean;
import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.report.Util.DownloadFileUtil;
import com.thbs.mis.framework.report.excel.ReadExcelConfigTeampleUtil;
import com.thbs.mis.framework.report.excel.WriteExcel;
import com.thbs.mis.framework.report.excel.bean.ExcelAllSheetData;
import com.thbs.mis.framework.report.excel.bean.ReportTemplateConfigureBean;
import com.thbs.mis.gsr.bean.GSRReportGenerateBean;
import com.thbs.mis.gsr.bean.GsrGetAllReportingManagersBean;
import com.thbs.mis.gsr.bean.GsrReportFilterInputBean;
import com.thbs.mis.gsr.bean.GsrReportInitContentBean;
import com.thbs.mis.gsr.bean.GsrReportInputBean;
@Service
public class GSRReportService {
	private static final AppLog LOG = LogFactory.getLog(GSRReportService.class);
	@PersistenceContext
    private EntityManager entityManager;
	int CONSTANT_FILTER_EMPLOYEE_ID=1, CONSTANT_FILTER_EMPLOYEE_NAME=2,
		CONSTANT_FILTER_REPORTING_MANAGER_ID=3,
		CONSTANT_FILTER_REPORTING_MANAGER_NAME=4,
		CONSTANT_FILTER_ALL_SLABS=5,
		CONSTANT_FINAL_RATING_STATUS=6,
		CONSTANT_GOAL_NAME=7,
		CONSTANT_GOAL_WEIGHTAGE=8;
		 
	
	public List<?> getCustomQueryData(GsrReportInputBean inputBean) {
		try {
			List  outputGoalList = new ArrayList();
				 Query query = entityManager.createNativeQuery(inputBean.getQuery());
				 return	 outputGoalList =   query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Autowired
	GsrCommonService gsrCommonService;
	@Autowired
	CommonService commonService;
	@Autowired
	GSRGoalService gsrGoalService;
	public List<GsrReportInitContentBean> getReportInitContentData(GsrReportFilterInputBean inputBean) throws DataAccessException, CommonCustomException {
		List<GsrReportInitContentBean> output = new ArrayList<GsrReportInitContentBean>();
		//By Hr
		 if(inputBean.getFilterIds() !=null && inputBean.getFilterIds().length>0){
			 GsrReportInitContentBean gsrReportInitContentBean = new GsrReportInitContentBean();
			 String[] filterIds = inputBean.getFilterIds();
			 for (int i = 0; i < filterIds.length; i++) {
				if(Integer.parseInt(filterIds[i]) == CONSTANT_FILTER_EMPLOYEE_NAME || Integer.parseInt(filterIds[i]) == CONSTANT_FILTER_EMPLOYEE_ID){
					if(inputBean.getRoleName().equalsIgnoreCase("HRD_SENIOR_EXECUTIVE")){
						List<EmployeesBean> allEmployee = commonService.getAllActiveEmployeeList();
						if(allEmployee.size()>0){
							Collections.sort(allEmployee, new Comparator<EmployeesBean>() {
							@Override
							public int compare(EmployeesBean lhs, EmployeesBean rhs) {
							return lhs.getEmployeeFirstName().compareTo(rhs.getEmployeeFirstName());
							}
							});
						gsrReportInitContentBean.setAllEmployeesList(allEmployee);
						}
					}
				}else if(Integer.parseInt(filterIds[i]) == CONSTANT_FILTER_REPORTING_MANAGER_NAME
						||Integer.parseInt(filterIds[i]) == CONSTANT_FILTER_REPORTING_MANAGER_ID){
					if(inputBean.getRoleName().equalsIgnoreCase("HRD_SENIOR_EXECUTIVE")){
						List<GsrGetAllReportingManagersBean> allReportingManager = gsrCommonService.getAllReportingManagers();
						if(allReportingManager.size()>0){
							Collections.sort(allReportingManager, new Comparator<GsrGetAllReportingManagersBean>() {
							@Override
							public int compare(GsrGetAllReportingManagersBean lhs, GsrGetAllReportingManagersBean rhs) {
							return lhs.getEmpName().compareTo(rhs.getEmpName ());
							}
							});
						gsrReportInitContentBean.setGetAllReportingManagerList(allReportingManager);
						}
					}
					}else if(Integer.parseInt(filterIds[i]) == CONSTANT_FILTER_ALL_SLABS){
						gsrReportInitContentBean.setGetAllSlabs(gsrCommonService.getAllgsrSlabs());
					}else if(Integer.parseInt(filterIds[i]) == CONSTANT_FINAL_RATING_STATUS){
					
					}else if(Integer.parseInt(filterIds[i]) == CONSTANT_GOAL_NAME){
						
					}else if(Integer.parseInt(filterIds[i]) == CONSTANT_GOAL_WEIGHTAGE){
						
					}
				}
			 output.add(gsrReportInitContentBean);
			}
		return output;
	}
	@Autowired
	private ServletContext servletContext;
	public boolean  generateReport(GSRReportGenerateBean inputBean,HttpServletResponse response) throws WriteException, IOException, IllegalArgumentException, IllegalAccessException, CommonCustomException {
		boolean result = true;
		System.out.println(inputBean.toString());
		System.out.println("base path  generateReport :"+servletContext
						.getRealPath("/WEB-INF/classes/com/thbs/mis/framework/report/template/"));
		String BASEPATH = servletContext
				.getRealPath("/WEB-INF/classes/com/thbs/mis/framework/report/template/");
	 WriteExcel test = new WriteExcel();
     test.setOutputFile(BASEPATH+"gsrReport.xls");
      result = test.write(inputBean.getHeaders(), inputBean.getReportData());
     // System.out.println("result "+result);
      if(result==true){
    	  DownloadFileUtil.downloadFile(response, BASEPATH, "gsrReport.xls");
      }else
    	  System.out.println("failed");
		return  result;
	}
	public List<ReportTemplateConfigureBean>  getExcelFilterConfigData(Integer empId) throws IOException, CommonCustomException {
		List<ExcelAllSheetData> data = new ArrayList<ExcelAllSheetData>();
		List<ReportTemplateConfigureBean> output = new ArrayList<ReportTemplateConfigureBean>();
		System.out.println("base path  getExcelFilterConfigData:"+servletContext
				.getRealPath("/WEB-INF/classes/com/thbs/mis/framework/report/template/"));
		String BASEPATH = servletContext
				.getRealPath("/WEB-INF/classes/com/thbs/mis/framework/report/template/");
		ReportTemplateConfigureBean  reportTemplateConfigureBean=ReadExcelConfigTeampleUtil.getExcelTemplateConfigdata(BASEPATH, DownloadFileUtil.GSR_REPORT_CONFIG_TEMPLATE);
		if(reportTemplateConfigureBean!=null)
		output.add(reportTemplateConfigureBean);
		return output;
	}
}
