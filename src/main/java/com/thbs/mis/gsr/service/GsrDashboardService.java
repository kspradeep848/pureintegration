/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrDashboardService.java           			     */
/*                                                                   */
/*  Author      :  Anil kumar B K	                                 */
/*                                                                   */
/*  Date        :  24-Nov-2016                                       */
/*                                                                   */
/*  Description :  Serice class for GSR dash board    			     */
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 24-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bean.GsrDisagreeBean;
import com.thbs.mis.gsr.bean.GsrDisagreeRatingDetailBean;
import com.thbs.mis.gsr.bean.GsrDisagreeRatingForPrvsSlabsBean;
import com.thbs.mis.gsr.bean.GsrEmployeeBean;
import com.thbs.mis.gsr.bean.GsrEmployeeDashBoardBean;
import com.thbs.mis.gsr.bean.GsrEmployeePerformanceRatingBean;
import com.thbs.mis.gsr.bean.GsrEmployeesAvgRatingBean;
import com.thbs.mis.gsr.bean.GsrEmployeesRatingBean;
import com.thbs.mis.gsr.bean.GsrEmployeesRatingForHrBean;
import com.thbs.mis.gsr.bean.GsrReporteesBean;
import com.thbs.mis.gsr.bean.GsrReporteesDashBoardBean;
import com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO;
import com.thbs.mis.gsr.bo.GsrSlabBO;
import com.thbs.mis.gsr.bo.GsrWindowBO;
import com.thbs.mis.gsr.constants.GsrConstants;
import com.thbs.mis.gsr.dao.GSREmployeegoalsRepository;
import com.thbs.mis.gsr.dao.GsrRatingRepository;
import com.thbs.mis.gsr.dao.GsrRatingSpecification;
import com.thbs.mis.gsr.dao.GsrSlabRepository;
import com.thbs.mis.gsr.dao.GsrWindowRepository;
import com.thbs.mis.gsr.dao.GsrWindowSpecification;

@Service
public class GsrDashboardService {

	public enum WorkFlowName {

		GOALS_CREATION("Goals Creation"), ENTER_ACCOMPLISHMENTS(
				"Enter Accomplishments"), PROVIDE_FEEDBACK("Provide Feedback"), GOALS_CLOSURE(
				"Goals Closure"), ATTENDEE_FEEDBACK("Attendee Feedback"), POST_APPLICATION_FEEDBACK(
				"Post Application Feedback");
		private String value;

		WorkFlowName(String value) {
			this.value = value;
		}

	}

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(GsrDashboardService.class);

	@Autowired
	private GsrRatingRepository ratingRepository;

	@Autowired
	private DatEmployeeProfessionalRepository professionalRepository;

	@Autowired
	private GsrSlabRepository gsrSlabRepository;

	/**
	 * Instance of <code>DatEmpPersonalDetailsRepository</code>
	 */
	@Autowired
	private EmployeePersonalDetailsRepository personalRepository;

	@Autowired
	private GSREmployeegoalsRepository gsrEmployeeGoalsRepository;

	@Autowired
	private GsrWindowRepository gsrWindowRepository;

	List<GsrDatEmpFinalRatingBO> rating = new ArrayList<GsrDatEmpFinalRatingBO>();

	public GsrEmployeeBean getEmployeePerformance(Integer empId)
			throws DataAccessException {

		LOG.startUsecase("getEmployeePerformance");
		GsrEmployeeBean finalResult = new GsrEmployeeBean();
		List<GsrEmployeePerformanceRatingBean> result = new ArrayList<GsrEmployeePerformanceRatingBean>();
		GsrEmployeePerformanceRatingBean rating = null;
		GsrEmployeeDashBoardBean gsrEmployeeDashBoardBean = new GsrEmployeeDashBoardBean();

		try {

			Specification<GsrSlabBO> specSlab = GsrRatingSpecification
					.getCurrentSlab();
			GsrSlabBO gsrCurrentSlabBo = gsrSlabRepository.findOne(specSlab);
			DatEmpProfessionalDetailBO professionalBO = professionalRepository
					.findByFkMainEmpDetailId(empId);

			DatEmpPersonalDetailBO personalBO = personalRepository
					.findByFkEmpDetailId(professionalBO
							.getFkEmpReportingMgrId());

			Short currentSlab = gsrCurrentSlabBo.getPkGsrSlabId();

			List<GsrDatEmpFinalRatingBO> currentSlabRating = ratingRepository
					.findByFkGsrFinalRatingEmpIdAndFkGsrFinalRatingSlabId(
							empId, currentSlab);

			Integer countOfGoalsPerSlab = gsrEmployeeGoalsRepository
					.countByFkGsrSlabIdAndFkGsrDatEmpIdAndFkGsrGoalStatusNotIn(currentSlab, empId, GsrConstants.GSR_REJECTED);
		
			GsrWindowSpecification gsrWindowSpec = new GsrWindowSpecification();
			Specification<GsrWindowBO> windowSpec = gsrWindowSpec
					.getGsrWindowEndDate(currentSlab);
			List<GsrWindowBO> gsrWindowData = gsrWindowRepository
					.findAll(windowSpec);

			for (GsrWindowBO gsrWindow : gsrWindowData) {
				gsrEmployeeDashBoardBean.setGsrWindowEndDate(gsrWindow
						.getGsrWindowEndDate());

				for (WorkFlowName name : WorkFlowName.values()) {

					if (name.toString().equals(
							gsrWindow.getMasWorkFlowBO().getWorkFlowKeyName())) {

						gsrEmployeeDashBoardBean.setWorkFlowName(name.value);
					}
				}

			}
			if (currentSlabRating.size() == 0) {
				currentSlab--;
			}

			Short[] slabs = { currentSlab--, currentSlab--, currentSlab--,
					currentSlab-- };
			List<Short> listOfSlab = new ArrayList<Short>();
			for (int i = slabs.length - 1; i >= 0; i--) {
				listOfSlab.add(slabs[i]);
			}

			List<Short> listOfSlabs = Arrays.asList(slabs);

			GsrRatingSpecification specification = new GsrRatingSpecification();
			Specification<GsrDatEmpFinalRatingBO> spec = specification
					.getEmployeesPerformanceRating(empId, listOfSlabs);
			List<GsrDatEmpFinalRatingBO> data = ratingRepository.findAll(spec);

			for (Short slab : listOfSlabs) {
				rating = new GsrEmployeePerformanceRatingBean();
				for (GsrDatEmpFinalRatingBO bo : data) {

					if (slab == bo.getGsrSlabBO_slab_id().getPkGsrSlabId()) {

						rating.setRating(bo.getGsrFinalRating());

					}
				}
				GsrSlabBO gsrCurrentSlabDetails = gsrSlabRepository
						.findByPkGsrSlabId(slab);

				if (rating.getRating() == null) {
					rating.setRating(0f);
				}
				rating.setSlabId(slab);
				rating.setSlabName(gsrCurrentSlabDetails.getGsrSlabName());
				rating.setSlab(gsrCurrentSlabDetails.getGsrSlabShortName());
				rating.setSlabYear(Short.parseShort(gsrCurrentSlabDetails
						.getGsrSlabYear()));
				result.add(rating);
			}

			gsrEmployeeDashBoardBean.setMgrId(professionalBO
					.getFkEmpReportingMgrId());
			gsrEmployeeDashBoardBean.setMgrName(personalBO.getEmpFirstName()
					+ " " + personalBO.getEmpLastName());
			gsrEmployeeDashBoardBean
					.setTotalGoalSetForCurrentSlab(countOfGoalsPerSlab);

			finalResult.setEmpWorkFlowDetails(gsrEmployeeDashBoardBean);
			finalResult.setEmpRatings(result);

			Collections.sort(result);
		} catch (Exception e) {

			throw new DataAccessException(
					"Exception occured at getEmployeePerformance ", e);
		}

		LOG.endUsecase("getEmployeePerformance :");
		return finalResult;

	}

	public GsrReporteesBean getEmployeesRatingCountByRmId(Integer rmId)
			throws DataAccessException {

		LOG.startUsecase("getEmployeesRatingCountByRmId");
		GsrEmployeesRatingBean gsrEmployeesRatingBean;
		GsrReporteesBean finalRatingDetails = new GsrReporteesBean();
		GsrReporteesDashBoardBean reporteesDashBoardBean = new GsrReporteesDashBoardBean();

		List<DatEmpProfessionalDetailBO> proDetails = new ArrayList<DatEmpProfessionalDetailBO>();
		List<Integer> allReportees = new ArrayList<Integer>();

		List<GsrEmployeesRatingBean> listOfEmp = new ArrayList<GsrEmployeesRatingBean>();
		try {

			proDetails = professionalRepository.findByFkEmpReportingMgrId(rmId);

			if (proDetails.size() == 0) {

				throw new CommonCustomException("No reportees for given mgr Id");
			} else {

				for (DatEmpProfessionalDetailBO pro : proDetails) {
					allReportees.add(pro.getFkMainEmpDetailId());
				}
			}

			Specification<GsrSlabBO> specSlab = GsrRatingSpecification
					.getCurrentSlab();
			GsrSlabBO gsrCurrentSlabBo = gsrSlabRepository.findOne(specSlab);

			Short currentSlab = gsrCurrentSlabBo.getPkGsrSlabId();

			GsrWindowSpecification gsrWindowSpec = new GsrWindowSpecification();

			Specification<GsrWindowBO> windowSpec = gsrWindowSpec
					.getGsrWindowEndDate(currentSlab);
			List<GsrWindowBO> gsrWindowData = gsrWindowRepository
					.findAll(windowSpec);

			Integer countOfGoalsForCurrentSlab = gsrEmployeeGoalsRepository
					.countByFkGsrSlabIdAndFkGsrDatMgrIdAndFkGsrGoalStatusNotIn(currentSlab,
							rmId ,GsrConstants.GSR_REJECTED);

			reporteesDashBoardBean.setNoOfReportees(allReportees.size());
			reporteesDashBoardBean
					.setNoOfgoalSetForCurrentSlab(countOfGoalsForCurrentSlab);
			for (GsrWindowBO gsrWindow : gsrWindowData) {
				reporteesDashBoardBean.setGsrWindowEndDate(gsrWindow
						.getGsrWindowEndDate());

				for (WorkFlowName name : WorkFlowName.values()) {

					if (name.toString().equals(
							gsrWindow.getMasWorkFlowBO().getWorkFlowKeyName())) {

						reporteesDashBoardBean.setWorkFlowName(name.value);
					}
				}

			}

			Short[] slabs = { --currentSlab, --currentSlab, --currentSlab,
					--currentSlab };

			List<Short> listOfSlabs = Arrays.asList(slabs);
			List<GsrDatEmpFinalRatingBO> data = ratingRepository
					.getAllEmpRatingsForRm(allReportees, listOfSlabs);

			for (Short slab : listOfSlabs) {
				int belowTwo = 0, underThree = 0, underFour = 0, underFive = 0;
				gsrEmployeesRatingBean = new GsrEmployeesRatingBean();

				for (GsrDatEmpFinalRatingBO ratingBO : data) {

					if (slab == ratingBO.getGsrSlabBO_slab_id()
							.getPkGsrSlabId()) {

						if (ratingBO.getGsrFinalRating() < 2.0) {
							belowTwo++;
						} else if (ratingBO.getGsrFinalRating() >= 2.0
								&& ratingBO.getGsrFinalRating() <= 2.9) {
							underThree++;
						} else if (ratingBO.getGsrFinalRating() >= 3.0
								&& ratingBO.getGsrFinalRating() <= 3.9) {
							underFour++;

						} else if (ratingBO.getGsrFinalRating() >= 4.0
								&& ratingBO.getGsrFinalRating() <= 5.0) {
							underFive++;
						}

					}

				}
				GsrSlabBO gsrCurrentSlabDetails = gsrSlabRepository
						.findByPkGsrSlabId(slab);
				gsrEmployeesRatingBean.setSlabName(gsrCurrentSlabDetails
						.getGsrSlabName());
				gsrEmployeesRatingBean.setSlabYear(gsrCurrentSlabDetails
						.getGsrSlabYear());
				gsrEmployeesRatingBean.setSlabShortName(gsrCurrentSlabDetails
						.getGsrSlabShortName());
				gsrEmployeesRatingBean.setSlabId(slab);
				gsrEmployeesRatingBean.setCountOfBelowTwo(belowTwo);
				gsrEmployeesRatingBean.setCountOfRangeTwoToThree(underThree);
				gsrEmployeesRatingBean.setCountOfRangeThreeToFour(underFour);
				gsrEmployeesRatingBean.setCountOfRangeFourToFive(underFive);

				listOfEmp.add(gsrEmployeesRatingBean);

			}
			Collections.sort(listOfEmp);
			finalRatingDetails
					.setReporteesWorkFlowDetails(reporteesDashBoardBean);
			finalRatingDetails.setEmpRatings(listOfEmp);

		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}

		LOG.endUsecase("getEmployeesRatingCountByRmId :");
		return finalRatingDetails;

	}

	public GsrReporteesBean getEmployeesRatingCountByDmId(Integer dmId)
			throws DataAccessException {

		LOG.startUsecase("getEmployeesRatingCountByDmId");
		GsrReporteesBean finalRatingDetails = new GsrReporteesBean();
		GsrReporteesDashBoardBean reporteesDashBoardBean = new GsrReporteesDashBoardBean();
		GsrEmployeesRatingBean gsrEmployeesRatingBean;

		List<DatEmpProfessionalDetailBO> proDetails = new ArrayList<DatEmpProfessionalDetailBO>();
		List<Integer> allReporteesForDM = new CopyOnWriteArrayList<Integer>();

		List<GsrEmployeesRatingBean> listOfEmp = new ArrayList<GsrEmployeesRatingBean>();
		try {

			proDetails = professionalRepository.findByFkEmpDomainMgrId(dmId);

			if (proDetails.size() == 0) {

				throw new CommonCustomException(
						"No reportees for given Domain mgr Id");
			} else {

				for (DatEmpProfessionalDetailBO pro : proDetails) {
					allReporteesForDM.add(pro.getFkMainEmpDetailId());
				}
			}

			Specification<GsrSlabBO> specSlab = GsrRatingSpecification
					.getCurrentSlab();
			GsrSlabBO gsrCurrentSlabBo = gsrSlabRepository.findOne(specSlab);

			Short currentSlab = gsrCurrentSlabBo.getPkGsrSlabId();

			GsrWindowSpecification gsrWindowSpec = new GsrWindowSpecification();
			Specification<GsrWindowBO> windowSpec = gsrWindowSpec
					.getGsrWindowEndDate(currentSlab);
			List<GsrWindowBO> gsrWindowData = gsrWindowRepository
					.findAll(windowSpec);

			Integer countOfGoalsForCurrentSlab = gsrEmployeeGoalsRepository
			.countByFkGsrSlabIdAndFkEmpGoalDomainMgrIdAndFkGsrGoalStatusNotIn(currentSlab,
							dmId, GsrConstants.GSR_REJECTED);
			reporteesDashBoardBean.setNoOfReportees(allReporteesForDM.size());
			reporteesDashBoardBean
					.setNoOfgoalSetForCurrentSlab(countOfGoalsForCurrentSlab);

			for (GsrWindowBO gsrWindow : gsrWindowData) {
				reporteesDashBoardBean.setGsrWindowEndDate(gsrWindow
						.getGsrWindowEndDate());

				for (WorkFlowName name : WorkFlowName.values()) {

					if (name.toString().equals(
							gsrWindow.getMasWorkFlowBO().getWorkFlowKeyName())) {

						reporteesDashBoardBean.setWorkFlowName(name.value);
					}
				}

			}

			Short[] slabs = { --currentSlab, --currentSlab, --currentSlab,
					--currentSlab };

			List<Short> listOfSlabs = Arrays.asList(slabs);
			List<GsrDatEmpFinalRatingBO> data = ratingRepository
					.getAllEmpRatingsForRm(allReporteesForDM, listOfSlabs);

			for (Short slab : listOfSlabs) {
				int belowTwo = 0, underThree = 0, underFour = 0, underFive = 0;
				gsrEmployeesRatingBean = new GsrEmployeesRatingBean();

				for (GsrDatEmpFinalRatingBO ratingBO : data) {
					if (slab == ratingBO.getGsrSlabBO_slab_id()
							.getPkGsrSlabId()) {

						if (ratingBO.getGsrFinalRating() < 2.0) {
							belowTwo++;

						} else if (ratingBO.getGsrFinalRating() >= 2.0
								&& ratingBO.getGsrFinalRating() <= 2.9) {
							underThree++;
						} else if (ratingBO.getGsrFinalRating() >= 3.0
								&& ratingBO.getGsrFinalRating() <= 3.9) {

							underFour++;
						} else if (ratingBO.getGsrFinalRating() >= 4.0
								&& ratingBO.getGsrFinalRating() <= 5.0) {
							underFive++;
						}

					}
				}
				GsrSlabBO gsrCurrentSlabDetails = gsrSlabRepository
						.findByPkGsrSlabId(slab);

				gsrEmployeesRatingBean.setSlabName(gsrCurrentSlabDetails
						.getGsrSlabName());
				gsrEmployeesRatingBean.setSlabYear(gsrCurrentSlabDetails
						.getGsrSlabYear());
				gsrEmployeesRatingBean.setSlabShortName(gsrCurrentSlabDetails
						.getGsrSlabShortName());
				gsrEmployeesRatingBean.setSlabId(slab);
				gsrEmployeesRatingBean.setCountOfBelowTwo(belowTwo);
				gsrEmployeesRatingBean.setCountOfRangeTwoToThree(underThree);
				gsrEmployeesRatingBean.setCountOfRangeThreeToFour(underFour);
				gsrEmployeesRatingBean.setCountOfRangeFourToFive(underFive);
				listOfEmp.add(gsrEmployeesRatingBean);
			}
			Collections.sort(listOfEmp);

			finalRatingDetails
					.setReporteesWorkFlowDetails(reporteesDashBoardBean);
			finalRatingDetails.setEmpRatings(listOfEmp);
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());

		}

		LOG.endUsecase("getEmployeesRatingCountByDmId :");
		return finalRatingDetails;

	}

	public GsrReporteesBean getEmployeesRatingCountByBuId(Short buId)
			throws DataAccessException {

		GsrEmployeesRatingBean gsrEmployeesRatingBean;
		LOG.startUsecase("getEmployeesRatingCountByBuId");
		GsrReporteesBean finalRatingDetails = new GsrReporteesBean();
		GsrReporteesDashBoardBean reporteesDashBoardBean = new GsrReporteesDashBoardBean();

		List<DatEmpProfessionalDetailBO> proDetails = new ArrayList<DatEmpProfessionalDetailBO>();
		List<Integer> allReporteesForBu = new CopyOnWriteArrayList<Integer>();

		List<GsrEmployeesRatingBean> listOfEmp = new ArrayList<GsrEmployeesRatingBean>();
		try {

			proDetails = professionalRepository.findByFkEmpBuUnit(buId);

			if (proDetails.size() == 0) {
				throw new CommonCustomException(
						"No reportees for given Business unit Id");
			} else {
				for (DatEmpProfessionalDetailBO pro : proDetails) {
					allReporteesForBu.add(pro.getFkMainEmpDetailId());
				}
			}

			Specification<GsrSlabBO> specSlab = GsrRatingSpecification
					.getCurrentSlab();
			GsrSlabBO gsrCurrentSlabBo = gsrSlabRepository.findOne(specSlab);

			Short currentSlab = gsrCurrentSlabBo.getPkGsrSlabId();

			GsrWindowSpecification gsrWindowSpec = new GsrWindowSpecification();
			Specification<GsrWindowBO> windowSpec = gsrWindowSpec
					.getGsrWindowEndDate(currentSlab);
			List<GsrWindowBO> gsrWindowData = gsrWindowRepository
					.findAll(windowSpec);

			Integer countOfGoalsForCurrentSlab = gsrEmployeeGoalsRepository
					.countByFkGsrSlabIdAndFkEmpGoalBuIdAndFkGsrGoalStatusNotIn(currentSlab,
							buId, GsrConstants.GSR_REJECTED);
			LOG.info("countOfGoalsForCurrentSlab : "+countOfGoalsForCurrentSlab);
			LOG.info("currentSlab : "+currentSlab);
			LOG.info("buId : "+buId);
			
			reporteesDashBoardBean.setNoOfReportees(allReporteesForBu.size());
			reporteesDashBoardBean
					.setNoOfgoalSetForCurrentSlab(countOfGoalsForCurrentSlab);

			for (GsrWindowBO gsrWindow : gsrWindowData) {
				reporteesDashBoardBean.setGsrWindowEndDate(gsrWindow
						.getGsrWindowEndDate());

				for (WorkFlowName name : WorkFlowName.values()) {

					if (name.toString().equals(
							gsrWindow.getMasWorkFlowBO().getWorkFlowKeyName())) {

						reporteesDashBoardBean.setWorkFlowName(name.value);
					}
				}

			}

			Short[] slabs = { --currentSlab, --currentSlab, --currentSlab,
					--currentSlab };

			List<Short> listOfSlabs = Arrays.asList(slabs);
			List<GsrDatEmpFinalRatingBO> data = ratingRepository
					.getAllEmpRatingsForRm(allReporteesForBu, listOfSlabs);

			for (Short slab : listOfSlabs) {
				int belowTwo = 0, underThree = 0, underFour = 0, underFive = 0;
				gsrEmployeesRatingBean = new GsrEmployeesRatingBean();

				for (GsrDatEmpFinalRatingBO ratingBO : data) {

					if (slab == ratingBO.getGsrSlabBO_slab_id()
							.getPkGsrSlabId()) {

						if (ratingBO.getGsrFinalRating() < 2) {
							belowTwo++;

						} else if (ratingBO.getGsrFinalRating() >= 2.0
								&& ratingBO.getGsrFinalRating() <= 2.9) {
							underThree++;
						} else if (ratingBO.getGsrFinalRating() >= 3.0
								&& ratingBO.getGsrFinalRating() <= 3.9) {

							underFour++;
						} else if (ratingBO.getGsrFinalRating() >= 4.0
								&& ratingBO.getGsrFinalRating() <= 5.0) {
							underFive++;
						}

					}

				}
				GsrSlabBO gsrCurrentSlabDetails = gsrSlabRepository
						.findByPkGsrSlabId(slab);

				gsrEmployeesRatingBean.setSlabName(gsrCurrentSlabDetails
						.getGsrSlabName());
				gsrEmployeesRatingBean.setSlabYear(gsrCurrentSlabDetails
						.getGsrSlabYear());
				gsrEmployeesRatingBean.setSlabShortName(gsrCurrentSlabDetails
						.getGsrSlabShortName());
				gsrEmployeesRatingBean.setSlabId(slab);
				gsrEmployeesRatingBean.setCountOfBelowTwo(belowTwo);
				gsrEmployeesRatingBean.setCountOfRangeTwoToThree(underThree);
				gsrEmployeesRatingBean.setCountOfRangeThreeToFour(underFour);
				gsrEmployeesRatingBean.setCountOfRangeFourToFive(underFive);
				listOfEmp.add(gsrEmployeesRatingBean);
			}
			Collections.sort(listOfEmp);

			finalRatingDetails
					.setReporteesWorkFlowDetails(reporteesDashBoardBean);
			finalRatingDetails.setEmpRatings(listOfEmp);
		} catch (Exception e) {

			throw new DataAccessException(e.getMessage());

		}

		LOG.endUsecase("getEmployeesRatingCountByBuId ");
		return finalRatingDetails;

	}

	public GsrEmployeesRatingForHrBean getEmployeesRatingCountByHr()
			throws DataAccessException {

		GsrEmployeesRatingBean gsrEmployeesRatingBean;
		LOG.startUsecase("getEmployeesRatingCountByHr");

		List<GsrEmployeesRatingBean> listOfEmp = new ArrayList<GsrEmployeesRatingBean>();
		Integer totalGoalSet = 0, totalRatedFourAndAbove = 0, totalRatedTwoPointFiveAndBelow = 0;
		GsrEmployeesAvgRatingBean employeesAvgRating = new GsrEmployeesAvgRatingBean();
		GsrEmployeesRatingForHrBean employeesRatingForHr = new GsrEmployeesRatingForHrBean();
		try {

			Specification<GsrSlabBO> specSlab = GsrRatingSpecification
					.getCurrentSlab();
			GsrSlabBO gsrCurrentSlabBo = gsrSlabRepository.findOne(specSlab);

			Short currentSlab = gsrCurrentSlabBo.getPkGsrSlabId();

			Short[] slabs = { --currentSlab, --currentSlab, --currentSlab,
					--currentSlab };

			List<Short> listOfSlabs = Arrays.asList(slabs);

			List<GsrDatEmpFinalRatingBO> data = ratingRepository
					.getAllEmpRatingsForHr(listOfSlabs);

			totalGoalSet = gsrEmployeeGoalsRepository
					.countByFkGsrSlabIdIn(slabs);

			for (Short slab : listOfSlabs) {
				gsrEmployeesRatingBean = new GsrEmployeesRatingBean();
				int belowTwo = 0, underThree = 0, underFour = 0, underFive = 0, belowTwoPointFive = 0;
				for (GsrDatEmpFinalRatingBO ratingBO : data) {
					if (slab == ratingBO.getGsrSlabBO_slab_id()
							.getPkGsrSlabId()) {

						if (ratingBO.getGsrFinalRating() < 2.0) {
							belowTwo++;

						} else if (ratingBO.getGsrFinalRating() >= 2.0
								&& ratingBO.getGsrFinalRating() <= 2.9) {
							underThree++;
						} else if (ratingBO.getGsrFinalRating() >= 3.0
								&& ratingBO.getGsrFinalRating() <= 3.9) {

							underFour++;
						} else if (ratingBO.getGsrFinalRating() >= 4.0
								&& ratingBO.getGsrFinalRating() <= 5.0) {
							underFive++;
						}

						if (ratingBO.getGsrFinalRating() <= 2.5) {
							belowTwoPointFive++;
						}
						
					}
				}
				
				
				GsrSlabBO gsrCurrentSlabDetails = gsrSlabRepository
						.findByPkGsrSlabId(slab);

				gsrEmployeesRatingBean.setSlabName(gsrCurrentSlabDetails
						.getGsrSlabName());
				gsrEmployeesRatingBean.setSlabYear(gsrCurrentSlabDetails
						.getGsrSlabYear());
				gsrEmployeesRatingBean.setSlabShortName(gsrCurrentSlabDetails
						.getGsrSlabShortName());
				totalRatedFourAndAbove = totalRatedFourAndAbove + underFive;
				totalRatedTwoPointFiveAndBelow = totalRatedTwoPointFiveAndBelow
						+ belowTwoPointFive;
				gsrEmployeesRatingBean.setSlabId(slab);
				gsrEmployeesRatingBean.setCountOfBelowTwo(belowTwo);
				gsrEmployeesRatingBean.setCountOfRangeTwoToThree(underThree);
				gsrEmployeesRatingBean.setCountOfRangeThreeToFour(underFour);
				gsrEmployeesRatingBean.setCountOfRangeFourToFive(underFive);

				listOfEmp.add(gsrEmployeesRatingBean);
			}
			employeesAvgRating.setTotalGoalsSet(totalGoalSet);
			employeesAvgRating.setRatedFourAndAbove(totalRatedFourAndAbove);
			employeesAvgRating
					.setRatedTwoPointFiveAndBelow(totalRatedTwoPointFiveAndBelow);
			Collections.sort(listOfEmp);
			employeesRatingForHr.setEmployeesAvgRating(employeesAvgRating);
			employeesRatingForHr.setListOfEmployeesRating(listOfEmp);

		} catch (Exception e) {
			throw new DataAccessException(
					"Exception occured at getEmployeesRatingCountByHr : ", e);

		}

		LOG.endUsecase("getEmployeesRatingCountByHr :");
		return employeesRatingForHr;

	}

	public GsrDisagreeBean getDisagreedRatingOfAllReporteeesOnBuId(Short buId)
			throws DataAccessException, CommonCustomException {
		LOG.startUsecase("getDisagreedRatingOfAllReporteeesOnBuId");
		GsrRatingSpecification specification = new GsrRatingSpecification();
		GsrDisagreeRatingForPrvsSlabsBean disagreeForPrvsSlab;
		List<GsrDisagreeRatingForPrvsSlabsBean> listOfData = new ArrayList<GsrDisagreeRatingForPrvsSlabsBean>();
		GsrDisagreeRatingDetailBean gsrDisagreeDetail = new GsrDisagreeRatingDetailBean();
		GsrDisagreeBean gsrDisagreeBean = new GsrDisagreeBean();

		List<DatEmpProfessionalDetailBO> proDetails = new ArrayList<DatEmpProfessionalDetailBO>();
		List<Integer> allReporteesForBu = new CopyOnWriteArrayList<Integer>();
		List<String> listOfSameDisagreeCount = new LinkedList<>();
		try {

			proDetails = professionalRepository.findByFkEmpBuUnit(buId);

			if (proDetails.size() == 0) {
				throw new CommonCustomException(
						"No reportees for given Business unit Id");
			} else {
				for (DatEmpProfessionalDetailBO pro : proDetails) {
					allReporteesForBu.add(pro.getFkMainEmpDetailId());
				}
			}

			Specification<GsrSlabBO> specSlab = GsrRatingSpecification
					.getCurrentSlab();
			GsrSlabBO gsrCurrentSlabBo = gsrSlabRepository.findOne(specSlab);
			String highestDisagreeQuarter = " ";
			int max = 0;
			Integer totalDisagree = 0;

			Short currentSlab = gsrCurrentSlabBo.getPkGsrSlabId();

			Short[] slabs = { --currentSlab, --currentSlab, --currentSlab,
					--currentSlab };

			List<Short> listOfSlabs = Arrays.asList(slabs);
			Specification<GsrDatEmpFinalRatingBO> spec = specification
					.getAllEmpGsrDisagreeForBu(allReporteesForBu, listOfSlabs);
			List<GsrDatEmpFinalRatingBO> data = (List<GsrDatEmpFinalRatingBO>) ratingRepository
					.findAll(spec);
			for (Short slab : listOfSlabs) {
				disagreeForPrvsSlab = new GsrDisagreeRatingForPrvsSlabsBean();
				Integer totalDisagreePerSlab = 0;

				for (GsrDatEmpFinalRatingBO ratingBo : data) {

					if (slab == ratingBo.getGsrSlabBO_slab_id()
							.getPkGsrSlabId()) {
						totalDisagreePerSlab = totalDisagreePerSlab + 1;

					}

				}

				GsrSlabBO gsrCurrentSlabDetails = gsrSlabRepository
						.findByPkGsrSlabId(slab);

				disagreeForPrvsSlab.setSlabId(slab);
				disagreeForPrvsSlab.setSlabName(gsrCurrentSlabDetails
						.getGsrSlabName());
				disagreeForPrvsSlab.setSlabShortName(gsrCurrentSlabDetails
						.getGsrSlabShortName());
				disagreeForPrvsSlab.setSlabYear(gsrCurrentSlabDetails
						.getGsrSlabYear());

				disagreeForPrvsSlab
						.setCountOfDisagreedRating(totalDisagreePerSlab);

				totalDisagree = totalDisagree
						+ disagreeForPrvsSlab.getCountOfDisagreedRating();
				listOfData.add(disagreeForPrvsSlab);

			}

			for (int k = 0; k < listOfData.size(); k++) {

				if (listOfData.get(k).getCountOfDisagreedRating() != null) {
					if (max < listOfData.get(k).getCountOfDisagreedRating()) {
						max = listOfData.get(k).getCountOfDisagreedRating();
						highestDisagreeQuarter = listOfData.get(k)
								.getSlabShortName()
								+ "-"
								+ listOfData.get(k).getSlabYear();

					}
				}
			}

			for (int j = 0; j < listOfData.size(); j++) {
				if (listOfData.get(j).getCountOfDisagreedRating() != null) {
					if (max == listOfData.get(j).getCountOfDisagreedRating()) {
						highestDisagreeQuarter = listOfData.get(j)
								.getSlabShortName()
								+ "-"
								+ listOfData.get(j).getSlabYear();
						listOfSameDisagreeCount.add(highestDisagreeQuarter);

					}
				}
			}

			Collections.sort(listOfData);

			String joinData = String.join(",", listOfSameDisagreeCount);

			if (listOfSameDisagreeCount.size() > 1) {
				gsrDisagreeDetail.setHighestDisagreeInQuarter(joinData);
			} else {
				gsrDisagreeDetail
						.setHighestDisagreeInQuarter(highestDisagreeQuarter);
			}
			gsrDisagreeDetail.setTotalDisagrees(totalDisagree);

			gsrDisagreeBean.setGsrDisagreeRatingDetail(gsrDisagreeDetail);
			gsrDisagreeBean.setListOfGsrDisagree(listOfData);

		} catch (Exception e) {

			throw new DataAccessException(e.getMessage());

		}

		LOG.endUsecase("getDisagreedRatingOfAllReporteeesOnBuId :");

		return gsrDisagreeBean;

	}

	public GsrDisagreeBean getDisagreeOfRatingOfAllReporteees()
			throws DataAccessException {
		LOG.startUsecase("getDisagreeOfRatingOfAllReporteees");
		GsrRatingSpecification specification = new GsrRatingSpecification();
		GsrDisagreeRatingForPrvsSlabsBean disagreeForPrvsSlab;
		List<GsrDisagreeRatingForPrvsSlabsBean> listOfData = new ArrayList<GsrDisagreeRatingForPrvsSlabsBean>();
		GsrDisagreeRatingDetailBean gsrDisagreeDetail = new GsrDisagreeRatingDetailBean();
		GsrDisagreeBean gsrDisagreeBean = new GsrDisagreeBean();
		try {

			Specification<GsrSlabBO> specSlab = GsrRatingSpecification
					.getCurrentSlab();
			GsrSlabBO gsrCurrentSlabBo = gsrSlabRepository.findOne(specSlab);
			String highestDisagreeQuarter = " ";
			int max = 0;
			Integer totalDisagree = 0;

			Short currentSlab = gsrCurrentSlabBo.getPkGsrSlabId();

			Short[] slabs = { --currentSlab, --currentSlab, --currentSlab,
					--currentSlab };

			List<Short> listOfSlabs = Arrays.asList(slabs);
			Specification<GsrDatEmpFinalRatingBO> spec = specification
					.getAllEmpGsrDisagree(listOfSlabs);
			List<GsrDatEmpFinalRatingBO> data = (List<GsrDatEmpFinalRatingBO>) ratingRepository
					.findAll(spec);
			for (Short slab : listOfSlabs) {
				Integer totalDisagreePerSlab = 0;
				disagreeForPrvsSlab = new GsrDisagreeRatingForPrvsSlabsBean();

				for (GsrDatEmpFinalRatingBO ratingBo : data) {

					if (slab == ratingBo.getGsrSlabBO_slab_id()
							.getPkGsrSlabId()) {
						totalDisagreePerSlab = totalDisagreePerSlab + 1;

					}

				}

				GsrSlabBO gsrCurrentSlabDetails = gsrSlabRepository
						.findByPkGsrSlabId(slab);

				disagreeForPrvsSlab.setSlabId(slab);
				disagreeForPrvsSlab.setSlabName(gsrCurrentSlabDetails
						.getGsrSlabName());
				disagreeForPrvsSlab.setSlabShortName(gsrCurrentSlabDetails
						.getGsrSlabShortName());
				disagreeForPrvsSlab.setSlabYear(gsrCurrentSlabDetails
						.getGsrSlabYear());

				disagreeForPrvsSlab
						.setCountOfDisagreedRating(totalDisagreePerSlab);

				totalDisagree = totalDisagree
						+ disagreeForPrvsSlab.getCountOfDisagreedRating();

				listOfData.add(disagreeForPrvsSlab);
			}
			List<String> listOfSameDisagreeCount = new LinkedList<>();

			for (int i = 0; i < listOfData.size(); i++) {

				if (max < listOfData.get(i).getCountOfDisagreedRating()) {
					max = listOfData.get(i).getCountOfDisagreedRating();
					highestDisagreeQuarter = listOfData.get(i)
							.getSlabShortName()
							+ "-"
							+ listOfData.get(i).getSlabYear();

				}

			}

			for (int i = 0; i < listOfData.size(); i++) {

				if (max == listOfData.get(i).getCountOfDisagreedRating()) {
					highestDisagreeQuarter = listOfData.get(i)
							.getSlabShortName()
							+ "-"
							+ listOfData.get(i).getSlabYear();
					listOfSameDisagreeCount.add(highestDisagreeQuarter);
				}
			}

			Collections.sort(listOfData);
			String joinData = String.join(",", listOfSameDisagreeCount);

			if (listOfSameDisagreeCount.size() > 1) {
				gsrDisagreeDetail.setHighestDisagreeInQuarter(joinData);
			} else {
				gsrDisagreeDetail
						.setHighestDisagreeInQuarter(highestDisagreeQuarter);
			}
			gsrDisagreeDetail.setTotalDisagrees(totalDisagree);

			gsrDisagreeBean.setGsrDisagreeRatingDetail(gsrDisagreeDetail);
			gsrDisagreeBean.setListOfGsrDisagree(listOfData);

		} catch (Exception e) {
			throw new DataAccessException(
					"Exception occured at getDisagreeOfRatingOfAllReporteees : ",
					e);

		}

		LOG.endUsecase("getDisagreeOfRatingOfAllReporteees :");
		return gsrDisagreeBean;

	}

	public List<Integer> getEmpIdListsForDMId(Map<Integer, Integer> mis_emp,
			List<Integer> res, int id) {
		LOG.info("Entering getEmpIdLists with id -->" + id);

		try {
			for (Map.Entry<Integer, Integer> entry : mis_emp.entrySet()) {
				int reportee = entry.getKey();
				int manager = entry.getValue();

				if ((id == manager) && (id != reportee)) {

					res.add(reportee);

				}
			}
		} catch (Exception e) {
			LOG.info("Error --> " + e);
		}
		return res;
	}

	public List<Integer> getEmpIdLists(Map<Integer, Integer> mis_emp,
			List<Integer> res, int id,
			List<DatEmpProfessionalDetailBO> listOfEmployees) {
		LOG.info("Entering getEmpIdLists with id -->" + id);

		try {
			for (Map.Entry<Integer, Integer> entry : mis_emp.entrySet()) {
				int reportee = entry.getKey();
				int manager = entry.getValue();

				if ((id == manager) && (id != reportee)) {

					for (DatEmpProfessionalDetailBO bo : listOfEmployees) {

						if (bo.getFkMainEmpDetailId() == manager
								&& bo.getHasReporteesFlag().equals(
										"YES_HAS_REPORTEES")) {

							res.add(reportee);
						}

						if (bo.getFkMainEmpDetailId() == reportee
								&& bo.getHasReporteesFlag().equals(
										"YES_HAS_REPORTEES")) {

							getEmpIdLists(mis_emp, res, reportee,
									listOfEmployees);
						}
					}
				}
			}
		} catch (Exception e) {
			LOG.info("Error --> " + e);
		}

		return res;
	}
}
