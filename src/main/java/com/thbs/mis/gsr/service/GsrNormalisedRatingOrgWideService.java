/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrNormalisedRatingOrgWideService.java                            */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the methods for the           */
/*                 GSRCommonController                               */
/*  Created By  : Manikandan_Chinnasamy                              */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.gsr.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bo.GsrNormalisedRatingOrgWideBO;
import com.thbs.mis.gsr.dao.GsrNormalisedRatingOrgWideRepository;

@Service
public class GsrNormalisedRatingOrgWideService {

	private static final AppLog LOG = LogFactory
			.getLog(GsrNormalisedRatingOrgWideService.class);
	@Autowired
	GsrNormalisedRatingOrgWideRepository gsrNormalisedRatingOrgWideRepository;
	
	
	static String[] cellData;
	
	static String[] header = { "EMP ID", "Normalised Rating Year",
			"Normalised Rating", "Created By" };
	
	static SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
	
	static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * 
	 * @param year
	 * @return
	 * @throws ParseException
	 */
	public List<GsrNormalisedRatingOrgWideBO> goalNormalisedRatingHistory(
			String year) throws ParseException {
		List<GsrNormalisedRatingOrgWideBO> result = new ArrayList<GsrNormalisedRatingOrgWideBO>();
	 
		result = gsrNormalisedRatingOrgWideRepository
				.findByGsrNormalisedRatingYear(year);
	 
		return result;
	}

	/*
	 * public List<GsrNormalisedRatingOrgWideBO> uploadGoalNormalisedRating()
	 * throws IOException, ParseException { String fileName = "D:/as.xlsx";
	 * String ext2 = FilenameUtils.getExtension(fileName);
	 * List<GsrNormalisedRatingOrgWideBO> output = new
	 * ArrayList<GsrNormalisedRatingOrgWideBO>(); GsrNormalisedRatingOrgWideBO
	 * gsrNormalisedRatingOrgWideBO = new GsrNormalisedRatingOrgWideBO();
	 * FileInputStream ExcelFileToRead = new FileInputStream(fileName); String[]
	 * excelData; if (ext2.equalsIgnoreCase("xls")) { excelData =
	 * readXLSFile(ExcelFileToRead); } else { excelData =
	 * readXLSXFile(ExcelFileToRead); }
	 * System.out.println("excelData length "+excelData.length); try { for (int
	 * i = 0;i <= excelData.length ; i++) { gsrNormalisedRatingOrgWideBO
	 * .setFkGsrNormalisedRatingEmpId((int) Double .parseDouble(cellData[0]));
	 * Date normalizedRatingYear = yearFormat.parse(cellData[1]);
	 * gsrNormalisedRatingOrgWideBO
	 * .setGsrNormalisedRatingYear(normalizedRatingYear + "");
	 * gsrNormalisedRatingOrgWideBO.setGsrNormalisedRating(Float
	 * .parseFloat(cellData[2])); gsrNormalisedRatingOrgWideBO
	 * .setGsrNormalisedRatingCreatedBy((int) Double .parseDouble(cellData[3]));
	 * Date currentDate = new Date(dateFormat.format(new Date()));
	 * gsrNormalisedRatingOrgWideBO
	 * .setGsrNormalisedRatingCreatedDate(currentDate); //
	 * gsrNormalisedRatingOrgWideBO =
	 * gsrNormalisedRatingOrgWideRepository.save(gsrNormalisedRatingOrgWideBO);
	 * output.add(gsrNormalisedRatingOrgWideBO); }
	 * System.out.println("output "+output); } catch (Exception e) {
	 * System.out.println("Exception -->"+e.getMessage()); } return output; }
	 * 
	 * private static String[] readXLSFile(FileInputStream ExcelFileToRead)
	 * throws IOException, ParseException { List<GsrNormalisedRatingOrgWideBO>
	 * excelData = new ArrayList<GsrNormalisedRatingOrgWideBO>(); HSSFWorkbook
	 * wb = new HSSFWorkbook(ExcelFileToRead); HSSFSheet sheet =
	 * wb.getSheetAt(0); HSSFRow row; HSSFCell cell; Iterator rows =
	 * sheet.rowIterator(); while (rows.hasNext()) { row = (HSSFRow)
	 * rows.next(); Iterator cells = row.cellIterator(); cellData = new
	 * String[10]; int cellIndex = 0; GsrNormalisedRatingOrgWideBO
	 * gsrNormalisedRatingOrgWideBO = new GsrNormalisedRatingOrgWideBO(); while
	 * (cells.hasNext()) { cell = (HSSFCell) cells.next(); cellData[cellIndex] =
	 * cell.toString(); cellIndex++; } } return cellData; }
	 * 
	 * private static String[] readXLSXFile(InputStream ExcelFileToRead) throws
	 * IOException, ParseException { List<GsrNormalisedRatingOrgWideBO>
	 * excelData = new ArrayList<GsrNormalisedRatingOrgWideBO>(); XSSFWorkbook
	 * wb = new XSSFWorkbook(ExcelFileToRead); XSSFWorkbook test = new
	 * XSSFWorkbook(); XSSFSheet sheet = wb.getSheetAt(0); XSSFRow row; XSSFCell
	 * cell; Iterator rows = sheet.rowIterator(); while (rows.hasNext()) { row =
	 * (XSSFRow) rows.next(); Iterator cells = row.cellIterator(); cellData =
	 * new String[10]; int cellIndex = 0; GsrNormalisedRatingOrgWideBO
	 * gsrNormalisedRatingOrgWideBO = new GsrNormalisedRatingOrgWideBO(); while
	 * (cells.hasNext()) { cell = (XSSFCell) cells.next(); cellData[cellIndex] =
	 * cell.toString(); cellIndex++; } } return cellData; }
	 */
}
