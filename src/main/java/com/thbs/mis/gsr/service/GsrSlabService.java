/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRSlabRepository.java                            */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  October 28, 2016                                  */
/*                                                                   */
/*  Description :   This class contains all the methods              */
/*                 for the GSRSlabRepository  
 * 																	/*
 * Created By   :  Manikandan_chinnasamy                             */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/*October 28, 2016     THBS     1.0        Initial version created   */
/*********************************************************************/

package com.thbs.mis.gsr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bo.GsrSlabBO;
import com.thbs.mis.gsr.dao.GsrSlabRepository;

/**
 * 
 * @author Thbs
 *
 */
@Service
public class GsrSlabService {
	private static final AppLog LOG = LogFactory.getLog(GsrSlabService.class);

	@Autowired(required = true)
	private GsrSlabRepository gsrSlabRepository;

	/**
	 * 
	 * @param slabId
	 * @return
	 * @throws DataAccessException
	 */
	public GsrSlabBO getSlabInfoById(Short slabId) throws DataAccessException {
		LOG.startUsecase("Entered getSlabInfoById");
		GsrSlabBO slabInfo = new GsrSlabBO();
		try {
			slabInfo = gsrSlabRepository.findByPkGsrSlabId(slabId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get the data from Database");
		}
		LOG.endUsecase("Exited getSlabInfoById");
		return slabInfo;
	}

}
