package com.thbs.mis.gsr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bean.GsrKpiWeightageBean;
import com.thbs.mis.gsr.bo.GsrKpiWeightageBO;
import com.thbs.mis.gsr.dao.GsrWeightageRepository;



@Service
public class GsrWeightageService {

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(GsrWeightageService.class);
	@Autowired
	private GsrWeightageRepository gsrWeightageRepository;
	
	/**
	 * @Description:Method to create GSR weightage
	 * @param Employee level id ,KPI goal id and KPI goal weightage
	 * @return GsrKpiWeightageBO
	 * @exception DataAccessException
	 */
	/*@Autowired CommonService commonService;
	private boolean ValidateGSRWeightage (int empid,byte weightage) throws DataAccessException{
		DatEmpProfessionalDetailBO datEmpProfessionalDetailBO = commonService.getEmpoyeeDeatailsById(empid);
		return true;
	}*/
	
	public GsrKpiWeightageBO createGsrKpiWeightage(GsrKpiWeightageBean gsrKpiWeightageBean) throws DataAccessException
	{
		GsrKpiWeightageBO result;
		GsrKpiWeightageBO gsrKpiWeightageBO=new GsrKpiWeightageBO();
		
		short emplevel=gsrKpiWeightageBean.getFkEmpLevelId();
		System.out.println("Emp level id"+emplevel);
		
		
		LOG.startUsecase("getTrainingTopics");
		
		try{
			gsrKpiWeightageBO.setFkEmpLevelId(gsrKpiWeightageBean.getFkEmpLevelId());
			gsrKpiWeightageBO.setFkGsrKpiGoalId(gsrKpiWeightageBean.getFkGsrKpiGoalId());
			gsrKpiWeightageBO.setGsrKpiGoalWeightage(gsrKpiWeightageBean.getGsrKpiGoalWeightage());
			
		result=	gsrWeightageRepository.save(gsrKpiWeightageBO);
		}
		catch(Exception e)
		{
		
			throw new DataAccessException("Exception occured",e);
		}
		LOG.endUsecase("getTrainingTopics");
		return result;
	}
	
	/**
	 * @Description Service to update the KPI Weightage
	 * @input Emplevelid,GoalID,GoalWeightage,KpiweightageID
	 * @output GsrKpiWeightageBO
	 * @param gsrKpiBean
	 * @return updated kpi weightages
	 * @throws DataAccessException
	 */

	public GsrKpiWeightageBO updateGsrKpiWeightage(
			GsrKpiWeightageBean gsrKpiBean) throws DataAccessException {

		GsrKpiWeightageBO result = new GsrKpiWeightageBO();
		GsrKpiWeightageBO gsrKpiWeightageBO = new GsrKpiWeightageBO();
		LOG.startUsecase("Update_gsr_weightage_service");
		try {

			gsrKpiWeightageBO.setFkEmpLevelId(gsrKpiBean.getFkEmpLevelId());
			gsrKpiWeightageBO.setFkGsrKpiGoalId(gsrKpiBean.getFkGsrKpiGoalId());
			gsrKpiWeightageBO.setGsrKpiGoalWeightage(gsrKpiBean.getGsrKpiGoalWeightage());
			gsrKpiWeightageBO.setPkGsrKpiWeightageId(gsrKpiBean.getPkGsrKpiWeightageId());

			result = gsrWeightageRepository.save(gsrKpiWeightageBO);
		} catch (Exception e) {
			System.out.println("Exception " + e);

			throw new DataAccessException("Exception occured", e);
		}
		LOG.endUsecase("Create_gsr_weightage_service");
		return result;

	}
 
}
