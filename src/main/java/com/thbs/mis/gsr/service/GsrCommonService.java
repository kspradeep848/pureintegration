/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrCommonService.java                             */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contain all the methods related to     */
/*                  the GsrCommonService related calls			     */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.MasTrainingTopicNameBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bean.GsrGetAllReportingManagersBean;
import com.thbs.mis.gsr.bean.GsrSlabOutputBean;
import com.thbs.mis.gsr.bean.GsrWorflowPermissionBean;
import com.thbs.mis.gsr.bo.GsrAdminConfigurationBO;
import com.thbs.mis.gsr.bo.GsrSlabBO;
import com.thbs.mis.gsr.bo.GsrWorkflowPermissionBO;
import com.thbs.mis.gsr.dao.GSRAdminConfigRepository;
import com.thbs.mis.gsr.dao.GSRCommonRepository;
import com.thbs.mis.gsr.dao.GSRWorkFlowRepository;
import com.thbs.mis.gsr.dao.GsrSlabRepository;

@Service
public class GsrCommonService {

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory.getLog(GsrCommonService.class);

	@Autowired
	private GSRAdminConfigRepository adminRepository; // added by Rajesh Kumar

	@Autowired
	private GSRCommonRepository repository;

	@Autowired
	private GsrSlabRepository gsrslabrepository;// added by prathibha

	@Autowired
	private GSRWorkFlowRepository workflowrepository; // added by Smrithi

	@Autowired
	private EmpDetailRepository empDetailRepository;
	
	
	@Value("${employee.not.include}")
	private String empIds;

	// Added by Rajesh Kumar on 15/12/2016
	/**
	 * 
	 * <Description getAdminConfig:> method is call the repository method to
	 * view all the Admin Configuration Setting data in the data base. It will
	 * take the BO and passes it to the Repository for update the Record.
	 * 
	 * @author THBS
	 * 
	 * @param GsrAdminConfigurationBO
	 *            is the argument for this method.
	 * 
	 * @return an Iterable<GsrAdminConfigurationBO> return the list of Gsr Admin
	 *         Configuration BO according to the input details.
	 * 
	 * @throws DataAccessException
	 *             throws if any exception occurs during the data retrieval.
	 * 
	 */
	public List<GsrAdminConfigurationBO> getAdminConfig(int slabId)
			throws DataAccessException {
		GsrAdminConfigurationBO result = null;
		List<GsrAdminConfigurationBO> resultList = new ArrayList<GsrAdminConfigurationBO>();

		LOG.startUsecase("getAdminConfig");
		try {

			result = adminRepository.findByFkGsrSlabId(slabId);
		} catch (Exception e) {
			throw new DataAccessException("Exception occured", e);
		}
		
		if(result!= null)
			resultList.add(result);
		LOG.endUsecase("getAdminConfig");
		return resultList;
	}

	// Ended by Rajesh Kumar on 15/12/2016

	// Added by Pratibha T R on 17/11/2016
	/**
	 * 
	 * @return List<MasTrainingTopicNameBO> contains the list of
	 *         MasTrainingTopicName BO's
	 * @throws DataAccessException
	 *             when any data access/retrieval error occurs.
	 * @throws CommonCustomException
	 * @Description :This Service has been implemented to retrieve the training
	 *              topics
	 */
	public List<MasTrainingTopicNameBO> getTrainingTopics()
			throws DataAccessException, CommonCustomException {
		LOG.startUsecase("getTrainingTopics");
		List<MasTrainingTopicNameBO> trainingTopicresult;
		try {
			trainingTopicresult = repository.findAll();
			if (trainingTopicresult == null) {
				throw new CommonCustomException("No Data Found");

			}
		} catch (Exception e) {
			throw new CommonCustomException("No Data Found");
		}
		LOG.endUsecase("getTrainingTopics");
		return trainingTopicresult;
	}

	/**
	 * 
	 * @param gsrSlabYear
	 * @return List<GsrSlabBO> contains the list of GsrSlab Bo's
	 * @throws DataAccessException
	 *             throws if any exception occurs during the data retrieval.
	 * @throws CommonCustomException
	 * @Description :This Service has been implemented to retrieve the slab details 
	                    based on year. 
	 */
	public List<GsrSlabBO> viewSlabDetailsBasedOnSlabYear(String gsrSlabYear)
			throws DataAccessException, CommonCustomException {
		LOG.startUsecase("viewSlabDetailsBasedOnSlabYear");
		List<GsrSlabBO> gsrSlabList = new ArrayList<GsrSlabBO>();
		try {
			gsrSlabList = gsrslabrepository
					.findByGsrSlabYearAndGsrSlabEnabledOrderByPkGsrSlabIdDesc(
							gsrSlabYear, "ENABLED");
			if (gsrSlabList == null) {
				throw new CommonCustomException("No Data Found");

			}
		} catch (Exception e) {
			throw new CommonCustomException("No Data Found");
		}
		LOG.endUsecase("viewSlabDetailsBasedOnSlabYear");
		return gsrSlabList;
	}

	// Ended by Pratibh T R on 17/11/2016

	// Added by Kamal Anand
	/**
	 * 
	 * @param gsrSlabYear
	 * @return List<GsrSlabBO> contains the list of GsrSlab Bo's
	 * @throws DataAccessException
	 *             throws if any exception occurs during the data retrieval.
	 * 
	 * @throws CommonCustomException
	 */
	public List<GsrSlabBO> viewSlabDetailsOnSlabYear(String gsrSlabYear)
			throws DataAccessException, CommonCustomException {
		LOG.startUsecase("viewSlabDetailsOnSlabYear");
		List<GsrSlabBO> gsrSlabList = new ArrayList<GsrSlabBO>();
		try {
			gsrSlabList = gsrslabrepository
					.findByGsrSlabYearOrderByPkGsrSlabIdDesc(gsrSlabYear);
			if (gsrSlabList == null) {
				throw new CommonCustomException("No Data Found");
			}
		} catch (Exception e) {
			throw new CommonCustomException("No Data Found");
		}
		LOG.endUsecase("viewSlabDetailsOnSlabYear");
		return gsrSlabList;
	}

	// End of Addition by Kamal Anand
	// Added by Smrithi
	/**
	 * @author THBS
	 * @return List<GsrWorflowPermissionBean> contains the list of Work Flow
	 *         Permission fields.
	 * 
	 * @throws DataAccessException
	 *             when any data access/retrieval error occurs.
	 * 
	 * @Description: This method is used to get the workflow permissions.
	 */
	public List<GsrWorflowPermissionBean> getWorkflowPermission(short slabId,
			Integer empId) throws DataAccessException {
		List<GsrWorkflowPermissionBO> gsrWorkflowList;
		List<GsrWorflowPermissionBean> gsrWorkflowPermissionBean = new ArrayList<GsrWorflowPermissionBean>();

		LOG.startUsecase("getWorkflowPermission");
		try {

			gsrWorkflowList = workflowrepository
					.findByGsrSlabPkGsrSlabIdAndDatEmployeePkEmpId(slabId,
							empId);
			for (GsrWorkflowPermissionBO workflowDetail : gsrWorkflowList) {

				GsrWorflowPermissionBean workflowBean = new GsrWorflowPermissionBean();
				workflowBean.setFkGsrWorkflowPermissionEmpId(workflowDetail
						.getFkGsrWorkflowPermissionEmpId());
				workflowBean.setPkGsrWorkflowPermissionId(workflowDetail
						.getPkGsrWorkflowPermissionId());
				workflowBean.setFkGsrWorkflowPermissionSlabId(workflowDetail
						.getFkGsrWorkflowPermissionSlabId());
				workflowBean.setEnableGoalsCreation(workflowDetail
						.getEnableGoalsCreation());
				workflowBean.setEnableEnterAccomplishments(workflowDetail
						.getEnableEnterAccomplishments());
				workflowBean.setEnableFeedback(workflowDetail
						.getEnableFeedback());
				workflowBean.setGsrWorkflowPermissionRequestedBy(workflowDetail
						.getGsrWorkflowPermissionRequestedBy());
				workflowBean
						.setGsrWorkflowPermissionRequestedDate(workflowDetail
								.getGsrWorkflowPermissionRequestedDate());
				workflowBean.setGsrWorkflowPermissionStartDate(workflowDetail
						.getGsrWorkflowPermissionStartDate());
				workflowBean.setGsrWorkflowPermissionEndDate(workflowDetail
						.getGsrWorkflowPermissionEndDate());
				workflowBean.setEmpName(getEmployeeName(workflowDetail
						.getFkGsrWorkflowPermissionEmpId()));
				gsrWorkflowPermissionBean.add(workflowBean);

			}

		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new DataAccessException("Exception occured", e);
		}
		LOG.endUsecase("getWorkflowPermission");
		return gsrWorkflowPermissionBean;
	}

	@Autowired(required = true)
	EmployeePersonalDetailsRepository employeePersonalDetailsRepository;

	public String getEmployeeName(Integer empId) {
		LOG.startUsecase("Entered getEmployeeName");
		DatEmpPersonalDetailBO empData = employeePersonalDetailsRepository
				.findByFkEmpDetailId(empId);
		LOG.endUsecase("Exited getEmployeeName");
		return empData.getEmpFirstName() + " " + empData.getEmpLastName();
	}

	/**
	 * @author THBS
	 * @return List<DatEmpDetailBO> contains the list of Employee details BO's
	 * 
	 * @throws DataAccessException
	 *             when any data access/retrieval error occurs.
	 * @throws CommonCustomException
	 * 
	 * @Description: This method is used to get the list of reporting manager
	 *               name and ID using the role id.
	 */
	public List<GsrGetAllReportingManagersBean> getAllReportingManagers()
			throws DataAccessException, CommonCustomException {
		List<DatEmpDetailBO> gsrEmpDetailList;
		List<GsrGetAllReportingManagersBean> reportingManagersList = new ArrayList<GsrGetAllReportingManagersBean>();
		/*
		 * String roleId="2,4,5"; List<Integer> empRoles= new
		 * ArrayList<Integer>(); empRoles.add(2); empRoles.add( 4);
		 * empRoles.add(5);
		 */
		LOG.startUsecase("getAllReportingManagers");
		// try {
		
		List<Integer> empIDList = new ArrayList<Integer>();
		String[] empIdArray = empIds.split(",");
		for (String s : empIdArray)
		empIDList.add(Integer.parseInt(s));
		gsrEmpDetailList = empDetailRepository
				.getAllReportingManagers("YES_HAS_REPORTEES",empIDList);
		System.out.println("gsrEmpDetailList" + gsrEmpDetailList.size());
		if (gsrEmpDetailList != null) {
			for (DatEmpDetailBO empDetail : gsrEmpDetailList) {
				GsrGetAllReportingManagersBean empGoalOutputBean = new GsrGetAllReportingManagersBean();
				empGoalOutputBean.setEmpId(empDetail.getPkEmpId());
				empGoalOutputBean.setEmpName(empDetail.getEmpFirstName() + " "
						+ empDetail.getEmpLastName());
				reportingManagersList.add(empGoalOutputBean);
			}
		} else {
			// LOG.error("Exception occured", e);
			throw new CommonCustomException(
					"Exception occurred in GSRGetAllReportingManagers");
		}
		/*
		 * } catch (Exception e) { LOG.error("Exception occured", e); throw new
		 * CommonCustomException("Exception occured", e); }
		 */
		LOG.endUsecase("getAllReportingManagers");
		System.out.println("reportingManagersList"
				+ reportingManagersList.size());
		return reportingManagersList;
	}

	// EOA by Smrithi

	/**
	 * @author THBS
	 * @return
	 * @throws DataAccessException
	 * @Description:This method is used to get all GSR slabs.
	 */
	// Added by Prathibha

	public List<GsrSlabOutputBean> getAllgsrSlabs() throws DataAccessException {
		List<GsrSlabOutputBean> gsrBeanList = new ArrayList<GsrSlabOutputBean>();
		List<GsrSlabBO> slabsList;

		LOG.startUsecase("getAllgsrSlabs");
		try {

			slabsList = gsrslabrepository.findAllByOrderByPkGsrSlabIdDesc();

			if (!slabsList.isEmpty()) {
				for (GsrSlabBO gsrSlabBO : slabsList) {
					GsrSlabOutputBean gsrSlabBean = new GsrSlabOutputBean();
					gsrSlabBean.setGsrSlabName(gsrSlabBO.getGsrSlabName());
					gsrSlabBean.setPkGsrSlabId(gsrSlabBO.getPkGsrSlabId());
					gsrSlabBean.setGsrSlabYear(gsrSlabBO.getGsrSlabYear());
					gsrSlabBean.setGsrSlabShortName(gsrSlabBO
							.getGsrSlabShortName());
					gsrSlabBean.setGsrSlabStartDate(gsrSlabBO
							.getGsrSlabStartDate());
					gsrSlabBean
							.setGsrSlabEndDate(gsrSlabBO.getGsrSlabEndDate());
					gsrSlabBean
							.setGsrSlabEnabled(gsrSlabBO.getGsrSlabEnabled());
					if (gsrSlabBO.getGsrSlabEnabled().equalsIgnoreCase(
							"ENABLED")) {
						gsrBeanList.add(gsrSlabBean);
					} // made changes by Rajesh Kumar
				}
			}

		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new DataAccessException("Exception occured", e);
		}
		LOG.endUsecase("getAllgsrSlabs");
		return gsrBeanList;

	}

}
