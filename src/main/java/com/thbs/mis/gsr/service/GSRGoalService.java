/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRGoalService.java                               */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  October 28, 2016                                  */
/*                                                                   */
/*  Description :  All the goal services are done here 	             */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* October 28, 2016     THBS     1.0        Initial version created  */
/*********************************************************************/

package com.thbs.mis.gsr.service;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.google.common.base.Optional;
import com.thbs.mis.common.bean.SeparationLegacyOutputBean;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.dao.BusinessUnitRepository;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.common.service.SeparationLegacyService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.ConfigException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.exception.GoalNotFoundException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.mail.EmailServiceUtil;
import com.thbs.mis.framework.util.DateTimeUtil;
import com.thbs.mis.framework.util.EntityBeanList;
import com.thbs.mis.gsr.bean.BUHeadreporteesgoalinputbean;
import com.thbs.mis.gsr.bean.BUHeadreporteesgoaloutputbean;
import com.thbs.mis.gsr.bean.DMEmpGoalInputBean;
import com.thbs.mis.gsr.bean.DMViewEmpGoalsOutputBean;
import com.thbs.mis.gsr.bean.DelegationGoalInputBeanHR;
import com.thbs.mis.gsr.bean.DelegationGoalsBean;
import com.thbs.mis.gsr.bean.DelegationGoalsInputBean;
import com.thbs.mis.gsr.bean.DelegationGoalsOutputBean;
import com.thbs.mis.gsr.bean.DelegationResultFromDbBean;
import com.thbs.mis.gsr.bean.EmpPreNormalizedRatingOutputBean;
import com.thbs.mis.gsr.bean.FeedbackListBean;
import com.thbs.mis.gsr.bean.FetchPastGoalsForEmployeeListInputBean;
import com.thbs.mis.gsr.bean.ForceClosedGoalsInputBean;
import com.thbs.mis.gsr.bean.GSRDelegatedEmpGoalBean;
import com.thbs.mis.gsr.bean.GoalDetails;
import com.thbs.mis.gsr.bean.GoalDetailsForDelegationBean;
import com.thbs.mis.gsr.bean.GsrAcceptDelegatedGoalsBean;
import com.thbs.mis.gsr.bean.GsrApproveAndRejectGoalBean;
import com.thbs.mis.gsr.bean.GsrApproveGoalBean;
import com.thbs.mis.gsr.bean.GsrCreateDelegatedGoalOutputBean;
import com.thbs.mis.gsr.bean.GsrCreateGoalInputBean;
import com.thbs.mis.gsr.bean.GsrCreateSlabBean;
import com.thbs.mis.gsr.bean.GsrDatEmpGoalBOBeanInput;
import com.thbs.mis.gsr.bean.GsrDelegateGoalBean;
import com.thbs.mis.gsr.bean.GsrDelegateOutputBean;
import com.thbs.mis.gsr.bean.GsrDelegetedOutputBean;
import com.thbs.mis.gsr.bean.GsrDisagreeRatingBean;
import com.thbs.mis.gsr.bean.GsrEmployeeGoalDetailsBean;
import com.thbs.mis.gsr.bean.GsrEmployeeGoalDetailsBeanPagenation;
import com.thbs.mis.gsr.bean.GsrEnableSettingsBean;
import com.thbs.mis.gsr.bean.GsrEnterAccomplishmentBean;
import com.thbs.mis.gsr.bean.GsrEnterFeedbackBean;
import com.thbs.mis.gsr.bean.GsrEnterFeedbackForManagerBean;
import com.thbs.mis.gsr.bean.GsrFetchPastGoalsForEmployeeOutputBean;
import com.thbs.mis.gsr.bean.GsrGoalClosedStatusBean;
import com.thbs.mis.gsr.bean.GsrGoalOutputBean;
import com.thbs.mis.gsr.bean.GsrGoalOutputForMultipleReporteesBean;
import com.thbs.mis.gsr.bean.GsrGoalSetStatusBean;
import com.thbs.mis.gsr.bean.GsrGoalsClosedBean;
import com.thbs.mis.gsr.bean.GsrKpiGoalsBean;
import com.thbs.mis.gsr.bean.GsrKpiWeightageInputBean;
import com.thbs.mis.gsr.bean.GsrKpiWeightageOutputBean;
import com.thbs.mis.gsr.bean.GsrMaximumKpiOutputBean;
import com.thbs.mis.gsr.bean.GsrMaximumKpiWeightageBean;
import com.thbs.mis.gsr.bean.GsrReopenGoalsByHrBean;
import com.thbs.mis.gsr.bean.GsrSetWorkflowDatesBean;
import com.thbs.mis.gsr.bean.GsrTeamGoalDetailsBean;
import com.thbs.mis.gsr.bean.GsrTeamGoalStatusBean;
import com.thbs.mis.gsr.bean.GsrTrainingRequestBean;
import com.thbs.mis.gsr.bean.GsrTrainingRequestSearchBean;
import com.thbs.mis.gsr.bean.GsrUpdateCommonGoalsBean;
import com.thbs.mis.gsr.bean.GsrUpdateGoalInputBean;
import com.thbs.mis.gsr.bean.GsrViewCurrentGoalsBean;
import com.thbs.mis.gsr.bean.GsrWorflowPermissionBean;
import com.thbs.mis.gsr.bean.HrViewEmpGoalsInputBean;
import com.thbs.mis.gsr.bean.HrViewEmployeeGoalsOutputBean;
import com.thbs.mis.gsr.bean.HrViewTeamGoalsInputBean;
import com.thbs.mis.gsr.bean.PreNormalizedRatingBean;
import com.thbs.mis.gsr.bean.ReporteesBean;
import com.thbs.mis.gsr.bean.RmViewEmployeeGoalsOutputBean;
import com.thbs.mis.gsr.bean.ViewEmployeeGoalsInputBean;
import com.thbs.mis.gsr.bean.ViewEmployeeGoalsOutputBean;
import com.thbs.mis.gsr.bean.ViewTeamGoalsInputBean;
import com.thbs.mis.gsr.bean.ViewTeamGoalsOutputBean;
import com.thbs.mis.gsr.bo.GsrAdminConfigurationBO;
import com.thbs.mis.gsr.bo.GsrDatEmpDelegatedGoalBO;
import com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO;
import com.thbs.mis.gsr.bo.GsrDatEmpGoalBO;
import com.thbs.mis.gsr.bo.GsrKpiGoalBO;
import com.thbs.mis.gsr.bo.GsrKpiWeightageBO;
import com.thbs.mis.gsr.bo.GsrMasGoalStatusBO;
import com.thbs.mis.gsr.bo.GsrSlabBO;
import com.thbs.mis.gsr.bo.GsrTrainingRequestBO;
import com.thbs.mis.gsr.bo.GsrWorkflowPermissionBO;
import com.thbs.mis.gsr.constants.GsrConstants;
import com.thbs.mis.gsr.dao.GSRAdminConfigRepository;
import com.thbs.mis.gsr.dao.GSRCreateCommonGoalsRepository;
import com.thbs.mis.gsr.dao.GSRCreateGoalRepository;
import com.thbs.mis.gsr.dao.GSRCreateSlabRepository;
import com.thbs.mis.gsr.dao.GSRDefaultGoalsRepository;
import com.thbs.mis.gsr.dao.GSRDelegatedGoalRepository;
import com.thbs.mis.gsr.dao.GSRDisagreeRatingRepository;
import com.thbs.mis.gsr.dao.GSREmployeegoalsRepository;
import com.thbs.mis.gsr.dao.GSREnterAccomplishmentRepository;
import com.thbs.mis.gsr.dao.GSRFinalRatingRepository;
import com.thbs.mis.gsr.dao.GSRForceCloseGoalsRepository;
import com.thbs.mis.gsr.dao.GSRReporteesGoalsRepository;
import com.thbs.mis.gsr.dao.GSRTeamGoalsRepository;
import com.thbs.mis.gsr.dao.GSRTrainingRequestRepository;
import com.thbs.mis.gsr.dao.GSRWorflowPermissionRepository;
import com.thbs.mis.gsr.dao.GsrAcceptRatingRepository;
import com.thbs.mis.gsr.dao.GsrDatEmpGoalRepository;
import com.thbs.mis.gsr.dao.GsrEmpGoalsSpecification;
import com.thbs.mis.gsr.dao.GsrGoalStatusRepository;
import com.thbs.mis.gsr.dao.GsrRatingRepository;
import com.thbs.mis.gsr.dao.GsrSlabRepository;
import com.thbs.mis.gsr.dao.GsrTrainingRequestSpecifications;
import com.thbs.mis.gsr.dao.GsrViewPastGoalsRepo;
import com.thbs.mis.gsr.dao.GsrWeightageRepository;
import com.thbs.mis.gsr.dao.GsrWokflowPermissionRepository;

@Service
public class GSRGoalService {
//hi
	private static final AppLog LOG = LogFactory.getLog(GSRGoalService.class);
	/* added by Smrithi */

	// private static final Object MailTemplate = null;

	@Autowired
	private GSREmployeegoalsRepository repository;
	@Autowired
	private GSRDisagreeRatingRepository disagreeRatingRepository;

	@Autowired
	private EmployeePersonalDetailsRepository empPersonalRepository;

	@Autowired
	private DatEmployeeProfessionalRepository employeeProfessionalRepository;

	@Autowired
	private GSRDelegatedGoalRepository delegatedGoalRepository;

	@Autowired
	private GSRForceCloseGoalsRepository forceCloseGoalsRepository;

	@Autowired
	private GSRDefaultGoalsRepository defaultGoalsRepository;

	@Autowired
	private GSRTeamGoalsRepository teamGoalsrepository;

	@Autowired
	private GSRReporteesGoalsRepository reporteesGoalsrepository;

	@Autowired
	private EmailServiceUtil misEmailService;

	@Autowired
	private CommonService commonService;

	/* Ended by Smrithi */
	/* added by Pratibha */
	@Autowired
	private GSRCreateSlabRepository slabRepository;
	@Autowired
	private GSRCreateCommonGoalsRepository goalRepository;

	@Autowired
	private GSRAdminConfigRepository ConfigRepository;

	@Autowired
	private GsrWokflowPermissionRepository workRepository;

	@Autowired
	private GsrDatEmpGoalRepository gsrDatEmpGoalRepository;

	@Autowired
	private GsrDatEmpGoalRepository empGoalRepository;
	/* added by Pratibha */

	@Autowired
	private GsrViewPastGoalsRepo gsrViewPastGols;

	@Autowired
	private GSRCreateGoalRepository createGoalrepository;

	// Added by Rajesh Kumar

	@Autowired
	private GSREnterAccomplishmentRepository gsrRepository;

	@Autowired
	private GSRDelegatedGoalRepository delegateGoalRepository;

	@Autowired
	private GsrAcceptRatingRepository acceptRatingRepository;

	// Ended by Rajesh Kumar

	// Added by Mani for GSR module service committed on 30-11-2016

	@Autowired(required = true)
	private GSRFinalRatingRepository gsrDatEmpFinalRatingRepository;

	// End of addition by Mani for GSR module service committed on 30-11-2016

	// Added by Balaji 15-12-16
	@Autowired
	private GSRCreateGoalRepository createGoalRepository;

	@Autowired
	private GSRWorflowPermissionRepository permissionRepository;

	@Autowired
	private GSRTrainingRequestRepository trainingRepository;

	// End of Addition by Balaji

	// Added by Kamal Anand
	@Autowired
	private GsrGoalStatusRepository goalStatusRepository;

	@Autowired
	private EmpDetailRepository empDetailsRepository;

	@Autowired
	private GsrSlabRepository gsrSlabRepository;

	@Autowired
	private GsrRatingRepository ratingRepository;

	@Autowired
	private GsrWeightageRepository gsrKpiWeightageRepos;

	@Autowired
	private EmployeePersonalDetailsRepository employeePersonalDetailsRepository;

	// End of Addition by Kamal Anand

	@Value("${goal.accomplishmentsEntered.status}")
	private byte goalAccomplishmentEnteredStatus;

	@Value("${goal.rated.status}")
	private byte goalRatedStatus;

	@Value("${goal.closed.status}")
	private byte goalClosedStatus;

	@Value("${goal.forceclose.status}")
	private byte goalForceClosedStatus;

	@Value("${goal.reopen.status}")
	private byte goalReopenStatus;

	@Value("${final.rating.accepted.status}")
	private byte finalRatingAcceptedStatus;

	@Value("${final.rating.forceclose.status}")
	private byte finalRatingForceCloseStatus;

	@Value("${final.rating.rated.status}")
	private byte finalRatingRatedStatus;

	@Value("${final.rating.rejected.status}")
	private byte finalRatingRejectedStatus;

	@Value("${goal.managerFeedback.status}")
	private byte goalManagerFeedbackStatus;

	@Value("${goal.waitingForManagerApproval.status}")
	private byte goalWaitingForManagerApprovalStatus;

	@Value("${goal.accomplishmentsEntered.status}")
	private byte goalAccomplishmentsEnteredStatus;

	@Value("${goal.goalSet.status}")
	private byte goalGoalSetStatus;

	@Value("${hrd.senior.executive.role}")
	private byte hrdSeniorExecutiveRole;

	@Value("${hrd.manager.role}")
	private byte hrdManagerRole;

	@Value("${delegation.waitingForAcceptance.status}")
	private byte delegationWaitingForAcceptanceStatus;

	@Value("${delegation.accepted.status}")
	private byte delegationAcceptedStatus;

	@Value("${delegation.rejected.status}")
	private byte delegationRejectedStatus;

	@Value("${goal.allowed.rate.days}")
	private byte goalAllowedRateDays;

	@Value("${final.rating.reopen.status}")
	private byte finalRatingReopenStatus;

	@Value("${goal.rejected.status}")
	private byte goalRejectedStatus;

	int year = Year.now().getValue();

	SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
	// Added by Smrithi
	@Value("${employee.not.include}")
	private String empIds;

	@Value("${hr.mail.id}")
	private String hrMailId;

	@Value("${goal.subtract.days}")
	private Integer subtractDays;

	@Value("${disagreeRating.template}")
	private File disagreeRating;

	@PersistenceContext
	private EntityManager entityManager;

	private int CONSTANT_GOAL_NOT_SET = 1;
	private int CONSTANT_GOAL_SET = 2;
	private int CONSTANT_GOAL_CLOSED = 3;
	private int CONSTANT_GOAL_NOT_CLOSED = 4;
	private int CONSTANT_NOT_APPLICABLE = 5;
	private int CONSTANT_GOAL_PENDING = 0;
	private int CONSTANT_GOAL_NO_REPORTEE = 6;
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Value("${legacy.service.url}")
	private String purchaseLegacyUrl;
	
	@Value("${separation.accepted.by.rm.status}")
	private Integer separationAcceptedByRmStatus;
	
	@Value("${separation.accepted.by.dm.status}")
	private Integer separationAcceptedByDmStatus;
	
	@Value("${separation.withdrawn.status}")
	private Integer separationWithdrawnStatus;
	
	@Value("${separation.cancelled.status}")
	private Integer separationCancelledStatus;
	
	@Value("${separation.initiated.status}")
	private Integer separationInitiatedStatus;
	
	/**
	 * @author THBS
	 * @param empBean
	 * @return List<GsrDatEmpGoalBO>
	 * @throws DataAccessException
	 * @throws ParseException
	 * @throws MethodArgumentNotValidException
	 * @Description: This method is used to view the employee goals.
	 */
	public List<ViewEmployeeGoalsOutputBean> viewEmployeeGoals(
			GsrEmployeeGoalDetailsBean empGoalDetailsBean)
			throws DataAccessException, ParseException,
			MethodArgumentNotValidException {
		List<GsrDatEmpGoalBO> gsrEmpGoalList = new ArrayList<GsrDatEmpGoalBO>();
		List<ViewEmployeeGoalsOutputBean> empGoalsList = new ArrayList<ViewEmployeeGoalsOutputBean>();
		LOG.startUsecase("view Employee Goals");
		List<FeedbackListBean> feedbackList = null;
		Map<Short, List<GsrDatEmpGoalBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpGoalBO>>();
		boolean otherMgrIdIsPresentIsGoal = false;
		List<GsrDatEmpGoalBO> tempGoalList = new ArrayList<GsrDatEmpGoalBO>();
		try {

			Date truncatedStartDate = DateUtils.truncate(
					empGoalDetailsBean.getEmpStartDate(), Calendar.DATE);
			Date truncatedEndDate = DateUtils.truncate(
					empGoalDetailsBean.getEmpEndDate(), Calendar.DATE);

			gsrEmpGoalList = repository
					.findByFkGsrDatEmpIdAndGsrGoalStartDateGreaterThanEqualAndGsrGoalEndDateLessThanEqualOrderByFkGsrSlabIdDescPkGsrDatEmpGoalIdAscEmpGoalKpiWeightageIdAsc(
							empGoalDetailsBean.getId(), truncatedStartDate,
							truncatedEndDate);

			for (GsrDatEmpGoalBO empGoal : gsrEmpGoalList) {
				if (!empGoalsMap
						.containsKey(empGoal.getEmpGoalKpiWeightageId())) {
					List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
					finalBean.add(empGoal);
					empGoalsMap.put(empGoal.getEmpGoalKpiWeightageId(),
							finalBean);
					continue;
				}
				if (empGoalsMap.containsKey(empGoal.getEmpGoalKpiWeightageId())) {
					List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
					finalBean.add(empGoal);
					empGoalsMap.get(empGoal.getEmpGoalKpiWeightageId()).addAll(
							finalBean);
				}
			}
			for (Map.Entry<Short, List<GsrDatEmpGoalBO>> entry : empGoalsMap
					.entrySet()) {
				List<GsrDatEmpGoalBO> empList = (List<GsrDatEmpGoalBO>) entry
						.getValue();
				if (entry.getKey() == 0) {
					tempGoalList.addAll(empList);
				} else {
					Collections.sort(
							empList,
							Comparator.comparing(
									GsrDatEmpGoalBO::getPkGsrDatEmpGoalId)
									.reversed());
					tempGoalList.add(empList.get(0));
					empList.remove(empList.get(0));
					for (GsrDatEmpGoalBO gsrDatEmpGoalBO : empList) {
						if (gsrDatEmpGoalBO.getFkGsrGoalStatus() == goalRejectedStatus)
							tempGoalList.add(gsrDatEmpGoalBO);
					}
				}
			}

			for (int i = 0; i < tempGoalList.size(); i++) {
				feedbackList = new ArrayList<FeedbackListBean>();
				ViewEmployeeGoalsOutputBean empGoalOutputBean = new ViewEmployeeGoalsOutputBean();
				empGoalOutputBean.setEmpId(tempGoalList.get(i)
						.getFkGsrDatEmpId().intValue());// Added by Shyam for
														// bug fix
				empGoalOutputBean.setEmpName(getEmpNameByEmpId(tempGoalList
						.get(i).getFkGsrDatEmpId().intValue())); // Added by
																	// Smrithi
																	// to get
																	// empName.
				empGoalOutputBean.setGoalName(tempGoalList.get(i)
						.getGsrDatEmpGoalName());
				empGoalOutputBean.setGoalWeightage(tempGoalList.get(i)
						.getGsrGoalWeightage() + "");
				empGoalOutputBean.setGoalDuration(tempGoalList.get(i)
						.getGsrSlabBO_slab_id().getGsrSlabName());
				empGoalOutputBean.setMgrId(tempGoalList.get(i)
						.getFkGsrDatMgrId());
				empGoalOutputBean.setMgrName(getEmpNameByEmpId(tempGoalList
						.get(i).getFkGsrDatMgrId()));
				empGoalOutputBean.setGoalDescription(tempGoalList.get(i)
						.getGsrGoalDescription());
				empGoalOutputBean.setGoalMeasurement(tempGoalList.get(i)
						.getGsrGoalMeasurement());
				empGoalOutputBean.setSelfRating(tempGoalList.get(i)
						.getGsrGoalSelfRating() + "");
				empGoalOutputBean.setAccomplishments(tempGoalList.get(i)
						.getGsrGoalAccomplishment());
				empGoalOutputBean.setMgrFeedback(tempGoalList.get(i)
						.getGsrGoalMgrFeedback());
				empGoalOutputBean.setMgrRating(tempGoalList.get(i)
						.getGsrGoalMgrRating() + "");
				empGoalOutputBean.setSlabId(tempGoalList.get(i)
						.getFkGsrSlabId());
				empGoalOutputBean.setSlabStartDate(tempGoalList.get(i)
						.getGsrGoalStartDate());// added
				empGoalOutputBean.setSlabEndDate(tempGoalList.get(i)
						.getGsrGoalEndDate());
				empGoalOutputBean.setGoalStatusId(tempGoalList.get(i)
						.getFkGsrGoalStatus());
				empGoalOutputBean.setGoalStatusName(tempGoalList.get(i)
						.getGsrMasGoalStatusBOgoalstatus()
						.getGsrGoalStatusName());
				empGoalOutputBean.setGoalId(tempGoalList.get(i)
						.getPkGsrDatEmpGoalId());
				empGoalOutputBean.setKpiGoalId(tempGoalList.get(i)
						.getEmpGoalKpiWeightageId());
				empGoalOutputBean.setClosedBy(tempGoalList.get(i)
						.getGsrGoalClosedBy());
				empGoalOutputBean.setGsrGoalModifiedBy(tempGoalList.get(i)
						.getGsrGoalModifiedBy());
				if (empGoalOutputBean.getGsrGoalModifiedBy() != null) // null
																		// check
																		// for
																		// modifiedByName
					empGoalOutputBean
							.setGsrGoalModifiedByName(getEmpNameByEmpId(tempGoalList
									.get(i).getGsrGoalModifiedBy().intValue())); // added
																					// by
																					// Smrithi
																					// on
																					// 13-9-17.
				else
					empGoalOutputBean.setGsrGoalModifiedByName(null);
				empGoalOutputBean.setGsrGoalModifiedDate(new Date());

				if (tempGoalList.get(i).getGsrGoalClosedBy() != null)
					empGoalOutputBean
							.setClosedByName(getEmpNameByEmpId(tempGoalList
									.get(i).getGsrGoalClosedBy()));
				else
					empGoalOutputBean.setClosedByName(null);
				if (tempGoalList.get(i).getEmpGoalKpiWeightageId() != null) {
					empGoalOutputBean.setKpiGoalWeightage(tempGoalList.get(i)
							.getGsrKpiWeightageDetails() == null ? null
							: tempGoalList.get(i).getGsrKpiWeightageDetails()
									.getGsrKpiGoalWeightage());
				} else {
					empGoalOutputBean.setKpiGoalWeightage(null);
				}

				GsrDatEmpFinalRatingBO empFinalRating = gsrDatEmpFinalRatingRepository
						.findByFkGsrFinalRatingSlabIdAndFkGsrFinalRatingEmpId(
								tempGoalList.get(i).getFkGsrSlabId(),
								tempGoalList.get(i).getFkGsrDatEmpId());
				if (empFinalRating != null) {
					empGoalOutputBean.setFinalRating(empFinalRating
							.getGsrFinalRating() + "");
					if (empFinalRating.getFinalRatingStatus() == 1)
						empGoalOutputBean.setFinalRatingStatus("Not Yet Rated");
					else if (empFinalRating.getFinalRatingStatus() == 2)
						empGoalOutputBean.setFinalRatingStatus("Accepted");
					else if (empFinalRating.getFinalRatingStatus() == 3)
						empGoalOutputBean.setFinalRatingStatus("Rejected");
					else if (empFinalRating.getFinalRatingStatus() == 4)
						empGoalOutputBean.setFinalRatingStatus("Resolved");
					else if (empFinalRating.getFinalRatingStatus() == 5)
						empGoalOutputBean.setFinalRatingStatus("Rated");
					else if (empFinalRating.getFinalRatingStatus() == 6) // Added
																			// by
																			// Shyam
																			// for
																			// adding
																			// of
																			// new
																			// status
						empGoalOutputBean.setFinalRatingStatus("Reopened");
					empGoalOutputBean.setIsOverridden(empFinalRating
							.getIsOverriddenFlag());
					empGoalOutputBean
							.setRatingAcceptedRejectedDate(empFinalRating
									.getRatingAcceptOrRejectDate());
					empGoalOutputBean.setFinalRatingModifiedDate(new Date());
				} else {
					empGoalOutputBean.setFinalRating("");
					empGoalOutputBean.setFinalRatingStatus("Not Yet Rated");
					empGoalOutputBean.setIsOverridden("");
					empGoalOutputBean.setRatingAcceptedRejectedDate(null);
				}

				if (tempGoalList.get(i).getEmpGoalKpiWeightageId() > 0) {

					List<GsrDatEmpGoalBO> goalList = empGoalsMap
							.get(tempGoalList.get(i).getEmpGoalKpiWeightageId());

					empGoalOutputBean.setGoalName(tempGoalList.get(i)
							.getGsrDatEmpGoalName());
					empGoalOutputBean.setGoalWeightage(tempGoalList.get(i)
							.getGsrGoalWeightage() + "");
					empGoalOutputBean.setGoalDuration(tempGoalList.get(i)
							.getGsrSlabBO_slab_id().getGsrSlabName());
					empGoalOutputBean.setMgrId(tempGoalList.get(i)
							.getFkGsrDatMgrId());
					empGoalOutputBean.setMgrName(getEmpNameByEmpId(tempGoalList
							.get(i).getFkGsrDatMgrId()));
					empGoalOutputBean.setGoalDescription(tempGoalList.get(i)
							.getGsrGoalDescription());
					empGoalOutputBean.setGoalMeasurement(tempGoalList.get(i)
							.getGsrGoalMeasurement());
					empGoalOutputBean.setSelfRating(tempGoalList.get(i)
							.getGsrGoalSelfRating() + "");
					empGoalOutputBean.setAccomplishments(tempGoalList.get(i)
							.getGsrGoalAccomplishment());
					empGoalOutputBean.setMgrFeedback(tempGoalList.get(i)
							.getGsrGoalMgrFeedback());
					empGoalOutputBean.setMgrRating(tempGoalList.get(i)
							.getGsrGoalMgrRating() + "");
					empGoalOutputBean.setSlabId(tempGoalList.get(i)
							.getFkGsrSlabId());
					empGoalOutputBean.setSlabStartDate(tempGoalList.get(i)
							.getGsrGoalStartDate());// added
					empGoalOutputBean.setSlabEndDate(tempGoalList.get(i)
							.getGsrGoalEndDate());
					empGoalOutputBean.setGoalStatusId(tempGoalList.get(i)
							.getFkGsrGoalStatus());
					empGoalOutputBean.setGoalStatusName(tempGoalList.get(i)
							.getGsrMasGoalStatusBOgoalstatus()
							.getGsrGoalStatusName());
					empGoalOutputBean.setGoalId(tempGoalList.get(i)
							.getPkGsrDatEmpGoalId());
					empGoalOutputBean.setKpiGoalId(tempGoalList.get(i)
							.getEmpGoalKpiWeightageId());
					empGoalOutputBean.setGsrGoalModifiedBy(tempGoalList.get(i)
							.getGsrGoalModifiedBy());
					empGoalOutputBean.setGsrGoalModifiedDate(new Date());
					if (tempGoalList.get(i).getGsrGoalClosedBy() != null)
						empGoalOutputBean
								.setClosedByName(getEmpNameByEmpId(tempGoalList
										.get(i).getGsrGoalClosedBy()));
					else
						empGoalOutputBean.setClosedByName(null);

					for (GsrDatEmpGoalBO gsrDatEmpGoalBO : goalList) {
						if (gsrDatEmpGoalBO.getFkGsrGoalStatus() != goalRejectedStatus)
							feedbackList
									.add(populateFeedbackDetails(gsrDatEmpGoalBO));
						else
							continue;
					}
					empGoalOutputBean.setFeedbackList(feedbackList);
				} else {
					feedbackList.add(populateFeedbackDetails(tempGoalList
							.get(i)));
					empGoalOutputBean.setFeedbackList(feedbackList);
				}

				// Added by Shyam for RM change scenario
				// Check in a slab whether employee have two different manager
				// id.
				List<GsrDatEmpGoalBO> empGoalList = new ArrayList<GsrDatEmpGoalBO>();
				try {
					empGoalList = gsrDatEmpGoalRepository
							.getAllEnteredGsrGoalsOfASlab(tempGoalList.get(i)
									.getFkGsrSlabId().shortValue(),
									tempGoalList.get(i).getFkGsrDatEmpId()
											.intValue());
				} catch (Exception ex) {
					throw new CommonCustomException(
							"Unable to get all entered goals detail. Kindly touch with "
									+ "Nucleus Support team for this issue.");
				}

				int tempGoalMgrId = 0;
				if (!empGoalList.isEmpty()) {
					for (GsrDatEmpGoalBO gsrDatEmpGoalBO4 : empGoalList) {
						if (tempGoalMgrId == 0) {
							tempGoalMgrId = gsrDatEmpGoalBO4.getFkGsrDatMgrId()
									.intValue();
						} else {
							if (tempGoalMgrId != gsrDatEmpGoalBO4
									.getFkGsrDatMgrId().intValue()) {
								otherMgrIdIsPresentIsGoal = true;
								break;
							}
						}
					}
				}
				// End of addition by Shyam for RM change scenario
				empGoalOutputBean.setRmChangeStatus(otherMgrIdIsPresentIsGoal);// Added
																				// by
																				// Shyam

				DatEmpProfessionalDetailBO reporteeProfessionalDetail = new DatEmpProfessionalDetailBO();
				reporteeProfessionalDetail = employeeProfessionalRepository
						.findByFkMainEmpDetailId(tempGoalList.get(i)
								.getFkGsrDatEmpId().intValue());
				if (reporteeProfessionalDetail == null) {
					throw new CommonCustomException(
							"Unable to retrive your reportee's detail. Kindly touch with Nucleus Support team for this issue.");
				}
				empGoalOutputBean
						.setCurrentReportingMgrId(reporteeProfessionalDetail
								.getFkEmpReportingMgrId());

				empGoalsList.add(empGoalOutputBean);
			}

		} catch (Exception e) {
			LOG.error("Exception occured", e);
			e.printStackTrace();
		}
		LOG.endUsecase("view Employee Goals");
		return empGoalsList;
	}

	boolean isValidGoalId(int goalId) throws DataAccessException,
			CommonCustomException {
		GsrDatEmpDelegatedGoalBO goalInfo = new GsrDatEmpDelegatedGoalBO();
		LOG.startUsecase("accept delegated goals");
		boolean result = true;
		if (goalId < 0) {
			result = false;
			throw new CommonCustomException(
					"GoalId should not to be less than 1");
		} else {
			goalInfo = delegatedGoalRepository.findByFkGsrDatEmpGoalId(goalId);
			if (goalInfo == null) {
				result = false;
				throw new CommonCustomException(goalId
						+ "  Is an Invalid goal  id");
			} else {
				result = true;
			}
		}

		return result;

	}

	/**
	 * @author THBS
	 * @param acceptDelBean
	 * @return misResponse
	 * @throws DataAccessException
	 * @throws ParseException
	 * @throws CommonCustomException
	 * @Description: This method is used to accept the delegated goals.
	 */
	public MISResponse acceptOrRejectDelegatedGoals(
			GsrAcceptDelegatedGoalsBean acceptDelBean)
			throws DataAccessException, ParseException, CommonCustomException {

		LOG.startUsecase("accept Delegated Goals");
		MISResponse misResponse = new MISResponse();
		int count = 0;
		GsrDatEmpDelegatedGoalBO gsrDelegateGoalBO = new GsrDatEmpDelegatedGoalBO();

		List<Integer> goalId = acceptDelBean.getGoalId();
		Byte status = acceptDelBean.getStatus();
		boolean isvalidGoalid = true;

		if (goalId.isEmpty()) {
			throw new CommonCustomException("Delegated Goal Id cannot be empty");
		} else {
			for (int i = 0; i < goalId.size(); i++) {
				isvalidGoalid = isValidGoalId(goalId.get(i));
				if (isvalidGoalid) {

					gsrDelegateGoalBO = delegatedGoalRepository
							.findByFkGsrDatEmpGoalId(goalId.get(i));
					if (gsrDelegateGoalBO.getDelegationStatus() != 1) {
						throw new CommonCustomException(
								goalId.get(i)
										+ " goal id  delegation status should be in Waiting for approval.");
					}
				}
			}
			if (isvalidGoalid) {
				for (int i = 0; i < goalId.size(); i++) {

					System.out.println("Goal id:" + goalId.get(i));
					gsrDelegateGoalBO = delegatedGoalRepository
							.findByFkGsrDatEmpGoalId(goalId.get(i));
					if (gsrDelegateGoalBO.getDelegationStatus() == 1) {
						gsrDelegateGoalBO.setDelegationStatus(status);
						gsrDelegateGoalBO.setAcceptedOrRejectedDate(new Date());
						delegatedGoalRepository.save(gsrDelegateGoalBO);
						count++;

					}
					if (count > 0) {
						misResponse.setStatus(MISConstants.SUCCESS);
						misResponse.setStatusCode(HttpStatus.OK.value());
						misResponse.setMessage("Status changed successfully.");
					} else {
						misResponse.setStatus(MISConstants.SUCCESS);
						misResponse.setStatusCode(HttpStatus.OK.value());
						misResponse
								.setMessage("No delegated goals to accept/reject.");
					}

				}
			}
		}

		LOG.endUsecase("accept Delegated Goals");
		return misResponse;
	}

	/**
	 * @author THBS
	 * @param empId
	 * @return misResponse
	 * @throws DataAccessException
	 * @throws ParseException
	 * @throws CommonCustomException
	 * @Description: This method is used to force close the goals.
	 */

	public boolean forceCloseGoals(ForceClosedGoalsInputBean forceCloseGoals)
			throws DataAccessException, ParseException, CommonCustomException {
		// Added by Kamal Anand
		LOG.startUsecase("Entering forceCloseGoals");
		GsrDatEmpFinalRatingBO finalRating = null;
		boolean forceClosedStatus = false;
		List<GsrDatEmpGoalBO> empGoalsList = null;
		long ratedSum = 0;
		long rejectedSum = 0;
		long reopenSum = 0;

		try {
			empGoalsList = gsrDatEmpGoalRepository
					.findByfkGsrDatEmpIdAndFkGsrSlabId(
							forceCloseGoals.getEmpId(),
							forceCloseGoals.getSlabId());

			Optional<List<GsrDatEmpGoalBO>> empGoalDetails = Optional
					.fromNullable(empGoalsList);

			if (empGoalDetails.isPresent()) {
				for (GsrDatEmpGoalBO gsrDatEmpGoalBO : empGoalsList) {
					if (gsrDatEmpGoalBO.getFkGsrGoalStatus() == goalRatedStatus)
						ratedSum = ratedSum + 1;
					else if (gsrDatEmpGoalBO.getFkGsrGoalStatus() == goalReopenStatus)
						reopenSum = reopenSum + 1;
					else if (gsrDatEmpGoalBO.getFkGsrGoalStatus() == goalRejectedStatus)
						rejectedSum = rejectedSum + 1;
				}

				finalRating = gsrDatEmpFinalRatingRepository
						.findByFkGsrFinalRatingSlabIdAndFkGsrFinalRatingEmpId(
								forceCloseGoals.getSlabId(),
								forceCloseGoals.getEmpId());

				Stream<GsrDatEmpGoalBO> forceClosedStatusFilter = empGoalsList
						.stream()
						.filter(o -> o.getFkGsrGoalStatus() == goalForceClosedStatus);
				long forceClosedSum = forceClosedStatusFilter
						.collect(Collectors.counting());

				Optional<GsrDatEmpFinalRatingBO> finalRatingDetails = Optional
						.fromNullable(finalRating);

				if (finalRatingDetails.isPresent()) {
					if (forceClosedSum == empGoalsList.size()
							&& finalRating.getFinalRatingStatus() == finalRatingForceCloseStatus)
						throw new CommonCustomException(
								"Goals Already Resolved");

					if (finalRating.getFinalRatingStatus() == finalRatingRatedStatus
							|| finalRating.getFinalRatingStatus() == finalRatingRejectedStatus
							|| finalRating.getFinalRatingStatus() == finalRatingReopenStatus) {
						if ((reopenSum + rejectedSum + ratedSum) == empGoalsList
								.size()) {
							empGoalsList
									.forEach(goal -> {
										if (goal.getFkGsrGoalStatus() != goalRejectedStatus) {
											goal.setGsrGoalModifiedDate(new Date());
											goal.setGsrGoalModifiedBy(forceCloseGoals
													.getClosedByEmpId());
											goal.setGsrGoalClosedBy(forceCloseGoals
													.getClosedByEmpId());
											goal.setFkGsrGoalStatus(goalForceClosedStatus);

											gsrDatEmpGoalRepository.save(goal);
										}
									});
							finalRating
									.setFinalRatingStatus(finalRatingForceCloseStatus);
							finalRating.setClosedBy(forceCloseGoals
									.getClosedByEmpId());
							finalRating.setModifiedDate(new Date());
							gsrDatEmpFinalRatingRepository.save(finalRating);

							forceClosedStatus = true;

						} else
							throw new CommonCustomException(
									"Some of the goals are not yet rated");
					} else
						throw new CommonCustomException(
								"Only Rated and Rejected status goals can be resolved");
				} else
					throw new CommonCustomException(
							"Final Rating Details Not Found for this Employee Id and Slab Id");
			} else
				throw new CommonCustomException(
						"No Goals found for this employee Id and slab Id");
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase(" exiting forceCloseGoals");
		return forceClosedStatus;
		// End of Addition by Kamal Anand
	}

	/**
	 * @author THBS
	 * @return List<GsrKpiGoalBO>
	 * @throws DataAccessException
	 * @throws BusinessException
	 * @Description: This method is used to get the default goals of all
	 *               employees.
	 */
	public List<GsrDelegetedOutputBean> getDefaultGoals(int empId)
			throws DataAccessException, BusinessException {
		// Update by Annu as per new KPi requirements based on employee
		// level and BU
		List<GsrDelegetedOutputBean> listOutput = new ArrayList<GsrDelegetedOutputBean>();

		LOG.startUsecase("getDefaultGoals");
		try {

			List<GsrKpiWeightageBO> gsrKpiWeightages = gsrKpiWeightageRepos
					.getAllKpiGoalsWithWeightageForEmpLevel(empId);

			if (gsrKpiWeightages != null && !gsrKpiWeightages.isEmpty()) {

				for (GsrKpiWeightageBO gsrKpiWeightageBO : gsrKpiWeightages) {
					GsrKpiGoalBO gsrKpiGoalBO = defaultGoalsRepository
							.findByPkGsrKpiGoalId(gsrKpiWeightageBO
									.getFkGsrKpiGoalId());
					GsrDelegetedOutputBean gsrDelegatedOutputBean = new GsrDelegetedOutputBean();
					gsrDelegatedOutputBean.setFkGsrKpiGoalId(gsrKpiGoalBO
							.getPkGsrKpiGoalId());
					gsrDelegatedOutputBean.setGsrKpiGoalName(gsrKpiGoalBO
							.getGsrKpiGoalName());
					gsrDelegatedOutputBean
							.setGsrKpiGoalDescription(gsrKpiGoalBO
									.getGsrKpiGoalDescription());
					gsrDelegatedOutputBean
							.setGsrKpiGoalMeasurement(gsrKpiGoalBO
									.getGsrKpiGoalMeasurement());
					gsrDelegatedOutputBean.setGsrKpiGoalActiveFlag(gsrKpiGoalBO
							.getGsrkpigoalactiveflag());
					gsrDelegatedOutputBean
							.setGsrKpiGoalWeightage(gsrKpiWeightageBO
									.getGsrKpiGoalWeightage());
					gsrDelegatedOutputBean
							.setPkGsrKpiWeightageId(gsrKpiWeightageBO
									.getPkGsrKpiWeightageId());
					gsrDelegatedOutputBean.setFkGsrKpiGoalId(gsrKpiWeightageBO
							.getFkGsrKpiGoalId());
					gsrDelegatedOutputBean.setFkEmpLevelId(gsrKpiWeightageBO
							.getFkEmpLevelId());
					listOutput.add(gsrDelegatedOutputBean);
				}

			} else {
				throw new BusinessException(
						"Default weightages for this employee not found.");
			}

		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new BusinessException("Invalid empId", e);
		}
		LOG.endUsecase("getDefaultGoals");
		return listOutput;
	}

	/**
	 * @author THBS
	 * @param teamGoalsBean
	 * @return List<GsrDatEmpGoalBO>
	 * @throws DataAccessException
	 * @throws ParseException
	 * @Description : This method is used to view the team goals.
	 */
	@Deprecated
	public List<GsrDatEmpGoalBO> viewTeamGoals(
			GsrTeamGoalDetailsBean teamGoalsBean) throws DataAccessException,
			ParseException {
		List<GsrDatEmpGoalBO> gsrTeamGoalsList;
		LOG.startUsecase("view Team Goals");
		LOG.startUsecase(" start date");
		try {
			int temp1 = Integer.parseInt(teamGoalsBean.getStatus().toString());
			byte temp = (byte) temp1;

			Date truncatedStartDate = DateUtils.truncate(
					teamGoalsBean.getStartDate(), Calendar.DATE);
			Date truncatedEndDate = DateUtils.truncate(
					teamGoalsBean.getEndDate(), Calendar.DATE);

			gsrTeamGoalsList = teamGoalsrepository
					.findByFkGsrDatMgrIdAndFkGsrGoalStatusAndGsrGoalStartDateGreaterThanEqualAndGsrGoalEndDateLessThanEqual(
							teamGoalsBean.getMgrId(), temp, truncatedStartDate,
							truncatedEndDate);

		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new DataAccessException("Exception occured", e);
		}
		LOG.endUsecase("view Team Goals");
		return gsrTeamGoalsList;
	}

	/**
	 * @author THBS
	 * @param mgrId
	 * @return List<GsrDatEmpGoalBO>
	 * @throws DataAccessException
	 * @throws ParseException
	 * @Description : This method is used to view the reportees goals.
	 */
	public List<GsrDatEmpGoalBO> reporteesCloseGoals(int mgrId)
			throws DataAccessException, ParseException {

		List<GsrDatEmpGoalBO> gsrEmpGoalList;

		LOG.startUsecase("view reportees Goals");
		try {
			gsrEmpGoalList = reporteesGoalsrepository
					.findByFkGsrDatMgrId(mgrId);

		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new DataAccessException("Exception occured", e);
		}
		LOG.endUsecase("view reportees Goals");
		return gsrEmpGoalList;
	}

	boolean isValidEmpId(Integer empId) throws DataAccessException,
			CommonCustomException {
		DatEmpProfessionalDetailBO employeeInfo = new DatEmpProfessionalDetailBO();
		LOG.startUsecase("disagree Rating Status");
		boolean result = true;
		if (empId < 0) {
			result = false;
			throw new CommonCustomException(
					"Empid should not to be less than 1");
		} else {
			employeeInfo = commonService.getEmpoyeeDeatailsById(empId);

			if (employeeInfo == null) {
				result = false;
				throw new CommonCustomException("Invalid emp id");
			} else {
				result = true;
			}
		}

		return result;

	}


	/**
	 * @author THBS
	 * @param empId
	 *            and slabId
	 * @return gsrEmpFinalRating
	 * @throws DataAccessException
	 * @throws ParseException
	 * @throws ConfigException
	 * @throws BusinessException
	 * @throws CommonCustomException
	 * @Description: This method is used to give disagree rating from the
	 *               employees for a particular slab.
	 */
	public boolean disagreeRatingStatus(
			GsrDisagreeRatingBean gsrDisagreeRatingBean)
			throws DataAccessException, ParseException, ConfigException,
			BusinessException, CommonCustomException {
		boolean isUpdated = false;
		int output = 0;
		DatEmpProfessionalDetailBO empDetails = null;
		DatEmpProfessionalDetailBO rmDetails = null;
		DatEmpProfessionalDetailBO dmDetails = null;

		List<String> skipLevelManagerEmail = new ArrayList<String>();
		List<String> cc = new ArrayList<String>();
	   
		boolean isvalidEmpid = isValidEmpId(gsrDisagreeRatingBean.getEmpId());
		if (isvalidEmpid) {
			output = disagreeRatingRepository.updateFinalRatingStatusToRejected(
					gsrDisagreeRatingBean.getEmpId(),
					gsrDisagreeRatingBean.getSlabId());
			if (output != 0) {
				isUpdated = true;
				empDetails = getEmpProfessionalDetailsByEmpId(gsrDisagreeRatingBean
						.getEmpId());

				if (empDetails != null) {
					rmDetails = getEmpProfessionalDetailsByEmpId(empDetails
							.getFkEmpReportingMgrId());
					if (rmDetails != null)
						cc.add(rmDetails.getEmpProfessionalEmailId());
					dmDetails = getEmpProfessionalDetailsByEmpId(rmDetails
							.getFkEmpReportingMgrId());
					if (dmDetails != null)
						skipLevelManagerEmail.add(dmDetails
								.getEmpProfessionalEmailId());
				}
				
				if (rmDetails.getFkEmpReportingMgrId().equals(1) || empDetails.getFkEmpReportingMgrId().equals(1)){
					
					List<String> emailIdList = new ArrayList<String>();
					cc.clear();
					String hrMail = hrMailId;
					emailIdList.add(hrMail);
					skipLevelManagerEmail.add(hrMailId);
					cc.add(empDetails
							.getEmpProfessionalEmailId());
					cc.add(hrMailId);
					
					if(rmDetails.getFkEmpReportingMgrId().equals(1))
					{
						cc.add(rmDetails.getEmpProfessionalEmailId());
					}
					
					try {
						
					
						misEmailService.sendMail(emailIdList,cc,"Disagree Rating",disagreeRating.getPath(),
								getEmpNameByEmpId(dmDetails.getFkMainEmpDetailId()),
								getEmpNameByEmpId(rmDetails.getFkMainEmpDetailId()),
								getEmpNameByEmpId(empDetails.getFkMainEmpDetailId()));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				else
				{
					Map map1 = new HashMap();
					try {
						List<String> emailIdList = new ArrayList<String>();
						cc.clear();
						emailIdList.add(dmDetails.getEmpProfessionalEmailId());
						cc.add(hrMailId);
						cc.add(rmDetails.getEmpProfessionalEmailId());
						cc.add(empDetails
								.getEmpProfessionalEmailId());
						
						misEmailService.sendMail(emailIdList,cc,"Disagree Rating",disagreeRating.getPath(),
								getEmpNameByEmpId(dmDetails.getFkMainEmpDetailId()),
								getEmpNameByEmpId(rmDetails.getFkMainEmpDetailId()),
								getEmpNameByEmpId(empDetails.getFkMainEmpDetailId()));
					} catch (Exception e) {
						e.printStackTrace();
					}
				
				}
			} else {
				isUpdated = false;
			}
			LOG.endUsecase("disagree Rating Status");
		}
		return isUpdated;
	}

	public DatEmpProfessionalDetailBO getEmpProfessionalDetailsByEmpId(int empId) {
		DatEmpProfessionalDetailBO profDetails = null;
		try {
			profDetails = employeeProfessionalRepository
					.findByFkMainEmpDetailId(empId);
			if (profDetails != null) {
				return profDetails;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * <Description fetchPastGoalsForEmployee:> method is is used to fetch the
	 * details the GsrGoal repository method for the viewing the GsrGoals for
	 * the current year and for the past year in the data base. It will take the
	 * empId and passes it to the Repository for viewing the Record.
	 * 
	 * @param Integer
	 * @param empId
	 * 
	 * @param GsrDatEmpGoalBO
	 *            is the argument for this method.
	 * 
	 * @return a boolean value whether it is true or false.
	 * @throws GoalNotFoundException
	 *             ,BusinessException
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * 
	 */
	public List<GsrFetchPastGoalsForEmployeeOutputBean> fetchPastGoalsForEmployee(
			Integer empId) throws GoalNotFoundException, BusinessException,
			DataAccessException, CommonCustomException, GoalNotFoundException {
		LOG.startUsecase("Fetch Past Goals For Employee ID");
		List<GsrDatEmpGoalBO> viewGoal = new ArrayList<GsrDatEmpGoalBO>();
		DatEmpDetailBO empDetail = null;

		List<GsrFetchPastGoalsForEmployeeOutputBean> gsrGoalsList = new ArrayList<GsrFetchPastGoalsForEmployeeOutputBean>();
		List<ViewEmployeeGoalsOutputBean> empGoalOutputBeanList = new ArrayList<ViewEmployeeGoalsOutputBean>();
		List<GsrKpiWeightageBO> kpiGoalList = new ArrayList<GsrKpiWeightageBO>();
		List<GsrKpiGoalsBean> kpiGoals = new ArrayList<GsrKpiGoalsBean>();
		boolean isvalidEmpid = true;

		empDetail = empDetailsRepository.findByPkEmpId(empId);

		if (empDetail == null)
			throw new CommonCustomException("Employee Id not found.");
		else {

			viewGoal = gsrDatEmpGoalRepository.fetchGoalsByEmpId(empId);

			LOG.debug("output viewGoal " + viewGoal);
			//
			// if (viewGoal.size() == 0) {
			// throw new GoalNotFoundException("Goal does not exist");
			// } else {
			viewGoal.forEach(goal -> {
				ViewEmployeeGoalsOutputBean empGoalOutputBean = new ViewEmployeeGoalsOutputBean();
				empGoalOutputBean.setGoalName(goal.getGsrDatEmpGoalName());
				empGoalOutputBean.setGoalWeightage(goal.getGsrGoalWeightage()
						+ "");
				empGoalOutputBean.setGoalDuration(goal.getGsrSlabName());
				empGoalOutputBean.setMgrId(goal.getFkGsrDatMgrId());
				empGoalOutputBean.setMgrName(getEmpNameByEmpId(goal
						.getFkGsrDatMgrId()));
				empGoalOutputBean
						.setKpiGoalId(goal.getGsrKpiWeightageDetails() != null ? goal
								.getGsrKpiWeightageDetails()
								.getPkGsrKpiWeightageId() : 0);
				empGoalOutputBean.setKpiGoalWeightage(goal
						.getGsrKpiWeightageDetails() != null ? goal
						.getGsrKpiWeightageDetails().getGsrKpiGoalWeightage()
						: 0);
				empGoalOutputBean.setGoalDescription(goal
						.getGsrGoalDescription());
				empGoalOutputBean.setGoalMeasurement(goal
						.getGsrGoalMeasurement());
				empGoalOutputBean.setSelfRating(goal.getGsrGoalSelfRating()
						+ "");
				empGoalOutputBean.setAccomplishments(goal
						.getGsrGoalAccomplishment());
				empGoalOutputBean.setMgrFeedback(goal.getGsrGoalMgrFeedback());
				empGoalOutputBean.setMgrRating(goal.getGsrGoalMgrRating() + "");
				empGoalOutputBean.setSlabId(goal.getFkGsrSlabId());
				empGoalOutputBean.setGoalStatusId(goal.getFkGsrGoalStatus());
				empGoalOutputBean.setGoalId(goal.getPkGsrDatEmpGoalId());
				empGoalOutputBean.setEmpGoalKpiWeightageId(goal
						.getEmpGoalKpiWeightageId());
				empGoalOutputBean.setSlabYear(gsrSlabRepository
						.findByPkGsrSlabId(goal.getFkGsrSlabId())
						.getGsrSlabYear());
				empGoalOutputBean.setSlabShortName(gsrSlabRepository
						.findByPkGsrSlabId(goal.getFkGsrSlabId())
						.getGsrSlabShortName());
				empGoalOutputBean.setBuId(goal.getFkEmpGoalBuId()); // added by
																	// pratibha
				DatEmpPersonalDetailBO empPersonalDetails = new DatEmpPersonalDetailBO();
				empPersonalDetails = employeePersonalDetailsRepository
						.findByFkEmpDetailId(goal.getFkGsrDatEmpId());
				if (empPersonalDetails != null) {

					empGoalOutputBean.setEmpId(empPersonalDetails
							.getFkEmpDetailId());
					empGoalOutputBean.setEmpName(empPersonalDetails
							.getEmpFirstName()
							+ " "
							+ empPersonalDetails.getEmpLastName());
				}

				GsrDatEmpFinalRatingBO empFinalRating = gsrDatEmpFinalRatingRepository
						.findByFkGsrFinalRatingSlabIdAndFkGsrFinalRatingEmpId(
								goal.getFkGsrSlabId(), goal.getFkGsrDatEmpId());
				if (empFinalRating != null) {
					empGoalOutputBean.setFinalRating(empFinalRating
							.getGsrFinalRating() + "");
					if (empFinalRating.getFinalRatingStatus() != null) {
						if (empFinalRating.getFinalRatingStatus() == 1)
							empGoalOutputBean
									.setFinalRatingStatus("Not Yet Rated");
						if (empFinalRating.getFinalRatingStatus() == 2)
							empGoalOutputBean.setFinalRatingStatus("Accepted");
						if (empFinalRating.getFinalRatingStatus() == 3)
							empGoalOutputBean.setFinalRatingStatus("Rejected");
						if (empFinalRating.getFinalRatingStatus() == 4)
							empGoalOutputBean
									.setFinalRatingStatus("Force Closed");
						if (empFinalRating.getFinalRatingStatus() == 5)
							empGoalOutputBean.setFinalRatingStatus("Rated");
					}
					empGoalOutputBean.setIsOverridden(empFinalRating
							.getIsOverriddenFlag());
					empGoalOutputBean
							.setRatingAcceptedRejectedDate(empFinalRating
									.getRatingAcceptOrRejectDate());
					empGoalOutputBean.setOverridenBy(empFinalRating
							.getOverriddenBy());
					if (empFinalRating.getOverriddenBy() != null)
						empGoalOutputBean
								.setOverridenByName(getEmpNameByEmpId(empFinalRating
										.getOverriddenBy()));
					else
						empGoalOutputBean.setOverridenByName("");
					empGoalOutputBean.setFinalRatingModifiedDate(empFinalRating
							.getModifiedDate());
				} else {
					empGoalOutputBean.setFinalRating("");
					empGoalOutputBean.setFinalRatingStatus("Not Yet Rated");
					empGoalOutputBean.setIsOverridden("");
					empGoalOutputBean.setRatingAcceptedRejectedDate(null);
					empGoalOutputBean.setOverridenBy(null);
					empGoalOutputBean.setOverridenByName("");
					empGoalOutputBean.setFinalRatingModifiedDate(null);
				}
				empGoalOutputBeanList.add(empGoalOutputBean);

			});

			// }
		}
		GsrFetchPastGoalsForEmployeeOutputBean outputBean = new GsrFetchPastGoalsForEmployeeOutputBean();

		outputBean.setPastGoals(empGoalOutputBeanList);
		kpiGoalList = gsrKpiWeightageRepos
				.getAllKpiGoalsWithWeightageForEmpLevel(empId);
		kpiGoalList.forEach(kpiBo -> {
			GsrKpiGoalsBean kpiBean = new GsrKpiGoalsBean();
			kpiBean.setFkEmpLevelId(kpiBo.getFkEmpLevelId());
			kpiBean.setFkGsrKpiGoalId(kpiBo.getFkGsrKpiGoalId());
			kpiBean.setGsrKpiBuId(kpiBo.getGsrKpiBuId());
			kpiBean.setGsrKpiGoalName(goalRepository.findByPkGsrKpiGoalId(
					kpiBo.getFkGsrKpiGoalId()).getGsrKpiGoalName());
			kpiBean.setGsrKpiGoalWeightage(kpiBo.getGsrKpiGoalWeightage());
			kpiBean.setPkGsrKpiWeightageId(kpiBo.getPkGsrKpiWeightageId());
			kpiGoals.add(kpiBean);
		});
		outputBean.setKpiGoals(kpiGoals);
		// kpiGoals.clear();
		gsrGoalsList.add(outputBean);

		LOG.endUsecase("Fetch Past Goals For Employee ID");

		return gsrGoalsList;

	}

	List<GsrKpiGoalsBean> kpiGoals;

	/**
	 * 
	 * <Description fetchPastGoalsForEmployeeList:> method is is used to fetch
	 * the details the GsrGoal repository method for the viewing the GsrGoals
	 * for the current year and for the past year in the data base. It will take
	 * the empId and passes it to the Repository for viewing the Record.
	 * 
	 * @param Integer
	 * @param List
	 *            <empId>
	 * 
	 * @param GsrDatEmpGoalBO
	 *            is the argument for this method.
	 * 
	 * @return a boolean value whether it is true or false.
	 * @throws GoalNotFoundException
	 *             ,BusinessException
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * 
	 */

	public List<GsrFetchPastGoalsForEmployeeOutputBean> fetchPastGoalsForEmployeeList(
			FetchPastGoalsForEmployeeListInputBean inputBean)
			throws GoalNotFoundException, BusinessException,
			DataAccessException, CommonCustomException {
		LOG.startUsecase("Fetch Past Goals For Employee ID");

		List<GsrDatEmpGoalBO> viewGoal = new ArrayList<GsrDatEmpGoalBO>();

		List<GsrFetchPastGoalsForEmployeeOutputBean> gsrGoalsList = new ArrayList<GsrFetchPastGoalsForEmployeeOutputBean>();
		List<ViewEmployeeGoalsOutputBean> empGoalOutputBeanList = new ArrayList<ViewEmployeeGoalsOutputBean>();
		List<GsrKpiWeightageBO> kpiGoalList = new ArrayList<GsrKpiWeightageBO>();
		List<GsrKpiGoalsBean> kpiGoals = new ArrayList<GsrKpiGoalsBean>();
		Integer invalidEmpId = null;
		boolean isvalidEmpid = false;
		try {

			for (Integer empId : inputBean.getEmpId()) {
				DatEmpProfessionalDetailBO employeeInfo = null;
				employeeInfo = getEmpProfessionalDetailsByEmpId(empId);
				if (employeeInfo == null) {
					isvalidEmpid = false;
					invalidEmpId = empId;
					break;
				} else {
					isvalidEmpid = true;
				}
			}

			if (isvalidEmpid) {

				viewGoal = gsrDatEmpGoalRepository
						.fetchGoalsByEmpIdList(inputBean.getEmpId());

				if (viewGoal == null) {
					throw new GoalNotFoundException("Goal does not exist");
				} else {
					viewGoal.forEach(goal -> {
						ViewEmployeeGoalsOutputBean empGoalOutputBean = new ViewEmployeeGoalsOutputBean();
						empGoalOutputBean.setGoalName(goal
								.getGsrDatEmpGoalName());
						empGoalOutputBean.setGoalWeightage(goal
								.getGsrGoalWeightage() + "");
						empGoalOutputBean.setGoalDuration(goal.getGsrSlabName());
						empGoalOutputBean.setMgrId(goal.getFkGsrDatMgrId());
						empGoalOutputBean.setMgrName(getEmpNameByEmpId(goal
								.getFkGsrDatMgrId()));
						empGoalOutputBean.setKpiGoalId(goal
								.getGsrKpiWeightageDetails() != null ? goal
								.getGsrKpiWeightageDetails()
								.getPkGsrKpiWeightageId() : 0);
						empGoalOutputBean.setKpiGoalWeightage(goal
								.getGsrKpiWeightageDetails() != null ? goal
								.getGsrKpiWeightageDetails()
								.getGsrKpiGoalWeightage() : 0);
						empGoalOutputBean.setGoalDescription(goal
								.getGsrGoalDescription());
						empGoalOutputBean.setGoalMeasurement(goal
								.getGsrGoalMeasurement());
						empGoalOutputBean.setSelfRating(goal
								.getGsrGoalSelfRating() + "");
						empGoalOutputBean.setAccomplishments(goal
								.getGsrGoalAccomplishment());
						empGoalOutputBean.setMgrFeedback(goal
								.getGsrGoalMgrFeedback());
						empGoalOutputBean.setMgrRating(goal
								.getGsrGoalMgrRating() + "");
						empGoalOutputBean.setSlabId(goal.getFkGsrSlabId());
						empGoalOutputBean.setGoalStatusId(goal
								.getFkGsrGoalStatus());
						empGoalOutputBean.setGoalId(goal.getPkGsrDatEmpGoalId());
						empGoalOutputBean.setEmpGoalKpiWeightageId(goal
								.getEmpGoalKpiWeightageId());
						empGoalOutputBean.setSlabYear(gsrSlabRepository
								.findByPkGsrSlabId(goal.getFkGsrSlabId())
								.getGsrSlabYear());
						empGoalOutputBean.setSlabShortName(gsrSlabRepository
								.findByPkGsrSlabId(goal.getFkGsrSlabId())
								.getGsrSlabShortName());
						empGoalOutputBean.setBuId(goal.getFkEmpGoalBuId()); // added
																			// by
																			// pratibha
						DatEmpPersonalDetailBO empPersonalDetails = new DatEmpPersonalDetailBO();
						empPersonalDetails = employeePersonalDetailsRepository
								.findByFkEmpDetailId(goal.getFkGsrDatEmpId());
						if (empPersonalDetails != null) {

							empGoalOutputBean.setEmpId(empPersonalDetails
									.getFkEmpDetailId());
							empGoalOutputBean.setEmpName(empPersonalDetails
									.getEmpFirstName()
									+ " "
									+ empPersonalDetails.getEmpLastName());
						}

						GsrDatEmpFinalRatingBO empFinalRating = gsrDatEmpFinalRatingRepository
								.findByFkGsrFinalRatingSlabIdAndFkGsrFinalRatingEmpId(
										goal.getFkGsrSlabId(),
										goal.getFkGsrDatEmpId());
						if (empFinalRating != null) {
							empGoalOutputBean.setFinalRating(empFinalRating
									.getGsrFinalRating() + "");
							if (empFinalRating.getFinalRatingStatus() != null) {
								if (empFinalRating.getFinalRatingStatus() == 1)
									empGoalOutputBean
											.setFinalRatingStatus("Not Yet Rated");
								if (empFinalRating.getFinalRatingStatus() == 2)
									empGoalOutputBean
											.setFinalRatingStatus("Accepted");
								if (empFinalRating.getFinalRatingStatus() == 3)
									empGoalOutputBean
											.setFinalRatingStatus("Rejected");
								if (empFinalRating.getFinalRatingStatus() == 4)
									empGoalOutputBean
											.setFinalRatingStatus("Force Closed");
								if (empFinalRating.getFinalRatingStatus() == 5)
									empGoalOutputBean
											.setFinalRatingStatus("Rated");
							}
							empGoalOutputBean.setIsOverridden(empFinalRating
									.getIsOverriddenFlag());
							empGoalOutputBean
									.setRatingAcceptedRejectedDate(empFinalRating
											.getRatingAcceptOrRejectDate());
							empGoalOutputBean.setOverridenBy(empFinalRating
									.getOverriddenBy());
							if (empFinalRating.getOverriddenBy() != null)
								empGoalOutputBean
										.setOverridenByName(getEmpNameByEmpId(empFinalRating
												.getOverriddenBy()));
							else
								empGoalOutputBean.setOverridenByName("");
							empGoalOutputBean
									.setFinalRatingModifiedDate(empFinalRating
											.getModifiedDate());
						} else {
							empGoalOutputBean.setFinalRating("");
							empGoalOutputBean
									.setFinalRatingStatus("Not Yet Rated");
							empGoalOutputBean.setIsOverridden("");
							empGoalOutputBean
									.setRatingAcceptedRejectedDate(null);
							empGoalOutputBean.setOverridenBy(null);
							empGoalOutputBean.setOverridenByName("");
							empGoalOutputBean.setFinalRatingModifiedDate(null);
						}
						empGoalOutputBeanList.add(empGoalOutputBean);

					});

				}
				GsrFetchPastGoalsForEmployeeOutputBean outputBean = new GsrFetchPastGoalsForEmployeeOutputBean();

				outputBean.setPastGoals(empGoalOutputBeanList);
				kpiGoalList = gsrKpiWeightageRepos
						.getAllKpiGoalsWithWeightageForEmpLevelList(inputBean
								.getEmpId());
				/*
				 * if(kpiGoalList.size() > 0) { GsrKpiGoalsBean kpiBean = new
				 * GsrKpiGoalsBean();
				 * kpiBean.setFkEmpLevelId(kpiGoalList.get(0).
				 * getFkEmpLevelId());
				 * kpiBean.setFkGsrKpiGoalId(kpiGoalList.get(
				 * 0).getFkGsrKpiGoalId());
				 * kpiBean.setGsrKpiBuId(kpiGoalList.get(0).getGsrKpiBuId());
				 * kpiBean.setGsrKpiGoalName(goalRepository
				 * .findByPkGsrKpiGoalId(kpiGoalList.get(0).getFkGsrKpiGoalId())
				 * .getGsrKpiGoalName());
				 * kpiBean.setGsrKpiGoalWeightage(kpiGoalList.get(0)
				 * .getGsrKpiGoalWeightage());
				 * kpiBean.setPkGsrKpiWeightageId(kpiGoalList.get(0)
				 * .getPkGsrKpiWeightageId()); kpiGoals.add(kpiBean); }
				 */
				kpiGoalList
						.forEach(kpiBo -> {
							GsrKpiGoalsBean kpiBean = new GsrKpiGoalsBean();
							kpiBean.setFkEmpLevelId(kpiBo.getFkEmpLevelId());
							kpiBean.setFkGsrKpiGoalId(kpiBo.getFkGsrKpiGoalId());
							kpiBean.setGsrKpiBuId(kpiBo.getGsrKpiBuId());
							kpiBean.setGsrKpiGoalName(goalRepository
									.findByPkGsrKpiGoalId(
											kpiBo.getFkGsrKpiGoalId())
									.getGsrKpiGoalName());
							kpiBean.setGsrKpiGoalWeightage(kpiBo
									.getGsrKpiGoalWeightage());
							kpiBean.setPkGsrKpiWeightageId(kpiBo
									.getPkGsrKpiWeightageId());
							System.out.println("kpiGoals.contains(kpiBean.getGsrKpiGoalName().trim()):"
									+ kpiGoals.contains(kpiBean
											.getGsrKpiGoalName().trim()));

							List<GsrKpiGoalsBean> kpiGoalNameList = kpiGoals
									.stream()
									.filter(p -> p
											.getGsrKpiGoalName()
											.trim()
											.equals((kpiBean
													.getGsrKpiGoalName().trim())))
									.collect(Collectors.toList());
							if (kpiGoalNameList.size() == 0)
								kpiGoals.add(kpiBean);
							kpiGoalNameList.clear();
							/*
							 * if(!kpiGoals.contains(kpiBean.getGsrKpiGoalName().
							 * trim())) kpiGoals.add(kpiBean);
							 */
						});
				outputBean.setKpiGoals(kpiGoals);
				// kpiGoals.clear();
				gsrGoalsList.add(outputBean);

			} else {
				throw new CommonCustomException("Invalid empId: "
						+ invalidEmpId);
			}

		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("Fetch Past Goals For Employee ID");

		return gsrGoalsList;

	}

	/**
	 * 
	 * <Description fetchPastGoalsForRM:> method is is used to fetch the details
	 * the GsrGoal repository method for the viewing the GsrGoals for the
	 * current year and for the past year for all his/her reportees in the data
	 * base. It will take the mgrId and passes it to the Repository for viewing
	 * the Record.
	 * 
	 * @param Integer
	 * @param mgrId
	 * 
	 * @param GsrDatEmpGoalBO
	 *            is the argument for this method.
	 * 
	 * @return a boolean value whether it is true or false.
	 * @throws GoalNotFoundException
	 *             ,BusinessException
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * 
	 */
	public List<GsrFetchPastGoalsForEmployeeOutputBean> fetchPastGoalsForReportingManager(
			Integer mgrId) throws GoalNotFoundException, BusinessException,
			DataAccessException, CommonCustomException {
		LOG.startUsecase("Fetch Past Goals For Reporting Manager ID");
		List<GsrDatEmpGoalBO> viewGoal = new ArrayList<GsrDatEmpGoalBO>();
		DatEmpDetailBO empDetail = null;

		List<GsrFetchPastGoalsForEmployeeOutputBean> gsrGoalsList = new ArrayList<GsrFetchPastGoalsForEmployeeOutputBean>();
		List<ViewEmployeeGoalsOutputBean> empGoalOutputBeanList = new ArrayList<ViewEmployeeGoalsOutputBean>();
		List<GsrKpiWeightageBO> kpiGoalBoList = new ArrayList<GsrKpiWeightageBO>();
		kpiGoals = new ArrayList<GsrKpiGoalsBean>();

		boolean isvalidEmpid = true;
		// String mgrIds;
		empDetail = empDetailsRepository.findByPkEmpId(mgrId);

		if (empDetail == null)
			throw new CommonCustomException(" Id not found.");
		else {

			viewGoal = gsrDatEmpGoalRepository.fetchGoalsByMgrId(mgrId);

			LOG.debug("output viewGoal " + viewGoal);

			if (viewGoal.size() == 0) {
				// throw new GoalNotFoundException("Goal does not exist");
			} else {
				viewGoal.forEach(goal -> {
					ViewEmployeeGoalsOutputBean empGoalOutputBean = new ViewEmployeeGoalsOutputBean();
					empGoalOutputBean.setGoalName(goal.getGsrDatEmpGoalName());
					empGoalOutputBean.setGoalWeightage(goal
							.getGsrGoalWeightage() + "");
					empGoalOutputBean.setGoalDuration(goal.getGsrSlabName());
					empGoalOutputBean.setMgrId(goal.getFkGsrDatMgrId());
					empGoalOutputBean.setMgrName(getEmpNameByEmpId(goal
							.getFkGsrDatMgrId()));
					empGoalOutputBean.setKpiGoalId(goal
							.getGsrKpiWeightageDetails() != null ? goal
							.getGsrKpiWeightageDetails()
							.getPkGsrKpiWeightageId() : 0);
					empGoalOutputBean.setKpiGoalWeightage(goal
							.getGsrKpiWeightageDetails() != null ? goal
							.getGsrKpiWeightageDetails()
							.getGsrKpiGoalWeightage() : 0);
					empGoalOutputBean.setGoalDescription(goal
							.getGsrGoalDescription());
					empGoalOutputBean.setGoalMeasurement(goal
							.getGsrGoalMeasurement());
					empGoalOutputBean.setSelfRating(goal.getGsrGoalSelfRating()
							+ "");
					empGoalOutputBean.setAccomplishments(goal
							.getGsrGoalAccomplishment());
					empGoalOutputBean.setMgrFeedback(goal
							.getGsrGoalMgrFeedback());
					empGoalOutputBean.setMgrRating(goal.getGsrGoalMgrRating()
							+ "");
					empGoalOutputBean.setSlabId(goal.getFkGsrSlabId());
					empGoalOutputBean.setGoalStatusId(goal.getFkGsrGoalStatus());
					empGoalOutputBean.setGoalId(goal.getPkGsrDatEmpGoalId());
					empGoalOutputBean.setEmpGoalKpiWeightageId(goal
							.getEmpGoalKpiWeightageId());
					empGoalOutputBean.setSlabYear(gsrSlabRepository
							.findByPkGsrSlabId(goal.getFkGsrSlabId())
							.getGsrSlabYear());
					empGoalOutputBean.setSlabShortName(gsrSlabRepository
							.findByPkGsrSlabId(goal.getFkGsrSlabId())
							.getGsrSlabShortName());
					empGoalOutputBean.setBuId(goal.getFkEmpGoalBuId()); // added
																		// by
																		// pratibha
					DatEmpPersonalDetailBO empPersonalDetails = new DatEmpPersonalDetailBO();
					empPersonalDetails = employeePersonalDetailsRepository
							.findByFkEmpDetailId(goal.getFkGsrDatEmpId());
					if (empPersonalDetails != null) {

						empGoalOutputBean.setEmpId(empPersonalDetails
								.getFkEmpDetailId());
						empGoalOutputBean.setEmpName(empPersonalDetails
								.getEmpFirstName()
								+ " "
								+ empPersonalDetails.getEmpLastName());
					}

					GsrDatEmpFinalRatingBO empFinalRating = gsrDatEmpFinalRatingRepository
							.findByFkGsrFinalRatingSlabIdAndFkGsrFinalRatingEmpId(
									goal.getFkGsrSlabId(),
									goal.getFkGsrDatEmpId());
					if (empFinalRating != null) {
						empGoalOutputBean.setFinalRating(empFinalRating
								.getGsrFinalRating() + "");
						if (empFinalRating.getFinalRatingStatus() != null) {
							if (empFinalRating.getFinalRatingStatus() == 1)
								empGoalOutputBean
										.setFinalRatingStatus("Not Yet Rated");
							if (empFinalRating.getFinalRatingStatus() == 2)
								empGoalOutputBean
										.setFinalRatingStatus("Accepted");
							if (empFinalRating.getFinalRatingStatus() == 3)
								empGoalOutputBean
										.setFinalRatingStatus("Rejected");
							if (empFinalRating.getFinalRatingStatus() == 4)
								empGoalOutputBean
										.setFinalRatingStatus("Force Closed");
							if (empFinalRating.getFinalRatingStatus() == 5)
								empGoalOutputBean.setFinalRatingStatus("Rated");
						}
						empGoalOutputBean.setIsOverridden(empFinalRating
								.getIsOverriddenFlag());
						empGoalOutputBean
								.setRatingAcceptedRejectedDate(empFinalRating
										.getRatingAcceptOrRejectDate());
						empGoalOutputBean.setOverridenBy(empFinalRating
								.getOverriddenBy());
						if (empFinalRating.getOverriddenBy() != null)
							empGoalOutputBean
									.setOverridenByName(getEmpNameByEmpId(empFinalRating
											.getOverriddenBy()));
						else
							empGoalOutputBean.setOverridenByName("");
						empGoalOutputBean
								.setFinalRatingModifiedDate(empFinalRating
										.getModifiedDate());
					} else {
						empGoalOutputBean.setFinalRating("");
						empGoalOutputBean.setFinalRatingStatus("Not Yet Rated");
						empGoalOutputBean.setIsOverridden("");
						empGoalOutputBean.setRatingAcceptedRejectedDate(null);
						empGoalOutputBean.setOverridenBy(null);
						empGoalOutputBean.setOverridenByName("");
						empGoalOutputBean.setFinalRatingModifiedDate(null);
					}
					empGoalOutputBeanList.add(empGoalOutputBean);

				});

			}
		}
		GsrFetchPastGoalsForEmployeeOutputBean outputBean = new GsrFetchPastGoalsForEmployeeOutputBean();

		outputBean.setPastGoals(empGoalOutputBeanList);
		kpiGoalBoList = gsrKpiWeightageRepos
				.getAllKpiGoalsWithWeightageForEmpLevel(mgrId);
		kpiGoalBoList.forEach(kpiBo -> {
			GsrKpiGoalsBean kpiBean = new GsrKpiGoalsBean();
			kpiBean.setFkEmpLevelId(kpiBo.getFkEmpLevelId());
			kpiBean.setFkGsrKpiGoalId(kpiBo.getFkGsrKpiGoalId());
			kpiBean.setGsrKpiBuId(kpiBo.getGsrKpiBuId());
			kpiBean.setGsrKpiGoalName(goalRepository.findByPkGsrKpiGoalId(
					kpiBo.getFkGsrKpiGoalId()).getGsrKpiGoalName());
			kpiBean.setGsrKpiGoalWeightage(kpiBo.getGsrKpiGoalWeightage());
			kpiBean.setPkGsrKpiWeightageId(kpiBo.getPkGsrKpiWeightageId());
			kpiGoals.add(kpiBean);
		});
		outputBean.setKpiGoals(kpiGoals);
		gsrGoalsList.add(outputBean);
		// kpiGoals.clear();
		LOG.endUsecase("Fetch Past Goals For Reporting Manager ID");
		return gsrGoalsList;
	}

	/**
	 * @author THBS
	 * @param GsrReopenGoalsByHrBean
	 * @return misResponse
	 * @throws DataAccessException
	 * @throws ParseException
	 * @throws CommonCustomException
	 * @Description: This method is used to Re-open the GsrGoals by the HR.
	 */
	public Integer reopenGsrGoal(GsrReopenGoalsByHrBean gsrReopenGoalsByHrBean)
			throws DataAccessException, ParseException, CommonCustomException {

		LOG.startUsecase("Re-open the GsrGoals by the HR");
		MISResponse misResponse = new MISResponse();
		GsrDatEmpGoalBO gsrDatEmpGoalBO = new GsrDatEmpGoalBO();
		GsrDatEmpFinalRatingBO gsrDatEmpFinalRatingBO = new GsrDatEmpFinalRatingBO();
		List<Integer> empIDList = new ArrayList<Integer>();
		String[] empIdArray = empIds.split(",");
		Integer i = 0;
		for (String s : empIdArray)
			empIDList.add(Integer.parseInt(s));
		i = gsrDatEmpGoalRepository.updateReopenGoal(
				gsrReopenGoalsByHrBean.getEmpId(),
				gsrReopenGoalsByHrBean.getSlabId(),
				gsrReopenGoalsByHrBean.getModifiedBy(), empIDList);
		LOG.startUsecase("i  1:" + i);
		i = i
				+ gsrDatEmpFinalRatingRepository
						.updateReopenGoalForFinalRating(
								gsrReopenGoalsByHrBean.getEmpId(),
								gsrReopenGoalsByHrBean.getSlabId(), empIDList);

		LOG.endUsecase("Re-open the GsrGoals by the HR");
		return i;
	}

	/**
	 * 
	 * @param feedbackBean
	 *            bean contains the input details are to be used in the service.
	 * @return
	 * @throws CommonCustomException
	 * @throws Exception
	 * @Description: This service is used to enter feedback for manager
	 */
	public boolean enterFeedbackForManager(
			GsrEnterFeedbackForManagerBean feedbackBean)
			throws CommonCustomException {
		LOG.startUsecase("enterFeedback");
		boolean feedback = false;
		/*
		 * final int GSR_STATUS_ACCOMPLISHMENT_ENTERED = 3; final int
		 * GSR_STATUS_MANAGER_FEEDBACK = 8; final int GSR_STATUS_REOPEN = 9;
		 */
		/*
		 * @javadoc : If all goals have been rated, then value will be zero (0).
		 */
		GsrDatEmpGoalBO empGoalFeedback = new GsrDatEmpGoalBO();
		try {
			empGoalFeedback = empGoalRepository
					.findByPkGsrDatEmpGoalId(feedbackBean
							.getPkGsrDatEmpGoalId());
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Failed to fetch goal details from goal id. "
							+ ex.getMessage());
		}
		if (empGoalFeedback != null) {
			if (empGoalFeedback.getEmpGoalKpiWeightageId() > 0) {
				if (empGoalFeedback.getFkGsrGoalStatus() == goalAccomplishmentEnteredStatus
						|| empGoalFeedback.getFkGsrGoalStatus() == goalManagerFeedbackStatus
						|| empGoalFeedback.getFkGsrGoalStatus() == goalReopenStatus) {
					if (empGoalFeedback.getFkGsrDatMgrId().intValue() != feedbackBean
							.getManagerId().intValue()) {
						if (empGoalFeedback.getFkGsrDelegatedGoalId() != null) {
							GsrDatEmpDelegatedGoalBO delegatedKpiGoalDetails = new GsrDatEmpDelegatedGoalBO();
							try {
								delegatedKpiGoalDetails = delegatedGoalRepository
										.findByPkGsrDelegatedGoalId(empGoalFeedback
												.getFkGsrDelegatedGoalId());
							} catch (Exception ex) {
								throw new CommonCustomException(
										"Failed to fetch delegated kpi goal detail from "
												+ "delegated goal id. "
												+ ex.getMessage());
							}

							if (delegatedKpiGoalDetails != null) {
								if (delegatedKpiGoalDetails
										.getFkMgrIdDelegatedTo().intValue() == feedbackBean
										.getManagerId().intValue()) {
									empGoalFeedback
											.setGsrGoalModifiedBy(feedbackBean
													.getManagerId());
									empGoalFeedback
											.setGsrGoalModifiedDate(new Date());
									empGoalFeedback
											.setFkGsrGoalStatus(Byte
													.valueOf(goalManagerFeedbackStatus));
									empGoalFeedback
											.setGsrGoalMgrFeedback(feedbackBean
													.getGsrGoalMgrFeedback());
									try {
										empGoalRepository.save(empGoalFeedback);
									} catch (Exception ex) {
										throw new CommonCustomException(
												"Failed to save or update goal "
														+ "details by delegated mgr. "
														+ ex.getMessage());
									}
									feedback = true;
								} else {
									throw new CommonCustomException(
											"Mgr id : "
													+ feedbackBean
															.getManagerId()
													+ " is not allowed to update this goal because he is neither a "
													+ "delegated nor reporting manager of this delegated "
													+ "goal "
													+ delegatedKpiGoalDetails
															.getPkGsrDelegatedGoalId()
													+ " .");
								}
							} else {
								throw new CommonCustomException(
										"Delegated goal "
												+ empGoalFeedback
														.getFkGsrDelegatedGoalId()
												+ " is not exists.");
							}
						} else {
							throw new CommonCustomException(
									"Other mgr "
											+ feedbackBean.getManagerId()
											+ " are not allowed to provide mgr's feedback in non-delegated kpi goal.");
						}
					} else {
						empGoalFeedback.setGsrGoalModifiedBy(feedbackBean
								.getManagerId());
						empGoalFeedback.setGsrGoalModifiedDate(new Date());
						empGoalFeedback.setFkGsrGoalStatus(Byte
								.valueOf(goalManagerFeedbackStatus));
						empGoalFeedback.setGsrGoalMgrFeedback(feedbackBean
								.getGsrGoalMgrFeedback());
						try {
							empGoalRepository.save(empGoalFeedback);
						} catch (Exception ex) {
							throw new CommonCustomException(
									"Failed to save or update goal details "
											+ "by goal mgr. " + ex.getMessage());
						}
						feedback = true;
					}
				} else if (empGoalFeedback.getFkGsrGoalStatus() < goalAccomplishmentEnteredStatus) {
					feedback = false;
					throw new CommonCustomException(
							"Accomplishments are not entered by employee,so you can't provide the feedback.");
				} else {
					feedback = false;
					throw new CommonCustomException("Since the input goal id "
							+ feedbackBean.getPkGsrDatEmpGoalId()
							+ " is already rated,feedback cannot be allowed ");
				}
			} else {
				/*
				 * throw new
				 * CommonCustomException("Input kpi weightage goal id " +
				 * feedbackBean.getPkGsrDatEmpGoalId() + " is not a kpi goal.");
				 */
				if (empGoalFeedback.getFkGsrGoalStatus() == goalAccomplishmentEnteredStatus
						|| empGoalFeedback.getFkGsrGoalStatus() == goalManagerFeedbackStatus
						|| empGoalFeedback.getFkGsrGoalStatus() == goalReopenStatus) {
					if (empGoalFeedback.getFkGsrDatMgrId().intValue() != feedbackBean
							.getManagerId().intValue()) {
						if (empGoalFeedback.getFkGsrDelegatedGoalId() != null) {
							GsrDatEmpDelegatedGoalBO delegatedProjectGoalDetails = new GsrDatEmpDelegatedGoalBO();
							try {
								delegatedProjectGoalDetails = delegatedGoalRepository
										.findByPkGsrDelegatedGoalId(empGoalFeedback
												.getFkGsrDelegatedGoalId());
							} catch (Exception ex) {
								throw new CommonCustomException(
										"Failed to fetch delegated project goal detail from "
												+ "delegated goal id. "
												+ ex.getMessage());
							}

							if (delegatedProjectGoalDetails != null) {
								if (delegatedProjectGoalDetails
										.getFkMgrIdDelegatedTo().intValue() == feedbackBean
										.getManagerId().intValue()) {
									empGoalFeedback
											.setGsrGoalModifiedBy(feedbackBean
													.getManagerId());
									empGoalFeedback
											.setGsrGoalModifiedDate(new Date());
									empGoalFeedback
											.setFkGsrGoalStatus(Byte
													.valueOf(goalManagerFeedbackStatus));
									empGoalFeedback
											.setGsrGoalMgrFeedback(feedbackBean
													.getGsrGoalMgrFeedback());
									try {
										empGoalRepository.save(empGoalFeedback);
									} catch (Exception ex) {
										throw new CommonCustomException(
												"Failed to save or update project goal "
														+ "details by delegated mgr. "
														+ ex.getMessage());
									}
									feedback = true;
								} else {
									throw new CommonCustomException(
											"Mgr id : "
													+ feedbackBean
															.getManagerId()
													+ " is not allowed to update this goal because he is neither a "
													+ "delegated nor reporting manager of this delegated "
													+ "goal "
													+ delegatedProjectGoalDetails
															.getPkGsrDelegatedGoalId()
													+ " .");
								}
							} else {
								throw new CommonCustomException(
										"Delegated goal "
												+ empGoalFeedback
														.getFkGsrDelegatedGoalId()
												+ " is not exists.");
							}
						} else {
							throw new CommonCustomException(
									"Other mgr "
											+ feedbackBean.getManagerId()
											+ " are not allowed to provide mgr's feedback in non-delegated project goal.");
						}
					} else {
						empGoalFeedback.setGsrGoalModifiedBy(feedbackBean
								.getManagerId());
						empGoalFeedback.setGsrGoalModifiedDate(new Date());
						empGoalFeedback.setFkGsrGoalStatus(Byte
								.valueOf(goalManagerFeedbackStatus));
						empGoalFeedback.setGsrGoalMgrFeedback(feedbackBean
								.getGsrGoalMgrFeedback());
						LOG.info("empGoalFeedback ::: " + empGoalFeedback);
						try {
							empGoalRepository.save(empGoalFeedback);
						} catch (Exception ex) {
							throw new CommonCustomException(
									"Failed to save or update project goal details "
											+ "by goal mgr. " + ex.getMessage());
						}
						feedback = true;
					}
				} else if (empGoalFeedback.getFkGsrGoalStatus() < goalAccomplishmentEnteredStatus) {
					feedback = false;
					throw new CommonCustomException(
							"Accomplishments are not entered by employee,so you can't provide the feedback.");
				} else {
					feedback = false;
					throw new CommonCustomException("Since the input goal id "
							+ feedbackBean.getPkGsrDatEmpGoalId()
							+ " is already rated,feedback cannot be allowed ");
				}
			}
		} else {
			throw new CommonCustomException("Goal id does not exist.");
		}
		return feedback;
	}

	// EOA by Smrithi

	/* Added By Pratibha */

	/**
	 * 
	 * @param slabBean
	 *            contains the input details are to be used in the service.
	 * @return GsrSlabBO
	 * @throws DataAccessException
	 *             if there is present any Data Access Exception
	 * @Description: This service is used to create slab
	 */

	public boolean createSlab(GsrCreateSlabBean slabInputBean)
			throws CommonCustomException {
		LOG.startUsecase("createSlab");
		boolean flag = false;
		GsrSlabBO slab;
		GsrSlabBO slabOutput = new GsrSlabBO();
		List<GsrSlabBO> gsrSlab = new ArrayList<GsrSlabBO>();
		List<GsrSlabBO> slabsList = new ArrayList<GsrSlabBO>();
		slabOutput.setGsrSlabName(slabInputBean.getGsrSlabName());
		slabOutput.setGsrSlabStartDate(slabInputBean.getGsrSlabStartDate());
		slabOutput.setGsrSlabEndDate(slabInputBean.getGsrSlabEndDate());
		slabOutput.setGsrSlabYear(slabInputBean.getGsrSlabYear());
		slabOutput.setGsrSlabShortName(slabInputBean.getGsrSlabName());
		slabOutput.setGsrSlabEnabled(slabInputBean.getGsrSlabEnabled());
		try {
			slabsList = slabRepository
					.findByGsrSlabStartDateGreaterThanEqualAndGsrSlabEndDateLessThanEqual(
							DateTimeUtil.dateWithMinTimeofDay(slabInputBean
									.getGsrSlabStartDate()), DateTimeUtil
									.dateWithMinTimeofDay(slabInputBean
											.getGsrSlabEndDate()));

			gsrSlab = slabRepository.findAll();
			for (GsrSlabBO gsrSlabBO : gsrSlab) {
				if (slabInputBean.getGsrSlabName().equalsIgnoreCase(
						gsrSlabBO.getGsrSlabName())) {
					throw new CommonCustomException(
							"Slab has already created with this name.");
				}
			}

			if (slabsList.isEmpty()) {
				slab = slabRepository.save(slabOutput);
			} else {
				throw new CommonCustomException(
						"Slab has been already created for the given date range.");
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (slab != null) {
			flag = true;
		}
		LOG.endUsecase("createSlab");
		return flag;
	}

	/**
	 * 
	 * @param kpiBean
	 *            bean contains the input details are to be used in the service.
	 * @return GsrKpiGoalBO
	 * @throws DataAccessException
	 *             if there is present any Data Access Exception
	 * @throws GoalNotFoundException
	 *             if Id does not present
	 * @throws CommonCustomException
	 *             if goal is already deleted
	 * @Description: This service is used to update common goals
	 */
	public GsrUpdateCommonGoalsBean updateGoals(
			GsrUpdateCommonGoalsBean updateCommonGoalsBean)
			throws DataAccessException, GoalNotFoundException,
			CommonCustomException {
		LOG.startUsecase("updateGoals");
		GsrKpiGoalBO kpiUpdateGoal = new GsrKpiGoalBO();
		GsrUpdateCommonGoalsBean goalBean = new GsrUpdateCommonGoalsBean();
		try {
			kpiUpdateGoal = goalRepository
					.findByPkGsrKpiGoalId(updateCommonGoalsBean
							.getPkGsrKpiGoalId());
			if (kpiUpdateGoal == null) {
				throw new GoalNotFoundException("Goal Id does not exist.");
			} else {
				kpiUpdateGoal.setPkGsrKpiGoalId(updateCommonGoalsBean
						.getPkGsrKpiGoalId());
				kpiUpdateGoal.setGsrKpiGoalDescription(updateCommonGoalsBean
						.getGsrKpiGoalDescription());
				kpiUpdateGoal.setGsrKpiGoalMeasurement(updateCommonGoalsBean
						.getGsrKpiGoalMeasurement());
				kpiUpdateGoal.setGsrkpigoalactiveflag("ENABLED");
				kpiUpdateGoal = goalRepository.save(kpiUpdateGoal);
			}// converting BO to Bean
			goalBean.setPkGsrKpiGoalId(kpiUpdateGoal.getPkGsrKpiGoalId());
			goalBean.setGsrKpiGoalDescription(kpiUpdateGoal
					.getGsrKpiGoalDescription());
			goalBean.setGsrKpiGoalMeasurement(kpiUpdateGoal
					.getGsrKpiGoalMeasurement());
		} catch (GoalNotFoundException e) {
			throw new GoalNotFoundException("Goal Id does not exist.", e);
		}
		LOG.endUsecase("updateGoals");
		return goalBean;
	}

	/**
	 * 
	 * @param pkGsrKpiGoalId
	 * @return GsrKpiGoalBO
	 * @throws DataAccessException
	 *             if there is present any Data Access Exception
	 * @throws CommonCustomException
	 *             if there is present any Common Custom Exception
	 * @throws GoalNotFoundException
	 * @Description: This service is used to delete common goals
	 */
	public GsrKpiGoalBO deleteGoals(short pkGsrKpiGoalId)
			throws DataAccessException, CommonCustomException,
			GoalNotFoundException {
		LOG.startUsecase("deleteGoals");
		GsrKpiGoalBO kpiDeleteGoal = new GsrKpiGoalBO();
		try {
			kpiDeleteGoal = goalRepository.findByPkGsrKpiGoalId(pkGsrKpiGoalId);

			if (kpiDeleteGoal == null) {
				throw new GoalNotFoundException("Goal does not exist.");

			} else if (kpiDeleteGoal.getGsrkpigoalactiveflag().equals(
					"DISABLED")) {
				throw new CommonCustomException(
						"Someting bad happened, please contact your administrator.");
			} else {
				kpiDeleteGoal.setGsrkpigoalactiveflag("DISABLED");
				goalRepository.save(kpiDeleteGoal);
			}

		} catch (CommonCustomException e) {
			throw new CommonCustomException(
					"Someting bad happened, please contact your administrator.",
					e);
		} catch (GoalNotFoundException e) {
			throw new GoalNotFoundException("Goal does not exist.", e);
		}
		LOG.endUsecase("deleteGoals");
		return kpiDeleteGoal;
	}

	/**
	 * 
	 * @param enableBean
	 *            bean contains the input details are to be used in the service.
	 * @return GsrAdminConfigurationBO
	 * @throws DataAccessException
	 *             if there is present any Data Access Exception
	 * @Description: This service is used to enable and disable settings
	 */
	public GsrAdminConfigurationBO enableDisableSettings(
			GsrEnableSettingsBean enableBean) throws DataAccessException {
		LOG.startUsecase("enableDisableSettings");
		GsrAdminConfigurationBO adminConfig = new GsrAdminConfigurationBO();

		try {

			adminConfig = ConfigRepository.findByPkGsrAdminConfigId(enableBean
					.getPkGsrAdminConfigId());

			if (enableBean.getEnableDisableSettings().equalsIgnoreCase(
					"ENABLED")) {
				adminConfig.setEnableGsrWindow(enableBean
						.getEnableDisableSettings());

			} else {
				adminConfig.setEnableGsrWindow(enableBean
						.getEnableDisableSettings());
			}

			adminConfig = ConfigRepository.save(adminConfig);
		} catch (Exception e) {

			throw new DataAccessException("Exception occured.", e);
		}
		LOG.endUsecase("enableDisableSettings");
		return adminConfig;
	}

	/**
	 * 
	 * @param workFlowBean
	 *            bean contains the input details are to be used in the service.
	 * @return GsrWorkflowPermissionBO
	 * @throws DataAccessException
	 *             if there is present any Data Access Exception
	 * @Description: This service is used to set work flow dates
	 */
	public GsrWorkflowPermissionBO setDatesWorkflow(
			GsrSetWorkflowDatesBean workFlowBean) throws DataAccessException {
		LOG.startUsecase("setDatesWorkflow");
		GsrWorkflowPermissionBO workFlowResult = new GsrWorkflowPermissionBO();

		try {
			workFlowResult = workRepository
					.findByPkGsrWorkflowPermissionId(workFlowBean
							.getPkGsrWorkflowPermissionId());
			LOG.info(workFlowBean.getStartDate() + ""
					+ workFlowBean.getEndDate());
			workFlowResult.setGsrWorkflowPermissionStartDate(workFlowBean
					.getStartDate());
			workFlowResult.setGsrWorkflowPermissionEndDate(workFlowBean
					.getEndDate());
			workRepository.save(workFlowResult);
		} catch (Exception e) {

			throw new DataAccessException("Exception occured.", e);
		}
		LOG.endUsecase("setDatesWorkflow");
		return workFlowResult;
	}

	/**
	 * 
	 * @param approveGoalBean
	 *            bean contains the input details are to be used in the service.
	 * @return GsrDatEmpGoalBO
	 * @throws GoalNotFoundException
	 *             if Id does not present
	 * @throws CommonCustomException
	 *             if there is present any Common Custom Exception
	 * @Description: This service is used to Set approve or reject goal
	 */
	public GsrDatEmpGoalBO approveOrRejectStatus(
			GsrApproveAndRejectGoalBean approveGoalBean)
			throws GoalNotFoundException, CommonCustomException {
		LOG.startUsecase("approveStatus");
		GsrDatEmpGoalBO empGoalApprove = new GsrDatEmpGoalBO();
		empGoalApprove = empGoalRepository
				.findByPkGsrDatEmpGoalId(approveGoalBean.getPkGsrDatEmpGoalId());
		if (empGoalApprove == null) {
			throw new GoalNotFoundException("Goal does not exist.");
		} else if (empGoalApprove.getFkGsrGoalStatus() == 2) {
			throw new CommonCustomException("Goal is already approved.");
		} else if (empGoalApprove.getFkGsrGoalStatus() == 6) {
			throw new CommonCustomException("Goal is already rejected.");
		} else {
			empGoalApprove.setFkGsrGoalStatus(approveGoalBean
					.getFkGsrGoalStatus());
		}
		if (empGoalApprove.getFkGsrGoalStatus() == 2
				|| empGoalApprove.getFkGsrGoalStatus() == 6) {
			empGoalApprove.setGsrGoalModifiedBy(approveGoalBean
					.getGoalModifiedBy());
			empGoalApprove.setGsrGoalModifiedDate(new Date());
			empGoalRepository.save(empGoalApprove);
		}

		else {
			throw new CommonCustomException(
					"Invalid status set in given input. Status must be set to 2 for approval and 6 for rejection.");
		}

		LOG.endUsecase("approveStatus");
		return empGoalApprove;
	}

	/**
	 * 
	 * @param approveGoalBean
	 *            bean contains the input details are to be used in the service.
	 * @return GsrDatEmpGoalBO
	 * @throws GoalNotFoundException
	 *             if Id does not present
	 * @throws CommonCustomException
	 *             if there is present any Common Custom Exception
	 * @Description: This service is used to Set approve goal
	 */

	public GsrDatEmpGoalBO approveStatus(GsrApproveGoalBean approveGoalBean)
			throws CommonCustomException {
		LOG.startUsecase("approveStatus");
		GsrDatEmpGoalBO empGoalBO = new GsrDatEmpGoalBO();
		GsrDatEmpGoalBO inputGsrDatEmpGoalBO = new GsrDatEmpGoalBO();
		GsrDatEmpGoalBO outputGsrDatEmpGoalBO = new GsrDatEmpGoalBO();
		boolean otherMgrIdIsPresentIsGoal = false;
		try {
			empGoalBO = empGoalRepository
					.findByPkGsrDatEmpGoalId(approveGoalBean
							.getPkGsrDatEmpGoalId());
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to retrieve your reportee's goal. Kinldy contact to Nucleus "
							+ "Support team for this issue.");
		}

		if (empGoalBO == null) {
			throw new CommonCustomException("Goal does not exist.");
		} else {

			// Default addition of other properties of empGoalBO
			inputGsrDatEmpGoalBO.setPkGsrDatEmpGoalId(empGoalBO
					.getPkGsrDatEmpGoalId());
			inputGsrDatEmpGoalBO.setFkGsrDatEmpId(empGoalBO.getFkGsrDatEmpId());
			// inputGsrDatEmpGoalBO.setFkGsrDatMgrId(gsrDatEmpGoalBO.getFkGsrDatMgrId());
			inputGsrDatEmpGoalBO.setFkGsrGoalStatus(Byte
					.valueOf(goalGoalSetStatus));
			inputGsrDatEmpGoalBO.setFkGsrSlabId(empGoalBO.getFkGsrSlabId());
			inputGsrDatEmpGoalBO.setGsrDatEmpGoalName(empGoalBO
					.getGsrDatEmpGoalName());
			inputGsrDatEmpGoalBO.setGsrGoalCreatedBy(empGoalBO
					.getGsrGoalCreatedBy());
			inputGsrDatEmpGoalBO.setGsrGoalCreatedDate(empGoalBO
					.getGsrGoalCreatedDate());
			inputGsrDatEmpGoalBO.setGsrGoalEndDate(empGoalBO
					.getGsrGoalEndDate());
			inputGsrDatEmpGoalBO.setGsrGoalStartDate(empGoalBO
					.getGsrGoalStartDate());
			inputGsrDatEmpGoalBO.setFkEmpGoalDomainMgrId(empGoalBO
					.getFkEmpGoalDomainMgrId());
			inputGsrDatEmpGoalBO.setFkEmpGoalBuId(empGoalBO.getFkEmpGoalBuId());
			inputGsrDatEmpGoalBO.setGsrGoalModifiedDate(new Date());

			// inputGsrDatEmpGoalBO.setGsrGoalModifiedBy(approveGoalBean.getGoalModifiedBy());
			// Added by Shyam for RM Change
			// Added by Shyam for RM change scenario
			List<GsrDatEmpGoalBO> empGoalList = new ArrayList<GsrDatEmpGoalBO>();
			try {
				empGoalList = gsrDatEmpGoalRepository
						.getAllEnteredGsrGoalsOfASlab(empGoalBO
								.getFkGsrSlabId().shortValue(), empGoalBO
								.getFkGsrDatEmpId().intValue());
			} catch (Exception ex) {
				throw new CommonCustomException(
						"Unable to get all entered goals detail. Kindly touch with "
								+ "Nucleus Support team for this issue.");
			}
			// LOG.info("empGoalList :: " + empGoalList);

			int tempGoalMgrId3 = 0;
			// List<GsrDatEmpGoalBO> otherMgrGoalDetails = new
			// ArrayList<GsrDatEmpGoalBO>();
			if (!empGoalList.isEmpty()) {
				for (GsrDatEmpGoalBO gsrDatEmpGoalBO4 : empGoalList) {
					// LOG.info("Goal Mgr ID ::: " +
					// gsrDatEmpGoalBO4.getFkGsrDatMgrId().intValue());
					if (tempGoalMgrId3 == 0) {
						tempGoalMgrId3 = gsrDatEmpGoalBO4.getFkGsrDatMgrId()
								.intValue();
						// otherMgrGoalDetails.add(gsrDatEmpGoalBO4);
					} else {
						if (tempGoalMgrId3 != gsrDatEmpGoalBO4
								.getFkGsrDatMgrId().intValue()) {
							otherMgrIdIsPresentIsGoal = true;
							break;
						}/*
						 * else{ otherMgrGoalDetails.add(gsrDatEmpGoalBO4); }
						 */
					}
				}
			}
			// EOA by Shyam for RM chnage

			if (empGoalBO.getEmpGoalKpiWeightageId().shortValue() > 0) {
				inputGsrDatEmpGoalBO.setEmpGoalKpiWeightageId(empGoalBO
						.getEmpGoalKpiWeightageId());
			}

			// ------------------------------------------------------------------------------------
			if (empGoalBO.getFkGsrGoalStatus() == goalWaitingForManagerApprovalStatus
					|| empGoalBO.getFkGsrGoalStatus() == goalGoalSetStatus) {
				// Reportee's Professional Detail
				DatEmpProfessionalDetailBO reporteeProfessionalDetail = new DatEmpProfessionalDetailBO();
				try {
					reporteeProfessionalDetail = employeeProfessionalRepository
							.findByFkMainEmpDetailId(empGoalBO
									.getFkGsrDatEmpId().intValue());
				} catch (Exception ex) {
					throw new CommonCustomException(
							"Unbale to get the reportee's professional detail.");
				}

				if (reporteeProfessionalDetail == null) {
					throw new CommonCustomException(
							"Reportee's professional detail not found.");
				}

				String reportingMgrName = getEmpNameByEmpId(empGoalBO
						.getFkGsrDatMgrId());

				// Check for org-wide goal is considering for that slab or not.
				GsrAdminConfigurationBO gsrAdminConfigurationBO = new GsrAdminConfigurationBO();
				try {
					gsrAdminConfigurationBO = ConfigRepository
							.findByFkGsrSlabId(empGoalBO.getFkGsrSlabId()
									.intValue());
				} catch (Exception ex) {
					throw new CommonCustomException(
							"Unable to fetch Gsr Configuration details of a quarter "
									+ empGoalBO.getGsrSlabBO_slab_id()
											.getGsrSlabName() + " . ");
				}

				// Get all goals entered for a slab
				List<GsrDatEmpGoalBO> allGoalsEnteredForASlabUnderAMgrList = new ArrayList<GsrDatEmpGoalBO>();
				try {
					allGoalsEnteredForASlabUnderAMgrList = gsrDatEmpGoalRepository
							.getAllGoalsEnteredByEmpOfASlabUnderAMgr(
									empGoalBO.getFkGsrDatEmpId(),
									empGoalBO.getFkGsrSlabId(),
									empGoalBO.getFkGsrDatMgrId());
				} catch (Exception ex) {
					throw new CommonCustomException(
							"Unable to retrieve all entered goals of a quarter.");
				}
				// LOG.info("allGoalsEnteredForASlabUnderAMgrList :: " +
				// allGoalsEnteredForASlabUnderAMgrList);

				List<GsrKpiWeightageBO> nonEnteredKpiGoals = new ArrayList<GsrKpiWeightageBO>();
				boolean moreThanOneOrgWideGoalMgrFlag = false;
				int tempMgrId2 = 0;
				// Get all organisation goals mapped to emp level and bu id.
				List<GsrKpiWeightageBO> empLevelIdAndBuIdGsrKpiWeightageList = new ArrayList<GsrKpiWeightageBO>();
				if (gsrAdminConfigurationBO.getEnableOrgWideGoal()
						.equalsIgnoreCase("ENABLED")) {
					// Scenario : After RM change, it should not check for KPI
					// goals weightage

					// Find all goals under a employee of a slab.
					List<GsrDatEmpGoalBO> employeeGoalList = new ArrayList<GsrDatEmpGoalBO>();
					try {
						employeeGoalList = gsrDatEmpGoalRepository
								.getAllEnteredGoalsOfASlab(empGoalBO
										.getGsrSlabId(), empGoalBO
										.getFkGsrDatEmpId().intValue());
					} catch (Exception ex) {
						throw new CommonCustomException(
								"Unable to retrieve all gaols fo input employee.");
					}

					for (GsrDatEmpGoalBO gsrDatEmpGoalBO3 : employeeGoalList) {
						if (tempMgrId2 == 0) {
							tempMgrId2 = gsrDatEmpGoalBO3.getFkGsrDatMgrId()
									.intValue();
						} else {
							if (tempMgrId2 != gsrDatEmpGoalBO3
									.getFkGsrDatMgrId().intValue()) {
								moreThanOneOrgWideGoalMgrFlag = true;
								break;
							}
						}
					}

					if (moreThanOneOrgWideGoalMgrFlag) {
						nonEnteredKpiGoals.clear();
					} else {
						// End of scenario by Shyam
						try {
							empLevelIdAndBuIdGsrKpiWeightageList = gsrKpiWeightageRepos
									.getAllKpiGoalsWithWeightageForEmpLevel(empGoalBO
											.getFkGsrDatEmpId().intValue());
						} catch (Exception ex) {
							throw new CommonCustomException(
									"Unable to fetch all Org-wide goals w.r.t employee level and business unit.");
						}
						// LOG.info("empLevelIdAndBuIdGsrKpiWeightageList :: " +
						// empLevelIdAndBuIdGsrKpiWeightageList);

						if (!empLevelIdAndBuIdGsrKpiWeightageList.isEmpty()) {
							nonEnteredKpiGoals
									.addAll(empLevelIdAndBuIdGsrKpiWeightageList);

							for (GsrDatEmpGoalBO eachGoalDetail : allGoalsEnteredForASlabUnderAMgrList) {
								for (GsrKpiWeightageBO gsrKpiWeightageBO : empLevelIdAndBuIdGsrKpiWeightageList) {
									if (eachGoalDetail
											.getEmpGoalKpiWeightageId()
											.shortValue() == gsrKpiWeightageBO
											.getPkGsrKpiWeightageId()) {
										nonEnteredKpiGoals
												.remove(gsrKpiWeightageBO);
									}
								}
							}
							// LOG.info("After nonEnteredKpiGoals :: " +
							// nonEnteredKpiGoals);
						} else {
							throw new CommonCustomException(
									"Org-wide goals have not set for employee "
											+ empGoalBO.getFkGsrDatEmpId()
											+ " .");
						}
					}
				}

				int totalWeightageOfAllGoalsUnderAMgr = 0;
				if (approveGoalBean.getGoalWeightage() != empGoalBO
						.getGsrGoalWeightage().byteValue()) {
					int diffInWeightage = approveGoalBean.getGoalWeightage()
							- empGoalBO.getGsrGoalWeightage().byteValue();
					// LOG.info("diffInWeightage ::: " + diffInWeightage);

					// For organisation goal
					if (empGoalBO.getEmpGoalKpiWeightageId().shortValue() > 0) {
						// Check for minimum kpi goal weightage.
						GsrKpiWeightageBO kpiWeightageDetail = new GsrKpiWeightageBO();
						try {
							kpiWeightageDetail = gsrKpiWeightageRepos
									.findByPkGsrKpiWeightageId(empGoalBO
											.getEmpGoalKpiWeightageId());
						} catch (Exception ex) {
							throw new CommonCustomException(
									"Unable to fetch Org-wide goals.");
						}
						// LOG.info("kpiWeightageDetail :: " +
						// kpiWeightageDetail);

						if (kpiWeightageDetail != null) {
							if (kpiWeightageDetail.getGsrKpiGoalWeightage() <= approveGoalBean
									.getGoalWeightage().intValue()) {
								totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
										+ diffInWeightage;
							} else {
								throw new CommonCustomException(
										"Weightage assigned to org-wide goal "
												+ kpiWeightageDetail
														.getGsrKpiGoal()
														.getGsrKpiGoalName()
												+ " can not be less than the weightage pre-defined by your BU for this goal.");
							}
						} else {
							throw new CommonCustomException(
									"Org-wide goal does not exist.");
						}

						// Find out the total weightage of entered goal
						for (GsrDatEmpGoalBO eachGoalOfEmp : allGoalsEnteredForASlabUnderAMgrList) {
							totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
									+ eachGoalOfEmp.getGsrGoalWeightage()
											.byteValue();
						}
						// LOG.info("In KPI, Entered Total Goal Weightage :: " +
						// totalWeightageOfAllGoalsUnderAMgr);

						if (!nonEnteredKpiGoals.isEmpty()) {
							for (GsrKpiWeightageBO nonEnteredKpiGoal : nonEnteredKpiGoals) {
								totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
										+ nonEnteredKpiGoal
												.getGsrKpiGoalWeightage();
							}
							// LOG.info("After addition of non-entered kpi goals weightage, Total Goal Weightage :: "
							// + totalWeightageOfAllGoalsUnderAMgr);
						}

						// 100 % weightage check
						if (totalWeightageOfAllGoalsUnderAMgr <= 100) {
							// inputGsrDatEmpGoalBO.setEmpGoalKpiWeightageId(gsrDatEmpGoalBO.getEmpGoalKpiWeightageId());
							inputGsrDatEmpGoalBO
									.setGsrGoalWeightage(approveGoalBean
											.getGoalWeightage());
						} else {
							/*
							 * throw new CommonCustomException(
							 * "After updation of organisation goal weightage "
							 * + gsrUpdateGoalInputBean.getGoalWeightage()+
							 * " , total weightage is exceeding 100%.");
							 */
							throw new CommonCustomException(
									"It is mandatory for goal weightages to sum up to 100%. "
											+ "Your goals currently sum up to "
											+ totalWeightageOfAllGoalsUnderAMgr
											+ " .");
						}
					}// For project goal
					else {
						// Check for organisation goal is enabled or disabled
						// for the input goal slab
						if (gsrAdminConfigurationBO.getEnableOrgWideGoal()
								.equalsIgnoreCase("ENABLED")) {
							// Find out the total weightage of entered goal
							for (GsrDatEmpGoalBO eachGoalOfEmp : allGoalsEnteredForASlabUnderAMgrList) {
								totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
										+ eachGoalOfEmp.getGsrGoalWeightage()
												.byteValue();
							}
							// LOG.info("In project goal (when org wide goal is Enabled), Entered Total Goal Weightage :: "
							// + totalWeightageOfAllGoalsUnderAMgr);

							if (otherMgrIdIsPresentIsGoal) {
								nonEnteredKpiGoals.clear();
							}
							// End of addition by Shyam for RM chnage scenario
							// LOG.info("nonEnteredKpiGoals ::: " +
							// nonEnteredKpiGoals);
							// Add non-entered kpi goal weightage to total
							if (!nonEnteredKpiGoals.isEmpty()) {
								for (GsrKpiWeightageBO nonEnteredKpiGoal : nonEnteredKpiGoals) {
									totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
											+ nonEnteredKpiGoal
													.getGsrKpiGoalWeightage();
								}
								// LOG.info("After addition of non-entered kpi goals weightage in project goal , Total Goal Weightage :: "
								// + totalWeightageOfAllGoalsUnderAMgr);
							}

							totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
									+ diffInWeightage;
							// LOG.info("After addition of non-entered kpi goals weightage in project goal -1 , Total Goal Weightage :: "
							// + totalWeightageOfAllGoalsUnderAMgr);

							if (totalWeightageOfAllGoalsUnderAMgr > 100) {
								/*
								 * throw new CommonCustomException(
								 * "After adding of updated project goal weightage "
								 * + gsrUpdateGoalInputBean.getGoalWeightage()+
								 * " , total weightage is exceeding 100%.");
								 */
								throw new CommonCustomException(
										"It is mandatory for goal weightages to sum up to 100%. "
												+ "Your goals currently sum up to "
												+ totalWeightageOfAllGoalsUnderAMgr
												+ " .");
							} else {
								// inputGsrDatEmpGoalBO.setEmpGoalKpiWeightageId(gsrDatEmpGoalBO.getEmpGoalKpiWeightageId());
								// LOG.info("gsrUpdateGoalInputBean.getGoalWeightage() ::: "
								// + gsrUpdateGoalInputBean.getGoalWeightage());
								inputGsrDatEmpGoalBO
										.setGsrGoalWeightage(approveGoalBean
												.getGoalWeightage());
							}
						}// When input goal is project and org wide goal is not
							// considered in that slab
						else {

							// Find out the total weightage of entered goal
							for (GsrDatEmpGoalBO eachGoalOfEmp : allGoalsEnteredForASlabUnderAMgrList) {
								if (eachGoalOfEmp.getEmpGoalKpiWeightageId()
										.shortValue() > 0) {
									throw new CommonCustomException(
											"Org-wide goals are disabled by HR for this quarter.");
								} else {
									totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
											+ eachGoalOfEmp
													.getGsrGoalWeightage()
													.byteValue();
								}
							}
							// LOG.info("In project goal, Entered Total Goal Weightage :: "
							// + totalWeightageOfAllGoalsUnderAMgr);

							totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
									+ diffInWeightage;
							// LOG.info("In project goal, Entered Total Goal Weightage - 1 :: "
							// + totalWeightageOfAllGoalsUnderAMgr);
							if (totalWeightageOfAllGoalsUnderAMgr > 100) {
								/*
								 * throw new CommonCustomException(
								 * "After adding of updated project goal weightage "
								 * + gsrUpdateGoalInputBean.getGoalWeightage()+
								 * " , total weightage is exceeding 100%.");
								 */
								throw new CommonCustomException(
										"It is mandatory for goal weightages to sum up to 100%. "
												+ "Your goals currently sum up to "
												+ totalWeightageOfAllGoalsUnderAMgr
												+ " .");
							} else {
								// inputGsrDatEmpGoalBO.setEmpGoalKpiWeightageId(gsrDatEmpGoalBO.getEmpGoalKpiWeightageId());
								inputGsrDatEmpGoalBO
										.setGsrGoalWeightage(approveGoalBean
												.getGoalWeightage());
							}
						}
					}// End of project goal
				} else {
					inputGsrDatEmpGoalBO.setGsrGoalWeightage(approveGoalBean
							.getGoalWeightage());
				}

				if (approveGoalBean.getGoalDescription() != null
						&& !approveGoalBean.getGoalDescription().isEmpty()) {
					inputGsrDatEmpGoalBO.setGsrGoalDescription(approveGoalBean
							.getGoalDescription());
				} else {
					throw new CommonCustomException(
							"Goal description cannot be empty or blank.");
				}

				if (approveGoalBean.getGoalMeasurement() != null
						&& !approveGoalBean.getGoalMeasurement().isEmpty()) {
					inputGsrDatEmpGoalBO.setGsrGoalMeasurement(approveGoalBean
							.getGoalMeasurement());
				} else {
					throw new CommonCustomException(
							"Goal measurement cannot be empty or blank.");
				}

				// Find out the total weightage entered by a manager in a slab
				int totalWeighatgeUnderAMgr = 0;

				for (GsrDatEmpGoalBO gsrDatEmpGoalBO1 : allGoalsEnteredForASlabUnderAMgrList) {
					totalWeighatgeUnderAMgr = totalWeighatgeUnderAMgr
							+ gsrDatEmpGoalBO1.getGsrGoalWeightage()
									.byteValue();
				}

				// If employee himself or herself is trying to update the gsr
				// goal
				/*
				 * if (empGoalBO.getFkGsrDatEmpId() ==
				 * approveGoalBean.getGoalModifiedBy()) { // Goal status should
				 * be "Waiting For Manager Approval" if
				 * (empGoalBO.getFkGsrGoalStatus().byteValue() ==
				 * goalWaitingForManagerApprovalStatus) {
				 * inputGsrDatEmpGoalBO.setGsrGoalModifiedBy
				 * (approveGoalBean.getLoggedInEmployee()); } else { throw new
				 * CommonCustomException
				 * ("You are prohibited to update this goal."); //
				 * "To update your goal, your goal status should be Waiting For Manager Approval."
				 * ); } }//If manager is trying to update the goal else
				 */
				if (empGoalBO.getFkGsrDatMgrId().intValue() == approveGoalBean
						.getGoalModifiedBy().intValue()) {
					inputGsrDatEmpGoalBO.setFkGsrDatMgrId(empGoalBO
							.getFkGsrDatMgrId());
				} else {
					// When BU Head, Domain Mgr, Skip level Mgr and HR try to
					// update the employee goal having reporting mgr id in goal
					// mgr id.
					if (empGoalBO.getFkGsrDatMgrId().intValue() == reporteeProfessionalDetail
							.getFkEmpReportingMgrId().intValue()) {
						inputGsrDatEmpGoalBO.setFkGsrDatMgrId(empGoalBO
								.getFkGsrDatMgrId());
					} else {
						throw new CommonCustomException(
								"Goals have been created by "
										+ reportingMgrName
										+ " for "
										+ totalWeighatgeUnderAMgr
										+ " %. Hence you are not allowed to update "
										+ "the goals for this quarter.");
					}
				}
				DatEmpProfessionalDetailBO reportingMgrProfDetail = new DatEmpProfessionalDetailBO();
				reportingMgrProfDetail = employeeProfessionalRepository
						.findByFkMainEmpDetailId(reporteeProfessionalDetail
								.getFkEmpReportingMgrId().intValue());

				DatEmpDetailBO modifiedByDetail = new DatEmpDetailBO();
				modifiedByDetail = empDetailsRepository
						.findByPkEmpId(approveGoalBean.getGoalModifiedBy());
				// For modifiedBy field
				LOG.info("otherMgrIdIsPresentIsGoal ::: "
						+ otherMgrIdIsPresentIsGoal);

				if (otherMgrIdIsPresentIsGoal) {
					if (approveGoalBean.getGoalModifiedBy().intValue() != empGoalBO
							.getFkGsrDatMgrId().intValue()
							&& approveGoalBean.getGoalModifiedBy().intValue() != empGoalBO
									.getFkEmpGoalDomainMgrId().intValue()
							&& approveGoalBean.getGoalModifiedBy().intValue() != reporteeProfessionalDetail
									.getFkEmpReportingMgrId().intValue()
							&& approveGoalBean.getGoalModifiedBy().intValue() != reporteeProfessionalDetail
									.getFkEmpDomainMgrId().intValue()
							&& approveGoalBean.getGoalModifiedBy().intValue() != reporteeProfessionalDetail
									.getMasBuUnitBO().getFkBuHeadEmpId()
							&& approveGoalBean.getGoalModifiedBy().intValue() != reportingMgrProfDetail
									.getFkEmpReportingMgrId().intValue())// For Skip level mgr
					{
						if (modifiedByDetail.getFkEmpRoleId().shortValue() == hrdManagerRole
								|| modifiedByDetail.getFkEmpRoleId()
										.shortValue() == hrdSeniorExecutiveRole) {
							inputGsrDatEmpGoalBO
									.setGsrGoalModifiedBy(approveGoalBean
											.getGoalModifiedBy());
						} else {
							throw new CommonCustomException(
									"Goals have been created by "
											+ reportingMgrName
											+ " for "
											+ totalWeighatgeUnderAMgr
											+ " %. Hence you are not allowed to update "
											+ "the goals for this quarter.");
						}
					} else {
						inputGsrDatEmpGoalBO
								.setGsrGoalModifiedBy(approveGoalBean
										.getGoalModifiedBy());
					}
				} else {
					inputGsrDatEmpGoalBO.setGsrGoalModifiedBy(approveGoalBean
							.getGoalModifiedBy());
				}

				try {
					outputGsrDatEmpGoalBO = gsrDatEmpGoalRepository
							.save(inputGsrDatEmpGoalBO);
				} catch (Exception ex) {
					throw new CommonCustomException(
							"Unable to update the goal.");
				}
			} else {
				throw new CommonCustomException(
						"This goal has crossed 'Goal Set' status, hence it is restricting you to update.");
			}
		}
		LOG.endUsecase("approveStatus");
		return outputGsrDatEmpGoalBO;
	}

	boolean updateGsrGoalToRated(GsrDatEmpGoalBO empGsrGoalObject,
			GsrEnterFeedbackBean feedbackBean,
			boolean considerOtherManagerIdFlag, boolean rmChangeFlag)
			throws CommonCustomException {
		boolean feedback = false;
		if (feedbackBean.getGsrGoalMgrRating() < 1) {
			throw new CommonCustomException(
					"Manager rating should not be less than 1.");
		}
		if (feedbackBean.getGsrGoalMgrRating() > 5) {
			throw new CommonCustomException(
					"Manager rating should not be greater than 5.");
		}
		if (considerOtherManagerIdFlag) {
			if (empGsrGoalObject.getFkGsrGoalStatus() == goalReopenStatus
					|| empGsrGoalObject.getFkGsrGoalStatus() == goalRatedStatus
					// || empGsrGoalObject.getFkGsrGoalStatus() ==
					// goalManagerFeedbackStatus
					|| empGsrGoalObject.getFkGsrGoalStatus() == goalAccomplishmentsEnteredStatus) {
				empGsrGoalObject.setFkGsrGoalStatus((byte) goalRatedStatus);
				empGsrGoalObject.setGsrGoalMgrFeedback(feedbackBean
						.getGsrGoalMgrFeedback());
				empGsrGoalObject.setGsrGoalMgrRating(feedbackBean
						.getGsrGoalMgrRating());
				empGsrGoalObject.setGsrGoalModifiedBy(feedbackBean
						.getEnteredBy());
				empGsrGoalObject.setGsrGoalModifiedDate(new Date());
				try {
					empGoalRepository.save(empGsrGoalObject);
				} catch (Exception ex) {
					throw new CommonCustomException(
							"Failed to update gsr goal "
									+ empGsrGoalObject.getPkGsrDatEmpGoalId()
									+ " by other than reporting manager . "
									+ ex.getMessage());
				}
				feedback = true;
			} else {
				throw new CommonCustomException(
						"Current goal id "
								+ feedbackBean.getPkGsrDatEmpGoalId()
								+ " status is not 'Re-open' or 'Rated' or 'Accomplishments Entered' . Hence enteredBy "
								+ feedbackBean.getEnteredBy()
								+ " employee is not allowed to update /rate it.");
			}
		} else {
			if (empGsrGoalObject.getFkGsrGoalStatus() == goalAccomplishmentsEnteredStatus
					|| empGsrGoalObject.getFkGsrGoalStatus() == goalRatedStatus
					|| empGsrGoalObject.getFkGsrGoalStatus() == goalManagerFeedbackStatus) {
				if (rmChangeFlag) {
					if (empGsrGoalObject.getFkGsrGoalStatus() == goalManagerFeedbackStatus
							|| empGsrGoalObject.getFkGsrGoalStatus() == goalRatedStatus) {
						/*
						 * DatEmpProfessionalDetailBO datEmpProfessionalBO = new
						 * DatEmpProfessionalDetailBO(); datEmpProfessionalBO =
						 * employeeProfessionalRepository
						 * .findByFkMainEmpDetailId
						 * (empGsrGoalObject.getFkGsrDatEmpId().intValue());
						 * if(datEmpProfessionalBO != null &&
						 * datEmpProfessionalBO
						 * .getFkEmpReportingMgrId().intValue() !=
						 * empGsrGoalObject.getFkGsrDatMgrId().intValue()) {
						 * throw new CommonCustomException(
						 * "You are not allowed to rate this goal since RM change has happened."
						 * ); }
						 */
					}
				}
				empGsrGoalObject.setFkGsrGoalStatus((byte) goalRatedStatus);
				empGsrGoalObject.setGsrGoalMgrFeedback(feedbackBean
						.getGsrGoalMgrFeedback());
				empGsrGoalObject.setGsrGoalMgrRating(feedbackBean
						.getGsrGoalMgrRating());
				empGsrGoalObject.setGsrGoalModifiedBy(feedbackBean
						.getEnteredBy());
				empGsrGoalObject.setGsrGoalModifiedDate(new Date());
				try {
					empGoalRepository.save(empGsrGoalObject);
				} catch (Exception ex) {
					throw new CommonCustomException(
							"Failed to update gsr goal "
									+ empGsrGoalObject.getPkGsrDatEmpGoalId()
									+ " by reporting manager . "
									+ ex.getMessage());
				}
				feedback = true;
			} else if (empGsrGoalObject.getFkGsrGoalStatus() < goalAccomplishmentsEnteredStatus) {
				feedback = false;
				throw new CommonCustomException(
						"Employee has not entered his accomplishments. Hence feedback cannot be provided.");
			} else if (empGsrGoalObject.getFkGsrGoalStatus() > goalRatedStatus) {
				feedback = false;
				throw new CommonCustomException(
						"Invalid goal status. The goal must be in one of these states i.e accomplishments entered rated and manager feedback for providing feedback and rating.");
			} else {
				feedback = false;
			}
		}

		return feedback;
	}

	/**
	 * 
	 * @param feedbackBean
	 *            bean contains the input details are to be used in the service.
	 * @return GsrDatEmpGoalBO
	 * @throws Exception
	 * @Description: This service is used to enter feedback
	 */
	public boolean enterFeedback(GsrEnterFeedbackBean feedbackBean)
			throws Exception {
		LOG.startUsecase("enterFeedback");
		boolean feedback = false;
		boolean considerOtherManagerIdFlag = false;
		/*
		 * @javadoc : If all goals have been rated, then value will be zero (0).
		 */
		final int ALL_GOALS_ARE_RATED = 0;
		// final int numberOfDaysAfterAllowedToRate = 14;
		GsrDatEmpGoalBO empGoalFeedback = new GsrDatEmpGoalBO();
		try {
			empGoalFeedback = empGoalRepository.findByPkGsrDatEmpGoalId(feedbackBean.getPkGsrDatEmpGoalId());
		} catch (Exception ex) {
			throw new CommonCustomException("Failed to fetch goal details from goal id. " + ex.getMessage());
		}

		if (empGoalFeedback != null) {
			// organizational goal check
			if (empGoalFeedback.getEmpGoalKpiWeightageId() > 0) {
				//Hit legacy service to get separation details
				SeparationLegacyService separationLegacyService = new SeparationLegacyService();
				SeparationLegacyOutputBean legacyOutputBean = null;
				//List<SeparationLegacyOutputBean> outputList = new ArrayList<SeparationLegacyOutputBean>();
				legacyOutputBean = separationLegacyService.getEmployeeSeparationDetails(empGoalFeedback.
						getFkGsrDatEmpId(), purchaseLegacyUrl);
				
				boolean twoWeeksValidationFlag = false;
				boolean separationFlag = false;	
				boolean separationAcceptedByRmOrDmFlag = false;
				if(legacyOutputBean.getSeparationStatusId() != null){
					if(legacyOutputBean.getSeparationStatusId().intValue() != separationWithdrawnStatus.intValue()
							&& legacyOutputBean.getSeparationStatusId().intValue() != 
								separationCancelledStatus.intValue() && legacyOutputBean.
								getSeparationStatusId().intValue() != separationInitiatedStatus.intValue()
					)
									separationFlag = true;
					
					if(legacyOutputBean.getSeparationStatusId().intValue() == separationAcceptedByRmStatus.intValue() || 
							legacyOutputBean.getSeparationStatusId().intValue() == separationAcceptedByDmStatus.intValue())
					{
						separationAcceptedByRmOrDmFlag = true;
					}
				}
								
				// 2 weeks before the gsr end date condition for kpi goal only.
				ZoneId defaultZoneId = ZoneId.systemDefault();
				// Convert Date -> Instant
				Instant instant = empGoalFeedback.getGsrGoalEndDate().toInstant();
				// Instant + system default time zone + toLocalDate() = LocalDate
				LocalDate localGoalEndDate = instant.atZone(defaultZoneId).toLocalDate();
				LocalDate localTodayDate = LocalDate.now();
				if (localTodayDate.isAfter(localGoalEndDate.minusDays(goalAllowedRateDays))) 
				{
					twoWeeksValidationFlag = true;
				}	
					
				if(separationAcceptedByRmOrDmFlag)
				{	
								
					// Check input manager id should be same as the gsr goal object.
					if (empGoalFeedback.getFkGsrDatMgrId().intValue() == feedbackBean.getEnteredBy().intValue()) 
					{
						feedback = gsrGoalProcessMethod(empGoalFeedback, feedbackBean, considerOtherManagerIdFlag);
					} else {
						// get employee professional object from empGoalFeedback
						DatEmpProfessionalDetailBO empProfDetail = new DatEmpProfessionalDetailBO();
						try {
							empProfDetail = employeeProfessionalRepository.findByFkMainEmpDetailId(empGoalFeedback
									.getFkGsrDatEmpId());
						} catch (Exception ex) {
							throw new CommonCustomException("Failed to fetch employee professional details from employee id "
									+ empGoalFeedback.getFkGsrDatEmpId() + " . " + ex.getMessage());
						}

						// Entered employee is BU head or Domain Manager
						if (empProfDetail.getFkEmpDomainMgrId().intValue() == feedbackBean.getEnteredBy().intValue()
								|| empProfDetail.getMasBuUnitBO().getFkBuHeadEmpId() == feedbackBean.getEnteredBy().intValue()) 
						{
							considerOtherManagerIdFlag = true;
							feedback = gsrGoalProcessMethod(empGoalFeedback, feedbackBean, considerOtherManagerIdFlag);
						} else {
							DatEmpDetailBO enteredEmpDetail = new DatEmpDetailBO();
							try {
								enteredEmpDetail = empDetailsRepository.findByPkEmpId(feedbackBean.getEnteredBy());
							} catch (Exception ex) {
								throw new CommonCustomException(
										"Failed to fetch entered by employee details from entered by id. " + ex.getMessage());
							}

							DatEmpProfessionalDetailBO empProfessionalDetail = new DatEmpProfessionalDetailBO();
							empProfessionalDetail = employeeProfessionalRepository.findByFkMainEmpDetailId(
									empGoalFeedback.getFkGsrDatEmpId().intValue());

							DatEmpProfessionalDetailBO reportingMgrDetail = new DatEmpProfessionalDetailBO();
							try {
								reportingMgrDetail = employeeProfessionalRepository.findByFkMainEmpDetailId(
										empProfessionalDetail.getFkEmpReportingMgrId().intValue());
							} catch (Exception ex) {
								throw new CommonCustomException(
										"Failed to fetch skip level manager details from his/ her reportees id "
										+ empGoalFeedback.getFkGsrDatMgrId().intValue() + ". " + ex.getMessage());
							}
							// LOG.info("reportingMgrDetail :: " + reportingMgrDetail);

							// Entered employee is HRD Senior Executive
							if (enteredEmpDetail.getFkEmpRoleId().byteValue() == hrdSeniorExecutiveRole
									|| enteredEmpDetail.getFkEmpRoleId().byteValue() == hrdManagerRole) 
							{
								considerOtherManagerIdFlag = true;
								feedback = gsrGoalProcessMethod(empGoalFeedback, feedbackBean, considerOtherManagerIdFlag);
							}
							// For Skip Level manager
							else if (reportingMgrDetail.getFkEmpReportingMgrId().intValue() == feedbackBean
									.getEnteredBy().intValue()) 
							{
								considerOtherManagerIdFlag = true;
								feedback = gsrGoalProcessMethod( empGoalFeedback, feedbackBean,
										considerOtherManagerIdFlag);
							} else if (empGoalFeedback.getFkGsrDelegatedGoalId() != null) 
							{
								GsrDatEmpDelegatedGoalBO delegatedGoalDetails = new GsrDatEmpDelegatedGoalBO();
								try {
									delegatedGoalDetails = delegatedGoalRepository.findByPkGsrDelegatedGoalId(
											empGoalFeedback.getFkGsrDelegatedGoalId());
								} catch (Exception ex) {
									throw new CommonCustomException(
											"Failed to fetch delegated goals details from delegated goal id "
													+ empGoalFeedback.getFkGsrDelegatedGoalId()
													+ " . " + ex.getMessage());
								}

								if (delegatedGoalDetails.getFkMgrIdDelegatedTo().intValue() == feedbackBean
										.getEnteredBy().intValue()) 
								{
									considerOtherManagerIdFlag = true;
									feedback = gsrGoalProcessMethod(empGoalFeedback, feedbackBean,
											considerOtherManagerIdFlag);
								} else {
									throw new CommonCustomException("Neither input manager id " + feedbackBean.getEnteredBy()
													+ " did not match with the goal's manager id " + empGoalFeedback
													.getFkGsrDatMgrId()
													+ " nor s/he is BU head or domain manager of employee nor has a role of HRD Senior Executive.");
								}
							} else {
								throw new CommonCustomException("Input Manager id " + feedbackBean.getEnteredBy()
												+ "  is not accessed to rate the goal as it did not match with the goal's manager id "
												+ empGoalFeedback.getFkGsrDatMgrId()
												+ " nor s/he is BU head or domain manager of employee nor has a role of HRD Manager / Senior Executive.");
							}
						}
					}
				} else {
					
					if(!separationFlag)//If anybody has applied for separation and crossed the status accepted by Rm or Dm.
					{
						
						if(twoWeeksValidationFlag)
						{
						
							// Check input manager id should be same as the gsr goal object.
							if (empGoalFeedback.getFkGsrDatMgrId().intValue() == feedbackBean.getEnteredBy().intValue()) 
							{
								feedback = gsrGoalProcessMethod(empGoalFeedback, feedbackBean, considerOtherManagerIdFlag);
							} else {
								// get employee professional object from empGoalFeedback
								DatEmpProfessionalDetailBO empProfDetail = new DatEmpProfessionalDetailBO();
								try {
									empProfDetail = employeeProfessionalRepository.findByFkMainEmpDetailId(empGoalFeedback
											.getFkGsrDatEmpId());
								} catch (Exception ex) {
									throw new CommonCustomException("Failed to fetch employee professional details from employee id "
											+ empGoalFeedback.getFkGsrDatEmpId() + " . " + ex.getMessage());
								}

								// Entered employee is BU head or Domain Manager
								if (empProfDetail.getFkEmpDomainMgrId().intValue() == feedbackBean.getEnteredBy().intValue()
										|| empProfDetail.getMasBuUnitBO().getFkBuHeadEmpId() == feedbackBean.getEnteredBy().intValue()) 
								{
									considerOtherManagerIdFlag = true;
									feedback = gsrGoalProcessMethod(empGoalFeedback, feedbackBean, considerOtherManagerIdFlag);
								} else {
									DatEmpDetailBO enteredEmpDetail = new DatEmpDetailBO();
									try {
										enteredEmpDetail = empDetailsRepository.findByPkEmpId(feedbackBean.getEnteredBy());
									} catch (Exception ex) {
										throw new CommonCustomException(
												"Failed to fetch entered by employee details from entered by id. " + ex.getMessage());
									}

									DatEmpProfessionalDetailBO empProfessionalDetail = new DatEmpProfessionalDetailBO();
									empProfessionalDetail = employeeProfessionalRepository.findByFkMainEmpDetailId(
											empGoalFeedback.getFkGsrDatEmpId().intValue());

									DatEmpProfessionalDetailBO reportingMgrDetail = new DatEmpProfessionalDetailBO();
									try {
										reportingMgrDetail = employeeProfessionalRepository.findByFkMainEmpDetailId(
												empProfessionalDetail.getFkEmpReportingMgrId().intValue());
									} catch (Exception ex) {
										throw new CommonCustomException(
												"Failed to fetch skip level manager details from his/ her reportees id "
												+ empGoalFeedback.getFkGsrDatMgrId().intValue() + ". " + ex.getMessage());
									}
									// LOG.info("reportingMgrDetail :: " + reportingMgrDetail);

									// Entered employee is HRD Senior Executive
									if (enteredEmpDetail.getFkEmpRoleId().byteValue() == hrdSeniorExecutiveRole
											|| enteredEmpDetail.getFkEmpRoleId().byteValue() == hrdManagerRole) 
									{
										considerOtherManagerIdFlag = true;
										feedback = gsrGoalProcessMethod(empGoalFeedback, feedbackBean, considerOtherManagerIdFlag);
									}
									// For Skip Level manager
									else if (reportingMgrDetail.getFkEmpReportingMgrId().intValue() == feedbackBean
											.getEnteredBy().intValue()) 
									{
										considerOtherManagerIdFlag = true;
										feedback = gsrGoalProcessMethod( empGoalFeedback, feedbackBean,
												considerOtherManagerIdFlag);
									} else if (empGoalFeedback.getFkGsrDelegatedGoalId() != null) 
									{
										GsrDatEmpDelegatedGoalBO delegatedGoalDetails = new GsrDatEmpDelegatedGoalBO();
										try {
											delegatedGoalDetails = delegatedGoalRepository.findByPkGsrDelegatedGoalId(
													empGoalFeedback.getFkGsrDelegatedGoalId());
										} catch (Exception ex) {
											throw new CommonCustomException(
													"Failed to fetch delegated goals details from delegated goal id "
															+ empGoalFeedback.getFkGsrDelegatedGoalId()
															+ " . " + ex.getMessage());
										}

										if (delegatedGoalDetails.getFkMgrIdDelegatedTo().intValue() == feedbackBean
												.getEnteredBy().intValue()) 
										{
											considerOtherManagerIdFlag = true;
											feedback = gsrGoalProcessMethod(empGoalFeedback, feedbackBean,
													considerOtherManagerIdFlag);
										} else {
											throw new CommonCustomException("Neither input manager id " + feedbackBean.getEnteredBy()
															+ " did not match with the goal's manager id " + empGoalFeedback
															.getFkGsrDatMgrId()
															+ " nor s/he is BU head or domain manager of employee nor has a role of HRD Senior Executive.");
										}
									} else {
										throw new CommonCustomException("Input Manager id " + feedbackBean.getEnteredBy()
														+ "  is not accessed to rate the goal as it did not match with the goal's manager id "
														+ empGoalFeedback.getFkGsrDatMgrId()
														+ " nor s/he is BU head or domain manager of employee nor has a role of HRD Manager / Senior Executive.");
									}
								}
							}
							
						
						}else{
							throw new CommonCustomException(
								"You are not accessed to rate the organizational goal, because you are trying to rate "
									+ "before 2 weeks of the gsr goal end date " + empGoalFeedback.getGsrGoalEndDate()
									+ " .");
						}
						
					}else{
						throw new CommonCustomException("This employee is already in the process of exit clearance, hence it is not allowed you to rate his/her goal. ");	
					}
				}

			} else {// Project goals

				// Check input manager id should be same as the gsr goal object.
				if (empGoalFeedback.getFkGsrDatMgrId().intValue() == feedbackBean
						.getEnteredBy().intValue()) {
					feedback = gsrGoalProcessMethod(empGoalFeedback,
							feedbackBean, considerOtherManagerIdFlag);
				} else {
					// get employee professional object from empGoalFeedback
					DatEmpProfessionalDetailBO empProfDetail = new DatEmpProfessionalDetailBO();
					try {
						empProfDetail = employeeProfessionalRepository
								.findByFkMainEmpDetailId(empGoalFeedback
										.getFkGsrDatEmpId());
					} catch (Exception ex) {
						throw new CommonCustomException(
								"Failed to fetch emp professional detail from employee id "
										+ empGoalFeedback.getFkGsrDatEmpId()
										+ " . " + ex.getMessage());
					}

					DatEmpProfessionalDetailBO reportingMgrDetail = new DatEmpProfessionalDetailBO();
					try {
						reportingMgrDetail = employeeProfessionalRepository
								.findByFkMainEmpDetailId(empGoalFeedback
										.getFkGsrDatMgrId().intValue());
						// empDetailsRepository.findByPkEmpId(empGoalFeedback.getFkGsrDatMgrId());
					} catch (Exception ex) {
						throw new CommonCustomException(
								"Failed to fetch skip level manager details from his/her reportee id "
										+ empGoalFeedback.getFkGsrDatMgrId()
										+ " . " + ex.getMessage());
					}
					// Entered employee is BU head or Domain Manager
					if (empProfDetail.getFkEmpDomainMgrId().intValue() == feedbackBean
							.getEnteredBy().intValue()
							|| empProfDetail.getMasBuUnitBO()
									.getFkBuHeadEmpId() == feedbackBean
									.getEnteredBy().intValue()) {
						considerOtherManagerIdFlag = true;
						feedback = gsrGoalProcessMethod(empGoalFeedback,
								feedbackBean, considerOtherManagerIdFlag);
					} else {
						DatEmpDetailBO enteredEmpDetail = new DatEmpDetailBO();
						try {
							enteredEmpDetail = empDetailsRepository
									.findByPkEmpId(feedbackBean.getEnteredBy());
						} catch (Exception ex) {
							throw new CommonCustomException(
									"Failed to fetch entered emp detail from entered by id "
											+ feedbackBean.getEnteredBy()
											+ " . " + ex.getMessage());
						}
						// Entered employee is HRD Senior Executive
						if (enteredEmpDetail.getFkEmpRoleId().byteValue() == hrdSeniorExecutiveRole
								|| enteredEmpDetail.getFkEmpRoleId()
										.byteValue() == hrdManagerRole) {
							considerOtherManagerIdFlag = true;
							feedback = gsrGoalProcessMethod(empGoalFeedback,
									feedbackBean, considerOtherManagerIdFlag);
						}// For Skip Level manager
						else if (reportingMgrDetail.getFkEmpReportingMgrId()
								.intValue() == feedbackBean.getEnteredBy()
								.intValue()) {
							considerOtherManagerIdFlag = true;
							feedback = gsrGoalProcessMethod(empGoalFeedback,
									feedbackBean, considerOtherManagerIdFlag);
						} else if (empGoalFeedback.getFkGsrDelegatedGoalId() != null) {
							GsrDatEmpDelegatedGoalBO delegatedGoalDetails = new GsrDatEmpDelegatedGoalBO();
							try {
								delegatedGoalDetails = delegatedGoalRepository
										.findByPkGsrDelegatedGoalId(empGoalFeedback
												.getFkGsrDelegatedGoalId());
							} catch (Exception ex) {
								throw new CommonCustomException(
										"Failed to fetch delegated goal details from delegated goal id "
												+ empGoalFeedback
														.getFkGsrDelegatedGoalId()
														.intValue() + " . "
												+ ex.getMessage());
							}

							if (delegatedGoalDetails.getFkMgrIdDelegatedTo()
									.intValue() == feedbackBean.getEnteredBy()
									.intValue()) {
								considerOtherManagerIdFlag = true;
								feedback = gsrGoalProcessMethod(
										empGoalFeedback, feedbackBean,
										considerOtherManagerIdFlag);
							} else {
								throw new CommonCustomException(
										"Neither input manager id "
												+ feedbackBean.getEnteredBy()
												+ " did not match with the goal's manager id "
												+ empGoalFeedback
														.getFkGsrDatMgrId()
												+ " nor s/he is BU head or domain manager or skip level mgr or delegated goal mgr of employee nor has a role of HRD Manager / Senior Executive.");
							}
						} else {
							throw new CommonCustomException(
									"Neither input manager id "
											+ feedbackBean.getEnteredBy()
											+ " did not match with the goal's manager id "
											+ empGoalFeedback
													.getFkGsrDatMgrId()
											+ " nor s/he is BU head or domain manager of employee nor has a role of HRD Senior Executive.");
						}
					}
				}
			}

		} else {
			throw new CommonCustomException("Goal id "
					+ feedbackBean.getPkGsrDatEmpGoalId() + " does not exist.");
		}

		// Final rating calculation part started from this point
		if (feedback) {
			// 1. Check is there any goals of reportee pending for rating
			int goalCounter = 0;
			float avgOfFinalrating = 0.0f;
			List<Float> finalRatingFloats = new ArrayList<Float>();
			// Do final rating calculation: list of goals for that slab
			List<GsrDatEmpGoalBO> empGsrGoalsList = new ArrayList<GsrDatEmpGoalBO>();
			empGsrGoalsList = gsrDatEmpGoalRepository.getAllGoalsOfAnEmployee(
					empGoalFeedback.getFkGsrDatEmpId(),
					empGoalFeedback.getFkGsrSlabId());

			// Find out the list of unique manager id
			Set<Integer> managerIdSet = new TreeSet<Integer>();
			for (GsrDatEmpGoalBO gsrDatEmpGoalBO : empGsrGoalsList) {
				managerIdSet.add(gsrDatEmpGoalBO.getFkGsrDatMgrId());
			}

			boolean finalRatingFlag = false;
			List<GsrDatEmpGoalBO> empGsrGoalsListByMgr = new ArrayList<GsrDatEmpGoalBO>();
			for (Integer integerMgr : managerIdSet) {
				goalCounter = empGoalRepository
						.getNumberOfGoalsClosedOfEmployee(
								empGoalFeedback.getFkGsrDatEmpId(),
								empGoalFeedback.getFkGsrSlabId(), integerMgr);

				if (goalCounter == ALL_GOALS_ARE_RATED) {
					finalRatingFlag = true;
					empGsrGoalsListByMgr = gsrDatEmpGoalRepository
							.getAllGoalsOfAnEmployeeUnderMgr(
									empGoalFeedback.getFkGsrDatEmpId(),
									empGoalFeedback.getFkGsrSlabId(),
									integerMgr);

					List<Float> multiplicationOfWeightageAndMgrRating = new ArrayList<Float>();
					int sumOfWeightage = 0;
					// Calculate of each gsr goal
					for (GsrDatEmpGoalBO gsrDatEmpGoalBO : empGsrGoalsListByMgr) {
						multiplicationOfWeightageAndMgrRating
								.add(gsrDatEmpGoalBO.getGsrGoalMgrRating()
										.floatValue()
										* gsrDatEmpGoalBO.getGsrGoalWeightage()
												.intValue());
						// LOG.info("multiplicationOfWeightageAndMgrRating ::: "
						// + multiplicationOfWeightageAndMgrRating);

						sumOfWeightage = sumOfWeightage
								+ gsrDatEmpGoalBO.getGsrGoalWeightage();
						// LOG.info("sumOfWeightage :: " + sumOfWeightage);
						// LOG.info("==============================================================================");
					}

					if (sumOfWeightage < 100) {
						finalRatingFlag = false;
						break;
					}

					float sumOfMultiplyOfWeightageAndMgrRating = 0.0f;
					// Summation of all goals calculated
					for (Float float1 : multiplicationOfWeightageAndMgrRating) {
						sumOfMultiplyOfWeightageAndMgrRating = sumOfMultiplyOfWeightageAndMgrRating
								+ float1;
						// LOG.info("sumOfMultiplyOfWeightageAndMgrRating  ::: "
						// + sumOfMultiplyOfWeightageAndMgrRating);
					}

					// LOG.info("***************************************************");
					float finalRatingFloat = 0.0f;
					finalRatingFloat = sumOfMultiplyOfWeightageAndMgrRating
							/ sumOfWeightage;
					// LOG.info("finalRatingFloat  ::: " + finalRatingFloat);
					finalRatingFloats.add(finalRatingFloat);

				} else {
					finalRatingFlag = false;
					break;
				}
			}// End of for (Integer integerMgr : managerIdSet)

			// LOG.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			if (finalRatingFlag) {
				float sumOfFloatFinalRating = 0.0f;
				for (Float f : finalRatingFloats) {
					sumOfFloatFinalRating = sumOfFloatFinalRating + f;
					// LOG.info("sumOfFloatFinalRating ::: " +
					// sumOfFloatFinalRating);
				}
				float numberOfManagers = 0.0f;
				numberOfManagers = finalRatingFloats.size();
				// LOG.info("numberOfManagers :: " + numberOfManagers);
				avgOfFinalrating = sumOfFloatFinalRating / numberOfManagers;
				// Coding for single decimal place number
				int number = 0;
				int count = 0;
				String output = "";
				String stringValue = Float.toString(avgOfFinalrating);
				char[] charValue = Float.toString(avgOfFinalrating)
						.toCharArray();
				if (stringValue.contains(".")) {
					number = stringValue.indexOf(".");
				}

				for (char eachChar : charValue) {
					if (count == number + 1) {
						output = output + eachChar;
						break;
					}
					output = output + eachChar;
					++count;
				}

				avgOfFinalrating = Float.parseFloat(output);
				// End of conversion into single decimal number

				GsrDatEmpFinalRatingBO gsrDatEmpFinalRatingBO = new GsrDatEmpFinalRatingBO();
				gsrDatEmpFinalRatingBO = gsrDatEmpFinalRatingRepository
						.findByFkGsrFinalRatingSlabIdAndFkGsrFinalRatingEmpId(
								empGoalFeedback.getFkGsrSlabId(),
								empGoalFeedback.getFkGsrDatEmpId());

				// 4.If yes, just send an object with status like need to update
				// the reportee's final rating.
				if (gsrDatEmpFinalRatingBO != null) {
					if (empGoalFeedback.getFkGsrDatMgrId() != feedbackBean
							.getEnteredBy()) {
						// ManagerId represents to HR/BU-Head/Domain Manager Id
						gsrDatEmpFinalRatingBO
								.setIsOverriddenFlag(GsrConstants.OVERRIDEN);
						gsrDatEmpFinalRatingBO
								.setGsrFinalRating(avgOfFinalrating);
						/*
						 * if (gsrDatEmpFinalRatingBO.getFinalRatingStatus() ==
						 * finalRatingRejectedStatus) {
						 * gsrDatEmpFinalRatingBO.setFinalRatingStatus((byte)
						 * finalRatingRatedStatus); }//Commented by Shyam, as
						 * requested by Annapoorani & test team.
						 */
						gsrDatEmpFinalRatingBO.setModifiedDate(new Date());
						gsrDatEmpFinalRatingBO.setOverriddenBy(feedbackBean
								.getEnteredBy());
						gsrDatEmpFinalRatingBO
								.setOverriddenComments("Final rating is done by system automatically.");
						gsrDatEmpFinalRatingBO.setClosedBy(feedbackBean
								.getEnteredBy());

						gsrDatEmpFinalRatingRepository
								.save(gsrDatEmpFinalRatingBO);
					} else {
						gsrDatEmpFinalRatingBO.setIsOverriddenFlag("OVERRIDEN");
						gsrDatEmpFinalRatingBO
								.setGsrFinalRating(avgOfFinalrating);
						gsrDatEmpFinalRatingBO.setModifiedDate(new Date());
						gsrDatEmpFinalRatingBO.setOverriddenBy(empGoalFeedback
								.getFkGsrDatMgrId());
						gsrDatEmpFinalRatingBO.setClosedBy(feedbackBean
								.getEnteredBy());
						// gsrDatEmpFinalRatingBO.setCreatedDate(new Date());
						gsrDatEmpFinalRatingRepository
								.save(gsrDatEmpFinalRatingBO);
					}
					// Update these fields : finalRating, Overriden status,
					// overridden date, modified date
					// gsrDatEmpFinalRatingBO.setGsrFinalRating((float)finalRatingFloat);

				}
				// 5.If no, then create an entry into the
				// gsr_dat_emp_final_rating table.
				else {
					GsrDatEmpFinalRatingBO empFinalRatingObject = new GsrDatEmpFinalRatingBO();
					empFinalRatingObject
							.setFkGsrFinalRatingEmpId(empGoalFeedback
									.getFkGsrDatEmpId());
					empFinalRatingObject
							.setFkGsrFinalRatingMgrId(empGoalFeedback
									.getFkGsrDatMgrId());
					empFinalRatingObject
							.setFkGsrFinalRatingSlabId(empGoalFeedback
									.getFkGsrSlabId());
					empFinalRatingObject.setGsrFinalRating(avgOfFinalrating);
					empFinalRatingObject.setCreatedDate(new Date());
					empFinalRatingObject
							.setIsOverriddenFlag(GsrConstants.RATING_NOT_OVERRIDEN);
					empFinalRatingObject
							.setFinalRatingStatus((byte) finalRatingRatedStatus);
					gsrDatEmpFinalRatingRepository.save(empFinalRatingObject);
				}
			}
		}

		LOG.endUsecase("enterFeedback");
		return feedback;

	}

	private boolean gsrGoalProcessMethod(GsrDatEmpGoalBO empGoalFeedback,
			GsrEnterFeedbackBean feedbackBean,
			boolean considerOtherManagerIdFlag) throws CommonCustomException {
		boolean feedbackFlag = false;
		boolean rmChangeFlag = false;
		// find out which type of gsr goal. (Normal goal or kpi goal)
		if (empGoalFeedback.getEmpGoalKpiWeightageId() > 0) {
			// LOG.info("inside if(empGoalFeedback.getEmpGoalKpiWeightageId() > 0)");
			// Find out is there any goals having same kpi_id and slab_id and
			// goal status either of these :
			// Accomplishments entered, rated, manager feedback
			// LOG.info("KpiWeightageGoalId :: " +
			// empGoalFeedback.getEmpGoalKpiWeightageId());

			List<GsrDatEmpGoalBO> kpiWeightageIdMappedGsrGoalList = new ArrayList<GsrDatEmpGoalBO>();
			try {
				kpiWeightageIdMappedGsrGoalList = empGoalRepository
						.getGoalHavingKpiIdAndSlabIdAndMgrFeedbackStatus(
								empGoalFeedback.getEmpGoalKpiWeightageId(),
								empGoalFeedback.getFkGsrSlabId(),
								empGoalFeedback.getFkGsrDatEmpId(),
								feedbackBean.getPkGsrDatEmpGoalId());
			} catch (Exception ex) {
				throw new CommonCustomException(
						"Failed to fetch related kpiWeightage details from kpiWeightageId "
								+ empGoalFeedback.getEmpGoalKpiWeightageId()
								+ " , slab id "
								+ empGoalFeedback.getFkGsrSlabId()
								+ " , employee id "
								+ empGoalFeedback.getFkGsrDatEmpId()
								+ " and input goal id "
								+ feedbackBean.getPkGsrDatEmpGoalId() + " . "
								+ ex.getMessage());
			}
			List<GsrDatEmpGoalBO> mgrChangeGoalList = new ArrayList<GsrDatEmpGoalBO>();

			// This list contains both GsrDatEmpGoalBO entries, if it is
			// delegated. If not, then you always get only 1 record.
			if (kpiWeightageIdMappedGsrGoalList != null
					&& !kpiWeightageIdMappedGsrGoalList.isEmpty()) {
				rmChangeFlag = true;
				// LOG.info("inside if(kpiWeightageIdMappedGsrGoalList != null && kpiWeightageIdMappedGsrGoalList.size() > 1)");
				// Update all these GsrDatEmpGoalBO object into db
				for (GsrDatEmpGoalBO gsrDatEmpGoalBO : kpiWeightageIdMappedGsrGoalList) {
					boolean allowedToRateDelegateGoalFlag = false;
					if (gsrDatEmpGoalBO.getFkGsrDelegatedGoalId() != null
							&& gsrDatEmpGoalBO.getFkGsrDelegatedGoalId() > 0) {
						allowedToRateDelegateGoalFlag = true;
					}

					// LOG.info("allowedToRateDelegateGoalFlag :: " +
					// allowedToRateDelegateGoalFlag);
					if (allowedToRateDelegateGoalFlag) {
						// LOG.info("GSR Object Manager Id : " +
						// gsrDatEmpGoalBO.getFkGsrDatMgrId());
						// LOG.info("Input manager id : " +
						// feedbackBean.getManagerId());
						if (considerOtherManagerIdFlag) {
							if (gsrDatEmpGoalBO.getFkGsrGoalStatus() == goalReopenStatus
									|| gsrDatEmpGoalBO.getFkGsrGoalStatus() == goalRatedStatus
									|| gsrDatEmpGoalBO.getFkGsrGoalStatus() == goalManagerFeedbackStatus) {
								// delegatedGoalBO.setPkGsrDatEmpGoalId(feedbackBean.getPkGsrDatEmpGoalId());
								gsrDatEmpGoalBO
										.setGsrGoalMgrRating(feedbackBean
												.getGsrGoalMgrRating());
								gsrDatEmpGoalBO
										.setGsrGoalModifiedBy(feedbackBean
												.getEnteredBy());
								gsrDatEmpGoalBO
										.setGsrGoalModifiedDate(new Date());
								gsrDatEmpGoalBO
										.setFkGsrGoalStatus(goalRatedStatus);// Issue
																				// no.
																				// 133
								mgrChangeGoalList.add(gsrDatEmpGoalBO);

							} else {
								throw new CommonCustomException(
										"This goal id "
												+ gsrDatEmpGoalBO
														.getPkGsrDatEmpGoalId()
												+ " having delegate goal id "
												+ gsrDatEmpGoalBO
														.getFkGsrDelegatedGoalId()
												+ " status is not 'Re-open'. Hence, it is not allowing to provide rating and feedback "
												+ "to org-wide goal id "
												+ gsrDatEmpGoalBO
														.getEmpGoalKpiWeightageId());
							}

						} else {
							if (gsrDatEmpGoalBO.getFkGsrDatMgrId() != feedbackBean
									.getEnteredBy()) {
								if (gsrDatEmpGoalBO.getFkGsrGoalStatus() == goalManagerFeedbackStatus
										|| gsrDatEmpGoalBO.getFkGsrGoalStatus() == goalReopenStatus
										|| gsrDatEmpGoalBO.getFkGsrGoalStatus() == goalRatedStatus)// Added
																									// by
																									// Shyam
																									// for
																									// issue
																									// #276
								// Now it will allow managers to do re-modified
								// their rating.
								{
									// delegatedGoalBO.setPkGsrDatEmpGoalId(feedbackBean.getPkGsrDatEmpGoalId());
									gsrDatEmpGoalBO
											.setGsrGoalMgrRating(feedbackBean
													.getGsrGoalMgrRating());
									gsrDatEmpGoalBO
											.setGsrGoalModifiedBy(feedbackBean
													.getEnteredBy());
									gsrDatEmpGoalBO
											.setGsrGoalModifiedDate(new Date());
									gsrDatEmpGoalBO
											.setFkGsrGoalStatus(goalRatedStatus);
									mgrChangeGoalList.add(gsrDatEmpGoalBO);

								} else {
									throw new CommonCustomException(
											"This goal id "
													+ gsrDatEmpGoalBO
															.getPkGsrDatEmpGoalId()
													+ " having delegate goal id "
													+ gsrDatEmpGoalBO
															.getFkGsrDelegatedGoalId()
													+ " status is not 'Manager Feedback'. Hence, it is not allowing to provide rating and feedback "
													+ "to org-wide goal id "
													+ gsrDatEmpGoalBO
															.getEmpGoalKpiWeightageId());
								}

							} else {
								throw new CommonCustomException(
										"Here goal id "
												+ gsrDatEmpGoalBO
														.getPkGsrDatEmpGoalId()
												+ " is delegated to manager id "
												+ gsrDatEmpGoalBO
														.getFkGsrDatMgrId()
												+ " . Hence, input manager id "
												+ feedbackBean.getEnteredBy()
												+ " is not allowed to rate this goal.");
							}
						}

					} else {
						// If delegated goal flag is false and reporting manager
						// change happened.
						if (gsrDatEmpGoalBO.getFkGsrGoalStatus() == goalRatedStatus
								|| gsrDatEmpGoalBO.getFkGsrGoalStatus() == goalManagerFeedbackStatus
								|| gsrDatEmpGoalBO.getFkGsrGoalStatus() == goalReopenStatus)// Added
																							// by
																							// Shyam
						{
							gsrDatEmpGoalBO.setGsrGoalMgrRating(feedbackBean
									.getGsrGoalMgrRating());
							gsrDatEmpGoalBO.setGsrGoalModifiedBy(feedbackBean
									.getEnteredBy());
							gsrDatEmpGoalBO.setGsrGoalModifiedDate(new Date());
							gsrDatEmpGoalBO.setFkGsrGoalStatus(goalRatedStatus);
							mgrChangeGoalList.add(gsrDatEmpGoalBO);
						}
					}

				}// End of for (GsrDatEmpGoalBO gsrDatEmpGoalBO :
					// kpiWeightageIdMappedGsrGoalList)
			}
			/* } */
			// First update the current manager goal, if it fails then should
			// not allow the delegates goal/copied goal too.
			feedbackFlag = updateGsrGoalToRated(empGoalFeedback, feedbackBean,
					considerOtherManagerIdFlag, rmChangeFlag);
			LOG.info("mgrChangeGoalList :::  " + mgrChangeGoalList);
			if (mgrChangeGoalList != null && !mgrChangeGoalList.isEmpty()) {
				for (GsrDatEmpGoalBO otherMgrGoalBO : mgrChangeGoalList) {
					try {
						empGoalRepository.save(otherMgrGoalBO);
					} catch (Exception ex) {
						throw new CommonCustomException(
								"Failed to update emp goal. " + ex.getMessage());
					}
				}
			}

		} else {// If it is normal goal then flow will not be changed.
			feedbackFlag = updateGsrGoalToRated(empGoalFeedback, feedbackBean,
					considerOtherManagerIdFlag, rmChangeFlag);

		}
		return feedbackFlag;
	}

	/**
	 * 
	 * @param pkGsrDatEmpGoalId
	 * @return GsrDatEmpGoalBO
	 * @throws DataAccessException
	 *             if there is present any Data Access Exception
	 * @throws CommonCustomException
	 *             if Goal is already deleted
	 * @Description: This service is used to delete goal
	 */
	public GsrDatEmpGoalBO deleteGoal(Integer pkGsrDatEmpGoalId)
			throws DataAccessException, CommonCustomException,
			GoalNotFoundException {
		LOG.startUsecase("deleteGoal");
		GsrDatEmpGoalBO EmpGoalDelete = new GsrDatEmpGoalBO();
		EmpGoalDelete = empGoalRepository
				.findByPkGsrDatEmpGoalId(pkGsrDatEmpGoalId);
		if (EmpGoalDelete == null) {
			throw new GoalNotFoundException("Goal does not exist.");
		} else {
			empGoalRepository.delete(EmpGoalDelete);
		}

		return EmpGoalDelete;
	}

	/**
	 * 
	 * @param weightageInputBean
	 *            bean contains the input details are to be used in the service
	 * @return List<GsrKpiWeightageOutputBean>
	 * @throws BusinessException
	 * @Description: This service is used to Get goal weightage details
	 */

	public List<GsrKpiWeightageOutputBean> getWeightageDetails(
			GsrKpiWeightageInputBean weightageInputBean)
			throws BusinessException {
		LOG.startUsecase("getGsrGoalService");
		List<GsrKpiWeightageBO> weightageDetailsList = new ArrayList<GsrKpiWeightageBO>();
		List<GsrKpiWeightageOutputBean> outputBeanList = new ArrayList<GsrKpiWeightageOutputBean>();

		try {
			List<DatEmpProfessionalDetailBO> empDataList = employeeProfessionalRepository
					.findByFkMainEmpDetailIdIn(weightageInputBean
							.getEmpIdList());
			empDataList.forEach(empData -> {
				byte buId = Byte.parseByte(empData.getFkEmpBuUnit() + "");
				GsrKpiWeightageBO kpiData = gsrKpiWeightageRepos.getWeightage(
						empData.getFkEmpLevel(), buId,
						weightageInputBean.getKpiWeitageId());
				kpiData.setEmpId(empData.getFkMainEmpDetailId());
				weightageDetailsList.add(kpiData);
			});
			if (!weightageDetailsList.isEmpty()) {

				for (GsrKpiWeightageBO gsrKpiWeightageBO : weightageDetailsList) {
					GsrKpiWeightageOutputBean outputBeanDetails = new GsrKpiWeightageOutputBean();

					outputBeanDetails.setEmpId(gsrKpiWeightageBO.getEmpId());
					outputBeanDetails.setKpiWeightage(gsrKpiWeightageBO
							.getGsrKpiGoalWeightage());
					outputBeanDetails.setKpiWeightageId(gsrKpiWeightageBO
							.getPkGsrKpiWeightageId());
					outputBeanList.add(outputBeanDetails);
				}
			} else {
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		LOG.endUsecase("getGsrGoalService");
		return outputBeanList;
	}

	/**
	 * 
	 * @param weightageInputBean
	 *            bean contains the input details are to be used in the service
	 * @return List<GsrMaximumKpiOutputBean>
	 * @throws BusinessException
	 * @throws CommonCustomException
	 * @Description: This service is used to Get kpi and non kpi weightage
	 *               details
	 */

	public List<GsrMaximumKpiOutputBean> getMaximumKpiWeightage(
			GsrMaximumKpiWeightageBean weightageInputBean)
			throws BusinessException, CommonCustomException {
		try {
			List<GsrMaximumKpiOutputBean> kpiOutputBean = new ArrayList<GsrMaximumKpiOutputBean>();
			LOG.startUsecase("getGsrGoalService");
			if (weightageInputBean.getEmpIdList().size() > 0) {
				List<DatEmpProfessionalDetailBO> empDataList = employeeProfessionalRepository
						.findByFkMainEmpDetailIdIn(weightageInputBean
								.getEmpIdList());
				if (empDataList.size() == 0)
					throw new CommonCustomException(
							"Some exception occured in emplist ids.Please check manually ,all the values are not null or not.");
				else {
					for (DatEmpProfessionalDetailBO empData : empDataList) {
						Short slabId = null;
						Integer empId = empData.getFkMainEmpDetailId();
						Integer managerId = null;
						Short empLevelId = null;
						Byte buId = null;
						if (weightageInputBean.getSlabId() != null)
							slabId = weightageInputBean.getSlabId();
						else
							throw new CommonCustomException(
									"slab id shouble not to be null");
						if (empData.getFkEmpReportingMgrId().intValue() == weightageInputBean
								.getMgrId().intValue())
							managerId = empData.getFkEmpReportingMgrId();
						else
							managerId = weightageInputBean.getMgrId();
						if (managerId != null) {
							List<GsrDatEmpGoalBO> empkpiGoal = new ArrayList<GsrDatEmpGoalBO>();
							empkpiGoal = gsrDatEmpGoalRepository
									.findByFkGsrSlabIdAndFkGsrDatEmpIdAndFkGsrDatMgrIdAndEmpGoalKpiWeightageIdIsNotNull(
											slabId, empId, managerId);
							if (empkpiGoal.size() > 0) {
								List<Short> weightageIds = empkpiGoal
										.stream()
										.filter(p -> p.getFkGsrGoalStatus() != 6)
										.map(p -> p.getEmpGoalKpiWeightageId())
										.collect(Collectors.toList());
								if (weightageIds.size() > 0) {
									List<GsrKpiWeightageBO> kpiData = new ArrayList<GsrKpiWeightageBO>();
									for (Short weighatgeId : weightageIds) {
										GsrKpiWeightageBO data = gsrKpiWeightageRepos
												.findByPkGsrKpiWeightageId(weighatgeId);
										if (data != null)
											kpiData.add(data);
									}
									List<Byte> buIds = kpiData.stream()
											.map(p -> p.getGsrKpiBuId())
											.collect(Collectors.toList());
									if (buIds.size() > 0)
										if (buIds.stream().allMatch(
												buIds.get(0)::equals)) {
											buId = buIds.get(0);
											List<Short> levelids = kpiData
													.stream()
													.map(p -> p
															.getFkEmpLevelId())
													.collect(
															Collectors.toList());
											if (levelids.size() > 0)
												if (levelids
														.stream()
														.allMatch(
																levelids.get(0)::equals)) {
													empLevelId = levelids
															.get(0);
												} else {
													throw new CommonCustomException(
															"All kpi weighatage ids should be same level");
												}

										} else {
											throw new CommonCustomException(
													"All kpi weighatage ids should be same bu");
										}

								} else {
									empLevelId = empData.getFkEmpLevel();
									buId = Byte.parseByte(empData
											.getFkEmpBuUnit() + "");
								}

							} else {
								managerId = empData.getFkEmpReportingMgrId();
								empLevelId = empData.getFkEmpLevel();
								buId = Byte.parseByte(empData.getFkEmpBuUnit()
										+ "");
							}
							if (empLevelId != null && buId != null) {
								List<GsrKpiWeightageBO> kpiData = new ArrayList<GsrKpiWeightageBO>();
								kpiData = gsrKpiWeightageRepos.getAllWeightage(
										empData.getFkEmpLevel(), buId);
								if (kpiData.size() > 0) {
									Integer totalWeightage = 0;
									GsrMaximumKpiOutputBean output = new GsrMaximumKpiOutputBean();
									for (GsrKpiWeightageBO bo : kpiData) {
										String isEnable = defaultGoalsRepository
												.findByPkGsrKpiGoalId(
														bo.getFkGsrKpiGoalId())
												.getGsrkpigoalactiveflag();
										if (isEnable
												.equalsIgnoreCase("ENABLED"))
											totalWeightage = totalWeightage
													+ bo.getGsrKpiGoalWeightage();
									}
									output.setEmpBuId(Integer.parseInt(buId
											.toString()));
									output.setEmpLevelId(Integer
											.parseInt(empLevelId.toString()));
									output.setEmpReportingManager(Integer
											.parseInt(managerId.toString()));
									output.setKpiWeightage(totalWeightage);// 1
									output.setNonKpiWeightage(100 - totalWeightage);// 2
									output.setEmpId(empId);
									if (slabId != null) {
										output.setSlabId(slabId);// 3
										Integer totalKpiWeighatge = this
												.getKpiGoaldDataByemployee(
														output, slabId, empId,
														managerId);
										output.setKpiWeightageUsed(totalKpiWeighatge);// 4
										output.setKpiWeightageAvailable(output
												.getKpiWeightage()
												- totalKpiWeighatge);// 5
										Integer totalNonKpiWeighatge = this
												.getNonKpiGoaldDataByemployee(
														output, slabId, empId,
														managerId);
										output.setNonKpiWeightageUsed(totalNonKpiWeighatge);// 6
										Integer yetToBeUseKpi = this
												.getKpiGoalDataByemployeeRemainingWeighatge(
														slabId, empId,
														managerId, kpiData);
										output.setNonKpiWeightageAvailable(100
												- output.getKpiWeightageUsed()
												- yetToBeUseKpi
												- output.getNonKpiWeightageUsed());// 7
										output.setKpiWeightageAvailable(100 - (output
												.getKpiWeightageUsed() + output
												.getNonKpiWeightageUsed()));// 8
									}
									kpiOutputBean.add(output);
								}
							} else {
								throw new CommonCustomException(
										"Exception occured for retriving employee level and bu id of in this slab and slab id is "
												+ slabId);
							}
						}
					}
				}
				LOG.endUsecase("getGsrGoalService");
			}
			return kpiOutputBean;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private Integer getKpiGoaldDataByemployee(GsrMaximumKpiOutputBean output,
			Short slabId, Integer empId, Integer managerId)
			throws CommonCustomException {
		Integer totalKpiWeightage = 0;
		List<GsrDatEmpGoalBO> empkpiGoal = new ArrayList<GsrDatEmpGoalBO>();
		// if(managerId == null){
		// empkpiGoal = gsrDatEmpGoalRepository
		// .findByFkGsrSlabIdAndFkGsrDatEmpIdAndEmpGoalKpiWeightageIdIsNotNull(
		// slabId, empId);
		// }else{
		empkpiGoal = gsrDatEmpGoalRepository
				.findByFkGsrSlabIdAndFkGsrDatEmpIdAndFkGsrDatMgrIdAndEmpGoalKpiWeightageIdIsNotNull(
						slabId, empId, managerId);
		// }
		if (empkpiGoal.size() > 0) {
			for (GsrDatEmpGoalBO bo : empkpiGoal) {
				if (bo.getFkGsrGoalStatus() != 6) {
					totalKpiWeightage = totalKpiWeightage
							+ bo.getGsrGoalWeightage();
				}
			}

		}
		return totalKpiWeightage;
	}

	private Integer getKpiGoalDataByemployeeRemainingWeighatge(Short slabId,
			Integer empId, Integer managerId, List<GsrKpiWeightageBO> kpiData)
			throws CommonCustomException {
		Integer totalRemainingKpiWeightage = 0;
		List<GsrDatEmpGoalBO> empkpiGoal = new ArrayList<GsrDatEmpGoalBO>();
		// if(managerId == null){
		// empkpiGoal = gsrDatEmpGoalRepository
		// .findByFkGsrSlabIdAndFkGsrDatEmpIdAndEmpGoalKpiWeightageIdIsNotNull(
		// slabId, empId);
		// }else{
		empkpiGoal = gsrDatEmpGoalRepository
				.findByFkGsrSlabIdAndFkGsrDatEmpIdAndFkGsrDatMgrIdAndEmpGoalKpiWeightageIdIsNotNull(
						slabId, empId, managerId);
		List<Byte> statusList = new ArrayList<Byte>();

		// }
		if (empkpiGoal.size() > 0) {
			for (GsrDatEmpGoalBO gsrDatEmpGoalBO : empkpiGoal) {
				if (gsrDatEmpGoalBO.getFkGsrGoalStatus() != (byte) 6)
					statusList.add(gsrDatEmpGoalBO.getFkGsrGoalStatus());
			}
			if (statusList.size() > 0) {
				if (statusList.stream().allMatch(statusList.get(0)::equals)
						&& statusList.contains((byte) 3)) {
					totalRemainingKpiWeightage = 0;
				} else {
					for (GsrKpiWeightageBO kpi : kpiData) {
						String isEnable = defaultGoalsRepository
								.findByPkGsrKpiGoalId(kpi.getFkGsrKpiGoalId())
								.getGsrkpigoalactiveflag();
						if (isEnable.equalsIgnoreCase("ENABLED")) {
							List<GsrDatEmpGoalBO> matchedData = empkpiGoal
									.stream()
									.filter(empGoal -> empGoal
											.getEmpGoalKpiWeightageId() == kpi
											.getPkGsrKpiWeightageId()
											&& empGoal.getFkGsrGoalStatus() != 6)
									.map(data -> data)
									.collect(Collectors.toList());
							if (matchedData.size() == 0) {
								totalRemainingKpiWeightage = totalRemainingKpiWeightage
										+ kpi.getGsrKpiGoalWeightage();
							}
						}
					}
				}
			} else {
				for (GsrKpiWeightageBO kpi : kpiData) {
					String isEnable = defaultGoalsRepository
							.findByPkGsrKpiGoalId(kpi.getFkGsrKpiGoalId())
							.getGsrkpigoalactiveflag();
					if (isEnable.equalsIgnoreCase("ENABLED")) {
						List<GsrDatEmpGoalBO> matchedData = empkpiGoal
								.stream()
								.filter(empGoal -> empGoal
										.getEmpGoalKpiWeightageId() == kpi
										.getPkGsrKpiWeightageId()
										&& empGoal.getFkGsrGoalStatus() != 6)
								.map(data -> data).collect(Collectors.toList());
						if (matchedData.size() == 0) {
							totalRemainingKpiWeightage = totalRemainingKpiWeightage
									+ kpi.getGsrKpiGoalWeightage();
						}
					}
				}

			}
		} else {
			if (kpiData.size() > 0) {
				for (GsrKpiWeightageBO kpi : kpiData) {
					String isEnable = defaultGoalsRepository
							.findByPkGsrKpiGoalId(kpi.getFkGsrKpiGoalId())
							.getGsrkpigoalactiveflag();
					if (isEnable.equalsIgnoreCase("ENABLED"))
						totalRemainingKpiWeightage = totalRemainingKpiWeightage
								+ kpi.getGsrKpiGoalWeightage();
				}
			}
		}
		System.out.println("totalRemainingKpiWeightage "
				+ totalRemainingKpiWeightage);
		return totalRemainingKpiWeightage;
	}

	private Integer getNonKpiGoaldDataByemployee(
			GsrMaximumKpiOutputBean output, Short slabId, Integer empId,
			Integer managerId) {
		Integer totalNonKpiWeightage = 0;
		List<GsrDatEmpGoalBO> empkpiGoal = new ArrayList<GsrDatEmpGoalBO>();
		// if(managerId == null){
		// empkpiGoal = gsrDatEmpGoalRepository
		// .findByFkGsrSlabIdAndFkGsrDatEmpIdAndEmpGoalKpiWeightageIdIsNull(
		// slabId, empId);
		// }else{
		empkpiGoal = gsrDatEmpGoalRepository
				.findByFkGsrSlabIdAndFkGsrDatEmpIdAndFkGsrDatMgrIdAndEmpGoalKpiWeightageIdIsNull(
						slabId, empId, managerId);
		// }

		if (empkpiGoal.size() > 0) {
			for (GsrDatEmpGoalBO bo : empkpiGoal)
				if (bo.getFkGsrGoalStatus() != 6) {
					totalNonKpiWeightage = totalNonKpiWeightage
							+ bo.getGsrGoalWeightage();
				}

		}
		return totalNonKpiWeightage;
	}

	// ended by Pratibha

	/**
	 * 
	 * <Description deleteGsrGoal:> method is is used to delete the GsrGoal
	 * repository method for the deleting the GsrGoal in the data base. It will
	 * take the BO and passes it to the Repository for update the Record.
	 * 
	 * @param GsrDatEmpGoalBO
	 *            is the argument for this method.
	 * 
	 * @return a boolean value whether it is true or false.
	 * @throws BusinessException
	 * 
	 */
	public GsrDatEmpGoalBO deleteGsrGoal(Integer pkGsrDatEmpGoalId /*
																	 * GsrCreateGoalBean
																	 * bean
																	 */)
			throws BusinessException, CommonCustomException,
			GoalNotFoundException {
		LOG.startUsecase("delete gsr goal service.");
		GsrDatEmpGoalBO deleteGoal = new GsrDatEmpGoalBO();

		byte closed = 5;
		try {
			deleteGoal = createGoalrepository
					.findByPkGsrDatEmpGoalId(pkGsrDatEmpGoalId/*
															 * .getPkGsrDatEmpGoalId
															 * ()
															 */);

			if (deleteGoal == null) {
				throw new GoalNotFoundException("Goal does not exist");

			} else if (deleteGoal.getFkGsrGoalStatus() == 5) {
				throw new CommonCustomException("Goal is already Deleted");
			} else {
				deleteGoal.setFkGsrGoalStatus(closed);
				repository.save(deleteGoal);
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException("Goal is already Deleted", e);
		} catch (GoalNotFoundException e) {
			throw new GoalNotFoundException("Goal does not exist", e);
		} catch (Exception e) {
			throw new BusinessException(" Error ", e);
		}
		LOG.endUsecase(" delete gsr goal service");
		return deleteGoal;
	}

	/**
	 * 
	 * <Description viewGsrGoal:> method is is used to view the details the
	 * GsrGoal repository method for the viewing the GsrGoal in the data base.
	 * It will take the GsrDatEmpGoalId and passes it to the Repository for
	 * viewing the Record.
	 * 
	 * @param Integer
	 * @param FkEmpDetailId
	 * 
	 * @param GsrDatEmpGoalBO
	 *            is the argument for this method.
	 * 
	 * @return a boolean value whether it is true or false.
	 * @throws GoalNotFoundException
	 *             ,BusinessException
	 * 
	 */
	// Service edited by Smrithi on 20-05-2017 , edited by Rajesh Kumar 14/7/17
	public List<ViewEmployeeGoalsOutputBean> viewGsrGoal(
			Integer pkGsrDatEmpGoalId) throws CommonCustomException {
		LOG.startUsecase("view gsr goal service.");
		GsrDatEmpGoalBO getGoalBOFromGoalId = new GsrDatEmpGoalBO();
		List<GsrDatEmpGoalBO> getAnotherCopyOfGoalList = new ArrayList<GsrDatEmpGoalBO>();

		List<FeedbackListBean> feedbackList = new ArrayList<FeedbackListBean>();
		FeedbackListBean feedbackBean = new FeedbackListBean();
		List<ViewEmployeeGoalsOutputBean> gsrGoalsList = new ArrayList<ViewEmployeeGoalsOutputBean>();
		GsrDatEmpDelegatedGoalBO delegatedGoalDetails = null;

		try {
			getGoalBOFromGoalId = createGoalrepository
					.findByPkGsrDatEmpGoalId(pkGsrDatEmpGoalId);
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Failed to fetch goal details from goal id."
							+ ex.getMessage());
		}

		ViewEmployeeGoalsOutputBean empGoalOutputBean = new ViewEmployeeGoalsOutputBean();

		if (getGoalBOFromGoalId == null) {
			throw new CommonCustomException("Goal does not exist");
		} else {
			// fetching some object which is required for view goal process

			// 1. Delegate goal details
			GsrDatEmpDelegatedGoalBO delegatedGoalBO = new GsrDatEmpDelegatedGoalBO();
			try {
				delegatedGoalBO = delegatedGoalRepository
						.findByPkGsrDelegatedGoalId(getGoalBOFromGoalId
								.getFkGsrDelegatedGoalId());
			} catch (Exception ex) {
				throw new CommonCustomException(
						"Failed to retrieve delegate goal "
								+ "object by delegated goal id. "
								+ ex.getMessage());
			}

			// 2. Final rating details
			GsrDatEmpFinalRatingBO finalRatingBO = new GsrDatEmpFinalRatingBO();
			try {
				finalRatingBO = gsrDatEmpFinalRatingRepository
						.findByFkGsrFinalRatingSlabIdAndFkGsrFinalRatingEmpId(
								getGoalBOFromGoalId.getFkGsrSlabId(),
								getGoalBOFromGoalId.getFkGsrDatEmpId());
			} catch (Exception ex) {
				throw new CommonCustomException(
						"Failed to retrieve final rating object from given slab id "
								+ "and employee id. " + ex.getMessage());
			}

			// 3. Employee details
			DatEmpProfessionalDetailBO empProfDetailBO = new DatEmpProfessionalDetailBO();
			try {
				empProfDetailBO = employeeProfessionalRepository
						.getEmployeeProfessionalDetail(getGoalBOFromGoalId
								.getFkGsrDatEmpId());
			} catch (Exception ex) {
				throw new CommonCustomException(
						"Failed to fetch employee professional detail from employee id. "
								+ ex.getMessage());
			}

			// 4. Manager details
			DatEmpProfessionalDetailBO mgrProfDetailBO = new DatEmpProfessionalDetailBO();
			try {
				mgrProfDetailBO = employeeProfessionalRepository
						.getEmployeeProfessionalDetail(getGoalBOFromGoalId
								.getFkGsrDatMgrId());
			} catch (Exception ex) {
				throw new CommonCustomException(
						"Failed to fetch manager professional detail from manager id. "
								+ ex.getMessage());
			}

			// 5. Final rating ovverrriden by details
			DatEmpProfessionalDetailBO overriddenByProfDetailBO = new DatEmpProfessionalDetailBO();
			if (finalRatingBO != null) {
				try {
					overriddenByProfDetailBO = employeeProfessionalRepository
							.getEmployeeProfessionalDetail(finalRatingBO
									.getOverriddenBy());
				} catch (Exception ex) {
					throw new CommonCustomException(
							"Failed to fetch overriddenBy professional detail from overriddenBy id. "
									+ ex.getMessage());
				}
			}

			// 6. Gsr Goal Closed By details
			DatEmpProfessionalDetailBO closedByProfDetailBO = new DatEmpProfessionalDetailBO();
			if (getGoalBOFromGoalId.getGsrGoalClosedBy() != null) {
				try {
					closedByProfDetailBO = employeeProfessionalRepository
							.getEmployeeProfessionalDetail(getGoalBOFromGoalId
									.getGsrGoalClosedBy());
				} catch (Exception ex) {
					throw new CommonCustomException(
							"Failed to fetch goal closed by professional detail from closedBy id. "
									+ ex.getMessage());
				}
			}

			// -----------------------------------------------------------------------------------
			empGoalOutputBean.setIsLastTwoWeeksCheck(false);// by default is
															// false

			// 2 weeks before the gsr end date condition for kpi goal only.
			ZoneId defaultZoneId = ZoneId.systemDefault();
			// Convert Date -> Instant
			Instant instant = getGoalBOFromGoalId.getGsrGoalEndDate()
					.toInstant();
			// Instant + system default time zone + toLocalDate() = LocalDate
			LocalDate localGoalEndDate = instant.atZone(defaultZoneId)
					.toLocalDate();
			LocalDate localTodayDate = LocalDate.now();
			if (localTodayDate.isAfter(localGoalEndDate
					.minusDays(goalAllowedRateDays))) {
				// Overwrite the value
				empGoalOutputBean.setIsLastTwoWeeksCheck(true);
			}

			if (empProfDetailBO != null) {
				empGoalOutputBean.setEmpId(empProfDetailBO
						.getFkMainEmpDetailId());
				empGoalOutputBean.setEmpName(empProfDetailBO.getEmpFirstName()
						+ " " + empProfDetailBO.getEmpLastName());
			}

			if (mgrProfDetailBO != null) {
				empGoalOutputBean.setMgrId(mgrProfDetailBO
						.getFkMainEmpDetailId());
				empGoalOutputBean.setMgrName(mgrProfDetailBO.getEmpFirstName()
						+ " " + mgrProfDetailBO.getEmpLastName());
				empGoalOutputBean.setCurrentReportingMgrId(getGoalBOFromGoalId
						.getFkGsrDatMgrId()); // Added to display the current
												// reporting manager id.
				feedbackBean.setMgrId(mgrProfDetailBO.getFkMainEmpDetailId());
				feedbackBean.setMgrName(mgrProfDetailBO.getEmpFirstName() + " "
						+ mgrProfDetailBO.getEmpLastName());
			}
			DatEmpPersonalDetailBO personalDetails;
			if (delegatedGoalBO != null) {
				empGoalOutputBean.setFkGsrDelegatedGoalId(delegatedGoalBO
						.getPkGsrDelegatedGoalId());
				empGoalOutputBean.setFkGsrDelegatedFromMgrId(delegatedGoalBO
						.getFkMgrIdDelegatedFrom());
				empGoalOutputBean.setDelegationStatus(delegatedGoalBO
						.getDelegationStatus());
				empGoalOutputBean.setFkMgrIdDelegatedTo(delegatedGoalBO
						.getFkMgrIdDelegatedTo());

				feedbackBean
						.setDelegatedMgrName(getEmpNameByEmpId(delegatedGoalBO
								.getFkMgrIdDelegatedTo()));
				feedbackBean.setDelegationStatus(delegatedGoalBO
						.getDelegationStatus());
				feedbackBean.setFkMgrIdDelegatedTo(delegatedGoalBO
						.getFkMgrIdDelegatedTo());

				try {
					personalDetails = empPersonalRepository
							.findByFkEmpDetailId(delegatedGoalBO
									.getFkMgrIdDelegatedTo());
				} catch (Exception ex) {
					throw new CommonCustomException(
							"Failed to fetch personal details from goal id ");
				}
				empGoalOutputBean.setDelegatedMgrName(personalDetails
						.getEmpFirstName()
						+ " "
						+ personalDetails.getEmpLastName());

			}

			if (finalRatingBO != null) {
				if (finalRatingBO.getSkipLevelMgrId() != null) {
					empGoalOutputBean.setSkipLevelMgrId(finalRatingBO
							.getSkipLevelMgrId());
				}

				empGoalOutputBean.setFinalRating(Float.toString(finalRatingBO
						.getGsrFinalRating()));
				switch (finalRatingBO.getFinalRatingStatus()) {
				case 1:
					empGoalOutputBean.setFinalRatingStatus("Not Yet Rated");
					break;
				case 2:
					empGoalOutputBean.setFinalRatingStatus("Accepted");
					break;
				case 3:
					empGoalOutputBean.setFinalRatingStatus("Rejected");
					break;
				case 4:
					empGoalOutputBean.setFinalRatingStatus("Resolved");
					break;
				case 5:
					empGoalOutputBean.setFinalRatingStatus("Rated");
					break;
				case 6:
					empGoalOutputBean.setFinalRatingStatus("Re-Open");
					break;
				default:
					throw new CommonCustomException(
							"Invalid final rating status id. ");
				}

				empGoalOutputBean.setIsOverridden(finalRatingBO
						.getIsOverriddenFlag());
				empGoalOutputBean.setFinalRatingModifiedDate(finalRatingBO
						.getModifiedDate());

				if (finalRatingBO.getRatingAcceptOrRejectDate() != null)
					empGoalOutputBean
							.setRatingAcceptedRejectedDate(finalRatingBO
									.getRatingAcceptOrRejectDate());

				if (finalRatingBO.getOverriddenBy() != null) {
					empGoalOutputBean.setOverridenBy(finalRatingBO
							.getOverriddenBy());
					empGoalOutputBean
							.setOverridenByName(overriddenByProfDetailBO
									.getEmpFirstName()
									+ " "
									+ overriddenByProfDetailBO.getEmpLastName());
					// empGoalOutputBean.setFinalRatingModifiedDate(finalRatingBO.getModifiedDate());
				}
			}

			if (closedByProfDetailBO != null) {
				empGoalOutputBean.setClosedBy(getGoalBOFromGoalId
						.getGsrGoalClosedBy());
				empGoalOutputBean.setClosedByName(closedByProfDetailBO
						.getEmpFirstName()
						+ " "
						+ closedByProfDetailBO.getEmpLastName());
			}

			empGoalOutputBean.setGoalId(getGoalBOFromGoalId
					.getPkGsrDatEmpGoalId().intValue());
			empGoalOutputBean.setGoalName(getGoalBOFromGoalId
					.getGsrDatEmpGoalName());
			empGoalOutputBean.setGoalWeightage(Byte
					.toString(getGoalBOFromGoalId.getGsrGoalWeightage()));
			empGoalOutputBean.setSlabId(getGoalBOFromGoalId
					.getGsrSlabBO_slab_id().getPkGsrSlabId());
			empGoalOutputBean.setSlabYear(getGoalBOFromGoalId
					.getGsrSlabBO_slab_id().getGsrSlabYear());
			empGoalOutputBean.setSlabStartDate(getGoalBOFromGoalId
					.getGsrGoalStartDate());
			empGoalOutputBean.setSlabEndDate(getGoalBOFromGoalId
					.getGsrGoalEndDate());
			empGoalOutputBean.setGsrGoalModifiedBy(getGoalBOFromGoalId
					.getGsrGoalModifiedBy()); // added by Smrithi on 4-9-17.
			if (empGoalOutputBean.getGsrGoalModifiedBy() != null) // null check
																	// for
																	// modifiedByName
				empGoalOutputBean
						.setGsrGoalModifiedByName(getEmpNameByEmpId(getGoalBOFromGoalId
								.getGsrGoalModifiedBy().intValue())); // added
																		// by
																		// Smrithi
																		// on
																		// 4-9-17.
			else
				empGoalOutputBean.setGsrGoalModifiedByName(null);
			empGoalOutputBean.setGsrGoalModifiedDate(getGoalBOFromGoalId
					.getGsrGoalModifiedDate()); // added by Smrithi on 4-9-17.
			empGoalOutputBean.setSlabShortName(getGoalBOFromGoalId
					.getGsrSlabBO_slab_id().getGsrSlabShortName());
			empGoalOutputBean.setGoalDuration(getGoalBOFromGoalId
					.getGsrSlabBO_slab_id().getGsrSlabName());

			if (getGoalBOFromGoalId.getGsrGoalMgrFeedback() != null) {
				if (getGoalBOFromGoalId.getGsrGoalMgrRating() != null) {
					empGoalOutputBean
							.setMgrRating(Float.toString(getGoalBOFromGoalId
									.getGsrGoalMgrRating()));
					feedbackBean.setRating(getGoalBOFromGoalId
							.getGsrGoalMgrRating());
				}
				empGoalOutputBean.setMgrFeedback(getGoalBOFromGoalId
						.getGsrGoalMgrFeedback());
				feedbackBean.setMgrFeedback(getGoalBOFromGoalId
						.getGsrGoalMgrFeedback());
			}

			empGoalOutputBean.setGoalDescription(getGoalBOFromGoalId
					.getGsrGoalDescription());
			empGoalOutputBean.setGoalMeasurement(getGoalBOFromGoalId
					.getGsrGoalMeasurement());

			if (getGoalBOFromGoalId.getGsrGoalAccomplishment() != null) {
				empGoalOutputBean.setAccomplishments(getGoalBOFromGoalId
						.getGsrGoalAccomplishment());
			}

			if (getGoalBOFromGoalId.getGsrGoalSelfRating() != null) {
				empGoalOutputBean.setSelfRating(Float
						.toString(getGoalBOFromGoalId.getGsrGoalSelfRating()));
			}

			empGoalOutputBean.setGoalStatusId(getGoalBOFromGoalId
					.getFkGsrGoalStatus());
			empGoalOutputBean.setGoalStatusName(getGoalBOFromGoalId
					.getGsrMasGoalStatusBOgoalstatus().getGsrGoalStatusName());

			feedbackList.add(feedbackBean);

			if (getGoalBOFromGoalId.getEmpGoalKpiWeightageId() > 0) {
				empGoalOutputBean.setKpiGoalWeightage(getGoalBOFromGoalId
						.getGsrKpiWeightageDetails().getGsrKpiGoalWeightage());
				empGoalOutputBean.setKpiGoalId(getGoalBOFromGoalId
						.getEmpGoalKpiWeightageId());
				empGoalOutputBean.setEmpGoalKpiWeightageId(getGoalBOFromGoalId
						.getEmpGoalKpiWeightageId());

				// If reporting manager change happen then fetch previous mgr
				// goal(s) details too.
				try {
					getAnotherCopyOfGoalList = createGoalrepository
							.findByFkGsrDatEmpIdAndFkGsrSlabIdAndEmpGoalKpiWeightageIdAndPkGsrDatEmpGoalIdNot(
									getGoalBOFromGoalId.getFkGsrDatEmpId(),
									getGoalBOFromGoalId.getFkGsrSlabId(),
									getGoalBOFromGoalId
											.getEmpGoalKpiWeightageId(),
									getGoalBOFromGoalId.getPkGsrDatEmpGoalId());
				} catch (Exception ex) {
					throw new CommonCustomException(
							"Failed to fetch goal details from goal id, "
									+ "employee id, slab id and kpiWeightageId."
									+ ex.getMessage());
				}

				if (!getAnotherCopyOfGoalList.isEmpty()) {

					if (getGoalBOFromGoalId.getPkGsrDatEmpGoalId() < getAnotherCopyOfGoalList
							.get(0).getPkGsrDatEmpGoalId()) // Added to display
															// the current
															// reporting manager
															// id.
						empGoalOutputBean
								.setCurrentReportingMgrId(getAnotherCopyOfGoalList
										.get(0).getFkGsrDatMgrId()); // Added to
																		// display
																		// the
																		// current
																		// reporting
																		// manager
																		// id.
					if (getGoalBOFromGoalId.getFkGsrGoalStatus() != goalRejectedStatus) { // Added
																							// to
																							// remove
																							// the
																							// duplicate
																							// feedback
																							// .
						for (GsrDatEmpGoalBO gsrDatEmpGoalBO : getAnotherCopyOfGoalList) {
							if (gsrDatEmpGoalBO.getFkGsrGoalStatus() != goalRejectedStatus) // Added
																							// to
																							// remove
																							// the
																							// duplicate
																							// feedback
																							// .
							{
								if (getGoalBOFromGoalId.getPkGsrDatEmpGoalId() < gsrDatEmpGoalBO
										.getPkGsrDatEmpGoalId()) // Added to
																	// display
																	// the
																	// current
																	// reporting
																	// manager
																	// id.
									empGoalOutputBean
											.setCurrentReportingMgrId(gsrDatEmpGoalBO
													.getFkGsrDatMgrId()); // Added
																			// to
																			// display
																			// the
																			// current
																			// reporting
																			// manager
																			// id.
								feedbackBean = new FeedbackListBean();
								feedbackBean.setMgrId(gsrDatEmpGoalBO
										.getFkGsrDatMgrId());
								feedbackBean
										.setMgrName(getEmpNameByEmpId(gsrDatEmpGoalBO
												.getFkGsrDatMgrId()));

								if (gsrDatEmpGoalBO.getGsrGoalMgrRating() != null)
									feedbackBean.setRating(gsrDatEmpGoalBO
											.getGsrGoalMgrRating());

								if (gsrDatEmpGoalBO.getFkGsrDelegatedGoalId() != null) {
									delegatedGoalDetails = delegatedGoalRepository
											.findByPkGsrDelegatedGoalId(gsrDatEmpGoalBO
													.getFkGsrDelegatedGoalId());
									feedbackBean
											.setDelegatedMgrName(getEmpNameByEmpId(delegatedGoalDetails
													.getFkMgrIdDelegatedTo()));
									feedbackBean
											.setDelegationStatus(delegatedGoalDetails
													.getDelegationStatus());
									feedbackBean
											.setFkMgrIdDelegatedTo(delegatedGoalDetails
													.getFkMgrIdDelegatedTo());
								} else {
									feedbackBean.setDelegatedMgrName(null);
									feedbackBean.setDelegationStatus(null);
									feedbackBean.setFkMgrIdDelegatedTo(null);
								}
								if (gsrDatEmpGoalBO.getGsrGoalMgrFeedback() != null)
									feedbackBean.setMgrFeedback(gsrDatEmpGoalBO
											.getGsrGoalMgrFeedback());
								feedbackList.add(feedbackBean);
							}
						}

					}

				}
			}

		}
		empGoalOutputBean.setFeedbackList(feedbackList);
		gsrGoalsList.add(empGoalOutputBean);
		LOG.endUsecase(" view gsr goal service");
		return gsrGoalsList;
	}

	// EOA by Ritesh and Edited by Smrithi 13/06/2017, Edited by Rajesh Kumar
	// 14/7/17

	/* Started By Rajesh Kumar */
	/**
	 * 
	 * <Description getDatEmpGoal:> method is call the repository method to view
	 * the Current Goal data in the data base. It will take the BO and passes it
	 * to the Repository for update the Record.
	 * 
	 * @author THBS
	 * 
	 * @param GsrDatEmpGoalBO
	 *            is the argument for this method.
	 * 
	 * @return return the list of data in Gsr Dat Emp Goal BO according to the
	 *         input details.
	 * 
	 * @throws BusinessException
	 *             throws if any exception occurs during the data retrieval.
	 * 
	 */
	public GsrDatEmpGoalBO getDatEmpGoal(GsrViewCurrentGoalsBean bean)
			throws BusinessException, GoalNotFoundException {
		LOG.startUsecase("getGsrGoalService");
		GsrDatEmpGoalBO viewCurrentGoal = new GsrDatEmpGoalBO();

		try {
			viewCurrentGoal = empGoalRepository
					.findByPkGsrDatEmpGoalIdAndGsrGoalStartDateAndGsrGoalEndDate(
							bean.getpkGsrDatEmpGoalId(),
							bean.getgsrGoalStartDate(),
							bean.getgsrGoalEndDate());
			if (viewCurrentGoal == null) {
				throw new GoalNotFoundException("Goal does not exist");

			}
		} catch (GoalNotFoundException e) {
			throw new GoalNotFoundException("Goal does not exist", e);
		} catch (Exception e) {
			throw new BusinessException("exception ", e);
		}
		LOG.endUsecase("getGsrGoalService");
		return viewCurrentGoal;
	}

	/**
	 * 
	 * <Description getAccomplishment:> method is call the repository method to
	 * Enter Accomplishment data in the data base. It will take the BO and
	 * passes it to the Repository for update the Record.
	 * 
	 * @author THBS
	 * 
	 * @param GsrDatEmpGoalBO
	 *            is the argument for this method.
	 * 
	 * @return return the list of data in Gsr Dat Emp Goal BO according to the
	 *         input details.
	 * 
	 * @throws BusinessException
	 *             throws if any exception occurs during the data retrieval.
	 * @throws GoalNotFoundException
	 * @throws CommonCustomException
	 * 
	 */
	public GsrEnterAccomplishmentBean getAccomplishment(
			GsrEnterAccomplishmentBean bean) throws BusinessException,
			ParseException, GoalNotFoundException, CommonCustomException {
		LOG.startUsecase("getGsrGoalService");
		GsrEnterAccomplishmentBean outputBean;
		GsrDatEmpGoalBO enterAccomplishment = new GsrDatEmpGoalBO();
		outputBean = new GsrEnterAccomplishmentBean();
		enterAccomplishment = gsrRepository.findByPkGsrDatEmpGoalId(bean
				.getPkGsrDatEmpGoalId());
		if (enterAccomplishment == null) {
			throw new GoalNotFoundException("ID " + bean.getPkGsrDatEmpGoalId()
					+ " does not exist.");
		} else {
			if (enterAccomplishment.getFkGsrDatEmpId().intValue() == bean
					.getFkGsrDatEmpId().intValue()) {
				if (enterAccomplishment.getFkGsrGoalStatus() == 2) {
					enterAccomplishment.setGsrGoalAccomplishment(bean
							.getGsrGoalAccomplishment());
					if (bean.getGsrGoalSelfRating() == null) {
						enterAccomplishment
								.setGsrGoalSelfRating(enterAccomplishment
										.getGsrGoalSelfRating());
					} else {
						enterAccomplishment.setGsrGoalSelfRating(bean
								.getGsrGoalSelfRating());
					}
					enterAccomplishment.setGsrGoalModifiedDate(new Date());
					enterAccomplishment.setGsrGoalModifiedBy(bean
							.getFkGsrDatEmpId());
					enterAccomplishment.setFkGsrGoalStatus((byte) 3);
					outputBean.setUpdateGoalFlag(false);
					gsrRepository.save(enterAccomplishment);
				} else if (enterAccomplishment.getFkGsrGoalStatus() == 3) {
					enterAccomplishment.setGsrGoalAccomplishment(bean
							.getGsrGoalAccomplishment());
					if (bean.getGsrGoalSelfRating() == null) {
						enterAccomplishment
								.setGsrGoalSelfRating(enterAccomplishment
										.getGsrGoalSelfRating());
					} else {
						enterAccomplishment.setGsrGoalSelfRating(bean
								.getGsrGoalSelfRating());
					}
					enterAccomplishment.setGsrGoalModifiedDate(new Date());
					outputBean.setUpdateGoalFlag(true);
					gsrRepository.save(enterAccomplishment);
				} else if (enterAccomplishment.getFkGsrGoalStatus() == 4) {
					throw new CommonCustomException(
							"Accomplishments can not be entered for a goal whose status is approved.");

				} else if (enterAccomplishment.getFkGsrGoalStatus() == 5) {
					throw new CommonCustomException(
							"Accomplishments can not be entered for a goal whose status is closed.");

				} else if (enterAccomplishment.getFkGsrGoalStatus() == 6) {
					throw new CommonCustomException(
							"Accomplishments can not be entered for a goal whose status is rejected.");

				} else if (enterAccomplishment.getFkGsrGoalStatus() == 7) {
					throw new CommonCustomException(
							"Accomplishments can not be entered for a goal whose status is rejected.");

				} else {
					throw new CommonCustomException(
							"Goals need to be set before entering accomplishments.");
				}
			} else {
				throw new CommonCustomException(
						"Invalid emploee id / goal id. Employee ID does not match with the Goal Id.");
			}
		}
		outputBean.setFkGsrDatEmpId(enterAccomplishment.getFkGsrDatEmpId());
		outputBean.setPkGsrDatEmpGoalId(enterAccomplishment
				.getPkGsrDatEmpGoalId());
		outputBean.setGsrGoalAccomplishment(enterAccomplishment
				.getGsrGoalAccomplishment());
		outputBean.setGsrGoalSelfRating(enterAccomplishment
				.getGsrGoalSelfRating());
		outputBean.setGsrGoalModifiedDate(enterAccomplishment
				.getGsrGoalModifiedDate());
		LOG.endUsecase("getGsrGoalService");
		return outputBean;

	}

	/**
	 * 
	 * @param bean
	 *            bean contains the input details are to be used in the service.
	 * @return GsrCreateDelegatedGoalOutputBean
	 * @throws CommonCustomException
	 *             if there is present any Common Custom Exception
	 * @throws DataAccessExceptio
	 * @Description: This service is used to create delegate goals
	 */

	public GsrCreateDelegatedGoalOutputBean createDelegateGoal(
			GsrDelegateGoalBean bean) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("getGsrDelegateGoal");
		GsrDatEmpGoalBO datEmpGoalBo = new GsrDatEmpGoalBO();

		if (!isValidEmpId(bean.getDelegateToManagerId())) {
			throw new CommonCustomException("Invalid delegated to manager id.");
		}

		if (!isValidEmpId(bean.getDelegatedBy())) {
			throw new CommonCustomException("Invalid delegatedBy id.");
		}

		List<GsrDelegateOutputBean> listOfFailedCreatedDelegatedGoal = new ArrayList<GsrDelegateOutputBean>();
		List<GsrDelegateOutputBean> listOfSuccessfulCreatedDelegatedGoal = new ArrayList<GsrDelegateOutputBean>();
		GsrCreateDelegatedGoalOutputBean gsrCreateDelegatedGoalOutputBean = new GsrCreateDelegatedGoalOutputBean();
		if (bean.getDelegatedBy().equals(bean.getDelegateToManagerId())) {
			throw new CommonCustomException(
					"DelegatedFromManager Id should be different with DelegatedToManager Id.");
		}

		if (!bean.getGoalIdList().isEmpty()) {
			for (Integer goalId : bean.getGoalIdList()) {
				// boolean goalConsiderForCreateFlag = false;
				try {
					datEmpGoalBo = repository
							.getNonRejectedGoalDetailsByGoalId(goalId);
				} catch (Exception ex) {
					throw new CommonCustomException(
							"Failed to fetch gsr goals for a reportee."
									+ ex.getMessage());
				}
				if (datEmpGoalBo != null) {
					if (datEmpGoalBo.getFkGsrDatEmpId().equals(
							bean.getDelegateToManagerId())) {
						throw new CommonCustomException(
								"Goal should not be delegated to same employee");
					}

					if (datEmpGoalBo.getFkGsrGoalStatus().byteValue() == goalAccomplishmentEnteredStatus
							|| datEmpGoalBo.getFkGsrGoalStatus().byteValue() == goalGoalSetStatus)// as
																									// shyam
																									// told
					{
						GsrDatEmpDelegatedGoalBO delegateGoal = new GsrDatEmpDelegatedGoalBO();
						delegateGoal = delegatedGoalRepository
								.checkDelegatedGoalByGsrGoalId(goalId);

						if (delegateGoal != null) {
							// check delegated goal status
							// if it is accepted, check the
							// Fk_mgr_id_delegated_to = delegated by
							// if yes,

							if (delegateGoal.getDelegationStatus().byteValue() == delegationAcceptedStatus) {
								if (delegateGoal.getFkMgrIdDelegatedTo()
										.intValue() == bean.getDelegatedBy()
										.intValue()) {
									delegateGoal.setFkMgrIdDelegatedFrom(bean
											.getDelegatedBy());
									delegateGoal.setFkMgrIdDelegatedTo(bean
											.getDelegateToManagerId());
									delegateGoal.setDelegatedBy(bean
											.getDelegatedBy());
									delegateGoal
											.setDelegationStatus(delegationWaitingForAcceptanceStatus);
									delegateGoal
											.setAcceptedOrRejectedDate(null);
									delegateGoal.setDelegatedDate(new Date());
									try {
										delegateGoal = delegatedGoalRepository
												.save(delegateGoal);
									} catch (Exception e) {
										throw new CommonCustomException(
												"Failed to save/store delegate goal."
														+ e.getMessage());
									}

									GsrDelegateOutputBean outputBean = new GsrDelegateOutputBean();
									if (delegateGoal != null) {
										GsrDatEmpGoalBO empGoal = new GsrDatEmpGoalBO();
										datEmpGoalBo
												.setFkGsrDelegatedGoalId(delegateGoal
														.getPkGsrDelegatedGoalId());
										empGoal = repository.save(datEmpGoalBo);

										outputBean
												.setAcceptOrRejectedDate(delegateGoal
														.getAcceptedOrRejectedDate());
										outputBean.setDelegatedBy(delegateGoal
												.getDelegatedBy());
										outputBean
												.setDelegatedDate(delegateGoal
														.getDelegatedDate());
										outputBean
												.setDelegatedEmpId(delegateGoal
														.getFkGsrDelegatedEmpId());
										outputBean
												.setDelegatedFrom(delegateGoal
														.getFkMgrIdDelegatedFrom());
										outputBean
												.setDelegatedGoalId(delegateGoal
														.getPkGsrDelegatedGoalId());
										outputBean
												.setDelegatedStatus(delegateGoal
														.getDelegationStatus());
										outputBean.setDelegatedTo(delegateGoal
												.getFkMgrIdDelegatedTo());
										outputBean.setEmpGoalID(delegateGoal
												.getFkGsrDatEmpGoalId());
										outputBean.setGoalName(empGoal
												.getGsrDatEmpGoalName());
										outputBean
												.setDelegatedToMgrName(getEmpNameByEmpId(delegateGoal
														.getFkMgrIdDelegatedTo()));
										outputBean
												.setDelegationStatusMessage("Goal has been delegated to "
														+ getEmpNameByEmpId(delegateGoal
																.getFkMgrIdDelegatedTo())
														+ "-"
														+ delegateGoal
																.getFkMgrIdDelegatedTo()
														+ ". Please await response from "
														+ getEmpNameByEmpId(delegateGoal
																.getFkMgrIdDelegatedTo())
														+ "-"
														+ delegateGoal
																.getFkMgrIdDelegatedTo()
														+ ".");
										listOfSuccessfulCreatedDelegatedGoal
												.add(outputBean);
									}
								} else {
									// If no,
									GsrDelegateOutputBean outputBean = new GsrDelegateOutputBean();
									outputBean.setEmpGoalID(goalId);
									outputBean
											.setDelegationStatusMessage("FAILED : Only accepted manager"
													+ delegateGoal
															.getFkMgrIdDelegatedTo()
															.intValue()
													+ " can delegate goals to other");
									listOfFailedCreatedDelegatedGoal
											.add(outputBean);
									/*
									 * throw new CommonCustomException(
									 * "Only accepted manager " +
									 * delegateGoal.getFkMgrIdDelegatedTo
									 * ().intValue() +
									 * " can delegate goals to other");
									 */
								}
							}

							else if (delegateGoal.getDelegationStatus()
									.byteValue() == delegationRejectedStatus) {
								GsrDelegateOutputBean outputBean = new GsrDelegateOutputBean();
								delegateGoal.setFkMgrIdDelegatedFrom(bean
										.getDelegatedBy());
								delegateGoal.setFkMgrIdDelegatedTo(bean
										.getDelegateToManagerId());
								delegateGoal
										.setDelegationStatus(delegationWaitingForAcceptanceStatus);
								delegateGoal.setAcceptedOrRejectedDate(null);
								delegateGoal.setDelegatedDate(new Date());
								try {
									delegateGoal = delegatedGoalRepository
											.save(delegateGoal);
								} catch (Exception e) {
									throw new CommonCustomException(
											"Failed to save/store delegate goal."
													+ e.getMessage());
								}

								GsrDelegateOutputBean outputBean1 = new GsrDelegateOutputBean();
								if (delegateGoal != null) {
									GsrDatEmpGoalBO empGoal = new GsrDatEmpGoalBO();
									datEmpGoalBo
											.setFkGsrDelegatedGoalId(delegateGoal
													.getPkGsrDelegatedGoalId());
									empGoal = repository.save(datEmpGoalBo);

									outputBean1
											.setAcceptOrRejectedDate(delegateGoal
													.getAcceptedOrRejectedDate());
									outputBean1.setDelegatedBy(delegateGoal
											.getDelegatedBy());
									outputBean1.setDelegatedDate(delegateGoal
											.getDelegatedDate());
									outputBean1.setDelegatedEmpId(delegateGoal
											.getFkGsrDelegatedEmpId());
									outputBean1.setDelegatedFrom(delegateGoal
											.getFkMgrIdDelegatedFrom());
									outputBean1.setDelegatedGoalId(delegateGoal
											.getPkGsrDelegatedGoalId());
									outputBean1.setDelegatedStatus(delegateGoal
											.getDelegationStatus());
									outputBean1.setDelegatedTo(delegateGoal
											.getFkMgrIdDelegatedTo());
									outputBean1.setEmpGoalID(delegateGoal
											.getFkGsrDatEmpGoalId());
									outputBean1.setGoalName(empGoal
											.getGsrDatEmpGoalName());
									outputBean1
											.setDelegatedToMgrName(getEmpNameByEmpId(delegateGoal
													.getFkMgrIdDelegatedTo()));
									outputBean1
											.setDelegationStatusMessage("Goal has been delegated to "
													+ getEmpNameByEmpId(delegateGoal
															.getFkMgrIdDelegatedTo())
													+ "-"
													+ delegateGoal
															.getFkMgrIdDelegatedTo()
													+ ". Please await response from "
													+ getEmpNameByEmpId(delegateGoal
															.getFkMgrIdDelegatedTo())
													+ "-"
													+ delegateGoal
															.getFkMgrIdDelegatedTo()
													+ ".");
									listOfSuccessfulCreatedDelegatedGoal
											.add(outputBean1);
								} else {
									outputBean1.setEmpGoalID(goalId);
									outputBean1
											.setDelegationStatusMessage("FAILED : Only accepted manager "
													+ delegateGoal
															.getFkMgrIdDelegatedTo()
															.intValue()
													+ " can delegate goals to other");
									listOfFailedCreatedDelegatedGoal
											.add(outputBean1);
								}

							} else {
								GsrDelegateOutputBean gsrDelegateOutputBean = new GsrDelegateOutputBean();
								gsrDelegateOutputBean.setEmpGoalID(goalId);
								gsrDelegateOutputBean
										.setDelegationStatusMessage("FAILED : Goal "
												+ datEmpGoalBo
														.getGsrDatEmpGoalName()
												+ "  has already been delegated.");
								listOfFailedCreatedDelegatedGoal
										.add(gsrDelegateOutputBean);
							}
						} else {
							GsrDatEmpDelegatedGoalBO delegateGoalObj = new GsrDatEmpDelegatedGoalBO();
							delegateGoalObj.setFkGsrDelegatedEmpId(datEmpGoalBo
									.getFkGsrDatEmpId());
							delegateGoalObj
									.setFkMgrIdDelegatedFrom(datEmpGoalBo
											.getFkGsrDatMgrId());
							delegateGoalObj.setFkMgrIdDelegatedTo(bean
									.getDelegateToManagerId());
							delegateGoalObj
									.setDelegationStatus(delegationWaitingForAcceptanceStatus);
							delegateGoalObj.setDelegatedBy(bean
									.getDelegatedBy());
							delegateGoalObj.setDelegatedDate(new Date());
							delegateGoalObj.setFkGsrDatEmpGoalId(goalId);
							try {
								delegateGoalObj = delegateGoalRepository
										.save(delegateGoalObj);
							} catch (Exception ex) {
								throw new CommonCustomException(
										"Failed to save/store delegate goal."
												+ ex.getMessage());
							}

							if (delegateGoalObj != null) {
								GsrDatEmpGoalBO empGoal = new GsrDatEmpGoalBO();
								datEmpGoalBo
										.setFkGsrDelegatedGoalId(delegateGoalObj
												.getPkGsrDelegatedGoalId());
								// confirm wih anu
								// datEmpGoalBo.setGsrGoalModifiedDate(new
								// Date());
								// datEmpGoalBo.setGsrGoalModifiedBy(delegateGoalObj.getFkMgrIdDelegatedFrom());
								try {
									empGoal = repository.save(datEmpGoalBo);
								} catch (Exception ex) {
									throw new CommonCustomException(
											"Failed to save/store delegate goal id into gsr_dat_emp_goal table."
													+ ex.getMessage());
								}

								GsrDelegateOutputBean outputBean = new GsrDelegateOutputBean();
								outputBean
										.setAcceptOrRejectedDate(delegateGoalObj
												.getAcceptedOrRejectedDate());
								outputBean.setDelegatedBy(delegateGoalObj
										.getDelegatedBy());
								outputBean.setDelegatedDate(delegateGoalObj
										.getDelegatedDate());
								outputBean.setDelegatedEmpId(delegateGoalObj
										.getFkGsrDelegatedEmpId());
								outputBean.setDelegatedFrom(delegateGoalObj
										.getFkMgrIdDelegatedFrom());
								outputBean.setDelegatedGoalId(delegateGoalObj
										.getPkGsrDelegatedGoalId());
								outputBean.setDelegatedStatus(delegateGoalObj
										.getDelegationStatus());
								outputBean.setDelegatedTo(delegateGoalObj
										.getFkMgrIdDelegatedTo());
								outputBean.setEmpGoalID(delegateGoalObj
										.getFkGsrDatEmpGoalId());
								outputBean.setGoalName(empGoal
										.getGsrDatEmpGoalName());
								outputBean
										.setDelegatedToMgrName(getEmpNameByEmpId(delegateGoalObj
												.getFkMgrIdDelegatedTo()));
								outputBean
										.setDelegationStatusMessage("Goal has been delegated to "
												+ getEmpNameByEmpId(delegateGoalObj
														.getFkMgrIdDelegatedTo())
												+ "-"
												+ delegateGoalObj
														.getFkMgrIdDelegatedTo()
												+ ". Please await response from "
												+ getEmpNameByEmpId(delegateGoalObj
														.getFkMgrIdDelegatedTo())
												+ "-"
												+ delegateGoalObj
														.getFkMgrIdDelegatedTo()
												+ ".");
								listOfSuccessfulCreatedDelegatedGoal
										.add(outputBean);
							} else {
								throw new CommonCustomException(
										"Delegate goal does not exists in DB.");
							}
						}
					} else {
						throw new CommonCustomException(
								datEmpGoalBo.getGsrDatEmpGoalName()
										+ " Only goals with status a accomplishments entered and goal set can be delegated");
					}
				}

				else {
					GsrDelegateOutputBean invalidGoalIdBean = new GsrDelegateOutputBean();
					invalidGoalIdBean.setEmpGoalID(goalId);
					invalidGoalIdBean
							.setDelegationStatusMessage("FAILED : Invalid goal Id.");
					listOfFailedCreatedDelegatedGoal.add(invalidGoalIdBean);
				}
			}
		} else {
			throw new CommonCustomException("GoalId list is empty");
		}

		gsrCreateDelegatedGoalOutputBean
				.setListOfFailedCreatedDelegatedGoal(listOfFailedCreatedDelegatedGoal);
		gsrCreateDelegatedGoalOutputBean
				.setListofSuccessfulCreatedDelegatedGoal(listOfSuccessfulCreatedDelegatedGoal);
		return gsrCreateDelegatedGoalOutputBean;
	}

	/**
	 * 
	 * <Description getAcceptRating:> method is call the repository method to
	 * Update Accept Rating data in the data base. It will take the BO and
	 * passes it to the Repository for update the Record.
	 * 
	 * @author THBS
	 * 
	 * @param GsrDatEmpFinalRatingBO
	 *            is the argument for this method.
	 * 
	 * @return return the list of data in Gsr Dat Emp Final Rating BO according
	 *         to the input details.
	 * 
	 * @throws BusinessException
	 *             throws if any exception occurs during the data retrieval.
	 * @throws CommonCustomException
	 * 
	 */
	public boolean getAcceptRating(Integer empId, Short slabId)
			throws BusinessException, ParseException, GoalNotFoundException,
			CommonCustomException {
		LOG.startUsecase("getAcceptRating");
		List<GsrDatEmpGoalBO> goalDetails = null;
		GsrDatEmpFinalRatingBO finalRating = null;
		boolean acceptedRatingFlag = false;
		try {
			goalDetails = gsrDatEmpGoalRepository
					.findByfkGsrDatEmpIdAndFkGsrSlabId(empId, slabId);

			Optional<List<GsrDatEmpGoalBO>> goalDetailsData = Optional
					.fromNullable(goalDetails);

			if (goalDetailsData.isPresent()) {

				finalRating = gsrDatEmpFinalRatingRepository
						.findByFkGsrFinalRatingSlabIdAndFkGsrFinalRatingEmpId(
								slabId, empId);

				Optional<GsrDatEmpFinalRatingBO> finalRatingDetails = Optional
						.fromNullable(finalRating);

				if (finalRatingDetails.isPresent()) {

					if (finalRating.getFinalRatingStatus() == finalRatingAcceptedStatus)
						throw new CommonCustomException(
								"Goals has already been closed");
					if (finalRating.getFinalRatingStatus() == finalRatingRatedStatus) {
						goalDetails
								.forEach(goal -> {
									if (goal.getFkGsrGoalStatus() != goalRejectedStatus) {
										goal.setGsrGoalModifiedDate(new Date());
										goal.setGsrGoalClosedBy(empId);
										goal.setFkGsrGoalStatus(goalClosedStatus);
										gsrDatEmpGoalRepository.save(goal);
									}
								});

						finalRating.setClosedBy(empId);
						finalRating
								.setFinalRatingStatus(finalRatingAcceptedStatus);
						finalRating.setRatingAcceptOrRejectDate(new Date());

						gsrDatEmpFinalRatingRepository.save(finalRating);
						acceptedRatingFlag = true;
					} else {
						throw new CommonCustomException(
								"Only rated final rating status can be accepted");
					}
				} else {
					throw new CommonCustomException(
							"Final Rating Details not found for this employee Id and slabId");
				}
			} else {
				throw new CommonCustomException(
						"No goals found for this employee Id and slab Id");
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("getAcceptRating");
		return acceptedRatingFlag;
	}

	/**
	 * 
	 * @param delegationGoalsBean
	 * @return DelegationGoalsBean
	 * @throws DataAccessException
	 * @Description : This service is used to view the goals that can be
	 *              delegated and goals which are delegated to or from empId
	 */
	public DelegationGoalsBean viewDelegatedGoalsinHR(
			DelegationGoalInputBeanHR delegationGoalsBean)
			throws DataAccessException {
		LOG.startUsecase("Entering viewDelegatedGoalsinHR service");
		List<DelegationResultFromDbBean> empGoals = null;
		List<DelegationGoalsOutputBean> delGoalsOutput = new ArrayList<DelegationGoalsOutputBean>();
		GoalDetailsForDelegationBean goalDetailsDelOutput = null;
		DelegationGoalsBean delegationGoalsOutputBean = null;
		List<DelegationResultFromDbBean> tempGoalsBean = new ArrayList<DelegationResultFromDbBean>();
		List<GoalDetailsForDelegationBean> goalDetailsList = new ArrayList<GoalDetailsForDelegationBean>();
		List<Integer> empIdList = new ArrayList<Integer>();
		try {
			empGoals = repository
					.getEmpGoalsForDelegationHR(delegationGoalsBean
							.getEmpIdList());
			if (empGoals.size() > 0) {
				delegationGoalsOutputBean = new DelegationGoalsBean();
				for (int i = 0; i < empGoals.size(); i++) {
					if (i == 0) {
						tempGoalsBean.add(empGoals.get(i));
					} else {
						if (empGoals.get(i - 1).getFkGsrDatEmpId()
								.equals(empGoals.get(i).getFkGsrDatEmpId())) {
							if (empGoals.get(i - 1).getFkGsrSlabId()
									.equals(empGoals.get(i).getFkGsrSlabId())) {
								tempGoalsBean.add(empGoals.get(i));
								if (i + 1 == empGoals.size()) {
									goalDetailsDelOutput = new GoalDetailsForDelegationBean();
									goalDetailsDelOutput
											.setAcceptedOrRejectedDate(empGoals
													.get(i - 1)
													.getFinalRatingAccRejDate());
									goalDetailsDelOutput.setEmpId(empGoals.get(
											i - 1).getFkGsrDatEmpId());
									goalDetailsDelOutput.setEmpName(empGoals
											.get(i - 1).getEmpName());
									goalDetailsDelOutput
											.setFinalRating(empGoals.get(i)
													.getFinalRating());
									goalDetailsDelOutput
											.setFinalRatingStatusId(empGoals
													.get(i - 1)
													.getFinalRatingStatus());
									goalDetailsDelOutput
											.setFinalRatingStatusName(empGoals
													.get(i)
													.getFinalRatingStatusName());
									goalDetailsDelOutput.setSlabId(empGoals
											.get(i - 1).getFkGsrSlabId());
									goalDetailsDelOutput.setSlabName(empGoals
											.get(i - 1).getGsrSlabName());
									goalDetailsDelOutput
											.setSlabShortName(empGoals.get(
													i - 1).getSlabShortName());
									delGoalsOutput = populateDelegationDetails(tempGoalsBean);
									tempGoalsBean.clear();
									tempGoalsBean.add(empGoals.get(i));
									goalDetailsDelOutput
											.setGoals(delGoalsOutput);
									goalDetailsList.add(goalDetailsDelOutput);
								}
							} else {
								goalDetailsDelOutput = new GoalDetailsForDelegationBean();
								goalDetailsDelOutput
										.setAcceptedOrRejectedDate(empGoals
												.get(i - 1)
												.getFinalRatingAccRejDate());
								goalDetailsDelOutput.setEmpId(empGoals.get(
										i - 1).getFkGsrDatEmpId());
								goalDetailsDelOutput.setEmpName(empGoals.get(
										i - 1).getEmpName());
								goalDetailsDelOutput.setFinalRating(empGoals
										.get(i).getFinalRating());
								goalDetailsDelOutput
										.setFinalRatingStatusId(empGoals.get(
												i - 1).getFinalRatingStatus());
								goalDetailsDelOutput
										.setFinalRatingStatusName(empGoals.get(
												i).getFinalRatingStatusName());
								goalDetailsDelOutput.setSlabId(empGoals.get(
										i - 1).getFkGsrSlabId());
								goalDetailsDelOutput.setSlabName(empGoals.get(
										i - 1).getGsrSlabName());
								goalDetailsDelOutput.setSlabShortName(empGoals
										.get(i - 1).getSlabShortName());
								delGoalsOutput = populateDelegationDetails(tempGoalsBean);
								tempGoalsBean.clear();
								tempGoalsBean.add(empGoals.get(i));
								goalDetailsDelOutput.setGoals(delGoalsOutput);
								goalDetailsList.add(goalDetailsDelOutput);
							}
						} else {
							goalDetailsDelOutput = new GoalDetailsForDelegationBean();
							goalDetailsDelOutput
									.setAcceptedOrRejectedDate(empGoals.get(
											i - 1).getFinalRatingAccRejDate());
							goalDetailsDelOutput.setEmpId(empGoals.get(i - 1)
									.getFkGsrDatEmpId());
							goalDetailsDelOutput.setEmpName(empGoals.get(i - 1)
									.getEmpName());
							goalDetailsDelOutput.setFinalRating(empGoals.get(i)
									.getFinalRating());
							goalDetailsDelOutput
									.setFinalRatingStatusId(empGoals.get(i - 1)
											.getFinalRatingStatus());
							goalDetailsDelOutput
									.setFinalRatingStatusName(empGoals.get(i)
											.getFinalRatingStatusName());
							goalDetailsDelOutput.setSlabId(empGoals.get(i - 1)
									.getFkGsrSlabId());
							goalDetailsDelOutput.setSlabName(empGoals
									.get(i - 1).getGsrSlabName());
							goalDetailsDelOutput.setSlabShortName(empGoals.get(
									i - 1).getSlabShortName());
							delGoalsOutput = populateDelegationDetails(tempGoalsBean);
							tempGoalsBean.clear();
							tempGoalsBean.add(empGoals.get(i));
							goalDetailsDelOutput.setGoals(delGoalsOutput);
							goalDetailsList.add(goalDetailsDelOutput);
						}
					}
				}
				delegationGoalsOutputBean.setReportees(goalDetailsList);
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Some exception occured in View Delegate Goals in HR");
		}
		LOG.endUsecase("Exiting viewDelegatedGoalsinHR service");
		return delegationGoalsOutputBean;
	}

	/**
	 * @author THBS
	 * @param empGoalDetailsBean
	 * @return List<GsrDatEmpGoalBO>
	 * @throws DataAccessException
	 * @throws ParseException
	 * @throws MethodArgumentNotValidException
	 * @Description: This method is used to view the employee goals which are
	 *               delegated.
	 */
	public List<ViewEmployeeGoalsOutputBean> viewEmployeeGoalsWhichAreDelegated(
			GSRDelegatedEmpGoalBean empGoalDetailsBean)
			throws DataAccessException, ParseException,
			MethodArgumentNotValidException {
		List<GsrDatEmpGoalBO> gsrEmpGoalList = new ArrayList<GsrDatEmpGoalBO>();
		List<ViewEmployeeGoalsOutputBean> empGoalsList = new ArrayList<ViewEmployeeGoalsOutputBean>();
		LOG.startUsecase("view Employee Goals which are delegated");
		List<FeedbackListBean> feedbackList = null;
		try {
			Date truncatedStartDate = DateUtils.truncate(
					empGoalDetailsBean.getSlabStartDate(), Calendar.DATE);
			Date truncatedEndDate = DateUtils.truncate(
					empGoalDetailsBean.getSlabEndDate(), Calendar.DATE);
			gsrEmpGoalList = repository.getDelegatedGoals(
					empGoalDetailsBean.getEmpId(), truncatedStartDate,
					truncatedEndDate, empGoalDetailsBean.getDelegatedTo());
			for (int i = 0; i < gsrEmpGoalList.size(); i++) {
				feedbackList = new ArrayList<FeedbackListBean>();
				ViewEmployeeGoalsOutputBean empGoalOutputBean = new ViewEmployeeGoalsOutputBean();
				empGoalOutputBean.setEmpId(gsrEmpGoalList.get(i)
						.getFkGsrDatEmpId().intValue());// Added by Rajesh for
														// bug fix.
				empGoalOutputBean.setGoalName(gsrEmpGoalList.get(i)
						.getGsrDatEmpGoalName());
				empGoalOutputBean.setGoalWeightage(gsrEmpGoalList.get(i)
						.getGsrGoalWeightage() + "");
				empGoalOutputBean.setGoalDuration(slabRepository
						.findByPkGsrSlabId(
								(short) gsrEmpGoalList.get(i).getFkGsrSlabId())
						.getGsrSlabName());
				empGoalOutputBean.setMgrId(gsrEmpGoalList.get(i)
						.getFkGsrDatMgrId());
				empGoalOutputBean.setMgrName(getEmpNameByEmpId(gsrEmpGoalList
						.get(i).getFkGsrDatMgrId()));
				empGoalOutputBean.setGoalDescription(gsrEmpGoalList.get(i)
						.getGsrGoalDescription());
				empGoalOutputBean.setGoalMeasurement(gsrEmpGoalList.get(i)
						.getGsrGoalMeasurement());
				empGoalOutputBean.setSelfRating(gsrEmpGoalList.get(i)
						.getGsrGoalSelfRating() + "");
				empGoalOutputBean.setAccomplishments(gsrEmpGoalList.get(i)
						.getGsrGoalAccomplishment());
				empGoalOutputBean.setMgrFeedback(gsrEmpGoalList.get(i)
						.getGsrGoalMgrFeedback());
				empGoalOutputBean.setMgrRating(gsrEmpGoalList.get(i)
						.getGsrGoalMgrRating() + "");
				empGoalOutputBean.setSlabId(gsrEmpGoalList.get(i)
						.getFkGsrSlabId());
				empGoalOutputBean.setGoalStatusId(gsrEmpGoalList.get(i)
						.getFkGsrGoalStatus());
				empGoalOutputBean.setGoalStatusName(goalStatusRepository
						.findByPkGsrGoalStatusId(
								(byte) gsrEmpGoalList.get(i)
										.getFkGsrGoalStatus())
						.getGsrGoalStatusName());
				empGoalOutputBean.setGoalId(gsrEmpGoalList.get(i)
						.getPkGsrDatEmpGoalId());
				empGoalOutputBean.setKpiGoalId(gsrEmpGoalList.get(i)
						.getEmpGoalKpiWeightageId());
				empGoalOutputBean.setClosedBy(gsrEmpGoalList.get(i)
						.getGsrGoalClosedBy());
				if (gsrEmpGoalList.get(i).getGsrGoalClosedBy() != null)
					empGoalOutputBean
							.setClosedByName(getEmpNameByEmpId(gsrEmpGoalList
									.get(i).getGsrGoalClosedBy()));
				else
					empGoalOutputBean.setClosedByName(null);
				if (gsrEmpGoalList.get(i).getEmpGoalKpiWeightageId() != null) {
					empGoalOutputBean.setKpiGoalWeightage(gsrEmpGoalList.get(i)
							.getGsrKpiWeightageDetails() == null ? null
							: gsrEmpGoalList.get(i).getGsrKpiWeightageDetails()
									.getGsrKpiGoalWeightage());
				} else {
					empGoalOutputBean.setKpiGoalWeightage(null);
				}
				GsrDatEmpFinalRatingBO empFinalRating = gsrDatEmpFinalRatingRepository
						.findByFkGsrFinalRatingSlabIdAndFkGsrFinalRatingEmpId(
								gsrEmpGoalList.get(i).getFkGsrSlabId(),
								gsrEmpGoalList.get(i).getFkGsrDatEmpId());
				if (empFinalRating != null) {
					empGoalOutputBean.setFinalRating(empFinalRating
							.getGsrFinalRating() + "");
					if (empFinalRating.getFinalRatingStatus() == 1)
						empGoalOutputBean.setFinalRatingStatus("Not Yet Rated");
					else if (empFinalRating.getFinalRatingStatus() == 2)
						empGoalOutputBean.setFinalRatingStatus("Accepted");
					else if (empFinalRating.getFinalRatingStatus() == 3)
						empGoalOutputBean.setFinalRatingStatus("Rejected");
					else if (empFinalRating.getFinalRatingStatus() == 4)
						empGoalOutputBean.setFinalRatingStatus("Resolved");
					else if (empFinalRating.getFinalRatingStatus() == 5)
						empGoalOutputBean.setFinalRatingStatus("Rated");
					else if (empFinalRating.getFinalRatingStatus() == 6) // Added
																			// by
																			// Rajesh
																			// for
																			// adding
																			// of
																			// new
																			// status
																			// in
																			// service
						empGoalOutputBean.setFinalRatingStatus("Reopened");
					empGoalOutputBean.setIsOverridden(empFinalRating
							.getIsOverriddenFlag());
					empGoalOutputBean
							.setRatingAcceptedRejectedDate(empFinalRating
									.getRatingAcceptOrRejectDate());
				} else {
					empGoalOutputBean.setFinalRating("");
					empGoalOutputBean.setFinalRatingStatus("Not Yet Rated");
					empGoalOutputBean.setIsOverridden("");
					empGoalOutputBean.setRatingAcceptedRejectedDate(null);
				}
				if (gsrEmpGoalList.get(i).getEmpGoalKpiWeightageId() > 0) {
					if (i > 0) {
						if ((i + 1) < gsrEmpGoalList.size()) {
							if (gsrEmpGoalList
									.get(i)
									.getEmpGoalKpiWeightageId()
									.equals(gsrEmpGoalList.get(i + 1)
											.getEmpGoalKpiWeightageId())) {
								empGoalOutputBean.setGoalName(gsrEmpGoalList
										.get(i + 1).getGsrDatEmpGoalName());
								empGoalOutputBean
										.setGoalWeightage(gsrEmpGoalList.get(
												i + 1).getGsrGoalWeightage()
												+ "");
								empGoalOutputBean
										.setGoalDuration(gsrEmpGoalList
												.get(i + 1)
												.getGsrSlabBO_slab_id()
												.getGsrSlabName());
								empGoalOutputBean.setMgrId(gsrEmpGoalList.get(
										i + 1).getFkGsrDatMgrId());
								empGoalOutputBean
										.setMgrName(getEmpNameByEmpId(gsrEmpGoalList
												.get(i + 1).getFkGsrDatMgrId()));
								empGoalOutputBean
										.setGoalDescription(gsrEmpGoalList.get(
												i + 1).getGsrGoalDescription());
								empGoalOutputBean
										.setGoalMeasurement(gsrEmpGoalList.get(
												i + 1).getGsrGoalMeasurement());
								empGoalOutputBean
										.setSelfRating(gsrEmpGoalList
												.get(i + 1)
												.getGsrGoalSelfRating()
												+ "");
								empGoalOutputBean
										.setAccomplishments(gsrEmpGoalList.get(
												i + 1)
												.getGsrGoalAccomplishment());
								empGoalOutputBean.setMgrFeedback(gsrEmpGoalList
										.get(i + 1).getGsrGoalMgrFeedback());
								empGoalOutputBean.setMgrRating(gsrEmpGoalList
										.get(i + 1).getGsrGoalMgrRating() + "");
								empGoalOutputBean.setSlabId(gsrEmpGoalList.get(
										i + 1).getFkGsrSlabId());
								empGoalOutputBean
										.setGoalStatusId(gsrEmpGoalList.get(
												i + 1).getFkGsrGoalStatus());
								empGoalOutputBean
										.setGoalStatusName(gsrEmpGoalList
												.get(i + 1)
												.getGsrMasGoalStatusBOgoalstatus()
												.getGsrGoalStatusName());
								empGoalOutputBean.setGoalId(gsrEmpGoalList.get(
										i + 1).getPkGsrDatEmpGoalId());
								empGoalOutputBean.setKpiGoalId(gsrEmpGoalList
										.get(i + 1).getEmpGoalKpiWeightageId());
								if (gsrEmpGoalList.get(i + 1)
										.getGsrGoalClosedBy() != null)
									empGoalOutputBean
											.setClosedByName(getEmpNameByEmpId(gsrEmpGoalList
													.get(i + 1)
													.getGsrGoalClosedBy()));
								else
									empGoalOutputBean.setClosedByName(null);
								feedbackList
										.add(populateFeedbackDetails(gsrEmpGoalList
												.get(i)));
								feedbackList
										.add(populateFeedbackDetails(gsrEmpGoalList
												.get(i + 1)));
								empGoalOutputBean.setFeedbackList(feedbackList);
								i = i + 1;
							} else {
								feedbackList
										.add(populateFeedbackDetails(gsrEmpGoalList
												.get(i)));
								empGoalOutputBean.setFeedbackList(feedbackList);
							}
						} else {
							feedbackList
									.add(populateFeedbackDetails(gsrEmpGoalList
											.get(i)));
							empGoalOutputBean.setFeedbackList(feedbackList);
						}
					}
				} else {
					feedbackList.add(populateFeedbackDetails(gsrEmpGoalList
							.get(i)));
					empGoalOutputBean.setFeedbackList(feedbackList);
				}
				empGoalsList.add(empGoalOutputBean);
			}
		} catch (Exception e) {
			LOG.error("Exception occured", e);
			e.printStackTrace();
		}
		LOG.endUsecase("view Employee Goals which are delegated");
		return empGoalsList;
	}

	/* Ended By Rajesh Kumar */

	// Added by Mani for GSR module service committed on 30-11-2016
	/**
	 * 
	 * @param bean
	 * @param dbAction
	 * @return
	 */
	// Commented by Shyam for re-coding of create goals of multiple reportees
	/*
	 * public List<GsrDatEmpGoalBO> saveGoals(List<GsrCreateGoalBean> bean,
	 * String dbAction) { List<GsrDatEmpGoalBO> output = new
	 * ArrayList<GsrDatEmpGoalBO>(); for (GsrCreateGoalBean gsrCreateGoalBean :
	 * bean) { GsrDatEmpGoalBO result = new GsrDatEmpGoalBO(); if
	 * (dbAction.equalsIgnoreCase("UPDATE")) {
	 * result.setFkGsrDatEmpId(gsrCreateGoalBean .getPkGsrDatEmpGoalId()); }
	 * result.setFkGsrDatEmpId(gsrCreateGoalBean.getFkGsrDatEmpId());
	 * result.setFkGsrDatMgrId(gsrCreateGoalBean.getFkGsrDatMgrId());
	 * result.setFkGsrGoalStatus(gsrCreateGoalBean.getFkGsrGoalStatus());
	 * result.setFkGsrSlabId(gsrCreateGoalBean.getFkGsrSlabId());
	 * result.setGsrDatEmpGoalName(gsrCreateGoalBean .getGsrDatEmpGoalName());
	 * result.setGsrGoalAccomplishment(gsrCreateGoalBean
	 * .getGsrGoalAccomplishment());
	 * result.setGsrGoalClosedBy(gsrCreateGoalBean.getGsrGoalClosedBy());
	 * result.setGsrGoalCreatedBy(gsrCreateGoalBean.getGsrGoalCreatedBy());
	 * result.setGsrGoalCreatedDate(gsrCreateGoalBean .getGsrGoalCreatedDate());
	 * result.setGsrGoalDescription(gsrCreateGoalBean .getGsrGoalDescription());
	 * result.setGsrGoalMeasurement(gsrCreateGoalBean .getGsrGoalMeasurement());
	 * result.setGsrGoalMgrFeedback(gsrCreateGoalBean .getGsrGoalMgrFeedback());
	 * result.setGsrGoalMgrRating(gsrCreateGoalBean.getGsrGoalMgrRating());
	 * result.setGsrGoalModifiedBy(gsrCreateGoalBean .getGsrGoalModifiedBy());
	 * result.setGsrGoalSelfRating(gsrCreateGoalBean .getGsrGoalSelfRating());
	 * result.setGsrGoalStartDate(gsrCreateGoalBean.getGsrGoalStartDate());
	 * result.setGsrGoalEndDate(gsrCreateGoalBean.getGsrGoalEndDate());
	 * result.setGsrGoalModifiedDate(gsrCreateGoalBean
	 * .getGsrGoalModifiedDate()); try { result =
	 * createGoalrepository.save(result); output.add(result); } catch (Exception
	 * e) { System.out.println("Error in Create Service."); e.printStackTrace();
	 * System.out.println("exception  service " + e.getMessage()); } } return
	 * output; }
	 */

	/**
	 * This service is for get the employee goal set Status using slab id.
	 * 
	 * @param employeeInfo
	 * 
	 * @param input
	 *            as the employee id and slab id.
	 * @return output as the goal set status for an employee.
	 * @throws BusinessException
	 * @throws DataAccessException
	 */

	public List<GsrGoalSetStatusBean> goalCurrentYearSetStatusForEmployee(
			GsrGoalSetStatusBean gsrGoalSetStatusBean,
			DatEmpProfessionalDetailBO employeeInfo) throws BusinessException,
			CommonCustomException, DataAccessException {
		List<GsrGoalSetStatusBean> gsrGoalSetStatusBeanOutPutList = new ArrayList<GsrGoalSetStatusBean>();
		List<GsrSlabBO> currentYearAllSlabList = new ArrayList<GsrSlabBO>();

		try {
			currentYearAllSlabList = gsrSlabRepository
					.findByGsrSlabYearOrderByPkGsrSlabIdAsc(year + "");
		} catch (Exception e1) {
			throw new DataAccessException(
					"unable to retrieve the current year slabs. Please contact to administrator");
		}

		Set<GsrSlabBO> currentYearSlabIds = new TreeSet<GsrSlabBO>();
		if (currentYearAllSlabList.size() > 0) {
			currentYearSlabIds = currentYearAllSlabList.stream()
					.map(slab -> slab).collect(Collectors.toSet());
		}
		if (currentYearSlabIds.size() > 0) {
			List<GsrDatEmpGoalBO> gsrEmployeeGoalList = new ArrayList<GsrDatEmpGoalBO>();
			for (GsrSlabBO gsrSlabBO : currentYearSlabIds) {
				String duration = monthFormat.format(
						gsrSlabBO.getGsrSlabStartDate()).toUpperCase()
						+ "-"
						+ monthFormat.format(gsrSlabBO.getGsrSlabEndDate())
								.toUpperCase();
				if (employeeInfo.getEmpConfirmationGraduateFlag() == 1) {
					LocalDate confimation;
					if (employeeInfo.getEmpDateOfConfirmation() != null) {
						confimation = LocalDate
								.parse(simpleDateFormat.format(employeeInfo
										.getEmpDateOfConfirmation()));
					} else {
						confimation = LocalDate.parse(simpleDateFormat
								.format(employeeInfo.getEmpDateOfJoining()));
					}
					if (confimation == null)
						throw new CommonCustomException(
								"Exception occured in get the employee confimation date ");
					else {
						LocalDate slabEndDate = LocalDate.parse(
								simpleDateFormat.format(gsrSlabBO
										.getGsrSlabEndDate())).minusDays(15);
						long daysBetween = ChronoUnit.DAYS.between(confimation,
								slabEndDate);
						if (daysBetween >= 0) {
							GsrGoalSetStatusBean data = this
									.getEmployeeGoalStatus(
											gsrSlabBO.getPkGsrSlabId(),
											gsrGoalSetStatusBean.getEmpId());
							if (data != null)
								gsrGoalSetStatusBeanOutPutList.add(data);
						} else {
							GsrGoalSetStatusBean dataBean = new GsrGoalSetStatusBean();
							dataBean.setEmpId(gsrGoalSetStatusBean.getEmpId());
							dataBean.setSlabDuration(duration);
							dataBean.setSlabId(gsrSlabBO.getPkGsrSlabId());
							dataBean.setSlabShortName(slabRepository
									.findByPkGsrSlabId(
											gsrSlabBO.getPkGsrSlabId())
									.getGsrSlabShortName());
							dataBean.setSlabName(slabRepository
									.findByPkGsrSlabId(
											gsrSlabBO.getPkGsrSlabId())
									.getGsrSlabName());
							dataBean.setRemainingWeightage(100l);
							dataBean.setTotalWeightage(0l);
							dataBean.setStatus("NOT_APPLICABLE");
							dataBean.setStatusId(CONSTANT_NOT_APPLICABLE);
							if (dataBean != null)
								gsrGoalSetStatusBeanOutPutList.add(dataBean);

						}
					}

				} else {
					GsrGoalSetStatusBean dataBean = new GsrGoalSetStatusBean();
					dataBean.setEmpId(gsrGoalSetStatusBean.getEmpId());
					dataBean.setSlabDuration(duration);
					dataBean.setSlabId(gsrSlabBO.getPkGsrSlabId());
					dataBean.setSlabShortName(slabRepository.findByPkGsrSlabId(
							gsrSlabBO.getPkGsrSlabId()).getGsrSlabShortName());
					dataBean.setSlabName(slabRepository.findByPkGsrSlabId(
							gsrSlabBO.getPkGsrSlabId()).getGsrSlabName());
					dataBean.setRemainingWeightage(100l);
					dataBean.setTotalWeightage(0l);
					dataBean.setStatus("NOT_APPLICABLE");
					dataBean.setStatusId(CONSTANT_NOT_APPLICABLE);
					if (dataBean != null)
						gsrGoalSetStatusBeanOutPutList.add(dataBean);
				}
			}

		} else {
			throw new CommonCustomException(
					"unable to get the current year slab ids. Please contact to administrator");
		}
		if (gsrGoalSetStatusBeanOutPutList.size() > 0)
			Collections.sort(gsrGoalSetStatusBeanOutPutList,
					new Comparator<GsrGoalSetStatusBean>() {
						@Override
						public int compare(GsrGoalSetStatusBean lhs,
								GsrGoalSetStatusBean rhs) {
							return lhs.getSlabId().compareTo(rhs.getSlabId());
						}
					});
		return gsrGoalSetStatusBeanOutPutList;
	}

	/**
	 * 
	 * @param employeeInfo
	 * @param slabData
	 * @return
	 * @throws DataAccessException
	 */
	public List<GsrGoalClosedStatusBean> IsGoalClosedForLastThreeSlab(
			GsrGoalClosedStatusBean gsrGoalClosedStatusBean,
			DatEmpProfessionalDetailBO employeeInfo) throws DataAccessException {
		List<GsrGoalClosedStatusBean> gsrGoalClosedStatusDeatils = new ArrayList<GsrGoalClosedStatusBean>();
		List<GsrDatEmpGoalBO> gsrDatEmpGoalBoTempList = new ArrayList<GsrDatEmpGoalBO>();
		int loop = 0;
		Short slabId = gsrGoalClosedStatusBean.getSlabId();
		try {
			while (loop < 3) {
				GsrGoalClosedStatusBean gsrGoalClosedStatusBeanOutPut = new GsrGoalClosedStatusBean();
				String goalSetStatus = "GOAL CLOSED";
				System.out.println("loop and slab " + loop + "-" + slabId);
				gsrDatEmpGoalBoTempList = createGoalrepository
						.findByFkGsrDatEmpIdAndFkGsrSlabId(
								gsrGoalClosedStatusBean.getEmpId(), slabId);

				if (gsrDatEmpGoalBoTempList == null) {
					throw new BusinessException(
							"Exception Occured Method of  'goalSetStatus' in `GSRGoalController` And The  Exception is Employee  GOAL Is Null",
							null);
				} else {
					for (GsrDatEmpGoalBO gsrDatEmpGoalBO : gsrDatEmpGoalBoTempList) {
						if (gsrDatEmpGoalBO.getFkGsrGoalStatus() != 5
								&& gsrDatEmpGoalBO.getFkGsrGoalStatus() != 7) {
							goalSetStatus = "GOAL NOT CLOSED";
						}
					}
					gsrGoalClosedStatusBeanOutPut
							.setEmpId(gsrGoalClosedStatusBean.getEmpId());
					gsrGoalClosedStatusBeanOutPut.setSlabId(slabId);
					gsrGoalClosedStatusBeanOutPut.setStatus(goalSetStatus);
					gsrGoalClosedStatusBeanOutPut
							.setReportingManagerId(employeeInfo
									.getFkEmpReportingMgrId());
					gsrGoalClosedStatusBeanOutPut
							.setDomainManagerId(employeeInfo
									.getFkEmpDomainMgrId());
					gsrGoalClosedStatusBeanOutPut.setBuUnit(employeeInfo
							.getFkEmpBuUnit());
					gsrGoalClosedStatusDeatils
							.add(gsrGoalClosedStatusBeanOutPut);
				}
				slabId--;
				loop++;
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured Method of  'goalSetStatus' in `GSRGoalController`   Exception is ",
					e);
		}
		return gsrGoalClosedStatusDeatils;
	}

	/**
	 * 
	 * @param gsrDatEmpFinalRatingBO
	 * @return
	 * @throws DataAccessException
	 */
	public GsrDatEmpFinalRatingBO overideenFinalRating(
			GsrDatEmpFinalRatingBO gsrDatEmpFinalRatingBO)
			throws DataAccessException {
		System.out.println("data " + gsrDatEmpFinalRatingBO.toString());
		GsrDatEmpFinalRatingBO gsrDatEmpFinalRatingResultBO = new GsrDatEmpFinalRatingBO();
		try {
			gsrDatEmpFinalRatingResultBO = gsrDatEmpFinalRatingRepository
					.save(gsrDatEmpFinalRatingBO);
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception Occured Method of  'goalSetStatus' in `GSRGoalController`   Exception is ",
					e);
		}
		return gsrDatEmpFinalRatingResultBO;
	}

	// End of addition by Mani for GSR module service committed on 30-11-2016
	// Added by Balaji 15-12-16
	/**
	 * 
	 * @param empId
	 * @param startDate
	 * @param endDate
	 * @return goalStatus
	 * @throws DataAccessException
	 * @description This service will return the goal status is closed or not
	 */
	public String isGoalStatusClosed(GsrGoalsClosedBean gsrGoalsClosedBean)
			throws DataAccessException {
		// TODO Auto-generated method stub
		// int Status = "";
		String goalStatus = "";
		boolean isGoalClosed = true;
		int GOAL_CLOSED_STATUS = 5;
		int GOAL_FORCE_CLOSED_STATUS = 7;
		int GOAL_REJECTED_STATUS = 6;
		List<GsrDatEmpGoalBO> result = new ArrayList<GsrDatEmpGoalBO>();
		LOG.startUsecase("entering isGoalStatusClosed");
		try {

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			if (gsrGoalsClosedBean.getGsrGoalStartDate() != null
					&& gsrGoalsClosedBean.getGsrGoalEndDate() != null) {
				String strDate = sm.format(gsrGoalsClosedBean
						.getGsrGoalStartDate());
				String eDate = sm
						.format(gsrGoalsClosedBean.getGsrGoalEndDate());
				Date startDate = sm.parse(strDate);
				Date endDate = sm.parse(eDate);
				System.out.println(gsrGoalsClosedBean.toString());
				result = createGoalRepository
						.findByFkGsrDatEmpIdAndGsrGoalStartDateGreaterThanEqualAndGsrGoalEndDateLessThanEqual(
								gsrGoalsClosedBean.getFkGsrDatEmpId(),
								startDate, endDate);
			} else /*
					 * if (gsrGoalsClosedBean.getFkGsrDatEmpId()!= null &&
					 * gsrGoalsClosedBean.getGsrGoalStartDate().equals(null) &&
					 * gsrGoalsClosedBean.getGsrGoalEndDate().equals(null))
					 */{
				result = createGoalRepository
						.findByFkGsrDatEmpId(gsrGoalsClosedBean
								.getFkGsrDatEmpId());
			}

			if (result.isEmpty() || result.size() < 1) {
				goalStatus = "No Data Found";
			} else {

				for (GsrDatEmpGoalBO gsrDatEmpGoalBo : result) {
					if ((gsrDatEmpGoalBo.getFkGsrGoalStatus() != GOAL_CLOSED_STATUS)
							&& (gsrDatEmpGoalBo.getFkGsrGoalStatus() != GOAL_FORCE_CLOSED_STATUS)
							&& (gsrDatEmpGoalBo.getFkGsrGoalStatus() != GOAL_REJECTED_STATUS)) {
						isGoalClosed = false;
						break;
					}
				}
				if (isGoalClosed == true) {
					goalStatus = "Goal are closed";
				} else {
					goalStatus = "Goals are not closed";
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error Occured" + e.getMessage());
		}

		LOG.endUsecase("exiting isGoalStatusClosed");
		return goalStatus;
	}

	/**
	 * 
	 * @param empId
	 * @param startDate
	 * @param endDate
	 * @return goalStatus
	 * @throws DataAccessException
	 * @description This service will return the goal status is closed or not
	 */
	public List<Integer> isGoalStatusClosedForReportees(
			GsrGoalsClosedBean gsrGoalsClosedBean) throws DataAccessException {

		// int Status = "";
		String goalStatus = "";
		boolean isGoalClosed = true;
		int GOAL_CLOSED_STATUS = 5;
		int GOAL_FORCE_CLOSED_STATUS = 7;
		int GOAL_REJECTED_STATUS = 6;
		List<GsrDatEmpGoalBO> result = new ArrayList<GsrDatEmpGoalBO>();
		List<Integer> openGoals = new ArrayList<Integer>();

		LOG.startUsecase("entering isGoalStatusClosed");
		try {

			/*
			 * if (gsrGoalsClosedBean.getFkGsrDatEmpId()!= null &&
			 * gsrGoalsClosedBean.getGsrGoalStartDate().equals(null) &&
			 * gsrGoalsClosedBean.getGsrGoalEndDate().equals(null))
			 */
			result = createGoalRepository
					.findByFkGsrDatMgrId(gsrGoalsClosedBean.getFkGsrDatEmpId());

			if (result.isEmpty() || result.size() < 1) {
				goalStatus = "No Data Found";
			} else {

				for (GsrDatEmpGoalBO gsrDatEmpGoalBo : result) {

					if ((gsrDatEmpGoalBo.getFkGsrGoalStatus() != GOAL_CLOSED_STATUS)
							&& (gsrDatEmpGoalBo.getFkGsrGoalStatus() != GOAL_FORCE_CLOSED_STATUS)
							&& (gsrDatEmpGoalBo.getFkGsrGoalStatus() != GOAL_REJECTED_STATUS)) {

						openGoals.add(gsrDatEmpGoalBo.getFkGsrDatEmpId());

					}
				}

			}
		} catch (Exception e) {
			throw new DataAccessException("Error Occured" + e.getMessage());
		}

		LOG.endUsecase("exiting isGoalStatusClosed");
		return openGoals;
	}

	/**
	 * @Description This service is used to view the training requests.
	 * @param gsrTrainingRequestInputBean
	 * @return gsrTrainingRequests
	 * @throws DataAccessException
	 */
	public List<GsrTrainingRequestBO> viewTrainingRequested(
			GsrTrainingRequestSearchBean gsrTrainingRequestInputBean)
			throws DataAccessException {

		List<GsrTrainingRequestBO> gsrTrainingRequests = new ArrayList<GsrTrainingRequestBO>();
		GsrTrainingRequestSpecifications traReqSpecs = new GsrTrainingRequestSpecifications();
		LOG.startUsecase("entering viewTrainingRequested");
		try {
			Specification<GsrTrainingRequestBO> requestSpecification = traReqSpecs
					.getTrainingRequestDatesByDateRangeOrTopicId(gsrTrainingRequestInputBean);

			gsrTrainingRequests = trainingRepository
					.findAll(requestSpecification);
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception occured in viewTrainingRequested ", e);
		}
		LOG.endUsecase("exiting viewTrainingRequested");
		return gsrTrainingRequests;
	}

	/**
	 * @Description This service is used to create the
	 * @param gsrTrainingRequestBean
	 * @return gsrTrainingRequestBoOutputList
	 * @description This service used to create the new training request
	 */
	public GsrTrainingRequestBO createRequestForTraining(
			GsrTrainingRequestBean gsrTrainingRequestBean) {
		GsrTrainingRequestBO gsrTrainingRequestOutput = new GsrTrainingRequestBO();
		LOG.startUsecase("entering createRequestForTraining");

		gsrTrainingRequestOutput.setFkGsrTrainingTopicId(gsrTrainingRequestBean
				.getFkGsrTrainingTopicId());
		gsrTrainingRequestOutput
				.setGsrTrainingRequestedForEmpId(gsrTrainingRequestBean
						.getFkTrainingRequestedForEmpId());
		gsrTrainingRequestOutput
				.setOtherTopicDescription(gsrTrainingRequestBean
						.getOtherTopicDescription());
		gsrTrainingRequestOutput
				.setGsrTrainingRequestedBy(gsrTrainingRequestBean
						.getGsrTrainingRequestedBy());
		gsrTrainingRequestOutput
				.setGsrTrainingRequestedDate(gsrTrainingRequestBean
						.getGsrTrainingRequestedDate());
		gsrTrainingRequestOutput.setComments(gsrTrainingRequestBean
				.getComments());
		try {
			gsrTrainingRequestOutput = trainingRepository
					.save(gsrTrainingRequestOutput);
		} catch (Exception e) {
			System.out.println("exception  service " + e.getMessage());
		}

		LOG.endUsecase("exiting createRequestForTraining");
		return gsrTrainingRequestOutput;
	}

	/**
	 * @Description This service is to update the Workflow permission by its
	 *              Permission type.
	 * @param gsrWorflowPermissionBeanInputList
	 * @return gsrWorkflowPermissionBoOutputList
	 */
	public List<GsrWorkflowPermissionBO> allowPermissionForGoal(
			EntityBeanList<GsrWorflowPermissionBean> gsrWorflowPermissionBeanInputList) {
		int PERMISSION_TYPE_GOAL_CREATION = 1, PERMISSION_TYPE_ACCOMPLISHMENT = 2, PERMISSION_TYPE_FEEDBACK = 3;
		String PERMISSION_ENABLED = "ENABLED", PERMISSION_DISABLED = "DISABLED";
		List<GsrWorkflowPermissionBO> gsrWorkflowPermissionBoOutputList = new ArrayList<GsrWorkflowPermissionBO>();
		LOG.startUsecase("entering allowPermissionForGoal");
		try {
			for (GsrWorflowPermissionBean gsrWorkflowPermission : gsrWorflowPermissionBeanInputList
					.getEntityList()) {
				GsrWorkflowPermissionBO gsrWorkflowPermissionOutput = new GsrWorkflowPermissionBO();

				gsrWorkflowPermissionOutput
						.setPkGsrWorkflowPermissionId(permissionRepository
								.findByFkGsrWorkflowPermissionEmpId(
										gsrWorkflowPermission
												.getFkGsrWorkflowPermissionEmpId())
								.getPkGsrWorkflowPermissionId());
				gsrWorkflowPermissionOutput
						.setFkGsrWorkflowPermissionEmpId(gsrWorkflowPermission
								.getFkGsrWorkflowPermissionEmpId());
				gsrWorkflowPermissionOutput
						.setFkGsrWorkflowPermissionSlabId(gsrWorkflowPermission
								.getFkGsrWorkflowPermissionSlabId());
				gsrWorkflowPermissionOutput
						.setGsrWorkflowPermissionStartDate(gsrWorkflowPermission
								.getGsrWorkflowPermissionStartDate());
				gsrWorkflowPermissionOutput
						.setGsrWorkflowPermissionEndDate(gsrWorkflowPermission
								.getGsrWorkflowPermissionEndDate());
				gsrWorkflowPermissionOutput
						.setGsrWorkflowPermissionRequestedBy(gsrWorkflowPermission
								.getGsrWorkflowPermissionRequestedBy());
				gsrWorkflowPermissionOutput
						.setGsrWorkflowPermissionRequestedDate(gsrWorkflowPermission
								.getGsrWorkflowPermissionRequestedDate());
				if (gsrWorkflowPermission.getPermissionType() == PERMISSION_TYPE_GOAL_CREATION) {
					gsrWorkflowPermissionOutput
							.setEnableGoalsCreation(PERMISSION_ENABLED);
					gsrWorkflowPermissionOutput
							.setEnableEnterAccomplishments(PERMISSION_ENABLED);
					gsrWorkflowPermissionOutput
							.setEnableFeedback(PERMISSION_ENABLED);
				} else if (gsrWorkflowPermission.getPermissionType() == PERMISSION_TYPE_ACCOMPLISHMENT) {
					gsrWorkflowPermissionOutput
							.setEnableGoalsCreation(PERMISSION_DISABLED);
					gsrWorkflowPermissionOutput
							.setEnableEnterAccomplishments(PERMISSION_ENABLED);
					gsrWorkflowPermissionOutput
							.setEnableFeedback(PERMISSION_ENABLED);
				} else if (gsrWorkflowPermission.getPermissionType() == PERMISSION_TYPE_FEEDBACK) {
					gsrWorkflowPermissionOutput
							.setEnableGoalsCreation(PERMISSION_DISABLED);
					gsrWorkflowPermissionOutput
							.setEnableEnterAccomplishments(PERMISSION_DISABLED);
					gsrWorkflowPermissionOutput
							.setEnableFeedback(PERMISSION_ENABLED);
				}
				gsrWorkflowPermissionOutput = permissionRepository
						.save(gsrWorkflowPermissionOutput);

				gsrWorkflowPermissionBoOutputList
						.add(gsrWorkflowPermissionOutput);

			}
		} catch (Exception e) {
			LOG.error("Error Occured: ", e);
		}
		LOG.endUsecase("exiting allowPermissionForGoal");
		return gsrWorkflowPermissionBoOutputList;
	}

	// End of Addition by Balaji

	/**
	 * @Description Service to view past goals of perticular employee
	 * @input GsrDatEmpGoalBOBean
	 * @output GsrDatEmpGoalBO
	 * @param gsrDatGoalBean
	 * @return Iterable<GsrDatEmpGoalBO> Previous goals of mentioned employee
	 * @throws DataAccessException
	 */

	public List<GsrDatEmpGoalBO> viewPastGaols(
			GsrDatEmpGoalBOBeanInput gsrDatGoalBean) throws DataAccessException {
		List<GsrDatEmpGoalBO> emppastgoals = null;

		System.out.println("Reached in service.");
		LOG.startUsecase("VIEW _PASTGOALS_service");
		try {
			System.out.println("emp id  Service_Try : "
					+ gsrDatGoalBean.getFkGsrDatEmpId());
			System.out.println("Start_Date : "
					+ gsrDatGoalBean.getGsrGoalStartDate());

			/*
			 * result = gsrViewPastGols
			 * .findByFkGsrDatEmpIdAndGsrGoalStartDateBetween(
			 * gsrDatGoalBean.getFkGsrDatEmpId(),
			 * gsrDatGoalBean.getGsrGoalStartDate(),
			 * gsrDatGoalBean.getGsrGoalEndDate());
			 */

			emppastgoals = gsrViewPastGols
					.findByFkGsrDatEmpIdAndGsrGoalStartDateGreaterThanEqualAndGsrGoalEndDateLessThanEqual(
							gsrDatGoalBean.getFkGsrDatEmpId(),
							gsrDatGoalBean.getGsrGoalStartDate(),
							gsrDatGoalBean.getGsrGoalEndDate());

			// System.out.println("data " + emppastgoals.toString());
		}

		catch (Exception e) {
			System.out.println("Exception _Srevice _Catch" + e.getMessage());
		}

		LOG.endUsecase("VIEW _PASTGOALS_service");
		// System.out.println("result " + emppastgoals.toString());
		return emppastgoals;
	}

	// EOC FOR VIEW PAST GOALS

	/***************************************************************************************************************************
	 * /**
	 * 
	 * @Description view all goals status within domain by DM
	 * @param gsrDatGoalBean
	 * @return Goals of reportees by domain manager
	 * @input DomainMangerID,GoalStart_Date,GoalEnd_Date
	 * @throws DataAccessException
	 */

	/*
	 * public List<GsrDatEmpGoalBO> viewEmployeeGoalswithinDomainByDm(
	 * DatEmpProfDetailandGsrEmpGoalBean datEmpProfDetailandGsrEmpGoalBean)
	 * throws DataAccessException {
	 * 
	 * LOG.startUsecase("VIEW _goals_within_domain_by_dm_serivce***");
	 * List<GsrDatEmpGoalBO> goalsofDMreportees = new
	 * ArrayList<GsrDatEmpGoalBO>();
	 * 
	 * try {
	 * 
	 * goalsofDMreportees = gsrDatEmpGoalRepository.viewReporteeGoalsByDM(
	 * datEmpProfDetailandGsrEmpGoalBean.getFkEmpDomainMgrId(),
	 * datEmpProfDetailandGsrEmpGoalBean.getGsrGoalStartDate(),
	 * datEmpProfDetailandGsrEmpGoalBean.getGsrGoalEndDate(),
	 * datEmpProfDetailandGsrEmpGoalBean.getFkGsrGoalStatus());
	 * 
	 * if (goalsofDMreportees != null) { System.out.println("------Result---" +
	 * goalsofDMreportees.size()); }
	 * 
	 * }
	 * 
	 * catch (Exception e) { e.printStackTrace();
	 * System.out.println("Exception " + e.getMessage()); throw new
	 * DataAccessException("Exception occured", e); }
	 * 
	 * LOG.endUsecase("VIEW _goals_within_domain_by_dm_service"); return
	 * goalsofDMreportees;
	 * 
	 * }
	 * 
	 * Added by prathibha for View Employee goals by BU Head
	 * 
	 * public List<GsrDatEmpGoalBO> viewEmployeeGoalsByBUHead(
	 * GsrDatEmpGoalandProfessionalBOBean gsrDatEmpGoalandProfessionalBOBean)
	 * throws DataAccessException
	 * 
	 * {
	 * 
	 * LOG.startUsecase("VIEW _goals_within_domain_by_bu_head");
	 * 
	 * List<GsrDatEmpGoalBO> goalsofBUHreportees = new
	 * ArrayList<GsrDatEmpGoalBO>();
	 * 
	 * try {
	 * 
	 * goalsofBUHreportees = gsrDatEmpGoalRepository .viewEmployeeGoalsByBUHead(
	 * gsrDatEmpGoalandProfessionalBOBean .getFkMainEmpDetailId(),
	 * gsrDatEmpGoalandProfessionalBOBean .getGsrGoalStartDate(),
	 * gsrDatEmpGoalandProfessionalBOBean .getGsrGoalEndDate(),
	 * gsrDatEmpGoalandProfessionalBOBean .getFkGsrGoalStatus());
	 * 
	 * }
	 * 
	 * catch (Exception e) {
	 * 
	 * System.out.println("Exception " + e.getMessage()); throw new
	 * DataAccessException("Exception occured", e); }
	 * 
	 * LOG.endUsecase("VIEW _goals_within_domain_by_bu_head"); return
	 * goalsofBUHreportees;
	 * 
	 * }
	 */

	/**
	 * 07/3/2017
	 * 
	 * @Description view all goals within domain by Domain Manager
	 * @param gsrDatGoalBean
	 * @return Goals of reportee's by domain manager
	 * @input DomainMangerID,GoalStart_Date,GoalEnd_Date
	 * @throws DataAccessException
	 */
	public List<DMViewEmpGoalsOutputBean> viewEmployeeGoalsByDM(
			DMEmpGoalInputBean dmRepGoalBean) throws DataAccessException {
		List<GsrDatEmpGoalBO> dmRepoteesGoalList = new ArrayList<GsrDatEmpGoalBO>();
		List<DMViewEmpGoalsOutputBean> dmReporteGoalList = new ArrayList<DMViewEmpGoalsOutputBean>();

		try {
			dmRepoteesGoalList = gsrDatEmpGoalRepository.viewDMeReporeetsGoal(
					dmRepGoalBean.getDomainMgrId(), dmRepGoalBean.getSlabID());

			if (!dmRepoteesGoalList.isEmpty()) {
				for (GsrDatEmpGoalBO gsrDatEmpGoalBO : dmRepoteesGoalList) {

					DMViewEmpGoalsOutputBean dmViewBean = new DMViewEmpGoalsOutputBean();
					dmViewBean.setPkGsrDatEmpGoalId(gsrDatEmpGoalBO
							.getPkGsrDatEmpGoalId());
					dmViewBean.setGsrDatEmpGoalName(gsrDatEmpGoalBO
							.getGsrDatEmpGoalName());
					dmViewBean.setFkGsrDatEmpId(gsrDatEmpGoalBO
							.getFkGsrDatEmpId());
					// dmViewBean.setFkEmpGoalBuId(gsrDatEmpGoalBO.getFkEmpGoalBuId());
					dmViewBean.setGsrGoalStartDate(gsrDatEmpGoalBO
							.getGsrGoalStartDate());
					dmViewBean.setGsrGoalEndDate(gsrDatEmpGoalBO
							.getGsrGoalEndDate());
					dmViewBean.setFkGsrSlabId(gsrDatEmpGoalBO.getFkGsrSlabId());
					dmViewBean.setGsrGoalWeightage(gsrDatEmpGoalBO
							.getGsrGoalWeightage());
					dmViewBean.setGsrGoalDescription(gsrDatEmpGoalBO
							.getGsrGoalDescription());
					dmViewBean.setGsrGoalMeasurement(gsrDatEmpGoalBO
							.getGsrGoalMeasurement());
					dmViewBean.setGsrGoalAccomplishment(gsrDatEmpGoalBO
							.getGsrGoalAccomplishment());
					dmViewBean.setGsrGoalSelfRating(gsrDatEmpGoalBO
							.getGsrGoalSelfRating());
					dmViewBean.setFkGsrGoalStatus(gsrDatEmpGoalBO
							.getFkGsrGoalStatus());
					dmViewBean.setGsrSlabId(gsrDatEmpGoalBO.getFkGsrSlabId());
					dmViewBean.setSlabName(gsrDatEmpGoalBO.getSlabName());
					dmViewBean.setGsrSlabYear(gsrDatEmpGoalBO.getGsrSlabYear());
					dmViewBean.setEmpFirstName(gsrDatEmpGoalBO
							.getEmpFirstName());
					dmViewBean.setEmpLastName(gsrDatEmpGoalBO.getEmpLastName());
					dmViewBean.setGsrGoalStatusName(gsrDatEmpGoalBO
							.getGsrGoalStatusName());
					dmReporteGoalList.add(dmViewBean);

				}

			}

		} catch (Exception e) {

			throw new DataAccessException(
					"Exception occured while retrieving reportees Goals", e);
		}

		return dmReporteGoalList;
	}

	// EOC by Prathibha for DM view reportee goals

	// /Added by prathibha for view reportees goals by BU Head

	/**
	 * @description method used by BU Head to view goals of employees in
	 *              particular BU
	 * @param DatEmpProfDetailandGsrEmpGoalBean
	 * @return List<GsrDatEmpGoalBO> List of goals of employees in BU
	 * @throws DataAccessException
	 * @Input BU Head ID,Slab Id
	 */

	public List<BUHeadreporteesgoaloutputbean> viewRepoGoalsByBUHead(
			BUHeadreporteesgoalinputbean buHeadreporteesgoalinputbean)
			throws DataAccessException

	{

		LOG.startUsecase("VIEW _goals_within_domain_by_bu_head");

		List<GsrDatEmpGoalBO> goalsofBUHreportees = new ArrayList<GsrDatEmpGoalBO>();
		List<BUHeadreporteesgoaloutputbean> buReporteegoalList = new ArrayList<BUHeadreporteesgoaloutputbean>();

		try {

			goalsofBUHreportees = gsrDatEmpGoalRepository
					.viewRepoGoalsByBUHead(
							buHeadreporteesgoalinputbean.getBuHeadEmpid(),
							buHeadreporteesgoalinputbean.getSlabId());

			if (!goalsofBUHreportees.isEmpty()) {
				for (GsrDatEmpGoalBO gsrDatEmpGoalBO : goalsofBUHreportees) {
					BUHeadreporteesgoaloutputbean buHeadreoglbean = new BUHeadreporteesgoaloutputbean();

					buHeadreoglbean.setPkGsrDatEmpGoalId(gsrDatEmpGoalBO
							.getPkGsrDatEmpGoalId());
					buHeadreoglbean.setGsrDatEmpGoalName(gsrDatEmpGoalBO
							.getGsrDatEmpGoalName());
					buHeadreoglbean.setFkGsrDatEmpId(gsrDatEmpGoalBO
							.getFkGsrDatEmpId());
					buHeadreoglbean.setFkEmpGoalBuId(gsrDatEmpGoalBO
							.getFkEmpGoalBuId());
					buHeadreoglbean.setGsrGoalStartDate(gsrDatEmpGoalBO
							.getGsrGoalStartDate());
					buHeadreoglbean.setGsrGoalEndDate(gsrDatEmpGoalBO
							.getGsrGoalEndDate());
					buHeadreoglbean.setGsrSlabId(gsrDatEmpGoalBO
							.getFkGsrSlabId());
					buHeadreoglbean.setGsrGoalWeightage(gsrDatEmpGoalBO
							.getGsrGoalWeightage());
					buHeadreoglbean.setGsrGoalDescription(gsrDatEmpGoalBO
							.getGsrGoalDescription());
					buHeadreoglbean.setGsrGoalMeasurement(gsrDatEmpGoalBO
							.getGsrGoalMeasurement());
					buHeadreoglbean.setGsrGoalAccomplishment(gsrDatEmpGoalBO
							.getGsrGoalAccomplishment());
					buHeadreoglbean.setGsrGoalSelfRating(gsrDatEmpGoalBO
							.getGsrGoalSelfRating());
					buHeadreoglbean.setFkGsrGoalStatus(gsrDatEmpGoalBO
							.getFkGsrGoalStatus());
					buHeadreoglbean
							.setGsrSlabId(gsrDatEmpGoalBO.getGsrSlabId());
					buHeadreoglbean.setSlabName(gsrDatEmpGoalBO
							.getGsrSlabName());
					buHeadreoglbean.setGsrSlabYear(gsrDatEmpGoalBO
							.getGsrSlabYear());
					buHeadreoglbean.setEmpFirstName(gsrDatEmpGoalBO
							.getEmpFirstName());
					buHeadreoglbean.setEmpLastName(gsrDatEmpGoalBO
							.getEmpLastName());
					buHeadreoglbean.setGsrGoalStatusName(gsrDatEmpGoalBO
							.getGsrGoalStatusName());
					buHeadreoglbean.setBuHeadEmpId(gsrDatEmpGoalBO
							.getBuHeadEmpid());

					buReporteegoalList.add(buHeadreoglbean);
				}

			}
		}

		catch (Exception e) {

			throw new DataAccessException("Exception occured", e);
		}

		LOG.endUsecase("VIEW _goals_within_domain_by_bu_head");
		return buReporteegoalList;

	}

	// Added by prathibha

	// Added by Kamal Anand
	/**
	 * @author THBS
	 * @param empBean
	 * @return List<GsrDatEmpGoalBO>
	 * @throws DataAccessException
	 * @throws ParseException
	 * @throws MethodArgumentNotValidException
	 * @Description: This method is used to view the employee goals.
	 */
	public Page<GsrDatEmpGoalBO> viewEmployeeGoalsPage(
			GsrEmployeeGoalDetailsBeanPagenation empGoalDetailsBean)
			throws DataAccessException, ParseException,
			MethodArgumentNotValidException {
		LOG.startUsecase("view Employee Goals Page");

		Pageable pageable = new PageRequest(empGoalDetailsBean.getPageNumber(),
				empGoalDetailsBean.getPageSize());

		Page<GsrDatEmpGoalBO> empGoals = null;

		try {

			Date truncatedStartDate = DateUtils.truncate(
					empGoalDetailsBean.getGoalStartDate(), Calendar.DATE);
			Date truncatedEndDate = DateUtils.truncate(
					empGoalDetailsBean.getGoalEndDate(), Calendar.DATE);

			empGoals = repository.getEmpGoals(empGoalDetailsBean.getEmpId(),
					truncatedStartDate, truncatedEndDate, pageable);

			if (empGoals.getContent().size() > 0) {
				for (int i = 0; i < empGoals.getContent().size(); i++) {
					Byte ratingStatus = empGoals.getContent().get(i)
							.getFinalRatingStatus();
					if (ratingStatus == null) {
						empGoals.getContent().get(i)
								.setFinalRatingStatusName(" ");
					} else {
						if (ratingStatus == 1)
							empGoals.getContent().get(i)
									.setFinalRatingStatusName("Not Yet Rated");
						if (ratingStatus == 2)
							empGoals.getContent().get(i)
									.setFinalRatingStatusName("Accepted");
						if (ratingStatus == 3)
							empGoals.getContent().get(i)
									.setFinalRatingStatusName("Rejected");
						if (ratingStatus == 4)
							empGoals.getContent().get(i)
									.setFinalRatingStatusName("Force Closed");
						if (ratingStatus == 5)
							empGoals.getContent().get(i)
									.setFinalRatingStatusName("Rated");
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new DataAccessException("Exception occured", e);
		}
		LOG.endUsecase("view Employee Goals");
		return empGoals;
	}

	/**
	 * 
	 * @param empId
	 * @return personalDetail
	 * @Description This method is used to retrieve the employee name
	 */
	public String getEmpNameByEmpId(Integer empId) {
		LOG.startUsecase("getEmpNameByEmpId");
		DatEmpPersonalDetailBO personalDetail = new DatEmpPersonalDetailBO();
		try {
			personalDetail = empPersonalRepository.findByFkEmpDetailId(empId);
			if (personalDetail != null) {
				return personalDetail.getEmpFirstName() + " "
						+ personalDetail.getEmpLastName();
			}
		} catch (Exception e) {
			LOG.info("Exception Occured: ", e);
		}
		LOG.endUsecase("getEmpNameByEmpId");
		return "";

	}

	/**
	 * 
	 * <Description getAllGoalStatus:> This method is used to get all Goal
	 * Status
	 * 
	 * @return List<GsrMasGoalStatusBO>
	 * @throws DataAccessException
	 */
	public List<GsrMasGoalStatusBO> getAllGoalStatus()
			throws DataAccessException {
		List<GsrMasGoalStatusBO> gsrGoalList = null;
		LOG.startUsecase("View All Goal Status");

		try {
			gsrGoalList = goalStatusRepository.findAll();
		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new DataAccessException(
					"Exception occured while getting all goal status", e);
		}
		LOG.endUsecase("View All Goal Status");
		return gsrGoalList;

	}

	/**
	 * 
	 * <Description viewReporteesGoalsByReportingManager:> This method is used
	 * to view the reportees goals.
	 * 
	 * @param mgrId
	 * @return List<RmViewEmployeeGoalsOutputBean>
	 * @throws DataAccessException
	 */
	public List<RmViewEmployeeGoalsOutputBean> viewReporteesGoalsByReportingManager(
			int mgrId) throws DataAccessException {
		LOG.startUsecase("Entering viewReporteesGoalsByReportingManager");
		List<GsrDatEmpGoalBO> gsrEmpGoalList = null;
		List<RmViewEmployeeGoalsOutputBean> empGoalList = new ArrayList<RmViewEmployeeGoalsOutputBean>();

		try {
			gsrEmpGoalList = reporteesGoalsrepository
					.findByFkGsrDatMgrIdOrderByFkGsrSlabIdDescFkGsrDatEmpIdAscEmpGoalKpiWeightageIdAscPkGsrDatEmpGoalIdAsc(mgrId);

			if (gsrEmpGoalList.size() > 0 || gsrEmpGoalList != null) {
				empGoalList = populateEmpGoalsData(gsrEmpGoalList);
			}
		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new DataAccessException(
					"Exception occured in viewReporteesGoalsByReportingManager",
					e);
		}
		LOG.endUsecase("Exiting viewReporteesGoalsByReportingManager");
		return empGoalList;
	}

	/**
	 * 
	 * <Description populateEmpGoalsData:> This method is used to populate
	 * Employee Goals List
	 * 
	 * @param empGoalsList
	 * @return List<RmViewEmployeeGoalsOutputBean>
	 */
	public List<RmViewEmployeeGoalsOutputBean> populateEmpGoalsData(
			List<GsrDatEmpGoalBO> empGoalsList) {
		RmViewEmployeeGoalsOutputBean empGoalBean = null;
		List<RmViewEmployeeGoalsOutputBean> empGoalList = new ArrayList<RmViewEmployeeGoalsOutputBean>();
		List<FeedbackListBean> feedbackList = null;
		for (int i = 0; i < empGoalsList.size(); i++) {
			feedbackList = new ArrayList<FeedbackListBean>();
			empGoalBean = new RmViewEmployeeGoalsOutputBean();
			empGoalBean.setGoalId(empGoalsList.get(i).getPkGsrDatEmpGoalId());
			empGoalBean.setGoalName(empGoalsList.get(i).getGsrDatEmpGoalName());
			empGoalBean.setGoalStatus(empGoalsList.get(i)
					.getGsrMasGoalStatusBOgoalstatus().getGsrGoalStatusName());
			empGoalBean.setEmpId(empGoalsList.get(i).getFkGsrDatEmpId());
			empGoalBean.setEmpName(getEmpNameByEmpId(empGoalsList.get(i)
					.getFkGsrDatEmpId()));
			empGoalBean.setSlabId(empGoalsList.get(i).getFkGsrSlabId());
			empGoalBean.setSlabName(empGoalsList.get(i).getGsrSlabBO_slab_id()
					.getGsrSlabName());
			empGoalBean.setSlabShortName(empGoalsList.get(i)
					.getGsrSlabBO_slab_id().getGsrSlabShortName());
			empGoalBean.setGoalStartDate(empGoalsList.get(i)
					.getGsrGoalStartDate());
			empGoalBean.setGoalEndDate(empGoalsList.get(i).getGsrGoalEndDate());
			empGoalBean.setGoalWeightage(empGoalsList.get(i)
					.getGsrGoalWeightage());

			empGoalBean.setKpiGoalId(empGoalsList.get(i)
					.getEmpGoalKpiWeightageId());
			if (empGoalsList.get(i).getEmpGoalKpiWeightageId() != null) {
				empGoalBean.setKpiGoalWeightage(empGoalsList.get(i)
						.getGsrKpiWeightageDetails() == null ? null
						: empGoalsList.get(i).getGsrKpiWeightageDetails()
								.getGsrKpiGoalWeightage());
			} else {
				empGoalBean.setKpiGoalWeightage(null);
			}
			if (empGoalsList.get(i).getEmpGoalKpiWeightageId() > 0) {
				if (i > 0) {
					if ((i + 1) < empGoalsList.size()) {
						if (empGoalsList
								.get(i)
								.getEmpGoalKpiWeightageId()
								.equals(empGoalsList.get(i + 1)
										.getEmpGoalKpiWeightageId())) {
							empGoalBean.setGoalId(empGoalsList.get(i + 1)
									.getPkGsrDatEmpGoalId());
							empGoalBean.setGoalName(empGoalsList.get(i + 1)
									.getGsrDatEmpGoalName());
							empGoalBean.setGoalStatus(empGoalsList.get(i + 1)
									.getGsrMasGoalStatusBOgoalstatus()
									.getGsrGoalStatusName());
							empGoalBean.setEmpId(empGoalsList.get(i + 1)
									.getFkGsrDatEmpId());
							empGoalBean
									.setEmpName(getEmpNameByEmpId(empGoalsList
											.get(i + 1).getFkGsrDatEmpId()));
							empGoalBean.setSlabId(empGoalsList.get(i + 1)
									.getFkGsrSlabId());
							empGoalBean.setSlabName(empGoalsList.get(i + 1)
									.getGsrSlabBO_slab_id().getGsrSlabName());
							empGoalBean.setSlabShortName(empGoalsList
									.get(i + 1).getGsrSlabBO_slab_id()
									.getGsrSlabShortName());
							empGoalBean.setGoalStartDate(empGoalsList
									.get(i + 1).getGsrGoalStartDate());
							empGoalBean.setGoalEndDate(empGoalsList.get(i + 1)
									.getGsrGoalEndDate());
							empGoalBean.setGoalWeightage(empGoalsList
									.get(i + 1).getGsrGoalWeightage());
							feedbackList
									.add(populateFeedbackDetails(empGoalsList
											.get(i)));
							feedbackList
									.add(populateFeedbackDetails(empGoalsList
											.get(i + 1)));
							empGoalBean.setFeedbackList(feedbackList);
							empGoalList.add(empGoalBean);
							i = i + 1;
						} else {
							feedbackList
									.add(populateFeedbackDetails(empGoalsList
											.get(i)));
							empGoalBean.setFeedbackList(feedbackList);
							empGoalList.add(empGoalBean);
						}
					} else {
						feedbackList.add(populateFeedbackDetails(empGoalsList
								.get(i)));
						empGoalBean.setFeedbackList(feedbackList);
						empGoalList.add(empGoalBean);
					}
				}
			} else {
				feedbackList.add(populateFeedbackDetails(empGoalsList.get(i)));
				empGoalBean.setFeedbackList(feedbackList);
				empGoalList.add(empGoalBean);
			}
		}
		return empGoalList;
	}

	/**
	 * 
	 * @param empGoal
	 * @return FeedbackListBean
	 * @Description : This method is used to populate Feedback Bean
	 */
	public FeedbackListBean populateFeedbackDetails(GsrDatEmpGoalBO empGoal) {
		FeedbackListBean feedbackBean = new FeedbackListBean();
		GsrDatEmpDelegatedGoalBO delegatedGoalDetail = null;
		feedbackBean.setMgrFeedback(empGoal.getGsrGoalMgrFeedback());
		feedbackBean.setMgrId(empGoal.getFkGsrDatMgrId());
		feedbackBean.setMgrName(getEmpNameByEmpId(empGoal.getFkGsrDatMgrId()));
		feedbackBean.setRating(empGoal.getGsrGoalMgrRating());

		// Added by Smrithi to display the below details.
		if (empGoal.getFkGsrDelegatedGoalId() != null) {
			delegatedGoalDetail = delegatedGoalRepository
					.findByPkGsrDelegatedGoalId(empGoal
							.getFkGsrDelegatedGoalId());
			if (delegatedGoalDetail != null) {
				feedbackBean
						.setDelegatedMgrName(getEmpNameByEmpId(delegatedGoalDetail
								.getFkMgrIdDelegatedTo()));
				feedbackBean.setFkMgrIdDelegatedTo(delegatedGoalDetail
						.getFkMgrIdDelegatedTo());
				feedbackBean.setDelegationStatus(delegatedGoalDetail
						.getDelegationStatus());
			}
		}
		// EOA by Smrithi.
		return feedbackBean;
	}

	/**
	 * 
	 * <Description viewReporteesGoalsByRM:> This method is used to view
	 * reportees goals based on goal status,slabid,list of reportees
	 * 
	 * @param goalBean
	 * @return List<RmViewEmployeeGoalsOutputBean>
	 * @throws DataAccessException
	 */
	public List<RmViewEmployeeGoalsOutputBean> viewReporteesGoalsByRM(
			ViewEmployeeGoalsInputBean goalBean) throws DataAccessException {
		LOG.startUsecase("Entering viewReporteesGoalsByRM");
		List<RmViewEmployeeGoalsOutputBean> empGoalList = new ArrayList<RmViewEmployeeGoalsOutputBean>();
		List<GsrDatEmpGoalBO> gsrEmpGoalList = null;
		RmViewEmployeeGoalsOutputBean outputBean = new RmViewEmployeeGoalsOutputBean();
		GsrDatEmpGoalBO empGoalBo = new GsrDatEmpGoalBO();
		try {
			Specification<GsrDatEmpGoalBO> empGoals = GsrEmpGoalsSpecification
					.getReporteesGoals(goalBean);
			gsrEmpGoalList = reporteesGoalsrepository.findAll(empGoals);
			if (gsrEmpGoalList.size() > 0 || gsrEmpGoalList != null) {
				empGoalList = populateEmpGoalsData(gsrEmpGoalList);
			}

		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new DataAccessException(
					"Exception occured in viewReporteesGoalsByRM", e);
		}

		LOG.endUsecase("Exiting viewReporteesGoalsByRM");
		return empGoalList;
	}

	/**
	 * 
	 * <Description getGoalStatusById:> This method is used to get Slab Details
	 * based on slab id
	 * 
	 * @param goalStatusId
	 * @return GsrSlabBO
	 * @throws DataAccessException
	 */
	public GsrMasGoalStatusBO getGoalStatusById(Byte goalStatusId)
			throws DataAccessException {
		LOG.startUsecase("Entered getSlabInfoById");
		GsrMasGoalStatusBO goalStatusInfo = new GsrMasGoalStatusBO();
		try {
			goalStatusInfo = goalStatusRepository
					.findByPkGsrGoalStatusId(goalStatusId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get the data from Database");
		}
		LOG.endUsecase("Exited getSlabInfoById");
		return goalStatusInfo;
	}

	/**
	 * 
	 * <Description viewEmployeeGoalsByHR:> This method is used to get All
	 * Employee Goals based on Search Criteria
	 * 
	 * @param goalBean
	 * @return List<HrViewEmployeeGoalsOutputBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	public List<HrViewEmployeeGoalsOutputBean> viewEmployeeGoalsByHR(
			HrViewEmpGoalsInputBean goalBean) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("Entering viewEmployeeGoalsByHR");
		List<GsrDatEmpGoalBO> gsrEmpGoalList = null;
		List<HrViewEmployeeGoalsOutputBean> empGoalList = new ArrayList<HrViewEmployeeGoalsOutputBean>();
		HrViewEmployeeGoalsOutputBean empGoalBean = null;
		List<DatEmpProfessionalDetailBO> empIdInBu = new ArrayList<DatEmpProfessionalDetailBO>();
		List<Integer> empIdListInBU = new ArrayList<Integer>();
		List<Integer> tempEmpIdList = new ArrayList<Integer>();
		List<FeedbackListBean> feedbackList = null;
		try {
			if (goalBean.getBuId() != null) {
				empIdInBu = employeeProfessionalRepository
						.findByFkEmpBuUnit(goalBean.getBuId());
				if (empIdInBu.size() > 0) {
					for (DatEmpProfessionalDetailBO empDetail : empIdInBu) {
						empIdListInBU.add(empDetail.getFkMainEmpDetailId());
					}
					if (goalBean.getEmpIdList() != null) {
						if (goalBean.getEmpIdList().size() > 0) {
							for (Integer empId : goalBean.getEmpIdList()) {
								if (empIdListInBU.contains(empId)) {
									tempEmpIdList.add(empId);
								}
							}
							if (tempEmpIdList.size() == 0) {
								throw new CommonCustomException(
										"No Employee present");
							}
							goalBean.setEmpIdList(tempEmpIdList);
						}
					} else {
						goalBean.setEmpIdList(empIdListInBU);
					}
				} else {
					throw new CommonCustomException(
							"No employee's present in BU");
				}
			}

			Specification<GsrDatEmpGoalBO> empGoals = GsrEmpGoalsSpecification
					.getGoalsByHR(goalBean);
			gsrEmpGoalList = reporteesGoalsrepository.findAll(empGoals);

			if (gsrEmpGoalList.size() > 0) {
				for (int i = 0; i < gsrEmpGoalList.size(); i++) {
					feedbackList = new ArrayList<FeedbackListBean>();
					empGoalBean = new HrViewEmployeeGoalsOutputBean();
					empGoalBean.setGoalId(gsrEmpGoalList.get(i)
							.getPkGsrDatEmpGoalId());
					empGoalBean.setGoalName(gsrEmpGoalList.get(i)
							.getGsrDatEmpGoalName());
					empGoalBean.setGoalStatus(gsrEmpGoalList.get(i)
							.getGsrMasGoalStatusBOgoalstatus()
							.getGsrGoalStatusName());
					empGoalBean.setEmpId(gsrEmpGoalList.get(i)
							.getFkGsrDatEmpId());
					empGoalBean.setEmpName(getEmpNameByEmpId(gsrEmpGoalList
							.get(i).getFkGsrDatEmpId()));
					empGoalBean.setSlabId(gsrEmpGoalList.get(i)
							.getFkGsrSlabId());
					empGoalBean.setSlabName(gsrEmpGoalList.get(i)
							.getGsrSlabBO_slab_id().getGsrSlabName());
					empGoalBean.setBuName(commonService
							.getEmpoyeeDeatailsById(
									gsrEmpGoalList.get(i).getFkGsrDatEmpId())
							.getMasBuUnitBO().getBuUnitName());
					empGoalBean.setSlabShortName(gsrEmpGoalList.get(i)
							.getGsrSlabBO_slab_id().getGsrSlabShortName());
					empGoalBean.setGoalStartDate(gsrEmpGoalList.get(i)
							.getGsrGoalStartDate());
					empGoalBean.setGoalEndDate(gsrEmpGoalList.get(i)
							.getGsrGoalEndDate());
					empGoalBean.setGoalWeightage(gsrEmpGoalList.get(i)
							.getGsrGoalWeightage());
					empGoalBean.setKpiGoalId(gsrEmpGoalList.get(i)
							.getEmpGoalKpiWeightageId());
					empGoalBean.setClosedBy(gsrEmpGoalList.get(i)
							.getGsrGoalClosedBy());
					if (gsrEmpGoalList.get(i).getGsrGoalClosedBy() != null)
						empGoalBean
								.setClosedByName(getEmpNameByEmpId(gsrEmpGoalList
										.get(i).getGsrGoalClosedBy()));
					else
						empGoalBean.setClosedByName(null);

					if (gsrEmpGoalList.get(i).getEmpGoalKpiWeightageId() != null) {
						empGoalBean.setKpiGoalWeightage(gsrEmpGoalList.get(i)
								.getGsrKpiWeightageDetails() == null ? null
								: gsrEmpGoalList.get(i)
										.getGsrKpiWeightageDetails()
										.getGsrKpiGoalWeightage());
					} else {
						empGoalBean.setKpiGoalWeightage(null);

					}
					if (gsrEmpGoalList.get(i).getEmpGoalKpiWeightageId() > 0) {
						if (i > 0) {
							if ((i + 1) < gsrEmpGoalList.size()) {
								if (gsrEmpGoalList
										.get(i)
										.getEmpGoalKpiWeightageId()
										.equals(gsrEmpGoalList.get(i + 1)
												.getEmpGoalKpiWeightageId())) {
									empGoalBean.setGoalId(gsrEmpGoalList.get(
											i + 1).getPkGsrDatEmpGoalId());
									empGoalBean.setGoalName(gsrEmpGoalList.get(
											i + 1).getGsrDatEmpGoalName());
									empGoalBean.setGoalStatus(gsrEmpGoalList
											.get(i + 1)
											.getGsrMasGoalStatusBOgoalstatus()
											.getGsrGoalStatusName());
									empGoalBean.setEmpId(gsrEmpGoalList.get(
											i + 1).getFkGsrDatEmpId());
									empGoalBean
											.setEmpName(getEmpNameByEmpId(gsrEmpGoalList
													.get(i + 1)
													.getFkGsrDatEmpId()));
									empGoalBean.setSlabId(gsrEmpGoalList.get(
											i + 1).getFkGsrSlabId());
									empGoalBean.setSlabName(gsrEmpGoalList
											.get(i + 1).getGsrSlabBO_slab_id()
											.getGsrSlabName());
									empGoalBean.setSlabShortName(gsrEmpGoalList
											.get(i + 1).getGsrSlabBO_slab_id()
											.getGsrSlabShortName());
									empGoalBean.setGoalStartDate(gsrEmpGoalList
											.get(i + 1).getGsrGoalStartDate());
									empGoalBean.setGoalEndDate(gsrEmpGoalList
											.get(i + 1).getGsrGoalEndDate());
									empGoalBean.setGoalWeightage(gsrEmpGoalList
											.get(i + 1).getGsrGoalWeightage());
									if (gsrEmpGoalList.get(i + 1)
											.getGsrGoalClosedBy() != null)
										empGoalBean
												.setClosedByName(getEmpNameByEmpId(gsrEmpGoalList
														.get(i + 1)
														.getGsrGoalClosedBy()));
									else
										empGoalBean.setClosedByName(null);
									feedbackList
											.add(populateFeedbackDetails(gsrEmpGoalList
													.get(i)));
									feedbackList
											.add(populateFeedbackDetails(gsrEmpGoalList
													.get(i + 1)));
									empGoalBean.setFeedbackList(feedbackList);
									empGoalList.add(empGoalBean);
									i = i + 1;
								} else {
									feedbackList
											.add(populateFeedbackDetails(gsrEmpGoalList
													.get(i)));
									empGoalBean.setFeedbackList(feedbackList);
									empGoalList.add(empGoalBean);
								}
							} else {
								feedbackList
										.add(populateFeedbackDetails(gsrEmpGoalList
												.get(i)));
								empGoalBean.setFeedbackList(feedbackList);
								empGoalList.add(empGoalBean);
							}
						}
					} else {
						feedbackList.add(populateFeedbackDetails(gsrEmpGoalList
								.get(i)));
						empGoalBean.setFeedbackList(feedbackList);
						empGoalList.add(empGoalBean);
					}
				}
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("Exiting viewEmployeeGoalsByHR");
		return empGoalList;
	}

	/**
	 * 
	 * @param empId
	 * @return List<Map>
	 * @throws CommonCustomException
	 * @Description : This method is used to get current year and previous 3
	 *              year's employee slab and final pre normalized rating
	 */
	public List<PreNormalizedRatingBean> getPreviousSlabAndPreNormalizedRatingForEmployee(
			Integer empId) throws CommonCustomException {
		LOG.startUsecase("Entering getPreviousSlabAndPreNormalizedRatingForEmployee service");
		List<EmpPreNormalizedRatingOutputBean> preNormalizedRatingList = new ArrayList<EmpPreNormalizedRatingOutputBean>();
		List<GsrSlabBO> slabList = null;
		DatEmpDetailBO empDetail = null;
		Map<String, List<EmpPreNormalizedRatingOutputBean>> map = new HashMap<String, List<EmpPreNormalizedRatingOutputBean>>();
		PreNormalizedRatingBean slabBean = null;
		List<PreNormalizedRatingBean> slabBeanList = new ArrayList<PreNormalizedRatingBean>();

		try {
			empDetail = empDetailsRepository.findByPkEmpId(empId);

			Optional<DatEmpDetailBO> empDetailsData = Optional
					.fromNullable(empDetail);

			if (empDetailsData.isPresent()) {
				slabList = gsrSlabRepository
						.getPreviousSlabAndPreNormalizedRating(empId);

				Optional<List<GsrSlabBO>> slabDetailsData = Optional
						.fromNullable(slabList);

				if (slabDetailsData.isPresent()) {

					slabList.forEach(slab -> {
						EmpPreNormalizedRatingOutputBean preNormalizedRatingBean = new EmpPreNormalizedRatingOutputBean();
						preNormalizedRatingBean.setSlabId(slab.getPkGsrSlabId());
						preNormalizedRatingBean.setSlabName(slab
								.getGsrSlabName());
						preNormalizedRatingBean.setSlabShortName(slab
								.getGsrSlabShortName());
						preNormalizedRatingBean.setSlabStartDate(slab
								.getGsrSlabStartDate());
						preNormalizedRatingBean.setSlabEndDate(slab
								.getGsrSlabEndDate());
						preNormalizedRatingBean.setSlabYear(slab
								.getGsrSlabYear());
						preNormalizedRatingBean.setPreNormalizedRating(slab
								.getFinalRating());
						preNormalizedRatingList.add(preNormalizedRatingBean);
					});

					preNormalizedRatingList
							.stream()
							.forEach(
									rating -> {
										if (!map.containsKey(rating
												.getSlabYear())) {
											List<EmpPreNormalizedRatingOutputBean> finalBean = new ArrayList<EmpPreNormalizedRatingOutputBean>();
											finalBean.add(rating);
											map.put(rating.getSlabYear(),
													finalBean);
											return;
										}
										if (map.containsKey(rating
												.getSlabYear())) {
											List<EmpPreNormalizedRatingOutputBean> finalBean = new ArrayList<EmpPreNormalizedRatingOutputBean>();
											finalBean.add(rating);
											map.get(rating.getSlabYear())
													.addAll(finalBean);
										}
									});

					for (Map.Entry<String, List<EmpPreNormalizedRatingOutputBean>> entry : map
							.entrySet()) {
						slabBean = new PreNormalizedRatingBean();
						slabBean.setYear(entry.getKey());
						slabBean.setSlabs(entry.getValue());
						slabBeanList.add(slabBean);
					}
				} else {
					throw new CommonCustomException("No slab Details Present");
				}
			} else {
				throw new CommonCustomException("Invalid Employee Id");
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("Exiting getPreviousSlabAndPreNormalizedRatingForEmployee service");
		return slabBeanList;
	}

	/**
	 * 
	 * @return List<GsrSlabBO>
	 * @throws DataAccessException
	 * @Description : This service has been implemented to get current and
	 *              previous slab details
	 */
	public List<GsrSlabBO> getCurrentAndPreviousSlabs()
			throws DataAccessException {
		LOG.startUsecase("Entering getCurrentAndPreviousSlabs service");
		List<GsrSlabBO> slabDetails = new ArrayList<GsrSlabBO>();
		Date currentDate = new Date();
		int day = 30;
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		cal.add(Calendar.DAY_OF_MONTH, -day);
		Date previousDate = cal.getTime();

		try {
			slabDetails = gsrSlabRepository.getCurrentAndPreviousSlab(
					currentDate, previousDate);
		} catch (Exception e) {
			throw new DataAccessException(
					"Some Exception happened in getCurrentAndPreviousSlabs");
		}
		LOG.endUsecase("Exiting getCurrentAndPreviousSlabs service");
		return slabDetails;
	}

	/**
	 * 
	 * @param teamGoalsBean
	 * @return ViewTeamGoalsOutputBean
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This method is used to View Team Goals by Reporting
	 *              Manager
	 */
	public ViewTeamGoalsOutputBean viewTeamGoalsByRm(
			ViewTeamGoalsInputBean teamGoalsBean) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("Entering viewTeamGoalsByRm service");
		List<GsrDatEmpGoalBO> empGoals = null;
		List<GsrDatEmpGoalBO> tempList = new ArrayList<GsrDatEmpGoalBO>();
		GsrDatEmpGoalBO tempGoalBean = null;
		GsrDatEmpGoalBO empGoalBo = new GsrDatEmpGoalBO();
		ViewTeamGoalsOutputBean teamGoalsOutput = new ViewTeamGoalsOutputBean();
		List<ReporteesBean> reportees = new ArrayList<ReporteesBean>();

		try {
			Specification<GsrDatEmpGoalBO> empGoalsSpecification = GsrEmpGoalsSpecification
					.getTeamGoalsByRm(teamGoalsBean);

			empGoals = reporteesGoalsrepository.findAll(empGoalsSpecification);

			Optional<List<GsrDatEmpGoalBO>> empGoalDetailsList = Optional
					.fromNullable(empGoals);

			if (empGoalDetailsList.isPresent()) {
				for (int i = 0; i < empGoals.size(); i++) {
					tempGoalBean = new GsrDatEmpGoalBO();
					if (empGoals.size() > 1) {
						if (i > 0) {
							if (!(empGoals.get(i).getFkGsrSlabId()
									.equals(empGoals.get(i - 1)
											.getFkGsrSlabId()))
									|| !(empGoals.get(i).getFkGsrDatEmpId()
											.equals(empGoals.get(i - 1)
													.getFkGsrDatEmpId()))
									|| (i + 1) == empGoals.size()) {
								if ((i + 1) == empGoals.size()) {
									if (!(empGoals.get(i).getFkGsrSlabId()
											.equals(empGoals.get(i - 1)
													.getFkGsrSlabId()))
											|| !(empGoals.get(i)
													.getFkGsrDatEmpId()
													.equals(empGoals.get(i - 1)
															.getFkGsrDatEmpId()))) {
										reportees.add(populateTeamGoals(
												empGoals.get(i - 1)
														.getFkGsrDatEmpId(),
												empGoals.get(i - 1)
														.getFkGsrSlabId(),
												tempList));
										tempList.clear();
										tempList.add(empGoals.get(i));
										reportees.add(populateTeamGoals(
												empGoals.get(i)
														.getFkGsrDatEmpId(),
												empGoals.get(i)
														.getFkGsrSlabId(),
												tempList));
									} else {
										tempList.add(empGoals.get(i));
										reportees.add(populateTeamGoals(
												empGoals.get(i - 1)
														.getFkGsrDatEmpId(),
												empGoals.get(i - 1)
														.getFkGsrSlabId(),
												tempList));
									}
								} else {
									reportees
											.add(populateTeamGoals(empGoals
													.get(i - 1)
													.getFkGsrDatEmpId(),
													empGoals.get(i - 1)
															.getFkGsrSlabId(),
													tempList));
									tempList.clear();
									tempList.add(empGoals.get(i));
								}
							} else {
								tempList.add(empGoals.get(i));
							}
						} else {
							tempList.add(empGoals.get(i));
						}
					} else if (empGoals.size() == 1) {
						tempList.add(empGoals.get(i));
						reportees.add(populateTeamGoals(empGoals.get(i)
								.getFkGsrDatEmpId(), empGoals.get(i)
								.getFkGsrSlabId(), tempList));
					}
				}
			} else {
				throw new CommonCustomException("No Data Present");
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		teamGoalsOutput.setReportees(reportees);
		LOG.endUsecase("Exiting viewTeamGoalsByRm service");
		return teamGoalsOutput;
	}

	/**
	 * 
	 * @param empId
	 * @param slabId
	 * @param goalList
	 * @return ReporteesBean
	 * @throws DataAccessException
	 * @Description : This method is used to populate Team Goals
	 */
	public ReporteesBean populateTeamGoals(int empId, short slabId,
			List<GsrDatEmpGoalBO> goalList) throws DataAccessException {
		LOG.startUsecase("Entering populateTeamGoals in Service");
		GsrDatEmpFinalRatingBO finalRating = null;
		ReporteesBean reporteesBean = new ReporteesBean();
		GoalDetails goalDetail = null;
		List<GsrDatEmpGoalBO> tempGoalList = new ArrayList<GsrDatEmpGoalBO>();
		List<GoalDetails> goalDetailsList = new ArrayList<GoalDetails>();
		DatEmpProfessionalDetailBO professionalDetails = null;
		Map<Short, List<GsrDatEmpGoalBO>> empGoalsMap = new HashMap<Short, List<GsrDatEmpGoalBO>>();

		try {
			finalRating = gsrDatEmpFinalRatingRepository
					.findByFkGsrFinalRatingSlabIdAndFkGsrFinalRatingEmpId(
							slabId, empId);

			Optional<GsrDatEmpFinalRatingBO> finalRatingData = Optional
					.fromNullable(finalRating);

			if (goalList.size() > 0) {
				professionalDetails = employeeProfessionalRepository
						.findByFkMainEmpDetailId(goalList.get(0)
								.getFkGsrDatEmpId());
				reporteesBean.setCurrentRmId(professionalDetails
						.getFkEmpReportingMgrId());
				reporteesBean
						.setCurrentMgrName(getEmpNameByEmpId(professionalDetails
								.getFkEmpReportingMgrId()));
			}
			reporteesBean.setEmpId(empId);
			reporteesBean.setSlabId(slabId);
			reporteesBean.setName(getEmpNameByEmpId(empId));

			// Added by Shyam for slab start and end date
			GsrSlabBO gsrSlabBO = new GsrSlabBO();
			gsrSlabBO = gsrSlabRepository.findByPkGsrSlabId(Short
					.valueOf(slabId));

			if (gsrSlabBO != null) {
				reporteesBean.setSlabStartDate(gsrSlabBO.getGsrSlabStartDate());
				reporteesBean.setSlabEndDate(gsrSlabBO.getGsrSlabEndDate());
			} else {
				throw new CommonCustomException(
						"Unable to retrieve Gsr Slab details. Kindly get in touch with Nucleus Support team.");
			}
			// EOA by Shyam

			if (finalRatingData.isPresent()) {
				reporteesBean
						.setSkipLevelMgrId(finalRating.getSkipLevelMgrId());
				if (finalRating.getSkipLevelMgrId() != null)
					reporteesBean
							.setSkipLevelMgrName(getEmpNameByEmpId(finalRating
									.getSkipLevelMgrId()));
				reporteesBean.setFinalRating(finalRating.getGsrFinalRating());
				reporteesBean.setFinalRatingStatusId((int) finalRating
						.getFinalRatingStatus());
				Byte ratingStatus = finalRating.getFinalRatingStatus();
				if (ratingStatus == null) {
					reporteesBean.setFinalRatingStatusName(" ");
				} else {
					if (ratingStatus == 1)
						reporteesBean.setFinalRatingStatusName("Not Yet Rated");
					if (ratingStatus == 2)
						reporteesBean.setFinalRatingStatusName("Accepted");
					if (ratingStatus == 3)
						reporteesBean.setFinalRatingStatusName("Rejected");
					if (ratingStatus == 4)
						reporteesBean.setFinalRatingStatusName("Force Closed");
					if (ratingStatus == 5)
						reporteesBean.setFinalRatingStatusName("Rated");
				}
				reporteesBean.setAcceptedRejectedDate(finalRating
						.getRatingAcceptOrRejectDate());
			} else {
				reporteesBean.setFinalRating(null);
				reporteesBean.setFinalRatingStatusId(null);
				reporteesBean.setFinalRatingStatusName("");
				reporteesBean.setAcceptedRejectedDate(null);
			}
			/*
			 * Date tempSlabStartDate = new Date(); Date tempSlabEndDate = new
			 * Date(); int counter = 0;
			 */

			for (GsrDatEmpGoalBO empGoal : goalList) {
				// Added by Shyam
				// For Slab Start Date
				/*
				 * if(counter == 0) { tempSlabStartDate =
				 * empGoal.getGsrGoalStartDate(); }else{
				 * if(tempSlabStartDate.compareTo(empGoal.getGsrGoalStartDate())
				 * < 0) { tempSlabStartDate = empGoal.getGsrGoalStartDate(); } }
				 * 
				 * //For Slab End Date if(counter == 0) { tempSlabEndDate =
				 * empGoal.getGsrGoalEndDate(); }else{
				 * if(tempSlabEndDate.before(empGoal.getGsrGoalEndDate())) {
				 * tempSlabEndDate = empGoal.getGsrGoalEndDate(); } } ++counter;
				 */
				// EOA by Shyam

				if (!empGoalsMap
						.containsKey(empGoal.getEmpGoalKpiWeightageId())) {
					List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
					finalBean.add(empGoal);
					empGoalsMap.put(empGoal.getEmpGoalKpiWeightageId(),
							finalBean);
					continue;
				}
				if (empGoalsMap.containsKey(empGoal.getEmpGoalKpiWeightageId())) {
					List<GsrDatEmpGoalBO> finalBean = new ArrayList<GsrDatEmpGoalBO>();
					finalBean.add(empGoal);
					empGoalsMap.get(empGoal.getEmpGoalKpiWeightageId()).addAll(
							finalBean);
				}

			}

			/*
			 * reporteesBean.setSlabStartDate(tempSlabStartDate);
			 * reporteesBean.setSlabEndDate(tempSlabEndDate);
			 */

			for (Map.Entry<Short, List<GsrDatEmpGoalBO>> entry : empGoalsMap
					.entrySet()) {
				List<GsrDatEmpGoalBO> empList = (List<GsrDatEmpGoalBO>) entry
						.getValue();
				if (entry.getKey() == 0) {
					tempGoalList.addAll(empList);
				} else {
					Collections.sort(
							empList,
							Comparator.comparing(
									GsrDatEmpGoalBO::getPkGsrDatEmpGoalId)
									.reversed());
					tempGoalList.add(empList.get(0));
					empList.remove(empList.get(0));
					for (GsrDatEmpGoalBO gsrDatEmpGoalBO : empList) {
						if (gsrDatEmpGoalBO.getFkGsrDatMgrId().equals(
								reporteesBean.getCurrentRmId()))
							tempGoalList.add(gsrDatEmpGoalBO);
					}
				}
			}
			for (GsrDatEmpGoalBO empGoal : tempGoalList) {
				reporteesBean.setBuId(empGoal.getFkEmpGoalBuId());
				reporteesBean.setBuName(commonService
						.getEmpoyeeDeatailsById(empGoal.getFkGsrDatEmpId())
						.getMasBuUnitBO().getBuUnitName());
				reporteesBean.setSlabName(empGoal.getGsrSlabBO_slab_id()
						.getGsrSlabName());
				reporteesBean.setSlabShortName(empGoal.getGsrSlabBO_slab_id()
						.getGsrSlabShortName());
				goalDetail = new GoalDetails();
				goalDetail.setGoalId(empGoal.getPkGsrDatEmpGoalId());
				goalDetail.setGoalAcomplishment(empGoal
						.getGsrGoalAccomplishment());
				goalDetail.setGoalDescription(empGoal.getGsrGoalDescription());
				goalDetail.setGoalEndDate(empGoal.getGsrGoalEndDate());
				goalDetail.setGoalMeasurement(empGoal.getGsrGoalMeasurement());
				goalDetail.setGoalMgrRating(empGoal.getGsrGoalMgrRating());
				goalDetail.setGoalName(empGoal.getGsrDatEmpGoalName());
				goalDetail.setGoalSelfRating(empGoal.getGsrGoalSelfRating());
				goalDetail.setGoalStartDate(empGoal.getGsrGoalStartDate());
				goalDetail.setGoalStatusId((int) empGoal.getFkGsrGoalStatus());
				goalDetail.setGoalStatusName(empGoal
						.getGsrMasGoalStatusBOgoalstatus()
						.getGsrGoalStatusName());
				goalDetail
						.setGoalWeightage((int) empGoal.getGsrGoalWeightage());
				goalDetail.setMgrFeedback(empGoal.getGsrGoalMgrFeedback());
				goalDetail.setFkGsrDelegatedGoalId(empGoal
						.getFkGsrDelegatedGoalId());
				if (empGoal.getFkGsrDelegatedGoalId() != null) {
					goalDetail.setDelegatedFromMgrId(getDelegatedGoalDetails(
							empGoal.getFkGsrDelegatedGoalId())
							.getFkMgrIdDelegatedFrom());
					if (goalDetail.getDelegatedFromMgrId() != null)
						goalDetail
								.setDelegatedFromMgrName(getEmpNameByEmpId(goalDetail
										.getDelegatedFromMgrId()));
					goalDetail.setDelegatedToMgrId(getDelegatedGoalDetails(
							empGoal.getFkGsrDelegatedGoalId())
							.getFkMgrIdDelegatedTo());
					if (goalDetail.getDelegatedToMgrId() != null)
						goalDetail
								.setDelegatedToMgrName(getEmpNameByEmpId(goalDetail
										.getDelegatedToMgrId()));
					goalDetail.setDelegationStatus(getDelegatedGoalDetails(
							empGoal.getFkGsrDelegatedGoalId())
							.getDelegationStatus());
				}
				goalDetail.setKpiGoalId(empGoal.getEmpGoalKpiWeightageId());
				if (empGoal.getEmpGoalKpiWeightageId() != null) {
					goalDetail.setKpiGoalWeightage(empGoal
							.getGsrKpiWeightageDetails() == null ? null
							: empGoal.getGsrKpiWeightageDetails()
									.getGsrKpiGoalWeightage());
				} else {
					goalDetail.setKpiGoalWeightage(null);
				}
				goalDetail.setMgrId(empGoal.getFkGsrDatMgrId());
				goalDetailsList.add(goalDetail);
			}
			Collections.sort(
					goalDetailsList,
					(goal1, goal2) -> Integer.compare(goal1.getGoalId(),
							goal2.getGoalId()));
			reporteesBean.setGoals(goalDetailsList);
			reporteesBean.setGoals(goalDetailsList);

		} catch (Exception e) {
			throw new DataAccessException(
					"Some exception happened in populateTeamGoals", e);
		}
		LOG.endUsecase("Exiting populateTeamGoals in Service");
		return reporteesBean;
	}

	/**
	 * 
	 * @param fkDelegatedGoalId
	 * @return
	 * @throws CommonCustomException
	 * @Description : This method is used to Get the Delegated Goal Details.
	 */
	public GsrDatEmpDelegatedGoalBO getDelegatedGoalDetails(
			Integer fkDelegatedGoalId) throws CommonCustomException {
		LOG.startUsecase("getDelegatedGoalDetails");
		GsrDatEmpDelegatedGoalBO delegateGoals = new GsrDatEmpDelegatedGoalBO();
		try {
			delegateGoals = delegatedGoalRepository
					.findByPkGsrDelegatedGoalId(fkDelegatedGoalId);
		} catch (Exception e) {
			LOG.info("Exception Occured: ", e);
		}
		if (delegateGoals == null) {
			throw new CommonCustomException("Delegated goals are not available");
		}
		LOG.endUsecase("getDelegatedGoalDetails");
		return delegateGoals;
	}

	/**
	 * 
	 * @param teamGoalsBean
	 * @return
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This method is used to view Team Goals by HR.
	 */
	public ViewTeamGoalsOutputBean viewTeamGoalsByHr(
			HrViewTeamGoalsInputBean teamGoalsBean)
			throws CommonCustomException, DataAccessException {

		LOG.startUsecase("Entering viewTeamGoalsByHr service");
		List<DatEmpProfessionalDetailBO> empIdInBu = new ArrayList<DatEmpProfessionalDetailBO>();
		List<Integer> empIdListInBU = new ArrayList<Integer>();
		List<Integer> tempEmpIdList = new ArrayList<Integer>();
		List<GsrDatEmpGoalBO> empGoals = null;
		List<GsrDatEmpGoalBO> tempList = new ArrayList<GsrDatEmpGoalBO>();
		GsrDatEmpGoalBO tempGoalBean = null;
		GsrDatEmpGoalBO empGoalBo = new GsrDatEmpGoalBO();
		List<ReporteesBean> reportees = new ArrayList<ReporteesBean>();
		ViewTeamGoalsOutputBean teamGoalsOutput = new ViewTeamGoalsOutputBean();
		try {
			if (teamGoalsBean.getBuId() != null) {
				empIdInBu = employeeProfessionalRepository
						.findByFkEmpBuUnit(teamGoalsBean.getBuId());
				if (empIdInBu.size() > 0) {
					for (DatEmpProfessionalDetailBO empDetail : empIdInBu) {
						empIdListInBU.add(empDetail.getFkMainEmpDetailId());
					}
					if (teamGoalsBean.getEmpIdList() != null) {
						if (teamGoalsBean.getEmpIdList().size() > 0) {
							for (Integer empId : teamGoalsBean.getEmpIdList()) {
								if (empIdListInBU.contains(empId)) {
									tempEmpIdList.add(empId);
								}
							}
							if (tempEmpIdList.size() == 0) {
								throw new CommonCustomException(
										"No Employee present");
							}
							teamGoalsBean.setEmpIdList(tempEmpIdList);
						}
					} else {
						teamGoalsBean.setEmpIdList(empIdListInBU);
					}
				} else {
					throw new CommonCustomException(
							"No employee's present in BU");
				}
			}

			// teamGoalsOutput.setKpiGoalId(empGoalBo.getEmpGoalKpiWeightageId());
			// if(empGoalBo.getEmpGoalKpiWeightageId()>0)
			// {
			// teamGoalsOutput.setKpiGoalWeightage(empGoalBo.getGsrKpiWeightageDetails()==null
			// ?
			// null:empGoalBo.getGsrKpiWeightageDetails().getGsrKpiGoalWeightage());
			// }
			// else
			// {
			// teamGoalsOutput.setKpiGoalWeightage(null);
			//
			// }

			Specification<GsrDatEmpGoalBO> empGoalsSpecification = GsrEmpGoalsSpecification
					.getTeamGoalsByHr(teamGoalsBean);

			empGoals = reporteesGoalsrepository.findAll(empGoalsSpecification);

			Optional<List<GsrDatEmpGoalBO>> empGoalDetails = Optional
					.fromNullable(empGoals);

			if (empGoalDetails.isPresent()) {
				for (int i = 0; i < empGoals.size(); i++) {
					tempGoalBean = new GsrDatEmpGoalBO();
					if (empGoals.size() > 1) {
						if (i > 0) {
							if (!(empGoals.get(i).getFkGsrSlabId()
									.equals(empGoals.get(i - 1)
											.getFkGsrSlabId()))
									|| !(empGoals.get(i).getFkGsrDatEmpId()
											.equals(empGoals.get(i - 1)
													.getFkGsrDatEmpId()))
									|| (i + 1) == empGoals.size()) {
								if ((i + 1) == empGoals.size()) {
									if (!(empGoals.get(i).getFkGsrSlabId()
											.equals(empGoals.get(i - 1)
													.getFkGsrSlabId()))
											|| !(empGoals.get(i)
													.getFkGsrDatEmpId()
													.equals(empGoals.get(i - 1)
															.getFkGsrDatEmpId()))) {
										reportees.add(populateTeamGoals(
												empGoals.get(i - 1)
														.getFkGsrDatEmpId(),
												empGoals.get(i - 1)
														.getFkGsrSlabId(),
												tempList));
										tempList.clear();
										tempList.add(empGoals.get(i));
										reportees.add(populateTeamGoals(
												empGoals.get(i)
														.getFkGsrDatEmpId(),
												empGoals.get(i)
														.getFkGsrSlabId(),
												tempList));
									} else {
										tempList.add(empGoals.get(i));
										reportees.add(populateTeamGoals(
												empGoals.get(i - 1)
														.getFkGsrDatEmpId(),
												empGoals.get(i - 1)
														.getFkGsrSlabId(),
												tempList));
									}
								} else {
									reportees
											.add(populateTeamGoals(empGoals
													.get(i - 1)
													.getFkGsrDatEmpId(),
													empGoals.get(i - 1)
															.getFkGsrSlabId(),
													tempList));
									tempList.clear();
									tempList.add(empGoals.get(i));
								}
							} else {
								tempList.add(empGoals.get(i));
							}
						} else {
							tempList.add(empGoals.get(i));
						}
					} else if (empGoals.size() == 1) {
						tempList.add(empGoals.get(i));
						reportees.add(populateTeamGoals(empGoals.get(i)
								.getFkGsrDatEmpId(), empGoals.get(i)
								.getFkGsrSlabId(), tempList));
					}
				}
			} else {
				throw new CommonCustomException("No Data Present");
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		teamGoalsOutput.setReportees(reportees);
		LOG.endUsecase("Exiting viewTeamGoalsByHr service");
		return teamGoalsOutput;
	}

	/**
	 * 
	 * @param delegationGoalsBean
	 * @return DelegationGoalsBean
	 * @throws DataAccessException
	 * @Description : This service is used to view the goals that can be
	 *              delegated and goals which are delegated to or from empId
	 */
	public DelegationGoalsBean viewDelegatedGoals(
			DelegationGoalsInputBean delegationGoalsBean)
			throws DataAccessException {
		LOG.startUsecase("Entering viewDelegatedGoals service");
		List<DelegationResultFromDbBean> empGoals = null;
		List<DelegationGoalsOutputBean> delGoalsOutput = new ArrayList<DelegationGoalsOutputBean>();
		GoalDetailsForDelegationBean goalDetailsDelOutput = null;
		DelegationGoalsBean delegationGoalsOutputBean = null;
		List<DelegationResultFromDbBean> tempGoalsBean = new ArrayList<DelegationResultFromDbBean>();
		List<GoalDetailsForDelegationBean> goalDetailsList = new ArrayList<GoalDetailsForDelegationBean>();
		List<Integer> empIdList = new ArrayList<Integer>();
		try {
			if (delegationGoalsBean.getEmpIdList() == null
					|| delegationGoalsBean.getEmpIdList().size() == 0) {
				empIdList = repository
						.getReporteesEmpIdForRm(delegationGoalsBean
								.getDelegationMgrId());
				if (empIdList.size() == 0)
					delegationGoalsBean.setEmpIdList(null);
				else
					delegationGoalsBean.setEmpIdList(empIdList);
			}
			empGoals = repository.getEmpGoalsForDelegation(
					delegationGoalsBean.getEmpIdList(),
					delegationGoalsBean.getDelegationMgrId());
			if (empGoals != null || empGoals.size() > 0) {
				delegationGoalsOutputBean = new DelegationGoalsBean();
				for (int i = 0; i < empGoals.size(); i++) {
					if (empGoals.size() > 1) {
						if (i > 0) {
							if (!(empGoals.get(i).getFkGsrSlabId()
									.equals(empGoals.get(i - 1)
											.getFkGsrSlabId()))
									|| !(empGoals.get(i).getFkGsrDatEmpId()
											.equals(empGoals.get(i - 1)
													.getFkGsrDatEmpId()))
									|| (i + 1) == empGoals.size()) {
								if ((i + 1) == empGoals.size()) {
									if (!(empGoals.get(i).getFkGsrSlabId()
											.equals(empGoals.get(i - 1)
													.getFkGsrSlabId()))
											|| !(empGoals.get(i)
													.getFkGsrDatEmpId()
													.equals(empGoals.get(i - 1)
															.getFkGsrDatEmpId()))) {
										goalDetailsDelOutput = new GoalDetailsForDelegationBean();
										goalDetailsDelOutput
												.setAcceptedOrRejectedDate(empGoals
														.get(i - 1)
														.getFinalRatingAccRejDate());
										goalDetailsDelOutput.setEmpId(empGoals
												.get(i - 1).getFkGsrDatEmpId());
										goalDetailsDelOutput
												.setEmpName(empGoals.get(i - 1)
														.getEmpName());
										goalDetailsDelOutput
												.setFinalRating(empGoals.get(
														i - 1).getFinalRating());
										goalDetailsDelOutput
												.setFinalRatingStatusId(empGoals
														.get(i - 1)
														.getFinalRatingStatus());
										goalDetailsDelOutput
												.setFinalRatingStatusName(empGoals
														.get(i - 1)
														.getFinalRatingStatusName());
										goalDetailsDelOutput.setSlabId(empGoals
												.get(i - 1).getFkGsrSlabId());
										goalDetailsDelOutput
												.setSlabName(empGoals
														.get(i - 1)
														.getGsrSlabName());
										goalDetailsDelOutput
												.setSlabShortName(empGoals.get(
														i - 1)
														.getSlabShortName());
										delGoalsOutput = populateDelegationDetails(tempGoalsBean);
										tempGoalsBean.clear();
										tempGoalsBean.add(empGoals.get(i));
										goalDetailsDelOutput
												.setGoals(delGoalsOutput);
										goalDetailsList
												.add(goalDetailsDelOutput);

										goalDetailsDelOutput = new GoalDetailsForDelegationBean();
										goalDetailsDelOutput
												.setAcceptedOrRejectedDate(empGoals
														.get(i)
														.getFinalRatingAccRejDate());
										goalDetailsDelOutput.setEmpId(empGoals
												.get(i).getFkGsrDatEmpId());
										goalDetailsDelOutput
												.setEmpName(empGoals.get(i)
														.getEmpName());
										goalDetailsDelOutput
												.setFinalRating(empGoals.get(i)
														.getFinalRating());
										goalDetailsDelOutput
												.setFinalRatingStatusId(empGoals
														.get(i)
														.getFinalRatingStatus());
										goalDetailsDelOutput
												.setFinalRatingStatusName(empGoals
														.get(i)
														.getFinalRatingStatusName());
										goalDetailsDelOutput.setSlabId(empGoals
												.get(i).getFkGsrSlabId());
										goalDetailsDelOutput
												.setSlabName(empGoals.get(i)
														.getGsrSlabName());
										goalDetailsDelOutput
												.setSlabShortName(empGoals.get(
														i).getSlabShortName());
										delGoalsOutput = populateDelegationDetails(tempGoalsBean);
										tempGoalsBean.clear();
										tempGoalsBean.add(empGoals.get(i));
										goalDetailsDelOutput
												.setGoals(delGoalsOutput);
										goalDetailsList
												.add(goalDetailsDelOutput);
									} else {
										goalDetailsDelOutput = new GoalDetailsForDelegationBean();
										goalDetailsDelOutput
												.setAcceptedOrRejectedDate(empGoals
														.get(i - 1)
														.getFinalRatingAccRejDate());
										goalDetailsDelOutput.setEmpId(empGoals
												.get(i - 1).getFkGsrDatEmpId());
										goalDetailsDelOutput
												.setEmpName(empGoals.get(i - 1)
														.getEmpName());
										goalDetailsDelOutput
												.setFinalRating(empGoals.get(
														i - 1).getFinalRating());
										goalDetailsDelOutput
												.setFinalRatingStatusId(empGoals
														.get(i - 1)
														.getFinalRatingStatus());
										goalDetailsDelOutput
												.setFinalRatingStatusName(empGoals
														.get(i - 1)
														.getFinalRatingStatusName());
										goalDetailsDelOutput.setSlabId(empGoals
												.get(i - 1).getFkGsrSlabId());
										goalDetailsDelOutput
												.setSlabName(empGoals
														.get(i - 1)
														.getGsrSlabName());
										goalDetailsDelOutput
												.setSlabShortName(empGoals.get(
														i - 1)
														.getSlabShortName());
										tempGoalsBean.add(empGoals.get(i));
										delGoalsOutput = populateDelegationDetails(tempGoalsBean);
										tempGoalsBean.clear();
										tempGoalsBean.add(empGoals.get(i));
										goalDetailsDelOutput
												.setGoals(delGoalsOutput);
										goalDetailsList
												.add(goalDetailsDelOutput);
									}
								} else {
									if (!(empGoals.get(i).getFkGsrSlabId()
											.equals(empGoals.get(i - 1)
													.getFkGsrSlabId()))
											|| !(empGoals.get(i)
													.getFkGsrDatEmpId()
													.equals(empGoals.get(i - 1)
															.getFkGsrDatEmpId()))) {
										goalDetailsDelOutput = new GoalDetailsForDelegationBean();
										goalDetailsDelOutput
												.setAcceptedOrRejectedDate(empGoals
														.get(i - 1)
														.getFinalRatingAccRejDate());
										goalDetailsDelOutput.setEmpId(empGoals
												.get(i - 1).getFkGsrDatEmpId());
										goalDetailsDelOutput
												.setEmpName(empGoals.get(i - 1)
														.getEmpName());
										goalDetailsDelOutput
												.setFinalRating(empGoals.get(
														i - 1).getFinalRating());
										goalDetailsDelOutput
												.setFinalRatingStatusId(empGoals
														.get(i - 1)
														.getFinalRatingStatus());
										goalDetailsDelOutput
												.setFinalRatingStatusName(empGoals
														.get(i - 1)
														.getFinalRatingStatusName());
										goalDetailsDelOutput.setSlabId(empGoals
												.get(i - 1).getFkGsrSlabId());
										goalDetailsDelOutput
												.setSlabName(empGoals
														.get(i - 1)
														.getGsrSlabName());
										goalDetailsDelOutput
												.setSlabShortName(empGoals.get(
														i - 1)
														.getSlabShortName());
										delGoalsOutput = populateDelegationDetails(tempGoalsBean);
										tempGoalsBean.clear();
										tempGoalsBean.add(empGoals.get(i));
										goalDetailsDelOutput
												.setGoals(delGoalsOutput);
										goalDetailsList
												.add(goalDetailsDelOutput);
									} else {
										tempGoalsBean.add(empGoals.get(i));
									}
								}
							} else {
								tempGoalsBean.add(empGoals.get(i));
							}
						} else {
							tempGoalsBean.add(empGoals.get(i));
						}
					} else if (empGoals.size() == 1) {
						tempGoalsBean.add(empGoals.get(i));

						goalDetailsDelOutput = new GoalDetailsForDelegationBean();
						goalDetailsDelOutput.setAcceptedOrRejectedDate(empGoals
								.get(i).getFinalRatingAccRejDate());
						goalDetailsDelOutput.setEmpId(empGoals.get(i)
								.getFkGsrDatEmpId());
						goalDetailsDelOutput.setEmpName(empGoals.get(i)
								.getEmpName());
						goalDetailsDelOutput.setFinalRating(empGoals.get(i)
								.getFinalRating());
						goalDetailsDelOutput.setFinalRatingStatusId(empGoals
								.get(i).getFinalRatingStatus());
						goalDetailsDelOutput.setFinalRatingStatusName(empGoals
								.get(i).getFinalRatingStatusName());
						goalDetailsDelOutput.setSlabId(empGoals.get(i)
								.getFkGsrSlabId());
						goalDetailsDelOutput.setSlabName(empGoals.get(i)
								.getGsrSlabName());
						goalDetailsDelOutput.setSlabShortName(empGoals.get(i)
								.getSlabShortName());
						delGoalsOutput = populateDelegationDetails(tempGoalsBean);
						goalDetailsDelOutput.setGoals(delGoalsOutput);
						goalDetailsList.add(goalDetailsDelOutput);
					}
				}
				delegationGoalsOutputBean.setReportees(goalDetailsList);
			}
		} catch (Exception e) {
			throw new DataAccessException(
					"Some exception occured in View Delegate Goals");
		}
		LOG.endUsecase("Exiting viewDelegatedGoals service");
		return delegationGoalsOutputBean;
	}

	/**
	 * 
	 * @param goalList
	 * @return List<DelegationGoalsOutputBean>
	 * @Description : This method is used to populate Delegation Details
	 */
	public List<DelegationGoalsOutputBean> populateDelegationDetails(
			List<DelegationResultFromDbBean> goalList) {
		List<DelegationGoalsOutputBean> delegationGoalsList = new ArrayList<DelegationGoalsOutputBean>();
		if (goalList.size() > 0) {
			goalList.forEach(delGoal -> {
				DelegationGoalsOutputBean delGoalBean = new DelegationGoalsOutputBean();
				delGoalBean.setAcceptedOrRejectedDate(delGoal
						.getDelAccRejDate());
				delGoalBean.setDelegatedBy(delGoal.getDelegatedByEmpId());
				delGoalBean.setDelegatedByName(delGoal.getDelegatedByEmpName());
				delGoalBean.setDelegatedDate(delGoal.getDelegatedDate());
				delGoalBean.setDelegatedFrom(delGoal.getDelegatedFromEmpId());
				delGoalBean.setDelegatedFromName(delGoal
						.getDelegatedFromEmpName());
				delGoalBean.setDelegatedGoalId(delGoal
						.getFkGsrDelegatedGoalId());
				delGoalBean.setDelegatedStatus(delGoal.getDelegationStatus());
				delGoalBean.setDelegatedTo(delGoal.getDelegatedToEmpId());
				delGoalBean.setDelegatedToName(delGoal.getDelegatedToEmpName());
				delGoalBean.setGoalAccomplishment(delGoal
						.getGsrGoalAccomplishment());
				delGoalBean.setGoalDescription(delGoal.getGsrGoalDescription());
				delGoalBean.setGoalEndDate(delGoal.getGsrGoalEndDate());
				delGoalBean.setGoalId(delGoal.getPkGsrDatEmpGoalId());
				delGoalBean.setGoalMeasurement(delGoal.getGsrGoalMeasurement());
				delGoalBean.setGoalMgrRating(delGoal.getGsrGoalMgrRating());
				delGoalBean.setGoalName(delGoal.getGsrDatEmpGoalName());
				delGoalBean.setGoalSelfRating(delGoal.getGsrGoalSelfRating());
				delGoalBean.setGoalStartDate(delGoal.getGsrGoalStartDate());
				delGoalBean.setGoalStatusId(delGoal.getFkGsrGoalStatus());
				delGoalBean.setGoalStatusName(delGoal.getGsrGoalStatusName());
				delGoalBean.setGoalWeightage(delGoal.getGsrGoalWeightage());
				delGoalBean.setKpiGoalId(delGoal.getFkGsrKpiGoalId());
				delGoalBean.setKpiGoalWeightage(delGoal
						.getEmpGoalKpiWeightageId());
				delGoalBean.setMgrFeedback(delGoal.getGsrGoalMgrFeedback());
				delGoalBean.setDelegationStatusName(delGoal
						.getDelegationStatusName());
				delegationGoalsList.add(delGoalBean);
			});
		}
		return delegationGoalsList;
	}

	// End of Addition by Kamal Anand

	// Added by Shyam for creation of new gsr goal
	/**
	 * @method storeGsrGoal
	 * @param GsrCreateGoalInputBean
	 * @return GsrGoalOutputBean
	 * @throws CommonCustomException
	 *             , DataAccessException , BusinessException
	 * @Description : This method is used to store input gsr goal into DB(used
	 *              by employee and manager too).
	 */
	public GsrGoalOutputBean storeGsrGoal(
			GsrCreateGoalInputBean gsrCreateGoalInputBean)
			throws CommonCustomException {
		// LOG.info("gsrCreateGoalInputBean :: " + gsrCreateGoalInputBean);
		// //Added for testing
		GsrGoalOutputBean gsrGoalOutputBean = new GsrGoalOutputBean();
		GsrDatEmpGoalBO inputGsrGoalBO = new GsrDatEmpGoalBO();

		// Default varibales are needed to be set.
		inputGsrGoalBO.setGsrGoalCreatedDate(new Date());

		// Few datas are required to do validation just like as employeeId,
		// managerId,
		// goalName, goalStartDate, slabId, goalWeightage, goalCreatedBy and
		// gsrKpiWeightageId

		// #1. Fetch list of "DatEmpProfessionalDetailBO" object,
		// Input --> employeeId and managerId
		List<DatEmpProfessionalDetailBO> empProfDetailList = new ArrayList<DatEmpProfessionalDetailBO>();
		boolean empValidExistFlag = false;
		boolean mgrValidExistFlag = false;
		int employeeId = gsrCreateGoalInputBean.getEmployeeId();
		int managerId = gsrCreateGoalInputBean.getManagerId();
		DatEmpProfessionalDetailBO employeeProfDetailObject = new DatEmpProfessionalDetailBO();
		
		try {
			empProfDetailList = employeeProfessionalRepository
					.getEmpProfDetailList(employeeId, managerId);
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to get employee and managers details");
		}

		if (!empProfDetailList.isEmpty()) {
			for (DatEmpProfessionalDetailBO datEmpProfessionalDetailBO : empProfDetailList) {
				if (datEmpProfessionalDetailBO.getFkMainEmpDetailId()
						.intValue() == employeeId) {
					employeeProfDetailObject = datEmpProfessionalDetailBO;
					empValidExistFlag = true;
				}

				if (datEmpProfessionalDetailBO.getFkMainEmpDetailId()
						.intValue() == managerId) {
					// mgrProfDetailObject = datEmpProfessionalDetailBO;
					mgrValidExistFlag = true;
				}
			}
		}

		if (!empValidExistFlag) {
			throw new CommonCustomException("Employee Id " + employeeId
					+ " does not exist");
		} else {
			inputGsrGoalBO.setFkGsrDatEmpId(gsrCreateGoalInputBean
					.getEmployeeId());
			// Set Domain manager id and bu id from employeeProfDetailObject
			inputGsrGoalBO.setFkEmpGoalDomainMgrId(employeeProfDetailObject
					.getFkEmpDomainMgrId());
			inputGsrGoalBO.setFkEmpGoalBuId(employeeProfDetailObject
					.getFkEmpBuUnit());
		}

		// LOG.info("mgrProfDetailObject ::: " + mgrProfDetailObject);
		if (employeeId != gsrCreateGoalInputBean.getGoalCreatedBy()
				&& managerId != gsrCreateGoalInputBean.getGoalCreatedBy()) {
			throw new CommonCustomException("The goalCreatedBy id "
					+ gsrCreateGoalInputBean.getGoalCreatedBy()
					+ " must be a valid employee id");
		} else {
			inputGsrGoalBO.setGsrGoalCreatedBy(gsrCreateGoalInputBean
					.getGoalCreatedBy());
			// Set Gsr Goal Status. This depends upon the who created this goal.
			if (gsrCreateGoalInputBean.getGoalCreatedBy() == employeeId)
				inputGsrGoalBO
						.setFkGsrGoalStatus(goalWaitingForManagerApprovalStatus);
			// if(gsrCreateGoalInputBean.getGoalCreatedBy() == managerId)
			else
				inputGsrGoalBO.setFkGsrGoalStatus(goalGoalSetStatus);
		}

		// Added by Shyam for RM change scenario
		// Check in a slab whether employee have two different manager id.
		List<GsrDatEmpGoalBO> empGoalList = new ArrayList<GsrDatEmpGoalBO>();
		try {
			empGoalList = gsrDatEmpGoalRepository.getAllEnteredGsrGoalsOfASlab(
					gsrCreateGoalInputBean.getSlabId(),
					gsrCreateGoalInputBean.getEmployeeId());
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to get all entered goals detail. Kindly touch with "
							+ "Nucleus Support team for this issue.");
		}

		boolean otherMgrIdIsPresentIsGoal = false;
		int tempGoalMgrId = 0;
		List<GsrDatEmpGoalBO> otherMgrGoalDetails = new ArrayList<GsrDatEmpGoalBO>();
		if (!empGoalList.isEmpty()) {
			for (GsrDatEmpGoalBO gsrDatEmpGoalBO4 : empGoalList) {
				if (tempGoalMgrId == 0) {
					tempGoalMgrId = gsrDatEmpGoalBO4.getFkGsrDatMgrId()
							.intValue();
					otherMgrGoalDetails.add(gsrDatEmpGoalBO4);
				} else {
					if (tempGoalMgrId != gsrDatEmpGoalBO4.getFkGsrDatMgrId()
							.intValue()) {
						otherMgrIdIsPresentIsGoal = true;
						break;
					} else {
						otherMgrGoalDetails.add(gsrDatEmpGoalBO4);
					}
				}
			}
		}

		// Few checks of employee and manager id shifted at last. When BuHead,
		// Domain Mgr, Skip Level Mgr and HR.
		// Check any goal is created for that slab or not
		List<GsrDatEmpGoalBO> allEnteredGoals = new ArrayList<GsrDatEmpGoalBO>();
		// List<GsrDatEmpGoalBO> previousMgrGoals = new
		// ArrayList<GsrDatEmpGoalBO>();
		boolean previousMgrTryToCreateGoal = false;
		if (otherMgrIdIsPresentIsGoal) {
			try {
				allEnteredGoals = gsrDatEmpGoalRepository
						.getAllEnteredGoalsOfASlab(
								gsrCreateGoalInputBean.getSlabId(),
								gsrCreateGoalInputBean.getEmployeeId());
			} catch (Exception ex) {
				throw new CommonCustomException(
						"Unable to retrieve all entered goals. Please contact to Nucleus Suport team");
			}

			if (!otherMgrGoalDetails.isEmpty()) {
				for (GsrDatEmpGoalBO gsrDatEmpGoalBO5 : otherMgrGoalDetails) {
					if (gsrCreateGoalInputBean.getManagerId() == gsrDatEmpGoalBO5
							.getFkGsrDatMgrId().intValue())
					// gsrDatEmpGoalBO5.getFkGsrGoalStatus().byteValue() ==
					// goalManagerFeedbackStatus)
					{
						if (gsrDatEmpGoalBO5.getFkGsrGoalStatus().byteValue() == goalManagerFeedbackStatus
								|| gsrDatEmpGoalBO5.getFkGsrGoalStatus()
										.byteValue() == goalRatedStatus) {
							previousMgrTryToCreateGoal = true;
							break;
						}
					}
				}
			}
		} else {
			allEnteredGoals.addAll(empGoalList);
		}

		String reportingMgrName = "";
		int totalEnteredWeightage = 0;

		if (previousMgrTryToCreateGoal) {
			for (GsrDatEmpGoalBO gsrDatEmpGoalBO : allEnteredGoals) {
				if (reportingMgrName.isEmpty()) {
					reportingMgrName = getEmpNameByEmpId(gsrDatEmpGoalBO
							.getFkGsrDatMgrId());
				}
				totalEnteredWeightage = totalEnteredWeightage
						+ gsrDatEmpGoalBO.getGsrGoalWeightage().byteValue();

			}

			throw new CommonCustomException("Goals have been created by "
					+ reportingMgrName + " for " + totalEnteredWeightage
					+ " %. Hence you are not allowed to create "
					+ "the goals for this quarter.");
		}
		// End of addition by Shyam for RM change scenario

		// --------------------------------------------------------------------

		if (!mgrValidExistFlag) {
			throw new CommonCustomException("Manager Id " + managerId
					+ " does not exist");
		} else {
			if (allEnteredGoals.isEmpty()) {
				inputGsrGoalBO.setFkGsrDatMgrId(Integer
						.valueOf(gsrCreateGoalInputBean.getManagerId()));
				managerId = gsrCreateGoalInputBean.getManagerId();
			} else {

				boolean reportingMgrIdIsNotInAllEnteredGoalsFlag = false;
				boolean currentMgrIdIsNotPresentInEnteredGoalFlag = false;

				// To get total weightage of goal entered by goal mgr for UI
				// message.
				for (GsrDatEmpGoalBO gsrDatEmpGoalBO1 : allEnteredGoals) {
					totalEnteredWeightage = totalEnteredWeightage
							+ gsrDatEmpGoalBO1.getGsrGoalWeightage();
				}

				// When any Goal create by employee's reporting mgr
				if (gsrCreateGoalInputBean.getGoalCreatedBy() == employeeProfDetailObject
						.getFkEmpReportingMgrId().intValue()) {
					// LOG.info("part --1111 ");
					if (allEnteredGoals.isEmpty()) {
						inputGsrGoalBO.setFkGsrDatMgrId(gsrCreateGoalInputBean
								.getManagerId());
						managerId = gsrCreateGoalInputBean.getManagerId();
					} else {
						// Check if reporting manager does not have any goal
						// entry and other manager id is present in existing
						// goal
						for (GsrDatEmpGoalBO gsrDatEmpGoalBO : allEnteredGoals) {
							if (employeeProfDetailObject
									.getFkEmpReportingMgrId().intValue() != gsrDatEmpGoalBO
									.getFkGsrDatMgrId().intValue()) {
								reportingMgrName = getEmpNameByEmpId(gsrDatEmpGoalBO
										.getFkGsrDatMgrId().intValue());
								reportingMgrIdIsNotInAllEnteredGoalsFlag = true;
								break;
							}
						}

						if (reportingMgrIdIsNotInAllEnteredGoalsFlag) {
							throw new CommonCustomException(
									"Goals have been created by "
											+ reportingMgrName
											+ " for "
											+ totalEnteredWeightage
											+ " %. Hence you are not allowed to create "
											+ "the goals for this quarter.");
						} else {
							inputGsrGoalBO
									.setFkGsrDatMgrId(gsrCreateGoalInputBean
											.getManagerId());
							managerId = gsrCreateGoalInputBean.getManagerId();
						}
					}
				}// If any goal is created by employee
				else if (gsrCreateGoalInputBean.getGoalCreatedBy() == employeeProfDetailObject
						.getFkMainEmpDetailId().intValue()) {
					// Check is there any goal entered for that slab
					if (allEnteredGoals.isEmpty()) {
						// From UI always sending employee's manager id in input
						// json
						inputGsrGoalBO
								.setFkGsrDatMgrId(employeeProfDetailObject
										.getFkEmpReportingMgrId());
						managerId = gsrCreateGoalInputBean.getManagerId();
					} else {
						for (GsrDatEmpGoalBO gsrDatEmpGoalBO1 : allEnteredGoals) {
							if (gsrCreateGoalInputBean.getManagerId() != gsrDatEmpGoalBO1
									.getFkGsrDatMgrId().intValue()) {
								reportingMgrName = getEmpNameByEmpId(gsrDatEmpGoalBO1
										.getFkGsrDatMgrId().intValue());
								currentMgrIdIsNotPresentInEnteredGoalFlag = true;
								break;
							}
						}

						if (currentMgrIdIsNotPresentInEnteredGoalFlag) {
							throw new CommonCustomException(
									"Goals have been created by "
											+ reportingMgrName
											+ " for "
											+ totalEnteredWeightage
											+ " %. Hence you are not allowed to create "
											+ "the goals for this quarter.");
						} else {
							inputGsrGoalBO
									.setFkGsrDatMgrId(employeeProfDetailObject
											.getFkEmpReportingMgrId());
							managerId = gsrCreateGoalInputBean.getManagerId();
						}
					}
					// managerId = gsrCreateGoalInputBean.getManagerId();
					// =====Work completed tiil here
				}// If other mgr try to create a goal
				else {
					// LOG.info("part ---222 ");
					// Get list of all other mgrs including HR for creation of
					// employee's goal
					if (allEnteredGoals.isEmpty()) {
						inputGsrGoalBO
								.setFkGsrDatMgrId(Integer
										.valueOf(gsrCreateGoalInputBean
												.getManagerId()));
						managerId = gsrCreateGoalInputBean.getManagerId();
					} else {
						for (GsrDatEmpGoalBO gsrDatEmpGoalBO3 : allEnteredGoals) {
							// Check reporting mgr have any entered goals
							if (employeeProfDetailObject
									.getFkEmpReportingMgrId().intValue() != gsrDatEmpGoalBO3
									.getFkGsrDatMgrId().intValue()) {
								reportingMgrIdIsNotInAllEnteredGoalsFlag = true;
							}

							if (gsrCreateGoalInputBean.getManagerId() != gsrDatEmpGoalBO3
									.getFkGsrDatMgrId().intValue()) {
								// LOG.info("part  ---222 --- 3333");
								reportingMgrName = getEmpNameByEmpId(gsrDatEmpGoalBO3
										.getFkGsrDatMgrId().intValue());
								currentMgrIdIsNotPresentInEnteredGoalFlag = true;
								break;
							}
						}

						/*
						 * LOG.info(
						 * "-----------------------------------------------------------------------------------"
						 * );
						 * LOG.info("currentMgrIdIsNotPresentInEnteredGoalFlag :: "
						 * + currentMgrIdIsNotPresentInEnteredGoalFlag);
						 * LOG.info
						 * ("reportingMgrIdIsNotInAllEnteredGoalsFlag :: " +
						 * reportingMgrIdIsNotInAllEnteredGoalsFlag); LOG.info(
						 * "-----------------------------------------------------------------------------------"
						 * );
						 */
						if (currentMgrIdIsNotPresentInEnteredGoalFlag) {
							// Have reporting mgr id in Gsr Gool mgr id.
							if (!reportingMgrIdIsNotInAllEnteredGoalsFlag) {
								inputGsrGoalBO
										.setFkGsrDatMgrId(employeeProfDetailObject
												.getFkEmpReportingMgrId());
								managerId = employeeProfDetailObject
										.getFkEmpReportingMgrId().intValue();
							} else {
								throw new CommonCustomException(
										"Goals have been created by "
												+ reportingMgrName
												+ " for "
												+ totalEnteredWeightage
												+ " %. Hence you are not allowed to create "
												+ "the goals for this quarter.");
							}
						} else {
							inputGsrGoalBO
									.setFkGsrDatMgrId(gsrCreateGoalInputBean
											.getManagerId());
							managerId = gsrCreateGoalInputBean.getManagerId();
						}
					}
				}
			}
		}
		// -------------------------------------------------------------------

		// #2. Fetch a "GsrSlabBO" object
		// Input --> slabId
		GsrSlabBO gsrSlabBOBySlabId = new GsrSlabBO();
		try {
			gsrSlabBOBySlabId = gsrSlabRepository
					.findByPkGsrSlabId(gsrCreateGoalInputBean.getSlabId());
		} catch (Exception ex) {
			throw new CommonCustomException("Unable to get gsr slab details.");
		}

		if (gsrSlabBOBySlabId == null)
			throw new CommonCustomException("GSR quarter "
					+ gsrCreateGoalInputBean.getSlabId() + " does not exist");
		// -------------------------------------------------------------------

		// #3. Fetch a "GsrSlabBO" object
		// Input --> gsr start date and end date
		GsrSlabBO gsrSlabBOByStartAndEndDate = new GsrSlabBO();
		Date startDate = gsrCreateGoalInputBean.getGoalStartDate();
		Date endDate = gsrCreateGoalInputBean.getGoalEndDate();

		if (startDate == null)
			throw new CommonCustomException("Goal start date is mandatory");
		if (endDate == null)
			throw new CommonCustomException("Goal end date is mandatory");

		try {
			gsrSlabBOByStartAndEndDate = gsrSlabRepository
					.getGsrSlabBOByStartAndEndDate(startDate, endDate);
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to fetch slab details from slab start date and end date");
		}

		if (gsrSlabBOByStartAndEndDate == null)
			throw new CommonCustomException(
					"Either goal start date or goal end date is invalid or both are invalid");
		// -------------------------------------------------------------------

		// #4. Fetch list of "GsrKpiWeightageBO" which are associated to input
		// employee id.
		List<GsrKpiWeightageBO> empLevelAndBuGsrKpiWeightageList = new ArrayList<GsrKpiWeightageBO>();
		try {
			empLevelAndBuGsrKpiWeightageList = gsrKpiWeightageRepos
					.getAllKpiGoalsWithWeightageForEmpLevel(employeeId);
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to fetch pre-defined Org-wide goals");
		}
		// -------------------------------------------------------------------
		// Added by Shyam for RM change scenario
		// Change managerId, if otherMgrIdIsPresentIsGoal is true. take the
		// current mgr id.
		if (otherMgrIdIsPresentIsGoal)
			managerId = employeeProfDetailObject.getFkEmpReportingMgrId()
					.intValue();
		// End of addition by Shyam for RM change scenario

		// #5. Fetch list of "GsrDatEmpGoalBO"
		// Input --> employeeId, managerId and slabId
		List<GsrDatEmpGoalBO> allEnteredGoalsOfSlabUnderAManagerList = new ArrayList<GsrDatEmpGoalBO>();
		try {
			allEnteredGoalsOfSlabUnderAManagerList = gsrDatEmpGoalRepository
					.getAllGoalsEnteredByEmpOfASlabUnderAMgr(employeeId,
							gsrCreateGoalInputBean.getSlabId(), managerId);
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to retrieve all entered goal details");
		}

		// -------------------------------------------------------------------

		// #6. Find out the organisation wide goals will consider or not flag.
		// Input --> slabId
		boolean orgwideGoalMandatoryFlag = false;
		GsrAdminConfigurationBO gsrAdminConfigurationBO = new GsrAdminConfigurationBO();
		try {
			gsrAdminConfigurationBO = ConfigRepository
					.findByFkGsrSlabId((int) gsrCreateGoalInputBean.getSlabId());
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to fetch GSR configuration details of a quarter");
		}

		if (gsrAdminConfigurationBO != null) {
			if (gsrAdminConfigurationBO.getEnableOrgWideGoal()
					.equalsIgnoreCase("ENABLED"))
				orgwideGoalMandatoryFlag = true;
		}

		// =========================================================================

		// *1. Slab-id validation with gsr start and end date.
		// All these 3 fields are manadatory for these service.

		if (gsrCreateGoalInputBean.getSlabId() == 0) // byte default value is 0
		{
			throw new CommonCustomException("Provided slab id cannot be zero");
		} else {
			if (gsrSlabBOBySlabId.getPkGsrSlabId() != gsrSlabBOByStartAndEndDate
					.getPkGsrSlabId()) {
				throw new CommonCustomException("Provided goal start date "
						+ gsrCreateGoalInputBean.getGoalStartDate()
						+ " and goal end date "
						+ gsrCreateGoalInputBean.getGoalEndDate()
						+ " doesnot related to slab id "
						+ gsrCreateGoalInputBean.getSlabId() + " ");
			} else {
				inputGsrGoalBO.setFkGsrSlabId(gsrCreateGoalInputBean
						.getSlabId());
				inputGsrGoalBO.setGsrGoalStartDate(gsrCreateGoalInputBean
						.getGoalStartDate());
				inputGsrGoalBO.setGsrGoalEndDate(gsrCreateGoalInputBean
						.getGoalEndDate());
			}
		}

		// ==============================================================================

		// *2. gsrKpiWeightageId validation
		boolean kpiWeightageEnteredFlag = false;
		boolean kpiWeightageIdExistInGsrKpiWeightageBOFlag = false;
		if (orgwideGoalMandatoryFlag) {
			if (gsrCreateGoalInputBean.getGsrKpiWeightageId() > 0) {
				short tempKpiWeightageId = gsrCreateGoalInputBean
						.getGsrKpiWeightageId();
				for (GsrKpiWeightageBO gsrKpiWeightageBO : empLevelAndBuGsrKpiWeightageList) {
					if (gsrKpiWeightageBO.getPkGsrKpiWeightageId() == tempKpiWeightageId) {
						kpiWeightageIdExistInGsrKpiWeightageBOFlag = true;
						break;
					}
				}

				for (GsrDatEmpGoalBO gsrDatEmpGoalBO : allEnteredGoalsOfSlabUnderAManagerList) {
					if (gsrDatEmpGoalBO.getEmpGoalKpiWeightageId() == tempKpiWeightageId) {
						kpiWeightageEnteredFlag = true;
						break;
					}
				}

				if (kpiWeightageIdExistInGsrKpiWeightageBOFlag) {
					if (kpiWeightageEnteredFlag) {
						throw new CommonCustomException("The Org-wide goal "
								+ gsrCreateGoalInputBean.getGoalName()
								+ " has already been created for this quarter");
					} else {
						inputGsrGoalBO
								.setEmpGoalKpiWeightageId(tempKpiWeightageId);
					}
				} else {
					throw new CommonCustomException("Org-wide goal "
							// + gsrCreateGoalInputBean.getGsrKpiWeightageId()
							+ gsrCreateGoalInputBean.getGoalName()
							+ "	does not exists");
				}
			}
		} else {
			if (gsrCreateGoalInputBean.getGsrKpiWeightageId() > 0) {
				throw new CommonCustomException(
						"Org-wide goals cannot be created for this quarter as it is disabled");
			}
		}
		// ===================================================================================

		// *3. goalName validation

		if ((gsrCreateGoalInputBean.getGoalName().replaceAll("\\s", ""))
				.equalsIgnoreCase("")) {
			throw new CommonCustomException("Goal name cannot be empty");
		}

		char goalNameArray[] = gsrCreateGoalInputBean.getGoalName()
				.toCharArray();
		int goalNameLength = goalNameArray.length;

		if (goalNameLength > 100) {
			throw new CommonCustomException(
					"Goal name cannot exceed 100 characters");
		}

		// goalNameFlag needed by both org and project goals (in both
		// orgwideGoalMandatoryFlag scenarios too)
		boolean goalNameFlag = false;
		// Check this goal name is exist or not
		for (GsrDatEmpGoalBO enteredGsrGoalsOfEmp : allEnteredGoalsOfSlabUnderAManagerList) {
			// LOG.info("Existing goal name :: " +
			// enteredGsrGoalsOfEmp.getGsrDatEmpGoalName());
			// LOG.info("Input Goal name :: " +
			// gsrCreateGoalInputBean.getGoalName());
			if (enteredGsrGoalsOfEmp.getGsrDatEmpGoalName().equalsIgnoreCase(
					gsrCreateGoalInputBean.getGoalName())) {
				goalNameFlag = true;
				break;
			}
			// LOG.info("--------------------------");
		}

		if (orgwideGoalMandatoryFlag) {
			if (gsrCreateGoalInputBean.getGsrKpiWeightageId() > 0) {
				boolean kpiGoalNameFlag = false;

				if (goalNameFlag) {
					throw new CommonCustomException("The Org-wide goal "
							+ gsrCreateGoalInputBean.getGoalName()
							+ " has already been created for this quarter");
				} else {
					// Check input kpi goal name should be same as the mentioned
					// in DB.
					for (GsrKpiWeightageBO gsrKpiWeightageBO : empLevelAndBuGsrKpiWeightageList) {
						// LOG.info("KPI goal name --> " +
						// gsrKpiWeightageBO.getGsrKpiGoal().getGsrKpiGoalName());
						if (gsrKpiWeightageBO
								.getGsrKpiGoal()
								.getGsrKpiGoalName()
								.equalsIgnoreCase(
										gsrCreateGoalInputBean.getGoalName())
								&& gsrKpiWeightageBO.getPkGsrKpiWeightageId() == gsrCreateGoalInputBean
										.getGsrKpiWeightageId()) {
							kpiGoalNameFlag = true;
							break;
						}
					}
				}

				if (kpiGoalNameFlag)
					inputGsrGoalBO.setGsrDatEmpGoalName(gsrCreateGoalInputBean
							.getGoalName());
				else
					throw new CommonCustomException("Org-wide goal name "
							+ gsrCreateGoalInputBean.getGoalName()
							+ " doesnot match with the goals defined by HR");
			} else {
				// For project goals
				if (goalNameFlag)
				{
					throw new CommonCustomException("Project goal "
							+ gsrCreateGoalInputBean.getGoalName()
							+ " has already been created for this quarter");
				}else{
					//New Change Request after the feedback
					boolean kpiGoalFlag = false;
					String matchedKpiGoalName = "";
					short kpiGoalWeightageId = 0;
					String goalName = gsrCreateGoalInputBean.getGoalName().trim();
					
					for (GsrKpiWeightageBO kpiGoalWeightageBO : empLevelAndBuGsrKpiWeightageList) 
					{
						if(kpiGoalWeightageBO.getGsrKpiGoal().getGsrKpiGoalName().equalsIgnoreCase(goalName))
						{
							matchedKpiGoalName = kpiGoalWeightageBO.getGsrKpiGoal().getGsrKpiGoalName(); 
							kpiGoalWeightageId = kpiGoalWeightageBO.getPkGsrKpiWeightageId();
							kpiGoalFlag = true;
							break;
						}
					}
					
					if(kpiGoalFlag)
					{
						inputGsrGoalBO.setGsrDatEmpGoalName(matchedKpiGoalName);
						inputGsrGoalBO.setEmpGoalKpiWeightageId(Short.valueOf(kpiGoalWeightageId));
					}else{
						inputGsrGoalBO.setGsrDatEmpGoalName(gsrCreateGoalInputBean.getGoalName());
					}
					//End of Change Request after the feedback
				}
			}
		}// -------------------------
		else {
			// Kpi / org goals are not considered. If kpi goal id is greater
			// than 0, then throw message
			if (gsrCreateGoalInputBean.getGsrKpiWeightageId() > 0) {
				throw new CommonCustomException("Either org-wide goal "
						+ gsrCreateGoalInputBean.getGoalName()
						+ " doesnot exist or it is disabled for this quarter");
			}

			// Check the goal name is match with all entered goals for that
			// slab.
			if (goalNameFlag)
				throw new CommonCustomException("Project goal name "
						+ gsrCreateGoalInputBean.getGoalName()
						+ " is already created for this quarter");
			else
				inputGsrGoalBO.setGsrDatEmpGoalName(gsrCreateGoalInputBean
						.getGoalName());
		}

		// ======================================================================================

		// *4. Goal Weightage Validation
		int totalWeightageForSlab = 0;
		boolean moreThanOneOrgWideGoalMgrFlag = false;
		int tempMgrId = 0;
		// Find all goals under a employee of a slab.
		// Check whether an employee have two mgrs goal in one slab.(included
		// mgr feedback and rated goal status too.)
		List<GsrDatEmpGoalBO> allEnteredGsrGoals = new ArrayList<GsrDatEmpGoalBO>();
		try {
			allEnteredGsrGoals = gsrDatEmpGoalRepository
					.getAllEnteredGsrGoalsOfASlab(
							gsrCreateGoalInputBean.getSlabId(),
							gsrCreateGoalInputBean.getEmployeeId());
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unbale to retrive all entered goals details. Please contact"
							+ " Nucleus Support team for this issue.");
		}

		/*
		 * List<GsrDatEmpGoalBO> employeeGoalList = new
		 * ArrayList<GsrDatEmpGoalBO>(); try{ employeeGoalList =
		 * gsrDatEmpGoalRepository.getAllEnteredGoalsOfASlab(
		 * gsrCreateGoalInputBean.getSlabId(),
		 * gsrCreateGoalInputBean.getEmployeeId()); }catch(Exception ex){ throw
		 * new
		 * CommonCustomException("Unable to retrieve all gaols fo input employee."
		 * ); }
		 */

		for (GsrDatEmpGoalBO gsrDatEmpGoalBO2 : allEnteredGsrGoals) {
			if (tempMgrId == 0) {
				tempMgrId = gsrDatEmpGoalBO2.getFkGsrDatMgrId().intValue();
			} else {
				if (tempMgrId != gsrDatEmpGoalBO2.getFkGsrDatMgrId().intValue()) {
					moreThanOneOrgWideGoalMgrFlag = true;
					break;
				}
			}
		}

		// Calculate total weightage of that slab under input manager id
		for (GsrDatEmpGoalBO empGoalsForWeightage : allEnteredGoalsOfSlabUnderAManagerList) {
			totalWeightageForSlab = totalWeightageForSlab
					+ empGoalsForWeightage.getGsrGoalWeightage();
		}

		// Below code is unused one
		if (totalWeightageForSlab > 100)
			throw new CommonCustomException(
					"It is mandatory for goal weightages to sum up to 100%. "
							+ "Your goals currently sum up to "
							+ totalWeightageForSlab + "% ");

		if (orgwideGoalMandatoryFlag) {
			// Find out the non-enter kpi goals of employee
			List<GsrKpiWeightageBO> nonEnteredKpiGoals = new ArrayList<GsrKpiWeightageBO>();
			// Added all org-wide goals which are associated with input employee
			// id.
			if (!moreThanOneOrgWideGoalMgrFlag) {
				nonEnteredKpiGoals.addAll(empLevelAndBuGsrKpiWeightageList);

				for (GsrDatEmpGoalBO enteredEmpGoals : allEnteredGoalsOfSlabUnderAManagerList) {
					for (GsrKpiWeightageBO gsrKpiWeightage : empLevelAndBuGsrKpiWeightageList) {
						if (enteredEmpGoals.getEmpGoalKpiWeightageId()
								.shortValue() == gsrKpiWeightage
								.getPkGsrKpiWeightageId()) {
							nonEnteredKpiGoals.remove(gsrKpiWeightage);
						}
					}
				}
			}

			if (gsrCreateGoalInputBean.getGsrKpiWeightageId() > 0) {
				if (!kpiWeightageEnteredFlag
						&& kpiWeightageIdExistInGsrKpiWeightageBOFlag) {
					GsrKpiWeightageBO empGsrKpiWeightageBO = new GsrKpiWeightageBO();
					for (GsrKpiWeightageBO gsrKpiWeightageBO : empLevelAndBuGsrKpiWeightageList) {
						if (gsrKpiWeightageBO.getPkGsrKpiWeightageId() == gsrCreateGoalInputBean
								.getGsrKpiWeightageId()) {
							empGsrKpiWeightageBO = gsrKpiWeightageBO;
							break;
						}
					}

					if (gsrCreateGoalInputBean.getGoalWeightage() < empGsrKpiWeightageBO
							.getGsrKpiGoalWeightage()) {
						throw new CommonCustomException(
								"Weightage assigned to org-wide goal "
										+ gsrCreateGoalInputBean.getGoalName()
										+ " can not be less than the weightage pre-defined by your BU for this goal");
					} else /*
							 * if(gsrCreateGoalInputBean.getGoalWeightage() ==
							 * empGsrKpiWeightageBO.getGsrKpiGoalWeightage()) {
							 * totalWeightageForSlab = totalWeightageForSlab +
							 * gsrCreateGoalInputBean.getGoalWeightage();
							 * nonEnteredKpiGoals.remove(empGsrKpiWeightageBO);
							 * }else
							 */{
						totalWeightageForSlab = totalWeightageForSlab
								+ gsrCreateGoalInputBean.getGoalWeightage();
						nonEnteredKpiGoals.remove(empGsrKpiWeightageBO);
					}

					if (!nonEnteredKpiGoals.isEmpty()) {
						for (GsrKpiWeightageBO nonEnteredKpiWeightageBO : nonEnteredKpiGoals) {
							totalWeightageForSlab = totalWeightageForSlab
									+ nonEnteredKpiWeightageBO
											.getGsrKpiGoalWeightage();
						}

						if (totalWeightageForSlab > 100)
							throw new CommonCustomException(
									"It is mandatory for goal weightages to sum up to 100%. "
											+ "After adding unentered org-wide goals weightage, your goals currently sum up to "
											+ totalWeightageForSlab + "% ");
					}

					if (totalWeightageForSlab > 100) {
						throw new CommonCustomException(
								"It is mandatory for goal weightages to sum up to 100%. "
										+ "Your goals currently sum up to "
										+ totalWeightageForSlab + "% ");
					} else {
						// Set gsr goal weighatge
						inputGsrGoalBO
								.setGsrGoalWeightage((byte) gsrCreateGoalInputBean
										.getGoalWeightage());
					}
				}

			}// ------------------------------------------
			else {// For Project Goals
				totalWeightageForSlab = totalWeightageForSlab
						+ gsrCreateGoalInputBean.getGoalWeightage();

				if (totalWeightageForSlab > 100) {
					throw new CommonCustomException(
							"It is mandatory for goal weightages to sum up to 100%. "
									+ "Your goals currently sum up to "
									+ totalWeightageForSlab + "% ");
				}

				if (nonEnteredKpiGoals.isEmpty()) {
					/*
					 * if(totalWeightageForSlab > 100) throw new
					 * CommonCustomException("Goal total weightage " +
					 * totalWeightageForSlab +
					 * " is exceeding the max bound 100 %. "); else
					 */
					inputGsrGoalBO
							.setGsrGoalWeightage((byte) gsrCreateGoalInputBean
									.getGoalWeightage());
				} else {
					for (GsrKpiWeightageBO nonEnteredKpiWeightageBO : nonEnteredKpiGoals) {
						totalWeightageForSlab = totalWeightageForSlab
								+ nonEnteredKpiWeightageBO
										.getGsrKpiGoalWeightage();
					}

					if (totalWeightageForSlab > 100) {
						throw new CommonCustomException(
								"It is mandatory for goal weightages to sum up to 100%. "
										+ "After adding of unentered org-wide goals weightage, your goals currently sum up to "
										+ totalWeightageForSlab + "% ");
					} else {
						inputGsrGoalBO
								.setGsrGoalWeightage((byte) gsrCreateGoalInputBean
										.getGoalWeightage());
					}
				}
			}
		} else {// -----------------------------------
				// When kpi / org goals are not considered. If kpi goal id is
				// greater than 0, then throw message
			if (gsrCreateGoalInputBean.getGsrKpiWeightageId() == 0)
				totalWeightageForSlab = totalWeightageForSlab
						+ gsrCreateGoalInputBean.getGoalWeightage();
			else
				throw new CommonCustomException(
						"Org-wide goals cannot be created for this quarter as it is disabled");

			if (totalWeightageForSlab > 100)
				throw new CommonCustomException(
						"It is mandatory for goal weightages to sum up to 100%. "
								+ "Your goals currently sum up to "
								+ totalWeightageForSlab + "% ");
			else
				inputGsrGoalBO
						.setGsrGoalWeightage((byte) gsrCreateGoalInputBean
								.getGoalWeightage());
		}

		// ===============================================================================================

		// *5. Goal Description Validation
		if ((gsrCreateGoalInputBean.getGoalDescription().replaceAll("\\s", ""))
				.equalsIgnoreCase("")) {
			throw new CommonCustomException("Goal description cannot be empty.");
		} else
			inputGsrGoalBO.setGsrGoalDescription(gsrCreateGoalInputBean
					.getGoalDescription());

		// ==============================================================================================

		// *6. Goal measurement Validation
		if (gsrCreateGoalInputBean.getGoalMeasurement().replaceAll("\\s", "")
				.equalsIgnoreCase(""))
			throw new CommonCustomException("Goal measurement cannot be empty");
		else
			inputGsrGoalBO.setGsrGoalMeasurement(gsrCreateGoalInputBean
					.getGoalMeasurement());

		// ===============================================================================================

		LOG.info("inputGsrGoalBO ::: " + inputGsrGoalBO);
		// Store inputGsrGoalBO to db.
		GsrDatEmpGoalBO storedNewGsrGoalObject = new GsrDatEmpGoalBO();
		try {
			storedNewGsrGoalObject = gsrDatEmpGoalRepository
					.save(inputGsrGoalBO);
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to save the goal. Kindly contact to Nucleus Support team for this issue");
		}

		// =================================================================================================
		// Converting from inputGsrGoalBO to gsrGoalOutputBean
		gsrGoalOutputBean.setGoalId(storedNewGsrGoalObject
				.getPkGsrDatEmpGoalId());
		gsrGoalOutputBean.setGoalName(storedNewGsrGoalObject
				.getGsrDatEmpGoalName());
		gsrGoalOutputBean.setEmployeeId(storedNewGsrGoalObject
				.getFkGsrDatEmpId());
		gsrGoalOutputBean.setReportingMgrId(storedNewGsrGoalObject
				.getFkGsrDatMgrId());
		gsrGoalOutputBean.setGsrStartDate(storedNewGsrGoalObject
				.getGsrGoalStartDate());
		gsrGoalOutputBean.setGsrEndDate(storedNewGsrGoalObject
				.getGsrGoalEndDate());
		gsrGoalOutputBean
				.setGoalSlabId(storedNewGsrGoalObject.getFkGsrSlabId());
		gsrGoalOutputBean.setGoalWeightage(storedNewGsrGoalObject
				.getGsrGoalWeightage());
		gsrGoalOutputBean.setGoalDesciption(storedNewGsrGoalObject
				.getGsrGoalDescription());
		gsrGoalOutputBean.setGoalMeasurement(storedNewGsrGoalObject
				.getGsrGoalMeasurement());
		gsrGoalOutputBean.setGoalStatus(storedNewGsrGoalObject
				.getFkGsrGoalStatus());
		gsrGoalOutputBean.setGoalCreatedBy(storedNewGsrGoalObject
				.getGsrGoalCreatedBy());
		gsrGoalOutputBean.setGoalCreatedDate(storedNewGsrGoalObject
				.getGsrGoalCreatedDate());
		gsrGoalOutputBean.setEmpGoalKpiWeightageId(storedNewGsrGoalObject
				.getEmpGoalKpiWeightageId());
		gsrGoalOutputBean.setFkEmpGoalBuId(storedNewGsrGoalObject
				.getFkEmpGoalBuId());
		gsrGoalOutputBean.setFkEmpGoalDomainMgrId(storedNewGsrGoalObject
				.getFkEmpGoalDomainMgrId());

		return gsrGoalOutputBean;
	}

	// End of addition by Shyam for creation of gsr goal

	public List<GsrTeamGoalStatusBean> goalCurrentYearSetStatusForDomainManager(
			GsrGoalSetStatusBean gsrGoalSetStatusBean)
			throws DataAccessException, CommonCustomException,
			BusinessException {

		List<GsrTeamGoalStatusBean> tempList = new ArrayList<GsrTeamGoalStatusBean>();
		List<GsrTeamGoalStatusBean> outputList = new ArrayList<GsrTeamGoalStatusBean>();
		List<GsrSlabBO> gsrSlabList = gsrSlabRepository
				.findByGsrSlabYearOrderByPkGsrSlabIdAsc(year + "");
		if (gsrSlabList.size() > 0) {
			for (GsrSlabBO gsrSlabBO : gsrSlabList) {
				GsrTeamGoalStatusBean gsrTeamGoalStatusBean = new GsrTeamGoalStatusBean();
				String duration = monthFormat.format(
						gsrSlabBO.getGsrSlabStartDate()).toUpperCase()
						+ "-"
						+ monthFormat.format(gsrSlabBO.getGsrSlabEndDate())
								.toUpperCase();
				gsrTeamGoalStatusBean.setSlabDuration(duration);
				gsrTeamGoalStatusBean.setSlabId(gsrSlabBO.getPkGsrSlabId());
				gsrTeamGoalStatusBean.setSlabShortName(gsrSlabBO
						.getGsrSlabShortName());
				List<DatEmpProfessionalDetailBO> proDetails = new ArrayList<DatEmpProfessionalDetailBO>();
				LocalDate slabDate = LocalDate.parse(simpleDateFormat
						.format(gsrSlabBO.getGsrSlabEndDate()));
				Date slabDateMinus15Days = Date.from(slabDate.minusDays(15)
						.atStartOfDay(ZoneId.systemDefault()).toInstant());
				proDetails = employeeProfessionalRepository
						.getConfirmedReporteesByDMAndDateRange(
								slabDateMinus15Days,
								gsrGoalSetStatusBean.getDomainManagerId());
				Set<Integer> slabReportees = new HashSet<Integer>();
				for (DatEmpProfessionalDetailBO pro : proDetails) {
					slabReportees.add(pro.getFkMainEmpDetailId());
				}
				gsrTeamGoalStatusBean.setTeamCount(slabReportees.size());
				if (gsrSlabBO.getGsrSlabEnabled().equalsIgnoreCase("ENABLED")) {
					List<GsrDatEmpGoalBO> gsrDatEmpGoalBoTempList = createGoalrepository
							.findByFkEmpGoalDomainMgrIdAndFkGsrSlabId(
									gsrGoalSetStatusBean.getDomainManagerId(),
									gsrSlabBO.getPkGsrSlabId());
					if (gsrDatEmpGoalBoTempList.size() > 0) {
						for (GsrDatEmpGoalBO gsrDatEmpGoalBO : gsrDatEmpGoalBoTempList) {
							slabReportees.add(gsrDatEmpGoalBO
									.getFkGsrDatEmpId());
						}
						gsrTeamGoalStatusBean
								.setTeamCount(slabReportees.size());
						Integer localGoalClosedCount = 0;
						Integer localGoalNotClosedCount = 0;
						Integer localGoalSetCount = 0;
						Integer localGoalNotSetCount = 0;
						for (Integer reporteeId : slabReportees) {
							GsrGoalSetStatusBean goalSetStatusBean = this
									.getEmployeeGoalStatus(
											gsrSlabBO.getPkGsrSlabId(),
											reporteeId);
							if (goalSetStatusBean != null) {
								if (goalSetStatusBean.getStatusId().intValue() == this.CONSTANT_GOAL_NOT_SET)
									localGoalNotSetCount++;
								else if (goalSetStatusBean.getStatusId()
										.intValue() == this.CONSTANT_GOAL_SET)
									localGoalSetCount++;
								else if (goalSetStatusBean.getStatusId()
										.intValue() == this.CONSTANT_GOAL_CLOSED)
									localGoalClosedCount++;
								else if (goalSetStatusBean.getStatusId()
										.intValue() == this.CONSTANT_GOAL_NOT_CLOSED)
									localGoalNotClosedCount++;
							}
						}
						gsrTeamGoalStatusBean
								.setStatusGoalClosedCount(localGoalClosedCount);
						gsrTeamGoalStatusBean
								.setStatusNotGoalClosedCount(localGoalNotClosedCount);
						gsrTeamGoalStatusBean
								.setStatusGoalSetCount(localGoalSetCount);
						gsrTeamGoalStatusBean
								.setStatusNotGoalSetCount(localGoalNotSetCount);
						if (localGoalNotClosedCount >= localGoalClosedCount
								&& localGoalNotClosedCount >= localGoalSetCount
								&& localGoalNotClosedCount >= localGoalNotSetCount) {
							gsrTeamGoalStatusBean.setStatus("GOAL NOT CLOSED");
							gsrTeamGoalStatusBean.setStatusId(3);
						} else if (localGoalClosedCount >= localGoalNotClosedCount
								&& localGoalClosedCount >= localGoalSetCount
								&& localGoalClosedCount >= localGoalNotSetCount) {
							gsrTeamGoalStatusBean.setStatus("GOAL CLOSED");
							gsrTeamGoalStatusBean.setStatusId(4);
						} else if (localGoalSetCount >= localGoalNotClosedCount
								&& localGoalSetCount >= localGoalClosedCount
								&& localGoalSetCount >= localGoalNotSetCount) {
							gsrTeamGoalStatusBean.setStatus("GOAL SET");
							gsrTeamGoalStatusBean.setStatusId(2);
						} else if (localGoalNotSetCount >= localGoalNotClosedCount
								&& localGoalNotSetCount >= localGoalClosedCount
								&& localGoalNotSetCount >= localGoalSetCount) {
							gsrTeamGoalStatusBean.setStatus("GOAL NOT SET");
							gsrTeamGoalStatusBean.setStatusId(1);
						}
					} else {
						gsrTeamGoalStatusBean.setStatus("GOAL NOT SET");
						gsrTeamGoalStatusBean.setStatusId(1);
						gsrTeamGoalStatusBean
								.setStatusNotGoalSetCount(slabReportees.size());
						gsrTeamGoalStatusBean.setStatusGoalClosedCount(0);
						gsrTeamGoalStatusBean.setStatusNotGoalClosedCount(0);
						gsrTeamGoalStatusBean.setStatusGoalSetCount(0);
					}
					if (gsrTeamGoalStatusBean.getTeamCount().intValue() == 0) {
						gsrTeamGoalStatusBean.setStatus("NO_REPORTEE");
						gsrTeamGoalStatusBean
								.setStatusId(CONSTANT_GOAL_NO_REPORTEE);
						gsrTeamGoalStatusBean.setStatusGoalClosedCount(0);
						gsrTeamGoalStatusBean.setTeamCount(0);
						gsrTeamGoalStatusBean.setStatusNotGoalClosedCount(0);
						gsrTeamGoalStatusBean.setStatusGoalSetCount(0);
					}
				} else {
					gsrTeamGoalStatusBean.setStatus("PENDING");
					gsrTeamGoalStatusBean.setStatusId(0);
					gsrTeamGoalStatusBean
							.setStatusNotGoalSetCount(slabReportees.size());
					gsrTeamGoalStatusBean.setStatusGoalClosedCount(0);
					gsrTeamGoalStatusBean.setStatusNotGoalClosedCount(0);
					gsrTeamGoalStatusBean.setStatusGoalSetCount(0);
				}
				
				outputList.add(gsrTeamGoalStatusBean);
			}
		} else {
			throw new CommonCustomException(
					"unable to retrieve the current year slabs. Please contact to administrator");
		}
		return outputList;
	}

	Integer reportingManagerId = 0;

	public List<GsrTeamGoalStatusBean> goalCurrentYearSetStatusForRM(
			GsrGoalSetStatusBean gsrGoalSetStatusBean)
			throws DataAccessException, CommonCustomException,
			BusinessException {
		List<GsrTeamGoalStatusBean> tempList = new ArrayList<GsrTeamGoalStatusBean>();
		List<GsrTeamGoalStatusBean> outputList = new ArrayList<GsrTeamGoalStatusBean>();
		if (gsrGoalSetStatusBean.getReportingManagerId().size() < 1) {
			throw new CommonCustomException(
					"Manager Id is should not to be empty");
		}
		if (gsrGoalSetStatusBean.getReportingManagerId().size() > 1) {
			throw new CommonCustomException(
					"Multiple manager id is not allowed here");
		} else {
			gsrGoalSetStatusBean.getReportingManagerId().forEach(rmId -> {
				this.reportingManagerId = rmId;
			});
			if (this.reportingManagerId != 0) {
				Set<Integer> allReportees = new HashSet<Integer>();
				List<GsrSlabBO> gsrSlabList = gsrSlabRepository
						.findByGsrSlabYearOrderByPkGsrSlabIdAsc(year + "");
				for (GsrSlabBO gsrSlabBO : gsrSlabList) {
					GsrTeamGoalStatusBean gsrTeamGoalStatusBean = new GsrTeamGoalStatusBean();
					String duration = monthFormat.format(
							gsrSlabBO.getGsrSlabStartDate()).toUpperCase()
							+ "-"
							+ monthFormat.format(gsrSlabBO.getGsrSlabEndDate())
									.toUpperCase();

					gsrTeamGoalStatusBean.setSlabDuration(duration);
					gsrTeamGoalStatusBean.setSlabId(gsrSlabBO.getPkGsrSlabId());
					gsrTeamGoalStatusBean.setSlabShortName(gsrSlabBO
							.getGsrSlabShortName());
					List<DatEmpProfessionalDetailBO> proDetails = new ArrayList<DatEmpProfessionalDetailBO>();
					LocalDate slabDate = LocalDate.parse(simpleDateFormat
							.format(gsrSlabBO.getGsrSlabEndDate()));
					Date slabDateMinus15Days = Date.from(slabDate.minusDays(15)
							.atStartOfDay(ZoneId.systemDefault()).toInstant());
					proDetails = employeeProfessionalRepository
							.getConfirmedReporteesByRmAndDateRange(
									slabDateMinus15Days,
									this.reportingManagerId);
					Set<Integer> slabReportees = new HashSet<Integer>();
					for (DatEmpProfessionalDetailBO pro : proDetails) {
						slabReportees.add(pro.getFkMainEmpDetailId());
					}

					gsrTeamGoalStatusBean.setTeamCount(slabReportees.size());
					if (gsrSlabBO.getGsrSlabEnabled().equalsIgnoreCase(
							"ENABLED")) {
						List<GsrDatEmpGoalBO> gsrDatEmpGoalBoTempList = createGoalrepository
								.findByFkGsrDatMgrIdAndFkGsrSlabId(
										this.reportingManagerId,
										gsrSlabBO.getPkGsrSlabId());
						if (gsrDatEmpGoalBoTempList.size() > 0) {
							for (GsrDatEmpGoalBO gsrDatEmpGoalBO : gsrDatEmpGoalBoTempList) {
								slabReportees.add(gsrDatEmpGoalBO
										.getFkGsrDatEmpId());
							}
							gsrTeamGoalStatusBean.setTeamCount(slabReportees
									.size());
							Integer localGoalClosedCount = 0;
							Integer localGoalNotClosedCount = 0;
							Integer localGoalSetCount = 0;
							Integer localGoalNotSetCount = 0;
							for (Integer reporteeId : slabReportees) {
								GsrGoalSetStatusBean goalSetStatusBean = this
										.getEmployeeGoalStatus(
												gsrSlabBO.getPkGsrSlabId(),
												reporteeId);
								if (goalSetStatusBean != null) {
									if (goalSetStatusBean.getStatusId()
											.intValue() == this.CONSTANT_GOAL_NOT_SET)
										localGoalNotSetCount++;
									else if (goalSetStatusBean.getStatusId()
											.intValue() == this.CONSTANT_GOAL_SET)
										localGoalSetCount++;
									else if (goalSetStatusBean.getStatusId()
											.intValue() == this.CONSTANT_GOAL_CLOSED)
										localGoalClosedCount++;
									else if (goalSetStatusBean.getStatusId()
											.intValue() == this.CONSTANT_GOAL_NOT_CLOSED)
										localGoalNotClosedCount++;
								}
							}
							gsrTeamGoalStatusBean
									.setStatusGoalClosedCount(localGoalClosedCount);
							gsrTeamGoalStatusBean
									.setStatusNotGoalClosedCount(localGoalNotClosedCount);
							gsrTeamGoalStatusBean
									.setStatusGoalSetCount(localGoalSetCount);
							gsrTeamGoalStatusBean
									.setStatusNotGoalSetCount(localGoalNotSetCount);
							if (localGoalNotClosedCount != 0
									&& localGoalNotClosedCount >= localGoalClosedCount
									&& localGoalNotClosedCount >= localGoalSetCount
									&& localGoalNotClosedCount >= localGoalNotSetCount) {
								gsrTeamGoalStatusBean
										.setStatus("GOAL NOT CLOSED");
								gsrTeamGoalStatusBean.setStatusId(3);
							} else if (localGoalClosedCount != 0
									&& localGoalClosedCount >= localGoalNotClosedCount
									&& localGoalClosedCount >= localGoalSetCount
									&& localGoalClosedCount >= localGoalNotSetCount) {
								gsrTeamGoalStatusBean.setStatus("GOAL CLOSED");
								gsrTeamGoalStatusBean.setStatusId(4);
							} else if (localGoalSetCount != 0
									&& localGoalSetCount >= localGoalNotClosedCount
									&& localGoalSetCount >= localGoalClosedCount
									&& localGoalSetCount >= localGoalNotSetCount) {
								gsrTeamGoalStatusBean.setStatus("GOAL SET");
								gsrTeamGoalStatusBean.setStatusId(2);
							} else if (localGoalNotSetCount != 0
									&& localGoalNotSetCount >= localGoalNotClosedCount
									&& localGoalNotSetCount >= localGoalClosedCount
									&& localGoalNotSetCount >= localGoalSetCount) {
								gsrTeamGoalStatusBean.setStatus("GOAL NOT SET");
								gsrTeamGoalStatusBean.setStatusId(1);
							}
						} else {
							gsrTeamGoalStatusBean.setStatus("GOAL NOT SET");
							gsrTeamGoalStatusBean.setStatusId(1);
							gsrTeamGoalStatusBean
									.setStatusNotGoalSetCount(slabReportees
											.size());
							gsrTeamGoalStatusBean.setStatusGoalClosedCount(0);
							gsrTeamGoalStatusBean
									.setStatusNotGoalClosedCount(0);
							gsrTeamGoalStatusBean.setStatusGoalSetCount(0);
						}
						if (gsrTeamGoalStatusBean.getTeamCount().intValue() == 0) {
							gsrTeamGoalStatusBean.setStatus("NO_REPORTEE");
							gsrTeamGoalStatusBean
									.setStatusId(CONSTANT_GOAL_NO_REPORTEE);
							gsrTeamGoalStatusBean.setStatusGoalClosedCount(0);
							gsrTeamGoalStatusBean.setTeamCount(0);
							gsrTeamGoalStatusBean.setStatusNotGoalClosedCount(0);
							gsrTeamGoalStatusBean.setStatusGoalSetCount(0);
						}
					} else {
						gsrTeamGoalStatusBean.setStatus("PENDING");
						gsrTeamGoalStatusBean.setStatusId(0);
						gsrTeamGoalStatusBean
								.setStatusNotGoalSetCount(slabReportees.size());
						gsrTeamGoalStatusBean.setStatusGoalClosedCount(0);
						gsrTeamGoalStatusBean.setStatusNotGoalClosedCount(0);
						gsrTeamGoalStatusBean.setStatusGoalSetCount(0);
					}
					
					outputList.add(gsrTeamGoalStatusBean);
				}
			}
		}
		return outputList;
	}

	// Added by Shyam for updation of GSR goal
	/**
	 * @method updateGsrGoal
	 * @param GsrUpdateGoalInputBean
	 * @return GsrGoalOutputBean
	 * @throws CommonCustomException
	 *             , DataAccessException
	 * @Description : This method is used to update input gsr goal into DB(used
	 *              by employee and manager too).
	 */
	public GsrGoalOutputBean updateGsrGoal(
			GsrUpdateGoalInputBean gsrUpdateGoalInputBean)
			throws CommonCustomException, DataAccessException {
		GsrDatEmpGoalBO inputGsrDatEmpGoalBO = new GsrDatEmpGoalBO();
		GsrDatEmpGoalBO outputGsrDatEmpGoalBO = new GsrDatEmpGoalBO();
		GsrGoalOutputBean gsrGoalOutputBean = new GsrGoalOutputBean();
		boolean otherMgrIdIsPresentIsGoal = false;
		// 1.Check the status of goal
		GsrDatEmpGoalBO gsrDatEmpGoalBO = new GsrDatEmpGoalBO();
		try {
			gsrDatEmpGoalBO = gsrDatEmpGoalRepository
					.findByPkGsrDatEmpGoalId(gsrUpdateGoalInputBean.getGoalId());
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to retrieve entered goal details.");
		}

		// LOG.info("gsrDatEmpGoalBO :: " + gsrDatEmpGoalBO);
		if (gsrDatEmpGoalBO != null) {

			// Added by Shyam for RM change scenario
			// Check in a slab whether employee have two different manager id.
			List<GsrDatEmpGoalBO> empGoalList = new ArrayList<GsrDatEmpGoalBO>();
			// LOG.info("Slab id :: " + gsrDatEmpGoalBO.getGsrSlabId());
			// LOG.info("Employee id :: " +
			// gsrDatEmpGoalBO.getFkGsrDatEmpId().intValue());
			try {
				empGoalList = gsrDatEmpGoalRepository
						.getAllEnteredGsrGoalsOfASlab(gsrDatEmpGoalBO
								.getFkGsrSlabId().shortValue(), gsrDatEmpGoalBO
								.getFkGsrDatEmpId().intValue());
			} catch (Exception ex) {
				throw new CommonCustomException(
						"Unable to get all entered goals detail. Kindly touch with "
								+ "Nucleus Support team for this issue.");
			}
			// LOG.info("empGoalList :: " + empGoalList);

			int tempGoalMgrId = 0;
			List<GsrDatEmpGoalBO> otherMgrGoalDetails = new ArrayList<GsrDatEmpGoalBO>();
			if (!empGoalList.isEmpty()) {
				for (GsrDatEmpGoalBO gsrDatEmpGoalBO4 : empGoalList) {
					// LOG.info("Goal Mgr ID ::: " +
					// gsrDatEmpGoalBO4.getFkGsrDatMgrId().intValue());
					if (tempGoalMgrId == 0) {
						tempGoalMgrId = gsrDatEmpGoalBO4.getFkGsrDatMgrId()
								.intValue();
						otherMgrGoalDetails.add(gsrDatEmpGoalBO4);
					} else {
						if (tempGoalMgrId != gsrDatEmpGoalBO4
								.getFkGsrDatMgrId().intValue()) {
							otherMgrIdIsPresentIsGoal = true;
							break;
						} else {
							otherMgrGoalDetails.add(gsrDatEmpGoalBO4);
						}
					}
				}
			}
			// LOG.info("tempGoalMgrId ::: " + tempGoalMgrId);
			// End of addition by Shyam for RM change scenario

			// Added condition for Domain mgr, Bu Head, HR, Skip level and
			// reporting mgr try to update goal.
			DatEmpProfessionalDetailBO datEmpProfDetailBO = new DatEmpProfessionalDetailBO();
			DatEmpProfessionalDetailBO loggedInEmpObject = new DatEmpProfessionalDetailBO();
			try {
				datEmpProfDetailBO = employeeProfessionalRepository
						.findByFkMainEmpDetailId(gsrDatEmpGoalBO
								.getFkGsrDatEmpId().intValue());
				loggedInEmpObject = employeeProfessionalRepository
						.findByFkMainEmpDetailId(gsrUpdateGoalInputBean
								.getLoggedInEmployee());
			} catch (Exception ex) {
				throw new CommonCustomException(
						"Unbale to retrieve employee profeesional details. Kindly contact to Nucleus Support team.");
			}

			if (loggedInEmpObject == null) {
				throw new CommonCustomException(
						"Invalid employee logged-in id. kindly contact Nuclues Support team for this issue.");
			}
			String reportingMgrName = getEmpNameByEmpId(gsrDatEmpGoalBO
					.getFkGsrDatMgrId());

			// Employee self logged in
			if (gsrUpdateGoalInputBean.getLoggedInEmployee() == datEmpProfDetailBO
					.getFkMainEmpDetailId().intValue()) {
				if (gsrDatEmpGoalBO.getFkGsrGoalStatus().byteValue() == goalWaitingForManagerApprovalStatus) {
					inputGsrDatEmpGoalBO.setFkGsrDatMgrId(gsrDatEmpGoalBO
							.getFkGsrDatMgrId().intValue());
				} else {
					throw new CommonCustomException(
							"This goal has crossed 'Waiting for Manager Approval' status, hence it is restricting"
									+ " you to do update.");
				}
			}// For others logged in (Reporting Mgr, Bu Head, Skip level, Domain
				// Mgr, HR)
			else {
				// When reporting manager logged in
				if (gsrUpdateGoalInputBean.getLoggedInEmployee() == datEmpProfDetailBO
						.getFkEmpReportingMgrId().intValue()) {
					if (gsrDatEmpGoalBO.getFkGsrGoalStatus().byteValue() == goalWaitingForManagerApprovalStatus
							|| gsrDatEmpGoalBO.getFkGsrGoalStatus().byteValue() == goalGoalSetStatus) {
						inputGsrDatEmpGoalBO
								.setFkGsrDatMgrId(datEmpProfDetailBO
										.getFkEmpReportingMgrId());
						/*
						 * }else{ throw new
						 * CommonCustomException("Please contact " +
						 * reportingMgrName + " for updation of this goal."); }
						 */
					} else {
						throw new CommonCustomException(
								"This goal has crossed 'Goal Set' status, hence it is restricting you to do update.");
					}
				} else {
					// Other manager logged in
					if (gsrDatEmpGoalBO.getFkGsrGoalStatus().byteValue() == goalWaitingForManagerApprovalStatus
							|| gsrDatEmpGoalBO.getFkGsrGoalStatus().byteValue() == goalGoalSetStatus) {
						if (gsrDatEmpGoalBO.getFkGsrDatMgrId().intValue() == gsrUpdateGoalInputBean
								.getLoggedInEmployee()) {
							inputGsrDatEmpGoalBO
									.setFkGsrDatMgrId(gsrDatEmpGoalBO
											.getFkGsrDatMgrId());
						} else {
							if (gsrDatEmpGoalBO.getFkGsrDatMgrId().intValue() == datEmpProfDetailBO
									.getFkEmpReportingMgrId().intValue()) {
								inputGsrDatEmpGoalBO
										.setFkGsrDatMgrId(datEmpProfDetailBO
												.getFkEmpReportingMgrId());
							} else {
								throw new CommonCustomException(
										"Please contact " + reportingMgrName
												+ " for updation of this goal.");
							}
						}
					} else {
						throw new CommonCustomException(
								"This goal has crossed 'Goal Set' status, hence it is restricting you to do update.");
					}
				}
			}

			// Default addition of other properties of GsrDatEmpGoalBO
			inputGsrDatEmpGoalBO.setPkGsrDatEmpGoalId(gsrUpdateGoalInputBean
					.getGoalId());
			inputGsrDatEmpGoalBO.setFkGsrDatEmpId(gsrDatEmpGoalBO
					.getFkGsrDatEmpId());
			// inputGsrDatEmpGoalBO.setFkGsrDatMgrId(gsrDatEmpGoalBO.getFkGsrDatMgrId());
			inputGsrDatEmpGoalBO.setFkGsrGoalStatus(gsrDatEmpGoalBO
					.getFkGsrGoalStatus());
			inputGsrDatEmpGoalBO.setFkGsrSlabId(gsrDatEmpGoalBO
					.getFkGsrSlabId());
			inputGsrDatEmpGoalBO.setGsrDatEmpGoalName(gsrDatEmpGoalBO
					.getGsrDatEmpGoalName());
			inputGsrDatEmpGoalBO.setGsrGoalCreatedBy(gsrDatEmpGoalBO
					.getGsrGoalCreatedBy());
			inputGsrDatEmpGoalBO.setGsrGoalCreatedDate(gsrDatEmpGoalBO
					.getGsrGoalCreatedDate());
			inputGsrDatEmpGoalBO.setGsrGoalEndDate(gsrDatEmpGoalBO
					.getGsrGoalEndDate());
			inputGsrDatEmpGoalBO.setGsrGoalStartDate(gsrDatEmpGoalBO
					.getGsrGoalStartDate());
			inputGsrDatEmpGoalBO.setFkEmpGoalDomainMgrId(gsrDatEmpGoalBO
					.getFkEmpGoalDomainMgrId());
			inputGsrDatEmpGoalBO.setFkEmpGoalBuId(gsrDatEmpGoalBO
					.getFkEmpGoalBuId());
			inputGsrDatEmpGoalBO.setGsrGoalModifiedDate(new Date());
			inputGsrDatEmpGoalBO.setGsrGoalModifiedBy(gsrUpdateGoalInputBean
					.getLoggedInEmployee());
			if (gsrDatEmpGoalBO.getEmpGoalKpiWeightageId().shortValue() > 0) {
				inputGsrDatEmpGoalBO.setEmpGoalKpiWeightageId(gsrDatEmpGoalBO
						.getEmpGoalKpiWeightageId());
			}
		} else {
			throw new CommonCustomException(
					"This goal "
							+ gsrUpdateGoalInputBean.getGoalId()
							+ " doesnot exist in Nucleus. Kindly contact Nucleus Support team for this issue.");
		}

		// Issue no #43
		GsrAdminConfigurationBO gsrAdminConfigurationBO = new GsrAdminConfigurationBO();
		try {
			gsrAdminConfigurationBO = ConfigRepository
					.findByFkGsrSlabId(gsrDatEmpGoalBO.getFkGsrSlabId()
							.intValue());
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to fetch Gsr Configuration details of a quarter "
							+ gsrDatEmpGoalBO.getFkGsrSlabId() + " . ");
		}

		// Get all goals entered for a slab
		List<GsrDatEmpGoalBO> allGoalsEnteredForASlabUnderAMgrList = new ArrayList<GsrDatEmpGoalBO>();
		try {
			allGoalsEnteredForASlabUnderAMgrList = gsrDatEmpGoalRepository
					.getAllGoalsEnteredByEmpOfASlabUnderAMgr(
							gsrDatEmpGoalBO.getFkGsrDatEmpId(),
							gsrDatEmpGoalBO.getFkGsrSlabId(),
							gsrDatEmpGoalBO.getFkGsrDatMgrId());
		} catch (Exception ex) {
			throw new CommonCustomException(
					"Unable to retrieve all entered goals of a quarter.");
		}
		// LOG.info("allGoalsEnteredForASlabUnderAMgrList :: " +
		// allGoalsEnteredForASlabUnderAMgrList);

		List<GsrKpiWeightageBO> nonEnteredKpiGoals = new ArrayList<GsrKpiWeightageBO>();
		// Get all organisation goals mapped to emp level and bu id.
		List<GsrKpiWeightageBO> empLevelIdAndBuIdGsrKpiWeightageList = new ArrayList<GsrKpiWeightageBO>();

		boolean moreThanOneOrgWideGoalMgrFlag = false;
		int tempMgrId1 = 0;
		if (gsrAdminConfigurationBO.getEnableOrgWideGoal().equalsIgnoreCase(
				"ENABLED")) {
			try {
				empLevelIdAndBuIdGsrKpiWeightageList = gsrKpiWeightageRepos
						.getAllKpiGoalsWithWeightageForEmpLevel(gsrDatEmpGoalBO
								.getFkGsrDatEmpId().intValue());
			} catch (Exception ex) {
				throw new CommonCustomException(
						"Unable to fetch all Org-wide goals w.r.t employee level and business unit.");
			}
			// LOG.info("empLevelIdAndBuIdGsrKpiWeightageList :: " +
			// empLevelIdAndBuIdGsrKpiWeightageList);

			if (!empLevelIdAndBuIdGsrKpiWeightageList.isEmpty()) {
				nonEnteredKpiGoals.addAll(empLevelIdAndBuIdGsrKpiWeightageList);

				for (GsrDatEmpGoalBO eachGoalDetail : allGoalsEnteredForASlabUnderAMgrList) {
					for (GsrKpiWeightageBO gsrKpiWeightageBO : empLevelIdAndBuIdGsrKpiWeightageList) {
						if (eachGoalDetail.getEmpGoalKpiWeightageId()
								.shortValue() == gsrKpiWeightageBO
								.getPkGsrKpiWeightageId()) {
							nonEnteredKpiGoals.remove(gsrKpiWeightageBO);
						}
					}
				}
				// LOG.info("After nonEnteredKpiGoals :: " +
				// nonEnteredKpiGoals);
			} else {
				throw new CommonCustomException(
						"Org-wide goals have not set for employee "
								+ inputGsrDatEmpGoalBO.getFkGsrDatEmpId()
								+ " .");
			}

			// Scenario : After RM change, it should not check for KPI goals
			// weightage

			// Find all goals under a employee of a slab.
			List<GsrDatEmpGoalBO> employeeGoalList = new ArrayList<GsrDatEmpGoalBO>();
			try {
				employeeGoalList = gsrDatEmpGoalRepository
						.getAllEnteredGoalsOfASlab(gsrDatEmpGoalBO
								.getGsrSlabId(), gsrDatEmpGoalBO
								.getFkGsrDatEmpId().intValue());
			} catch (Exception ex) {
				throw new CommonCustomException(
						"Unable to retrieve all gaols fo input employee.");
			}

			for (GsrDatEmpGoalBO gsrDatEmpGoalBO3 : employeeGoalList) {
				if (tempMgrId1 == 0) {
					tempMgrId1 = gsrDatEmpGoalBO3.getFkGsrDatMgrId().intValue();
				} else {
					if (tempMgrId1 != gsrDatEmpGoalBO3.getFkGsrDatMgrId()
							.intValue()) {
						moreThanOneOrgWideGoalMgrFlag = true;
						break;
					}
				}
			}
		}

		// LOG.info("moreThanOneOrgWideGoalMgrFlag :: " +
		// moreThanOneOrgWideGoalMgrFlag);
		// LOG.info("otherMgrIdIsPresentIsGoal :: " +
		// otherMgrIdIsPresentIsGoal);
		if (moreThanOneOrgWideGoalMgrFlag || otherMgrIdIsPresentIsGoal) {
			nonEnteredKpiGoals.clear();
		}
		// LOG.info("nonEnteredKpiGoals  11111111111 ::: " +
		// nonEnteredKpiGoals);
		// End of scenario by Shyam

		int totalWeightageOfAllGoalsUnderAMgr = 0;
		if (gsrUpdateGoalInputBean.getGoalWeightage() != gsrDatEmpGoalBO
				.getGsrGoalWeightage().byteValue()) {
			int diffInWeightage = gsrUpdateGoalInputBean.getGoalWeightage()
					- gsrDatEmpGoalBO.getGsrGoalWeightage().byteValue();
			// LOG.info("diffInWeightage ::: " + diffInWeightage);

			// For organisation goal
			if (gsrDatEmpGoalBO.getEmpGoalKpiWeightageId().shortValue() > 0) {
				// Check for minimum kpi goal weightage.
				GsrKpiWeightageBO kpiWeightageDetail = new GsrKpiWeightageBO();
				try {
					kpiWeightageDetail = gsrKpiWeightageRepos
							.findByPkGsrKpiWeightageId(gsrDatEmpGoalBO
									.getEmpGoalKpiWeightageId());
				} catch (Exception ex) {
					throw new CommonCustomException(
							"Unable to fetch Org-wide goals.");
				}
				// LOG.info("kpiWeightageDetail :: " + kpiWeightageDetail);

				if (kpiWeightageDetail != null) {
					if (kpiWeightageDetail.getGsrKpiGoalWeightage() <= gsrUpdateGoalInputBean
							.getGoalWeightage()) {
						totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
								+ diffInWeightage;
					} else {
						throw new CommonCustomException(
								"Weightage assigned to org-wide goal "
										+ kpiWeightageDetail.getGsrKpiGoal()
												.getGsrKpiGoalName()
										+ " can not be less than the weightage pre-defined by your BU for this goal.");
					}
				} else {
					throw new CommonCustomException(
							"Org-wide goal does not exist.");
				}

				// Find out the total weightage of entered goal
				for (GsrDatEmpGoalBO eachGoalOfEmp : allGoalsEnteredForASlabUnderAMgrList) {
					totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
							+ eachGoalOfEmp.getGsrGoalWeightage().byteValue();
				}
				// LOG.info("In KPI, Entered Total Goal Weightage :: " +
				// totalWeightageOfAllGoalsUnderAMgr);
				// LOG.info("nonEnteredKpiGoals  22222 ::: " +
				// nonEnteredKpiGoals);
				// End of scenario by Shyam
				if (!nonEnteredKpiGoals.isEmpty()) {
					for (GsrKpiWeightageBO nonEnteredKpiGoal : nonEnteredKpiGoals) {
						totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
								+ nonEnteredKpiGoal.getGsrKpiGoalWeightage();
					}
					// LOG.info("After addition of non-entered kpi goals weightage, Total Goal Weightage :: "
					// + totalWeightageOfAllGoalsUnderAMgr);
				}

				// 100 % weightage check
				if (totalWeightageOfAllGoalsUnderAMgr <= 100) {
					// inputGsrDatEmpGoalBO.setEmpGoalKpiWeightageId(gsrDatEmpGoalBO.getEmpGoalKpiWeightageId());
					inputGsrDatEmpGoalBO
							.setGsrGoalWeightage(Byte
									.valueOf(gsrUpdateGoalInputBean
											.getGoalWeightage()));
				} else {
					/*
					 * throw new CommonCustomException(
					 * "After updation of organisation goal weightage " +
					 * gsrUpdateGoalInputBean.getGoalWeightage()+
					 * " , total weightage is exceeding 100%.");
					 */
					throw new CommonCustomException(
							"It is mandatory for goal weightages to sum up to 100%. "
									+ "Your goals currently sum up to "
									+ totalWeightageOfAllGoalsUnderAMgr + " .");
				}
			}// For project goal
			else {
				// Check for organisation goal is enabled or disabled for the
				// input goal slab
				if (gsrAdminConfigurationBO.getEnableOrgWideGoal()
						.equalsIgnoreCase("ENABLED")) {
					// Find out the total weightage of entered goal
					for (GsrDatEmpGoalBO eachGoalOfEmp : allGoalsEnteredForASlabUnderAMgrList) {
						totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
								+ eachGoalOfEmp.getGsrGoalWeightage()
										.byteValue();
					}
					// LOG.info("In project goal (when org wide goal is Enabled), Entered Total Goal Weightage :: "
					// + totalWeightageOfAllGoalsUnderAMgr);
					// LOG.info("nonEnteredKpiGoals  33333 ::: " +
					// nonEnteredKpiGoals);
					// End of scenario by Shyam
					// Add non-entered kpi goal weightage to total
					if (!nonEnteredKpiGoals.isEmpty()) {
						for (GsrKpiWeightageBO nonEnteredKpiGoal : nonEnteredKpiGoals) {
							totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
									+ nonEnteredKpiGoal
											.getGsrKpiGoalWeightage();
						}
						// LOG.info("After addition of non-entered kpi goals weightage in project goal , Total Goal Weightage :: "
						// + totalWeightageOfAllGoalsUnderAMgr);
					}

					totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
							+ diffInWeightage;
					// LOG.info("After addition of non-entered kpi goals weightage in project goal -1 , Total Goal Weightage :: "
					// + totalWeightageOfAllGoalsUnderAMgr);

					if (totalWeightageOfAllGoalsUnderAMgr > 100) {
						throw new CommonCustomException(
								"It is mandatory for goal weightages to sum up to 100%. "
										+ "Your goals currently sum up to "
										+ totalWeightageOfAllGoalsUnderAMgr
										+ " .");
					} else {
						// inputGsrDatEmpGoalBO.setEmpGoalKpiWeightageId(gsrDatEmpGoalBO.getEmpGoalKpiWeightageId());
						// LOG.info("gsrUpdateGoalInputBean.getGoalWeightage() ::: "
						// + gsrUpdateGoalInputBean.getGoalWeightage());
						inputGsrDatEmpGoalBO
								.setGsrGoalWeightage(gsrUpdateGoalInputBean
										.getGoalWeightage());
					}
				}// When input goal is project and org wide goal is not
					// considered in that slab
				else {

					// Find out the total weightage of entered goal
					for (GsrDatEmpGoalBO eachGoalOfEmp : allGoalsEnteredForASlabUnderAMgrList) {
						if (eachGoalOfEmp.getEmpGoalKpiWeightageId()
								.shortValue() > 0) {
							throw new CommonCustomException(
									"Org-wide goals are disabled by HR for this quarter.");
						} else {
							totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
									+ eachGoalOfEmp.getGsrGoalWeightage()
											.byteValue();
						}
					}
					// LOG.info("In project goal, Entered Total Goal Weightage :: "
					// + totalWeightageOfAllGoalsUnderAMgr);

					totalWeightageOfAllGoalsUnderAMgr = totalWeightageOfAllGoalsUnderAMgr
							+ diffInWeightage;
					// LOG.info("In project goal, Entered Total Goal Weightage - 1 :: "
					// + totalWeightageOfAllGoalsUnderAMgr);
					if (totalWeightageOfAllGoalsUnderAMgr > 100) {
						/*
						 * throw new CommonCustomException(
						 * "After adding of updated project goal weightage " +
						 * gsrUpdateGoalInputBean.getGoalWeightage()+
						 * " , total weightage is exceeding 100%.");
						 */
						throw new CommonCustomException(
								"It is mandatory for goal weightages to sum up to 100%. "
										+ "Your goals currently sum up to "
										+ totalWeightageOfAllGoalsUnderAMgr
										+ " .");
					} else {
						// inputGsrDatEmpGoalBO.setEmpGoalKpiWeightageId(gsrDatEmpGoalBO.getEmpGoalKpiWeightageId());
						inputGsrDatEmpGoalBO
								.setGsrGoalWeightage(gsrUpdateGoalInputBean
										.getGoalWeightage());
					}
				}
			}// End of project goal
		} else {
			inputGsrDatEmpGoalBO.setGsrGoalWeightage(gsrUpdateGoalInputBean
					.getGoalWeightage());
		}

		if (gsrUpdateGoalInputBean.getGoalDescription() != null
				&& !gsrUpdateGoalInputBean.getGoalDescription().isEmpty()) {
			inputGsrDatEmpGoalBO.setGsrGoalDescription(gsrUpdateGoalInputBean
					.getGoalDescription());
		} else {
			throw new CommonCustomException(
					"Goal description cannot be empty or blank.");
		}

		if (gsrUpdateGoalInputBean.getGoalMeasurement() != null
				&& !gsrUpdateGoalInputBean.getGoalMeasurement().isEmpty()) {
			inputGsrDatEmpGoalBO.setGsrGoalMeasurement(gsrUpdateGoalInputBean
					.getGoalMeasurement());
		} else {
			throw new CommonCustomException(
					"Goal measurement cannot be empty or blank.");
		}

		try {
			outputGsrDatEmpGoalBO = gsrDatEmpGoalRepository
					.save(inputGsrDatEmpGoalBO);
		} catch (Exception ex) {
			throw new CommonCustomException("Unable to update the goal.");
		}

		if (outputGsrDatEmpGoalBO != null) {
			gsrGoalOutputBean.setGoalId(outputGsrDatEmpGoalBO
					.getPkGsrDatEmpGoalId());
			gsrGoalOutputBean.setGoalName(outputGsrDatEmpGoalBO
					.getGsrDatEmpGoalName());
			gsrGoalOutputBean.setEmployeeId(outputGsrDatEmpGoalBO
					.getFkGsrDatEmpId());
			gsrGoalOutputBean.setReportingMgrId(outputGsrDatEmpGoalBO
					.getFkGsrDatMgrId());
			gsrGoalOutputBean.setGsrStartDate(outputGsrDatEmpGoalBO
					.getGsrGoalStartDate());
			gsrGoalOutputBean.setGsrEndDate(outputGsrDatEmpGoalBO
					.getGsrGoalEndDate());
			gsrGoalOutputBean.setGoalSlabId(outputGsrDatEmpGoalBO
					.getFkGsrSlabId());
			gsrGoalOutputBean.setGoalWeightage(outputGsrDatEmpGoalBO
					.getGsrGoalWeightage());
			gsrGoalOutputBean.setGoalDesciption(outputGsrDatEmpGoalBO
					.getGsrGoalDescription());
			gsrGoalOutputBean.setGoalMeasurement(outputGsrDatEmpGoalBO
					.getGsrGoalMeasurement());
			gsrGoalOutputBean.setGoalStatus(outputGsrDatEmpGoalBO
					.getFkGsrGoalStatus());
			gsrGoalOutputBean.setGoalCreatedBy(outputGsrDatEmpGoalBO
					.getGsrGoalCreatedBy());
			gsrGoalOutputBean.setGoalCreatedDate(outputGsrDatEmpGoalBO
					.getGsrGoalCreatedDate());
			gsrGoalOutputBean.setEmpGoalKpiWeightageId(outputGsrDatEmpGoalBO
					.getEmpGoalKpiWeightageId());
			gsrGoalOutputBean.setFkEmpGoalDomainMgrId(outputGsrDatEmpGoalBO
					.getFkEmpGoalDomainMgrId());
			gsrGoalOutputBean.setFkEmpGoalBuId(outputGsrDatEmpGoalBO
					.getFkEmpGoalBuId());
		}

		return gsrGoalOutputBean;
	}

	// End of addition by Shyam for updation of GSR goal

	public List<GsrTeamGoalStatusBean> goalCurrentYearSetStatusForBu(
			GsrGoalSetStatusBean gsrGoalSetStatusBean)
			throws DataAccessException, CommonCustomException,
			BusinessException {
		List<GsrTeamGoalStatusBean> tempList = new ArrayList<GsrTeamGoalStatusBean>();
		List<GsrTeamGoalStatusBean> outputList = new ArrayList<GsrTeamGoalStatusBean>();
		List<GsrSlabBO> gsrSlabList = gsrSlabRepository
				.findByGsrSlabYearOrderByPkGsrSlabIdAsc(year + "");
		if (gsrSlabList.size() > 0) {
			for (GsrSlabBO gsrSlabBO : gsrSlabList) {
				GsrTeamGoalStatusBean gsrTeamGoalStatusBean = new GsrTeamGoalStatusBean();
				String duration = monthFormat.format(
						gsrSlabBO.getGsrSlabStartDate()).toUpperCase()
						+ "-"
						+ monthFormat.format(gsrSlabBO.getGsrSlabEndDate())
								.toUpperCase();
				gsrTeamGoalStatusBean.setSlabDuration(duration);
				gsrTeamGoalStatusBean.setSlabId(gsrSlabBO.getPkGsrSlabId());
				gsrTeamGoalStatusBean.setSlabShortName(gsrSlabBO
						.getGsrSlabShortName());
				List<DatEmpProfessionalDetailBO> proDetails = new ArrayList<DatEmpProfessionalDetailBO>();
				LocalDate slabDate = LocalDate.parse(simpleDateFormat
						.format(gsrSlabBO.getGsrSlabEndDate()));
				Date slabDateMinus15Days = Date.from(slabDate.atStartOfDay(
						ZoneId.systemDefault()).toInstant());
				proDetails = employeeProfessionalRepository
						.getConfirmedReporteesByBUAndDateRange(
								slabDateMinus15Days,
								gsrGoalSetStatusBean.getBuUnit());
				Set<Integer> slabReportees = new HashSet<Integer>();
				for (DatEmpProfessionalDetailBO pro : proDetails) {
					slabReportees.add(pro.getFkMainEmpDetailId());
				}
				gsrTeamGoalStatusBean.setTeamCount(slabReportees.size());
				if (gsrSlabBO.getGsrSlabEnabled().equalsIgnoreCase("ENABLED")) {
					List<GsrDatEmpGoalBO> gsrDatEmpGoalBoTempList = createGoalrepository
							.findByFkEmpGoalBuIdAndAndFkGsrSlabId(
									gsrGoalSetStatusBean.getBuUnit(),
									gsrSlabBO.getPkGsrSlabId());
					if (gsrDatEmpGoalBoTempList.size() > 0) {
						for (GsrDatEmpGoalBO gsrDatEmpGoalBO : gsrDatEmpGoalBoTempList) {
							slabReportees.add(gsrDatEmpGoalBO
									.getFkGsrDatEmpId());
						}
						gsrTeamGoalStatusBean
								.setTeamCount(slabReportees.size());
						Integer localGoalClosedCount = 0;
						Integer localGoalNotClosedCount = 0;
						Integer localGoalSetCount = 0;
						Integer localGoalNotSetCount = 0;
						for (Integer reporteeId : slabReportees) {
							GsrGoalSetStatusBean goalSetStatusBean = this
									.getEmployeeGoalStatus(
											gsrSlabBO.getPkGsrSlabId(),
											reporteeId);
							if (goalSetStatusBean != null) {
								if (goalSetStatusBean.getStatusId().intValue() == this.CONSTANT_GOAL_NOT_SET)
									localGoalNotSetCount++;
								else if (goalSetStatusBean.getStatusId()
										.intValue() == this.CONSTANT_GOAL_SET)
									localGoalSetCount++;
								else if (goalSetStatusBean.getStatusId()
										.intValue() == this.CONSTANT_GOAL_CLOSED)
									localGoalClosedCount++;
								else if (goalSetStatusBean.getStatusId()
										.intValue() == this.CONSTANT_GOAL_NOT_CLOSED)
									localGoalNotClosedCount++;
							}
						}
						gsrTeamGoalStatusBean
								.setStatusGoalClosedCount(localGoalClosedCount);
						gsrTeamGoalStatusBean
								.setStatusNotGoalClosedCount(localGoalNotClosedCount);
						gsrTeamGoalStatusBean
								.setStatusGoalSetCount(localGoalSetCount);
						gsrTeamGoalStatusBean
								.setStatusNotGoalSetCount(localGoalNotSetCount);
						if (localGoalNotClosedCount >= localGoalClosedCount
								&& localGoalNotClosedCount >= localGoalSetCount
								&& localGoalNotClosedCount >= localGoalNotSetCount) {
							gsrTeamGoalStatusBean.setStatus("GOAL NOT CLOSED");
							gsrTeamGoalStatusBean.setStatusId(3);
						} else if (localGoalClosedCount >= localGoalNotClosedCount
								&& localGoalClosedCount >= localGoalSetCount
								&& localGoalClosedCount >= localGoalNotSetCount) {
							gsrTeamGoalStatusBean.setStatus("GOAL CLOSED");
							gsrTeamGoalStatusBean.setStatusId(4);
						} else if (localGoalSetCount >= localGoalNotClosedCount
								&& localGoalSetCount >= localGoalClosedCount
								&& localGoalSetCount >= localGoalNotSetCount) {
							gsrTeamGoalStatusBean.setStatus("GOAL SET");
							gsrTeamGoalStatusBean.setStatusId(2);
						} else if (localGoalNotSetCount >= localGoalNotClosedCount
								&& localGoalNotSetCount >= localGoalClosedCount
								&& localGoalNotSetCount >= localGoalSetCount) {
							gsrTeamGoalStatusBean.setStatus("GOAL NOT SET");
							gsrTeamGoalStatusBean.setStatusId(1);
						}
					} else {
						gsrTeamGoalStatusBean.setStatus("GOAL NOT SET");
						gsrTeamGoalStatusBean.setStatusId(1);
						gsrTeamGoalStatusBean
								.setStatusNotGoalSetCount(slabReportees.size());
						gsrTeamGoalStatusBean.setStatusGoalClosedCount(0);
						gsrTeamGoalStatusBean.setStatusNotGoalClosedCount(0);
						gsrTeamGoalStatusBean.setStatusGoalSetCount(0);
					}
					if (gsrTeamGoalStatusBean.getTeamCount().intValue() == 0) {
						gsrTeamGoalStatusBean.setStatus("NO_REPORTEE");
						gsrTeamGoalStatusBean
								.setStatusId(CONSTANT_GOAL_NO_REPORTEE);
						gsrTeamGoalStatusBean.setStatusGoalClosedCount(0);
						gsrTeamGoalStatusBean.setTeamCount(0);
						gsrTeamGoalStatusBean.setStatusNotGoalClosedCount(0);
						gsrTeamGoalStatusBean.setStatusGoalSetCount(0);
					}
				} else {
					gsrTeamGoalStatusBean.setStatus("PENDING");
					gsrTeamGoalStatusBean.setStatusId(0);
					gsrTeamGoalStatusBean
							.setStatusNotGoalSetCount(slabReportees.size());
					gsrTeamGoalStatusBean.setStatusGoalClosedCount(0);
					gsrTeamGoalStatusBean.setStatusNotGoalClosedCount(0);
					gsrTeamGoalStatusBean.setStatusGoalSetCount(0);
				}
				
				outputList.add(gsrTeamGoalStatusBean);
			}
		} else {
			throw new CommonCustomException(
					"unable to retrieve the current year slabs. Please contact to administrator");
		}
		return outputList;

	}

	GsrTeamGoalStatusBean gsrTeamGoalStatusBean;
	Integer goalClosedCount, goalNotClosedCount, goalSetCount, goalNotSetCount,
			domainManagerId, teamCount;
	Long totalWeightage = 0l;
	Short buid;
	String slabShortName = "";
	String slabName = "";

	public List<GsrTeamGoalStatusBean> getTeamGoalStatusUsingReporeeList(
			List<GsrDatEmpGoalBO> gsrDatEmpGoalBoTempList)
			throws DataAccessException {
		List<GsrGoalSetStatusBean> gsrGoalSetStatusBeanOutPutList = new ArrayList<GsrGoalSetStatusBean>();
		List<GsrTeamGoalStatusBean> selectedData = new ArrayList<GsrTeamGoalStatusBean>();
		Set<Integer> reporteeEmpId = new TreeSet<Integer>();
		List<GsrTeamGoalStatusBean> gsrTeamGoalStatusBeanOutList = new ArrayList<GsrTeamGoalStatusBean>();
		try {
			gsrDatEmpGoalBoTempList
					.forEach(element -> {
						GsrGoalSetStatusBean gsrGoalSetStatusBeanOutPut = new GsrGoalSetStatusBean();
						List<GsrDatEmpFinalRatingBO> finalRating = ratingRepository
								.findByFkGsrFinalRatingEmpIdAndFkGsrFinalRatingSlabId(
										element.getFkGsrDatEmpId(),
										element.getFkGsrSlabId());
						if (finalRating.size() > 0) {
							finalRating.forEach(rating -> {
								if (rating.getFinalRatingStatus() == 2
										|| rating.getFinalRatingStatus() == 4) {
									gsrGoalSetStatusBeanOutPut
											.setStatus("GOAL  CLOSED");
								} else {
									gsrGoalSetStatusBeanOutPut
											.setStatus("GOAL NOT CLOSED");
								}
							});
						} else {
							if (element.getTotalWeightage() == 100) {
								gsrGoalSetStatusBeanOutPut
										.setStatus("GOAL SET");
							} else {
								gsrGoalSetStatusBeanOutPut
										.setStatus("GOAL NOT SET");
							}
						}
						gsrGoalSetStatusBeanOutPut.setSlabId(element
								.getFkGsrSlabId());
						GsrSlabBO slabData = gsrSlabRepository
								.findByPkGsrSlabId(element.getFkGsrSlabId());
						gsrGoalSetStatusBeanOutPut.setSlabShortName(slabData
								.getGsrSlabShortName());
						String duration = monthFormat.format(
								slabData.getGsrSlabStartDate()).toUpperCase()
								+ "-"
								+ monthFormat.format(
										slabData.getGsrSlabEndDate())
										.toUpperCase();
						GsrTeamGoalStatusBean temporaryInputBean = new GsrTeamGoalStatusBean();
						temporaryInputBean.setSlabId(element.getFkGsrSlabId());
						temporaryInputBean.setSlabDuration(duration);
						temporaryInputBean.setSlabShortName(element
								.getSlabShortName());
						selectedData.add(temporaryInputBean);
						reporteeEmpId.add(element.getFkGsrDatEmpId());
						gsrGoalSetStatusBeanOutPutList
								.add(gsrGoalSetStatusBeanOutPut);
					});
			int reporteeCount = reporteeEmpId.size();

			Collection<GsrTeamGoalStatusBean> nonDuplicatedEmployees = selectedData
					.stream()
					.<Map<Short, GsrTeamGoalStatusBean>> collect(HashMap::new,
							(m, e) -> m.put(e.getSlabId(), e), Map::putAll)
					.values();

			nonDuplicatedEmployees
					.forEach(slab -> {
						gsrTeamGoalStatusBean = new GsrTeamGoalStatusBean();
						goalClosedCount = 0;
						goalNotClosedCount = 0;
						goalSetCount = 0;
						goalNotSetCount = 0;
						slabShortName = "";
						gsrGoalSetStatusBeanOutPutList
								.stream()
								.filter(fileterSlab -> slab.getSlabId() == fileterSlab
										.getSlabId())
								.forEach(
										reportee -> {
											slabShortName = reportee
													.getSlabShortName();
											if (reportee.getStatus()
													.equalsIgnoreCase(
															"GOAL SET")) {
												goalSetCount++;
											} else if (reportee.getStatus()
													.equalsIgnoreCase(
															"GOAL NOT SET")) {
												goalNotSetCount++;
											} else if (reportee.getStatus()
													.equalsIgnoreCase(
															"GOAL  CLOSED")) {
												goalClosedCount++;
											} else if (reportee.getStatus()
													.equalsIgnoreCase(
															"GOAL NOT CLOSED")) {
												goalNotClosedCount++;
											}
										});
						if (goalNotClosedCount > 0
								&& goalNotClosedCount >= goalNotSetCount) {
							gsrTeamGoalStatusBean.setStatus("GOAL NOT CLOSED");
						}
						if (goalNotSetCount > 0
								&& goalNotSetCount > goalNotClosedCount) {
							gsrTeamGoalStatusBean.setStatus("GOAL NOT SET");
						}
						if (goalNotSetCount == 0 && goalNotClosedCount == 0
								&& goalSetCount == 0 && goalClosedCount != 0) {
							gsrTeamGoalStatusBean.setStatus("GOAL CLOSED");
						}
						if (goalNotSetCount == 0 && goalNotClosedCount == 0
								&& goalSetCount != 0 && goalClosedCount == 0) {
							gsrTeamGoalStatusBean.setStatus("GOAL  SET");
						}

						if (goalNotSetCount == 0 && goalNotClosedCount == 0
								&& goalSetCount == 0 && goalClosedCount == 0) {
							gsrTeamGoalStatusBean.setStatus("GOAL  NOT SET");
						}

						gsrTeamGoalStatusBean.setSlabShortName(slabShortName);
						gsrTeamGoalStatusBean.setTeamCount(reporteeCount);
						gsrTeamGoalStatusBean.setSlabDuration(slab
								.getSlabDuration());
						gsrTeamGoalStatusBean.setSlabId(slab.getSlabId());
						gsrTeamGoalStatusBean
								.setStatusGoalClosedCount(goalClosedCount);
						gsrTeamGoalStatusBean
								.setStatusNotGoalClosedCount(goalNotClosedCount);
						gsrTeamGoalStatusBean
								.setStatusGoalSetCount(goalSetCount);
						gsrTeamGoalStatusBean
								.setStatusNotGoalSetCount(goalNotSetCount);
						gsrTeamGoalStatusBeanOutList.add(gsrTeamGoalStatusBean);
					});

		} catch (Exception e) {

		}
		LOG.endUsecase("Exited Goal Set Status For an Employee in `GSRGoalService` And The OutPut Are ");

		return gsrTeamGoalStatusBeanOutList;

	}

	// Added by Shyam for creation of gsr goals for multiple reportees
	/**
	 * @method storeGsrGoal
	 * @param GsrCreateGoalInputBean
	 * @return GsrGoalOutputBean
	 * @throws CommonCustomException
	 *             , DataAccessException , BusinessException
	 * @Description : This method is used to store input gsr goal into DB(used
	 *              by employee and manager too).
	 */
	public List<GsrGoalOutputForMultipleReporteesBean> storeGsrGoalForMultipleReportees(
			List<GsrCreateGoalInputBean> gsrCreateGoalInputBeanList)
			throws DataAccessException, CommonCustomException {
		List<GsrGoalOutputForMultipleReporteesBean> gsrGoalOutputBeanList = new ArrayList<GsrGoalOutputForMultipleReporteesBean>();
		for (GsrCreateGoalInputBean gsrCreateGoalInputBean : gsrCreateGoalInputBeanList) {
			GsrGoalOutputBean gsrGoalOutputBean = new GsrGoalOutputBean();
			GsrGoalOutputForMultipleReporteesBean createGoalForMultiReportees = new GsrGoalOutputForMultipleReporteesBean();
			try {
				gsrGoalOutputBean = storeGsrGoal(gsrCreateGoalInputBean);
				createGoalForMultiReportees.setEmployeeId(gsrGoalOutputBean
						.getEmployeeId());
				createGoalForMultiReportees
						.setEmployeeNameWithId(getEmpNameByEmpId(gsrCreateGoalInputBean
								.getEmployeeId())
								+ " -"
								+ gsrCreateGoalInputBean.getEmployeeId()); // added
																			// by
																			// Rajesh
																			// for
																			// displaying
																			// name
																			// in
																			// UI
																			// on
																			// 11/8/17
				createGoalForMultiReportees.setProcessStatus("SUCCESS");
				createGoalForMultiReportees
						.setProcessDesc("Successfully stored into database");
			} catch (Exception ex) {
				createGoalForMultiReportees
						.setEmployeeId(gsrCreateGoalInputBean.getEmployeeId());
				createGoalForMultiReportees
						.setEmployeeNameWithId(getEmpNameByEmpId(gsrCreateGoalInputBean
								.getEmployeeId())
								+ " -"
								+ gsrCreateGoalInputBean.getEmployeeId()); // added
																			// by
																			// Rajesh
																			// for
																			// displaying
																			// name
																			// in
																			// UI
																			// on
																			// 11/8/17
				createGoalForMultiReportees.setProcessStatus("FAILED");
				createGoalForMultiReportees.setProcessDesc(ex.getMessage());
			}
			createGoalForMultiReportees.setGsrGoalOutputBean(gsrGoalOutputBean);
			gsrGoalOutputBeanList.add(createGoalForMultiReportees);
		}
		return gsrGoalOutputBeanList;
	}

	// End of addition by Shyam for creation of gsr goals for multiple reportees

	// Added by Mani for HR dashBoard Screen
	public List<GsrTeamGoalStatusBean> goalCurrentYearSetStatusAllEmployees()
			throws DataAccessException, CommonCustomException,
			BusinessException {
		try {
			List<GsrTeamGoalStatusBean> tempList = new ArrayList<GsrTeamGoalStatusBean>();
			List<GsrTeamGoalStatusBean> outputList = new ArrayList<GsrTeamGoalStatusBean>();
			List<GsrSlabBO> gsrSlabList = gsrSlabRepository
					.findByGsrSlabYearOrderByPkGsrSlabIdAsc(year + "");
			if (gsrSlabList.size() > 0) {
				for (GsrSlabBO gsrSlabBO : gsrSlabList) {
					GsrTeamGoalStatusBean gsrTeamGoalStatusBean = new GsrTeamGoalStatusBean();
					String duration = monthFormat.format(
							gsrSlabBO.getGsrSlabStartDate()).toUpperCase()
							+ "-"
							+ monthFormat.format(gsrSlabBO.getGsrSlabEndDate())
									.toUpperCase();
					gsrTeamGoalStatusBean.setSlabDuration(duration);
					gsrTeamGoalStatusBean.setSlabId(gsrSlabBO.getPkGsrSlabId());
					gsrTeamGoalStatusBean.setSlabShortName(gsrSlabBO
							.getGsrSlabShortName());
					List<DatEmpProfessionalDetailBO> proDetails = new ArrayList<DatEmpProfessionalDetailBO>();
					LocalDate slabDate = LocalDate.parse(simpleDateFormat
							.format(gsrSlabBO.getGsrSlabEndDate()));
					Date slabDateMinus15Days = Date.from(slabDate.minusDays(15)
							.atStartOfDay(ZoneId.systemDefault()).toInstant());
					proDetails = employeeProfessionalRepository
							.getConfirmedReporteesByDateRange(slabDateMinus15Days);
					Set<Integer> slabReportees = new HashSet<Integer>();
					for (DatEmpProfessionalDetailBO pro : proDetails) {
						slabReportees.add(pro.getFkMainEmpDetailId());
					}
					gsrTeamGoalStatusBean.setTeamCount(slabReportees.size());
					if (gsrSlabBO.getGsrSlabEnabled().equalsIgnoreCase(
							"ENABLED")) {
						List<GsrDatEmpGoalBO> gsrDatEmpGoalBoTempList = createGoalrepository
								.findByFkGsrSlabId(gsrSlabBO.getPkGsrSlabId());
						if (gsrDatEmpGoalBoTempList.size() > 0) {
							for (GsrDatEmpGoalBO gsrDatEmpGoalBO : gsrDatEmpGoalBoTempList) {
								slabReportees.add(gsrDatEmpGoalBO
										.getFkGsrDatEmpId());
							}
							gsrTeamGoalStatusBean.setTeamCount(slabReportees
									.size());
							Integer localGoalClosedCount = 0;
							Integer localGoalNotClosedCount = 0;
							Integer localGoalSetCount = 0;
							Integer localGoalNotSetCount = 0;
							for (Integer reporteeId : slabReportees) {
								GsrGoalSetStatusBean goalSetStatusBean = this
										.getEmployeeGoalStatus(
												gsrSlabBO.getPkGsrSlabId(),
												reporteeId);
								if (goalSetStatusBean != null) {
									if (goalSetStatusBean.getStatusId()
											.intValue() == this.CONSTANT_GOAL_NOT_SET)
										localGoalNotSetCount++;
									else if (goalSetStatusBean.getStatusId()
											.intValue() == this.CONSTANT_GOAL_SET)
										localGoalSetCount++;
									else if (goalSetStatusBean.getStatusId()
											.intValue() == this.CONSTANT_GOAL_CLOSED)
										localGoalClosedCount++;
									else if (goalSetStatusBean.getStatusId()
											.intValue() == this.CONSTANT_GOAL_NOT_CLOSED)
										localGoalNotClosedCount++;
								}
							}
							gsrTeamGoalStatusBean
									.setStatusGoalClosedCount(localGoalClosedCount);
							gsrTeamGoalStatusBean
									.setStatusNotGoalClosedCount(localGoalNotClosedCount);
							gsrTeamGoalStatusBean
									.setStatusGoalSetCount(localGoalSetCount);
							gsrTeamGoalStatusBean
									.setStatusNotGoalSetCount(localGoalNotSetCount);
							if (localGoalNotClosedCount >= localGoalClosedCount
									&& localGoalNotClosedCount >= localGoalSetCount
									&& localGoalNotClosedCount >= localGoalNotSetCount) {
								gsrTeamGoalStatusBean
										.setStatus("GOAL NOT CLOSED");
								gsrTeamGoalStatusBean.setStatusId(3);
							} else if (localGoalClosedCount >= localGoalNotClosedCount
									&& localGoalClosedCount >= localGoalSetCount
									&& localGoalClosedCount >= localGoalNotSetCount) {
								gsrTeamGoalStatusBean.setStatus("GOAL CLOSED");
								gsrTeamGoalStatusBean.setStatusId(4);
							} else if (localGoalSetCount >= localGoalNotClosedCount
									&& localGoalSetCount >= localGoalClosedCount
									&& localGoalSetCount >= localGoalNotSetCount) {
								gsrTeamGoalStatusBean.setStatus("GOAL SET");
								gsrTeamGoalStatusBean.setStatusId(2);
							} else if (localGoalNotSetCount >= localGoalNotClosedCount
									&& localGoalNotSetCount >= localGoalClosedCount
									&& localGoalNotSetCount >= localGoalSetCount) {
								gsrTeamGoalStatusBean.setStatus("GOAL NOT SET");
								gsrTeamGoalStatusBean.setStatusId(1);
							}
						} else {
							gsrTeamGoalStatusBean.setStatus("GOAL NOT SET");
							gsrTeamGoalStatusBean.setStatusId(1);
							gsrTeamGoalStatusBean
									.setStatusNotGoalSetCount(slabReportees
											.size());
							gsrTeamGoalStatusBean.setStatusGoalClosedCount(0);
							gsrTeamGoalStatusBean
									.setStatusNotGoalClosedCount(0);
							gsrTeamGoalStatusBean.setStatusGoalSetCount(0);
						}
					} else {
						gsrTeamGoalStatusBean.setStatus("PENDING");
						gsrTeamGoalStatusBean.setStatusId(0);
						gsrTeamGoalStatusBean
								.setStatusNotGoalSetCount(slabReportees.size());
						gsrTeamGoalStatusBean.setStatusGoalClosedCount(0);
						gsrTeamGoalStatusBean.setStatusNotGoalClosedCount(0);
						gsrTeamGoalStatusBean.setStatusGoalSetCount(0);
					}
					outputList.add(gsrTeamGoalStatusBean);
				}
			} else {
				throw new CommonCustomException(
						"unable to retrieve the current year slabs. Please contact to administrator");
			}
			return outputList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// End of addition By mani

	private GsrGoalSetStatusBean getEmployeeGoalStatus(Short slabId,
			Integer empId) throws CommonCustomException {

		GsrSlabBO gsrSlabBO = new GsrSlabBO();
		gsrSlabBO = gsrSlabRepository.findByPkGsrSlabId(slabId);

		String duration = monthFormat.format(gsrSlabBO.getGsrSlabStartDate())
				.toUpperCase()
				+ "-"
				+ monthFormat.format(gsrSlabBO.getGsrSlabEndDate())
						.toUpperCase();

		GsrGoalSetStatusBean goalSetStatusBean = new GsrGoalSetStatusBean();
		goalSetStatusBean.setEmpId(empId);
		goalSetStatusBean.setSlabId(slabId);
		goalSetStatusBean.setSlabShortName(slabRepository.findByPkGsrSlabId(
				slabId).getGsrSlabShortName());
		goalSetStatusBean.setSlabName(slabRepository.findByPkGsrSlabId(slabId)
				.getGsrSlabName());
		goalSetStatusBean.setSlabDuration(duration);
		if (gsrSlabBO.getGsrSlabEnabled().equalsIgnoreCase("ENABLED")) {
			List<GsrDatEmpGoalBO> goalSetStatusBySlab = new ArrayList<GsrDatEmpGoalBO>();
			goalSetStatusBySlab = createGoalrepository
					.findByFkGsrDatEmpIdAndFkGsrSlabId(empId, slabId);
			if (goalSetStatusBySlab.size() > 0) {
				Set<Integer> rmIdsList = goalSetStatusBySlab.stream()
						.filter(goal -> goal.getFkGsrGoalStatus() != 6)
						.map(goal -> goal.getFkGsrDatMgrId())
						.collect(Collectors.toSet());
				Short buId = 0;
				List<Byte> statusList = new ArrayList<Byte>();
				String slabShortName = "";
				Long remainingWeightage = 0l;
				Long totalWeightage = 0l;
				for (GsrDatEmpGoalBO gsrDatEmpGoalBO : goalSetStatusBySlab) {
					if (gsrDatEmpGoalBO.getFkGsrGoalStatus() != (byte) 6) {
						totalWeightage = totalWeightage
								+ gsrDatEmpGoalBO.getGsrGoalWeightage();
						statusList.add(gsrDatEmpGoalBO.getFkGsrGoalStatus());
					}
				}
				int rmCount = rmIdsList.size();
				goalSetStatusBean.setReportingManagerId(rmIdsList);
				if (statusList.size() == 0) {
					goalSetStatusBean.setStatus("GOAL NOT SET");
					goalSetStatusBean.setStatusId(this.CONSTANT_GOAL_NOT_SET);
					goalSetStatusBean.setTotalWeightage(0l);
					goalSetStatusBean.setRemainingWeightage(100l);
				} else {
					if (statusList.contains((byte) 1)) {
						goalSetStatusBean.setStatus("GOAL NOT SET");
						goalSetStatusBean
								.setStatusId(this.CONSTANT_GOAL_NOT_SET);
						goalSetStatusBean.setTotalWeightage(0l);
						goalSetStatusBean
								.setRemainingWeightage((long) (rmCount * 100));
					}

					if (statusList.contains((byte) 2)) {
						long localTotalWeightage = 0l;
						for (GsrDatEmpGoalBO gsrDatEmpGoalBO : goalSetStatusBySlab) {
							if (gsrDatEmpGoalBO.getFkGsrGoalStatus() != (byte) 6
									&& gsrDatEmpGoalBO.getFkGsrGoalStatus() == 2) {
								localTotalWeightage = localTotalWeightage
										+ gsrDatEmpGoalBO.getGsrGoalWeightage();
							}
						}
						if (statusList.stream().allMatch(
								statusList.get(0)::equals)
								&& statusList.contains((byte) 2)
								&& ((totalWeightage / rmCount) - 100 == 0)) {
							goalSetStatusBean.setStatus("GOAL  SET");
							goalSetStatusBean
									.setStatusId(this.CONSTANT_GOAL_SET);
							goalSetStatusBean
									.setTotalWeightage(localTotalWeightage);
							goalSetStatusBean
									.setRemainingWeightage((rmCount * 100)
											- (localTotalWeightage));
						} else {
							goalSetStatusBean
									.setTotalWeightage(localTotalWeightage);
							goalSetStatusBean
									.setRemainingWeightage((rmCount * 100)
											- (localTotalWeightage));
							goalSetStatusBean.setStatus("GOAL NOT SET");
							goalSetStatusBean.setStatusId(1);
						}
					}
					if (statusList.contains((byte) 5)) {
						long localTotalWeightage = 0l;
						for (GsrDatEmpGoalBO gsrDatEmpGoalBO : goalSetStatusBySlab) {
							if (gsrDatEmpGoalBO.getFkGsrGoalStatus() != (byte) 6
									&& gsrDatEmpGoalBO.getFkGsrGoalStatus() == 5) {
								localTotalWeightage = localTotalWeightage
										+ gsrDatEmpGoalBO.getGsrGoalWeightage();
							}
						}
						goalSetStatusBean
								.setTotalWeightage(localTotalWeightage);
						goalSetStatusBean.setRemainingWeightage((rmCount * 100)
								- (localTotalWeightage));

						if (statusList.stream().allMatch(
								statusList.get(0)::equals)
								&& (statusList.contains((byte) 5))) {
							goalSetStatusBean.setStatus("GOAL  CLOSED");
							goalSetStatusBean
									.setStatusId(this.CONSTANT_GOAL_CLOSED);
							goalSetStatusBean
									.setTotalWeightage(localTotalWeightage);
							goalSetStatusBean
									.setRemainingWeightage((rmCount * 100)
											- (localTotalWeightage));
						} else {
							goalSetStatusBean.setStatus("GOAL  NOT CLOSED");
							goalSetStatusBean
									.setStatusId(this.CONSTANT_GOAL_NOT_CLOSED);
							goalSetStatusBean
									.setTotalWeightage(localTotalWeightage);
							goalSetStatusBean
									.setRemainingWeightage((rmCount * 100)
											- (localTotalWeightage));
						}

					}
					if (statusList.contains((byte) 7)) {
						long localTotalWeightage = 0l;
						for (GsrDatEmpGoalBO gsrDatEmpGoalBO : goalSetStatusBySlab) {
							if (gsrDatEmpGoalBO.getFkGsrGoalStatus() != (byte) 6
									&& gsrDatEmpGoalBO.getFkGsrGoalStatus() == 7) {
								localTotalWeightage = localTotalWeightage
										+ gsrDatEmpGoalBO.getGsrGoalWeightage();
							}
						}
						goalSetStatusBean
								.setTotalWeightage(localTotalWeightage);
						goalSetStatusBean.setRemainingWeightage((rmCount * 100)
								- (localTotalWeightage));

						if (statusList.stream().allMatch(
								statusList.get(0)::equals)
								&& (statusList.contains((byte) 7))) {
							goalSetStatusBean.setStatus("GOAL  CLOSED");
							goalSetStatusBean
									.setStatusId(this.CONSTANT_GOAL_CLOSED);
							goalSetStatusBean
									.setTotalWeightage(localTotalWeightage);
							goalSetStatusBean
									.setRemainingWeightage((rmCount * 100)
											- (localTotalWeightage));
						} else {
							goalSetStatusBean.setStatus("GOAL  NOT CLOSED");
							goalSetStatusBean
									.setStatusId(this.CONSTANT_GOAL_NOT_CLOSED);
							goalSetStatusBean
									.setTotalWeightage(localTotalWeightage);
							goalSetStatusBean
									.setRemainingWeightage((rmCount * 100)
											- (localTotalWeightage));
						}

					}
					if (statusList.contains((byte) 3)
							|| statusList.contains((byte) 4)
							|| statusList.contains((byte) 8)
							|| statusList.contains((byte) 9)) {
						// long localTotalWeightage =0l;
						// for (GsrDatEmpGoalBO gsrDatEmpGoalBO :
						// goalSetStatusBySlab) {
						// if(gsrDatEmpGoalBO.getFkGsrGoalStatus() != (byte)6 &&
						// gsrDatEmpGoalBO.getFkGsrGoalStatus() == 3 ){
						// localTotalWeightage =
						// localTotalWeightage+gsrDatEmpGoalBO.getGsrGoalWeightage();
						// }
						// }
						goalSetStatusBean.setTotalWeightage(0l);
						goalSetStatusBean.setRemainingWeightage(100l);
						goalSetStatusBean.setStatus("GOAL  NOT CLOSED");
						goalSetStatusBean
								.setStatusId(this.CONSTANT_GOAL_NOT_CLOSED);
					}
					// if(statusList.contains((byte)4)){
					// long localTotalWeightage =0l;
					// for (GsrDatEmpGoalBO gsrDatEmpGoalBO :
					// goalSetStatusBySlab) {
					// if(gsrDatEmpGoalBO.getFkGsrGoalStatus() != (byte)6 &&
					// gsrDatEmpGoalBO.getFkGsrGoalStatus() == 4 ){
					// localTotalWeightage =
					// localTotalWeightage+gsrDatEmpGoalBO.getGsrGoalWeightage();
					// }
					// }
					// goalSetStatusBean.setTotalWeightage(localTotalWeightage);
					// goalSetStatusBean.setRemainingWeightage((rmCount * 100
					// )-(localTotalWeightage));
					// goalSetStatusBean.setStatus("GOAL  NOT CLOSED");
					// goalSetStatusBean.setStatusId(this.CONSTANT_GOAL_NOT_CLOSED);
					// }
					// if(statusList.contains((byte)8)){
					// long localTotalWeightage =0l;
					// for (GsrDatEmpGoalBO gsrDatEmpGoalBO :
					// goalSetStatusBySlab) {
					// if(gsrDatEmpGoalBO.getFkGsrGoalStatus() != (byte)6 &&
					// gsrDatEmpGoalBO.getFkGsrGoalStatus() == 8 ){
					// localTotalWeightage =
					// localTotalWeightage+gsrDatEmpGoalBO.getGsrGoalWeightage();
					// }
					// }
					// goalSetStatusBean.setTotalWeightage(localTotalWeightage);
					// goalSetStatusBean.setRemainingWeightage((rmCount * 100
					// )-(localTotalWeightage));
					// goalSetStatusBean.setStatus("GOAL  NOT CLOSED");
					// goalSetStatusBean.setStatusId(this.CONSTANT_GOAL_NOT_CLOSED);
					// }
					// if(statusList.contains((byte)9)){
					// long localTotalWeightage =0l;
					// for (GsrDatEmpGoalBO gsrDatEmpGoalBO :
					// goalSetStatusBySlab) {
					// if(gsrDatEmpGoalBO.getFkGsrGoalStatus() != (byte)6 &&
					// gsrDatEmpGoalBO.getFkGsrGoalStatus() == 9 ){
					// localTotalWeightage =
					// localTotalWeightage+gsrDatEmpGoalBO.getGsrGoalWeightage();
					// }
					// }
					// goalSetStatusBean.setTotalWeightage(localTotalWeightage);
					// goalSetStatusBean.setRemainingWeightage((rmCount * 100
					// )-(localTotalWeightage));
					// goalSetStatusBean.setStatus("GOAL  NOT CLOSED");
					// goalSetStatusBean.setStatusId(this.CONSTANT_GOAL_NOT_CLOSED);
					// }
					if (statusList.stream().allMatch(statusList.get(0)::equals)
							&& (statusList.contains((byte) 6))) {
						goalSetStatusBean.setStatus("GOAL  NOT SET");
						goalSetStatusBean
								.setStatusId(this.CONSTANT_GOAL_NOT_SET);
						goalSetStatusBean.setTotalWeightage(0l);
						goalSetStatusBean
								.setRemainingWeightage((long) (rmCount * 100));
					}
				}
			} else {
				goalSetStatusBean.setStatus("GOAL  NOT SET");
				goalSetStatusBean.setStatusId(this.CONSTANT_GOAL_NOT_SET);
				goalSetStatusBean.setTotalWeightage(0l);
				goalSetStatusBean.setRemainingWeightage(100l);
			}
		} else {
			goalSetStatusBean.setStatus("PENDING");
			goalSetStatusBean.setStatusId(CONSTANT_GOAL_PENDING);
			goalSetStatusBean.setTotalWeightage(0l);
			goalSetStatusBean.setRemainingWeightage(100l);
		}
		return goalSetStatusBean;
	}

	@Autowired
	private BusinessUnitRepository businessUnitRepository;

	public List<GsrTeamGoalStatusBean> getGoalStatusForAllActiveBu()
			throws DataAccessException, CommonCustomException,
			BusinessException {
		 
			List<GsrTeamGoalStatusBean> outputList = new ArrayList<GsrTeamGoalStatusBean>();

			List<GsrSlabBO> gsrSlabList = gsrSlabRepository
					.findByGsrSlabYearOrderByPkGsrSlabIdAsc(year + "");
			if (gsrSlabList.size() > 0) {
				List<List<GsrTeamGoalStatusBean>> allBuDataList = new ArrayList<List<GsrTeamGoalStatusBean>>();
				List<MasBuUnitBO> allActiveBu = businessUnitRepository
						.allActiveBU("ACTIVE");
				for (MasBuUnitBO masBuUnitBO : allActiveBu) {
					GsrGoalSetStatusBean gsrGoalSetStatusBean = new GsrGoalSetStatusBean();
					gsrGoalSetStatusBean.setBuUnit(masBuUnitBO.getPkBuUnitId());
					List<GsrTeamGoalStatusBean> buWiseData = this
							.goalCurrentYearSetStatusForBu(gsrGoalSetStatusBean);
					if (buWiseData.size() > 0)
						allBuDataList.add(buWiseData);
				}
				for (GsrSlabBO gsrSlabBO : gsrSlabList) {
					gsrTeamGoalStatusBean = new GsrTeamGoalStatusBean();
					String duration = monthFormat.format(
							gsrSlabBO.getGsrSlabStartDate()).toUpperCase()
							+ "-"
							+ monthFormat.format(gsrSlabBO.getGsrSlabEndDate())
									.toUpperCase();
					gsrTeamGoalStatusBean.setSlabDuration(duration);
					gsrTeamGoalStatusBean.setSlabId(gsrSlabBO.getPkGsrSlabId());
					gsrTeamGoalStatusBean.setSlabShortName(gsrSlabBO
							.getGsrSlabShortName());
					goalClosedCount = 0;
					goalNotClosedCount = 0;
					goalSetCount = 0;
					goalNotSetCount = 0;
					teamCount = 0;
					if (gsrSlabBO.getGsrSlabEnabled().equalsIgnoreCase("ENABLED")) {
						allBuDataList
								.stream()
								.forEach(
										buWiseList -> {
											buWiseList
													.stream()
													.filter(statusBean -> statusBean
															.getSlabId() == gsrSlabBO
															.getPkGsrSlabId())
													.forEach(
															statusData -> {
																teamCount = teamCount
																		+ statusData
																				.getTeamCount();
																goalClosedCount = goalClosedCount
																		+ statusData
																				.getStatusGoalClosedCount();
																goalNotClosedCount = goalNotClosedCount
																		+ statusData
																				.getStatusNotGoalClosedCount();
																goalSetCount = goalSetCount
																		+ statusData
																				.getStatusGoalSetCount();
																goalNotSetCount = goalNotSetCount
																		+ statusData
																				.getStatusNotGoalSetCount();
															});
										});
						gsrTeamGoalStatusBean
								.setStatusGoalClosedCount(goalClosedCount);
						gsrTeamGoalStatusBean
								.setStatusNotGoalClosedCount(goalNotClosedCount);
						gsrTeamGoalStatusBean.setStatusGoalSetCount(goalSetCount);
						gsrTeamGoalStatusBean
								.setStatusNotGoalSetCount(goalNotSetCount);
						gsrTeamGoalStatusBean.setTeamCount(teamCount);
						if (goalNotClosedCount >= goalClosedCount
								&& goalNotClosedCount >= goalSetCount
								&& goalNotClosedCount >= goalNotSetCount) {
							gsrTeamGoalStatusBean.setStatus("GOAL NOT CLOSED");
							gsrTeamGoalStatusBean.setStatusId(3);
						} else if (goalClosedCount >= goalNotClosedCount
								&& goalClosedCount >= goalSetCount
								&& goalClosedCount >= goalNotSetCount) {
							gsrTeamGoalStatusBean.setStatus("GOAL CLOSED");
							gsrTeamGoalStatusBean.setStatusId(4);
						} else if (goalSetCount >= goalNotClosedCount
								&& goalSetCount >= goalClosedCount
								&& goalSetCount >= goalNotSetCount) {
							gsrTeamGoalStatusBean.setStatus("GOAL SET");
							gsrTeamGoalStatusBean.setStatusId(2);
						} else if (goalNotSetCount >= goalNotClosedCount
								&& goalNotSetCount >= goalClosedCount
								&& goalNotSetCount >= goalSetCount) {
							gsrTeamGoalStatusBean.setStatus("GOAL NOT SET");
							gsrTeamGoalStatusBean.setStatusId(1);
						}
						if (gsrTeamGoalStatusBean.getTeamCount().intValue() == 0) {
							gsrTeamGoalStatusBean.setStatus("NO_REPORTEE");
							gsrTeamGoalStatusBean
									.setStatusId(CONSTANT_GOAL_NO_REPORTEE);
							gsrTeamGoalStatusBean.setStatusGoalClosedCount(0);
							gsrTeamGoalStatusBean.setTeamCount(0);
							gsrTeamGoalStatusBean.setStatusNotGoalClosedCount(0);
							gsrTeamGoalStatusBean.setStatusGoalSetCount(0);
						}
					} else {

						gsrTeamGoalStatusBean.setStatus("PENDING");
						gsrTeamGoalStatusBean.setStatusId(0);
						gsrTeamGoalStatusBean.setTeamCount(teamCount);
						gsrTeamGoalStatusBean
								.setStatusNotGoalSetCount(goalNotSetCount);
						gsrTeamGoalStatusBean
								.setStatusGoalClosedCount(goalClosedCount);
						gsrTeamGoalStatusBean
								.setStatusNotGoalClosedCount(goalNotClosedCount);
						gsrTeamGoalStatusBean.setStatusGoalSetCount(goalSetCount);
					}
					
					outputList.add(gsrTeamGoalStatusBean);
				}
			} else {
				throw new CommonCustomException(
						"unable to retrieve the current year slabs. Please contact to administrator");
			}
			return outputList;
	}

}
