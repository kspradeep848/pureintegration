package com.thbs.mis.gsr.constants;

public class GsrConstants {
	
	public static final String CREATE_REQUEST = "/rest/gsr/goal/insert";
	
	public static final String RATING_NOT_OVERRIDEN = "NOT_OVERRIDEN";
	
	public static final String OVERRIDEN = "OVERRIDEN";
	
	public static final byte STATUS_GOAL_APPROVED = 2;
	
	public static final byte STATUS_WAITING_MANGER_APPROVAL =1;
	
	public static final byte STATUS_FINAL_RATING_REOPEN = 6;
	
	public static final byte STATUS_FINAL_RATING_RESOLVED = 4;
	
	public static final byte STATUS_FINAL_RATING_RATED = 5;
	
	public static final byte STATUS_FINAL_RATING_REJECTED = 3;
	
	public static final byte GSR_WAITING_FOR_MANAGER_APPROVAL = 1;
	public static final byte GSR_GOAL_SET = 2;
	public static final byte GSR_ACCOMPLISHMENTS_ENTERED = 3;
	public static final byte GSR_RATED = 4;
	public static final byte GSR_CLOSED = 5;
	public static final byte GSR_REJECTED = 6;
	public static final byte GSR_RESOLVED = 7;
	public static final byte GSR_MANAGER_FEEDBACK_PROVIDED = 8;
	
}
