/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrURIConstants.java                              */
/*                                                                   */
/*  Author      :  THBS                                              */
/*                                                                   */
/*  Date        : October 28, 2016                                   */
/*                                                                   */
/*  Description : This is used to create URI for all the services.   */
/*		 									                         */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* October 28, 2016     THBS     1.0        Initial version created  */
/*********************************************************************/

package com.thbs.mis.gsr.constants;

/**
 * 
 * @author THBS
 * @Description: This is used to create URI for all the services.
 */
public class GsrURIConstants {
	/* Added by Smrithi J */
	
	public static final String CREATE_REQUEST = "/gsr/goal/insert";
	public static final String GET_TRAINING_TOPICS = "/gsr/training/names";
	public static final String GET_WORKFLOW_PERMISSION = "/gsr/workflow/dates/{empId}/{slabId}";
	public static final String GET_GSR_SLABS = "/gsr/slabs"; //Edited by Rajesh Kumar for slab sorting and Enable20/06/17
	public static final String VIEW_EMPLOYEE_GOALS = "/gsr/employee/goals";
	public static final String ACCEPT_REJECT_DELEGATED_GOALS = "/gsr/delegated/goals";
	public static final String FORCE_CLOSE_GOALS = "/gsr/forceClose/goals";
	public static final String GET_DEFAULT_GOALS = "/gsr/default/goals/{empId}";
	public static final String VIEW_TEAM_GOALS = "/gsr/team/goals";
	public static final String VIEW_REPORTEES_GOALS = "/gsr/reportee/goals/{mgrId}";
	public static final String DISAGREE_RATING = "/gsr/disagree/rating";
	public static final String FETCH_GOALS_FOR_REPORTING_MANAGER="/gsr/fetch/goals/{mgrId}";
	public static final String FETCH_GOALS_FOR_EMPLOYEE="/gsr/employee/goals/fetch/{empId}";
	public static final String FETCH_GOALS_FOR_EMPLOYEE_LIST="/gsr/employee/list/goals";
	public static final String REOPEN_GOALS_BY_HR = "/gsr/reopen/goals";
	public static final String ENTER_FEEDBACK_FOR_MANAGER = "/gsr/feedback/manager";
	
	/* Ended by Smrithi J */


	/* Added by Pratibha T R */
	public static final String CREATE_SLAB = "/gsr/slab/create";
	public static final String CREATE_GOALS = "/gsr/goals/common/create";
	public static final String UPDATE_GOALS = "/gsr/goals/common/update";
	public static final String DELETE_COMMON_GOALS = "/gsr/goals/common/delete/{pkGsrKpiGoalId}";
	public static final String ENABLE_SETTINGS = "/gsr/settings/enable";
	public static final String SET_WORKFLOW_DATES = "/gsr/workflow/dates/set";
	public static final String SET_APPROVE_OR_REJECT_GOAL = "/gsr/goal/approve/reject";
	public static final String SET_APPROVE_GOAL = "/gsr/goal/approve";
	public static final String ENTER_FEDDBACK = "/gsr/enter/feedback";
	public static final String DELETE_GOAL = "/gsr/goal/delete/{pkGsrDatEmpGoalId}";
	public static final String VIEW_ENABLED_SLAB_DETAILS_BASED_ON_SLABYEAR = "/gsr/goal/slab/details/{gsrSlabYear}";
	public static final String GET_GOAL_WEIGHTAGE_DETAILS = "/gsr/goal/weightage/details";
	public static final String GET_MAXIMUM_KPI_WEIGHTAGE = "/employee/weighatge/limit";
	/* Ended by Pratibha T R */
	
	/* Added by Rajesh Kumar */
	public static final String GET_GSR_ADMIN_CONFIG_SETTINGS = "/gsr/adminConfigSettings/{slabId}";
	public static final String GSR_VIEW_CURRENT_GOALS = "/gsr/viewCurrentGoals";
	public static final String GSR_ENTER_ACCOMPLISHMENT = "/gsr/enterAccomplishment";
	public static final String GSR_DELEGATE_GOAL = "/gsr/delegateGoal";
	public static final String GSR_ACCEPT_RATING = "/gsr/acceptRating/employee/{empId}/slab/{slabId}";
	//public static final String GSR_CREATE_GOAL = "/gsr/createGoal";
	public static final String GSR_UPDATE_GOAL = "/gsr/updateGoal";
	public static final String GSR_DELETE_GOAL = "/gsr/deleteGoal/{pkGsrDatEmpGoalId}";
	public static final String GSR_VIEW_GOAL = "/gsr/viewGoal/{pkGsrDatEmpGoalId}";
	public static final String GET_GOALS_TO_DELEGATE_IN_HR = "/gsr/view/goals/delegate/hr";
	public static final String GET_EMPLOYEE_GOALS_DELEGATED = "/gsr/view/delegatedGoals";
	/* Ended by Rajesh Kumar */

	public static final String GET_EMPLOYEE_PERFORMANCE = "/gsr/rating/empid/{empId}";
	public static final String GET_EMPLOYEES_RATING_COUNT_BY_MGRID = "/gsr/ratingcount/rmId/{rmId}";
	public static final String GET_EMPLOYEES_RATING_COUNT_BY_DMID = "/gsr/ratingcount/dmId/{dmId}";
	public static final String GET_EMPLOYEES_RATING_COUNT_BY_BUID = "/gsr/ratingcount/buId/{buId}";
	public static final String GET_EMPLOYEES_RATING_COUNT_BY_HR = "/gsr/ratingcount/hr";
	public static final String GET_EMPLOYEES_DISAGREED_RATING_ON_BUID = "/gsr/rating/disagree/buId/{buId}";
	public static final String GET_EMPLOYEES_DISAGREED_RATING = "/gsr/rating/disagree";
	public static final String COPY_GOAL_FOR_RM_CHANGE = "/gsr/copygoal/newRmId/{newRmId}/empId/{empId}";

	// Added by Mani for GSR module service committed on 30-11-2016
	public static final String GOALS_SET_STATUS = "/gsr/employee/goalset/status";

	public static final String GOALS_STATUS_DIRECT_REPORTEES = "/gsr/RM/goalset/status";

	public static final String GOALS_STATUS_BU = "/gsr/BU/goalset/status";

	public static final String GOALS_STATUS_DOMAIN = "/gsr/DM/goalset/status";
	
	public static final String GOALS_STATUS_HR = "/gsr/HR/goalset/status";

	public static final String UPLOAD_NORMALIZED_RATING = "/gsr/normalised/rating/upload";

	public static final String NORMALISED_RATING_HISTORY = "/gsr/normalised/rating/history/{year}";

	public static final String IS_GOAL_CLOSED_FOR_PAST_THREE_SLAB = "/gsr/goalstatus/closed";

	public static final String OVERRIDEEN_FEEDBACK = "/gsr/feedback/overrideen";
	
	//public static final String CREATE_GOALS_LIST_OF_EMP = "/gsr/creategoals";
	//Commented by Shyam for enhancement of above service.
	public static final String UPDATE_GOALS_LIST_OF_EMP = "/gsr/updategoals";

	// End of addition by Mani for GSR module service committed on 30-11-2016
	// Added by Balaji
	public static final String IS_GOAL_CLOSED = "/gsr/goals/isclosed";
	public static final String IS_REPORTEES_GOALS_CLOSED = "/gsr/goals/isclosed/rmId";
	

	public static final String VIEW_TRAINING_REQUESTED = "/gsr/training/requested";

	public static final String REQUEST_FOR_TRAINING = "/gsr/request/training";

	public static final String ALLOW_GOAL_PERMISSIONS = "/gsr/goal/permission";
	// End of Addition
	
	
	//added by prathibha on 04/11/2016 for view past goals for empid for date range specified
	public static final String VIEW_PAST_GOALS_EMPID="/gsr/goals/employee/pastgoals";
			
	// Added by prathibha for View Employee Goals Within Domain, (inputs)DM empid, goal status, date range
	public static final String  VIEW_EMPLOYEE_GOALS_DOMAIN_BYDM="/gsr/viewdomaingoals/bydm";
		
	//Added by prathibha for View Employee goals within bu by bu head
	public static final String  VIEW_EMPLOYEE_GOALS_BYBUHEAD="/gsr/employeegoals/bybuh";
			
			
	// Added by prathibha after changes on 22/3/2017
			
	// view employee goals by DM passing DM ID adn Slab ID
	public static final String VIEW_GOALS_BY_DOMAINMGR="/gsr/viewgoals/bydm/dmid/slabid";
						
	public static final String VIEW_EMP_GOALS_BYBUHD="/gsr/employeegoals/bybuh";
						
	//Create GSR Weightages
	public static final String CREATE_GSR_WEIGHTAGES="/gsr/create/weightage";
						
	public static final String UPDATE_GSR_WEIGHTAGES="gsr/weightage/update";
			
	//Added by Kamal Anand
	public static final String VIEW_EMPLOYEE_GOALS_PAGENATION = "/gsr/employee/goals/page";
	
	public static final String GET_ALL_GOAL_STATUS = "/gsr/goal/status";
	
	public static final String VIEW_ALL_REPORTEES_GOALS_REPORTING_MANAGER = "/gsr/reportingmanager/goals/{mgrId}";
	
	public static final String VIEW_ALL_REPORTEES_GOALS = "/gsr/reportingmanager/goals";
	
	public static final String VIEW_ALL_EMPLOYEE_GOALS_HR = "/gsr/hr/employee/goals";
	
	public static final String GET_PREVIOUS_SLAB_PRENORMALIZED_RATING = "/gsr/employee/goal/previous/slab/{empId}";
	
	public static final String GET_CURRENT_AND_PREVIOUS_SLAB_DETAILS = "/gsr/slab/details";
	
	public static final String GET_ALL_SLABS_BASED_SLAB_YEAR = "/gsr/slab/details/year/{slabYear}";
	
	public static final String REPORTING_MANAGER_VIEW_TEAM_GOALS = "/gsr/reportingmanager/view/team/goals";
	
	public static final String HR_VIEW_TEAM_GOALS = "/gsr/hr/view/team/goals";
	
	public static final String GET_GOALS_TO_DELEGATE = "/gsr/view/goals/delegate";
	//End of Addition by Kamal Anand
	
	//Added by Shyam Lama
	public static final String DISAGREE_AND_RESOLVED_FINAL_RATING_UNDER_DM_LOGIN = "/gsr/finalRating/disagreeAndResolved/DomainMgr";
	public static final String DISAGREE_AND_RESOLVED_FINAL_RATING_UNDER_BU_HEAD_LOGIN = "/gsr/finalRating/disagreeAndResolved/BUHead";
	public static final String DISAGREE_AND_RESOLVED_FINAL_RATING_UNDER_SKIP_LEVEL_MANAGER = "/gsr/finalRating/disagreeAndResolved/skipLevelMgr";
	public static final String CREATE_A_NEW_GOAL_FOR_A_SLAB = "/gsr/createGoal";
	public static final String GIVEN_EMPLOYEE_ID_GSR_HIERARCHY_UNDER_HR_ROLE = "/gsr/hierarchyView/{employeeId}";
	public static final String CREATE_A_NEW_GOAL_BY_MANAGER_FOR_MULTIPLE_EMPLOYEE_AT_A_TIME ="/gsr/createGoalByMgr/multiReportees";
	//End of addition by Shyam Lama
			
	public static final String GET_CUSTOM_QUERY_DATA = "/gsr/custom/query/data";	
	
	public static final String GENERATE_GSR_REPORT = "/gsr/report/query/data";	
	
	public static final String GET_GSR_REPORT_INIT_LOAD_DATA = "/gsr/report/init";
	
	public static final String GET_GSR_REPORT_FILTER_CONFIG_TEMPLATE = "/gsr/report/config/template/{empId}";
			
}
