package com.thbs.mis.gsr.validator;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.gsr.bean.GsrCreateSlabBean;
import com.thbs.mis.gsr.bean.GsrSetWorkflowDatesBean;


public class GsrSlabValidator implements Validator {

	@Override
	public void validate(Object obj, Errors e) {
		GsrCreateSlabBean requestBeanSearch = (GsrCreateSlabBean) obj;
System.out.println("Entered validator");
		Date slab_start_date = requestBeanSearch.getGsrSlabStartDate();
		Date slab_end_date = requestBeanSearch.getGsrSlabEndDate();
		if (slab_start_date != null) {
			if (slab_end_date == null) {
				e.rejectValue("gsrSlabEndDate", "End date should not be null");
			} else {
				if (slab_end_date.before(slab_start_date)) {
					e.rejectValue("gsrSlabEndDate",
							"End date should greater than Start date");
				}
			
					else if(slab_end_date.equals(slab_start_date)){
						e.rejectValue("gsrSlabEndDate",
								"End date should greater than Start date");
					}
				}
				} else 
				{
					System.out.println("Condition Success");
				}
			}

		

	
	

	@Override
	public boolean supports(Class<?> clazz) {
		return GsrCreateSlabBean.class.equals(clazz);
	}
}
