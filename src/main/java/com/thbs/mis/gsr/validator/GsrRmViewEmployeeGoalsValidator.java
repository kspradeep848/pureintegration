package com.thbs.mis.gsr.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.gsr.bean.ViewEmployeeGoalsInputBean;

public class GsrRmViewEmployeeGoalsValidator implements Validator
{

	@Override
	public boolean supports(Class<?> clazz)
	{
		return ViewEmployeeGoalsInputBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e)
	{
		ViewEmployeeGoalsInputBean empBean = (ViewEmployeeGoalsInputBean) obj;
		
		if(empBean.getMgrId() == null)
		{
			e.rejectValue("mgrId", "Manager Id is a Mandatory Field");
		}
		if(empBean.getEmpIdList() != null)
		{
			if(empBean.getEmpIdList().size() == 0)
			{
				e.rejectValue("empIdList", "Please give employee id's");
			}
		}
	}

}
