
	package com.thbs.mis.gsr.validator;

	import java.util.Date;

	import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


import com.thbs.mis.gsr.bean.GsrTeamGoalDetailsBean;



	public class GsrTeamGoalsValidator implements Validator {

		@Override
		public void validate(Object obj, Errors e) {
			GsrTeamGoalDetailsBean requestBeanSearch = (GsrTeamGoalDetailsBean) obj;
	System.out.println("Entered validator");
			Date start_date = requestBeanSearch.getStartDate();
			Date end_date = requestBeanSearch.getEndDate();
			/*ValidationUtils.rejectIfEmptyOrWhitespace(e, "empStartDate",
					"Start date should not be null");
			ValidationUtils.rejectIfEmptyOrWhitespace(e, "empEndDate",
					"End date should not be null");*/
			if (start_date != null) {
				if (end_date == null) {
					e.rejectValue("endDate", "End date should not be null");
				} else {
					if (end_date.before(start_date)) {
						e.rejectValue("endDate",
								"End date should greater than Start date");

					} else {
						System.out.println("Condition Success");
					}
				}

			}

		}

		@Override
		public boolean supports(Class<?> clazz) {
			return GsrTeamGoalDetailsBean.class.equals(clazz);
		}
	}

