package com.thbs.mis.gsr.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.gsr.bean.ForceClosedGoalsInputBean;

public class ForceClosedGoalsInputBeanValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return ForceClosedGoalsInputBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		ForceClosedGoalsInputBean forceCloseGoalsBean = (ForceClosedGoalsInputBean) obj;

		if (forceCloseGoalsBean.getEmpId() == null) {
			e.rejectValue("empId", "Employee Id is a mandatory field");
		}
		if (forceCloseGoalsBean.getSlabId() == null) {
			e.rejectValue("slabId", "Slab Id is a mandatory field");
		}
		if (forceCloseGoalsBean.getClosedByEmpId() == null) {
			e.rejectValue("closedByEmpId",
					"Closed By Employee Id is a mandatory field");
		}

	}

}
