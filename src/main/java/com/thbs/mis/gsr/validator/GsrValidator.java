
	package com.thbs.mis.gsr.validator;

	import java.util.Date;

	import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.thbs.mis.gsr.bean.GsrEmployeeGoalDetailsBean;



	public class GsrValidator implements Validator {

		@Override
		public void validate(Object obj, Errors e) {
			GsrEmployeeGoalDetailsBean requestBeanSearch = (GsrEmployeeGoalDetailsBean) obj;
	System.out.println("Entered validator");
			Date emp_start_date = requestBeanSearch.getEmpStartDate();
			Date emp_end_date = requestBeanSearch.getEmpEndDate();
			/*ValidationUtils.rejectIfEmptyOrWhitespace(e, "empStartDate",
					"Start date should not be null");
			ValidationUtils.rejectIfEmptyOrWhitespace(e, "empEndDate",
					"End date should not be null");*/
			if (emp_start_date != null) {
				if (emp_end_date == null) {
					e.rejectValue("empEndDate", "End date should not be null");
				} else {
					if (emp_end_date.before(emp_start_date)) {
						e.rejectValue("empEndDate",
								"End date should greater than Start date");

					} else {
						System.out.println("Condition Success");
					}
				}

			}

		}

		@Override
		public boolean supports(Class<?> clazz) {
			return GsrEmployeeGoalDetailsBean.class.equals(clazz);
		}
	}

