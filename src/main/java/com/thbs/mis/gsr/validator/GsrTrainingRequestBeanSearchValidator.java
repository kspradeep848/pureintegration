package com.thbs.mis.gsr.validator;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.gsr.bean.GsrTrainingRequestSearchBean;

public class GsrTrainingRequestBeanSearchValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return GsrTrainingRequestSearchBean.class.equals(clazz);
	}
	
	@Override
	public void validate(Object obj, Errors e) {
		GsrTrainingRequestSearchBean requestBeanSearch = (GsrTrainingRequestSearchBean) obj;
		Date start_date = requestBeanSearch.getStartDate();
		Date end_date = requestBeanSearch.getEndDate();
		if (start_date != null) {
			if (end_date == null) {
				e.rejectValue("endDate", "End date should not be null");
			} else {
				if (end_date.before(start_date)) {
					e.rejectValue("endDate",
							"End date should greater than Start date");

				} else {
					System.out.println("Condition Success");
				}
			}

		}

	}

	
}
