package com.thbs.mis.gsr.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.gsr.bean.DelegationGoalsInputBean;

public class DelegationGoalsInputBeanValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz)
	{
		return DelegationGoalsInputBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e)
	{
		DelegationGoalsInputBean empBean = (DelegationGoalsInputBean) obj;
		
		if(empBean.getDelegationMgrId() == null)
		{
			e.rejectValue("delegationMgrId", "delegationMgrId is a mandatory field");
		}
	}

}
