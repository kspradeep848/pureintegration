package com.thbs.mis.gsr.validator;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.gsr.bean.GsrViewCurrentGoalsBean;

public class GsrViewCurrentGoalBeanValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return GsrViewCurrentGoalsBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		// TODO Auto-generated method stub
		GsrViewCurrentGoalsBean requestBeanSearch = (GsrViewCurrentGoalsBean) obj;
		Date start_date = requestBeanSearch.getgsrGoalStartDate();
		Date gsrGoalEndDate = requestBeanSearch.getgsrGoalEndDate();
		if (start_date != null) {
			if (gsrGoalEndDate == null) {
				e.rejectValue("gsrGoalEndDate", "End date should not be null");
			} else {
				if (gsrGoalEndDate.before(start_date)) {
					e.rejectValue("gsrGoalEndDate",
							"End date is lesser than Start date");

				} else {
					System.out.println("Condition Success");
				}
			}

		}

	}
}
