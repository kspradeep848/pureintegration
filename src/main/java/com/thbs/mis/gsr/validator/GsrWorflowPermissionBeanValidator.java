package com.thbs.mis.gsr.validator;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.framework.util.EntityBeanList;
import com.thbs.mis.gsr.bean.GsrWorflowPermissionBean;

public class GsrWorflowPermissionBeanValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return EntityBeanList.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {

		EntityBeanList<GsrWorflowPermissionBean> beanTest = (EntityBeanList<GsrWorflowPermissionBean>) obj;

		System.out.println("Entering Validator 1");

		for (GsrWorflowPermissionBean bean : beanTest.getEntityList()) {
			validate(bean, errors);
		}
	}

	private void validate(GsrWorflowPermissionBean bean, Errors e) {
		Date startDate = bean.getGsrWorkflowPermissionStartDate();
		Date endDate = bean.getGsrWorkflowPermissionEndDate();
		if (startDate != null) {
			if (endDate == null) {
				e.rejectValue("entityList", "End date should not be null");
			} else {
				if (endDate.before(startDate)) {
					e.rejectValue("entityList",
							"End date should greater than Start date");
				} else {
					System.out.println("Condition Success");
				}
			}
		}
		
		System.out.println("Exited ");
	}

}
