package com.thbs.mis.gsr.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.gsr.bean.HrViewTeamGoalsInputBean;

public class HrViewTeamGoalsInputBeanValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return HrViewTeamGoalsInputBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		HrViewTeamGoalsInputBean teamGoals = (HrViewTeamGoalsInputBean) obj;

		/*if (teamGoals.getMgrId() == null) {
			e.rejectValue("mgrId", "Manager Id is a mandatory field");
		}*/
	}

}
