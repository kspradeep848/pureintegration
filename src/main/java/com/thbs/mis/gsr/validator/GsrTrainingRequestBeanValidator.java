package com.thbs.mis.gsr.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.gsr.bean.GsrTrainingRequestBean;

public class GsrTrainingRequestBeanValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return GsrTrainingRequestBean.class.equals(clazz);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		System.out.println("Entering Validator");
	}

}
