package com.thbs.mis.gsr.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.thbs.mis.gsr.bean.GsrCreateGoalBean;

public class GsrCreateGoalBeanValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return GsrCreateGoalBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		System.out.println("Entered validator 2 ");
		GsrCreateGoalBean gsrCreateGoalBean = (GsrCreateGoalBean) obj;

		/*ValidationUtils.rejectIfEmptyOrWhitespace(e, "gsrGoalStartDate",
				"Start Date should not be Null");
		ValidationUtils.rejectIfEmptyOrWhitespace(e, "gsrGoalEndDate",
				"End Date should not be Null");
*/
		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = sm.format(gsrCreateGoalBean.getGsrGoalStartDate());
		String eDate = sm.format(gsrCreateGoalBean.getGsrGoalEndDate());
		/*if(strDate == null)
		{
			e.rejectValue("gsrGoalStartDate", "Start date should not be null");

		}
		if(eDate == null)
		{
			e.rejectValue("gsrGoalEndDate", "End date should not be null");

		}*/
		
		Date startDate = null;
		try {
			startDate = sm.parse(strDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		Date endDate = null;
		try {
			endDate = sm.parse(eDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		if (startDate != null) {
			if (endDate == null) {
				e.rejectValue("gsrGoalEndDate", "End date should not be null");
			} else {
				if (endDate.before(startDate)) {
					e.rejectValue("gsrGoalEndDate",
							"End date should greater than Start date");

				} else {
					System.out.println("Condition Success");
				}
			}

		}
		/*if (startDate == null) {
			e.rejectValue("gsrGoalStartDate", "End date should not be null");
		}

		if (endDate == null) {
			e.rejectValue("gsrGoalEndDate", "End date should not be null");
		}*/
	}
}
