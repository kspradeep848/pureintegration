package com.thbs.mis.gsr.validator;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.thbs.mis.gsr.bean.GsrEmployeeGoalDetailsBeanPagenation;

public class GsrViewEmployeeGoalsValidator implements Validator
{
	

	@Override
	public boolean supports(Class<?> clazz) {
		return GsrEmployeeGoalDetailsBeanPagenation.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e)
	{
		GsrEmployeeGoalDetailsBeanPagenation empBean = (GsrEmployeeGoalDetailsBeanPagenation) obj;
		Date emp_start_date = empBean.getGoalStartDate();
		Date emp_end_date = empBean.getGoalEndDate();
		if (emp_start_date != null) {
			if (emp_end_date == null) {
				e.rejectValue("goalEndDate", "End date should not be null");
			} else {
				if (emp_end_date.before(emp_start_date)) {
					e.rejectValue("empEndDate",
							"End date should greater than Start date");
				} else {
					System.out.println("Condition Success");
				}
			}
		}
		if (empBean.getPageNumber() == null) {
			e.rejectValue("pageNumber", "Page Number is a mandatory field");
		}
		if (empBean.getPageSize() == null) {
			e.rejectValue("pageSize", "Page Size is a mandatory field");
		}
	}
}
