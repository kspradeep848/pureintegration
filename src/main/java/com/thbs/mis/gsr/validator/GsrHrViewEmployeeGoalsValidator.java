package com.thbs.mis.gsr.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.gsr.bean.HrViewEmpGoalsInputBean;

public class GsrHrViewEmployeeGoalsValidator implements Validator
{

	@Override
	public boolean supports(Class<?> clazz)
	{
		return HrViewEmpGoalsInputBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e)
	{
		HrViewEmpGoalsInputBean empBean = (HrViewEmpGoalsInputBean) obj;
		
		if(empBean.getBuId() == null && empBean.getEmpIdList() == null && empBean.getGoalStatus() == null && empBean.getSlabId() == null)
		{
			e.rejectValue("All", "Please provide value for any one field");
		}
		if(empBean.getEmpIdList() != null)
		{
			if(empBean.getEmpIdList().size() == 0)
			{
				e.rejectValue("empIdList", "Please give employee id's");
			}
		}
	}

}
