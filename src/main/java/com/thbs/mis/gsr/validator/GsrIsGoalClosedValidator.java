package com.thbs.mis.gsr.validator;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.gsr.bean.GsrGoalsClosedBean;

public class GsrIsGoalClosedValidator implements Validator{

	@Override
	public void validate(Object obj, Errors e) {
		GsrGoalsClosedBean requestBeanSearch = (GsrGoalsClosedBean) obj;
		System.out.println("Entered GsrIsGoalClosedValidator validator");
		Date emp_start_date = requestBeanSearch.getGsrGoalStartDate();
		Date emp_end_date = requestBeanSearch.getGsrGoalEndDate();
		/*
		 * ValidationUtils.rejectIfEmptyOrWhitespace(e, "empStartDate",
		 * "Start date should not be null");
		 * ValidationUtils.rejectIfEmptyOrWhitespace(e, "empEndDate",
		 * "End date should not be null");
		 */
		if(emp_start_date != null && emp_end_date != null)
		{
				if (emp_end_date.before(emp_start_date)) {
					e.rejectValue("gsrGoalEndDate",
							"End date should greater than Start date");

				} 
			}
	}
	@Override
	public boolean supports(Class<?> clazz) {
		return GsrGoalsClosedBean.class.equals(clazz);
	}
}
