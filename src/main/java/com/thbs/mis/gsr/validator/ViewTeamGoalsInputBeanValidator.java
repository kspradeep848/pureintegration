package com.thbs.mis.gsr.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.gsr.bean.ViewTeamGoalsInputBean;

public class ViewTeamGoalsInputBeanValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return ViewTeamGoalsInputBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		ViewTeamGoalsInputBean teamGoals = (ViewTeamGoalsInputBean) obj;

		if (teamGoals.getMgrId() == null && teamGoals.getSlabId() == null && teamGoals.getEmpIdList() == null) {
			e.rejectValue("mgrId", "Any one of the input is a mandatory");
		}
	}

}
