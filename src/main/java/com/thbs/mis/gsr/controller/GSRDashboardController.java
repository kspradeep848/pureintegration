/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRDashboardController.java           			 */
/*                                                                   */
/*  Author      :  Anil kumar B K	                                 */
/*                                                                   */
/*  Date        :  24-Nov-2016                                       */
/*                                                                   */
/*  Description :  Controller class for GSR dash board    			 */
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 24-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.common.bean.BusinessUnitBean;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bean.GsrDisagreeBean;
import com.thbs.mis.gsr.bean.GsrEmployeeBean;
import com.thbs.mis.gsr.bean.GsrEmployeePerformanceRatingBean;
import com.thbs.mis.gsr.bean.GsrEmployeesRatingBean;
import com.thbs.mis.gsr.bean.GsrEmployeesRatingForHrBean;
import com.thbs.mis.gsr.bean.GsrReporteesBean;
import com.thbs.mis.gsr.constants.GsrURIConstants;
import com.thbs.mis.gsr.service.GsrDashboardService;

@Api(value = "Dashboard services of GSR", description = "Operations Related to the GSR Dashboard", basePath = "http://localhost:8080/", consumes = "Rest Service", position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "Dashboard services of GSR")
@Controller
public class GSRDashboardController {

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(GSRDashboardController.class);

	/**
	 * Instance of <code>GsrDashboardService</code>
	 */
	@Autowired
	private GsrDashboardService gsrDashboardService;

	/**
	 * Instance of <code>GsrDashboardService</code>
	 */
	@Autowired
	private CommonService commonService;

	/**
	 * <Description getEmployeePerformance:> This method is used to return the
	 * GSR ratings of an employee for last four slab's
	 * 
	 * @param empId
	 * @return ratings of an employee of past four slab's
	 * @throws DataAccessException
	 */
	@ApiOperation(tags = "GSR", value = "Employee performance graph", httpMethod = "GET", notes = "This Service has been implemented to get GSR ratings of an employee for last four slab's."

			+ "<br>The employee Id send in the request."

			+ "<br>The Response will be the ratings of an employee of past four slab's."

			+ "<br><b>Inputs :</b><br> employee Id. <br><br>", nickname = "GSR", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrEmployeePerformanceRatingBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrEmployeePerformanceRatingBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The Employee performance graph", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The Employee performance graph", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_EMPLOYEE_PERFORMANCE, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getEmployeePerformance(
			@ApiParam(name = "empId", example = "5", value = "Employee performance graph") @PathVariable Integer empId)
			throws DataAccessException {
		LOG.startUsecase("getEmployeePerformance");
		GsrEmployeeBean result = new GsrEmployeeBean();
		List<GsrEmployeeBean> finalresult = new ArrayList<GsrEmployeeBean>();
		Byte status = 0;

		status = commonService.getEmployeeLoginStatus(empId);

		if (status == 2) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Inactive employee"));
		} else if (status == 0) {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Give proper input, Given employee Id does not exist"));
		}
		try {

			result = gsrDashboardService.getEmployeePerformance(empId);
			finalresult.add(result);

		} catch (Exception e) {
			throw new DataAccessException(
					"Error while getting getEmployeePerformance", e);
		}
		LOG.endUsecase("getEmployeePerformance");

		if (finalresult.size() != 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Employee GSR performance retrived successfully",
							finalresult));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"There is no final rating for given employee"));
		}

	}

	/**
	 * <Description getEmployeePerformance:> This method is used to return count
	 * of GSR ratings of all reportee's of a Reporting manager for last four
	 * slab's
	 * 
	 * @param rmId
	 * @return count of ratings of all reportee's for the past four slab's
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	@ApiOperation(tags = "GSR", value = "Performance graph of all reportees of Reporting manager", httpMethod = "GET",

	notes = "This Service has been implemented to get count of GSR ratings of all reportees of a Reporting manager for last four slab's."

			+ "<br>The reporing manager Id send in the request."

			+ "<br>The Response will be count of ratings of all reportees for the past four slab's."

			+ "<br><b>Inputs :</b><br> Reporting manager Id. <br><br>", nickname = "GSR", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrEmployeesRatingBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrEmployeesRatingBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The Employee performance graph", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The Employee performance graph", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_EMPLOYEES_RATING_COUNT_BY_MGRID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getCountOfRatingOfAllReporteeesOfRM(
			@ApiParam(name = "rmId", example = "2", value = "Performance graph of all reportees of Reporting manager") @PathVariable Integer rmId)
			throws DataAccessException, CommonCustomException {
		LOG.startUsecase("getEmployeesRatingCountByRmId");

		GsrReporteesBean reporteesBean = new GsrReporteesBean();
		List<GsrReporteesBean> finalresult = new ArrayList<GsrReporteesBean>();
		Byte status = 0;

		status = commonService.getEmployeeLoginStatus(rmId);

		if (status == 2) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Inactive employee"));
		} else if (status == 0) {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Give proper input, Given employee Id does not exist"));
		}
		try {
			reporteesBean = gsrDashboardService
					.getEmployeesRatingCountByRmId(rmId);
			finalresult.add(reporteesBean);

		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("getEmployeesRatingCountByRmId");
		if (finalresult.size() != 0) {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(
							HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"RM reportees GSR performance retrived successfully",
							finalresult));
		} else {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"There is no final rating for given Reporting manager reportees"));
		}

	}

	/**
	 * <Description getEmployeePerformance:> This method is used to return count
	 * of GSR ratings of all reportee's of a Domain manager for last four slab's
	 * 
	 * @param dmId
	 * @return count of ratings of all reportees for the past four slab's
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */

	@ApiOperation(tags = "GSR", value = "Performance graph of all reportees of Domain manager", httpMethod = "GET",

	notes = "This Service has been implemented to get count of GSR ratings of all reportees of a Domain manager for last four slab's."

			+ "<br>The Domain manager Id send in the request."

			+ "<br>The Response will be count of ratings of all reportees for the past four slab's."

			+ "<br><b>Inputs :</b><br> Domain manager Id. <br><br>", nickname = "GSR", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrEmployeesRatingBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrEmployeesRatingBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The Employee performance graph", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The Employee performance graph", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_EMPLOYEES_RATING_COUNT_BY_DMID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getCountOfRatingOfAllReporteeesOfDM(
			@ApiParam(name = "dmId", example = "2", value = "Performance graph of all reportees of Domain manager") @PathVariable Integer dmId)
			throws DataAccessException, CommonCustomException {
		LOG.startUsecase("getEmployeesRatingCountByDmId");

		GsrReporteesBean reporteesBean = new GsrReporteesBean();
		List<GsrReporteesBean> finalresult = new ArrayList<GsrReporteesBean>();
		Byte status = 0;

		status = commonService.getEmployeeLoginStatus(dmId);

		if (status == 2) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Inactive employee"));
		} else if (status == 0) {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Give proper input, Given employee Id does't exist"));
		}
		try {
			reporteesBean = gsrDashboardService
					.getEmployeesRatingCountByDmId(dmId);
			finalresult.add(reporteesBean);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("getEmployeesRatingCountByDmId");
		if (finalresult.size() != 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Employees GSR ratings retrived successfully",
							finalresult));
		} else {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"There is no final rating for given Domain mgr reportees"));
		}

	}

	/**
	 * <Description getEmployeePerformance:> This method is used to return count
	 * of GSR ratings of all reportee's of a Business unit for last four slab's
	 * 
	 * @param buId
	 * @return count of ratings of all reportees for the past four slab's
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	@ApiOperation(tags = "GSR", value = "Performance graph of all reportees of Business unit", httpMethod = "GET",

	notes = "This Service has been implemented to get count of GSR ratings of all reportees of Business unit for last four slab's."

			+ "<br>The Business unit Id send in the request."

			+ "<br>The Response will be count of ratings of all reportees for the past four slab's."

			+ "<br><b>Inputs :</b><br> Business unit Id. <br><br>", nickname = "GSR", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrEmployeesRatingBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrEmployeesRatingBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Performance graph of all reportees of Business unit", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Performance graph of all reportees of Business unit", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_EMPLOYEES_RATING_COUNT_BY_BUID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getCountOfRatingOfAllReporteeesOfBU(
			@ApiParam(name = "buId", example = "29", value = "Performance graph of all reportees of Business unit") @PathVariable Short buId)
			throws DataAccessException, CommonCustomException {

		LOG.startUsecase("getEmployeesRatingCountByBuId");

		GsrReporteesBean reporteesBean = new GsrReporteesBean();
		List<GsrReporteesBean> finalresult = new ArrayList<GsrReporteesBean>();
		try {

			boolean buExist = false;

			List<BusinessUnitBean> buList = commonService
					.getAllActiveBusinessUnits();
			for (BusinessUnitBean bu : buList) {

				if (bu.getPkBuUnitId() == buId) {
					buExist = true;
					break;
				}
			}
			if (buExist) {
			reporteesBean = gsrDashboardService
						.getEmployeesRatingCountByBuId(buId);
				finalresult.add(reporteesBean);

			} else {
				MasBuUnitBO businessUnitBean = commonService.getBuDetailsById(buId);
				
				if(businessUnitBean!=null){
				if(businessUnitBean.getActiveFlag().equals("INACTIVE")){
					return ResponseEntity
							.status(HttpStatus.OK.value())
							.body(new MISResponse(HttpStatus.OK.value(),
									MISConstants.SUCCESS,
									"Give proper input, Given BU Id is inactive"));
				}
				}else{
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Give proper input, Given BU Id does not exist"));
				}
				

			}

		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("getEmployeesRatingCountByBuId");
		if (finalresult.size() != 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Employees GSR ratings retrived successfully",
							finalresult));
		} else {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"There is no final rating for given Business unit employees"));
		}

	}

	/**
	 * <Description getEmployeePerformance:> This method is used to return count
	 * of GSR ratings of all reportee's of a HR for last four slab's
	 * 
	 * @return
	 * @throws DataAccessException
	 */

	@ApiOperation(tags = "GSR", value = "Performance graph of all reportees of HR", httpMethod = "GET",

	notes = "This Service has been implemented to get count of GSR ratings of all reportees of HR for last four slab's."

			+ "<br>The Response will be count of ratings of all reportees for the past four slab's."

			+ "<br><b>Inputs :</b><br> No input. <br><br>", nickname = "GSR", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrEmployeesRatingBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrEmployeesRatingBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Performance graph of all reportees of HR", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Performance graph of all reportees of HR", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_EMPLOYEES_RATING_COUNT_BY_HR, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getCountOfRatingOfAllReporteeesOfHR()
			throws DataAccessException {

		LOG.startUsecase("getEmployeesRatingCountByHr");
		List<GsrEmployeesRatingForHrBean> listOfEmployeesRating = new ArrayList<GsrEmployeesRatingForHrBean>();
		GsrEmployeesRatingForHrBean employeesRatingForHr = new GsrEmployeesRatingForHrBean();
		try {

			employeesRatingForHr = gsrDashboardService
					.getEmployeesRatingCountByHr();
			listOfEmployeesRating.add(employeesRatingForHr);
		} catch (Exception e) {
			throw new DataAccessException(
					"Error while getting getEmployeesRatingCountByBuId", e);
		}
		LOG.endUsecase("getEmployeesRatingCountByHr");
		if (listOfEmployeesRating.size() != 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Employees GSR ratings retrived successfully",
							listOfEmployeesRating));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"There is no final rating for employees"));
		}

	}

	/**
	 * <Description getEmployeePerformance:> This method is used to return count
	 * of Disagreed GSR ratings of all reportee's for last four slab's
	 * 
	 * @return
	 * @throws DataAccessException
	 * @throws CommonCustomException 
	 */

	@ApiOperation(tags = "GSR", value = "Performance graph of all reportees of disagreed gsr ratings Business unit wise", httpMethod = "GET",

	notes = "This Service has been implemented to get count of GSR ratings of BU's disagreed gsr ratings for slabs"

			+ "<br>The Response will be count of ratings of BU reportees for the past four slab's."

			+ "<br><b>Inputs :</b><br> No input. <br><br>", nickname = "GSR", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDisagreeBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDisagreeBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Performance graph of all reportees of HR", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Performance graph of all reportees of HR", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_EMPLOYEES_DISAGREED_RATING_ON_BUID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getDisagreeOfRatingOfReporteeesOnBuId(
			@ApiParam(name = "buId", example = "24", value = "Performance graph of all reportees of Business unit") @PathVariable Short buId)
			throws DataAccessException, CommonCustomException {

		LOG.startUsecase("getDisagreeOfRatingOfAllReporteees");
		GsrDisagreeBean gsrDisagreeBean = new GsrDisagreeBean();
		List<GsrDisagreeBean> listOfGsrDisagreeRating = new ArrayList<GsrDisagreeBean>();
		try {

			boolean buExist = false;

			List<BusinessUnitBean> buList = commonService
					.getAllActiveBusinessUnits();
			for (BusinessUnitBean bu : buList) {

				if (bu.getPkBuUnitId() == buId) {
					buExist = true;
					break;
				}
			}
			
			if (buExist) {
			gsrDisagreeBean = gsrDashboardService
					.getDisagreedRatingOfAllReporteeesOnBuId(buId);
			if(gsrDisagreeBean.getListOfGsrDisagree().size() !=0){
			listOfGsrDisagreeRating.add(gsrDisagreeBean);
			}
			
			}
			 else {
					MasBuUnitBO businessUnitBean = commonService.getBuDetailsById(buId);
					
					if(businessUnitBean!=null){
					if(businessUnitBean.getActiveFlag().equals("INACTIVE")){
						return ResponseEntity
								.status(HttpStatus.OK.value())
								.body(new MISResponse(HttpStatus.OK.value(),
										MISConstants.SUCCESS,
										"Give proper input, Given BU Id is inactive"));
					}
					}else{
					return ResponseEntity
							.status(HttpStatus.OK.value())
							.body(new MISResponse(HttpStatus.OK.value(),
									MISConstants.SUCCESS,
									"Give proper input, Given BU Id does not exist"));
					}
					

				}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("getEmployeesRatingCountByHr");
		if (listOfGsrDisagreeRating.size() != 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Disagreed GSR rating data retrived successfully",
							listOfGsrDisagreeRating));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"There is no Disagreed final rating for given Business unit Id"));
		}

	}
	
	/**
	 * <Description getEmployeePerformance:> This method is used to return count
	 * of Disagreed GSR ratings of all reportee's for last four slab's
	 * 
	 * @return
	 * @throws DataAccessException
	 */

	@ApiOperation(tags = "GSR", value = "Performance graph of all reportees of disagreed gsr ratings", httpMethod = "GET",

	notes = "This Service has been implemented to get count of GSR ratings of all disagreed gsr ratings for slabs"

			+ "<br>The Response will be count of ratings of all reportees for the past four slab's."

			+ "<br><b>Inputs :</b><br> No input. <br><br>", nickname = "GSR", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDisagreeBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDisagreeBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Performance graph of all reportees of HR", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Performance graph of all reportees of HR", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_EMPLOYEES_DISAGREED_RATING, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getDisagreeOfRatingOfAllReporteees()
			throws DataAccessException {

		LOG.startUsecase("getDisagreeOfRatingOfAllReporteees");
		GsrDisagreeBean gsrDisagreeBean = new GsrDisagreeBean();
		List<GsrDisagreeBean> listOfGsrDisagreeRating = new ArrayList<GsrDisagreeBean>();
		try {

			gsrDisagreeBean = gsrDashboardService
					.getDisagreeOfRatingOfAllReporteees();
			listOfGsrDisagreeRating.add(gsrDisagreeBean);
		} catch (Exception e) {
			throw new DataAccessException(
					"Error while getting getDisagreeOfRatingOfAllReporteees", e);
		}
		LOG.endUsecase("getEmployeesRatingCountByHr");
		if (listOfGsrDisagreeRating.size() != 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Disagreed GSR rating data retrived successfully",
							listOfGsrDisagreeRating));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"There is no final rating for employees"));
		}

	}
}
