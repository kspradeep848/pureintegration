package com.thbs.mis.gsr.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bean.GsrHierarchyBean;
import com.thbs.mis.gsr.constants.GsrURIConstants;
import com.thbs.mis.gsr.service.GSRHierarchyViewService;

@Controller
public class GSRHierarchyViewController {
	
	/**`
	 * Instance of <code>Log</code>
	 */
	@SuppressWarnings("unused")
	private static final AppLog LOG = LogFactory.getLog(GSRHierarchyViewController.class);
	
	@Autowired
	private GSRHierarchyViewService gsrHierarchyViewService;
	
	//Added by Shyam Lama
	/**
	 * This service method is used for the retrieval of GSR hierarchy list. 
	 * @param Integer employeeId
	 * @return List<GsrHierarchyBean> 
	 * @throws BusinessException if is there present any Business exception in the service.
	 * @throws DataAccessException if there is present any Data Access Exception
	 */
		
	@ApiOperation(tags="GSR",value = "Get the GSR hierarchy structure of provided employee id. "
						+ "This service will be called from the front-end when the user wants to view the hierarchy structure of an employee", 
				httpMethod = "GET", 
				notes = "This Service has been implemented to fetch the GSR hierarchy structure list of provided employee id.",
	nickname = "View GSR Hieararchy Structure",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrHierarchyBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrHierarchyBean.class),
			@ApiResponse(code = 201, message = "List of GSR hierarchy structure details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "GSR Hierarchy Structure Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "GSR Hierarchy Structure Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
		
	@RequestMapping(value = GsrURIConstants.GIVEN_EMPLOYEE_ID_GSR_HIERARCHY_UNDER_HR_ROLE, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> showGsrHierarchyOfGivenEmployeeIdUnderHr(
			@ApiParam(name = "Employee Id", example = "2188", value = "View Employee Id") 
			@PathVariable Integer employeeId)throws BusinessException, DataAccessException 
	{
		List<GsrHierarchyBean> hierarchyList = new ArrayList<GsrHierarchyBean>();
		GsrHierarchyBean hierarchyBean = new GsrHierarchyBean();
		try {
			hierarchyBean = gsrHierarchyViewService.getGsrHierarchyStructureOfGivenEmp(employeeId);		
		} catch (Exception e) {
			throw new BusinessException("Failed while fetching gsr hierarchy view under HR", e);
		}
		
		if(hierarchyBean != null){
			hierarchyList.add(hierarchyBean);
		}
		
		if(!hierarchyList.isEmpty())
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
						"GSR Hierarchy struture of provided employee id has been retrieved successfully", hierarchyList));
		}else{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
						"Failed to retrieve GSR hierarchy structure of provided employee id", hierarchyList));
		}
	}
		

}
