/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRGoalController.java                            */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  October 28, 2016                                  */
/*                                                                   */
/*  Description :  CRUD operations for Goals                         */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* October 28, 2016     THBS     1.0        Initial version created  */
/*********************************************************************/

package com.thbs.mis.gsr.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.framework.constants.LegacyMISResponse;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.constants.Response;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.ConfigException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.exception.GoalNotFoundException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.EntityBeanList;
import com.thbs.mis.framework.util.ValidationUtils;
import com.thbs.mis.gsr.bean.BUHeadreporteesgoalinputbean;
import com.thbs.mis.gsr.bean.BUHeadreporteesgoaloutputbean;
import com.thbs.mis.gsr.bean.DMEmpGoalInputBean;
import com.thbs.mis.gsr.bean.DMViewEmpGoalsOutputBean;
import com.thbs.mis.gsr.bean.DelegationGoalInputBeanHR;
import com.thbs.mis.gsr.bean.DelegationGoalsBean;
import com.thbs.mis.gsr.bean.DelegationGoalsInputBean;
import com.thbs.mis.gsr.bean.FetchPastGoalsForEmployeeListInputBean;
import com.thbs.mis.gsr.bean.ForceClosedGoalsInputBean;
import com.thbs.mis.gsr.bean.GSRDelegatedEmpGoalBean;
import com.thbs.mis.gsr.bean.GsrAcceptDelegatedGoalsBean;
import com.thbs.mis.gsr.bean.GsrApproveAndRejectGoalBean;
import com.thbs.mis.gsr.bean.GsrApproveGoalBean;
import com.thbs.mis.gsr.bean.GsrCreateDelegatedGoalOutputBean;
import com.thbs.mis.gsr.bean.GsrCreateGoalInputBean;
import com.thbs.mis.gsr.bean.GsrCreateSlabBean;
import com.thbs.mis.gsr.bean.GsrDatEmpGoalBOBeanInput;
import com.thbs.mis.gsr.bean.GsrDelegateGoalBean;
import com.thbs.mis.gsr.bean.GsrDelegetedOutputBean;
import com.thbs.mis.gsr.bean.GsrDisagreeRatingBean;
import com.thbs.mis.gsr.bean.GsrEmployeeGoalDetailsBean;
import com.thbs.mis.gsr.bean.GsrEmployeeGoalDetailsBeanPagenation;
import com.thbs.mis.gsr.bean.GsrEnableSettingsBean;
import com.thbs.mis.gsr.bean.GsrEnterAccomplishmentBean;
import com.thbs.mis.gsr.bean.GsrEnterFeedbackBean;
import com.thbs.mis.gsr.bean.GsrEnterFeedbackForManagerBean;
import com.thbs.mis.gsr.bean.GsrFetchPastGoalsForEmployeeOutputBean;
import com.thbs.mis.gsr.bean.GsrGoalClosedStatusBean;
import com.thbs.mis.gsr.bean.GsrGoalOutputBean;
import com.thbs.mis.gsr.bean.GsrGoalOutputForMultipleReporteesBean;
import com.thbs.mis.gsr.bean.GsrGoalSetStatusBean;
import com.thbs.mis.gsr.bean.GsrGoalsClosedBean;
import com.thbs.mis.gsr.bean.GsrKpiWeightageInputBean;
import com.thbs.mis.gsr.bean.GsrKpiWeightageOutputBean;
import com.thbs.mis.gsr.bean.GsrMaximumKpiOutputBean;
import com.thbs.mis.gsr.bean.GsrMaximumKpiWeightageBean;
import com.thbs.mis.gsr.bean.GsrReopenGoalsByHrBean;
import com.thbs.mis.gsr.bean.GsrSetWorkflowDatesBean;
import com.thbs.mis.gsr.bean.GsrTeamGoalDetailsBean;
import com.thbs.mis.gsr.bean.GsrTeamGoalStatusBean;
import com.thbs.mis.gsr.bean.GsrTrainingRequestBean;
import com.thbs.mis.gsr.bean.GsrTrainingRequestSearchBean;
import com.thbs.mis.gsr.bean.GsrUpdateCommonGoalsBean;
import com.thbs.mis.gsr.bean.GsrUpdateGoalInputBean;
import com.thbs.mis.gsr.bean.GsrViewCurrentGoalsBean;
import com.thbs.mis.gsr.bean.GsrWorflowPermissionBean;
import com.thbs.mis.gsr.bean.HrViewEmpGoalsInputBean;
import com.thbs.mis.gsr.bean.HrViewEmployeeGoalsOutputBean;
import com.thbs.mis.gsr.bean.HrViewTeamGoalsInputBean;
import com.thbs.mis.gsr.bean.PreNormalizedRatingBean;
import com.thbs.mis.gsr.bean.RmViewEmployeeGoalsOutputBean;
import com.thbs.mis.gsr.bean.ViewEmployeeGoalsInputBean;
import com.thbs.mis.gsr.bean.ViewEmployeeGoalsOutputBean;
import com.thbs.mis.gsr.bean.ViewTeamGoalsInputBean;
import com.thbs.mis.gsr.bean.ViewTeamGoalsOutputBean;
import com.thbs.mis.gsr.bo.GsrAdminConfigurationBO;
import com.thbs.mis.gsr.bo.GsrDatEmpDelegatedGoalBO;
import com.thbs.mis.gsr.bo.GsrDatEmpFinalRatingBO;
import com.thbs.mis.gsr.bo.GsrDatEmpGoalBO;
import com.thbs.mis.gsr.bo.GsrKpiGoalBO;
import com.thbs.mis.gsr.bo.GsrMasGoalStatusBO;
import com.thbs.mis.gsr.bo.GsrSlabBO;
import com.thbs.mis.gsr.bo.GsrTrainingRequestBO;
import com.thbs.mis.gsr.bo.GsrWorkflowPermissionBO;
import com.thbs.mis.gsr.constants.GsrURIConstants;
import com.thbs.mis.gsr.service.GSRGoalService;
import com.thbs.mis.gsr.service.GsrSlabService;
import com.thbs.mis.gsr.validator.DelegationGoalsInputBeanValidator;
import com.thbs.mis.gsr.validator.ForceClosedGoalsInputBeanValidator;
import com.thbs.mis.gsr.validator.GsrEmployeeGoalsValidator;
import com.thbs.mis.gsr.validator.GsrHrViewEmployeeGoalsValidator;
import com.thbs.mis.gsr.validator.GsrIsGoalClosedValidator;
import com.thbs.mis.gsr.validator.GsrRmViewEmployeeGoalsValidator;
import com.thbs.mis.gsr.validator.GsrSlabValidator;
import com.thbs.mis.gsr.validator.GsrTeamGoalsValidator;
import com.thbs.mis.gsr.validator.GsrTrainingRequestBeanSearchValidator;
import com.thbs.mis.gsr.validator.GsrViewCurrentGoalBeanValidator;
import com.thbs.mis.gsr.validator.GsrViewEmployeeGoalsValidator;
import com.thbs.mis.gsr.validator.GsrWorflowPermissionBeanValidator;
import com.thbs.mis.gsr.validator.GsrWorkflowValidator;
import com.thbs.mis.gsr.validator.HrViewTeamGoalsInputBeanValidator;
import com.thbs.mis.gsr.validator.ViewTeamGoalsInputBeanValidator;

@Controller
public class GSRGoalController {

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(GSRGoalController.class);

	@Autowired
	private GSRGoalService gsrGoalService;

	// Added by Mani for GSR module service committed on 30-11-2016

	@Autowired(required = true)
	private CommonService commonService;

	@Autowired
	private GsrSlabService gsrSlabService;

	// End of addition by Mani for GSR module service committed on 30-11-2016

	@InitBinder
	protected void initBinder(WebDataBinder binder) throws BindException {
		if (binder.getObjectName().equalsIgnoreCase("GsrViewCurrentGoalsBean")) {
			binder.addValidators(new GsrViewCurrentGoalBeanValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"GsrEmployeeGoalDetailsBeanPagenation")) {
			binder.addValidators(new GsrViewEmployeeGoalsValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"ViewEmployeeGoalsInputBean")) {
			binder.addValidators(new GsrRmViewEmployeeGoalsValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"HrViewEmpGoalsInputBean")) {
			binder.addValidators(new GsrHrViewEmployeeGoalsValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"ForceClosedGoalsInputBean")) {
			binder.addValidators(new ForceClosedGoalsInputBeanValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"ViewTeamGoalsInputBean")) {
			binder.addValidators(new ViewTeamGoalsInputBeanValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"HrViewTeamGoalsInputBean")) {
			binder.addValidators(new HrViewTeamGoalsInputBeanValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"DelegationGoalsInputBean")) {
			binder.addValidators(new DelegationGoalsInputBeanValidator());
		} else {
			binder.close();
		}
	}

	/* added by Pratibha T R */

	@InitBinder()
	protected void initValidator(WebDataBinder binder) throws BindException {
		if (binder.getObjectName().equalsIgnoreCase("GsrSetWorkflowDatesBean")) {
			binder.addValidators(new GsrWorkflowValidator());
		} else if (binder.getObjectName().equalsIgnoreCase("GsrCreateSlabBean")) {
			binder.addValidators(new GsrSlabValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"GsrTeamGoalDetailsBean")) {
			binder.addValidators(new GsrTeamGoalsValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"GsrEmployeeGoalDetailsBean")) {
			binder.addValidators(new GsrEmployeeGoalsValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"GsrTrainingRequestSearchBean")) {
			binder.addValidators(new GsrTrainingRequestBeanSearchValidator());
		} else if (binder.getObjectName().equalsIgnoreCase("entityBeanList")) {
			EntityBeanList list = (EntityBeanList) binder.getTarget();
			if (list.getObjectName().contains("GsrWorflowPermissionBean")) {
				binder.addValidators(new GsrWorflowPermissionBeanValidator());
			}
		} else if (binder.getObjectName()
				.equalsIgnoreCase("GsrGoalsClosedBean")) {
			binder.addValidators(new GsrIsGoalClosedValidator());
		} else {
			binder.close();
		}
	}

	/* ended by Pratibha T R */
	/* added by Smrithi */
	/**
	 * 
	 * @param gsrEmpGoalDetailsBean
	 * @return List<ViewEmployeeGoalsOutputBean>
	 * @throws BusinessException
	 * @throws MethodArgumentNotValidException
	 * @Description: This method is used to view the employee goals based on
	 *               empId, empStartDate and empEndDate.
	 */
	@ApiOperation(tags = "GSR", value = " View Employee Goals"
			+ "This service will be called from the front-end when the user wants to view the employee goals based on empId, empStartDate and empEndDate", httpMethod = "POST", notes = "This Service has been implemented to view the employee goals based on empId, empStartDate and empEndDate."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the created goals and status"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> GsrEmployeeGoalDetailsBean <br><br>"
			+ "<br> id : mgrId in the format of integer <br>"
			+ "<br> empStartDate : Date in the format yyyy-MM-dd. <br>"
			+ "<br> empEndDate : Date in the format yyyy-MM-dd. <br>",

	nickname = "GSR View Employee Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Employee goals  resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.VIEW_EMPLOYEE_GOALS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewEmployeeGoals(
			@Valid @ApiParam(name = "GSR View Employee Goals", example = "1", value = "{"
					+ "<br> id: 1,"
					+ "<br> empStartDate: 2016-10-01,"
					+ "<br> empEndDate: 2016-12-31," + "<br> " + "}") @RequestBody GsrEmployeeGoalDetailsBean gsrEmpGoalDetailsBean,
			BindingResult binding) throws BusinessException,
			MethodArgumentNotValidException {
		LOG.startUsecase("view Employee Goals");
		try {

			if (binding.hasErrors()) {

				return ValidationUtils.handleValidationErrors(binding);
			}
			List<ViewEmployeeGoalsOutputBean> viewEmployeeGoals = gsrGoalService
					.viewEmployeeGoals(gsrEmpGoalDetailsBean);
			LOG.endUsecase("view Employee Goals");

			if (viewEmployeeGoals.size() == 0) {

				return ResponseEntity.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"No Employee  Goals to display",viewEmployeeGoals));
			} else {
				LOG.endUsecase("view Team Goals");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS, "Employee Goals are displayed,",
								viewEmployeeGoals));
			}
		} catch (Exception e) {

			throw new BusinessException("exception ", e);
		}

	}

	/**
	 * 
	 * @param acceptDelBean
	 * @return
	 * @throws BusinessException
	 * @throws CommonCustomException
	 * @throws ParseException
	 * @throws DataAccessException
	 * @Description: This method is used when the reporting manager wants to
	 *               delegate goals of his/her reportees to another manager.The
	 *               delegated manager will accept the goals based on goal Id.
	 */
	@ApiOperation(tags = "GSR", value = "  GSR Accept Delegated Goals"
			+ "This service will be called from the front-end when the reporting manager wants to delegate goals of his/her reportees to another manager.The delegated manager will accept the goals based on goal Id.", httpMethod = "POST", notes = "This Service has been implemented to accept delegated goals."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_delegated_goal -> It contains all the delegated goals and status"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> GSR Accept Delegated Goals <br><br>"
			+ "<br> List of goalId : goalId in the format of integer <br>",

	nickname = "GSR Accept Delegated Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpDelegatedGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpDelegatedGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Accept Delegate Goals resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.ACCEPT_REJECT_DELEGATED_GOALS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> acceptOrRejectDelegatedGoals(
			@ApiParam(name = "GSR Accept Delegate Goals", example = "1", value = "{"
					+ "<br> goalId: [1,2]," + "<br> " + "}") @RequestBody GsrAcceptDelegatedGoalsBean acceptDelBean)
			throws BusinessException, DataAccessException, ParseException,
			CommonCustomException {
		LOG.startUsecase("accept Delegated Goals");
		MISResponse misResponse = new MISResponse();

		misResponse = gsrGoalService.acceptOrRejectDelegatedGoals(acceptDelBean);
		return ResponseEntity.status(HttpStatus.OK.value()).body(misResponse);

	}

	/**
	 * 
	 * @param empId
	 * @return List<GsrDatEmpGoalBO>
	 * @throws BusinessException
	 * @throws ParseException
	 * @throws DataAccessException
	 * @Description: This method is used to force close the goals when the
	 *               skip-level manager wants to force close goals of employees
	 *               whose goals are rated and rejected.Force close goals can
	 *               also be done by HR when employees abscond from the company.
	 */
	@ApiOperation(tags = "GSR", value = "  GSR Force Close Goals"
			+ "This service will be called from the front-end when the skip-level manager wants to force close goals of employees whose goals are rated and rejected.Force close goals can also be done by HR when employees abscond from the company.", httpMethod = "POST", notes = "This Service has been implemented to force close goals."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_final_rating -> It contains all final rating id and status"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> GSR Force Close Goals <br><br>"
			+ "<br> empId : empId in the format of integer <br>"
			+ "<br> slabId : slabId in the format of integer <br>",

	nickname = "GSR Force Close Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Force Close Goals  resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.FORCE_CLOSE_GOALS, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> forceCloseGoals(
			@Valid @ApiParam(name = "ForceClosedGoalsInputBean", example = "1", value = "{"
					+ "<br> empId: 1," + "<br> slabId: 12," + "<br> " + "}") @RequestBody ForceClosedGoalsInputBean forceCloseGoalsBean,
			BindingResult binding) throws BusinessException,
			CommonCustomException, DataAccessException, ParseException {
		LOG.startUsecase("force Close Goals");

		boolean forceCloseGoals;

		System.out.println("forceCloseGoalsBean:" + forceCloseGoalsBean);
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				forceCloseGoals = gsrGoalService
						.forceCloseGoals(forceCloseGoalsBean);

				if (forceCloseGoals) {
					LOG.endUsecase("Force Close Goals");
					return ResponseEntity
							.status(HttpStatus.OK.value())
							.body(new MISResponse(HttpStatus.OK.value(),
									MISConstants.SUCCESS,
									"Goals for the employee " + gsrGoalService.getEmpNameByEmpId(forceCloseGoalsBean.getEmpId())+"-" +forceCloseGoalsBean.getEmpId() +" has been resolved successfully."));
				} else {
					LOG.endUsecase("Force Close Goals");
					return ResponseEntity
							.status(HttpStatus.OK.value())
							.body(new MISResponse(HttpStatus.OK.value(),
									MISConstants.SUCCESS,
									"Some error occured while resolving goals of the employee "+ gsrGoalService.getEmpNameByEmpId(forceCloseGoalsBean.getEmpId())+"-" +forceCloseGoalsBean.getEmpId()+", Please contact Nucleus Team."));
				}
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
	}

	/**
	 * @param empId
	 * @return List<GsrKpiGoalBO>
	 * @throws BusinessException
	 * @Description: This method is used to get the default goals of all
	 *               employees.
	 */
	@ApiOperation(tags = "GSR", value = " GSR Get Default Goals"

			+ "This service will be called from the front-end when the user wants to get the default goals", httpMethod = "GET", notes = "This Service has been implemented to get default goals."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the goals and status"
			+ "<br>",

	nickname = "GSR Get Default Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrKpiGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrKpiGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of default goals  resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_DEFAULT_GOALS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getDefaultGoals(
			@PathVariable("empId") int empId) throws BusinessException {
		LOG.startUsecase("getDefaultGoals");
		List<GsrDelegetedOutputBean> gsrKpiGoalList = null;
		try
		{

			// Update by Annu as per new KPi requirements based on
			// employee
			// level and BU
			gsrKpiGoalList = gsrGoalService.getDefaultGoals(empId);

			if (gsrKpiGoalList.size() == 0)
			{

				LOG.endUsecase("view Team Goals");
				return ResponseEntity.status(
						HttpStatus.NOT_FOUND.value()).body(
						new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.SUCCESS,
								"No Goals to display", gsrKpiGoalList));
			} else
			{
				LOG.endUsecase("view Team Goals");
				return ResponseEntity.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Default goals are displayed",
								gsrKpiGoalList));
			}

		} catch (BusinessException e)
		{
			LOG.error("Exception occured", e);
			throw new BusinessException(e.getMessage());
		}
		
		catch (Exception e)
		{
			LOG.error("Exception occured", e);
			throw new BusinessException("exception ", e);
		}

	}

	/**
	 * 
	 * @param teamGoalBean
	 * @return List<GsrDatEmpGoalBO>
	 * @throws BusinessException
	 * @Description : This method is used to when the reporting manager wants to
	 *              view Team goals of his/her reportees.This service has been
	 *              deprecated as of now.
	 */
	@Deprecated
	@ApiOperation(tags = "GSR", value = " GSR View Team Goals"
			+ "This service will be called from the front-end when the reporting manager wants to view Team goals of his/her reportees.This service has been deprecated as of now.", httpMethod = "POST", notes = "This Service has been implemented when the reporting manager wants to view Team goals of his/her reportees."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the goals and status"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> GSR View Team Goals <br><br>"
			+ "<br> mgrId : mgrId in the format of integer <br>"
			+ "<br> startDate : Date in the format yyyy-MM-dd. <br>"
			+ "<br> endDate : Date in the format yyyy-MM-dd. <br>"
			+ "<br> status : status in the format of integer and should be between 1 to 7 <br>",

	nickname = "GSR View Team Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of  Team Goals resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.VIEW_TEAM_GOALS, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> viewTeamGoals(
			@Valid @ApiParam(name = "GSR View Team Goals", example = "1", value = "{"
					+ "<br> mgrId: 1,"
					+ "<br> startDate: 2016-10-01,"
					+ "<br> endDate: 2016-12-31,"
					+ "<br> status: 4,"
					+ "<br> "
					+ "}") @RequestBody GsrTeamGoalDetailsBean teamGoalBean,
			BindingResult binding) throws BusinessException {
		LOG.startUsecase("view Team Goals");
		List<GsrDatEmpGoalBO> gsrTeamGoalsList = new ArrayList<GsrDatEmpGoalBO>();
		MISResponse misResponse = new MISResponse();
		try {

			if (binding.hasErrors()) {

				return ValidationUtils.handleValidationErrors(binding);
			}
			gsrTeamGoalsList = gsrGoalService.viewTeamGoals(teamGoalBean);

			if (gsrTeamGoalsList.size() == 0) {

				LOG.endUsecase("view Team Goals");
				return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.FAILURE,
								"No Team Goals to display"));
			} else {
				LOG.endUsecase("view Team Goals");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS, "Team Goals",
								gsrTeamGoalsList));
			}

		} catch (Exception e) {

			throw new BusinessException("exception ", e);
		}

	}

	/**
	 * 
	 * @param mgrId
	 * @return List<GsrDatEmpGoalBO>
	 * @throws BusinessException
	 * @Description : This method is used when the reporting manager wants to
	 *              view Reportees goals.
	 */
	@ApiOperation(tags = "GSR", value = " GSR View Reportees Goals"
			+ "This service will be called from the front-end when the reporting manager wants to view Reportees goals", httpMethod = "POST", notes = "This Service has been implemented when the reporting manager wants to view Reportees goals."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the goals and status"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> GSR View Reportees Goals <br><br>"
			+ "<br> mgrId: mgrId in the format integer <br>",

	nickname = "GSR View Reportees Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Reportee goals resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.VIEW_REPORTEES_GOALS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> viewReporteesGoals(
			@PathVariable("mgrId") int mgrId) throws BusinessException {
		LOG.startUsecase("viewReporteesGoals");
		List<GsrDatEmpGoalBO> gsrEmpGoalList = null;
		try {

			gsrEmpGoalList = gsrGoalService.reporteesCloseGoals(mgrId);
			if (gsrEmpGoalList.size() > 0) {
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Reportee Goals are displayed.", gsrEmpGoalList));
			} else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.SUCCESS, "No Goals to display.",
								gsrEmpGoalList));
			}

		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new BusinessException("exception ", e);
		}

	}

	/**
	 * 
	 * @param empId
	 *            and slabId
	 * @return
	 * @throws BusinessException
	 * @throws ConfigException
	 * @throws ParseException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 * @Description: This method is used to give disagree rating from the
	 *               employees for a particular slab.
	 */
	@ApiOperation(tags = "GSR", value = " GSR Disagree Rating Status"
			+ "This service will be called from the front-end when the employees wants to disagree the rating for  a particular slab.", httpMethod = "POST", notes = "This Service has been implemented for the employees to disagree the ratings"
			+ "<br>After the employee disagrees the rating, an e-mail is immediately sent to the skip-level manager."
			+ "<br>In DB after hitting the service with proper inputs, final_rating_status changes to rejected (ie) 3, rating_accept_or_reject_date is changed to current date and also skip_level_mgr_id is updated accordingly."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_final_rating -> It contains all final rating id and status"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> GSR Disagree Rating <br><br>"
			+ "<br> empId : empId in the format of integer <br>"
			+ "<br> slabId : slabId in the format of short <br>",

	nickname = "GSR Disagree Rating", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpFinalRatingBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpFinalRatingBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Disagree Rating Details  resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.DISAGREE_RATING, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> disagreeRatingStatus(
			@Valid @ApiParam(name = "GSR View Team Goals", example = "1", value = "{"
					+ "<br> empId: 1," + "<br> slabId: 16," + "<br> " + "}") @RequestBody GsrDisagreeRatingBean gsrDisagreeRatingBean)
			throws BusinessException, DataAccessException, ParseException,
			ConfigException, CommonCustomException {
		LOG.startUsecase("disagree Rating Status");
		boolean isUpdated = false;
		GsrDatEmpFinalRatingBO gsrFinalRating = new GsrDatEmpFinalRatingBO();
		isUpdated = gsrGoalService.disagreeRatingStatus(gsrDisagreeRatingBean);
		LOG.endUsecase("disagree Rating Status");
		if (isUpdated) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Status changed  successfully."));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Only rated goals can be rejected."));
		}
	}
	
	/**
	 * <Description fetchPastGoalsForEmployee:> method is is used to fetch the
	 * details the GsrGoal repository method for the viewing the GsrGoals for
	 * the current year and for the past year in the data base.
	 * 
	 * @param empId
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @see
	 */
	@ApiOperation(tags = "GSR", value = "Fetch the past goals for Employee."
			+ "To fetch the past goals for Employee.", httpMethod = "GET", notes = "This Service has been implemented to fetch the past goals for Employee."
			+ "<br>The Response will be the detailed view of the current and past one year goals."
			+ "<br><b>Inputs :</b><br> GSR View Goal. <br><br>"
			+ "<br> empId : empId in the format of integer <br>"
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the goals and status",

	nickname = "GSR Fetch the past goals for Employee", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Fetch the past goals for Employee resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.FETCH_GOALS_FOR_EMPLOYEE, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> fetchPastGoalsForEmployee(
			@PathVariable("empId") Integer empId) throws BusinessException,
			GoalNotFoundException, DataAccessException, CommonCustomException {
		LOG.startUsecase("Fetch Past Goals For Employee");
		try {
			List<GsrFetchPastGoalsForEmployeeOutputBean> finalOutput = gsrGoalService
					.fetchPastGoalsForEmployee(empId);

			LOG.startUsecase("Final Output:" + finalOutput.size());

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"GSR Fetch Past goals by Employee ID is displayed",
							finalOutput));

		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (GoalNotFoundException e) {
			throw new GoalNotFoundException(e.getMessage());
		}

	}

	/**
	 * <Description fetchPastGoalsForEmployeeList:> method is is used to fetch
	 * the details the GsrGoal repository method for the viewing the GsrGoals
	 * for the current year and for the past year in the data base.
	 * 
	 * @param List
	 *            <empId>
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @see
	 */
	@ApiOperation(tags = "GSR", value = "Fetch the past goals for Employee List."
			+ "To fetch the past goals for Employee List.", httpMethod = "GET", notes = "This Service has been implemented to fetch the past goals for Employee list."
			+ "<br>The Response will be the detailed view of the current and past one year goals for Employee List."
			+ "<br><b>Inputs :</b><br> GSR View Goal. <br><br>"
			+ "<br> List<empId> : List<empId> in the format of integer <br>"
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the goals and status",

	nickname = "GSR Fetch the past goals for Employee", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Fetch the past goals for Employee resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.FETCH_GOALS_FOR_EMPLOYEE_LIST, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> fetchPastGoalsForEmployeeList(
			@RequestBody FetchPastGoalsForEmployeeListInputBean inputBean)
			throws BusinessException, GoalNotFoundException,
			DataAccessException, CommonCustomException {
		LOG.startUsecase("Fetch Past Goals For Employee");

		try {
			List<GsrFetchPastGoalsForEmployeeOutputBean> finalOutput = gsrGoalService
					.fetchPastGoalsForEmployeeList(inputBean);

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"GSR Fetch Past goals by Employee ID List is displayed",
							finalOutput));
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
	}

	/**
	 * <Description fetchPastGoalsForRM:> method is is used to fetch the
	 * details the GsrGoal repository method for the viewing the GsrGoals for
	 * the current year and for the past year for all his/her reportees in the data base .
	 * 
	 * @param mgrId
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @see
	 */
	@ApiOperation(tags = "GSR", value = "Fetch the past goals for Reporting Manager."
			+ "To fetch the past goals for Reporting Manager.", httpMethod = "GET", notes = "This Service has been implemented to fetch the past goals for Reporting Manager."
			+ "<br>The Response will be the detailed view of the current and past one year goals for all his/her reportees."
			+ "<br><b>Inputs :</b><br> GSR View Goal. <br><br>"
			+ "<br> mgrId : mgrId in the format of integer <br>"
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the goals and status",

	nickname = "GSR Fetch the past goals for Reporting Manager", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Fetch the past goals for Reporting Manager resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.FETCH_GOALS_FOR_REPORTING_MANAGER, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> fetchPastGoalsForReportingManager(
			@PathVariable("mgrId") Integer mgrId) throws BusinessException,
			GoalNotFoundException, DataAccessException, CommonCustomException {
		LOG.startUsecase("Fetch Past Goals For Reporting Manager");
		try {
			List<GsrFetchPastGoalsForEmployeeOutputBean> finalOutput = gsrGoalService
					.fetchPastGoalsForReportingManager(mgrId);

			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(
							HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"GSR Fetch Past goals by Reporting Manager ID is displayed",
							finalOutput));
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}

		catch (GoalNotFoundException e) {
			throw new GoalNotFoundException(e.getMessage());
		}
	}

	/**
	 * <Description re-open goal:> method is is used to re-open the GsrGoal by
	 * the HR for viewing or making changes in the GSR Rating .
	 * 
	 * @param empId
	 *            ,slabId,modifiedBy
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @throws ParseException
	 * @see
	 */
	@ApiOperation(tags = "GSR", value = " GSR Re-open the GsrGoals by the HR."
			+ "To re-open the GsrGoals by the HR.", httpMethod = "POST", notes = "This Service has been implemented to Re-open the GsrGoals by the HR for viewing or making changes in the  GSR Rating."
			+ "<br>The Response will be the detailed view of the gsr goals for the particular employee."
			+ "<br><b>Inputs :</b><br> GSR Re-open Goals. <br><br>"
			+ "<br> empId : empId in the format of integer <br>"
			+ "<br> slabId : slabId in the format of Short <br>"
			+ "<br> modifiedBy : modifiedBy in the format of integer <br>"
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the goals and status of all employees.",

	nickname = "GSR Re-open the GsrGoals by the HR", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Re-open the GsrGoals by the HR resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.REOPEN_GOALS_BY_HR, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> reopenGsrGoals(
			@Valid @RequestBody GsrReopenGoalsByHrBean gsrReopenGoalsByHrBean)
			throws BusinessException, GoalNotFoundException,
			DataAccessException, CommonCustomException, ParseException {
		LOG.startUsecase("Re-open the GsrGoals by the HR");

		Integer output = gsrGoalService.reopenGsrGoal(gsrReopenGoalsByHrBean);
		System.out.println("output++" + output.toString());

		if (output >= 1) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Goals for the employee "+gsrGoalService.getEmpNameByEmpId(gsrReopenGoalsByHrBean.getEmpId()) +"-" +gsrReopenGoalsByHrBean.getEmpId()+" has been re-opened successfully."));
		} else {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Goals cannot be re-opened for the requested employee "+gsrGoalService.getEmpNameByEmpId(gsrReopenGoalsByHrBean.getEmpId())+"-" +gsrReopenGoalsByHrBean.getEmpId()+". Please contact Nucleus Team."));
		}

	}
	
	
	/**
	 * 
	 * @param inputbean
	 *            bean contains the input details are to be used in the service.
	 * @return GsrDatEmpGoalBO
	 * @throws Exception
	 * @Description: This service is used to enter feedback
	 */
	@ApiOperation(tags = "GSR", value = "Enter feedback for manager."
			+ "This service will be called from the front-end when manager wants to enter only the feedback.", httpMethod = "PUT", notes = "This Service has been implemented to enter only the feedback."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the gsr emp goal details"
			+ "2)gsr_dat_emp_final_rating -> It contains all the gsr emp final rating details."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> Enter feedback <br><br>"
			+ "<br> gsrGoalMgrFeedback : Feedback in the format of string <br>"
			+ "<br> pkGsrDatEmpGoalId : EmpGoalId in the format of integer <br>"
			+ "<br> managerId : managerId in the format of integer.<br>", nickname = "Gsr enter feedback", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Enter feedback for manager Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.ENTER_FEEDBACK_FOR_MANAGER, method = RequestMethod.PUT)
	public @ResponseBody/* Response */ResponseEntity<MISResponse> enterFeedbackForManager(
			@ApiParam(name = "Enter feedback", example = "1", value = "{"
					+ "<br> gsrGoalMgrFeedback: Excellent,"
					+ "<br> pkGsrDatEmpGoalId: 2" + "<br> managerId: 2,"
					+ "<br> " + "}") @Valid @RequestBody GsrEnterFeedbackForManagerBean inputbean)
			throws CommonCustomException 
	{
		LOG.startUsecase("enterFeedback");
		//Response sucess = new Response();
		boolean status = false;
		GsrDatEmpGoalBO DatEmpGoal = null;
		List<GsrDatEmpGoalBO> empGoal = new ArrayList<GsrDatEmpGoalBO>();

		status = gsrGoalService.enterFeedbackForManager(inputbean);
		empGoal.add(DatEmpGoal);
		if (status) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),MISConstants.SUCCESS,
						"Feedback is added successfully.", empGoal));

		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),MISConstants.FAILURE,
						"Feedback can not be provided for the mentioned goal id.", empGoal));
		}
	}
	
	/* Ended by Smrithi */

	/* Added By Pratibha T R */
	/**
	 * 
	 * @param slabBean
	 *            contains the input details are to be used in the service.
	 * @param binding
	 * @return GsrSlabBO
	 * @throws BusinessException
	 *             if is there present any Business exception in the service.
	 * @Description: This service is used to create slab
	 */
	@ApiOperation(tags = "GSR", value = "Create slab"
			+ "This service will be called from the front-end when the HR wants to create slab.", httpMethod = "POST", notes = "This Service has been implemented to create slab."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_slab -> It contains all the slab details."
			+ "<br>"
			+ "<br> <b> Rules : </b> </br>"
			+ "1)gsrSlabEnabled -> gsrSlabEnabled filed in the gsr_slab table will accept only 'ENABLED' or 'DISABLED' value."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> Create slab <br><br>"
			+ "<br> gsrSlabName : slabName in the format of string <br>"
			+ "<br> gsrSlabStartDate : Date in the format yyyy-MM-dd. <br>"
			+ "<br> gsrSlabEndDate : Date in the format yyyy-MM-dd. <br>"
			+ "<br> gsrSlabYear : slabYear in format of string <br>"
			+ "<br> gsrSlabShortName : gsrSlabShortName in the format string. <br>"
			+ "<br> gsrSlabEnabled : gsrSlabEnabled in the format string. <br>", nickname = "GSR create slab", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create slab detail resource.", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.CREATE_SLAB, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> createSlab(
			@ApiParam(name = "Create slab", example = "1", value = "{"
					+ "<br> gsrSlabName: 2016 - Q1 (Apr  to Jun 2016),"
					+ "<br> gsrSlabStartDate: 2016-04-01,"
					+ "<br> gsrSlabEndDate: 2016-06-30,"
					+ "<br> gsrSlabYear: 2016," + "<br>  gsrSlabShortName:Q1,"
					+ "<br> gsrSlabEnabled:ENABLED" + "<br> " + "}") @RequestBody @Valid GsrCreateSlabBean createSlab,
			BindingResult binding) throws BusinessException,
			CommonCustomException {
		LOG.startUsecase("createSlab");
		boolean slabCreationOutput = false;
		if (binding.hasErrors()) {
			return ValidationUtils.handleValidationErrors(binding);
		}
		try {
			slabCreationOutput = gsrGoalService.createSlab(createSlab);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (slabCreationOutput) {
			LOG.endUsecase("createSlab");
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Slab created successfully."));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Error occurred while creating slab."));
		}

	}

	//Commented by Shyam for re-coding of creation of gsr goal
	/**
	 * 
	 * @param goalBo
	 * @return GsrKpiGoalBO
	 * @throws BusinessException
	 *             if is there present any Business exception in the service.
	 * @Description: This service is used to create common goals
	 */
	/*@ApiOperation(tags = "GSR", value = "Create common goals."
			+ "This service will be called from the front-end when the HR wants to create common goals.", httpMethod = "POST", notes = "This Service has been implemented to create common goals."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_kpi_goal -> It contains all the details of the common goals."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> Create common goals <br><br>"
			+ "<br> gsrKpiGoalDescription : Description in the format of string <br>"
			+ "<br> gsrKpiGoalMeasurement : Measurement in the format string <br>"
			+ "<br> gsrKpiGoalName : GoalName in the format string. <br>", nickname = "Gsr create common  goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create common goals resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.CREATE_GOALS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> createGoals(
			@ApiParam(name = "Create common goals", example = "1", value = "{"
					+ "<br> gsrKpiGoalDescription: Consultancy,"
					+ "<br> gsrKpiGoalMeasurement: Rating,"
					+ "<br> gsrKpiGoalName: Good" + "<br> " + "}") @Valid @RequestBody GsrCreateCommonGoalsBean goalBo)
			throws BusinessException {
		LOG.startUsecase("createGoals");
		GsrCreateCommonGoalsBean gsrkpiGoalBean = new GsrCreateCommonGoalsBean();
		List<GsrCreateCommonGoalsBean> gsrKpiGoalList = new ArrayList<GsrCreateCommonGoalsBean>();
		try {
			gsrkpiGoalBean = gsrGoalService.createGoals(goalBo);
			gsrKpiGoalList.add(gsrkpiGoalBean);
			if (gsrKpiGoalList.size() > 0) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Common goals are created successfully.",
								gsrKpiGoalList));
			} else {
				return ResponseEntity
						.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.FAILURE,
								"Some Error happend while creating common goals."));
			}
		} catch (Exception e) {

			throw new BusinessException("exception ", e);
		}
	}*/

	/**
	 * 
	 * @param goalBo
	 * @return GsrKpiGoalBO
	 * @throws CommonCustomException
	 *             if goal is already deleted
	 * @throws GoalNotFoundException
	 *             if Id does not present
	 * @Description: This service is used to update common goals
	 */
	@ApiOperation(tags = "GSR", value = "update common goals."
			+ "This service will be called from the front-end when the HR wants to update common goals.", httpMethod = "PUT", notes = "This Service has been implemented to update goals."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_kpi_goal -> It contains all the details of the common goals."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> update common goals <br><br>"
			+ "<br> pkGsrKpiGoalId : goalId in the format of integer <br>"
			+ "<br> gsrKpiGoalDescription : Description in the format of string <br>"
			+ "<br> gsrKpiGoalMeasurement : Measurement in the format string <br>", nickname = "Gsr update common goal", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Update common goals Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.UPDATE_GOALS, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> updateGoals(
			@ApiParam(name = "update common goals", example = "1", value = "{"
					+ "<br> pkGsrKpiGoalId: 1,"
					+ "<br> gsrKpiGoalDescription: Consultancy,"
					+ "<br> gsrKpiGoalMeasurement: Rating" + "<br> " + "}") @Valid @RequestBody GsrUpdateCommonGoalsBean goalBo)
			throws CommonCustomException, GoalNotFoundException {
		LOG.startUsecase("updateGoals");
		GsrUpdateCommonGoalsBean gsrKpiGoal = new GsrUpdateCommonGoalsBean();
		List<GsrUpdateCommonGoalsBean> gsrKpiGoalList = new ArrayList<GsrUpdateCommonGoalsBean>();
		try {
			gsrKpiGoal = gsrGoalService.updateGoals(goalBo);
			gsrKpiGoalList.add(gsrKpiGoal);
			if (gsrKpiGoalList.size() > 0) {
				LOG.endUsecase("updateGoals");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Your goal has been updated successfully.",
								gsrKpiGoalList));
			} else {
				return ResponseEntity
						.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.FAILURE,
								"Error occurred while updating org-wide goals."));
			}
		} catch (GoalNotFoundException e) {
			throw new GoalNotFoundException("Goal does not exists.", e);

		} catch (Exception e) {

			throw new CommonCustomException("Goal is already deleted.", e);

		}

	}

	/**
	 * 
	 * @param pkGsrKpiGoalId
	 * @return GsrKpiGoalBO
	 * @throws BusinessException
	 *             if is there present any Business exception in the service.
	 * @throws CommonCustomException
	 *             if goal is already deleted
	 * @throws GoalNotFoundException
	 *             if Id does not present
	 * @Description: This service is used to delete common goals
	 */
	@ApiOperation(tags = "GSR", value = "Delete Common goal."
			+ "This service will be called from the front-end when the HR wants to delete common goals", httpMethod = "DELETE", notes = "This Service has been implemented to delete goals."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_kpi_goal -> It contains all the details of the common goals."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> Delete common goals <br><br>"
			+ "<br> pkGsrKpiGoalId : goalId in the format of integer <br>", nickname = "Gsr delete common goal", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Delete common goal Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.DELETE_COMMON_GOALS, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<MISResponse> deleteGoals(
			@PathVariable short pkGsrKpiGoalId) throws BusinessException,
			CommonCustomException, GoalNotFoundException {
		GsrKpiGoalBO gsrKpiGoal = null;
		List<GsrKpiGoalBO> gsrKpiGoalList = new ArrayList<GsrKpiGoalBO>();
		Response sucess = new Response();
		LOG.startUsecase("deleteGoals");
		try {
			gsrKpiGoal = gsrGoalService.deleteGoals(pkGsrKpiGoalId);
			gsrKpiGoalList.add(gsrKpiGoal);
			sucess.setSucessCode(HttpStatus.OK.value());
			sucess.setSuceesMessageKey(MISConstants.DELETED);
			sucess.setSucessMessage("Deleted Suceessfully");
			if (gsrKpiGoalList.size() > 0) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Org-wide goals are deleted successfully.",
								gsrKpiGoalList));
			} else {
				LOG.endUsecase("deleteGoals");
				return ResponseEntity
						.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.FAILURE,
								"Error occurred while deleting org-wide goals."));
			}
		} catch (GoalNotFoundException e) {
			throw new GoalNotFoundException("Goal does not exists.", e);

		} catch (Exception e) {

			throw new CommonCustomException("Goal is already deleted.", e);

		}

	}

	/**
	 * 
	 * @param goalBo
	 * @return GsrAdminConfigurationBO
	 * @throws BusinessException
	 *             if is there present any Business exception in the service.
	 * @Description: This service is used to enable and disable settings
	 */

	@ApiOperation(tags = "GSR", value = "Enable Disable settings."
			+ "This service will be called from the front-end when the HR wants to enable and disable settings.", httpMethod = "PUT", notes = "This Service has been implemented to enable and disable settings."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_admin_configuration -> It contains enable and disable settings details of the gsrwindow,selfRating and training request."
			+ "<br>"
			+ "<br> <b> Rules : </b> </br>"
			+ "1)enableDisableSettings -> gsrSlabEnabled filed in the gsr_admin_configuration table will accept only 'ENABLED' or 'DISABLED' value."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> Enable Disable settings <br><br>"
			+ "<br> pkGsrAdminConfigId : AdminConfigId in the format of integer <br>"
			+ "<br> enableDisableSettings : enableDisableSettings in the format of string.<br>", nickname = "Gsr enable disable settings", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Enable Disable settings Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.ENABLE_SETTINGS, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> enableDisableSettings(
			@ApiParam(name = "Enable Disable settings", example = "1", value = "{"
					+ "<br> pkGsrAdminConfigId:1"
					+ "<br> enableDisableSettings:ENABLE," + "<br> " + "}") @Valid @RequestBody GsrEnableSettingsBean goalBo)
			throws BusinessException {
		LOG.startUsecase("enableDisableSettings");
		GsrAdminConfigurationBO gsrAdminConfig;
		List<GsrAdminConfigurationBO> gsrAdminConfigList = new ArrayList<GsrAdminConfigurationBO>();
		try {
			gsrAdminConfig = gsrGoalService.enableDisableSettings(goalBo);
			gsrAdminConfigList.add(gsrAdminConfig);
			if (gsrAdminConfigList.size() > 0) {
				LOG.endUsecase("enableDisableSettings");	
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"GSR Settings have been enabled or disabled.",
								gsrAdminConfigList));
			} else {
				return ResponseEntity
						.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.FAILURE,
								"Error occurred while enabling or disabling gsr settings."));
			}
		} catch (Exception e) {

			throw new BusinessException("exception ", e);
		}

	}

	/**
	 * 
	 * @param inputbean
	 *            bean contains the input details are to be used in the service.
	 * @param binding
	 * @return GsrWorkflowPermissionBO
	 * @throws BusinessException
	 *             if is there present any Business exception in the service.
	 * @Description: This service is used to set work flow dates
	 */

	@ApiOperation(tags = "GSR", value = "set workflow dates."
			+ "This service will be called from the front-end when the HR wants to set workflow dates.", httpMethod = "PUT", notes = "This Service has been implemented to set workflow dates."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_workflow_permission -> It contains all the gsr workflow permission details."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> set workflow dates <br><br>"
			+ "<br> pkGsrWorkflowPermissionId : Id in the format of integer <br>"
			+ "<br> startDate : Date in the format yyyy-MM-dd.<br>"
			+ "<br> endDate : Date in the format yyyy-MM-dd.<br>", nickname = "Gsr set workflow dates", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Set workflow dates Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.SET_WORKFLOW_DATES, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> setDatesWorkflow(
			@ApiParam(name = "set workflow dates", example = "1", value = "{"
					+ "<br> pkGsrWorkflowPermissionId: 1,"
					+ "<br> startDate: 2016-04-01,"
					+ "<br> endDate: 2016-06-30," + "<br> " + "}") @Valid @RequestBody GsrSetWorkflowDatesBean inputbean,
			BindingResult binding) throws BusinessException {
		LOG.startUsecase("setDatesWorkflow");
		GsrWorkflowPermissionBO gsrWorkflowPermission;
		List<GsrWorkflowPermissionBO> gsrWorkflowPermissionList = new ArrayList<GsrWorkflowPermissionBO>();
		try {

			if (binding.hasErrors()) {

				return ValidationUtils.handleValidationErrors(binding);
			}
			gsrWorkflowPermission = gsrGoalService.setDatesWorkflow(inputbean);
			gsrWorkflowPermissionList.add(gsrWorkflowPermission);
			if (gsrWorkflowPermissionList.size() > 0) {
			LOG.endUsecase("setDatesWorkflow");	
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Workflow dates are set successfully.",
								gsrWorkflowPermissionList));
			} else {
				return ResponseEntity
						.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.FAILURE,
								"Error occurred while setting workflow dates."));
			}
		} catch (Exception e) {

			throw new BusinessException("exception ", e);
		}

	}

	/**
	 * 
	 * @param inputbean
	       bean contains the input details are to be used in the service.
	 * @return GsrDatEmpGoalBO
	 * @throws GoalNotFoundException
	              if Id does not present
	 * @throws CommonCustomException
	       if there is present any Common Custom Exception
	 * @Description: This service is used to Set approve or reject goal
	 */
	@ApiOperation(tags = "GSR", value = "Set approve or reject goal."
			+ "This service will be called from the front-end when the manager wants to set approve or reject goal.", httpMethod = "PUT", notes = "This Service has been implemented to set approve or reject goal."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the gsr emp goal details."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> Set approve or reject goal<br><br>"
			+ "<br> pkGsrDatEmpGoalId : Id in the format of integer <br>"
			+ "<br> fkGsrGoalStatus : Status in the format of integer.<br>"
			+ "<br> goalModifiedBy : goalModifiedBy in the format of integer.<br>", nickname = "Gsr Set approve or reject goal", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Set approve or reject Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.SET_APPROVE_OR_REJECT_GOAL, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> approveOrRejectStatus(
			@ApiParam(name = "Set approve or reject goal", example = "1", value = "{"
					+ "<br> pkGsrDatEmpGoalId:1,"
					+ "<br> fkGsrGoalStatus: 2 or 6,"
					+ "<br> goalModifiedBy: 1" + "}") @Valid @RequestBody GsrApproveAndRejectGoalBean inputbean)
			throws GoalNotFoundException, CommonCustomException {
		LOG.startUsecase("approveOrRejectStatus");
		GsrDatEmpGoalBO DatEmpGoal;
		List<GsrDatEmpGoalBO> empGoal = new ArrayList<GsrDatEmpGoalBO>();
		DatEmpGoal = gsrGoalService.approveOrRejectStatus(inputbean);
		empGoal.add(DatEmpGoal);
		if (DatEmpGoal.getFkGsrGoalStatus() == 6) {
			LOG.endUsecase("approveOrRejectStatus");
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "The goal created by your reportee has been rejected."));

		} else if (DatEmpGoal.getFkGsrGoalStatus() == 2) {

			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "The goal created by your reportee has been approved and s/he may now enter accomplishments."));

		} else {
			return ResponseEntity
					.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Invalid status set in given input. Goal can not be approved or rejected for the given status."));
		}
	}

	/**
	 * 
	 * @param inputbean
	       bean contains the input details are to be used in the service.
	 * @return GsrDatEmpGoalBO
	 * @throws GoalNotFoundException
	       if Id does not present
	 * @throws CommonCustomException
	       if there is present any Common Custom Exception
	 * @Description: This service is used to Set approve goal
	 */
	
	@ApiOperation(tags = "GSR", value = "Set approve goal."
			+ "This service will be called from the front-end when the manager wants to set approve goal.", httpMethod = "PUT", notes = "This Service has been implemented to set approve goal."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the gsr emp goal details."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> Set approve goal<br><br>"
			+ "<br> goalWeightage : Weightage in the format of integer <br>"
			+ "<br> goalMeasurement : Measurement in the format of string.<br>"
			+ "<br> goalDescription : Description in the format of string <br>"
			+ "<br> pkGsrDatEmpGoalId : Id in the format of integer.<br>"
			+ "<br> goalModifiedBy : ModifiedBy in the format of integer <br>", nickname = "Gsr Set approve goal", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Set approve or reject Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.SET_APPROVE_GOAL, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> approveStatus(
			@ApiParam(name = "Set approve goal", example = "1", value = "{"
					+ "<br> goalWeightage: 20,"
					+ "<br> goalMeasurement: Rating - 5 Being professional.,"
					+ "<br> goalDescription: Promote and maintain a healthy work,"
					+ "<br> pkGsrDatEmpGoalId: 2016,"
					+ "<br> goalModifiedBy: 2" + "<br> " + "}") @Valid @RequestBody GsrApproveGoalBean inputbean)
			throws CommonCustomException 
	{
		LOG.startUsecase("approveStatus");
		GsrDatEmpGoalBO gsrDatEmpGoal = new GsrDatEmpGoalBO();
		List<GsrDatEmpGoalBO> empGoalList = new ArrayList<GsrDatEmpGoalBO>();

		gsrDatEmpGoal = gsrGoalService.approveStatus(inputbean);
		
		if(gsrDatEmpGoal != null)
			empGoalList.add(gsrDatEmpGoal);
		
		if(!empGoalList.isEmpty())
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
				MISConstants.SUCCESS, 
				"The goal created by your reportee has been approved and s/he may now enter accomplishments.",
				empGoalList));

		}else{
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
				MISConstants.SUCCESS, 
				"Unable to update your reprtee's goal. Kindly check with Nuclues Support team."));
		} 
	}


	/**
	 * 
	 * @param inputbean
	 *            bean contains the input details are to be used in the service.
	 * @return GsrDatEmpGoalBO
	 * @throws Exception
	 * @Description: This service is used to enter feedback
	 */
	@ApiOperation(tags = "GSR", value = "Enter feedback."
			+ "This service will be called from the front-end when manager wants to enter feedback.", httpMethod = "PUT", notes = "This Service has been implemented to enter feedback."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the gsr emp goal details"
			+ "2)gsr_dat_emp_final_rating -> It contains all the gsr emp final rating details."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> Enter feedback <br><br>"
			+ "<br> gsrGoalMgrFeedback : Feedback in the format of string <br>"
			+ "<br> gsrGoalMgrRating : MgrRating in the format of float.<br>"
			+ "<br> pkGsrDatEmpGoalId : EmpGoalId in the format of integer <br>"
			+ "<br> enteredBy : managerId in the format of integer.<br>", nickname = "Gsr enter feedback", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Enter feedback Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.ENTER_FEDDBACK, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> enterFeedback(
			@ApiParam(name = "Enter feedback", example = "1", value = "{"
					+ "<br> gsrGoalMgrFeedback: Excellent,"
					+ "<br> gsrGoalMgrRating: 3.5,"
					+ "<br> pkGsrDatEmpGoalId: 2" + "<br> enteredBy: 2,"
					+ "<br> " + "}") @Valid @RequestBody GsrEnterFeedbackBean inputbean)
			throws Exception {
		LOG.startUsecase("enterFeedback");
		//Response sucess = new Response();
		boolean status;
		GsrDatEmpGoalBO DatEmpGoal = null;
		List<GsrDatEmpGoalBO> empGoal = new ArrayList<GsrDatEmpGoalBO>();

		status = gsrGoalService.enterFeedback(inputbean);
		empGoal.add(DatEmpGoal);
		if (status) {
			LOG.endUsecase("enterFeedback");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Feedback and ratings are added successfully.",
							empGoal));

		} else {

			return ResponseEntity
					.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(
							HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Feedback can not be provided for the mentioned status.",
							empGoal));

		}

	}
	/**
	 * 
	 * @param pkGsrDatEmpGoalId
	 * @return GsrDatEmpGoalBO
	 * @throws BusinessException
	 *             if is there present any Business exception in the service.
	 * @throws CommonCustomException
	 *             if Goal is already deleted
	 * @Description: This service is used to delete goal
	 */
	@ApiOperation(tags = "GSR", value = "Delete goal."
			+ "This service will be called from the front-end when manager wants to delete goal", httpMethod = "DELETE", notes = "This Service has been implemented to delete goals."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the gsr emp goal details."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> Delete goal <br><br>"
			+ "<br> pkGsrDatEmpGoalId : EmpGoalId in the format of integer <br>", nickname = "Gsr delete goal", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Delete goal Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.DELETE_GOAL, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<MISResponse> deleteGoal(
			@PathVariable Integer pkGsrDatEmpGoalId) throws BusinessException,
			CommonCustomException, GoalNotFoundException {
		LOG.startUsecase("deleteGoal");
		GsrDatEmpGoalBO datEmpGoalDelete = new GsrDatEmpGoalBO();
		try {
			datEmpGoalDelete = gsrGoalService.deleteGoal(pkGsrDatEmpGoalId);
		} catch (Exception e) {
			throw new CommonCustomException("Goal does not exist", e);
		}
		LOG.endUsecase("deleteGoal");
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"Goal deleted successfully."));
	}

	/**
	 * 
	 * @param kpiWeightageInputBean
	            bean contains the input details are to be used in the service.
	 * @return List<GsrKpiWeightageOutputBean>
	 * @throws BusinessException
	 * @Description: This service is used to Get goal weightage details
	 */
	
	@ApiOperation(tags = "GSR", value = "Get goal weightage details."
			+ "This service will be called from the front-end when manager wants get goal weightage details", httpMethod = "DELETE", notes = "This Service has been implemented to get goal weightage details."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_kpi_weightage -> It contains all the goal weightage details."
			+ "2)dat_emp_professional_detail -> It contains all the professional details of the employee."
			+ "<br><b>Inputs :</b><br> Get goal weightage details <br><br>"
			+ "<br> empIdList : empIdList in the format of integer <br>" 
			+ "<br> kpiWeitageId : kpiWeitageId in the format of integer <br>",nickname = "Goal weightage details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Goal weightage details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })	
	@RequestMapping(value = GsrURIConstants.GET_GOAL_WEIGHTAGE_DETAILS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> getWeightageDetails(
			@ApiParam(name = "Get goal weightage details", example = "1", value = "{"
					+ "<br> empIdList:[3811,3812],"
					+ "<br> kpiWeitageId:875,"
				    + "<br> " + "}") @Valid @RequestBody GsrKpiWeightageInputBean kpiWeightageInputBean) throws BusinessException {
		LOG.startUsecase("getWeightageDetails");
		List<GsrKpiWeightageOutputBean> professionalBean;
		try {
			professionalBean = gsrGoalService.getWeightageDetails(kpiWeightageInputBean);
			
			if (professionalBean.size() > 0) {
		LOG.endUsecase("getWeightageDetails");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Goal weightage details.",
								professionalBean));
			} else {
				return ResponseEntity
						.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.FAILURE,
								"Some Error happened while getting goal weightage details."));
			}
		} catch (Exception e) {

			throw new BusinessException("exception ", e);
		}

	}
	
	/**
	 * 
	 * @param gsrMaximumKpiWeightageBean
	           bean contains the input details are to be used in the service
	 * @return List<GsrMaximumKpiOutputBean>
	 * @throws BusinessException
	 * @throws CommonCustomException
	 * @Description: This service is used to Get kpi and non kpi weightage details
	 */
	@ApiOperation(tags = "GSR", value = "Get maximum kpi weightage details."
			+ "This service will be called from the front-end when manager wants get maximum kpi  weightage details", httpMethod = "DELETE", notes = "This Service has been implemented to get maximum kpi weightage details."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)dat_emp_professional_detail -> It contains all the professional details of the employee."
			+ "2)gsr_kpi_weightage -> It contains all the goal weightage details."
			+ "3)gsr_dat_emp_goal -> It contains all the gsr emp goal details."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> Get goal weightage details <br><br>"
			+ "<br> empIdList : empIdList in the format of integer <br>" 
			+ "<br> slabId : slabId in the format of integer <br>"
			+ "<br> mgrId : mgrId in the format of integer <br>",nickname = "Goal maximum kpi weightage details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Goal maximum kpi weightage details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })	
	@RequestMapping(value = GsrURIConstants.GET_MAXIMUM_KPI_WEIGHTAGE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> getMaximumKpiWeightage(
			@ApiParam(name = "Get goal weightage details", example = "1", value = "{"
					+ "<br> empIdList:[3811,3812],"
					+ "<br> slabId:17,"
					+ "<br> mgrId:1340,"
				    + "<br> " + "}") @Valid @RequestBody GsrMaximumKpiWeightageBean gsrMaximumKpiWeightageBean) throws BusinessException, CommonCustomException {
		LOG.startUsecase("get Maximum Kpi Weightage");
		List<GsrMaximumKpiOutputBean> kpiweightageBo = new ArrayList<GsrMaximumKpiOutputBean>();
			kpiweightageBo = gsrGoalService.getMaximumKpiWeightage(gsrMaximumKpiWeightageBean);
			if(!kpiweightageBo.isEmpty()){
		LOG.endUsecase("get Maximum Kpi Weightage");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Goal weightage details.",kpiweightageBo));	
				}else
				{
					return ResponseEntity.status(HttpStatus.OK.value()).body(
							new MISResponse(HttpStatus.OK.value(),
									MISConstants.FAILURE,
									"Goal weightage details not found."));	
					
				}
		
	}
	/* Ended By Pratibha T R */
	
	
	/* Added By Ritesh and Edited by Rajesh Kumar 15/12/2016 */

	/**
	 * <Description getCurrentGoals:> This contains the methods used for the
	 * GSR_CREATE_GOAL for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @see
	 */
	/*
	 * @ApiOperation(tags="GSR",value = "Create the GSR Goal." +
	 * "To create goal details.", httpMethod = "POST", notes =
	 * "This Service has been implemented to Create GSR Goal." +
	 * "<br>The Response will be the detailed of the Create goals." +
	 * "<br><b>Inputs :</b><br> GSR Create Goal. <br><br>",
	 * 
	 * nickname = "GSR Create Goal", protocols = "HTTP", responseReference =
	 * "application/json", produces = "application/json", response =
	 * GsrDatEmpGoalBO.class)
	 * 
	 * @ApiResponses(value = {
	 * 
	 * @ApiResponse(code = 200, message = "Request Executed Successfully",
	 * response = GsrDatEmpGoalBO.class),
	 * 
	 * @ApiResponse(code = 201, message = "Created Successfully",
	 * responseContainer = "Http Response", responseHeaders =
	 * 
	 * @ResponseHeader(name = "Location", description =
	 * "The created Program Details resource", response = URI.class)),
	 * 
	 * @ApiResponse(code = 404, message = "Resource NOT FOUND",
	 * responseContainer = "Http Response"),
	 * 
	 * @ApiResponse(code = 500, message = "Internal Server Error",
	 * responseContainer = "Http Response") })
	 * 
	 * @RequestMapping(value = GsrURIConstants.GSR_CREATE_GOAL, method =
	 * RequestMethod.POST) public @ResponseBody ResponseEntity<MISResponse>
	 * createGsrGoal(
	 * 
	 * @Valid @RequestBody GsrCreateGoalBean bean) throws BusinessException {
	 * LOG.startUsecase("createGoal"); GsrDatEmpGoalBO createGoal = null;
	 * List<GsrDatEmpGoalBO> createGoal1 = new ArrayList<GsrDatEmpGoalBO>(); try
	 * { createGoal = gsrGoalService.createGsrGoal(bean);
	 * createGoal1.add(createGoal); return
	 * ResponseEntity.status(HttpStatus.OK.value()).body( new
	 * MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
	 * "Create Goal is displayed.", createGoal1));
	 * 
	 * } catch (Exception e) { throw new BusinessException("exception ", e); }
	 * // LOG.endUsecase("createGoal"); // return createGoal;
	 * 
	 * }
	 */

	/**
	 * <Description getCurrentGoals:> This contains the methods used for the
	 * GSR_UPDATE_GOAL for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @see
	 */
	/*
	 * @ApiOperation(tags="GSR",value = "Update the GSR Goal." +
	 * "To update goal details.", httpMethod = "PUT", notes =
	 * "This Service has been implemented to update GSR Goal details." +
	 * "<br>The Response will be the detailed of the update goal." +
	 * "<br><b>Inputs :</b><br> GSR Update Goal. <br><br>",
	 * 
	 * nickname = "GSR Update Goal", protocols = "HTTP", responseReference =
	 * "application/json", produces = "application/json", response =
	 * GsrDatEmpGoalBO.class)
	 * 
	 * @ApiResponses(value = {
	 * 
	 * @ApiResponse(code = 200, message = "Request Executed Successfully",
	 * response = GsrDatEmpGoalBO.class),
	 * 
	 * @ApiResponse(code = 201, message = "Created Successfully",
	 * responseContainer = "Http Response", responseHeaders =
	 * 
	 * @ResponseHeader(name = "Location", description =
	 * "The created Program Details resource", response = URI.class)),
	 * 
	 * @ApiResponse(code = 404, message = "Resource NOT FOUND",
	 * responseContainer = "Http Response"),
	 * 
	 * @ApiResponse(code = 500, message = "Internal Server Error",
	 * responseContainer = "Http Response") })
	 * 
	 * @RequestMapping(value = GsrURIConstants.GSR_UPDATE_GOAL, method =
	 * RequestMethod.PUT) public @ResponseBody ResponseEntity<MISResponse>
	 * updateGsrGoal(
	 * 
	 * @Valid @RequestBody GsrCreateGoalBean bean) throws BusinessException,
	 * GoalNotFoundException { LOG.startUsecase("updateGoal"); GsrDatEmpGoalBO
	 * updateGoal = null; List<GsrDatEmpGoalBO> updateGoal1 = new
	 * ArrayList<GsrDatEmpGoalBO>(); try { updateGoal =
	 * gsrGoalService.updateGsrGoal(bean); updateGoal1.add(updateGoal); return
	 * ResponseEntity.status(HttpStatus.OK.value()).body( new
	 * MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
	 * "Update Goal is displayed ", updateGoal1)); } catch
	 * (GoalNotFoundException e) { throw new
	 * GoalNotFoundException("Goal does not exist", e); } catch (Exception e) {
	 * throw new BusinessException("exception ", e); } //
	 * LOG.endUsecase("updateGoal"); //
	 * System.out.println("Exited %%%%%%%%%%%%%%%%%%%%%%5 in controller"); //
	 * return updateGoal;
	 * 
	 * }
	 */
	/**
	 * <Description getCurrentGoals:> This contains the methods used for the
	 * GSR_DELETE_GOAL for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws CommonCustomException
	 * @throws GoalNotFoundException
	 * @see
	 */
	@ApiOperation(tags = "GSR", value = "Delete the GSR Goal."
			+ "To Delete goal details.", httpMethod = "DELETE", notes = "This Service has been implemented to delete GSR Goal details."
			+ "<br>The Response will be the detailed of the Delete goal."
			+ "<br><b>Inputs :</b><br> GSR Delete Goal. <br><br>",

	nickname = "GSR Delete Goal", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GSR_DELETE_GOAL, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<MISResponse> deleteGsrGoal(
			@PathVariable("pkGsrDatEmpGoalId") Integer pkGsrDatEmpGoalId
	/* @RequestBody GsrCreateGoalBean bean */) throws BusinessException,
			CommonCustomException, GoalNotFoundException {
		LOG.startUsecase("deleteGoal");
		GsrDatEmpGoalBO deleteGoal = null;
		List<GsrDatEmpGoalBO> deleteGoal1 = new ArrayList<GsrDatEmpGoalBO>();
		try {
			deleteGoal = gsrGoalService.deleteGsrGoal(pkGsrDatEmpGoalId);
			deleteGoal1.add(deleteGoal);
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Delete Goal is displayed by employee Goal Id of '"
									+ pkGsrDatEmpGoalId + "'", deleteGoal1));
		} catch (CommonCustomException e) {
			throw new CommonCustomException("Goal is already Deleted", e);
		} catch (GoalNotFoundException e) {
			throw new GoalNotFoundException("Goal does not exist", e);
		} catch (Exception e) {
			throw new BusinessException("exception ", e);
		}
		// LOG.endUsecase("deleteGoal");
		// System.out.println("Exited %%%%%%%%%%%%%%%%%%%%%%5 in controller");
		// return deleteGoal;

	}

	/**
	 * <Description getCurrentGoals:> This contains the methods used for the
	 * GSR_VIEW_CURRENT_GOALS for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @see
	 */
	@ApiOperation(tags = "GSR", value = "View the GSR Goal."
			+ "To view goal details.", httpMethod = "GET", notes = "This Service has been implemented to view GSR Goal details."
			+ "<br>The Response will be the detailed of the view goal."
			+ "<br><b>Inputs :</b><br> GSR View Goal. <br><br>",

	nickname = "GSR View Goal", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GSR_VIEW_GOAL, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> viewGsrGoal(
			@PathVariable("pkGsrDatEmpGoalId") Integer pkGsrDatEmpGoalId)
			throws CommonCustomException {
		LOG.startUsecase("viewGoal");
		List<ViewEmployeeGoalsOutputBean> viewGsrGoal = gsrGoalService
				.viewGsrGoal(pkGsrDatEmpGoalId);
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"View Goal is displayed by pkGsrDatEmpGoalId ",
						viewGsrGoal));
	}

	/* Ended By Ritesh and Edited by Rajesh Kumar 15/12/2016 */

	/* Started By Rajesh Kumar */
	/**
	 * <Description getCurrentGoals:> This contains the methods used for the
	 * GSR_VIEW_CURRENT_GOALS for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws GoalNotFoundException
	 * @see
	 */
	@ApiOperation(tags = "GSR", value = "View the GSR Current Goals."
			+ "To view current goals details.", httpMethod = "PUT", notes = "This Service has been implemented to view GSR Current Goals details."
			+ "<br>The Response will be the detailed of the current goals based on GoalID.", nickname = "GSR View Current Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GSR_VIEW_CURRENT_GOALS, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> viewCurrentGoals(
			@Valid @ApiParam(name = "viewcurrentgoals", example = "1", value = "{"
					+ "<br> pkGsrDatEmpGoalId: 1,"
					+ "<br> gsrGoalStartDate: '2016-10-01 00:00:00',"
					+ "<br> gsrGoalEndDate: '2016-12-31 00:00:00',"
					+ "<br> "
					+ "}") @RequestBody GsrViewCurrentGoalsBean bean,
			BindingResult binding) throws BusinessException,
			MethodArgumentNotValidException, GoalNotFoundException {
		LOG.startUsecase("View Current Goals");
		GsrDatEmpGoalBO viewCurrentGoal = new GsrDatEmpGoalBO();
		List<GsrDatEmpGoalBO> viewCurrentGoal1 = new ArrayList<GsrDatEmpGoalBO>();
		try {
			if (binding.hasErrors()) {

				return ValidationUtils.handleValidationErrors(binding);
			}
			viewCurrentGoal = gsrGoalService.getDatEmpGoal(bean);
			viewCurrentGoal1.add(viewCurrentGoal);
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(
							HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Current Goals are displayed by GoalID, StartDate and EndDate.",
							viewCurrentGoal1));
		} catch (GoalNotFoundException e) {
			throw new GoalNotFoundException("Goal does not exist");
		} catch (Exception e) {
			throw new BusinessException("exception ", e);
		}
		// LOG.endUsecase("View Current Goals");
		// return viewCurrentGoal;
	}

	/**
	 * <Description getAccomplishment:> This contains the methods used for the
	 * GSR_ENTER_ACCOMPLISHMENT for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws GoalNotFoundException
	 * @throws CommonCustomException
	 * @throws ParseException
	 * @see
	 */
	@ApiOperation(tags = "GSR", value = "Enter Accomplishment."
			+ "To enter Accomplishment details.", httpMethod = "PUT", notes = "This Service has been implemented to enter Accomplishment details."
			+ "<br>The Response will be the detailed of the Accomplishment of the given GoalID.", nickname = "GSR Enter Accomplishment", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GSR_ENTER_ACCOMPLISHMENT, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> getAccomplishment(
			@Valid @ApiParam(name = "enteraccomplishment", example = "1", value = "{"
					+ "<br> pkGsrDatEmpGoalId: 1,"
					+ "<br> gsrGoalSelfRating: 4.5,"
					+ "<br> gsrGoalAccomplishment: 'Good'," + "<br> " + "}") @RequestBody GsrEnterAccomplishmentBean bean)
			throws BusinessException, GoalNotFoundException, ParseException,
			CommonCustomException {
		LOG.startUsecase("Enter Accomplishment");
		GsrEnterAccomplishmentBean enterAccomplishment = new GsrEnterAccomplishmentBean();
		List<GsrEnterAccomplishmentBean> enterAccomplishment1 = new ArrayList<GsrEnterAccomplishmentBean>();
		enterAccomplishment = gsrGoalService.getAccomplishment(bean);
		enterAccomplishment1.add(enterAccomplishment);
		
		if(enterAccomplishment.isUpdateGoalFlag()){
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"Your accomplishments have been updated successfully.",
							enterAccomplishment1));
		}else{	
			return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"Your accomplishments have been entered successfully.",
						enterAccomplishment1));
		}
	}

	/**
	 * 
	 * @param bean
	      bean contains the input details are to be used in the service.
	 * @return GsrCreateDelegatedGoalOutputBean
	 * @throws CommonCustomException
	      if there is present any Common Custom Exception
	 * @throws DataAccessExceptio
	 * @Description: This service is used to create delegate goals
	 */
	
	@ApiOperation(tags = "GSR", value = "Delegate Goal."
			+ "To the Delegate goal details.", httpMethod = "POST", notes = "This Service has been implemented the Delegate goals details."
			+ "<br>The Response will be the detailed of the Delegate goal.", nickname = "GSR Delegate goal", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpDelegatedGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpDelegatedGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GSR_DELEGATE_GOAL, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> createDelegateGoal(
			@Valid @ApiParam(name = "delegategoal", example = "1", value = "{"
					+ "<br> goalIdList:125,"
					+ "<br> delegateToManagerId:1340,"
					+ "<br> delegatedBy:3811,"
					+ "<br> " + "}") @RequestBody GsrDelegateGoalBean inputBean)
			throws CommonCustomException,
			GoalNotFoundException, DataAccessException {
		LOG.startUsecase("Delegate Goal");
		List<GsrCreateDelegatedGoalOutputBean> outputBeanList = new ArrayList<GsrCreateDelegatedGoalOutputBean>();
		GsrCreateDelegatedGoalOutputBean delegatedGoalOutputBean = new GsrCreateDelegatedGoalOutputBean();
		delegatedGoalOutputBean = gsrGoalService.createDelegateGoal(inputBean);
		
		outputBeanList.add(delegatedGoalOutputBean);
		
		if(!outputBeanList.isEmpty()){
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Goals delegated successfully.",
							outputBeanList));
		}
			else {
				return ResponseEntity
						.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.FAILURE,
								"Goals delegation failed."));
			}
			
	}


	/**
	 * <Description getAcceptRating:> This contains the methods used for the
	 * GSR_ACCEPT_RATING for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws GoalNotFoundException
	 * @throws ParseException
	 * @throws CommonCustomException
	 * @see
	 */
	@ApiOperation(tags = "GSR", value = "Accept Rating."
			+ "To the Accept Rating details.", httpMethod = "PUT", notes = "This Service has been implemented for Accept Rating."
			+ "<br>The Response will be the detailed of the Accept Rating.", nickname = "GSR Accept Rating", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpFinalRatingBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpFinalRatingBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GSR_ACCEPT_RATING, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> getAcceptRating(
			@Valid @ApiParam(name = "acceptrating", example = "1", value = "Accept Rating.") @PathVariable("empId") Integer empId,
			@PathVariable("slabId") Short slabId) throws BusinessException,
			GoalNotFoundException, ParseException, CommonCustomException {
		LOG.startUsecase("Accept Rating");
		boolean acceptRating;
		acceptRating = gsrGoalService.getAcceptRating(empId, slabId);

		if (acceptRating) {
			LOG.endUsecase("Accept Rating");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Employee has Accepted the rating."));
		} else {
			LOG.endUsecase("Accept Rating");
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.SUCCESS,
							"Some error happened in Accept Rating."));
		}

	}

	/**
	 * 
	 * @param delegationGoalsBean
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This service is used to view the goals that can be
	 *              delegated and goals which are delegated to or from empId
	 */
	@ApiOperation(tags = "GSR", value = " View Delegation Goals for HR."
			+ "This service will be called from the front-end when the user wants to View Delegated Goals", httpMethod = "POST", notes = "This Service has been implemented to view Team goals."
			+ "<br>The Details will be retrieved in database.",

	nickname = "GSR View Delegated Goals HR", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Delegated goals resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_GOALS_TO_DELEGATE_IN_HR, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> viewDelegatedGoalsinHR(
			@ApiParam(name = "DelegationGoalInputBeanHR", example = "DelegationGoalInputBeanHR", value = "Delegation Goals Input Bean with all tha data to view the Team Goals.") @Valid @RequestBody DelegationGoalInputBeanHR delegationGoalsBean,
			BindingResult binding) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("Entering viewDelegatedGoalsinHR");
		ViewTeamGoalsOutputBean empGoals;
		DelegationGoalsBean goalBean;
		List<DelegationGoalsBean> delegatedGoalList = new ArrayList<DelegationGoalsBean>();
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				goalBean = gsrGoalService
						.viewDelegatedGoalsinHR(delegationGoalsBean);
				delegatedGoalList.add(goalBean);
			}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (goalBean.getReportees().size() > 0) {
			LOG.endUsecase("Exiting viewDelegatedGoalsinHR");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Team Goals List.",
							delegatedGoalList));
		} else {
			LOG.endUsecase("Exiting viewDelegatedGoalsinHR");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS, "No record found.",
							delegatedGoalList));
		}
	}
	
	/**
	 * 
	 * @param gsrEmpGoalDetailsBean
	 * @return List<ViewEmployeeGoalsOutputBean>
	 * @throws BusinessException
	 * @throws MethodArgumentNotValidException
	 * @Description: This method is used to view the employee goals based on
	 *               empId, empStartDate and empEndDate.
	 */
	@ApiOperation(tags = "GSR", value = "GET EMPLOYEE GOALS DELEGATED"
			+ "This service will be called from the front-end when the user wants to view the employee goals based on empId, empStartDate and empEndDate", httpMethod = "POST", notes = "This Service has been implemented to view the employee goals based on empId, empStartDate and empEndDate."
			+ "<br>The Details will be stored in database."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the created goals and status"
			+ "<br>"
			+ "<br><b>Inputs :</b><br> GsrEmployeeGoalDetailsBean <br><br>"
			+ "<br> id : mgrId in the format of integer <br>"
			+ "<br> empStartDate : Date in the format yyyy-MM-dd. <br>"
			+ "<br> empEndDate : Date in the format yyyy-MM-dd. <br>",

	nickname = "GSR EMPLOYEE GOALS DELEGATED", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Employee goals  resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_EMPLOYEE_GOALS_DELEGATED, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewEmployeeGoalsWhichAreDelegated(
			@Valid @ApiParam(name = "GSR View Employee Goals", example = "1", value = "{"
					+ "<br> empId: 1,"
					+ "<br> empStartDate: 2016-10-01,"
					+ "<br> empEndDate: 2016-12-31," 
					+ "<br> delegatedTo: 1 "+ "<br> " + "}") @RequestBody GSRDelegatedEmpGoalBean gsrEmpGoalDetailsBean,
			BindingResult binding) throws BusinessException,
			MethodArgumentNotValidException {
		LOG.startUsecase("view Employee Goals which are delegated");
		try {

			if (binding.hasErrors()) {

				return ValidationUtils.handleValidationErrors(binding);
			}
			List<ViewEmployeeGoalsOutputBean> viewEmployeeGoals = gsrGoalService
					.viewEmployeeGoalsWhichAreDelegated(gsrEmpGoalDetailsBean);
			LOG.endUsecase("view Employee Goals which are delegated");

			if (viewEmployeeGoals.size() == 0) {

				return ResponseEntity.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"No Employee  Goals to display",viewEmployeeGoals));
			} else {
				LOG.endUsecase("view Team Goals");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS, "Employee Goals are displayed,",
								viewEmployeeGoals));
			}
		} catch (Exception e) {

			throw new BusinessException("exception ", e);
		}

	}
	
	/* Ended By Rajesh Kumar */

	// Added by Mani for GSR module service committed on 30-11-2016

	@ApiOperation(tags = "GSR", value = " To View The Employee Goal Set Status For current Year."
			+ "", httpMethod = " GET ", notes = " This Service Has Been Implemented To View The Employee Goal Set Status Details by Passing Employee Id and Slab Id."
			+ "<br> <b> Table : </b> </br>"
			+ "1)<b>gsr_dat_emp_goal</b> -> It contains all the created goals and status <br>"
			+ "2)<b>gsr_dat_emp_final_rating</b> ->Checking for final rating status whether goal is closed or not."
			+ "<br>"
			+ "<b>Rules:</b></br>"
			+ "1)Weightage is 100 then goal status is 'goal set' or else 'goal not set'.<br>"
			+ "2)If employee got the final rating and has accepted and force closed from HR then goal status will be 'Goal closed' or else goal status will be not 'goal not closed'</br></br>", nickname = " GSR Goal Set For Employee Id with Slab wise ", protocols = " HTTP ", responseReference = " application/json ", produces = " application/json", response = GsrGoalSetStatusBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Goal Set Status Retrieveed Successfully.", response = GsrGoalSetStatusBean.class),
			@ApiResponse(code = 404, message = "Employee Id or Slab Id  Not Found In Table.", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GOALS_SET_STATUS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> getGoalSetStatusForEmployeeBySlabId(
			@ApiParam(name = "goal status for employee", value = "{"
					+ "<br> empId: 1<br>"
				 + "}") @RequestBody @Valid GsrGoalSetStatusBean gsrGoalSetStatusBean,
			BindingResult binding) throws BusinessException,
			DataAccessException, CommonCustomException {
		List<GsrGoalSetStatusBean> gsrGoalSetStatusBeanList = new ArrayList<GsrGoalSetStatusBean>();
		DatEmpProfessionalDetailBO employeeInfo = new DatEmpProfessionalDetailBO();
		 
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				employeeInfo = commonService
						.getEmpoyeeDeatailsById(gsrGoalSetStatusBean.getEmpId());
				if (employeeInfo != null) {
					gsrGoalSetStatusBeanList = gsrGoalService
							.goalCurrentYearSetStatusForEmployee(
									gsrGoalSetStatusBean, employeeInfo);
				} else {
					return ResponseEntity.status(HttpStatus.OK.value()).body(
							new MISResponse(HttpStatus.PRECONDITION_FAILED
									.value(), MISConstants.FAILURE,
									"Invalid employee id"));
				}
				if (gsrGoalSetStatusBeanList.size() > 0) {
					return ResponseEntity.status(HttpStatus.OK.value()).body(
							new MISResponse(HttpStatus.OK.value(),
									MISConstants.SUCCESS,
									"Goal Set Status For An Employee.",
									gsrGoalSetStatusBeanList));
				} else {
					return ResponseEntity
							.status(HttpStatus.OK.value())
							.body(new MISResponse(
									HttpStatus.NO_CONTENT.value(),
									MISConstants.FAILURE,
									"Resource Data Is Not Available For Your Inputs"));
				}
			}

	}

	@ApiOperation(tags = "GSR", value = " To View The Employee Goal Set Status For current year under domain manager id"
			+ "", httpMethod = " GET ", notes = " This Service Has Been Implemented To View The Employee Goal Set Status Details by Passing Employee Id and Slab Id."
			+ "<br> <b> Table : </b> </br>"
			+ "1)<b>gsr_dat_emp_goal</b> -> It contains all the created goals and status for domain manager<br/>"
			+ "2)<b>gsr_dat_emp_final_rating</b> ->Checking for final rating status whether goal is closed or not."
			+ "<br>"
			+ "<b>Rules:</b></br>"
			+ "1)Weightage is 100 then goal status is 'goal set' or else 'goal not set'.</br>"
			+ "2)If employee got the final rating and has accepted and force closed from HR then goal status will be 'Goal closed' or else goal status will be not 'goal not closed'</br></br>", nickname = " GSR Goal Set For Employee Id with Slab wise ", protocols = " HTTP ", responseReference = " application/json ", produces = " application/json", response = GsrGoalSetStatusBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Goal Set Status Retrieveed Successfully.", response = GsrGoalSetStatusBean.class),
			@ApiResponse(code = 404, message = "Employee Id or Slab Id  Not Found In Table.", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GOALS_STATUS_DOMAIN, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> getGoalStatusForDomain(
			@ApiParam(name = "goal status for domain manager", value = "{"
					+ "<br> domainManagerId: 115,<br>" + "}") @RequestBody @Valid GsrGoalSetStatusBean gsrGoalSetStatusBean,
			BindingResult binding) throws BusinessException,
			DataAccessException, CommonCustomException {
		List<GsrTeamGoalStatusBean> gsrTeamGoalStatusBeanOutList = new ArrayList<GsrTeamGoalStatusBean>();
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				gsrTeamGoalStatusBeanOutList = gsrGoalService
						.goalCurrentYearSetStatusForDomainManager(gsrGoalSetStatusBean);
				if (gsrTeamGoalStatusBeanOutList.size() > 0) {
					LOG.endUsecase("Exited Goal Set Status For an Employee in `GSRGoalController`");
					return ResponseEntity.status(HttpStatus.OK.value()).body(
							new MISResponse(HttpStatus.OK.value(),
									MISConstants.SUCCESS,
									"Goal Set Status For Domain Manager of emp id is "+gsrGoalSetStatusBean.getDomainManagerId(),
									gsrTeamGoalStatusBeanOutList));
				} else {
					return ResponseEntity
							.status(HttpStatus.OK.value())
							.body(new MISResponse(
									HttpStatus.NO_CONTENT.value(),
									MISConstants.FAILURE,
									"Resource Data Is Not Available For Your Inputs"));
				}
			}
	}

	@ApiOperation(tags = "GSR", value = " To View The Employee current year  Goal Set Status under bu  "
			+ "", httpMethod = " GET ", notes = " This Service Has Been Implemented To View The Employee Goal Set Status Details by Passing Employee Id and Slab Id."
			+ "<br> <b> Table : </b> </br>"
			+ "1)<b>gsr_dat_emp_goal</b> -> It contains all the created goals and status for business unit<br/>"
			+ "2)<b>gsr_dat_emp_final_rating </b>->Checking for final rating status whether goal is closed or not."
			+ "<br>"
			+ "<b>Rules:</b></br>"
			+ "1)Weightage is 100 then goal status is 'goal set' or else 'goal not set'.</br>"
			+ "2)If employee got the final rating and has accepted and force closed from HR then goal status will be 'Goal closed' or else goal status will be not 'goal not closed'</br></br>", nickname = " GSR Goal Set For Employee Id with Slab wise ", protocols = " HTTP ", responseReference = " application/json ", produces = " application/json", response = GsrGoalSetStatusBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Goal Set Status Retrieveed Successfully.", response = GsrGoalSetStatusBean.class),
			@ApiResponse(code = 404, message = "Employee Id or Slab Id  Not Found In Table.", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	/**
	 * 
	 * @param empId
	 * @param slabId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@RequestMapping(value = GsrURIConstants.GOALS_STATUS_BU, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> goalStatusForBu(
			@ApiParam(name = "goal status for domain bu", value = "{"
					+ "<br> buUnit: 23,<br>" + "}") @RequestBody @Valid GsrGoalSetStatusBean gsrGoalSetStatusBean,
			BindingResult binding) throws BusinessException,
			DataAccessException {
		List<GsrTeamGoalStatusBean> gsrTeamGoalStatusBeanOutList = new ArrayList<GsrTeamGoalStatusBean>();
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				gsrTeamGoalStatusBeanOutList = gsrGoalService
						.goalCurrentYearSetStatusForBu(gsrGoalSetStatusBean);
				if (gsrTeamGoalStatusBeanOutList.size() > 0) {
					LOG.endUsecase("Exited Goal Set Status For an Employee in `GSRGoalController`");
					return ResponseEntity.status(HttpStatus.OK.value()).body(
							new MISResponse(HttpStatus.OK.value(),
									MISConstants.SUCCESS,
									"Goal Set Status For a  Bu of "+gsrGoalSetStatusBean.getBuUnit(),
									gsrTeamGoalStatusBeanOutList));
				} else {
					return ResponseEntity
							.status(HttpStatus.OK.value())
							.body(new MISResponse(
									HttpStatus.NO_CONTENT.value(),
									MISConstants.FAILURE,
									"Resource Data Is Not Available For Your Inputs"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(
					"Exception Occured Method of  'goalSetStatus' in `GSRGoalController`   Exception is ",
					e);
		}

	}

	@ApiOperation(tags = "GSR", value = " To View The Employee current year Goal Set Status under reporting manager."
			+ "", httpMethod = " GET ", notes = " This Service Has Been Implemented To View The Employee Goal Set Status Details by Passing Employee Id and Slab Id."
			+ "<br> <b> Table : </b> </br>"
			+ "1)<b>gsr_dat_emp_goal</b> -> It contains all the created goals and status for reporting  manager<br/>"
			+ "2)<b>gsr_dat_emp_final_rating</b> ->Checking for final rating status whether goal is closed or not."
			+ "<br>"
			+ "<b>Rules:</b></br>"
			+ "1)Weightage is 100 then goal status is 'goal set' or else 'goal not set'.</br>"
			+ "2)If employee got the final rating and has accepted and force closed from HR then goal status will be 'Goal closed' or else goal status will be not 'goal not closed'</br></br>", nickname = " GSR Goal Set For Employee Id with Slab wise ", protocols = " HTTP ", responseReference = " application/json ", produces = " application/json", response = GsrGoalSetStatusBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Goal Set Status Retrieveed Successfully.", response = GsrGoalSetStatusBean.class),
			@ApiResponse(code = 404, message = "Employee Id or Slab Id  Not Found In Table.", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	/**
	 * 
	 * @param empId
	 * @param slabId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@RequestMapping(value = GsrURIConstants.GOALS_STATUS_DIRECT_REPORTEES, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> getGoalStatusForReportingManagerId(
			@ApiParam(name = "goal status for reporing  manager", value = "{"
					+ "<br> reportingManagerId: [1340]<br>" + "}") @RequestBody @Valid GsrGoalSetStatusBean gsrGoalSetStatusBean,
			BindingResult binding) throws BusinessException,
			DataAccessException, CommonCustomException {
		List<GsrTeamGoalStatusBean> gsrTeamGoalStatusBeanOutList = new ArrayList<GsrTeamGoalStatusBean>();
	
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				gsrTeamGoalStatusBeanOutList = gsrGoalService
						.goalCurrentYearSetStatusForRM(gsrGoalSetStatusBean);
				if (gsrTeamGoalStatusBeanOutList.size() > 0) {
					LOG.endUsecase("Exited Goal Set Status For an Employee in `GSRGoalController`");
					return ResponseEntity.status(HttpStatus.OK.value()).body(
							new MISResponse(HttpStatus.OK.value(),
									MISConstants.SUCCESS,
									"Goal Set Status For a RM.",
									gsrTeamGoalStatusBeanOutList));
				} else {
					return ResponseEntity
							.status(HttpStatus.OK.value())
							.body(new MISResponse(
									HttpStatus.NO_CONTENT.value(),
									MISConstants.FAILURE,
									"Resource Data Is Not Available For Your Inputs"));
				}
			}
		 

	}
	
/*	@ApiOperation(tags = "GSR", value = " To View The Employee Goal Set Status For the Slab."
			+ "", httpMethod = " GET ", notes = " This Service Has Been Implemented To View The Employee Goal Set Status Details by Passing Employee Id and Slab Id."
			+ " <br> "
			+ " <br> "
			+ " <b>"
			+ " Inputs : </b> <br> Employee Id and Slab Id <br> <br>", nickname = " GSR Goal Set For Employee Id with Slab wise ", protocols = " HTTP ", responseReference = " application/json ", produces = " application/json", response = GsrGoalSetStatusBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Goal Set Status Retrieveed Successfully.", response = GsrGoalSetStatusBean.class),
			@ApiResponse(code = 404, message = "Employee Id or Slab Id  Not Found In Table.", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })*/
	/**
	 * 
	 * @param empId
	 * @param slabId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	//Commented by Shyam for re-coding of creation of goals for multiple reportees
	/*@RequestMapping(value = GsrURIConstants.CREATE_GOALS_LIST_OF_EMP, method = RequestMethod.POST)
	@ResponseBody
	ResponseEntity<?> createGsrGoalsForListOfEmployees(
			@Valid @RequestBody List<GsrCreateGoalBean> bean)
			throws BusinessException {
		LOG.startUsecase("createGoal");
		List<GsrDatEmpGoalBO> output = new ArrayList<GsrDatEmpGoalBO>();
		String CREATE = "CREATE";
		try {
			output = gsrGoalService.saveGoals(bean, CREATE);
			System.out.println("Output " + output);
		} catch (Exception e) {
			throw new BusinessException("exception ", e);
		}
		LOG.endUsecase("createGoal");
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"Created Goals For Reportees", output));
	}
*/
	/*@RequestMapping(value = GsrURIConstants.UPDATE_GOALS_LIST_OF_EMP, method = RequestMethod.POST)
	@ResponseBody
	ResponseEntity<?> updateGsrGoalsForListOfEmployees(
			@Valid @RequestBody List<GsrCreateGoalBean> bean)
			throws BusinessException {
		LOG.startUsecase("createGoal");
		List<GsrDatEmpGoalBO> output = new ArrayList<GsrDatEmpGoalBO>();
		String UPDATE = "UPDATE";
		try {
			output = gsrGoalService.saveGoals(bean, UPDATE);
		} catch (Exception e) {
			throw new BusinessException("exception ", e);
		}
		LOG.endUsecase("createGoal");
		return new ResponseEntity<Object>(output, HttpStatus.OK);
	}*/

	@ApiOperation(tags = "GSR", value = " To View The Employee Goal Set Status For the Slab."
			+ "", httpMethod = " GET ", notes = " This Service Has Been Implemented To View The Employee Goal Set Status Details by Passing Employee Id and Slab Id."
			+ " <br> "
			+ " <br> "
			+ " <b>"
			+ " Inputs : </b> <br> Employee Id and Slab Id <br> <br>", nickname = " GSR Goal Set For Employee Id with Slab wise ", protocols = " HTTP ", responseReference = " application/json ", produces = " application/json", response = GsrGoalSetStatusBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Goal Set Status Retrieveed Successfully.", response = GsrGoalSetStatusBean.class),
			@ApiResponse(code = 404, message = "Employee Id or Slab Id  Not Found In Table.", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	/**
	 * 
	 * @param empId
	 * @param slabId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@RequestMapping(value = GsrURIConstants.IS_GOAL_CLOSED_FOR_PAST_THREE_SLAB, method = RequestMethod.GET)
	@ResponseBody
	ResponseEntity<?> IsGoalClosedForLastThreeSlab(
			@RequestBody @Valid GsrGoalClosedStatusBean gsrGoalClosedStatusBean,
			BindingResult binding) throws BusinessException {
		LOG.startUsecase("IsGoalClosedForLastThreeSlab");
		List<GsrGoalClosedStatusBean> gsrGoalClosedStatusList = new ArrayList<GsrGoalClosedStatusBean>();
		GsrSlabBO slabDataById = new GsrSlabBO();
		DatEmpProfessionalDetailBO employeeInfo = new DatEmpProfessionalDetailBO();
		try {
			if (binding.hasErrors()) {

				return ValidationUtils.handleValidationErrors(binding);

			} else {

				employeeInfo = commonService
						.getEmpoyeeDeatailsById(gsrGoalClosedStatusBean
								.getEmpId());

				gsrGoalClosedStatusList = gsrGoalService
						.IsGoalClosedForLastThreeSlab(gsrGoalClosedStatusBean,
								employeeInfo);

			}

		} catch (Exception e) {
			throw new BusinessException("Exception", e);
		}
		LOG.endUsecase("IsGoalClosedForLastThreeSlab");
		return new ResponseEntity<Object>(gsrGoalClosedStatusList,
				HttpStatus.OK);
	}

	@ApiOperation(tags = "GSR", value = " To View The Employee Goal Set Status For the Slab."
			+ "", httpMethod = " GET ", notes = " This Service Has Been Implemented To View The Employee Goal Set Status Details by Passing Employee Id and Slab Id."
			+ " <br> "
			+ " <br> "
			+ " <b>"
			+ " Inputs : </b> <br> Employee Id and Slab Id <br> <br>", nickname = " GSR Goal Set For Employee Id with Slab wise ", protocols = " HTTP ", responseReference = " application/json ", produces = " application/json", response = GsrGoalSetStatusBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Goal Set Status Retrieveed Successfully.", response = GsrGoalSetStatusBean.class),
			@ApiResponse(code = 404, message = "Employee Id or Slab Id  Not Found In Table.", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	/**
	 * 
	 * @param empId
	 * @param slabId
	 * @return
	 * @throws BusinessException
	 * @throws DataAccessException
	 */
	@RequestMapping(value = GsrURIConstants.OVERRIDEEN_FEEDBACK, method = RequestMethod.POST)
	@ResponseBody
	ResponseEntity<?> overideenFinalRating(
			@RequestBody GsrDatEmpFinalRatingBO gsrDatEmpFinalRatingBO) {
		try {
			gsrDatEmpFinalRatingBO = gsrGoalService
					.overideenFinalRating(gsrDatEmpFinalRatingBO);
		} catch (DataAccessException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Object>(gsrDatEmpFinalRatingBO, HttpStatus.OK);

	}

	// End of addition by Mani for GSR module service committed on 30-11-2016
	// Added by Balaji 15-12-16
	/**
	 * 
	 * @param empId
	 *            represents the employee id of the organization.
	 * @param startDate
	 *            represents the GSR start date.
	 * @param endDate
	 *            represents the GSR end date.
	 * @return goalStatus represents the status of the Goal.
	 * @throws DataAccessException
	 * @description This service will return Status of which is closed or not.
	 */
	@ApiOperation(tags = "GSR", value = "To Check whether goals are closed or not.", httpMethod = "POST", notes = "This Service has been implemented to check the goal status."
			+ "<br>The Response will be the detailed of Goal closed status."
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1) gsr_dat_emp_goal <br><br>"
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Goal status display closed when all the goals of respective employee should be closed",

	nickname = "Is Goal Closed",

	protocols = "HTTP", responseReference = "application/json", produces = "application/json",

	response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Employee Goals status displayed", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Education resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Employee has no Goals", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.IS_GOAL_CLOSED, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> isGoalStatusClosed(
			@Valid @ApiParam(name = "IsGoalClosedInputs", value = "{"
					+ "<br> fkGsrDatEmpId : 1,"
					+ "<br> gsrGoalStartDate: 2016-10-01,"
					+ "<br> gsrGoalEndDate: 2016-12-31," + "<br> " + "}") @RequestBody GsrGoalsClosedBean gsrGoalsClosedBean,
			BindingResult binding) throws DataAccessException {
		String goalStatusMessage = null;
		List<String> goalStatus = new ArrayList<String>();
		// TODO
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				goalStatusMessage = gsrGoalService
						.isGoalStatusClosed(gsrGoalsClosedBean);
				goalStatus.add(goalStatusMessage);
			}

		} catch (Exception e) {
			throw new DataAccessException("Error while getting goals", e);
		}
		if (goalStatus.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Employee Goals status displayed", goalStatus));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE, "Employee has no Goals",
							goalStatus));
		}
	}
	
	/**
	 * 
	 * @param empId
	 *            represents the employee id of the organization.
	 * @param startDate
	 *            represents the GSR start date.
	 * @param endDate
	 *            represents the GSR end date.
	 * @return goalStatus represents the status of the Goal.
	 * @throws DataAccessException
	 * @description This service will return Status of which is closed or not.
	 */
	@ApiOperation(tags = "GSR", value = "To Check whether goals are closed or not for reportees", httpMethod = "POST", notes = "This Service has been implemented to check the goal status."
			+ "<br>The Response will be the detailed of Goal closed status."
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1) gsr_dat_emp_goal <br><br>"
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Goal status display closed when all the goals of respective reportees should be closed",

	nickname = "Is Goal Closed",

	protocols = "HTTP", responseReference = "application/json", produces = "application/json",

	response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Employee Goals status displayed", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Education resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Employee has no Goals", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.IS_REPORTEES_GOALS_CLOSED, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<LegacyMISResponse> isGoalStatusClosedForReportees(
			@Valid @ApiParam(name = "IsGoalClosedInputs", value = "{"
					+ "<br> fkGsrDatEmpId : 1,"
					 + "<br> " + "}") @RequestBody GsrGoalsClosedBean gsrGoalsClosedBean,
			BindingResult binding) throws DataAccessException {
		String goalStatusMessage = null;
		List<Integer> goalBo = new ArrayList<Integer>();
		GsrGoalsClosedBean gsrClosedGoalBean = new GsrGoalsClosedBean();
		// TODO
		try {
			
				goalBo = gsrGoalService
						.isGoalStatusClosedForReportees(gsrGoalsClosedBean);
				gsrClosedGoalBean.setListOfEmpId(goalBo);
			

		} catch (Exception e) {
			throw new DataAccessException("Error while getting goals", e);
		}
		if (gsrClosedGoalBean.getListOfEmpId().size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new LegacyMISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Employee Goals status displayed", gsrClosedGoalBean.getListOfEmpId()));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new LegacyMISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE, "Employee has no Goals",
							gsrClosedGoalBean.getListOfEmpId()));
		}
	}

	/**
	 * 
	 * @Description This service is used to view the requested training.
	 * @param gsrTrainingRequestInputBean
	 *            is represents Input Bean.
	 * @param binding
	 * @return gsrTrainingRequestBOList
	 * @throws BusinessException
	 */
	@ApiOperation(tags = "GSR", value = "This service will fetch the requested training details and display it.", httpMethod = "POST", notes = "This Service has been implemented to see the details of requested training for an employee."
			+ "<br>The Response will be the detailed of the requested training details."
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1)gsr_training_request. <br><br>"
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) This service is view requested training details by passing either topic id or by date range",

	nickname = "View the requested Training",

	protocols = "HTTP", responseReference = "application/json", produces = "application/json",

	response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Employee Requests are displayed", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Education resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Training Topic Id not found", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.VIEW_TRAINING_REQUESTED, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewTrainingRequest(
			@Valid @ApiParam(name = "View training inputs", value = "{"
					+ "<br> fkGsrTrainingTopicId : 1," + "<br> (or),"
					+ "<br> gsrGoalStartDate: 2016-11-01,"
					+ "<br> gsrGoalEndDate: 2016-11-04," + "<br> " + "}") @RequestBody GsrTrainingRequestSearchBean gsrTrainingRequestInputBean,
			BindingResult binding) throws BusinessException {
		List<GsrTrainingRequestBO> gsrTrainingRequestBOList;
		try {
			// TODO
			if (binding.hasErrors()) {

				return ValidationUtils.handleValidationErrors(binding);
			}

			gsrTrainingRequestBOList = gsrGoalService
					.viewTrainingRequested(gsrTrainingRequestInputBean);
			if (gsrTrainingRequestBOList.size() < 1) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.FAILURE,
								"Training Topic Id not found.",
								gsrTrainingRequestBOList));
			} else {
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Employee Requests are displayed.",
								gsrTrainingRequestBOList));
			}
		} catch (Exception e) {
			throw new BusinessException("Exception occured", e);
		}

	}

	/**
	 * 
	 * @Description This service is used to create the training requests.
	 * @param gsrTrainingRequestBeanInput
	 *            is represents Input Bean.
	 * @return trainingRequestBo
	 * @throws BusinessException
	 */
	@ApiOperation(tags = "Voucher", value = "This service used to create the new training request.", httpMethod = "POST", notes = "This Service has been implemented to Submit Gift Voucher Claims details."
			+ "<br>The Response will be the detailed of Creating service request."
			+ "<br> 1)gsr_training_request. <br><br>"
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) This service is Create training Training request by passing all required fields",

	nickname = "Create the Training request",

	protocols = "HTTP", responseReference = "application/json", produces = "application/json",

	response = GsrTrainingRequestBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Employee Requests are displayed", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Education resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Request is not created", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.REQUEST_FOR_TRAINING, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> createRequestForTraining(
			@Valid @ApiParam(name = "Training Creation inputs", value = "{"
					+ "<br> comments : TEST ,"
					+ "<br> fkGsrTrainingTopicId: 1,"
					+ "<br> gsrTrainingRequestedBy: 3,"
					+ "<br> gsrTrainingRequestedDate: 2016-11-02,"
					+ "<br> fkTrainingRequestedForEmpId: 3,"
					+ "<br> otherTopicDescription: Testtopic," + "<br> " + "}") @RequestBody GsrTrainingRequestBean gsrTrainingRequestBeanInput)
			throws BusinessException {
		// TODO
		GsrTrainingRequestBO gsrTrainingRequestBeanOutput = new GsrTrainingRequestBO();
		List<GsrTrainingRequestBO> gsrTrainingRequestOutput = new ArrayList<GsrTrainingRequestBO>();
		try {
			gsrTrainingRequestBeanOutput = gsrGoalService
					.createRequestForTraining(gsrTrainingRequestBeanInput);
			if (gsrTrainingRequestBeanOutput == null) {
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.SUCCESS, "Request is not created."));
			} else {
				gsrTrainingRequestOutput.add(gsrTrainingRequestBeanOutput);
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Employee Requests are displayed.",
								gsrTrainingRequestOutput));
			}

		} catch (Exception e) {
			throw new BusinessException("Exception occured", e);

		}

	}

	/**
	 * 
	 * @Description This service used to give the workflow permission.
	 * @param gsrWorflowPermissionBeanInputList
	 *            is represents Input Bean.
	 * @param binding
	 * @return gsrWorkflowPermissionBoOutputList
	 * @throws BusinessException
	 */

	@ApiOperation(tags = "Voucher", value = "This service used to Allow permission to modify the Goals.", httpMethod = "POST", notes = "This Service has been implemented to Submit Gift Voucher Claims details."
			+ "<br>The Response will be the detailed Changing the workflow permission."
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1)gsr_workflow_permission <br><br>"
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) This service updates the workflow permission by changing the workflow permission type"
			+ "<br> 2) For Goal Creation permission type id is 1. "
			+ "<br> 3) For Goal Accomplishment permission type id is 2. "
			+ "<br> 4) For Goal Feedback permission type id is 3. ",

	nickname = "Allow permission to modify the goals",

	protocols = "HTTP", responseReference = "application/json", produces = "application/json",

	response = GsrWorkflowPermissionBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK,Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Education resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.ALLOW_GOAL_PERMISSIONS, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> allowPermissionForGoalModification(
			@Valid @ApiParam(name = "Workflow permission inputs", value = "{"
					+ "<br> entityList: [" + "<br> EmpId : 1 ,"
					+ "<br> SlabId: 14," + "<br> RequestedBy: 3,"
					+ "<br> RequestedDate: 2016-11-02,"
					+ "<br> StartDate: 2017-11-03,"
					+ "<br> EndDate: 2017-11-08," + "<br> permissionType: 3,"
					+ "<br> ]" + "}") @RequestBody EntityBeanList<GsrWorflowPermissionBean> gsrWorflowPermissionBeanInputList,
			BindingResult binding) throws BusinessException {

		// TODO
		List<GsrWorkflowPermissionBO> gsrWorkflowPermissionBoOutputList = new ArrayList<GsrWorkflowPermissionBO>();
		try {
			if (binding.hasErrors()) {

				return ValidationUtils.handleValidationErrors(binding);

			} else {
				gsrWorkflowPermissionBoOutputList = gsrGoalService
						.allowPermissionForGoal(gsrWorflowPermissionBeanInputList);
			}
		} catch (Exception e) {
			System.out.println("Exception " + e);
		}
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"Employee Requests are displayed.",
						gsrWorkflowPermissionBoOutputList));

	}

	// End of Addition by Balaji

	/**
	 * 
	 * @param GsrDatEmpGoalBOBeanInput
	 * @return previous goals of particular employee
	 * @throws BusinessException
	 * @input Employee ID
	 * @Description to view past GSR goals of employee id given as input
	 */

	@ApiOperation(tags = "GSR", value = "View past goals"
			+ "This service will be called from the front-end when the user want to view past goals .", httpMethod = "POST", notes = "This Service has been implemented to view past goals",

	nickname = "VIEW_PAST_GOALS_EMPID", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDatEmpGoalBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.VIEW_PAST_GOALS_EMPID, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewGoalsEmpidDate(
			@ApiParam(name = "GsrDatEmpGoalBOBeanInput", example = "1", value = "GsrDatEmpGoalBOBeanInput is required to retrieve the past goals of employees within the given date range.") @RequestBody @Valid GsrDatEmpGoalBOBeanInput gsrDatEmpGoalBOBean,
			BindingResult binding) throws BusinessException {

		LOG.startUsecase("VIEW_PAST_GOALS");
		System.out.println("VIEW_PAST_GOALS_EMPID_CONTROLLER ");

		List<GsrDatEmpGoalBO> emppastgoals = new ArrayList<GsrDatEmpGoalBO>();

		try {

			if (binding.hasErrors()) {

				return ValidationUtils.handleValidationErrors(binding);

			}
			emppastgoals = gsrGoalService.viewPastGaols(gsrDatEmpGoalBOBean);

		}

		catch (Exception e) {
			System.out.println("Exception -------------" + e.getMessage());
			e.printStackTrace();
			throw new BusinessException("exception ", e);
		}
		LOG.endUsecase("VIEW_PAST_GOALS");
		System.out.println("exiting_VIEW_PAST_GOALS_EMPID_CONTROLLER");

		// return new ResponseEntity<Object> (emppastgoals, HttpStatus.OK);
		if (emppastgoals.size() > 0) {
			LOG.endUsecase("exiting getGoalsByEmpId");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Goals List", emppastgoals));
		} else {
			LOG.endUsecase("exiting getGoalsByEmpId");
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No goals present for empid"));
		}
	}

	/**
	 * @input DatEmpProfDetailandGsrEmpGoalBean
	 * @output GsrDatEmpGoalBO
	 * @param gsrDatGoalBean
	 * @return Goals of repotees under Domain Manager
	 * @throws BusinessException
	 * @Description View all goals of doamin by Domain Manager
	 */
	/*
	 * 
	 * @ApiOperation(tags="GSR",value = "View Employee goals by Domain Manager"
	 * +
	 * "This service will be called from the front-end when the Domain Manger  want to view his reportees goals ."
	 * , httpMethod = "POST", notes =
	 * "This Service has been implemented to view past goals",
	 * 
	 * nickname = "VIEW_EMPLOYEE_GOALS_DOMAIN_BYDM", protocols = "HTTP",
	 * responseReference = "application/json", produces = "application/json",
	 * response = GsrDatEmpGoalBO.class)
	 * 
	 * @ApiResponses(value = {
	 * 
	 * @ApiResponse(code = 200, message = "Request Executed Successfully",
	 * response = GsrDatEmpGoalBO.class),
	 * 
	 * @ApiResponse(code = 201, message = "Created Successfully",
	 * responseContainer = "Http Response", responseHeaders =
	 * 
	 * @ResponseHeader(name = "Location", description =
	 * "The created Program Details resource", response = URI.class)),
	 * 
	 * @ApiResponse(code = 404, message = "Resource NOT FOUND",
	 * responseContainer = "Http Response"),
	 * 
	 * @ApiResponse(code = 500, message = "Internal Server Error",
	 * responseContainer = "Http Response") })
	 * 
	 * @RequestMapping(value = GsrURIConstants.VIEW_EMPLOYEE_GOALS_DOMAIN_BYDM,
	 * method = RequestMethod.POST) public @ResponseBody
	 * ResponseEntity<MISResponse> viewEmployeeGoalswithinDomainByDm(
	 * 
	 * @ApiParam(name = "gsrDatGoalBean", value =
	 * "gsrDatGoalBean is required  to retrieve  goals of reportees of Domain Manager"
	 * ) @RequestBody @Valid DatEmpProfDetailandGsrEmpGoalBean gsrDatGoalBean,
	 * BindingResult binding) throws BusinessException
	 * 
	 * {
	 * 
	 * LOG.startUsecase("View_all_goals_by_dm???????");
	 * 
	 * List<GsrDatEmpGoalBO> dmReporteegoals = null; try {
	 * 
	 * System.out .println("VIEW_EMPLOYEE_GOALS_DOMAIN_BYDM _Controller _TRY ");
	 * 
	 * if (binding.hasErrors()) {
	 * 
	 * return ValidationUtils.handleValidationErrors(binding); }
	 * 
	 * dmReporteegoals = gsrGoalService
	 * .viewEmployeeGoalswithinDomainByDm(gsrDatGoalBean);
	 * 
	 * } catch (Exception e) { e.printStackTrace(); throw new
	 * BusinessException("exception ", e); }
	 * LOG.endUsecase("View_all_goals_by_dm"); if (dmReporteegoals.size() > 0) {
	 * LOG.endUsecase("exiting View_all_goals_by_bu_DM"); return
	 * ResponseEntity.status(HttpStatus.OK.value()).body( new
	 * MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
	 * "Goals List OF DM Reportees", dmReporteegoals));
	 * 
	 * } else {
	 * 
	 * LOG.endUsecase("exiting View_all_goals_by_bu_DM"); return
	 * ResponseEntity.status(HttpStatus.OK.value()).body( new
	 * MISResponse(HttpStatus.OK.value(), MISConstants.FAILURE,
	 * "No reportees for DM", dmReporteegoals));
	 * 
	 * }
	 * 
	 * }
	 *//**
	 * @Description VIEW_EMPLOYEE_GOALS_DOMAIN_BY BU HEAD
	 * @input DatEmpProfDetailandGsrEmpGoalBean
	 * @output GsrDatEmpGoalBO
	 * @param gsrDatGoalBean
	 * @return Goals of repotees under Domain Manager
	 * @throws BusinessException
	 * @Description View all goals of doamin by Domain Manager
	 */
	/*
	 * 
	 * @ApiOperation(tags="GSR",value = "View goals by BU Head" +
	 * "This service will be called from the front-end when the BU Head wants to view  goals of his employees under his BU ."
	 * , httpMethod = "POST", notes =
	 * "This Service has been implemented to view past goals",
	 * 
	 * nickname = "VIEW_EMPLOYEE_GOALS_DOMAIN_BYDM", protocols = "HTTP",
	 * responseReference = "application/json", produces = "application/json",
	 * response = GsrDatEmpGoalBO.class)
	 * 
	 * @ApiResponses(value = {
	 * 
	 * @ApiResponse(code = 200, message = "Request Executed Successfully",
	 * response = GsrDatEmpGoalBO.class),
	 * 
	 * @ApiResponse(code = 201, message = "Created Successfully",
	 * responseContainer = "Http Response", responseHeaders =
	 * 
	 * @ResponseHeader(name = "Location", description =
	 * "The created Program Details resource", response = URI.class)),
	 * 
	 * @ApiResponse(code = 404, message = "Resource NOT FOUND",
	 * responseContainer = "Http Response"),
	 * 
	 * @ApiResponse(code = 500, message = "Internal Server Error",
	 * responseContainer = "Http Response") })
	 * 
	 * @RequestMapping(value = GsrURIConstants.VIEW_EMPLOYEE_GOALS_BYBUHEAD,
	 * method = RequestMethod.POST) public @ResponseBody
	 * ResponseEntity<MISResponse> viewEmployeeGoalsByBUHead(
	 * 
	 * @ApiParam(name = "gsrMasProfBUBean", value =
	 * "gsrMasProfBUBean is used  to retrieve  goals of reportees of BU Head")
	 * 
	 * @RequestBody @Valid GsrDatEmpGoalandProfessionalBOBean gsrMasProfBUBean,
	 * BindingResult binding) throws BusinessException
	 * 
	 * { LOG.startUsecase("View_all_goals_by_bu_head");
	 * 
	 * List<GsrDatEmpGoalBO> buHeadreporteegoals = new
	 * ArrayList<GsrDatEmpGoalBO>(); try {
	 * 
	 * if (binding.hasErrors()) {
	 * 
	 * return ValidationUtils.handleValidationErrors(binding); }
	 * buHeadreporteegoals = gsrGoalService
	 * .viewEmployeeGoalsByBUHead(gsrMasProfBUBean); }
	 * 
	 * catch (Exception e) {
	 * 
	 * throw new BusinessException("exception ", e); }
	 * LOG.endUsecase("View_all_goals_by_bu_head");
	 * 
	 * if (buHeadreporteegoals.size() > 0) {
	 * LOG.endUsecase("exiting View_all_goals_by_bu_head"); return
	 * ResponseEntity.status(HttpStatus.OK.value()).body( new
	 * MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Goals List",
	 * buHeadreporteegoals));
	 * 
	 * }
	 * 
	 * else { LOG.endUsecase("exiting View_all_goals_by_bu_head"); return
	 * ResponseEntity.status(HttpStatus.OK.value()).body( new
	 * MISResponse(HttpStatus.OK.value(), MISConstants.FAILURE,
	 * "BU Head has no repotees goals", buHeadreporteegoals));
	 * 
	 * } // return new
	 * ResponseEntity<Object>(buHeadreporteegoals,HttpStatus.OK);
	 * 
	 * }
	 */
	/**
	 * @input DatEmpProfDetailandGsrEmpGoalBean
	 * @output GsrDatEmpGoalBO
	 * @param gsrDatGoalBean
	 * @return Goals of reportee's under Domain Manager
	 * @throws BusinessException
	 * @Description View all goals of doamin by Domain Manager
	 */
	@ApiOperation(tags = "GSR", value = "View Reportee goals by Domain Manager"
			+ "This service will be called from the front-end when the Domain Manger  want to view his reportees goals .", httpMethod = "POST", notes = "This service has been implemented for Domain Manager to view his/her reportees goals details "
			+ "<br>The ouput will be goal details of reportees under domain manager given as input"
			+ "<br><b>Tables</b>"
			+ "<br>gsr_dat_emp_goal,gsr_slab,dat_emp_personal_detail,gsr_mas_goal_status<br>"
			+ "<b><b>Inputs : </b> <br> Domain Manager Id and Slab Id <br> <br>", nickname = "VIEW_REPORTEEGOALS_BYDM", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = DMViewEmpGoalsOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Retrieved Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.VIEW_GOALS_BY_DOMAINMGR, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewGoalsByDM(
			@ApiParam(name = "gsrDatGoalBean", value = "[" + "<br> {"
					+ "<br>domainMgrId:1," + " <br> }," + "<br> {"
					+ "<br>slabID:14," + "<br> }" + "<br> ]") @RequestBody @Valid DMEmpGoalInputBean dmReporteeGoalBean,
			BindingResult binding) throws BusinessException,
			CommonCustomException {

		List<DMViewEmpGoalsOutputBean> dmEmpgoalList = new ArrayList<DMViewEmpGoalsOutputBean>();

		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			}

			dmEmpgoalList = gsrGoalService
					.viewEmployeeGoalsByDM(dmReporteeGoalBean);
		} catch (Exception e) {

			throw new BusinessException(
					"Exception occured while retriveing reportees goal", e);

		}

		if (!dmEmpgoalList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Goals List of DM Reportees",
							dmEmpgoalList));
		} else {

			LOG.endUsecase("exiting View_all_goals_by_bu_DM");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "No reportees for DM",
							dmEmpgoalList));
		}

	}

	// Added by Prathibha

	@ApiOperation(tags = "GSR", value = "View goals of reportees by BU Head"
			+ "This service will be called from the front-end when the BU Head wants to view  goals of reportees under his/her BU .", httpMethod = "POST", notes = "This service is implemented to view goals of reportees under BU Head given as input"
			+ "<br>The ouput will be goal details of reportees under domain manager given as input"
			+ "<br><b>Tables</b>"
			+ "<br>gsr_dat_emp_goal,gsr_slab,dat_emp_personal_detail,gsr_mas_goal_status,mas_bu_unit<br>"
			+ "<b><b>Inputs : </b> <br> BU Head Id and Slab Id <br> <br>", nickname = "VIEW_EMPLOYEE_GOALS_BYBUHead_NEW", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = BUHeadreporteesgoaloutputbean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDatEmpGoalBO.class),
			@ApiResponse(code = 201, message = "Retrieved Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.VIEW_EMP_GOALS_BYBUHD, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewEmpGoalsByBUHead(
			@ApiParam(name = "BUHeadreporteesgoalinputbean", value = "["
					+ "<br> {" + "<br>buHeadEmpid:3," + " <br> }," + "<br> {"
					+ "<br>slabId:14," + "<br> }" + "<br> ]") @RequestBody @Valid BUHeadreporteesgoalinputbean buHeadreporteesgoalinputbean,
			BindingResult binding) throws BusinessException

	{
		LOG.startUsecase("View_all_goals_by_bu_head");

		// List<GsrDatEmpGoalBO> buHeadreporteeGoalsList = new
		// ArrayList<GsrDatEmpGoalBO>();
		List<BUHeadreporteesgoaloutputbean> buHeadreporteeGoalsList = new ArrayList<BUHeadreporteesgoaloutputbean>();
		try {

			if (binding.hasErrors()) {

				return ValidationUtils.handleValidationErrors(binding);
			} else {
				buHeadreporteeGoalsList = gsrGoalService
						.viewRepoGoalsByBUHead(buHeadreporteesgoalinputbean);
			}
		} catch (Exception e) {

			throw new BusinessException("exception ", e);
		}
		LOG.endUsecase("View_all_goals_by_bu_head");

		if (buHeadreporteeGoalsList.size() > 0) {
			LOG.endUsecase("exiting View_all_goals_by_bu_head");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Goals List",
							buHeadreporteeGoalsList));
		}

		else {
			LOG.endUsecase("exiting View_all_goals_by_bu_head");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"BU Head has no repotees goals",
							buHeadreporteeGoalsList));

		}

	} // EOA by Prathibha

	// Added by Kamal Anand
	/**
	 * 
	 * <Description viewEmployeeGoalsPagenation:> This method is used to view
	 * the employee goals
	 * 
	 * @param gsrEmpGoalDetailsBean
	 * @param binding
	 * @return MISResponse
	 * @throws BusinessException
	 * @throws MethodArgumentNotValidException
	 */
	@ApiOperation(tags = "GSR", value = " View Employee Goals"
			+ "This service will be called from the front-end when the user wants to view the employee goals", httpMethod = "POST", notes = "This Service has been implemented to view the employee goals."
			+ "<br>The Details will be stored in database.",

	nickname = "GSR View Employee Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Employee goals resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.VIEW_EMPLOYEE_GOALS_PAGENATION, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewEmployeeGoalsPagenation(
			@ApiParam(name = "GsrEmployeeGoalDetailsBeanPagenation", example = "GsrEmployeeGoalDetailsBeanPagenation", value = "Employee Goal Details Bean with all tha data to view the Employee Goals.") @Valid @RequestBody GsrEmployeeGoalDetailsBeanPagenation gsrEmpGoalDetailsBean,
			BindingResult binding) throws BusinessException,
			MethodArgumentNotValidException {
		LOG.startUsecase("View Employee Goals");
		Page<GsrDatEmpGoalBO> gsrEmpGoalList;
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				gsrEmpGoalList = gsrGoalService
						.viewEmployeeGoalsPage(gsrEmpGoalDetailsBean);
			}
			if (gsrEmpGoalList.getContent().size() == 0) {
				LOG.endUsecase("View Employee Goals");
				return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.SUCCESS,
								"No Employee  Goals to display"));
			} else {
				LOG.endUsecase("View Employee Goals");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS, "Employee Goals",
								gsrEmpGoalList));
			}
		} catch (Exception e) {
			throw new BusinessException("exception ", e);
		}
	}

	/**
	 * 
	 * <Description getAllGoalStatus:> This method is used to get the default
	 * goals of all employees.
	 * 
	 * @return MISResponse
	 * @throws BusinessException
	 */
	@ApiOperation(tags = "GSR", value = " GSR Get All Goal Status"
			+ "This service will be called from the front-end when the user wants to get all goal status", httpMethod = "GET", notes = "This Service has been implemented to get all goal status."
			+ "<br>The Details will be stored in database.",

	nickname = "GSR Get Default Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of all goal status resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_ALL_GOAL_STATUS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAllGoalStatus()
			throws BusinessException {
		LOG.startUsecase("Get All Goal Status");
		List<GsrMasGoalStatusBO> gsrGoalStatusList = null;
		try {
			gsrGoalStatusList = gsrGoalService.getAllGoalStatus();

			if (gsrGoalStatusList.size() == 0) {

				LOG.endUsecase("Get All Goal Status");
				return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.SUCCESS, "No Goals to display",
								gsrGoalStatusList));
			} else {
				LOG.endUsecase("Get All Goal Status");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS, "GSR goal status list",
								gsrGoalStatusList));
			}

		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new BusinessException("exception ", e);
		}

	}

	/**
	 * 
	 * <Description viewReporteesGoalsByReportingManager:> This method is used
	 * to view the reportees goals.
	 * 
	 * @param mgrId
	 * @return MISResponse
	 * @throws BusinessException
	 */
	@ApiOperation(tags = "GSR", value = " GSR View Reportees Goals"
			+ "This service will be called from the front-end when the user wants to view Reportees goals", httpMethod = "GET", notes = "This Service has been implemented to view Reportees goals."
			+ "<br>The Details will be retrieved in database."
			+ "<br><b>Inputs :</b><br> GSR View Reportees Goals <br><br>",

	nickname = "GSR View Reportees Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Reportee goals resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.VIEW_ALL_REPORTEES_GOALS_REPORTING_MANAGER, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> viewReporteesGoalsByReportingManager(
			@PathVariable("mgrId") int mgrId) throws BusinessException {
		LOG.startUsecase("viewReporteesGoalsByReportingManager");
		List<RmViewEmployeeGoalsOutputBean> gsrEmpGoalList = null;
		try {
			gsrEmpGoalList = gsrGoalService
					.viewReporteesGoalsByReportingManager(mgrId);
			if (gsrEmpGoalList.size() > 0) {
				LOG.endUsecase("viewReporteesGoalsByReportingManager");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS, "Reportee Goals List.",
								gsrEmpGoalList));
			} else {
				LOG.endUsecase("viewReporteesGoalsByReportingManager");
				return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.SUCCESS, "No Goals to display.",
								gsrEmpGoalList));
			}
		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new BusinessException("exception ", e);
		}
	}

	/**
	 * 
	 * @param ViewEmployeeGoalsInputBean
	 * @return MISResponse
	 * @throws BusinessException
	 * @Description : This method is used to view reportees goals based on goal
	 *              status,slabid,list of reportees
	 */
	@ApiOperation(tags = "GSR", value = " GSR View Reportees Goals"
			+ "This service will be called from the front-end when the user wants to view Reportees goals", httpMethod = "POST", notes = "This Service has been implemented to view Reportees goals."
			+ "<br>The Details will be retrieved in database.",

	nickname = "GSR View Reportees Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Reportee goals resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.VIEW_ALL_REPORTEES_GOALS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewReporteesGoalsByRM(
			@ApiParam(name = "ViewEmployeeGoalsInputBean", example = "ViewEmployeeGoalsInputBean", value = "Employee Goals Input Bean with all tha data to view the Employee Goals.") @Valid @RequestBody ViewEmployeeGoalsInputBean goalBean,
			BindingResult binding) throws BusinessException {
		LOG.startUsecase("viewReporteesGoalsByRM");
		List<RmViewEmployeeGoalsOutputBean> gsrEmpGoalList = null;
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				gsrEmpGoalList = gsrGoalService
						.viewReporteesGoalsByRM(goalBean);
			}
			if (gsrEmpGoalList.size() > 0) {
				LOG.endUsecase("viewReporteesGoalsByRM");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS, "Reportee Goals List.",
								gsrEmpGoalList));
			} else {
				LOG.endUsecase("viewReporteesGoalsByRM");
				return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.SUCCESS, "No Goals to display.",
								gsrEmpGoalList));
			}
		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new BusinessException(e.getMessage());
		}
	}

	/**
	 * 
	 * <Description viewEmployeeGoalsByHR:> This method is used to view All
	 * Employee Goals based on Search Criteria
	 * 
	 * @param goalBean
	 * @param binding
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws BusinessException
	 */
	@ApiOperation(tags = "GSR", value = " GSR View All Employee Goals"
			+ "This service will be called from the front-end when the user wants to view All Employee goals", httpMethod = "POST", notes = "This Service has been implemented to view All Employee goals."
			+ "<br>The Details will be retrieved in database.",

	nickname = "GSR View All Employee Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Reportee goals resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.VIEW_ALL_EMPLOYEE_GOALS_HR, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> viewEmployeeGoalsByHR(
			@ApiParam(name = "HrViewEmpGoalsInputBean", example = "HrViewEmpGoalsInputBean", value = "Employee Goal Input Bean with all tha data to view the Employee Goals.") @Valid @RequestBody HrViewEmpGoalsInputBean goalBean,
			BindingResult binding) throws CommonCustomException,
			BusinessException {
		LOG.startUsecase("viewReporteesGoalsByRM");
		List<HrViewEmployeeGoalsOutputBean> gsrEmpGoalList = null;
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				gsrEmpGoalList = gsrGoalService.viewEmployeeGoalsByHR(goalBean);
			}
			if (gsrEmpGoalList.size() > 0) {
				LOG.endUsecase("viewReporteesGoalsByRM");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS, "Employee Goals List.",
								gsrEmpGoalList));
			} else {
				LOG.endUsecase("viewReporteesGoalsByRM");
				return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.SUCCESS, "No Goals to display.",
								gsrEmpGoalList));
			}
		} catch (CommonCustomException e) {
			LOG.error("Exception occured", e);
			throw new CommonCustomException(e.getMessage());
		} catch (DataAccessException e) {
			LOG.error("Exception occured", e);
			throw new BusinessException(e.getMessage());
		}
	}

	/**
	 * 
	 * @param empId
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @Description : This service is used to get current year and previous 3
	 *              year's employee slab and final prenormalized rating
	 */
	@ApiOperation(tags = "GSR", value = "View Past Year Goal's Final Rating"
			+ "This service will be called from the front-end when the user wants to view Past Year Goal's Final Rating", httpMethod = "GET", notes = "This Service has been implemented to view Employee's previous year goal final rating."
			+ "<br>The Details will be retrieved in database."
			+ "<br><b>Inputs :</b><br> GSR View Employee past Goals <br><br>",

	nickname = "GSR View Employee past Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Employee Previous Final Rating resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_PREVIOUS_SLAB_PRENORMALIZED_RATING, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getPreviousSlabAndPreNormalizedRatingForEmployee(
			@PathVariable Integer empId) throws CommonCustomException {
		LOG.startUsecase("Entering getPreviousSlabAndPreNormalizedRatingForEmployee");
		List<PreNormalizedRatingBean> empGoalsList = null;
		try {
			empGoalsList = gsrGoalService
					.getPreviousSlabAndPreNormalizedRatingForEmployee(empId);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (empGoalsList.size() > 0) {
			LOG.endUsecase("Exiting getPreviousSlabAndPreNormalizedRatingForEmployee");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Employee PreNormalized Rating.", empGoalsList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS,
							"No Employee PreNormalized Rating Details.",
							empGoalsList));
		}
	}

	/**
	 * 
	 * @return MISResponse
	 * @throws BusinessException
	 * @Description : This service has been implemented to get current and
	 *              previous slab details
	 */
	@ApiOperation(tags = "GSR", value = "View Current And Previous slab details"
			+ "This service will be called from the front-end when the user wants to View Current And Previous slab details", httpMethod = "GET", notes = "This Service has been implemented to View Current And Previous slab details."
			+ "<br>The Details will be retrieved in database.",

	nickname = "View Current And Previous slab details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Slab Details Resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_CURRENT_AND_PREVIOUS_SLAB_DETAILS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getCurrentAndPreviousSlabs()
			throws BusinessException {
		LOG.startUsecase("Entering getCurrentAndPreviousSlabs");
		List<GsrSlabBO> slabListList = null;
		try {
			slabListList = gsrGoalService.getCurrentAndPreviousSlabs();
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		if (slabListList.size() > 0) {
			LOG.endUsecase("Exiting getCurrentAndPreviousSlabs");
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Slab Details.", slabListList));
		} else {
			LOG.endUsecase("Exiting getCurrentAndPreviousSlabs");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS, "No Slab Details Found.",
							slabListList));
		}
	}

	/**
	 * 
	 * @param teamGoalsBean
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This service is used to get Team goals by Reporting
	 *              Manager
	 */

	@ApiOperation(tags = "GSR", value = " View Team Goals by Reporting Manager"
			+ "This service will be called from the front-end when the user wants to view his/her Team Goals", httpMethod = "POST", notes = "This Service has been implemented to view Team goals."
			+ "<br>The Details will be retrieved in database.",

	nickname = "GSR View Team Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Team goals resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.REPORTING_MANAGER_VIEW_TEAM_GOALS, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> viewTeamGoalsByRm(
			@ApiParam(name = "ViewTeamGoalsInputBean", example = "ViewTeamGoalsInputBean", value = "Team Goal Input Bean with all tha data to view the Team Goals.") @Valid @RequestBody ViewTeamGoalsInputBean teamGoalsBean,
			BindingResult binding) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("Entering getCurrentAndPreviousSlabs");
		ViewTeamGoalsOutputBean empGoals;
		List<ViewTeamGoalsOutputBean> teamGoalList = new ArrayList<ViewTeamGoalsOutputBean>();
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				empGoals = gsrGoalService.viewTeamGoalsByRm(teamGoalsBean);
				teamGoalList.add(empGoals);
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (empGoals != null) {
			LOG.endUsecase("Exiting getCurrentAndPreviousSlabs");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Team Goals List.",
							teamGoalList));
		} else {
			LOG.endUsecase("Exiting getCurrentAndPreviousSlabs");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS, "No record found.",
							teamGoalList));
		}
	}

	/**
	 * 
	 * @param teamGoalsBean
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This service is used to get Team Goals by HR.
	 */

	@ApiOperation(tags = "GSR", value = " View Team Goals by HR."
			+ "This service will be called from the front-end when the user wants to view his/her Team Goals", httpMethod = "POST", notes = "This Service has been implemented to view Team goals."
			+ "<br>The Details will be retrieved in database.",

	nickname = "GSR View Team Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Team goals resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.HR_VIEW_TEAM_GOALS, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> viewTeamGoalsByHr(
			@ApiParam(name = "HrViewTeamGoalsInputBean", example = "HrViewTeamGoalsInputBean", value = "Team Goal Input Bean with all tha data to view the Team Goals.") @Valid @RequestBody HrViewTeamGoalsInputBean teamGoalsBean,
			BindingResult binding) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("Entering viewTeamGoalsByHr");
		ViewTeamGoalsOutputBean empGoals;
		List<ViewTeamGoalsOutputBean> teamGoalList = new ArrayList<ViewTeamGoalsOutputBean>();
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				empGoals = gsrGoalService.viewTeamGoalsByHr(teamGoalsBean);
				teamGoalList.add(empGoals);
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (empGoals != null) {
			LOG.endUsecase("Exiting viewTeamGoalsByHr");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Team Goals List.",
							teamGoalList));
		} else {
			LOG.endUsecase("Exiting viewTeamGoalsByHr");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS, "No record found.",
							teamGoalList));
		}
	}

	/**
	 * 
	 * @param delegationGoalsBean
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This service is used to view the goals that can be
	 *              delegated and goals which are delegated to or from empId
	 */
	@ApiOperation(tags = "GSR", value = " View Delegation Goals."
			+ "This service will be called from the front-end when the user wants to View Delegated Goals", httpMethod = "POST", notes = "This Service has been implemented to view Team goals."
			+ "<br>The Details will be retrieved in database.",

	nickname = "GSR View Delegated Goals", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of Delegated goals resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_GOALS_TO_DELEGATE, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> viewDelegatedGoals(
			@ApiParam(name = "DelegationGoalsInputBean", example = "DelegationGoalsInputBean", value = "Delegation Goals Input Bean with all tha data to view the Team Goals.") @Valid @RequestBody DelegationGoalsInputBean delegationGoalsBean,
			BindingResult binding) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("Entering viewDelegatedGoals");
		ViewTeamGoalsOutputBean empGoals;
		DelegationGoalsBean goalBean;
		List<DelegationGoalsBean> delegatedGoalList = new ArrayList<DelegationGoalsBean>();
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				goalBean = gsrGoalService
						.viewDelegatedGoals(delegationGoalsBean);
				delegatedGoalList.add(goalBean);
			}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (goalBean.getReportees().size() > 0) {
			LOG.endUsecase("Exiting viewDelegatedGoals");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Team Goals List.",
							delegatedGoalList));
		} else {
			LOG.endUsecase("Exiting viewTeamGoalsByHr");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS, "No record found.",
							delegatedGoalList));
		}
	}
	// End of Addition by Kamal Anand

	// Added by Shyam for create goal service

	/**
	 * 
	 * @param GsrCreateGoalInputBean
	 * @return List<GsrGoalOutputBean>
	 * @throws BusinessException
	 *             if is there present any Business exception in the service.
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description: This service is used to create goals
	 */
	@ApiOperation(tags = "GSR", value = "CREATE_A_NEW_GOAL_FOR_A_SLAB."
			+ "This service will be called from the front-end when a user(employee/manager) wants to create a new goals.", httpMethod = "POST", notes = "This Service has been implemented to create a new goal.<br> This service can be consumed by both employee(to create their new goal) and "
			+ " manager( to create a new goal for his/her reportee)."
			+ "<br> <b>Table Details :</b>"
			+ "<br>  1) gsr_dat_emp_goal. <br><br>"
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Total weightage of goals(under each manager) should not exceed 100."
			+ "<br> 2) User have to use Kpi goals to create new goal, which have been mapped to their employee level"
			+ "<br> 3) User can't reduce the KPI goal weightage, while creating a new KPI goal."
			+ "<br> 4) When a new goal created by User then goal status should be WAITING FOR MANAGER APPROVAL."
			+ "<br>    When a new goal craeted by Manager then goal status should be GOAL SET", nickname = "CREATE A NEW GSR GOAL", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrGoalOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrGoalOutputBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Create goals resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.CREATE_A_NEW_GOAL_FOR_A_SLAB, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> createGsrGoal(
			@RequestBody @Valid @ApiParam(name = "GsrCreateGoalInputBean", example = "1", value = "{"
					+ "<br>	\"employeeId\" : 2188, "
					+ "<br>	\"managerId\" : 1340, "
					+ "<br> \"goalName\" : \"GOAL123\" , "
					+ "<br> \"goalStartDate\" : \"2017-01-01\", "
					+ "<br>	\"goalEndDate\" : \"2017-03-31\", "
					+ "<br>	\"slabId\" : 16, "
					+ "<br>	\"goalWeightage\" : 20, "
					+ "<br>	\"goalDescription\" : \"GOAL DESCRIPTION 123\", "
					+ "<br>	\"goalMeasurement\" : \"GOAL MEASUREMENT 123\", "
					+ "<br>	\"goalCreatedBy\" : 2188, "
					+ "<br>	\"gsrKpiWeightageId\" : 0 " + "}") GsrCreateGoalInputBean gsrCreateGoalInputBean)
			throws 	CommonCustomException 
	{
		String goalType = "";
		String goalName = "";
		GsrGoalOutputBean gsrGoalOutputBean = new GsrGoalOutputBean();
		List<GsrGoalOutputBean> gsrGoalOutputBeanList = new ArrayList<GsrGoalOutputBean>();

		gsrGoalOutputBean = gsrGoalService.storeGsrGoal(gsrCreateGoalInputBean);
		
		if(gsrGoalOutputBean.getEmpGoalKpiWeightageId() > 0)
			goalType = "Org-wide";
		else
			goalType = "Project";
			
		goalName = gsrGoalOutputBean.getGoalName();

		if (gsrGoalOutputBean != null) {
			gsrGoalOutputBeanList.add(gsrGoalOutputBean);
		}

		if (!gsrGoalOutputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Your " + goalType + " goal - " + goalName + "  has been successfully created.",
							gsrGoalOutputBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE, "Due to some technical issue, your goal could not be saved."
									+ " Please contact the Nucleus team for support.",
							gsrGoalOutputBeanList));
		}

	}

	// End of addition by Shyam for create goal service

	// Added by Shyam for update goal service
	/**
	 * @param GsrUpdateGoalInputBean
	 * @return List<GsrGoalOutputBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description: This service is used to update goal.
	 */
	@ApiOperation(tags = "GSR", value = "GSR_UPDATE_GOAL."
			+ "This service will be called from the front-end when a user(employee/manager) wants to update a goal.", httpMethod = "PUT", notes = "This Service has been implemented to update a goal.<br> This service can be used under both employee(to update their own goal) and "
			+ " manager( to update a goal for his/her reportee)."
			+ "<br> <b>Table Details :</b>"
			+ "<br>  1) gsr_dat_emp_goal. <br><br>"
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Total weightage of goals(under each manager) should not exceed 100."
			+ "<br> 2) User can't reduce the KPI goal weightage, while updating existing KPI goal."
			+ "<br> 3) When a goal updated by User then goal status should be WAITING FOR MANAGER APPROVAL."
			+ "<br>    When a goal updated by Manager then goal status should be either WAITING FOR MANAGER APPROVAL or GOAL SET"
			+ "<br> 4) In GsrUpdateGoalInputBean object, loggedInEmployee is a property of which is used to ascertain whether it is modified by manager or employee.", nickname = "CREATE A NEW GSR GOAL", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrGoalOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrGoalOutputBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Update goal resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GSR_UPDATE_GOAL, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> updateGsrGoal(
			@Valid @RequestBody @ApiParam(name = "GsrUpdateGoalInputBean", example = "1", value = "{"
					+ "<br>	\"goalId\" : 68, "
					+ "<br>	\"goalWeightage\" : 20, "
					+ "<br> \"goalDescription\" : \"GOAL DESC TEST\" , "
					+ "<br> \"goalMeasurement\" : \"GOAL MEASUREMENT TEST\", "
					+ "<br>	\"loggedInEmployee\" : \"2188\", " + "}") GsrUpdateGoalInputBean gsrUpdateGoalInputBean)
			throws CommonCustomException, DataAccessException 
	{
		GsrGoalOutputBean gsrGoalOutputBean = new GsrGoalOutputBean();
		List<GsrGoalOutputBean> updateGoalList = new ArrayList<GsrGoalOutputBean>();

		gsrGoalOutputBean = gsrGoalService.updateGsrGoal(gsrUpdateGoalInputBean);

		if (gsrGoalOutputBean != null) {
			updateGoalList.add(gsrGoalOutputBean);
		}

		if (!updateGoalList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Your goal has been updated successfully.",
							updateGoalList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE, "Failed to update the goal",
							updateGoalList));
		}
	}

	// End of addition by Shyam for update goal service
	
	
	//Added by Shyam for creation of a goal for multiple reportees at a time.
	/**
	 * @param List<GsrCreateGoalInputBean> gsrCreateGoalInputBeanList
	 * @return List<GsrGoalOutputForMultipleReporteesBean> gsrGoalOutputBeanList
	 * @throws CommonCustomException
	 * @throws DataAccessException 
	 * @Description: This service is used to create nultiple reportee's goal at a time.
	 */
	@ApiOperation(tags = "GSR", value = "CREATE_A_NEW_GOAL_BY_MANAGER_FOR_MULTIPLE_EMPLOYEE_AT_A_TIME."
			+ "This service will be called from the front-end when a user(manager) wants to create multiplt reportee's goala at single shot.", httpMethod = "POST", 
				notes = "This Service has been implemented to create multiple reportee's goal at single shot.<br> This service can be used under both employee(to update their own goal) and "
			+ " manager( to update a goal for his/her reportee)."
			+ "<br> <b>Table Details :</b>"
			+ "<br>  1) gsr_dat_emp_goal. <br><br>"
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Total weightage of goals(under each manager) should not exceed 100."
			+ "<br> 2) If Kpi goal consideration flag is active for the slab, then manager can create Kpi goals, "
			+ "which are mapped to their reportee's level and bu id."
			+ "<br> 3) Manager can't reduce the  reportee's KPI goal weightage, while creating a new KPI goal."
			+ "<br> 4) When a new goal created by manager then goal status should be GOAL SET."
			, nickname = "CREATE A NEW GSR GOAL BY MANAGER FOR MULTIPLE REPORTEES_AT_A_TIME", protocols = "HTTP", 
			responseReference = "application/json", produces = "application/json", response = GsrGoalOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrGoalOutputBean.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", 
				responseHeaders = @ResponseHeader(name = "Location", description = "Create goals for multiple reportees resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = GsrURIConstants.CREATE_A_NEW_GOAL_BY_MANAGER_FOR_MULTIPLE_EMPLOYEE_AT_A_TIME, 
			method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> createGsrGoalByManagerForMultipleReportees(

			@RequestBody @Valid @ApiParam(name = "List<GsrCreateGoalInputBean>", example = "1", value = "{"
					+ "<br>	\"employeeId\" : 2188, "
					+ "<br>	\"managerId\" : 1340, "
					+ "<br> \"goalName\" : \"GOAL123\" , "
					+ "<br> \"goalStartDate\" : \"2017-01-01\", "
					+ "<br>	\"goalEndDate\" : \"2017-03-31\", "
					+ "<br>	\"slabId\" : 16, "
					+ "<br>	\"goalWeightage\" : 20, "
					+ "<br>	\"goalDescription\" : \"GOAL DESCRIPTION 123\", "
					+ "<br>	\"goalMeasurement\" : \"GOAL MEASUREMENT 123\", "
					+ "<br>	\"goalCreatedBy\" : 1340, "
					+ "<br>	\"gsrKpiWeightageId\" : 0 " + "},{"
					
					+ "<br>	\"employeeId\" : 3071, "
					+ "<br>	\"managerId\" : 1340, "
					+ "<br> \"goalName\" : \"GOAL123\" , "
					+ "<br> \"goalStartDate\" : \"2017-01-01\", "
					+ "<br>	\"goalEndDate\" : \"2017-03-31\", "
					+ "<br>	\"slabId\" : 16, "
					+ "<br>	\"goalWeightage\" : 15, "
					+ "<br>	\"goalDescription\" : \"GOAL DESCRIPTION 123\", "
					+ "<br>	\"goalMeasurement\" : \"GOAL MEASUREMENT 123\", "
					+ "<br>	\"goalCreatedBy\" : 1340, "
					+ "<br>	\"gsrKpiWeightageId\" : 0 " + "}") 
			List<GsrCreateGoalInputBean> gsrCreateGoalInputBeanList) throws CommonCustomException, DataAccessException
	{
		List<GsrGoalOutputForMultipleReporteesBean> gsrGoalOutputBeanList = new ArrayList<GsrGoalOutputForMultipleReporteesBean>();

		gsrGoalOutputBeanList = gsrGoalService.storeGsrGoalForMultipleReportees(gsrCreateGoalInputBeanList);

		if (!gsrGoalOutputBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
							"Create goal service has been processed completely",gsrGoalOutputBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.FAILURE, 
							"Failed to complete the process of create goal service",gsrGoalOutputBeanList));
		}
	}
	//End of addition by Shyam for creation of a goal for multiple reortees at a time.
	
	//Added by Mani for HR dashboard screen changes
	@ApiOperation(tags = "GSR", value = " To View The Employee Goal Set Status For current year under domain manager id"
			+ "", httpMethod = " GET ", notes = " This Service Has Been Implemented To View The Employee Goal Set Status Details by Passing Employee Id and Slab Id."
			+ "<br> <b> Table : </b> </br>"
			+ "1)<b>gsr_dat_emp_goal</b> -> It contains all the created goals and status for domain manager<br/>"
			+ "2)<b>gsr_dat_emp_final_rating</b> ->Checking for final rating status whether goal is closed or not."
			+ "<br>"
			+ "<b>Rules:</b></br>"
			+ "1)Weightage is 100 then goal status is 'goal set' or else 'goal not set'.</br>"
			+ "2)If employee got the final rating and has accepted and force closed from HR then goal status will be 'Goal closed' or else goal status will be not 'goal not closed'</br></br>", nickname = " GSR Goal Set For Employee Id with Slab wise ", protocols = " HTTP ", responseReference = " application/json ", produces = " application/json", response = GsrGoalSetStatusBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Goal Set Status Retrieveed Successfully.", response = GsrGoalSetStatusBean.class),
			@ApiResponse(code = 404, message = "Employee Id or Slab Id  Not Found In Table.", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GOALS_STATUS_HR, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getGoalStatusForDomain(
			) throws BusinessException,
			DataAccessException, CommonCustomException {
		List<GsrTeamGoalStatusBean> gsrTeamGoalStatusBeanOutList = new ArrayList<GsrTeamGoalStatusBean>();
				gsrTeamGoalStatusBeanOutList = gsrGoalService
						.getGoalStatusForAllActiveBu();
				if (gsrTeamGoalStatusBeanOutList.size() > 0) {
					LOG.endUsecase("Exited Goal Set Status For an Employee in `GSRGoalController`");
					return ResponseEntity.status(HttpStatus.OK.value()).body(
							new MISResponse(HttpStatus.OK.value(),
									MISConstants.SUCCESS,
									"Goal Set Status For All Employees ",
									gsrTeamGoalStatusBeanOutList));
				} else {
					return ResponseEntity
							.status(HttpStatus.OK.value())
							.body(new MISResponse(
									HttpStatus.NO_CONTENT.value(),
									MISConstants.FAILURE,
									"Resource Data Is Not Available"));
				}
			 
	}
	
	//End of addition by Mani
	
	
}
