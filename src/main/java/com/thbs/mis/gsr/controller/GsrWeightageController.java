package com.thbs.mis.gsr.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;






import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bean.GsrKpiWeightageBean;
import com.thbs.mis.gsr.bo.GsrAdminConfigurationBO;
import com.thbs.mis.gsr.bo.GsrKpiWeightageBO;
import com.thbs.mis.gsr.constants.GsrURIConstants;
import com.thbs.mis.gsr.service.GsrWeightageService;



@Controller 
public class GsrWeightageController {

	
	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(GsrWeightageController.class);
	
	@Autowired
	private GsrWeightageService gsrWeightageService;
	
	/**
	 * @Description method to create GSR weightage
	 * @param gsrKpiBean
	 * @return GsrKpiWeightageBO
	 * @throws BusinessException
	 */
	
	@ApiOperation(tags="GSR",value = "Create GSR Weightage.", httpMethod = "GET", 
			notes = "This Service has been implemented to create GSR weightages "
			+ "<br><b>Inputs :</b><br> Employee ID and Slab ID <br><br>",

	nickname = "Create GSR Weightages",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrAdminConfigurationBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrAdminConfigurationBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = GsrURIConstants.CREATE_GSR_WEIGHTAGES, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> createGSRWeightage(
			@Valid @RequestBody GsrKpiWeightageBean gsrKpiBean) throws BusinessException
	{
		LOG.startUsecase("CREATE_GOAL_WEIGHTAGE");
		GsrKpiWeightageBO gsrKpiWeightageBO = new GsrKpiWeightageBO();
		try{
			gsrKpiWeightageBO = gsrWeightageService.createGsrKpiWeightage(gsrKpiBean);
			
			if (gsrKpiWeightageBO!=null)
			{
				LOG.endUsecase("CREATE_GSR_WEIGHTAGES");
				return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
						.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
								MISConstants.SUCCESS, "Weightage created successfuly"));
			}
			else
			{
				LOG.endUsecase("CREATE_GSR_WEIGHTAGES");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Weightage colud not be created"));
			}
			
		}
		catch(Exception e)
		{	
		throw new BusinessException("exception ",e);	
		}
	}
	
	@ApiOperation(tags="GSR",value = "Update weightage Weightages"
			+ "This service will be called from the front-end when the user want update goal weightages.", httpMethod = "PUT", 
			notes = "This Service has been implemented to create goal weightage",

	nickname = "UPDATE_GOAL_WEIGHTAGE", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrKpiWeightageBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrKpiWeightageBO.class),
			@ApiResponse(code = 201, message = "Updated Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	
	
	@RequestMapping(value = GsrURIConstants.UPDATE_GSR_WEIGHTAGES, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> updateGSRWeightage(
			 @Valid @RequestBody GsrKpiWeightageBean gsrKpiBean1) throws BusinessException
			{
		LOG.startUsecase("UPDATE_GOAL_WEIGHTAGE");
		GsrKpiWeightageBO updategsrKpiWeightageBO = new GsrKpiWeightageBO();
		List<GsrKpiWeightageBO> updategsrKpiWeightageBOList = new ArrayList<GsrKpiWeightageBO>();
		try{
			updategsrKpiWeightageBO = gsrWeightageService.updateGsrKpiWeightage(gsrKpiBean1);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		throw new BusinessException("exception ",e);	
		}
		
		if(updategsrKpiWeightageBO != null){
			updategsrKpiWeightageBOList.add(updategsrKpiWeightageBO);
		}
		
		LOG.endUsecase("UPDATE_GOAL_WEIGHTAGE");
		if(!updategsrKpiWeightageBOList.isEmpty()){
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,"goal weightages updated sucessfuly",updategsrKpiWeightageBOList));
		}else{
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,"goal weightage could not be updated",updategsrKpiWeightageBOList));
		}
		
		//return updategsrKpiWeightageBO;
			}
	
	
	
}
