/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  GSRFinalRatingController.java                     */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  13-Nov-2016                                       */
/*                                                                   */
/*  Description :  Contains all the controller methods related to    */
/*                 the Final rating functionalities.                 */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 13-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bean.GsrDisagreeAndResolvedFinalRatingOutputBean;
import com.thbs.mis.gsr.bean.GsrDisagreeAndResolvedFinalRatingUnderBUHeadBean;
import com.thbs.mis.gsr.bean.GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean;
import com.thbs.mis.gsr.bean.GsrDisagreeAndResolvedFinalRatingUnderDmBean;
import com.thbs.mis.gsr.bean.GsrDisagreeAndResolvedFinalRatingUnderSkipLevelMgr;
import com.thbs.mis.gsr.constants.GsrURIConstants;
import com.thbs.mis.gsr.service.GSRFinalRatingService;


@Controller
public class GSRFinalRatingController {
	
	/**`
	 * Instance of <code>Log</code>
	 */
	@SuppressWarnings("unused")
	private static final AppLog LOG = LogFactory.getLog(GSRFinalRatingController.class);

	@Autowired(required=true)
	private GSRFinalRatingService gsrFinalRatingService;
	

	//Added by Shyam Lama
	/**
	 * This service method is used for the retrieval of the DisAgree candidate of a slab for a domain manager. 
	 * 
	 * @param GsrDisagreeAndResolvedFinalRatingUnderDmBean contains the input details for the retrieval of the DisAgree employee details.
	 * @return List<GsrAgreeDisAgreeDetailsBean> contains the GsrDisAgreeDetailsBean
	 * 
	 * @throws BusinessException if is there present any Business exception in the service.
	 * @throws DataAccessException if there is present any Data Access Exception
	 * 
	 */
	
	@ApiOperation(tags="GSR",value = "Get all the employee details present under a particular domain manager who are not agree with their final ratings."
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to fetch all the reportees details present under a particular domain who are disagree with their final rating of a slab."
			+ "<br>The Response will be the list of details like emp Id,emp Name etc who are disagree with their ratings.",

	nickname = "Disagree Final Rating Details Under DM",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean.class),
			@ApiResponse(code = 201, message = "List of disagree reportees details retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Disagree Final Rating Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Disagree Final Rating Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = GsrURIConstants.DISAGREE_AND_RESOLVED_FINAL_RATING_UNDER_DM_LOGIN, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> showDisagreeAndResolvedFinalRatingListUnderDomainMgr(
			@RequestBody @Valid GsrDisagreeAndResolvedFinalRatingUnderDmBean 
				disagreeAndResolvedFinalRatingInputBean) throws CommonCustomException
	{
		List<GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean> disagreeFinalRatingForDomMgrList = 
				new ArrayList<GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean>();
		GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean outputBean = new
				GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean();
		
		try {
			outputBean = gsrFinalRatingService.getDisagreeAndResolvedRatingOfASlabForDomainMgr(
					disagreeAndResolvedFinalRatingInputBean);	
		} catch (Exception e) {
			
			throw new CommonCustomException("Failed while fetching disagree and resolved final rating "
					+ "detials under DM. " + e.getMessage());
		}
		
		disagreeFinalRatingForDomMgrList.add(outputBean);
		
		if(!disagreeFinalRatingForDomMgrList.isEmpty())
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
						"All disagree and resolved final rating for a slab have been retrieved successfully", 
						disagreeFinalRatingForDomMgrList));
		}else{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"Neither disagree nor resolved final rating found", disagreeFinalRatingForDomMgrList));
		}
	}
	
	
	/**
	 * This service method is used for the retrieval of the Disagree and Resolved Final rating of Reportees of a slab for a BU. 
	 * 
	 * @param GsrDisagreeAndResolvedFinalRatingUnderBUHeadBean contains the input details for the retrieval of the DisAgree and Resolved final rating of employee details.
	 * @return List<GsrAgrDisAgrGroupByManageBean> contains the list of GsrAgrDisAgrGroupByManageBean for the user.
	 * 
	 * @throws BusinessException if is there present any Business exception in the service.
	 * @throws DataAccessException if there is present any Data Access Exception
	 * 
	 */
	
	@ApiOperation(tags="GSR",value = "Get all the employee details present under a particular BU, who are not agree with their final ratings."
			+ " .", httpMethod = "POST",

	notes = "This Service has been implemented to fetch all the reportees details present under a particular BU who are disagree with their final rating of a slab."
			+ "<br>The Response will be the list of details like emp Id,emp Name,manager emp id and manager name etc who are disagree with their ratings.",

	nickname = "Disagree Final Rating Details Under BU Head",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDisagreeAndResolvedFinalRatingOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDisagreeAndResolvedFinalRatingOutputBean.class),
			@ApiResponse(code = 201, message = "List of disagree/resolved reportees details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Disagree/Resolved Final Rating Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Disagree/Resolved Final Rating Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = GsrURIConstants.DISAGREE_AND_RESOLVED_FINAL_RATING_UNDER_BU_HEAD_LOGIN, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> showDisAgreeAndResolvedFinalRatingUnderBUHead(
			@RequestBody @Valid GsrDisagreeAndResolvedFinalRatingUnderBUHeadBean disagreeOrResolvedFinalRatingUnderBUHeadBean)
			throws BusinessException, DataAccessException 
	{
		List<GsrDisagreeAndResolvedFinalRatingOutputBean> disagreeAndResolvedFinalRatingList = new ArrayList<GsrDisagreeAndResolvedFinalRatingOutputBean>();
		try {
			disagreeAndResolvedFinalRatingList = gsrFinalRatingService.getDisagreeAndResolvedRatingOfASlabForBUHead(disagreeOrResolvedFinalRatingUnderBUHeadBean);
			
		} catch (Exception e) {
			throw new BusinessException("Failed while fetching disagree and resolved final rating detials under BU Head", e);
		}

		if(!disagreeAndResolvedFinalRatingList.isEmpty())
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"All disagree and resolved final rating for a slab have been retrieved successfully", disagreeAndResolvedFinalRatingList));
		}else{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"No disagree/resolved final rating found for provided slab", disagreeAndResolvedFinalRatingList));
		}	
	}
	
	
	/**
	 * This service method is used for the retrieval of the Disagree and resolved reportee's final rating of a slab for a skip level manager. 
	 * 
	 * @param GsrDisagreeAndResolvedFinalRatingUnderSkipLevelMgr contains the input details for the retrieval of the Disagree and resolved final rating of indirect reportes details.
	 * @return List<GsrAgreeDisAgreeDetailsBean> contains the GsrDisAgreeDetailsBean
	 * 
	 * @throws CommonCustomException if is there present any exception in the service.
	 * 
	 */
	
	@ApiOperation(tags="GSR",value = "View final rating of reportees."
			+ "Get all the employee details present under a particular domain manager who are disagree "
			+ "and resolved their final rating issue with their final ratings."
			+ " .", httpMethod = "POST",
			notes = "This Service has been implemented to view list of reportee's are disagree and resolved final rating based on slab id and employee id."
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_final_rating -> It contains all final rating details with slab id, employee id and skip level manager id too."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> GsrDisagreeAndResolvedFinalRatingUnderSkipLevelMgr <br><br>"
			+ "<br> slabId : slabId in the format of integer <br>"
			+ "<br> skipLevelMgrId : skipLevelMgrId in the format integer. <br>",
	
	nickname = "Disagree And Resolved Final Rating Details Under Skip Level Mgr",   protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean.class),
			@ApiResponse(code = 201, message = "List of disagree reportees details retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Disagree And Resolved Final Rating Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Disagree And Resolved Final Rating Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	
	@RequestMapping(value = GsrURIConstants.DISAGREE_AND_RESOLVED_FINAL_RATING_UNDER_SKIP_LEVEL_MANAGER, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> showDisagreeAndResolvedFinalRatingListUnderSkipLevelMgr(
			@RequestBody @Valid @ApiParam(name = "GSR View Disagree And Resolved Final Rating Details", example = "1", value = "{"
					+ "<br> slabId: 15,"
					+ "<br> skipLevelMgrId: 1963" + "<br> " + "}") 
		GsrDisagreeAndResolvedFinalRatingUnderSkipLevelMgr disagreeAndResolvedInputBean) 
				throws CommonCustomException
	{
		List<GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean> disagreeAndResolvedForSkipLevelMgrList = 
				new ArrayList<GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean>();
		GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean outputBean = new
				GsrDisagreeAndResolvedFinalRatingUnderDMOutputBean();
		
		try {
			outputBean = gsrFinalRatingService.getDisagreeAndResolvedRatingOfASlabForSkipLevelMgr(
					disagreeAndResolvedInputBean);	
		} catch (Exception e) {
			
			throw new CommonCustomException("Failed while fetching disagree and resolved final rating "
					+ "detials under DM. " + e.getMessage());
		}
		
		disagreeAndResolvedForSkipLevelMgrList.add(outputBean);
		
		if(!disagreeAndResolvedForSkipLevelMgrList.isEmpty())
		{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
						"All disagree and resolved final rating for a slab have been retrieved successfully", 
						disagreeAndResolvedForSkipLevelMgrList));
		}else{
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, 
							"Neither disagree nor resolved final rating found", disagreeAndResolvedForSkipLevelMgrList));
		}
	}
	
	
	//End of addition by Shyam Lama

}
