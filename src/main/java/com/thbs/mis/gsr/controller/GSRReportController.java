package com.thbs.mis.gsr.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import jxl.write.WriteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.report.excel.bean.ReportTemplateConfigureBean;
import com.thbs.mis.gsr.bean.GSRReportGenerateBean;
import com.thbs.mis.gsr.bean.GsrReportFilterInputBean;
import com.thbs.mis.gsr.bean.GsrReportInitContentBean;
import com.thbs.mis.gsr.bean.GsrReportInputBean;
import com.thbs.mis.gsr.constants.GsrURIConstants;
import com.thbs.mis.gsr.service.GSRReportService;

@Controller
public class GSRReportController {

	private static final AppLog LOG = LogFactory
			.getLog(GSRReportController.class);

	@Autowired
	GSRReportService gsrReportService;

	@RequestMapping(value = GsrURIConstants.GET_CUSTOM_QUERY_DATA, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> getCustomQueryData(
			@RequestBody GsrReportInputBean inputBean)
			throws BusinessException, DataAccessException,
			CommonCustomException {
		List getGoalList = new ArrayList();
		getGoalList = gsrReportService.getCustomQueryData(inputBean);
		if (getGoalList.size() > 0) {
			LOG.endUsecase("Exited Goal Set Status For an Employee in `GSRGoalController`");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Goal Set Status For All Employees ", getGoalList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.NO_CONTENT.value(),
							MISConstants.FAILURE,
							"Resource Data Is Not Available"));
		}

	}

	@Autowired
	private ServletContext servletContext;

	@RequestMapping(value = GsrURIConstants.GET_GSR_REPORT_FILTER_CONFIG_TEMPLATE, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getExcelFilterConfigData(
			HttpServletResponse response,@PathVariable("empId") int empId) throws IOException, CommonCustomException {
		List<ReportTemplateConfigureBean> data =  gsrReportService.getExcelFilterConfigData(empId);
		if (data.size() > 0) {
			LOG.endUsecase("Exited Goal Set Status For an Employee in `GSRGoalController`");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Goal Set Status For All Employees ", data));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.NO_CONTENT.value(),
							MISConstants.FAILURE,
							"Resource Data Is Not Available"));
		}

	}

	@RequestMapping(value = GsrURIConstants.GET_GSR_REPORT_INIT_LOAD_DATA, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> getReportInitContentData(
			@RequestBody GsrReportFilterInputBean inputBean) throws CommonCustomException,
			DataAccessException, IOException {
		List<GsrReportInitContentBean> output = new ArrayList<GsrReportInitContentBean>();
		output = gsrReportService.getReportInitContentData(inputBean);
		if (output.size() > 0) {
			LOG.endUsecase("Exited Goal Set Status For an Employee in `GSRGoalController`");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "GSR Report data loaded",
							output));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.NO_CONTENT.value(),
							MISConstants.FAILURE,
							"Resource Data Is Not Available"));
		}
	}

	@RequestMapping(value = GsrURIConstants.GENERATE_GSR_REPORT, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> generateReport(
			@RequestBody GSRReportGenerateBean inputBean,
			HttpServletResponse response) throws BusinessException,
			DataAccessException, CommonCustomException, WriteException,
			IOException, IllegalArgumentException, IllegalAccessException {
		boolean result = gsrReportService.generateReport(inputBean, response);
		if (result) {
			LOG.endUsecase("Exited Goal Set Status For an Employee in `GSRGoalController`");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "success"));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.NO_CONTENT.value(),
							MISConstants.FAILURE, "Failed"));
		}

	}


}
