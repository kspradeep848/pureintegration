/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrCommonService.java                            */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the methods for the           */
/*                 GSRCommonController                               */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.gsr.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.MasTrainingTopicNameBO;
import com.thbs.mis.common.constants.CommonURIConstants;
import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bean.GsrGetAllReportingManagersBean;
import com.thbs.mis.gsr.bean.GsrSlabOutputBean;
import com.thbs.mis.gsr.bean.GsrWorflowPermissionBean;
import com.thbs.mis.gsr.bo.GsrAdminConfigurationBO;
import com.thbs.mis.gsr.bo.GsrSlabBO;
import com.thbs.mis.gsr.bo.GsrWorkflowPermissionBO;
import com.thbs.mis.gsr.constants.GsrURIConstants;
import com.thbs.mis.gsr.service.GsrCommonService;
import com.thbs.mis.gsr.service.GsrSlabService;

@Controller
public class GSRCommonController {

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(GSRCommonController.class);

	@Autowired
	private GsrCommonService gsrCommonService;

	@Autowired(required = true)
	private CommonService commonService;

	@Autowired
	private GsrSlabService gsrSlabService;

	// Added by Rajesh Kumar on 15/12/2016
	/**
	 * <Description getAdminConfig:> This contains the methods used for the
	 * GET_GSR_ADMIN_CONFIG_SETTINGS for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @see
	 */
	@ApiOperation(tags = "GSR", value = "Admin Configuration Setting."
			+ "To the Admin Configuration Setting details.", httpMethod = "GET", notes = "This Service has been implemented the Admin Configuration Setting details."
			+ "<br>The Response will be the detailed of the Admin Configuration Setting."
			+ "<br><b>Inputs :</b><br> GSR Admin Configuration Setting. <br><br>",

	nickname = "GSR Admin configuration Setting", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrAdminConfigurationBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrAdminConfigurationBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_GSR_ADMIN_CONFIG_SETTINGS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getAdminConfig(
			@PathVariable("slabId") int slabId) throws BusinessException {
		LOG.startUsecase("getAdminConfig");
		List<GsrAdminConfigurationBO> adminConfigSettings = null;
		List<GsrAdminConfigurationBO> adminConfigSettings1 = new ArrayList<GsrAdminConfigurationBO>();
		try {
			adminConfigSettings = gsrCommonService.getAdminConfig(slabId);
			adminConfigSettings1.addAll(adminConfigSettings);
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"All Admin Configuration Settings are Displayed.",
							adminConfigSettings1));
		} catch (Exception e) {
			throw new BusinessException("exception ", e);
		}
		// LOG.endUsecase("getAdminConfig");
		// return adminConfigSettings;

	}

	// Ended by Rajesh Kumar on 15/12/2016

	// Added by Smrithi on 28/10/2016
	/**
	 * 
	 * @return List<GsrWorflowPermissionBean> contains the list of Work Flow
	 *         Permission fields.
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 * @Description: This method is used to get the workflow permissions.
	 * 
	 */

	@ApiOperation(tags = "GSR", value = "Get Workflow Permission"
			+ "This service will be called from the front-end when the user want to retrive the workflow permission.", httpMethod = "GET", notes = "This Service has been implemented to retrive the workflow permission"
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_workflow_permission -> It contains all the workflow permission id and accomplishments."
			+ "<br>"
			+ "<br><b>Inputs :</b><br> GsrEmployeeGoalDetailsBean <br><br>"
			+ "<br> empId : empId in the format of integer <br>"
			+ "<br> slabId : slabId in the format of integer <br>",

	nickname = "workflow permission", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrWorkflowPermissionBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrWorkflowPermissionBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of workflow permission  resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_WORKFLOW_PERMISSION, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getWorkflowPermission(
			@ApiParam(name = "GSR Get Workflow Permission", example = "1", value = "{"
					+ "<br> empId: 1," + "<br> slabId: 1," + "<br> " + "}") @PathVariable("empId") Integer empId,
			@PathVariable("slabId") short slabId)

	throws BusinessException, DataAccessException, CommonCustomException {
		LOG.startUsecase("getWorkflowPermission");
		List<GsrWorkflowPermissionBO> gsrWorkflowPermissionList = null;
		DatEmpProfessionalDetailBO employeeInfo = new DatEmpProfessionalDetailBO();
		GsrSlabBO slabInfo = new GsrSlabBO();

		employeeInfo = commonService.getEmpoyeeDeatailsById(empId);
		if (employeeInfo == null) {
			throw new CommonCustomException("Invalid empId");
		}
		slabInfo = gsrSlabService.getSlabInfoById(slabId);
		if (slabInfo == null) {
			throw new CommonCustomException("Invalid slabId");
		}
		if (employeeInfo != null && slabInfo != null) {
			List<GsrWorflowPermissionBean> workflowBean = gsrCommonService
					.getWorkflowPermission(slabId, empId);
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Workflow Permission is displayed successfully.",
							workflowBean));

		}
		return null;

	}

	/**
	 * 
	 * @return List<DatEmpDetailBO> contains the list of Employee details
	 *         Permission BO's
	 * @throws BusinessException
	 * @Description: This method is used to get the reporting manager name and
	 *               ID using the role id.
	 * 
	 */

	@ApiOperation(tags = "GSR", value = "Get All Reporting Managers."
			+ "This service will be called from the front-end when the user want to get the reporting manager name and ID using the role id.", httpMethod = "GET", notes = "This Service has been implemented to get the reporting manager name and ID using the role id"
			+ "<br> <b> Table : </b> </br>"
			+ "1)gsr_dat_emp_goal -> It contains all the created goals and status"
			+ "<br>",

	nickname = "reporting manager", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = DatEmpDetailBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = DatEmpDetailBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of workflow permission  resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = CommonURIConstants.GET_ALL_REPORTING_MANAGERS_BY_ROLE_ID, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getAllReportingManager()
			throws BusinessException {
		LOG.startUsecase("gsrGetAllReportingManager");
		List<DatEmpDetailBO> gsrGetAllReportingManagerList = null;
		try {
			List<GsrGetAllReportingManagersBean> gsrGetAllReportingManagersList = gsrCommonService
					.getAllReportingManagers();
			System.out.println("gsrGetAllReportingManagersList"
					+ gsrGetAllReportingManagersList.size());
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Gsr Get All Reporting Managers are displayed successfully.",
							gsrGetAllReportingManagersList));
		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new BusinessException("exception ", e);
		}

	}

	// EOA by Smrithi

	// Added by Prathibha on 28/10/2016
	/**
	 * @Description method to view all GSR slabs
	 * @return List of GSR Slabs
	 * @throws BusinessException
	 * @throws DataAccessException
	 */

	@ApiOperation(tags = "GSR",value = "TO view all GSR slabs"
			+ "This service will be called from the front-end when the user want to retrive GSR slabs.", httpMethod = "GET", notes = "This Service has been implemented to retrive GSR slabs",

	nickname = "view all GSR slabs", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = GsrWorkflowPermissionBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = GsrSlabBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_GSR_SLABS, method = RequestMethod.GET)
	public @ResponseBody List<GsrSlabOutputBean>getAllgsrSlabs()
			throws BusinessException, DataAccessException {
		LOG.info("Get All GSR Slabs");
		
		List<GsrSlabOutputBean> gsrBeanList=new ArrayList<GsrSlabOutputBean>();
		try {
			gsrBeanList = gsrCommonService.getAllgsrSlabs();
			
		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new BusinessException("exception ", e);
		}
		LOG.endUsecase("Get all GSR Slabs");
		return gsrBeanList;
	}
	
	//EOC by Prathibha
	// Added by Pratibh T R on 17/11/2016
	/**
	 * 
	 * @return List<MasTrainingTopicNameBO> contains the list of
	 *         MasTrainingTopicName BO's
	 * @throws BusinessException
	 *             if is there present any Business exception in the service.
	 * @Description :This Service has been implemented to retrieve the training
	 *              topics
	 */
	@ApiOperation(tags = "GSR",value = "view training topics"
			+ "This service will be called from the front-end when the user want to retrive the training topics.", httpMethod = "GET", notes = "This Service has been implemented to retrieve the training topics"
			+ "<br> <b> Table : </b> </br>"
			+ "1)mas_training_topic_name -> It contains all the training topics details."
			+ "<br>", nickname = "Training topics", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Training topics Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_TRAINING_TOPICS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getTrainingTopics()
			throws BusinessException {
		LOG.startUsecase("getTrainingTopics");
		List<MasTrainingTopicNameBO> trainingTopicNameList = null;
		try {
			trainingTopicNameList = gsrCommonService.getTrainingTopics();
			if (trainingTopicNameList.size() > 0) {
				LOG.endUsecase("getTrainingTopics");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS, "Training topics.",
								trainingTopicNameList));
			} else {
				LOG.endUsecase("getTrainingTopics");
			}
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Training topic details are not found"));
		}

		catch (Exception e) {
			throw new BusinessException("exception ", e);
		}

	}

	/**
	 * 
	 * @param gsrSlabYear
	 * @return List<GsrSlabBO> contains the list of GsrSlab Bo's
	 * @throws BusinessException
	 *           if is there present any Business exception in the service.
	 * @Description :This Service has been implemented to retrieve the slab details 
	                    based on year.           
	 */
	
	@ApiOperation(tags = "GSR",value = "View Enabled SlabDetails Based On SlabYear"
			+ "This service will be called from the front-end when the user want to retrive the Enabled slab details based on slabYear.", httpMethod = "GET", notes = "This Service has been implemented to view enabled slab details based on slabYear", nickname = "view Slab Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Enabled Slab Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.VIEW_ENABLED_SLAB_DETAILS_BASED_ON_SLABYEAR, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> viewSlabDetailsBasedOnSlabYear(@PathVariable String gsrSlabYear)
			throws BusinessException {
		LOG.startUsecase("viewSlabDetailsBasedOnSlabYear");
		List<GsrSlabBO> gsrSlabList = new ArrayList<GsrSlabBO>();
		try {
			gsrSlabList = gsrCommonService.viewSlabDetailsBasedOnSlabYear(gsrSlabYear);
			if (gsrSlabList.size() > 0) {
				LOG.endUsecase("viewSlabDetailsBasedOnSlabYear");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS, "Slab Details.",
								gsrSlabList));
			} else {
				LOG.endUsecase("viewSlabDetailsBasedOnSlabYear");
			}
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Slab Details are not found"));
		}

		catch (Exception e) {
			throw new BusinessException("exception ", e);
		}

	}
	

	// Ended by Pratibha T R on 17/11/2016
	
	//Added by Kamal Anand
	@ApiOperation(tags = "GSR",value = "View Enabled SlabDetails Based On SlabYear"
			+ "This service will be called from the front-end when the user want to retrive the Enaled slab details based on slabYear.", httpMethod = "GET", notes = "This Service has been implemented to view enabled slab details based on slabYear", nickname = "view Slab Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Enabled Slab Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = GsrURIConstants.GET_ALL_SLABS_BASED_SLAB_YEAR, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> viewSlabDetailsOnSlabYear(@PathVariable String slabYear)
			throws BusinessException {
		LOG.startUsecase("viewSlabDetailsBasedOnSlabYear");
		List<GsrSlabBO> gsrSlabList = new ArrayList<GsrSlabBO>();
		try {
			gsrSlabList = gsrCommonService.viewSlabDetailsOnSlabYear(slabYear);
			if (gsrSlabList.size() > 0) {
				LOG.endUsecase("viewSlabDetailsBasedOnSlabYear");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS, "view Slab Details.",
								gsrSlabList));
			} else {
				LOG.endUsecase("viewSlabDetailsBasedOnSlabYear");
			}
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"view Slab Details are not found"));
		}

		catch (Exception e) {
			throw new BusinessException("exception ", e);
		}

	}
	//End of Addition by Kamal Anand


	/*public @ResponseBody ResponseEntity<MISResponse> getGsrSettingsForUser(
			@PathVariable("slabId") int slabId) throws BusinessException {
		LOG.startUsecase("getAdminConfig");
		List<GsrAdminConfigurationBO> adminConfigSettings = null;
		List<GsrAdminConfigurationBO> adminConfigSettings1 = new ArrayList<GsrAdminConfigurationBO>();
		try {
			adminConfigSettings = gsrCommonService.getAdminConfig(slabId);
			adminConfigSettings1.addAll(adminConfigSettings);
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"All Admin Configuration Settings are Displayed.",
							adminConfigSettings1));
		} catch (Exception e) {
			throw new BusinessException("exception ", e);
		}
		// LOG.endUsecase("getAdminConfig");
		// return adminConfigSettings;

	}*/

}
// Ended by Pratibha T R on 17/11/2016

//Welcome to GIT 

