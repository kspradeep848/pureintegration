/*********************************************************************/
/*                                                                   */
/*  FileName    :  GsrCommonService.java                            */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  09-Nov-2016                                       */
/*                                                                   */
/*  Description :  This class contains the methods for the           */
/*                 GSRCommonController                               */
/*  Created By  : Manikandan_Chinnasamy                              */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 07-Nov-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.gsr.controller;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.ErrorResponse;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.gsr.bo.GsrNormalisedRatingOrgWideBO;
import com.thbs.mis.gsr.constants.GsrURIConstants;
import com.thbs.mis.gsr.service.GsrNormalisedRatingOrgWideService;

@Controller
public class GsrNormalisedRatingOrgWideController {

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(GsrNormalisedRatingOrgWideController.class);

	@Autowired
	GsrNormalisedRatingOrgWideService gsrNormalisedRatingOrgWideService;

	@RequestMapping(value = GsrURIConstants.NORMALISED_RATING_HISTORY, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> goalNormalisedRatingHistory(
			@PathVariable("year") String year) throws BusinessException {
		System.out.println("Entered GOALS_STATUS_BU ");
		List<GsrNormalisedRatingOrgWideBO> output = new ArrayList<GsrNormalisedRatingOrgWideBO>();
		int thisYear = Year.now().getValue();
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(500);
		try {
			if (thisYear > Integer.parseInt(year)) {
				errorResponse
						.setMessage("Year Should Be Current or Previous Year");
				errorResponse.setMessageKey("Invalid  Input");
				return new ResponseEntity<Object>(errorResponse,
						HttpStatus.INTERNAL_SERVER_ERROR);
			} else {
				output = gsrNormalisedRatingOrgWideService
						.goalNormalisedRatingHistory(year);
			}
		} catch (Exception e) {
			System.out.println("excepitoin in GOALS_STATUS_BU "
					+ e.getMessage());
			// throw new BusinessException("exception ",e);
		}
		System.out.println("Exited goal set ");
		return new ResponseEntity<Object>(output, HttpStatus.OK);
	}

	/*
	 * @RequestMapping(value = GsrURIConstants.UPLOAD_NORMALIZED_RATING, method
	 * = RequestMethod.POST) public @ResponseBody
	 * List<GsrNormalisedRatingOrgWideBO> uploadGoalNormalisedRating( ) throws
	 * BusinessException {
	 * System.out.println("Entered uploadGoalNormalisedRating ");
	 * List<GsrNormalisedRatingOrgWideBO> output = new
	 * ArrayList<GsrNormalisedRatingOrgWideBO>(); try { output =
	 * gsrNormalisedRatingOrgWideService.uploadGoalNormalisedRating(); } catch
	 * (Exception e) {
	 * System.out.println("excepitoin in uploadGoalNormalisedRating " +
	 * e.getMessage()); // throw new BusinessException("exception ",e); } return
	 * output; }
	 */

}
