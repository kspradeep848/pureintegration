package com.thbs.mis.usermanagement.dao;

import java.util.List;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.usermanagement.bo.MapMasOrgCountryBO;

public interface MapMasOrgCountryRepository extends GenericRepository<MapMasOrgCountryBO, Long>{

	public List<MapMasOrgCountryBO> findByFkOrgId(Short orgId);
}
