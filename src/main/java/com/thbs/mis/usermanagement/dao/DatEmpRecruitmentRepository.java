package com.thbs.mis.usermanagement.dao;

import org.springframework.data.repository.CrudRepository;

import com.thbs.mis.usermanagement.bo.DatEmpRecruitmentDetailBO;

public interface DatEmpRecruitmentRepository extends CrudRepository<DatEmpRecruitmentDetailBO, Long>{

	public DatEmpRecruitmentDetailBO findByFkEmpId(Integer empId);
}
