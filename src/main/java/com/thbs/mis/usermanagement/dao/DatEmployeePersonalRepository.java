package com.thbs.mis.usermanagement.dao;

import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.dao.GenericRepository;

public interface DatEmployeePersonalRepository extends GenericRepository<DatEmpPersonalDetailBO, Long>{

}
