package com.thbs.mis.usermanagement.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.GenericRepository;

public interface DatEmploymentDetailsRepository extends GenericRepository<DatEmpProfessionalDetailBO, Integer>{
	
	//Added  by Adavayya Hiremath for User Management module service
	@Query("SELECT new com.thbs.mis.common.bo.DatEmpProfessionalDetailBO( "
            + "dateEmpProf.fkMainEmpDetailId,dateEmpPer.empFirstName,dateEmpPer.empMiddleName,dateEmpPer.empLastName,"
            + "dateEmpProf.empProfessionalEmailId,masbu.buUnitName,design.empDesignationName,locate.locationParentName,"
            + "dateEmpPer.empMobileNo,dateEmpPer.empAlternateMobileNo,"
            + "emp_det.fkEmpMainStatus,dateEmpProf.empExtensionNumber) FROM "
            + "DatEmpProfessionalDetailBO dateEmpProf JOIN DatEmpPersonalDetailBO dateEmpPer ON dateEmpProf.fkMainEmpDetailId = dateEmpPer.fkEmpDetailId JOIN "
            + "DatEmpDetailBO emp_det ON dateEmpProf.fkMainEmpDetailId = emp_det.pkEmpId JOIN "
			+ "MasBuUnitBO masbu ON masbu.pkBuUnitId = dateEmpProf.fkEmpBuUnit JOIN "
			+ "MasEmpDesignationBO design ON design.pkEmpDesignationId = dateEmpProf.fkEmpDesignation JOIN "
			+ "MasLocationParentBO locate ON locate.pkLocationParentId = dateEmpProf.fkEmpLocationParentId "
            + "WHERE dateEmpProf.fkMainEmpDetailId = ?1 ")
	List<DatEmpProfessionalDetailBO> findByfkEmpDetailId(@Param("empId") Integer empId);
	//EOA by Adavayya Hiremath for user management Module service
	
}
