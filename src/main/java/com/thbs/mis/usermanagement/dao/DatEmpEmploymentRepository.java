package com.thbs.mis.usermanagement.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.usermanagement.bo.DatEmpEmploymentDetailBO;

public interface DatEmpEmploymentRepository extends GenericRepository<DatEmpEmploymentDetailBO, Long>{

	public List<DatEmpEmploymentDetailBO> findByFkEmpId(Integer empId);
	
	public DatEmpEmploymentDetailBO findByEmployerSequenceNumber(Integer sequenceNo);

	@Query("SELECT p FROM DatEmpEmploymentDetailBO p WHERE "
			+ " p.pkEmploymentId IN (SELECT MAX(b.pkEmploymentId) FROM "
			+ " DatEmpEmploymentDetailBO b) ")
	public DatEmpEmploymentDetailBO findByLastRow();
}
