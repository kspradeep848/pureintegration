package com.thbs.mis.usermanagement.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.usermanagement.bo.MasEmpSpecializationBO;


@Repository
public interface MasEmpSpecilizationRepository extends
		GenericRepository<MasEmpSpecializationBO, Long> {

	public List<MasEmpSpecializationBO> findAll();

}
