package com.thbs.mis.usermanagement.dao;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.usermanagement.bo.DatEmpLastEmployeeIdBO;

public interface DatEmpLastEmployeeIdRepository extends GenericRepository<DatEmpLastEmployeeIdBO, Long>{

	public DatEmpLastEmployeeIdBO findByEmpTypeId(int empType);
}
