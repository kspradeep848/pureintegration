package com.thbs.mis.usermanagement.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.usermanagement.bean.EmployeeLocChangeInputBean;
import com.thbs.mis.usermanagement.bo.DatEmpLocationApprovalBO;

public class EmployeeLocChangeSpecification {

	public static Specification<DatEmpLocationApprovalBO> getEmployeeLocChangeDetails(EmployeeLocChangeInputBean bean){
		
		return new Specification<DatEmpLocationApprovalBO>() {
			
			@Override
			public Predicate toPredicate(Root<DatEmpLocationApprovalBO> root,
					CriteriaQuery<?> cq, CriteriaBuilder cb) {
				
				final Collection<Predicate> predicates = new ArrayList<>();
				Calendar calStartDate = Calendar.getInstance();
				Calendar calEndDate = Calendar.getInstance();
				
				if(bean.getEmpId() != null){
					final Predicate empIdPredicate = cb.equal(root.get("fkEmpId"), bean.getEmpId());
					predicates.add(empIdPredicate);
				}
				
				if(bean.getLocation() != null){
					final Predicate newLocationPredicate = cb.equal(root.get("newLocation"), bean.getLocation());
					final Predicate oldLocationPredicate = cb.equal(root.get("oldLocation"), bean.getLocation());
					predicates.add(newLocationPredicate);
					predicates.add(oldLocationPredicate);
				}
				
				if(bean.getFromDate()!=null && bean.getToDate()!=null){
					
					calStartDate.setTime(bean.getFromDate());
					calStartDate.set(Calendar.HOUR_OF_DAY,0);
					calStartDate.set(Calendar.MINUTE,0);
					calStartDate.set(Calendar.SECOND,0);
					calEndDate.setTime(bean.getToDate());
					calEndDate.set(Calendar.HOUR_OF_DAY,23);
					calEndDate.set(Calendar.MINUTE,59);
					calEndDate.set(Calendar.SECOND,59);
					
					final Predicate dateRangePredicate =  cb.and(cb.greaterThanOrEqualTo(root.get("requestedDate"),
							calStartDate.getTime()), cb.lessThanOrEqualTo(
							root.get("requestedDate"), calEndDate.getTime()));
					
					predicates.add(dateRangePredicate);
				}
				
				if(bean.getStatus()!=null){
					final Predicate statusPredicate = cb.equal(root.get("status"), bean.getStatus());
					predicates.add(statusPredicate);
				}
				cq.orderBy(cb.asc(root.get("status")));
				
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
	}
}
