package com.thbs.mis.usermanagement.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.thbs.mis.usermanagement.bean.EmployeeLocChangeOutputBean;
import com.thbs.mis.usermanagement.bo.DatEmpLocationApprovalBO;

public interface DatEmpLocApprovalRepository extends CrudRepository<DatEmpLocationApprovalBO, Long>, JpaSpecificationExecutor<DatEmpLocationApprovalBO>{

	public DatEmpLocationApprovalBO findByFkEmpIdAndStatus(Integer empId, byte status);
	
	public DatEmpLocationApprovalBO findByPkId(Integer pkId);
	
	@Query(name = "UserManagement.employeeLocationDetails")
	public List<EmployeeLocChangeOutputBean> getEmployeeLocChangesDetails(@Param("id") List<Integer> id);
}
