package com.thbs.mis.usermanagement.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.usermanagement.bo.MasEmpStreamBO;

@Repository
public interface MasEmpStreamRepository extends GenericRepository<MasEmpStreamBO, Long>{
	
	public List<MasEmpStreamBO> findAll();


}
