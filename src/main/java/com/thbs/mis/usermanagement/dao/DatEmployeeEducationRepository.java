package com.thbs.mis.usermanagement.dao;

import java.util.List;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.usermanagement.bo.DatEmpEducationDetailBO;

public interface DatEmployeeEducationRepository extends GenericRepository<DatEmpEducationDetailBO, Long>{

	public List<DatEmpEducationDetailBO> findByFkEmpId(Integer empId);
}
