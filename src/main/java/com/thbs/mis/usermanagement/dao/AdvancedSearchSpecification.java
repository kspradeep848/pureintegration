package com.thbs.mis.usermanagement.dao;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.MasLocationParentBO;
import com.thbs.mis.usermanagement.bean.AdvancedSearchInputBean;
public class AdvancedSearchSpecification {
	public static Specification<DatEmpProfessionalDetailBO> getEmpProfessionalDetails(
			AdvancedSearchInputBean searchBean, List<Integer> empIdList) {
		return new Specification<DatEmpProfessionalDetailBO>() {
			@Override
			public Predicate toPredicate(Root<DatEmpProfessionalDetailBO> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				final Collection<Predicate> predicates = new ArrayList<>();
				if (searchBean.getEmployementType() != null) {
					Join<DatEmpProfessionalDetailBO, DatEmpDetailBO> empTypeJoin = root
							.join("DatEmpDetailBO_main_emp_id");
					Predicate empTypePredicate = cb.equal(
							empTypeJoin.get("empType"),
							searchBean.getEmployementType());
					predicates.add(empTypePredicate);
				}
				if (searchBean.getEmpStatus() != null) {
					Join<DatEmpProfessionalDetailBO, DatEmpDetailBO> empStatusJoin = root
							.join("DatEmpDetailBO_main_emp_id");
					Predicate empStatusPredicate = cb.equal(
							empStatusJoin.get("fkEmpMainStatus"),
							searchBean.getEmpStatus());
					predicates.add(empStatusPredicate);
				}
				if (searchBean.getEmpSubStatus() != null) {
					Join<DatEmpProfessionalDetailBO, DatEmpDetailBO> empsubStatusJoin = root
							.join("DatEmpDetailBO_main_emp_id");
					Predicate empsubStatusPredicate = cb.equal(
							empsubStatusJoin.get("fkEmpSubStatus"),
							searchBean.getEmpSubStatus());
					predicates.add(empsubStatusPredicate);
				}
				if (searchBean.getBuId() != null) {
					final Predicate buPredicate = cb.equal(
							root.get("fkEmpBuUnit"), searchBean.getBuId());
					predicates.add(buPredicate);
				}
				if (searchBean.getLocationId() != null) {
					final Predicate locationPredicate = cb.equal(
							root.get("fkEmpLocationParentId"),
							searchBean.getLocationId());
					predicates.add(locationPredicate);
				}
				if (searchBean.getLevelId() != null) {
					final Predicate levelPredicate = cb.equal(
							root.get("fkEmpLevel"), searchBean.getLevelId());
					predicates.add(levelPredicate);
				}
				if (searchBean.getDesignationId() != null) {
					final Predicate designationPredicate = cb.equal(
							root.get("fkEmpDesignation"),
							searchBean.getDesignationId());
					predicates.add(designationPredicate);
				}
				if (searchBean.getBootcampId() != null) {
					final Predicate bootcampPredicate = cb.equal(
							root.get("fkEmpBootCampId"),
							searchBean.getBootcampId());
					predicates.add(bootcampPredicate);
				}
				if (searchBean.getStartDate() != null
						&& searchBean.getEndDate() != null) {
					final Predicate dateRangePredicate = cb.and(cb
							.greaterThanOrEqualTo(root.get("empDateOfJoining"),
									searchBean.getStartDate()), cb
							.lessThanOrEqualTo(root.get("empDateOfJoining"),
									searchBean.getEndDate()));
					predicates.add(dateRangePredicate);
				}
				if (searchBean.getCountryId() != null) {
					Join<DatEmpProfessionalDetailBO, MasLocationParentBO> empCountryLocJoin = root
							.join("masLocationParentBO");
					Predicate countryIdPredicate = cb.equal(
							empCountryLocJoin.get("fkCountryId"),
							searchBean.getCountryId());
					predicates.add(countryIdPredicate);
				}
				if (!empIdList.isEmpty()) {
					final Predicate empListPredicate = root.get(
							"fkMainEmpDetailId").in(empIdList);
					predicates.add(empListPredicate);
				}
				return cb.and(predicates.toArray(new Predicate[predicates
						.size()]));
			}
		};
	}
	public static Specification<DatEmpPersonalDetailBO> getEmpPersonalDetails(
			AdvancedSearchInputBean searchBean, List<Integer> empIdList) {
		return new Specification<DatEmpPersonalDetailBO>() {
			@Override
			public Predicate toPredicate(Root<DatEmpPersonalDetailBO> root,
					CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				final Collection<Predicate> predicates = new ArrayList<>();
				if (searchBean.getGenderId() != null) {
					final Predicate genderPredicate = cb.equal(
							root.get("empGender"), searchBean.getGenderId());
					predicates.add(genderPredicate);
				}
				
				if (!empIdList.isEmpty()) {
					final Predicate empListPredicate = root.get(
							"fkEmpDetailId").in(empIdList);
					predicates.add(empListPredicate);
				}
				
				return cb.and(predicates.toArray(new Predicate[predicates
						.size()]));
			}
		};
	}
}