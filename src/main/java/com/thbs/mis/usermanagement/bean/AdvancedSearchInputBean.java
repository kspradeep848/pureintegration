package com.thbs.mis.usermanagement.bean;

import java.util.Date;

public class AdvancedSearchInputBean {

	private String empIdOrName;
	private Byte employementType;
	private Byte empStatus;
	private Short buId;
	private Short locationId;
	private Short countryId;
	private Short levelId;
	private Short designationId;
	private Short bootcampId;
	private Byte genderId;
	private Date startDate;
	private Date endDate;
	private Byte empSubStatus;

	public Byte getEmpSubStatus() {
		return empSubStatus;
	}

	public void setEmpSubStatus(Byte empSubStatus) {
		this.empSubStatus = empSubStatus;
	}

	public String getEmpIdOrName() {
		return empIdOrName;
	}

	public void setEmpIdOrName(String empIdOrName) {
		this.empIdOrName = empIdOrName;
	}

	public Byte getEmployementType() {
		return employementType;
	}

	public void setEmployementType(Byte employementType) {
		this.employementType = employementType;
	}

	public Byte getEmpStatus() {
		return empStatus;
	}

	public void setEmpStatus(Byte empStatus) {
		this.empStatus = empStatus;
	}

	public Short getBuId() {
		return buId;
	}

	public void setBuId(Short buId) {
		this.buId = buId;
	}

	public Short getLocationId() {
		return locationId;
	}

	public void setLocationId(Short locationId) {
		this.locationId = locationId;
	}

	public Short getCountryId() {
		return countryId;
	}

	public void setCountryId(Short countryId) {
		this.countryId = countryId;
	}

	public Short getLevelId() {
		return levelId;
	}

	public void setLevelId(Short levelId) {
		this.levelId = levelId;
	}

	public Short getDesignationId() {
		return designationId;
	}

	public void setDesignationId(Short designationId) {
		this.designationId = designationId;
	}

	public Short getBootcampId() {
		return bootcampId;
	}

	public void setBootcampId(Short bootcampId) {
		this.bootcampId = bootcampId;
	}

	public Byte getGenderId() {
		return genderId;
	}

	public void setGenderId(Byte genderId) {
		this.genderId = genderId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "AdvancedSearchInputBean [empIdOrName=" + empIdOrName
				+ ", employementType=" + employementType + ", empStatus="
				+ empStatus + ", buId=" + buId + ", locationId=" + locationId
				+ ", countryId=" + countryId + ", levelId=" + levelId
				+ ", designationId=" + designationId + ", bootcampId="
				+ bootcampId + ", genderId=" + genderId + ", startDate="
				+ startDate + ", endDate=" + endDate + ", empSubStatus="
				+ empSubStatus + "]";
	}

}
