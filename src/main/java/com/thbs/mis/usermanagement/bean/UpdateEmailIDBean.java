package com.thbs.mis.usermanagement.bean;

public class UpdateEmailIDBean {

	private Integer empId;

	private String emailID;
	
	private Integer loggedInEmpId;


	public Integer getEmpId() {

		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public Integer getLoggedInEmpId() {
		return loggedInEmpId;
	}

	public void setLoggedInEmpId(Integer loggedInEmpId) {
		this.loggedInEmpId = loggedInEmpId;
	}

	@Override
	public String toString() {

		return "UpdateEmailIDBean [empId=" + empId + ", emailID=" + emailID + ", loggedInEmpId=" + loggedInEmpId + "]";

	}


}
