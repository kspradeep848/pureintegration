package com.thbs.mis.usermanagement.bean;

import java.util.Date;

public class AdvancedSearchOutputBean {

	private int empId;
	private String empName;
	private Short designationId;
	private String designationName;
	private String location;
	private Byte empStatusId;
	private String empStatusName;
	private Date dateOfJoining;
	private String buName;

	public AdvancedSearchOutputBean(int empId, String empName,
			Short designationId, String designationName, String location,
			Byte empStatusId, String empStatusName, Date dateOfJoining,
			String buName) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.designationId = designationId;
		this.designationName = designationName;
		this.location = location;
		this.empStatusId = empStatusId;
		this.empStatusName = empStatusName;
		this.dateOfJoining = dateOfJoining;
		this.buName = buName;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Short getDesignationId() {
		return designationId;
	}

	public void setDesignationId(Short designationId) {
		this.designationId = designationId;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Byte getEmpStatusId() {
		return empStatusId;
	}

	public void setEmpStatusId(Byte empStatusId) {
		this.empStatusId = empStatusId;
	}

	public String getEmpStatusName() {
		return empStatusName;
	}

	public void setEmpStatusName(String empStatusName) {
		this.empStatusName = empStatusName;
	}

	public Date getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public String getBuName() {
		return buName;
	}

	public void setBuName(String buName) {
		this.buName = buName;
	}

	@Override
	public String toString() {
		return "AdvancedSearchOutputBean [empId=" + empId + ", empName="
				+ empName + ", designationId=" + designationId
				+ ", designationName=" + designationName + ", location="
				+ location + ", empStatusId=" + empStatusId
				+ ", empStatusName=" + empStatusName + ", dateOfJoining="
				+ dateOfJoining + ", buName=" + buName + "]";
	}

}
