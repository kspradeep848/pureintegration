package com.thbs.mis.usermanagement.bean;

public class EmployeeEducationalInputBean {

	private Integer empId;
	private Integer educationalLevel;
	private String nameOfSchool;
	private Integer stream;
	private Integer specilization;
	private Integer yearOfCompletion;
	private Integer percentage;
	private Integer loggedInEmpId;
	public Integer getEmpId() {
		return empId;
	}
	
	
	public Integer getLoggedInEmpId() {
		return loggedInEmpId;
	}


	public void setLoggedInEmpId(Integer loggedInEmpId) {
		this.loggedInEmpId = loggedInEmpId;
	}


	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public Integer getEducationalLevel() {
		return educationalLevel;
	}
	public void setEducationalLevel(Integer educationalLevel) {
		this.educationalLevel = educationalLevel;
	}
	public String getNameOfSchool() {
		return nameOfSchool;
	}
	public void setNameOfSchool(String nameOfSchool) {
		this.nameOfSchool = nameOfSchool;
	}
	public Integer getStream() {
		return stream;
	}
	public void setStream(Integer stream) {
		this.stream = stream;
	}
	public Integer getSpecilization() {
		return specilization;
	}
	public void setSpecilization(Integer specilization) {
		this.specilization = specilization;
	}
	public Integer getYearOfCompletion() {
		return yearOfCompletion;
	}
	public void setYearOfCompletion(Integer yearOfCompletion) {
		this.yearOfCompletion = yearOfCompletion;
	}
	public Integer getPercentage() {
		return percentage;
	}
	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}


	@Override
	public String toString() {
		return "EmployeeEducationalInputBean [empId=" + empId
				+ ", educationalLevel=" + educationalLevel + ", nameOfSchool="
				+ nameOfSchool + ", stream=" + stream + ", specilization="
				+ specilization + ", yearOfCompletion=" + yearOfCompletion
				+ ", percentage=" + percentage + ", loggedInEmpId="
				+ loggedInEmpId + "]";
	}
	
	
	
}
