package com.thbs.mis.usermanagement.bean;

public class EmployeeLocationBean {

	
	private Short thbsOrg;
	private Boolean onSiteOrOffShore;
	private Boolean domestic;
	private Boolean international;
	private Boolean onsiteClient;

	public Short getThbsOrg() {
		return thbsOrg;
	}
	public void setThbsOrg(Short thbsOrg) {
		this.thbsOrg = thbsOrg;
	}
	public Boolean getOnSiteOrOffShore() {
		return onSiteOrOffShore;
	}
	public void setOnSiteOrOffShore(Boolean onSiteOrOffShore) {
		this.onSiteOrOffShore = onSiteOrOffShore;
	}
	public Boolean getDomestic() {
		return domestic;
	}
	public void setDomestic(Boolean domestic) {
		this.domestic = domestic;
	}
	public Boolean getInternational() {
		return international;
	}
	public void setInternational(Boolean international) {
		this.international = international;
	}
	public Boolean getOnsiteClient() {
		return onsiteClient;
	}
	public void setOnsiteClient(Boolean onsiteClient) {
		this.onsiteClient = onsiteClient;
	}
	@Override
	public String toString() {
		return "EmployeeLocationBean [thbsOrg=" + thbsOrg
				+ ", onSiteOrOffShore=" + onSiteOrOffShore + ", domestic="
				+ domestic + ", International=" + international
				+ ", onsiteClient=" + onsiteClient + "]";
	}
	
	
	
}
