package com.thbs.mis.usermanagement.bean;

public class EmployeeDetailInputBean {

	private String empFirstName;
	private String empMiddleName;
	private String empLastName;
	private Byte empType;
	private Short bootCampId;
	private Short buUnit;
	private Integer reportingManagerId;
	private Integer logginEmployee;
	
	
	public Integer getLogginEmployee() {
		return logginEmployee;
	}
	public void setLogginEmployee(Integer logginEmployee) {
		this.logginEmployee = logginEmployee;
	}
	public String getEmpFirstName() {
		return empFirstName;
	}
	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}
	public String getEmpMiddleName() {
		return empMiddleName;
	}
	public void setEmpMiddleName(String empMiddleName) {
		this.empMiddleName = empMiddleName;
	}
	public String getEmpLastName() {
		return empLastName;
	}
	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}
	public Byte getEmpType() {
		return empType;
	}
	public void setEmpType(Byte empType) {
		this.empType = empType;
	}
	public Short getBootCampId() {
		return bootCampId;
	}
	public void setBootCampId(Short bootCampId) {
		this.bootCampId = bootCampId;
	}
	public Short getBuUnit() {
		return buUnit;
	}
	public void setBuUnit(Short buUnit) {
		this.buUnit = buUnit;
	}
	public Integer getReportingManagerId() {
		return reportingManagerId;
	}
	public void setReportingManagerId(Integer reportingManagerId) {
		this.reportingManagerId = reportingManagerId;
	}
	@Override
	public String toString() {
		return "EmployeeDetailInputBean [empFirstName=" + empFirstName
				+ ", empMiddleName=" + empMiddleName + ", empLastName="
				+ empLastName + ", empType=" + empType + ", bootCampId="
				+ bootCampId + ", buUnit=" + buUnit + ", reportingManagerId="
				+ reportingManagerId + ", logginEmployee=" + logginEmployee
				+ "]";
	}
	
	
	
}
