package com.thbs.mis.usermanagement.bean;

public class DateRangeOutputBean {

	private Integer empId;
	private String empName;
	private String empType;
	private Byte empTypeID;
	private Short buUnitId;
	private String buUnitName;
	private short bootCampId;
	private String bootCampName;
	private Integer reportingManager;
	private String reportingManagerName;
	private String emailID;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpType() {
		return empType;
	}

	public void setEmpType(String empType) {
		this.empType = empType;
	}

	public Byte getEmpTypeID() {
		return empTypeID;
	}

	public void setEmpTypeID(Byte empTypeID) {
		this.empTypeID = empTypeID;
	}

	public Short getBuUnitId() {
		return buUnitId;
	}

	public void setBuUnitId(Short buUnitId) {
		this.buUnitId = buUnitId;
	}

	public String getBuUnitName() {
		return buUnitName;
	}

	public void setBuUnitName(String buUnitName) {
		this.buUnitName = buUnitName;
	}

	public short getBootCampId() {
		return bootCampId;
	}

	public void setBootCampId(short bootCampId) {
		this.bootCampId = bootCampId;
	}

	public String getBootCampName() {
		return bootCampName;
	}

	public void setBootCampName(String bootCampName) {
		this.bootCampName = bootCampName;
	}

	public Integer getReportingManager() {
		return reportingManager;
	}

	public void setReportingManager(Integer reportingManager) {
		this.reportingManager = reportingManager;
	}

	public String getReportingManagerName() {
		return reportingManagerName;
	}

	public void setReportingManagerName(String reportingManagerName) {
		this.reportingManagerName = reportingManagerName;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	@Override
	public String toString() {
		return "DateRangeOutputBean [empId=" + empId + ", empName=" + empName + ", empType=" + empType + ", empTypeID="
				+ empTypeID + ", buUnitId=" + buUnitId + ", buUnitName=" + buUnitName + ", bootCampId=" + bootCampId
				+ ", bootCampName=" + bootCampName + ", reportingManager=" + reportingManager
				+ ", reportingManagerName=" + reportingManagerName + ", emailID=" + emailID + "]";
	}

}
