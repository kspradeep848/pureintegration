package com.thbs.mis.usermanagement.bean;

import java.util.List;


public class EmployeeRecruitmentBean {

	
	private Integer empSubSourceId;


	private Integer empId;

	
	private Integer recruiterId;

	
	private List<Integer> managerialPanel;

	
	private Integer loggedInEmpId;

	
	private Integer referralEmpId;

	
	private List<Integer> technicalPanel;

	
	private Integer empSourceId;


	public Integer getEmpSubSourceId() {
		return empSubSourceId;
	}


	public void setEmpSubSourceId(Integer empSubSourceId) {
		this.empSubSourceId = empSubSourceId;
	}


	public Integer getEmpId() {
		return empId;
	}


	public void setEmpId(Integer empId) {
		this.empId = empId;
	}


	public Integer getRecruiterId() {
		return recruiterId;
	}


	public void setRecruiterId(Integer recruiterId) {
		this.recruiterId = recruiterId;
	}

	public Integer getLoggedInEmpId() {
		return loggedInEmpId;
	}


	public void setLoggedInEmpId(Integer loggedInEmpId) {
		this.loggedInEmpId = loggedInEmpId;
	}


	public Integer getReferralEmpId() {
		return referralEmpId;
	}


	public void setReferralEmpId(Integer referralEmpId) {
		this.referralEmpId = referralEmpId;
	}


	public Integer getEmpSourceId() {
		return empSourceId;
	}


	public void setEmpSourceId(Integer empSourceId) {
		this.empSourceId = empSourceId;
	}


	
	public List<Integer> getManagerialPanel() {
		return managerialPanel;
	}


	public void setManagerialPanel(List<Integer> managerialPanel) {
		this.managerialPanel = managerialPanel;
	}


	public List<Integer> getTechnicalPanel() {
		return technicalPanel;
	}


	public void setTechnicalPanel(List<Integer> technicalPanel) {
		this.technicalPanel = technicalPanel;
	}


	@Override
	public String toString() {
		return "EmployeeRecruitmentBean [empSubSourceId=" + empSubSourceId
				+ ", empId=" + empId + ", recruiterId=" + recruiterId
				+ ", managerialPanel=" + managerialPanel + ", loggedInEmpId="
				+ loggedInEmpId + ", referralEmpId=" + referralEmpId
				+ ", technicalPanel=" + technicalPanel + ", empSourceId="
				+ empSourceId + "]";
	}


	
	
}
