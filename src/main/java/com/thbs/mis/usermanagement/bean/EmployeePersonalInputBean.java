package com.thbs.mis.usermanagement.bean;

import java.util.Date;

public class EmployeePersonalInputBean {

	private Integer empId;
	private String empFirstName;
	private String empMiddleName;
	private String empLastName;
	private Date dateOfBirth;
	private Byte gender;
	private String bloodGroup;
	private String mobileNumber;
	private String homePhone;
	private String personalEmail;
	private String martialStatus;
	private String spouseName;
	private Date spouseDOB;
	private String presentAddressLineOne;
	private String presentAddressLineTwo;
	private String permanentAddressLineOne;
	private String permanentAddressLineTwo;
	private Short permanentAddressCity;
	private Short permanentAddressState;
	private Short permanentAddressCountry;
	private Short presentAddressCity;
	private Short presentAddressState;
	private Short presentAddressCountry;
	private Integer loggedInEmpId;
	private Boolean isAadharAvailable;
	private Boolean isPanCardAvailable;
	private Boolean isVoterIdAvailable;
	private Boolean isPassportAvailable;
	private String aadharPath;
	private String panCardPath;
	private String voterIdPath;
	private String passPortPath;
	private Boolean isProfilePhotoAvailable;
	private String profilePhotoPath;
	private EmployeeKYCInputBean kycInputBean;
	
	
	
	public Integer getLoggedInEmpId() {
		return loggedInEmpId;
	}
	public void setLoggedInEmpId(Integer loggedInEmpId) {
		this.loggedInEmpId = loggedInEmpId;
	}
	public Boolean getIsProfilePhotoAvailable() {
		return isProfilePhotoAvailable;
	}
	public void setIsProfilePhotoAvailable(Boolean isProfilePhotoAvailable) {
		this.isProfilePhotoAvailable = isProfilePhotoAvailable;
	}
	public String getProfilePhotoPath() {
		return profilePhotoPath;
	}
	public void setProfilePhotoPath(String profilePhotoPath) {
		this.profilePhotoPath = profilePhotoPath;
	}
	public String getAadharPath() {
		return aadharPath;
	}
	public void setAadharPath(String aadharPath) {
		this.aadharPath = aadharPath;
	}
	public String getPanCardPath() {
		return panCardPath;
	}
	public void setPanCardPath(String panCardPath) {
		this.panCardPath = panCardPath;
	}
	public String getVoterIdPath() {
		return voterIdPath;
	}
	public void setVoterIdPath(String voterIdPath) {
		this.voterIdPath = voterIdPath;
	}
	public String getPassPortPath() {
		return passPortPath;
	}
	public void setPassPortPath(String passPortPath) {
		this.passPortPath = passPortPath;
	}
	public Boolean getIsAadharAvailable() {
		return isAadharAvailable;
	}
	public void setIsAadharAvailable(Boolean isAadharAvailable) {
		this.isAadharAvailable = isAadharAvailable;
	}
	public Boolean getIsPanCardAvailable() {
		return isPanCardAvailable;
	}
	public void setIsPanCardAvailable(Boolean isPanCardAvailable) {
		this.isPanCardAvailable = isPanCardAvailable;
	}
	public Boolean getIsVoterIdAvailable() {
		return isVoterIdAvailable;
	}
	public void setIsVoterIdAvailable(Boolean isVoterIdAvailable) {
		this.isVoterIdAvailable = isVoterIdAvailable;
	}
	public Boolean getIsPassportAvailable() {
		return isPassportAvailable;
	}
	public void setIsPassportAvailable(Boolean isPassportAvailable) {
		this.isPassportAvailable = isPassportAvailable;
	}
	public EmployeeKYCInputBean getKycInputBean() {
		return kycInputBean;
	}
	public void setKycInputBean(EmployeeKYCInputBean kycInputBean) {
		this.kycInputBean = kycInputBean;
	}
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getEmpFirstName() {
		return empFirstName;
	}
	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}
	public String getEmpMiddleName() {
		return empMiddleName;
	}
	public void setEmpMiddleName(String empMiddleName) {
		this.empMiddleName = empMiddleName;
	}
	public String getEmpLastName() {
		return empLastName;
	}
	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}
	
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public Byte getGender() {
		return gender;
	}
	public void setGender(Byte gender) {
		this.gender = gender;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public String getPersonalEmail() {
		return personalEmail;
	}
	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}
	public String getMartialStatus() {
		return martialStatus;
	}
	public void setMartialStatus(String martialStatus) {
		this.martialStatus = martialStatus;
	}
	public String getSpouseName() {
		return spouseName;
	}
	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}
	public Date getSpouseDOB() {
		return spouseDOB;
	}
	public void setSpouseDOB(Date spouseDOB) {
		this.spouseDOB = spouseDOB;
	}
	
	
	public String getPresentAddressLineOne() {
		return presentAddressLineOne;
	}
	public void setPresentAddressLineOne(String presentAddressLineOne) {
		this.presentAddressLineOne = presentAddressLineOne;
	}
	public String getPresentAddressLineTwo() {
		return presentAddressLineTwo;
	}
	public void setPresentAddressLineTwo(String presentAddressLineTwo) {
		this.presentAddressLineTwo = presentAddressLineTwo;
	}
	public String getPermanentAddressLineOne() {
		return permanentAddressLineOne;
	}
	public void setPermanentAddressLineOne(String permanentAddressLineOne) {
		this.permanentAddressLineOne = permanentAddressLineOne;
	}
	public String getPermanentAddressLineTwo() {
		return permanentAddressLineTwo;
	}
	public void setPermanentAddressLineTwo(String permanentAddressLineTwo) {
		this.permanentAddressLineTwo = permanentAddressLineTwo;
	}
	public Short getPermanentAddressCity() {
		return permanentAddressCity;
	}
	public void setPermanentAddressCity(Short permanentAddressCity) {
		this.permanentAddressCity = permanentAddressCity;
	}
	public Short getPermanentAddressState() {
		return permanentAddressState;
	}
	public void setPermanentAddressState(Short permanentAddressState) {
		this.permanentAddressState = permanentAddressState;
	}
	public Short getPermanentAddressCountry() {
		return permanentAddressCountry;
	}
	public void setPermanentAddressCountry(Short permanentAddressCountry) {
		this.permanentAddressCountry = permanentAddressCountry;
	}
	public Short getPresentAddressCity() {
		return presentAddressCity;
	}
	public void setPresentAddressCity(Short presentAddressCity) {
		this.presentAddressCity = presentAddressCity;
	}
	public Short getPresentAddressState() {
		return presentAddressState;
	}
	public void setPresentAddressState(Short presentAddressState) {
		this.presentAddressState = presentAddressState;
	}
	public Short getPresentAddressCountry() {
		return presentAddressCountry;
	}
	public void setPresentAddressCountry(Short presentAddressCountry) {
		this.presentAddressCountry = presentAddressCountry;
	}
	@Override
	public String toString() {
		return "EmployeePersonalInputBean [empId=" + empId + ", empFirstName="
				+ empFirstName + ", empMiddleName=" + empMiddleName
				+ ", empLastName=" + empLastName + ", dateOfBirth="
				+ dateOfBirth + ", gender=" + gender + ", bloodGroup="
				+ bloodGroup + ", mobileNumber=" + mobileNumber
				+ ", homePhone=" + homePhone + ", personalEmail="
				+ personalEmail + ", martialStatus=" + martialStatus
				+ ", spouseName=" + spouseName + ", spouseDOB=" + spouseDOB
				+ ", presentAddressLineOne=" + presentAddressLineOne
				+ ", presentAddressLineTwo=" + presentAddressLineTwo
				+ ", permanentAddressLineOne=" + permanentAddressLineOne
				+ ", permanentAddressLineTwo=" + permanentAddressLineTwo
				+ ", permanentAddressCity=" + permanentAddressCity
				+ ", permanentAddressState=" + permanentAddressState
				+ ", permanentAddressCountry=" + permanentAddressCountry
				+ ", presentAddressCity=" + presentAddressCity
				+ ", presentAddressState=" + presentAddressState
				+ ", presentAddressCountry=" + presentAddressCountry
				+ ", loggedInEmpId=" + loggedInEmpId + ", isAadharAvailable="
				+ isAadharAvailable + ", isPanCardAvailable="
				+ isPanCardAvailable + ", isVoterIdAvailable="
				+ isVoterIdAvailable + ", isPassportAvailable="
				+ isPassportAvailable + ", aadharPath=" + aadharPath
				+ ", panCardPath=" + panCardPath + ", voterIdPath="
				+ voterIdPath + ", passPortPath=" + passPortPath
				+ ", isProfilePhotoAvailable=" + isProfilePhotoAvailable
				+ ", profilePhotoPath=" + profilePhotoPath + ", kycInputBean="
				+ kycInputBean + "]";
	}
	
	
}
