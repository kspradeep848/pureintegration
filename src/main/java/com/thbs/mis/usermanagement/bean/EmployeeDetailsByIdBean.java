package com.thbs.mis.usermanagement.bean;

public class EmployeeDetailsByIdBean
{

	private Integer empId;
	private String empName;
	private String emailId;
	private String businessUnit;
	private String designation;
	private String location;
	private String mobileNumber;
	private String homeNumber;
	private String empStatus;
	private String extension;
	
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getHomeNumber() {
		return homeNumber;
	}
	public void setHomeNumber(String homeNumber) {
		this.homeNumber = homeNumber;
	}
	public String getEmpStatus() {
		return empStatus;
	}
	public void setEmpStatus(String empStatus) {
		this.empStatus = empStatus;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	@Override
	public String toString() {
		return "EmployeeDetailsByIdBean [empId=" + empId + ", empName=" + empName + ", emailId=" + emailId
				+ ", businessUnit=" + businessUnit + ", designation=" + designation + ", location=" + location
				+ ", mobileNumber=" + mobileNumber + ", homeNumber=" + homeNumber + ", empStatus=" + empStatus
				+ ", extension=" + extension + "]";
	}
	
	
	
}
