package com.thbs.mis.usermanagement.bean;

import java.util.List;

public class EmployeeProfileDetailBean {

	private String employeeStatus;
	private Byte employeeStatusId;
	private String employeeSubStatus;
	private Byte employeeSubStatusId;
	private String extensionNumber;
	
	private EmployeePersonalInputBean personalDetail;
	private List<CreateEmpProfDetailBean> professionalDetails;
	private List<EmployeeEducationalOutputBean> educationalDetail;
	private EmployeeOrganizationOutputBean organizationDetail;
	
	
	
	public String getEmployeeStatus() {
		return employeeStatus;
	}
	public void setEmployeeStatus(String employeeStatus) {
		this.employeeStatus = employeeStatus;
	}
	public Byte getEmployeeStatusId() {
		return employeeStatusId;
	}
	public void setEmployeeStatusId(Byte employeeStatusId) {
		this.employeeStatusId = employeeStatusId;
	}
	public String getEmployeeSubStatus() {
		return employeeSubStatus;
	}
	public void setEmployeeSubStatus(String employeeSubStatus) {
		this.employeeSubStatus = employeeSubStatus;
	}
	public Byte getEmployeeSubStatusId() {
		return employeeSubStatusId;
	}
	public void setEmployeeSubStatusId(Byte employeeSubStatusId) {
		this.employeeSubStatusId = employeeSubStatusId;
	}
	public String getExtensionNumber() {
		return extensionNumber;
	}
	public void setExtensionNumber(String extensionNumber) {
		this.extensionNumber = extensionNumber;
	}
	public EmployeePersonalInputBean getPersonalDetail() {
		return personalDetail;
	}
	public void setPersonalDetail(EmployeePersonalInputBean personalDetail) {
		this.personalDetail = personalDetail;
	}
	public List<CreateEmpProfDetailBean> getProfessionalDetails() {
		return professionalDetails;
	}
	public void setProfessionalDetails(
			List<CreateEmpProfDetailBean> professionalDetails) {
		this.professionalDetails = professionalDetails;
	}
	public List<EmployeeEducationalOutputBean> getEducationalDetail() {
		return educationalDetail;
	}
	public void setEducationalDetail(List<EmployeeEducationalOutputBean> educationalDetail) {
		this.educationalDetail = educationalDetail;
	}
	public EmployeeOrganizationOutputBean getOrganizationDetail() {
		return organizationDetail;
	}
	public void setOrganizationDetail(
			EmployeeOrganizationOutputBean organizationDetail) {
		this.organizationDetail = organizationDetail;
	}
	@Override
	public String toString() {
		return "EmployeeProfileDetailBean [employeeStatus=" + employeeStatus
				+ ", employeeStatusId=" + employeeStatusId
				+ ", employeeSubStatus=" + employeeSubStatus
				+ ", employeeSubStatusId=" + employeeSubStatusId
				+ ", extensionNumber=" + extensionNumber + ", personalDetail="
				+ personalDetail + ", professionalDetails="
				+ professionalDetails + ", educationalDetail="
				+ educationalDetail + ", organizationDetail="
				+ organizationDetail + "]";
	}
	

	
}
