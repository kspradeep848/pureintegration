package com.thbs.mis.usermanagement.bean;

public class EmployeeDetailOutputBean {

	private Integer empId;
	private String empName;
	private Byte empTypeId;
	private String empType;
	private String buUnit;
	private Short buUnitId;
	private Short bootCampId;
	private String bootCamp;
	private Integer reportingManagerId;
	private String reportingManagerName;
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public Byte getEmpTypeId() {
		return empTypeId;
	}
	public void setEmpTypeId(Byte empTypeId) {
		this.empTypeId = empTypeId;
	}
	public String getEmpType() {
		return empType;
	}
	public void setEmpType(String empType) {
		this.empType = empType;
	}
	public String getBuUnit() {
		return buUnit;
	}
	public void setBuUnit(String buUnit) {
		this.buUnit = buUnit;
	}
	public Short getBuUnitId() {
		return buUnitId;
	}
	public void setBuUnitId(Short buUnitId) {
		this.buUnitId = buUnitId;
	}
	public Short getBootCampId() {
		return bootCampId;
	}
	public void setBootCampId(Short bootCampId) {
		this.bootCampId = bootCampId;
	}
	public Integer getReportingManagerId() {
		return reportingManagerId;
	}
	public void setReportingManagerId(Integer reportingManagerId) {
		this.reportingManagerId = reportingManagerId;
	}
	public String getReportingManagerName() {
		return reportingManagerName;
	}
	public void setReportingManagerName(String reportingManagerName) {
		this.reportingManagerName = reportingManagerName;
	}
	
	public String getBootCamp() {
		return bootCamp;
	}
	public void setBootCamp(String bootCamp) {
		this.bootCamp = bootCamp;
	}
	@Override
	public String toString() {
		return "EmployeeDetailOutputBean [empId=" + empId + ", empName="
				+ empName + ", empTypeId=" + empTypeId + ", empType=" + empType
				+ ", buUnit=" + buUnit + ", buUnitId=" + buUnitId
				+ ", bootCampId=" + bootCampId + ", bootCamp=" + bootCamp
				+ ", reportingManagerId=" + reportingManagerId
				+ ", reportingManagerName=" + reportingManagerName + "]";
	}
	

	
}
