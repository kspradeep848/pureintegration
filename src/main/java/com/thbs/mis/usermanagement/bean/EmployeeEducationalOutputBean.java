package com.thbs.mis.usermanagement.bean;

public class EmployeeEducationalOutputBean {

	private Integer empId;
	private Integer educationalLevelId;
	private String educationalLevel;
	private String nameOfSchool;
	private Integer streamId;
	private String stream;
	private String specilization;
	private Integer specilizationId;
	private Integer yearOfCompletion;
	private Integer percentage;

	
	public Integer getEmpId() {
		return empId;
	}
	
	
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getEducationalLevel() {
		return educationalLevel;
	}
	public void setEducationalLevel(String educationalLevel) {
		this.educationalLevel = educationalLevel;
	}
	public String getNameOfSchool() {
		return nameOfSchool;
	}
	public void setNameOfSchool(String nameOfSchool) {
		this.nameOfSchool = nameOfSchool;
	}
	public String getStream() {
		return stream;
	}
	public void setStream(String stream) {
		this.stream = stream;
	}
	public String getSpecilization() {
		return specilization;
	}
	public void setSpecilization(String specilization) {
		this.specilization = specilization;
	}
	public Integer getYearOfCompletion() {
		return yearOfCompletion;
	}
	public void setYearOfCompletion(Integer yearOfCompletion) {
		this.yearOfCompletion = yearOfCompletion;
	}
	public Integer getPercentage() {
		return percentage;
	}
	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}


	public Integer getEducationalLevelId() {
		return educationalLevelId;
	}


	public void setEducationalLevelId(Integer educationalLevelId) {
		this.educationalLevelId = educationalLevelId;
	}


	public Integer getStreamId() {
		return streamId;
	}


	public void setStreamId(Integer streamId) {
		this.streamId = streamId;
	}


	public Integer getSpecilizationId() {
		return specilizationId;
	}


	public void setSpecilizationId(Integer specilizationId) {
		this.specilizationId = specilizationId;
	}


	@Override
	public String toString() {
		return "EmployeeEducationalOutputBean [empId=" + empId
				+ ", educationalLevelId=" + educationalLevelId
				+ ", educationalLevel=" + educationalLevel + ", nameOfSchool="
				+ nameOfSchool + ", streamId=" + streamId + ", stream="
				+ stream + ", specilization=" + specilization
				+ ", specilizationId=" + specilizationId
				+ ", yearOfCompletion=" + yearOfCompletion + ", percentage="
				+ percentage + "]";
	}


	
}
