package com.thbs.mis.usermanagement.bean;

import java.util.Date;

public class DateRangeBean {

	private String fromDate;
	private String toDate;
	private Integer loggedInEmpId;

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Integer getLoggedInEmpId() {
		return loggedInEmpId;
	}

	public void setLoggedInEmpId(Integer loggedInEmpId) {
		this.loggedInEmpId = loggedInEmpId;
	}

	@Override
	public String toString() {
		return "DateRangeBean [fromDate=" + fromDate + ", toDate=" + toDate + ", loggedInEmpId=" + loggedInEmpId + "]";
	}

}
