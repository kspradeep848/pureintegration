package com.thbs.mis.usermanagement.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class EmployeeLocChangeOutputBean {

	private Integer empId;
	private String empName;
	private Byte statusId;
	private String status;
	private Short oldLocationId;
	private Short newLocationId;
	private String oldLocationName;
	private String newLocationName;
	private Date requestedDate;
	private Integer projCoordinatorId;
	private String projCoordinatorName;
	private Integer pkLocationId;

	public EmployeeLocChangeOutputBean(Integer empId, Integer pkLocationId, String empName,
			Byte statusId, String status, Short oldLocationId,
			Short newLocationId, String oldLocationName,
			String newLocationName, Date requestedDate,
			Integer projCoordinatorId, String projCoordinatorName) {
		super();
		this.empId = empId;
		this.pkLocationId = pkLocationId;
		this.empName = empName;
		this.statusId = statusId;
		this.status = status;
		this.oldLocationId = oldLocationId;
		this.newLocationId = newLocationId;
		this.oldLocationName = oldLocationName;
		this.newLocationName = newLocationName;
		this.requestedDate = requestedDate;
		this.projCoordinatorId = projCoordinatorId;
		this.projCoordinatorName = projCoordinatorName;
	}
	
	
	public Integer getPkLocationId() {
		return pkLocationId;
	}


	public void setPkLocationId(Integer pkLocationId) {
		this.pkLocationId = pkLocationId;
	}


	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	
	public Byte getStatusId() {
		return statusId;
	}
	public void setStatusId(Byte statusId) {
		this.statusId = statusId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Short getOldLocationId() {
		return oldLocationId;
	}
	public void setOldLocationId(Short oldLocationId) {
		this.oldLocationId = oldLocationId;
	}
	public Short getNewLocationId() {
		return newLocationId;
	}
	public void setNewLocationId(Short newLocationId) {
		this.newLocationId = newLocationId;
	}
	public String getOldLocationName() {
		return oldLocationName;
	}
	public void setOldLocationName(String oldLocationName) {
		this.oldLocationName = oldLocationName;
	}
	public String getNewLocationName() {
		return newLocationName;
	}
	public void setNewLocationName(String newLocationName) {
		this.newLocationName = newLocationName;
	}
	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getRequestedDate() {
		return requestedDate;
	}
	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}
	public Integer getProjCoordinatorId() {
		return projCoordinatorId;
	}
	public void setProjCoordinatorId(Integer projCoordinatorId) {
		this.projCoordinatorId = projCoordinatorId;
	}
	public String getProjCoordinatorName() {
		return projCoordinatorName;
	}
	public void setProjCoordinatorName(String projCoordinatorName) {
		this.projCoordinatorName = projCoordinatorName;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}


	@Override
	public String toString() {
		return "EmployeeLocChangeOutputBean [empId=" + empId + ", empName="
				+ empName + ", statusId=" + statusId + ", status=" + status
				+ ", oldLocationId=" + oldLocationId + ", newLocationId="
				+ newLocationId + ", oldLocationName=" + oldLocationName
				+ ", newLocationName=" + newLocationName + ", requestedDate="
				+ requestedDate + ", projCoordinatorId=" + projCoordinatorId
				+ ", projCoordinatorName=" + projCoordinatorName
				+ ", pkLocationId=" + pkLocationId + "]";
	}
	
	
	
	
}
