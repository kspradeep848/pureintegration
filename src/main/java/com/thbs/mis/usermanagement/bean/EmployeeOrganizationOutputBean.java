package com.thbs.mis.usermanagement.bean;

import java.util.Date;

public class EmployeeOrganizationOutputBean {

	private Integer empId;
	private Date dateOfJoining;
	private String emailAddress;
	private Short bootCampId;
	private String bootCampName;
	private Integer probationPeriod;
	private Byte employmentStatus;
	private String employmentStatusName;
	private String employmentType;
	private Byte employmentTypeId;
	private String employeeType;
	private Short organisation;
	private String orgName;
	private Short currentLocation;
	private String currentLocationName;
	private Short businessUnitId;
	private String businessUnit;
	private Short roleId;
	private String role;
	private Short designation;
	private String designationName;
	private Short level;
	private String levelName;
	private Integer domainManager;
	private String domainManagerName;
	private Integer reportingManager;
	private String reportingManagerName;
	private Date dateOfReporting;
	private Date endDateForFTE;
	private String onsiteOrOffshore;
	private Integer empLoc;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	
	

	public String getOnsiteOrOffshore() {
		return onsiteOrOffshore;
	}

	public void setOnsiteOrOffshore(String onsiteOrOffshore) {
		this.onsiteOrOffshore = onsiteOrOffshore;
	}

	public Integer getEmpLoc() {
		return empLoc;
	}

	public void setEmpLoc(Integer empLoc) {
		this.empLoc = empLoc;
	}

	public String getBootCampName() {
		return bootCampName;
	}

	public void setBootCampName(String bootCampName) {
		this.bootCampName = bootCampName;
	}

	public String getEmploymentStatusName() {
		return employmentStatusName;
	}

	public void setEmploymentStatusName(String employmentStatusName) {
		this.employmentStatusName = employmentStatusName;
	}

	public Byte getEmploymentTypeId() {
		return employmentTypeId;
	}

	public void setEmploymentTypeId(Byte employmentTypeId) {
		this.employmentTypeId = employmentTypeId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getCurrentLocationName() {
		return currentLocationName;
	}

	public void setCurrentLocationName(String currentLocationName) {
		this.currentLocationName = currentLocationName;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public String getDomainManagerName() {
		return domainManagerName;
	}

	public void setDomainManagerName(String domainManagerName) {
		this.domainManagerName = domainManagerName;
	}

	public String getReportingManagerName() {
		return reportingManagerName;
	}

	public void setReportingManagerName(String reportingManagerName) {
		this.reportingManagerName = reportingManagerName;
	}

	public Date getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Short getBootCampId() {
		return bootCampId;
	}

	public void setBootCampId(Short bootCampId) {
		this.bootCampId = bootCampId;
	}

	public Integer getProbationPeriod() {
		return probationPeriod;
	}

	public void setProbationPeriod(Integer probationPeriod) {
		this.probationPeriod = probationPeriod;
	}

	public Byte getEmploymentStatus() {
		return employmentStatus;
	}

	public void setEmploymentStatus(Byte employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

	public String getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	public String getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

	public Short getOrganisation() {
		return organisation;
	}

	public void setOrganisation(Short organisation) {
		this.organisation = organisation;
	}

	public Short getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(Short currentLocation) {
		this.currentLocation = currentLocation;
	}

	public Short getBusinessUnitId() {
		return businessUnitId;
	}

	public void setBusinessUnitId(Short businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

	public Short getRoleId() {
		return roleId;
	}

	public void setRoleId(Short roleId) {
		this.roleId = roleId;
	}

	public Short getDesignation() {
		return designation;
	}

	public void setDesignation(Short designation) {
		this.designation = designation;
	}

	public Short getLevel() {
		return level;
	}

	public void setLevel(Short level) {
		this.level = level;
	}

	public Integer getDomainManager() {
		return domainManager;
	}

	public void setDomainManager(Integer domainManager) {
		this.domainManager = domainManager;
	}

	public Integer getReportingManager() {
		return reportingManager;
	}

	public void setReportingManager(Integer reportingManager) {
		this.reportingManager = reportingManager;
	}

	public Date getDateOfReporting() {
		return dateOfReporting;
	}

	public void setDateOfReporting(Date dateOfReporting) {
		this.dateOfReporting = dateOfReporting;
	}

	public Date getEndDateForFTE() {
		return endDateForFTE;
	}

	public void setEndDateForFTE(Date endDateForFTE) {
		this.endDateForFTE = endDateForFTE;
	}

	@Override
	public String toString() {
		return "EmployeeOrganizationOutputBean [empId=" + empId
				+ ", dateOfJoining=" + dateOfJoining + ", emailAddress="
				+ emailAddress + ", bootCampId=" + bootCampId
				+ ", bootCampName=" + bootCampName + ", probationPeriod="
				+ probationPeriod + ", employmentStatus=" + employmentStatus
				+ ", employmentStatusName=" + employmentStatusName
				+ ", employmentType=" + employmentType + ", employmentTypeId="
				+ employmentTypeId + ", employeeType=" + employeeType
				+ ", organisation=" + organisation + ", orgName=" + orgName
				+ ", currentLocation=" + currentLocation
				+ ", currentLocationName=" + currentLocationName
				+ ", businessUnitId=" + businessUnitId + ", businessUnit="
				+ businessUnit + ", roleId=" + roleId + ", role=" + role
				+ ", designation=" + designation + ", designationName="
				+ designationName + ", level=" + level + ", levelName="
				+ levelName + ", domainManager=" + domainManager
				+ ", domainManagerName=" + domainManagerName
				+ ", reportingManager=" + reportingManager
				+ ", reportingManagerName=" + reportingManagerName
				+ ", dateOfReporting=" + dateOfReporting + ", endDateForFTE="
				+ endDateForFTE + ", onsiteOrOffshore=" + onsiteOrOffshore
				+ ", empLoc=" + empLoc + "]";
	}

	
}
