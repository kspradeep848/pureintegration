package com.thbs.mis.usermanagement.bean;

public class UpdateExtensionNumBean {

	private Integer empId;

	private String extensionNumber;

	private Integer loggedInEmpId;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getExtensionNumber() {
		return extensionNumber;
	}

	public void setExtensionNumber(String extensionNumber) {
		this.extensionNumber = extensionNumber;
	}

	public Integer getLoggedInEmpId() {
		return loggedInEmpId;
	}

	public void setLoggedInEmpId(Integer loggedInEmpId) {
		this.loggedInEmpId = loggedInEmpId;
	}

	@Override
	public String toString() {
		return "UpdateExtensionNumBean [empId=" + empId + ", extensionNumber=" + extensionNumber + ", loggedInEmpId="
				+ loggedInEmpId + "]";
	}

}
