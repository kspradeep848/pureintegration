package com.thbs.mis.usermanagement.bean;

import java.util.Date;

public class OrganizationDetailBean {

	private Integer empId;
	private Date dateOfJoining;
	private String emailAddress;
	private Short bootCampId;
	private Integer probationPeriod;
	private Byte employmentStatus;
	private String employmentType;
	private String employeeType;
	private Short organisation;
	private Short currentLocation;
	private Short businessUnitId;
	private Short roleId;
	private Short designation;
	private Short level;
	private Integer domainManager;
	private Integer reportingManager;
	private Date dateOfReporting;
	private Date endDateForFTE;
	private String onsiteOrOffshore;
	private Integer empLoc;
	private String tupeStatus;
	private String tupeOrganisation;

	private Integer loggedInEmpId;

	public Integer getLoggedInEmpId() {
		return loggedInEmpId;
	}

	public void setLoggedInEmpId(Integer loggedInEmpId) {
		this.loggedInEmpId = loggedInEmpId;
	}

	public String getOnsiteOrOffshore() {
		return onsiteOrOffshore;
	}

	public void setOnsiteOrOffshore(String onsiteOrOffshore) {
		this.onsiteOrOffshore = onsiteOrOffshore;
	}

	public Integer getEmpLoc() {
		return empLoc;
	}

	public void setEmpLoc(Integer empLoc) {
		this.empLoc = empLoc;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Date getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Short getBootCampId() {
		return bootCampId;
	}

	public void setBootCampId(Short bootCampId) {
		this.bootCampId = bootCampId;
	}

	public Integer getProbationPeriod() {
		return probationPeriod;
	}

	public void setProbationPeriod(Integer probationPeriod) {
		this.probationPeriod = probationPeriod;
	}

	public Byte getEmploymentStatus() {
		return employmentStatus;
	}

	public void setEmploymentStatus(Byte employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

	public String getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	public String getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

	public Short getOrganisation() {
		return organisation;
	}

	public void setOrganisation(Short organisation) {
		this.organisation = organisation;
	}

	public Short getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(Short currentLocation) {
		this.currentLocation = currentLocation;
	}

	public Short getBusinessUnitId() {
		return businessUnitId;
	}

	public void setBusinessUnitId(Short businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

	public Short getRoleId() {
		return roleId;
	}

	public void setRoleId(Short roleId) {
		this.roleId = roleId;
	}

	public Short getDesignation() {
		return designation;
	}

	public void setDesignation(Short designation) {
		this.designation = designation;
	}

	public Short getLevel() {
		return level;
	}

	public void setLevel(Short level) {
		this.level = level;
	}

	public Integer getDomainManager() {
		return domainManager;
	}

	public void setDomainManager(Integer domainManager) {
		this.domainManager = domainManager;
	}

	public Integer getReportingManager() {
		return reportingManager;
	}

	public void setReportingManager(Integer reportingManager) {
		this.reportingManager = reportingManager;
	}

	public Date getDateOfReporting() {
		return dateOfReporting;
	}

	public void setDateOfReporting(Date dateOfReporting) {
		this.dateOfReporting = dateOfReporting;
	}

	public Date getEndDateForFTE() {
		return endDateForFTE;
	}

	public void setEndDateForFTE(Date endDateForFTE) {
		this.endDateForFTE = endDateForFTE;
	}

	
	public String getTupeStatus() {
		return tupeStatus;
	}

	public void setTupeStatus(String tupeStatus) {
		this.tupeStatus = tupeStatus;
	}

	public String getTupeOrganisation() {
		return tupeOrganisation;
	}

	public void setTupeOrganisation(String tupeOrganisation) {
		this.tupeOrganisation = tupeOrganisation;
	}

	@Override
	public String toString() {
		return "OrganizationDetailBean [empId=" + empId + ", dateOfJoining="
				+ dateOfJoining + ", emailAddress=" + emailAddress
				+ ", bootCampId=" + bootCampId + ", probationPeriod="
				+ probationPeriod + ", employmentStatus=" + employmentStatus
				+ ", employmentType=" + employmentType + ", employeeType="
				+ employeeType + ", organisation=" + organisation
				+ ", currentLocation=" + currentLocation + ", businessUnitId="
				+ businessUnitId + ", roleId=" + roleId + ", designation="
				+ designation + ", level=" + level + ", domainManager="
				+ domainManager + ", reportingManager=" + reportingManager
				+ ", dateOfReporting=" + dateOfReporting + ", endDateForFTE="
				+ endDateForFTE + ", onsiteOrOffshore=" + onsiteOrOffshore
				+ ", empLoc=" + empLoc + ", tupeStatus=" + tupeStatus
				+ ", tupeOrganisation=" + tupeOrganisation + ", loggedInEmpId="
				+ loggedInEmpId + "]";
	}


}
