package com.thbs.mis.usermanagement.bean;

import java.util.Date;

public class CreateEmpProfDetailBean 
{
	private Integer empId;
	private String employerName;
	private String positionHeld;
	private Date durationFromDate;
	private Date durationToDate;
	private String reasonForLeaving;
	private Integer loggedInEmpID;
	
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getEmployerName() {
		return employerName;
	}
	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}
	public String getPositionHeld() {
		return positionHeld;
	}
	public void setPositionHeld(String positionHeld) {
		this.positionHeld = positionHeld;
	}
	public Date getDurationFromDate() {
		return durationFromDate;
	}
	public void setDurationFromDate(Date durationFromDate) {
		this.durationFromDate = durationFromDate;
	}
	public Date getDurationToDate() {
		return durationToDate;
	}
	public void setDurationToDate(Date durationToDate) {
		this.durationToDate = durationToDate;
	}
	public String getReasonForLeaving() {
		return reasonForLeaving;
	}
	public void setReasonForLeaving(String reasonForLeaving) {
		this.reasonForLeaving = reasonForLeaving;
	}
	public Integer getLoggedInEmpID() {
		return loggedInEmpID;
	}
	public void setLoggedInEmpID(Integer loggedInEmpID) {
		this.loggedInEmpID = loggedInEmpID;
	}
	@Override
	public String toString() {
		return "CreateEmpProfDetailBean [empId=" + empId + ", employerName=" + employerName + ", positionHeld="
				+ positionHeld + ", durationFromDate=" + durationFromDate + ", durationToDate=" + durationToDate
				+ ", reasonForLeaving=" + reasonForLeaving + ", loggedInEmpID=" + loggedInEmpID + "]";
	}
	
}
