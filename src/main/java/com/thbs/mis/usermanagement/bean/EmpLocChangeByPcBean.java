package com.thbs.mis.usermanagement.bean;

public class EmpLocChangeByPcBean {

	private Integer empId;
	private Integer loggedEmpId;
	private Short currentLocation;
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public Integer getLoggedEmpId() {
		return loggedEmpId;
	}
	public void setLoggedEmpId(Integer loggedEmpId) {
		this.loggedEmpId = loggedEmpId;
	}
	public Short getCurrentLocation() {
		return currentLocation;
	}
	public void setCurrentLocation(Short currentLocation) {
		this.currentLocation = currentLocation;
	}
	@Override
	public String toString() {
		return "EmpLocChangeByPcBean [empId=" + empId + ", loggedEmpId="
				+ loggedEmpId + ", currentLocation=" + currentLocation + "]";
	}
	
	
}
