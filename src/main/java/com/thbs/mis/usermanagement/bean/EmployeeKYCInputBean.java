package com.thbs.mis.usermanagement.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class EmployeeKYCInputBean {

	private String empNameAsPerAadharCard;
	private String empNameAsPerPanCard;
	private String empNameAsPerVoterId;
	private String panCardNumber;
	private String passportNumber;
	private String hasUanFlag;
	private String voterId;
	private String aadharNo;
	private String empNameAsPerPassPort;
	private Date dateOfIssue;
	private String placeOfIssue;
	private Date dateOfExpiry;
	
	
	public String getPanCardNumber() {
		return panCardNumber;
	}
	public void setPanCardNumber(String panCardNumber) {
		this.panCardNumber = panCardNumber;
	}
	public String getEmpNameAsPerAadharCard() {
		return empNameAsPerAadharCard;
	}
	public void setEmpNameAsPerAadharCard(String empNameAsPerAadharCard) {
		this.empNameAsPerAadharCard = empNameAsPerAadharCard;
	}
	public String getEmpNameAsPerPanCard() {
		return empNameAsPerPanCard;
	}
	public void setEmpNameAsPerPanCard(String empNameAsPerPanCard) {
		this.empNameAsPerPanCard = empNameAsPerPanCard;
	}
	public String getEmpNameAsPerVoterId() {
		return empNameAsPerVoterId;
	}
	public void setEmpNameAsPerVoterId(String empNameAsPerVoterId) {
		this.empNameAsPerVoterId = empNameAsPerVoterId;
	}
	
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	public String getHasUanFlag() {
		return hasUanFlag;
	}
	public void setHasUanFlag(String hasUanFlag) {
		this.hasUanFlag = hasUanFlag;
	}
	public String getVoterId() {
		return voterId;
	}
	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}
	public String getAadharNo() {
		return aadharNo;
	}
	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}
	public String getEmpNameAsPerPassPort() {
		return empNameAsPerPassPort;
	}
	public void setEmpNameAsPerPassPort(String empNameAsPerPassPort) {
		this.empNameAsPerPassPort = empNameAsPerPassPort;
	}
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getDateOfIssue() {
		return dateOfIssue;
	}
	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}
	public String getPlaceOfIssue() {
		return placeOfIssue;
	}
	public void setPlaceOfIssue(String placeOfIssue) {
		this.placeOfIssue = placeOfIssue;
	}
	@JsonSerialize(using=CustomTimeSerializer.class)
	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}
	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}
	@Override
	public String toString() {
		return "EmployeeKYCInputBean [empNameAsPerAadharCard="
				+ empNameAsPerAadharCard + ", empNameAsPerPanCard="
				+ empNameAsPerPanCard + ", empNameAsPerVoterId="
				+ empNameAsPerVoterId + ", panCardNumber=" + panCardNumber
				+ ", passportNumber=" + passportNumber + ", hasUanFlag="
				+ hasUanFlag + ", voterId=" + voterId + ", aadharNo="
				+ aadharNo + ", empNameAsPerPassPort=" + empNameAsPerPassPort
				+ ", dateOfIssue=" + dateOfIssue + ", placeOfIssue="
				+ placeOfIssue + ", dateOfExpiry=" + dateOfExpiry + "]";
	}
	
	
}
