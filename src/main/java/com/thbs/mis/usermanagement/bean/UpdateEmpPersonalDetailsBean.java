package com.thbs.mis.usermanagement.bean;

public class UpdateEmpPersonalDetailsBean {

	private Integer empId;
	private String mobileNumber;
	private String homeNumber;
	private String personalEmail;
	private String maritialStatus;
	private String presentAddress;
	private String permanentAddress;
	private String passportNumber;
	private String placeOfIssue;
	private String expiryDate;
	private String dateOfIssue;
	private String empVoterIdNo;
	private String empNameAsPerVoterId;
	private Integer loggedInEmpId;
	private String empPanNo;
	private String empNameAsPerPanCard;
	private String empNameAsPerAadharCard;
	private String adharNo;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getHomeNumber() {
		return homeNumber;
	}

	public void setHomeNumber(String homeNumber) {
		this.homeNumber = homeNumber;
	}

	public String getPersonalEmail() {
		return personalEmail;
	}

	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	public String getMaritialStatus() {
		return maritialStatus;
	}

	public void setMaritialStatus(String maritialStatus) {
		this.maritialStatus = maritialStatus;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getPlaceOfIssue() {
		return placeOfIssue;
	}

	public void setPlaceOfIssue(String placeOfIssue) {
		this.placeOfIssue = placeOfIssue;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getEmpVoterIdNo() {
		return empVoterIdNo;
	}

	public void setEmpVoterIdNo(String empVoterIdNo) {
		this.empVoterIdNo = empVoterIdNo;
	}

	public String getEmpNameAsPerVoterId() {
		return empNameAsPerVoterId;
	}

	public void setEmpNameAsPerVoterId(String empNameAsPerVoterId) {
		this.empNameAsPerVoterId = empNameAsPerVoterId;
	}

	public Integer getLoggedInEmpId() {
		return loggedInEmpId;
	}

	public void setLoggedInEmpId(Integer loggedInEmpId) {
		this.loggedInEmpId = loggedInEmpId;
	}

	public String getEmpPanNo() {
		return empPanNo;
	}

	public void setEmpPanNo(String empPanNo) {
		this.empPanNo = empPanNo;
	}

	public String getEmpNameAsPerPanCard() {
		return empNameAsPerPanCard;
	}

	public void setEmpNameAsPerPanCard(String empNameAsPerPanCard) {
		this.empNameAsPerPanCard = empNameAsPerPanCard;
	}

	public String getEmpNameAsPerAadharCard() {
		return empNameAsPerAadharCard;
	}

	public void setEmpNameAsPerAadharCard(String empNameAsPerAadharCard) {
		this.empNameAsPerAadharCard = empNameAsPerAadharCard;
	}

	public String getAdharNo() {
		return adharNo;
	}

	public void setAdharNo(String adharNo) {
		this.adharNo = adharNo;
	}

	@Override
	public String toString() {
		return "UpdateEmpPersonalDetailsBean [empId=" + empId + ", mobileNumber=" + mobileNumber + ", homeNumber="
				+ homeNumber + ", personalEmail=" + personalEmail + ", maritialStatus=" + maritialStatus
				+ ", presentAddress=" + presentAddress + ", permanentAddress=" + permanentAddress + ", passportNumber="
				+ passportNumber + ", placeOfIssue=" + placeOfIssue + ", expiryDate=" + expiryDate + ", dateOfIssue="
				+ dateOfIssue + ", empVoterIdNo=" + empVoterIdNo + ", empNameAsPerVoterId=" + empNameAsPerVoterId
				+ ", loggedInEmpId=" + loggedInEmpId + ", empPanNo=" + empPanNo + ", empNameAsPerPanCard="
				+ empNameAsPerPanCard + ", empNameAsPerAadharCard=" + empNameAsPerAadharCard + ", adharNo=" + adharNo
				+ "]";
	}

}
