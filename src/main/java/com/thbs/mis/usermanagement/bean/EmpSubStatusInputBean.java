package com.thbs.mis.usermanagement.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class EmpSubStatusInputBean {

	@NumberFormat(style = Style.NUMBER, pattern = "empId should be numeric value")
	@NotNull(message = "empId can not be null")
	@Min(1)
	private Integer empId;
	
	@NotNull(message = "empSubStatus can not be null")
	@Min(1)
	private Byte empSubStatus;
	
	@NotNull(message = "loggedInUserId can not be null")
	@Min(1)
	private Integer loggedInUserId;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Byte getEmpSubStatus() {
		return empSubStatus;
	}

	public void setEmpSubStatus(Byte empSubStatus) {
		this.empSubStatus = empSubStatus;
	}

	public Integer getLoggedInUserId() {
		return loggedInUserId;
	}

	public void setLoggedInUserId(Integer loggedInUserId) {
		this.loggedInUserId = loggedInUserId;
	}

	@Override
	public String toString() {
		return "EmpSubStatusInputBean [empId=" + empId + ", empSubStatus="
				+ empSubStatus + ", loggedInUserId=" + loggedInUserId + "]";
	}

}
