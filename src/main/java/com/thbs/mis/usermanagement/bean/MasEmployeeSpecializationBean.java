package com.thbs.mis.usermanagement.bean;

public class MasEmployeeSpecializationBean {

	private Integer specializationId;
	private String specializationName;
	public Integer getSpecializationId() {
		return specializationId;
	}
	public void setSpecializationId(Integer specializationId) {
		this.specializationId = specializationId;
	}
	public String getSpecializationName() {
		return specializationName;
	}
	public void setSpecializationName(String specializationName) {
		this.specializationName = specializationName;
	}
	@Override
	public String toString() {
		return "MasEmployeeSpecializationBean [specializationId="
				+ specializationId + ", specializationName="
				+ specializationName + "]";
	}
	
	
}
