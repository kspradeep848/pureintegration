package com.thbs.mis.usermanagement.bean;

public class MasEmployeeStreamBean {

	private Integer streamId;
	private String streamName;
	public Integer getStreamId() {
		return streamId;
	}
	public void setStreamId(Integer streamId) {
		this.streamId = streamId;
	}
	public String getStreamName() {
		return streamName;
	}
	public void setStreamName(String streamName) {
		this.streamName = streamName;
	}
	@Override
	public String toString() {
		return "MasStreamBean [streamId=" + streamId + ", streamName="
				+ streamName + "]";
	}
	
	
}
