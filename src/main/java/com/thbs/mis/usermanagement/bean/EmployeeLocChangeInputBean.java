package com.thbs.mis.usermanagement.bean;

import java.util.Date;

public class EmployeeLocChangeInputBean {

	private Integer empId;
	private Byte status;
	private Short location;
	private Date fromDate;
	private Date toDate;
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	public Short getLocation() {
		return location;
	}
	public void setLocation(Short location) {
		this.location = location;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	@Override
	public String toString() {
		return "EmployeeLocChangeInputBean [empId=" + empId + ", status="
				+ status + ", location=" + location + ", fromDate=" + fromDate
				+ ", toDate=" + toDate + "]";
	}
	
	
	
}
