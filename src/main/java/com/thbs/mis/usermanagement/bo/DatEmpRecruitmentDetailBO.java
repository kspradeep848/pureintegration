package com.thbs.mis.usermanagement.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;

import java.util.Date;


/**
 * The persistent class for the dat_emp_recruitment_details database table.
 * 
 */
@Entity
@Table(name="dat_emp_recruitment_details")
@NamedQuery(name="DatEmpRecruitmentDetailBO.findAll", query="SELECT d FROM DatEmpRecruitmentDetailBO d")
public class DatEmpRecruitmentDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_recruitment_id")
	private Integer pkRecruitmentId;

	@Column(name="created_by")
	private Integer createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="emp_sub_source_id")
	private Integer empSubSourceId;

	@Column(name="fk_emp_id")
	private Integer fkEmpId;

	@Column(name="fk_recruiter_id")
	private Integer fkRecruiterId;

	@Column(name="managerial_panel")
	private String managerialPanel;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="modified_date")
	private Date modifiedDate;

	@Column(name="referral_emp_id")
	private Integer referralEmpId;

	@Column(name="technical_panel")
	private String technicalPanel;

	@Column(name="emp_source_id")
	private Integer fkEmpSourceId;
	
	@OneToOne
	@JoinColumn(name = "created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailCreatedBy;
	
	@OneToOne
	@JoinColumn(name = "emp_source_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmploymentSourceBO masEmploymentSourceBO;
	
	@OneToOne
	@JoinColumn(name = "modified_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailUpdatedBy;
	
	@OneToOne
	@JoinColumn(name = "fk_recruiter_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailRecruiterId;
	
	@OneToOne
	@JoinColumn(name = "referral_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailReferralId;

	public DatEmpRecruitmentDetailBO() {
	}

	public Integer getPkRecruitmentId() {
		return this.pkRecruitmentId;
	}

	public void setPkRecruitmentId(Integer pkRecruitmentId) {
		this.pkRecruitmentId = pkRecruitmentId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getEmpSubSourceId() {
		return this.empSubSourceId;
	}

	public void setEmpSubSourceId(Integer empSubSourceId) {
		this.empSubSourceId = empSubSourceId;
	}

	public Integer getFkEmpId() {
		return this.fkEmpId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public Integer getFkRecruiterId() {
		return this.fkRecruiterId;
	}

	public void setFkRecruiterId(Integer fkRecruiterId) {
		this.fkRecruiterId = fkRecruiterId;
	}

	public String getManagerialPanel() {
		return this.managerialPanel;
	}

	public void setManagerialPanel(String managerialPanel) {
		this.managerialPanel = managerialPanel;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getReferralEmpId() {
		return this.referralEmpId;
	}

	public void setReferralEmpId(Integer referralEmpId) {
		this.referralEmpId = referralEmpId;
	}

	public String getTechnicalPanel() {
		return this.technicalPanel;
	}

	public void setTechnicalPanel(String technicalPanel) {
		this.technicalPanel = technicalPanel;
	}

	public Integer getFkEmpSourceId() {
		return fkEmpSourceId;
	}

	public void setFkEmpSourceId(Integer fkEmpSourceId) {
		this.fkEmpSourceId = fkEmpSourceId;
	}

	public DatEmpDetailBO getDatEmpDetailCreatedBy() {
		return datEmpDetailCreatedBy;
	}

	public void setDatEmpDetailCreatedBy(DatEmpDetailBO datEmpDetailCreatedBy) {
		this.datEmpDetailCreatedBy = datEmpDetailCreatedBy;
	}

	public MasEmploymentSourceBO getMasEmploymentSourceBO() {
		return masEmploymentSourceBO;
	}

	public void setMasEmploymentSourceBO(MasEmploymentSourceBO masEmploymentSourceBO) {
		this.masEmploymentSourceBO = masEmploymentSourceBO;
	}

	public DatEmpDetailBO getDatEmpDetailUpdatedBy() {
		return datEmpDetailUpdatedBy;
	}

	public void setDatEmpDetailUpdatedBy(DatEmpDetailBO datEmpDetailUpdatedBy) {
		this.datEmpDetailUpdatedBy = datEmpDetailUpdatedBy;
	}

	public DatEmpDetailBO getDatEmpDetailRecruiterId() {
		return datEmpDetailRecruiterId;
	}

	public void setDatEmpDetailRecruiterId(DatEmpDetailBO datEmpDetailRecruiterId) {
		this.datEmpDetailRecruiterId = datEmpDetailRecruiterId;
	}

	public DatEmpDetailBO getDatEmpDetailReferralId() {
		return datEmpDetailReferralId;
	}

	public void setDatEmpDetailReferralId(DatEmpDetailBO datEmpDetailReferralId) {
		this.datEmpDetailReferralId = datEmpDetailReferralId;
	}

	@Override
	public String toString() {
		return "DatEmpRecruitmentDetailBO [pkRecruitmentId=" + pkRecruitmentId
				+ ", createdBy=" + createdBy + ", createdDate=" + createdDate
				+ ", empSubSourceId=" + empSubSourceId + ", fkEmpId=" + fkEmpId
				+ ", fkRecruiterId=" + fkRecruiterId + ", managerialPanel="
				+ managerialPanel + ", modifiedBy=" + modifiedBy
				+ ", modifiedDate=" + modifiedDate + ", referralEmpId="
				+ referralEmpId + ", technicalPanel=" + technicalPanel
				+ ", fkEmpSourceId=" + fkEmpSourceId
				+ ", datEmpDetailCreatedBy=" + datEmpDetailCreatedBy
				+ ", masEmploymentSourceBO=" + masEmploymentSourceBO
				+ ", datEmpDetailUpdatedBy=" + datEmpDetailUpdatedBy
				+ ", datEmpDetailRecruiterId=" + datEmpDetailRecruiterId
				+ ", datEmpDetailReferralId=" + datEmpDetailReferralId + "]";
	}

	

	
}