package com.thbs.mis.usermanagement.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.thbs.mis.common.bo.MasCountryBO;
import com.thbs.mis.common.bo.MasOrganizationBO;


/**
 * The persistent class for the map_mas_org_country database table.
 * 
 */
@Entity
@Table(name="map_mas_org_country")
@NamedQuery(name="MapMasOrgCountryBO.findAll", query="SELECT m FROM MapMasOrgCountryBO m")
public class MapMasOrgCountryBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_id")
	private Integer pkId;

	@Column(name="fk_country_id")
	private Short fkCountryId;

	@Column(name="fk_org_id")
	private Short fkOrgId;

	public MapMasOrgCountryBO() {
	}

	
	@OneToOne
	@JoinColumn(name = "fk_country_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasCountryBO masCountryBO;
	
	@OneToOne
	@JoinColumn(name = "fk_org_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasOrganizationBO masOrgBO;
	
	
	public Integer getPkId() {
		return this.pkId;
	}

	public void setPkId(Integer pkId) {
		this.pkId = pkId;
	}

	public Short getFkCountryId() {
		return this.fkCountryId;
	}

	public void setFkCountryId(Short fkCountryId) {
		this.fkCountryId = fkCountryId;
	}

	public Short getFkOrgId() {
		return this.fkOrgId;
	}

	public void setFkOrgId(Short fkOrgId) {
		this.fkOrgId = fkOrgId;
	}
	
	

	public MasCountryBO getMasCountryBO() {
		return masCountryBO;
	}

	public void setMasCountryBO(MasCountryBO masCountryBO) {
		this.masCountryBO = masCountryBO;
	}

	public MasOrganizationBO getMasOrgBO() {
		return masOrgBO;
	}

	public void setMasOrgBO(MasOrganizationBO masOrgBO) {
		this.masOrgBO = masOrgBO;
	}

	@Override
	public String toString() {
		return "MapMasOrgCountryBO [pkId=" + pkId + ", fkCountryId="
				+ fkCountryId + ", fkOrgId=" + fkOrgId + ", masCountryBO="
				+ masCountryBO + ", masOrgBO=" + masOrgBO + "]";
	}

	
}