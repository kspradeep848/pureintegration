package com.thbs.mis.usermanagement.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mas_social_media database table.
 * 
 */
@Entity
@Table(name="mas_social_media")
@NamedQuery(name="MasSocialMediaBO.findAll", query="SELECT m FROM MasSocialMediaBO m")
public class MasSocialMediaBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_id")
	private int pkId;

	@Column(name="key_name")
	private String keyName;

	@Column(name="social_media_name")
	private String socialMediaName;

	public MasSocialMediaBO() {
	}

	public int getPkId() {
		return this.pkId;
	}

	public void setPkId(int pkId) {
		this.pkId = pkId;
	}

	public String getKeyName() {
		return this.keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public String getSocialMediaName() {
		return this.socialMediaName;
	}

	public void setSocialMediaName(String socialMediaName) {
		this.socialMediaName = socialMediaName;
	}

	@Override
	public String toString() {
		return "MasSocialMediaBO [pkId=" + pkId + ", keyName=" + keyName
				+ ", socialMediaName=" + socialMediaName + "]";
	}

	
}