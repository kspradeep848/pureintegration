package com.thbs.mis.usermanagement.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.thbs.mis.common.bo.DatEmpDetailBO;

import java.util.Date;


/**
 * The persistent class for the dat_emp_employment_details database table.
 * 
 */
@Entity
@Table(name="dat_emp_employment_details")
@NamedQuery(name="DatEmpEmploymentDetailBO.findAll", query="SELECT d FROM DatEmpEmploymentDetailBO d")
public class DatEmpEmploymentDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="pk_employment_id")
	private Integer pkEmploymentId;

	@Column(name="created_by")
	private Integer createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="emp_employer_name")
	private String empEmployerName;

	@Column(name="emp_employment_desg")
	private String empEmploymentDesg;

	@Column(name="employer_sequence_number")
	private Integer employerSequenceNumber;

	@Temporal(TemporalType.DATE)
	@Column(name="end_date")
	private Date endDate;

	@Column(name="fk_emp_id")
	private Integer fkEmpId;

	@Column(name="reason_for_leaving")
	private String reasonForLeaving;

	@Temporal(TemporalType.DATE)
	@Column(name="start_date")
	private Date startDate;

	@Column(name="updated_by")
	private Integer updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	@OneToOne
	@JoinColumn(name = "created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailCreatedBy;
	
	@OneToOne
	@JoinColumn(name = "updated_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailUpdatedBy;
	

	@OneToOne
	@JoinColumn(name = "fk_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailEmpId;
	
	public DatEmpEmploymentDetailBO() {
	}

	

	public Integer getPkEmploymentId() {
		return pkEmploymentId;
	}



	public void setPkEmploymentId(Integer pkEmploymentId) {
		this.pkEmploymentId = pkEmploymentId;
	}



	public Integer getCreatedBy() {
		return createdBy;
	}



	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}



	public Date getCreatedDate() {
		return createdDate;
	}



	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}



	public String getEmpEmployerName() {
		return empEmployerName;
	}



	public void setEmpEmployerName(String empEmployerName) {
		this.empEmployerName = empEmployerName;
	}



	public String getEmpEmploymentDesg() {
		return empEmploymentDesg;
	}



	public void setEmpEmploymentDesg(String empEmploymentDesg) {
		this.empEmploymentDesg = empEmploymentDesg;
	}



	public Integer getEmployerSequenceNumber() {
		return employerSequenceNumber;
	}



	public void setEmployerSequenceNumber(Integer employerSequenceNumber) {
		this.employerSequenceNumber = employerSequenceNumber;
	}



	public Date getEndDate() {
		return endDate;
	}



	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}



	public Integer getFkEmpId() {
		return fkEmpId;
	}



	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}



	public String getReasonForLeaving() {
		return reasonForLeaving;
	}



	public void setReasonForLeaving(String reasonForLeaving) {
		this.reasonForLeaving = reasonForLeaving;
	}



	public Date getStartDate() {
		return startDate;
	}



	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}



	public Integer getUpdatedBy() {
		return updatedBy;
	}



	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}



	public Date getUpdatedDate() {
		return updatedDate;
	}



	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}



	public DatEmpDetailBO getDatEmpDetailCreatedBy() {
		return datEmpDetailCreatedBy;
	}



	public void setDatEmpDetailCreatedBy(DatEmpDetailBO datEmpDetailCreatedBy) {
		this.datEmpDetailCreatedBy = datEmpDetailCreatedBy;
	}



	public DatEmpDetailBO getDatEmpDetailUpdatedBy() {
		return datEmpDetailUpdatedBy;
	}



	public void setDatEmpDetailUpdatedBy(DatEmpDetailBO datEmpDetailUpdatedBy) {
		this.datEmpDetailUpdatedBy = datEmpDetailUpdatedBy;
	}



	public DatEmpDetailBO getDatEmpDetailEmpId() {
		return datEmpDetailEmpId;
	}



	public void setDatEmpDetailEmpId(DatEmpDetailBO datEmpDetailEmpId) {
		this.datEmpDetailEmpId = datEmpDetailEmpId;
	}



	@Override
	public String toString() {
		return "DatEmpEmploymentDetailBO [pkEmploymentId=" + pkEmploymentId
				+ ", createdBy=" + createdBy + ", createdDate=" + createdDate
				+ ", empEmployerName=" + empEmployerName
				+ ", empEmploymentDesg=" + empEmploymentDesg
				+ ", employerSequenceNumber=" + employerSequenceNumber
				+ ", endDate=" + endDate + ", fkEmpId=" + fkEmpId
				+ ", reasonForLeaving=" + reasonForLeaving + ", startDate="
				+ startDate + ", updatedBy=" + updatedBy + ", updatedDate="
				+ updatedDate + ", datEmpDetailCreatedBy="
				+ datEmpDetailCreatedBy + ", datEmpDetailUpdatedBy="
				+ datEmpDetailUpdatedBy + ", datEmpDetailEmpId="
				+ datEmpDetailEmpId + "]";
	}

	

	
}