package com.thbs.mis.usermanagement.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the mas_emp_specialization database table.
 * 
 */
@Entity
@Table(name="mas_emp_specialization")
@NamedQuery(name="MasEmpSpecializationBO.findAll", query="SELECT m FROM MasEmpSpecializationBO m")
public class MasEmpSpecializationBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_specialization_id")
	private int pkSpecializationId;

	@Column(name="specialization_name")
	private String specializationName;


	public MasEmpSpecializationBO() {
	}

	public int getPkSpecializationId() {
		return this.pkSpecializationId;
	}

	public void setPkSpecializationId(int pkSpecializationId) {
		this.pkSpecializationId = pkSpecializationId;
	}

	public String getSpecializationName() {
		return this.specializationName;
	}

	public void setSpecializationName(String specializationName) {
		this.specializationName = specializationName;
	}

	@Override
	public String toString() {
		return "MasEmpSpecializationBO [pkSpecializationId="
				+ pkSpecializationId + ", specializationName="
				+ specializationName + "]";
	}

	
	
}