package com.thbs.mis.usermanagement.bo;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.thbs.mis.common.bo.DatEmpDetailBO;

import java.util.Date;

/**
 * The persistent class for the dat_emp_education_details database table.
 * 
 */
@Entity
@Table(name = "dat_emp_education_details")
@NamedQuery(name = "DatEmpEducationDetailBO.findAll", query = "SELECT d FROM DatEmpEducationDetailBO d")
public class DatEmpEducationDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_edu_id")
	private Integer pkEduId;

	@Column(name = "created_by")
	private Integer createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "edu_level")
	private Integer eduLevel;

	@Column(name = "fk_emp_id")
	private Integer fkEmpId;

	@Column(name = "name_of_university")
	private String nameOfUniversity;

	@Column(name = "percentage")
	private Integer percentage;

	@Column(name = "updated_by")
	private Integer updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date")
	private Date updatedDate;

	@Column(name = "year_of_completion")
	private Integer yearOfCompletion;

	@Column(name = "fk_specialization_id")
	private Integer fkSpecializationId;

	@Column(name = "fk_stream_id")
	private Integer fkStreamId;

	@OneToOne
	@JoinColumn(name = "fk_specialization_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpSpecializationBO masEmpSpecialization;

	@OneToOne
	@JoinColumn(name = "fk_stream_id", unique = true, nullable = true, insertable = false, updatable = false)
	private MasEmpStreamBO masEmpStream;

	@OneToOne
	@JoinColumn(name = "updated_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailModifiedBy;

	@OneToOne
	@JoinColumn(name = "created_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailCreatedBy;

	@OneToOne
	@JoinColumn(name = "fk_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailEmpId;

	public DatEmpEducationDetailBO() {
	}

	public Integer getPkEduId() {
		return pkEduId;
	}

	public void setPkEduId(Integer pkEduId) {
		this.pkEduId = pkEduId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getEduLevel() {
		return eduLevel;
	}

	public void setEduLevel(Integer eduLevel) {
		this.eduLevel = eduLevel;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public String getNameOfUniversity() {
		return nameOfUniversity;
	}

	public void setNameOfUniversity(String nameOfUniversity) {
		this.nameOfUniversity = nameOfUniversity;
	}

	public Integer getPercentage() {
		return percentage;
	}

	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Integer getYearOfCompletion() {
		return yearOfCompletion;
	}

	public void setYearOfCompletion(Integer yearOfCompletion) {
		this.yearOfCompletion = yearOfCompletion;
	}

	public Integer getFkSpecializationId() {
		return fkSpecializationId;
	}

	public void setFkSpecializationId(Integer fkSpecializationId) {
		this.fkSpecializationId = fkSpecializationId;
	}

	public Integer getFkStreamId() {
		return fkStreamId;
	}

	public void setFkStreamId(Integer fkStreamId) {
		this.fkStreamId = fkStreamId;
	}

	public MasEmpSpecializationBO getMasEmpSpecialization() {
		return masEmpSpecialization;
	}

	public void setMasEmpSpecialization(
			MasEmpSpecializationBO masEmpSpecialization) {
		this.masEmpSpecialization = masEmpSpecialization;
	}

	public MasEmpStreamBO getMasEmpStream() {
		return masEmpStream;
	}

	public void setMasEmpStream(MasEmpStreamBO masEmpStream) {
		this.masEmpStream = masEmpStream;
	}

	public DatEmpDetailBO getDatEmpDetailModifiedBy() {
		return datEmpDetailModifiedBy;
	}

	public void setDatEmpDetailModifiedBy(DatEmpDetailBO datEmpDetailModifiedBy) {
		this.datEmpDetailModifiedBy = datEmpDetailModifiedBy;
	}

	public DatEmpDetailBO getDatEmpDetailCreatedBy() {
		return datEmpDetailCreatedBy;
	}

	public void setDatEmpDetailCreatedBy(DatEmpDetailBO datEmpDetailCreatedBy) {
		this.datEmpDetailCreatedBy = datEmpDetailCreatedBy;
	}

	public DatEmpDetailBO getDatEmpDetailEmpId() {
		return datEmpDetailEmpId;
	}

	public void setDatEmpDetailEmpId(DatEmpDetailBO datEmpDetailEmpId) {
		this.datEmpDetailEmpId = datEmpDetailEmpId;
	}

	@Override
	public String toString() {
		return "DatEmpEducationDetailBO [pkEduId=" + pkEduId + ", createdBy="
				+ createdBy + ", createdDate=" + createdDate + ", eduLevel="
				+ eduLevel + ", fkEmpId=" + fkEmpId + ", nameOfUniversity="
				+ nameOfUniversity + ", percentage=" + percentage
				+ ", updatedBy=" + updatedBy + ", updatedDate=" + updatedDate
				+ ", yearOfCompletion=" + yearOfCompletion
				+ ", fkSpecializationId=" + fkSpecializationId
				+ ", fkStreamId=" + fkStreamId + ", masEmpSpecialization="
				+ masEmpSpecialization + ", masEmpStream=" + masEmpStream
				+ ", datEmpDetailModifiedBy=" + datEmpDetailModifiedBy
				+ ", datEmpDetailCreatedBy=" + datEmpDetailCreatedBy
				+ ", datEmpDetailEmpId=" + datEmpDetailEmpId + "]";
	}

}