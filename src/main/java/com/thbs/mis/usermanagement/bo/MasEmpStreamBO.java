package com.thbs.mis.usermanagement.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the mas_emp_stream database table.
 * 
 */
@Entity
@Table(name="mas_emp_stream")
@NamedQuery(name="MasEmpStreamBO.findAll", query="SELECT m FROM MasEmpStreamBO m")
public class MasEmpStreamBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_stream_id")
	private int pkStreamId;

	@Column(name="stream_name")
	private String streamName;

	
	public MasEmpStreamBO() {
	}

	public int getPkStreamId() {
		return this.pkStreamId;
	}

	public void setPkStreamId(int pkStreamId) {
		this.pkStreamId = pkStreamId;
	}

	public String getStreamName() {
		return this.streamName;
	}

	public void setStreamName(String streamName) {
		this.streamName = streamName;
	}

	@Override
	public String toString() {
		return "MasEmpStreamBO [pkStreamId=" + pkStreamId + ", streamName="
				+ streamName + "]";
	}


	

}