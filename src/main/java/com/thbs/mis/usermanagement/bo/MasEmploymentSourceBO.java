package com.thbs.mis.usermanagement.bo;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the mas_employment_sources database table.
 * 
 */
@Entity
@Table(name="mas_employment_sources")
@NamedQuery(name="MasEmploymentSourceBO.findAll", query="SELECT m FROM MasEmploymentSourceBO m")
public class MasEmploymentSourceBO implements Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_employment_source_id")
	private Integer pkEmploymentSourceId;

	@Column(name="employment_source_name")
	private String employmentSourceName;

	@Column(name="key_name")
	private String keyName;

	

	public MasEmploymentSourceBO() {
	}

	public Integer getPkEmploymentSourceId() {
		return this.pkEmploymentSourceId;
	}

	public void setPkEmploymentSourceId(Integer pkEmploymentSourceId) {
		this.pkEmploymentSourceId = pkEmploymentSourceId;
	}

	public String getEmploymentSourceName() {
		return this.employmentSourceName;
	}

	public void setEmploymentSourceName(String employmentSourceName) {
		this.employmentSourceName = employmentSourceName;
	}

	public String getKeyName() {
		return this.keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	
	@Override
	public String toString() {
		return "MasEmploymentSourceBO [pkEmploymentSourceId="
				+ pkEmploymentSourceId + ", employmentSourceName="
				+ employmentSourceName + ", keyName=" + keyName + "]";
	}
	
}