package com.thbs.mis.usermanagement.bo;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.framework.util.CustomTimeSerializer;
import com.thbs.mis.usermanagement.bean.EmployeeLocChangeOutputBean;

import java.util.Date;




/**
 * The persistent class for the dat_emp_location_approval database table.
 * 
 */
@Entity
@Table(name="dat_emp_location_approval")
@NamedQuery(name="DatEmpLocationApprovalBO.findAll", query="SELECT d FROM DatEmpLocationApprovalBO d")
@SqlResultSetMappings({
	@SqlResultSetMapping(name="employeeLocationResultSet" , classes = {@ConstructorResult(targetClass = EmployeeLocChangeOutputBean.class, columns={
	@ColumnResult(name = "fk_emp_id", type = Integer.class),
	@ColumnResult(name = "pk_id", type = Integer.class),
	@ColumnResult(name = "empName", type = String.class),
	@ColumnResult(name = "status", type = Byte.class),
	@ColumnResult(name = "statusName", type = String.class),
	@ColumnResult(name = "old_location", type = Short.class),
	@ColumnResult(name = "new_location", type = Short.class),
	@ColumnResult(name = "oldLocationName", type = String.class),
	@ColumnResult(name = "newLocationName", type = String.class),
	@ColumnResult(name = "requested_date", type = Date.class),
	@ColumnResult(name = "requested_by", type = Integer.class),
	@ColumnResult(name = "projCoordinatorName", type = String.class)

})})})
@NamedNativeQueries({@NamedNativeQuery(name="UserManagement.employeeLocationDetails", query="select emploc.fk_emp_id, emploc.pk_id, "
		+ "if(per.emp_middle_name is not null,concat(per.emp_first_name,' ',per.emp_middle_name,' ',per.emp_last_name), "
		+ "concat(per.emp_first_name,' ',per.emp_last_name)) as empName, "
		+ "emploc.status, case when emploc.status = 1 then 'PENDING FOR APPROVAL'"
        + "when emploc.status = 2 then 'APPROVED'"
        + "when emploc.status = 3 then 'REJECTED'"
        + "END as statusName, "
		+ "emploc.old_location, emploc.new_location, loc.location_parent_name as oldLocationName ,loc1.location_parent_name as newLocationName,"
		+ "emploc.requested_date, emploc.requested_by, "
		+ "if(personal.emp_middle_name is not null,concat(personal.emp_first_name,' ',personal.emp_middle_name,' ',personal.emp_last_name), "
		+ "concat(personal.emp_first_name,' ',personal.emp_last_name)) as projCoordinatorName "
		+ "from dat_emp_location_approval emploc "
		+ "join mas_location_parent loc on emploc.old_location = loc.pk_location_parent_id "
		+ "join mas_location_parent loc1 on emploc.new_location = loc1.pk_location_parent_id "
		+ "join dat_emp_personal_detail per on emploc.fk_emp_id = per.fk_emp_detail_id "
		+ "join dat_emp_personal_detail personal on emploc.requested_by = personal.fk_emp_detail_id "
		+ "where emploc.pk_id in(:id) order by emploc.status asc",resultSetMapping="employeeLocationResultSet")})
public class DatEmpLocationApprovalBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_id")
	private Integer pkId;

	@Column(name="fk_emp_id")
	private Integer fkEmpId;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="modified_date")
	private Date modifiedDate;

	@Column(name="new_location")
	private Short newLocation;

	@Column(name="old_location")
	private Short oldLocation;

	@Column(name="requested_by")
	private Integer requestedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="requested_date")
	private Date requestedDate;

	@Column(name="status")
	private Byte status;
	
	
	@OneToOne
	@JoinColumn(name = "fk_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailFkEmpId;
	
	@OneToOne
	@JoinColumn(name = "requested_by", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetailRequestedBy;
	

	public DatEmpLocationApprovalBO() {
	}

	
	
	public Integer getPkId() {
		return pkId;
	}



	public void setPkId(Integer pkId) {
		this.pkId = pkId;
	}



	public Integer getFkEmpId() {
		return fkEmpId;
	}



	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}



	public Integer getModifiedBy() {
		return modifiedBy;
	}



	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}



	public Date getModifiedDate() {
		return modifiedDate;
	}



	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}



	public Short getNewLocation() {
		return newLocation;
	}



	public void setNewLocation(Short newLocation) {
		this.newLocation = newLocation;
	}



	public Short getOldLocation() {
		return oldLocation;
	}



	public void setOldLocation(Short oldLocation) {
		this.oldLocation = oldLocation;
	}



	public Integer getRequestedBy() {
		return requestedBy;
	}



	public void setRequestedBy(Integer requestedBy) {
		this.requestedBy = requestedBy;
	}


	public Date getRequestedDate() {
		return requestedDate;
	}



	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}


	public Byte getStatus() {
		return status;
	}



	public void setStatus(Byte status) {
		this.status = status;
	}



	public DatEmpDetailBO getDatEmpDetailFkEmpId() {
		return datEmpDetailFkEmpId;
	}



	public void setDatEmpDetailFkEmpId(DatEmpDetailBO datEmpDetailFkEmpId) {
		this.datEmpDetailFkEmpId = datEmpDetailFkEmpId;
	}



	public DatEmpDetailBO getDatEmpDetailRequestedBy() {
		return datEmpDetailRequestedBy;
	}



	public void setDatEmpDetailRequestedBy(DatEmpDetailBO datEmpDetailRequestedBy) {
		this.datEmpDetailRequestedBy = datEmpDetailRequestedBy;
	}



	@Override
	public String toString() {
		return "DatEmpLocationApproval [pkId=" + pkId + ", fkEmpId=" + fkEmpId
				+ ", modifiedBy=" + modifiedBy + ", modifiedDate="
				+ modifiedDate + ", newLocation=" + newLocation
				+ ", oldLocation=" + oldLocation + ", requestedBy="
				+ requestedBy + ", requestedDate=" + requestedDate
				+ ", status=" + status + ", datEmpDetailFkEmpId="
				+ datEmpDetailFkEmpId + ", datEmpDetailRequestedBy="
				+ datEmpDetailRequestedBy + "]";
	}

	
}