package com.thbs.mis.usermanagement.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the dat_emp_last_employee_id database table.
 * 
 */
@Entity
@Table(name="dat_emp_last_employee_id")
@NamedQuery(name="DatEmpLastEmployeeIdBO.findAll", query="SELECT d FROM DatEmpLastEmployeeIdBO d")
public class DatEmpLastEmployeeIdBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pk_id")
	private int pkId;

	@Column(name="emp_type")
	private String empType;

	@Column(name="emp_type_id")
	private int empTypeId;

	@Column(name="last_employee_id")
	private int lastEmployeeId;

	public DatEmpLastEmployeeIdBO() {
	}

	public int getPkId() {
		return this.pkId;
	}

	public void setPkId(int pkId) {
		this.pkId = pkId;
	}

	public String getEmpType() {
		return this.empType;
	}

	public void setEmpType(String empType) {
		this.empType = empType;
	}

	public int getEmpTypeId() {
		return this.empTypeId;
	}

	public void setEmpTypeId(int empTypeId) {
		this.empTypeId = empTypeId;
	}

	public int getLastEmployeeId() {
		return this.lastEmployeeId;
	}

	public void setLastEmployeeId(int lastEmployeeId) {
		this.lastEmployeeId = lastEmployeeId;
	}

	@Override
	public String toString() {
		return "DatEmpLastEmployeeId [pkId=" + pkId + ", empType=" + empType
				+ ", empTypeId=" + empTypeId + ", lastEmployeeId="
				+ lastEmployeeId + "]";
	}

	
}