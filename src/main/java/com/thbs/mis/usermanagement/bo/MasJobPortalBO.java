package com.thbs.mis.usermanagement.bo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mas_job_portals database table.
 * 
 */
@Entity
@Table(name="mas_job_portals")
@NamedQuery(name="MasJobPortalBO.findAll", query="SELECT m FROM MasJobPortalBO m")
public class MasJobPortalBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_id")
	private int pkId;

	@Column(name="job_portal_name")
	private String jobPortalName;

	@Column(name="key_name")
	private String keyName;

	public MasJobPortalBO() {
	}

	public int getPkId() {
		return this.pkId;
	}

	public void setPkId(int pkId) {
		this.pkId = pkId;
	}

	public String getJobPortalName() {
		return this.jobPortalName;
	}

	public void setJobPortalName(String jobPortalName) {
		this.jobPortalName = jobPortalName;
	}

	public String getKeyName() {
		return this.keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	@Override
	public String toString() {
		return "MasJobPortalBO [pkId=" + pkId + ", jobPortalName="
				+ jobPortalName + ", keyName=" + keyName + "]";
	}

	
}