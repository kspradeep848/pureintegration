package com.thbs.mis.usermanagement.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thbs.mis.common.bean.CountryBean;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.usermanagement.bean.AdvancedSearchInputBean;
import com.thbs.mis.usermanagement.bean.AdvancedSearchOutputBean;
import com.thbs.mis.usermanagement.bean.EmpSubStatusInputBean;
import com.thbs.mis.usermanagement.bean.EmployeeDetailInputBean;
import com.thbs.mis.usermanagement.bean.EmployeeDetailOutputBean;
import com.thbs.mis.usermanagement.bean.EmployeeEducationalInputBean;
import com.thbs.mis.usermanagement.bean.EmployeeLocChangeInputBean;
import com.thbs.mis.usermanagement.bean.EmployeeLocChangeOutputBean;
import com.thbs.mis.usermanagement.bean.EmployeeLocationBean;
import com.thbs.mis.usermanagement.bean.EmployeePersonalInputBean;
import com.thbs.mis.usermanagement.bean.EmployeeProfileDetailBean;
import com.thbs.mis.usermanagement.bean.EmployeeRecruitmentBean;
import com.thbs.mis.usermanagement.bean.OrganizationDetailBean;
import com.thbs.mis.usermanagement.constants.UserManagementConstants;
import com.thbs.mis.usermanagement.constants.UserManagementURIConstants;
import com.thbs.mis.usermanagement.service.UserManagementHRService;

@Api(value = "User Management Related Operations", description = "Operations Related to the User Management", basePath = "http://localhost:8080/", consumes = "Rest Service", position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "User Management HR Related Services")
@Controller
public class UserManagementHRController {

	private static final AppLog LOG = LogFactory
			.getLog(UserManagementHRController.class);

	@Autowired
	UserManagementHRService userManagementHRService;

	// Added by Kamal Anand
	/**
	 * 
	 * @param subStatusBean
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This service is used to Deactivate Employee Profuile
	 */
	@ApiOperation(tags = "User Management", value = "To Deactivate Employee Profile."
			+ "This service will be called from the front-end when the HR wants to deactivate Employee Profile", httpMethod = "PUT", notes = "This Service has been implemented to deactivate Employee Profile."
			+ "<br>The Details will be stored in database.", nickname = "Deactivate Employee Profile", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Updated Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Deactivate Employee Profile", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UserManagementURIConstants.HR_DEACTIVATE_EMP_PROFILE, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> deactivateEmpProfile(
			@ApiParam(name = "EmpSubStatusInputBean", example = "EmpSubStatusInputBean", value = "EmpSubStatusInputBean with all tha data to deactivate Employee Profile.")@Valid @RequestBody EmpSubStatusInputBean subStatusBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering deactivateEmpProfile controller");
		boolean isProfileDeactivated = false;
		try {
			isProfileDeactivated = userManagementHRService
					.deactivateEmpProfile(subStatusBean);

			if (isProfileDeactivated) {
				LOG.endUsecase("Exiting deactivateEmpProfile controller");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Employee Profile Deactivated Successfully"));
			} else {
				LOG.endUsecase("Exiting deactivateEmpProfile controller");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.FAILURE,
								"Unable to Deactivate Employee Profile."));
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
	}

	/**
	 * 
	 * @param subStatusBean
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This service is used to change Sub Status of Active
	 *              Employee Profile
	 */
	@ApiOperation(tags = "User Management", value = "To Change Sub status of Employee Profile."
			+ "This service will be called from the front-end when the HR wants to change substatus of Employee Profile", httpMethod = "PUT", notes = "This Service has been implemented to change sub status of Employee Profile."
			+ "<br>The Details will be stored in database.", nickname = "Substatus change of Active Employee Profile", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Updated Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Change Substatus of Active Employee Profile", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UserManagementURIConstants.HR_ACTIVE_EMP_SUBSTATUS_CHANGE, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> changeActiveEmpSubstatus(
			@ApiParam(name = "EmpSubStatusInputBean", example = "EmpSubStatusInputBean", value = "EmpSubStatusInputBean with all tha data to change Substatus of Active Employee Profile.")@Valid @RequestBody EmpSubStatusInputBean subStatusBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering changeActiveEmpSubstatus controller");
		boolean isStatusChanged = false;
		try {
			isStatusChanged = userManagementHRService
					.changeActiveEmpSubstatus(subStatusBean);

			if (isStatusChanged) {
				LOG.endUsecase("Exiting changeActiveEmpSubstatus controller");
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Employee Profile Sub Status Changed Successfully"));
			} else {
				LOG.endUsecase("Exiting changeActiveEmpSubstatus controller");
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.FAILURE,
								"Unable to Change Sub Status of Employee Profile."));
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
	}

	/**
	 * 
	 * @param searchBean
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This service is used to Search Employee Profile.
	 */
	@ApiOperation(tags = "User Management", value = "To search Employee Profile."
			+ "This service will be called from the front-end when the HR wants to search Employee Profile", httpMethod = "POST", notes = "This Service has been implemented to search Employee Profile."
			+ "<br>The Details will be retrieved from Database.", nickname = "Search Employee Profile", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Request Executed Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Search Employee Profile", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UserManagementURIConstants.HR_ADVANCED_SEARCH, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> advancedSearchByHr(
			@ApiParam(name = "AdvancedSearchInputBean", example = "AdvancedSearchInputBean", value = "AdvancedSearchInputBean with all tha data to search Employee Profile.") @RequestBody AdvancedSearchInputBean searchBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering advancedSearch controller");
		List<AdvancedSearchOutputBean> searchResultList = new ArrayList<AdvancedSearchOutputBean>();
		try {
			searchResultList = userManagementHRService
					.advancedSearchByHr(searchBean);

			if (!searchResultList.isEmpty()) {
				LOG.endUsecase("Exiting advancedSearch controller");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS, "Search Result.",
								searchResultList));
			} else {
				LOG.endUsecase("Exiting advancedSearch controller");
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS, "No Records Found."));
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());

		}
	}

	// End of Addition by Kamal Anand


	
	@RequestMapping(value = UserManagementURIConstants.HR_CREATE_EMP_PROFILE, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> createEmpProfile(
			@RequestBody List<EmployeeDetailInputBean> listEmpBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering createEmpProfile controller");
		List<EmployeeDetailOutputBean> listEmployeeDetailBean;
		try {
			listEmployeeDetailBean = userManagementHRService
					.createEmpProfile(listEmpBean);
			LOG.endUsecase("Exiting createEmpProfile controller");
			if (listEmployeeDetailBean.size() > 0) {
				
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Employee Profile created Successfully",listEmployeeDetailBean));
			} else {

				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.FAILURE,
								"Unable to Create Employee Profile."));
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
	}

	@RequestMapping(value = UserManagementURIConstants.HR_CREATE_EMP_EDUCATION, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> createEmpEducation(
			@RequestBody List<EmployeeEducationalInputBean> listEmpBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering createEmpEducation controller");
	
		try {
			Boolean success = userManagementHRService
					.createEmpEducation(listEmpBean);
			LOG.endUsecase("Exiting createEmpEducation controller");
			if (success) {
				
				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Employee Educational details created Successfully"));
			} else {

				return ResponseEntity.status(HttpStatus.OK.value()).body(
						new MISResponse(HttpStatus.OK.value(),
								MISConstants.FAILURE,
								"Unable to Create Employee Educational details."));
								}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
	}


	

	//Added By Adavayya Hiremath
	/**
	 * 
	 * <Description updateOrganizationDetailsHR:> This service will be called from the front-end when the user wants to update Organizational Details of Employee
	 * @param orgDetailBean
	 * @return MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	@ApiOperation(tags = "User Management", value = "To update the Organizational Details."
			+ "This service will be called from the front-end when the user wants to update Organizational Details of Employee", httpMethod = "PUT", notes = "This Service has been implemented to update  Organizational Details of Employee."
			+ "<br>The Details will be stored in database.", nickname = "Update  Organizational Details of Employee", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Updated Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Update Organizational Details of Employee", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UserManagementConstants.UPDATE_ORGNIZATION_DETAIL, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> updateOrganizationDetailsHR(
			@ApiParam(name = "OrganizationDetailBean", example = "OrganizationDetailBean", value = "OrganizationDetailBean with all tha data to update Employee Organizational Details.")
			@RequestBody OrganizationDetailBean orgDetailBean)
			throws BusinessException, DataAccessException, CommonCustomException {
		Boolean success = false;
		LOG.startUsecase("Update Organizational Details of Employee");
		try {
			success = userManagementHRService
					.updateOrganizationDetail(orgDetailBean);
		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception occured in Update Organizational Details of Employee :", e);
		}
		if (success == true) {
			LOG.endUsecase("Update Organizational Details of Employee");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Organizational Details of Employee Updated Successfully."));
		} else {
			LOG.endUsecase("Update Organizational Details of Employee");
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
							.value(), MISConstants.FAILURE,
							"Some Error happend while Updating Organizational Details of Employee."));
		}
	}
	@RequestMapping(value = UserManagementConstants.UPDATE_LOCATION_DETAIL, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> updateLocationDetails(@RequestBody OrganizationDetailBean orgDetailBean)
			throws BusinessException, DataAccessException, CommonCustomException {
		Boolean success = false;
		LOG.startUsecase("Update Location Details of Employee");
		try {
			success = userManagementHRService
					.updateLocationDetail(orgDetailBean);
		} catch (DataAccessException e) {
			throw new DataAccessException(
					"Exception occured in Update Location Details of Employee :", e);
		}
		if (success == true) {
			LOG.endUsecase("Update Location Details of Employee");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Location Details of Employee Updated Successfully."));
		} else {
			LOG.endUsecase("Update Location Details of Employee");
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
							.value(), MISConstants.FAILURE,
							"Some Error happend while Updating Location Details of Employee."));
		}
	}
	//EOD By Adavayya Hiremath
	
	@RequestMapping(value = UserManagementURIConstants.HR_GET_EMP_DETAILS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getEmpDetailsByID(@PathVariable Integer empId)
			throws BusinessException, DataAccessException {

		LOG.startUsecase("diplay Employee Detail");
		
		EmployeeProfileDetailBean employeeProfileBean = new EmployeeProfileDetailBean();
		List<EmployeeProfileDetailBean> listOfemployeeProfileBean = new ArrayList<EmployeeProfileDetailBean>();
		try {
			employeeProfileBean = userManagementHRService.getEmpDetails(empId);
			listOfemployeeProfileBean.add(employeeProfileBean);
		} catch (Exception e) {
			throw new BusinessException("Failed to retrieve all Employee Details", e);
		}

		LOG.endUsecase("update Employee Detail completed");
		if (employeeProfileBean !=null) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All Employees Details have been retrieved successfully", listOfemployeeProfileBean));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "No Employees Details"));
		}
	}

	
	@RequestMapping(value = UserManagementURIConstants.UPDATE_PERSONAL_EMP_DETAILS, method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> updateEmpPersonalDetail(
			@RequestParam(value = "empPersonalDetailBean", name = "empPersonalDetailBean") String empPersonalDetailBean,
			@RequestParam(value = "passPort", name = "passPort", required = false) MultipartFile passPort,
			@RequestParam(value = "voterId", name = "voterId", required = false) MultipartFile voterId,
			@RequestParam(value = "aadhar", name = "aadhar", required = false) MultipartFile aadhar,
			@RequestParam(value = "panCard", name = "panCard", required = false) MultipartFile panCard)
			throws BusinessException, DataAccessException, ParseException,
			MethodArgumentNotValidException, CommonCustomException {
		LOG.startUsecase("Update personal Employee details");

		Boolean success = false;
		ObjectMapper objectMapper = new ObjectMapper();
		EmployeePersonalInputBean inputBean = new EmployeePersonalInputBean();
		
		
		try {
			inputBean = objectMapper.readValue(empPersonalDetailBean,
					EmployeePersonalInputBean.class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new BusinessException("Some exception occurred while mapping bean ");
		} catch (IOException e) {
			e.printStackTrace();
			throw new BusinessException("Some IOException occurred while mapping bean ");
		}
		try {

			success = userManagementHRService.updatePersonalEmploymentDetail(inputBean,passPort,voterId,aadhar,panCard);

			if (success) {
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Employement Persoanl Details are updated successfully."));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
								.value(), MISConstants.FAILURE,
								"Employement Personal Details are not updated."));
			}

		} catch (DataAccessException e) {
			e.printStackTrace();
			throw new BusinessException("Exception occured", e);
		}

	}

	@RequestMapping(value = UserManagementURIConstants.HR_CHANGE_EMP_TYPE, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> changeEmpTypeByHr(
			@PathVariable Integer empId,@PathVariable Integer loggedInEmpId)
			throws BusinessException, DataAccessException, ParseException,
			MethodArgumentNotValidException, CommonCustomException {
		LOG.startUsecase("changeEmpTypeByHr");

		EmployeeProfileDetailBean employeeProfileBean = new EmployeeProfileDetailBean();
		List<EmployeeProfileDetailBean> listOfemployeeProfileBean = new ArrayList<EmployeeProfileDetailBean>();
		try {

			employeeProfileBean = userManagementHRService.changeEmpType(empId,loggedInEmpId);
			listOfemployeeProfileBean.add(employeeProfileBean);


		} catch (Exception e) {
			
			throw new BusinessException("Exception occured", e);
		}
			if (listOfemployeeProfileBean.size() > 0) {
				return ResponseEntity
						.status(HttpStatus.OK.value())
						.body(new MISResponse(HttpStatus.OK.value(),
								MISConstants.SUCCESS,
								"Employement type has been changed successfully.",listOfemployeeProfileBean));
			} else {
				return ResponseEntity.status(
						HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
						new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR
								.value(), MISConstants.FAILURE,
								"Exception occured while changing emp type"));
			}


	}
	
	@RequestMapping(value = UserManagementURIConstants.EMPLOYEE_LOCATION, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> employeeLocationBasedOnCountry(@RequestBody EmployeeLocationBean locationBean) throws DataAccessException{
		
		List<CountryBean> listOfCountryBean = new ArrayList<CountryBean>();
		try{
			
			listOfCountryBean = userManagementHRService.employeeLocationBasedOnCountry(locationBean);
			
			
		}catch(DataAccessException e){
			throw new DataAccessException(e.getMessage());
		}
		if (listOfCountryBean.size() > 0) {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"List of countries retrieved successfully ",listOfCountryBean));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"No data for given input"));
		}
		

		
	}

	@RequestMapping(value = UserManagementURIConstants.EMPLOYEE_RECRUITER_DETAIL, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> employeeRecruiterDetail(@RequestBody EmployeeRecruitmentBean recruitmentBean) throws DataAccessException{
		Boolean success = false;
		List<CountryBean> listOfCountryBean = new ArrayList<CountryBean>();
		try{
			
			success = userManagementHRService.employeeRecruiterDetail(recruitmentBean);
			
			
		}catch(DataAccessException e){
			throw new DataAccessException(e.getMessage());
		}
		if (success) {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Employee Recruiter Detail added successfully ",listOfCountryBean));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"No data for given input"));
		}
		

		
	}
	
	
	@RequestMapping(value = UserManagementURIConstants.UPDATE_EMP_LOC_BY_HR, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> updateEmpLocByHr(@RequestBody EmployeeRecruitmentBean recruitmentBean) throws DataAccessException{
		Boolean success = false;
		List<CountryBean> listOfCountryBean = new ArrayList<CountryBean>();
		try{
			
			success = userManagementHRService.employeeRecruiterDetail(recruitmentBean);
			
			
		}catch(DataAccessException e){
			throw new DataAccessException(e.getMessage());
		}
		if (success) {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Employee Recruiter Detail added successfully ",listOfCountryBean));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"No data for given input"));
		}
		

		
	}
	
	@RequestMapping(value = UserManagementURIConstants.EMPLOYEE_RECRUITMENT_UPDATE, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> deactivateEmpProfile(@RequestBody EmployeeRecruitmentBean bean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering employee Recruitment update controller");
		boolean flag = false;
		flag = userManagementHRService.employeeRecruitmentUpdateService(bean);

		if (flag) {
			LOG.endUsecase("Exiting employee Recruitment update controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "employee Recruitment updated Successfully"));
		} else {
			LOG.endUsecase("Exiting employee Recruitment update controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.FAILURE, "Unable to update employee Recruitment details."));
		}
	}
	
	@RequestMapping(value = UserManagementURIConstants.APPROVE_EMPLOYEE_LOCATION_CHANGE_BY_HR, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> empLocationApproveByHR(@PathVariable Integer pkLocId,@PathVariable Byte status)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering employee empLocationChangeByPC controller");
		boolean flag = false;
		flag = userManagementHRService.employeeLocationApproveByHrService(pkLocId,status);

		if (flag) {
			LOG.endUsecase("Exiting employee empLocationChangeByPC controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "employee location changes Successfully"));
		} else {
			LOG.endUsecase("Exiting employee empLocationChangeByPC controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.FAILURE, "Unable to create employee location details."));
		}
	}
	
	@RequestMapping(value = UserManagementURIConstants.GET_EMPLOYEE_LOC_CHANGE_DETAIL_BY_HR, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> getEmployeeLocationChangeDetailByHr(@RequestBody EmployeeLocChangeInputBean bean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering employee getEmployeeLocationChangeDetailByHr controller");
	
		List<EmployeeLocChangeOutputBean> searchResultList = new ArrayList<EmployeeLocChangeOutputBean>();
		searchResultList = userManagementHRService.getEmployeeLocationChangeDetailByHr(bean);

		if (searchResultList.size() > 0) {
			LOG.endUsecase("Exiting employee getEmployeeLocationChangeDetailByHr controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "employee location changes retrieved successfully",searchResultList));
		} else {
			LOG.endUsecase("Exiting employee empLocationChangeByPC controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.FAILURE, "Unable to create employee location details."));
		}
	}

}
