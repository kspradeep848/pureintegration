
package com.thbs.mis.usermanagement.controller;

import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thbs.mis.common.bean.EmployeeNameWithRMIdBean;
import com.thbs.mis.common.controller.UserController;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.usermanagement.bean.CreateEmpProfDetailBean;
import com.thbs.mis.usermanagement.bean.DateRangeBean;
import com.thbs.mis.usermanagement.bean.DateRangeOutputBean;
import com.thbs.mis.usermanagement.bean.EmpLocChangeByPcBean;
import com.thbs.mis.usermanagement.bean.EmployeeDetailsByIdBean;
import com.thbs.mis.usermanagement.bean.EmployeeRecruitmentBean;
import com.thbs.mis.usermanagement.bean.MasEmployeeSpecializationBean;
import com.thbs.mis.usermanagement.bean.MasEmployeeStreamBean;
import com.thbs.mis.usermanagement.bean.UpdateEmailIDBean;
import com.thbs.mis.usermanagement.bean.UpdateEmpPersonalDetailsBean;
import com.thbs.mis.usermanagement.bean.UpdateExtensionNumBean;
import com.thbs.mis.usermanagement.bo.DatEmpEmploymentDetailBO;
import com.thbs.mis.usermanagement.constants.UserManagementConstants;
import com.thbs.mis.usermanagement.constants.UserManagementURIConstants;
import com.thbs.mis.usermanagement.service.UserManagementService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@Api(value = "Common Operations", description = "Common Operations", basePath = "http://localhost:8080/", consumes = "Rest Service", position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "Common Services")
@Controller
public class UserManagementController {

	private static final AppLog LOG = LogFactory.getLog(UserController.class);

	@Autowired
	private UserManagementService userService;

	/**
	 * <Description UpdateExtension:> This method is used to display Employee
	 * details
	 * 
	 * @param user
	 * @return MIS Response
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Display Employee Details." + " .", httpMethod = "POST",

			notes = "This Service has been implemented to display Employee details"
					+ "<br>The Details will be stored in database.",

			nickname = "Dsiplay Employee Details ", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = EmployeeDetailsByIdBean.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Succefully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Display employee details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Display employee details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UserManagementURIConstants.EMP_DETAILS_BY_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getEmpDetailsByID(@PathVariable Integer empId)
			throws BusinessException, DataAccessException {

		LOG.startUsecase("diplay Employee Detail");
		List<EmployeeDetailsByIdBean> empdetailList = null;

		try {
			empdetailList = userService.getAllEmpDetails(empId);
		} catch (DataAccessException e) {
			throw new BusinessException("Failed to retrieve all Employee Details", e);
		}

		LOG.endUsecase("update Employee Detail completed");
		if (!empdetailList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "All Employees Details have been retrieved successfully", empdetailList));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE,
							"No Employees Details", empdetailList));
		}
	}

	/**
	 * <Description addUser:> This method is used to update Employee Personal
	 * Details
	 * 
	 * @param user
	 * @return MIS Response
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 * @throws BusinessException
	 */
	@ApiOperation(value = "Update a Employee Personal Details." + " .", httpMethod = "POST",

			notes = "This Service has been implemented to update Employee Personal details."
					+ "<br>The Details will be store back in database.",

			nickname = "update Employee Personal details", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Succefully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Updated employee details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Updated employee details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UserManagementURIConstants.UPDATE_PERSONAL_DETAILS_BY_EMPLOYEE_ID, method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> updateEmpPersonalDetails(
			@RequestParam(value = "inputBean", name = "inputBean", required = false) String inputBean,
			@RequestParam(value = "passPort", name = "passPort", required = false) MultipartFile passPort,
			@RequestParam(value = "voterId", name = "voterId", required = false) MultipartFile voterId,
			@RequestParam(value = "aadhar", name = "aadhar", required = false) MultipartFile aadhar,
			@RequestParam(value = "panCard", name = "panCard", required = false) MultipartFile panCard)
			throws DataAccessException, CommonCustomException, BusinessException {

		LOG.startUsecase("Update personal Employee details");

		Boolean success = false;
		ObjectMapper objectMapper = new ObjectMapper();
		UpdateEmpPersonalDetailsBean bean = new UpdateEmpPersonalDetailsBean();

		try {
			bean = objectMapper.readValue(inputBean, UpdateEmpPersonalDetailsBean.class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new BusinessException("Some exception occurred while mapping bean ");
		} catch (IOException e) {
			e.printStackTrace();
			throw new BusinessException("Some IOException occurred while mapping bean ");
		}
		try {

			success = userService.updatePersonalDetails(bean, passPort, voterId, aadhar, panCard);

			if (success) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS, "Employement Persoanl Details are updated successfully."));
			} else {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
						.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE,
								"Employement Personal Details are not updated."));
			}

		} catch (DataAccessException e) {
			e.printStackTrace();
			throw new BusinessException("Exception occured", e);
		}

	}

	/**
	 * <Description updateExtensionNumber:> This method is used to update
	 * extension
	 * 
	 * @param user
	 * @return MIS Response
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	@ApiOperation(value = "Update a Employee Extension Number in employee profiles by employee."
			+ " .", httpMethod = "POST",

			notes = "This Service has been implemented to update Extension Number in employee profiles by Employee."
					+ "<br>The Details will be stored in database.",

			nickname = "update Extension Number by Employee", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Succefully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Updated employee details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Updated employee details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UserManagementURIConstants.UPDATE_EXTENSION_NUMBER_BY_EMPLOYEE, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> updateExtension(@RequestBody UpdateExtensionNumBean bean)
			throws DataAccessException, CommonCustomException {
		LOG.startUsecase("updateExtension");
		Boolean success = false;
		try {
			success = userService.updateExtensionNumber(bean);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("updateExtension");

		if (success == true) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "User Extension Number updated successfully"));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE,
							"Some error occured while updating User Extension Number."));
		}

	}

	/**
	 * <Description addUser:> This method is used to update email ID by Admin
	 * 
	 * @param user
	 * @return ResponseEntity
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 */
	@ApiOperation(value = "Update a Employee Email ID in employee profiles by system admin."
			+ " .", httpMethod = "POST",

			notes = "This Service has been implemented to update email id in employee profiles by system admin."
					+ "<br>The Details will be stored in database.",

			nickname = "update employee email ID by Admin", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Request Executed Successfully"),
			@ApiResponse(code = 201, message = "Created Succefully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Updated employee details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Updated employee details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UserManagementURIConstants.UPDATE_EMAIL_BY_ADMIN, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> updateEmailID(@RequestBody UpdateEmailIDBean user) throws CommonCustomException {

		LOG.startUsecase("updateEmailID");
		Boolean success = false;
		try {
			success = userService.updateEmailID(user);
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("updateEmailID");

		if (success == true) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "User Email ID updated successfully"));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE,
							"Some error occured while creating User Email ID."));
		}

	}

	/**
	 * 
	 * <Description getAllEmpDetailsONDateRange:> Employee Details under given
	 * Date Range
	 * 
	 * @param dateRange
	 * @return MIS Response
	 * @throws DataAccessException
	 * @throws BusinessException
	 * @throws CommonCustomException
	 * @throws ParseException
	 */

	@ApiOperation(value = "Get all the employee details who joined the company between given Date Range."
			+ " .", httpMethod = "POST",

			notes = "This Service has been implemented to fetch all the employee details who joined the company between given Date Range.."
					+ "<br>The Response will be the list of details like emp Id,emp Name etc who are joined the company between given Date Range..",

			nickname = "Employee Details under given Date Range", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = DateRangeOutputBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = DateRangeOutputBean.class),
			@ApiResponse(code = 201, message = "List of Employee details retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Employee Details under give Date Range", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "Employee Details under give Date Range", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })

	@RequestMapping(value = UserManagementURIConstants.DATE_RANGE_EMP_DETAIL, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> getAllEmpDetailsONDateRange(@RequestBody DateRangeBean dateRange)
			throws DataAccessException, BusinessException, CommonCustomException, ParseException {

		LOG.entering("getAllEmpDetailsONDateRange");

		List<DateRangeOutputBean> listOfEmpRequest = new ArrayList<DateRangeOutputBean>();
		try {

			listOfEmpRequest = userService.viewAllEmpDetailsONDateRange(dateRange);
		} catch (CommonCustomException e) {
			throw new BusinessException(e.getMessage());
		}

		if (listOfEmpRequest.isEmpty()) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Employees List is empty", listOfEmpRequest));
		} else {

			LOG.endUsecase("getAllEmpDetailsONDateRange");

			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Employees List retrieved successfully", listOfEmpRequest));
		}

	}

	// Added by Balaji
	/**
	 * @category This service is used to get All Employee Specialization details
	 *           in a Organization
	 * 
	 * @return List<RoleDetailsOutputBean>
	 * @throws BusinessException
	 */
	@ApiOperation(value = "This service will be called when user wants to view all the details of Roles in Organization"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all the Employee Specialization details."
					+ "<br>The Response will be the list of the Employee Specialization details.",

			nickname = "Fetch all Employee Specialization Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeNameWithRMIdBean.class),
			@ApiResponse(code = 201, message = "All Employee Specialization details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Employee Specialization Details of Organization", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Role Details of Organization", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UserManagementConstants.GET_ALL_EMPLOYEE_SPECIALIZATION_DETAILS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getEmployeeSpecializationDetails() throws BusinessException {
		LOG.startUsecase("Entering getEmployeeSpecializationDetails controller");
		List<MasEmployeeSpecializationBean> specDetails = null;
		try {
			specDetails = userService.getEmpSpecDetails();
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		if (!specDetails.isEmpty()) {
			LOG.endUsecase("Exiting getEmployeeSpecializationDetails controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Employee Specialization Details List", specDetails));
		} else {
			LOG.endUsecase("Exiting getRoleDetails controller");
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No Details found"));
		}
	}

	/**
	 * @category This service is used to get All Employee Stream details in a
	 *           Organization
	 * 
	 * @return List<RoleDetailsOutputBean>
	 * @throws BusinessException
	 */
	@ApiOperation(value = "This service will be called when user wants to view all the details of Employee Stream Details"
			+ " .", httpMethod = "GET",

			notes = "This Service has been implemented to fetch all the Role0 details."
					+ "<br>The Response will be the list of the reporting managers based on emp Id.",

			nickname = "Fetch all Employee Stream Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = EmployeeNameWithRMIdBean.class),
			@ApiResponse(code = 201, message = "All Employee Stream details are retrieved successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Employee Stream Details", response = URI.class)),
			@ApiResponse(code = 401, message = "Unauthorized HTTP responses", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "All Employee Stream Details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UserManagementConstants.GET_ALL_EMPLOYEE_STREAM_DETAILS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getEmployeeStreamDetails() throws BusinessException {
		LOG.startUsecase("Entering getEmployeeStreamDetails controller");
		List<MasEmployeeStreamBean> streamDetails = null;
		try {
			streamDetails = userService.getEmpStreamDetails();
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		if (!streamDetails.isEmpty()) {
			LOG.endUsecase("Exiting getEmployeeStreamDetails controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Employee Stream Details List", streamDetails));
		} else {
			LOG.endUsecase("Exiting getRoleDetails controller");
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No Details found"));
		}
	}
	// EOA of Balaji

	// Added by Adavayya Hiremath

	/**
	 * 
	 * <Description createEmpProfDetail:> This service helps to create Employee
	 * Details
	 * 
	 * @param createEmpProfDetailBeans
	 * @return MISResponse
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws ParseException
	 * @throws MethodArgumentNotValidException
	 */

	@ApiOperation(tags = "Training", value = " To create Employement Details"
			+ "This service will be called from the front-end when the user wants to create Employement Details", httpMethod = "POST", notes = "This Service has been implemented to create Employement Details."
					+ "<br>The Details will be stored in database.", nickname = "Create Employement Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "List of professional details", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UserManagementConstants.CREATE_PROF_EMP_DETAILS, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> createEmpProfDetail(
			@ApiParam(name = "DatEmpEmploymentDetailBO", example = "CreateEmpProfDetailBean", value = "creating professional Employee details Bo with all tha data to professional Employee details.") @Valid @RequestBody List<CreateEmpProfDetailBean> createEmpProfDetailBeans)
			throws BusinessException, DataAccessException, ParseException, MethodArgumentNotValidException {
		LOG.startUsecase("Create professional Employee details");

		List<DatEmpEmploymentDetailBO> datEmpEmploymentDetailBOs;
		try {

			datEmpEmploymentDetailBOs = userService.createProfEmploymentDetail(createEmpProfDetailBeans);

			if (datEmpEmploymentDetailBOs.size() > 0) {
				return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
						MISConstants.SUCCESS, "Employement Details are created successfully."));
			} else {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
						.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), MISConstants.FAILURE,
								"Employement Details are not created."));
			}

		} catch (DataAccessException e) {
			e.printStackTrace();
			throw new BusinessException("Exception occured", e);
		}

	}

	// EOA by Adavayya Hiremath

	
	@RequestMapping(value = UserManagementURIConstants.EMPLOYEE_LOCATION_CHANGE_BY_PC, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> empLocationChangeByPC(@RequestBody EmpLocChangeByPcBean bean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering employee empLocationChangeByPC controller");
		boolean flag = false;
		flag = userService.employeeLocationChangeByPcService(bean);

		if (flag) {
			LOG.endUsecase("Exiting employee empLocationChangeByPC controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "employee location changes Successfully"));
		} else {
			LOG.endUsecase("Exiting employee empLocationChangeByPC controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.FAILURE, "Unable to create employee location details."));
		}
	}
	
	
}
