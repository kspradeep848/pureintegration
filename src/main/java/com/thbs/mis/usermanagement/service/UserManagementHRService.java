package com.thbs.mis.usermanagement.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.thbs.mis.common.bean.CountryBean;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.MasEmpMainStatusBO;
import com.thbs.mis.common.bo.MasEmpSubStatusBO;
import com.thbs.mis.common.dao.BootcampRepository;
import com.thbs.mis.common.dao.BusinessUnitRepository;
import com.thbs.mis.common.dao.CountryRepository;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.DesignationRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.common.dao.LevelRepository;
import com.thbs.mis.common.dao.MasEmpMainDetailRepository;
import com.thbs.mis.common.dao.MasEmpSubStatusRepository;
import com.thbs.mis.common.dao.ParentLocationRepository;
import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.DateTimeUtil;
import com.thbs.mis.gsr.service.GSRGoalService;
import com.thbs.mis.uan.bo.DatEmpKycDetailBO;
import com.thbs.mis.uan.dao.EmpKycDetailRepository;
import com.thbs.mis.usermanagement.bean.AdvancedSearchInputBean;
import com.thbs.mis.usermanagement.bean.AdvancedSearchOutputBean;
import com.thbs.mis.usermanagement.bean.CreateEmpProfDetailBean;
import com.thbs.mis.usermanagement.bean.EmpSubStatusInputBean;
import com.thbs.mis.usermanagement.bean.EmployeeDetailInputBean;
import com.thbs.mis.usermanagement.bean.EmployeeDetailOutputBean;
import com.thbs.mis.usermanagement.bean.EmployeeEducationalInputBean;
import com.thbs.mis.usermanagement.bean.EmployeeEducationalOutputBean;
import com.thbs.mis.usermanagement.bean.EmployeeKYCInputBean;
import com.thbs.mis.usermanagement.bean.EmployeeLocChangeInputBean;
import com.thbs.mis.usermanagement.bean.EmployeeLocChangeOutputBean;
import com.thbs.mis.usermanagement.bean.EmployeeLocationBean;
import com.thbs.mis.usermanagement.bean.EmployeeOrganizationOutputBean;
import com.thbs.mis.usermanagement.bean.EmployeePersonalInputBean;
import com.thbs.mis.usermanagement.bean.EmployeeProfileDetailBean;
import com.thbs.mis.usermanagement.bean.EmployeeRecruitmentBean;
import com.thbs.mis.usermanagement.bean.OrganizationDetailBean;
import com.thbs.mis.usermanagement.bo.DatEmpEducationDetailBO;
import com.thbs.mis.usermanagement.bo.DatEmpEmploymentDetailBO;
import com.thbs.mis.usermanagement.bo.DatEmpLastEmployeeIdBO;
import com.thbs.mis.usermanagement.bo.DatEmpLocationApprovalBO;
import com.thbs.mis.usermanagement.bo.DatEmpRecruitmentDetailBO;
import com.thbs.mis.usermanagement.bo.MapMasOrgCountryBO;
import com.thbs.mis.usermanagement.dao.AdvancedSearchSpecification;
import com.thbs.mis.usermanagement.dao.DatEmpEmploymentRepository;
import com.thbs.mis.usermanagement.dao.DatEmpLastEmployeeIdRepository;
import com.thbs.mis.usermanagement.dao.DatEmpLocApprovalRepository;
import com.thbs.mis.usermanagement.dao.DatEmpRecruitmentRepository;
import com.thbs.mis.usermanagement.dao.DatEmployeeEducationRepository;
import com.thbs.mis.usermanagement.dao.DatEmployeePersonalRepository;
import com.thbs.mis.usermanagement.dao.EmployeeLocChangeSpecification;
import com.thbs.mis.usermanagement.dao.MapMasOrgCountryRepository;

@Service
public class UserManagementHRService {

	private static final AppLog LOG = LogFactory.getLog(UserManagementHRService.class);

	@Autowired
	EmpDetailRepository empDetailRepository;

	@Autowired
	private EmployeeProfessionalRepository professionalRepository;

	@Autowired
	private EmployeePersonalDetailsRepository personalRepository;

	@Autowired
	private GSRGoalService gsrGoalService;

	@Autowired
	private DatEmpLastEmployeeIdRepository empLastEmployeeIdRepository;

	@Autowired
	DatEmployeeProfessionalRepository empProfRepository;

	@Autowired
	MasEmpSubStatusRepository empSubStatusRepository;

	@Autowired
	MasEmpMainDetailRepository empMainStatusRepository;

	@Autowired
	DesignationRepository empDesignationRepository;

	@Autowired
	LevelRepository empLevelRepository;

	@Autowired
	BootcampRepository bootCampRepository;

	@Autowired
	ParentLocationRepository parentLocRepository;

	@Autowired
	CountryRepository countryRepository;

	@Autowired
	BusinessUnitRepository buRepository;

	@Autowired
	private DatEmployeeEducationRepository empEducationalRepository;

	@Autowired
	private DatEmployeePersonalRepository empPersonalRepository;

	@Autowired
	private EmpKycDetailRepository empKycRepository;

	@Autowired
	private DatEmpEmploymentRepository datEmpEmploymentRepository;

	@Autowired
	private CountryRepository countryRepos;

	@Autowired
	private MapMasOrgCountryRepository mapOrgCountryRepository;

	@Autowired
	private CommonService commonSrvc;
	
	@Autowired
	private DatEmpRecruitmentRepository recruitmentRepo;
	
	@Autowired
	private DatEmpLocApprovalRepository locationApprovalRepo;

	@Value("${common.system.projectCoOrdinator.role}")
	private String projCoOrdinatorRole;

	@Value("${emp.status.active}")
	private byte empActiveStatus;

	@Value("${emp.status.inactive}")
	private byte empInactiveStatus;

	@Value("${emp.status.waitingforprofilecreation}")
	private Byte empWaitingStatus;

	// Added by Kamal Anand

	@Value("${emp.active.status.mapping}")
	private String activeSubStatus;

	@Value("${deactivate.profile.role}")
	private String decativeProfileRoles;

	@Value("${substatus.change.role}")
	private String subStatusChangeRoles;

	@Value("${employement.type}")
	private String employementType;

	@Value("${emp.gender.type}")
	private String empGender;

	@Value("${emp.deactive.status.mapping}")
	private String deactiveSubStatus;

	@Value("${kyc.file.basepath}")
	private String kycBasePath;

	@Value("${user.doc.url}")
	private String userDocUrl;

	@Value("${user.photo.url}")
	private String userPhotoUrl;

	@Value("${profile.photo.basepath}")
	private String photoBasePath;

	@Value("${aadhar.file.name}")
	private String aadharFileName;

	@Value("${pan.file.name}")
	private String panCardFileName;

	@Value("${voter.file.name}")
	private String voterFileName;

	@Value("${passport.file.name}")
	private String passPortFileName;
	
	@Value("${common.system.hr.roles}")
	private String hrRoles;

	public enum EducationalLevel {

		TENTH(1), TWELTH(2), DIPLOMA(3), UNDERGRADUATE(4), POSTGRADUATE(5);
		private int value;

		EducationalLevel(int value) {
			this.value = value;
		}

	}

	public enum EmployeeType {

		Permanent(1), Contract(2), FTE(3), Trainee(4);
		private int value;

		EmployeeType(int value) {
			this.value = value;
		}

	}
	// Added by Kamal Anand

	/**
	 * 
	 * @param subStatusBean
	 * @return boolean
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This service is used to deactivate Employee Profile
	 */

	public boolean deactivateEmpProfile(EmpSubStatusInputBean subStatusBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering deactivateEmpProfile() service");
		boolean isProfileDeactivated = false;
		DatEmpDetailBO empDetail = null;
		DatEmpDetailBO updateEmpDetail = null;
		DatEmpDetailBO hasAccessEmpDetail = null;
		DatEmpProfessionalDetailBO profDetail = null;

		try {
			List<String> subStatusList = Arrays.asList(deactiveSubStatus.split(","));
			List<Byte> deactiveSubStatusList = subStatusList.stream().map(Byte::valueOf).collect(Collectors.toList());
			List<Short> deactivationRoleIdList = Arrays.asList(decativeProfileRoles.split(",")).stream()
					.map(Short::valueOf).collect(Collectors.toList());
			try {
				empDetail = empDetailRepository.findByPkEmpId(subStatusBean.getEmpId());
			} catch (Exception e) {
				throw new DataAccessException("Some Exception occured while getting Employee Detail from back end.");
			}
			if (empDetail != null) {
				if (empDetail.getFkEmpMainStatus() == empInactiveStatus)
					throw new CommonCustomException("Employee Profile is already deactivated");
				else {
					boolean idExists = deactiveSubStatusList.stream()
							.anyMatch(status -> status.equals(subStatusBean.getEmpSubStatus()));

					if (idExists) {
						hasAccessEmpDetail = empDetailRepository.findByPkEmpIdAndFkEmpRoleIdIn(
								subStatusBean.getLoggedInUserId(), deactivationRoleIdList);
						if (hasAccessEmpDetail != null) {
							empDetail.setFkEmpSubStatus(subStatusBean.getEmpSubStatus());
							empDetail.setFkEmpMainStatus(empInactiveStatus);
							empDetail.setModifiedBy(subStatusBean.getLoggedInUserId());
							empDetail.setModifiedDate(new Date());
							try {
								updateEmpDetail = empDetailRepository.save(empDetail);
							} catch (Exception e) {
								throw new DataAccessException(
										"Some exception occurred while updating employee details");
							}
							if (updateEmpDetail != null) {
								try {
									profDetail = professionalRepository
											.findByFkMainEmpDetailId(subStatusBean.getEmpId());
								} catch (Exception e) {
									throw new DataAccessException(
											"Some exception occurred while getting employee professional details");
								}
								profDetail.setEmpLastWorkingDate(new Date());
								try {
									professionalRepository.save(profDetail);
									isProfileDeactivated = true;
								} catch (Exception e) {
									throw new DataAccessException(
											"Some exception occurred while deactivating employee profile");
								}
							}
						} else {
							throw new CommonCustomException("Access denied because you are not an authorized person.");
						}
					} else {
						throw new CommonCustomException("Invaild Sub Status. Please enter the proper Sub Status.");
					}
				}
			} else {
				throw new CommonCustomException("Given employee Id does't exist, Please pass proper employee Id");
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("Exiting deactivateEmpProfile() service");
		return isProfileDeactivated;
	}

	// End of Addition by Kamal Anand

	public List<EmployeeDetailOutputBean> createEmpProfile(List<EmployeeDetailInputBean> listEmpBean)
			throws DataAccessException, CommonCustomException {

		List<EmployeeDetailOutputBean> listEmpOutputBean = new ArrayList<EmployeeDetailOutputBean>();

		Iterable<DatEmpLastEmployeeIdBO> iterableOfLastEmpId = new ArrayList<DatEmpLastEmployeeIdBO>();
		List<DatEmpLastEmployeeIdBO> listOfLastEmpId = new ArrayList<DatEmpLastEmployeeIdBO>();
		List<EmployeeDetailInputBean> listOfPermanent = new ArrayList<EmployeeDetailInputBean>();
		List<EmployeeDetailInputBean> listOfContract = new ArrayList<EmployeeDetailInputBean>();
		List<EmployeeDetailInputBean> listOfTrainee = new ArrayList<EmployeeDetailInputBean>();
		int permanent = 0;
		int contract = 0;
		int trainee = 0;
		try {

			iterableOfLastEmpId = empLastEmployeeIdRepository.findAll();

			listOfLastEmpId = StreamSupport.stream(iterableOfLastEmpId.spliterator(), false)
					.collect(Collectors.toList());
			if (listOfLastEmpId.size() > 0) {
				for (DatEmpLastEmployeeIdBO empIdBo : listOfLastEmpId) {
					if (empIdBo.getEmpTypeId() == 1 || empIdBo.getEmpTypeId() == 3) {
						permanent = empIdBo.getLastEmployeeId();
					} else if (empIdBo.getEmpTypeId() == 2) {
						contract = empIdBo.getLastEmployeeId();
					} else if (empIdBo.getEmpTypeId() == 4) {
						trainee = empIdBo.getLastEmployeeId();
					}
				}
			}

			if (listEmpBean.size() > 0) {
				for (EmployeeDetailInputBean empBean : listEmpBean) {
					if (empBean.getEmpType() == 1 || empBean.getEmpType() == 3) {
						listOfPermanent.add(empBean);
					} else if (empBean.getEmpType() == 2) {
						listOfContract.add(empBean);
					} else if (empBean.getEmpType() == 4) {
						listOfTrainee.add(empBean);
					}
				}
			}

			if (listOfPermanent.size() > 0)
				listEmpOutputBean = saveEmpDetails(listOfPermanent, permanent);

			if (listOfContract.size() > 0)
				listEmpOutputBean.addAll(saveEmpDetails(listOfContract, contract));

			if (listOfTrainee.size() > 0)
				listEmpOutputBean.addAll(saveEmpDetails(listOfTrainee, trainee));

		} catch (DataAccessException e) {

			throw new DataAccessException(e.getMessage());

		}
		return listEmpOutputBean;

	}

	public List<EmployeeDetailOutputBean> saveEmpDetails(List<EmployeeDetailInputBean> listOfInput, int newEmpId)
			throws DataAccessException {

		DatEmpDetailBO datEmpDetailBo;
		DatEmpPersonalDetailBO personalBo;
		DatEmpProfessionalDetailBO professionalBo;
		EmployeeDetailOutputBean empOutputBean;
		List<EmployeeDetailOutputBean> listEmpOutputBean = new ArrayList<EmployeeDetailOutputBean>();
		List<DatEmpDetailBO> listEmpDetailBo = new ArrayList<DatEmpDetailBO>();
		List<DatEmpPersonalDetailBO> listPersonalDetailBo = new ArrayList<DatEmpPersonalDetailBO>();
		List<DatEmpProfessionalDetailBO> listProfessionalDetailBo = new ArrayList<DatEmpProfessionalDetailBO>();

		if (listOfInput.size() > 0) {
			for (EmployeeDetailInputBean empBean : listOfInput) {
				datEmpDetailBo = new DatEmpDetailBO();
				personalBo = new DatEmpPersonalDetailBO();
				professionalBo = new DatEmpProfessionalDetailBO();

				newEmpId++;

				// Dat Employee table data
				datEmpDetailBo.setEmpType(empBean.getEmpType());
				datEmpDetailBo.setProfileCreatedBy(empBean.getLogginEmployee());
				datEmpDetailBo.setProfileCreatedDate(new Date());
				datEmpDetailBo.setFkEmpMainStatus((byte) 3);
				datEmpDetailBo.setFkEmpSubStatus((byte) 7);
				datEmpDetailBo.setPkEmpId(newEmpId);
				listEmpDetailBo.add(datEmpDetailBo);

				// Dat Employee personal table data
				personalBo.setEmpFirstName(empBean.getEmpFirstName());
				personalBo.setEmpMiddleName(empBean.getEmpMiddleName());
				personalBo.setEmpLastName(empBean.getEmpLastName());
				// personalBo.setEmpPersonalDetailModifiedBy(empBean.getLogginEmployee());
				// personalBo.setEmpPersonalDetailModifiedDate(new Date());
				personalBo.setFkEmpDetailId(newEmpId);
				listPersonalDetailBo.add(personalBo);

				// Dat Employee professional table data
				professionalBo.setFkEmpBootCampId(empBean.getBootCampId());
				professionalBo.setFkEmpBuUnit(empBean.getBuUnit());
				professionalBo.setFkEmpReportingMgrId(empBean.getReportingManagerId());
				professionalBo.setFkMainEmpDetailId(newEmpId);
				listProfessionalDetailBo.add(professionalBo);
				// }
			}

			try {
				Iterable<DatEmpDetailBO> empDetailBO = empDetailRepository.save(listEmpDetailBo);
				List<DatEmpDetailBO> listOfDatEmp = StreamSupport.stream(empDetailBO.spliterator(), false)
						.collect(Collectors.toList());

				Iterable<DatEmpProfessionalDetailBO> empDetailProBO = professionalRepository
						.save(listProfessionalDetailBo);
				List<DatEmpProfessionalDetailBO> listOfDatPro = StreamSupport
						.stream(empDetailProBO.spliterator(), false).collect(Collectors.toList());
				Iterable<DatEmpPersonalDetailBO> empDetailPerBO = personalRepository.save(listPersonalDetailBo);
				List<DatEmpPersonalDetailBO> listOfDatPer = StreamSupport.stream(empDetailPerBO.spliterator(), false)
						.collect(Collectors.toList());

				for (int i = 0; i < listOfDatEmp.size(); i++) {
					empOutputBean = new EmployeeDetailOutputBean();
					empOutputBean.setEmpId(listOfDatEmp.get(i).getPkEmpId());
					empOutputBean.setEmpName(
							listOfDatPer.get(i).getEmpFirstName() + " " + listOfDatPer.get(i).getEmpLastName());

					DatEmpProfessionalDetailBO proDetail = professionalRepository
							.findByFkMainEmpDetailId(listOfDatEmp.get(i).getPkEmpId());

					if (proDetail != null) {
						empOutputBean.setBuUnit(proDetail.getMasBuUnitBO().getBuUnitName());
						empOutputBean.setBootCamp(proDetail.getMasBootCampBO().getBootCampName());
					}

					empOutputBean.setBuUnitId(listOfDatPro.get(i).getFkEmpBuUnit());
					empOutputBean.setBootCampId(listOfDatPro.get(i).getFkEmpBootCampId());

					empOutputBean.setEmpTypeId(listOfDatEmp.get(i).getEmpType());

					if (listOfDatEmp.get(i).getEmpType() == 1) {
						empOutputBean.setEmpType("Permanent");
					} else if (listOfDatEmp.get(i).getEmpType() == 2) {
						empOutputBean.setEmpType("Contract");
					} else if (listOfDatEmp.get(i).getEmpType() == 3) {
						empOutputBean.setEmpType("FTE");
					} else if (listOfDatEmp.get(i).getEmpType() == 4) {
						empOutputBean.setEmpType("Trainee");
					}
					empOutputBean.setReportingManagerId(listOfDatPro.get(i).getFkEmpReportingMgrId());
					String rmName = gsrGoalService.getEmpNameByEmpId(listOfDatPro.get(i).getFkEmpReportingMgrId());
					if (rmName != null) {
						empOutputBean.setReportingManagerName(rmName);
					}

					if (listOfDatEmp.size() - 1 == i) {
						DatEmpLastEmployeeIdBO lastEmpIdBO;
						if (empOutputBean.getEmpTypeId() == 1 || empOutputBean.getEmpTypeId() == 3) {
							lastEmpIdBO = empLastEmployeeIdRepository.findByEmpTypeId(1);
						} else {
							lastEmpIdBO = empLastEmployeeIdRepository
									.findByEmpTypeId((int) empOutputBean.getEmpTypeId());
						}

						lastEmpIdBO.setLastEmployeeId(empOutputBean.getEmpId());
						empLastEmployeeIdRepository.save(lastEmpIdBO);
					}

					listEmpOutputBean.add(empOutputBean);
				}

			} catch (Exception e) {

				throw new DataAccessException("Exception occurred while saving data to DB " + e.getMessage());
			}

		}

		return listEmpOutputBean;

	}

	public Boolean createEmpEducation(List<EmployeeEducationalInputBean> empEduBean) throws DataAccessException {

		Boolean flag = false;
		DatEmpEducationDetailBO empEduBO;
		List<DatEmpEducationDetailBO> listEmpEduBO = new ArrayList<DatEmpEducationDetailBO>();
		try {
			if (empEduBean.size() > 0) {

				for (EmployeeEducationalInputBean eduBean : empEduBean) {
					empEduBO = new DatEmpEducationDetailBO();
					empEduBO.setEduLevel(eduBean.getEducationalLevel());
					empEduBO.setFkEmpId(eduBean.getEmpId());
					empEduBO.setNameOfUniversity(eduBean.getNameOfSchool());
					empEduBO.setPercentage(eduBean.getPercentage());
					empEduBO.setYearOfCompletion(eduBean.getYearOfCompletion());
					empEduBO.setFkSpecializationId(eduBean.getSpecilization());
					empEduBO.setFkStreamId(eduBean.getStream());
					empEduBO.setCreatedBy(eduBean.getLoggedInEmpId());
					empEduBO.setCreatedDate(new Date());
					listEmpEduBO.add(empEduBO);

				}
			}
			empEducationalRepository.save(listEmpEduBO);
			flag = true;
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			throw new DataAccessException(e.getMessage());
		}

		return flag;

	}

	/**
	 * 
	 * @param subStatusBean
	 * @return boolean
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This service is used to change Sub Status of a Active
	 *              Employee
	 */
	public boolean changeActiveEmpSubstatus(EmpSubStatusInputBean subStatusBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering changeActiveEmpSubstatus() service");
		boolean isStatusChanged = false;
		DatEmpDetailBO empDetail = null;
		DatEmpDetailBO updateEmpDetail = null;
		DatEmpDetailBO hasAccessEmpDetail = null;
		try {
			List<String> subStatusList = Arrays.asList(activeSubStatus.split(","));
			List<Byte> activeSubStatusList = subStatusList.stream().map(Byte::valueOf).collect(Collectors.toList());
			List<Short> subStatusChangeRoleIdList = Arrays.asList(subStatusChangeRoles.split(",")).stream()
					.map(Short::valueOf).collect(Collectors.toList());
			try {
				empDetail = empDetailRepository.findByPkEmpId(subStatusBean.getEmpId());
			} catch (Exception e) {
				throw new DataAccessException("Some Exception occured while getting Employee Detail from back end.");
			}
			if (empDetail != null) {
				if (empDetail.getFkEmpMainStatus() == empInactiveStatus
						|| empDetail.getFkEmpMainStatus() == empWaitingStatus)
					throw new CommonCustomException("Employee Profile is not Active");
				else {
					if (subStatusBean.getEmpSubStatus().equals(empDetail.getFkEmpSubStatus()))
						throw new CommonCustomException("Employee Profile substatus is already same");
					else {
						boolean idExists = activeSubStatusList.stream()
								.anyMatch(status -> status.equals(subStatusBean.getEmpSubStatus()));
						if (idExists) {
							hasAccessEmpDetail = empDetailRepository.findByPkEmpIdAndFkEmpRoleIdIn(
									subStatusBean.getLoggedInUserId(), subStatusChangeRoleIdList);

							if (hasAccessEmpDetail != null) {
								empDetail.setFkEmpSubStatus(subStatusBean.getEmpSubStatus());
								empDetail.setModifiedBy(subStatusBean.getLoggedInUserId());
								empDetail.setModifiedDate(new Date());
								try {
									updateEmpDetail = empDetailRepository.save(empDetail);
								} catch (Exception e) {
									throw new DataAccessException(
											"Some exception occurred while deactivating employee profile");
								}
								if (updateEmpDetail != null)
									isStatusChanged = true;
							} else {
								throw new CommonCustomException(
										"Access denied because you are not an authorized person.");
							}
						} else {
							throw new CommonCustomException("Invaild Sub Status. Please enter the proper Sub Status.");
						}
					}
				}
			} else {
				throw new CommonCustomException("Given employee Id does't exist, Please pass proper employee Id");
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("Exiting changeActiveEmpSubstatus() service");
		return isStatusChanged;
	}

	/**
	 * 
	 * @param searchBean
	 * @return List<AdvancedSearchOutputBean>
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This method is used for Advanced Search by HR.
	 */
	public List<AdvancedSearchOutputBean> advancedSearchByHr(AdvancedSearchInputBean searchBean)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entering advancedSearchByHr() service ");
		List<AdvancedSearchOutputBean> resultList = new ArrayList<AdvancedSearchOutputBean>();
		List<DatEmpPersonalDetailBO> perDetailList = new ArrayList<DatEmpPersonalDetailBO>();
		List<DatEmpProfessionalDetailBO> profDetailList = new ArrayList<DatEmpProfessionalDetailBO>();
		Integer empId = null;
		String empName = null;
		Date startDate = null;
		Date endDate = null;
		List<Integer> empIdList = new ArrayList<Integer>();
		String errMsg = "";
		boolean hasEnteredFlag = false;
		try {
			try {
				errMsg = validateAdvancedSearchBean(searchBean);

				if (!errMsg.isEmpty())
					throw new CommonCustomException(errMsg);
			} catch (DataAccessException e) {
				throw new DataAccessException(e.getMessage());
			}
			if (searchBean.getStartDate() != null || searchBean.getEndDate() != null) {
				startDate = DateTimeUtil.dateWithMinTimeofDay(searchBean.getStartDate());
				endDate = DateTimeUtil.dateWithMaxTimeofDay(searchBean.getEndDate());
				searchBean.setStartDate(startDate);
				searchBean.setEndDate(endDate);
			}

			if (searchBean.getEmpIdOrName() != null) {
				hasEnteredFlag = true;
				boolean isEmpId = searchBean.getEmpIdOrName().trim().matches("[0-9]*");
				if (isEmpId)
					empId = Integer.parseInt(searchBean.getEmpIdOrName());
				else
					empName = searchBean.getEmpIdOrName();
				if (empId != null) {
					empIdList.clear();
					empIdList.add(empId);
				}
				if (empName != null) {
					try {
						perDetailList = personalRepository.getEmployeeByEmpName(empName, null);
					} catch (Exception e) {
						throw new DataAccessException(
								"Some exception occured while fetching employee's by employee name");
					}
					if (!perDetailList.isEmpty()) {
						empIdList.clear();
						empIdList.addAll(perDetailList.stream().map(DatEmpPersonalDetailBO::getFkEmpDetailId)
								.collect(Collectors.toList()));
						perDetailList.clear();
					}
				}
			}

			if (searchBean.getBuId() != null || searchBean.getLocationId() != null || searchBean.getLevelId() != null
					|| searchBean.getDesignationId() != null || searchBean.getBootcampId() != null
					|| searchBean.getStartDate() != null || searchBean.getEndDate() != null
					|| searchBean.getCountryId() != null || searchBean.getEmployementType() != null
					|| searchBean.getEmpStatus() != null || searchBean.getEmpSubStatus() != null) {
				Specification<DatEmpProfessionalDetailBO> empProfDetailsSpecification = null;

				if (!empIdList.isEmpty()) {
					empProfDetailsSpecification = AdvancedSearchSpecification.getEmpProfessionalDetails(searchBean,
							empIdList);
					profDetailList = empProfRepository.findAll(empProfDetailsSpecification);
				} else {
					if (!hasEnteredFlag && empIdList.isEmpty()) {
						empProfDetailsSpecification = AdvancedSearchSpecification.getEmpProfessionalDetails(searchBean,
								new ArrayList<Integer>());
						profDetailList = empProfRepository.findAll(empProfDetailsSpecification);
					} else {
						return resultList;
					}
				}
				if (!profDetailList.isEmpty()) {
					empIdList.clear();
					empIdList.addAll(profDetailList.stream().map(DatEmpProfessionalDetailBO::getFkMainEmpDetailId)
							.collect(Collectors.toList()));
					profDetailList.clear();
				} else {
					empIdList.clear();
				}
				hasEnteredFlag = true;
			}
			if (searchBean.getGenderId() != null) {
				perDetailList.clear();
				Specification<DatEmpPersonalDetailBO> empPerDetailsSpecification = null;

				if (!empIdList.isEmpty()) {
					empPerDetailsSpecification = AdvancedSearchSpecification.getEmpPersonalDetails(searchBean,
							empIdList);
					perDetailList = personalRepository.findAll(empPerDetailsSpecification);
				} else {
					if (!hasEnteredFlag && empIdList.isEmpty()) {
						empPerDetailsSpecification = AdvancedSearchSpecification.getEmpPersonalDetails(searchBean,
								new ArrayList<Integer>());
						perDetailList = personalRepository.findAll(empPerDetailsSpecification);
					} else {
						return resultList;
					}
				}
				if (!perDetailList.isEmpty()) {
					empIdList.clear();
					empIdList.addAll(perDetailList.stream().map(DatEmpPersonalDetailBO::getFkEmpDetailId)
							.collect(Collectors.toList()));
					perDetailList.clear();
				} else {
					empIdList.clear();
				}
			}
			if (!empIdList.isEmpty()) {
				resultList = empDetailRepository.getAdvancedSearchResults(empIdList);
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("Exiting advancedSearchByHr() service");
		return resultList;
	}

	/**
	 * 
	 * @param searchBean
	 * @return String
	 * @throws DataAccessException
	 * @Description : Validator for Advanced Search
	 */
	public String validateAdvancedSearchBean(AdvancedSearchInputBean searchBean) throws DataAccessException {
		LOG.startUsecase("Entering validateAdvancedSearchBean() validator");
		String errMsg = "";
		MasEmpMainStatusBO empMainStatus = null;
		MasEmpSubStatusBO empSubstatus = null;
		try {
			if (searchBean.getStartDate() == null && searchBean.getEndDate() == null
					&& searchBean.getEmpIdOrName() == null && searchBean.getEmployementType() == null
					&& searchBean.getEmpStatus() == null && searchBean.getEmpSubStatus() == null
					&& searchBean.getBuId() == null && searchBean.getLocationId() == null
					&& searchBean.getLevelId() == null && searchBean.getDesignationId() == null
					&& searchBean.getBootcampId() == null && searchBean.getStartDate() == null
					&& searchBean.getEndDate() == null && searchBean.getCountryId() == null
					&& searchBean.getGenderId() == null) {
				return "Invalid Input. Any one of the search criteria is mandatory";
			}
			if (searchBean.getEmployementType() != null) {
				List<Byte> employmentTypeList = Arrays.asList(employementType.split(",")).stream().map(Byte::valueOf)
						.collect(Collectors.toList());
				boolean idExists = employmentTypeList.stream()
						.anyMatch(status -> status.equals(searchBean.getEmployementType()));
				if (!idExists)
					return "Invalid Employement Type";
			}
			if (searchBean.getEmpStatus() != null || searchBean.getEmpSubStatus() != null) {
				if (searchBean.getEmpStatus() != null && searchBean.getEmpSubStatus() != null) {
					try {
						empSubstatus = empSubStatusRepository.findByPkEmpSubStatusIdAndFkEmpMainStatus(
								searchBean.getEmpSubStatus(), searchBean.getEmpStatus());
					} catch (Exception e) {
						throw new DataAccessException(
								"Some exception occurred while validating Emp Main and Sub Status");
					}
					if (empSubstatus == null)
						return "Sub status does not match the Main status";
				} else if (searchBean.getEmpStatus() != null) {
					try {
						empMainStatus = empMainStatusRepository.findByPkEmpMainStatusId(searchBean.getEmpStatus());
					} catch (Exception e) {
						throw new DataAccessException("Some exception occurred while validating Emp Main Status");
					}
					if (empMainStatus == null)
						return "Invalid Main Status Id.";

				} else if (searchBean.getEmpSubStatus() != null) {
					try {
						empSubstatus = empSubStatusRepository.findByPkEmpSubStatusId(searchBean.getEmpSubStatus());
					} catch (Exception e) {
						throw new DataAccessException("Some exception occurred while validating Emp Sub Status");
					}
					if (empMainStatus == null)
						return "Invalid Sub Status Id.";
				}
			}
			if (searchBean.getBuId() != null) {
				try {
					if (buRepository.findByPkBuUnitId(searchBean.getBuId()) == null)
						return "Invalid BU Id";
				} catch (Exception e) {
					throw new DataAccessException("Some exception occurred while validating BU Id");
				}
			}
			if (searchBean.getCountryId() != null || searchBean.getLocationId() != null) {
				if (searchBean.getCountryId() != null && searchBean.getLocationId() != null) {
					try {
						if (parentLocRepository.findByPkLocationParentIdAndFkCountryId(searchBean.getLocationId(),
								searchBean.getCountryId()) == null)
							return "Location Id does not match Country Id";
					} catch (Exception e) {
						throw new DataAccessException(
								"Some exception occurred while validating Country Id and Location Id");
					}
				} else if (searchBean.getCountryId() != null) {
					try {
						if (countryRepository.findByPkCountryId(searchBean.getCountryId()) == null)
							return "Invalid Country Id";
					} catch (Exception e) {
						throw new DataAccessException("Some exception occurred while validating Country Id");
					}
				} else if (searchBean.getLocationId() != null) {
					try {
						if (parentLocRepository.findByPkLocationParentId(searchBean.getLocationId()) == null)
							return "Invalid Parent Location Id";
					} catch (Exception e) {
						throw new DataAccessException("Some exception occurred while validating Parent Location Id");
					}
				}
			}
			if (searchBean.getLevelId() != null) {
				try {
					if (empLevelRepository.findByPkEmpLevelId(searchBean.getLevelId()) == null)
						return "Invalid Level Id";
				} catch (Exception e) {
					throw new DataAccessException("Some exception occurred while validating Level Id");
				}
			}
			if (searchBean.getDesignationId() != null) {
				try {
					if (empDesignationRepository.findByPkEmpDesignationId(searchBean.getDesignationId()) == null)
						return "Invalid Designation Id";
				} catch (Exception e) {
					throw new DataAccessException("Some exception occurred while validating Designation Id");
				}
			}
			if (searchBean.getBootcampId() != null) {
				try {
					if (bootCampRepository.findByPkBootCampId(searchBean.getBootcampId()) == null)
						return "Invalid Boot Camp Id";
				} catch (Exception e) {
					throw new DataAccessException("Some exception occurred while validating Boot Camp Id");
				}
			}
			if (searchBean.getGenderId() != null) {
				List<Byte> employmentTypeList = Arrays.asList(empGender.split(",")).stream().map(Byte::valueOf)
						.collect(Collectors.toList());
				boolean idExists = employmentTypeList.stream()
						.anyMatch(status -> status.equals(searchBean.getGenderId()));
				if (!idExists)
					return "Invalid Gender Type";
			}
			if (searchBean.getStartDate() != null || searchBean.getEndDate() != null) {
				if (searchBean.getEndDate() == null) {
					return "End date is a mandatory field.";
				}
				if (searchBean.getStartDate() == null) {
					return "Start date is a mandatory field.";
				}
				if (searchBean.getStartDate() != null && searchBean.getEndDate() != null) {
					if (searchBean.getStartDate().after(searchBean.getEndDate())) {
						return "Start date must be less than end date";
					}
				}
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("Exiting validateAdvancedSearchBean() validator");
		return errMsg;
	}

	// End of Addition by Kamal Anand

	// Addition By Adavayya Hiremath
	/**
	 * 
	 * <Description updateOrganizationDetailsHR:> This service will be called
	 * from the front-end when the user wants to update Organizational Details
	 * of Employee
	 * 
	 * @param orgDetailBean
	 * @return MIS Response
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	public Boolean updateOrganizationDetail(OrganizationDetailBean orgDetailBean)
			throws DataAccessException, CommonCustomException {
		LOG.info("About to update data : ");
		Boolean success = false;

		try{
			// Collect all role ids of System HR.
			List<String> hrRoleIdList = new ArrayList<String>();
			String[] empIdArray = hrRoles.split(",");
			for (String s : empIdArray)
				hrRoleIdList.add(s);
			DatEmpDetailBO hrEmpDetailBo = new DatEmpDetailBO();
		
			DatEmpProfessionalDetailBO profDetails = new DatEmpProfessionalDetailBO();
			DatEmpDetailBO empDetail = new DatEmpDetailBO();
	
			try{
				hrEmpDetailBo = empDetailRepository
						.getSystemAdminRoleDetailsForUserManagmentModule(orgDetailBean.getLoggedInEmpId(), hrRoleIdList);
			
				profDetails = empProfRepository.findByFkMainEmpDetailId(orgDetailBean.getEmpId());
				empDetail = empDetailRepository.findByPkEmpId(orgDetailBean.getEmpId());
			}catch(Exception d){
				throw new DataAccessException("Exception occurred while fetching employee details from DB ");
			}
		if (hrEmpDetailBo != null && hrEmpDetailBo.getFkEmpMainStatus() != 2) {
		if (profDetails != null && empDetail != null) {
			profDetails.setFkMainEmpDetailId(orgDetailBean.getEmpId());
			profDetails.setEmpDateOfJoining(orgDetailBean.getDateOfJoining());
			profDetails.setFkEmpBootCampId(orgDetailBean.getBootCampId());
			profDetails.setProbationMonths(orgDetailBean.getProbationPeriod());
			
			if(orgDetailBean.getEmploymentType() == "Permanent")
			{
				empDetail.setEmpType((byte)1);
			}
			else if(orgDetailBean.getEmploymentType() == "Contract")
			{
				empDetail.setEmpType((byte)2);
			}
			else if(orgDetailBean.getEmploymentType() == "FTE")
			{
				empDetail.setEmpType((byte)3);
			}
			else if(orgDetailBean.getEmploymentType() == "Trainee")
			{
				empDetail.setEmpType((byte)4);
			}
			profDetails.setFkEmpOrganizationId(orgDetailBean.getOrganisation());
			profDetails.setFkEmpLocationParentId(orgDetailBean.getCurrentLocation());
			profDetails.setFkEmpBuUnit(orgDetailBean.getBusinessUnitId());
			profDetails.setEmpRoleId(orgDetailBean.getRoleId());
			profDetails.setFkEmpDesignation(orgDetailBean.getDesignation());
			profDetails.setFkEmpLevel(orgDetailBean.getLevel());
			profDetails.setFkEmpDomainMgrId(orgDetailBean.getDomainManager());
			profDetails.setFkEmpReportingMgrId(orgDetailBean.getReportingManager());
			profDetails.setEffectiveDateOfReportingMgrChange(orgDetailBean.getDateOfReporting());
			profDetails.setEmpContractEndDate(orgDetailBean.getEndDateForFTE());	
			profDetails.setOnsiteOrOffshore(orgDetailBean.getOnsiteOrOffshore());
			profDetails.setEmpLoc(orgDetailBean.getEmpLoc());
			if(empDetail.getFkEmpMainStatus() == empWaitingStatus && profDetails.getEmpProfessionalEmailId() != null){
	
					empDetail.setFkEmpMainStatus(empActiveStatus);
				}

				empProfRepository.save(profDetails);
				empDetailRepository.save(empDetail);
			} else {

				throw new CommonCustomException("Employee data does't exist's");
			}

			
			if(profDetails.getFkEmpOrganizationId() == 1){
				if(orgDetailBean.getTupeStatus().equals("YES")){
					profDetails.setTupeStatus(orgDetailBean.getTupeStatus());
					if(orgDetailBean.getTupeOrganisation() != null){
						profDetails.setTupeOrganisation(orgDetailBean.getTupeOrganisation());
					}
				}
			}
	
			empProfRepository.save(profDetails);
			empDetailRepository.save(empDetail);

		}
		 else {
				throw new CommonCustomException("You are not Authorized to change the details");
			}
		success = true;
		}catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("About to update Data is completed");
		return success;
	}

	public Boolean updateLocationDetail(OrganizationDetailBean orgDetailBean)
			throws DataAccessException, CommonCustomException {
		LOG.info("About to update data : ");
		Boolean success = false;

		// Collect all role ids of System HR.
		List<String> projCordRoleIdList = new ArrayList<String>();
		String[] empIdArray = projCoOrdinatorRole.split(",");
		for (String s : empIdArray)
			projCordRoleIdList.add(s);
		DatEmpDetailBO projCordinatorDetailBo = new DatEmpDetailBO();

		projCordinatorDetailBo = empDetailRepository
				.getSystemAdminRoleDetailsForUserManagmentModule(orgDetailBean.getLoggedInEmpId(), projCordRoleIdList);

		if (projCordinatorDetailBo != null && projCordinatorDetailBo.getFkEmpMainStatus() != 2) {
			DatEmpProfessionalDetailBO profDetails = empProfRepository
					.findByFkMainEmpDetailId(orgDetailBean.getEmpId());
			DatEmpDetailBO empDetail = empDetailRepository.findByPkEmpId(orgDetailBean.getEmpId());

			if (profDetails != null && empDetail != null) {
				profDetails.setFkMainEmpDetailId(orgDetailBean.getEmpId());
				profDetails.setFkEmpLocationParentId(orgDetailBean.getCurrentLocation());
				profDetails.setProfessionalDetailModifiedBy(orgDetailBean.getLoggedInEmpId());
				empProfRepository.save(profDetails);
				empDetailRepository.save(empDetail);
			} else {

				throw new CommonCustomException("Employee data does't exist's");
			}
		} else {
			throw new CommonCustomException("You are not Project Co-Ordinator to change the details");
		}
		success = true;
		LOG.endUsecase("About to update Data is completed");
		return success;
	}
	// EOD By Adavayya Hiremath

	public EmployeeProfileDetailBean getEmpDetails(Integer empId) {
		LOG.entering("Entering getEmpDetails service method");
		EmployeeProfileDetailBean empProfileBean = new EmployeeProfileDetailBean();
		DatEmpPersonalDetailBO personalBo = new DatEmpPersonalDetailBO();
		DatEmpProfessionalDetailBO profBo = new DatEmpProfessionalDetailBO();
		List<DatEmpEmploymentDetailBO> listOfEmployersBo = new ArrayList<DatEmpEmploymentDetailBO>();
		List<DatEmpEducationDetailBO> listOfEducationalBo = new ArrayList<DatEmpEducationDetailBO>();
		DatEmpKycDetailBO kycBo = new DatEmpKycDetailBO();
		EmployeeKYCInputBean kycInputBean = new EmployeeKYCInputBean();
		EmployeePersonalInputBean personalDetail = new EmployeePersonalInputBean();
		List<CreateEmpProfDetailBean> professionalDetails = new ArrayList<CreateEmpProfDetailBean>();
		List<EmployeeEducationalOutputBean> educationalDetail = new ArrayList<EmployeeEducationalOutputBean>();
		EmployeeOrganizationOutputBean organizationDetail = new EmployeeOrganizationOutputBean();
		try {
			personalBo = personalRepository.findByFkEmpDetailId(empId);
			listOfEmployersBo = datEmpEmploymentRepository.findByFkEmpId(empId);
			listOfEducationalBo = empEducationalRepository.findByFkEmpId(empId);
			profBo = professionalRepository.findByFkMainEmpDetailId(empId);
			try {
				kycBo = empKycRepository.findByFkEmpDetailId(empId);
			} catch (Exception e) {
				e.printStackTrace();
				throw new DataAccessException(
						"Some exception occured while fetching employee's KYC detail by employee Id");
			}

			if (personalBo != null) {
				personalDetail.setBloodGroup(personalBo.getEmpBloodGroup());
				personalDetail.setDateOfBirth(personalBo.getEmpDateOfBirth());
				personalDetail.setEmpFirstName(personalBo.getEmpFirstName());
				personalDetail.setEmpId(personalBo.getFkEmpDetailId());
				personalDetail.setEmpLastName(personalBo.getEmpLastName());
				personalDetail.setEmpMiddleName(personalBo.getEmpMiddleName());
				personalDetail.setGender(personalBo.getEmpGender());
				personalDetail.setHomePhone(personalBo.getEmpAlternateMobileNo());
				personalDetail.setMartialStatus(personalBo.getEmpMaritalStatus());
				personalDetail.setMobileNumber(personalBo.getEmpMobileNo());
				personalDetail.setPersonalEmail(personalBo.getEmpPersonalEmailId());
				personalDetail.setSpouseDOB(personalBo.getEmpSpouseDateOfBirth());
				personalDetail.setSpouseName(personalBo.getEmpSpouseName());

				personalDetail.setPermanentAddressLineOne(personalBo.getEmpPermanentAddressLineOne());
				personalDetail.setPermanentAddressLineTwo(personalBo.getEmpPermanentAddressLineTwo());
				personalDetail.setPermanentAddressCity(personalBo.getEmpPermanentAddressCity());
				personalDetail.setPermanentAddressState(personalBo.getEmpPermanentAddressState());
				personalDetail.setPermanentAddressCountry(personalBo.getEmpPermanentAddressCountry());
				personalDetail.setPresentAddressLineOne(personalBo.getEmpPresentAddressLineOne());
				personalDetail.setPresentAddressLineTwo(personalBo.getEmpPresentAddressLineTwo());
				personalDetail.setPresentAddressCity(personalBo.getEmpPresentAddressCity());
				personalDetail.setPresentAddressState(personalBo.getEmpPresentAddressState());
				personalDetail.setPresentAddressCountry(personalBo.getEmpPresentAddressCountry());

				if (kycBo != null) {

					kycInputBean.setAadharNo(kycBo.getAadharNo());
					kycInputBean.setEmpNameAsPerAadharCard(kycBo.getEmpNameAsPerAadharCard());
					kycInputBean.setEmpNameAsPerPanCard(kycBo.getEmpNameAsPerPanCard());
					kycInputBean.setEmpNameAsPerPassPort(kycBo.getEmpNameAsPerPassport());
					kycInputBean.setEmpNameAsPerVoterId(kycBo.getEmpNameAsPerVoterId());
					kycInputBean.setPanCardNumber(kycBo.getEmpPanNo());
					kycInputBean.setPassportNumber(kycBo.getEmpPassportNo());
					kycInputBean.setVoterId(kycBo.getEmpVoterIdNo());
					kycInputBean.setHasUanFlag(kycBo.getHasUanFlag());
					kycInputBean.setDateOfIssue(kycBo.getPassportDateOfIssue());
					kycInputBean.setDateOfExpiry(kycBo.getPassportExpiryDate());
					kycInputBean.setPlaceOfIssue(kycBo.getPassportplaceOfIssue());
					// kycInputBean.setKycUpdatedOn(kycBo.get);
					// kycInputBean.set(kycBo.getFkEmpDetailId());
					// kycInputBean.setKycCreatedOn(new Date());

					personalDetail.setKycInputBean(kycInputBean);
				}

			}
			CreateEmpProfDetailBean employerBean;
			if (listOfEmployersBo.size() > 0) {
				for (DatEmpEmploymentDetailBO employer : listOfEmployersBo) {
					employerBean = new CreateEmpProfDetailBean();
					employerBean.setDurationFromDate(employer.getStartDate());
					employerBean.setDurationToDate(employer.getEndDate());
					employerBean.setEmpId(employer.getFkEmpId());
					employerBean.setEmployerName(employer.getEmpEmployerName());
					employerBean.setPositionHeld(employer.getEmpEmploymentDesg());
					employerBean.setReasonForLeaving(employer.getReasonForLeaving());
					professionalDetails.add(employerBean);
				}
			}
			EmployeeEducationalOutputBean educationalBean;
			if (listOfEducationalBo.size() > 0) {

				for (DatEmpEducationDetailBO educational : listOfEducationalBo) {
					educationalBean = new EmployeeEducationalOutputBean();

					educationalBean.setEducationalLevelId(educational.getEduLevel());

					for (EducationalLevel name : EducationalLevel.values()) {

						if (name.value == educational.getEduLevel()) {
							educationalBean.setEducationalLevel(name.toString());

						}
					}

					educationalBean.setEmpId(educational.getFkEmpId());
					educationalBean.setNameOfSchool(educational.getNameOfUniversity());
					educationalBean.setPercentage(educational.getPercentage());
					educationalBean.setSpecilizationId(educational.getFkSpecializationId());
					if (educational.getFkSpecializationId() != null)
						educationalBean.setSpecilization(educational.getMasEmpSpecialization().getSpecializationName());

					educationalBean.setStreamId(educational.getFkStreamId());
					if (educational.getFkStreamId() != null)
						educationalBean.setStream(educational.getMasEmpStream().getStreamName());
					educationalBean.setYearOfCompletion(educational.getYearOfCompletion());

					educationalDetail.add(educationalBean);
				}
			}

			if (profBo != null) {
				organizationDetail.setBootCampId(profBo.getFkEmpBootCampId());
				organizationDetail.setBootCampName(profBo.getMasBootCampBO().getBootCampName());
				organizationDetail.setBusinessUnit(profBo.getMasBuUnitBO().getBuUnitName());
				organizationDetail.setBusinessUnitId(profBo.getFkEmpBuUnit());
				organizationDetail.setCurrentLocation(profBo.getFkEmpLocationParentId());
				if (profBo.getFkEmpLocationParentId() != null)
					organizationDetail.setCurrentLocationName(profBo.getMasLocationParentBO().getLocationParentName());
				organizationDetail.setDateOfJoining(profBo.getEmpDateOfJoining());
				organizationDetail.setDateOfReporting(profBo.getEffectiveDateOfReportingMgrChange());
				organizationDetail.setDesignation(profBo.getFkEmpDesignation());
				if (profBo.getFkEmpDesignation() != null)
					organizationDetail.setDesignationName(profBo.getMasEmpDesignationBO().getEmpDesignationName());
				organizationDetail.setDomainManager(profBo.getFkEmpDomainMgrId());

				String dmName = gsrGoalService.getEmpNameByEmpId(profBo.getFkEmpDomainMgrId());
				if (dmName != null) {
					organizationDetail.setDomainManagerName(dmName);
				}
				organizationDetail.setEmailAddress(profBo.getEmpProfessionalEmailId());
				organizationDetail.setEmpId(profBo.getFkMainEmpDetailId());
				organizationDetail.setOnsiteOrOffshore(profBo.getOnsiteOrOffshore());
				organizationDetail.setEmpLoc(profBo.getEmpLoc());

				organizationDetail.setEmploymentStatus(profBo.getDatEmpDetailBO_main_emp_id().getFkEmpMainStatus());
				organizationDetail.setEmploymentStatusName(
						profBo.getDatEmpDetailBO_main_emp_id().getMasEmpMainStatusBO().getEmpMainStatusName());
				organizationDetail.setEmploymentTypeId(profBo.getDatEmpDetailBO_main_emp_id().getEmpType());
				for (EmployeeType type : EmployeeType.values()) {

					if (type.value == profBo.getDatEmpDetailBO_main_emp_id().getEmpType()) {

						organizationDetail.setEmploymentType(type.toString());

					}
				}

				if (profBo.getEmpPreviousExperience() != null)
					organizationDetail
							.setEmployeeType(profBo.getEmpPreviousExperience() > 0 ? "Experienced" : "Fresher");
				organizationDetail.setEndDateForFTE(profBo.getEmpContractEndDate());
				organizationDetail.setLevel(profBo.getFkEmpLevel());
				if (profBo.getFkEmpLevel() != null)
					organizationDetail.setLevelName(profBo.getMasEmpLevelBO().getEmpLevelName());
				organizationDetail.setOrganisation(profBo.getFkEmpOrganizationId());
				if (profBo.getFkEmpOrganizationId() != null)
					organizationDetail.setOrgName(profBo.getMasOrganizationBO().getOrganizationName());
				organizationDetail.setProbationPeriod(profBo.getProbationMonths());
				organizationDetail.setReportingManager(profBo.getFkEmpReportingMgrId());
				String rmName = gsrGoalService.getEmpNameByEmpId(profBo.getFkEmpReportingMgrId());
				if (rmName != null) {
					organizationDetail.setReportingManagerName(rmName);
				}

				if (profBo.getDatEmpDetailBO_main_emp_id().getFkEmpRoleId() != null)
					organizationDetail
							.setRole(profBo.getDatEmpDetailBO_main_emp_id().getMasEmpRoleBO().getEmpRoleName());
				organizationDetail.setRoleId(profBo.getDatEmpDetailBO_main_emp_id().getFkEmpRoleId());
				empProfileBean.setEmployeeStatusId(profBo.getDatEmpDetailBO_main_emp_id().getFkEmpMainStatus());
				empProfileBean.setEmployeeStatus(
						profBo.getDatEmpDetailBO_main_emp_id().getMasEmpMainStatusBO().getEmpMainStatusName());
				empProfileBean.setEmployeeSubStatus(
						profBo.getDatEmpDetailBO_main_emp_id().getMasEmpSubStatusBO().getEmpSubStatusName());
				empProfileBean.setEmployeeSubStatusId(profBo.getDatEmpDetailBO_main_emp_id().getFkEmpSubStatus());
				empProfileBean.setExtensionNumber(profBo.getEmpExtensionNumber());
			}

			String path = kycBasePath + empId;

			String photoPath = photoBasePath + empId + ".jpg";

			File file = new File(path);
			File photoFile = new File(photoPath);

			personalDetail.setIsProfilePhotoAvailable(false);
			personalDetail.setIsAadharAvailable(false);
			personalDetail.setIsPanCardAvailable(false);
			personalDetail.setIsPassportAvailable(false);
			personalDetail.setIsVoterIdAvailable(false);

			File[] listOfFiles = file.listFiles();

			if (file.isDirectory()) {
				if (listOfFiles.length > 0) {

					for (File f : listOfFiles) {
						if (f.getName().contains(aadharFileName)) {
							personalDetail.setIsAadharAvailable(true);
							personalDetail.setAadharPath(userDocUrl + empId + "/" + f.getName());

						} else if (f.getName().contains(panCardFileName)) {
							personalDetail.setIsPanCardAvailable(true);
							personalDetail.setPanCardPath(userDocUrl + empId + "/" + f.getName());
						} else if (f.getName().contains(passPortFileName)) {
							personalDetail.setIsPassportAvailable(true);
							personalDetail.setPassPortPath(userDocUrl + empId + "/" + f.getName());

						} else if (f.getName().contains(voterFileName)) {
							personalDetail.setIsVoterIdAvailable(true);
							personalDetail.setVoterIdPath(userDocUrl + empId + "/" + f.getName());
						}
					}

				}
			}
			if (photoFile.exists()) {
				if (photoFile.isFile()) {

					if (photoFile.getName().contains(empId.toString())) {
						personalDetail.setIsProfilePhotoAvailable(true);
						personalDetail.setProfilePhotoPath(userPhotoUrl + photoFile.getName());
					}

				}
			}

			empProfileBean.setPersonalDetail(personalDetail);
			empProfileBean.setEducationalDetail(educationalDetail);
			empProfileBean.setProfessionalDetails(professionalDetails);
			empProfileBean.setOrganizationDetail(organizationDetail);

		} catch (Exception e) {
			e.printStackTrace();
		}

		LOG.exiting("Existing getEmpDetails service method");
		return empProfileBean;

	}

	public Boolean updatePersonalEmploymentDetail(EmployeePersonalInputBean personalBean, MultipartFile passPort,
			MultipartFile voterId, MultipartFile aadhar, MultipartFile panCard)
			throws DataAccessException, CommonCustomException {
		DatEmpPersonalDetailBO personalBo = new DatEmpPersonalDetailBO();
		DatEmpKycDetailBO kycBo = new DatEmpKycDetailBO();
		Boolean flag = false;

		Map<String, MultipartFile> listOfMapFiles = new HashMap<String, MultipartFile>();
		try {

			if (panCard != null && !panCard.isEmpty())
				listOfMapFiles.put(panCardFileName, panCard);
			if (aadhar != null && !aadhar.isEmpty())
				listOfMapFiles.put(aadharFileName, aadhar);
			if (voterId != null && !voterId.isEmpty())
				listOfMapFiles.put(voterFileName, voterId);
			if (passPort != null && !passPort.isEmpty())
				listOfMapFiles.put(passPortFileName, passPort);

			if (listOfMapFiles.size() > 0) {

				for (Map.Entry<String, MultipartFile> map : listOfMapFiles.entrySet()) {
					long fileSizeInKB = map.getValue().getSize() / 1024;

					if (!map.getValue().getOriginalFilename().toLowerCase().endsWith(".pdf")
							&& !map.getValue().getOriginalFilename().toLowerCase().endsWith(".jpeg")
							&& !map.getValue().getOriginalFilename().toLowerCase().endsWith(".jpg")
							&& !map.getValue().getOriginalFilename().toLowerCase().endsWith(".png")) {
						throw new CommonCustomException(
								"uploaded file is in different format, Please upload only pdf,jpeg,jpg or png files");
					} else if (fileSizeInKB > 1024) {
						throw new CommonCustomException("uploaded file should be 1 MB or less than 1 MB");
					}
				}

			}

			try {
				personalBo = personalRepository.findByFkEmpDetailId(personalBean.getEmpId());

			} catch (Exception e) {
				throw new DataAccessException("Some exception occured while fetching employee's detail by employee Id");
			}

			try {

				kycBo = empKycRepository.findByFkEmpDetailId(personalBean.getEmpId());
			} catch (Exception e) {
				e.printStackTrace();
				throw new DataAccessException(
						"Some exception occured while fetching employee's KYC detail by employee Id");
			}

			if (personalBo != null) {

				personalBo.setEmpAlternateMobileNo(personalBean.getHomePhone());
				personalBo.setEmpBloodGroup(personalBean.getBloodGroup());
				personalBo.setEmpDateOfBirth(personalBean.getDateOfBirth());
				personalBo.setEmpFirstName(personalBean.getEmpFirstName());
				personalBo.setEmpGender(personalBean.getGender());
				personalBo.setEmpLastName(personalBean.getEmpLastName());
				personalBo.setEmpMaritalStatus(personalBean.getMartialStatus());
				personalBo.setEmpMiddleName(personalBean.getEmpMiddleName());
				personalBo.setEmpMobileNo(personalBean.getMobileNumber());
				personalBo.setEmpPermanentAddressLineOne(personalBean.getPermanentAddressLineOne());
				personalBo.setEmpPermanentAddressLineTwo(personalBean.getPermanentAddressLineTwo());
				personalBo.setEmpPermanentAddressCity(personalBean.getPermanentAddressCity());
				personalBo.setEmpPermanentAddressState(personalBean.getPermanentAddressState());
				personalBo.setEmpPermanentAddressCountry(personalBean.getPermanentAddressCountry());
				personalBo.setEmpPersonalEmailId(personalBean.getPersonalEmail());
				personalBo.setEmpPresentAddressLineOne(personalBean.getPresentAddressLineOne());
				personalBo.setEmpPresentAddressLineTwo(personalBean.getPresentAddressLineTwo());
				personalBo.setEmpPresentAddressCity(personalBean.getPresentAddressCity());
				personalBo.setEmpPresentAddressState(personalBean.getPermanentAddressState());
				personalBo.setEmpPresentAddressCountry(personalBean.getPresentAddressCountry());

			personalBo.setEmpSpouseDateOfBirth(personalBean.getSpouseDOB());
			personalBo.setFkEmpDetailId(personalBean.getEmpId());
			personalBo.setEmpSpouseName(personalBean.getSpouseName());
			personalBo.setEmpPersonalDetailModifiedBy(personalBean.getLoggedInEmpId());
			personalBo.setEmpPersonalDetailModifiedDate(new Date());
	
				if (kycBo != null) {

					if (kycBo.getAadharNo() != null && (personalBean.getKycInputBean().getAadharNo() == null
							|| personalBean.getKycInputBean().getAadharNo().trim().isEmpty())) {
						throw new CommonCustomException("Aadhar number can't be empty, please pass proper data");
					}

					if ((kycBo.getEmpVoterIdNo() != null) && (personalBean.getKycInputBean().getVoterId() == null
							|| personalBean.getKycInputBean().getVoterId().trim().isEmpty())) {
						throw new CommonCustomException("voter id number can't be empty, please pass proper data");
					} else if ((kycBo.getEmpVoterIdNo() == null) && (personalBean.getKycInputBean().getVoterId() == null
							|| personalBean.getKycInputBean().getVoterId().trim().isEmpty())) {
						kycBo.setEmpVoterIdNo(null);
					} else {
						kycBo.setEmpVoterIdNo(personalBean.getKycInputBean().getVoterId().trim());
					}

					kycBo.setAadharNo(personalBean.getKycInputBean().getAadharNo().trim());
					kycBo.setEmpNameAsPerAadharCard(personalBean.getKycInputBean().getEmpNameAsPerAadharCard());
					kycBo.setEmpNameAsPerPanCard(personalBean.getKycInputBean().getEmpNameAsPerPanCard());
					kycBo.setEmpNameAsPerPassport(personalBean.getKycInputBean().getEmpNameAsPerPassPort());
					kycBo.setEmpNameAsPerVoterId(personalBean.getKycInputBean().getEmpNameAsPerVoterId());
					kycBo.setEmpPanNo(personalBean.getKycInputBean().getPanCardNumber().trim());
					kycBo.setEmpPassportNo(personalBean.getKycInputBean().getPassportNumber().trim());
					// kycBo.setEmpVoterIdNo(personalBean.getKycInputBean().getVoterId().trim());
					kycBo.setPassportDateOfIssue(personalBean.getKycInputBean().getDateOfIssue());
					kycBo.setPassportExpiryDate(personalBean.getKycInputBean().getDateOfExpiry());
					kycBo.setPassportplaceOfIssue(personalBean.getKycInputBean().getPlaceOfIssue());
					kycBo.setFkEmpDetailId(personalBean.getEmpId());

					kycBo.setKycUpdatedOn(new Date());
				} else {

					kycBo = new DatEmpKycDetailBO();
					kycBo.setAadharNo(personalBean.getKycInputBean().getAadharNo());
					kycBo.setEmpNameAsPerAadharCard(personalBean.getKycInputBean().getEmpNameAsPerAadharCard());
					kycBo.setEmpNameAsPerPanCard(personalBean.getKycInputBean().getEmpNameAsPerPanCard());
					kycBo.setEmpNameAsPerPassport(personalBean.getKycInputBean().getEmpNameAsPerPassPort());
					kycBo.setEmpNameAsPerVoterId(personalBean.getKycInputBean().getEmpNameAsPerVoterId());
					kycBo.setEmpPanNo(personalBean.getKycInputBean().getPanCardNumber());
					kycBo.setEmpPassportNo(personalBean.getKycInputBean().getPassportNumber());
					kycBo.setEmpVoterIdNo(personalBean.getKycInputBean().getVoterId());
					kycBo.setHasUanFlag(personalBean.getKycInputBean().getHasUanFlag());
					kycBo.setPassportDateOfIssue(personalBean.getKycInputBean().getDateOfIssue());
					kycBo.setPassportExpiryDate(personalBean.getKycInputBean().getDateOfExpiry());
					kycBo.setPassportplaceOfIssue(personalBean.getKycInputBean().getPlaceOfIssue());
					kycBo.setKycUpdatedOn(new Date());
					kycBo.setFkEmpDetailId(personalBean.getEmpId());
					kycBo.setKycCreatedOn(new Date());

				}
			} else {
				throw new CommonCustomException("The given Employee id is not valid");
			}

			DatEmpPersonalDetailBO personalBO = empPersonalRepository.save(personalBo);
			if (personalBO != null) {
				empKycRepository.save(kycBo);
				flag = true;
			} else {
				throw new DataAccessException("Some exception occurred while Saving employee personal detail");
			}

			try {
				if (listOfMapFiles.size() > 0) {

					for (Map.Entry<String, MultipartFile> map : listOfMapFiles.entrySet()) {

						String[] name = map.getValue().getOriginalFilename().split("\\.");
						String newFileName = map.getKey() + "." + name[1].toString();
						File dir = new File(kycBasePath + personalBean.getEmpId());

						if (dir.mkdirs()) {
							File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
							FileCopyUtils.copy(map.getValue().getBytes(), fileToBeStored);
						} else {
							File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
							FileCopyUtils.copy(map.getValue().getBytes(), fileToBeStored);
						}
					}

				}
			} catch (IOException e) {
				throw new CommonCustomException("Document files not uploaded");
			}

		} catch (CommonCustomException c) {
			flag = false;

			throw new CommonCustomException(c.getMessage());
		} catch (DataAccessException e) {
			flag = false;

			throw new DataAccessException(e.getMessage());
		}

		return flag;

	}

	public EmployeeProfileDetailBean changeEmpType(Integer empId, Integer loggedInEmpId) throws DataAccessException {

		DatEmpDetailBO datEmpDetailBo;
		DatEmpPersonalDetailBO personalBo;
		DatEmpProfessionalDetailBO professionalBo;
		datEmpDetailBo = new DatEmpDetailBO();
		personalBo = new DatEmpPersonalDetailBO();
		professionalBo = new DatEmpProfessionalDetailBO();
		List<DatEmpEmploymentDetailBO> listOfEmployersBo = new ArrayList<DatEmpEmploymentDetailBO>();
		List<DatEmpEducationDetailBO> listOfEducationalBo = new ArrayList<DatEmpEducationDetailBO>();
		List<DatEmpEmploymentDetailBO> listOfAllEmployersBo = new ArrayList<DatEmpEmploymentDetailBO>();
		List<DatEmpEmploymentDetailBO> listOfEmployerBo = new ArrayList<DatEmpEmploymentDetailBO>();
		List<DatEmpEducationDetailBO> listOfAllEducationalBo = new ArrayList<DatEmpEducationDetailBO>();
		List<DatEmpEducationDetailBO> listOfEducationBo = new ArrayList<DatEmpEducationDetailBO>();
		List<DatEmpDetailBO> listEmpDetailBo = new ArrayList<DatEmpDetailBO>();
		List<DatEmpPersonalDetailBO> listPersonalDetailBo = new ArrayList<DatEmpPersonalDetailBO>();
		List<DatEmpProfessionalDetailBO> listProfessionalDetailBo = new ArrayList<DatEmpProfessionalDetailBO>();
		List<DatEmpKycDetailBO> listOfKycBo = new ArrayList<DatEmpKycDetailBO>();
		Iterable<DatEmpLastEmployeeIdBO> iterableOfLastEmpId = new ArrayList<DatEmpLastEmployeeIdBO>();
		List<DatEmpLastEmployeeIdBO> listOfLastEmpId = new ArrayList<DatEmpLastEmployeeIdBO>();
		DatEmpLastEmployeeIdBO lastEmpIdBO = new DatEmpLastEmployeeIdBO();
		EmployeeProfileDetailBean empProfileBean = new EmployeeProfileDetailBean();

		EmployeeKYCInputBean kycInputBean = new EmployeeKYCInputBean();
		EmployeePersonalInputBean personalDetail = new EmployeePersonalInputBean();
		List<CreateEmpProfDetailBean> professionalDetails = new ArrayList<CreateEmpProfDetailBean>();
		List<EmployeeEducationalOutputBean> educationalDetail = new ArrayList<EmployeeEducationalOutputBean>();
		EmployeeOrganizationOutputBean organizationDetail = new EmployeeOrganizationOutputBean();
		DatEmpKycDetailBO kycBo;
		int permanent = 0;
		int newEmpId;

		try {

			iterableOfLastEmpId = empLastEmployeeIdRepository.findAll();

			listOfLastEmpId = StreamSupport.stream(iterableOfLastEmpId.spliterator(), false)
					.collect(Collectors.toList());
			if (listOfLastEmpId.size() > 0) {
				for (DatEmpLastEmployeeIdBO empIdBo : listOfLastEmpId) {
					if (empIdBo.getEmpTypeId() == 1) {
						permanent = empIdBo.getLastEmployeeId();
					}
				}
			}
			newEmpId = permanent + 1;

			datEmpDetailBo = empDetailRepository.findByPkEmpId(empId);
			datEmpDetailBo.setEmpType((byte) 1);
			datEmpDetailBo.setProfileCreatedBy(loggedInEmpId);
			datEmpDetailBo.setProfileCreatedDate(new Date());
			datEmpDetailBo.setFkEmpMainStatus((byte) 1);
			datEmpDetailBo.setFkEmpSubStatus((byte) 7);
			datEmpDetailBo.setPkEmpId(newEmpId);
			listEmpDetailBo.add(datEmpDetailBo);

			professionalBo = professionalRepository.findByFkMainEmpDetailId(empId);

			/*
			 * professionalBo.setFkEmpBuUnit(empBean.getBuUnit());
			 * professionalBo.setFkEmpReportingMgrId(empBean.
			 * getReportingManagerId());
			 */
			professionalBo.setFkMainEmpDetailId(newEmpId);
			professionalBo.setPkEmpProfessionalDetailId(null);
			listProfessionalDetailBo.add(professionalBo);

			personalBo = personalRepository.findByFkEmpDetailId(empId);

			personalBo.setFkEmpDetailId(newEmpId);
			personalBo.setPkEmpPersonalDetailId(null);
			listPersonalDetailBo.add(personalBo);

			listOfEmployersBo = datEmpEmploymentRepository.findByFkEmpId(empId);
			listOfEducationalBo = empEducationalRepository.findByFkEmpId(empId);

			for (DatEmpEmploymentDetailBO employment : listOfEmployersBo) {
				employment.setPkEmploymentId(null);
				employment.setFkEmpId(newEmpId);
				listOfAllEmployersBo.add(employment);
			}
			// listOfAllEmployersBo.addAll(listOfEmployersBo);

			for (DatEmpEducationDetailBO edu : listOfEducationalBo) {
				edu.setPkEduId(null);
				edu.setFkEmpId(newEmpId);
				listOfAllEducationalBo.add(edu);
			}
			// listOfAllEducationalBo.addAll(listOfEducationalBo);
			kycBo = empKycRepository.findByFkEmpDetailId(empId);
			if (kycBo != null) {
				kycBo.setPkEmpKycDetailId(null);
				kycBo.setFkEmpDetailId(newEmpId);
				listOfKycBo.add(kycBo);
			}

			DatEmpDetailBO empDetailBO = empDetailRepository.save(datEmpDetailBo);

			DatEmpProfessionalDetailBO empDetailProBO = professionalRepository.save(professionalBo);

			DatEmpPersonalDetailBO empDetailPerBO = personalRepository.save(personalBo);

			if (listOfAllEmployersBo.size() > 0) {
				Iterable<DatEmpEmploymentDetailBO> empDetailEmpBO = datEmpEmploymentRepository
						.save(listOfAllEmployersBo);
				listOfEmployerBo = StreamSupport.stream(empDetailEmpBO.spliterator(), false)
						.collect(Collectors.toList());
			}

			if (listOfAllEducationalBo.size() > 0) {
				Iterable<DatEmpEducationDetailBO> empDetailEduBO = empEducationalRepository
						.save(listOfAllEducationalBo);

				listOfEducationBo = StreamSupport.stream(empDetailEduBO.spliterator(), false)
						.collect(Collectors.toList());
			}

			if(listOfKycBo.size() > 0)
				kycBo = empKycRepository.save(kycBo);
			
			
			lastEmpIdBO =empLastEmployeeIdRepository.findByEmpTypeId(1);
			lastEmpIdBO.setLastEmployeeId(newEmpId);
			empLastEmployeeIdRepository.save(lastEmpIdBO);
				
			if(empDetailPerBO != null){
					personalDetail.setBloodGroup(empDetailPerBO.getEmpBloodGroup());
					personalDetail.setDateOfBirth(empDetailPerBO.getEmpDateOfBirth());
					personalDetail.setEmpFirstName(empDetailPerBO.getEmpFirstName());
					personalDetail.setEmpId(empDetailPerBO.getFkEmpDetailId());
					personalDetail.setEmpLastName(empDetailPerBO.getEmpLastName());
					personalDetail.setEmpMiddleName(empDetailPerBO.getEmpMiddleName());
					personalDetail.setGender(empDetailPerBO.getEmpGender());
					personalDetail.setHomePhone(empDetailPerBO.getEmpAlternateMobileNo());
					personalDetail.setMartialStatus(empDetailPerBO.getEmpMaritalStatus());
					personalDetail.setMobileNumber(empDetailPerBO.getEmpMobileNo());
					personalDetail.setPersonalEmail(empDetailPerBO.getEmpPersonalEmailId());
					personalDetail.setPermanentAddressLineOne(personalBo.getEmpPermanentAddressLineOne());
					personalDetail.setPermanentAddressLineTwo(personalBo.getEmpPermanentAddressLineTwo());
					personalDetail.setPermanentAddressCity(personalBo.getEmpPermanentAddressCity());
					personalDetail.setPermanentAddressState(personalBo.getEmpPermanentAddressState());
					personalDetail.setPermanentAddressCountry(personalBo.getEmpPermanentAddressCountry());
					personalDetail.setPresentAddressLineOne(personalBo.getEmpPresentAddressLineOne());
					personalDetail.setPresentAddressLineTwo(personalBo.getEmpPresentAddressLineTwo());
					personalDetail.setPresentAddressCity(personalBo.getEmpPresentAddressCity());
					personalDetail.setPresentAddressState(personalBo.getEmpPresentAddressState());
					personalDetail.setPresentAddressCountry(personalBo.getEmpPresentAddressCountry());
					personalDetail.setSpouseDOB(empDetailPerBO.getEmpSpouseDateOfBirth());
					personalDetail.setSpouseName(empDetailPerBO.getEmpSpouseName());
					
					if(kycBo!=null){
						
						kycInputBean.setAadharNo(kycBo.getAadharNo());
						kycInputBean.setEmpNameAsPerAadharCard(kycBo.getEmpNameAsPerAadharCard());
						kycInputBean.setEmpNameAsPerPanCard(kycBo.getEmpNameAsPerPanCard());
						kycInputBean.setEmpNameAsPerPassPort(kycBo.getEmpNameAsPerPassport());
						kycInputBean.setEmpNameAsPerVoterId(kycBo.getEmpNameAsPerVoterId());
						kycInputBean.setPanCardNumber(kycBo.getEmpPanNo());
						kycInputBean.setPassportNumber(kycBo.getEmpPassportNo());
						kycInputBean.setVoterId(kycBo.getEmpVoterIdNo());
						kycInputBean.setHasUanFlag(kycBo.getHasUanFlag());
						kycInputBean.setDateOfIssue(kycBo.getPassportDateOfIssue());
						kycInputBean.setDateOfExpiry(kycBo.getPassportExpiryDate());
						kycInputBean.setPlaceOfIssue(kycBo.getPassportplaceOfIssue());
						//kycInputBean.setKycUpdatedOn(kycBo.get);
						//kycInputBean.set(kycBo.getFkEmpDetailId());
						//kycInputBean.setKycCreatedOn(new Date());
						
						personalDetail.setKycInputBean(kycInputBean);
					}
						
			CreateEmpProfDetailBean employerBean;
			if (listOfEmployersBo.size() > 0) {
				for (DatEmpEmploymentDetailBO employer : listOfEmployerBo) {
					employerBean = new CreateEmpProfDetailBean();
					employerBean.setDurationFromDate(employer.getStartDate());
					employerBean.setDurationToDate(employer.getEndDate());
					employerBean.setEmpId(employer.getFkEmpId());
					employerBean.setEmployerName(employer.getEmpEmployerName());
					employerBean.setPositionHeld(employer.getEmpEmploymentDesg());
					employerBean.setReasonForLeaving(employer.getReasonForLeaving());
					professionalDetails.add(employerBean);
				}
			}
			EmployeeEducationalOutputBean educationalBean;
			if (listOfEducationBo.size() > 0) {

				for (DatEmpEducationDetailBO educational : listOfEducationBo) {
					educationalBean = new EmployeeEducationalOutputBean();

					educationalBean.setEducationalLevelId(educational.getEduLevel());

					for (EducationalLevel name : EducationalLevel.values()) {

						if (name.value == educational.getEduLevel()) {
							educationalBean.setEducationalLevel(name.toString());

						}
					}

					educationalBean.setEmpId(educational.getFkEmpId());
					educationalBean.setNameOfSchool(educational.getNameOfUniversity());
					educationalBean.setPercentage(educational.getPercentage());
					educationalBean.setSpecilizationId(educational.getFkSpecializationId());
					if (educational.getFkSpecializationId() != null)
						educationalBean.setSpecilization(educational.getMasEmpSpecialization().getSpecializationName());

					educationalBean.setStreamId(educational.getFkStreamId());
					if (educational.getFkStreamId() != null)
						educationalBean.setStream(educational.getMasEmpStream().getStreamName());
					educationalBean.setYearOfCompletion(educational.getYearOfCompletion());

					educationalDetail.add(educationalBean);
				}
			}

			if (empDetailProBO != null) {
				organizationDetail.setBootCampId(empDetailProBO.getFkEmpBootCampId());
				organizationDetail.setBootCampName(empDetailProBO.getMasBootCampBO().getBootCampName());
				organizationDetail.setBusinessUnit(empDetailProBO.getMasBuUnitBO().getBuUnitName());
				organizationDetail.setBusinessUnitId(empDetailProBO.getFkEmpBuUnit());
				organizationDetail.setCurrentLocation(empDetailProBO.getFkEmpLocationParentId());
				organizationDetail
						.setCurrentLocationName(empDetailProBO.getMasLocationParentBO().getLocationParentName());
				organizationDetail.setDateOfJoining(empDetailProBO.getEmpDateOfJoining());
				organizationDetail.setDateOfReporting(empDetailProBO.getEffectiveDateOfReportingMgrChange());
				organizationDetail.setDesignation(empDetailProBO.getFkEmpDesignation());
				organizationDetail.setDesignationName(empDetailProBO.getMasEmpDesignationBO().getEmpDesignationName());
				organizationDetail.setDomainManager(empDetailProBO.getFkEmpDomainMgrId());

				String dmName = gsrGoalService.getEmpNameByEmpId(empDetailProBO.getFkEmpDomainMgrId());
				if (dmName != null) {
					organizationDetail.setDomainManagerName(dmName);
				}
				organizationDetail.setEmailAddress(empDetailProBO.getEmpProfessionalEmailId());
				organizationDetail.setEmpId(empDetailProBO.getFkMainEmpDetailId());

				organizationDetail
						.setEmploymentStatus(empDetailProBO.getDatEmpDetailBO_main_emp_id().getFkEmpMainStatus());
				organizationDetail.setEmploymentStatusName(
						empDetailProBO.getDatEmpDetailBO_main_emp_id().getMasEmpMainStatusBO().getEmpMainStatusName());
				organizationDetail.setEmploymentTypeId(empDetailProBO.getDatEmpDetailBO_main_emp_id().getEmpType());
				for (EmployeeType type : EmployeeType.values()) {

					if (type.value == empDetailProBO.getDatEmpDetailBO_main_emp_id().getEmpType()) {

						organizationDetail.setEmploymentType(type.toString());

					}
				}

				organizationDetail
						.setEmployeeType(empDetailProBO.getEmpPreviousExperience() > 0 ? "Experienced" : "Fresher");
				organizationDetail.setEndDateForFTE(empDetailProBO.getEmpContractEndDate());
				organizationDetail.setLevel(empDetailProBO.getFkEmpLevel());
				organizationDetail.setLevelName(empDetailProBO.getMasEmpLevelBO().getEmpLevelName());
				organizationDetail.setOrganisation(empDetailProBO.getFkEmpOrganizationId());
				organizationDetail.setOrgName(empDetailProBO.getMasOrganizationBO().getOrganizationName());
				organizationDetail.setProbationPeriod(empDetailProBO.getProbationMonths());
				organizationDetail.setReportingManager(empDetailProBO.getFkEmpReportingMgrId());
				String rmName = gsrGoalService.getEmpNameByEmpId(empDetailProBO.getFkEmpReportingMgrId());
				if (rmName != null) {
					organizationDetail.setReportingManagerName(rmName);
				}
				organizationDetail
						.setRole(empDetailProBO.getDatEmpDetailBO_main_emp_id().getMasEmpRoleBO().getEmpRoleName());
				organizationDetail.setRoleId(empDetailProBO.getDatEmpDetailBO_main_emp_id().getFkEmpRoleId());
				empProfileBean.setEmployeeStatusId(empDetailProBO.getDatEmpDetailBO_main_emp_id().getFkEmpMainStatus());
				empProfileBean.setEmployeeStatus(
						empDetailProBO.getDatEmpDetailBO_main_emp_id().getMasEmpMainStatusBO().getEmpMainStatusName());
				empProfileBean.setEmployeeSubStatus(
						empDetailProBO.getDatEmpDetailBO_main_emp_id().getMasEmpSubStatusBO().getEmpSubStatusName());
				empProfileBean
						.setEmployeeSubStatusId(empDetailProBO.getDatEmpDetailBO_main_emp_id().getFkEmpSubStatus());
				empProfileBean.setExtensionNumber(empDetailProBO.getEmpExtensionNumber());
			}

			empProfileBean.setPersonalDetail(personalDetail);
			empProfileBean.setEducationalDetail(educationalDetail);
			empProfileBean.setProfessionalDetails(professionalDetails);
			empProfileBean.setOrganizationDetail(organizationDetail);

		}
		}catch (Exception e) {
			e.printStackTrace();
			throw new DataAccessException(e.getMessage());
		}
		return empProfileBean;

	}

	public List<CountryBean> employeeLocationBasedOnCountry(EmployeeLocationBean bean) throws DataAccessException {

		LOG.entering("employeeLocationBasedOnCountry service method");
		List<MapMasOrgCountryBO> listOfOrgCountry = new ArrayList<MapMasOrgCountryBO>();
		List<CountryBean> listOfCountry = new ArrayList<CountryBean>();
		CountryBean ctrBean;
		try {
			// onsite
			if (bean.getOnSiteOrOffShore() == true) {
				if (bean.getDomestic() == true) {
					try {
						listOfOrgCountry = mapOrgCountryRepository.findByFkOrgId(bean.getThbsOrg());
					} catch (Exception e) {
						throw new DataAccessException("Some exception occured while retriving country based on org ");
					}
				} else if (bean.getInternational() == true) {
					try {
						listOfCountry = commonSrvc.getAllCountry();
					} catch (DataAccessException e) {
						throw new DataAccessException("Failed to retrieve all countries", e);
					}
				}
				// offshore
			} else {
				if (bean.getOnsiteClient() == true) {
					try {
						listOfOrgCountry = mapOrgCountryRepository.findByFkOrgId(bean.getThbsOrg());
					} catch (Exception e) {
						throw new DataAccessException("Some exception occured while retriving country based on org ");
					}
				}

				if (bean.getOnsiteClient() == false) {
					try {
						listOfCountry = commonSrvc.getAllCountry();
					} catch (DataAccessException e) {
						throw new DataAccessException("Failed to retrieve all countries", e);
					}
				}
			}

			if (listOfOrgCountry.size() > 0) {
				for (MapMasOrgCountryBO bo : listOfOrgCountry) {
					ctrBean = new CountryBean();
					ctrBean.setCountryCode(bo.getMasCountryBO().getCountryCode());
					ctrBean.setCountryId(bo.getMasCountryBO().getPkCountryId());
					ctrBean.setCountryName(bo.getMasCountryBO().getCountryName());
					listOfCountry.add(ctrBean);
				}
			}

		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}

		LOG.exiting("employeeLocationBasedOnCountry service method");
		return listOfCountry;

	}

		
	public Boolean employeeRecruiterDetail(EmployeeRecruitmentBean recruitmentBean) throws DataAccessException{
		
		Boolean flag = false;
		DatEmpRecruitmentDetailBO recruitmentBo = new DatEmpRecruitmentDetailBO();
		DatEmpRecruitmentDetailBO recruitment = null;
		try{
			
			try{
				recruitment = recruitmentRepo.findByFkEmpId(recruitmentBean.getEmpId());
				
			}catch(Exception d){
			
				throw new DataAccessException("Some exception occurred while retrieveing Recruitment data from DB  ");
			}
			/*	
			if(recruitmentBo!=null){
				throw new DataAccessException("Recruitment data already exist's for given employee id, Please pass proper employee id");
			}*/
			if(recruitment != null){
				throw new DataAccessException("Recruitment data already exist's for given employee id");	
			}
			recruitmentBo.setFkEmpId(recruitmentBean.getEmpId());	
			recruitmentBo.setFkEmpSourceId(recruitmentBean.getEmpSourceId());
			recruitmentBo.setEmpSubSourceId(recruitmentBean.getEmpSubSourceId());
			recruitmentBo.setFkRecruiterId(recruitmentBean.getRecruiterId());
			recruitmentBo.setManagerialPanel(recruitmentBean.getManagerialPanel().toString());
			recruitmentBo.setReferralEmpId(recruitmentBean.getReferralEmpId());
			recruitmentBo.setTechnicalPanel(recruitmentBean.getTechnicalPanel().toString());
			recruitmentBo.setCreatedDate(new Date());
			recruitmentBo.setCreatedBy(recruitmentBean.getLoggedInEmpId());
			
			recruitmentBo = recruitmentRepo.save(recruitmentBo);
			
			
			if(recruitmentBo.getPkRecruitmentId() != null){
				flag = true;
			}
			
		}catch(Exception e){
			
			throw new DataAccessException(e.getMessage());
		}
		
		return flag;
	}
	

	public boolean employeeRecruitmentUpdateService(EmployeeRecruitmentBean bean) throws CommonCustomException {

		LOG.info("entering into employeeRecruitmentUpdateService service method");
		boolean flag = false;
		// Collect all role id's of System HR.
		List<String> hrRoleIdList = new ArrayList<String>();
		String[] empIdArray = hrRoles.split(",");
		for (String s : empIdArray)
			hrRoleIdList.add(s);
		DatEmpDetailBO hrEmpDetailBo = new DatEmpDetailBO();
		DatEmpRecruitmentDetailBO detailBO = new DatEmpRecruitmentDetailBO();
		hrEmpDetailBo = empDetailRepository.getSystemAdminRoleDetailsForUserManagmentModule(bean.getLoggedInEmpId(),
				hrRoleIdList);
		if (hrEmpDetailBo != null || bean.getLoggedInEmpId() == bean.getEmpId()) {
			detailBO = recruitmentRepo.findByFkEmpId(bean.getEmpId());
			
			detailBO.setEmpSubSourceId(bean.getEmpSubSourceId());
			detailBO.setFkEmpId(bean.getEmpId());
			detailBO.setFkRecruiterId(bean.getRecruiterId());
			String temp = "[";
			for(int i = 0; i< bean.getManagerialPanel().size();i++){
						temp += Integer.toString(bean.getManagerialPanel().get(i).intValue());
						if(bean.getManagerialPanel().size() != i+1){
							temp += ",";
						}
			}
			temp += "]";
			String newList1 = "[";
			for(int j = 0; j< bean.getTechnicalPanel().size();j++){
				newList1 += bean.getTechnicalPanel().get(j).intValue();
				if(bean.getTechnicalPanel().size() != j+1)
					newList1 += ",";
			}
			newList1 += "]";
			detailBO.setManagerialPanel(temp);
			detailBO.setReferralEmpId(bean.getReferralEmpId());
			detailBO.setTechnicalPanel(newList1);
			detailBO.setFkEmpSourceId(bean.getEmpSourceId());
			detailBO.setModifiedBy(bean.getLoggedInEmpId());
			detailBO.setModifiedDate(new Date());
			recruitmentRepo.save(detailBO);
			flag = true;

		} else {
			flag = false;
			throw new CommonCustomException("You are not Authorized to update Employee Recruitment Details");
		}
		LOG.info("Exiting from employeeRecruitmentUpdateService method from service");
		return flag;
	}
	
	public Boolean employeeLocationApproveByHrService(Integer pkEmpLocId, Byte status) throws CommonCustomException{
		
		Boolean flag = false;
		DatEmpLocationApprovalBO empLocApproveBo = new DatEmpLocationApprovalBO();
		
		try{
			 empLocApproveBo = locationApprovalRepo.findByPkId(pkEmpLocId);
			 
			 if(empLocApproveBo!=null){
				 empLocApproveBo.setStatus(status);
				 locationApprovalRepo.save(empLocApproveBo);
				 flag = true;
			 }
			
			
		}catch(Exception e){
			throw new CommonCustomException(e.getMessage());
		}
		return flag;
	}
	
	public List<EmployeeLocChangeOutputBean> getEmployeeLocationChangeDetailByHr(EmployeeLocChangeInputBean bean) throws DataAccessException{
		
		List<DatEmpLocationApprovalBO> listOfEmpLocdetails = new ArrayList<DatEmpLocationApprovalBO>();
		List<EmployeeLocChangeOutputBean> listOfOutputBean = new ArrayList<EmployeeLocChangeOutputBean>();
		List<Integer> listOfPkId = new ArrayList<Integer>();
		try{
			
			try {
				Specification<DatEmpLocationApprovalBO> empLocSpecification = EmployeeLocChangeSpecification
						.getEmployeeLocChangeDetails(bean);
				listOfEmpLocdetails = locationApprovalRepo.findAll(empLocSpecification);
			} catch (Exception e) {
				throw new DataAccessException("Exception occured while fetching data from employees location change data ");
			}
			
			listOfPkId.addAll(listOfEmpLocdetails.stream().map(DatEmpLocationApprovalBO::getPkId).collect(Collectors.toList()));
		
			
			try{
			listOfOutputBean = locationApprovalRepo.getEmployeeLocChangesDetails(listOfPkId);
			}catch(Exception e){
				throw new DataAccessException("Exception occured at service method getAllAssetRequests ", e);
			}
			
		}catch(Exception e){
			
			throw new DataAccessException(e.getMessage());
		}
		return listOfOutputBean;
		
	}

}
