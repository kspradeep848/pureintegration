package com.thbs.mis.usermanagement.service;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.multipart.MultipartFile;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.uan.bo.DatEmpKycDetailBO;
import com.thbs.mis.uan.dao.EmpKycDetailRepository;
import com.thbs.mis.usermanagement.bean.CreateEmpProfDetailBean;
import com.thbs.mis.usermanagement.bean.DateRangeBean;
import com.thbs.mis.usermanagement.bean.DateRangeOutputBean;
import com.thbs.mis.usermanagement.bean.EmpLocChangeByPcBean;
import com.thbs.mis.usermanagement.bean.EmployeeDetailsByIdBean;
import com.thbs.mis.usermanagement.bean.MasEmployeeSpecializationBean;
import com.thbs.mis.usermanagement.bean.MasEmployeeStreamBean;
import com.thbs.mis.usermanagement.bean.UpdateEmailIDBean;
import com.thbs.mis.usermanagement.bean.UpdateEmpPersonalDetailsBean;
import com.thbs.mis.usermanagement.bean.UpdateExtensionNumBean;
import com.thbs.mis.usermanagement.bo.DatEmpEmploymentDetailBO;
import com.thbs.mis.usermanagement.bo.DatEmpLocationApprovalBO;
import com.thbs.mis.usermanagement.bo.MasEmpSpecializationBO;
import com.thbs.mis.usermanagement.bo.MasEmpStreamBO;
import com.thbs.mis.usermanagement.dao.DatEmpEmploymentRepository;
import com.thbs.mis.usermanagement.dao.DatEmpLocApprovalRepository;
import com.thbs.mis.usermanagement.dao.DatEmploymentDetailsRepository;
import com.thbs.mis.usermanagement.dao.MasEmpSpecilizationRepository;
import com.thbs.mis.usermanagement.dao.MasEmpStreamRepository;

import groovy.ui.SystemOutputInterceptor;

@Service
public class UserManagementService {

	private static final AppLog LOG = LogFactory.getLog(UserManagementService.class);

	@Autowired
	private DatEmploymentDetailsRepository empDetailrepos;

	@Autowired
	private DatEmpEmploymentRepository datEmpEmploymentRepository;

	@Autowired
	private EmployeeProfessionalRepository datEmpProfessionalDetailsRepository;

	@Autowired
	private EmpKycDetailRepository kycRepository;

	@Autowired
	private EmpDetailRepository datEmpRepository;

	@Autowired
	private EmployeePersonalDetailsRepository datEmpPersonalDetailsRepository;

	@Autowired
	EmpDetailRepository empDetailRepository;
	
	@Autowired
	private DatEmpLocApprovalRepository locationApprovalRepo;

	@Value("${emp.status.active}")
	private byte active;

	@Value("${emp.marital.status.single}")
	private String single;

	@Value("${emp.marital.status.married}")
	private String married;

	@Value("${emp.prof.sequenceNo}")
	private Integer sequenceNo;

	@Value("${emp.status.waitingforprofilecreation}")
	private Byte empWaitingStatus;

	@Value("${emp.status.active}")
	private byte empActiveStatus;

	@Value("${common.system.admin.roles}")
	private String systemAdminRoles;

	@Value("${common.system.hr.roles}")
	private String hrRoles;

	@Value("${aadhar.file.name}")
	private String aadharFileName;

	@Value("${pan.file.name}")
	private String panCardFileName;

	@Value("${voter.file.name}")
	private String voterFileName;

	@Value("${passport.file.name}")
	private String passPortFileName;

	@Value("${kyc.file.basepath}")
	private String kycBasePath;

	@Autowired
	private MasEmpSpecilizationRepository masEmpSpecRepository;

	@Autowired
	private MasEmpStreamRepository masEmpStreamRepository;

	/**
	 * <Description getAllEmpDetails:> This method is used to display Employee
	 * details
	 * 
	 * @param user
	 * @return MIS Response
	 * @throws DataAccessException
	 */
	public List<EmployeeDetailsByIdBean> getAllEmpDetails(Integer empId) throws DataAccessException {
		List<EmployeeDetailsByIdBean> detailsBeanList = new ArrayList<EmployeeDetailsByIdBean>();
		List<DatEmpProfessionalDetailBO> profDetailBO;

		try {
			profDetailBO = (List<DatEmpProfessionalDetailBO>) empDetailrepos.findByfkEmpDetailId(empId);
		} catch (Exception e) {
			throw new DataAccessException("Failed to retrieve list of employee details : ", e);
		}
		if (profDetailBO.size() > 0) {
			for (DatEmpProfessionalDetailBO datProfDetailBo : profDetailBO) {
				EmployeeDetailsByIdBean empDetailBeanObject = new EmployeeDetailsByIdBean();
				empDetailBeanObject.setEmpId(datProfDetailBo.getFkMainEmpDetailId());
				if (datProfDetailBo.getEmpMiddleName() == null) {
					empDetailBeanObject
							.setEmpName(datProfDetailBo.getEmpFirstName() + " " + datProfDetailBo.getEmpLastName());
				} else {
					empDetailBeanObject.setEmpName(datProfDetailBo.getEmpFirstName() + " "
							+ datProfDetailBo.getEmpMiddleName() + " " + datProfDetailBo.getEmpLastName());
				}

				empDetailBeanObject.setEmailId(datProfDetailBo.getEmpProfessionalEmailId());
				empDetailBeanObject.setBusinessUnit(datProfDetailBo.getBuUnitName());
				empDetailBeanObject.setDesignation(datProfDetailBo.getEmpDesignationName());
				empDetailBeanObject.setLocation(datProfDetailBo.getLocationParentName());
				empDetailBeanObject.setMobileNumber(datProfDetailBo.getEmpMobileNo());
				empDetailBeanObject.setHomeNumber(datProfDetailBo.getEmpAlternateMobileNo());
				if (datProfDetailBo.getFkEmpMainStatus() == active) {
					empDetailBeanObject.setEmpStatus("active");
				} else {
					empDetailBeanObject.setEmpStatus("Inactive");
				}
				empDetailBeanObject.setExtension(datProfDetailBo.getEmpExtensionNumber());
				detailsBeanList.add(empDetailBeanObject);
			}
		}
		return detailsBeanList;
	}

	/**
	 * <Description addUser:> This method is used to update Employee Personal
	 * Details
	 * 
	 * @param user
	 * @return MIS Response
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	public Boolean updatePersonalDetails(UpdateEmpPersonalDetailsBean bean, MultipartFile passPort,
			MultipartFile voterId, MultipartFile aadhar, MultipartFile panCard)
			throws DataAccessException, CommonCustomException {

		LOG.debug("About to update data");
		Boolean flag = false;
		DatEmpPersonalDetailBO personalDetails = new DatEmpPersonalDetailBO();
		DatEmpKycDetailBO kycDetail = new DatEmpKycDetailBO();
		Map<String, MultipartFile> listOfMapFiles = new HashMap<String, MultipartFile>();

		if (passPort == null && voterId == null && aadhar == null && panCard == null) {
			throw new CommonCustomException("any one file must be mandatory to upload");
		} else {
			try {

				if (panCard != null && !panCard.isEmpty())
					listOfMapFiles.put(panCardFileName, panCard);
				if (aadhar != null && !aadhar.isEmpty())
					listOfMapFiles.put(aadharFileName, aadhar);
				if (voterId != null && !voterId.isEmpty())
					listOfMapFiles.put(voterFileName, voterId);
				if (passPort != null && !passPort.isEmpty())
					listOfMapFiles.put(passPortFileName, passPort);

				if (listOfMapFiles.size() > 0) {

					for (Map.Entry<String, MultipartFile> map : listOfMapFiles.entrySet()) {
						long fileSizeInKB = map.getValue().getSize() / 1024;

						if (!map.getValue().getOriginalFilename().toLowerCase().endsWith(".pdf")
								&& !map.getValue().getOriginalFilename().toLowerCase().endsWith(".jpeg")
								&& !map.getValue().getOriginalFilename().toLowerCase().endsWith(".jpg")
								&& !map.getValue().getOriginalFilename().toLowerCase().endsWith(".png")) {
							throw new CommonCustomException(
									"uploaded file is in different format, Please upload only pdf,jpeg,jpg or png files");
						} else if (fileSizeInKB > 1024) {
							throw new CommonCustomException("uploaded file should be 1 MB or less than 1 MB");
						}
					}

				}

				// Collect all role ids of System HR.
				List<String> hrRoleIdList = new ArrayList<String>();
				String[] empIdArray = hrRoles.split(",");
				for (String s : empIdArray)
					hrRoleIdList.add(s);
				DatEmpDetailBO hrEmpDetailBo = new DatEmpDetailBO();

				hrEmpDetailBo = datEmpRepository
						.getSystemAdminRoleDetailsForUserManagmentModule(bean.getLoggedInEmpId(), hrRoleIdList);
				personalDetails = datEmpPersonalDetailsRepository.findByFkEmpDetailId(bean.getEmpId());
				kycDetail = kycRepository.findByFkEmpDetailId(bean.getEmpId());

				if (hrEmpDetailBo != null || bean.getLoggedInEmpId().equals(bean.getEmpId())) {

					if (hrEmpDetailBo == null && bean.getLoggedInEmpId().equals(bean.getEmpId())) {

						if (kycDetail.getEmpPanNo().isEmpty()) {
							kycDetail.setEmpNameAsPerPanCard(bean.getEmpNameAsPerPanCard());
							kycDetail.setEmpPanNo(bean.getEmpPanNo());
						} else {
							throw new CommonCustomException("PanCard Details already existed");
						}
						if (kycDetail.getAadharNo().isEmpty()) {
							kycDetail.setEmpNameAsPerAadharCard(bean.getEmpNameAsPerAadharCard());
							kycDetail.setAadharNo(bean.getAdharNo());
						} else {
							throw new CommonCustomException("Adhaar Details already existed");
						}
					}

					/*
					 * if (kycDetail.getEmpPanNo() != null &&
					 * (bean.getEmpPanNo() != null)) { if
					 * (bean.getEmpPanNo().trim().equalsIgnoreCase(kycDetail.
					 * getEmpPanNo().trim())) {
					 * kycDetail.setEmpNameAsPerPanCard(bean.
					 * getEmpNameAsPerPanCard());
					 * kycDetail.setEmpPanNo(bean.getEmpPanNo()); } else { throw
					 * new CommonCustomException(
					 * "You can't update PanCard Details please raise the request to Finance team to update PanCard Details"
					 * ); }
					 * 
					 * } else { kycDetail.setEmpNameAsPerPanCard(bean.
					 * getEmpNameAsPerPanCard());
					 * kycDetail.setEmpPanNo(bean.getEmpPanNo()); } if
					 * (kycDetail.getAadharNo() != null && (bean.getAdharNo() !=
					 * null)) { if
					 * (kycDetail.getAadharNo().trim().equalsIgnoreCase(bean.
					 * getAdharNo().trim())) {
					 * kycDetail.setEmpNameAsPerAadharCard(bean.
					 * getEmpNameAsPerAadharCard());
					 * kycDetail.setAadharNo(bean.getAdharNo()); } else { throw
					 * new CommonCustomException(
					 * "You can't update Aadhaar Details please raise the request to Finance team to update PanCard Details"
					 * ); } } else { kycDetail.setEmpNameAsPerAadharCard(bean.
					 * getEmpNameAsPerAadharCard());
					 * kycDetail.setAadharNo(bean.getAdharNo()); }
					 */
					if (personalDetails != null && kycDetail != null) {

						personalDetails.setFkEmpDetailId(bean.getEmpId());
						String phoneRegex = "^[2-9]{2}[0-9]{8}$";
						boolean pregex = bean.getMobileNumber().matches(phoneRegex);
						boolean homeRegex = bean.getHomeNumber().matches(phoneRegex);
						if (bean.getHomeNumber() == null || bean.getHomeNumber() == " ") {
							throw new CommonCustomException(
									"Mobile Number is mandatory and please provide valid Mobile Number");
						} else {
							if (pregex == false || homeRegex == false) {
								throw new CommonCustomException("please enter valid Phone Number Contaning 10 Digits");
							} else {
								personalDetails.setEmpMobileNo(bean.getMobileNumber());
								personalDetails.setEmpAlternateMobileNo(bean.getHomeNumber());
							}
						}
						List<DatEmpPersonalDetailBO> mailIdDetails = new ArrayList<DatEmpPersonalDetailBO>();
						mailIdDetails = datEmpPersonalDetailsRepository
								.findByEmpPersonalEmailId(bean.getPersonalEmail());
						List<DatEmpPersonalDetailBO> mailIdSameEmp = new ArrayList<DatEmpPersonalDetailBO>();
						mailIdSameEmp = datEmpPersonalDetailsRepository
								.checkForDuplicateEmailIdOfSameEmployee(bean.getEmpId(), bean.getPersonalEmail());
						if (bean.getPersonalEmail() == null || bean.getPersonalEmail().isEmpty()) {
							throw new CommonCustomException("Email Id should be mandatory");
						} else if ((mailIdDetails.size() == 0 && mailIdSameEmp.size() == 0)
								|| (mailIdDetails.size() == 1 && mailIdSameEmp.size() == 1)) {
							String emailregex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
									+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
							Boolean regex = bean.getPersonalEmail().matches(emailregex);

							if (regex == false) {
								throw new CommonCustomException("please enter valid Email Id");
							} else {
								personalDetails.setEmpPersonalEmailId(bean.getPersonalEmail());
							}

						} else if ((mailIdDetails.size() == 1 && mailIdSameEmp.size() == 0)) {

							throw new CommonCustomException("this email id is allready exists");
						}
						if (bean.getMaritialStatus().equalsIgnoreCase("single")) {
							personalDetails.setEmpMaritalStatus(single);
						} else if (bean.getMaritialStatus().equalsIgnoreCase("married")) {
							personalDetails.setEmpMaritalStatus(married);
						} else {
							throw new CommonCustomException("provide Valid input for Maritail Status(single/married)");
						}
					/*	if (((bean.getPresentAddress() == null) || "".equals(bean.getPresentAddress().trim()))) {
							throw new CommonCustomException("Present Address is mendatory");
						} else {
							personalDetails.setEmpPresentAddress(bean.getPresentAddress());
						}
						if (((bean.getPermanentAddress() == null) || "".equals(bean.getPermanentAddress().trim()))) {
							throw new CommonCustomException("Permanant Address is mendatory");
						} else {

							personalDetails.setEmpPermanentAddress(bean.getPermanentAddress());
						}*/

						List<DatEmpKycDetailBO> kycDetailList = new ArrayList<DatEmpKycDetailBO>();
						kycDetailList = kycRepository.findByEmpPassportNo(bean.getPassportNumber());
						if (kycDetailList.size() == 0) {
							String passportRegex = "^[0-9a-zA-Z]+$";
							boolean pregexBoolean = bean.getPassportNumber().matches(passportRegex);

							if (pregexBoolean == false) {
								throw new CommonCustomException(
										"please enter valid Passport Number which is alphanumeric ");
							} else {
								kycDetail.setEmpPassportNo(bean.getPassportNumber());

							}
						} else {
							throw new CommonCustomException("Passport Number is allready exists");
						}

						for (char c : bean.getPlaceOfIssue().toCharArray()) {
							if (Character.isDigit(c)) {
								throw new CommonCustomException("Place of issue should not be a Digit");
							} else {
								kycDetail.setPassportplaceOfIssue(bean.getPlaceOfIssue());
							}
						}
						String fDate = bean.getDateOfIssue();
						String tDate = bean.getExpiryDate();

						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

						Date dOIssue = null;
						Date expDate = null;
						df.setLenient(false);

						try {

							dOIssue = df.parse(fDate);
							expDate = df.parse(tDate);

							if (dOIssue.getTime() > expDate.getTime()) {
								throw new CommonCustomException("dateOf Issue should be less than to Expiry date");
							}

							kycDetail.setPassportDateOfIssue(dOIssue);
							kycDetail.setPassportExpiryDate(expDate);

						} catch (Exception e) {
							e.printStackTrace();
							throw new CommonCustomException(e.getMessage());
						}

						personalDetails.setEmpPersonalDetailModifiedBy(bean.getEmpId());
						personalDetails.setEmpPersonalDetailModifiedDate(new Date());
						kycDetail.setEmpNameAsPerVoterId(bean.getEmpNameAsPerVoterId());
						kycDetail.setEmpVoterIdNo(bean.getEmpVoterIdNo());

						datEmpPersonalDetailsRepository.save(personalDetails);
						kycRepository.save(kycDetail);
						flag = true;
					} else {
						throw new DataAccessException("Some exception occurred while Saving employee personal detail");
					}
				} else {
					throw new CommonCustomException("You are not Authorized to change the details");
				}
				try {
					if (listOfMapFiles.size() > 0) {

						for (Map.Entry<String, MultipartFile> map : listOfMapFiles.entrySet()) {

							String[] name = map.getValue().getOriginalFilename().split("\\.");
							String newFileName = map.getKey() + "." + name[1].toString();
							File dir = new File(kycBasePath + bean.getEmpId());

							if (dir.mkdirs()) {
								File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
								FileCopyUtils.copy(map.getValue().getBytes(), fileToBeStored);
							} else {
								File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
								FileCopyUtils.copy(map.getValue().getBytes(), fileToBeStored);
							}
						}

					}
				} catch (IOException e) {
					throw new CommonCustomException("Document files not uploaded");
				}
			} catch (CommonCustomException c) {
				flag = false;

				throw new CommonCustomException(c.getMessage());
			}

			LOG.endUsecase("About to update Date is completed");
			return flag;
		}

	}

	/**
	 * <Description addUser:> This method is used to update extension
	 * 
	 * @param user
	 * @return MIS Response
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	public Boolean updateExtensionNumber(UpdateExtensionNumBean bean)
			throws CommonCustomException, DataAccessException {

		LOG.debug("About to update data");
		Boolean success = false;
		DatEmpProfessionalDetailBO extensionDetails = new DatEmpProfessionalDetailBO();

		// Collect all role ids of System HR.
		List<String> hrRoleIdList = new ArrayList<String>();
		String[] empIdArray = hrRoles.split(",");
		for (String s : empIdArray)
			hrRoleIdList.add(s);
		DatEmpDetailBO hrEmpDetailBo = new DatEmpDetailBO();

		try {
			hrEmpDetailBo = datEmpRepository.getSystemAdminRoleDetailsForUserManagmentModule(bean.getLoggedInEmpId(),
					hrRoleIdList);
			extensionDetails = datEmpProfessionalDetailsRepository.findByFkMainEmpDetailId(bean.getEmpId());

		} catch (Exception e) {
			throw new DataAccessException("invalid Employee ID");
		}

		if (bean.getLoggedInEmpId() == null || bean.getLoggedInEmpId() == 0) {
			throw new CommonCustomException("please enter valid LoggedIn user ID");
		} else if (hrEmpDetailBo == null) {
			throw new CommonCustomException("invalid LoggedIn user ID");
		} else {
			if (bean.getEmpId().equals(bean.getLoggedInEmpId()) || hrEmpDetailBo != null) {
				if (hrEmpDetailBo != null && hrEmpDetailBo.getFkEmpMainStatus() == 2) {
					throw new CommonCustomException("Only active HR team Employees can Update the Extension Number");
				} else {
					if (extensionDetails != null) {
						String extensionRegex = "[0-9]+";
						Boolean regex = bean.getExtensionNumber().matches(extensionRegex);
						if (bean.getExtensionNumber() == null) {
							throw new CommonCustomException("Extension Number is mandatory");
						} else if (bean.getExtensionNumber().length() <= 0 || regex == false) {
							throw new CommonCustomException("Please enter valid Extension Number");
						} else {
							extensionDetails.setEmpExtensionNumber(bean.getExtensionNumber());
							datEmpProfessionalDetailsRepository.save(extensionDetails);
						}
					} else {
						throw new CommonCustomException("invalid Employee ID");
					}
				}
			}

			else {
				throw new CommonCustomException("You are not Authorized to change the details");
			}
		}

		success = true;

		LOG.endUsecase("Update User Extension Number");
		return success;

	}

	/**
	 * <Description addUser:> This method is used to update email id by admin
	 * 
	 * @param user
	 * @return Boolean
	 * @throws DataAccessException
	 * @throws CommonCustomException
	 */
	@SuppressWarnings("unused")
	public boolean updateEmailID(UpdateEmailIDBean empBO) throws DataAccessException, CommonCustomException {
		LOG.debug("About to update data");
		Boolean success = false;
		// Collect all role ids of System Admin.
		List<String> systemAdminRoleIdList = new ArrayList<String>();
		String[] empIdArray = systemAdminRoles.split(",");
		for (String s : empIdArray)
			systemAdminRoleIdList.add(s);
		DatEmpDetailBO systemAdminEmpDetailBo = new DatEmpDetailBO();

		systemAdminEmpDetailBo = datEmpRepository
				.getSystemAdminRoleDetailsForUserManagmentModule(empBO.getLoggedInEmpId(), systemAdminRoleIdList);

		if (empBO.getLoggedInEmpId() == null || empBO.getLoggedInEmpId() == 0) {
			throw new CommonCustomException("Invalid logged in User ID");
		} else if (empBO.getEmpId() == null || empBO.getEmpId() == 0) {
			throw new CommonCustomException("Invalid Employee ID");
		}
		if (systemAdminEmpDetailBo != null) {

			if (systemAdminEmpDetailBo.getFkEmpMainStatus() != 1) {
				throw new CommonCustomException("Inactive admins can't update the employee details");
			} else {
				List<DatEmpProfessionalDetailBO> mailIdDetails = new ArrayList<DatEmpProfessionalDetailBO>();
				DatEmpDetailBO datEmpDetail = datEmpRepository.findByPkEmpId(empBO.getEmpId());
				if (datEmpDetail != null) {
					Byte empType = datEmpDetail.getEmpType();
					if (empBO.getEmailID() == null) {
						throw new CommonCustomException("Email Id should Not be Null");
					} else {
						mailIdDetails = datEmpProfessionalDetailsRepository
								.findByEmpProfessionalEmailId(empBO.getEmailID());
					}

					if (mailIdDetails.size() == 0) {
						String emailregex = "^[a-zA-Z0-9._%+-]+@thbs\\.com$";
						Boolean regex = empBO.getEmailID().matches(emailregex);
						if (regex == false) {
							throw new CommonCustomException("please enter valid Email Id");
						} else {
							DatEmpProfessionalDetailBO mailIdDetail = datEmpProfessionalDetailsRepository
									.findByFkMainEmpDetailId(empBO.getEmpId());

							mailIdDetail.setEmpProfessionalEmailId(empBO.getEmailID());
							mailIdDetail.setProfessionalDetailModifiedBy(empBO.getLoggedInEmpId());
							mailIdDetail.setProfessionalDetailModifiedDate(new Date());

							if (datEmpDetail.getFkEmpMainStatus() == empWaitingStatus
									&& mailIdDetail.getFkEmpDomainMgrId() != null
									&& mailIdDetail.getFkEmpDesignation() != null) {
								datEmpDetail.setFkEmpMainStatus(empActiveStatus);
								datEmpDetail.setModifiedBy(empBO.getLoggedInEmpId());
								datEmpDetail.setModifiedDate(new Date());
								empDetailRepository.save(datEmpDetail);
							}
							datEmpProfessionalDetailsRepository.save(mailIdDetail);
						}
					} else {
						throw new CommonCustomException("This Email Id is allready exists");
					}
				} else {
					throw new CommonCustomException("invalid Employee ID");
				}
			}

		} else {
			throw new CommonCustomException("You are not authorized to update the details");
		}
		success = true;
		LOG.endUsecase("Update User Data");
		return success;
	}

	/**
	 * <Description getAllEmpDetailsONDateRange:> Employee Details under given
	 * Date Range
	 * 
	 * @param dateRange
	 * @return MIS Response
	 * @throws CommonCustomException
	 * @throws ParseException
	 * @throws DataAccessException
	 */
	@SuppressWarnings("unused")
	public List<DateRangeOutputBean> viewAllEmpDetailsONDateRange(DateRangeBean dateRange)
			throws CommonCustomException, ParseException, DataAccessException {

		LOG.startUsecase("Entering View EMP Details service");

		List<DatEmpProfessionalDetailBO> tempEmpIdList = new ArrayList<>();
		List<DatEmpPersonalDetailBO> listOfDatEmpPersonalDetailBO = new ArrayList<>();

		List<String> systemAdminRoleIdList = new ArrayList<String>();
		String[] empIdArray = systemAdminRoles.split(",");
		for (String s : empIdArray)
			systemAdminRoleIdList.add(s);
		DatEmpDetailBO systemAdminEmpDetailBo = new DatEmpDetailBO();

		try {
			systemAdminEmpDetailBo = datEmpRepository.getSystemAdminRoleDetailsForUserManagmentModule(
					dateRange.getLoggedInEmpId(), systemAdminRoleIdList);
			Short roleId = systemAdminEmpDetailBo.getFkEmpRoleId();
		} catch (Exception e) {
			throw new CommonCustomException("The requestor is not having system admin role");
		}
		String fDate = dateRange.getFromDate();
		String tDate = dateRange.getToDate();

		final List<DateRangeOutputBean> listOfDateRangeOutputBean = new ArrayList<>();

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDate = null;
		Date toDate = null;
		df.setLenient(false);

		try {

			fromDate = df.parse(fDate);
			toDate = df.parse(tDate);
			if (fromDate.getTime() > toDate.getTime()) {
				throw new CommonCustomException("From date should be less than to date");
			} else if (fromDate.getTime() == 0 && toDate.getTime() == 0) {
				throw new CommonCustomException("both Dates should not be null");
			}

			tempEmpIdList = datEmpProfessionalDetailsRepository.getEmployeeIdByDateOfJoiningDateRange(fromDate, toDate);

		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		if (systemAdminEmpDetailBo != null) {
			for (final DatEmpProfessionalDetailBO datEmpProfessionalDetailBO : tempEmpIdList) {

				final DateRangeOutputBean dateRangeOutputBean = new DateRangeOutputBean();

				dateRangeOutputBean.setEmpId(datEmpProfessionalDetailBO.getFkMainEmpDetailId());
				if (datEmpProfessionalDetailBO.getEmpMiddleName() == null) {
					dateRangeOutputBean.setEmpName(datEmpProfessionalDetailBO.getEmpFirstName() + " "
							+ datEmpProfessionalDetailBO.getEmpLastName());
				} else {
					dateRangeOutputBean.setEmpName(datEmpProfessionalDetailBO.getEmpFirstName() + " "
							+ datEmpProfessionalDetailBO.getEmpMiddleName() + " "
							+ datEmpProfessionalDetailBO.getEmpLastName());
				}
				dateRangeOutputBean.setEmpTypeID(datEmpProfessionalDetailBO.getEmpType());
				if (datEmpProfessionalDetailBO.getEmpType() == 1) {
					dateRangeOutputBean.setEmpType("Permanent");
				} else if (datEmpProfessionalDetailBO.getEmpType() == 2) {
					dateRangeOutputBean.setEmpType("Contract");
				} else if (datEmpProfessionalDetailBO.getEmpType() == 3) {
					dateRangeOutputBean.setEmpType("FTE");
				} else if (datEmpProfessionalDetailBO.getEmpType() == 4) {
					dateRangeOutputBean.setEmpType("Trainee");
				}
				dateRangeOutputBean.setBuUnitId(datEmpProfessionalDetailBO.getPkBuUnitId());
				dateRangeOutputBean.setBuUnitName(datEmpProfessionalDetailBO.getBuUnitName());
				dateRangeOutputBean.setBootCampId(datEmpProfessionalDetailBO.getPkBootCampId());
				dateRangeOutputBean.setBootCampName(datEmpProfessionalDetailBO.getBootCampName());
				dateRangeOutputBean.setReportingManager(datEmpProfessionalDetailBO.getFkEmpReportingMgrId());
				listOfDatEmpPersonalDetailBO = datEmpPersonalDetailsRepository
						.findEmpDetailsByFkEmpReportingMgrId(datEmpProfessionalDetailBO.getFkEmpReportingMgrId());
				if (listOfDatEmpPersonalDetailBO.get(0).getEmpMiddleName() == null) {
					dateRangeOutputBean.setReportingManagerName(listOfDatEmpPersonalDetailBO.get(0).getEmpFirstName()
							+ " " + listOfDatEmpPersonalDetailBO.get(0).getEmpLastName());

				} else {
					dateRangeOutputBean.setReportingManagerName(listOfDatEmpPersonalDetailBO.get(0).getEmpFirstName()
							+ " " + listOfDatEmpPersonalDetailBO.get(0).getEmpMiddleName() + " "
							+ listOfDatEmpPersonalDetailBO.get(0).getEmpLastName());
				}
				dateRangeOutputBean.setEmailID(datEmpProfessionalDetailBO.getEmpProfessionalEmailId());

				listOfDateRangeOutputBean.add(dateRangeOutputBean);
			}
		}

		else {
			throw new CommonCustomException("You are not Authorized to change the details");
		}
		return listOfDateRangeOutputBean;

	}

	// Added by Balaji
	/**
	 * @category This service is used to get Employee Specialization details
	 * 
	 * @return empSpecOutputListBean
	 * @throws CommonCustomException
	 */
	public List<MasEmployeeSpecializationBean> getEmpSpecDetails() throws CommonCustomException {

		LOG.startUsecase("Entering getEmpSpecDetails service");
		List<MasEmployeeSpecializationBean> empSpecOutputListBean = new ArrayList<MasEmployeeSpecializationBean>();
		List<MasEmpSpecializationBO> empSpecBO = new ArrayList<MasEmpSpecializationBO>();

		try {
			empSpecBO = masEmpSpecRepository.findAll();

		} catch (Exception e) {

			throw new CommonCustomException("Failed to Employee Specialization details");
		}
		if (empSpecBO == null) {

			throw new CommonCustomException("Failed to Employee Specialization details");

		}
		for (MasEmpSpecializationBO masEmpSpecializationBO : empSpecBO) {

			MasEmployeeSpecializationBean empSpecOutputBean = new MasEmployeeSpecializationBean();
			empSpecOutputBean.setSpecializationId(masEmpSpecializationBO.getPkSpecializationId());
			empSpecOutputBean.setSpecializationName(masEmpSpecializationBO.getSpecializationName());
			empSpecOutputListBean.add(empSpecOutputBean);
		}
		LOG.endUsecase("Exiting getEmpSpecDetails service");
		return empSpecOutputListBean;
	}

	/**
	 * @category This service is used to get Employee Stream details
	 * 
	 * @return empStreamOutputListBean
	 * @throws CommonCustomException
	 */
	public List<MasEmployeeStreamBean> getEmpStreamDetails() throws CommonCustomException {

		LOG.startUsecase("Entering getEmpStreamDetails service");
		List<MasEmployeeStreamBean> empStreamOutputListBean = new ArrayList<MasEmployeeStreamBean>();
		List<MasEmpStreamBO> empStreamBO = new ArrayList<MasEmpStreamBO>();

		try {
			empStreamBO = masEmpStreamRepository.findAll();

		} catch (Exception e) {

			throw new CommonCustomException("Failed to Employee Stream details");
		}
		if (empStreamBO == null) {

			throw new CommonCustomException("Failed to Employee Stream details");

		}
		for (MasEmpStreamBO masEmpStreamBO : empStreamBO) {
			MasEmployeeStreamBean outputBean = new MasEmployeeStreamBean();
			outputBean.setStreamId(masEmpStreamBO.getPkStreamId());
			outputBean.setStreamName(masEmpStreamBO.getStreamName());
			empStreamOutputListBean.add(outputBean);

		}
		LOG.endUsecase("Exiting getEmpStreamDetails service");
		return empStreamOutputListBean;
	}
	// EOA of Balaji

	// Added by Adavayya Hiremath
	/**
	 *
	 * <Description createEmpProfDetail:> This service helps to create Employee
	 * Details
	 *
	 * @param createEmpProfDetailBeans
	 * @return MISResponse
	 * @throws BusinessException
	 * @throws DataAccessException
	 * @throws ParseException
	 * @throws MethodArgumentNotValidException
	 */
	public List<DatEmpEmploymentDetailBO> createProfEmploymentDetail(
			List<CreateEmpProfDetailBean> createEmpProfDetailBeans)
			throws DataAccessException, ParseException, MethodArgumentNotValidException {
		LOG.startUsecase("create professional Employee details");
		List<DatEmpEmploymentDetailBO> datEmpEmploymentDetailBOs = new ArrayList<DatEmpEmploymentDetailBO>();
		DatEmpEmploymentDetailBO dateEmpEmploymentDetailBO;

		try {

			for (int i = 0; i < createEmpProfDetailBeans.size(); i++) {
				dateEmpEmploymentDetailBO = new DatEmpEmploymentDetailBO();
				dateEmpEmploymentDetailBO.setFkEmpId(createEmpProfDetailBeans.get(i).getEmpId());
				dateEmpEmploymentDetailBO.setEmpEmployerName(createEmpProfDetailBeans.get(i).getEmployerName());
				dateEmpEmploymentDetailBO.setEmpEmploymentDesg(createEmpProfDetailBeans.get(i).getPositionHeld());
				dateEmpEmploymentDetailBO.setStartDate(createEmpProfDetailBeans.get(i).getDurationFromDate());
				dateEmpEmploymentDetailBO.setEndDate(createEmpProfDetailBeans.get(i).getDurationToDate());
				dateEmpEmploymentDetailBO.setReasonForLeaving(createEmpProfDetailBeans.get(i).getReasonForLeaving());
				dateEmpEmploymentDetailBO.setCreatedBy(createEmpProfDetailBeans.get(i).getLoggedInEmpID());
				dateEmpEmploymentDetailBO.setCreatedDate(new Date());

				DatEmpEmploymentDetailBO findByLastRow = datEmpEmploymentRepository.findByLastRow();
				if (findByLastRow != null)
					dateEmpEmploymentDetailBO.setEmployerSequenceNumber(findByLastRow.getEmployerSequenceNumber() + 1);
				else
					dateEmpEmploymentDetailBO.setEmployerSequenceNumber(sequenceNo);

				List<DatEmpEmploymentDetailBO> findByFkEmpId = datEmpEmploymentRepository
						.findByFkEmpId(createEmpProfDetailBeans.get(i).getEmpId());

				for (int j = 0; j < findByFkEmpId.size(); j++) {
					dateEmpEmploymentDetailBO.setPkEmploymentId(findByFkEmpId.get(j).getPkEmploymentId());
					dateEmpEmploymentDetailBO
							.setEmployerSequenceNumber(findByFkEmpId.get(j).getEmployerSequenceNumber());
				}
				datEmpEmploymentDetailBOs.add(dateEmpEmploymentDetailBO);
				datEmpEmploymentRepository.save(dateEmpEmploymentDetailBO);
			}

		} catch (Exception e) {
			LOG.error("Exception occured", e);
			throw new DataAccessException("Exception occured", e);
		}
		LOG.endUsecase("create professional Employee details");
		return datEmpEmploymentDetailBOs;
	}

	// EOA by Adavayya Hiremath
	
	
	public Boolean employeeLocationChangeByPcService(EmpLocChangeByPcBean locBean) throws DataAccessException{
		
		LOG.startUsecase("Entering employeeLocationChangeByPcService service");
		
		Boolean flag = false;
		DatEmpProfessionalDetailBO profDetails = new DatEmpProfessionalDetailBO();
		DatEmpLocationApprovalBO empLocBo = new DatEmpLocationApprovalBO();
		DatEmpLocationApprovalBO empLocBoTwo = new DatEmpLocationApprovalBO();
		try{
			
			try{
			
				profDetails = datEmpProfessionalDetailsRepository.findByFkMainEmpDetailId(locBean.getEmpId());
				
				empLocBo = locationApprovalRepo.findByFkEmpIdAndStatus(locBean.getEmpId(),(byte)1);
			
				
			}catch(Exception d){
				
				throw new DataAccessException("Exception occurred while fetching employee details from DB ");
			}
		
		
		try{
			if(empLocBo != null){
			
				empLocBo.setNewLocation(locBean.getCurrentLocation());
				empLocBo.setModifiedBy(locBean.getLoggedEmpId());
				empLocBo.setModifiedDate(new Date());
			
				locationApprovalRepo.save(empLocBo);
				
			}else{
				empLocBoTwo.setFkEmpId(locBean.getEmpId());
				empLocBoTwo.setNewLocation(locBean.getCurrentLocation());
				empLocBoTwo.setOldLocation(profDetails.getFkEmpLocationParentId());
				empLocBoTwo.setRequestedBy(locBean.getLoggedEmpId());
				empLocBoTwo.setRequestedDate(new Date());
				empLocBoTwo.setStatus((byte)1);
				
				locationApprovalRepo.save(empLocBoTwo);
			}
			flag = true;
		}catch(Exception e){
		
			throw new DataAccessException("Exception occurred while inserting employee location details to DB ");
		}
			
		}catch(Exception e){
			
			throw new DataAccessException(e.getMessage());
		}
		
		LOG.endUsecase("create employeeLocationChangeByPcService details");
		
		return flag;
	}
}
