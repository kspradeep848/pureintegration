package com.thbs.mis.usermanagement.constants;

public class UserManagementURIConstants {
	
	//Added by Adavayya Hiremath
	public static final String EMP_DETAILS_BY_ID = "/usermanagement/employee/empdetails/{empId}";
	public static final String UPDATE_PERSONAL_DETAILS_BY_EMPLOYEE_ID = "/usermanagement/employee/personal";
	public static final String UPDATE_EXTENSION_NUMBER_BY_EMPLOYEE = "/usermanagement/emp/extension";
    public static final String UPDATE_EMAIL_BY_ADMIN = "/usermanagement/sysadmin/email";
    public static final String DATE_RANGE_EMP_DETAIL = "/usermanagement/sysadmin/empdetails";

	//Added by Kamal Anand 
	
	public static final String HR_DEACTIVATE_EMP_PROFILE = "/usermanagement/hr/deactivate";
	
	public static final String HR_ACTIVE_EMP_SUBSTATUS_CHANGE = "/usermanagement/hr/active/substatus";
	
	public static final String HR_ADVANCED_SEARCH = "/usermanagement/hr/advancesearch";
	
	//End of Addition by Kamal Anand
	
	public static final String HR_CREATE_EMP_PROFILE = "/usermanagement/hr/empdetail";
	public static final String HR_CREATE_EMP_EDUCATION = "/usermanagement/hr/educational";
	
	public static final String HR_GET_EMP_DETAILS = "/usermanagement/hr/empdetails/{empId}";
	public static final String UPDATE_PERSONAL_EMP_DETAILS = "/usermanagement/hr/personaldetail";
	
	public static final String HR_CHANGE_EMP_TYPE = "/usermanagement/hr/emptype/{empId}/loggedinid/{loggedInEmpId}";
	
	public static final String EMPLOYEE_LOCATION = "/usermanagement/hr/emplocation";
	
	public static final String EMPLOYEE_RECRUITER_DETAIL = "/usermanagement/hr/recruiter";
	
	public static final String UPDATE_EMP_LOC_BY_HR = "/usermanagement/hr/recruiter";
	
	//Added by Adavayya
	public static final String EMPLOYEE_RECRUITMENT_UPDATE = "/usermanagement/hr/recruitment";
	
	public static final String EMPLOYEE_LOCATION_CHANGE_BY_PC = "/usermanagement/pc/emplocation";
	
	public static final String APPROVE_EMPLOYEE_LOCATION_CHANGE_BY_HR = "/usermanagement/hr/approve/emplocation/pkid/{pkLocId}/status/{status}";
	
	public static final String GET_EMPLOYEE_LOC_CHANGE_DETAIL_BY_HR = "/usermanagement/hr/locations";
	
}
