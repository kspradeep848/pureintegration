package com.thbs.mis.usermanagement.constants;

public class UserManagementConstants {
	
	public static final String GET_ALL_EMPLOYEE_SPECIALIZATION_DETAILS = "/user/specialization/details"; // added by Balaji
	
	public static final String GET_ALL_EMPLOYEE_STREAM_DETAILS = "/user/stream/details"; // added by Balaji
	
	public static final String CREATE_PROF_EMP_DETAILS = "/usermanagement/hr/professional"; //Added by Adavayya Hiremath

	public static final String UPDATE_ORGNIZATION_DETAIL = "/usermanagement/hr/organizational"; //Added by Adavayya Hiremath
	
	public static final String UPDATE_LOCATION_DETAIL = "/usermanagement/hr/organizational/location"; //Added by Adavayya Hiremath
	
	
}
