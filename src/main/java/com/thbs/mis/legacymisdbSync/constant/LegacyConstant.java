/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  LegacyConstant.java                               */
/*                                                                   */
/*  Author      :  Anil kumar B K                                    */
/*                                                                   */
/*  Date        : January 09, 2017                                   */
/*                                                                   */
/*  Description : This is used to create URI for Legacy  services.   */
/*		 									                         */
/*                                                                   */
/*********************************************************************/
/* Date                 Who     Version      Comments                */
/*-------------------------------------------------------------------*/
/* January 09, 2017     THBS     1.0        Initial version created  */
/*********************************************************************/
package com.thbs.mis.legacymisdbSync.constant;

public class LegacyConstant {

	public static final String CREATE_OLD_MIS_USER = "/legacy/mis/user";
	public static final String UPDATE_OLD_MIS_USER = "/legacy/mis/user";
	public static final String UPDATE_OLD_MIS_USER_CONFIRMATION = "/legacy/mis/user/confirmation";
	public static final String UPDATE_OLD_MIS_USER_TERMS_AND_CONDITION_STATUS = "/legacy/mis/user/termsAndConditionStatus";
	public static final String UPDATE_OLD_MIS_USER_AADHAR = "/legacy/mis/user/aadharNumber";
	public static final String UPDATE_OLD_MIS_USER_CHANGE_PASSWORD = "/legacy/mis/user/changePassword";
	public static final String EMPLOYEE_SEPARATION_STATUS = "/legacy/separation/employee/status/{empId}";
	
}