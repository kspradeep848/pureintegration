/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  LegacyUserController.java                         */
/*                                                                   */
/*  Author      :  Anil kumar B K                                    */
/*                                                                   */
/*  Date        : January 09, 2017                                   */
/*                                                                   */
/*  Description : Controller class for Legacy user                   */
/*		 									                         */
/*                                                                   */
/*********************************************************************/
/* Date                 Who     Version      Comments                */
/*-------------------------------------------------------------------*/
/* January 09, 2017     THBS     1.0        Initial version created  */
/*********************************************************************/
package com.thbs.mis.legacymisdbSync.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.MultipartFilter;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.legacymisdbSync.constant.LegacyConstant;
import com.thbs.mis.legacymisdbSync.bean.MisUser;
import com.thbs.mis.legacymisdbSync.service.LegacyUserService;

@Controller
public class LegacyUserController {

	/**
	 * Instance of <code>LOG</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(LegacyUserController.class);

	/**
	 * Instance of <code>LegacyUserService</code>
	 */
	@Autowired
	private LegacyUserService userService;

	/**
	 * 
	 * <Description addUser:> This service is used to create user profile from
	 * legacy MIS to New MIS
	 * 
	 * @param user
	 * @return ResponseEntity
	 * @throws DataAccessException
	 */
	@RequestMapping(value = LegacyConstant.CREATE_OLD_MIS_USER, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> oldMisAddUser(@RequestBody MisUser user) {

		LOG.startUsecase("oldMisAddUser post");

		Boolean success = false;
		try {
			success = userService.saveUserFromOldMis(user);
		} catch (Exception e) {
			e.printStackTrace();
		}

		LOG.endUsecase("oldMisAddUser");
		if (success == true) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"User profile created successfully"));
		} else {
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Some error occured while creating User profile."));
		}

	}

	/**
	 * 
	 * <Description addUser:> This service is used to update user profile from
	 * legacy MIS to New MIS
	 * 
	 * @param user
	 * @return ResponseEntity
	 * @throws DataAccessException
	 */
	@RequestMapping(value = LegacyConstant.UPDATE_OLD_MIS_USER, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> oldMisUpdateUser(
			@RequestBody MisUser user) {

		LOG.startUsecase("oldMisUpdateUser");

		Boolean success = false;
		try {
			success = userService.updateUserFromOldMis(user);
		} catch (Exception e) {
			e.printStackTrace();
		}

		LOG.endUsecase("oldMisUpdateUser");
		if (success == true) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"User profile updated successfully"));
		} else {
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Some error occured while updating User profile."));
		}

	}

	/**
	 * 
	 * <Description addUser:> This service is used to update confirmation status
	 * of user profile from legacy MIS to New MIS
	 * 
	 * @param user
	 * @return ResponseEntity
	 * @throws DataAccessException
	 */
	@RequestMapping(value = LegacyConstant.UPDATE_OLD_MIS_USER_CONFIRMATION, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> oldMisUpdateUserConfirmation(
			@RequestBody MisUser user) {

		LOG.startUsecase("oldMisUpdateUserConfirmation ");

		Boolean success = false;
		try {
			success = userService.updateUserConfirmationFromOldMis(user);
		} catch (Exception e) {
			LOG.error("Exception while updating user profile for confirmation");
		}
		LOG.endUsecase("oldMisUpdateUserConfirmation");
		if (success == true) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"User profile updated successfully"));
		} else {
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Some error occured while updating User profile."));
		}

	}

	/**
	 * 
	 * <Description addUser:> This service is used to update acceptance status
	 * of terms and condition of company into user profile from legacy MIS to
	 * New MIS
	 * 
	 * @param empId
	 * @return ResponseEntity
	 * @throws DataAccessException
	 */
	@RequestMapping(value = LegacyConstant.UPDATE_OLD_MIS_USER_TERMS_AND_CONDITION_STATUS, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> oldMisUpdateUserTermsAndCondition(
			@RequestBody MisUser user) {

		LOG.startUsecase("oldMisUpdateUserTermsAndCondition ");

		Boolean success = false;
		try {
			success = userService.oldMisUpdateUserTermsAndCondition(user.getEmpId());
		} catch (Exception e) {
			e.printStackTrace();
		}

		LOG.endUsecase("oldMisUpdateUserTermsAndCondition");
		if (success == true) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"User profile updated successfully"));
		} else {
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Some error occured while updating User profile."));
		}

	}

	/**
	 * <Description addUser:> This service is used to update Aadhar number to
	 * user profile from legacy MIS to New MIS
	 * 
	 * @param aadharNumber
	 * @param empId
	 * @return ResponseEntity
	 * @throws DataAccessException
	 */
	@RequestMapping(value = LegacyConstant.UPDATE_OLD_MIS_USER_AADHAR, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> oldMisUpdateUserAddAadhar(
			@RequestBody MisUser user) {

		LOG.startUsecase("oldMisUpdateUserAddAadhar ");

		Boolean success = false;
		try {
			success = userService
					.oldMisUpdateUserAddAadhar(user.getAadharNumber(), user.getEmpId());
		} catch (Exception e) {
			e.printStackTrace();
		}

		LOG.endUsecase("oldMisUpdateUserAddAadhar");
		if (success == true) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"User profile updated successfully"));
		} else {
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Some error occured while updating User profile."));
		}

	}

	
	/**
	 * <Description addUser:> This service is used to update Change password date to
	 * user profile from legacy MIS to New MIS
	 * 
	 * @param user
	 * @return ResponseEntity
	 * @throws DataAccessException
	 */
	@RequestMapping(value = LegacyConstant.UPDATE_OLD_MIS_USER_CHANGE_PASSWORD, method = RequestMethod.PUT)
	public ResponseEntity<MISResponse> oldMisUpdateUserChangePassword(
			@RequestBody MisUser user) {

		LOG.startUsecase("oldMisUpdateUserChangePassword ");

		Boolean success = false;
		try {
			success = userService
					.oldMisUpdateUserChangePassword(user.getPasswordUpdatedOn(), user.getEmpId());
		} catch (Exception e) {
			e.printStackTrace();
		}

		LOG.endUsecase("oldMisUpdateUserChangePassword");
		if (success == true) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"User profile updated successfully"));
		} else {
			return ResponseEntity.status(
					HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
					new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							MISConstants.FAILURE,
							"Some error occured while updating User profile."));
		}

	}
	
	/**
	 * <Description addUser:> This service is used to check for employee separation is RM approval status 
	 * 
	 * @param empId
	 * @return ResponseEntity
	 * @throws DataAccessException
	 */
	@RequestMapping(value = LegacyConstant.EMPLOYEE_SEPARATION_STATUS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> employeeSeparationStatus(
			  @PathVariable Integer empId) {

		LOG.startUsecase("employeeSeparationStatus ");

	
		ResponseEntity<MISResponse> out = null;
		try {
			out = userService
					.employeeSeparationStatus(empId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		LOG.endUsecase("employeeSeparationStatus");
		
		return out;

	}
	

}
