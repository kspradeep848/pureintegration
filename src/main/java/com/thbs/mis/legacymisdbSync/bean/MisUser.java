/********************************************************************/
/*                  FILE HEADER                                     */
/********************************************************************/
/*                                                                  */
/*                                                                  */
/*  FileName    :  User.java                                        */
/*                                                                  */
/*  Author      :  Santosh B.M (THBS)                               */
/*                                                                  */
/*  Date        :  Oct 11,2005                                      */
/*                                                                  */
/*  Description :  Business object representing the                 */
/*                 user entity.                                     */
/*                                                                  */
/*                                                                  */
/********************************************************************/
/* Date        Who     Version      PRF         Comments            */
/*------------------------------------------------------------------*/
/* Oct 11,2005 THBS      1.0               Initial version created  */
/********************************************************************/

package com.thbs.mis.legacymisdbSync.bean;

import java.util.Date;
import java.util.Vector;


/**
 * Business object representing the user entity.
 * 
 * @author Santosh B.M.
 * @version 1.0, 11/10/2005
 */

public class MisUser extends BaseBO implements Comparable<MisUser> {

	/**
	 * Represents the employee id
	 */
	private int empId;

	/**
	 * Represents the employee first Name.
	 */
	private String firstName;

	/**
	 * Represents the employee middle Name.
	 */
	private String midName;

	/**
	 * Represents the employee last Name.
	 */
	private String lastName;

	/**
	 * Represents the employee initials.
	 */
	private String initials;

	/**
	 * Represents the employee password.
	 */
	private String password;

	/**
	 * Represents the employee password. Used only for encrypting and decryption
	 * purpose.
	 */
	private String tempPassword;

	/**
	 * Represents the employee date of joining.
	 */
	private Date DOJ;
	
	/**
	 * Represents the password update date
	 */
	private Date passwordUpdatedOn;
	
	/**
	 * Represents the employee qualification.
	 */
	private String qual;

	/**
	 * Represents the employee phone number.
	 */
	private String phone;

	/**
	 * Represents the employee extn.
	 */
	private String extn;
	/**
	 * Represents the employee mobileNo.
	 */
	private String mobileNo;
	
	private String consultant;

	public String getConsultant() {
		return consultant;
	}

	public void setConsultant(String consultant) {
		
		this.consultant = consultant;
		
	}
	
	/**
	 * Represents the employee BloodGroup.
	 */
	private String bloodGroup;
	
	
	
	private String aadharNumber;
	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}
	private String aadharNumberValue;
	
	
	public String getAadharNumberValue() {
		return aadharNumberValue;
	}

	public void setAadharNumberValue(String aadharNumberValue) {
		this.aadharNumberValue = aadharNumberValue;
	}

	private Double currentRating;
	public Double getCurrentRating() {
		return currentRating;
	}

	public void setCurrentRating(Double currentRating) {
		this.currentRating = currentRating;
	}

	private Double previousRating;
	
	public Double getPreviousRating() {
		return previousRating;
	}

	public void setPreviousRating(Double previousRating) {
		this.previousRating = previousRating;
	}		
	
			private int citizenship;

			public int getCitizenship() {
				return citizenship;
			}

			public void setCitizenship(int citizenship) {
				this.citizenship = citizenship;
			}

			private int firstCitizenship;

			private int secondCitizenship;

			public int getFirstCitizenship() {
				return firstCitizenship;
			}

			public void setFirstCitizenship(int firstCitizenship) {
				this.firstCitizenship = firstCitizenship;
			}

			public int getSecondCitizenship() {
				return secondCitizenship;
			}

			public void setSecondCitizenship(int secondCitizenship) {
				this.secondCitizenship = secondCitizenship;
			}

			private String firstCitizenshipName;

			public String getFirstCitizenshipName() {
				return firstCitizenshipName;
			}

			public void setFirstCitizenshipName(String firstCitizenshipName) {
				this.firstCitizenshipName = firstCitizenshipName;
			}

			private String secondCitizenshipName;

			public String getSecondCitizenshipName() {
				return secondCitizenshipName;
			}

			public void setSecondCitizenshipName(String secondCitizenshipName) {
				this.secondCitizenshipName = secondCitizenshipName;
			}

			

	/**
	 * Represents the employee Permanent address.
	 */
	private String permanentAddr;

	/**
	 * Represents the employee present address.
	 */
	private String presentAddr;

	/**
	 * Represents the employee designation.
	 */
	private String desg;

	/**
	 * Represents the employee level.
	 */
	private String level;

	/**
	 * Represents the employee location ID.
	 */
	private int locationId;

	/**
	 * Represents the employee organization id.
	 */
	private int organizationId;

	/**
	 * Represents the employee boot camp id.
	 */
	private int bootCampId;

	/**
	 * Represents the employee experience.
	 */
	private float experience;

	/**
	 * Represents the employee reporting manager ID.
	 */
	private int reportingMgrId;

	
	private Date effectiveDateOfChangeInRM;
	/**
	 * Represents the employee competency manager ID.
	 */
	private int competencyMgrId;

	/**
	 * Represents the employee on bench status. true :: employee is on bench.
	 * false :: employee is not on bench.
	 */
	private boolean onBenchFlag;

	/**
	 * Represents the employee login status. true :: employee login is enabled.
	 * false :: employee login is disabled.
	 */
	private boolean loginFlag;

	/**
	 * Represents the vector containing employee's projects.
	 */
	private Vector projectsVector;

	/**
	 * Represents the employee role ID.
	 */
	private int roleId;

	/**
	 * Represents the employee location name.
	 */
	private String locationName;

	/**
	 * Represents the employee organization name.
	 */
	private String organizationName;

	/**
	 * Represents the boot camp name.
	 */
	private String bootCampName;

	/**
	 * Represents the employee email.
	 */
	private String email;

	/**
	 * Represents the employee competency manager name.
	 */
	private String competencyMgrName;

	/**
	 * Represents the employee reporting manager name.
	 */
	private String reportingMgrName;

	/**
	 * Represents the employee in terms of cost unit,used to find out cost
	 * details.
	 */
	private float costUnit;

	private int businessUnitId;

	private String businessUnitName = "";

	private int businessUnitHeadEmpId;

	private String businessUnitHeadName;

	private String businessUnitHeadEmail;

	private int profileCreatedBy;
	
	private String requestType;
	
	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public int getProfileCreatedBy() {
		return profileCreatedBy;
	}

	public void setProfileCreatedBy(int profileCreatedBy) {
		this.profileCreatedBy = profileCreatedBy;
	}
	/**
	 * Represents status id of the user
	 */
	private int statusId;

	/**
	 * Represents status name of the user
	 */
	private String statusName;

	private int noOfReportees;

	private String personalEmail; 
	private String reportingMgrEmail;
	private String domainMgrEmail;
	
	private Integer lmsLocationId;
	public Integer getLmsLocationId() {
		return lmsLocationId;
	}
	public void setLmsLocationId(Integer lmsLocationId) {
		this.lmsLocationId = lmsLocationId;
	}
	
	private Date contractorEndDate;
	
	public String getDomainMgrEmail() {
		return domainMgrEmail;
	}

	public void setDomainMgrEmail(String domainMgrEmail) {
		this.domainMgrEmail = domainMgrEmail;
	}

	public String getReportingMgrEmail() {
		return reportingMgrEmail;
	}

	public void setReportingMgrEmail(String reportingMgrEmail) {
		this.reportingMgrEmail = reportingMgrEmail;
	}
	public void setNoOfReportees(int noOfReportees) {
		this.noOfReportees = noOfReportees;
	}

	public int getNoOfReportees() {
		return noOfReportees;
	}

	
	/**
	 * Represents newstatus id of the user
	 */
	private int newStatusId;

	/**
	 * @param newStatusId
	 *            the newStatusId to set
	 */
	public void setNewStatusId(int newStatusId) {
		this.newStatusId = newStatusId;
	}

	/**
	 * @return the newStatusId
	 */
	public int getNewStatusId() {
		return newStatusId;
	}

	/**
	 * Represents newstatus name of the user
	 */
	private String newStatusName;

	/**
	 * @param newStatusName
	 *            the newStatusName to set
	 */
	public void setNewStatusName(String newStatusName) {
		this.newStatusName = newStatusName;
	}

	/**
	 * @return the newStatusName
	 */
	public String getNewStatusName() {
		return newStatusName;
	}

	/**
	 * Represents sub-status id of the user
	 */
	private int subStatusId;

	/**
	 * Represents sub-status name of the user
	 */
	private String subStatusName;

	/**
	 * PIP means Performance in progress
	 * 
	 */
	private boolean isPIP;
	
	
	private boolean employeeTermsAndConditionStatus;
	
	private Date accepteddate;
	
	private String tempDate;
	
	public String getTempDate() {
		return tempDate;
	}

	public void setTempDate(String tempDate) {
		this.tempDate = tempDate;
	}

	

	public boolean isEmployeeTermsAndConditionStatus() {
		return employeeTermsAndConditionStatus;
	}

	public void setEmployeeTermsAndConditionStatus(
			boolean employeeTermsAndConditionStatus) {
		this.employeeTermsAndConditionStatus = employeeTermsAndConditionStatus;
	}

	public Date getAccepteddate() {
		return accepteddate;
	}

	public void setAccepteddate(Date accepteddate) {
		this.accepteddate = accepteddate;
	}
	

	/**
	 * @param isPIP
	 *            the isPIP to set
	 */
	public void setIsPIP(boolean isPIP) {
		this.isPIP = isPIP;
	}

	/**
	 * @return the isPIP
	 */
	public boolean getIsPIP() {
		return isPIP;
	}

	/**
	 * @param subStatusName
	 *            the subStatusName to set
	 */
	public void setSubStatusName(String subStatusName) {
		this.subStatusName = subStatusName;
	}

	/**
	 * @return the subStatusName
	 */
	public String getSubStatusName() {
		return subStatusName;
	}

	/**
	 * @param subStatusId
	 *            the subStatusId to set
	 */
	public void setSubStatusId(int subStatusId) {
		this.subStatusId = subStatusId;
	}

	/**
	 * @return the subStatusId
	 */
	public int getSubStatusId() {
		return subStatusId;
	}

	

	/**
	 * Represents the employee type
	 */
	private int employeeType;

	/**
	 * Represents the employee gender
	 */
	private String gender;

	/**
	 * 
	 */
	private boolean isGraduated;

	/**
	 * 
	 */
	private int parentLocationId;

	/**
	 * String represent role description
	 */
	private String roleDescription;

	/**
	 * Represents the employee reporting manager name.
	 */
	private int domainMgrId;

	/**
	 * String represent domain manager name
	 */
	private String domainMgrName;

	
	private String functionalSkill;

	public String getFunctionalSkill() {
		return functionalSkill;
	}

	public void setFunctionalSkill(String functionalSkill) {
		this.functionalSkill = functionalSkill;
	}

	

	/**
	 * @author krishna Represents BU Hr Name
	 */
	private String businessUnitHrName;

	/**
	 * @author krishna Getter for BU Hr name
	 * @return BU Hr Name
	 */
	public String getBusinessUnitHrName() {
		return businessUnitHrName;
	}

	/**
	 * @author krishna Setter of BU Hr Name
	 * @param businessUnitHrName
	 */
	public void setBusinessUnitHrName(String businessUnitHrName) {
		this.businessUnitHrName = businessUnitHrName;
	}

	/**
	 * 
	 * @return
	 */

	public String getRoleDescription() {
		return roleDescription;
	}

	/**
	 * 
	 * @param role
	 *            Description
	 */
	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	/**
	 * @author abhay Email id of the business unit HR for the correspoding
	 *         employee
	 */
	private String businessUnitHrEmail;


	private Date spouseDOB;

	public Date getSpouseDOB() {
		return spouseDOB;
	}

	public void setSpouseDOB(Date spouseDOB) {
		this.spouseDOB = spouseDOB;
	}



	/**
	 * @author krishna Field to store date of birth of employee
	 */
	private Date dateOfBirth;

	/**
	 * @author krishna Fied to store value of last working day if the employee
	 *         has resigned
	 */
	private Date lastWorkingDay;

	public Date getLastWorkingDay() {
		return lastWorkingDay;
	}

	public void setLastWorkingDay(Date lastWorkingDay) {
		this.lastWorkingDay = lastWorkingDay;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public int getParentLocationId() {
		return parentLocationId;
	}

	public void setParentLocationId(int parentLocationId) {
		this.parentLocationId = parentLocationId;
	}

	public void setGraduated(boolean isGraduated) {
		this.isGraduated = isGraduated;
	}

	/**
	 * 
	 * @return
	 */
	public boolean getIsGraduated() {
		return isGraduated;
	}

	/**
	 * 
	 * @param isGraduated
	 */
	public void setIsGraduated(boolean isGraduated) {
		this.isGraduated = isGraduated;
	}

	/**
	 * 
	 * @return
	 */

	public String getGender() {
		return gender;
	}

	/**
	 * 
	 * @param gender
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/** Creates a new instance of User */
	public MisUser() {
	}

	/**
	 * Returns Employee Id .
	 * 
	 * @return an integer containing the Employee ID.
	 */
	public int getEmpId() {
		return empId;
	}

	/**
	 * Sets the Employee Id.
	 * 
	 * @param empId
	 *            an integer containing the employee ID.
	 */
	public void setEmpId(int empId) {
		this.empId = empId;
	}

	/**
	 * Returns Employee cost unit
	 * 
	 * @return an float containing the Employee cost unit.
	 */
	public float getCostUnit() {
		return costUnit;
	}

	/**
	 * Sets the Employee cost unit.
	 * 
	 * @param costUnit
	 *            an float containing Employee cost unit.
	 */
	public void setCostUnit(float costUnit) {
		this.costUnit = costUnit;
	}

	/**
	 * Returns Competency Manager Id of project.
	 * 
	 * @return an integer containing the Competency Manager ID.
	 */
	public int getCompetencyMgrId() {
		return competencyMgrId;
	}

	/**
	 * Sets the Competency Manager Id.
	 * 
	 * @param competencyMgrId
	 *            an integer containing the competency ManagerId.
	 */
	public void setCompetencyMgrId(int competencyMgrId) {
		this.competencyMgrId = competencyMgrId;
	}

	/**
	 * Returns the name of the competency manager
	 * 
	 * @return a <code>String</code> containing the competency manager name.
	 */
	public String getCompetencyMgrName() {
		return competencyMgrName;
	}

	/**
	 * Sets the Competency Manager name.
	 * 
	 * @param competencyMgrName
	 *            a <code>String</code> containing the competency manager name.
	 */
	public void setCompetencyMgrName(String competencyMgrName) {
		this.competencyMgrName = competencyMgrName;
	}

	/**
	 * Returns the password of the employee
	 * 
	 * @return a <code>String</code> containing the password.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 * 
	 * @param password
	 *            a <code>String</code> containing the password.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Returns the temp password of the employee
	 * 
	 * @return a <code>String</code> containing the temp password.
	 */
	public String getTempPassword() {
		return tempPassword;
	}

	/**
	 * Sets the temp password.
	 * 
	 * @param temp
	 *            password a <code>String</code> containing the password.
	 */
	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}

	/**
	 * Returns the designation of the employee.
	 * 
	 * @return a <code>String</code> containing the employee designation.
	 */
	public String getDesg() {
		return desg;
	}

	/**
	 * Sets the designation of the employee
	 * 
	 * @param desg
	 *            a <code>String</code> containing the designation.
	 */
	public void setDesg(String desg) {
		this.desg = desg;
	}

	/**
	 * Returns the employee date of joining.
	 * 
	 * @return a <code>Date</code> containing employee date of joining.
	 */
	public Date getDOJ() {
		return DOJ;
	}

	/**
	 * Sets the employee date of joining.
	 * 
	 * @param a
	 *            <code>Date</code> containing the employee date of joining.
	 */
	public void setDOJ(Date DOJ) {
		this.DOJ = DOJ;
	}

	/**
	 * Returns the email id of the employee
	 * 
	 * @return a <code>String</code> containing the employee's email.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the employee email id.
	 * 
	 * @param email
	 *            a <code>String</code> containing the email id.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Returns Employee Experience.
	 * 
	 * @return an integer containing employee experience.
	 */
	public float getExperience() {
		return experience;
	}

	/**
	 * Sets the employee experience.
	 * 
	 * @param experience
	 *            an integer containing employee experience.
	 */
	public void setExperience(float experience) {
		this.experience = experience;
	}

	/**
	 * Returns the first name of the employee
	 * 
	 * @return a <code>String</code> containing the employee's first name.
	 * 
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the employee first name.
	 * 
	 * @param firstName
	 *            a <code>String</code> containing the employee first name.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Returns the initials of the employee
	 * 
	 * @return a <code>String</code> containing the employee's initials.
	 */
	public String getInitials() {
		return initials;
	}

	/**
	 * Sets the employee initials.
	 * 
	 * @param initials
	 *            a <code>String</code> containing the employee initials.
	 */
	public void setInitials(String initials) {
		this.initials = initials;
	}

	/**
	 * Returns the last name of the employee
	 * 
	 * @return a <code>String</code> containing the employee's last name.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the employee last name.
	 * 
	 * @param lastName
	 *            a <code>String</code> containing the employee lastName.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Returns the level of the employee
	 * 
	 * @return a <code>String</code> containing the employee's level.
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * Sets the employee level
	 * 
	 * @param level
	 *            a <code>String</code> containing the employee level.
	 */
	public void setLevel(String level) {
		this.level = level;
	}

	/**
	 * Returns Location ID
	 * 
	 * @return an integer containing the Location ID.
	 */
	public int getLocationId() {
		return locationId;
	}

	/**
	 * Sets the Location Id.
	 * 
	 * @param locationId
	 *            an integer containing the locationId
	 */
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	/**
	 * Returns Organization ID
	 * 
	 * @return an integer containing the Organization ID.
	 */
	public int getOrganizationId() {
		return this.organizationId;
	}

	/**
	 * Sets the Organization Id.
	 * 
	 * @param locationId
	 *            an integer containing the organization Id.
	 */
	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}

	/**
	 * Returns Boot Camp ID
	 * 
	 * @return an integer containing the Boot Camp Id.
	 */
	public int getBootCampId() {
		return this.bootCampId;
	}

	/**
	 * Sets the Boot Camp Id.
	 * 
	 * @param locationId
	 *            an integer containing the Boot Camp Id.
	 */
	public void setBootCampId(int bootCampId) {
		this.bootCampId = bootCampId;
	}

	/**
	 * Returns the location of the employee
	 * 
	 * @return a <code>String</code> containing the employee's location.
	 */
	public String getLocationName() {
		return locationName;
	}

	/**
	 * Sets the employee location
	 * 
	 * @param level
	 *            a <code>String</code> containing the employee location.
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	/**
	 * Returns the organization name of employee
	 * 
	 * @return a <code>String</code> containing the organization name
	 */
	public String getOrganizationName() {
		return organizationName;
	}

	/**
	 * Sets the organization name of employee
	 * 
	 * @param level
	 *            a <code>String</code> containing the organization name
	 */
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	/**
	 * Returns the boot camp name of employee
	 * 
	 * @return a <code>String</code> containing the boot camp name.
	 */
	public String getBootCampName() {
		return this.bootCampName;
	}

	/**
	 * Sets the bootcamp name of employee
	 * 
	 * @param level
	 *            a <code>String</code> containing the bootcamp name
	 */
	public void setBootCampName(String bootCampName) {
		this.bootCampName = bootCampName;
	}

	/**
	 * Returns the middle name of the employee
	 * 
	 * @return a <code>String</code> containing the employee's middle name
	 */
	public String getMidName() {
		return midName;
	}

	/**
	 * Sets the employee middle name
	 * 
	 * @param midName
	 *            a <code>String</code> containing the employee middle Name.
	 */
	public void setMidName(String midName) {
		this.midName = midName;
	}

	/**
	 * Returns boolean indicating whether emp is on bench
	 * 
	 * @return a boolean containing emp on bench status.
	 */
	public boolean getOnBenchFlag() {
		return onBenchFlag;
	}

	/**
	 * Sets the onBench flag.
	 * 
	 * @param onBenchFlag
	 *            a boolean containing the on bench status of employee.
	 */
	public void setOnBenchFlag(boolean onBenchFlag) {
		this.onBenchFlag = onBenchFlag;
	}

	/**
	 * Returns boolean indicating whether emp is allowed to login or not.
	 * 
	 * @return a boolean containing emp login status.
	 */
	public boolean getLoginFlag() {
		return loginFlag;
	}

	/**
	 * Sets the login flag.
	 * 
	 * @param loginFlag
	 *            a boolean containing the on login status of employee.
	 */
	public void setLoginFlag(boolean loginFlag) {
		this.loginFlag = loginFlag;
	}

	/**
	 * Returns the phone of the employee
	 * 
	 * @return a <code>String</code> containing the employee's phone number.
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Sets the employee phone number
	 * 
	 * @param phone
	 *            a <code>String</code> containing the employee phone number.
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Returns a vector containing employee's projects.
	 * 
	 * @return a vector containing employee's projects.
	 */
	public Vector getProjectsVector() {
		return projectsVector;
	}

	/**
	 * Sets the vector of employee's projects.
	 * 
	 * @param projectsVector
	 *            containing vector of employee's projects.
	 */
	public void setProjectsVector(Vector projectsVector) {
		this.projectsVector = projectsVector;
	}

	/**
	 * Returns the qualification of the employee
	 * 
	 * @return a <code>String</code> containing the employee's qualification.
	 */
	public String getQual() {
		return qual;
	}

	/**
	 * Sets the employee qualification.
	 * 
	 * @param qual
	 *            a <code>String</code> containing the employee qualification.
	 */
	public void setQual(String qual) {
		this.qual = qual;
	}

	/**
	 * Returns Reporting Manager Id
	 * 
	 * @return an integer containing the Reporting Manager ID.
	 */
	public int getReportingMgrId() {
		return reportingMgrId;
	}

	/**
	 * Sets the Reporting Manager Id.
	 * 
	 * @param reportingMgrId
	 *            an integer containing the reportingMgrId.
	 */
	public void setReportingMgrId(int reportingMgrId) {
		this.reportingMgrId = reportingMgrId;
	}

	/**
	 * Returns the reporting manager name
	 * 
	 * @return a <code>String</code> containing the reporting manager name
	 */
	public String getReportingMgrName() {
		return reportingMgrName;
	}

	/**
	 * Sets the reporting manager name
	 * 
	 * @param reporting
	 *            manager name a <code>String</code> containing the reporting
	 *            manger name.
	 */
	public void setReportingMgrName(String reportingMgrName) {
		this.reportingMgrName = reportingMgrName;
	}

	/**
	 * Returns Role ID
	 * 
	 * @return an integer containing the role ID of employee.
	 */
	public int getRoleId() {
		return roleId;
	}

	/**
	 * Sets the Role Id.
	 * 
	 * @param role
	 *            an integer containing the roleId.
	 */
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	/**
	 * @return the businessUnitHeadEmail
	 */
	public String getBusinessUnitHeadEmail() {
		return businessUnitHeadEmail;
	}

	/**
	 * @param businessUnitHeadEmail
	 *            the businessUnitHeadEmail to set
	 */
	public void setBusinessUnitHeadEmail(String businessUnitHeadEmail) {
		this.businessUnitHeadEmail = businessUnitHeadEmail;
	}

	/**
	 * @return the businessUnitHeadName
	 */
	public String getBusinessUnitHeadName() {
		return businessUnitHeadName;
	}

	/**
	 * @param businessUnitHeadName
	 *            the businessUnitHeadName to set
	 */
	public void setBusinessUnitHeadName(String businessUnitHeadName) {
		this.businessUnitHeadName = businessUnitHeadName;
	}

	/**
	 * @return the businessUnitId
	 */
	public int getBusinessUnitId() {
		return businessUnitId;
	}

	/**
	 * @param businessUnitId
	 *            the businessUnitId to set
	 */
	public void setBusinessUnitId(int businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

	/**
	 * @return the businessUnitHeadEmpId
	 */
	public int getBusinessUnitHeadEmpId() {
		return businessUnitHeadEmpId;
	}

	/**
	 * @param businessUnitHeadEmpId
	 *            the businessUnitHeadEmpId to set
	 */
	public void setBusinessUnitHeadEmpId(int businessUnitHeadEmpId) {
		this.businessUnitHeadEmpId = businessUnitHeadEmpId;
	}

	/**
	 * @return the businessUnitName
	 */
	public String getBusinessUnitName() {
		return businessUnitName;
	}

	/**
	 * @param businessUnitName
	 *            the businessUnitName to set
	 */
	public void setBusinessUnitName(String businessUnitName) {
		this.businessUnitName = businessUnitName;
	}

	/**
	 * @return Returns the statusId.
	 */
	public int getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId
	 *            The statusId to set.
	 */
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return Returns the statusName.
	 */
	public String getStatusName() {
		return statusName;
	}

	/**
	 * @param statusName
	 *            The statusName to set.
	 */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	/**
	 * @return Returns the employeeType.
	 */
	public int getEmployeeType() {
		return employeeType;
	}

	/**
	 * @param employeeType
	 *            The employeeType to set.
	 */
	public void setEmployeeType(int employeeType) {
		this.employeeType = employeeType;
	}

	public String getBusinessUnitHrEmail() {
		return businessUnitHrEmail;
	}

	public void setBusinessUnitHrEmail(String businessUnitHrEmail) {
		this.businessUnitHrEmail = businessUnitHrEmail;
	}

	public Date getPasswordUpdatedOn() {
		return passwordUpdatedOn;
	}

	public void setPasswordUpdatedOn(Date passwordUpdatedOn) {
		this.passwordUpdatedOn = passwordUpdatedOn;
	}

	// Added by rahul & nimesh
	public String getExtn() {
		return extn;
	}

	public void setExtn(String extn) {
		this.extn = extn;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	// Nimesh
	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getPermanentAddr() {
		return permanentAddr;
	}

	public void setPermanentAddr(String permanentAddr) {
		this.permanentAddr = permanentAddr;
	}

	public String getPresentAddr() {
		return presentAddr;
	}

	public void setPresentAddr(String presentAddr) {
		this.presentAddr = presentAddr;
	}

	public int getDomainMgrId() {
		return domainMgrId;
	}

	public void setDomainMgrId(int domainMgrId) {
		this.domainMgrId = domainMgrId;
	}

	public String getDomainMgrName() {
		return domainMgrName;
	}

	public void setDomainMgrName(String domainMgrName) {
		this.domainMgrName = domainMgrName;
	}

	public Date getEffectiveDateOfChangeInRM() {
		return effectiveDateOfChangeInRM;
	}

	public void setEffectiveDateOfChangeInRM(Date effectiveDateOfChangeInRM) {
		this.effectiveDateOfChangeInRM = effectiveDateOfChangeInRM;
	}

	
	/**
	 * @return the personalEmail
	 */
	public String getPersonalEmail() {
		return personalEmail;
	}

	/**
	 * @param personalEmail
	 *            the personalEmail to set
	 */
	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}


	private Integer updatedBy;

	

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	
	
	
	public boolean contains(String criteria) {
		if ((getFirstName() + " " + getLastName()).toUpperCase().contains(
				criteria.toUpperCase())) {
			return true;
		} else if (String.valueOf(getEmpId()).contains(criteria.toUpperCase())) {
			return true;
		} else if (getDesg().toUpperCase().contains(criteria.toUpperCase())) {
			return true;
		} else if (getBusinessUnitName().toUpperCase().contains(
				criteria.toUpperCase())) {
			return true;
		} else if (getPhone().contains(criteria)) {
			return true;
		} else if (getExtn().contains(criteria)) {
			return true;
		} else if (getMobileNo().contains(criteria)) {
			return true;
		} else if (getLocationName().toUpperCase().contains(
				criteria.toUpperCase())) {
			return true;
		} else if (getEmail().toUpperCase().contains(criteria.toUpperCase())) {
			return true;
		}
		return false;
	}
	
		private String includeProjects;
		private String excludeProjects;
		private String includeResources;

		public String getIncludeProjects() {
			return includeProjects;
		}

		public void setIncludeProjects(String includeProjects) {
			if (null == includeProjects) {
				includeProjects = "0";
			}
			this.includeProjects = includeProjects;
		}

		public String getExcludeProjects() {
			return excludeProjects;
		}

		public void setExcludeProjects(String excludeProjects) {
			if (null == excludeProjects) {
				excludeProjects = "0";
			}
			this.excludeProjects = excludeProjects;
		}

		public String getIncludeResources() {
			return includeResources;
		}

		public void setIncludeResources(String includeResources) {
			this.includeResources = includeResources;
		}
	
	
	
	
	
	
	
	
	/**
	 * Constructs a <code>String</code> with all attributes in name = value
	 * format.
	 * 
	 * @return a <code>String</code> representation of this object.
	 */
	public String toString() {
		final String TAB = "    ";

		String retValue = "";

		retValue = "User ( " + super.toString() + TAB + "empId = " + this.empId
				+ TAB + "firstName = " + this.firstName + TAB + "midName = "
				+ this.midName + TAB + "lastName = " + this.lastName + TAB
				+ "initials = " + this.initials + TAB + "DOJ = " + this.DOJ
				+ TAB + "passwordUpdatedOn = " + this.passwordUpdatedOn + TAB
				+ "qual = " + this.qual + TAB + "phone = " + this.phone + TAB
				+ "bloodGroup = " + this.bloodGroup + TAB
				+ "permanentAddress = " + this.permanentAddr + TAB
				+ "presentAddress = " + this.presentAddr + TAB + "mobileNo = "
				+ this.mobileNo + TAB + "extn = " + this.extn + TAB + "desg = "
				+ this.desg + TAB + "level = " + this.level + TAB
				+ "locationId = " + this.locationId + TAB + "organizationId = "
				+ this.organizationId + TAB + "bootCampId = " + this.bootCampId
				+ TAB + "experience = " + this.experience + TAB
				+ "reportingMgrId = " + this.reportingMgrId + TAB
				+ "effectiveDateOfChangeInRM = "
				+ this.effectiveDateOfChangeInRM + TAB
				+ "competencyMgrId = "
				+ this.competencyMgrId
				+ TAB
				+ "onBenchFlag = "
				+ this.onBenchFlag
				+ TAB
				+ "loginFlag = "
				+ this.loginFlag
				+ TAB
				+ "projectsVector = "
				+ this.projectsVector
				+ TAB
				+ "roleId = "
				+ this.roleId
				+ TAB
				+ "locationName = "
				+ this.locationName
				+ TAB
				+ "organizationName = "
				+ this.organizationName
				+ TAB
				+ "bootCampName = "
				+ this.bootCampName
				+ TAB
				+ "email = "
				+ this.email
				+ TAB
				+ "competencyMgrName = "
				+ this.competencyMgrName
				+ TAB
				+ "reportingMgrName = "
				+ this.reportingMgrName
				+ TAB
				+ "costUnit = "
				+ this.costUnit
				+ TAB
				+ "businessUnitId = "
				+ this.businessUnitId
				+ TAB
				+ "businessUnitName = "
				+ this.businessUnitName
				+ TAB
				+ "businessUnitHeadEmpId = "
				+ this.businessUnitHeadEmpId
				+ TAB
				+ "businessUnitHeadName = "
				+ this.businessUnitHeadName
				+ TAB
				+ "businessUnitHeadEmail = "
				+ this.businessUnitHeadEmail
				+ TAB
				+ "statusId = "
				+ this.statusId
				+ TAB
				+ "statusName = "
				+ this.statusName
				+ TAB
				+ "newStatusId = "
				+ this.newStatusId
				+ TAB // added by shyam
				+ "newStatusName = "
				+ this.newStatusName
				+ TAB // added by shyam
				+ "subStatusId = "
				+ this.subStatusId
				+ TAB // added by shyam
				+ "subStatusName = "
				+ this.subStatusName
				+ TAB // added by shyam
				+ "isPIP = "
				+ this.isPIP
				+ TAB // added by shyam
				+ "employeeType = " + this.employeeType + TAB + "gender = "
				+ this.gender + TAB + "isGraduated = " + this.isGraduated + TAB
				+ "parentLocationId = " + this.parentLocationId + TAB
				+ "roleDescription = " + this.roleDescription + TAB
				+ "businessUnitHrEmail = " + this.businessUnitHrEmail + TAB
				+ "dateOfBirth = " + this.dateOfBirth + TAB
				+ "lastWorkingDay = " + this.lastWorkingDay + TAB
				+ "domainManagerId = " + this.domainMgrId + TAB
				+ "domainManagerName = " + this.domainMgrName + TAB
				+ "noOfReportees = " + this.noOfReportees + TAB
				+ "updatedBy = " + this.updatedBy + TAB + "personalEmail = "
				+ this.personalEmail + TAB 
				
				+ "includeProjects = " + this.includeProjects + TAB
				+ "excludeProjects = " + this.excludeProjects + TAB
				+ "contractorEndDate = " + this.contractorEndDate + TAB
				
				+ "currentRating = " + this.currentRating + TAB
				+ "previousRating = " + this.previousRating + TAB
				
				+ "aadharNumber = " + this.aadharNumber + TAB 
				+ "aadharNumberValue = " + this.aadharNumberValue + TAB
				
				+ "employeeTermsAndConditionStatus = " + this.employeeTermsAndConditionStatus + TAB
				+ "accepteddate = " + this.accepteddate + TAB
				+ "tempDate = " + this.tempDate + TAB
				+ "profileCreatedBy = " + this.profileCreatedBy + TAB
				+ "requestType = " + this.requestType + TAB
				+ " )";

		return retValue;
	}

	/**
	 * @return the contractorEndDate
	 */
	public Date getContractorEndDate() {
		return contractorEndDate;
	}

	/**
	 * @param contractorEndDate the contractorEndDate to set
	 */
	public void setContractorEndDate(Date contractorEndDate) {
		this.contractorEndDate = contractorEndDate;
	}
	
	private String currentRatingAsString;
	
	public String getCurrentRatingAsString() {
		return currentRatingAsString;
	}
	public String getPreviousRatingAsString() {
		return previousRatingAsString;
	}
	public void setCurrentRatingAsString(String currentRatingAsString) {
		this.currentRatingAsString = currentRatingAsString;
	}
 
	public void setPreviousRatingAsString(String previousRatingAsString) {
		this.previousRatingAsString = previousRatingAsString;
	}
	private String previousRatingAsString;
	
	private String employeeTypeDesc;
	
	
	public String getemployeeTypeDesc(){
		return employeeTypeDesc;
	}
	
	public void setemployeeTypeDesc(String employeeTypeDesc ){
		this.employeeTypeDesc=employeeTypeDesc;
	}
	
		@Override
		public int compareTo(MisUser o) 
		{
			
			return this.firstName.compareTo(o.getFirstName());
		}
		

		
	 
}
