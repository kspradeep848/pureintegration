/********************************************************************/
/*                  FILE HEADER                                     */
/********************************************************************/
/*                                                                  */
/*                                                                  */
/*  FileName    :  BaseBO.java                                      */
/*                                                                  */
/*  Author      :  Santosh B.M (THBS)                               */
/*                                                                  */
/*  Date        :  Oct 11,2005                                      */
/*                                                                  */
/*  Description :  Abstract class that needs to be                  */
/*                 implemented by all BOs.                          */
/*                                                                  */
/*                                                                  */
/********************************************************************/
/* Date        Who     Version      PRF         Comments            */
/*------------------------------------------------------------------*/
/* Oct 11,2005 THBS      1.0               Initial version created  */
/********************************************************************/

package com.thbs.mis.legacymisdbSync.bean;

import java.io.Serializable;

/**
* Abstract class that needs to be implemented by all BOs.
*
* @author Santosh B.M
*
* @version 1.0 Oct 11, 2005
*/
public abstract class BaseBO implements Serializable{
    /**
     * Constructs a <code>String</code> with all attributes
     * in name = value format.
     *
     * @return a <code>String</code> representation 
     *         of this BO
     */
    public String toString() {
        String retValue = " ";
        return retValue;
    }
}
