/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  LegacyUserService.java                            */
/*                                                                   */
/*  Author      :  Anil kumar B K                                    */
/*                                                                   */
/*  Date        : January 09, 2017                                   */
/*                                                                   */
/*  Description : Service class for Legacy user                      */
/*		 									                         */
/*                                                                   */
/*********************************************************************/
/* Date                 Who     Version      Comments                */
/*-------------------------------------------------------------------*/
/* January 09, 2017     THBS     1.0        Initial version created  */
/*********************************************************************/
package com.thbs.mis.legacymisdbSync.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.thbs.mis.common.bean.EmpDesignationBean;
import com.thbs.mis.common.bean.EmployeeLevelBean;
import com.thbs.mis.common.bean.UserBean;
import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.common.service.UserService;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.legacymisdbSync.bean.MisUser;

@Service
public class LegacyUserService {

	
	/**
	 * Instance of <code>LOG</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(LegacyUserService.class);
	/**
	 * Instance of <code>UserService</code>
	 */
	@Autowired
	private UserService userService;

	/**
	 * Instance of <code>DatEmpDetailsRepository</code>
	 */
	@Autowired
	private EmpDetailRepository legacyUserRepository;

	/**
	 * Instance of <code>DatEmpPersonalDetailsRepository</code>
	 */
	@Autowired
	private EmployeePersonalDetailsRepository legacyUserPersonalRepository;

	/**
	 * Instance of <code>DatEmpProfessionalDetailsRepository</code>
	 */
	@Autowired
	private EmployeeProfessionalRepository legacyUserProfessionalRepository;

	@Autowired
	private CommonService commonService;
	
	@Value("${employee.separation.status}")
	private String legacyPath;
	

	/**
	 * 
	 * <Description addUser:> This service is used to create user profile from
	 * legacy MIS to New MIS
	 * 
	 * @param user
	 * @return Boolean
	 * @throws DataAccessException
	 */
	public Boolean saveUserFromOldMis(MisUser user) throws DataAccessException {

		List<EmpDesignationBean> designationsList = commonService
				.getAllDesignation();

		List<EmployeeLevelBean> empLevelBeanList = commonService.getAllLevels();
		

		UserBean newUser = new UserBean();
		Boolean success = false;
		try {
			if (user.isEmployeeTermsAndConditionStatus() == true) {
				newUser.setAcceptedThbsPolicyFlag("ACCEPTED");
			} else {
				newUser.setAcceptedThbsPolicyFlag("NOT_ACCEPTED");
			}
			newUser.setEffectiveDateOfReportingMgrChange(user
					.getEffectiveDateOfChangeInRM());
			newUser.setEmpAadharNumber(user.getAadharNumber());
			
			newUser.setEmpBloodGroup(user.getBloodGroup());

			newUser.setEmpCitizenshipStatus(String.valueOf(user
					.getCitizenship()));

			if (user.getCitizenship() == 1) {
				newUser.setEmpCitizenshipOne((short) user.getFirstCitizenship());
				newUser.setEmpCitizenshipTwo((short)1);
			}else
			if (user.getCitizenship() == 2) {
				newUser.setEmpCitizenshipOne((short) user.getFirstCitizenship());
				newUser.setEmpCitizenshipTwo((short) user
						.getSecondCitizenship());
			}else{
				newUser.setEmpCitizenshipStatus("NOT_KNOWN");
				newUser.setEmpCitizenshipOne((short)1);
				newUser.setEmpCitizenshipTwo((short)1);
			}
			newUser.setEmpConfirmationGraduateFlag((byte) (user
					.getIsGraduated() ? 1 : 0));
			newUser.setEmpContractEndDate(user.getContractorEndDate());
			newUser.setEmpCostUnit(user.getCostUnit());
			newUser.setEmpDateOfBirth(user.getDateOfBirth());
			newUser.setEmpDateOfJoining(user.getDOJ());
			newUser.setEmpExtensionNumber(user.getExtn());
			newUser.setEmpFirstName(user.getFirstName());
			newUser.setEmpGender((byte) ((user.getGender()
					.equalsIgnoreCase("Male")) ? 1 : 2));
			newUser.setEmpLastName(user.getLastName());
			newUser.setEmpMiddleName(user.getMidName());
			newUser.setEmpMobileNo(user.getMobileNo());
			newUser.setEmpOnBenchFlag((byte) (user.getOnBenchFlag() ? 1 : 0));
			newUser.setEmpPermanentAddress(user.getPermanentAddr());
			newUser.setEmpPersonalEmailId(user.getPersonalEmail());
			newUser.setEmpPhoneNumber(user.getPhone());
			newUser.setEmpPresentAddress(user.getPresentAddr());
			newUser.setEmpPreviousExperience(user.getExperience());
			newUser.setEmpProfessionalEmailId(user.getEmail());
			newUser.setEmpPwdUpdatedOn(user.getPasswordUpdatedOn());
			newUser.setEmpQualification(user.getQual());
			newUser.setEmpSpouseDateOfBirth(user.getSpouseDOB());
			newUser.setEmpType((byte) (user.getEmployeeType()));
			newUser.setFkEmpBootCampId((short) (user.getBootCampId()));
			newUser.setFkEmpBuUnit((short) (user.getBusinessUnitId()));
			newUser.setFkEmpCompetenceMgrId(user.getCompetencyMgrId());
			if(user.getConsultant()!=null){
			newUser.setConsultantReferenceEmpId(Integer.parseInt(user.getConsultant()));
			}
			for (EmpDesignationBean desig : designationsList) {
				if (user.getDesg().equalsIgnoreCase(desig.getDesignationName())) {
					newUser.setFkEmpDesignation((short) (desig
							.getDesignationId()));
				}
			}

			newUser.setFkEmpDetailId(user.getEmpId());
			newUser.setFkEmpDomainMgrId(user.getDomainMgrId());

			for (EmployeeLevelBean level : empLevelBeanList) {
				if (user.getLevel().equalsIgnoreCase(level.getLevelName())) {
					newUser.setFkEmpLevel((short) level.getLevelId());
				}
			}

			newUser.setFkEmpLocationParentId((short) (user
					.getParentLocationId()));
			newUser.setFkEmpLocationPhysicalId((short) user.getLocationId());
			newUser.setFkEmpMainStatus((byte) user.getStatusId());
			newUser.setFkEmpOrganizationId((short) user.getOrganizationId());
			newUser.setFkEmpReportingMgrId(user.getReportingMgrId());

			newUser.setFkEmpRoleId((short) user.getRoleId());
			newUser.setFkEmpSubStatus((byte) (user.getSubStatusId()));
			newUser.setFkMainEmpDetailId(user.getEmpId());
			newUser.setRole("ROLE_ALLUSERS");
			newUser.setPkEmpId(user.getEmpId());
			newUser.setProfileCreatedDate(new Date());
			newUser.setProfileCreatedBy(user.getProfileCreatedBy());
			userService.saveUser(newUser);
			success = true;
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception occured while upading user profile : ", e);
		}
		return success;

	}

	/**
	 * 
	 * <Description addUser:> This service is used to update user profile from
	 * legacy MIS to New MIS
	 * 
	 * @param user
	 * @return Boolean
	 * @throws DataAccessException
	 */
	public Boolean updateUserFromOldMis(MisUser user)
			throws DataAccessException {

		List<EmpDesignationBean> designationsList = commonService
				.getAllDesignation();

		List<EmployeeLevelBean> empLevelBeanList = commonService.getAllLevels();
		UserBean newUser = new UserBean();
		Boolean success = false;
		try {
			if (user.isEmployeeTermsAndConditionStatus() == true) {
				newUser.setAcceptedThbsPolicyFlag("ACCEPTED");
				newUser.setPolicyAcceptedDate(user.getAccepteddate());
			} else {
				newUser.setAcceptedThbsPolicyFlag("NOT_ACCEPTED");
			}
			newUser.setEffectiveDateOfReportingMgrChange(user
					.getEffectiveDateOfChangeInRM());
			
			if(user.getAadharNumber()!=null){
				newUser.setEmpAadharNumber(user.getAadharNumber());
			}
		
			
			newUser.setEmpBloodGroup(user.getBloodGroup());
			newUser.setEmpCitizenshipStatus(String.valueOf(user
					.getCitizenship()));

			if (user.getCitizenship() == 1) {
				newUser.setEmpCitizenshipOne((short) user.getFirstCitizenship());
				newUser.setEmpCitizenshipTwo((short)1);
			}else if (user.getCitizenship() == 2) {
				newUser.setEmpCitizenshipOne((short) user.getFirstCitizenship());
				newUser.setEmpCitizenshipTwo((short) user
						.getSecondCitizenship());
			}else{
				newUser.setEmpCitizenshipStatus("NOT_KNOWN");
				newUser.setEmpCitizenshipOne((short)1);
				newUser.setEmpCitizenshipTwo((short)1);
			}
			
			if(user.getRequestType()!=null){
				newUser.setRequestType(user.getRequestType());
			}
			newUser.setEmpConfirmationGraduateFlag((byte) (user
					.getIsGraduated() ? 1 : 0));
			newUser.setEmpContractEndDate(user.getContractorEndDate());
			newUser.setEmpCostUnit(user.getCostUnit());
			newUser.setEmpDateOfBirth(user.getDateOfBirth());
			newUser.setEmpDateOfJoining(user.getDOJ());
			newUser.setEmpExtensionNumber(user.getExtn());
			newUser.setEmpFirstName(user.getFirstName());
			newUser.setEmpGender((byte) ((user.getGender()
					.equalsIgnoreCase("Male")) ? 1 : 2));
			newUser.setEmpLastName(user.getLastName());
			newUser.setEmpMiddleName(user.getMidName());
			newUser.setEmpMobileNo(user.getMobileNo());
			newUser.setEmpOnBenchFlag((byte) (user.getOnBenchFlag() ? 1 : 0));
			newUser.setEmpPermanentAddress(user.getPermanentAddr());
			newUser.setEmpPersonalEmailId(user.getPersonalEmail());
			newUser.setEmpPhoneNumber(user.getPhone());
			newUser.setEmpPresentAddress(user.getPresentAddr());
			newUser.setEmpPreviousExperience(user.getExperience());
			newUser.setEmpProfessionalEmailId(user.getEmail());
			newUser.setEmpPwdUpdatedOn(user.getPasswordUpdatedOn());
			newUser.setEmpQualification(user.getQual());
			newUser.setEmpSpouseDateOfBirth(user.getSpouseDOB());
			newUser.setEmpType((byte) (user.getEmployeeType()));
			newUser.setFkEmpBootCampId((short) (user.getBootCampId()));
			newUser.setFkEmpBuUnit((short) (user.getBusinessUnitId()));
			newUser.setFkEmpCompetenceMgrId(user.getCompetencyMgrId());
			newUser.setModifiedBy(user.getUpdatedBy());
			newUser.setModifiedDate(new Date());
			newUser.setEmpLastWorkingDate(user.getLastWorkingDay());
			
			for (EmpDesignationBean desig : designationsList) {
				if (user.getDesg().equalsIgnoreCase(desig.getDesignationName())) {
					newUser.setFkEmpDesignation((short) (desig
							.getDesignationId()));
				}
			}
			
			newUser.setFkEmpDetailId(user.getEmpId());
			newUser.setFkEmpDomainMgrId(user.getDomainMgrId());
			for (EmployeeLevelBean level : empLevelBeanList) {
				if (user.getLevel().equalsIgnoreCase(level.getLevelName())) {
					newUser.setFkEmpLevel((short) level.getLevelId());
				}
			}
			newUser.setFkEmpLocationParentId((short) (user
					.getParentLocationId()));
			newUser.setFkEmpLocationPhysicalId((short) user.getLocationId());
			
			if(user.getLoginFlag() == false){
				newUser.setFkEmpMainStatus((byte)2);
			}else{
				newUser.setFkEmpMainStatus((byte)1);
			}
			
			newUser.setFkEmpOrganizationId((short) user.getOrganizationId());
			newUser.setFkEmpReportingMgrId(user.getReportingMgrId());
			newUser.setFkEmpRoleId((short) user.getRoleId());
			newUser.setFkEmpSubStatus((byte) (user.getSubStatusId()));
			newUser.setFkMainEmpDetailId(user.getEmpId());
			
			if(user.getConsultant()!=null){
				newUser.setConsultantReferenceEmpId(Integer.parseInt(user.getConsultant()));
			}
		
			newUser.setPkEmpId(user.getEmpId());
		
			newUser.setRole("ROLE_ALLUSERS");
			
			userService.updateUser(newUser);
			success = true;
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception occured while upading user profile : ", e);
		}
		return success;

	}

	/**
	 * 
	 * <Description addUser:> This service is used to update confirmation status
	 * of user profile from legacy MIS to New MIS
	 * 
	 * @param user
	 * @return Boolean
	 * @throws DataAccessException
	 */
	public Boolean updateUserConfirmationFromOldMis(MisUser user)
			throws DataAccessException {

		List<EmpDesignationBean> designationsList = commonService
				.getAllDesignation();

		List<EmployeeLevelBean> empLevelBeanList = commonService.getAllLevels();
		
		Boolean success = false;
		try {

			DatEmpProfessionalDetailBO professional = legacyUserProfessionalRepository
					.findByFkMainEmpDetailId(user.getEmpId());

			for (EmpDesignationBean desig : designationsList) {
				if (user.getDesg().equalsIgnoreCase(desig.getDesignationName())) {
					
					professional.setFkEmpDesignation(desig
							.getDesignationId());
				}
			}
			
			for (EmployeeLevelBean level : empLevelBeanList) {
				if (user.getLevel().equalsIgnoreCase(level.getLevelName())) {
					
					professional.setFkEmpLevel(level.getLevelId());
				}
			}

			professional.setProfessionalDetailModifiedBy(user.getUpdatedBy());
			professional.setProfessionalDetailModifiedDate(new Date());
			professional.setEmpConfirmationGraduateFlag((byte) 1);
			professional.setProfessionalDetailModifiedBy(user.getUpdatedBy());

			legacyUserProfessionalRepository.save(professional);
			success = true;
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception occured while upading user profile for confirmation : ",
					e);
		}
		return success;

	}

	/**
	 * 
	 * <Description addUser:> This service is used to update acceptance status
	 * of terms and condition of company into user profile from legacy MIS to
	 * New MIS
	 * 
	 * @param empId
	 * @return Boolean
	 * @throws DataAccessException
	 */
	public Boolean oldMisUpdateUserTermsAndCondition(Integer empId)
			throws DataAccessException {

		Boolean success = false;
		try {

			DatEmpDetailBO personal = legacyUserRepository.findByPkEmpId(empId);

			personal.setAcceptedThbsPolicyFlag("ACCEPTED");
			personal.setPolicyAcceptedDate(new Date());
			legacyUserRepository.save(personal);
			success = true;
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception occured while upading Terms & condition status of user profile : ",
					e);
		}
		return success;

	}

	/**
	 * <Description addUser:> This service is used to update Aadhar number to
	 * user profile from legacy MIS to New MIS
	 * 
	 * @param aadharNumber
	 * @param empId
	 * @return Boolean
	 * @throws DataAccessException
	 */
	public Boolean oldMisUpdateUserAddAadhar(String aadharNumber, Integer empId)
			throws DataAccessException {

		Boolean success = false;
		try {

			DatEmpPersonalDetailBO personal = legacyUserPersonalRepository
					.findByFkEmpDetailId(empId);

			personal.setEmpAadharNumber(aadharNumber);

			legacyUserPersonalRepository.save(personal);
			success = true;
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception occured while upading aadhar number of user profile : ",
					e);
		}
		return success;

	}
	
	
	
	/**
	 * <Description addUser:> This service is used to update passwordUpdateDate to
	 * user profile from legacy MIS to New MIS
	 * 
	 * @param passwordUpdateDate
	 * @param empId
	 * @return Boolean
	 * @throws DataAccessException
	 */
	public Boolean oldMisUpdateUserChangePassword(Date passwordUpdateDate, Integer empId)
			throws DataAccessException {

		Boolean success = false;
		try {

			DatEmpDetailBO empDetails = legacyUserRepository
					.findByPkEmpId(empId);

			empDetails.setEmpPwdUpdatedOn(passwordUpdateDate);

			legacyUserRepository.save(empDetails);
			success = true;
		} catch (Exception e) {
			throw new DataAccessException(
					"Exception occured while upading aadhar number of user profile : ",
					e);
		}
		return success;

	}
	
	/**
	 * <Description addUser:> This service is used to check for employee separation is RM approval status 
	 * 
	 * @param empId
	 * @return ResponseEntity
	 * @throws DataAccessException
	 */
	public ResponseEntity<MISResponse> employeeSeparationStatus(Integer empId)
			throws DataAccessException {

		
		ResponseEntity<MISResponse> out = null;
		try {
			LOG.info("employeeSeparationStatus : ");
			String rmReporteesGoalCloseUri = legacyPath+"legacy/separation/employee/status";
		
			  MisUser userBean = new MisUser();
			RestTemplate restTemplate = new RestTemplate();
			 userBean.setEmpId(empId);
			HttpHeaders headers = new HttpHeaders();
        	headers.setContentType(MediaType.APPLICATION_JSON);

        	//set your entity to send
        	HttpEntity entity = new HttpEntity(userBean,headers);

        	// send it
        	out = restTemplate.exchange(rmReporteesGoalCloseUri, HttpMethod.POST, entity
            	    , MISResponse.class);
		
        LOG.info("result empl clearence for gsr : "+out.getBody().toString());

      
		        }
		
		
        catch(Exception e){
        	LOG.info("Exception employeeSeparationStatus : "+e.getMessage());
        	throw new DataAccessException("Exception");
        }
		return out;

	}
}
