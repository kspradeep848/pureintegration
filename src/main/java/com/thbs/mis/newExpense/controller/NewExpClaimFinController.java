package com.thbs.mis.newExpense.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.newExpense.bean.NewExpClaimFinBean;
import com.thbs.mis.newExpense.bo.NewExpClaimFinBO;
import com.thbs.mis.newExpense.constants.NewExpURIConstants;
import com.thbs.mis.newExpense.service.NewExpClaimFinService;

@Controller
public class NewExpClaimFinController {

	private static final AppLog LOG = LogFactory.getLog(NewExpClaimFinController.class);

	List<NewExpClaimFinBean> newExpClaimFinBeanList;

	List<NewExpClaimFinBO> newExpClaimFinBOList;

	NewExpClaimFinBO newExpClaimFinBO;

	NewExpClaimFinBean newExpClaimFinBean;

	@Autowired
	NewExpClaimFinService newExpClaimFinService;

	@RequestMapping(value = NewExpURIConstants.EXP_VIEW_FINANCE_CLAIM, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> viewAllExpenseByFinance(
			@RequestBody NewExpClaimFinBean newExpClaimFinBean) throws CommonCustomException, IOException {
		this.newExpClaimFinBeanList = new ArrayList<NewExpClaimFinBean>();
		this.newExpClaimFinBeanList = newExpClaimFinService.viewAllExpenseByFinance(newExpClaimFinBean);
		if (this.newExpClaimFinBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Claim details retrieved Successfully", this.newExpClaimFinBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No records found"));
		}
	}

	@RequestMapping(value = NewExpURIConstants.EXP_VIEW_FINANCE_CLAIM_BY_EXP_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> viewExpenseByFinanceId(@PathVariable Integer expId)
			throws CommonCustomException, IOException {
		this.newExpClaimFinBeanList = new ArrayList<NewExpClaimFinBean>();
		this.newExpClaimFinBeanList = newExpClaimFinService.viewExpenseByFinanceId(expId);
		if (this.newExpClaimFinBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Claim details retrieved Successfully", this.newExpClaimFinBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "No records found"));
		}
	}

	@RequestMapping(value = NewExpURIConstants.EXP_CLOSE_REIMBURSEMENT, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> closeReimbursementByFinance(
			@RequestBody NewExpClaimFinBean newExpClaimFinBean) throws CommonCustomException, IOException {
		this.newExpClaimFinBeanList = new ArrayList<NewExpClaimFinBean>();
		this.newExpClaimFinBeanList = newExpClaimFinService.closeReimbursementByFinance(newExpClaimFinBean);
		if (this.newExpClaimFinBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Reimbursement Closed Successfully"));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured, please contact nucleus team"));
		}
	}

}
