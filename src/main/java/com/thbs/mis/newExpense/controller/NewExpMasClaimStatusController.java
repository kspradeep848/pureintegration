package com.thbs.mis.newExpense.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.newExpense.bean.NewExpMasClaimStatusBean;
import com.thbs.mis.newExpense.bo.NewExpMasClaimStatusBO;
import com.thbs.mis.newExpense.constants.NewExpURIConstants;
import com.thbs.mis.newExpense.service.NewExpMasClaimStatusService;

@Controller
public class NewExpMasClaimStatusController {

	private static final AppLog LOG = LogFactory.getLog(NewExpMasClaimStatusController.class);

	List<NewExpMasClaimStatusBean> newExpMasClaimStatusBeanList;

	List<NewExpMasClaimStatusBO> newExpMasClaimStatusBOList;

	NewExpMasClaimStatusBO newExpMasClaimStatusBO;

	NewExpMasClaimStatusBean newExpMasClaimStatusBean;
	
	@Autowired
	NewExpMasClaimStatusService newExpMasClaimStatusService;
	
	@RequestMapping(value = NewExpURIConstants.EXP_GET_CLAIM_STATUS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getExpClaimStaus() throws CommonCustomException {
		this.newExpMasClaimStatusBOList = new ArrayList<NewExpMasClaimStatusBO>();
		this.newExpMasClaimStatusBOList = newExpMasClaimStatusService.getExpClaimStaus();
		if (this.newExpMasClaimStatusBOList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "claim status listed Successfully", this.newExpMasClaimStatusBOList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured, no data found", new ArrayList()));
		}

	}

}
