package com.thbs.mis.newExpense.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.newExpense.bean.NewExpDatRecordHistoryBean;
import com.thbs.mis.newExpense.bo.NewExpDatRecordHistoryBO;
import com.thbs.mis.newExpense.constants.NewExpURIConstants;
import com.thbs.mis.newExpense.service.NewExpDatRecordHistoryService;

@Controller
public class NewExpDatRecordHistoryController {

	private static final AppLog LOG = LogFactory.getLog(NewExpDatRecordHistoryController.class);

	List<NewExpDatRecordHistoryBean> newExpDatRecordHistoryBeanList;

	List<NewExpDatRecordHistoryBO> newExpDatRecordHistoryBOList;

	NewExpDatRecordHistoryBO newExpDatRecordHistoryBO;

	NewExpDatRecordHistoryBean newExpDatRecordHistoryBean;

	@Autowired
	NewExpDatRecordHistoryService newExpDatRecordHistoryService;

	@RequestMapping(value = NewExpURIConstants.EXP_EXPENSE_RECORD_HISTORY, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getExpRecordHistory(@PathVariable Integer fkExpenseId)
			throws CommonCustomException {
		this.newExpDatRecordHistoryBeanList = new ArrayList<NewExpDatRecordHistoryBean>();
		this.newExpDatRecordHistoryBeanList = newExpDatRecordHistoryService.getExpRecordHistory(fkExpenseId);
		if (this.newExpDatRecordHistoryBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Record history listed Successfully", this.newExpDatRecordHistoryBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured, no data found", new ArrayList()));
		}

	}

}
