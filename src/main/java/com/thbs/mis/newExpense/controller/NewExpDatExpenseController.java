package com.thbs.mis.newExpense.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.newExpense.bean.NewExpClaimFinBean;
import com.thbs.mis.newExpense.bean.NewExpClaimRmBean;
import com.thbs.mis.newExpense.bean.NewExpDatExpenseBean;
import com.thbs.mis.newExpense.bo.NewExpDatExpenseBO;
import com.thbs.mis.newExpense.bo.NewExpMasCategoryBO;
import com.thbs.mis.newExpense.constants.NewExpURIConstants;
import com.thbs.mis.newExpense.dao.NewExpMasCategoryRepository;
import com.thbs.mis.newExpense.service.NewExpDatExpenseService;

@Controller
public class NewExpDatExpenseController {

	private static final AppLog LOG = LogFactory.getLog(NewExpDatExpenseController.class);

	List<NewExpDatExpenseBean> newExpDatExpenseBeanList;

	List<NewExpDatExpenseBO> newExpDatExpenseBOList;

	NewExpDatExpenseBO newExpDatExpenseBO;

	NewExpDatExpenseBean newExpDatExpenseBean;

	NewExpClaimRmBean newExpClaimRmBean;

	List<NewExpClaimRmBean> newExpClaimRmBeanList;

	ObjectMapper objectMapper;

	@Autowired
	NewExpDatExpenseService newExpDatExpenseService;

	@Autowired
	NewExpMasCategoryRepository newExpMasCategoryRepository;

	@RequestMapping(value = NewExpURIConstants.EXP_CREATE_EXPENSE, method = RequestMethod.POST, consumes = "multipart/form-data")
	public @ResponseBody ResponseEntity<MISResponse> createExpense(
			@RequestParam(value = "expenseInputJson", name = "expenseInputJson", required = true, defaultValue = "") String expenseInputJson,
			@RequestParam(value = "receiptFile", name = "receiptFile", required = false) MultipartFile receiptFile)
			throws CommonCustomException, IOException {
		this.newExpDatExpenseBean = new NewExpDatExpenseBean();
		this.newExpDatExpenseBO = new NewExpDatExpenseBO();
		this.objectMapper = new ObjectMapper();
		try {
			this.newExpDatExpenseBean = this.objectMapper.readValue(expenseInputJson, NewExpDatExpenseBean.class);
		} catch (Exception e) {
			throw new CommonCustomException("Unable to parse expense json parse.");
		}
		if (this.newExpDatExpenseBean.getFkCategoryTypeId() != null) {
			NewExpMasCategoryBO bo = newExpMasCategoryRepository
					.findByPkIdNewExpMasCategory(this.newExpDatExpenseBean.getFkCategoryTypeId());
			if (bo.getFileUploadMandatory().equalsIgnoreCase("YES"))
				if (receiptFile == null)
					throw new CommonCustomException("File has to be uploaded");
				else if (receiptFile.getSize() > 1000000)
					throw new CommonCustomException("File size is more than 1 MB");
		} else
			throw new CommonCustomException("Category Id cannot be null");
		if (this.newExpDatExpenseBean != null)
			this.newExpDatExpenseBO = newExpDatExpenseService.createExpense(this.newExpDatExpenseBean, receiptFile);
		else
			throw new CommonCustomException("Input data is required");
		if (this.newExpDatExpenseBO != null) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Expense created Successfully"));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured while creating expense, please contact nucleus"));
		}

	}

	@RequestMapping(value = NewExpURIConstants.EXP_UPDATE_EXPENSE, method = RequestMethod.POST, consumes = "multipart/form-data")
	public @ResponseBody ResponseEntity<MISResponse> updateExpense(
			@RequestParam(value = "expenseUpdateInputJson", name = "expenseUpdateInputJson", required = true, defaultValue = "") String expenseUpdateInputJson)
			throws CommonCustomException, IOException {
		this.newExpDatExpenseBean = new NewExpDatExpenseBean();
		this.newExpDatExpenseBO = new NewExpDatExpenseBO();
		this.objectMapper = new ObjectMapper();
		try {
			this.newExpDatExpenseBean = this.objectMapper.readValue(expenseUpdateInputJson, NewExpDatExpenseBean.class);
		} catch (Exception e) {
			throw new CommonCustomException("Unable to parse expense json parse.");
		}

		if (this.newExpDatExpenseBean != null)
			this.newExpDatExpenseBO = newExpDatExpenseService.updateExpense(this.newExpDatExpenseBean);
		else
			throw new CommonCustomException("Input data is required");
		if (this.newExpDatExpenseBO != null) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Expense Re-submitted Successfully"));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured while updating expense, please contact nucleus"));
		}
	}

	@RequestMapping(value = NewExpURIConstants.EXP_UPDATE_EXPENSE_ONLY_AMOUNT, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> updateAmountExpense(
			@RequestBody NewExpDatExpenseBean bean)
			throws CommonCustomException, IOException {
		this.newExpDatExpenseBO = new NewExpDatExpenseBO();
			this.newExpDatExpenseBO = newExpDatExpenseService.updateAmountExpense(bean);
		if (this.newExpDatExpenseBO != null) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Expense Updated Successfully"));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured while updating expense, please contact nucleus"));
		}
	}
	
	@RequestMapping(value = NewExpURIConstants.EXP_VIEW_EXPENSE, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> viewExpensebyId(@PathVariable Integer id)
			throws CommonCustomException, IOException {
		this.newExpDatExpenseBeanList = new ArrayList<NewExpDatExpenseBean>();
		this.newExpDatExpenseBeanList = newExpDatExpenseService.viewExpensebyId(id);
		if (this.newExpDatExpenseBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Expense details retrieved Successfully", this.newExpDatExpenseBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured while retrieve expense, please contact nucleus"));
		}
	}

	@RequestMapping(value = NewExpURIConstants.EXP_VIEW_ALL_EXPENSE_BY_EMPID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> viewAllExpenseByEmpId(@PathVariable Integer empId)
			throws CommonCustomException, IOException {
		this.newExpDatExpenseBeanList = new ArrayList<NewExpDatExpenseBean>();
		this.newExpDatExpenseBeanList = newExpDatExpenseService.viewAllExpenseByEmpId(empId);
		if (!this.newExpDatExpenseBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Expense details retrieved Successfully", this.newExpDatExpenseBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No records found"));
		}
	}

	@RequestMapping(value = NewExpURIConstants.EXP_ACCEPT_EXPENSES_BY_RM, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> acceptExpenseByRM(@RequestBody NewExpClaimRmBean newExpClaimRmBean)
			throws CommonCustomException, IOException {
		this.newExpDatExpenseBeanList = new ArrayList<NewExpDatExpenseBean>();
		this.newExpDatExpenseBeanList = newExpDatExpenseService.acceptExpenseByRM(newExpClaimRmBean);
		if (!this.newExpDatExpenseBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Expense Approved Successfully", this.newExpDatExpenseBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured while approving, please contact nucleus"));
		}
	}

	@RequestMapping(value = NewExpURIConstants.EXP_REJECT_EXPENSES_BY_RM, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> rejectExpenseByRM(@RequestBody NewExpClaimRmBean newExpClaimRmBean)
			throws CommonCustomException, IOException {
		this.newExpDatExpenseBeanList = new ArrayList<NewExpDatExpenseBean>();
		this.newExpDatExpenseBeanList = newExpDatExpenseService.rejectExpenseByRM(newExpClaimRmBean);
		if (this.newExpDatExpenseBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Expense Rejected Successfully", this.newExpDatExpenseBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured while rejecting, please contact nucleus"));
		}
	}

	@RequestMapping(value = NewExpURIConstants.EXP_FORWARD_EXPENSES_BY_RM, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> forwardExpenseByRM(
			@RequestBody NewExpClaimRmBean newExpClaimRmBean) throws CommonCustomException, IOException {
		this.newExpDatExpenseBeanList = new ArrayList<NewExpDatExpenseBean>();
		this.newExpDatExpenseBeanList = newExpDatExpenseService.forwardExpenseByRM(newExpClaimRmBean);
		if (this.newExpDatExpenseBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Expense Fowarded Successfully", this.newExpDatExpenseBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured while forwarding, please contact nucleus"));
		}
	}

	@RequestMapping(value = NewExpURIConstants.EXP_EMP_CANCEL, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> expenseCancelByEmp(
			@RequestBody NewExpDatExpenseBean newExpDatExpenseBean) throws CommonCustomException, IOException {
		this.newExpDatExpenseBeanList = new ArrayList<NewExpDatExpenseBean>();
		this.newExpDatExpenseBeanList = newExpDatExpenseService.expenseCancelByEmp(newExpDatExpenseBean);
		if (this.newExpDatExpenseBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Expense Cancelled Successfully"));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured, Please contact nucleus team"));
		}
	}

	@RequestMapping(value = NewExpURIConstants.EXP_DELETE, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<MISResponse> deleteExpense(
			@RequestBody NewExpDatExpenseBean newExpDatExpenseBean) throws CommonCustomException, IOException {
		Boolean check = newExpDatExpenseService.deleteExpense(newExpDatExpenseBean);
		if (check) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Expense Deleted Successfully"));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured, Please contact nucleus team"));
		}
	}

	@RequestMapping(value = NewExpURIConstants.EXP_DOWNLOAD, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> downloadExpense(@PathVariable Integer expId,
			HttpServletResponse response) throws CommonCustomException, IOException {
		Boolean check = newExpDatExpenseService.downloadExpense(expId, response);
		if (check) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Expense Downloaded Successfully"));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured, Please contact nucleus team"));
		}
	}

	@RequestMapping(value = NewExpURIConstants.EXP_ACCEPT_BY_FINANCE, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> acceptExpenseByFinance(
			@RequestBody NewExpClaimFinBean newExpClaimFinBean) throws CommonCustomException, IOException {
		this.newExpDatExpenseBeanList = new ArrayList<NewExpDatExpenseBean>();
		this.newExpDatExpenseBeanList = newExpDatExpenseService.acceptExpenseByFinance(newExpClaimFinBean);
		if (!this.newExpDatExpenseBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Expense Approved Successfully", this.newExpDatExpenseBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured while approving, please contact nucleus"));
		}
	}

	@RequestMapping(value = NewExpURIConstants.EXP_REJECT_BY_FINANCE, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> rejectExpenseByFinance(
			@RequestBody NewExpClaimFinBean newExpClaimFinBean) throws CommonCustomException, IOException {
		this.newExpDatExpenseBeanList = new ArrayList<NewExpDatExpenseBean>();
		this.newExpDatExpenseBeanList = newExpDatExpenseService.rejectExpenseByFinance(newExpClaimFinBean);
		if (!this.newExpDatExpenseBeanList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Expense Rejected Successfully", this.newExpDatExpenseBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured while approving, please contact nucleus"));
		}
	}
}
