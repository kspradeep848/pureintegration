package com.thbs.mis.newExpense.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.newExpense.bean.NewExpClaimRmBean;
import com.thbs.mis.newExpense.bo.NewExpClaimRmBO;
import com.thbs.mis.newExpense.constants.NewExpURIConstants;
import com.thbs.mis.newExpense.service.NewExpClaimRmService;

@Controller
public class NewExpClaimRmController {

	private static final AppLog LOG = LogFactory.getLog(NewExpClaimRmController.class);

	List<NewExpClaimRmBean> newExpClaimRmBeanList;

	List<NewExpClaimRmBO> newExpClaimRmBOList;

	NewExpClaimRmBO newExpClaimRmBO;

	NewExpClaimRmBean newExpClaimRmBean;

	@Autowired
	NewExpClaimRmService newExpClaimRmService;

	@RequestMapping(value = NewExpURIConstants.EXP_VIEW_RM_CLAIMS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> viewAllExpenseByRmId(@PathVariable Integer rmId)
			throws CommonCustomException, IOException {
		this.newExpClaimRmBeanList = new ArrayList<NewExpClaimRmBean>();
		this.newExpClaimRmBeanList = newExpClaimRmService.viewAllExpenseByRmId(rmId);
		if (this.newExpClaimRmBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Claim details retrieved Successfully", this.newExpClaimRmBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No records found"));
		}
	}

	@RequestMapping(value = NewExpURIConstants.EXP_VIEW_RM_CLAIMS_BY_CLAIM_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> viewAllExpenseByClaimId(@PathVariable Integer claimId)
			throws CommonCustomException, IOException {
		this.newExpClaimRmBeanList = new ArrayList<NewExpClaimRmBean>();
		this.newExpClaimRmBeanList = newExpClaimRmService.viewAllExpenseByClaimId(claimId);
		if (this.newExpClaimRmBeanList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Claim details retrieved Successfully", this.newExpClaimRmBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured while retrieve Claims, please contact nucleus"));
		}
	}

	@RequestMapping(value = NewExpURIConstants.EXP_ACCEPT_OR_REJECT_CLAIM_BY_RM, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<MISResponse> acceptOrRejectClaimByRM(
			@RequestBody NewExpClaimRmBean newExpClaimRmBean) throws CommonCustomException, IOException {
//		this.newExpClaimRmBean = new NewExpClaimRmBean();
		this.newExpClaimRmBeanList = new ArrayList<NewExpClaimRmBean>();
		this.newExpClaimRmBeanList = newExpClaimRmService.acceptOrRejectClaimByRM(newExpClaimRmBean);
		if (Optional.ofNullable(this.newExpClaimRmBeanList).isPresent() && this.newExpClaimRmBeanList.size()>0) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Claim actioned Successfully",this.newExpClaimRmBeanList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured while perfoming claim action, please contact nucleus"));
		}
	}

}
