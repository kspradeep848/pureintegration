package com.thbs.mis.newExpense.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.newExpense.bean.NewExpMasCategoryBean;
import com.thbs.mis.newExpense.bo.NewExpMasCategoryBO;
import com.thbs.mis.newExpense.constants.NewExpURIConstants;
import com.thbs.mis.newExpense.service.NewExpMasCategoryService;

@Controller
public class NewExpMasCategoryController {

	private static final AppLog LOG = LogFactory.getLog(NewExpMasCategoryController.class);

	List<NewExpMasCategoryBean> newExpMasCategoryBeanList;

	List<NewExpMasCategoryBO> newExpMasCategoryBOList;

	NewExpMasCategoryBO newExpMasCategoryBO;

	NewExpMasCategoryBean newExpMasCategoryBean;

	@Autowired
	NewExpMasCategoryService newExpMasCategoryService;

	@RequestMapping(value = NewExpURIConstants.EXP_GET_CATEGORY, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getExpCategory() throws CommonCustomException {
		this.newExpMasCategoryBOList = new ArrayList<NewExpMasCategoryBO>();
		this.newExpMasCategoryBOList = newExpMasCategoryService.getExpCategory();
		if (this.newExpMasCategoryBOList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Claim status listed Successfully", this.newExpMasCategoryBOList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured, no data found", new ArrayList()));
		}

	}

}
