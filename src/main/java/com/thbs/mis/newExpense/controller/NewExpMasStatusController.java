package com.thbs.mis.newExpense.controller;import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.newExpense.bean.NewExpMasStatusBean;
import com.thbs.mis.newExpense.bo.NewExpMasStatusBO;
import com.thbs.mis.newExpense.constants.NewExpURIConstants;
import com.thbs.mis.newExpense.service.NewExpMasStatusService;@Controller
public class NewExpMasStatusController {

	private static final AppLog LOG = LogFactory.getLog(NewExpMasStatusController.class);

	List<NewExpMasStatusBean> newExpMasStatusBeanList;

	List<NewExpMasStatusBO> newExpMasStatusBOList;

	NewExpMasStatusBO newExpMasStatusBO;

	NewExpMasStatusBean newExpMasStatusBean;
	
	@Autowired
	NewExpMasStatusService newExpMasStatusService;
	
	@RequestMapping(value = NewExpURIConstants.EXP_GET_EXPENSE_STATUS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getExpStatus() throws CommonCustomException {
		this.newExpMasStatusBOList = new ArrayList<NewExpMasStatusBO>();
		this.newExpMasStatusBOList = newExpMasStatusService.getExpStatus();
		if (this.newExpMasStatusBOList.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Status listed Successfully", this.newExpMasStatusBOList));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE,
							"Some error occured, no data found", new ArrayList()));
		}

	}
	

}
