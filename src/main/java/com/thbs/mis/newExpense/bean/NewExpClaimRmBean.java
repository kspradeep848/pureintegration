package com.thbs.mis.newExpense.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.googlecode.jmapper.annotations.JMap;
import com.thbs.mis.framework.util.CustomDateSerializer;
import com.thbs.mis.framework.util.CustomTimeSerializer;
import com.thbs.mis.newExpense.bo.NewExpDatExpenseBO;
import com.thbs.mis.newExpense.bo.NewExpMasClaimStatusBO;

public class NewExpClaimRmBean {

	@JMap
	private Integer pkidExpClaimRm;

	@JMap
	private Integer fkEmpId;

	@JMap
	private Integer fkRmId;

	@JMap
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date claimCreatedDate;

	@JMap
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date claimFromDate;

	@JMap
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date claimToDate;

	@JMap
	private Integer fkStatusId;

//	@JMap
	private Integer financeTeam;

	private List<NewExpDatExpenseBO> newExpDatExpenseBO;

	@JMap
	private NewExpMasClaimStatusBO newExpMasClaimStatusBO;

	private List<NewExpDatExpenseBO> expenseBO;//not needed

	private String empName;

	private List<Integer> actionList;
	
	private Integer toMgrId;
	
	private String rmComments;

	public Integer getPkidExpClaimRm() {
		return pkidExpClaimRm;
	}

	public void setPkidExpClaimRm(Integer pkidExpClaimRm) {
		this.pkidExpClaimRm = pkidExpClaimRm;
	}

	public Date getClaimFromDate() {
		return claimFromDate;
	}

	public void setClaimFromDate(Date claimFromDate) {
		this.claimFromDate = claimFromDate;
	}

	public Date getClaimToDate() {
		return claimToDate;
	}

	public void setClaimToDate(Date claimToDate) {
		this.claimToDate = claimToDate;
	}

	public Integer getFkStatusId() {
		return fkStatusId;
	}

	public void setFkStatusId(Integer fkStatusId) {
		this.fkStatusId = fkStatusId;
	}

	public Integer getFinanceTeam() {
		return financeTeam;
	}

	public void setFinanceTeam(Integer financeTeam) {
		this.financeTeam = financeTeam;
	}

	public Date getClaimCreatedDate() {
		return claimCreatedDate;
	}

	public void setClaimCreatedDate(Date claimCreatedDate) {
		this.claimCreatedDate = claimCreatedDate;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public NewExpMasClaimStatusBO getNewExpMasClaimStatusBO() {
		return newExpMasClaimStatusBO;
	}

	public void setNewExpMasClaimStatusBO(NewExpMasClaimStatusBO newExpMasClaimStatusBO) {
		this.newExpMasClaimStatusBO = newExpMasClaimStatusBO;
	}

	public List<NewExpDatExpenseBO> getExpenseBO() {
		return expenseBO;
	}

	public void setExpenseBO(List<NewExpDatExpenseBO> expenseBO) {
		this.expenseBO = expenseBO;
	}

	public List<NewExpDatExpenseBO> getNewExpDatExpenseBO() {
		return newExpDatExpenseBO;
	}

	public void setNewExpDatExpenseBO(List<NewExpDatExpenseBO> newExpDatExpenseBO) {
		this.newExpDatExpenseBO = newExpDatExpenseBO;
	}

	
	public List<Integer> getActionList() {
		return actionList;
	}

	public void setActionList(List<Integer> actionList) {
		this.actionList = actionList;
	}

	public Integer getFkRmId() {
		return fkRmId;
	}

	public void setFkRmId(Integer fkRmId) {
		this.fkRmId = fkRmId;
	}

	public Integer getToMgrId() {
		return toMgrId;
	}

	public void setToMgrId(Integer toMgrId) {
		this.toMgrId = toMgrId;
	}

	public String getRmComments() {
		return rmComments;
	}

	public void setRmComments(String rmComments) {
		this.rmComments = rmComments;
	}

	@Override
	public String toString() {
		return "NewExpClaimRmBean [pkidExpClaimRm=" + pkidExpClaimRm + ", fkEmpId=" + fkEmpId + ", fkRmId=" + fkRmId
				+ ", claimCreatedDate=" + claimCreatedDate + ", claimFromDate=" + claimFromDate + ", claimToDate="
				+ claimToDate + ", fkStatusId=" + fkStatusId + ", financeTeam=" + financeTeam + ", newExpDatExpenseBO="
				+ newExpDatExpenseBO + ", newExpMasClaimStatusBO=" + newExpMasClaimStatusBO + ", expenseBO=" + expenseBO
				+ ", empName=" + empName + ", actionList=" + actionList + ", toMgrId=" + toMgrId + ", rmComments="
				+ rmComments + "]";
	}

}
