package com.thbs.mis.newExpense.bean;

import com.googlecode.jmapper.annotations.JMap;

public class NewExpMasClaimStatusBean {

	@JMap
	private Integer pkIdExpMasClaimStatus;

	@JMap
	private String claimStatus;

	public Integer getPkIdExpMasClaimStatus() {
		return pkIdExpMasClaimStatus;
	}

	public void setPkIdExpMasClaimStatus(Integer pkIdExpMasClaimStatus) {
		this.pkIdExpMasClaimStatus = pkIdExpMasClaimStatus;
	}

	public String getClaimStatus() {
		return claimStatus;
	}

	public void setClaimStatus(String claimStatus) {
		this.claimStatus = claimStatus;
	}

	@Override
	public String toString() {
		return "NewExpMasClaimStatusBean [pkIdExpMasClaimStatus=" + pkIdExpMasClaimStatus + ", claimStatus="
				+ claimStatus + "]";
	}

}
