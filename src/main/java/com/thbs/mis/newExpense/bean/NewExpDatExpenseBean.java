package com.thbs.mis.newExpense.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.googlecode.jmapper.annotations.JMap;
import com.thbs.mis.common.bo.MasCurrencytypeBO;
import com.thbs.mis.framework.util.CustomDateSerializer;
import com.thbs.mis.framework.util.CustomTimeSerializer;
import com.thbs.mis.newExpense.bo.NewExpMasCategoryBO;
import com.thbs.mis.newExpense.bo.NewExpMasStatusBO;

public class NewExpDatExpenseBean {

	@JMap
	private Integer pkIdNewExpDatExpense;

	@JMap
	private Integer fkEmpid;

	@JMap
	private Integer fkExpRmClaimRecordId;

	@JMap
	private Integer fkExpFinClaimRecordId;

	@JMap
	private Integer fkCategoryTypeId;

	@JMap
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date expCreatedOn;

	@JMap
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date expModifiedOn;

	@JMap
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date expStartDate;

	@JMap
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date expEndDate;

	@JMap
	private String expFromPlace;

	@JMap
	private String expToPlace;

	@JMap
	private String description;

	@JMap
	private Integer fkRmEmpId;

	@JMap
	private String rmComments;
	
//	@JMap
	private Integer fkFinEmpId;

	@JMap
	private String finComments;

	@JMap
	private Short expCurrencyTypeId;

	@JMap
	private Double expAmount;

	@JMap
	private String isBillTaxInclusive;

	@JMap
	private Double taxAmount;

	@JMap
	private Double expTotalAmount;

	@JMap
	private Short convertedAmountToCurrencyTypeId;

	@JMap
	private Double convertedTotalAmountToPayroll;

	@JMap
	private String vendor;

	@JMap
	private Integer fkBuId;

	@JMap
	private Integer fkStatusId;

	@JMap
	private Integer financeTeam;

	@JMap
	private Integer fkClientId;

	@JMap
	private Integer fkProjectId;

	@JMap
	private Integer fkWorkorderId;

	@JMap
	private String billlableToClient;

	@JMap
	private Short fkEmpOrg;

	@JMap
	private Integer fkMileageId;

	@JMap
	private Double miles;

	@JMap
	private Integer modeOfTransport;

	@JMap
	private String ticketNumber;

	@JMap
	private Integer noOfAttendees;

	@JMap
	private Integer noOfNights;

	@JMap
	private Integer phoneAndIt;

	@JMap
	private Double duration;

	@JMap
	private String fileName;

	@JMap
	private String pathname;
	
	@JMap
	private NewExpMasStatusBO newExpMasStatusBO;
	
	@JMap
	private NewExpMasCategoryBO newExpMasCategoryBO;
	
	@JMap
	private MasCurrencytypeBO expCurrencytypeBO;
	
	@JMap
	private MasCurrencytypeBO convertedCurrencytypeBO;
	
	private String empName;
	
	private String rmName;
	
	private String financeName;

	public Integer getPkIdNewExpDatExpense() {
		return pkIdNewExpDatExpense;
	}

	public void setPkIdNewExpDatExpense(Integer pkIdNewExpDatExpense) {
		this.pkIdNewExpDatExpense = pkIdNewExpDatExpense;
	}

	public Integer getFkEmpid() {
		return fkEmpid;
	}

	public void setFkEmpid(Integer fkEmpid) {
		this.fkEmpid = fkEmpid;
	}

	public Integer getFkExpRmClaimRecordId() {
		return fkExpRmClaimRecordId;
	}

	public void setFkExpRmClaimRecordId(Integer fkExpRmClaimRecordId) {
		this.fkExpRmClaimRecordId = fkExpRmClaimRecordId;
	}

	public Integer getFkExpFinClaimRecordId() {
		return fkExpFinClaimRecordId;
	}

	public void setFkExpFinClaimRecordId(Integer fkExpFinClaimRecordId) {
		this.fkExpFinClaimRecordId = fkExpFinClaimRecordId;
	}

	public Integer getFkCategoryTypeId() {
		return fkCategoryTypeId;
	}

	public void setFkCategoryTypeId(Integer fkCategoryTypeId) {
		this.fkCategoryTypeId = fkCategoryTypeId;
	}

	public Date getExpCreatedOn() {
		return expCreatedOn;
	}

	public void setExpCreatedOn(Date expCreatedOn) {
		this.expCreatedOn = expCreatedOn;
	}

	public Date getExpModifiedOn() {
		return expModifiedOn;
	}

	public void setExpModifiedOn(Date expModifiedOn) {
		this.expModifiedOn = expModifiedOn;
	}

	public Date getExpStartDate() {
		return expStartDate;
	}

	public void setExpStartDate(Date expStartDate) {
		this.expStartDate = expStartDate;
	}

	public Date getExpEndDate() {
		return expEndDate;
	}

	public void setExpEndDate(Date expEndDate) {
		this.expEndDate = expEndDate;
	}

	public String getExpFromPlace() {
		return expFromPlace;
	}

	public void setExpFromPlace(String expFromPlace) {
		this.expFromPlace = expFromPlace;
	}

	public String getExpToPlace() {
		return expToPlace;
	}

	public void setExpToPlace(String expToPlace) {
		this.expToPlace = expToPlace;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getFkRmEmpId() {
		return fkRmEmpId;
	}

	public void setFkRmEmpId(Integer fkRmEmpId) {
		this.fkRmEmpId = fkRmEmpId;
	}

	public String getRmComments() {
		return rmComments;
	}

	public void setRmComments(String rmComments) {
		this.rmComments = rmComments;
	}

	public String getFinComments() {
		return finComments;
	}

	public void setFinComments(String finComments) {
		this.finComments = finComments;
	}

	public Short getExpCurrencyTypeId() {
		return expCurrencyTypeId;
	}

	public void setExpCurrencyTypeId(Short expCurrencyTypeId) {
		this.expCurrencyTypeId = expCurrencyTypeId;
	}

	public Double getExpAmount() {
		return expAmount;
	}

	public void setExpAmount(Double expAmount) {
		this.expAmount = expAmount;
	}

	public String getIsBillTaxInclusive() {
		return isBillTaxInclusive;
	}

	public void setIsBillTaxInclusive(String isBillTaxInclusive) {
		this.isBillTaxInclusive = isBillTaxInclusive;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Double getExpTotalAmount() {
		return expTotalAmount;
	}

	public void setExpTotalAmount(Double expTotalAmount) {
		this.expTotalAmount = expTotalAmount;
	}

	public Short getConvertedAmountToCurrencyTypeId() {
		return convertedAmountToCurrencyTypeId;
	}

	public void setConvertedAmountToCurrencyTypeId(Short convertedAmountToCurrencyTypeId) {
		this.convertedAmountToCurrencyTypeId = convertedAmountToCurrencyTypeId;
	}

	public Double getConvertedTotalAmountToPayroll() {
		return convertedTotalAmountToPayroll;
	}

	public void setConvertedTotalAmountToPayroll(Double convertedTotalAmountToPayroll) {
		this.convertedTotalAmountToPayroll = convertedTotalAmountToPayroll;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public Integer getFkBuId() {
		return fkBuId;
	}

	public void setFkBuId(Integer fkBuId) {
		this.fkBuId = fkBuId;
	}

	public Integer getFkStatusId() {
		return fkStatusId;
	}

	public void setFkStatusId(Integer fkStatusId) {
		this.fkStatusId = fkStatusId;
	}

	public Integer getFinanceTeam() {
		return financeTeam;
	}

	public void setFinanceTeam(Integer financeTeam) {
		this.financeTeam = financeTeam;
	}

	public Integer getFkClientId() {
		return fkClientId;
	}

	public void setFkClientId(Integer fkClientId) {
		this.fkClientId = fkClientId;
	}

	public Integer getFkProjectId() {
		return fkProjectId;
	}

	public void setFkProjectId(Integer fkProjectId) {
		this.fkProjectId = fkProjectId;
	}

	public Integer getFkWorkorderId() {
		return fkWorkorderId;
	}

	public void setFkWorkorderId(Integer fkWorkorderId) {
		this.fkWorkorderId = fkWorkorderId;
	}

	public String getBilllableToClient() {
		return billlableToClient;
	}

	public void setBilllableToClient(String billlableToClient) {
		this.billlableToClient = billlableToClient;
	}

	public Short getFkEmpOrg() {
		return fkEmpOrg;
	}

	public void setFkEmpOrg(Short fkEmpOrg) {
		this.fkEmpOrg = fkEmpOrg;
	}

	public Integer getFkMileageId() {
		return fkMileageId;
	}

	public void setFkMileageId(Integer fkMileageId) {
		this.fkMileageId = fkMileageId;
	}

	public Double getMiles() {
		return miles;
	}

	public void setMiles(Double miles) {
		this.miles = miles;
	}

	public Integer getModeOfTransport() {
		return modeOfTransport;
	}

	public void setModeOfTransport(Integer modeOfTransport) {
		this.modeOfTransport = modeOfTransport;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public Integer getNoOfAttendees() {
		return noOfAttendees;
	}

	public void setNoOfAttendees(Integer noOfAttendees) {
		this.noOfAttendees = noOfAttendees;
	}

	public Integer getNoOfNights() {
		return noOfNights;
	}

	public void setNoOfNights(Integer noOfNights) {
		this.noOfNights = noOfNights;
	}

	public Double getDuration() {
		return duration;
	}

	public void setDuration(Double duration) {
		this.duration = duration;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPathname() {
		return pathname;
	}

	public void setPathname(String pathname) {
		this.pathname = pathname;
	}

	public Integer getPhoneAndIt() {
		return phoneAndIt;
	}

	public void setPhoneAndIt(Integer phoneAndIt) {
		this.phoneAndIt = phoneAndIt;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getRmName() {
		return rmName;
	}

	public void setRmName(String rmName) {
		this.rmName = rmName;
	}

	public String getFinanceName() {
		return financeName;
	}

	public void setFinanceName(String financeName) {
		this.financeName = financeName;
	}

	public Integer getFkFinEmpId() {
		return fkFinEmpId;
	}

	public void setFkFinEmpId(Integer fkFinEmpId) {
		this.fkFinEmpId = fkFinEmpId;
	}

	public NewExpMasStatusBO getNewExpMasStatusBO() {
		return newExpMasStatusBO;
	}

	public void setNewExpMasStatusBO(NewExpMasStatusBO newExpMasStatusBO) {
		this.newExpMasStatusBO = newExpMasStatusBO;
	}

	public NewExpMasCategoryBO getNewExpMasCategoryBO() {
		return newExpMasCategoryBO;
	}

	public void setNewExpMasCategoryBO(NewExpMasCategoryBO newExpMasCategoryBO) {
		this.newExpMasCategoryBO = newExpMasCategoryBO;
	}

	public MasCurrencytypeBO getExpCurrencytypeBO() {
		return expCurrencytypeBO;
	}

	public void setExpCurrencytypeBO(MasCurrencytypeBO expCurrencytypeBO) {
		this.expCurrencytypeBO = expCurrencytypeBO;
	}

	public MasCurrencytypeBO getConvertedCurrencytypeBO() {
		return convertedCurrencytypeBO;
	}

	public void setConvertedCurrencytypeBO(MasCurrencytypeBO convertedCurrencytypeBO) {
		this.convertedCurrencytypeBO = convertedCurrencytypeBO;
	}

	@Override
	public String toString() {
		return "NewExpDatExpenseBean [pkIdNewExpDatExpense=" + pkIdNewExpDatExpense + ", fkEmpid=" + fkEmpid
				+ ", fkExpRmClaimRecordId=" + fkExpRmClaimRecordId + ", fkExpFinClaimRecordId=" + fkExpFinClaimRecordId
				+ ", fkCategoryTypeId=" + fkCategoryTypeId + ", expCreatedOn=" + expCreatedOn + ", expModifiedOn="
				+ expModifiedOn + ", expStartDate=" + expStartDate + ", expEndDate=" + expEndDate + ", expFromPlace="
				+ expFromPlace + ", expToPlace=" + expToPlace + ", description=" + description + ", fkRmEmpId="
				+ fkRmEmpId + ", rmComments=" + rmComments + ", fkFinEmpId=" + fkFinEmpId + ", finComments="
				+ finComments + ", expCurrencyTypeId=" + expCurrencyTypeId + ", expAmount=" + expAmount
				+ ", isBillTaxInclusive=" + isBillTaxInclusive + ", taxAmount=" + taxAmount + ", expTotalAmount="
				+ expTotalAmount + ", convertedAmountToCurrencyTypeId=" + convertedAmountToCurrencyTypeId
				+ ", convertedTotalAmountToPayroll=" + convertedTotalAmountToPayroll + ", vendor=" + vendor
				+ ", fkBuId=" + fkBuId + ", fkStatusId=" + fkStatusId + ", financeTeam=" + financeTeam + ", fkClientId="
				+ fkClientId + ", fkProjectId=" + fkProjectId + ", fkWorkorderId=" + fkWorkorderId
				+ ", billlableToClient=" + billlableToClient + ", fkEmpOrg=" + fkEmpOrg + ", fkMileageId=" + fkMileageId
				+ ", miles=" + miles + ", modeOfTransport=" + modeOfTransport + ", ticketNumber=" + ticketNumber
				+ ", noOfAttendees=" + noOfAttendees + ", noOfNights=" + noOfNights + ", phoneAndIt=" + phoneAndIt
				+ ", duration=" + duration + ", fileName=" + fileName + ", pathname=" + pathname
				+ ", newExpMasStatusBO=" + newExpMasStatusBO + ", newExpMasCategoryBO=" + newExpMasCategoryBO
				+ ", expCurrencytypeBO=" + expCurrencytypeBO + ", convertedCurrencytypeBO=" + convertedCurrencytypeBO
				+ ", empName=" + empName + ", rmName=" + rmName + ", financeName=" + financeName + "]";
	}

}
