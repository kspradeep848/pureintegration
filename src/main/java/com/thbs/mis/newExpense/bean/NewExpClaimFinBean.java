package com.thbs.mis.newExpense.bean;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.googlecode.jmapper.annotations.JMap;
import com.thbs.mis.framework.util.CustomDateSerializer;
import com.thbs.mis.framework.util.CustomTimeSerializer;
import com.thbs.mis.newExpense.bo.NewExpDatExpenseBO;
import com.thbs.mis.newExpense.bo.NewExpMasClaimStatusBO;

public class NewExpClaimFinBean {

	@JMap
	private Integer pkIdExpClaimFin;

	@JMap
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date claimFromDate;

	@JMap
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date claimToDate;

	@JMap
	private Integer fkStatusId;

	@JMap
	private Short claimCurrencyTypeId;

	@JMap
	private Double claimTotalAmount;

	@JMap
	private Short claimApprovedCurrencyTypeId;

	@JMap
	private Double claimApprovedAmount;
	
	@JMap
	private Integer financeTeam;
	
	@JMap
	private Integer fkEmpId;
	
	@JMap
	@Basic(optional = false)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date claimCreatedOn;
	
	@JMap
	private NewExpMasClaimStatusBO newExpMasClaimStatusBO;
	
	private List<NewExpDatExpenseBO> newExpDatExpenseBO;
	
	private Integer financeLogin;
	
	private String empName;
	
	private String finComments;
	
	private List<Integer> actionList;

	public Integer getPkIdExpClaimFin() {
		return pkIdExpClaimFin;
	}

	public void setPkIdExpClaimFin(Integer pkIdExpClaimFin) {
		this.pkIdExpClaimFin = pkIdExpClaimFin;
	}

	public Date getClaimFromDate() {
		return claimFromDate;
	}

	public void setClaimFromDate(Date claimFromDate) {
		this.claimFromDate = claimFromDate;
	}

	public Date getClaimToDate() {
		return claimToDate;
	}

	public void setClaimToDate(Date claimToDate) {
		this.claimToDate = claimToDate;
	}

	public Integer getFkStatusId() {
		return fkStatusId;
	}

	public void setFkStatusId(Integer fkStatusId) {
		this.fkStatusId = fkStatusId;
	}

	public Short getClaimCurrencyTypeId() {
		return claimCurrencyTypeId;
	}

	public void setClaimCurrencyTypeId(Short claimCurrencyTypeId) {
		this.claimCurrencyTypeId = claimCurrencyTypeId;
	}

	public Double getClaimTotalAmount() {
		return claimTotalAmount;
	}

	public void setClaimTotalAmount(Double claimTotalAmount) {
		this.claimTotalAmount = claimTotalAmount;
	}

	public Short getClaimApprovedCurrencyTypeId() {
		return claimApprovedCurrencyTypeId;
	}

	public void setClaimApprovedCurrencyTypeId(Short claimApprovedCurrencyTypeId) {
		this.claimApprovedCurrencyTypeId = claimApprovedCurrencyTypeId;
	}

	public Double getClaimApprovedAmount() {
		return claimApprovedAmount;
	}

	public void setClaimApprovedAmount(Double claimApprovedAmount) {
		this.claimApprovedAmount = claimApprovedAmount;
	}

	public NewExpMasClaimStatusBO getNewExpMasClaimStatusBO() {
		return newExpMasClaimStatusBO;
	}

	public void setNewExpMasClaimStatusBO(NewExpMasClaimStatusBO newExpMasClaimStatusBO) {
		this.newExpMasClaimStatusBO = newExpMasClaimStatusBO;
	}

	public Integer getFinanceTeam() {
		return financeTeam;
	}

	public void setFinanceTeam(Integer financeTeam) {
		this.financeTeam = financeTeam;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public Date getClaimCreatedOn() {
		return claimCreatedOn;
	}

	public void setClaimCreatedOn(Date claimCreatedOn) {
		this.claimCreatedOn = claimCreatedOn;
	}

	public Integer getFinanceLogin() {
		return financeLogin;
	}

	public void setFinanceLogin(Integer financeLogin) {
		this.financeLogin = financeLogin;
	}

	public List<NewExpDatExpenseBO> getNewExpDatExpenseBO() {
		return newExpDatExpenseBO;
	}

	public void setNewExpDatExpenseBO(List<NewExpDatExpenseBO> newExpDatExpenseBO) {
		this.newExpDatExpenseBO = newExpDatExpenseBO;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public List<Integer> getActionList() {
		return actionList;
	}

	public void setActionList(List<Integer> actionList) {
		this.actionList = actionList;
	}

	public String getFinComments() {
		return finComments;
	}

	public void setFinComments(String finComments) {
		this.finComments = finComments;
	}

	@Override
	public String toString() {
		return "NewExpClaimFinBean [pkIdExpClaimFin=" + pkIdExpClaimFin + ", claimFromDate=" + claimFromDate
				+ ", claimToDate=" + claimToDate + ", fkStatusId=" + fkStatusId + ", claimCurrencyTypeId="
				+ claimCurrencyTypeId + ", claimTotalAmount=" + claimTotalAmount + ", claimApprovedCurrencyTypeId="
				+ claimApprovedCurrencyTypeId + ", claimApprovedAmount=" + claimApprovedAmount + ", financeTeam="
				+ financeTeam + ", fkEmpId=" + fkEmpId + ", claimCreatedOn=" + claimCreatedOn
				+ ", newExpMasClaimStatusBO=" + newExpMasClaimStatusBO + ", newExpDatExpenseBO=" + newExpDatExpenseBO
				+ ", financeLogin=" + financeLogin + ", empName=" + empName + ", finComments=" + finComments
				+ ", actionList=" + actionList + "]";
	}

}
