package com.thbs.mis.newExpense.bean;

import com.googlecode.jmapper.annotations.JMap;

public class NewExpMasStatusBean {

	@JMap
	private Integer pkIdNewExpMasStatus;

	@JMap
	private String newExpMasStatusName;

	public Integer getPkIdNewExpMasStatus() {
		return pkIdNewExpMasStatus;
	}

	public void setPkIdNewExpMasStatus(Integer pkIdNewExpMasStatus) {
		this.pkIdNewExpMasStatus = pkIdNewExpMasStatus;
	}

	public String getNewExpMasStatusName() {
		return newExpMasStatusName;
	}

	public void setNewExpMasStatusName(String newExpMasStatusName) {
		this.newExpMasStatusName = newExpMasStatusName;
	}

	@Override
	public String toString() {
		return "NewExpMasStatusBean [pkIdNewExpMasStatus=" + pkIdNewExpMasStatus + ", newExpMasStatusName="
				+ newExpMasStatusName + "]";
	}

}
