package com.thbs.mis.newExpense.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.googlecode.jmapper.annotations.JMap;
import com.thbs.mis.framework.util.CustomTimeSerializer;
import com.thbs.mis.newExpense.bo.NewExpMasStatusBO;

public class NewExpDatRecordHistoryBean {

	@JMap
	private Integer pkExpRecordHistoryId;

	@JMap
	private Integer fkExpenseId;

	@JMap
	private Integer fkEmpid;

	@JMap
	private Integer fkStatusId;

	@JMap
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date recordDate;

	@JMap
	private Integer toEmpId;

	private String fkEmpIdName;

	private String toEmpIdName;

	@JMap
	private NewExpMasStatusBO newExpMasStatusBO;

	public NewExpDatRecordHistoryBean() {

	}

	public NewExpDatRecordHistoryBean(Integer pkExpRecordHistoryId, Integer fkExpenseId, Integer fkEmpid,
			Integer fkStatusId, Date recordDate, Integer toEmpId, String fkEmpIdName, String toEmpIdName,NewExpMasStatusBO newExpMasStatusBO) {
		this.pkExpRecordHistoryId = pkExpRecordHistoryId;
		this.fkExpenseId = fkExpenseId;
		this.fkEmpid = fkEmpid;
		this.fkStatusId = fkStatusId;
		this.recordDate = recordDate;
		this.toEmpId = toEmpId;
		this.fkEmpIdName = fkEmpIdName;
		this.toEmpIdName = toEmpIdName;
		this.newExpMasStatusBO = newExpMasStatusBO;
	}

	public NewExpDatRecordHistoryBean(Integer pkExpRecordHistoryId, Integer fkExpenseId, Integer fkEmpid,
			Integer fkStatusId, Date recordDate, Integer toEmpId, String fkEmpIdName,NewExpMasStatusBO newExpMasStatusBO) {
		this.pkExpRecordHistoryId = pkExpRecordHistoryId;
		this.fkExpenseId = fkExpenseId;
		this.fkEmpid = fkEmpid;
		this.fkStatusId = fkStatusId;
		this.recordDate = recordDate;
		this.toEmpId = toEmpId;
		this.fkEmpIdName = fkEmpIdName;
		this.newExpMasStatusBO = newExpMasStatusBO;
	}

	public Integer getPkExpRecordHistoryId() {
		return pkExpRecordHistoryId;
	}

	public void setPkExpRecordHistoryId(Integer pkExpRecordHistoryId) {
		this.pkExpRecordHistoryId = pkExpRecordHistoryId;
	}

	public Integer getFkExpenseId() {
		return fkExpenseId;
	}

	public void setFkExpenseId(Integer fkExpenseId) {
		this.fkExpenseId = fkExpenseId;
	}

	public Integer getFkEmpid() {
		return fkEmpid;
	}

	public void setFkEmpid(Integer fkEmpid) {
		this.fkEmpid = fkEmpid;
	}

	public Integer getFkStatusId() {
		return fkStatusId;
	}

	public void setFkStatusId(Integer fkStatusId) {
		this.fkStatusId = fkStatusId;
	}

	public Date getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}

	public Integer getToEmpId() {
		return toEmpId;
	}

	public void setToEmpId(Integer toEmpId) {
		this.toEmpId = toEmpId;
	}

	public String getFkEmpIdName() {
		return fkEmpIdName;
	}

	public void setFkEmpIdName(String fkEmpIdName) {
		this.fkEmpIdName = fkEmpIdName;
	}

	public String getToEmpIdName() {
		return toEmpIdName;
	}

	public void setToEmpIdName(String toEmpIdName) {
		this.toEmpIdName = toEmpIdName;
	}

	public NewExpMasStatusBO getNewExpMasStatusBO() {
		return newExpMasStatusBO;
	}

	public void setNewExpMasStatusBO(NewExpMasStatusBO newExpMasStatusBO) {
		this.newExpMasStatusBO = newExpMasStatusBO;
	}

	@Override
	public String toString() {
		return "NewExpDatRecordHistoryBean [pkExpRecordHistoryId=" + pkExpRecordHistoryId + ", fkExpenseId="
				+ fkExpenseId + ", fkEmpid=" + fkEmpid + ", fkStatusId=" + fkStatusId + ", recordDate=" + recordDate
				+ ", toEmpId=" + toEmpId + ", fkEmpIdName=" + fkEmpIdName + ", toEmpIdName=" + toEmpIdName
				+ ", newExpMasStatusBO=" + newExpMasStatusBO + "]";
	}

}
