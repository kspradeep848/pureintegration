package com.thbs.mis.newExpense.bean;

import com.googlecode.jmapper.annotations.JMap;

public class NewExpMasCategoryBean {

	@JMap
	private Integer pkIdNewExpMasCategory;

	@JMap
	private String newExpMasCategoryName;

	@JMap
	private String fileUploadMandatory;

	public Integer getPkIdNewExpMasCategory() {
		return pkIdNewExpMasCategory;
	}

	public void setPkIdNewExpMasCategory(Integer pkIdNewExpMasCategory) {
		this.pkIdNewExpMasCategory = pkIdNewExpMasCategory;
	}

	public String getNewExpMasCategoryName() {
		return newExpMasCategoryName;
	}

	public void setNewExpMasCategoryName(String newExpMasCategoryName) {
		this.newExpMasCategoryName = newExpMasCategoryName;
	}

	public String getFileUploadMandatory() {
		return fileUploadMandatory;
	}

	public void setFileUploadMandatory(String fileUploadMandatory) {
		this.fileUploadMandatory = fileUploadMandatory;
	}

	@Override
	public String toString() {
		return "NewExpMasCategoryBean [pkIdNewExpMasCategory=" + pkIdNewExpMasCategory + ", newExpMasCategoryName="
				+ newExpMasCategoryName + ", fileUploadMandatory=" + fileUploadMandatory + "]";
	}

}
