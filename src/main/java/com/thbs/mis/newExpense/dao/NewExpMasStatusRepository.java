package com.thbs.mis.newExpense.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.newExpense.bo.NewExpMasStatusBO;

@Repository
public interface NewExpMasStatusRepository extends GenericRepository<NewExpMasStatusBO, Integer> {

	public List<NewExpMasStatusBO> findAll();
}
