package com.thbs.mis.newExpense.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.newExpense.bo.NewExpDatRecordHistoryBO;

@Repository
public interface NewExpDatRecordHistoryRepository extends GenericRepository<NewExpDatRecordHistoryBO, Integer> {

	public List<NewExpDatRecordHistoryBO> findAll();
	
	public List<NewExpDatRecordHistoryBO> findByFkExpenseIdInOrderByPkExpRecordHistoryIdDesc(Integer fkExpenseId);
}
