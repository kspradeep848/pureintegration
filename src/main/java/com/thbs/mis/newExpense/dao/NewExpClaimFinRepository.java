package com.thbs.mis.newExpense.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.newExpense.bo.NewExpClaimFinBO;

@Repository
public interface NewExpClaimFinRepository extends GenericRepository<NewExpClaimFinBO, Integer> {

	public List<NewExpClaimFinBO> findAll();

	public NewExpClaimFinBO findByFkEmpIdAndFinanceTeamAndFkStatusId(Integer empId, Integer financeTeam,
			Integer fkStatusId);
	
	public NewExpClaimFinBO findByFkEmpIdAndFinanceTeamAndFkStatusIdAndClaimCurrencyTypeId(Integer empId, Integer financeTeam,
			Integer fkStatusId, Short claimCurrencyTypeId);

	public NewExpClaimFinBO findByPkIdExpClaimFin(Integer pkIdExpClaimFin);

	@Query("SELECT rep FROM NewExpClaimFinBO rep WHERE rep.fkStatusId IN(:fkStatusId) "
			+ " AND rep.claimCreatedOn BETWEEN :fromDate AND :toDate ORDER BY rep.pkIdExpClaimFin DESC")
	public List<NewExpClaimFinBO> fetchForFin(@Param("fkStatusId") List<Integer> fkStatusId,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

}
