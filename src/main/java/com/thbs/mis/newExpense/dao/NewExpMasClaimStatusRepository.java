package com.thbs.mis.newExpense.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.newExpense.bo.NewExpMasClaimStatusBO;

@Repository
public interface NewExpMasClaimStatusRepository extends GenericRepository<NewExpMasClaimStatusBO, Integer> {

	public List<NewExpMasClaimStatusBO> findAll();
}
