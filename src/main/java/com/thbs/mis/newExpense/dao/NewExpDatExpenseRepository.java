package com.thbs.mis.newExpense.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.newExpense.bo.NewExpDatExpenseBO;

@Repository
public interface NewExpDatExpenseRepository extends GenericRepository<NewExpDatExpenseBO, Integer> {

	public List<NewExpDatExpenseBO> findAll();

	public NewExpDatExpenseBO findByPkIdNewExpDatExpense(Integer pkIdNewExpDatExpense);

	@Transactional
	@Modifying
	@Query(" Select exp from NewExpDatExpenseBO exp where exp.pkIdNewExpDatExpense IN :pkIdNewExpDatExpense ")
	public List<NewExpDatExpenseBO> findExpenseByListOfId(
			@Param("pkIdNewExpDatExpense") List<Integer> pkIdNewExpDatExpense);

	public List<NewExpDatExpenseBO> findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(
			Integer fkExpRmClaimRecordId);

	public List<NewExpDatExpenseBO> findByFkExpRmClaimRecordIdAndFkStatusIdOrderByPkIdNewExpDatExpenseDesc(
			Integer fkExpRmClaimRecordId, Integer fkStatusId);

	public List<NewExpDatExpenseBO> findByFkEmpidAndFkStatusIdAndFinanceTeamOrderByPkIdNewExpDatExpenseDesc(
			Integer fkempId, Integer fkStatusId, Integer financeTeam);

	public List<NewExpDatExpenseBO> findByFkExpFinClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(
			Integer fkExpFinClaimRecordId);

	public List<NewExpDatExpenseBO> findByFkExpFinClaimRecordIdAndFkStatusIdOrderByPkIdNewExpDatExpenseDesc(
			Integer fkExpFinClaimRecordId, Integer fkStatusId);

	public List<NewExpDatExpenseBO> findByFkEmpid(Integer fkEmpid);

	public List<NewExpDatExpenseBO> findByFkExpFinClaimRecordId(Integer fkExpFinClaimRecordId);

	public List<NewExpDatExpenseBO> findByFkEmpidAndFkCategoryTypeId(Integer fkEmpid, Integer fkCategoryTypeId);

	public List<NewExpDatExpenseBO> findByFkEmpidAndFkCategoryTypeIdAndFkMileageId(Integer fkEmpid,
			Integer fkCategoryTypeId, Integer fkMileageId);

	@Transactional
	@Modifying
	@Query(" UPDATE NewExpDatExpenseBO exp SET exp.fkStatusId = :fkStatusId, exp.rmComments = null ,exp.financeTeam =:financeTeam "
			+ " WHERE exp.pkIdNewExpDatExpense IN :actionList ")
	public Integer approveExpenses(@Param("actionList") List<Integer> actionList,
			@Param("fkStatusId") Integer fkStatusId, @Param("financeTeam") Integer financeTeam);

	@Transactional
	@Modifying
	@Query(" UPDATE NewExpDatExpenseBO exp SET exp.fkStatusId = :fkStatusId, exp.rmComments = :comments, exp.fkExpFinClaimRecordId =null, exp.financeTeam = null "
			+ " WHERE exp.pkIdNewExpDatExpense IN :actionList ")
	public Integer rejectExpenses(@Param("actionList") List<Integer> actionList,
			@Param("fkStatusId") Integer fkStatusId, @Param("comments") String comments);

	@Transactional
	@Modifying
	@Query(" UPDATE NewExpDatExpenseBO exp SET exp.fkStatusId = :fkStatusId, exp.fkRmEmpId = :toId "
			+ " WHERE exp.pkIdNewExpDatExpense IN :actionList ")
	public Integer forwardExpenses(@Param("actionList") List<Integer> actionList,
			@Param("fkStatusId") Integer fkStatusId, @Param("toId") Integer toId);

	@Transactional
	@Modifying
	@Query(" UPDATE NewExpDatExpenseBO exp SET exp.fkStatusId = :fkStatusId "
			+ " WHERE exp.fkStatusId =1 AND exp.fkExpRmClaimRecordId = :fkExpRmClaimRecordId ")
	public Integer approveOrRejectExpense(@Param("fkExpRmClaimRecordId") Integer fkExpRmClaimRecordId,
			@Param("fkStatusId") Integer fkStatusId);

	@Transactional
	@Modifying
	@Query(" UPDATE NewExpDatExpenseBO exp SET exp.fkStatusId = :fkStatusId "
			+ " WHERE exp.pkIdNewExpDatExpense IN :pkIdNewExpDatExpense ")
	public Integer approveOrRejectExpensePartially(@Param("pkIdNewExpDatExpense") List<Integer> pkIdNewExpDatExpense,
			@Param("fkStatusId") Integer fkStatusId);

	@Transactional
	@Modifying
	@Query(" UPDATE NewExpDatExpenseBO exp SET exp.fkStatusId = :fkStatusId "
			+ " WHERE exp.pkIdNewExpDatExpense = :pkIdNewExpDatExpense ")
	public Integer approveOrRejectExpenseEach(@Param("pkIdNewExpDatExpense") Integer pkIdNewExpDatExpense,
			@Param("fkStatusId") Integer fkStatusId);

	@Transactional
	@Modifying
	@Query(" UPDATE NewExpDatExpenseBO exp SET exp.fkStatusId = :fkStatusId "
			+ " WHERE exp.pkIdNewExpDatExpense IN :actionList ")
	public Integer approveOrRejectExpensesByFinance(@Param("actionList") List<Integer> actionList,
			@Param("fkStatusId") Integer fkStatusId);

	@Transactional
	@Modifying
	@Query(" UPDATE NewExpDatExpenseBO exp SET exp.fkStatusId = :fkStatusId, exp.finComments =:finComments "
			+ " WHERE exp.pkIdNewExpDatExpense IN :actionList ")
	public Integer rejectExpensesByFinance(@Param("actionList") List<Integer> actionList,
			@Param("fkStatusId") Integer fkStatusId, @Param("finComments") String finComments);

}
