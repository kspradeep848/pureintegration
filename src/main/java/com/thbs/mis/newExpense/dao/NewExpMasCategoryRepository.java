package com.thbs.mis.newExpense.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.newExpense.bo.NewExpMasCategoryBO;

@Repository
public interface NewExpMasCategoryRepository extends GenericRepository<NewExpMasCategoryBO, Integer> {

	public List<NewExpMasCategoryBO> findAll();
	
	public NewExpMasCategoryBO findByPkIdNewExpMasCategory(Integer pkIdNewExpMasCategory);
}
