package com.thbs.mis.newExpense.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.newExpense.bo.NewExpClaimRmBO;

@Repository
public interface NewExpClaimRmRepository extends GenericRepository<NewExpClaimRmBO, Integer> {

	public List<NewExpClaimRmBO> findAll();

	public List<NewExpClaimRmBO> findByFkEmpId(Integer empId);

	public List<NewExpClaimRmBO> findByFkRmId(Integer rmId);

	public NewExpClaimRmBO findByPkidExpClaimRm(Integer pkidExpClaimRm);

	public List<NewExpClaimRmBO> findByFkEmpIdAndFkRmId(Integer empId, Integer rmId);

	public NewExpClaimRmBO findByFkEmpIdAndFkRmIdAndFkStatusId(Integer empId, Integer rmId, Integer status);

}
