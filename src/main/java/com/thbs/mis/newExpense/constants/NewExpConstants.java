package com.thbs.mis.newExpense.constants;

public class NewExpConstants {

	public final static Integer Claim_Awaiting_for_approval = 1;
	public final static Integer Claim_Forwarded = 2;
	public final static Integer Claim_Approved_by_RM = 3;
	public final static Integer Claim_Partially_Approved_by_RM = 4;
	public final static Integer Claim_Rejected_by_RM = 5;
	public final static Integer Claim_Payment_Initiated = 6;
	public final static Integer Claim_Payment_Initiated_Partially = 7;
	public final static Integer Claim_Rejected_by_Finance = 8;
	public final static Integer Claim_Reimbursed = 9;
	public final static Integer Claim_Reimbursed_Partially = 10;
	public final static Integer Claim_Reimbursement_Cancelled = 11;

	public final static Integer Exp_Awaititng_for_approval = 1;
	public final static Integer Exp_ReSubmitted = 2;
	public final static Integer Exp_Cancelled_By_Emp = 3;
	public final static Integer Exp_Forwarded = 4;
	public final static Integer Exp_Approved_by_RM = 5;
	public final static Integer Exp_Rejected_by_RM = 6;
	public final static Integer Exp_Payment_Initiated = 7;
	public final static Integer Exp_Rejected_By_Finance = 8;
	public final static Integer Exp_Reimbursed = 9;
	public final static Integer Exp_Reimbursement_Cancelled = 10;

	// Finace RoalId
	public final static Short FINANCE_HEAD = 12;
	public final static Short FINANCE_MANAGER = 13;
	public final static Short FINANCE_SENIOR_EXECUTIVE = 14;
	public final static Short FINANCE_EXECUTIVE = 15;
}
