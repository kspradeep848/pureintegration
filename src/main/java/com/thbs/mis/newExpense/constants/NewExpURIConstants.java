package com.thbs.mis.newExpense.constants;

public class NewExpURIConstants {

	public final static String EXP_GET_CATEGORY = "/EXP/get/category";

	public final static String EXP_GET_EXPENSE_STATUS = "/EXP/get/expense/status";

	public final static String EXP_GET_CLAIM_STATUS = "/EXP/get/claim/status";

	public final static String EXP_MILEAGE_RATES = "/EXP/mileage/rates"; // not developed need to use old

	public final static String EXP_EXPENSE_RECORD_HISTORY = "/EXP/record/history/{fkExpenseId}";

	public final static String EXP_CREATE_EXPENSE = "/EXP/create/expense";

	public final static String EXP_UPDATE_EXPENSE = "/EXP/update/expense";// Not using due to Ravi requirements

	public final static String EXP_UPDATE_EXPENSE_ONLY_AMOUNT = "/EXP/update/amount";

	public final static String EXP_VIEW_EXPENSE = "/EXP/view/expense/{id}";

	public final static String EXP_VIEW_ALL_EXPENSE_BY_EMPID = "/EXP/view/all/expense/{empId}";

	public final static String EXP_VIEW_EMP_EXPENSE_FILTER = "/EXP/view/filter";// not developed

	public final static String EXP_VIEW_RM_CLAIMS = "/EXP/rm/claim/{rmId}";

	public final static String EXP_VIEW_RM_CLAIMS_BY_CLAIM_ID = "/EXP/rm/claim/each/{claimId}";

	public final static String EXP_ACCEPT_OR_REJECT_CLAIM_BY_RM = "/EXP/rm/claim/approveOrReject";// need to delete this
																									// service

	public final static String EXP_ACCEPT_EXPENSES_BY_RM = "/EXP/rm/expense/approve";

	public final static String EXP_REJECT_EXPENSES_BY_RM = "/EXP/rm/expense/reject";

	public final static String EXP_FORWARD_EXPENSES_BY_RM = "/EXP/rm/expense/forward";

	public final static String EXP_EMP_CANCEL = "/EXP/emp/cancel";

	public final static String EXP_DELETE = "/EXP/delete";

	public final static String EXP_DOWNLOAD = "/EXP/Download/{expId}";

	public final static String EXP_VIEW_FINANCE_CLAIM = "/EXP/fin/all";

	public final static String EXP_VIEW_FINANCE_CLAIM_BY_EXP_ID = "/EXP/fin/{expId}";

	public final static String EXP_ACCEPT_BY_FINANCE = "/EXP/fin/approve";

	public final static String EXP_REJECT_BY_FINANCE = "/EXP/fin/reject";

	public final static String EXP_CLOSE_REIMBURSEMENT = "/EXP/close/reimbursement";

}
