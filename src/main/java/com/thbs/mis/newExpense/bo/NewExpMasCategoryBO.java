package com.thbs.mis.newExpense.bo;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "new_exp_mas_category")
@NamedQueries({ @NamedQuery(name = "NewExpMasCategoryBO.findAll", query = "SELECT e FROM NewExpMasCategoryBO e") })
public class NewExpMasCategoryBO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_id_new_exp_mas_category")
	private Integer pkIdNewExpMasCategory;

	@Basic(optional = false)
	@Column(name = "new_exp_mas_category_name")
	private String newExpMasCategoryName;

	@Basic(optional = false)
	@Column(name = "file_upload_mandatory")
	private String fileUploadMandatory;

	public Integer getPkIdNewExpMasCategory() {
		return pkIdNewExpMasCategory;
	}

	public void setPkIdNewExpMasCategory(Integer pkIdNewExpMasCategory) {
		this.pkIdNewExpMasCategory = pkIdNewExpMasCategory;
	}

	public String getNewExpMasCategoryName() {
		return newExpMasCategoryName;
	}

	public void setNewExpMasCategoryName(String newExpMasCategoryName) {
		this.newExpMasCategoryName = newExpMasCategoryName;
	}

	public String getFileUploadMandatory() {
		return fileUploadMandatory;
	}

	public void setFileUploadMandatory(String fileUploadMandatory) {
		this.fileUploadMandatory = fileUploadMandatory;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "NewExpMasCategoryBO [pkIdNewExpMasCategory=" + pkIdNewExpMasCategory + ", newExpMasCategoryName="
				+ newExpMasCategoryName + ", fileUploadMandatory=" + fileUploadMandatory + "]";
	}

}
