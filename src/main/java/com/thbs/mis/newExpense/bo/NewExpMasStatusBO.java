package com.thbs.mis.newExpense.bo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "new_exp_mas_status")
@NamedQueries({ @NamedQuery(name = "NewExpMasStatusBO.findAll", query = "SELECT e FROM NewExpMasStatusBO e") })
public class NewExpMasStatusBO {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_id_new_exp_mas_status")
	private Integer pkIdNewExpMasStatus;

	@Basic(optional = false)
	@Column(name = "new_exp_mas_status_name")
	private String newExpMasStatusName;

	public Integer getPkIdNewExpMasStatus() {
		return pkIdNewExpMasStatus;
	}

	public void setPkIdNewExpMasStatus(Integer pkIdNewExpMasStatus) {
		this.pkIdNewExpMasStatus = pkIdNewExpMasStatus;
	}

	public String getNewExpMasStatusName() {
		return newExpMasStatusName;
	}

	public void setNewExpMasStatusName(String newExpMasStatusName) {
		this.newExpMasStatusName = newExpMasStatusName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "NewExpMasStatusBO [pkIdNewExpMasStatus=" + pkIdNewExpMasStatus + ", newExpMasStatusName="
				+ newExpMasStatusName + "]";
	}

}
