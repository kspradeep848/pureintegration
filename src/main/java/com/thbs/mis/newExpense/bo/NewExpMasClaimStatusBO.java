package com.thbs.mis.newExpense.bo;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "new_exp_mas_claim_status")
@NamedQueries({
		@NamedQuery(name = "NewExpMasClaimStatusBO.findAll", query = "SELECT e FROM NewExpMasClaimStatusBO e") })
public class NewExpMasClaimStatusBO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_id_exp_mas_claim_status")
	private Integer pkIdExpMasClaimStatus;

	@Basic(optional = false)
	@Column(name = "claim_status")
	private String claimStatus;

	public Integer getPkIdExpMasClaimStatus() {
		return pkIdExpMasClaimStatus;
	}

	public void setPkIdExpMasClaimStatus(Integer pkIdExpMasClaimStatus) {
		this.pkIdExpMasClaimStatus = pkIdExpMasClaimStatus;
	}

	public String getClaimStatus() {
		return claimStatus;
	}

	public void setClaimStatus(String claimStatus) {
		this.claimStatus = claimStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "NewExpMasClaimStatusBO [pkIdExpMasClaimStatus=" + pkIdExpMasClaimStatus + ", claimStatus=" + claimStatus
				+ "]";
	}

}
