package com.thbs.mis.newExpense.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomDateSerializer;
import com.thbs.mis.framework.util.CustomTimeSerializer;

@Entity
@Table(name = "new_exp_claim_fin")
@NamedQueries({ @NamedQuery(name = "NewExpClaimFinBO.findAll", query = "SELECT e FROM NewExpClaimFinBO e") })
public class NewExpClaimFinBO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_id_exp_claim_fin")
	private Integer pkIdExpClaimFin;

	@Basic(optional = false)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomDateSerializer.class)
	@Column(name = "claim_from_date")
	private Date claimFromDate;

	@Basic(optional = false)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomDateSerializer.class)
	@Column(name = "claim_to_date")
	private Date claimToDate;

	@Basic(optional = false)
	@Column(name = "fk_status_id")
	private Integer fkStatusId;

	@Basic(optional = false)
	@Column(name = "claim_currency_type_id")
	private Short claimCurrencyTypeId;

	@Basic(optional = false)
	@Column(name = "claim_total_amount")
	private Double claimTotalAmount;

	@Basic(optional = false)
	@Column(name = "claim_approved_currency_type_id")
	private Short claimApprovedCurrencyTypeId;

	@Basic(optional = false)
	@Column(name = "claim_approved_amount")
	private Double claimApprovedAmount;

	@Basic(optional = false)
	@Column(name = "finance_team")
	private Integer financeTeam;

	@Basic(optional = false)
	@Column(name = "fk_emp_id")
	private Integer fkEmpId;

	@Basic(optional = false)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomTimeSerializer.class)
	@Column(name = "claim_created_on")
	private Date claimCreatedOn;

	@OneToOne
	@JoinColumn(name = "fk_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private NewExpMasClaimStatusBO newExpMasClaimStatusBO;

	@OneToOne
	@JoinColumn(name = "pk_id_exp_claim_fin", referencedColumnName = "fk_exp_fin_claim_record_id", unique = true, nullable = true, insertable = false, updatable = false)
	private NewExpDatExpenseBO newExpDatExpenseBO;

	public NewExpClaimFinBO() {

	}

	public NewExpClaimFinBO(Integer pkIdExpClaimFin, Integer fkEmpId, Integer financeTeam, Date claimFromDate,
			Date claimToDate, Integer fkStatusId, Date claimCreatedOn) {
		this.pkIdExpClaimFin = pkIdExpClaimFin;
		this.fkEmpId = fkEmpId;
		this.financeTeam = financeTeam;
		this.claimFromDate = claimFromDate;
		this.claimToDate = claimToDate;
		this.fkStatusId = fkStatusId;
		this.claimCreatedOn = claimCreatedOn;
	}

	public NewExpClaimFinBO(Integer pkIdExpClaimFin, Integer fkEmpId, Integer financeTeam, Date claimFromDate,
			Date claimToDate, Integer fkStatusId, Date claimCreatedOn, Short claimCurrencyTypeId,
			Double claimTotalAmount) {
		this.pkIdExpClaimFin = pkIdExpClaimFin;
		this.fkEmpId = fkEmpId;
		this.financeTeam = financeTeam;
		this.claimFromDate = claimFromDate;
		this.claimToDate = claimToDate;
		this.fkStatusId = fkStatusId;
		this.claimCreatedOn = claimCreatedOn;
		this.claimCurrencyTypeId = claimCurrencyTypeId;
		this.claimTotalAmount = claimTotalAmount;
	}

	public NewExpClaimFinBO(Integer fkEmpId, Integer financeTeam, Date claimFromDate, Date claimToDate,
			Integer fkStatusId, Date claimCreatedOn) {
		this.fkEmpId = fkEmpId;
		this.financeTeam = financeTeam;
		this.claimFromDate = claimFromDate;
		this.claimToDate = claimToDate;
		this.fkStatusId = fkStatusId;
		this.claimCreatedOn = claimCreatedOn;
	}

	public NewExpClaimFinBO(Integer fkEmpId, Integer financeTeam, Date claimFromDate, Date claimToDate,
			Integer fkStatusId, Date claimCreatedOn, Short claimCurrencyTypeId, Double claimTotalAmount) {
		this.fkEmpId = fkEmpId;
		this.financeTeam = financeTeam;
		this.claimFromDate = claimFromDate;
		this.claimToDate = claimToDate;
		this.fkStatusId = fkStatusId;
		this.claimCreatedOn = claimCreatedOn;
		this.claimCurrencyTypeId = claimCurrencyTypeId;
		this.claimTotalAmount = claimTotalAmount;
	}

	public Integer getPkIdExpClaimFin() {
		return pkIdExpClaimFin;
	}

	public void setPkIdExpClaimFin(Integer pkIdExpClaimFin) {
		this.pkIdExpClaimFin = pkIdExpClaimFin;
	}

	public Date getClaimFromDate() {
		return claimFromDate;
	}

	public void setClaimFromDate(Date claimFromDate) {
		this.claimFromDate = claimFromDate;
	}

	public Date getClaimToDate() {
		return claimToDate;
	}

	public void setClaimToDate(Date claimToDate) {
		this.claimToDate = claimToDate;
	}

	public Integer getFkStatusId() {
		return fkStatusId;
	}

	public void setFkStatusId(Integer fkStatusId) {
		this.fkStatusId = fkStatusId;
	}

	public Short getClaimCurrencyTypeId() {
		return claimCurrencyTypeId;
	}

	public void setClaimCurrencyTypeId(Short claimCurrencyTypeId) {
		this.claimCurrencyTypeId = claimCurrencyTypeId;
	}

	public Double getClaimTotalAmount() {
		return claimTotalAmount;
	}

	public void setClaimTotalAmount(Double claimTotalAmount) {
		this.claimTotalAmount = claimTotalAmount;
	}

	public Short getClaimApprovedCurrencyTypeId() {
		return claimApprovedCurrencyTypeId;
	}

	public void setClaimApprovedCurrencyTypeId(Short claimApprovedCurrencyTypeId) {
		this.claimApprovedCurrencyTypeId = claimApprovedCurrencyTypeId;
	}

	public Double getClaimApprovedAmount() {
		return claimApprovedAmount;
	}

	public void setClaimApprovedAmount(Double claimApprovedAmount) {
		this.claimApprovedAmount = claimApprovedAmount;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public NewExpMasClaimStatusBO getNewExpMasClaimStatusBO() {
		return newExpMasClaimStatusBO;
	}

	public void setNewExpMasClaimStatusBO(NewExpMasClaimStatusBO newExpMasClaimStatusBO) {
		this.newExpMasClaimStatusBO = newExpMasClaimStatusBO;
	}

	public Integer getFinanceTeam() {
		return financeTeam;
	}

	public void setFinanceTeam(Integer financeTeam) {
		this.financeTeam = financeTeam;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public Date getClaimCreatedOn() {
		return claimCreatedOn;
	}

	public void setClaimCreatedOn(Date claimCreatedOn) {
		this.claimCreatedOn = claimCreatedOn;
	}

	public NewExpDatExpenseBO getNewExpDatExpenseBO() {
		return newExpDatExpenseBO;
	}

	public void setNewExpDatExpenseBO(NewExpDatExpenseBO newExpDatExpenseBO) {
		this.newExpDatExpenseBO = newExpDatExpenseBO;
	}

	@Override
	public String toString() {
		return "NewExpClaimFinBO [pkIdExpClaimFin=" + pkIdExpClaimFin + ", claimFromDate=" + claimFromDate
				+ ", claimToDate=" + claimToDate + ", fkStatusId=" + fkStatusId + ", claimCurrencyTypeId="
				+ claimCurrencyTypeId + ", claimTotalAmount=" + claimTotalAmount + ", claimApprovedCurrencyTypeId="
				+ claimApprovedCurrencyTypeId + ", claimApprovedAmount=" + claimApprovedAmount + ", financeTeam="
				+ financeTeam + ", fkEmpId=" + fkEmpId + ", claimCreatedOn=" + claimCreatedOn
				+ ", newExpMasClaimStatusBO=" + newExpMasClaimStatusBO + ", newExpDatExpenseBO=" + newExpDatExpenseBO
				+ "]";
	}

}
