package com.thbs.mis.newExpense.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

@Entity
@Table(name = "new_exp_dat_record_history")
@NamedQueries({
		@NamedQuery(name = "NewExpDatRecordHistoryBO.findAll", query = "SELECT e FROM NewExpDatRecordHistoryBO e") })
public class NewExpDatRecordHistoryBO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_exp_record_history_id")
	private Integer pkExpRecordHistoryId;

	@Basic(optional = false)
	@Column(name = "fk_expense_id")
	private Integer fkExpenseId;

	@Basic(optional = false)
	@Column(name = "fk_emp_id")
	private Integer fkEmpid;

	@Basic(optional = false)
	@Column(name = "fk_status_id")
	private Integer fkStatusId;

	@Basic(optional = false)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomTimeSerializer.class)
	@Column(name = "record_date")
	private Date recordDate;

	@Basic(optional = false)
	@Column(name = "to_emp_id")
	private Integer toEmpId;
	
	@OneToOne
	@JoinColumn(name = "fk_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private NewExpMasStatusBO newExpMasStatusBO;

	public NewExpDatRecordHistoryBO() {
		
	}
	
	public NewExpDatRecordHistoryBO(Integer fkExpenseId, Integer fkEmpid,Integer fkStatusId,Date recordDate,Integer toEmpId) {
		this.fkExpenseId = fkExpenseId;
		this.fkEmpid = fkEmpid;
		this.fkStatusId = fkStatusId;
		this.recordDate = recordDate;
		this.toEmpId = toEmpId;
	}
	
	public NewExpDatRecordHistoryBO(Integer fkExpenseId, Integer fkEmpid,Integer fkStatusId,Date recordDate) {
		this.fkExpenseId = fkExpenseId;
		this.fkEmpid = fkEmpid;
		this.fkStatusId = fkStatusId;
		this.recordDate = recordDate;
	}
	
	public Integer getPkExpRecordHistoryId() {
		return pkExpRecordHistoryId;
	}

	public void setPkExpRecordHistoryId(Integer pkExpRecordHistoryId) {
		this.pkExpRecordHistoryId = pkExpRecordHistoryId;
	}

	public Integer getFkExpenseId() {
		return fkExpenseId;
	}

	public void setFkExpenseId(Integer fkExpenseId) {
		this.fkExpenseId = fkExpenseId;
	}

	public Integer getFkEmpid() {
		return fkEmpid;
	}

	public void setFkEmpid(Integer fkEmpid) {
		this.fkEmpid = fkEmpid;
	}

	public Integer getFkStatusId() {
		return fkStatusId;
	}

	public void setFkStatusId(Integer fkStatusId) {
		this.fkStatusId = fkStatusId;
	}

	public Date getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}

	public Integer getToEmpId() {
		return toEmpId;
	}

	public void setToEmpId(Integer toEmpId) {
		this.toEmpId = toEmpId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public NewExpMasStatusBO getNewExpMasStatusBO() {
		return newExpMasStatusBO;
	}

	public void setNewExpMasStatusBO(NewExpMasStatusBO newExpMasStatusBO) {
		this.newExpMasStatusBO = newExpMasStatusBO;
	}

	@Override
	public String toString() {
		return "NewExpDatRecordHistoryBO [pkExpRecordHistoryId=" + pkExpRecordHistoryId + ", fkExpenseId=" + fkExpenseId
				+ ", fkEmpid=" + fkEmpid + ", fkStatusId=" + fkStatusId + ", recordDate=" + recordDate + ", toEmpId="
				+ toEmpId + ", newExpMasStatusBO=" + newExpMasStatusBO + "]";
	}

}
