package com.thbs.mis.newExpense.bo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomDateSerializer;

@Entity
@Table(name = "new_exp_claim_rm")
@NamedQueries({ @NamedQuery(name = "NewExpClaimRmBO.findAll", query = "SELECT e FROM NewExpClaimRmBO e") })
public class NewExpClaimRmBO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_id_exp_claim_rm")
	private Integer pkidExpClaimRm;

	@Basic(optional = false)
	@Column(name = "fk_emp_id")
	private Integer fkEmpId;

	@Basic(optional = false)
	@Column(name = "fk_rm_id")
	private Integer fkRmId;

	@Basic(optional = false)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomDateSerializer.class)
	@Column(name = "claim_created_date")
	private Date claimCreatedDate;

	@Basic(optional = false)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomDateSerializer.class)
	@Column(name = "claim_from_date")
	private Date claimFromDate;

	@Basic(optional = false)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = CustomDateSerializer.class)
	@Column(name = "claim_to_date")
	private Date claimToDate;

	@Basic(optional = false)
	@Column(name = "fk_status_id")
	private Integer fkStatusId;

	@OneToOne
	@JoinColumn(name = "pk_id_exp_claim_rm", referencedColumnName = "fk_exp_rm_claim_record_id", unique = true, nullable = true, insertable = false, updatable = false)
	private NewExpDatExpenseBO newExpDatExpenseBO;

	@OneToOne
	@JoinColumn(name = "fk_status_id", unique = true, nullable = true, insertable = false, updatable = false)
	private NewExpMasClaimStatusBO newExpMasClaimStatusBO;

	public NewExpClaimRmBO() {

	}

	public NewExpClaimRmBO(Integer fkEmpId, Integer fkRmId, Date claimFromDate, Date claimToDate, Integer fkStatusId,
			Date claimCreatedDate) {
		this.fkEmpId = fkEmpId;
		this.fkRmId = fkRmId;
		this.claimFromDate = claimFromDate;
		this.claimToDate = claimToDate;
		this.fkStatusId = fkStatusId;
		this.claimCreatedDate = claimCreatedDate;
	}

	public NewExpClaimRmBO(Integer pkidExpClaimRm, Integer fkEmpId, Integer fkRmId, Date claimFromDate,
			Date claimToDate, Integer fkStatusId, Date claimCreatedDate) {
		this.pkidExpClaimRm = pkidExpClaimRm;
		this.fkEmpId = fkEmpId;
		this.fkRmId = fkRmId;
		this.claimFromDate = claimFromDate;
		this.claimToDate = claimToDate;
		this.fkStatusId = fkStatusId;
		this.claimCreatedDate = claimCreatedDate;
	}

	public NewExpClaimRmBO(Integer fkEmpId, Integer fkRmId, Date claimFromDate, Date claimToDate, Integer fkStatusId) {
		this.fkEmpId = fkEmpId;
		this.fkRmId = fkRmId;
		this.claimFromDate = claimFromDate;
		this.claimToDate = claimToDate;
		this.fkStatusId = fkStatusId;
	}

	public NewExpClaimRmBO(Integer pkidExpClaimRm, Integer fkEmpId, Integer fkRmId, Date claimFromDate,
			Date claimToDate, Integer fkStatusId) {
		this.pkidExpClaimRm = pkidExpClaimRm;
		this.fkEmpId = fkEmpId;
		this.fkRmId = fkRmId;
		this.claimFromDate = claimFromDate;
		this.claimToDate = claimToDate;
		this.fkStatusId = fkStatusId;
	}

	public Integer getPkidExpClaimRm() {
		return pkidExpClaimRm;
	}

	public void setPkidExpClaimRm(Integer pkidExpClaimRm) {
		this.pkidExpClaimRm = pkidExpClaimRm;
	}

	public Date getClaimFromDate() {
		return claimFromDate;
	}

	public void setClaimFromDate(Date claimFromDate) {
		this.claimFromDate = claimFromDate;
	}

	public Date getClaimToDate() {
		return claimToDate;
	}

	public void setClaimToDate(Date claimToDate) {
		this.claimToDate = claimToDate;
	}

	public Integer getFkStatusId() {
		return fkStatusId;
	}

	public void setFkStatusId(Integer fkStatusId) {
		this.fkStatusId = fkStatusId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public Integer getFkRmId() {
		return fkRmId;
	}

	public void setFkRmId(Integer fkRmId) {
		this.fkRmId = fkRmId;
	}

	public Date getClaimCreatedDate() {
		return claimCreatedDate;
	}

	public void setClaimCreatedDate(Date claimCreatedDate) {
		this.claimCreatedDate = claimCreatedDate;
	}

	public NewExpMasClaimStatusBO getNewExpMasClaimStatusBO() {
		return newExpMasClaimStatusBO;
	}

	public void setNewExpMasClaimStatusBO(NewExpMasClaimStatusBO newExpMasClaimStatusBO) {
		this.newExpMasClaimStatusBO = newExpMasClaimStatusBO;
	}

	public NewExpDatExpenseBO getNewExpDatExpenseBO() {
		return newExpDatExpenseBO;
	}

	public void setNewExpDatExpenseBO(NewExpDatExpenseBO newExpDatExpenseBO) {
		this.newExpDatExpenseBO = newExpDatExpenseBO;
	}

	@Override
	public String toString() {
		return "NewExpClaimRmBO [pkidExpClaimRm=" + pkidExpClaimRm + ", fkEmpId=" + fkEmpId + ", fkRmId=" + fkRmId
				+ ", claimCreatedDate=" + claimCreatedDate + ", claimFromDate=" + claimFromDate + ", claimToDate="
				+ claimToDate + ", fkStatusId=" + fkStatusId + ", newExpDatExpenseBO=" + newExpDatExpenseBO
				+ ", newExpMasClaimStatusBO=" + newExpMasClaimStatusBO + "]";
	}

}
