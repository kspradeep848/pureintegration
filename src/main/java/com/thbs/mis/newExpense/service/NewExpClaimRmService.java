package com.thbs.mis.newExpense.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.jmapper.JMapper;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.newExpense.bean.NewExpClaimRmBean;
import com.thbs.mis.newExpense.bo.NewExpClaimRmBO;
import com.thbs.mis.newExpense.bo.NewExpDatExpenseBO;
import com.thbs.mis.newExpense.bo.NewExpDatRecordHistoryBO;
import com.thbs.mis.newExpense.constants.NewExpConstants;
import com.thbs.mis.newExpense.dao.NewExpClaimRmRepository;
import com.thbs.mis.newExpense.dao.NewExpDatExpenseRepository;
import com.thbs.mis.newExpense.dao.NewExpDatRecordHistoryRepository;

@Service
public class NewExpClaimRmService {

	private static final AppLog LOG = LogFactory.getLog(NewExpClaimRmService.class);

	JMapper<NewExpClaimRmBean, NewExpClaimRmBO> newExpClaimRmBeanMapper = new JMapper<>(NewExpClaimRmBean.class,
			NewExpClaimRmBO.class);

	List<NewExpClaimRmBean> newExpClaimRmBeanList;

	List<NewExpClaimRmBO> newExpClaimRmBOList;

	NewExpClaimRmBO newExpClaimRmBO;

	NewExpClaimRmBean newExpClaimRmBean;

	List<NewExpDatExpenseBO> newExpDatExpenseBOList;

	NewExpDatExpenseBO newExpDatExpenseBO;

	NewExpDatRecordHistoryBO newExpDatRecordHistoryBO;

	@Autowired
	NewExpClaimRmRepository newExpClaimRmRepository;

	@Autowired
	EmployeePersonalDetailsRepository employeePersonalDetailRepo;

	@Autowired
	NewExpDatExpenseRepository newExpDatExpenseRepository;

	@Autowired
	NewExpDatRecordHistoryRepository newExpDatRecordHistoryRepository;

	Integer APPROVED_BY_RM = 3;

	Integer PARTIALLY_APPROVED_BY_RM = 4;

	Integer REJECTED_BY_RM = 5;

	Integer FORWARDED = 2;

	Integer AWATING_APPROVAL = 1;

	Integer count = 0;

	// Get Employee Name
	public String getEmployeeName(Integer empId) {
		DatEmpPersonalDetailBO empData = employeePersonalDetailRepo.findByFkEmpDetailId(empId);
		return empData.getEmpFirstName() + " " + empData.getEmpLastName();
	}

	public List<NewExpClaimRmBean> viewAllExpenseByRmId(Integer rmId) {
		this.newExpClaimRmBOList = new ArrayList<NewExpClaimRmBO>();
		this.newExpClaimRmBOList = newExpClaimRmRepository.findByFkRmId(rmId);
		return viewConvertedBeanListFromBoList(this.newExpClaimRmBOList);
	}

	public List<NewExpClaimRmBean> viewAllExpenseByClaimId(Integer claimId) throws CommonCustomException {
		this.newExpClaimRmBOList = new ArrayList<NewExpClaimRmBO>();
		this.newExpClaimRmBO = new NewExpClaimRmBO();
		this.newExpClaimRmBO = newExpClaimRmRepository.findByPkidExpClaimRm(claimId);
		if (Optional.ofNullable(this.newExpClaimRmBO).isPresent()) {
			this.newExpClaimRmBOList.add(this.newExpClaimRmBO);
			return viewConvertedBeanListFromBoList(this.newExpClaimRmBOList);
		} else
			throw new CommonCustomException("Entered Id doesn't exist");
	}

	public List<NewExpClaimRmBean> acceptOrRejectClaimByRM(NewExpClaimRmBean inputBean) throws CommonCustomException {
		// validation check
		validationToApproveOrReject(inputBean);
		try {
			this.newExpClaimRmBO = new NewExpClaimRmBO();
			this.newExpClaimRmBO = newExpClaimRmRepository.findByPkidExpClaimRm(inputBean.getPkidExpClaimRm());
			if (inputBean.getFkRmId().equals(this.newExpClaimRmBO.getFkRmId())) {
				if (this.newExpClaimRmBO.getFkStatusId().equals(NewExpConstants.Claim_Awaiting_for_approval)
						|| this.newExpClaimRmBO.getFkStatusId().equals(NewExpConstants.Claim_Forwarded)) {

					// Expense list for the input claim id
					this.newExpDatExpenseBOList = newExpDatExpenseRepository
							.findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(inputBean.getPkidExpClaimRm());

					// DB Expense status check
					for (NewExpDatExpenseBO obj : this.newExpDatExpenseBOList) {
						if (!obj.getFkStatusId().equals(NewExpConstants.Exp_Awaititng_for_approval)
								&& !obj.getFkStatusId().equals(NewExpConstants.Exp_Forwarded)) {
							throw new CommonCustomException("Expense are not in awaiting for approval");
						}
					}

					// sorting input bean
					Collections.sort(inputBean.getNewExpDatExpenseBO(), (o1, o2) -> {
						return o2.getPkIdNewExpDatExpense().compareTo(o1.getPkIdNewExpDatExpense());
					});
					inputBean.getNewExpDatExpenseBO().forEach(action -> {
						action.setExpCreatedOn(null);
						action.setExpEndDate(null);
						action.setExpStartDate(null);
					});
					this.newExpDatExpenseBOList.forEach(action -> {
						action.setExpCreatedOn(null);
						action.setExpEndDate(null);
						action.setExpStartDate(null);
					});
					if (Optional.ofNullable(this.newExpDatExpenseBOList).isPresent()
							&& this.newExpDatExpenseBOList.size() > 0) {
						if (this.newExpDatExpenseBOList.size() == inputBean.getNewExpDatExpenseBO().size()) {
							// checking the bean amount with DB amount
							for (int i = 1; i < this.newExpDatExpenseBOList.size(); i++) {
								for (int j = i; j <= i; j++) {
									if (this.newExpDatExpenseBOList.get(i).getExpTotalAmount()
											.equals(inputBean.getNewExpDatExpenseBO().get(j).getExpTotalAmount())
											&& this.newExpDatExpenseBOList.get(i).getExpCurrencyTypeId().equals(
													inputBean.getNewExpDatExpenseBO().get(j).getExpCurrencyTypeId())) {
										continue;
									} else {
										throw new CommonCustomException(
												"Some of the expenses details may get added/alterted in this claim, please refresh page");
									}
								}
							}

							// adding expense id into list
							List<Integer> listofExpense = new ArrayList<Integer>();
							this.newExpDatExpenseBOList.forEach(obj -> {
								listofExpense.add(obj.getPkIdNewExpDatExpense());
							});

							// checking the input list of approval is valid
							for (Integer obj : inputBean.getActionList())
								if (!listofExpense.contains(obj))
									throw new CommonCustomException("Expense Id didnt match with the claim");

							if (inputBean.getFkStatusId().equals(NewExpConstants.Claim_Approved_by_RM)) {
								try {
									Integer id = newExpDatExpenseRepository.approveOrRejectExpense(
											inputBean.getPkidExpClaimRm(), NewExpConstants.Exp_Approved_by_RM);
									if (id == 0)
										throw new CommonCustomException(
												"some error occured while approving all expense, please contact nucleus");
									this.newExpDatExpenseBOList.forEach(obj -> {
										this.createRecordHistory(obj.getPkIdNewExpDatExpense(), obj.getFkRmEmpId(),
												NewExpConstants.Exp_Approved_by_RM, null);
									});
									this.newExpClaimRmBO.setPkidExpClaimRm(this.newExpClaimRmBO.getPkidExpClaimRm());
									this.newExpClaimRmBO.setFkStatusId(NewExpConstants.Claim_Approved_by_RM);
									this.newExpClaimRmBO = newExpClaimRmRepository.save(this.newExpClaimRmBO);
								} catch (Exception e) {
									e.printStackTrace();
									throw new CommonCustomException(e.getMessage());
								}
							} else if (inputBean.getFkStatusId()
									.equals(NewExpConstants.Claim_Partially_Approved_by_RM)) {
								if (inputBean.getActionList().size() > 0
										&& Optional.ofNullable(inputBean.getActionList()).isPresent()) {
									try {
										Integer id = newExpDatExpenseRepository.approveOrRejectExpense(
												inputBean.getPkidExpClaimRm(), NewExpConstants.Exp_Rejected_by_RM);
										if (id == 0)
											throw new CommonCustomException(
													"some error occured while rejecting all expense, please contact nucleus");
										Integer partialApprove = newExpDatExpenseRepository
												.approveOrRejectExpensePartially(inputBean.getActionList(),
														NewExpConstants.Exp_Approved_by_RM);
										if (partialApprove == 0)
											throw new CommonCustomException(
													"some error occured while partially approve expense, please contact nucleus");
										List<NewExpDatExpenseBO> bo = new ArrayList<NewExpDatExpenseBO>();
										bo = newExpDatExpenseRepository
												.findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(
														inputBean.getPkidExpClaimRm());
										bo.forEach(obj -> {
											this.createRecordHistory(obj.getPkIdNewExpDatExpense(), obj.getFkRmEmpId(),
													obj.getFkStatusId(), null);
										});
										this.newExpClaimRmBO
												.setPkidExpClaimRm(this.newExpClaimRmBO.getPkidExpClaimRm());
										this.newExpClaimRmBO
												.setFkStatusId(NewExpConstants.Claim_Partially_Approved_by_RM);
										this.newExpClaimRmBO = newExpClaimRmRepository.save(this.newExpClaimRmBO);
									} catch (Exception e) {
										e.printStackTrace();
										throw new CommonCustomException(e.getMessage());
									}
								} else
									throw new CommonCustomException(
											"Approve list value is needed for partial approval");
							} else if (inputBean.getFkStatusId().equals(NewExpConstants.Claim_Rejected_by_RM)) {
								try {
									Integer id = newExpDatExpenseRepository.approveOrRejectExpense(
											inputBean.getPkidExpClaimRm(), NewExpConstants.Exp_Rejected_by_RM);
									if (id == 0)
										throw new CommonCustomException(
												"some error occured while rejecting all expense, please contact nucleus");
									this.newExpDatExpenseBOList.forEach(obj -> {
										this.createRecordHistory(obj.getPkIdNewExpDatExpense(), obj.getFkRmEmpId(),
												NewExpConstants.Exp_Rejected_by_RM, null);
									});
									this.newExpClaimRmBO.setPkidExpClaimRm(this.newExpClaimRmBO.getPkidExpClaimRm());
									this.newExpClaimRmBO.setFkStatusId(NewExpConstants.Claim_Rejected_by_RM);
									this.newExpClaimRmBO = newExpClaimRmRepository.save(this.newExpClaimRmBO);
								} catch (Exception e) {
									e.printStackTrace();
									throw new CommonCustomException(e.getMessage());
								}
							} else {
								throw new CommonCustomException("Only approve or reject status is allowed");
							}
						} else
							throw new CommonCustomException(
									"Some of the expenses may get added/alterted in this claim, please refresh page");
					} else
						throw new CommonCustomException("Expense are not available for this claim id");
				} else
					throw new CommonCustomException("Claim is not in awaiting status to approve");
			} else
				throw new CommonCustomException("RM to approve is mis-match");
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		return viewConvertedBeanListFromBo(this.newExpClaimRmBO);
	}

	public void validationToApproveOrReject(NewExpClaimRmBean inputBean) throws CommonCustomException {
		if (Optional.ofNullable(inputBean.getPkidExpClaimRm()).isPresent() == false)
			throw new CommonCustomException("claim id is mandatory");
		if (Optional.ofNullable(inputBean.getFkStatusId()).isPresent() == false)
			throw new CommonCustomException("status is mandatory");
		if (Optional.ofNullable(inputBean.getFkStatusId()).isPresent())
			if (!inputBean.getFkStatusId().equals(APPROVED_BY_RM)
					&& !inputBean.getFkStatusId().equals(PARTIALLY_APPROVED_BY_RM)
					&& !inputBean.getFkStatusId().equals(REJECTED_BY_RM))
				throw new CommonCustomException("Only approve or reject status is allowed");
		if (Optional.ofNullable(inputBean.getNewExpDatExpenseBO()).isPresent() == false
				|| inputBean.getNewExpDatExpenseBO().size() == 0)
			throw new CommonCustomException("Attached expense as the input");
		if (Optional.ofNullable(inputBean.getFinanceTeam()).isPresent() == false)
			throw new CommonCustomException("Finance team is mandatory");
		if (Optional.ofNullable(inputBean.getFkRmId()).isPresent() == false)
			throw new CommonCustomException("rm ID is mandatory");
	}

	// Expense Record History
	public Boolean createRecordHistory(Integer expenseId, Integer empId, Integer statusId, Integer toId) {
		try {
			if (toId == null)
				this.newExpDatRecordHistoryBO = new NewExpDatRecordHistoryBO(expenseId, empId, statusId, new Date());
			if (toId != null)
				this.newExpDatRecordHistoryBO = new NewExpDatRecordHistoryBO(expenseId, empId, statusId, new Date(),
						toId);
			this.newExpDatRecordHistoryBO = newExpDatRecordHistoryRepository.save(this.newExpDatRecordHistoryBO);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// convert the BO List into Bean List
	public List<NewExpClaimRmBean> viewConvertedBeanListFromBoList(List<NewExpClaimRmBO> bo) {
		this.newExpClaimRmBeanList = new ArrayList<NewExpClaimRmBean>();
		if (Optional.ofNullable(bo).isPresent() && bo.size() > 0) {
			bo.stream().forEach(obj -> {
				this.newExpClaimRmBean = new NewExpClaimRmBean();
				this.newExpClaimRmBean = newExpClaimRmBeanMapper.getDestination(this.newExpClaimRmBean, obj);
				this.newExpClaimRmBean.setEmpName(getEmployeeName(obj.getFkEmpId()));
				this.newExpClaimRmBean.setNewExpDatExpenseBO(newExpDatExpenseRepository
						.findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(obj.getPkidExpClaimRm()));
				this.newExpClaimRmBeanList.add(this.newExpClaimRmBean);
			});
			Collections.sort(this.newExpClaimRmBeanList, (o1, o2) -> {
				return o2.getPkidExpClaimRm().compareTo(o1.getPkidExpClaimRm());
			});
			return this.newExpClaimRmBeanList;
		} else
			return this.newExpClaimRmBeanList;
	}

	public List<NewExpClaimRmBean> viewConvertedBeanListFromBo(NewExpClaimRmBO bo) {
		this.newExpClaimRmBeanList = new ArrayList<NewExpClaimRmBean>();
		if (Optional.ofNullable(bo).isPresent()) {
			this.newExpClaimRmBean = new NewExpClaimRmBean();
			this.newExpClaimRmBean = newExpClaimRmBeanMapper.getDestination(this.newExpClaimRmBean, bo);
			this.newExpClaimRmBean.setEmpName(getEmployeeName(bo.getFkEmpId()));
			this.newExpClaimRmBean.setNewExpDatExpenseBO(newExpDatExpenseRepository
					.findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(bo.getPkidExpClaimRm()));
			this.newExpClaimRmBeanList.add(this.newExpClaimRmBean);
			Collections.sort(this.newExpClaimRmBeanList, (o1, o2) -> {
				return o2.getPkidExpClaimRm().compareTo(o1.getPkidExpClaimRm());
			});
			return this.newExpClaimRmBeanList;
		} else
			return this.newExpClaimRmBeanList;
	}

}
