package com.thbs.mis.newExpense.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.jmapper.JMapper;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.newExpense.bean.NewExpMasCategoryBean;
import com.thbs.mis.newExpense.bo.NewExpMasCategoryBO;
import com.thbs.mis.newExpense.dao.NewExpMasCategoryRepository;

@Service
public class NewExpMasCategoryService {

	private static final AppLog LOG = LogFactory.getLog(NewExpMasCategoryService.class);

	JMapper<NewExpMasCategoryBean, NewExpMasCategoryBO> newExpMasCategoryBeanMapper = new JMapper<>(
			NewExpMasCategoryBean.class, NewExpMasCategoryBO.class);

	List<NewExpMasCategoryBean> newExpMasCategoryBeanList;

	List<NewExpMasCategoryBO> newExpMasCategoryBOList;

	NewExpMasCategoryBO newExpMasCategoryBO;

	NewExpMasCategoryBean newExpMasCategoryBean;

	@Autowired
	NewExpMasCategoryRepository newExpMasCategoryRepository;

	public List<NewExpMasCategoryBO> getExpCategory() throws CommonCustomException {
		this.newExpMasCategoryBOList = new ArrayList<NewExpMasCategoryBO>();
		try {
			this.newExpMasCategoryBOList = newExpMasCategoryRepository.findAll();
			return this.newExpMasCategoryBOList;
		} catch (Exception e) {
			throw new CommonCustomException("Error occured " + e);
		}
	}

}
