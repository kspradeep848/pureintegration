package com.thbs.mis.newExpense.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.jmapper.JMapper;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.newExpense.bean.NewExpDatRecordHistoryBean;
import com.thbs.mis.newExpense.bo.NewExpDatRecordHistoryBO;
import com.thbs.mis.newExpense.dao.NewExpDatRecordHistoryRepository;

@Service
public class NewExpDatRecordHistoryService {

	private static final AppLog LOG = LogFactory.getLog(NewExpDatRecordHistoryService.class);

	JMapper<NewExpDatRecordHistoryBean, NewExpDatRecordHistoryBO> newExpDatRecordHistoryBeanMapper = new JMapper<>(
			NewExpDatRecordHistoryBean.class, NewExpDatRecordHistoryBO.class);

	List<NewExpDatRecordHistoryBean> newExpDatRecordHistoryBeanList;

	List<NewExpDatRecordHistoryBO> newExpDatRecordHistoryBOList;

	NewExpDatRecordHistoryBO newExpDatRecordHistoryBO;

	NewExpDatRecordHistoryBean newExpDatRecordHistoryBean;

	@Autowired
	NewExpDatRecordHistoryRepository newExpDatRecordHistoryRepository;

	@Autowired
	private EmployeePersonalDetailsRepository employeePersonalDetailRepo;

	public String getEmployeeName(Integer empId) {
		DatEmpPersonalDetailBO empData = employeePersonalDetailRepo.findByFkEmpDetailId(empId);
		return empData.getEmpFirstName() + " " + empData.getEmpLastName();
	}

	public List<NewExpDatRecordHistoryBean> getExpRecordHistory(Integer fkExpenseId) throws CommonCustomException {
		if (fkExpenseId != null) {
			this.newExpDatRecordHistoryBOList = new ArrayList<NewExpDatRecordHistoryBO>();
			this.newExpDatRecordHistoryBeanList = new ArrayList<NewExpDatRecordHistoryBean>();
			try {
				this.newExpDatRecordHistoryBOList = newExpDatRecordHistoryRepository
						.findByFkExpenseIdInOrderByPkExpRecordHistoryIdDesc(fkExpenseId);
			} catch (Exception e) {
				throw new CommonCustomException("Error occured " + e);
			}
			if (this.newExpDatRecordHistoryBOList.isEmpty())
				throw new CommonCustomException("Entered Expense ID doesn't have details");
			else {
				this.newExpDatRecordHistoryBOList.stream().forEach(obj -> {
					if (obj.getToEmpId() != null)
						this.newExpDatRecordHistoryBean = new NewExpDatRecordHistoryBean(obj.getPkExpRecordHistoryId(),
								obj.getFkExpenseId(), obj.getFkEmpid(), obj.getFkStatusId(), obj.getRecordDate(),
								obj.getToEmpId(), getEmployeeName(obj.getFkEmpid()), getEmployeeName(obj.getToEmpId()),
								obj.getNewExpMasStatusBO());
					else
						this.newExpDatRecordHistoryBean = new NewExpDatRecordHistoryBean(obj.getPkExpRecordHistoryId(),
								obj.getFkExpenseId(), obj.getFkEmpid(), obj.getFkStatusId(), obj.getRecordDate(),
								obj.getToEmpId(), getEmployeeName(obj.getFkEmpid()), obj.getNewExpMasStatusBO());
					this.newExpDatRecordHistoryBeanList.add(this.newExpDatRecordHistoryBean);
				});
				return this.newExpDatRecordHistoryBeanList;
			}
		} else {
			throw new CommonCustomException("Record Id should not be null");
		}
	}

}
