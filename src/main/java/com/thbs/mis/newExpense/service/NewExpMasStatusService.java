package com.thbs.mis.newExpense.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.jmapper.JMapper;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.newExpense.bean.NewExpMasStatusBean;
import com.thbs.mis.newExpense.bo.NewExpMasStatusBO;
import com.thbs.mis.newExpense.dao.NewExpMasStatusRepository;

@Service
public class NewExpMasStatusService {

	private static final AppLog LOG = LogFactory.getLog(NewExpMasStatusService.class);

	JMapper<NewExpMasStatusBean, NewExpMasStatusBO> newExpMasStatusBeanMapper = new JMapper<>(NewExpMasStatusBean.class,
			NewExpMasStatusBO.class);

	List<NewExpMasStatusBean> newExpMasStatusBeanList;

	List<NewExpMasStatusBO> newExpMasStatusBOList;

	NewExpMasStatusBO newExpMasStatusBO;

	NewExpMasStatusBean newExpMasStatusBean;

	@Autowired
	NewExpMasStatusRepository newExpMasStatusRepository;

	public List<NewExpMasStatusBO> getExpStatus() throws CommonCustomException {
		this.newExpMasStatusBOList = new ArrayList<NewExpMasStatusBO>();
		try {
			this.newExpMasStatusBOList = newExpMasStatusRepository.findAll();
			return this.newExpMasStatusBOList;
		} catch (Exception e) {
			throw new CommonCustomException("Error occured " + e);
		}
	}

}
