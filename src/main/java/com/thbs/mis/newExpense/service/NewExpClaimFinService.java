package com.thbs.mis.newExpense.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.jmapper.JMapper;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.expense.constants.ExpenseConstants;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.newExpense.bean.NewExpClaimFinBean;
import com.thbs.mis.newExpense.bean.NewExpClaimRmBean;
import com.thbs.mis.newExpense.bo.NewExpClaimFinBO;
import com.thbs.mis.newExpense.bo.NewExpClaimRmBO;
import com.thbs.mis.newExpense.bo.NewExpDatExpenseBO;
import com.thbs.mis.newExpense.bo.NewExpDatRecordHistoryBO;
import com.thbs.mis.newExpense.constants.NewExpConstants;
import com.thbs.mis.newExpense.dao.NewExpClaimFinRepository;
import com.thbs.mis.newExpense.dao.NewExpClaimRmRepository;
import com.thbs.mis.newExpense.dao.NewExpDatExpenseRepository;
import com.thbs.mis.newExpense.dao.NewExpDatRecordHistoryRepository;

@Service
public class NewExpClaimFinService {

	private static final AppLog LOG = LogFactory.getLog(NewExpClaimFinService.class);

	JMapper<NewExpClaimFinBean, NewExpClaimFinBO> newExpClaimFinBeanMapper = new JMapper<>(NewExpClaimFinBean.class,
			NewExpClaimFinBO.class);

	List<NewExpClaimFinBean> newExpClaimFinBeanList;

	List<NewExpClaimFinBO> newExpClaimFinBOList;

	NewExpClaimFinBO newExpClaimFinBO;

	NewExpClaimFinBean newExpClaimFinBean;

	List<NewExpDatExpenseBO> newExpDatExpenseBOList;

	NewExpDatExpenseBO newExpDatExpenseBO;

	NewExpDatRecordHistoryBO newExpDatRecordHistoryBO;

	NewExpClaimRmBO newExpClaimRmBO;

	@Autowired
	NewExpClaimFinRepository newExpClaimFinRepository;

	@Autowired
	private DatEmployeeProfessionalRepository datEmployeeProfessionalRepository;

	@Autowired
	EmployeePersonalDetailsRepository employeePersonalDetailRepo;

	@Autowired
	NewExpDatExpenseRepository newExpDatExpenseRepository;

	@Autowired
	NewExpDatRecordHistoryRepository newExpDatRecordHistoryRepository;

	@Autowired
	NewExpClaimRmRepository newExpClaimRmRepository;

	Integer approvedCount = 0;

	Integer rejectedCount = 0;

	Integer countExpense = 0;

	Integer paymentInitated = 0;

	Integer rejectedByFinance = 0;

	public List<NewExpClaimFinBean> viewAllExpenseByFinance(NewExpClaimFinBean newExpClaimFinBean)
			throws CommonCustomException {
		try {
			if (Optional.ofNullable(newExpClaimFinBean.getClaimFromDate()).isPresent()
					&& Optional.ofNullable(newExpClaimFinBean.getClaimToDate()).isPresent()
					&& Optional.ofNullable(newExpClaimFinBean.getFinanceLogin()).isPresent()) {
				SimpleDateFormat sdfWithSeconds = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String stringFromDate = sdf.format(newExpClaimFinBean.getClaimFromDate());
				String stringToDate = sdf.format(newExpClaimFinBean.getClaimToDate());
				stringFromDate = stringFromDate + " 00:00:00";
				stringToDate = stringToDate + " 23:59:59";
				Date fromDate = new Date();
				Date toDate = new Date();
				try {
					fromDate = sdfWithSeconds.parse(stringFromDate);
					toDate = sdfWithSeconds.parse(stringToDate);
				} catch (ParseException e) {
					e.printStackTrace();
					throw new CommonCustomException(e.getMessage());
				}
				this.newExpClaimFinBOList = new ArrayList<NewExpClaimFinBO>();
				List<Short> empRole = Arrays.asList(NewExpConstants.FINANCE_HEAD, NewExpConstants.FINANCE_MANAGER,
						NewExpConstants.FINANCE_SENIOR_EXECUTIVE, NewExpConstants.FINANCE_EXECUTIVE);
				List<DatEmpProfessionalDetailBO> finEmpId = datEmployeeProfessionalRepository
						.fetchListOfEmpFinExp(empRole);
				if (finEmpId.size() > 0) {
					boolean validFinEmp = finEmpId.stream()
							.anyMatch(obj -> obj.getFkMainEmpDetailId().equals(newExpClaimFinBean.getFinanceLogin()));
					if (validFinEmp) {
						List<Integer> finStatus = Arrays.asList(NewExpConstants.Claim_Approved_by_RM,
								NewExpConstants.Claim_Partially_Approved_by_RM, NewExpConstants.Claim_Payment_Initiated,
								NewExpConstants.Claim_Payment_Initiated_Partially,
								NewExpConstants.Claim_Rejected_by_Finance, NewExpConstants.Claim_Reimbursed,
								NewExpConstants.Claim_Reimbursed_Partially,
								NewExpConstants.Claim_Reimbursement_Cancelled);
						this.newExpClaimFinBOList = newExpClaimFinRepository.fetchForFin(finStatus, fromDate, toDate);
						return viewConvertedBeanListFromBoList(this.newExpClaimFinBOList);
					} else
						throw new CommonCustomException("Emp is not mapped with Finance role");
				}
			} else
				throw new CommonCustomException("EmpId, From date and To date is madatory");
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		return newExpClaimFinBeanList;

	}

	public List<NewExpClaimFinBean> viewConvertedBeanListFromBoList(List<NewExpClaimFinBO> bo) {
		this.newExpClaimFinBeanList = new ArrayList<NewExpClaimFinBean>();
		if (Optional.ofNullable(bo).isPresent() && bo.size() > 0) {
			bo.stream().forEach(obj -> {
				this.newExpClaimFinBean = new NewExpClaimFinBean();
				this.newExpClaimFinBean = newExpClaimFinBeanMapper.getDestination(this.newExpClaimFinBean, obj);
				this.newExpClaimFinBean.setEmpName(getEmployeeName(obj.getFkEmpId()));
				this.newExpClaimFinBean.setNewExpDatExpenseBO(newExpDatExpenseRepository
						.findByFkExpFinClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(obj.getPkIdExpClaimFin()));
				this.newExpClaimFinBeanList.add(this.newExpClaimFinBean);
			});
			Collections.sort(this.newExpClaimFinBeanList, (o1, o2) -> {
				return o2.getPkIdExpClaimFin().compareTo(o1.getPkIdExpClaimFin());
			});
			return this.newExpClaimFinBeanList;
		} else
			return this.newExpClaimFinBeanList;
	}

	public List<NewExpClaimFinBean> viewConvertedBeanListFromBo(NewExpClaimFinBO bo) {
		this.newExpClaimFinBeanList = new ArrayList<NewExpClaimFinBean>();
		if (Optional.ofNullable(bo).isPresent()) {
			this.newExpClaimFinBean = new NewExpClaimFinBean();
			this.newExpClaimFinBean = newExpClaimFinBeanMapper.getDestination(this.newExpClaimFinBean, bo);
			this.newExpClaimFinBean.setEmpName(getEmployeeName(bo.getFkEmpId()));
			this.newExpClaimFinBean.setNewExpDatExpenseBO(newExpDatExpenseRepository
					.findByFkExpFinClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(bo.getPkIdExpClaimFin()));
			this.newExpClaimFinBeanList.add(this.newExpClaimFinBean);
			Collections.sort(this.newExpClaimFinBeanList, (o1, o2) -> {
				return o2.getPkIdExpClaimFin().compareTo(o1.getPkIdExpClaimFin());
			});
			return this.newExpClaimFinBeanList;
		} else
			return this.newExpClaimFinBeanList;
	}

	// Get Employee Name
	public String getEmployeeName(Integer empId) {
		DatEmpPersonalDetailBO empData = employeePersonalDetailRepo.findByFkEmpDetailId(empId);
		return empData.getEmpFirstName() + " " + empData.getEmpLastName();
	}

	public List<NewExpClaimFinBean> viewExpenseByFinanceId(Integer expId) throws CommonCustomException {
		try {
			this.newExpClaimFinBeanList = new ArrayList<NewExpClaimFinBean>();
			this.newExpClaimFinBO = new NewExpClaimFinBO();
			this.newExpClaimFinBO = newExpClaimFinRepository.findByPkIdExpClaimFin(expId);
			return viewConvertedBeanListFromBo(this.newExpClaimFinBO);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
	}

	public List<NewExpClaimFinBean> closeReimbursementByFinance(NewExpClaimFinBean newExpClaimFinBean)
			throws CommonCustomException {
		try {
			this.approvedCount = 0;
			this.rejectedCount = 0;
			this.newExpClaimFinBeanList = new ArrayList<NewExpClaimFinBean>();
			this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
			Boolean check = this.validToFinanceActionForApproveExpense(newExpClaimFinBean);
			if (check) {
				this.newExpClaimFinBO = newExpClaimFinRepository
						.findByPkIdExpClaimFin(newExpClaimFinBean.getPkIdExpClaimFin());
				if (Optional.ofNullable(this.newExpClaimFinBO).isPresent()) {
					if (this.newExpClaimFinBO.getFkStatusId().equals(NewExpConstants.Claim_Payment_Initiated_Partially)
							|| this.newExpClaimFinBO.getFkStatusId().equals(NewExpConstants.Claim_Payment_Initiated)) {
						this.newExpDatExpenseBOList = newExpDatExpenseRepository
								.findByFkExpFinClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(
										newExpClaimFinBean.getPkIdExpClaimFin());
						if (!this.newExpDatExpenseBOList.isEmpty()) {
							this.newExpDatExpenseBOList.forEach(bo -> {
								if (bo.getFkStatusId().equals(NewExpConstants.Exp_Payment_Initiated))
									++this.approvedCount;
								else if (bo.getFkStatusId().equals(NewExpConstants.Exp_Rejected_By_Finance))
									++this.rejectedCount;
							});
							Integer totalCount = this.rejectedCount + this.approvedCount;
							if (totalCount.equals(this.newExpDatExpenseBOList.size())) {

								List<Integer> listOfId = new ArrayList<Integer>();
								this.newExpDatExpenseBOList.forEach(obj -> {
									if (obj.getFkStatusId().equals(NewExpConstants.Exp_Payment_Initiated)
											|| obj.getFkStatusId().equals(NewExpConstants.Exp_Rejected_By_Finance))
										listOfId.add(obj.getPkIdNewExpDatExpense());
								});
								List<Integer> listOfIdapprvoved = new ArrayList<Integer>();
								this.newExpDatExpenseBOList.forEach(obj -> {
									if (obj.getFkStatusId().equals(NewExpConstants.Exp_Payment_Initiated))
										listOfIdapprvoved.add(obj.getPkIdNewExpDatExpense());
								});
								Integer id = newExpDatExpenseRepository.approveOrRejectExpensesByFinance(
										listOfIdapprvoved, NewExpConstants.Exp_Reimbursed);
								if (id.equals(listOfIdapprvoved.size())) {
									if (totalCount.equals(id)) {
										this.newExpClaimFinBO
												.setPkIdExpClaimFin(this.newExpClaimFinBO.getPkIdExpClaimFin());
										this.newExpClaimFinBO.setFkStatusId(NewExpConstants.Claim_Reimbursed);
										newExpClaimFinRepository.save(this.newExpClaimFinBO);
									} else if (this.rejectedCount > 0 && !totalCount.equals(id)) {
										this.newExpClaimFinBO
												.setPkIdExpClaimFin(this.newExpClaimFinBO.getPkIdExpClaimFin());
										this.newExpClaimFinBO.setFkStatusId(NewExpConstants.Claim_Reimbursed_Partially);
										newExpClaimFinRepository.save(this.newExpClaimFinBO);
									} else if (totalCount != this.newExpDatExpenseBOList.size()) {
										throw new CommonCustomException(
												"To close reimbursement all should be actioned by Finance");
									}

									// adding status to rm claim
									this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
									this.newExpDatExpenseBOList = newExpDatExpenseRepository
											.findByFkExpFinClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(
													newExpClaimFinBean.getPkIdExpClaimFin());
									List<Integer> rmClaimId = new ArrayList<Integer>();
									for (NewExpDatExpenseBO bo : this.newExpDatExpenseBOList) {
										rmClaimId.add(bo.getFkExpRmClaimRecordId());
									}
									for (Integer pkid : rmClaimId) {
										this.paymentInitated = 0;
										this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
										this.newExpDatExpenseBOList = newExpDatExpenseRepository
												.findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(pkid);
										for (NewExpDatExpenseBO obj : this.newExpDatExpenseBOList) {
											if (obj.getFkStatusId()
													.equals(NewExpConstants.Exp_Awaititng_for_approval)) {
												this.newExpClaimRmBO = new NewExpClaimRmBO();
												this.newExpClaimRmBO = newExpClaimRmRepository
														.findByPkidExpClaimRm(pkid);
												this.newExpClaimRmBO
														.setPkidExpClaimRm(this.newExpClaimRmBO.getPkidExpClaimRm());
												this.newExpClaimRmBO
														.setFkStatusId(NewExpConstants.Claim_Awaiting_for_approval);
												newExpClaimRmRepository.save(this.newExpClaimRmBO);
												break;
											} else if (obj.getFkStatusId().equals(NewExpConstants.Exp_Forwarded)) {
												this.newExpClaimRmBO = new NewExpClaimRmBO();
												this.newExpClaimRmBO = newExpClaimRmRepository
														.findByPkidExpClaimRm(pkid);
												this.newExpClaimRmBO
														.setPkidExpClaimRm(this.newExpClaimRmBO.getPkidExpClaimRm());
												this.newExpClaimRmBO.setFkStatusId(NewExpConstants.Claim_Forwarded);
												newExpClaimRmRepository.save(this.newExpClaimRmBO);
												break;
											} else if (obj.getFkStatusId()
													.equals(NewExpConstants.Exp_Payment_Initiated))
												++this.paymentInitated;
										}

										if (this.paymentInitated.equals(this.newExpDatExpenseBOList.size())) {
											this.newExpClaimRmBO = new NewExpClaimRmBO();
											this.newExpClaimRmBO = newExpClaimRmRepository.findByPkidExpClaimRm(pkid);
											this.newExpClaimRmBO
													.setPkidExpClaimRm(this.newExpClaimRmBO.getPkidExpClaimRm());
											this.newExpClaimRmBO.setFkStatusId(NewExpConstants.Claim_Reimbursed);
											newExpClaimRmRepository.save(this.newExpClaimRmBO);
										} else if (this.paymentInitated > 0
												&& !this.paymentInitated.equals(this.newExpDatExpenseBOList.size())) {
											this.newExpClaimRmBO = new NewExpClaimRmBO();
											this.newExpClaimRmBO = newExpClaimRmRepository.findByPkidExpClaimRm(pkid);
											if (!this.newExpClaimRmBO.getFkStatusId()
													.equals(NewExpConstants.Claim_Awaiting_for_approval)
													&& !this.newExpClaimRmBO.getFkStatusId()
															.equals(NewExpConstants.Claim_Forwarded)) {
												this.newExpClaimRmBO
														.setPkidExpClaimRm(this.newExpClaimRmBO.getPkidExpClaimRm());
												this.newExpClaimRmBO
														.setFkStatusId(NewExpConstants.Claim_Reimbursed_Partially);
												newExpClaimRmRepository.save(this.newExpClaimRmBO);
											}
										} else {
											this.newExpClaimRmBO = new NewExpClaimRmBO();
											this.newExpClaimRmBO = newExpClaimRmRepository.findByPkidExpClaimRm(pkid);
											if (!this.newExpClaimRmBO.getFkStatusId()
													.equals(NewExpConstants.Claim_Awaiting_for_approval)
													&& !this.newExpClaimRmBO.getFkStatusId()
															.equals(NewExpConstants.Claim_Forwarded)) {
												this.newExpClaimRmBO
														.setPkidExpClaimRm(this.newExpClaimRmBO.getPkidExpClaimRm());
												this.newExpClaimRmBO
														.setFkStatusId(NewExpConstants.Claim_Rejected_by_Finance);
												newExpClaimRmRepository.save(this.newExpClaimRmBO);
											}
										}
									}

									// adding record history
									this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
									this.newExpDatExpenseBOList = newExpDatExpenseRepository
											.findByFkExpFinClaimRecordIdAndFkStatusIdOrderByPkIdNewExpDatExpenseDesc(
													newExpClaimFinBean.getPkIdExpClaimFin(),
													NewExpConstants.Exp_Reimbursed);
									this.newExpDatExpenseBOList.forEach(obj -> {
										this.createRecordHistory(obj.getPkIdNewExpDatExpense(),
												newExpClaimFinBean.getFinanceLogin(), NewExpConstants.Exp_Reimbursed,
												null);
									});
								} else
									throw new CommonCustomException(
											"Some error occured while updating status for expense");
							} else
								throw new CommonCustomException(
										"Some of the expense are not in payment initated/ Finance rejected status");
						} else
							throw new CommonCustomException("No expense mapped with the claim");
					} else
						throw new CommonCustomException(
								"Claim status should be payment initiated or payment initiated partially");
				} else
					throw new CommonCustomException("Given id does not exist");
			} else
				throw new CommonCustomException("some error occured for the expense while close reimbursement");
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		return this.viewConvertedBeanListFromBo(this.newExpClaimFinBO);
	}

	// Validation for finance action
	public Boolean validToFinanceActionForApproveExpense(NewExpClaimFinBean newExpClaimFinBean)
			throws CommonCustomException {
		this.countExpense = 0;
		try {
			if (Optional.ofNullable(newExpClaimFinBean.getPkIdExpClaimFin()).isPresent()) {
				if (Optional.ofNullable(newExpClaimFinBean.getFinanceLogin()).isPresent()) {
					DatEmpProfessionalDetailBO prop = datEmployeeProfessionalRepository
							.findByFkMainEmpDetailId(newExpClaimFinBean.getFinanceLogin());
					if (prop == null)
						throw new CommonCustomException("invalid employee id");
					else {
						List<Short> empRole = Arrays.asList(ExpenseConstants.FINANCE_HEAD,
								ExpenseConstants.FINANCE_MANAGER, ExpenseConstants.FINANCE_SENIOR_EXECUTIVE,
								ExpenseConstants.FINANCE_EXECUTIVE);
						List<DatEmpProfessionalDetailBO> finEmpId = datEmployeeProfessionalRepository
								.fetchListOfEmpFinExp(empRole);
						boolean validFinEmp = finEmpId.stream().anyMatch(
								obj -> obj.getFkMainEmpDetailId().equals(newExpClaimFinBean.getFinanceLogin()));
						if (validFinEmp) {
							this.newExpClaimFinBO = new NewExpClaimFinBO();
							this.newExpClaimFinBO = newExpClaimFinRepository
									.findByPkIdExpClaimFin(newExpClaimFinBean.getPkIdExpClaimFin());
							if (Optional.ofNullable(this.newExpClaimFinBO).isPresent()) {
								DatEmpProfessionalDetailBO dataObj = finEmpId
										.stream().filter(obj -> obj.getFkMainEmpDetailId()
												.intValue() == newExpClaimFinBean.getFinanceLogin().intValue())
										.findFirst().get();
								if (dataObj.getFkEmpOrganizationId()
										.equals(this.newExpClaimFinBO.getFinanceTeam().shortValue())) {
									this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
									this.newExpDatExpenseBOList = newExpDatExpenseRepository
											.findByFkExpFinClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(
													newExpClaimFinBean.getPkIdExpClaimFin());
									if (!this.newExpDatExpenseBOList.isEmpty()) {
										this.newExpDatExpenseBOList.forEach(obj -> {
											if (obj.getFkStatusId().equals(NewExpConstants.Exp_Payment_Initiated) || obj
													.getFkStatusId().equals(NewExpConstants.Exp_Rejected_By_Finance))
												++this.countExpense;
										});
										if (this.countExpense == this.newExpDatExpenseBOList.size())
											return true;
										else
											throw new CommonCustomException(
													"Some of the expense are not in Finance action status");
									} else
										throw new CommonCustomException("Expense list is empty");
								} else
									throw new CommonCustomException(
											"Taking Action for this report belongs to other finance team");
							} else
								throw new CommonCustomException("entered claim id is not available");
						} else
							throw new CommonCustomException("login is not finance employee");
					}
				} else
					throw new CommonCustomException("login is mandatory");
			} else
				throw new CommonCustomException("claim id is mandatory");
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		// return false;
	}

	public Boolean createRecordHistory(Integer expenseId, Integer empId, Integer statusId, Integer toId) {
		try {
			if (toId == null)
				this.newExpDatRecordHistoryBO = new NewExpDatRecordHistoryBO(expenseId, empId, statusId, new Date());
			if (toId != null)
				this.newExpDatRecordHistoryBO = new NewExpDatRecordHistoryBO(expenseId, empId, statusId, new Date(),
						toId);
			this.newExpDatRecordHistoryBO = newExpDatRecordHistoryRepository.save(this.newExpDatRecordHistoryBO);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
