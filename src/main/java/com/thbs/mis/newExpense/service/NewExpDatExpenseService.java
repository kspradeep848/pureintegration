package com.thbs.mis.newExpense.service;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.googlecode.jmapper.JMapper;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.DatForexConversionsBO;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.DatForexConversionsRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.expense.bo.ExpMileageRatesBO;
import com.thbs.mis.expense.constants.ExpenseConstants;
import com.thbs.mis.expense.dao.ExpMileageRatesRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.report.Util.DownloadFileUtil;
import com.thbs.mis.newExpense.bean.NewExpClaimFinBean;
import com.thbs.mis.newExpense.bean.NewExpClaimRmBean;
import com.thbs.mis.newExpense.bean.NewExpDatExpenseBean;
import com.thbs.mis.newExpense.bean.NewExpDatRecordHistoryBean;
import com.thbs.mis.newExpense.bo.NewExpClaimFinBO;
import com.thbs.mis.newExpense.bo.NewExpClaimRmBO;
import com.thbs.mis.newExpense.bo.NewExpDatExpenseBO;
import com.thbs.mis.newExpense.bo.NewExpDatRecordHistoryBO;
import com.thbs.mis.newExpense.bo.NewExpMasCategoryBO;
import com.thbs.mis.newExpense.constants.NewExpConstants;
import com.thbs.mis.newExpense.dao.NewExpClaimFinRepository;
import com.thbs.mis.newExpense.dao.NewExpClaimRmRepository;
import com.thbs.mis.newExpense.dao.NewExpDatExpenseRepository;
import com.thbs.mis.newExpense.dao.NewExpDatRecordHistoryRepository;
import com.thbs.mis.newExpense.dao.NewExpMasCategoryRepository;

@Service
public class NewExpDatExpenseService {

	private static final AppLog LOG = LogFactory.getLog(NewExpDatExpenseService.class);

	JMapper<NewExpDatExpenseBean, NewExpDatExpenseBO> newExpDatExpenseBeanMapper = new JMapper<>(
			NewExpDatExpenseBean.class, NewExpDatExpenseBO.class);

	List<NewExpDatExpenseBean> newExpDatExpenseBeanList;

	List<NewExpDatExpenseBO> newExpDatExpenseBOList;

	NewExpDatExpenseBO newExpDatExpenseBO;

	NewExpDatExpenseBean newExpDatExpenseBean;

	NewExpDatRecordHistoryBO newExpDatRecordHistoryBO;

	NewExpDatRecordHistoryBean newExpDatRecordHistoryBean;

	NewExpClaimRmBO newExpClaimRmBO;

	List<NewExpClaimRmBO> newExpClaimRmBOList;

	NewExpClaimRmBean newExpClaimRmBean;

	List<NewExpClaimRmBean> newExpClaimRmBeanList;

	NewExpClaimFinBO newExpClaimFinBO;

	List<NewExpClaimFinBO> newExpClaimFinBOList;

	@Autowired
	NewExpDatExpenseRepository newExpDatExpenseRepository;

	@Autowired
	DatEmployeeProfessionalRepository datEmployeeProfessionalRepository;

	@Autowired
	private DatForexConversionsRepository datForexConversionsRepository;

	@Autowired
	NewExpMasCategoryRepository newExpMasCategoryRepository;

	@Autowired
	NewExpDatRecordHistoryRepository newExpDatRecordHistoryRepository;

	@Autowired
	NewExpClaimRmRepository newExpClaimRmRepository;

	@Autowired
	NewExpClaimFinRepository newExpClaimFinRepository;

	@Autowired
	ExpMileageRatesRepository expMileageRatesRepository;

	@Autowired
	EmployeePersonalDetailsRepository employeePersonalDetailRepo;

	@Value("${expense.upload.directory}")
	private String EXPENSE_RECEIPT_UPLOAD_DIRECTORY;

	Integer countExpense = 0;

	Integer approvedCount = 0;

	Integer rejecteCount = 0;

	Integer forwardCount = 0;

	// Get Employee Name
	public String getEmployeeName(Integer empId) {
		DatEmpPersonalDetailBO empData = employeePersonalDetailRepo.findByFkEmpDetailId(empId);
		return empData.getEmpFirstName() + " " + empData.getEmpLastName();
	}

	// create expense code starts
	Double totalMilesTravelled = 0.0;

	Double mileageAmount = 0.0;

	Double maxMileageLimit = 10000.00;

	Short UK_CURRENCY = 3;

	public NewExpDatExpenseBO createExpense(NewExpDatExpenseBean newExpDatExpenseBean, MultipartFile receiptFile)
			throws IOException, CommonCustomException {
		try {
			// getting professional details
			DatEmpProfessionalDetailBO prop = datEmployeeProfessionalRepository
					.findByFkMainEmpDetailId(newExpDatExpenseBean.getFkEmpid());
			if (prop == null)
				throw new CommonCustomException("invalid employee id");
			else {
				this.newExpDatExpenseBO = new NewExpDatExpenseBO();

				// validate input bean
				this.validateInputExpenseBean(newExpDatExpenseBean);

				// setting values to BO based on category
				this.newExpDatExpenseBO = assignValuesBasedOnCategory(newExpDatExpenseBean);

				// setting common values to BO
				this.newExpDatExpenseBO.setFkEmpid(newExpDatExpenseBean.getFkEmpid());
				this.newExpDatExpenseBO.setFkCategoryTypeId(newExpDatExpenseBean.getFkCategoryTypeId());
				this.newExpDatExpenseBO.setExpCreatedOn(new Date());
				this.newExpDatExpenseBO.setDescription(newExpDatExpenseBean.getDescription());
				this.newExpDatExpenseBO.setFkRmEmpId(newExpDatExpenseBean.getFkRmEmpId());
				this.newExpDatExpenseBO.setFkBuId(newExpDatExpenseBean.getFkBuId());
				this.newExpDatExpenseBO.setFkStatusId(NewExpConstants.Exp_Awaititng_for_approval);
				this.newExpDatExpenseBO.setFkClientId(newExpDatExpenseBean.getFkClientId());
				this.newExpDatExpenseBO.setFkProjectId(newExpDatExpenseBean.getFkProjectId());
				this.newExpDatExpenseBO.setFkWorkorderId(newExpDatExpenseBean.getFkWorkorderId());
				this.newExpDatExpenseBO.setBilllableToClient(newExpDatExpenseBean.getBilllableToClient());
				this.newExpDatExpenseBO.setFkEmpOrg(prop.getFkEmpOrganizationId());
				this.newExpDatExpenseBO = newExpDatExpenseRepository.save(this.newExpDatExpenseBO);

				// Record history
				Boolean historyCreated = this.createRecordHistory(this.newExpDatExpenseBO.getPkIdNewExpDatExpense(),
						this.newExpDatExpenseBO.getFkEmpid(), this.newExpDatExpenseBO.getFkStatusId(),
						this.newExpDatExpenseBO.getFkRmEmpId());
				if (!historyCreated)
					throw new CommonCustomException("Some problem occured while expense history capturing");

				// file upload part
				NewExpMasCategoryBO bo = newExpMasCategoryRepository
						.findByPkIdNewExpMasCategory(newExpDatExpenseBean.getFkCategoryTypeId());
				if (bo.getFileUploadMandatory().equalsIgnoreCase("YES")) {
					String localFileName = receiptFile.getOriginalFilename().replaceAll("\\s+", "");
					String fileName1 = "EXP@" + newExpDatExpenseBean.getFkEmpid() + "@"
							+ this.newExpDatExpenseBO.getPkIdNewExpDatExpense() + "^" + localFileName;
					String name = fileName1;
					this.newExpDatExpenseBO.setPathname(name);
					this.newExpDatExpenseBO.setFileName(localFileName);
					this.newExpDatExpenseBO.setPkIdNewExpDatExpense(this.newExpDatExpenseBO.getPkIdNewExpDatExpense());
					this.newExpDatExpenseBO = newExpDatExpenseRepository.save(this.newExpDatExpenseBO);
					boolean result = this.upload(this.newExpDatExpenseBO.getFkEmpid(),
							this.newExpDatExpenseBO.getPkIdNewExpDatExpense(), receiptFile);
					if (result == false)
						throw new CommonCustomException(
								"expense data saved but upload failed, Please contact administrator.");
				} else {
					if (receiptFile != null) {
						String localFileName = receiptFile.getOriginalFilename().replaceAll("\\s+", "");
						String fileName1 = "EXP@" + newExpDatExpenseBean.getFkEmpid() + "@"
								+ this.newExpDatExpenseBO.getPkIdNewExpDatExpense() + "^" + localFileName;
						String name = fileName1;
						this.newExpDatExpenseBO.setPathname(name);
						this.newExpDatExpenseBO.setFileName(localFileName);
						this.newExpDatExpenseBO
								.setPkIdNewExpDatExpense(this.newExpDatExpenseBO.getPkIdNewExpDatExpense());
						this.newExpDatExpenseBO = newExpDatExpenseRepository.save(this.newExpDatExpenseBO);
						boolean result = this.upload(this.newExpDatExpenseBO.getFkEmpid(),
								this.newExpDatExpenseBO.getPkIdNewExpDatExpense(), receiptFile);
						if (result == false)
							throw new CommonCustomException(
									"expense data saved but upload failed.Please contact nucleus team");
					} else {
						this.newExpDatExpenseBO.setPathname(null);
						this.newExpDatExpenseBO.setFileName(null);
					}
				}

				// bundling expense into RM claims
				Boolean rmRecordIdCheck = this.addingExpenseIntoBundle(this.newExpDatExpenseBO);
				if (!rmRecordIdCheck)
					throw new CommonCustomException("some problem occured while expense bundling");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		return this.newExpDatExpenseBO;
	}

	public NewExpDatExpenseBO assignValuesBasedOnCategory(NewExpDatExpenseBean newExpDatExpenseBean)
			throws CommonCustomException {
		Double amount = 0.0;
		Double isTaxAmount = 0.0;
		Double expenseAmount = 0.0;
		switch (newExpDatExpenseBean.getFkCategoryTypeId()) {
		case 1: {
			// Travel
			this.newExpDatExpenseBO.setModeOfTransport(newExpDatExpenseBean.getModeOfTransport());
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setExpEndDate(newExpDatExpenseBean.getExpEndDate());
			this.newExpDatExpenseBO.setExpFromPlace(newExpDatExpenseBean.getExpFromPlace());
			this.newExpDatExpenseBO.setExpToPlace(newExpDatExpenseBean.getExpToPlace());
			if (newExpDatExpenseBean.getTicketNumber() != null)
				this.newExpDatExpenseBO.setTicketNumber(newExpDatExpenseBean.getTicketNumber());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				if (Optional.ofNullable(newExpDatExpenseBean.getTaxAmount()).isPresent()) {
					this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
					isTaxAmount = newExpDatExpenseBean.getTaxAmount();
					amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
				} else
					throw new CommonCustomException("Tax amount is mandatory");
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				this.newExpDatExpenseBO.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		case 2: {
			// Lodging
			this.newExpDatExpenseBO.setVendor(newExpDatExpenseBean.getVendor());
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setExpEndDate(newExpDatExpenseBean.getExpEndDate());
			this.newExpDatExpenseBO.setNoOfNights(newExpDatExpenseBean.getNoOfNights());
			this.newExpDatExpenseBO.setNoOfAttendees(newExpDatExpenseBean.getNoOfAttendees());
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				if (Optional.ofNullable(newExpDatExpenseBean.getTaxAmount()).isPresent()) {
					this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
					isTaxAmount = newExpDatExpenseBean.getTaxAmount();
					amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
				} else
					throw new CommonCustomException("Tax amount is mandatory");
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				this.newExpDatExpenseBO.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		case 3: {
			// Meals and Entertainment
			this.newExpDatExpenseBO.setVendor(newExpDatExpenseBean.getVendor());
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setNoOfAttendees(newExpDatExpenseBean.getNoOfAttendees());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				if (Optional.ofNullable(newExpDatExpenseBean.getTaxAmount()).isPresent()) {
					this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
					isTaxAmount = newExpDatExpenseBean.getTaxAmount();
					amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
				} else
					throw new CommonCustomException("Tax amount is mandatory");
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				this.newExpDatExpenseBO.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		case 4: {
			// Office supply
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				if (Optional.ofNullable(newExpDatExpenseBean.getTaxAmount()).isPresent()) {
					this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
					isTaxAmount = newExpDatExpenseBean.getTaxAmount();
					amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
				} else
					throw new CommonCustomException("Tax amount is mandatory");
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				this.newExpDatExpenseBO.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		case 5: {
			// Phone
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setPhoneAndIt(newExpDatExpenseBean.getPhoneAndIt());
			this.newExpDatExpenseBO.setDuration(newExpDatExpenseBean.getDuration());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				if (Optional.ofNullable(newExpDatExpenseBean.getTaxAmount()).isPresent()) {
					this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
					isTaxAmount = newExpDatExpenseBean.getTaxAmount();
					amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
				} else
					throw new CommonCustomException("Tax amount is mandatory");
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				this.newExpDatExpenseBO.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		case 6: {
			// IT and Internet
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setPhoneAndIt(newExpDatExpenseBean.getPhoneAndIt());
			this.newExpDatExpenseBO.setDuration(newExpDatExpenseBean.getDuration());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				if (Optional.ofNullable(newExpDatExpenseBean.getTaxAmount()).isPresent()) {
					this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
					isTaxAmount = newExpDatExpenseBean.getTaxAmount();
					amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
				} else
					throw new CommonCustomException("Tax amount is mandatory");
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				this.newExpDatExpenseBO.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		case 7: {
			// Mileage
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setFkMileageId(newExpDatExpenseBean.getFkMileageId());
			this.newExpDatExpenseBO.setMiles(newExpDatExpenseBean.getMiles());
			Double mileageAmount = mileageCalculation(newExpDatExpenseBean);
			this.newExpDatExpenseBO.setExpAmount(mileageAmount);
			this.newExpDatExpenseBO.setExpCurrencyTypeId(UK_CURRENCY);
			this.newExpDatExpenseBO.setExpTotalAmount(mileageAmount);
			break;
		}
		case 8: {
			// Parking
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setDuration(newExpDatExpenseBean.getDuration());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				if (Optional.ofNullable(newExpDatExpenseBean.getTaxAmount()).isPresent()) {
					this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
					isTaxAmount = newExpDatExpenseBean.getTaxAmount();
					amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
				} else
					throw new CommonCustomException("Tax amount is mandatory");
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				this.newExpDatExpenseBO.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		case 9: {
			// Others
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				if (Optional.ofNullable(newExpDatExpenseBean.getTaxAmount()).isPresent()) {
					this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
					isTaxAmount = newExpDatExpenseBean.getTaxAmount();
					amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
				} else
					throw new CommonCustomException("Tax amount is mandatory");
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				this.newExpDatExpenseBO.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		default:
			throw new CommonCustomException("Enter category within the max value");
		}
		return newExpDatExpenseBO;
	}

	public boolean upload(Integer empId, Integer expenseId, MultipartFile file)
			throws IOException, CommonCustomException {
		boolean result = false;
		try {
			String localFileName = file.getOriginalFilename().replaceAll("\\s+", "");
			String fileName = "EXP@" + empId + "@" + expenseId + "^" + localFileName;
			// File directory = new File(EXPENSE_RECEIPT_UPLOAD_DIRECTORY);
			// if(directory.exists() == false)
			// directory.mkdir();
			File saveFile = new File(EXPENSE_RECEIPT_UPLOAD_DIRECTORY, fileName);
			FileUtils.writeByteArrayToFile(saveFile, file.getBytes());
			result = true;
		} catch (Exception e) {
			result = false;
			throw new CommonCustomException("" + file.getOriginalFilename() + " saved failed");
		}
		return result;
	}

	public void validateInputExpenseBean(NewExpDatExpenseBean inputBean) throws CommonCustomException, IOException {

		if (inputBean.getFkCategoryTypeId().equals(1)) {
			if (Optional.ofNullable(inputBean.getModeOfTransport()).isPresent() == false)
				throw new CommonCustomException("Expense mode of transport is Manditory");
			if (Optional.ofNullable(inputBean.getExpFromPlace()).isPresent() == false)
				throw new CommonCustomException("Expense From place is Manditory");
			if (Optional.ofNullable(inputBean.getExpToPlace()).isPresent() == false)
				throw new CommonCustomException("Expense To place is Manditory");
			if (Optional.ofNullable(inputBean.getExpStartDate()).isPresent() == false)
				throw new CommonCustomException("Expense Start Date is Manditory");
			if (Optional.ofNullable(inputBean.getExpEndDate()).isPresent() == false)
				throw new CommonCustomException("Expense End Date is Manditory");
		} else if (inputBean.getFkCategoryTypeId().equals(2)) {
			if (Optional.ofNullable(inputBean.getExpStartDate()).isPresent() == false)
				throw new CommonCustomException("Expense Start Date is Manditory");
			if (Optional.ofNullable(inputBean.getExpEndDate()).isPresent() == false)
				throw new CommonCustomException("Expense End Date is Manditory");
			if (Optional.ofNullable(inputBean.getNoOfNights()).isPresent() == false)
				throw new CommonCustomException("No of nights is Manditory");
			if (Optional.ofNullable(inputBean.getNoOfAttendees()).isPresent() == false)
				throw new CommonCustomException("No of occupants is Manditory");
		} else if (inputBean.getFkCategoryTypeId().equals(3)) {
			if (Optional.ofNullable(inputBean.getExpStartDate()).isPresent() == false)
				throw new CommonCustomException("Expense Date is Manditory");
			if (Optional.ofNullable(inputBean.getNoOfAttendees()).isPresent() == false)
				throw new CommonCustomException("No of attendees is Manditory");
		} else if (inputBean.getFkCategoryTypeId().equals(4)) {
			if (Optional.ofNullable(inputBean.getExpStartDate()).isPresent() == false)
				throw new CommonCustomException("Expense Date is Manditory");
		} else if (inputBean.getFkCategoryTypeId().equals(5)) {
			if (Optional.ofNullable(inputBean.getPhoneAndIt()).isPresent() == false)
				throw new CommonCustomException("Landline or moblie selection is Manditory");
			if (Optional.ofNullable(inputBean.getDuration()).isPresent() == false)
				throw new CommonCustomException("Duration is Manditory");
			if (Optional.ofNullable(inputBean.getExpStartDate()).isPresent() == false)
				throw new CommonCustomException("Expense Date is Manditory");
		} else if (inputBean.getFkCategoryTypeId().equals(6)) {
			if (Optional.ofNullable(inputBean.getPhoneAndIt()).isPresent() == false)
				throw new CommonCustomException("Broadband or moblie selection is Manditory");
			if (Optional.ofNullable(inputBean.getDuration()).isPresent() == false)
				throw new CommonCustomException("Duration is Manditory");
			if (Optional.ofNullable(inputBean.getExpStartDate()).isPresent() == false)
				throw new CommonCustomException("Expense Date is Manditory");
		} else if (inputBean.getFkCategoryTypeId().equals(7)) {
			if (Optional.ofNullable(inputBean.getFkMileageId()).isPresent() == false)
				throw new CommonCustomException("Mile Travel mode selection is Manditory");
			if (Optional.ofNullable(inputBean.getMiles()).isPresent() == false)
				throw new CommonCustomException("Miles is Manditory");
			if (Optional.ofNullable(inputBean.getExpStartDate()).isPresent() == false)
				throw new CommonCustomException("Expense Date is Manditory");
		} else if (inputBean.getFkCategoryTypeId().equals(8)) {
			if (Optional.ofNullable(inputBean.getDuration()).isPresent() == false)
				throw new CommonCustomException("Duration is Manditory");
			if (Optional.ofNullable(inputBean.getExpStartDate()).isPresent() == false)
				throw new CommonCustomException("Expense Date is Manditory");
		} else if (inputBean.getFkCategoryTypeId().equals(9)) {
			if (Optional.ofNullable(inputBean.getExpStartDate()).isPresent() == false)
				throw new CommonCustomException("Expense Date is Manditory");
		} else {
			throw new CommonCustomException("Give proper category id");
		}

		if (Optional.ofNullable(inputBean.getFkEmpid()).isPresent() == false)
			throw new CommonCustomException("EmpId is Manditory");
		if (Optional.ofNullable(inputBean.getDescription()).isPresent() == false)
			throw new CommonCustomException("Description is Manditory");
		if (Optional.ofNullable(inputBean.getFkStatusId()).isPresent() == false)
			throw new CommonCustomException("Status is Manditory");
		if (Optional.ofNullable(inputBean.getFkRmEmpId()).isPresent() == false)
			throw new CommonCustomException("Manager ID is Manditory");
		if (Optional.ofNullable(inputBean.getExpCurrencyTypeId()).isPresent() == false)
			throw new CommonCustomException("Expense Currency type is Manditory");
		if (Optional.ofNullable(inputBean.getExpAmount()).isPresent() == false)
			throw new CommonCustomException("Expense Amount is Manditory");
		if (Optional.ofNullable(inputBean.getBilllableToClient()).isPresent() == false)
			throw new CommonCustomException("Expense Billable is Manditory");
		if (Optional.ofNullable(inputBean.getIsBillTaxInclusive()).isPresent() == false)
			throw new CommonCustomException("Expense Tax inclusive is Manditory");
		if (Optional.ofNullable(inputBean.getFkBuId()).isPresent() == false)
			throw new CommonCustomException("Expense BU id is Manditory");
		if (Optional.ofNullable(inputBean.getFkClientId()).isPresent() == false)
			throw new CommonCustomException("Expense Client id is Manditory");
		if (Optional.ofNullable(inputBean.getFkProjectId()).isPresent() == false)
			throw new CommonCustomException("Expense Project id is Manditory");
		if (Optional.ofNullable(inputBean.getFkWorkorderId()).isPresent() == false)
			throw new CommonCustomException("Expense WorkOrder is Manditory");

	}

	public Boolean createRecordHistory(Integer expenseId, Integer empId, Integer statusId, Integer toId) {
		try {
			if (toId == null)
				this.newExpDatRecordHistoryBO = new NewExpDatRecordHistoryBO(expenseId, empId, statusId, new Date());
			if (toId != null)
				this.newExpDatRecordHistoryBO = new NewExpDatRecordHistoryBO(expenseId, empId, statusId, new Date(),
						toId);
			this.newExpDatRecordHistoryBO = newExpDatRecordHistoryRepository.save(this.newExpDatRecordHistoryBO);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public Boolean addingExpenseIntoBundle(NewExpDatExpenseBO inputBo) throws CommonCustomException {
		try {
			this.newExpClaimRmBO = new NewExpClaimRmBO();
			this.newExpDatExpenseBO = new NewExpDatExpenseBO();
			this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
			this.newExpClaimRmBO = newExpClaimRmRepository.findByFkEmpIdAndFkRmIdAndFkStatusId(inputBo.getFkEmpid(),
					inputBo.getFkRmEmpId(), inputBo.getFkStatusId());
			// create record
			if (this.newExpClaimRmBO == null) {
				if (inputBo.getExpEndDate() != null)
					this.newExpClaimRmBO = new NewExpClaimRmBO(inputBo.getFkEmpid(), inputBo.getFkRmEmpId(),
							inputBo.getExpStartDate(), inputBo.getExpEndDate(), inputBo.getFkStatusId(), new Date());
				else
					this.newExpClaimRmBO = new NewExpClaimRmBO(inputBo.getFkEmpid(), inputBo.getFkRmEmpId(),
							inputBo.getExpStartDate(), inputBo.getExpStartDate(), inputBo.getFkStatusId(), new Date());
				this.newExpClaimRmBO = newExpClaimRmRepository.save(this.newExpClaimRmBO);
				// adding record id to expense
				this.newExpDatExpenseBO = newExpDatExpenseRepository
						.findByPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
				this.newExpDatExpenseBO.setPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
				this.newExpDatExpenseBO.setFkExpRmClaimRecordId(this.newExpClaimRmBO.getPkidExpClaimRm());
				newExpDatExpenseRepository.save(this.newExpDatExpenseBO);
			}
			// update record
			else {
				this.newExpDatExpenseBOList = newExpDatExpenseRepository
						.findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(
								this.newExpClaimRmBO.getPkidExpClaimRm());
				List<Date> listOfDates = new ArrayList<>();
				this.newExpDatExpenseBOList.stream().forEach(obj -> {
					if (obj.getExpStartDate() != null)
						listOfDates.add(obj.getExpStartDate());
					if (obj.getExpEndDate() != null)
						listOfDates.add(obj.getExpEndDate());
					if (inputBo.getExpStartDate() != null)
						listOfDates.add(inputBo.getExpStartDate());
					if (inputBo.getExpEndDate() != null)
						listOfDates.add(inputBo.getExpEndDate());
				});
				Collections.sort(listOfDates);
				Date firstDate = listOfDates.get(0);
				Date lastDate = listOfDates.get(listOfDates.size() - 1);
				this.newExpClaimRmBO = new NewExpClaimRmBO(this.newExpClaimRmBO.getPkidExpClaimRm(),
						inputBo.getFkEmpid(), inputBo.getFkRmEmpId(), firstDate, lastDate, inputBo.getFkStatusId(),
						this.newExpClaimRmBO.getClaimCreatedDate());
				this.newExpClaimRmBO = newExpClaimRmRepository.save(this.newExpClaimRmBO);
				// adding record id to expense
				this.newExpDatExpenseBO = newExpDatExpenseRepository
						.findByPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
				this.newExpDatExpenseBO.setPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
				this.newExpDatExpenseBO.setFkExpRmClaimRecordId(this.newExpClaimRmBO.getPkidExpClaimRm());
				newExpDatExpenseRepository.save(this.newExpDatExpenseBO);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@SuppressWarnings("deprecation")
	public Double mileageCalculation(NewExpDatExpenseBean inputBean) {
		this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
		this.newExpDatExpenseBOList = newExpDatExpenseRepository.findByFkEmpidAndFkCategoryTypeIdAndFkMileageId(
				inputBean.getFkCategoryTypeId(), inputBean.getFkEmpid(), inputBean.getFkMileageId());
		this.newExpDatExpenseBOList.stream()
				.filter(obj -> inputBean.getExpStartDate().getYear() == obj.getExpStartDate().getYear());
		ExpMileageRatesBO bo = expMileageRatesRepository.findByPkMileageId(inputBean.getFkMileageId());
		if (this.newExpDatExpenseBOList != null && this.newExpDatExpenseBOList.size() > 0) {
			this.newExpDatExpenseBOList.forEach(obj -> {
				totalMilesTravelled += obj.getMiles();
			});
			if (totalMilesTravelled + inputBean.getMiles() <= maxMileageLimit) {
				double firstmileageAmount = bo.getFirst10000Miles();
				return mileageAmount = inputBean.getMiles() * firstmileageAmount;
			} else {
				double remainingMiles = totalMilesTravelled + inputBean.getMiles() - maxMileageLimit;
				double previousMiles = inputBean.getMiles() - remainingMiles;
				double firstmileageAmount = bo.getFirst10000Miles();
				double secondmileageAmount = bo.getAbove10000Miles();
				return mileageAmount = previousMiles * firstmileageAmount + remainingMiles * secondmileageAmount;
			}
		} else {
			double firstmileageAmount = bo.getFirst10000Miles();
			return mileageAmount = inputBean.getMiles() * firstmileageAmount;
		}
	}

	// create expense code ends
	// update expense code starts
	public NewExpDatExpenseBO updateExpense(NewExpDatExpenseBean newExpDatExpenseBean)
			throws IOException, CommonCustomException {
		if (newExpDatExpenseBean.getPkIdNewExpDatExpense() != null) {
			this.newExpDatExpenseBO = new NewExpDatExpenseBO();
			// check with DB
			this.newExpDatExpenseBO = newExpDatExpenseRepository
					.findByPkIdNewExpDatExpense(newExpDatExpenseBean.getPkIdNewExpDatExpense());
			if (this.newExpDatExpenseBO != null) {
				// getting professional details
				DatEmpProfessionalDetailBO prop = datEmployeeProfessionalRepository
						.findByFkMainEmpDetailId(newExpDatExpenseBean.getFkEmpid());
				if (prop == null)
					throw new CommonCustomException("invalid employee id");
				else {

					// validate input bean
					this.validateInputExpenseBean(newExpDatExpenseBean);

					// setting values to BO based on category
					this.newExpDatExpenseBO = assignValuesBasedOnCategory(newExpDatExpenseBean);

					// setting common values to BO
					this.newExpDatExpenseBO.setPkIdNewExpDatExpense(newExpDatExpenseBean.getPkIdNewExpDatExpense());
					this.newExpDatExpenseBO.setFkEmpid(newExpDatExpenseBean.getFkEmpid());
					this.newExpDatExpenseBO.setFkCategoryTypeId(newExpDatExpenseBean.getFkCategoryTypeId());
					this.newExpDatExpenseBO.setExpCreatedOn(this.newExpDatExpenseBO.getExpCreatedOn());
					this.newExpDatExpenseBO.setExpModifiedOn(new Date());
					this.newExpDatExpenseBO.setDescription(newExpDatExpenseBean.getDescription());
					this.newExpDatExpenseBO.setFkRmEmpId(newExpDatExpenseBean.getFkRmEmpId());
					this.newExpDatExpenseBO.setFkBuId(newExpDatExpenseBean.getFkBuId());
					this.newExpDatExpenseBO.setFkStatusId(NewExpConstants.Exp_Awaititng_for_approval);
					this.newExpDatExpenseBO.setFkClientId(newExpDatExpenseBean.getFkClientId());
					this.newExpDatExpenseBO.setFkProjectId(newExpDatExpenseBean.getFkProjectId());
					this.newExpDatExpenseBO.setFkWorkorderId(newExpDatExpenseBean.getFkWorkorderId());
					this.newExpDatExpenseBO.setBilllableToClient(newExpDatExpenseBean.getBilllableToClient());
					this.newExpDatExpenseBO.setFkEmpOrg(prop.getFkEmpOrganizationId());
					this.newExpDatExpenseBO = newExpDatExpenseRepository.save(this.newExpDatExpenseBO);

					// Record history
					// Boolean historyCreated =
					// this.createRecordHistory(this.newExpDatExpenseBO.getPkIdNewExpDatExpense(),
					// this.newExpDatExpenseBO.getFkEmpid(),
					// this.newExpDatExpenseBO.getFkStatusId(),
					// this.newExpDatExpenseBO.getFkRmEmpId());
					// if (!historyCreated)
					// throw new CommonCustomException("Some problem occured while expense history
					// capturing");

					// bundling expense into RM claims
					Boolean rmRecordIdCheck = this.addingExpenseIntoBundle(this.newExpDatExpenseBO);
					if (!rmRecordIdCheck)
						throw new CommonCustomException("some problem occured while expense bundling");
				}
			} else
				throw new CommonCustomException("Expense id doesn't available");
		} else
			throw new CommonCustomException("Expense id is mandatory for update");
		return newExpDatExpenseBO;
	}
	// update expense code ends

	// update only amount
	public NewExpDatExpenseBO updateAmountExpense(NewExpDatExpenseBean bean) throws CommonCustomException {
		this.newExpDatExpenseBO = new NewExpDatExpenseBO();
		if (Optional.ofNullable(bean.getPkIdNewExpDatExpense()).isPresent()
				&& Optional.ofNullable(bean.getFkEmpid()).isPresent()) {
			this.newExpDatExpenseBO = newExpDatExpenseRepository
					.findByPkIdNewExpDatExpense(bean.getPkIdNewExpDatExpense());
			if (Optional.ofNullable(this.newExpDatExpenseBO).isPresent()) {
				if (this.newExpDatExpenseBO.getFkEmpid().equals(bean.getFkEmpid())) {
					if (this.newExpDatExpenseBO.getFkStatusId().equals(NewExpConstants.Exp_Awaititng_for_approval)) {
						if (!this.newExpDatExpenseBO.getFkCategoryTypeId().equals(7)) {
							if (Optional.ofNullable(bean.getExpTotalAmount()).isPresent()
									&& Optional.ofNullable(bean.getExpAmount()).isPresent()
									&& Optional.ofNullable(bean.getIsBillTaxInclusive()).isPresent()) {
								this.newExpDatExpenseBO
										.setPkIdNewExpDatExpense(this.newExpDatExpenseBO.getPkIdNewExpDatExpense());
								this.newExpDatExpenseBO.setExpTotalAmount(bean.getExpTotalAmount());
								if (bean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
									if (Optional.ofNullable(bean.getTaxAmount()).isPresent()) {
										this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
										this.newExpDatExpenseBO.setTaxAmount(bean.getTaxAmount());
									} else
										throw new CommonCustomException("Tax amount is mandatory");
								} else {
									this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
									this.newExpDatExpenseBO.setTaxAmount(null);
								}
								Double totalAmount = bean.getTaxAmount() + bean.getExpAmount();
								this.newExpDatExpenseBO.setExpAmount(totalAmount);
								this.newExpDatExpenseBO.setExpModifiedOn(new Date());
								newExpDatExpenseRepository.save(this.newExpDatExpenseBO);
							} else
								throw new CommonCustomException("amount and tax inclusive are mandatory");
						} else
							throw new CommonCustomException("For mileage category no update");
					} else
						throw new CommonCustomException("Expense is not in awaiting for approval");
				} else
					throw new CommonCustomException("Employee mis-match");
			} else
				throw new CommonCustomException("Id does not exist");
		} else
			throw new CommonCustomException("Expense id and Emp id is mandatory");
		return this.newExpDatExpenseBO;
	}

	// view expense details by id
	public List<NewExpDatExpenseBean> viewExpensebyId(Integer id) throws CommonCustomException {
		this.newExpDatExpenseBO = new NewExpDatExpenseBO();
		this.newExpDatExpenseBeanList = new ArrayList<NewExpDatExpenseBean>();
		this.newExpDatExpenseBO = newExpDatExpenseRepository.findByPkIdNewExpDatExpense(id);
		if (Optional.ofNullable(this.newExpDatExpenseBO).isPresent())
			return viewConvertedBeanListFromBo(this.newExpDatExpenseBO);
		else
			throw new CommonCustomException("Entered Id doesn't exist");
	}

	// view all expense details by empId
	public List<NewExpDatExpenseBean> viewAllExpenseByEmpId(Integer empId) throws CommonCustomException {
		this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
		this.newExpDatExpenseBeanList = new ArrayList<NewExpDatExpenseBean>();
		DatEmpProfessionalDetailBO bo = datEmployeeProfessionalRepository.findByFkMainEmpDetailId(empId);
		if (Optional.ofNullable(bo).isPresent()) {
			try {
				this.newExpDatExpenseBOList = newExpDatExpenseRepository.findByFkEmpid(empId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new CommonCustomException(e.getMessage());
			}
		} else
			throw new CommonCustomException("Employee is not available");
		if (!this.newExpDatExpenseBOList.isEmpty())
			return viewConvertedBeanListFromBoList(this.newExpDatExpenseBOList);
		else
			return this.newExpDatExpenseBeanList;
	}

	// Accept expense by RM
	public List<NewExpDatExpenseBean> acceptExpenseByRM(NewExpClaimRmBean newExpClaimRmBean)
			throws CommonCustomException {
		try {
			if (Optional.ofNullable(newExpClaimRmBean.getFinanceTeam()).isPresent()) {
				if (newExpClaimRmBean.getFinanceTeam().intValue() > 0
						&& newExpClaimRmBean.getFinanceTeam().intValue() <= 3) {
					Boolean check = this.validToRMAction(newExpClaimRmBean);
					if (check) {
						this.newExpClaimRmBeanList = new ArrayList<NewExpClaimRmBean>();
						this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
						this.newExpDatExpenseBO = new NewExpDatExpenseBO();
						Integer id = newExpDatExpenseRepository.approveExpenses(newExpClaimRmBean.getActionList(),
								NewExpConstants.Exp_Approved_by_RM, newExpClaimRmBean.getFinanceTeam());
						if (id == 0 || !id.equals(newExpClaimRmBean.getActionList().size()))
							throw new CommonCustomException("some error occured while performing approval");

						// Adding Currency convertion based on the emp org
						this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
						this.newExpDatExpenseBOList = newExpDatExpenseRepository
								.findExpenseByListOfId(newExpClaimRmBean.getActionList());
						for (NewExpDatExpenseBO bo : this.newExpDatExpenseBOList) {
							Boolean checkCurrency = this.addCurrencyConvertion(bo);
							if (!checkCurrency)
								throw new CommonCustomException("error occured while currency convertion");
						}

						this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
						this.newExpDatExpenseBOList = newExpDatExpenseRepository
								.findExpenseByListOfId(newExpClaimRmBean.getActionList());
						// Adding into Finance bundle
						for (NewExpDatExpenseBO bo : this.newExpDatExpenseBOList) {
							Boolean finCheck = this.addingExpenseIntoFinanceBundle(bo);
							if (!finCheck)
								throw new CommonCustomException("Error occured while bundling to finance");
						}

						// creating History
						this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
						this.newExpDatExpenseBOList = newExpDatExpenseRepository
								.findExpenseByListOfId(newExpClaimRmBean.getActionList());
						this.newExpDatExpenseBOList.forEach(obj -> {
							this.createRecordHistory(obj.getPkIdNewExpDatExpense(), obj.getFkRmEmpId(),
									NewExpConstants.Exp_Approved_by_RM, null);
							this.changeRMClaimStatus(obj);
						});
					}
				} else
					throw new CommonCustomException("We have only Indian, UK and UAE Finance Team");
			} else
				throw new CommonCustomException("Finance team selection is mandatory");
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		return viewConvertedBeanListFromBoList(this.newExpDatExpenseBOList);
	}

	// Reject expense by RM
	public List<NewExpDatExpenseBean> rejectExpenseByRM(NewExpClaimRmBean newExpClaimRmBean)
			throws CommonCustomException {
		try {
			if (Optional.ofNullable(newExpClaimRmBean.getRmComments()).isPresent()) {
				Boolean check = this.validToRMActionToReject(newExpClaimRmBean);
				if (check) {
					this.newExpClaimRmBeanList = new ArrayList<NewExpClaimRmBean>();
					this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
					this.newExpDatExpenseBOList = newExpDatExpenseRepository
							.findExpenseByListOfId(newExpClaimRmBean.getActionList());
					// Rejecting the approved expense
					this.newExpDatExpenseBOList.forEach(obj -> {
						if (obj.getFkStatusId().equals(NewExpConstants.Exp_Approved_by_RM)) {
							this.checkRejection(obj);
						}
					});

					Integer id = newExpDatExpenseRepository.rejectExpenses(newExpClaimRmBean.getActionList(),
							NewExpConstants.Exp_Rejected_by_RM, newExpClaimRmBean.getRmComments());
					if (id == 0 || id != newExpClaimRmBean.getActionList().size())
						throw new CommonCustomException("some error occured while performing rejection");

					// removing the bundling
					// if (rejectCount > 0) {
					// this.removeExpenseIntoFinanceBundle(this.newExpDatExpenseBOList.get(0));
					// }
					// Creating record history
					this.newExpDatExpenseBOList = newExpDatExpenseRepository
							.findExpenseByListOfId(newExpClaimRmBean.getActionList());
					this.newExpDatExpenseBOList.forEach(obj -> {
						this.createRecordHistory(obj.getPkIdNewExpDatExpense(), obj.getFkRmEmpId(),
								NewExpConstants.Exp_Rejected_by_RM, null);
					});
				}
			} else
				throw new CommonCustomException("Comments are mandatory");
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		return viewConvertedBeanListFromBoList(this.newExpDatExpenseBOList);
	}

	// Forward expense
	public List<NewExpDatExpenseBean> forwardExpenseByRM(NewExpClaimRmBean newExpClaimRmBean)
			throws CommonCustomException {
		try {
			if (Optional.ofNullable(newExpClaimRmBean.getToMgrId()).isPresent()
					&& Optional.ofNullable(newExpClaimRmBean.getFkRmId()).isPresent()) {
				DatEmpProfessionalDetailBO prop = datEmployeeProfessionalRepository
						.findByFkMainEmpDetailId(newExpClaimRmBean.getToMgrId());
				if (Optional.ofNullable(prop).isPresent()) {
					if (this.validToRMAction(newExpClaimRmBean)) {
						this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
						this.newExpDatExpenseBOList = newExpDatExpenseRepository
								.findExpenseByListOfId(newExpClaimRmBean.getActionList());
						Integer oldRmrecordId = this.newExpDatExpenseBOList.get(0).getFkExpRmClaimRecordId();
						Integer oldRmId = this.newExpDatExpenseBOList.get(0).getFkRmEmpId();
						Integer empId = this.newExpDatExpenseBOList.get(0).getFkEmpid();
						// if (this.newExpDatExpenseBOList.size() !=
						// newExpClaimRmBean.getActionList().size()) {
						// throw new CommonCustomException("Some of the expense are not available");
						// }
						// this.newExpDatExpenseBOList.forEach(obj -> {
						// if (obj.getFkRmEmpId().equals(newExpClaimRmBean.getFkRmId())) {
						// ++this.RMCheck;
						// }
						// });
						// if (this.RMCheck != newExpClaimRmBean.getActionList().size()) {
						// throw new CommonCustomException("Expense manager id mismatch");
						// }
						// this.newExpClaimRmBeanList = new ArrayList<NewExpClaimRmBean>();
						Integer id = newExpDatExpenseRepository.forwardExpenses(newExpClaimRmBean.getActionList(),
								NewExpConstants.Exp_Forwarded, newExpClaimRmBean.getToMgrId());
						if (id == 0 || id != newExpClaimRmBean.getActionList().size())
							throw new CommonCustomException("some error occured while performing forward");
						this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
						this.newExpDatExpenseBOList = newExpDatExpenseRepository
								.findExpenseByListOfId(newExpClaimRmBean.getActionList());
						// adding into bundle
						for (NewExpDatExpenseBO bo : this.newExpDatExpenseBOList) {
							Boolean rmRecordIdCheck = this.addingExpenseIntoForwardBundle(bo);
							if (!rmRecordIdCheck)
								throw new CommonCustomException("some problem occured while expense bundling");
						}
						// record history create
						this.newExpDatExpenseBOList.forEach(obj -> {
							this.createRecordHistory(obj.getPkIdNewExpDatExpense(), obj.getFkRmEmpId(),
									NewExpConstants.Exp_Forwarded, newExpClaimRmBean.getToMgrId());
						});

						// resort the old RM bundle
						this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
						this.newExpDatExpenseBOList = newExpDatExpenseRepository
								.findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(oldRmrecordId);
						if (!this.newExpDatExpenseBOList.isEmpty()) {
							List<Date> listOfDates = new ArrayList<>();
							this.newExpDatExpenseBOList.stream().forEach(obj -> {
								if (obj.getExpStartDate() != null)
									listOfDates.add(obj.getExpStartDate());
								if (obj.getExpEndDate() != null)
									listOfDates.add(obj.getExpEndDate());
								if (obj.getExpStartDate() != null)
									listOfDates.add(obj.getExpStartDate());
								if (obj.getExpEndDate() != null)
									listOfDates.add(obj.getExpEndDate());
							});
							Collections.sort(listOfDates);
							Date firstDate = listOfDates.get(0);
							Date lastDate = listOfDates.get(listOfDates.size() - 1);
							this.newExpClaimRmBO = newExpClaimRmRepository.findByFkEmpIdAndFkRmIdAndFkStatusId(
									this.newExpDatExpenseBOList.get(0).getFkEmpid(), oldRmId,
									NewExpConstants.Claim_Awaiting_for_approval);
							this.newExpClaimRmBO = new NewExpClaimRmBO(this.newExpClaimRmBO.getPkidExpClaimRm(), empId,
									oldRmId, firstDate, lastDate, this.newExpClaimRmBO.getFkStatusId(),
									this.newExpClaimRmBO.getClaimCreatedDate());
							this.newExpClaimRmBO = newExpClaimRmRepository.save(this.newExpClaimRmBO);
						} else {
							newExpClaimRmRepository.delete(oldRmrecordId);
						}

					}
				} else
					throw new CommonCustomException("Entered Id is not an employee");
			} else {
				throw new CommonCustomException("To Manager Id and login mgrId is mandatory");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		return viewConvertedBeanListFromBoList(this.newExpDatExpenseBOList);
	}

	// Forward bundle
	public Boolean addingExpenseIntoForwardBundle(NewExpDatExpenseBO inputBo) throws CommonCustomException {
		try {
			this.newExpClaimRmBO = new NewExpClaimRmBO();
			this.newExpDatExpenseBO = new NewExpDatExpenseBO();
			this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
			this.newExpClaimRmBO = newExpClaimRmRepository.findByFkEmpIdAndFkRmIdAndFkStatusId(inputBo.getFkEmpid(),
					inputBo.getFkRmEmpId(), inputBo.getFkStatusId());
			// create record
			if (this.newExpClaimRmBO == null) {
				if (inputBo.getExpEndDate() != null)
					this.newExpClaimRmBO = new NewExpClaimRmBO(inputBo.getFkEmpid(), inputBo.getFkRmEmpId(),
							inputBo.getExpStartDate(), inputBo.getExpEndDate(), inputBo.getFkStatusId(), new Date());
				else
					this.newExpClaimRmBO = new NewExpClaimRmBO(inputBo.getFkEmpid(), inputBo.getFkRmEmpId(),
							inputBo.getExpStartDate(), inputBo.getExpStartDate(), inputBo.getFkStatusId(), new Date());
				this.newExpClaimRmBO = newExpClaimRmRepository.save(this.newExpClaimRmBO);
				// adding record id to expense
				this.newExpDatExpenseBO = newExpDatExpenseRepository
						.findByPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
				this.newExpDatExpenseBO.setPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
				this.newExpDatExpenseBO.setFkExpRmClaimRecordId(this.newExpClaimRmBO.getPkidExpClaimRm());
				newExpDatExpenseRepository.save(this.newExpDatExpenseBO);
			}
			// update record
			else {
				this.newExpDatExpenseBOList = newExpDatExpenseRepository
						.findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(
								this.newExpClaimRmBO.getPkidExpClaimRm());
				List<Date> listOfDates = new ArrayList<>();
				this.newExpDatExpenseBOList.stream().forEach(obj -> {
					if (obj.getExpStartDate() != null)
						listOfDates.add(obj.getExpStartDate());
					if (obj.getExpEndDate() != null)
						listOfDates.add(obj.getExpEndDate());
					if (inputBo.getExpStartDate() != null)
						listOfDates.add(inputBo.getExpStartDate());
					if (inputBo.getExpEndDate() != null)
						listOfDates.add(inputBo.getExpEndDate());
				});
				Collections.sort(listOfDates);
				Date firstDate = listOfDates.get(0);
				Date lastDate = listOfDates.get(listOfDates.size() - 1);
				this.newExpClaimRmBO = new NewExpClaimRmBO(this.newExpClaimRmBO.getPkidExpClaimRm(),
						inputBo.getFkEmpid(), inputBo.getFkRmEmpId(), firstDate, lastDate, inputBo.getFkStatusId(),
						this.newExpClaimRmBO.getClaimCreatedDate());
				this.newExpClaimRmBO = newExpClaimRmRepository.save(this.newExpClaimRmBO);
				// adding record id to expense
				this.newExpDatExpenseBO = newExpDatExpenseRepository
						.findByPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
				this.newExpDatExpenseBO.setPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
				this.newExpDatExpenseBO.setFkExpRmClaimRecordId(this.newExpClaimRmBO.getPkidExpClaimRm());
				newExpDatExpenseRepository.save(this.newExpDatExpenseBO);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// adding the expense into Finance bundle
	public Boolean addingExpenseIntoFinanceBundle(NewExpDatExpenseBO inputBo) {
		try {
			this.newExpClaimFinBO = new NewExpClaimFinBO();
			this.newExpDatExpenseBO = new NewExpDatExpenseBO();
			this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
			this.newExpClaimFinBO = newExpClaimFinRepository
					.findByFkEmpIdAndFinanceTeamAndFkStatusIdAndClaimCurrencyTypeId(inputBo.getFkEmpid(),
							inputBo.getFinanceTeam(), NewExpConstants.Claim_Approved_by_RM,
							inputBo.getConvertedAmountToCurrencyTypeId());
			// create record
			if (this.newExpClaimFinBO == null) {
				if (inputBo.getExpEndDate() != null)
					this.newExpClaimFinBO = new NewExpClaimFinBO(inputBo.getFkEmpid(), inputBo.getFinanceTeam(),
							inputBo.getExpStartDate(), inputBo.getExpEndDate(), NewExpConstants.Claim_Approved_by_RM,
							new Date(), inputBo.getConvertedAmountToCurrencyTypeId(),
							inputBo.getConvertedTotalAmountToPayroll());
				else
					this.newExpClaimFinBO = new NewExpClaimFinBO(inputBo.getFkEmpid(), inputBo.getFinanceTeam(),
							inputBo.getExpStartDate(), inputBo.getExpStartDate(), NewExpConstants.Claim_Approved_by_RM,
							new Date(), inputBo.getConvertedAmountToCurrencyTypeId(),
							inputBo.getConvertedTotalAmountToPayroll());
				this.newExpClaimFinBO = newExpClaimFinRepository.save(this.newExpClaimFinBO);
				// adding record id to expense
				this.newExpDatExpenseBO = newExpDatExpenseRepository
						.findByPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
				this.newExpDatExpenseBO.setPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
				this.newExpDatExpenseBO.setFkExpFinClaimRecordId(this.newExpClaimFinBO.getPkIdExpClaimFin());
				newExpDatExpenseRepository.save(this.newExpDatExpenseBO);
			}
			// update record
			else {
				this.newExpDatExpenseBOList = newExpDatExpenseRepository
						.findByFkExpFinClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(
								this.newExpClaimFinBO.getPkIdExpClaimFin());
				if (!this.newExpDatExpenseBOList.isEmpty()) {
					List<Date> listOfDates = new ArrayList<>();
					this.newExpDatExpenseBOList.stream().forEach(obj -> {
						if (obj.getExpStartDate() != null)
							listOfDates.add(obj.getExpStartDate());
						if (obj.getExpEndDate() != null)
							listOfDates.add(obj.getExpEndDate());
						if (inputBo.getExpStartDate() != null)
							listOfDates.add(inputBo.getExpStartDate());
						if (inputBo.getExpEndDate() != null)
							listOfDates.add(inputBo.getExpEndDate());
					});
					Collections.sort(listOfDates);
					Date firstDate = listOfDates.get(0);
					Date lastDate = listOfDates.get(listOfDates.size() - 1);
					Double amount = 0.0;
					for (NewExpDatExpenseBO obj : this.newExpDatExpenseBOList) {
						amount = amount + obj.getConvertedTotalAmountToPayroll();
					}
					amount = inputBo.getConvertedTotalAmountToPayroll() + amount;
					this.newExpClaimFinBO = new NewExpClaimFinBO(this.newExpClaimFinBO.getPkIdExpClaimFin(),
							inputBo.getFkEmpid(), inputBo.getFinanceTeam(), firstDate, lastDate,
							this.newExpClaimFinBO.getFkStatusId(), this.newExpClaimFinBO.getClaimCreatedOn(),
							this.newExpClaimFinBO.getClaimCurrencyTypeId(), amount);
					this.newExpClaimFinBO = newExpClaimFinRepository.save(this.newExpClaimFinBO);
					// adding record id to expense
					this.newExpDatExpenseBO = newExpDatExpenseRepository
							.findByPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
					this.newExpDatExpenseBO.setPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
					this.newExpDatExpenseBO.setFkExpFinClaimRecordId(this.newExpClaimFinBO.getPkIdExpClaimFin());
					newExpDatExpenseRepository.save(this.newExpDatExpenseBO);
				} else {
					newExpClaimFinRepository.delete(this.newExpClaimFinBO.getPkIdExpClaimFin());
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// adding the expense currency conversion
	public Boolean addCurrencyConvertion(NewExpDatExpenseBO inputBo) throws CommonCustomException {
		try {
			this.newExpDatExpenseBO = new NewExpDatExpenseBO();
			this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
			List<DatForexConversionsBO> detailsBeanList = new ArrayList<DatForexConversionsBO>();
			// getting professional details
			DatEmpProfessionalDetailBO prop = datEmployeeProfessionalRepository
					.findByFkMainEmpDetailId(inputBo.getFkEmpid());
			if (prop == null)
				throw new CommonCustomException("invalid employee id");
			else {
				Short THBSUK = 1;
				Short THBSINDIA = 2;
				Short THBSCHINA = 3;
				Short THBSUSA = 4;
				Short THBSEUROPE = 5;
				Short THBSMIDEAST = 6;
				Byte INR = 1;
				Byte USD = 2;
				Byte GBP = 3;
				Byte EUR = 4;
				Byte SGD = 5;
				Byte AED = 6;
				Byte BHD = 7;
				Integer MileageAllowanceRelief = 7;

				if (Optional.ofNullable(inputBo.getExpTotalAmount()).isPresent()) {
					Byte currency = inputBo.getExpCurrencyTypeId().byteValue();
					Byte paymentCurrency = null;
					if (prop.getFkEmpOrganizationId() == THBSUK) {
						paymentCurrency = GBP;
					} else if (prop.getFkEmpOrganizationId() == THBSINDIA) {
						paymentCurrency = INR;
					} else if (prop.getFkEmpOrganizationId() == THBSCHINA) {
						paymentCurrency = GBP;
					} else if (prop.getFkEmpOrganizationId() == THBSUSA) {
						paymentCurrency = USD;
					} else if (prop.getFkEmpOrganizationId() == THBSEUROPE) {
						paymentCurrency = EUR;
					} else if (prop.getFkEmpOrganizationId() == THBSMIDEAST) {
						paymentCurrency = AED;
					}
					Byte lastValue = paymentCurrency;
					this.newExpDatExpenseBO = assignValuesBasedOnCategoryForBO(inputBo);
					this.newExpDatExpenseBO.setFkEmpOrg(prop.getFkEmpOrganizationId().shortValue());
					Date tempDate = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					tempDate = sdf.parse(sdf.format(inputBo.getExpStartDate()));
					detailsBeanList = datForexConversionsRepository.findDetailsByDates(tempDate);
					if (inputBo.getFkCategoryTypeId().equals(MileageAllowanceRelief)) {
						if (prop.getFkEmpOrganizationId() == THBSUK) {
							// this.newExpDatExpenseBO.setPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
							this.newExpDatExpenseBO.setConvertedAmountToCurrencyTypeId(GBP.shortValue());
							this.newExpDatExpenseBO.setConvertedTotalAmountToPayroll(inputBo.getExpTotalAmount());
							// newExpDatExpenseRepository.save(this.newExpDatExpenseBO);
						} else {
							throw new CommonCustomException("The employee is not mapped with the UK Organisation");
						}
					} else {
						if (detailsBeanList.isEmpty()) {
							throw new CommonCustomException(
									"No Currency conversion value for the given Date, Please contact nucleus team");
						} else {
							DatForexConversionsBO convertBo = detailsBeanList.stream()
									.filter(obj -> obj.getFkForexCurrencyTypeId().equals(lastValue)).findFirst().get();
							DatForexConversionsBO datForexConversionsBO = detailsBeanList.stream()
									.filter(obj -> obj.getFkForexCurrencyTypeId().equals(currency)).findFirst().get();
							switch (prop.getFkEmpOrganizationId()) {
							case 1:
								this.newExpDatExpenseBO.setConvertedTotalAmountToPayroll(
										inputBo.getExpTotalAmount() * datForexConversionsBO.getForexExchangeRate()
												/ convertBo.getForexExchangeRate());
								this.newExpDatExpenseBO.setConvertedAmountToCurrencyTypeId(INR.shortValue());
								break;
							case 2:
								this.newExpDatExpenseBO.setConvertedTotalAmountToPayroll(
										inputBo.getExpTotalAmount() * datForexConversionsBO.getForexExchangeRate()
												/ convertBo.getForexExchangeRate());
								this.newExpDatExpenseBO.setConvertedAmountToCurrencyTypeId(USD.shortValue());
								break;
							case 3:
								this.newExpDatExpenseBO.setConvertedTotalAmountToPayroll(
										inputBo.getExpTotalAmount() * datForexConversionsBO.getForexExchangeRate()
												/ convertBo.getForexExchangeRate());
								this.newExpDatExpenseBO.setConvertedAmountToCurrencyTypeId(GBP.shortValue());
								break;
							case 4:
								this.newExpDatExpenseBO.setConvertedTotalAmountToPayroll(
										inputBo.getExpTotalAmount() * datForexConversionsBO.getForexExchangeRate()
												/ convertBo.getForexExchangeRate());
								this.newExpDatExpenseBO.setConvertedAmountToCurrencyTypeId(EUR.shortValue());
								break;
							case 5:
								this.newExpDatExpenseBO.setConvertedTotalAmountToPayroll(
										inputBo.getExpTotalAmount() * datForexConversionsBO.getForexExchangeRate()
												/ convertBo.getForexExchangeRate());
								this.newExpDatExpenseBO.setConvertedAmountToCurrencyTypeId(SGD.shortValue());
								break;
							case 6:
								this.newExpDatExpenseBO.setConvertedTotalAmountToPayroll(
										inputBo.getExpTotalAmount() * datForexConversionsBO.getForexExchangeRate()
												/ convertBo.getForexExchangeRate());
								this.newExpDatExpenseBO.setConvertedAmountToCurrencyTypeId(AED.shortValue());
								break;
							}
						}
					}
					newExpDatExpenseRepository.save(this.newExpDatExpenseBO);
				} else
					throw new CommonCustomException("Total amount is not available");
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
	}

	// setting the BO values to BO
	public NewExpDatExpenseBO assignValuesBasedOnCategoryForBO(NewExpDatExpenseBO newExpDatExpenseBean)
			throws CommonCustomException {
		this.newExpDatExpenseBO = new NewExpDatExpenseBO();
		Double amount = 0.0;
		Double isTaxAmount = 0.0;
		Double expenseAmount = 0.0;
		this.newExpDatExpenseBO.setPkIdNewExpDatExpense(newExpDatExpenseBean.getPkIdNewExpDatExpense());
		switch (newExpDatExpenseBean.getFkCategoryTypeId()) {
		case 1: {
			// Travel
			this.newExpDatExpenseBO.setModeOfTransport(newExpDatExpenseBean.getModeOfTransport());
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setExpEndDate(newExpDatExpenseBean.getExpEndDate());
			this.newExpDatExpenseBO.setExpFromPlace(newExpDatExpenseBean.getExpFromPlace());
			this.newExpDatExpenseBO.setExpToPlace(newExpDatExpenseBean.getExpToPlace());
			if (newExpDatExpenseBean.getTicketNumber() != null)
				this.newExpDatExpenseBO.setTicketNumber(newExpDatExpenseBean.getTicketNumber());
			this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				isTaxAmount = newExpDatExpenseBean.getTaxAmount();
				amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				newExpDatExpenseBean.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		case 2: {
			// Lodging
			this.newExpDatExpenseBO.setVendor(newExpDatExpenseBean.getVendor());
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setExpEndDate(newExpDatExpenseBean.getExpEndDate());
			this.newExpDatExpenseBO.setNoOfNights(newExpDatExpenseBean.getNoOfNights());
			this.newExpDatExpenseBO.setNoOfAttendees(newExpDatExpenseBean.getNoOfAttendees());
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				isTaxAmount = newExpDatExpenseBean.getTaxAmount();
				amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				newExpDatExpenseBean.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		case 3: {
			// Meals and Entertainment
			this.newExpDatExpenseBO.setVendor(newExpDatExpenseBean.getVendor());
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
			this.newExpDatExpenseBO.setNoOfAttendees(newExpDatExpenseBean.getNoOfAttendees());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				isTaxAmount = newExpDatExpenseBean.getTaxAmount();
				amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				newExpDatExpenseBean.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		case 4: {
			// Office supply
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				isTaxAmount = newExpDatExpenseBean.getTaxAmount();
				amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				newExpDatExpenseBean.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		case 5: {
			// Phone
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setPhoneAndIt(newExpDatExpenseBean.getPhoneAndIt());
			this.newExpDatExpenseBO.setDuration(newExpDatExpenseBean.getDuration());
			this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				isTaxAmount = newExpDatExpenseBean.getTaxAmount();
				amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				newExpDatExpenseBean.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		case 6: {
			// IT and Internet
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setPhoneAndIt(newExpDatExpenseBean.getPhoneAndIt());
			this.newExpDatExpenseBO.setDuration(newExpDatExpenseBean.getDuration());
			this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				isTaxAmount = newExpDatExpenseBean.getTaxAmount();
				amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				newExpDatExpenseBean.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		case 7: {
			// Mileage
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setFkMileageId(newExpDatExpenseBean.getFkMileageId());
			this.newExpDatExpenseBO.setMiles(newExpDatExpenseBean.getMiles());
			// Double mileageAmount = mileageCalculation(newExpDatExpenseBean);
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(UK_CURRENCY);
			this.newExpDatExpenseBO.setExpTotalAmount(newExpDatExpenseBean.getExpAmount());
			break;
		}
		case 8: {
			// Parking
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setDuration(newExpDatExpenseBean.getDuration());
			this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				isTaxAmount = newExpDatExpenseBean.getTaxAmount();
				amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				newExpDatExpenseBean.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		case 9: {
			// Others
			this.newExpDatExpenseBO.setExpStartDate(newExpDatExpenseBean.getExpStartDate());
			this.newExpDatExpenseBO.setTaxAmount(newExpDatExpenseBean.getTaxAmount());
			if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("NO")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("NO");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				isTaxAmount = newExpDatExpenseBean.getTaxAmount();
				amount = expenseAmount.doubleValue() + isTaxAmount.doubleValue();
			} else if (newExpDatExpenseBean.getIsBillTaxInclusive().equalsIgnoreCase("YES")) {
				this.newExpDatExpenseBO.setIsBillTaxInclusive("YES");
				expenseAmount = newExpDatExpenseBean.getExpAmount();
				newExpDatExpenseBean.setTaxAmount(null);
				amount = expenseAmount.doubleValue();
			} else {
				throw new CommonCustomException("only NO and YES are the input");
			}
			this.newExpDatExpenseBO.setExpAmount(newExpDatExpenseBean.getExpAmount());
			this.newExpDatExpenseBO.setExpCurrencyTypeId(newExpDatExpenseBean.getExpCurrencyTypeId());
			this.newExpDatExpenseBO.setExpTotalAmount(amount);
			break;
		}
		default:
			throw new CommonCustomException("Enter category within the max value");
		}
		this.newExpDatExpenseBO.setFkEmpid(newExpDatExpenseBean.getFkEmpid());
		this.newExpDatExpenseBO.setFkCategoryTypeId(newExpDatExpenseBean.getFkCategoryTypeId());
		this.newExpDatExpenseBO.setExpCreatedOn(newExpDatExpenseBean.getExpCreatedOn());
		this.newExpDatExpenseBO.setDescription(newExpDatExpenseBean.getDescription());
		this.newExpDatExpenseBO.setFkRmEmpId(newExpDatExpenseBean.getFkRmEmpId());
		this.newExpDatExpenseBO.setFkBuId(newExpDatExpenseBean.getFkBuId());
		this.newExpDatExpenseBO.setFkStatusId(newExpDatExpenseBean.getFkStatusId());
		this.newExpDatExpenseBO.setFkClientId(newExpDatExpenseBean.getFkClientId());
		this.newExpDatExpenseBO.setFkProjectId(newExpDatExpenseBean.getFkProjectId());
		this.newExpDatExpenseBO.setFkWorkorderId(newExpDatExpenseBean.getFkWorkorderId());
		this.newExpDatExpenseBO.setBilllableToClient(newExpDatExpenseBean.getBilllableToClient());
		this.newExpDatExpenseBO.setFinanceTeam(newExpDatExpenseBean.getFinanceTeam());
		this.newExpDatExpenseBO.setFkExpFinClaimRecordId(newExpDatExpenseBean.getFkExpFinClaimRecordId());
		this.newExpDatExpenseBO.setFkExpRmClaimRecordId(newExpDatExpenseBean.getFkExpRmClaimRecordId());
		this.newExpDatExpenseBO.setFileName(newExpDatExpenseBean.getFileName());
		this.newExpDatExpenseBO.setPathname(newExpDatExpenseBean.getPathname());
		return newExpDatExpenseBO;
	}

	// validation for RM Actions approve and forward
	public Boolean validToRMAction(NewExpClaimRmBean newExpClaimRmBean) throws CommonCustomException {
		this.countExpense = 0;
		if (newExpClaimRmBean.getActionList().size() > 0
				&& Optional.ofNullable(newExpClaimRmBean.getActionList()).isPresent()) {
			if (Optional.ofNullable(newExpClaimRmBean.getFkRmId()).isPresent()) {
				this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
				this.newExpDatExpenseBOList = newExpDatExpenseRepository
						.findExpenseByListOfId(newExpClaimRmBean.getActionList());
				if (this.newExpDatExpenseBOList.size() > 0) {
					if (this.newExpDatExpenseBOList.size() == newExpClaimRmBean.getActionList().size()) {
						this.newExpDatExpenseBOList.forEach(obj -> {
							if ((obj.getFkStatusId().equals(NewExpConstants.Exp_Awaititng_for_approval)
									|| obj.getFkStatusId().equals(NewExpConstants.Exp_Forwarded))
									&& obj.getFkRmEmpId().equals(newExpClaimRmBean.getFkRmId())) {
								++this.countExpense;
							}
						});
						if (this.countExpense.equals(newExpClaimRmBean.getActionList().size()))
							return true;
						else
							throw new CommonCustomException(
									"Some of the expense which are selected are not in awaiting for approval/forward or approver mismatch");
					} else
						throw new CommonCustomException("Some of the given expense are not available");
				} else
					throw new CommonCustomException("Expense id are not available");
			} else
				throw new CommonCustomException("RM id is mandatory");
		} else
			throw new CommonCustomException("action list is empty");
	}

	// validation for RM Actions to reject
	public Boolean validToRMActionToReject(NewExpClaimRmBean newExpClaimRmBean) throws CommonCustomException {
		this.countExpense = 0;
		if (newExpClaimRmBean.getActionList().size() > 0
				&& Optional.ofNullable(newExpClaimRmBean.getActionList()).isPresent()) {
			if (Optional.ofNullable(newExpClaimRmBean.getFkRmId()).isPresent()) {
				this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
				this.newExpDatExpenseBOList = newExpDatExpenseRepository
						.findExpenseByListOfId(newExpClaimRmBean.getActionList());
				if (this.newExpDatExpenseBOList.size() > 0
						&& Optional.ofNullable(this.newExpDatExpenseBOList).isPresent()) {
					if (this.newExpDatExpenseBOList.size() == newExpClaimRmBean.getActionList().size()) {
						this.newExpDatExpenseBOList.forEach(obj -> {
							if ((obj.getFkStatusId().equals(NewExpConstants.Exp_Awaititng_for_approval)
									|| obj.getFkStatusId().equals(NewExpConstants.Exp_Forwarded)
									|| obj.getFkStatusId().equals(NewExpConstants.Exp_Approved_by_RM))
									&& obj.getFkRmEmpId().equals(newExpClaimRmBean.getFkRmId())) {
								++this.countExpense;
							}
						});
						if (this.countExpense.equals(newExpClaimRmBean.getActionList().size()))
							return true;
						else
							throw new CommonCustomException(
									"Some of the expense which are selected are not in awaiting for approval/forward or approver mismatch");
					} else
						throw new CommonCustomException("Some of the given expense are not available");
				} else
					throw new CommonCustomException("Expense id are not available");
			} else
				throw new CommonCustomException("RM id is mandatory");
		} else
			throw new CommonCustomException("action list is empty");
	}

	// convert the BO values into Bean List
	public List<NewExpDatExpenseBean> viewConvertedBeanListFromBo(NewExpDatExpenseBO bo) {
		this.newExpDatExpenseBeanList = new ArrayList<NewExpDatExpenseBean>();
		if (Optional.ofNullable(bo).isPresent()) {
			this.newExpDatExpenseBean = new NewExpDatExpenseBean();
			this.newExpDatExpenseBean = newExpDatExpenseBeanMapper.getDestination(this.newExpDatExpenseBean, bo);
			if (Optional.ofNullable(this.newExpDatExpenseBean.getFkEmpid()).isPresent())
				this.newExpDatExpenseBean.setEmpName(this.getEmployeeName(this.newExpDatExpenseBean.getFkEmpid()));
			if (Optional.ofNullable(this.newExpDatExpenseBean.getFkRmEmpId()).isPresent())
				this.newExpDatExpenseBean.setRmName(this.getEmployeeName(this.newExpDatExpenseBean.getFkRmEmpId()));
			if (Optional.ofNullable(this.newExpDatExpenseBean.getFkFinEmpId()).isPresent())
				this.newExpDatExpenseBean
						.setFinanceName(this.getEmployeeName(this.newExpDatExpenseBean.getFkFinEmpId()));
			this.newExpDatExpenseBeanList.add(this.newExpDatExpenseBean);
			Collections.sort(this.newExpDatExpenseBeanList, (o1, o2) -> {
				return o2.getPkIdNewExpDatExpense().compareTo(o1.getPkIdNewExpDatExpense());
			});
			return this.newExpDatExpenseBeanList;
		} else
			return this.newExpDatExpenseBeanList;
	}

	// convert the BO List into Bean List
	public List<NewExpDatExpenseBean> viewConvertedBeanListFromBoList(List<NewExpDatExpenseBO> bo) {
		this.newExpDatExpenseBeanList = new ArrayList<NewExpDatExpenseBean>();
		if (Optional.ofNullable(bo).isPresent() && bo.size() > 0) {
			bo.stream().forEach(obj -> {
				this.newExpDatExpenseBean = new NewExpDatExpenseBean();
				this.newExpDatExpenseBean = newExpDatExpenseBeanMapper.getDestination(this.newExpDatExpenseBean, obj);
				if (Optional.ofNullable(this.newExpDatExpenseBean.getFkEmpid()).isPresent())
					this.newExpDatExpenseBean.setEmpName(this.getEmployeeName(this.newExpDatExpenseBean.getFkEmpid()));
				if (Optional.ofNullable(this.newExpDatExpenseBean.getFkRmEmpId()).isPresent())
					this.newExpDatExpenseBean.setRmName(this.getEmployeeName(this.newExpDatExpenseBean.getFkRmEmpId()));
				if (Optional.ofNullable(this.newExpDatExpenseBean.getFkFinEmpId()).isPresent())
					this.newExpDatExpenseBean
							.setFinanceName(this.getEmployeeName(this.newExpDatExpenseBean.getFkFinEmpId()));
				this.newExpDatExpenseBeanList.add(this.newExpDatExpenseBean);
			});
			Collections.sort(this.newExpDatExpenseBeanList, (o1, o2) -> {
				return o2.getPkIdNewExpDatExpense().compareTo(o1.getPkIdNewExpDatExpense());
			});
			return this.newExpDatExpenseBeanList;
		} else
			return this.newExpDatExpenseBeanList;
	}

	// deselecting the rejected expense from finance bundle
	public Boolean checkRejection(NewExpDatExpenseBO inputBo) {
		try {
			this.newExpClaimFinBO = new NewExpClaimFinBO();
			this.newExpDatExpenseBO = new NewExpDatExpenseBO();
			this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
			// To make finance table date range changes
			this.newExpDatExpenseBOList = newExpDatExpenseRepository
					.findByFkExpFinClaimRecordId(inputBo.getFkExpFinClaimRecordId());
			// To make finance table value change
			this.newExpClaimFinBO = newExpClaimFinRepository.findByPkIdExpClaimFin(inputBo.getFkExpFinClaimRecordId());

			if (Optional.ofNullable(this.newExpClaimFinBO).isPresent()) {
				if (this.newExpDatExpenseBOList.size() > 0) {
					List<Date> listOfDates = new ArrayList<>();
					this.newExpDatExpenseBOList.stream().forEach(obj -> {
						if (obj.getExpStartDate() != null)
							listOfDates.add(obj.getExpStartDate());
						if (obj.getExpEndDate() != null)
							listOfDates.add(obj.getExpEndDate());
						if (obj.getExpStartDate() != null)
							listOfDates.add(obj.getExpStartDate());
						if (obj.getExpEndDate() != null)
							listOfDates.add(obj.getExpEndDate());
					});
					Collections.sort(listOfDates);
					Date firstDate = listOfDates.get(0);
					Date lastDate = listOfDates.get(listOfDates.size() - 1);

					// changing the finance expense bundle value
					this.newExpClaimFinBO = new NewExpClaimFinBO(this.newExpClaimFinBO.getPkIdExpClaimFin(),
							this.newExpClaimFinBO.getFkEmpId(), this.newExpClaimFinBO.getFinanceTeam(), firstDate,
							lastDate, this.newExpClaimFinBO.getFkStatusId(), this.newExpClaimFinBO.getClaimCreatedOn());
					this.newExpClaimFinBO = newExpClaimFinRepository.save(this.newExpClaimFinBO);

					// adding record id to expense
					this.newExpDatExpenseBO = newExpDatExpenseRepository
							.findByPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
					this.newExpDatExpenseBO.setPkIdNewExpDatExpense(inputBo.getPkIdNewExpDatExpense());
					this.newExpDatExpenseBO.setFkExpFinClaimRecordId(null);
					this.newExpDatExpenseBO.setFinanceTeam(null);
					this.newExpDatExpenseBO.setFkStatusId(NewExpConstants.Exp_Rejected_by_RM);
					newExpDatExpenseRepository.save(this.newExpDatExpenseBO);
					// // Remove claim from fin if its empty
					// this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
					// this.newExpDatExpenseBOList = newExpDatExpenseRepository
					// .findByFkExpFinClaimRecordId(inputBo.getFkExpFinClaimRecordId());
					// if (this.newExpDatExpenseBOList.isEmpty()) {
					// newExpClaimFinRepository.delete(inputBo.getFkExpFinClaimRecordId());
					// }
				} else
					throw new CommonCustomException("No expense for the finance bundle");
			} else
				throw new CommonCustomException("Enter finance packet id is not available");
			// Remove claim from fin if its empty
			this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
			this.newExpDatExpenseBOList = newExpDatExpenseRepository
					.findByFkExpFinClaimRecordId(inputBo.getFkExpFinClaimRecordId());
			if (this.newExpDatExpenseBOList.isEmpty()) {
				newExpClaimFinRepository.delete(inputBo.getFkExpFinClaimRecordId());
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// emp cancel expense
	public List<NewExpDatExpenseBean> expenseCancelByEmp(NewExpDatExpenseBean newExpDatExpenseBean)
			throws CommonCustomException {
		if (Optional.ofNullable(newExpDatExpenseBean.getFkEmpid()).isPresent()) {
			if (Optional.ofNullable(newExpDatExpenseBean.getPkIdNewExpDatExpense()).isPresent()) {
				this.newExpDatExpenseBO = newExpDatExpenseRepository
						.findByPkIdNewExpDatExpense(newExpDatExpenseBean.getPkIdNewExpDatExpense());
				if (Optional.ofNullable(this.newExpDatExpenseBO).isPresent()) {
					if (this.newExpDatExpenseBO.getFkEmpid().equals(newExpDatExpenseBean.getFkEmpid())) {
						if (this.newExpDatExpenseBO.getFkStatusId()
								.equals(NewExpConstants.Exp_Awaititng_for_approval)) {
							this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
							this.newExpClaimRmBO = new NewExpClaimRmBO();
							Integer rmClaimId = this.newExpDatExpenseBO.getFkExpRmClaimRecordId();
							this.newExpDatExpenseBOList = newExpDatExpenseRepository
									.findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(
											this.newExpDatExpenseBO.getFkExpRmClaimRecordId());
							this.newExpClaimRmBO = newExpClaimRmRepository
									.findByPkidExpClaimRm(this.newExpDatExpenseBO.getFkExpRmClaimRecordId());
							List<Date> listOfDates = new ArrayList<>();
							this.newExpDatExpenseBOList.stream().forEach(obj -> {
								if (obj.getExpStartDate() != null)
									listOfDates.add(obj.getExpStartDate());
								if (obj.getExpEndDate() != null)
									listOfDates.add(obj.getExpEndDate());
								if (obj.getExpStartDate() != null)
									listOfDates.add(obj.getExpStartDate());
								if (obj.getExpEndDate() != null)
									listOfDates.add(obj.getExpEndDate());
							});
							Collections.sort(listOfDates);
							Date firstDate = listOfDates.get(0);
							Date lastDate = listOfDates.get(listOfDates.size() - 1);
							if (Optional.ofNullable(this.newExpClaimRmBO).isPresent()) {
								if (Optional.ofNullable(this.newExpDatExpenseBOList).isPresent()
										&& this.newExpDatExpenseBOList.size() > 0) {
									this.newExpClaimRmBO = new NewExpClaimRmBO(this.newExpClaimRmBO.getPkidExpClaimRm(),
											this.newExpClaimRmBO.getFkEmpId(), this.newExpClaimRmBO.getFkRmId(),
											firstDate, lastDate, this.newExpClaimRmBO.getFkStatusId(),
											this.newExpClaimRmBO.getClaimCreatedDate());
									this.newExpClaimRmBO = newExpClaimRmRepository.save(this.newExpClaimRmBO);

									// adding record id to expense
									this.newExpDatExpenseBO
											.setPkIdNewExpDatExpense(this.newExpDatExpenseBO.getPkIdNewExpDatExpense());
									this.newExpDatExpenseBO.setFkStatusId(NewExpConstants.Exp_Cancelled_By_Emp);
									this.newExpDatExpenseBO.setFkExpRmClaimRecordId(null);
									newExpDatExpenseRepository.save(this.newExpDatExpenseBO);
									Boolean historyCreated = this.createRecordHistory(
											this.newExpDatExpenseBO.getPkIdNewExpDatExpense(),
											this.newExpDatExpenseBO.getFkEmpid(),
											this.newExpDatExpenseBO.getFkStatusId(), null);
									if (!historyCreated)
										throw new CommonCustomException(
												"Some problem occured while expense history capturing");
									this.newExpDatExpenseBOList = newExpDatExpenseRepository
											.findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(rmClaimId);
									if (this.newExpDatExpenseBOList.isEmpty()) {
										newExpClaimRmRepository.delete(rmClaimId);
									}
								}
							}
						} else
							throw new CommonCustomException("The expense is not in awaiting for approval");
					} else
						throw new CommonCustomException("Empid mismatch");
				} else
					throw new CommonCustomException("The id doesn't exist");
			} else
				throw new CommonCustomException("Expense id is mandatory");
		} else
			throw new CommonCustomException("Emp id is mandatory");

		return this.viewConvertedBeanListFromBo(this.newExpDatExpenseBO);

	}

	// Delete the expense
	public Boolean deleteExpense(NewExpDatExpenseBean newExpDatExpenseBean) throws CommonCustomException {
		try {
			if (Optional.ofNullable(newExpDatExpenseBean.getPkIdNewExpDatExpense()).isPresent()
					&& Optional.ofNullable(newExpDatExpenseBean.getFkEmpid()).isPresent()) {
				this.newExpDatExpenseBO = new NewExpDatExpenseBO();
				this.newExpDatExpenseBO = newExpDatExpenseRepository
						.findByPkIdNewExpDatExpense(newExpDatExpenseBean.getPkIdNewExpDatExpense());
				if (Optional.ofNullable(this.newExpDatExpenseBO).isPresent()) {
					if (this.newExpDatExpenseBO.getFkStatusId().equals(NewExpConstants.Exp_Awaititng_for_approval)) {
						if (newExpDatExpenseBean.getFkEmpid().equals(this.newExpDatExpenseBO.getFkEmpid())) {
							Integer rmClaimId = this.newExpDatExpenseBO.getFkExpRmClaimRecordId();
							File file = new File(
									EXPENSE_RECEIPT_UPLOAD_DIRECTORY + this.newExpDatExpenseBO.getPathname());
							if (file.delete()) {
								newExpDatExpenseRepository.delete(this.newExpDatExpenseBO.getPkIdNewExpDatExpense());
							} else
								throw new CommonCustomException("File is not deleted");
							this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
							this.newExpClaimRmBO = new NewExpClaimRmBO();
							this.newExpDatExpenseBOList = newExpDatExpenseRepository
									.findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(rmClaimId);
							this.newExpClaimRmBO = newExpClaimRmRepository.findByPkidExpClaimRm(rmClaimId);
							List<Date> listOfDates = new ArrayList<>();
							Date firstDate;
							Date lastDate;
							if (!this.newExpDatExpenseBOList.isEmpty()) {
								this.newExpDatExpenseBOList.stream().forEach(obj -> {
									if (obj.getExpStartDate() != null)
										listOfDates.add(obj.getExpStartDate());
									if (obj.getExpEndDate() != null)
										listOfDates.add(obj.getExpEndDate());
									if (obj.getExpStartDate() != null)
										listOfDates.add(obj.getExpStartDate());
									if (obj.getExpEndDate() != null)
										listOfDates.add(obj.getExpEndDate());
								});
								Collections.sort(listOfDates);
								firstDate = listOfDates.get(0);
								lastDate = listOfDates.get(listOfDates.size() - 1);
								if (Optional.ofNullable(this.newExpClaimRmBO).isPresent()) {
									this.newExpClaimRmBO = new NewExpClaimRmBO(this.newExpClaimRmBO.getPkidExpClaimRm(),
											this.newExpClaimRmBO.getFkEmpId(), this.newExpClaimRmBO.getFkRmId(),
											firstDate, lastDate, this.newExpClaimRmBO.getFkStatusId(),
											this.newExpClaimRmBO.getClaimCreatedDate());
									this.newExpClaimRmBO = newExpClaimRmRepository.save(this.newExpClaimRmBO);
									this.newExpDatExpenseBOList = newExpDatExpenseRepository
											.findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(rmClaimId);
									return true;
								} else
									throw new CommonCustomException("RM bundle is not available");
							} else {
								if (Optional.ofNullable(this.newExpClaimRmBO).isPresent()) {
									newExpClaimRmRepository.delete(rmClaimId);
									return true;
								}
							}
						} else
							throw new CommonCustomException("Emp id mismatch");
					} else
						throw new CommonCustomException("Status must be awaiting for approval");
				} else
					throw new CommonCustomException("The expense id doesn't exist");
			} else
				throw new CommonCustomException("Emp id and expense Id is mandatory");
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}

		return false;
	}

	// Download expense
	public Boolean downloadExpense(Integer expId, HttpServletResponse response)
			throws CommonCustomException, IOException {
		if (Optional.ofNullable(expId).isPresent()) {
			this.newExpDatExpenseBO = new NewExpDatExpenseBO();
			this.newExpDatExpenseBO = newExpDatExpenseRepository.findByPkIdNewExpDatExpense(expId);
			if (Optional.ofNullable(this.newExpDatExpenseBO).isPresent()) {
				String basePath = EXPENSE_RECEIPT_UPLOAD_DIRECTORY;
				DownloadFileUtil.downloadFile(response, basePath, this.newExpDatExpenseBO.getPathname());
				return true;
			} else
				throw new CommonCustomException("Id does not exist");
		} else
			throw new CommonCustomException("expense id is mandatory");
	}

	// Change the RM Claim status during RM Action
	public Boolean changeRMClaimStatus(NewExpDatExpenseBO bo) {
		try {
			this.approvedCount = 0;
			this.rejecteCount = 0;
			this.forwardCount = 0;
			this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
			this.newExpClaimRmBO = new NewExpClaimRmBO();
			this.newExpDatExpenseBOList = newExpDatExpenseRepository
					.findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(bo.getFkExpRmClaimRecordId());
			this.newExpClaimRmBO = newExpClaimRmRepository.findByPkidExpClaimRm(bo.getFkExpRmClaimRecordId());
			if (!this.newExpDatExpenseBOList.isEmpty()) {
				for (NewExpDatExpenseBO obj : this.newExpDatExpenseBOList) {
					if (obj.getFkStatusId().equals(NewExpConstants.Exp_Awaititng_for_approval)) {
						this.newExpClaimRmBO.setPkidExpClaimRm(bo.getFkExpRmClaimRecordId());
						this.newExpClaimRmBO.setFkStatusId(NewExpConstants.Claim_Awaiting_for_approval);
						newExpClaimRmRepository.save(this.newExpClaimRmBO);
						break;
					} else {
						if (obj.getFkStatusId().equals(NewExpConstants.Exp_Forwarded)) {
							++this.forwardCount;
						} else if (obj.getFkStatusId().equals(NewExpConstants.Exp_Approved_by_RM)) {
							++this.approvedCount;
						} else if (obj.getFkStatusId().equals(NewExpConstants.Exp_Rejected_by_RM)) {
							++this.rejecteCount;
						}
					}
				}
				if (this.approvedCount.equals(this.newExpDatExpenseBOList.size())) {
					this.newExpClaimRmBO.setPkidExpClaimRm(bo.getFkExpRmClaimRecordId());
					this.newExpClaimRmBO.setFkStatusId(NewExpConstants.Claim_Approved_by_RM);
					newExpClaimRmRepository.save(this.newExpClaimRmBO);
				} else if (this.approvedCount != this.newExpDatExpenseBOList.size() && this.rejecteCount > 0
						|| this.forwardCount > 0) {
					this.newExpClaimRmBO.setPkidExpClaimRm(bo.getFkExpRmClaimRecordId());
					this.newExpClaimRmBO.setFkStatusId(NewExpConstants.Claim_Partially_Approved_by_RM);
					newExpClaimRmRepository.save(this.newExpClaimRmBO);
				} else if (this.rejecteCount == this.newExpDatExpenseBOList.size()) {
					this.newExpClaimRmBO.setPkidExpClaimRm(bo.getFkExpRmClaimRecordId());
					this.newExpClaimRmBO.setFkStatusId(NewExpConstants.Claim_Rejected_by_RM);
					newExpClaimRmRepository.save(this.newExpClaimRmBO);
				} else if (this.forwardCount == this.newExpDatExpenseBOList.size()) {
					this.newExpClaimRmBO.setPkidExpClaimRm(bo.getFkExpRmClaimRecordId());
					this.newExpClaimRmBO.setFkStatusId(NewExpConstants.Claim_Forwarded);
					newExpClaimRmRepository.save(this.newExpClaimRmBO);
				}
			} else
				newExpClaimRmRepository.delete(bo.getFkExpRmClaimRecordId());

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// Accept expense by Finance
	public List<NewExpDatExpenseBean> acceptExpenseByFinance(NewExpClaimFinBean newExpClaimFinBean)
			throws CommonCustomException {
		try {
			if (!newExpClaimFinBean.getActionList().isEmpty()) {
				Boolean check = this.validToFinanceActionForApproveExpense(newExpClaimFinBean);
				if (check) {
					this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
					this.newExpDatExpenseBO = new NewExpDatExpenseBO();
					Integer id = newExpDatExpenseRepository.approveOrRejectExpensesByFinance(
							newExpClaimFinBean.getActionList(), NewExpConstants.Exp_Payment_Initiated);
					if (id != 0 && id == newExpClaimFinBean.getActionList().size()) {

						// Adding claim amount
						this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
						this.newExpDatExpenseBOList = newExpDatExpenseRepository
								.findExpenseByListOfId(newExpClaimFinBean.getActionList());
						for (NewExpDatExpenseBO bo : this.newExpDatExpenseBOList) {
							Boolean checkStatus = this.changeFinClaimData(bo);
							if (!checkStatus)
								throw new CommonCustomException("error occured while changing total amount in finance");
						}

						// creating History
						this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
						this.newExpDatExpenseBOList = newExpDatExpenseRepository
								.findExpenseByListOfId(newExpClaimFinBean.getActionList());
						this.newExpDatExpenseBOList.forEach(obj -> {
							this.createRecordHistory(obj.getPkIdNewExpDatExpense(),
									newExpClaimFinBean.getFinanceLogin(), NewExpConstants.Exp_Payment_Initiated, null);
							// adding finance claim status
							this.changeFinClaimStatus(obj);
							// adding RM claim status
							this.changeRMClaimStatusDuringFinanceAction(obj);
						});

					} else
						throw new CommonCustomException("some error occured while approving the expense");
				} else
					throw new CommonCustomException("Some of the expense are not vaild with the login employee");
			} else
				throw new CommonCustomException("Approve list is empty");
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		return viewConvertedBeanListFromBoList(this.newExpDatExpenseBOList);
	}

	// change the rm claim status during Finance action
	public Boolean changeRMClaimStatusDuringFinanceAction(NewExpDatExpenseBO bo) {
		try {
			this.approvedCount = 0;
			this.rejecteCount = 0;
			this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
			this.newExpClaimRmBO = new NewExpClaimRmBO();
			this.newExpDatExpenseBOList = newExpDatExpenseRepository
					.findByFkExpRmClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(bo.getFkExpRmClaimRecordId());
			this.newExpClaimRmBO = newExpClaimRmRepository.findByPkidExpClaimRm(bo.getFkExpRmClaimRecordId());
			if (!this.newExpDatExpenseBOList.isEmpty()) {
				for (NewExpDatExpenseBO obj : this.newExpDatExpenseBOList) {
					if (obj.getFkStatusId().equals(NewExpConstants.Exp_Awaititng_for_approval)) {
						this.newExpClaimRmBO.setPkidExpClaimRm(bo.getFkExpRmClaimRecordId());
						this.newExpClaimRmBO.setFkStatusId(NewExpConstants.Claim_Awaiting_for_approval);
						newExpClaimRmRepository.save(this.newExpClaimRmBO);
						break;
					} else if (obj.getFkStatusId().equals(NewExpConstants.Exp_Forwarded)) {
						this.newExpClaimRmBO.setPkidExpClaimRm(bo.getFkExpRmClaimRecordId());
						this.newExpClaimRmBO.setFkStatusId(NewExpConstants.Claim_Forwarded);
						newExpClaimRmRepository.save(this.newExpClaimRmBO);
						break;
					} else {
						if (obj.getFkStatusId().equals(NewExpConstants.Exp_Payment_Initiated)) {
							++this.approvedCount;
						} else if (obj.getFkStatusId().equals(NewExpConstants.Exp_Rejected_By_Finance)) {
							++this.rejecteCount;
						}
					}
				}
				if (this.approvedCount.equals(this.newExpDatExpenseBOList.size())) {
					this.newExpClaimRmBO.setPkidExpClaimRm(bo.getFkExpRmClaimRecordId());
					this.newExpClaimRmBO.setFkStatusId(NewExpConstants.Claim_Payment_Initiated);
					newExpClaimRmRepository.save(this.newExpClaimRmBO);
				} else if (this.approvedCount != this.newExpDatExpenseBOList.size() && this.rejecteCount > 0) {
					if (!this.newExpClaimRmBO.getFkStatusId().equals(NewExpConstants.Claim_Awaiting_for_approval)
							&& !this.newExpClaimRmBO.getFkStatusId().equals(NewExpConstants.Claim_Forwarded)) {
						this.newExpClaimRmBO.setPkidExpClaimRm(bo.getFkExpRmClaimRecordId());
						this.newExpClaimRmBO.setFkStatusId(NewExpConstants.Claim_Payment_Initiated_Partially);
						newExpClaimRmRepository.save(this.newExpClaimRmBO);
					}
				} else if (this.rejecteCount == this.newExpDatExpenseBOList.size()) {
					this.newExpClaimRmBO.setPkidExpClaimRm(bo.getFkExpRmClaimRecordId());
					this.newExpClaimRmBO.setFkStatusId(NewExpConstants.Claim_Rejected_by_Finance);
					newExpClaimRmRepository.save(this.newExpClaimRmBO);
				}
			} else
				newExpClaimRmRepository.delete(bo.getFkExpRmClaimRecordId());

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// Change the Finance Claim status during Finance Action
	public Boolean changeFinClaimStatus(NewExpDatExpenseBO bo) {
		try {
			this.approvedCount = 0;
			this.rejecteCount = 0;
			this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
			this.newExpClaimFinBO = new NewExpClaimFinBO();
			this.newExpDatExpenseBOList = newExpDatExpenseRepository
					.findByFkExpFinClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(bo.getFkExpFinClaimRecordId());
			this.newExpClaimFinBO = newExpClaimFinRepository.findByPkIdExpClaimFin(bo.getFkExpFinClaimRecordId());
			if (!this.newExpDatExpenseBOList.isEmpty()) {
				for (NewExpDatExpenseBO obj : this.newExpDatExpenseBOList) {
					if (obj.getFkStatusId().equals(NewExpConstants.Exp_Payment_Initiated)) {
						++this.approvedCount;
					} else if (obj.getFkStatusId().equals(NewExpConstants.Exp_Rejected_By_Finance)) {
						++this.rejecteCount;
					}
				}
				if (this.approvedCount.equals(this.newExpDatExpenseBOList.size())) {
					this.newExpClaimFinBO.setPkIdExpClaimFin(bo.getFkExpFinClaimRecordId());
					this.newExpClaimFinBO.setFkStatusId(NewExpConstants.Claim_Payment_Initiated);
					newExpClaimFinRepository.save(this.newExpClaimFinBO);
				} else if (this.approvedCount != this.newExpDatExpenseBOList.size() && this.rejecteCount > 0) {
					this.newExpClaimFinBO.setPkIdExpClaimFin(bo.getFkExpFinClaimRecordId());
					this.newExpClaimFinBO.setFkStatusId(NewExpConstants.Claim_Payment_Initiated_Partially);
					newExpClaimFinRepository.save(this.newExpClaimFinBO);
				} else if (this.rejecteCount == this.newExpDatExpenseBOList.size()) {
					this.newExpClaimFinBO.setPkIdExpClaimFin(bo.getFkExpFinClaimRecordId());
					this.newExpClaimFinBO.setFkStatusId(NewExpConstants.Claim_Rejected_by_Finance);
					newExpClaimFinRepository.save(this.newExpClaimFinBO);
				}
			} else
				newExpClaimFinRepository.delete(bo.getFkExpFinClaimRecordId());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// Change the Finance Claim amount while approve
	public Boolean changeFinClaimData(NewExpDatExpenseBO bo) {
		try {
			Double amount = 0.0;
			this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
			this.newExpClaimFinBO = new NewExpClaimFinBO();
			this.newExpDatExpenseBOList = newExpDatExpenseRepository
					.findByFkExpFinClaimRecordIdOrderByPkIdNewExpDatExpenseDesc(bo.getFkExpFinClaimRecordId());
			this.newExpClaimFinBO = newExpClaimFinRepository.findByPkIdExpClaimFin(bo.getFkExpFinClaimRecordId());
			if (!this.newExpDatExpenseBOList.isEmpty()) {
				for (NewExpDatExpenseBO obj : this.newExpDatExpenseBOList) {
					if (obj.getFkStatusId().equals(NewExpConstants.Exp_Payment_Initiated)) {
						this.newExpClaimFinBO.setPkIdExpClaimFin(bo.getFkExpFinClaimRecordId());
						amount = amount + obj.getConvertedTotalAmountToPayroll();
						this.newExpClaimFinBO.setClaimApprovedAmount(amount);
						this.newExpClaimFinBO.setClaimApprovedCurrencyTypeId(obj.getConvertedAmountToCurrencyTypeId());
						newExpClaimFinRepository.save(this.newExpClaimFinBO);
					}
				}
			} else
				newExpClaimFinRepository.delete(bo.getFkExpFinClaimRecordId());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// Reject expense by Finance
	public List<NewExpDatExpenseBean> rejectExpenseByFinance(NewExpClaimFinBean newExpClaimFinBean)
			throws CommonCustomException {
		try {
			if (Optional.ofNullable(newExpClaimFinBean.getFinComments()).isPresent()) {
				Boolean check = this.validToFinanceActionForApproveExpense(newExpClaimFinBean);
				if (check) {
					this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
					this.newExpDatExpenseBO = new NewExpDatExpenseBO();
					Integer id = newExpDatExpenseRepository.rejectExpensesByFinance(newExpClaimFinBean.getActionList(),
							NewExpConstants.Exp_Rejected_By_Finance, newExpClaimFinBean.getFinComments());
					if (id != 0 && id == newExpClaimFinBean.getActionList().size()) {

						// creating History
						this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
						this.newExpDatExpenseBOList = newExpDatExpenseRepository
								.findExpenseByListOfId(newExpClaimFinBean.getActionList());
						this.newExpDatExpenseBOList.forEach(obj -> {
							this.createRecordHistory(obj.getPkIdNewExpDatExpense(),
									newExpClaimFinBean.getFinanceLogin(), NewExpConstants.Exp_Rejected_By_Finance,
									null);
							// adding Finance claim status
							this.changeFinClaimStatus(obj);
							// adding RM calim status
							this.changeRMClaimStatusDuringFinanceAction(obj);
						});

					} else
						throw new CommonCustomException("some error occured while approving the expense");
				} else
					throw new CommonCustomException("Some of the expense are not vaild with the login employee");
			} else
				throw new CommonCustomException("Rejection comments is mandatory");
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		return viewConvertedBeanListFromBoList(this.newExpDatExpenseBOList);
	}

	// Validation for finance action
	public Boolean validToFinanceActionForApproveExpense(NewExpClaimFinBean newExpClaimFinBean)
			throws CommonCustomException {
		this.countExpense = 0;
		try {
			if (newExpClaimFinBean.getActionList().size() > 0
					&& Optional.ofNullable(newExpClaimFinBean.getActionList()).isPresent()) {
				if (Optional.ofNullable(newExpClaimFinBean.getFinanceLogin()).isPresent()) {
					DatEmpProfessionalDetailBO prop = datEmployeeProfessionalRepository
							.findByFkMainEmpDetailId(newExpClaimFinBean.getFinanceLogin());
					if (prop == null)
						throw new CommonCustomException("invalid employee id");
					else {
						List<Short> empRole = Arrays.asList(ExpenseConstants.FINANCE_HEAD,
								ExpenseConstants.FINANCE_MANAGER, ExpenseConstants.FINANCE_SENIOR_EXECUTIVE,
								ExpenseConstants.FINANCE_EXECUTIVE);
						List<DatEmpProfessionalDetailBO> finEmpId = datEmployeeProfessionalRepository
								.fetchListOfEmpFinExp(empRole);
						boolean validFinEmp = finEmpId.stream().anyMatch(
								obj -> obj.getFkMainEmpDetailId().equals(newExpClaimFinBean.getFinanceLogin()));
						if (validFinEmp) {
							this.newExpClaimFinBO = new NewExpClaimFinBO();
							this.newExpClaimFinBO = newExpClaimFinRepository
									.findByPkIdExpClaimFin(newExpClaimFinBean.getPkIdExpClaimFin());
							if (Optional.ofNullable(this.newExpClaimFinBO).isPresent()) {
								DatEmpProfessionalDetailBO dataObj = finEmpId
										.stream().filter(obj -> obj.getFkMainEmpDetailId()
												.intValue() == newExpClaimFinBean.getFinanceLogin().intValue())
										.findFirst().get();
								if (dataObj.getFkEmpOrganizationId()
										.equals(this.newExpClaimFinBO.getFinanceTeam().shortValue())) {
									this.newExpDatExpenseBOList = new ArrayList<NewExpDatExpenseBO>();
									this.newExpDatExpenseBOList = newExpDatExpenseRepository
											.findExpenseByListOfId(newExpClaimFinBean.getActionList());
									if (!this.newExpDatExpenseBOList.isEmpty()) {
										this.newExpDatExpenseBOList.forEach(obj -> {
											if (obj.getFkStatusId().equals(NewExpConstants.Exp_Approved_by_RM))
												++this.countExpense;
										});
										if (this.countExpense == this.newExpDatExpenseBOList.size())
											return true;
										else
											throw new CommonCustomException(
													"Some of the expense are not in approved by RM");
									} else
										throw new CommonCustomException("Expense list is empty");
								} else
									throw new CommonCustomException(
											"Taking Action for this report belongs to other finance team");
							} else
								throw new CommonCustomException("entered claim id is not available");
						} else
							throw new CommonCustomException("login is not finance employee");
					}
				} else
					throw new CommonCustomException("login is mandatory");
			} else
				throw new CommonCustomException("action list is empty");
		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonCustomException(e.getMessage());
		}
		// return false;
	}
}
