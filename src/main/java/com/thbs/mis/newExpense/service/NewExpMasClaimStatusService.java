package com.thbs.mis.newExpense.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.jmapper.JMapper;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.newExpense.bean.NewExpMasClaimStatusBean;
import com.thbs.mis.newExpense.bo.NewExpMasCategoryBO;
import com.thbs.mis.newExpense.bo.NewExpMasClaimStatusBO;
import com.thbs.mis.newExpense.dao.NewExpMasClaimStatusRepository;

@Service
public class NewExpMasClaimStatusService {

	private static final AppLog LOG = LogFactory.getLog(NewExpMasClaimStatusService.class);

	JMapper<NewExpMasClaimStatusBean, NewExpMasClaimStatusBO> newExpMasClaimStatusBeanMapper = new JMapper<>(
			NewExpMasClaimStatusBean.class, NewExpMasClaimStatusBO.class);

	List<NewExpMasClaimStatusBean> newExpMasClaimStatusBeanList;

	List<NewExpMasClaimStatusBO> newExpMasClaimStatusBOList;

	NewExpMasClaimStatusBO newExpMasClaimStatusBO;

	NewExpMasClaimStatusBean newExpMasClaimStatusBean;
	
	@Autowired
	NewExpMasClaimStatusRepository newExpMasClaimStatusRepository;
	
	public List<NewExpMasClaimStatusBO> getExpClaimStaus() throws CommonCustomException {
		this.newExpMasClaimStatusBOList = new ArrayList<NewExpMasClaimStatusBO>();
		try {
			this.newExpMasClaimStatusBOList = newExpMasClaimStatusRepository.findAll();
			return this.newExpMasClaimStatusBOList;
		} catch (Exception e) {
			throw new CommonCustomException("Error occured " + e);
		}
	}
}
