package com.thbs.mis.tax.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.tax.bo.DatTaxDeclartionBO;

@Repository
public interface DatTaxDeclartionRepository extends
		GenericRepository<DatTaxDeclartionBO, Long> {

	List<DatTaxDeclartionBO> findByFkEmpId(Integer fkEmpId);

	public DatTaxDeclartionBO findByPkTaxDeclarationId(Integer pkId);

	@Query("SELECT taxDeclare from DatTaxDeclartionBO taxDeclare"
			+ " WHERE taxDeclare.fkEmpId = ?1 AND taxDeclare.taxSubmittedDate BETWEEN ?2 AND ?3 ")
	public List<DatTaxDeclartionBO> empReviewService(
			@Param("empId") Integer empId, @Param("fromDate") Date fromDate,
			@Param("toDate") Date toDate);

	@Query("SELECT new com.thbs.mis.tax.bo.DatTaxDeclartionBO("
			+ " taxDeclare.pkTaxDeclarationId, taxDeclare.fkEmpId,taxDeclare.taxTypeId, "
			+ " taxDeclare.taxTypeParentId,	"
			+ " taxDeclare.fkTaxDocClearanceId, taxDeclare.taxAmount,		"
			+ " taxDeclare.taxSubmittedDate, type.taxTypeName,	"
			+ "		dateEmpProf.empProfessionalEmailId, dateEmpPer.empFirstName,		"
			+ "	dateEmpPer.empLastName, dateEmpPer.empMiddleName"
			+ " ) FROM "
			+ " DatTaxDeclartionBO taxDeclare JOIN "
			+ " DatEmpPersonalDetailBO dateEmpPer ON dateEmpPer.fkEmpDetailId =  taxDeclare.fkEmpId JOIN "
			+ " DatEmpProfessionalDetailBO dateEmpProf ON dateEmpProf.fkMainEmpDetailId =taxDeclare.fkEmpId "
			+ " JOIN MasTaxTypesBO type ON type.pkTaxTypeId = taxDeclare.taxTypeId"
			+ " WHERE taxDeclare.taxSubmittedDate BETWEEN ?1 AND ?2 "
			+ " GROUP BY taxDeclare.fkEmpId")
	public List<DatTaxDeclartionBO> getAllDetailsFromPersonalAndTaxDeclaration(
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);
	
			
	@Query("SELECT taxDeclare from DatTaxDeclartionBO taxDeclare "
			+ " JOIN MasTaxTypesBO masTypes ON masTypes.pkTaxTypeId = taxDeclare.taxTypeId"
			+ " WHERE taxDeclare.fkEmpId = ?1 AND taxDeclare.taxSubmittedDate BETWEEN ?2 AND ?3 ORDER BY masTypes.displayOrder")
	public List<DatTaxDeclartionBO> empviewService(
			@Param("empId") Integer empId, @Param("fromDate") Date fromDate,
			@Param("toDate") Date toDate);
	
	@Query("SELECT taxDeclare from DatTaxDeclartionBO taxDeclare"
			+ " WHERE taxDeclare.fkEmpId = ?1 AND taxDeclare.taxSubmittedDate BETWEEN ?2 AND ?3 AND taxDeclare.taxTypeParentId = ?4 AND taxDeclare.taxTypeId = ?5")
	public List<DatTaxDeclartionBO> empviewServiceByCategoryId(
			@Param("empId") Integer empId, @Param("fromDate") Date fromDate,
			@Param("toDate") Date toDate,@Param("categoryId") Integer categoryId, @Param("typeId") Integer typeId);

	@Query("SELECT new com.thbs.mis.tax.bo.DatTaxDeclartionBO("
			+ " taxDeclare.pkTaxDeclarationId, taxDeclare.fkEmpId,taxDeclare.taxTypeId, "
			+ " taxDeclare.taxTypeParentId,	"
			+ " taxDeclare.fkTaxDocClearanceId, taxDeclare.taxAmount,		"
			+ " taxDeclare.taxSubmittedDate, type.taxTypeName,	"
			+ "		dateEmpProf.empProfessionalEmailId, dateEmpPer.empFirstName,		"
			+ "	dateEmpPer.empLastName, dateEmpPer.empMiddleName"
			+ " ) FROM "
			+ " DatTaxDeclartionBO taxDeclare JOIN "
			+ " DatEmpPersonalDetailBO dateEmpPer ON dateEmpPer.fkEmpDetailId =  taxDeclare.fkEmpId JOIN "
			+ " DatEmpProfessionalDetailBO dateEmpProf ON dateEmpProf.fkMainEmpDetailId =taxDeclare.fkEmpId "
			+ " JOIN MasTaxTypesBO type ON type.pkTaxTypeId = taxDeclare.taxTypeId"
			+ " WHERE taxDeclare.fkEmpId =?1 "
			+ " GROUP BY taxDeclare.fkEmpId")
	public List<DatTaxDeclartionBO> getAllDetailsFromPersonalAndTaxDeclarationByEmpid(
			@Param("empId") Integer empId);
	
	List<DatTaxDeclartionBO> findByFkEmpIdAndTaxTypeIdAndTaxTypeParentId(Integer fkEmpId,Integer taxTypeId,Integer taxTypeParentId);
	
	@Query("SELECT new com.thbs.mis.tax.bo.DatTaxDeclartionBO("
			+ " taxDeclare.pkTaxDeclarationId, taxDeclare.fkEmpId,taxDeclare.taxTypeId, "
			+ " taxDeclare.taxTypeParentId,	"
			+ " taxDeclare.fkTaxDocClearanceId, taxDeclare.taxAmount,		"
			+ " taxDeclare.taxSubmittedDate, type.taxTypeName,	"
			+ "		dateEmpProf.empProfessionalEmailId, dateEmpPer.empFirstName,		"
			+ "	dateEmpPer.empLastName, dateEmpPer.empMiddleName"
			+ " ) FROM "
			+ " DatTaxDeclartionBO taxDeclare JOIN "
			+ " DatEmpPersonalDetailBO dateEmpPer ON dateEmpPer.fkEmpDetailId =  taxDeclare.fkEmpId JOIN "
			+ " DatEmpProfessionalDetailBO dateEmpProf ON dateEmpProf.fkMainEmpDetailId =taxDeclare.fkEmpId "
			+ " JOIN MasTaxTypesBO type ON type.pkTaxTypeId = taxDeclare.taxTypeId"
			+ " WHERE taxDeclare.fkEmpId =?1 AND taxDeclare.taxSubmittedDate BETWEEN ?2 AND ?3 GROUP BY taxDeclare.fkEmpId"
			)
	public List<DatTaxDeclartionBO> getAllDetailsFromPersonalAndTaxDeclarationByEmpidAndFinancialYear(
			@Param("empId") Integer empId,@Param("fromDate") Date fromDate,
			@Param("toDate") Date toDate);
	
}
