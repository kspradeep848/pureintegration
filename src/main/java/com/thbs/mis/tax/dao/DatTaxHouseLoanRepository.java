package com.thbs.mis.tax.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.tax.bo.DatTaxHouseLoanBO;

@Repository
public interface DatTaxHouseLoanRepository extends GenericRepository<DatTaxHouseLoanBO, Long>{

	DatTaxHouseLoanBO findByFkTaxDeclartionId(Integer declareId);
	
	@Query("SELECT d FROM DatTaxHouseLoanBO d WHERE d.fkTaxDeclartionId = ?")
	List<DatTaxHouseLoanBO> getTaxDeclarationFromDeclarationId(Integer declareId);

}
