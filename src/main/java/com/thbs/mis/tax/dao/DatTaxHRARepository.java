package com.thbs.mis.tax.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.tax.bo.DatTaxHraBO;

@Repository
public interface DatTaxHRARepository extends GenericRepository<DatTaxHraBO, Long>{

	DatTaxHraBO findByFkTaxDeclartionId(Integer declareId);
	
	@Query("SELECT d FROM DatTaxHraBO d WHERE d.fkTaxDeclartionId = ?")
	List<DatTaxHraBO> getTaxDeclarationFromDeclarationId(Integer declareId);

}
