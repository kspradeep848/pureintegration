package com.thbs.mis.tax.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.tax.bo.DatTaxFirstTimeHouseOwnerBO;

@Repository
public interface DatTaxFirstTimeHouseOwnerRepository extends GenericRepository<DatTaxFirstTimeHouseOwnerBO, Long> {

	public DatTaxFirstTimeHouseOwnerBO findByFkTaxDeclarionId(int pkId);
	
	@Query("SELECT d FROM DatTaxFirstTimeHouseOwnerBO d WHERE d.fkTaxDeclarionId = ?")
	public List<DatTaxFirstTimeHouseOwnerBO> getFirstTimeHosueOwnerDetails(int pkId);

}
