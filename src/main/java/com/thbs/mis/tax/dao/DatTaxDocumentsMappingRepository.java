package com.thbs.mis.tax.dao;

import java.util.List;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.SqlResultSetMapping;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.tax.bean.ViewEmployeesTaxDeclaration;
import com.thbs.mis.tax.bo.DatTaxDocumentsMappingBO;

@Repository
public interface DatTaxDocumentsMappingRepository extends GenericRepository<DatTaxDocumentsMappingBO, Long> {

	List<DatTaxDocumentsMappingBO> findByTaxDeclarationId(Integer taxDeclarationId);
	
	List<DatTaxDocumentsMappingBO> findByTaxDeclarationIdAndDocStatus(Integer taxDeclarationId,Integer status);

	List<DatTaxDocumentsMappingBO> findByTaxDeclarationIdInAndDocStatus(List<Integer> taxDeclarationId, Integer status);

//	DatTaxDocumentsMappingBO findByInAndDocKey(Integer taxDeclarationId, String key);

	DatTaxDocumentsMappingBO findByPkDocIdAndDocKey(Integer pkDocId, String key);

	DatTaxDocumentsMappingBO findByPkDocIdIn(List<DatTaxDocumentsMappingBO> listEmpDetailBo);
	
	DatTaxDocumentsMappingBO findByDocKey(String docKeys);
	
	@Query("SELECT d FROM DatTaxDocumentsMappingBO d WHERE d.docName != '' AND d.taxDeclarationId = ? ")
	List<DatTaxDocumentsMappingBO> getAllUploadDocumentList(Integer taxDeclarationId);
	
	@Query("SELECT count(*) FROM DatTaxDocumentsMappingBO docMap JOIN DatTaxDeclartionBO taxDec on docMap.taxDeclarationId = taxDec.pkTaxDeclarationId WHERE docMap.docStatus=:docStatus and docMap.financeYear=:financialYear and taxDec.fkEmpId=:empId ")
	Integer findDocStatusCount(@Param("empId") Integer empId, @Param("financialYear") String financialYear,@Param("docStatus") Integer docStatus);
	
	
	
	@Query("SELECT docMap FROM DatTaxDocumentsMappingBO docMap JOIN DatTaxDeclartionBO taxDec on docMap.taxDeclarationId = taxDec.pkTaxDeclarationId WHERE docMap.docStatus=:docStatus and docMap.financeYear=:financialYear and taxDec.fkEmpId=:empId ")
	List<DatTaxDocumentsMappingBO> findByDocStatusAndEmpIdAndFinancialYear(@Param("empId") Integer empId, @Param("financialYear") String financialYear,@Param("docStatus") Integer docStatus);	
	

}
