package com.thbs.mis.tax.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.tax.bo.DatTaxHraBO;
import com.thbs.mis.tax.bo.DatTaxMedicalInsurancePremiumBO;

@Repository
public interface DatTaxMedicalInsurancePremiumRepository extends GenericRepository<DatTaxMedicalInsurancePremiumBO, Long>{

	DatTaxMedicalInsurancePremiumBO findByFkTaxDeclartionId(Integer declareId);
	
	@Query("SELECT d FROM DatTaxMedicalInsurancePremiumBO d WHERE d.fkTaxDeclartionId = ?")
	List<DatTaxMedicalInsurancePremiumBO> getTaxDeclarationFromDeclarationId(Integer declareId);

}
