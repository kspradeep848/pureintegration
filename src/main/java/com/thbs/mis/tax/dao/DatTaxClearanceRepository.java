package com.thbs.mis.tax.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.tax.bo.DatTaxClearanceBO;

@Repository
public interface DatTaxClearanceRepository extends GenericRepository<DatTaxClearanceBO, Long>{
	
	List<DatTaxClearanceBO> findByFkEmpId(Integer fkEmpId);
	
	DatTaxClearanceBO findByFkEmpIdAndFyStartDateAndFyEndDate(Integer fkEmpId, Date fyStartDate,Date fyEndDate);

	 
}
