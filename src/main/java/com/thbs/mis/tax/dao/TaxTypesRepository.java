package com.thbs.mis.tax.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.tax.bo.MasTaxTypesBO;

@Repository
public interface TaxTypesRepository
		extends GenericRepository<MasTaxTypesBO, Long>{

	public List<MasTaxTypesBO> findByTaxTypeParentId(int typeId);

	public MasTaxTypesBO findByPkTaxTypeId(short typeId);

}
