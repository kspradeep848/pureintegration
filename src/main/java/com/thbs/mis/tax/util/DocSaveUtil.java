package com.thbs.mis.tax.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.thbs.mis.framework.exception.CommonCustomException;

@Configuration
@PropertySource("classpath:mis_tax_declaration.properties")
@Service
public class DocSaveUtil {

	@Value("${emp.doc.save}")
	private String TAX_UPLOAD_DIRECTORY;
 
	/*public boolean upload(Integer empId, Integer taxDecId,Integer typeId, MultipartFile file,
			String financialYeer) throws IOException, CommonCustomException {
		boolean result = false;
		try {
			String uploadDirectory = TAX_UPLOAD_DIRECTORY + File.separator + financialYeer
					+ File.separator+empId+File.separator;
			File directory = new File(uploadDirectory);
			if (directory.exists() == false)
				directory.mkdir();
			else {
				
				// Delete the old file from directory
				 File[] files = directory.listFiles();
				 if (files != null) {
						for (File fileInDir : files) {
							if (fileInDir.isFile()) {
								if (fileInDir.getName().contains(taxDecId.toString()+"_"+typeId+"_")) {
									fileInDir.delete();
									break;
								}
							}
						}
				 }
				 
			}
			
			// Create the new file
			String fileName =  taxDecId+"_"+typeId+"_"+ file.getOriginalFilename();
			File saveFile = new File(uploadDirectory, fileName);
			FileUtils.writeByteArrayToFile(saveFile, file.getBytes());
			result = true;
		} catch (Exception e) {
			result = false;
			throw new CommonCustomException("The tax declaration of "+taxDecId + "is failed and the  file name is " + file.getOriginalFilename());
		}
		return result;
	}*/
	
	/*public boolean deleteFile(Integer empId, Integer decId, Integer typeId, String financialYear) throws CommonCustomException {
		boolean result = false;
		String uploadDirectory = TAX_UPLOAD_DIRECTORY + File.separator +  financialYear
				+ File.separator+empId;
		File directory = new File(uploadDirectory);
		if (directory.exists() == false)
			throw new CommonCustomException("directory not exist");
		else{
			File[] files = new File(uploadDirectory).listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isFile()) {
						if (file.getName().contains(decId.toString()+"_"+typeId.toString())) {
							file.delete();
							result = true;
						}
					}
				}
			} else {
				throw new CommonCustomException("File not found");
			}
		}
		return result;
	}*/
	
	public boolean deleteFile(Integer empId, String fileName, String financialYear) throws CommonCustomException {
		boolean result = false;
		String uploadDirectory = TAX_UPLOAD_DIRECTORY + File.separator + financialYear + File.separator + empId;
		File directory = new File(uploadDirectory);
		if (directory.exists()) {

			File saveFile = new File(uploadDirectory + File.separator + fileName);
			if (saveFile != null && saveFile.exists()) {
				result = saveFile.delete();
				if (directory.list().length == 0)
					directory.delete();

			}

			else {
				throw new CommonCustomException("File not found");
			}
		} 
		return result;
	}
	
	
	/*public boolean uploadReviewDoc(Integer empId, Integer taxDecId, MultipartFile file,
			String financialYeer) throws IOException, CommonCustomException {
		boolean result = false;
		try {
			String uploadDirectory = TAX_UPLOAD_DIRECTORY + File.separator + financialYeer
					+ File.separator+empId+File.separator;
			File directory = new File(uploadDirectory);
			if (directory.exists() == false)
				directory.mkdir();
			else {
				
				// Delete the old file from directory
				 File[] files = directory.listFiles();
				 if (files != null) {
						for (File fileInDir : files) {
							if (fileInDir.isFile()) {
								if (fileInDir.getName().equals(taxDecId.toString()+"_"+ file.getOriginalFilename())) {
									fileInDir.delete();
									break;
								}
							}
						}
				 }
				 
			}
			
			// Create the new file
			String fileName =  taxDecId+"_"+ file.getOriginalFilename();
			File saveFile = new File(uploadDirectory, fileName);
			FileUtils.writeByteArrayToFile(saveFile, file.getBytes());
			result = true;
		} catch (Exception e) {
			result = false;
			throw new CommonCustomException("The tax declaration of "+taxDecId + "is failed and the  file name is " + file.getOriginalFilename());
		}
		return result;
	}
	*/
	
	
	public boolean upload(Integer empId, String fileName, MultipartFile file, String financialYeer)
			throws IOException, CommonCustomException {
		boolean result = false;
		try {
			String uploadDirectory = TAX_UPLOAD_DIRECTORY + File.separator + financialYeer + File.separator + empId;
			File directory = new File(uploadDirectory);
			String extension = FilenameUtils.getExtension(file.getOriginalFilename());
			File saveFile = new File(uploadDirectory + File.separator + fileName + "." + extension);

			if (!directory.exists())
				directory.mkdir();
			else {

				// Delete the old file from directory
				File[] files = directory.listFiles();
				if (files != null) {
					for (File fileInDir : files) {
						if (fileInDir.isFile()) {
							if (fileInDir.getName().equals(fileName)) {
								fileInDir.delete();
								break;
							}
						}
					}
				}

			}

			// Create the new file
			FileUtils.writeByteArrayToFile(saveFile, file.getBytes());
			result = true;

		} catch (Exception e) {
			result = false;
			throw new CommonCustomException(
					"The file upload for declaration failed and the  file name is " + file.getOriginalFilename());
		}
		return result;
	}
	
	
	public String getFileExtension(String fileName) {

		if (fileName != null) {
			String[] arr = fileName.split("\\.");
			if (arr != null && arr.length > 0)
				return arr[arr.length - 1];
		}

		return "";
	}
	
	
	
	public boolean uploadSignedDeclaration(Integer empId, String fileName, MultipartFile file, String financialYeer)
			throws IOException, CommonCustomException {
		boolean result = false;
		try {
			String uploadDirectory = TAX_UPLOAD_DIRECTORY + File.separator + financialYeer + File.separator + empId;
			File directory = new File(uploadDirectory);
			//String extension = FilenameUtils.getExtension(file.getOriginalFilename());
			File saveFile = new File(uploadDirectory + File.separator + fileName);

			if (!directory.exists())
				directory.mkdir();
			else {

				// Delete the old file from directory
				File[] files = directory.listFiles();
				if (files != null) {
					for (File fileInDir : files) {
						if (fileInDir.isFile()) {
							if (fileInDir.getName().equals(fileName)) {
								fileInDir.delete();
								break;
							}
						}
					}
				}

			}

			// Create the new file
			FileUtils.writeByteArrayToFile(saveFile, file.getBytes());
			result = true;

		} catch (Exception e) {
			result = false;
			throw new CommonCustomException(
					"The file upload for declaration failed and the  file name is " + file.getOriginalFilename());
		}
		return result;
	}
};
