package com.thbs.mis.tax.bo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "dat_tax_hra")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "DatTaxHraBO.findAll", query = "SELECT d FROM DatTaxHraBO d") })
public class DatTaxHraBO implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_id")
	private Integer pkId;
	@Column(name = "fk_tax_declartion_id")
	private Integer fkTaxDeclartionId;
	@Column(name = "month")
	private Integer month;
	
	@Column(name = "amount")
	private BigDecimal amount;

	public DatTaxHraBO() {
	}

	public DatTaxHraBO(Integer pkId, Integer fkTaxDeclartionId, Integer month, BigDecimal amount
			 ) {
		super();
		this.pkId = pkId;
		this.fkTaxDeclartionId = fkTaxDeclartionId;
		this.month = month;
		this.amount = amount;
	}

	public Integer getPkId() {
		return pkId;
	}

	public void setPkId(Integer pkId) {
		this.pkId = pkId;
	}

	public Integer getFkTaxDeclartionId() {
		return fkTaxDeclartionId;
	}

	public void setFkTaxDeclartionId(Integer fkTaxDeclartionId) {
		this.fkTaxDeclartionId = fkTaxDeclartionId;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "DatTaxHraBO [pkId=" + pkId + ", fkTaxDeclartionId="
				+ fkTaxDeclartionId + ", month=" + month + "]";
	}

}
