/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thbs.mis.tax.bo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "dat_tax_house_loan")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "DatTaxHouseLoanBO.findAll", query = "SELECT d FROM DatTaxHouseLoanBO d") })
public class DatTaxHouseLoanBO implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_id")
	private Integer pkId;
	@Column(name = "fk_tax_declartion_id")
	private Integer fkTaxDeclartionId;
	@Column(name = "loan_type")
	private Integer loanType;
	@Column(name = "location")
	private String location;
	@Column(name = "is_jointly_owned")
	private String isJointlyOwned;
	@Column(name = "tax_doc_pancard_status")
	private Integer taxDocPancardStatus;
	@Column(name = "loan_amount")
	private BigDecimal loanAmount;
	@Column(name = "tax_doc_loan_statement_status")
	private Integer taxDocLoanStatmentStatus;
	@Column(name = "house_rent_or_lease_amount")
	private BigDecimal houseRentOrLeaseAmount;
	@Column(name = "interest_capital_amount")
	private BigDecimal interestCapitalAmount;
	@Column(name = "municipal_tax_amount")
	private BigDecimal municipalTaxAmount;
	@Column(name = "net_annual_amount")
	private BigDecimal netAnnualAmount;
	@Column(name = "deduction_amont")
	private BigDecimal deductionAmont;
	@Column(name = "pre_construction_interest")
	private BigDecimal preConstructionInterest;

	public DatTaxHouseLoanBO() {
	}

	public DatTaxHouseLoanBO(Integer pkId, Integer fkTaxDeclartionId, Integer loanType, String location,
			String isJointlyOwned, Integer taxDocPancardStatus, BigDecimal loanAmount, Integer taxDocLoanStatmentStatus,
			BigDecimal houseRentOrLeaseAmount, BigDecimal interestCapitalAmount, BigDecimal municipalTaxAmount,
			BigDecimal netAnnualAmount, BigDecimal deductionAmont, BigDecimal preConstructionInterest) {
		super();
		this.pkId = pkId;
		this.fkTaxDeclartionId = fkTaxDeclartionId;
		this.loanType = loanType;
		this.location = location;
		this.isJointlyOwned = isJointlyOwned;
		this.taxDocPancardStatus = taxDocPancardStatus;
		this.loanAmount = loanAmount;
		this.taxDocLoanStatmentStatus = taxDocLoanStatmentStatus;
		this.houseRentOrLeaseAmount = houseRentOrLeaseAmount;
		this.interestCapitalAmount = interestCapitalAmount;
		this.municipalTaxAmount = municipalTaxAmount;
		this.netAnnualAmount = netAnnualAmount;
		this.deductionAmont = deductionAmont;
		this.preConstructionInterest = preConstructionInterest;
	}

	public Integer getPkId() {
		return pkId;
	}

	public void setPkId(Integer pkId) {
		this.pkId = pkId;
	}

	public Integer getFkTaxDeclartionId() {
		return fkTaxDeclartionId;
	}

	public void setFkTaxDeclartionId(Integer fkTaxDeclartionId) {
		this.fkTaxDeclartionId = fkTaxDeclartionId;
	}

	public Integer getLoanType() {
		return loanType;
	}

	public void setLoanType(Integer loanType) {
		this.loanType = loanType;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getIsJointlyOwned() {
		return isJointlyOwned;
	}

	public void setIsJointlyOwned(String isJointlyOwned) {
		this.isJointlyOwned = isJointlyOwned;
	}

	public Integer getTaxDocPancardStatus() {
		return taxDocPancardStatus;
	}

	public void setTaxDocPancardStatus(Integer taxDocPancardStatus) {
		this.taxDocPancardStatus = taxDocPancardStatus;
	}

	public BigDecimal getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(BigDecimal loanAmount) {
		this.loanAmount = loanAmount;
	}

	public Integer getTaxDocLoanStatmentStatus() {
		return taxDocLoanStatmentStatus;
	}

	public void setTaxDocLoanStatmentStatus(Integer taxDocLoanStatmentStatus) {
		this.taxDocLoanStatmentStatus = taxDocLoanStatmentStatus;
	}

	public BigDecimal getHouseRentOrLeaseAmount() {
		return houseRentOrLeaseAmount;
	}

	public void setHouseRentOrLeaseAmount(BigDecimal houseRentOrLeaseAmount) {
		this.houseRentOrLeaseAmount = houseRentOrLeaseAmount;
	}

	public BigDecimal getInterestCapitalAmount() {
		return interestCapitalAmount;
	}

	public void setInterestCapitalAmount(BigDecimal interestCapitalAmount) {
		this.interestCapitalAmount = interestCapitalAmount;
	}

	public BigDecimal getMunicipalTaxAmount() {
		return municipalTaxAmount;
	}

	public void setMunicipalTaxAmount(BigDecimal municipalTaxAmount) {
		this.municipalTaxAmount = municipalTaxAmount;
	}

	public BigDecimal getNetAnnualAmount() {
		return netAnnualAmount;
	}

	public void setNetAnnualAmount(BigDecimal netAnnualAmount) {
		this.netAnnualAmount = netAnnualAmount;
	}

	public BigDecimal getDeductionAmont() {
		return deductionAmont;
	}

	public void setDeductionAmont(BigDecimal deductionAmont) {
		this.deductionAmont = deductionAmont;
	}

	public BigDecimal getPreConstructionInterest() {
		return preConstructionInterest;
	}

	public void setPreConstructionInterest(BigDecimal preConstructionInterest) {
		this.preConstructionInterest = preConstructionInterest;
	}

	@Override
	public String toString() {
		return "DatTaxHouseLoanBO [pkId=" + pkId + ", fkTaxDeclartionId=" + fkTaxDeclartionId + ", loanType=" + loanType
				+ ", location=" + location + ", isJointlyOwned=" + isJointlyOwned + ", taxDocPancardStatus="
				+ taxDocPancardStatus + ", loanAmount=" + loanAmount + ", taxDocLoanStatmentStatus="
				+ taxDocLoanStatmentStatus + ", houseRentOrLeaseAmount=" + houseRentOrLeaseAmount
				+ ", interestCapitalAmount=" + interestCapitalAmount + ", municipalTaxAmount=" + municipalTaxAmount
				+ ", netAnnualAmount=" + netAnnualAmount + ", deductionAmont=" + deductionAmont
				+ ", preConstructionInterest=" + preConstructionInterest + "]";
	}

}
