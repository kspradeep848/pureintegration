/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thbs.mis.tax.bo;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "mas_tax_types")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "MasTaxTypesBO.findAll", query = "SELECT m FROM MasTaxTypesBO m") })
public class MasTaxTypesBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_tax_type_id")
	private Short pkTaxTypeId;

	@Column(name = "tax_type_name")
	private String taxTypeName;

	@Column(name = "tax_type_short_name")
	private String taxTypeShortName;

	@Column(name = "tax_type_parent_id")
	private Integer taxTypeParentId;

	@Column(name = "tax_section")
	private String taxSection;

	@Column(name = "display_order")
	private String displayOrder;

	public MasTaxTypesBO() {
	}

	public Short getPkTaxTypeId() {
		return pkTaxTypeId;
	}

	public void setPkTaxTypeId(Short pkTaxTypeId) {
		this.pkTaxTypeId = pkTaxTypeId;
	}

	public String getTaxTypeName() {
		return taxTypeName;
	}

	public void setTaxTypeName(String taxTypeName) {
		this.taxTypeName = taxTypeName;
	}

	public String getTaxTypeShortName() {
		return taxTypeShortName;
	}

	public void setTaxTypeShortName(String taxTypeShortName) {
		this.taxTypeShortName = taxTypeShortName;
	}

	public Integer getTaxTypeParentId() {
		return taxTypeParentId;
	}

	public void setTaxTypeParentId(Integer taxTypeParentId) {
		this.taxTypeParentId = taxTypeParentId;
	}

	public String getTaxSection() {
		return taxSection;
	}

	public void setTaxSection(String taxSection) {
		this.taxSection = taxSection;
	}

	public String getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(String displayOrder) {
		this.displayOrder = displayOrder;
	}

	@Override
	public String toString() {
		return "MasTaxTypesBO [pkTaxTypeId=" + pkTaxTypeId + ", taxTypeName=" + taxTypeName + ", taxTypeShortName="
				+ taxTypeShortName + ", taxTypeParentId=" + taxTypeParentId + ", taxSection=" + taxSection
				+ ", displayOrder=" + displayOrder + "]";
	}

}
