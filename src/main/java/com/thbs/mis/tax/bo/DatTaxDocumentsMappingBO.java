package com.thbs.mis.tax.bo;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

@Entity
@Table(name = "dat_tax_documents_mapping")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "DatTaxDocumentsMappingBO.findAll", query = "SELECT m FROM DatTaxDocumentsMappingBO m") })
public class DatTaxDocumentsMappingBO {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_doc_id")
	private Integer pkDocId;

	@Column(name = "tax_declaration_id")
	private Integer taxDeclarationId;

	@Column(name = "doc_status")
	private Integer docStatus;

	@Column(name = "doc_key")
	private String docKey;

	@Column(name = "doc_name")
	private String docName;

	@Column(name = "doc_ref_name")
	private String docRefName;

	@Column(name = "finance_year")
	private String financeYear;

	@Column(name = "finance_rejected_comments")
	private String financeRejectedComments;
	
	@JsonSerialize(using = CustomTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date")
	private Date updatedDate;

	public DatTaxDocumentsMappingBO() {
		
		this.updatedDate= new Date();

	}

	public Integer getPkDocId() {
		return pkDocId;
	}

	public void setPkDocId(Integer pkDocId) {
		this.pkDocId = pkDocId;
	}

	public Integer getTaxDeclarationId() {
		return taxDeclarationId;
	}

	public void setTaxDeclarationId(Integer taxDeclarationId) {
		this.taxDeclarationId = taxDeclarationId;
	}

	public Integer getDocStatus() {
		return docStatus;
	}

	public void setDocStatus(Integer docStatus) {
		this.docStatus = docStatus;
	}

	public String getDocKey() {
		return docKey;
	}

	public void setDocKey(String docKey) {
		this.docKey = docKey;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getDocRefName() {
		return docRefName;
	}

	public void setDocRefName(String docRefName) {
		this.docRefName = docRefName;
	}

	public String getFinanceYear() {
		return financeYear;
	}

	public void setFinanceYear(String financeYear) {
		this.financeYear = financeYear;
	}

	public String getFinanceRejectedComments() {
		return financeRejectedComments;
	}

	public void setFinanceRejectedComments(String financeRejectedComments) {
		this.financeRejectedComments = financeRejectedComments;
	}	
	

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "DatTaxDocumentsMappingBO [pkDocId=" + pkDocId + ", taxDeclarationId=" + taxDeclarationId
				+ ", docStatus=" + docStatus + ", docKey=" + docKey + ", docName=" + docName + ", docRefName="
				+ docRefName + ", financeYear=" + financeYear + ", financeRejectedComments=" + financeRejectedComments
				+ ", updatedDate=" + updatedDate + "]";
	}

	

}
