package com.thbs.mis.tax.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "dat_tax_declartion")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "DatTaxDeclartionBO.findAll", query = "SELECT d FROM DatTaxDeclartionBO d") })
public class DatTaxDeclartionBO implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_tax_declaration_id")
	Integer pkTaxDeclarationId;

	@Column(name = "fk_emp_id")
	Integer fkEmpId;

	@Column(name = "tax_type_id")
	Integer taxTypeId;

	@Column(name = "tax_type_parent_id")
	Integer taxTypeParentId;

	@Column(name = "fk_tax_doc_clearance_id")
	Integer fkTaxDocClearanceId;

	// @Column(name = "tax_doc_status")
	// Integer taxDocStatus;

	@Transient
	Integer taxStatus;

	@Column(name = "tax_amount")
	Double taxAmount;

	@Transient
	Integer taxDocCount;

	@Column(name = "tax_submitted_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date taxSubmittedDate;

	@Transient
	private String taxTypeName;
	@Transient
	private String empProfessionalEmailId;

	@Transient
	private String empFirstName;

	@Transient
	private String empLastName;

	@Transient
	private String empMiddleName;

	@Transient
	private String taxSection;

	public DatTaxDeclartionBO() {

	}

	public DatTaxDeclartionBO(Integer pkTaxDeclarationId, Integer fkEmpId, Integer taxTypeId, Integer taxTypeParentId,
			Double taxAmount, String taxSection) {
		super();
		this.pkTaxDeclarationId = pkTaxDeclarationId;
		this.fkEmpId = fkEmpId;
		this.taxTypeId = taxTypeId;
		this.taxTypeParentId = taxTypeParentId;
		this.taxAmount = taxAmount;
		this.taxSection = taxSection;
	}
	
	public DatTaxDeclartionBO(Integer taxTypeId, Integer fkEmpId, String empFirstName, String empMiddleName,
			String empLastName, String empProfessionalEmailId) {
		super();
		this.taxTypeId = taxTypeId;
		this.fkEmpId = fkEmpId;
		this.empFirstName = empFirstName;
		this.empMiddleName = empMiddleName;
		this.empLastName = empLastName;
		this.empProfessionalEmailId = empProfessionalEmailId;
	}

	public DatTaxDeclartionBO(Integer pkTaxDeclarationId, Integer fkEmpId, Integer taxTypeId, Integer taxTypeParentId,
			Integer fkTaxDocClearanceId, Double taxAmount, Date taxSubmittedDate, String taxTypeName,
			String empProfessionalEmailId, String empFirstName, String empLastName, String empMiddleName) {
		super();
		this.pkTaxDeclarationId = pkTaxDeclarationId;
		this.fkEmpId = fkEmpId;
		this.taxTypeId = taxTypeId;
		this.taxTypeParentId = taxTypeParentId;
		this.fkTaxDocClearanceId = fkTaxDocClearanceId;
		// this.taxStatus = taxStatus;
		this.taxAmount = taxAmount;
		// this.taxDocCount = taxDocCount;
		this.taxSubmittedDate = taxSubmittedDate;
		this.taxTypeName = taxTypeName;
		this.empProfessionalEmailId = empProfessionalEmailId;
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		this.empMiddleName = empMiddleName;
	}

	public Integer getPkTaxDeclarationId() {
		return pkTaxDeclarationId;
	}

	public void setPkTaxDeclarationId(Integer pkTaxDeclarationId) {
		this.pkTaxDeclarationId = pkTaxDeclarationId;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public Integer getTaxTypeId() {
		return taxTypeId;
	}

	public void setTaxTypeId(Integer typeId) {
		this.taxTypeId = typeId;
	}

	public Integer getTaxTypeParentId() {
		return taxTypeParentId;
	}

	public void setTaxTypeParentId(Integer taxTypeParentId) {
		this.taxTypeParentId = taxTypeParentId;
	}

	public Integer getFkTaxDocClearanceId() {
		return fkTaxDocClearanceId;
	}

	public void setFkTaxDocClearanceId(Integer fkTaxDocClearanceId) {
		this.fkTaxDocClearanceId = fkTaxDocClearanceId;
	}

	public Integer getTaxStatus() {
		return taxStatus;
	}

	public void setTaxStatus(Integer taxStatus) {
		this.taxStatus = taxStatus;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Integer getTaxDocCount() {
		return taxDocCount;
	}

	public void setTaxDocCount(Integer taxDocCount) {
		this.taxDocCount = taxDocCount;
	}

	public Date getTaxSubmittedDate() {
		return taxSubmittedDate;
	}

	public void setTaxSubmittedDate(Date taxSubmittedDate) {
		this.taxSubmittedDate = taxSubmittedDate;
	}

	public String getTaxTypeName() {
		return taxTypeName;
	}

	public void setTaxTypeName(String taxTypeName) {
		this.taxTypeName = taxTypeName;
	}

	public String getEmpProfessionalEmailId() {
		return empProfessionalEmailId;
	}

	public void setEmpProfessionalEmailId(String empProfessionalEmailId) {
		this.empProfessionalEmailId = empProfessionalEmailId;
	}

	public String getEmpFirstName() {
		return empFirstName;
	}

	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}

	public String getEmpLastName() {
		return empLastName;
	}

	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}

	public String getEmpMiddleName() {
		return empMiddleName;
	}

	public void setEmpMiddleName(String empMiddleName) {
		this.empMiddleName = empMiddleName;
	}

	public String getTaxSection() {
		return taxSection;
	}

	public void setTaxSection(String taxSection) {
		this.taxSection = taxSection;
	}

	@Override
	public String toString() {
		return "DatTaxDeclartionBO [pkTaxDeclarationId=" + pkTaxDeclarationId + ", fkEmpId=" + fkEmpId + ", taxTypeId="
				+ taxTypeId + ", taxTypeParentId=" + taxTypeParentId + ", fkTaxDocClearanceId=" + fkTaxDocClearanceId
				+ ", taxStatus=" + taxStatus + ", taxAmount=" + taxAmount + ", taxDocCount=" + taxDocCount
				+ ", taxSubmittedDate=" + taxSubmittedDate + ", taxTypeName=" + taxTypeName
				+ ", empProfessionalEmailId=" + empProfessionalEmailId + ", empFirstName=" + empFirstName
				+ ", empLastName=" + empLastName + ", empMiddleName=" + empMiddleName + ", taxSection=" + taxSection
				+ "]";
	}

}