/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thbs.mis.tax.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "dat_tax_medical_insurance_premium")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "DatTaxMedicalInsurancePremiumBO.findAll", query = "SELECT d FROM DatTaxMedicalInsurancePremiumBO d") })
public class DatTaxMedicalInsurancePremiumBO implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_id")
	private Integer pkId;
	@Column(name = "fk_tax_declartion_id")
	private Integer fkTaxDeclartionId;
	@Column(name = "premium_amount")
	private BigDecimal premiumAmount;
	@Column(name = "is_parents_insurance")
	private String isParentSInsurance;
	@Column(name = "is_above_age_of_60")
	private String isAboveAgeOf60;
//	@Column(name = "tax_doc_status")
//	private Integer taxDocStatus;

	public DatTaxMedicalInsurancePremiumBO() {
	}

	public DatTaxMedicalInsurancePremiumBO(Integer fkTaxDeclartionId, BigDecimal premiumAmount,
			String isParentSInsurance, String isAboveAgeOf60) {
		super();
		this.fkTaxDeclartionId = fkTaxDeclartionId;
		this.premiumAmount = premiumAmount;
		this.isParentSInsurance = isParentSInsurance;
		this.isAboveAgeOf60 = isAboveAgeOf60;
		 
	}

	public Integer getPkId() {
		return pkId;
	}

	public void setPkId(Integer pkId) {
		this.pkId = pkId;
	}

	public Integer getFkTaxDeclartionId() {
		return fkTaxDeclartionId;
	}

	public void setFkTaxDeclartionId(Integer fkTaxDeclartionId) {
		this.fkTaxDeclartionId = fkTaxDeclartionId;
	}

	public BigDecimal getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(BigDecimal premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

	public String getIsParentSInsurance() {
		return isParentSInsurance;
	}

	public void setIsParentSInsurance(String isParentSInsurance) {
		this.isParentSInsurance = isParentSInsurance;
	}

	public String getIsAboveAgeOf60() {
		return isAboveAgeOf60;
	}

	public void setIsAboveAgeOf60(String isAboveAgeOf60) {
		this.isAboveAgeOf60 = isAboveAgeOf60;
	}

	@Override
	public String toString() {
		return "DatTaxMedicalInsurancePremiumBO [pkId=" + pkId
				+ ", fkTaxDeclartionId=" + fkTaxDeclartionId
				+ ", premiumAmount=" + premiumAmount + ", isParentSInsurance="
				+ isParentSInsurance + ", isAboveAgeOf60=" + isAboveAgeOf60
				+ "]";
	}

//	public Integer getTaxDocStatus() {
//		return taxDocStatus;
//	}
//
//	public void setTaxDocStatus(Integer taxDocStatus) {
//		this.taxDocStatus = taxDocStatus;
//	}

  

}
