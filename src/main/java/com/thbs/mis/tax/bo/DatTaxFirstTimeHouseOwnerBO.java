/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thbs.mis.tax.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author manikandan_chinnasam
 */
@Entity
@Table(name = "dat_tax_first_time_house_owner")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "DatTaxFirstTimeHouseOwnerBO.findAll", query = "SELECT d FROM DatTaxFirstTimeHouseOwnerBO d") })
public class DatTaxFirstTimeHouseOwnerBO implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_id")
	private Integer pkId;
	@Column(name = "fk_tax_declarion_id")
	private Integer fkTaxDeclarionId;
	@Column(name = "is_first_time_home_buyer")
	private String isFirstTimeHomeBuyer;
	@Column(name = "date_of_possession")
	@Temporal(TemporalType.DATE)
	private Date dateOfPossession;
	@Column(name = "property_amount")
	private BigDecimal propertyAmount;
	@Column(name = "sanctioned_amount")
	private BigDecimal sanctionedAmount;

	public DatTaxFirstTimeHouseOwnerBO() {
	}

	public DatTaxFirstTimeHouseOwnerBO(Integer pkId) {
		this.pkId = pkId;
	}

	public Integer getPkId() {
		return pkId;
	}

	public void setPkId(Integer pkId) {
		this.pkId = pkId;
	}

	public Integer getFkTaxDeclarionId() {
		return fkTaxDeclarionId;
	}

	public void setFkTaxDeclarionId(Integer fkTaxDeclarionId) {
		this.fkTaxDeclarionId = fkTaxDeclarionId;
	}

	public String getIsFirstTimeHomeBuyer() {
		return isFirstTimeHomeBuyer;
	}

	public void setIsFirstTimeHomeBuyer(String isFirstTimeHomeBuyer) {
		this.isFirstTimeHomeBuyer = isFirstTimeHomeBuyer;
	}

	public Date getDateOfPossession() {
		return dateOfPossession;
	}

	public void setDateOfPossession(Date dateOfPossession) {
		this.dateOfPossession = dateOfPossession;
	}

	public BigDecimal getPropertyAmount() {
		return propertyAmount;
	}

	public void setPropertyAmount(BigDecimal propertyAmount) {
		this.propertyAmount = propertyAmount;
	}

	public BigDecimal getSanctionedAmount() {
		return sanctionedAmount;
	}

	public void setSanctionedAmount(BigDecimal sanctionedAmount) {
		this.sanctionedAmount = sanctionedAmount;
	}

	@Override
	public String toString() {
		return "DatTaxFirstTimeHouseOwnerBO [pkId=" + pkId + ", fkTaxDeclarionId=" + fkTaxDeclarionId
				+ ", isFirstTimeHomeBuyer=" + isFirstTimeHomeBuyer + ", dateOfPossession=" + dateOfPossession
				+ ", propertyAmount=" + propertyAmount + ", sanctionedAmount=" + sanctionedAmount + "]";
	}

}
