/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thbs.mis.tax.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "dat_tax_clearance")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "DatTaxClearanceBO.findAll", query = "SELECT d FROM DatTaxClearanceBO d") })
public class DatTaxClearanceBO implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pk_tax_clearance_id")
	private Integer pkTaxClearanceId;

	@Column(name = "fk_emp_id")
	private Integer fkEmpId;

	@Column(name = "fy_start_date")
	@Temporal(TemporalType.DATE)
	private Date fyStartDate;

	@Column(name = "fy_end_date")
	@Temporal(TemporalType.DATE)
	private Date fyEndDate;

	@Column(name = "is_emp_doc_clearance")
	private String isEmpDocClearnce;

	@Column(name = "modified_date")
	@Temporal(TemporalType.DATE)
	private Date modifiedDate;

	@Column(name = "modified_by_emp_id")
	private Integer modifiedByEmpId;

	public DatTaxClearanceBO() {
	}

	public DatTaxClearanceBO(Integer pkTaxClearanceId, Integer fkEmpId, Date fyStartDate, Date fyEndDate,
			String isEmpDocClearnce, Date modifiedDate, Integer modifiedByEmpId) {
		super();
		this.pkTaxClearanceId = pkTaxClearanceId;
		this.fkEmpId = fkEmpId;
		this.fyStartDate = fyStartDate;
		this.fyEndDate = fyEndDate;
		this.isEmpDocClearnce = isEmpDocClearnce;
		this.modifiedDate = modifiedDate;
		this.modifiedByEmpId = modifiedByEmpId;
	}

	public Integer getPkTaxClearanceId() {
		return pkTaxClearanceId;
	}

	public void setPkTaxClearanceId(Integer pkTaxClearanceId) {
		this.pkTaxClearanceId = pkTaxClearanceId;
	}

	public Integer getFkEmpId() {
		return fkEmpId;
	}

	public void setFkEmpId(Integer fkEmpId) {
		this.fkEmpId = fkEmpId;
	}

	public Date getFyStartDate() {
		return fyStartDate;
	}

	public void setFyStartDate(Date fyStartDate) {
		this.fyStartDate = fyStartDate;
	}

	public Date getFyEndDate() {
		return fyEndDate;
	}

	public void setFyEndDate(Date fyEndDate) {
		this.fyEndDate = fyEndDate;
	}

	public String getIsEmpDocClearnce() {
		return isEmpDocClearnce;
	}

	public void setIsEmpDocClearnce(String isEmpDocClearnce) {
		this.isEmpDocClearnce = isEmpDocClearnce;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getModifiedByEmpId() {
		return modifiedByEmpId;
	}

	public void setModifiedByEmpId(Integer modifiedByEmpId) {
		this.modifiedByEmpId = modifiedByEmpId;
	}

	@Override
	public String toString() {
		return "DatTaxClearanceBO [pkTaxClearanceId=" + pkTaxClearanceId + ", fkEmpId=" + fkEmpId + ", fyStartDate="
				+ fyStartDate + ", fyEndDate=" + fyEndDate + ", isEmpDocClearnce=" + isEmpDocClearnce
				+ ", modifiedDate=" + modifiedDate + ", modifiedByEmpId=" + modifiedByEmpId + "]";
	}

}
