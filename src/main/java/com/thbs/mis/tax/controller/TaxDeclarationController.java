package com.thbs.mis.tax.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Optional;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.tax.bean.DateRangeBean;
import com.thbs.mis.tax.bean.DeleteEmployeeDataBean;
import com.thbs.mis.tax.bean.EmpTaxDeclarationPdfOutputBean;
import com.thbs.mis.tax.bean.EmployeeApprovedStatusBean;
import com.thbs.mis.tax.bean.HRADocumentsRefBean;
import com.thbs.mis.tax.bean.TaxEmpReviewBean;
import com.thbs.mis.tax.bean.ViewEmployeeInputBean;
import com.thbs.mis.tax.bean.ViewEmployeeTaxDeclarationReviewBean;
import com.thbs.mis.tax.bean.ViewEmployeesTaxDeclaration;
import com.thbs.mis.tax.bo.DatTaxClearanceBO;
import com.thbs.mis.tax.bo.DatTaxDeclartionBO;
import com.thbs.mis.tax.bo.DatTaxFirstTimeHouseOwnerBO;
import com.thbs.mis.tax.bo.DatTaxHouseLoanBO;
import com.thbs.mis.tax.bo.DatTaxHraBO;
import com.thbs.mis.tax.bo.DatTaxMedicalInsurancePremiumBO;
import com.thbs.mis.tax.bo.MasTaxTypesBO;
import com.thbs.mis.tax.constants.TaxDeclarationConstants;
import com.thbs.mis.tax.service.TaxDeclarationService;

import io.swagger.annotations.Api;

@Api(value = "Common Operations", description = "Common Operations", basePath = "http://localhost:8080/", consumes = "Rest Service", position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "Common Services")
@Controller
public class TaxDeclarationController {
	
	private static final AppLog LOG = LogFactory.getLog(TaxDeclarationService.class);

	@Autowired
	private TaxDeclarationService taxService;

	ObjectMapper objectMapper = new ObjectMapper();

	@RequestMapping(value = TaxDeclarationConstants.GET_TAX_TYPE_BY_CATEGORY_ID, method = RequestMethod.GET)
	@ResponseBody
	ResponseEntity<?> getTaxTypeByCategory(@PathVariable Integer typeId) {
		List<MasTaxTypesBO> masTaxTypesBOList = taxService.getTaxTypeByCategory(typeId);
		if (Optional.of(masTaxTypesBOList).isPresent()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Tax types as below.", masTaxTypesBOList));
		} else
			return ResponseEntity.status(HttpStatus.CREATED.value())
					.body(new MISResponse(HttpStatus.CREATED.value(), MISConstants.SUCCESS, "NO RECORD FOUND"));
	}

	@RequestMapping(value = TaxDeclarationConstants.CREATE_TAX_HOUSE_LOAN, method = RequestMethod.POST)
	@ResponseBody
	ResponseEntity<MISResponse> createHouseLoan(@PathVariable("categoryId") Integer categoryId,
			@PathVariable("typeId") Integer typeId,
			@RequestParam(value = "taxDeclarationJson", name = "taxDeclarationJson", required = true, defaultValue = "") String taxDeclarationJson,
			@RequestParam(value = "houseLoanJson", name = "houseLoanJson", required = true, defaultValue = "") String inputJson,
			@RequestParam(value = "doc", name = "doc", required = false) MultipartFile uploadSupportDocs,
			@RequestParam(value = "doc1",name =  "doc1",required = false) MultipartFile uploadAdditionalDocs, @RequestParam(value = "loanDocRefKey",name =  "loanDocRefKey",required = false) String loanDocRefKey,@RequestParam(value = "supportDocRefKey",name =  "supportDocRefKey",required = false) String supportDocRefKey
			)
			throws JsonParseException, JsonMappingException, IOException, CommonCustomException {
		DatTaxHouseLoanBO datTaxHouseLoanBO = new DatTaxHouseLoanBO();
		datTaxHouseLoanBO = objectMapper.readValue(inputJson, DatTaxHouseLoanBO.class);
		

		DatTaxDeclartionBO datTaxDeclartionBO = new DatTaxDeclartionBO();
		try {
			datTaxDeclartionBO = objectMapper.readValue(taxDeclarationJson, DatTaxDeclartionBO.class);
			if (datTaxDeclartionBO != null) {
				datTaxDeclartionBO.setTaxTypeParentId(categoryId);
				datTaxDeclartionBO.setTaxTypeId(typeId);
			}

		} catch (Exception e) {
			throw new CommonCustomException("Invalid parsing  taxDeclarationJson");
		}
		if (datTaxDeclartionBO.getPkTaxDeclarationId() == null) {
			if (uploadSupportDocs == null)
				throw new CommonCustomException("uploadSupportDocs is required");
		}
		
		String loanKey = null;
		if(loanDocRefKey!=null && loanDocRefKey.length() > 0) {
			loanKey = objectMapper.readValue(loanDocRefKey, String.class);
		}
		
		String supportDocKey = null;
		if( supportDocRefKey!=null && supportDocRefKey.length() > 0) {
			supportDocKey = objectMapper.readValue(supportDocRefKey, String.class);
		}
		
		LOG.debug("loanKey : "+loanKey);
		
		LOG.debug("supportDocKey : "+supportDocKey);
		List<DatTaxHouseLoanBO> DatTaxHouseLoanList = this.taxService.createHouseLoan(datTaxHouseLoanBO,
				datTaxDeclartionBO, uploadSupportDocs,uploadAdditionalDocs,loanKey,supportDocKey);

		if (Optional.of(DatTaxHouseLoanList).isPresent()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Tax types as below.", DatTaxHouseLoanList));
		} else
			return ResponseEntity.status(HttpStatus.CREATED.value())
					.body(new MISResponse(HttpStatus.CREATED.value(), MISConstants.SUCCESS, "NO RECORD FOUND"));
	}

	@RequestMapping(value = TaxDeclarationConstants.CREATE_TAX_FIRST_TIME_HOUSE_OWNER, method = RequestMethod.POST)
	@ResponseBody
	ResponseEntity<MISResponse> createFirstTimeHouseOwner(@PathVariable("categoryId") Integer categoryId,
			@PathVariable("typeId") Integer typeId,
			@RequestParam(value = "taxDeclarationJson", name = "taxDeclarationJson", required = true, defaultValue = "") String taxDeclarationJson,
			@RequestParam(value = "fthwJson", name = "fthwJson", required = true, defaultValue = "") String fthwJson,
			@RequestParam(value = "doc", name = "doc", required = false) MultipartFile uploadSupportDocs,
			@RequestParam(value = "docRefKey", name = "docRefKey", required = false) String docRefKey)
			throws JsonParseException, JsonMappingException, IOException, CommonCustomException {

		DatTaxFirstTimeHouseOwnerBO datTaxFirstTimeHouseOwnerBO = new DatTaxFirstTimeHouseOwnerBO();
		datTaxFirstTimeHouseOwnerBO = objectMapper.readValue(fthwJson, DatTaxFirstTimeHouseOwnerBO.class);

		DatTaxDeclartionBO datTaxDeclartionBO = new DatTaxDeclartionBO();
		try {
			datTaxDeclartionBO = objectMapper.readValue(taxDeclarationJson, DatTaxDeclartionBO.class);
			if (datTaxDeclartionBO != null) {
				datTaxDeclartionBO.setTaxTypeParentId(categoryId);
				datTaxDeclartionBO.setTaxTypeId(typeId);
			}
			if (datTaxDeclartionBO.getPkTaxDeclarationId() == null) {
				if (uploadSupportDocs == null)
					throw new CommonCustomException("doc upload is required");
			}
		} catch (Exception e) {
			throw new CommonCustomException("Invalid parsing  taxDeclarationJson");
		}
		String key = null;
		if (docRefKey != null && docRefKey.length() > 0) {
			key = objectMapper.readValue(docRefKey, String.class);
		}
		List<DatTaxFirstTimeHouseOwnerBO> firstHouseOwnerList = taxService
				.createFirstTimeHouseOwner(datTaxFirstTimeHouseOwnerBO, datTaxDeclartionBO, uploadSupportDocs, key);
		if (Optional.of(firstHouseOwnerList).isPresent()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "House Owner details are as below.", firstHouseOwnerList));
		} else
			return ResponseEntity.status(HttpStatus.CREATED.value())
					.body(new MISResponse(HttpStatus.CREATED.value(), MISConstants.SUCCESS, "NO RECORD FOUND"));

	}

	@RequestMapping(value = TaxDeclarationConstants.CREATE_TAX_MEDICAL_INSURANCE_PREMIUM, method = RequestMethod.POST)
	@ResponseBody
	ResponseEntity<MISResponse> createMedicalInsurancePremium(@PathVariable("categoryId") Integer categoryId,
			@PathVariable("typeId") Integer typeId,
			@RequestParam(value = "taxDeclarationJson", name = "taxDeclarationJson", required = true, defaultValue = "") String taxDeclarationJson,
			@RequestParam(value = "mipJson", name = "mipJson", required = true, defaultValue = "") String mipJson,
			@RequestParam(value = "doc", name = "doc", required = false) MultipartFile doc,@RequestParam(value = "docRefKey", name = "docRefKey", required = false) String docRefKey)
			throws JsonParseException, JsonMappingException, IOException, CommonCustomException {
		
		DatTaxMedicalInsurancePremiumBO datTaxMedicalInsurancePremiumBO = new DatTaxMedicalInsurancePremiumBO();
		datTaxMedicalInsurancePremiumBO = objectMapper.readValue(mipJson, DatTaxMedicalInsurancePremiumBO.class);
		DatTaxDeclartionBO datTaxDeclartionBO = new DatTaxDeclartionBO();
		try {
			datTaxDeclartionBO = objectMapper.readValue(taxDeclarationJson, DatTaxDeclartionBO.class);
			if (datTaxDeclartionBO != null) {
				datTaxDeclartionBO.setTaxTypeParentId(categoryId);
				datTaxDeclartionBO.setTaxTypeId(typeId);
			}
			if (datTaxDeclartionBO.getPkTaxDeclarationId() == null) {
				if (doc == null) {
					throw new CommonCustomException("doc upload is required");
				}
			}
		} catch (Exception e) {
			throw new CommonCustomException("Invalid parsing  taxDeclarationJson");
		}
		String key = null;
		if(docRefKey!=null && docRefKey.length() > 0) {
		 key = objectMapper.readValue(docRefKey, String.class);
		}
		List<DatTaxMedicalInsurancePremiumBO> medicalInsuranceList = taxService
				.createMedicalInsurance(datTaxMedicalInsurancePremiumBO, datTaxDeclartionBO, doc,key);
		if (Optional.of(medicalInsuranceList).isPresent()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Premium Insurance details are as below.", medicalInsuranceList));
		} else
			return ResponseEntity.status(HttpStatus.CREATED.value())
					.body(new MISResponse(HttpStatus.CREATED.value(), MISConstants.SUCCESS, "NO RECORD FOUND"));
	}

	@RequestMapping(value = TaxDeclarationConstants.CREATE_TAX_HRA, method = RequestMethod.POST)
	@ResponseBody
	ResponseEntity<MISResponse> createTaxHRA(@PathVariable("categoryId") Integer categoryId,
			@PathVariable("typeId") Integer typeId,
			@RequestParam(value = "taxDeclarationJson", name = "taxDeclarationJson", required = true, defaultValue = "") String taxDeclarationJson,
			@RequestParam(value = "hraJson", name = "hraJson", required = true, defaultValue = "") String hraJson,
			@RequestParam(value = "rentReceipt", name = "rentReceipt", required = false) MultipartFile rentReceipt,
			@RequestParam(value = "rentAgreement", name = "rentAgreement", required = false) MultipartFile rentAgreement,
			@RequestParam(value = "utilityBill", name = "utilityBill", required = false) MultipartFile utilityBill,
			@RequestParam(value = "panCard", name = "panCard", required = false) MultipartFile panCard,@RequestParam(value = "refKeyJson", name = "refKeyJson", required = false, defaultValue = "") String refKeyJson)
			throws JsonParseException, JsonMappingException, IOException, CommonCustomException {
		if (taxDeclarationJson == null)
			throw new CommonCustomException("taxDeclarationJson required");
		if (hraJson == null)
			throw new CommonCustomException("hraJson required");
		List output = new ArrayList();
		if (taxDeclarationJson != null) {
			DatTaxDeclartionBO datTaxDeclartionBO = new DatTaxDeclartionBO();
			List<DatTaxHraBO> datTaxHra = new ArrayList<DatTaxHraBO>();
			try {
				datTaxDeclartionBO = objectMapper.readValue(taxDeclarationJson, DatTaxDeclartionBO.class);
				datTaxDeclartionBO.setTaxTypeParentId(categoryId);
				datTaxDeclartionBO.setTaxTypeId(typeId);
			} catch (Exception e) {
				throw new CommonCustomException("Invalid parsing  taxDeclarationJson");
			}
			try {
				datTaxHra = objectMapper.readValue(hraJson,
						objectMapper.getTypeFactory().constructCollectionType(List.class, DatTaxHraBO.class));
			} catch (Exception e) {
				
				throw new CommonCustomException("Invalid parsing  hraJson");
			}
			if (datTaxDeclartionBO.getPkTaxDeclarationId() == null) {
				if (rentReceipt == null)
					throw new CommonCustomException("rentReceipt required");
				if (rentAgreement == null)
					throw new CommonCustomException("rentAgreement required");
				if (utilityBill == null)
					throw new CommonCustomException("utilityBill required");

			}
			
			HRADocumentsRefBean bean = null;
			if (refKeyJson !=null && refKeyJson.length() >0) {
				bean = objectMapper.readValue(refKeyJson, HRADocumentsRefBean.class);
			}
			if (datTaxDeclartionBO != null && datTaxHra.size() > 0)
				output = taxService.createTaxHra(datTaxDeclartionBO, datTaxHra, rentReceipt, rentAgreement, utilityBill,
						panCard,bean);
			else
				return ResponseEntity.status(HttpStatus.CREATED.value())
						.body(new MISResponse(HttpStatus.CREATED.value(), MISConstants.SUCCESS, "unable to save"));
			if (output.size() > 0) {
				return ResponseEntity.status(HttpStatus.CREATED.value()).body(new MISResponse(
						HttpStatus.CREATED.value(), MISConstants.SUCCESS, "Sucessfully saved to database", output));
			} else {
				return ResponseEntity.status(HttpStatus.CREATED.value())
						.body(new MISResponse(HttpStatus.CREATED.value(), MISConstants.FAILURE, "Failed to save"));
			}
		}
		return ResponseEntity.status(HttpStatus.CREATED.value())
				.body(new MISResponse(HttpStatus.CREATED.value(), MISConstants.SUCCESS, "Failed to save"));
	}

	@RequestMapping(value = TaxDeclarationConstants.CREATE_TAX_CLEARANCE, method = RequestMethod.POST)
	@ResponseBody
	ResponseEntity<MISResponse> createTaxClearance(@RequestBody DatTaxClearanceBO datTaxClearanceBO) throws CommonCustomException {
		List<DatTaxClearanceBO> datTaxClearanceBOList = taxService.createTaxClearance(datTaxClearanceBO);
		if (Optional.of(datTaxClearanceBOList).isPresent()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Tax HRA details are as below.", datTaxClearanceBOList));
		} else
			return ResponseEntity.status(HttpStatus.CREATED.value())
					.body(new MISResponse(HttpStatus.CREATED.value(), MISConstants.SUCCESS, "NO RECORD FOUND"));
	}

	@RequestMapping(value = TaxDeclarationConstants.CREATE_TAX_DECLARATION, method = RequestMethod.POST)
	@ResponseBody
	ResponseEntity<MISResponse> createTaxDeclaration(@PathVariable("categoryId") Integer categoryId,
			@PathVariable("typeId") Integer typeId,
			@RequestParam(value = "inputJson", name = "inputJson", required = true, defaultValue = "") String inputJson,
			@RequestParam(value = "doc", name = "doc", required = false) MultipartFile uploadSupportDocs,@RequestParam(value = "docRefKey", name = "docRefKey", required = false, defaultValue = "") String docRefKey)
			throws JsonParseException, JsonMappingException, IOException, CommonCustomException {
		DatTaxDeclartionBO datTaxDeclartionBO = new DatTaxDeclartionBO();
		try {
			datTaxDeclartionBO = objectMapper.readValue(inputJson, DatTaxDeclartionBO.class);
		} catch (Exception e) {
			throw new CommonCustomException("Invalid parsing  taxDeclarationJson");
		}
		if (datTaxDeclartionBO.getPkTaxDeclarationId() == null) {
			if (categoryId.intValue() != 5 && typeId != 3)
				if (uploadSupportDocs == null)
					throw new CommonCustomException("uploadSupportDocs upload is  required");
		}
		datTaxDeclartionBO.setTaxTypeParentId(categoryId);
		datTaxDeclartionBO.setTaxTypeId(typeId);
		String key = null;
		if(docRefKey!=null && docRefKey.length() > 0) {
		 key = objectMapper.readValue(docRefKey, String.class);
		}
		
		List<DatTaxDeclartionBO> datTaxDeclartionList = taxService.createTaxDeclaration(datTaxDeclartionBO,
				uploadSupportDocs,key);
		if (Optional.of(datTaxDeclartionList).isPresent()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Tax declration has been saved successfully.", datTaxDeclartionList));
		} else
			return ResponseEntity.status(HttpStatus.CREATED.value())
					.body(new MISResponse(HttpStatus.CREATED.value(), MISConstants.SUCCESS, "NO RECORD FOUND"));

	}

	@RequestMapping(value = TaxDeclarationConstants.TAX_UPLOAD_EMP_SIGNATURE, method = RequestMethod.POST)
	@ResponseBody
	ResponseEntity<MISResponse> uploadSignature(@PathVariable("empId") Integer empId,
			@RequestParam(value = "taxDecIds", name = "taxDecIds", required = true, defaultValue = "") String taxDecIds,
			@RequestParam(value = "signatureDoc", name = "signatureDoc", required = false) MultipartFile signatureDoc)
			throws IOException, CommonCustomException {
		if (signatureDoc == null) {
			throw new CommonCustomException("signatureDoc upload is required");
		}
		String[] taxDeclarationIds = taxDecIds.split(",");
		boolean isUploadedSignatureDoc = taxService.uploadTaxDocWithSignature(empId, taxDeclarationIds, signatureDoc);
		if (isUploadedSignatureDoc)
			return ResponseEntity.status(HttpStatus.CREATED.value()).body(new MISResponse(HttpStatus.CREATED.value(),
					MISConstants.SUCCESS, "tax doc with Signature  uploaded successfully"));
		else
			return ResponseEntity.status(HttpStatus.CREATED.value()).body(new MISResponse(HttpStatus.CREATED.value(),
					MISConstants.FAILURE, "tax doc with Signature  uploaded failed"));
	}

	@RequestMapping(value = TaxDeclarationConstants.DELETE_DATA, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> deleteEmpTaxData(@RequestBody DeleteEmployeeDataBean bean)
			throws DataAccessException, CommonCustomException, IOException {

		Boolean success = false;

		try {
			success = taxService.deleteEmpTaxData(bean);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}

		if (success == true) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "Tax  Records deleted Succefully"));
		} else {
			return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED.value())
					.body(new MISResponse(HttpStatus.PRECONDITION_FAILED.value(), MISConstants.FAILURE,
							"Unable to delete the tax records.Kindly contact to MIS team."));
		}
	}

	@RequestMapping(value = TaxDeclarationConstants.EMP_REVIEW_SERVICE, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> getAllEmpDetailsONDateRange(@RequestBody TaxEmpReviewBean bean)
			throws DataAccessException, BusinessException, CommonCustomException, ParseException {

		List<ViewEmployeeTaxDeclarationReviewBean> listOfEmpRequest = new ArrayList<ViewEmployeeTaxDeclarationReviewBean>();
		try {

			listOfEmpRequest = taxService.viewAllEmpDetailsONDateRange(bean);
		} catch (CommonCustomException e) {
			throw new BusinessException(e.getMessage());
		}

		if (listOfEmpRequest.isEmpty()) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Employees List is empty", listOfEmpRequest));
		} else {

			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Employees List retrieved successfully", listOfEmpRequest));
		}

	}

	@RequestMapping(value = TaxDeclarationConstants.VIEW_ALL_TAX_DECLARATION_DETAILS, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> getTaxDeclarationDetails(@RequestBody TaxEmpReviewBean bean)
			throws BusinessException {
		List<ViewEmployeesTaxDeclaration> viewDetails = null;
		try {
			viewDetails = taxService.getEmployeeTaxDetails(bean);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		if (!viewDetails.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Employee Tax Details List Retrieved succefully", viewDetails));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "No Details found"));
		}
	}

	@RequestMapping(value = TaxDeclarationConstants.EMP_VIEW_SERVICE, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> taxEmpViewService(@RequestBody ViewEmployeeInputBean bean)
			throws BusinessException, ParseException {
		List<ViewEmployeeTaxDeclarationReviewBean> listOfEmpRequest = new ArrayList<ViewEmployeeTaxDeclarationReviewBean>();
		try {

			listOfEmpRequest = taxService.empViewService(bean);
		} catch (CommonCustomException e) {
			throw new BusinessException(e.getMessage());
		}

		if (listOfEmpRequest.isEmpty()) {

			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Employees List is empty", listOfEmpRequest));
		} else {

			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Employees List retrieved successfully", listOfEmpRequest));
		}

	}

	@RequestMapping(value = TaxDeclarationConstants.DOWNLOAD_JOINT_DECLARATION, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> downloadJointDeclarationForm(HttpServletResponse response)
			throws DataAccessException {
		Boolean isDownloaded = false;
		try {
			isDownloaded = taxService.downloadJointDeclarationForm(response);
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		if (isDownloaded) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "File downloaded"));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.FAILURE, "No file found"));
		}
	}

	@RequestMapping(value = TaxDeclarationConstants.UPDATE_DOC_STATUS, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> updateStatusDoc(@RequestBody List<EmployeeApprovedStatusBean> bean)
			throws DataAccessException, BusinessException, CommonCustomException, ParseException {
		boolean result;
		List<ViewEmployeeTaxDeclarationReviewBean> listOfEmpRequest = new ArrayList<ViewEmployeeTaxDeclarationReviewBean>();
		try {

			result = taxService.updateStatusDoc(bean);
		} catch (CommonCustomException e) {
			throw new BusinessException(e.getMessage());
		}

		if (result == false) {

			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "update doc failed"));
		} else {

			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "doc updated successfully"));
		}

	}

	@RequestMapping(value = TaxDeclarationConstants.GET_DOC_DOWNLOAD, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getDocDownload(HttpServletResponse response,
			@PathVariable("key") String key) throws BusinessException, CommonCustomException, IOException {

		boolean isReportGenerated = false;
		isReportGenerated = taxService.getDocDownload(key, response);
		if (isReportGenerated) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "doc has been downloaded"));

		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "Key is not found"));
		}
	}

	@RequestMapping(value = TaxDeclarationConstants.DOWNLOAD_EMP_REVIEW_PDF, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> downloadEmpReviewPdf(@RequestBody EmpTaxDeclarationPdfOutputBean taxInputBean,
			HttpServletResponse response) throws CommonCustomException, IOException {
		boolean successFlag = false;
		successFlag = taxService.downloadTaxEmpReviewPdf(taxInputBean, response);

		if (successFlag)
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "Employee Review Tax Pdf downloaded succefully"));
		else
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
					.body(new MISResponse(HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "No Pdf Found"));
	}

	@RequestMapping(value = TaxDeclarationConstants.TIME_RANGE_STATUS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> settingDateStatus() throws DataAccessException {
		DateRangeBean bean = new DateRangeBean();
		List<DateRangeBean> listBean = new ArrayList<DateRangeBean>();
		try {
			bean = taxService.getDateRangeStatusFlag();
			listBean.add(bean);
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		if (listBean.size() > 0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(new MISResponse(HttpStatus.OK.value(),
					MISConstants.SUCCESS, "setting status is successfull", listBean));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(new MISResponse(
					HttpStatus.NOT_FOUND.value(), MISConstants.FAILURE, "setting date status is failure"));
		}
	}
	
	
	@RequestMapping(value = TaxDeclarationConstants.DOWNLOAD_FORM_12C, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> downloadForm12C(HttpServletResponse response)
			throws DataAccessException {
		Boolean isDownloaded = false;
		try {
			isDownloaded = taxService.downloadForm12C(response);
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		if (isDownloaded) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "File downloaded"));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.FAILURE, "No file found"));
		}
	}
	
	
	@RequestMapping(value = TaxDeclarationConstants.DOWNLOAD_FINANCE_EMP_REVIEW_PDF, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> downloadDeclarationFormForFinance(HttpServletResponse response,@RequestBody TaxEmpReviewBean bean)
			throws DataAccessException {
		Boolean isDownloaded = false;
		try {
			isDownloaded = taxService.downloadDeclarationFormForFinance(response,bean);
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		if (isDownloaded) {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS, "File downloaded"));
		} else {
			return ResponseEntity.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(), MISConstants.FAILURE, "No file found"));
		}
	}

}
