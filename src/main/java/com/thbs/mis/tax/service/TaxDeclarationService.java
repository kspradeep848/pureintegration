package com.thbs.mis.tax.service;

import static com.thbs.mis.tax.constants.TaxDeclarationConstants.FILE_NAME_SEPARATOR;
import static com.thbs.mis.tax.constants.TaxDeclarationConstants.FIRST_TIME_HOUSE_OWNER_KEY;
import static com.thbs.mis.tax.constants.TaxDeclarationConstants.HOUSE_LOAN_KEY;
import static com.thbs.mis.tax.constants.TaxDeclarationConstants.JOINT_DECLARATION_KEY;
import static com.thbs.mis.tax.constants.TaxDeclarationConstants.MEDICAL_INSURANCE_KEY;
import static com.thbs.mis.tax.constants.TaxDeclarationConstants.PAN_HOUSE_OWNER_KEY;
import static com.thbs.mis.tax.constants.TaxDeclarationConstants.RENT_AGREEMENT_KEY;
import static com.thbs.mis.tax.constants.TaxDeclarationConstants.RENT_RECEIPTS_KEY;
import static com.thbs.mis.tax.constants.TaxDeclarationConstants.UTILITY_BILL_KEY;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.report.Util.DownloadFileUtil;
import com.thbs.mis.framework.report.pdf.CreatePdfFileUtil;
import com.thbs.mis.tax.bean.DateRangeBean;
import com.thbs.mis.tax.bean.DeleteEmployeeDataBean;
import com.thbs.mis.tax.bean.EmpTaxDeclarationPdfOutputBean;
import com.thbs.mis.tax.bean.EmpTaxDeclarationReviewPdfOutputBean;
import com.thbs.mis.tax.bean.EmployeeApprovedStatusBean;
import com.thbs.mis.tax.bean.HRADocumentsRefBean;
import com.thbs.mis.tax.bean.TaxEmpReviewBean;
import com.thbs.mis.tax.bean.TaxFinancialYearBean;
import com.thbs.mis.tax.bean.ViewEmployeeInputBean;
import com.thbs.mis.tax.bean.ViewEmployeeTaxDeclarationReviewBean;
import com.thbs.mis.tax.bean.ViewEmployeesTaxDeclaration;
import com.thbs.mis.tax.bo.DatTaxClearanceBO;
import com.thbs.mis.tax.bo.DatTaxDeclartionBO;
import com.thbs.mis.tax.bo.DatTaxDocumentsMappingBO;
import com.thbs.mis.tax.bo.DatTaxFirstTimeHouseOwnerBO;
import com.thbs.mis.tax.bo.DatTaxHouseLoanBO;
import com.thbs.mis.tax.bo.DatTaxHraBO;
import com.thbs.mis.tax.bo.DatTaxMedicalInsurancePremiumBO;
import com.thbs.mis.tax.bo.MasTaxTypesBO;
import com.thbs.mis.tax.dao.DatTaxClearanceRepository;
import com.thbs.mis.tax.dao.DatTaxDeclartionRepository;
import com.thbs.mis.tax.dao.DatTaxDocumentsMappingRepository;
import com.thbs.mis.tax.dao.DatTaxFirstTimeHouseOwnerRepository;
import com.thbs.mis.tax.dao.DatTaxHRARepository;
import com.thbs.mis.tax.dao.DatTaxHouseLoanRepository;
import com.thbs.mis.tax.dao.DatTaxMedicalInsurancePremiumRepository;
import com.thbs.mis.tax.dao.TaxTypesRepository;
import com.thbs.mis.tax.util.DocSaveUtil;


@Service
public class TaxDeclarationService {

	private static final AppLog LOG = LogFactory.getLog(TaxDeclarationService.class);

	@Autowired
	private TaxTypesRepository taxTypesRepository;

	@Autowired
	private DatTaxHouseLoanRepository datTaxHouseLoanRepository;

	@Autowired
	private DatTaxFirstTimeHouseOwnerRepository datTaxFirstTimeHouseOwnerRepository;

	@Autowired
	private DatTaxHRARepository datTaxHRARepository;

	@Autowired
	private DatTaxClearanceRepository datTaxClearanceRepository;

	@Autowired
	private DatTaxMedicalInsurancePremiumRepository datTaxMedicalInsurancePremiumRepository;

	@Autowired
	private DatTaxDocumentsMappingRepository datTaxMappingRepositiory;	

	@Autowired
	DocSaveUtil docSaveUtil;	

	@Autowired
	DatTaxDeclartionRepository datTaxDeclartionRepository;

	@Value("${doc.joint.declaration}")
	private String downloadJointDeclaration;
	
	@Value("${doc.form.12C}")
	private String downloadForm12C;

	@Value("${declaration.window.startDate}")
	private String declareWindowStartDate;

	@Value("${declaration.window.endDate}")
	private String declareWindowEndDate;

	@Value("${currection.window.startDate}")
	private String currectionWindowStartDate;

	@Value("${currection.window.endDate}")
	private String currectionWindowEndDate;	

	@Value("${emp.doc.save}")
	private String basePath;

	@Value("${tax.declaration.status.declared}")
	private String CONSTANT_TAX_DECLARED;

	@Value("${tax.declaration.status.pending}")
	private String CONSTANT_TAX_PENDING;

	@Value("${tax.declaration.status.approved}")
	private String CONSTANT_TAX_APPROVED;

	@Value("${tax.declaration.status.rejected}")
	private String CONSTANT_TAX_REJECTED;

	@Value("${tax.emp.review.file.name}")
	private String taxEmpReviewFileName;

	@Value("${tax.templates.basepath}")
	private String taxTemplatesBasepath;

	@Value("${tax.declaration.statement}")
	private String declarationStatement;
	
	@Value("${tax.education.declaration.id}")
	private int educationDecId;
	
	@Value("${tax.prior.employment.id}")
	private int priorEmployement;
	
	
	

	@Autowired
	private DatTaxDocumentsMappingRepository datTaxDocumentsMappingRepository;

	public List<MasTaxTypesBO> getTaxTypeByCategory(Integer typeId) {
		return taxTypesRepository.findByTaxTypeParentId(typeId);
	}

	public List<DatTaxHouseLoanBO> createHouseLoan(DatTaxHouseLoanBO houseLoanBO, DatTaxDeclartionBO datTaxDeclartionBO,
			MultipartFile uploadSupportDocs,MultipartFile uploadAdditionalDocs,String loanDocRefKey, String supportDocRefKey) throws IOException, CommonCustomException {
		
		
		List houseLoanList = new ArrayList();
		datTaxDeclartionBO.setTaxSubmittedDate(new Date());
		if (datTaxDeclartionBO.getPkTaxDeclarationId() == null) {
			List<DatTaxDeclartionBO> dataList = datTaxDeclartionRepository.findByFkEmpIdAndTaxTypeIdAndTaxTypeParentId(
					datTaxDeclartionBO.getFkEmpId(), datTaxDeclartionBO.getTaxTypeId(),
					datTaxDeclartionBO.getTaxTypeParentId());
			if (dataList.size() > 0) {
				throw new CommonCustomException("Entry already exist");
			}
		}
		DatTaxDeclartionBO datTaxDeclartion = datTaxDeclartionRepository.save(datTaxDeclartionBO);
		if (datTaxDeclartion == null)
			throw new CommonCustomException("tax declaration is failed");
		else {
			houseLoanBO.setFkTaxDeclartionId(datTaxDeclartion.getPkTaxDeclarationId());
			DatTaxHouseLoanBO houseLoanData = datTaxHouseLoanRepository.save(houseLoanBO);
			if (houseLoanData == null)
				throw new CommonCustomException("House loan entry failed");
			else {
				
				if (uploadSupportDocs != null) {
					String docRefName = this.getCategoryNameById(datTaxDeclartion.getTaxTypeParentId())
							+ HOUSE_LOAN_KEY+FILE_NAME_SEPARATOR+datTaxDeclartion.getTaxTypeId();
					String fileName =docRefName +FILE_NAME_SEPARATOR+ datTaxDeclartion.getPkTaxDeclarationId();
					boolean fileUploadSuccess = docSaveUtil.upload(datTaxDeclartion.getFkEmpId(),
							fileName, uploadSupportDocs,
							this.getFinanceYearByDate(datTaxDeclartion.getTaxSubmittedDate()));
					if (fileUploadSuccess) {
						DatTaxDocumentsMappingBO mapResult = this.saveMappingDcoument(loanDocRefKey == null ? null:loanDocRefKey,docRefName,datTaxDeclartion,uploadSupportDocs);
						if (mapResult != null) {
							houseLoanList.add(datTaxDeclartion);
							houseLoanList.add(houseLoanData);
						} else
							throw new CommonCustomException("unable to save doc mapping");
					} else {
						throw new CommonCustomException(
								"Data entry created but file upload failed so unable to make the doc mapping to database");
					}
				} else {
					houseLoanList.add(datTaxDeclartion);
					houseLoanList.add(houseLoanData);
				}
				
				
				// Upload Join declaration form
				if (uploadAdditionalDocs != null) {
					
					String docRefName = this.getCategoryNameById(datTaxDeclartion.getTaxTypeParentId())
							+ JOINT_DECLARATION_KEY+FILE_NAME_SEPARATOR+datTaxDeclartion.getTaxTypeId();
					String fileName =docRefName +FILE_NAME_SEPARATOR+ datTaxDeclartion.getPkTaxDeclarationId();
					
					boolean isAdditionalDocsUploadSucc = docSaveUtil.upload(datTaxDeclartion.getFkEmpId(),
							fileName,uploadAdditionalDocs,
							this.getFinanceYearByDate(datTaxDeclartion.getTaxSubmittedDate()));
					
					if (isAdditionalDocsUploadSucc) {
						DatTaxDocumentsMappingBO mapResult = this.saveMappingDcoument(supportDocRefKey == null ? null:supportDocRefKey,docRefName,datTaxDeclartion,uploadAdditionalDocs);
						if (mapResult != null) {
							houseLoanList.add(datTaxDeclartion);
							houseLoanList.add(houseLoanData);
						} else
							throw new CommonCustomException("unable to save doc mapping");
					} else {
						throw new CommonCustomException(
								"Data entry created but file upload failed so unable to make the doc mapping to database");
					}
				} else {
					houseLoanList.add(datTaxDeclartion);
					houseLoanList.add(houseLoanData);
				}								
				
			}
		}
		return houseLoanList;
	}

	public List<DatTaxFirstTimeHouseOwnerBO> createFirstTimeHouseOwner(DatTaxFirstTimeHouseOwnerBO firstHouseOwner,
			DatTaxDeclartionBO datTaxDeclartionBO, MultipartFile uploadSupportDocs, String docRefKey)
			throws IOException, CommonCustomException {
		List houseOwnerList = new ArrayList();
		datTaxDeclartionBO.setTaxSubmittedDate(new Date());
		if (datTaxDeclartionBO.getPkTaxDeclarationId() == null) {
			List<DatTaxDeclartionBO> dataList = datTaxDeclartionRepository.findByFkEmpIdAndTaxTypeIdAndTaxTypeParentId(
					datTaxDeclartionBO.getFkEmpId(), datTaxDeclartionBO.getTaxTypeId(),
					datTaxDeclartionBO.getTaxTypeParentId());
			if (dataList.size() > 0) {
				throw new CommonCustomException("Entry already exist");
			}
		}

		DatTaxDeclartionBO datTaxDeclartion = datTaxDeclartionRepository.save(datTaxDeclartionBO);
		// houseOwnerList.add(datTaxDeclartion);
		if (datTaxDeclartion == null)
			throw new CommonCustomException("House loan entry failed");
		else {
			firstHouseOwner.setFkTaxDeclarionId(datTaxDeclartion.getPkTaxDeclarationId());
			DatTaxFirstTimeHouseOwnerBO dataBO = datTaxFirstTimeHouseOwnerRepository.save(firstHouseOwner);
			if (dataBO == null)
				throw new CommonCustomException("first time House owner entry failed");
			else {
				if (uploadSupportDocs != null) {
					String docRefName = this.getCategoryNameById(datTaxDeclartion.getTaxTypeParentId())
							+ FIRST_TIME_HOUSE_OWNER_KEY+FILE_NAME_SEPARATOR+datTaxDeclartion.getTaxTypeId();
					String fileName =docRefName +FILE_NAME_SEPARATOR+ datTaxDeclartion.getPkTaxDeclarationId();
					boolean fileUploadSuccess = docSaveUtil.upload(datTaxDeclartion.getFkEmpId(),
							fileName, uploadSupportDocs,
							this.getFinanceYearByDate(datTaxDeclartion.getTaxSubmittedDate()));
					if (fileUploadSuccess) {
						DatTaxDocumentsMappingBO mapResult = this.saveMappingDcoument(docRefKey == null ? null:docRefKey,docRefName,datTaxDeclartion,uploadSupportDocs);
						if (mapResult != null) {
							houseOwnerList.add(datTaxDeclartion);
							houseOwnerList.add(dataBO);
						} else
							throw new CommonCustomException("unable to save doc mapping");
					} else {
						throw new CommonCustomException(
								"Data entry created but file upload failed so unable to make the doc mapping to database");
					}
				} else {
					houseOwnerList.add(datTaxDeclartion);
					houseOwnerList.add(dataBO);
				}
			}
		}
		return houseOwnerList;
	}

	public List<DatTaxMedicalInsurancePremiumBO> createMedicalInsurance(
			DatTaxMedicalInsurancePremiumBO medicalInsurance, DatTaxDeclartionBO datTaxDeclartionBO,
			MultipartFile uploadSupportDocs, String docRefKey) throws IOException, CommonCustomException {
		List medicalInsuranceBOList = new ArrayList();
		datTaxDeclartionBO.setTaxSubmittedDate(new Date());
		if (datTaxDeclartionBO.getPkTaxDeclarationId() == null) {
			List<DatTaxDeclartionBO> dataList = datTaxDeclartionRepository.findByFkEmpIdAndTaxTypeIdAndTaxTypeParentId(
					datTaxDeclartionBO.getFkEmpId(), datTaxDeclartionBO.getTaxTypeId(),
					datTaxDeclartionBO.getTaxTypeParentId());
			if (dataList.size() > 0) {
				throw new CommonCustomException("Entry already exist");
			}
		}
		DatTaxDeclartionBO datTaxDeclartion = datTaxDeclartionRepository.save(datTaxDeclartionBO);
		if (datTaxDeclartion == null)
			throw new CommonCustomException("medical  tax entry failed");
		else {
			medicalInsurance.setFkTaxDeclartionId(datTaxDeclartion.getPkTaxDeclarationId());
			DatTaxMedicalInsurancePremiumBO dataBo = datTaxMedicalInsurancePremiumRepository.save(medicalInsurance);
			if (dataBo == null)
				throw new CommonCustomException("medical insurance entry failed");
			else {
				if (uploadSupportDocs != null) {
					String docRefName = this.getCategoryNameById(datTaxDeclartion.getTaxTypeParentId())
							+ MEDICAL_INSURANCE_KEY+FILE_NAME_SEPARATOR+datTaxDeclartion.getTaxTypeId();
					String fileName =docRefName +FILE_NAME_SEPARATOR+ datTaxDeclartion.getPkTaxDeclarationId();
					boolean fileUploadSuccess = docSaveUtil.upload(datTaxDeclartion.getFkEmpId(),
							fileName, uploadSupportDocs,
							this.getFinanceYearByDate(datTaxDeclartion.getTaxSubmittedDate()));
					if (fileUploadSuccess) {
						DatTaxDocumentsMappingBO mapResult = this.saveMappingDcoument(docRefKey == null ? null:docRefKey,docRefName,datTaxDeclartion,uploadSupportDocs);
						if (mapResult != null) {
							medicalInsuranceBOList.add(datTaxDeclartion);
							medicalInsuranceBOList.add(dataBo);
						} else
							throw new CommonCustomException("unable to save doc mapping");
					} else {
						throw new CommonCustomException(
								"Data entry created but file upload failed so unable to make the doc mapping to database");
					}
				} else {
					medicalInsuranceBOList.add(datTaxDeclartion);
					medicalInsuranceBOList.add(dataBo);
				}
			}
		}
		return medicalInsuranceBOList;
	}

	public List<DatTaxClearanceBO> createTaxClearance(DatTaxClearanceBO datTaxClearanceBO)
			throws CommonCustomException {
		List<DatTaxClearanceBO> taxClearanceBOList = new ArrayList<DatTaxClearanceBO>();
		datTaxClearanceBO.setModifiedDate(new Date());
		DatTaxClearanceBO dataList = datTaxClearanceRepository.findByFkEmpIdAndFyStartDateAndFyEndDate(
				datTaxClearanceBO.getFkEmpId(), datTaxClearanceBO.getFyStartDate(), datTaxClearanceBO.getFyEndDate());

		if (dataList != null) {
			throw new CommonCustomException("The Record for this employee is already cleared for this financial year");
		} else {
			
			String financialYear = getFinanceYearByDate(datTaxClearanceBO.getFyStartDate());
			
			List<DatTaxDocumentsMappingBO> docsNotProcessed = datTaxDocumentsMappingRepository.findByDocStatusAndEmpIdAndFinancialYear(datTaxClearanceBO.getFkEmpId(), financialYear, 2);
			
			if(docsNotProcessed!=null && docsNotProcessed.size()>0) {
				throw new CommonCustomException("The clearence for this financial year can not be closed as some of the documents status is pending.");
			}
			
			List<DatTaxDeclartionBO> list = this.datTaxDeclartionRepository
					.empviewService(datTaxClearanceBO.getFkEmpId(),datTaxClearanceBO.getFyStartDate(),datTaxClearanceBO.getFyEndDate());
			
			datTaxClearanceBO.setModifiedDate(new Date());
			DatTaxClearanceBO dataBO = datTaxClearanceRepository.save(datTaxClearanceBO);
			
			for (DatTaxDeclartionBO bo : list) {
				bo.setFkTaxDocClearanceId(dataBO.getPkTaxClearanceId());
				datTaxDeclartionRepository.save(bo);
			}

			taxClearanceBOList.add(dataBO);
		}
		return taxClearanceBOList;

	}


	public List createTaxHra(DatTaxDeclartionBO datTaxClearance, List<DatTaxHraBO> datTaxHraList,
			MultipartFile rentReceipt, MultipartFile rentAgreement, MultipartFile utilityBill, MultipartFile panCard,HRADocumentsRefBean bean)
			throws IOException, CommonCustomException {
		List output = new ArrayList<>();

		datTaxClearance.setTaxSubmittedDate(new Date());
		
		if (datTaxClearance.getPkTaxDeclarationId() == null) {
			List<DatTaxDeclartionBO> dataList = datTaxDeclartionRepository.findByFkEmpIdAndTaxTypeIdAndTaxTypeParentId(
					datTaxClearance.getFkEmpId(), datTaxClearance.getTaxTypeId(), datTaxClearance.getTaxTypeParentId());
			
			if (dataList.size() > 0) {
				throw new CommonCustomException("Entry already exist");
			}
		}
		DatTaxDeclartionBO datTaxDeclartion = datTaxDeclartionRepository.save(datTaxClearance);
		List<DatTaxHraBO> updatedData = new ArrayList<DatTaxHraBO>();
		if (datTaxDeclartion != null) {
			for (DatTaxHraBO datTaxHraObj : datTaxHraList) {
				datTaxHraObj.setFkTaxDeclartionId(datTaxDeclartion.getPkTaxDeclarationId());
				datTaxHraObj = datTaxHRARepository.save(datTaxHraObj);
				updatedData.add(datTaxHraObj);
			}

			if (updatedData.size() > 0) {
				if (rentReceipt != null) {
					String docRefName = this.getCategoryNameById(datTaxDeclartion.getTaxTypeParentId())
							+ RENT_RECEIPTS_KEY+FILE_NAME_SEPARATOR+datTaxDeclartion.getTaxTypeId();
					String fileName =docRefName +FILE_NAME_SEPARATOR+ datTaxDeclartion.getPkTaxDeclarationId();
					boolean rentReceiptSuccess = docSaveUtil.upload(datTaxDeclartion.getFkEmpId(),
							fileName, rentReceipt,
							this.getFinanceYearByDate(datTaxDeclartion.getTaxSubmittedDate()));
					if (rentReceiptSuccess) {
						DatTaxDocumentsMappingBO mapResult = this.saveMappingDcoument(bean == null ? null : bean.getRenReceiptsRefKey(),docRefName,datTaxDeclartion,rentReceipt);
						if (mapResult == null)
							throw new CommonCustomException("unable to save doc mapping");
					} else {
						throw new CommonCustomException("rent receipt saved failed");
					}
				}
				//
				if (rentAgreement != null) {
					String docRefName = this.getCategoryNameById(datTaxDeclartion.getTaxTypeParentId())
							+ RENT_AGREEMENT_KEY+FILE_NAME_SEPARATOR+datTaxDeclartion.getTaxTypeId();
					String fileName =docRefName +FILE_NAME_SEPARATOR+ datTaxDeclartion.getPkTaxDeclarationId();
					boolean rentAgreementSuccess = docSaveUtil.upload(datTaxDeclartion.getFkEmpId(),
							fileName, rentAgreement,
							this.getFinanceYearByDate(datTaxDeclartion.getTaxSubmittedDate()));
					if (rentAgreementSuccess) {
						DatTaxDocumentsMappingBO mapResult = this.saveMappingDcoument(bean == null ? null : bean.getRentAgreementRefKey(),docRefName,datTaxDeclartion,rentAgreement);
						if (mapResult == null)
							throw new CommonCustomException("unable to save doc mapping");
					} else {
						throw new CommonCustomException("rent receipt saved failed");
					}
				}
				//
				if (utilityBill != null) {
					String docRefName = this.getCategoryNameById(datTaxDeclartion.getTaxTypeParentId())
							+ UTILITY_BILL_KEY+FILE_NAME_SEPARATOR+datTaxDeclartion.getTaxTypeId();
					String fileName =docRefName +FILE_NAME_SEPARATOR+ datTaxDeclartion.getPkTaxDeclarationId();
					boolean utilityBillSuccess = docSaveUtil.upload(datTaxDeclartion.getFkEmpId(),
							fileName, utilityBill,
							this.getFinanceYearByDate(datTaxDeclartion.getTaxSubmittedDate()));
					if (utilityBillSuccess) {
						DatTaxDocumentsMappingBO mapResult = this.saveMappingDcoument(bean == null ? null : bean.getUtilityBillRefKey(),docRefName,datTaxDeclartion,utilityBill);
						if (mapResult == null)
							throw new CommonCustomException("unable to save doc mapping");
					} else {
						throw new CommonCustomException("rent receipt saved failed");
					}
				}
				//
				if (panCard != null) {
					
					String docRefName = this.getCategoryNameById(datTaxDeclartion.getTaxTypeParentId())
							+ PAN_HOUSE_OWNER_KEY+FILE_NAME_SEPARATOR+datTaxDeclartion.getTaxTypeId();
					String fileName =docRefName +FILE_NAME_SEPARATOR+ datTaxDeclartion.getPkTaxDeclarationId();
					
					boolean panCardSuccess = docSaveUtil.upload(datTaxDeclartion.getFkEmpId(),
							fileName, panCard,
							this.getFinanceYearByDate(datTaxDeclartion.getTaxSubmittedDate()));
					if (panCardSuccess) {
						DatTaxDocumentsMappingBO mapResult = this.saveMappingDcoument(bean == null ? null : bean.getHouseOwnerPANKey(),docRefName,datTaxDeclartion,panCard);
						if (mapResult == null)
							throw new CommonCustomException("unable to save doc mapping");
					} else {
						throw new CommonCustomException("rent receipt saved failed");
					}
				}

			} else {
				throw new CommonCustomException("HRA  tax entry failed");
			}

		} else {
			throw new CommonCustomException("HRA  tax entry failed");
		}
		output.add(datTaxDeclartion);
		output.add(updatedData);
		return output;
	}

	public List<DatTaxDeclartionBO> createTaxDeclaration(DatTaxDeclartionBO datTaxDeclartionBO,
			MultipartFile uploadSupportDocs,String docRefKey) throws CommonCustomException, IOException {
		List<DatTaxDeclartionBO> output = new ArrayList<DatTaxDeclartionBO>();
		datTaxDeclartionBO.setTaxSubmittedDate(new Date());
		if (datTaxDeclartionBO.getPkTaxDeclarationId() == null) {
			List<DatTaxDeclartionBO> dataList = datTaxDeclartionRepository.findByFkEmpIdAndTaxTypeIdAndTaxTypeParentId(
					datTaxDeclartionBO.getFkEmpId(), datTaxDeclartionBO.getTaxTypeId(),
					datTaxDeclartionBO.getTaxTypeParentId());
			if (dataList.size() > 0) {
				throw new CommonCustomException("Entry already exist");
			}
		}
		DatTaxDeclartionBO datTaxDeclartion = datTaxDeclartionRepository.save(datTaxDeclartionBO);
		if (datTaxDeclartion == null)
			throw new CommonCustomException("unable to save to database");
		else {
			
			
			
			
			
			// Create record if supporting docs is not null or child education data is present
			if (uploadSupportDocs != null ) {
				String docRefName = this.getCategoryNameById(datTaxDeclartionBO.getTaxTypeParentId())+FILE_NAME_SEPARATOR+datTaxDeclartionBO.getTaxTypeId();
				String fileName =docRefName +FILE_NAME_SEPARATOR + datTaxDeclartion.getPkTaxDeclarationId();
				
				boolean fileUploadSuccess = docSaveUtil.upload(datTaxDeclartion.getFkEmpId(),
						fileName, uploadSupportDocs,
						this.getFinanceYearByDate(datTaxDeclartion.getTaxSubmittedDate()));
				if (fileUploadSuccess) {
					DatTaxDocumentsMappingBO mapResult = this.saveMappingDcoument(docRefKey == null ? null :docRefKey,docRefName,datTaxDeclartion,uploadSupportDocs);
					if (mapResult != null)
						output.add(datTaxDeclartion);
					else
						throw new CommonCustomException("unable to save doc mapping");
				} else {
					throw new CommonCustomException(
							"Data entry created but file upload failed so unable to make the doc mapping to database");
				}
			} else {
				
				// Handles child education allowance
				// Create record in mapping but no file is save in folder as file is not required
				String docRefName = this.getCategoryNameById(datTaxDeclartionBO.getTaxTypeParentId())+FILE_NAME_SEPARATOR+datTaxDeclartionBO.getTaxTypeId();
				DatTaxDocumentsMappingBO mapResult = this.saveMappingDcoument(docRefKey,docRefName,datTaxDeclartion,null);
				
				if (mapResult != null) {
					output.add(datTaxDeclartion);
					
				} else
					throw new CommonCustomException("unable to save doc mapping");
			}
		}
		return output;
	}

	public boolean uploadTaxDocWithSignature(Integer empId, String[] taxDeclarationIds, MultipartFile signatureDoc)
			throws IOException, CommonCustomException {
		for (String decId : taxDeclarationIds) {
			Integer declarationId = Integer.parseInt(decId);
			// fetch and update the status as pending of those declarations
			// whose status is 1-tax declared
			List<DatTaxDocumentsMappingBO> docList = datTaxDocumentsMappingRepository
					.findByTaxDeclarationIdAndDocStatus(declarationId, 1);
			if (docList.size() > 0)
				for (DatTaxDocumentsMappingBO bo : docList) {
					bo.setDocStatus(Integer.parseInt(CONSTANT_TAX_PENDING));
					bo.setUpdatedDate(new Date());
					datTaxDocumentsMappingRepository.save(bo);
				}
		}		
		
		String fileName = empId + FILE_NAME_SEPARATOR+taxEmpReviewFileName  ;
		return docSaveUtil.uploadSignedDeclaration(empId,fileName, signatureDoc, this.getFinanceYearByDate(new Date()));
	}

	public Boolean deleteEmpTaxData(DeleteEmployeeDataBean bean) throws CommonCustomException, IOException {

		Boolean dbDataIsDeleted = false;
		Date curDate = new Date();

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		Date windowStartDate = null;
		Date windowEndDate = null;
		df.setLenient(false);

		try {

			windowStartDate = df.parse(declareWindowStartDate);
			windowEndDate = df.parse(declareWindowEndDate);


			if (windowStartDate.getTime() > windowEndDate.getTime()) {
				throw new CommonCustomException("dateOf Issue should be less than to Expiry date");
			}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}

		DatTaxDeclartionBO taxDeclare = new DatTaxDeclartionBO();
		List<DatTaxDocumentsMappingBO> mappignBO = new ArrayList<DatTaxDocumentsMappingBO>();

		if (curDate.after(windowStartDate) && curDate.before(windowEndDate)) {

			taxDeclare = datTaxDeclartionRepository.findByPkTaxDeclarationId(bean.getDeclareId());
			LOG.info(">>>>>>>>>>>>>" + taxDeclare);
			if (taxDeclare == null) {
				throw new CommonCustomException("No data Found");
			} else {

				if (bean.getTypeID() == 1) {
					
					List<DatTaxHraBO> hraList = new ArrayList<DatTaxHraBO>();
					hraList = datTaxHRARepository.getTaxDeclarationFromDeclarationId(bean.getDeclareId());
					datTaxHRARepository.delete(hraList);
					deleteDocuments(bean);
					datTaxDeclartionRepository.delete(taxDeclare);
					dbDataIsDeleted = true;
				} else if (bean.getTypeID() == 2) {
					DatTaxHouseLoanBO taxLoan = new DatTaxHouseLoanBO();
					taxLoan = datTaxHouseLoanRepository.findByFkTaxDeclartionId(bean.getDeclareId());
					datTaxHouseLoanRepository.delete(taxLoan);
					deleteDocuments(bean);
					datTaxDeclartionRepository.delete(taxDeclare);
					dbDataIsDeleted = true;
				} else if (bean.getTypeID() == 4) {
					DatTaxFirstTimeHouseOwnerBO firstTimeHouseOwner = new DatTaxFirstTimeHouseOwnerBO();
					firstTimeHouseOwner = datTaxFirstTimeHouseOwnerRepository
							.findByFkTaxDeclarionId(bean.getDeclareId());
					datTaxFirstTimeHouseOwnerRepository.delete(firstTimeHouseOwner);
					deleteDocuments(bean);
					datTaxDeclartionRepository.delete(taxDeclare);
					dbDataIsDeleted = true;
				} else if (bean.getTypeID() == 6) {
					DatTaxMedicalInsurancePremiumBO medicalBO = new DatTaxMedicalInsurancePremiumBO();
					medicalBO = datTaxMedicalInsurancePremiumRepository.findByFkTaxDeclartionId(bean.getDeclareId());
					datTaxMedicalInsurancePremiumRepository.delete(medicalBO);
					deleteDocuments(bean);
					datTaxDeclartionRepository.delete(taxDeclare);
					dbDataIsDeleted = true;
				} else {
					deleteDocuments(bean);
					datTaxDeclartionRepository.delete(taxDeclare);
					dbDataIsDeleted = true;
				}

			}

		} else {
			
			throw new CommonCustomException("The current date is not in Declaration window and hence delete can not be performed");
			
		}
		return dbDataIsDeleted;
	}
	
	
	boolean deleteDocuments(DeleteEmployeeDataBean bean) throws CommonCustomException {
		boolean success = false;

		// Delete Mapping files from repository
		List<DatTaxDocumentsMappingBO> documents = datTaxMappingRepositiory.findByTaxDeclarationId(bean.getDeclareId());

		if (documents != null) {
			for (Iterator iterator = documents.iterator(); iterator.hasNext();) {
				
				DatTaxDocumentsMappingBO datTaxDocumentsMappingBO = (DatTaxDocumentsMappingBO) iterator.next();

				LOG.debug("datTaxDocumentsMappingBO : " + datTaxDocumentsMappingBO);

				String fileName = datTaxDocumentsMappingBO.getDocRefName() + FILE_NAME_SEPARATOR + bean.getDeclareId();
				String fileExtension = docSaveUtil.getFileExtension(datTaxDocumentsMappingBO.getDocName());

				LOG.debug("fileExtension " + fileExtension);
				fileName = fileName + "." + fileExtension;

				if (bean.getTypeID() != educationDecId) {

					this.docSaveUtil.deleteFile(bean.getEmpID(), fileName, datTaxDocumentsMappingBO.getFinanceYear());

				}
				datTaxMappingRepositiory.delete(datTaxDocumentsMappingBO);
				
				
				success = true;
			}
			
			LOG.debug("Fetching remaining docs for status update");
			
			// Update the status as 1 to others docs which are in pending status as the employee has made changes
			List<DatTaxDocumentsMappingBO> remainingDoc = datTaxMappingRepositiory
					.findByDocStatusAndEmpIdAndFinancialYear(bean.getEmpID(), bean.getFinancialYear(),
							Integer.valueOf(CONSTANT_TAX_PENDING));
			LOG.debug("Remaining doc List : " + remainingDoc);
			if (remainingDoc != null) {
				for (Iterator iterator = remainingDoc.iterator(); iterator.hasNext();) {
					DatTaxDocumentsMappingBO datTaxDocumentsMappingBO = (DatTaxDocumentsMappingBO) iterator.next();

					datTaxDocumentsMappingBO.setDocStatus(Integer.valueOf(CONSTANT_TAX_DECLARED));
					datTaxDocumentsMappingBO.setUpdatedDate(new Date());
					datTaxMappingRepositiory.save(datTaxDocumentsMappingBO);

				}
			}

		} 
		
		return success;

	}

	/*boolean deleteFile(Integer empId, Integer decId, String financialYear) throws CommonCustomException {
		boolean result = false;
		String path = basePath + financialYear + File.separator;
		File[] files = new File(path).listFiles();
		if (files != null) {
			for (File file : files) {
				if (file.isFile()) {
					if (file.getName().contains(decId.toString())) {
						file.delete();
						result = true;
					}
				}
			}
		} else {
			throw new CommonCustomException("File not found");
		}
		return result;
	}*/

	public List<ViewEmployeeTaxDeclarationReviewBean> viewAllEmpDetailsONDateRange(TaxEmpReviewBean bean)
			throws CommonCustomException, ParseException {

		List<DatTaxDeclartionBO> taxDeclarationList = new ArrayList<DatTaxDeclartionBO>();

		if (bean.getEmpId() == null)
			throw new CommonCustomException("empId should not be null");
		else if (bean.getStartDate() == null)
			throw new CommonCustomException("start date should not be null");
		else if (bean.getEndDate() == null)
			throw new CommonCustomException("end date should not be null");
		else {
			taxDeclarationList = datTaxDeclartionRepository.empviewService(bean.getEmpId(), bean.getStartDate(),
					bean.getEndDate());
		}

		List<ViewEmployeeTaxDeclarationReviewBean> output = new ArrayList<>();
		for (DatTaxDeclartionBO bo : taxDeclarationList) {
			ViewEmployeeTaxDeclarationReviewBean outputBean = new ViewEmployeeTaxDeclarationReviewBean();
			outputBean.setCategoryId(bo.getTaxTypeParentId());
			outputBean.setCategoryName(this.getCategoryNameById(bo.getTaxTypeParentId()));
			outputBean.setDocList(datTaxDocumentsMappingRepository.findByTaxDeclarationId(bo.getPkTaxDeclarationId()));
			outputBean.setEmpId(bo.getFkEmpId());
			DatEmpPersonalDetailBO personelBo = employeePersonalDetailsRepository.findByFkEmpDetailId(bo.getFkEmpId());
			if (personelBo != null) {
				outputBean.setEmpname(personelBo.getEmpFirstName() + "" + personelBo.getEmpLastName());
			}
			outputBean.setFirstTimeHouseOwner(
					this.datTaxFirstTimeHouseOwnerRepository.getFirstTimeHosueOwnerDetails(bo.getPkTaxDeclarationId()));
			outputBean.setHouseLoanList(
					this.datTaxHouseLoanRepository.getTaxDeclarationFromDeclarationId(bo.getPkTaxDeclarationId()));
			outputBean.setHraBOList(
					this.datTaxHRARepository.getTaxDeclarationFromDeclarationId(bo.getPkTaxDeclarationId()));
			outputBean.setMedicalInsurance(this.datTaxMedicalInsurancePremiumRepository
					.getTaxDeclarationFromDeclarationId(bo.getPkTaxDeclarationId()));

			outputBean.setTotalTaxAmount(bo.getTaxAmount());
			outputBean.setTypeId(bo.getTaxTypeId());
			if (bo.getTaxTypeId() != null) {
				short typeId = Short.parseShort(bo.getTaxTypeId().toString());
				outputBean.setTypeName(this.taxTypesRepository.findByPkTaxTypeId(typeId).getTaxTypeName());
				outputBean.setSection(taxTypesRepository.findByPkTaxTypeId(typeId).getTaxSection());
			}

			output.add(outputBean);
		}
		return output;

	}

	private ViewEmployeeTaxDeclarationReviewBean getDocumentDeatilsByDecId(Integer decId) {
		ViewEmployeeTaxDeclarationReviewBean output = new ViewEmployeeTaxDeclarationReviewBean();
		List<DatTaxDocumentsMappingBO> dataList = datTaxDocumentsMappingRepository.findByTaxDeclarationId(decId);
		if (dataList.size() > 0) {
			output.setDocCount(dataList.size());
		}
		output.setDocList(dataList);
		return output;
	}

	@SuppressWarnings("unchecked")
	private ViewEmployeeTaxDeclarationReviewBean getHouseLoanDeatilsByDecId(Integer decId) {
		ViewEmployeeTaxDeclarationReviewBean output = new ViewEmployeeTaxDeclarationReviewBean();
		List<DatTaxHouseLoanBO> dataList = datTaxHouseLoanRepository.getTaxDeclarationFromDeclarationId(decId);

		output.setHouseLoanList(dataList);
		return output;
	}

	private ViewEmployeeTaxDeclarationReviewBean getFirstTimeHouseOwnerDeatilsByDecId(Integer decId) {
		ViewEmployeeTaxDeclarationReviewBean output = new ViewEmployeeTaxDeclarationReviewBean();
		@SuppressWarnings("unchecked")
		List<DatTaxFirstTimeHouseOwnerBO> dataList = datTaxFirstTimeHouseOwnerRepository
				.getFirstTimeHosueOwnerDetails(decId);

		output.setFirstTimeHouseOwner(dataList);
		return output;
	}

	String getCategoryNameById(Integer key) {
		switch (key) {
		case 1:
			return "HOUSING";
		case 2:
			return "INSURANCE";
		case 3:
			return "INVESTMENTS";
		case 4:
			return "EXEMPTIONS";
		case 5:
			return "EDUCATION";
		case 6:
			return "PRIOR_EMPLOYMENT";
		case 7:
			return "OTHERS";
		}
		return null;
	}

	public List<ViewEmployeesTaxDeclaration> getEmployeeTaxDetails(TaxEmpReviewBean bean)
			throws CommonCustomException, ParseException {
		List<ViewEmployeesTaxDeclaration> output = new ArrayList<ViewEmployeesTaxDeclaration>();

		if (bean.getStartDate() == null && bean.getEndDate() == null && bean.getEmpId() == null) {
			throw new CommonCustomException("Provide either empId or Date Range");
		}
		List<DatTaxDeclartionBO> taxDeclarationList = new ArrayList<DatTaxDeclartionBO>();
		
		 if (bean.getEmpId() != null && bean.getStartDate() != null && bean.getEndDate() != null) {
				taxDeclarationList = datTaxDeclartionRepository.getAllDetailsFromPersonalAndTaxDeclarationByEmpidAndFinancialYear(bean.getEmpId(), bean.getStartDate(), bean.getEndDate());
						
				
			} else if (bean.getStartDate() != null && bean.getEndDate() != null) {
			taxDeclarationList = datTaxDeclartionRepository
					.getAllDetailsFromPersonalAndTaxDeclaration(bean.getStartDate(), bean.getEndDate());
			LOG.info(taxDeclarationList.toString());

		} else if (bean.getEmpId() != null) {
			
			taxDeclarationList = datTaxDeclartionRepository.getAllDetailsFromPersonalAndTaxDeclarationByEmpid(bean.getEmpId());
			
		}
			
			else {
		
			throw new CommonCustomException("Either give empID or Date range");
		}
		 
		LOG.info(taxDeclarationList.toString());

		if (!taxDeclarationList.isEmpty()) {
			for (DatTaxDeclartionBO bo : taxDeclarationList) {
				ViewEmployeesTaxDeclaration taxBean = new ViewEmployeesTaxDeclaration();
				taxBean.setEmailId(bo.getEmpProfessionalEmailId());
				taxBean.setEmpId(bo.getFkEmpId());
				taxBean.setEmpName(bo.getEmpFirstName() + " " + bo.getEmpLastName());

				Integer taxStatusCount = datTaxDocumentsMappingRepository.findDocStatusCount(bo.getFkEmpId(),
						getFinanceYearByDate(bo.getTaxSubmittedDate()), Integer.valueOf(CONSTANT_TAX_DECLARED));
				Integer pendingStatusCount = datTaxDocumentsMappingRepository.findDocStatusCount(bo.getFkEmpId(),
						getFinanceYearByDate(bo.getTaxSubmittedDate()), Integer.valueOf(CONSTANT_TAX_PENDING));
				Integer approvedStatusCount = datTaxDocumentsMappingRepository.findDocStatusCount(bo.getFkEmpId(),
						getFinanceYearByDate(bo.getTaxSubmittedDate()), Integer.valueOf(CONSTANT_TAX_APPROVED));
				Integer rejectedStatusCount = datTaxDocumentsMappingRepository.findDocStatusCount(bo.getFkEmpId(),
						getFinanceYearByDate(bo.getTaxSubmittedDate()), Integer.valueOf(CONSTANT_TAX_REJECTED));

				taxBean.setTaxDeclaredCount(taxStatusCount);
				taxBean.setApprovedCount(approvedStatusCount);
				taxBean.setPendingCount(pendingStatusCount);
				taxBean.setRejectedCount(rejectedStatusCount);

				if (bo.getFkTaxDocClearanceId() == null)
					taxBean.setIsAllDoccleared("NO");
				else
					taxBean.setIsAllDoccleared("YES");
				LOG.debug("taxBean Value : "+taxBean);
				
				if(taxBean.getPendingCount() == 0 && taxBean.getApprovedCount()==0 && taxBean.getRejectedCount()==0) {
					continue;
				} else {
					output.add(taxBean);
				}				
					
			}
		}
		return output;
	}


	private DatTaxDocumentsMappingBO updateDocStatusToMapping(DatTaxDocumentsMappingBO datTaxDocumentsMappingBO) {
		return datTaxDocumentsMappingRepository.save(datTaxDocumentsMappingBO);
	}

	String getFinanceYearByDate(Date submitedDate) {
		TaxFinancialYearBean financialYear = new TaxFinancialYearBean(submitedDate);
		String finYear = financialYear.getMonthName(financialYear.getFromMonth()) + " " + financialYear.getFromYear()
				+ " - " + financialYear.getMonthName(financialYear.getToMonth()) + " " + financialYear.getToYear();
		return finYear;
	}

	@Autowired
	EmployeePersonalDetailsRepository employeePersonalDetailsRepository;

	public List<ViewEmployeeTaxDeclarationReviewBean> empViewService(ViewEmployeeInputBean bean)
			throws CommonCustomException, ParseException {
		
		List<ViewEmployeeTaxDeclarationReviewBean> output = null;
		try {
			
			List<DatTaxDeclartionBO> taxDeclarationList = new ArrayList<DatTaxDeclartionBO>();
			if (bean.getEmpId() == null)
				throw new CommonCustomException("empId should not be null");
			if (bean.getStartDate() == null)
				throw new CommonCustomException("start date should not be null");
			if (bean.getEndDate() == null)
				throw new CommonCustomException("end date should not be null");
			if (bean.getCategoryId() == null) {
				if (bean.getTypeId() == null) {
					
					taxDeclarationList = datTaxDeclartionRepository.empviewService(bean.getEmpId(), bean.getStartDate(),
							bean.getEndDate());
					
				} else
					throw new CommonCustomException("type id should not be null if category id is not null");
			} else {
				taxDeclarationList = datTaxDeclartionRepository.empviewServiceByCategoryId(bean.getEmpId(),
						bean.getStartDate(), bean.getEndDate(), bean.getCategoryId(), bean.getTypeId());

			}
			
			output = new ArrayList<ViewEmployeeTaxDeclarationReviewBean>();
			if (taxDeclarationList == null || taxDeclarationList.size() == 0) {
				
				ViewEmployeeTaxDeclarationReviewBean taxBean = new ViewEmployeeTaxDeclarationReviewBean();

				DatTaxClearanceBO taxClearance = datTaxClearanceRepository.findByFkEmpIdAndFyStartDateAndFyEndDate(bean.getEmpId(), bean.getStartDate(), bean.getEndDate());
				
				LOG.debug("Employee Tax Cleared : "+taxClearance);
				
				if(taxClearance!=null) {
					taxBean.setDeclarationClosed(taxClearance.getIsEmpDocClearnce());
				} else {
					taxBean.setDeclarationClosed("NOT_DECLARED");
				}			
				output.add(taxBean);
			} else {
				for (DatTaxDeclartionBO bo : taxDeclarationList) {
					ViewEmployeeTaxDeclarationReviewBean outputBean = new ViewEmployeeTaxDeclarationReviewBean();
					outputBean.setTaxDeclarationId(bo.getPkTaxDeclarationId());
					outputBean.setCategoryId(bo.getTaxTypeParentId());
					outputBean.setCategoryName(this.getCategoryNameById(bo.getTaxTypeParentId()));
					outputBean.setDocList(datTaxDocumentsMappingRepository.findByTaxDeclarationId(bo.getPkTaxDeclarationId()));							
					outputBean.setEmpId(bo.getFkEmpId());
					DatEmpPersonalDetailBO personelBo = employeePersonalDetailsRepository
							.findByFkEmpDetailId(bo.getFkEmpId());
					if (personelBo != null) {
						outputBean.setEmpname(personelBo.getEmpFirstName() + " " + personelBo.getEmpLastName());
					}
					outputBean.setFirstTimeHouseOwner(this.datTaxFirstTimeHouseOwnerRepository
							.getFirstTimeHosueOwnerDetails(bo.getPkTaxDeclarationId()));
					outputBean.setHouseLoanList(this.datTaxHouseLoanRepository
							.getTaxDeclarationFromDeclarationId(bo.getPkTaxDeclarationId()));
					outputBean.setTotalTaxAmount(bo.getTaxAmount());
					outputBean.setHraBOList(
							this.datTaxHRARepository.getTaxDeclarationFromDeclarationId(bo.getPkTaxDeclarationId()));
					outputBean.setMedicalInsurance(this.datTaxMedicalInsurancePremiumRepository
							.getTaxDeclarationFromDeclarationId(bo.getPkTaxDeclarationId()));
					outputBean.setTypeId(bo.getTaxTypeId());
					if (bo.getTaxTypeId() != null) {
						short typeId = Short.parseShort(bo.getTaxTypeId().toString());
						outputBean.setTypeName(this.taxTypesRepository.findByPkTaxTypeId(typeId).getTaxTypeName());
						outputBean.setSection(taxTypesRepository.findByPkTaxTypeId(typeId).getTaxSection());

					}
					
					DatTaxClearanceBO taxClearance = datTaxClearanceRepository.findByFkEmpIdAndFyStartDateAndFyEndDate(bean.getEmpId(), bean.getStartDate(), bean.getEndDate());
					if(taxClearance!=null) {
						outputBean.setDeclarationClosed(taxClearance.getIsEmpDocClearnce());
					} else {
						outputBean.setDeclarationClosed("NO");
					}
					
					output.add(outputBean);
				}
			}
			//return output;
		} catch (Exception e) {
			LOG.error("Error occured while fetching data",e);
		}
		return output;
	}

	public boolean downloadJointDeclarationForm(HttpServletResponse response) throws CommonCustomException {
		Boolean isDownloaded = false;
		if (true) {
			try {
				// String BASEPATH =
				// "C:/Users/rajesh_kumar/Downloads/download/";
				// String BASEPATH =
				// "/opt/Documents/Tax_Declaration_Docs/Joint_Owners_Applicant_Form/";
				String BASEPATH = downloadJointDeclaration;
				DownloadFileUtil.downloadAllFile(response, BASEPATH);
				isDownloaded = true;
			} catch (Exception e) {
				throw new CommonCustomException(e.getMessage());
			}
		}
		return isDownloaded;
	}
	
	
	
	public boolean downloadForm12C(HttpServletResponse response) throws CommonCustomException {
		Boolean isDownloaded = false;
		if (true) {
			try {
				// String BASEPATH =
				// "C:/Users/rajesh_kumar/Downloads/download/";
				// String BASEPATH =
				// "/opt/Documents/Tax_Declaration_Docs/Joint_Owners_Applicant_Form/";
				String BASEPATH = downloadForm12C;
				DownloadFileUtil.downloadFile(response, BASEPATH,"");
				isDownloaded = true;
			} catch (Exception e) {
				throw new CommonCustomException(e.getMessage());
			}
		}
		return isDownloaded;
	}

	public boolean updateStatusDoc(List<EmployeeApprovedStatusBean> bean) throws CommonCustomException, ParseException {
		
		boolean result = false;
		for (EmployeeApprovedStatusBean beanData : bean) {
			DatTaxDocumentsMappingBO bo = datTaxDocumentsMappingRepository.findByDocKey(beanData.getDocKey());
			if (bo != null) {
				try {
					if(beanData.getStatus() == 4)
					{
						bo.setFinanceRejectedComments(beanData.getComments());
						bo.setDocStatus(beanData.getStatus());
					}
					else
					{
						bo.setDocStatus(beanData.getStatus());
					}
					bo.setUpdatedDate(new Date());
					datTaxDocumentsMappingRepository.save(bo);
					result = true;
				} catch (Exception e) {
					result = false;
					throw new CommonCustomException("Update failed for " + beanData.getDocKey());
				}
			}

		}
		return result;
	}

	public boolean getDocDownload(String key, HttpServletResponse response) throws IOException, CommonCustomException {
		boolean result = false;
		try {
			DatTaxDocumentsMappingBO docBo = datTaxDocumentsMappingRepository.findByDocKey(key);
			
			
			
			
			if (docBo != null) {
				DatTaxDeclartionBO taxBo = this.datTaxDeclartionRepository
						.findByPkTaxDeclarationId(docBo.getTaxDeclarationId());
				
				String baseDir = basePath + File.separator + docBo.getFinanceYear() + File.separator
						+ taxBo.getFkEmpId() + File.separator;
				
				

				String fileName =  docBo.getDocRefName()+FILE_NAME_SEPARATOR+docBo.getTaxDeclarationId()+"."+docSaveUtil.getFileExtension(docBo.getDocName()) ;
						
				DownloadFileUtil.downloadFile(response, baseDir, fileName);
				result = true;
			} else {
				throw new CommonCustomException("invalid key");
			}
		} catch (Exception e) {
			LOG.error("Documenty download failed", e);
		}
		return result;

	}

	public boolean downloadTaxEmpReviewPdf(EmpTaxDeclarationPdfOutputBean taxInputBean, HttpServletResponse response)
			throws CommonCustomException, IOException {
		boolean flag = false;
		boolean createNewPdfFlag = false;
		FileOutputStream fos = null;

		if (taxInputBean == null)
			throw new CommonCustomException("No employee tax data found");

		if (taxInputBean.getTaxDeclarationBeanList().isEmpty()) {
			throw new CommonCustomException("No tax related data found");
		}

		// Added a condition for update of this pdf
		List<ViewEmployeeTaxDeclarationReviewBean> taxReviewList = new ArrayList<ViewEmployeeTaxDeclarationReviewBean>();

		taxReviewList = taxInputBean.getTaxDeclarationBeanList();

		if (taxReviewList.isEmpty())
			throw new CommonCustomException("No Tax Declaration found");

		for (ViewEmployeeTaxDeclarationReviewBean reviewBean : taxReviewList) {
			List<DatTaxDocumentsMappingBO> taxDocumentMappingList = reviewBean.getDocList();

			if (taxDocumentMappingList.isEmpty()) {
				if(reviewBean.getTypeId() ==educationDecId || reviewBean.getTypeId() == priorEmployement ) {
					
					LOG.debug("Education or Prior Employment Details");
					
				} else {
					throw new CommonCustomException("No document found");
				}
				
			}
				

			for (DatTaxDocumentsMappingBO datTaxDocumentsMappingBO : taxDocumentMappingList) {
				// Check any one of the document;s status is tax declared
				if (datTaxDocumentsMappingBO.getDocStatus().intValue() == Integer.parseInt(CONSTANT_TAX_DECLARED)) {
					createNewPdfFlag = true;
					break;
				}
			}
		}
		
		String fileName = taxInputBean.getEmployeeId()+FILE_NAME_SEPARATOR+taxEmpReviewFileName;

		String filePath = basePath + File.separator+ this.getFinanceYearByDate(new Date())+File.separator+ taxInputBean.getEmployeeId().toString() + File.separator;
		File fileDir = new File(
				basePath + File.separator+ this.getFinanceYearByDate(new Date())+File.separator + taxInputBean.getEmployeeId().toString() + File.separator);

		filePath = filePath +fileName ;
		File taxFile = new File(filePath);

		try {

			/*
			 * if(!taxFile.exists()) {
			 */
			if (createNewPdfFlag) {
				if (!fileDir.exists()) {
					fileDir.mkdirs();

				}
				// taxFile = new File(filePath);

				fos = new FileOutputStream(taxFile);
				List<EmpTaxDeclarationReviewPdfOutputBean> pdfOutputList = new ArrayList<EmpTaxDeclarationReviewPdfOutputBean>();

				DecimalFormat df = new DecimalFormat("#.##");
				for (ViewEmployeeTaxDeclarationReviewBean beanObj : taxInputBean.getTaxDeclarationBeanList()) {
					EmpTaxDeclarationReviewPdfOutputBean obj = new EmpTaxDeclarationReviewPdfOutputBean();
					obj.setSection(beanObj.getSection());
					// obj.setCategoryName(beanObj.getCategoryName());
					
					
					
					// List<DatTaxDocumentsMappingBO> taxDocumentMappingList = null;
					List<DatTaxDocumentsMappingBO> taxDocumentMappingList = beanObj.getDocList();

					if (taxDocumentMappingList != null || !taxDocumentMappingList.isEmpty()) {
						int count = 0;
						for (DatTaxDocumentsMappingBO datTaxDocumentsMappingBO : taxDocumentMappingList) {
							if (!datTaxDocumentsMappingBO.getDocName().isEmpty()) {
								++count;
							}
						}
						obj.setDocCount(count);
					} else {
						obj.setDocCount(0);
					}
					
					
					if(beanObj.getTotalTaxAmount()!=null) {
						
						obj.setTotalTaxAmount(df.format(beanObj.getTotalTaxAmount()));
					} else {
						obj.setTotalTaxAmount(df.format(0.00));
					}
					obj.setTypeName(beanObj.getTypeName());
					
					LOG.info("Total Tax Amount ::: " + obj.getTotalTaxAmount());
					pdfOutputList.add(obj);
				}

				Map<String, String> parameter = new HashMap<String, String>();
				parameter.put("empNameWithId",
						taxInputBean.getEmployeeName() + "-" + taxInputBean.getEmployeeId().toString());
				parameter.put("assessmentYear", taxInputBean.getAssessmentYear());

				parameter.put("declarationStatement", declarationStatement);
				parameter.put("date", "Date : ");
				parameter.put("signature", "( Signature )");

				CreatePdfFileUtil.generateJasperReportFromListAndObjectOfBean(taxTemplatesBasepath, fos, pdfOutputList,
						parameter);
				// capexService.generateJasperReportPDF(taxTemplatesBasepath,
				// fos, pdfOutputList, parameter );
				LOG.debug("<<<<<<<<<<<< Tax Emp Review PDF Created >>>>>>>>");
			}
		} catch (FileNotFoundException e) {
			LOG.debug("Error while create tax emp review file : " + e);
		} catch (Exception ex) {
			LOG.info("Error :::: " + ex.getMessage());
		} finally {
			if (fos != null) {
				try {
					fos.flush();
					fos.close();
				} catch (IOException e) {
					LOG.debug("Error in download tax emp review file : closing file " + e);
				}
			}
		}

		if (taxFile.exists()) {
			filePath = basePath + File.separator +this.getFinanceYearByDate(new Date())+File.separator+ taxInputBean.getEmployeeId().toString() + File.separator;
			try {
				DownloadFileUtil.downloadFile(response, filePath, fileName);
			} catch (IOException e) {
				LOG.debug("Error in download tax emp review file : " + e);
			}
			flag = true;
		} else {
			throw new CommonCustomException("File does't exists in required server path ");
		}

		return flag;

	}

	public DateRangeBean getDateRangeStatusFlag() throws CommonCustomException, ParseException {
		
		Boolean declareWindow = false;
		Boolean currectionWindow = false;
		DateRangeBean bean = new DateRangeBean();
		Date curDate = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		//declareWindowEndDate = this.getDateIncByOneDay(declareWindowEndDate);
		//currectionWindowEndDate = this.getDateIncByOneDay(currectionWindowEndDate);

		Date declareStartDate = null;
		Date declareEndDate = null;
		Date currectionStartDate = null;
		Date currectionendDate = null;

		df.setLenient(false);

		try {

			declareStartDate = df.parse(declareWindowStartDate);
			
			declareEndDate = df.parse(declareWindowEndDate);
			currectionStartDate = df.parse(currectionWindowStartDate);
			currectionendDate = df.parse(currectionWindowEndDate);

			if (curDate.after(declareStartDate) && curDate.before(declareEndDate)) {
				bean.setDeclarationFlag(declareWindow = true);
				
			} else {
				bean.setDeclarationFlag(declareWindow = false);
			}
			if (curDate.after(currectionStartDate) && curDate.before(currectionendDate)) {
				bean.setCurrectionflag(currectionWindow = true);
			} else {
				bean.setCurrectionflag(currectionWindow = false);
			}
			if (!(curDate.after(declareStartDate) && curDate.before(declareEndDate))
					&& !(curDate.after(currectionStartDate) && curDate.before(currectionendDate))) {
				bean.setCurrectionflag(currectionWindow = false);
				bean.setDeclarationFlag(declareWindow = false);
			}
			

		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}

		return bean;
	}
	
	public DatTaxDocumentsMappingBO saveMappingDcoument(String docKeyRef, String docRefName,
			DatTaxDeclartionBO datTaxDeclartion, MultipartFile uploadSupportDocs) {

		DatTaxDocumentsMappingBO docMapping = null;
		
		if (docKeyRef != null) {
			docMapping = datTaxDocumentsMappingRepository.findByDocKey(docKeyRef);
		}

		if (docMapping == null) {
			docMapping = new DatTaxDocumentsMappingBO();
			docMapping.setDocKey(UUID.randomUUID().toString());
		} else {
			docMapping.setDocKey(docKeyRef);
		}

		docMapping.setDocName(uploadSupportDocs != null ? uploadSupportDocs.getOriginalFilename():"");
		docMapping.setDocRefName(docRefName);
		docMapping.setDocStatus(1);
		docMapping.setTaxDeclarationId(datTaxDeclartion.getPkTaxDeclarationId());
		docMapping.setFinanceYear(this.getFinanceYearByDate(datTaxDeclartion.getTaxSubmittedDate()));
		docMapping.setUpdatedDate(new Date());
		DatTaxDocumentsMappingBO mapResult = this.updateDocStatusToMapping(docMapping);

		return mapResult;
	}
	public String getDateIncByOneDay(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(date));
		c.add(Calendar.DATE, 1); // number of days to add
		date = sdf.format(c.getTime()); // dt is now the new date
		return date;
	}
	
	
	public boolean downloadDeclarationFormForFinance(HttpServletResponse response, TaxEmpReviewBean bean)
			throws CommonCustomException {
		Boolean isDownloaded = false;
		if (true) {
			try {

				String BASEPATH = basePath;

				BASEPATH = BASEPATH + File.separator + getFinanceYearByDate(bean.getStartDate()) + File.separator+bean.getEmpId()+File.separator;
				String fileName = bean.getEmpId() + FILE_NAME_SEPARATOR + taxEmpReviewFileName;
				DownloadFileUtil.downloadFile(response, BASEPATH, fileName);
				isDownloaded = true;
			} catch (Exception e) {
				throw new CommonCustomException(e.getMessage());
			}
		}
		return isDownloaded;
	}
};
