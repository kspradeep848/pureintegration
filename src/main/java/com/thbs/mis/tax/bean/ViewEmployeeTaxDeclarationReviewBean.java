package com.thbs.mis.tax.bean;

import java.util.List;

import com.thbs.mis.tax.bo.DatTaxDocumentsMappingBO;
import com.thbs.mis.tax.bo.DatTaxFirstTimeHouseOwnerBO;
import com.thbs.mis.tax.bo.DatTaxHouseLoanBO;
import com.thbs.mis.tax.bo.DatTaxHraBO;
import com.thbs.mis.tax.bo.DatTaxMedicalInsurancePremiumBO;

public class ViewEmployeeTaxDeclarationReviewBean {

	Integer categoryId;
	Integer typeId;
	Double totalTaxAmount;
	Integer docCount;
	List<DatTaxDocumentsMappingBO> docList;
	List<DatTaxHouseLoanBO> houseLoanList;
	List<DatTaxFirstTimeHouseOwnerBO> firstTimeHouseOwner;
	List<DatTaxMedicalInsurancePremiumBO> medicalInsurance;
	List<DatTaxHraBO> hraBOList;
	Integer approvedCount;
	Integer rejectedCount;
	Integer pendingCount;
	String categoryName;
	String typeName;
	String section;
	String empname;
	Integer empId;
	Integer taxDeclarationId;
	
	String declarationClosed;  

	public ViewEmployeeTaxDeclarationReviewBean() {

	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public Double getTotalTaxAmount() {
		return totalTaxAmount;
	}

	public Integer getDocCount() {
		return docCount;
	}

	public Integer getApprovedCount() {
		return approvedCount;
	}

	public Integer getRejectedCount() {
		return rejectedCount;
	}

	public Integer getPendingCount() {
		return pendingCount;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public void setTypeId(Integer integer) {
		this.typeId = integer;
	}

	public void setTotalTaxAmount(Double totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}

	public void setDocCount(Integer docCount) {
		this.docCount = docCount;
	}

	public void setApprovedCount(Integer approvedCount) {
		this.approvedCount = approvedCount;
	}

	public void setRejectedCount(Integer rejectedCount) {
		this.rejectedCount = rejectedCount;
	}

	public void setPendingCount(Integer pendingCount) {
		this.pendingCount = pendingCount;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public List<DatTaxDocumentsMappingBO> getDocList() {
		return docList;
	}

	public void setDocList(List<DatTaxDocumentsMappingBO> docList) {
		this.docList = docList;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public List<DatTaxHouseLoanBO> getHouseLoanList() {
		return houseLoanList;
	}

	public void setHouseLoanList(List<DatTaxHouseLoanBO> houseLoanList) {
		this.houseLoanList = houseLoanList;
	}

	public List<DatTaxFirstTimeHouseOwnerBO> getFirstTimeHouseOwner() {
		return firstTimeHouseOwner;
	}

	public void setFirstTimeHouseOwner(List<DatTaxFirstTimeHouseOwnerBO> firstTimeHouseOwner) {
		this.firstTimeHouseOwner = firstTimeHouseOwner;
	}

	public String getEmpname() {
		return empname;
	}

	public void setEmpname(String empname) {
		this.empname = empname;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public List<DatTaxMedicalInsurancePremiumBO> getMedicalInsurance() {
		return medicalInsurance;
	}

	public void setMedicalInsurance(List<DatTaxMedicalInsurancePremiumBO> medicalInsurance) {
		this.medicalInsurance = medicalInsurance;
	}

	public List<DatTaxHraBO> getHraBOList() {
		return hraBOList;
	}

	public void setHraBOList(List<DatTaxHraBO> hraBOList) {
		this.hraBOList = hraBOList;
	}	
	
	

	public Integer getTaxDeclarationId() {
		return taxDeclarationId;
	}

	public void setTaxDeclarationId(Integer taxDeclarationId) {
		this.taxDeclarationId = taxDeclarationId;
	}

	public String getDeclarationClosed() {
		return declarationClosed;
	}

	public void setDeclarationClosed(String declarationClosed) {
		this.declarationClosed = declarationClosed;
	}

	@Override
	public String toString() {
		return "ViewEmployeeTaxDeclarationReviewBean [categoryId=" + categoryId + ", typeId=" + typeId
				+ ", totalTaxAmount=" + totalTaxAmount + ", docCount=" + docCount + ", docList=" + docList
				+ ", houseLoanList=" + houseLoanList + ", firstTimeHouseOwner=" + firstTimeHouseOwner
				+ ", medicalInsurance=" + medicalInsurance + ", hraBOList=" + hraBOList + ", approvedCount="
				+ approvedCount + ", rejectedCount=" + rejectedCount + ", pendingCount=" + pendingCount
				+ ", categoryName=" + categoryName + ", typeName=" + typeName + ", section=" + section + ", empname="
				+ empname + ", empId=" + empId + ", taxDeclarationId=" + taxDeclarationId + ", declarationClosed="
				+ declarationClosed + "]";
	}
	

}
