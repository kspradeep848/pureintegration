package com.thbs.mis.tax.bean;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.Date;

public class TaxFinancialYearBean {
	Integer fromYear;

	Integer toYear;

	Integer fromMonth;

	Integer toMonth;

	public TaxFinancialYearBean(Date date) {
		LocalDate currentTime = date.toInstant().atZone(ZoneId.systemDefault())
				.toLocalDate();
		Integer currentYear = currentTime.getYear();
		Integer currentMonthValue = currentTime.getMonthValue();
		if (currentMonthValue <= 3) {
			this.toYear = currentYear.intValue();
			this.fromYear = ((currentYear.intValue()) - 1);
			this.toMonth = 3;
			this.fromMonth = 4;
		} else if (currentMonthValue > 3) {
			this.toYear = ((currentYear.intValue()) + 1);
			this.fromYear = currentYear.intValue();
			this.toMonth = 3;
			this.fromMonth = 4; 
		}
	}

	public Integer getFromYear() {
		return fromYear;
	}

	public Integer getToYear() {
		return toYear;
	}

	public Integer getFromMonth() {
		return fromMonth;
	}

	public Integer getToMonth() {
		return toMonth;
	}
	
	public String getMonthName(Integer monthValue) {
		return Month.of(monthValue).name().substring(0,3);
	}
	
	public String getFinancialYearStartDate() {
		
		return fromYear+"-"+fromMonth+"-"+"01";
	}
	
	
	public String getFinancialYearEndDate() {
		return toYear + "-" + toMonth + "-" + "31";
	}

}
