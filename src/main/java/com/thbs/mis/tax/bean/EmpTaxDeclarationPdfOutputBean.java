package com.thbs.mis.tax.bean;

import java.util.List;

public class EmpTaxDeclarationPdfOutputBean 
{
	private Integer employeeId;
	private String employeeName;
	private String assessmentYear;
	private List<ViewEmployeeTaxDeclarationReviewBean> taxDeclarationBeanList;
	
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public List<ViewEmployeeTaxDeclarationReviewBean> getTaxDeclarationBeanList() {
		return taxDeclarationBeanList;
	}
	public void setTaxDeclarationBeanList(
			List<ViewEmployeeTaxDeclarationReviewBean> taxDeclarationBeanList) {
		this.taxDeclarationBeanList = taxDeclarationBeanList;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getAssessmentYear() {
		return assessmentYear;
	}
	public void setAssessmentYear(String assessmentYear) {
		this.assessmentYear = assessmentYear;
	}
	@Override
	public String toString() {
		return "EmpTaxDeclarationPdfOutputBean [employeeId=" + employeeId
				+ ", employeeName=" + employeeName + ", assessmentYear="
				+ assessmentYear + ", taxDeclarationBeanList="
				+ taxDeclarationBeanList + "]";
	}
	
	
	
	
}
