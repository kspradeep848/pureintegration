package com.thbs.mis.tax.bean;


public class ViewEmployeesTaxDeclaration {

	private Integer approvedCount;
	private Integer rejectedCount;
	private Integer pendingCount;
	private Integer taxDeclaredCount;
	private Integer empId;
	private String empName;
	private String emailId;
	private String isAllDoccleared;
	
	public ViewEmployeesTaxDeclaration(){
		
	}
	
	public ViewEmployeesTaxDeclaration(Integer taxDeclaredCount, Integer pendingCount, Integer approvedCount,
			Integer rejectedCount) {
		this.taxDeclaredCount = taxDeclaredCount;
		this.pendingCount = pendingCount;
		this.approvedCount = approvedCount;
		this.rejectedCount = rejectedCount;

	}

	public Integer getApprovedCount() {
		return approvedCount;
	}
	public Integer getRejectedCount() {
		return rejectedCount;
	}
	public Integer getPendingCount() {
		return pendingCount;
	}
	public Integer getEmpId() {
		return empId;
	}
	public String getEmpName() {
		return empName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setApprovedCount(Integer approvedCount) {
		this.approvedCount = approvedCount;
	}
	public void setRejectedCount(Integer rejectedCount) {
		this.rejectedCount = rejectedCount;
	}
	public void setPendingCount(Integer pendingCount) {
		this.pendingCount = pendingCount;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getIsAllDoccleared() {
		return isAllDoccleared;
	}
	public void setIsAllDoccleared(String isAllDoccleared) {
		this.isAllDoccleared = isAllDoccleared;
	}
	public Integer getTaxDeclaredCount() {
		return taxDeclaredCount;
	}
	public void setTaxDeclaredCount(Integer taxDeclaredCount) {
		this.taxDeclaredCount = taxDeclaredCount;
	}
	@Override
	public String toString() {
		return "ViewEmployeesTaxDeclaration [approvedCount=" + approvedCount + ", rejectedCount=" + rejectedCount
				+ ", pendingCount=" + pendingCount + ", taxDeclaredCount=" + taxDeclaredCount + ", empId=" + empId
				+ ", empName=" + empName + ", emailId=" + emailId + ", isAllDoccleared=" + isAllDoccleared + "]";
	}
	
	
}
