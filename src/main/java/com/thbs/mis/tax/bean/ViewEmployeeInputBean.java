package com.thbs.mis.tax.bean;

import java.util.Date;

public class ViewEmployeeInputBean {

	private Integer empId;
	private Date startDate;
	private Date endDate;
	private Integer categoryId;
	private Integer typeId;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	@Override
	public String toString() {
		return "ViewEmployeeInputBean [empId=" + empId + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", categoryId=" + categoryId + ", typeId=" + typeId + "]";
	}

}
