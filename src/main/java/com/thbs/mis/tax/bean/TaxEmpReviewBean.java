package com.thbs.mis.tax.bean;

import java.util.Date;

public class TaxEmpReviewBean {
	private Integer empId;
	private Date startDate;
	private Date endDate;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "TaxEmpReviewBean [empId=" + empId + ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}

}
