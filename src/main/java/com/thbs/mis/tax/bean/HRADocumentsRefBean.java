/**
 * 
 */
package com.thbs.mis.tax.bean;

/**
 * @author annapoorani_n
 *
 */
public class HRADocumentsRefBean {
	
	private String renReceiptsRefKey ;
	
	private String rentAgreementRefKey;
	
	private String utilityBillRefKey;
	
	private String houseOwnerPANKey;

	public String getRenReceiptsRefKey() {
		return renReceiptsRefKey;
	}

	public void setRenReceiptsRefKey(String renReceiptsRefKey) {
		this.renReceiptsRefKey = renReceiptsRefKey;
	}

	public String getRentAgreementRefKey() {
		return rentAgreementRefKey;
	}

	public void setRentAgreementRefKey(String rentAgreementRefKey) {
		this.rentAgreementRefKey = rentAgreementRefKey;
	}

	public String getUtilityBillRefKey() {
		return utilityBillRefKey;
	}

	public void setUtilityBillRefKey(String utilityBillRefKey) {
		this.utilityBillRefKey = utilityBillRefKey;
	}

	public String getHouseOwnerPANKey() {
		return houseOwnerPANKey;
	}

	public void setHouseOwnerPANKey(String houseOwnerPANKey) {
		this.houseOwnerPANKey = houseOwnerPANKey;
	}

	@Override
	public String toString() {
		return "HRADocumentsRefBean [renReceiptsRefKey=" + renReceiptsRefKey + ", rentAgreementRefKey="
				+ rentAgreementRefKey + ", utilityBillRefKey=" + utilityBillRefKey + ", houseOwnerPANKey="
				+ houseOwnerPANKey + "]";
	}
	
	

}
