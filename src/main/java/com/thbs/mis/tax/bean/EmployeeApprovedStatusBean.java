package com.thbs.mis.tax.bean;

public class EmployeeApprovedStatusBean {

	private Integer declarationId;
	private String docKey;
	private Integer pkDocId;
	private Integer status;
	private String comments;

	public String getDocKey() {
		return docKey;
	}

	public void setDocKey(String docKey) {
		this.docKey = docKey;
	}

	public Integer getPkDocId() {
		return pkDocId;
	}

	public void setPkDocId(Integer pkDocId) {
		this.pkDocId = pkDocId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getDeclarationId() {
		return declarationId;
	}

	public void setDeclarationId(Integer declarationId) {
		this.declarationId = declarationId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "EmployeeApprovedStatusBean [declarationId=" + declarationId + ", docKey=" + docKey + ", pkDocId="
				+ pkDocId + ", status=" + status + ", comments=" + comments + "]";
	}

}
