package com.thbs.mis.tax.bean;

public class DeleteEmployeeDataBean {

	private Integer declareId;
	private Integer typeID;
	private String financialYear;
	private Integer empID;

	public Integer getDeclareId() {
		return declareId;
	}

	public void setDeclareId(Integer declareId) {
		this.declareId = declareId;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}

	public Integer getEmpID() {
		return empID;
	}

	public void setEmpID(Integer empID) {
		this.empID = empID;
	}

	@Override
	public String toString() {
		return "DeleteEmployeeDataBean [declareId=" + declareId + ", typeID=" + typeID + ", financialYear="
				+ financialYear + ", empID=" + empID + "]";
	}

}
