package com.thbs.mis.tax.bean;

public class DateRangeBean {

	private Boolean declarationFlag;
	private Boolean currectionflag;

	public Boolean getDeclarationFlag() {
		return declarationFlag;
	}

	public void setDeclarationFlag(Boolean declarationFlag) {
		this.declarationFlag = declarationFlag;
	}

	public Boolean getCurrectionflag() {
		return currectionflag;
	}

	public void setCurrectionflag(Boolean currectionflag) {
		this.currectionflag = currectionflag;
	}

	@Override
	public String toString() {
		return "DateRangeBean [declarationFlag=" + declarationFlag + ", currectionflag=" + currectionflag + "]";
	}

}
