package com.thbs.mis.tax.bean;


public class EmpTaxDeclarationReviewPdfOutputBean 
{
	String totalTaxAmount;
	Integer docCount;
	String categoryName;
	String typeName;
	String section;
	public String getTotalTaxAmount() {
		return totalTaxAmount;
	}
	public void setTotalTaxAmount(String totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}
	public Integer getDocCount() {
		return docCount;
	}
	public void setDocCount(Integer docCount) {
		this.docCount = docCount;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	@Override
	public String toString() {
		return "EmpTaxDeclarationReviewPdfOutputBean [totalTaxAmount="
				+ totalTaxAmount + ", docCount=" + docCount + ", categoryName="
				+ categoryName + ", typeName=" + typeName + ", section="
				+ section + "]";
	}
	

	
}
