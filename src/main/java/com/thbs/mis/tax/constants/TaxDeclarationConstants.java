package com.thbs.mis.tax.constants;

public class TaxDeclarationConstants {
	// Creation services
	public static final String GET_TAX_TYPE_BY_CATEGORY_ID = "/tax/types/category/{typeId}";
	public static final String CREATE_TAX_HOUSE_LOAN = "/tax/house/loan/{categoryId}/{typeId}";
	public static final String CREATE_TAX_FIRST_TIME_HOUSE_OWNER = "/tax/first/time/house/owner/{categoryId}/{typeId}";
	public static final String CREATE_TAX_MEDICAL_INSURANCE_PREMIUM = "/tax/medical/insurance/premium/{categoryId}/{typeId}";
	public static final String CREATE_TAX_HRA = "/tax/hra/{categoryId}/{typeId}";
	public static final String CREATE_TAX_CLEARANCE = "/tax/dat/cleareance";
	public static final String CREATE_TAX_DECLARATION = "/tax/declaration/{categoryId}/{typeId}";
	// view service
	// public static final String GET_TAX_DECLARATION_REVIEW_BY_EMP =
	// "/tax/declaration/employee/{empId}/{startDate}/{endDate}";
	// public static final String GET_ALL_TAX_DECLARATION =
	// "/tax/declaration/employees";
	// scheduler
	// public static final String TAX_EMAIL_NOTIFY_BY_EMP =
	// "/tax/declaration/correction/scheduler/{action}";
	public static final String TAX_UPLOAD_EMP_SIGNATURE = "/tax/declaration/employee/signature/{empId}/{fYear}";
	public static final String DELETE_DATA = "/tax/data/delete";
	public static final String EMP_REVIEW_SERVICE = "/tax/emp/review/service";
	public static final String VIEW_ALL_TAX_DECLARATION_DETAILS = "/tax/view/declaration/details";
	public static final String EMP_VIEW_SERVICE = "/tax/emp/view/service";
	public static final String UPDATE_DOC_STATUS = "/tax/emp/update/doc/status";
	public static final String TIME_RANGE_STATUS = "/tax/time/range/status";
	
	
	public static final String DOWNLOAD_JOINT_DECLARATION = "/tax/download/Jointform";
	public static final String GET_DOC_DOWNLOAD = "/tax/doc/download/{key}";
	
	//Added by Shyam
	public static final String DOWNLOAD_EMP_REVIEW_PDF = "/tax/emp/review/download";
	
	public static final String DOWNLOAD_FORM_12C="/tax/download/form12C";
	
	public static final String DOWNLOAD_FINANCE_EMP_REVIEW_PDF = "/tax/finance/emp/review/download";
	
	
	public static final String RENT_RECEIPTS_KEY = "_RENT_RECEIPT";
	
	public static final String FILE_NAME_SEPARATOR = "_";
	
	public static final String RENT_AGREEMENT_KEY = "_RENT_AGREEMENT";
	
	public static final String UTILITY_BILL_KEY ="_UTILITY_BILL";
	
	public static final String PAN_HOUSE_OWNER_KEY = "_PAN_HOUSE_OWNER";
	
	public static final String HOUSE_LOAN_KEY ="_HOUSE_LOAN";
	
	public static final String JOINT_DECLARATION_KEY = "_JOINT_DECLARATION";
	
	public static final String FIRST_TIME_HOUSE_OWNER_KEY ="_FIRST_TIME_HOUSE_OWNER";
	
	public static final String MEDICAL_INSURANCE_KEY = "_MEDICAL_INSURANCE";
	


}
