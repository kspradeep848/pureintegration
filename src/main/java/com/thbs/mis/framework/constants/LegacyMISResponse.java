package com.thbs.mis.framework.constants;

import java.util.List;

public class LegacyMISResponse {


	private int statusCode;
	private String status;
	private String message;
	private List<?> result;
	
	public LegacyMISResponse() {

	}

	public LegacyMISResponse(int statusCode, String status, String message) {
		this.statusCode = statusCode;
		this.status = status;
		this.message = message;
	}

	public LegacyMISResponse(int statusCode, String status, String message,
			List<?> result) {
		this.statusCode = statusCode;
		this.status = status;
		this.message = message;
		this.result = result;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<?> getResult() {
		return result;
	}

	public void setResult(List<?> result) {
		this.result = result;
	}



	@Override
	public String toString() {
		return "MISResponse [statusCode=" + statusCode + ", status=" + status
				+ ", message=" + message + ", result=" + result
				 + "]";
	}


}
