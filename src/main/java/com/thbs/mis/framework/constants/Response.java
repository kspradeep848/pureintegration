package com.thbs.mis.framework.constants;

public class Response {
	private int sucessCode;
	private String sucessMessage;
	private String suceesMessageKey;
	public int getSucessCode() {
		return sucessCode;
	}
	public void setSucessCode(int sucessCode) {
		this.sucessCode = sucessCode;
	}
	public String getSucessMessage() {
		return sucessMessage;
	}
	public void setSucessMessage(String sucessMessage) {
		this.sucessMessage = sucessMessage;
	}
	public String getSuceesMessageKey() {
		return suceesMessageKey;
	}
	public void setSuceesMessageKey(String suceesMessageKey) {
		this.suceesMessageKey = suceesMessageKey;
	}
	@Override
	public String toString() {
		return "Response [sucessCode=" + sucessCode + ", sucessMessage="
				+ sucessMessage + ", suceesMessageKey=" + suceesMessageKey
				+ "]";
	}
	
}
