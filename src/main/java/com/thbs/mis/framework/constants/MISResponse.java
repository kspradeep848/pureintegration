/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  MISResponse.java                                  */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  Dec 09, 2016                                      */
/*                                                                   */
/*  Description :  POJO class for setting the Response Entity    	 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* Jun 19, 2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.framework.constants;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;

public class MISResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int statusCode;
	private String status;
	private String message;
	private List<?> result;
	private Page<?> pageResult;
	
	private Boolean empStatus;
	public MISResponse() {

	}

	public MISResponse(int statusCode, String status, String message) {
		this.statusCode = statusCode;
		this.status = status;
		this.message = message;
	}

	public Boolean getEmpStatus() {
		return empStatus;
	}

	public void setEmpStatus(Boolean empStatus) {
		this.empStatus = empStatus;
	}

	public MISResponse(int statusCode, String status, String message,
			List<?> result) {
		this.statusCode = statusCode;
		this.status = status;
		this.message = message;
		this.result = result;
	}
	public MISResponse(int statusCode, String status, String message,
			 Page<?> pageResult) {
		super();
		this.statusCode = statusCode;
		this.status = status;
		this.message = message;
		this.result = result;
		this.pageResult = pageResult;
	}
	public MISResponse(int statusCode, String status, String message,
			Boolean empStatus) {
		super();
		this.statusCode = statusCode;
		this.status = status;
		this.message = message;
		this.empStatus = empStatus;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<?> getResult() {
		return result;
	}

	public void setResult(List<?> result) {
		this.result = result;
	}

	public Page<?> getPageResult() {
		return pageResult;
	}

	public void setPageResult(Page<?> pageResult) {
		this.pageResult = pageResult;
	}



	@Override
	public String toString() {
		return "MISResponse [statusCode=" + statusCode + ", status=" + status
				+ ", message=" + message + ", result=" + result
				+ ", pageResult=" + pageResult + "]";
	}

}