/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  MISConstants.java                                		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  Jun 19, 2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* Jun 19, 2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.framework.constants;

/**
 * <Description MISConstants:> TODO
 * @author THBS
 * @version 1.0
 * @see 
 */
public interface MISConstants
{

	String EXCEPTION_MESS = "Exception occured in method ";
	
	String COLON =" : ";
	
	String Accept="ACCEPTED";
	String ALREADY_ACCEPTED="Already Accepted";
	String FORCE_CLOSED="FORCE CLOSED";
	String CLOSED="CLOSED";
	String APPROVED="APPROVED";
	String REJECTED="REJECTED";
	String DELETED="DELETED";
	String ADDED="ADDED";
	
	Byte ForceCloseStatus=7;
	Byte DELEGATION_STATUS=2;
	String FEEDBACK_NOT_PROVIDED="FEEDBACK_NOT_PROVIDED";
	
	//Added by Kamal Anand for Response Entity Status
	String SUCCESS = "SUCCESS";
	String FAILURE = "FAILURE";
	//End of Addition by Kamal Anand for Response Entity Status
	
	//Added by Rajesh Kumar for Creation Status
	String CREATED = "CREATED";
	//End of Addition by Rajesh Kumar for Creation Status
}
