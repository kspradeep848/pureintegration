/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  MailTemplate.java           		             */
/*                                                                   */
/*  Author      :  Pradeep K S	                                     */
/*                                                                   */
/*  Date        :  16-Mar-2017                                       */
/*                                                                   */
/*  Description :  MailTemplate Class for New MIS Application  */
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 16-Mar-2017    THBS     1.0         Initial version created   	 */
/*********************************************************************/

package com.thbs.mis.framework.mail;

public class MailTemplate {

    private String templateId = null;

    private String subject = null;

    private String body = null;
    
 

    /**
	 * @param template
	 */
	public MailTemplate(MailTemplate template) {
		
		templateId=template.getTemplateId();
		subject=template.getSubject();
		body=template.getBody();
		
	}

	public MailTemplate() {
		
	}
	/**
     * @return Returns the body.
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body
     *            The body to set.
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return Returns the subject.
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject
     *            The subject to set.
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return Returns the templateId.
     */
    public String getTemplateId() {
        return templateId;
    }

    /**
     * @param templateId
     *            The templateId to set.
     */
    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    /**
     * Constructs a <code>String</code> with all attributes
     * in name = value format.
     *
     * @return a <code>String</code> representation 
     * of this object.
     */

	
    public String toString()
    {
        final String TAB = "    ";
        
        StringBuffer retValue = new StringBuffer();
        
        retValue.append("MailTemplate ( ")
            .append(super.toString()).append(TAB)
            .append("templateId = ").append(this.templateId).append(TAB)
            .append("subject = ").append(this.subject).append(TAB)
            .append("body = ").append(this.body).append(TAB)
             .append(" )");
        
        return retValue.toString();
    }

	
}