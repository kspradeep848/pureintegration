/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmailServiceUtil.java           		             */
/*                                                                   */
/*  Author      :  Pradeep K S	                                     */
/*                                                                   */
/*  Date        :  16-Mar-2017                                       */
/*                                                                   */
/*  Description :  EMail Service util Class for New MIS Application  */
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 16-Mar-2017    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.framework.mail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;



@Service
public class EmailServiceUtil  {

	private static final AppLog LOG = LogFactory
			.getLog(EmailServiceUtil.class);

	@Value("${smtp.host}")
	private String smtpHost;

	@Value("${smtp.port}")
	private int smtpPort;

	@Value("${smtp.username}")
	private String smtpUserName;

	@Value("${smtp.password}")
	private String smtpPassword;

	@Value("${notSendmailAddress}")
	private String notToSendmail;
	
	@Autowired
	private MailSender mailSender;
	
	@Value("${confirmation.template}")
	private File confirmationLetter;

	public void sendMail(List<String> to, List<String> cc, String subject,
			String emailBody) throws Exception {
		try {
			Properties props = new Properties();
			props.put("mail.smtp.host", smtpHost);
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.port", smtpPort);
			props.put("mail.smtp.auth", "true");
			props.put("smtp.username", smtpUserName);

			Authenticator auth = new SMTPAuthenticator();

			Session session = Session.getInstance(props, auth);
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(smtpUserName));
			for (String toAddress : to) {
				if (!toAddress.equalsIgnoreCase(notToSendmail))
					message.addRecipient(Message.RecipientType.TO,
							new InternetAddress(toAddress.toString()));
			}
			if (cc != null) {
				for (String ccAddress : cc) {
					if (!ccAddress.equalsIgnoreCase(notToSendmail))
						message.addRecipient(Message.RecipientType.CC,
								new InternetAddress(ccAddress.toString()));
				}
			}
			message.setSubject(subject);
			message.setContent(emailBody, "text/html; charset=ISO-8859-1");
			LOG.debug("Sending email to " + to.toString());
			Transport.send(message);
			LOG.debug("Email sent successfully to " + to.toString());
		} catch (Exception e) {
			LOG.error("An Exception occured while sending email:", e);
			throw new Exception(
					"An Exception occured while sending email:", e);
		}

	
	}


	public void sendMail(List<String> to, List<String> cc, String subject,
			String fileName, Object... args) throws Exception {
		// TODO Auto-generated method stub
		LOG.info("Email Template :" + fileName);

		StringBuilder template = new StringBuilder();
		try {
			InputStream inputStream = new FileInputStream(fileName);
			InputStreamReader templateReader = new InputStreamReader(
					inputStream);
			BufferedReader bufferedReader = new BufferedReader(templateReader);
			String mailData;
			while ((mailData = bufferedReader.readLine()) != null) {
				template.append(mailData);
				template.append("\n");
			}
			LOG.info("Email Template :" + template);
		} catch (IOException e) {
			LOG.error("An Exception occured while creating email content "
					+ " template file: " + fileName, e);
			throw new Exception(
					"An Exception occured while processing email content "
							+ " template file: " + fileName, e);
		}

		
		try {

			String emailBody = MessageFormat.format(template.toString(), args);
			LOG.debug("Email Body :" + emailBody);
			this.sendMail(to, cc, subject, emailBody);

		} catch (Exception e) {
			LOG.error("An Exception occured while sending email :", e);
		}
	}
	
	public void sendMailWithAttchment(List<String> to, List<String> cc, String subject, String filePath, String fileName, String empName, String mgrName, Integer status ) throws Exception {
		// TODO Auto-generated method stub
		//LOG.info("Email Template :" + fileName);

		StringBuilder template = new StringBuilder();
		Properties props = new Properties();
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.port", smtpPort);
		props.put("mail.smtp.auth", "true");
		props.put("smtp.username", smtpUserName);
		
		String toAddressList = to.stream()
                .collect(Collectors.joining(","));
		
		String ccAddressList = cc.stream()
                .collect(Collectors.joining(","));
		
		Authenticator auth = new SMTPAuthenticator();

		Session session = Session.getInstance(props, auth);
		String confLetterTemplate = confirmationLetter.getPath();
		String statusMsg = "employment has been confirmed.";
		
		if(status == 4) {
			 statusMsg = "probation period has been extended.";
		}
			 Message message = new MimeMessage(session);
		        message.setFrom(new InternetAddress("confirmations@thbs.com"));
		        message.setRecipients(Message.RecipientType.TO,
		                InternetAddress.parse(toAddressList));

		        message.setRecipients(Message.RecipientType.CC,
		                InternetAddress.parse(ccAddressList));
		        message.setSubject(subject);
		       // message.s(confLetterTemplate);
				
		        //File htmlTemplateFile = new File(confLetterTemplate);
		        String htmlString = "Dear " +empName 
		        		+ "\n \n After review of your performance by your Reporting Manager " +mgrName +","
		        		+ " we are pleased to let you know that your " + statusMsg
		        		+ " \n\n For further details, please log on to 'https://my.thbs.com'"
		        		+ " \n\n Regards \n Team HR "
		        		+ "\n\n P.S - This is an automated e-mail generated from system. "
		        		+ "Please do not reply to this e-mail. \n\n ";
		     
		        BodyPart messageBodyPart1 = new MimeBodyPart();

		        Multipart multipart = new MimeMultipart();

		        MimeBodyPart messageBodyPart2 = new MimeBodyPart();
		       
		        FileDataSource source = new FileDataSource(filePath);
		        messageBodyPart2.setDataHandler(new DataHandler(source));
		        messageBodyPart2.setFileName(fileName);
		        messageBodyPart1.setText(htmlString);
		        multipart.addBodyPart(messageBodyPart1);
		        multipart.addBodyPart(messageBodyPart2);
		        
		        message.setContent(multipart);

		        System.out.println("Sending");

		        Transport.send(message);

		        System.out.println("Sent Email");

			
		
		LOG.info("Email Template :" + template);
					}
		
		
	
	
	public void sendTextMail(List<String> to, List<String> cc, String subject,
			String fileName, Object... args) {

		/*StringBuilder template = new StringBuilder();
		try {
			InputStream inputStream = new FileInputStream(fileName);
			InputStreamReader templateReader = new InputStreamReader(
					inputStream);
			BufferedReader bufferedReader = new BufferedReader(templateReader);
			String mailData;
			while ((mailData = bufferedReader.readLine()) != null) {
				template.append(mailData);
				template.append("\n");
			}
			LOG.info("Email Template :" + template);
		} catch (IOException e) {
			LOG.error("An Exception occured while creating email content "
					+ " template file: " + fileName, e);
			throw new CISMailException(
					"An Exception occured while processing email content "
							+ " template file: " + fileName, e);
		}
		
		try {

			String emailBody = MessageFormat.format(template.toString(), args);
			LOG.debug("Email Body :" + emailBody);
			this.sendMail(to, cc, subject, emailBody);

		} catch (Exception e) {
			LOG.error("An Exception occured while sending email :", e);
		}*/
		SimpleMailMessage message = new SimpleMailMessage();
/*String to="pradeep_shanthaveerappa@thbs.com";
String cc="pradeep_shanthaveerappa@thbs.com";*/
		message.setTo("pradeep_shanthaveerappa@thbs.com");
		message.setCc("pradeep_shanthaveerappa@thbs.com");
			message.setFrom("mis-admin@thbs.com");
			message.setSubject("Welcome to Torry Harris Registration Portal");
			message.setText("Hi Pradeep,WelCome to the Torry Harris Registration portal.Please Login the portal with the credentials given below and fill your personal,educational and other details and submit the details.");
			
			mailSender.send(message);
	 
	}
	class SMTPAuthenticator extends javax.mail.Authenticator {

		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(smtpUserName, smtpPassword);
		}
	}

}
