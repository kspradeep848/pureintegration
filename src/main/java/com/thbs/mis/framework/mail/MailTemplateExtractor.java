/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  MailTemplate.java           		             */
/*                                                                   */
/*  Author      :  Pradeep K S	                                     */
/*                                                                   */
/*  Date        :  16-Mar-2017                                       */
/*                                                                   */
/*  Description :  MailTemplateExtractor Class for New MIS Application*/
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 16-Mar-2017    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.framework.mail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.thbs.mis.framework.exception.ConfigException;
import com.thbs.mis.framework.exception.TemplateNotFoundException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.IOUtil;
import com.thbs.mis.framework.util.StringUtil;
import com.thbs.mis.framework.util.XMLUtil;




/**
 * This class parses the template XML and forms <code>MailTemplate</code>
 * object
 * 
 * @author sougata
 *  
 */
public final class MailTemplateExtractor {

	
	private static final AppLog LOG = LogFactory
			.getLog(MailTemplateExtractor.class);
    /**
     * instance of MailTemplateExtractor
     */
    private static MailTemplateExtractor tmpExtr = null;

    /**
     * The name of the template tag
     */
    private static final String TEMPLATE_TAG = "template";

    /**
     * The name of the template id
     */
    private static final String TEMPLATE_TAG_ID = "id";

    /**
     * The name of the template subject
     */
    private static final String TEMPLATE_TAG_SUBJECT = "subject";

    /**
     * The name of the template body
     */
    private static final String TEMPLATE_TAG_BODY = "body";

    /**
     * instance of HashMap
     */
    private static Map templateMap = null;

    /**
     * file name for mail_templates.xml
     */
    private static final String MAIL_TEMPLATE_FILE 
    										= "./templates/mail_templates.xml";

    /**
     * default constructor
     * 
     * @throws ConfigException
     */
    private MailTemplateExtractor() throws ConfigException {

        Document d = null;

        try {

            if (!StringUtil.isEmpty(MAIL_TEMPLATE_FILE)) {

                URL url = IOUtil.locate(MAIL_TEMPLATE_FILE);

                if (url.getFile() != null) {

                    File file = new File(url.getFile());

                    d = XMLUtil.parse(file);

                } else
                    throw new ConfigException(
                            "Either file missing or can't read "
                                    + MAIL_TEMPLATE_FILE);
            } else
                throw new ConfigException("File " + MAIL_TEMPLATE_FILE
                        + " not found.");

            loadAllTemplates(d);

        } catch (ConfigException e) {
            LOG.fatal(e.getMessage(), e);
            throw new ConfigException(e);

        } catch (FileNotFoundException e) {
            LOG.fatal(e.getMessage(), e);
            throw new ConfigException(e);

        } catch (ParserConfigurationException e) {
            LOG.fatal(e.getMessage(), e);
            throw new ConfigException(e);

        } catch (SAXException e) {
            LOG.fatal(e.getMessage(), e);
            throw new ConfigException(e);

        } catch (IOException e) {
            LOG.fatal(e.getMessage(), e);
            throw new ConfigException(e);
        }
    }

    /**
     * load all templates from the given xml
     * 
     * @throws ConfigException
     */
    private static void loadAllTemplates(Document d) throws ConfigException {

        if (d != null) {

            NodeList nl = XMLUtil.getNodeList(d, TEMPLATE_TAG);

            if (nl == null || nl.getLength() == 0)
                throw new ConfigException("Error while parsing  "
                        + MAIL_TEMPLATE_FILE + " ,TAG[ " + TEMPLATE_TAG + " ] "
                        + " not Found ");

            templateMap = new HashMap(nl.getLength());
            MailTemplate template = null;
            String templateId = null;

            Element el = null;

            if (nl != null && nl.getLength() > 0) {
                for (int i = 0; i < nl.getLength(); i++) {
                    el = (Element) nl.item(i);

                    templateId = el.getAttribute(TEMPLATE_TAG_ID);

                    if (templateMap != null
                            && templateMap.containsKey(templateId)) {
                        templateMap = null;
                        throw new ConfigException(
                                "Duplicate template id in the File "
                                        + MAIL_TEMPLATE_FILE);
                    }

                    template = new MailTemplate();
                    template.setTemplateId(templateId);
                    template.setSubject(XMLUtil.getValueByTagName(el,
                            TEMPLATE_TAG_SUBJECT));
                    template.setBody(XMLUtil.getValueByTagName(el,
                            TEMPLATE_TAG_BODY));

                    templateMap.put(template.getTemplateId(), template);

                }
            }
        }
    }

    /**
     * 
     * @param templateId
     *            a <code>String</code> containing template id
     * @return <code>MailTemplate</code>, a Business Object
     * @throws ConfigException
     */
    public static MailTemplate getTemplate(String templateId)
            throws ConfigException {

        if (templateMap == null) {
            new MailTemplateExtractor();
        }

        if (templateMap != null && !templateMap.containsKey(templateId)) {
            LOG.fatal("No template found with id = " + templateId + " in "
                    + MAIL_TEMPLATE_FILE);

            throw new TemplateNotFoundException("No template found with id = "
                    + templateId + " in " + MAIL_TEMPLATE_FILE);
        }
        return templateMap == null || templateMap.size() == 0 ? null
                : (MailTemplate) templateMap.get(templateId);
    }

    public static MailTemplate getTemplate(String templateId, Map map)
            throws ConfigException {

        MailTemplate template = getTemplate(templateId); 

        if (template == null) {

            throw new TemplateNotFoundException("No template found with id = "
                    + templateId + " in " + MAIL_TEMPLATE_FILE);
        }
        MailTemplate temp = new MailTemplate(template);
        temp.setBody(substitute(temp.getBody(), map));

        return temp;

    }

    /**
     * Substitutes the values runtime.
     * 
     * @param target
     *            string to be substituted.
     * @param map
     *            key-pair used for substitution.
     * @return final text
     */
    private static String substitute(String target, Map map) {

        StringBuffer result = new StringBuffer(target);
      
        if (map != null && !map.isEmpty()) {

            Iterator itr = map.keySet().iterator();

            while (itr.hasNext()) {

                int i = 0;
                
                String key = (String) itr.next();

                String value = (String) map.get(key);

                i = result.indexOf(key);
                
                result.replace(i, i + key.length(), (String) map.get(key));
            }
        }
        return result.toString();
    }
}