package com.thbs.mis.framework.logging;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;

@Aspect
public class RestAccessLogger {

	@Autowired
	private HttpServletRequest request;
	
	
	
	
		/*@Pointcut("within(com.xyz.someapp.web..*)")
	  public void inWebLayer() {}*/
	
	/** Following is the definition for a pointcut to select
	    *  all the methods available. So advice will be called
	    *  for all the methods.
	    */
	
	
	
	
	
	  // @Pointcut("execution(* com.thbs.mis..*.*(..))")
		 @Pointcut("execution(* com.thbs.mis.*.controller.*(..))")
	   private void selectAll(){}

	@Before("selectAll()")
	public void logEntering(JoinPoint joinPoint) {

		AppLog log = LogFactory.getLog(joinPoint.getTarget().getClass());
		log.debug("Received request1 " + request.getRequestURL());
		log.info("Entering method :- " + joinPoint); //Added By Nibedita
		
		log.info("Entering method :- " + joinPoint.getArgs()); 
		
		//joinPoint.
		System.out.println("I am in the logger");
	}

	@After("selectAll()")
	public void logExeting(JoinPoint joinPoint) {

		AppLog log = LogFactory.getLog(joinPoint.getTarget().getClass());
		log.debug("Received request1 " + request.getRequestURL());
		log.info("Exiting method :- " + joinPoint); //Added By Nibedita
		System.out.println("I am in the logger");
	}
}
