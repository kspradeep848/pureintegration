/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  MISAspectLogger.java                                		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  Jun 19, 2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* Jun 19, 2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/

package com.thbs.mis.framework.logging;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import com.thbs.mis.framework.constants.MISConstants;

/**
 * <Description MISAspectLogger:> TODO
 * @author THBS
 * @version 1.0
 * @see 
 */
@Aspect
public class MISAspectLogger
{

	private AppLog LOG;

	/*
	 * @Pointcut("within(com.xyz.someapp.web..*)") public void
	 * inWebLayer() {}
	 */

	/**
	 * - Following is the definition for a pointcut to select all the
	 * methods available. So advice will be called for all the methods.
	 */

	@Pointcut("execution(* com.thbs.mis..*.*(..))")
	private void selectAll()
	{
	};

	@Pointcut("!within(com.thbs.mis..*..*BO)")
	private void excludeBOCalls()
	{
	};
	
	@Pointcut("!within(com.thbs.mis..*..*Bean)")
	private void excludeBeanCalls()
	{
	};

	@Before("selectAll() && excludeBOCalls() && excludeBeanCalls()")
	public void logEnteringMethod(JoinPoint joinPoint)
	{

		LOG = LogFactory.getLog(joinPoint.getTarget().getClass());

		LOG.entering(joinPoint.getSignature().getName(),
				joinPoint.getArgs());

	}

	@AfterReturning(pointcut = "selectAll() && excludeBOCalls() && excludeBeanCalls()", returning = "result")
	public void logExitingMethod(JoinPoint joinPoint, Object result)
	{

		LOG = LogFactory.getLog(joinPoint.getTarget().getClass());

		LOG.exiting(joinPoint.getSignature().getName(), result);
	}

	@AfterThrowing(pointcut = "selectAll()", throwing = "error")
	public void logAfterThrowingException(JoinPoint joinPoint,
			Throwable error)
	{

		LOG = LogFactory.getLog(joinPoint.getTarget().getClass());
		String exceptionMess = MISConstants.EXCEPTION_MESS
				+ joinPoint.getSignature().getName()
				+ MISConstants.COLON;
		LOG.fatal(exceptionMess, error);

	}
}
