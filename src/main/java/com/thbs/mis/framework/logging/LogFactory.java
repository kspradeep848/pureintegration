/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  LogFactory.java                                		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  16-Jun-2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 16-Jun-2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.framework.logging;

import java.util.HashMap;

public final class LogFactory
{
	/**
	 * Cache of loggers.
	 */
	private static final HashMap<String, AppLog> M_LOG_INSTANCES = new HashMap<String, AppLog>();

	/**
	 * Default Constructor
	 */
	private LogFactory()
	{
	}

	/**
	 * Gets the instance of logger which is associated with the given
	 * class.
	 * <p>
	 * This method searches its cache for the logger instance with the
	 * given class name.
	 * <p>
	 * If an instance of the logger is not found in the cache, then a
	 * new instance of logger is created and returned.
	 *
	 * @param clss
	 *            a <code>Class</code> object interested in using the
	 *            logger.
	 * 
	 * @return an implementation of <code>AppLog</code>
	 * 
	 */
	public static AppLog getLog(Class<?> clss)
	{

		String name = clss.getName();

		AppLog log = (AppLog) M_LOG_INSTANCES.get(name);

		if (log == null)
		{
			synchronized (LogFactory.class)
			{
				log = new AppLogger(clss);

				M_LOG_INSTANCES.put(name, log);
			}
		}
		return log;
	}
}