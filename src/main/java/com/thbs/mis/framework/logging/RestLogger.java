package com.thbs.mis.framework.logging;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;

@Aspect
public class RestLogger {

	@Autowired
	private HttpServletRequest request;

	@Before("execution(* com.thbs.mis.controller.*Controller.*(..))")
	public void logEntering(JoinPoint joinPoint) {

		Log log = LogFactory.getLog(joinPoint.getTarget().getClass());
		log.debug("Received request " + request.getRequestURL());
		log.info("Entering method :- " + joinPoint); //Added By Nibedita
		System.out.println("I am in the logger");
	}

	@After("execution(* com.thbs.mis.controller.*Controller.*(..))")
	public void logExeting(JoinPoint joinPoint) {

		Log log = LogFactory.getLog(joinPoint.getTarget().getClass());
		log.debug("Received request " + request.getRequestURL());
		log.info("Exiting method :- " + joinPoint); //Added By Nibedita
		System.out.println("I am in the logger");
	}
	/*//Added by Nibedita
	@Before("execution(* com.thbs.intranet.rest.service.impl.*.*(..))")
	public void logServiceImplEntering(JoinPoint joinPoint) {

		Log log = LogFactory.getLog(joinPoint.getTarget().getClass());
		log.info("Entering method :- " + joinPoint); 
	}
	@Before("execution(* com.thbs.intranet.rest.dao.misdatabase.impl.*.*(..))")
	public void logDaoImplEntering(JoinPoint joinPoint) {

		Log log = LogFactory.getLog(joinPoint.getTarget().getClass());
		log.debug("Received request " + request.getRequestURL());
		log.info("Entering method :- " + joinPoint); //Added By Nibedita
	}
	//End of addition by Nibedita
	@AfterReturning(pointcut = "execution(* com.thbs.intranet.rest.resource.*Resource.*(..))")
	public void logExiting(JoinPoint joinPoint) {

		Log log = LogFactory.getLog(joinPoint.getTarget().getClass());
		log.info("Request " + request.getRequestURL()
				+ " has been served successfully");
		log.info("Exiting method :- " + joinPoint); //Added By Nibedita
	}

	//Added by Nibedita
	@AfterReturning(pointcut = "execution(* com.thbs.intranet.rest.service.impl.*.*(..))")
	public void logServiceImplExiting(JoinPoint joinPoint) {
		Log log = LogFactory.getLog(joinPoint.getTarget().getClass());
		log.info("Exiting method :- " + joinPoint); 
	}
	@AfterReturning(pointcut = "execution(* com.thbs.intranet.rest.dao.misdatabase.impl.*.*(..))")
	public void logDaoImplExiting(JoinPoint joinPoint) {
		Log log = LogFactory.getLog(joinPoint.getTarget().getClass());
		log.info("Exiting method :- " + joinPoint); 
	}
	//End of addition by Nibedita
	
	@AfterThrowing(pointcut = "execution(* com.thbs.intranet.rest.resource.*Resource.*(..))", throwing = "e")
	public void logException(JoinPoint joinPoint, Throwable e) {

		Log log = LogFactory.getLog(joinPoint.getTarget().getClass());
		if (e instanceof Exception) {
			log.info("Inside  method :- " + joinPoint); //Added By Nibedita
			log.error("Request " + request.getRequestURL()
					+ " could not be served", e);
		}
		else {
			log.fatal(
					"Server encountered a severe problem while serving the request "
							+ request.getRequestURL(), e);
		}
	}*/
}
