/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  BusinessException.java                                		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  Jun 19, 2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* Jun 19, 2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.framework.exception;

/**
 * <Description BusinessException:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
public class BusinessException extends Exception
{

	/**
	 * Variable <code>serialVersionUID</code> holds <code>long</code>
	 * value.
	 */
	private static final long serialVersionUID = -5779430770431040399L;

	/**
	 * 
	 * <Description BusinessException:> TODO
	 * 
	 * @param message
	 */

	public BusinessException(String message)
	{
		super(message);
	}

	/**
	 * 
	 * <Description BusinessException:> TODO
	 * 
	 * @param message
	 * @param cause
	 */
	public BusinessException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * 
	 * <Description BusinessException:> TODO
	 * 
	 * @param cause
	 */
	public BusinessException(Throwable cause)
	{
		super(cause);
	}

}
