/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  UserNotFoundException.java                                		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  Jun 19, 2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* Jun 19, 2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.framework.exception;

/**
 * <Description UserNotFoundException:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
public class UserNotFoundException extends Exception
{

	/**
	 * Variable <code>serialVersionUID</code> holds <code>long</code>
	 * value.
	 */
	private static final long serialVersionUID = -7794053420099250512L;

	/**
	 * 
	 * <Description ConfigException:> TODO
	 * 
	 * @param message
	 */
	public UserNotFoundException(String message)
	{
		super(message);
	}

	/**
	 * 
	 * <Description ConfigException:> TODO
	 * 
	 * @param message
	 * @param cause
	 */
	public UserNotFoundException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * 
	 * <Description ConfigException:> TODO
	 * 
	 * @param cause
	 */
	public UserNotFoundException(Throwable cause)
	{
		super(cause);
	}

}
