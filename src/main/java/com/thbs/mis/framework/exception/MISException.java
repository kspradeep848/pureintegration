/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    : MISException.java                                  */
/*                                                                   */
/*  Author      :  (THBS)                                            */
/*                                                                   */
/*  Description : This class inherits from java.lang.Exception .	 */
/*					All other exception classes in mis should  		 */
/* 					inherit from MISException						 */
/*					 				 								 */
/*                                                                   */
/*********************************************************************/

package com.thbs.mis.framework.exception;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Hashtable;

import javax.naming.BinaryRefAddr;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.Reference;

public class MISException extends Exception
{
   
	/**
	 * The error category for all the 
	 * message key passed to this class.
	 */
	private static final  String ERROR_CATEGORY="MISException";

	/**
	 * Constant to represent the MIS error table. 
	 */
	private static final String MIS_ERROR_TABLE="MIS_ERROR_TABLE";

	/**
     * Root cause of this exception
     */
   	private Throwable rootCause;
   
   	/**
     * Key be used to get the error message.
     */
   	private String errorKey;
   
   	/**
     * An array of objects used to substitute
     * the place holders in the error message.
     */
   	private Object[] params;
  
   	/**
   	 * Constructs the new exception object.
   	 * @param key
   	 * @param params
   	 * @param cause
   	 */
   	public MISException(String key, Object[] params, Throwable cause) 
   	{
	   	this.errorKey = key;
	   	this.params = params;
	   	this.rootCause = cause;
   	}

   	/**
   	 * Constructs the new exception object.
   	 * @param key
   	 * @param cause
   	 */
   	public MISException(String key, Throwable cause) 
   	{
	   	this.errorKey = key;
	   	this.rootCause = cause;
   	}

   	/**
   	 * Constructs the new exception object.
   	 * @param key
   	 * @param params
   	 */
   	public MISException(String key, Object[] params) 
   	{
   		this.errorKey = key;
   		this.params = params;
   	}

   	/**
   	 * Constructs the new exception object.
   	 * @param key
   	 */
   	public MISException(String key) 
   	{
   		this.errorKey = key;
   	}
   	/**
   	 * Default constructor
   	 */
   	public MISException()	 
   	{
    
   	}
   
   	/**
     * Returns the error key set.
     * 
     * @return a string containing error key.
     */
   	public String getErrorKey() 
   	{
   		return this.errorKey;
   	}
   
    /**
     * Returns the error category this exception class. This must be
     * overridden by the derived classes.
     * 
     * @return a string containing the error category
     * 
     */
   	public String getErrorCategory() 
   	{
   		return MISException.ERROR_CATEGORY;
   	}
   
    /**
     * Returns the root cause of the exception
     * 
     * @return a <code>Throwable</code> object containing the
     *          root cause.
     */
   	public Throwable getCause() 
   	{
   		return this.rootCause;
   	}
   
    /**
     * Returns the array of values for 
     * substitution of place holders in the error message.
     * 
     * @return an array of objects containing values for 
     * substitution of place holders in the error message.
     */
	public Object[] getParams() 
	{
		return params;
	}
    /**====================================
     * Overidden Methods of Throwable....
     * ==================================== 
     */
 
	/** 
	 * inherit javadoc comment
	 */
	public String getMessage() 
	{
		 String msg="";
	        
	        msg+=this.getMessageFromErrorTable();
	        
	        if(this.rootCause != null)
	        {
	            msg +=" [ Root Cause: "+this.rootCause.getMessage()
	                    +" ]";
	        }
	        
	        return msg;
	}
	
	

	private String getMessageFromErrorTable() 
	{
		Hashtable errorTable;
		String description="";
		try 
		{
			InitialContext context=new InitialContext();
			Reference reference=(Reference)context.lookup(MISException.MIS_ERROR_TABLE);
			BinaryRefAddr refAddr=(BinaryRefAddr) reference.get("errorWrapper");
			byte[] bytes=(byte[]) refAddr.getContent();
			ByteArrayInputStream inputStream=new ByteArrayInputStream(bytes);
			ObjectInputStream objIn=new ObjectInputStream(inputStream);
			ErrorWrapper errorWrapper=(ErrorWrapper)objIn.readObject();
			
			errorTable=errorWrapper.getErrorTable();
			description=(String) errorTable.get(this.errorKey);
		} 
		catch (NamingException e) 
		{
			return e.toString();
		} 
		catch (IOException e) {
			return e.toString();
		} 
		catch (ClassNotFoundException e) 
		{
			return e.toString();
		}
		return description;
	}

	/** 
	 * inherit javadoc comment
	 */
    public void printStackTrace()
    {
        synchronized (System.err)
        {
            if(this.rootCause != null)
            {
                this.rootCause.printStackTrace();
            }
            
            System.err.println();
            
            super.printStackTrace();
        }
    }

   	
    /**
     * inherit javadoc comment
     */
    public void printStackTrace(PrintStream printStream)
    {
        if(printStream == null)
        {
            printStream=System.err;
        }
        
        synchronized (printStream)
        {
            if(this.rootCause != null)
            {
                this.rootCause.printStackTrace(printStream);
            }

            printStream.println();

            super.printStackTrace(printStream);
                
        }
    }
    
    /** 
     * inherit javadoc comment
     */
//  inherit javadoc comment
    public void printStackTrace(PrintWriter printWriter)
    {
        if(printWriter == null)
        {
            printWriter=new PrintWriter(System.err,true);
        }
        
        synchronized (printWriter)
        {
            if(this.rootCause != null)
            {
                this.rootCause.printStackTrace(printWriter);
            }

            printWriter.println();

            super.printStackTrace(printWriter);
        }
    }
    
    /** 
     * inherit javadoc comment
     */
    public String toString()
    {
        String msg=""+this.getClass().getName();
        
        msg+=": "+this.getMessageFromErrorTable();

        if(this.rootCause != null)
        {
            msg +=" [ Root Cause: "+this.rootCause
                    +" ]";
        }

        return msg;
    }
}
