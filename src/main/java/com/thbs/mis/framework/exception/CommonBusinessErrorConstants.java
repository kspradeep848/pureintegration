/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  CommonBusinessErrorConstants.java                 */
/*                                                                   */
/*  Author      :   (THBS)                                           */
/*                                                                   */
/*  Description : Holds all error codes that are relevant to         */
/*                common business usecase exceptions.                */
/*                                                                   */
/*                                                                   */
/*********************************************************************/

package com.thbs.mis.framework.exception;


/**
Holds common business error constants
 */
public interface CommonBusinessErrorConstants
{
	/**
	 * Constant to represent MIS Session expired.
	 */
	String SESSION_EXPIRED = "SESSION_EXPIRED";

	/**
	 * Constant to represent unauthorized access to MIS.
	 */
	String UNAUTHORIZED_ACCESS = "UNAUTHORIZED_ACCESS";

	/**
	 * Constant to represent invalid employee id
	 */
	String INVALID_EMPLOYEE_ID = "INVALID_EMPLOYEE_ID";

        /**
	 * Constant to represent input user bo is null.
	 */
        String USERBO_IS_NULL="USERBO_IS_NULL";

        /**
	 * Constant to represent emp project bo is null.
	 */
        String EMPPROJ_IS_NULL="EMPPROJ_IS_NULL";

        /**
	 * Constant to represent event bo is null.
	 */
        String EVENTBO_IS_NULL="EVENTBO_IS_NULL";

        /**
	 * Constant to represent users could not be loaded for project.
	 */
        String USERS_NOT_LOADED_FOR_PROJECT="USERS_NOT_LOADED_FOR_PROJECT";

         /**
          * Constant to represent retrieve all competency managers failed
          */
        String RETRIEVE_ALL_COMPETENCY_MGRS_FAILED=
                "RETRIEVE_ALL_COMPETENCY_MGRS_FAILED";

         /**
          * Constant to represent retrieve all account managers failed
          */
        String RETRIEVE_ALL_ACCOUNT_MGRS_FAILED=
                "RETRIEVE_ALL_ACCOUNT_MGRS_FAILED";

        /**
          * Constant to represent retrieve all account managers failed
          */
        String RETRIEVE_ALL_DELIVERY_MGRS_FAILED=
                "RETRIEVE_ALL_DELIVERY_MGRS_FAILED";
        /**
         * Constant to represent retrieve all roles failed
         */
        String RETRIEVE_ALL_ROLES_FAILED = "RETRIEVE_ALL_ROLES_FAILED";

        /**
         * Constant to represent retrieve all Locations failed
         */
        String RMS_RETRIEVE_ALL_LOCATIONS_FAILED =
                "RMS_RETRIEVE_ALL_LOCATIONS_FAILED";

        /**
         * Constant to represent retrieve all users failed
         */
        String RETRIEVE_ALL_USERS_FAILED =
                "RETRIEVE_ALL_USERS_FAILED";

        /**
         *Constant to represent emp project history bo's is null.
         */
        String EMPPROJHISTORY_IS_NULL =
                "EMPPROJHISTORY_IS_NULL";

        /**
         * Constant to represent retrieve employee info in project failed
         */
        String COULDNOT_RETRIEVE_EMPDETAILS_IN_PROJECT =
                "COULDNOT_RETRIEVE_EMPDETAILS_IN_PROJECT";

		/**
		 * Constant to represent reset password request was a failure due to invalid email
		 */
	String RESET_PASSWORD_FAILED_DUE_TO_INVALID_EMAIL = "RESET_PASSWORD_FAILED_DUE_TO_INVALID_EMAIL";

		/**
		 * Constant to represent an invalid project id
		 */
		String INVALID_PROJECT_ID = "INVALID_PROJECT_ID";

		/**
		 * Constant to represent resources graph creation was a failure.
		 */
		String COULD_NOT_CREATE_RESOURCES_GRAPH = "COULD_NOT_CREATE_RESOURCES_GRAPH";

	/**
	 * Constant to represent input client manager bo is null.
	 */
	String CLIENT_MANAGER_BO_IS_NULL = "CLIENT_MANAGER_BO_IS_NULL";

/*
         *Constant to represent input param is null.
         */
        String INPUTPARAM_IS_NULL = "INPUTPARAM_IS_NULL";

		String ALL_USERS_LIST_NULL = "ALL_USERS_LIST_NULL";

		String FILE_UPLOAD_FAILURE = "FILE_UPLOAD_FAILURE";

		String CORRUPT_CONFIGURATION = "MIS_CORRUPT_CONFIGURATION";

		String COULD_NOT_INITIALIZE="COULD_NOT_INITIALIZE";

		String NAVIGATION_FAILED = "COULD_NOT_INITIALIZE";

		String COULD_NOT_LOCATE_NAVIGATION_TABLE="COULD_NOT_LOCATE_NAVIGATION_TABLE";

		String COULD_NOT_READ_CONFIGURATION_FILE="COULD_NOT_READ_CONFIGURATION_FILE";

		String COULD_NOT_PARSE_REQUEST = "COULD_NOT_PARSE_REQUEST";

		String COULD_NOT_INITIALIZE_HIBERNATE_SESSIONFACTORY ="COULD_NOT_INITIALIZE_HIBERNATE_SESSIONFACTORY";

		String COULD_NOT_RETRIEVE_HIBERNATESESSION = "COULD_NOT_RETRIEVE_HIBERNATESESSION";

		String COULD_NOT_LOCATE_MIS_HIBERNATEUTIL="COULD_NOT_LOCATE_MIS_HIBERNATEUTIL";

		String COULDNOT_BEGIN_HIBERNATE_TX="COULDNOT_BEGIN_HIBERNATE_TX";

		String INVALID_CLIENT_REQUEST = "INVALID_CLIENT_REQUEST";

		String COULD_NOT_INITIALIZE_ENCRYPT_UTIL = "COULD_NOT_INITIALIZE_ENCRYPT_UTIL";

		String COULD_NOT_ENCRYPT_MESSAGE ="COULD_NOT_ENCRYPT_MESSAGE";

		String COULD_NOT_DECRYPT_MESSAGE ="COULD_NOT_ENCRYPT_MESSAGE";

		String COULD_NOT_SEND_MAIL	="COULD_NOT_SEND_MAIL";

		String ERROR_WHILE_STOPPING_APP = "ERROR_WHILE_STOPPING_APP";

		String USERS_TABLE_NULL = "USERS_TABLE_NULL";

		String COULD_NOT_INITIALIZE_MAILSERVER = "COULD_NOT_INITIALIZE_MAILSERVER";

		String RETRIEVE_ALL_USERS_UNDER_BU_FAILED = "RETRIEVE_ALL_USERS_UNDER_BU_FAILED";

		String RMS_COULDNOT_DELETE_BENCHDATA = "RMS_COULDNOT_DELETE_BENCHDATA";

		
		String INVALID_BU_ID = "INVALID_BU_ID";

		String INVALID_DATE = "INVALID_DATE";

		String INVALID_SUBMIT_INDEX = "INVALID_SUBMIT_INDEX";

		String INVALID_NAVIGATION = "INVALID_NAVIGATION";

		String INVALID_WO_ID = "INVALID_WO_ID";

		String INVALID_INVOICE_STATUS_BO = "INVALID_INVOICE_STATUS_BO";

		String INVALID_DATE_OPTION = "INVALID_DATE_OPTION";
		

		String RETRIEVE_ALL_ACTIVE_USERS_FAILED = "RETRIEVE_ALL_ACTIVE_USERS_FAILED";

		String RETRIEVE_ALL_BU_ACTIVE_USERS_FAILED = "RETRIEVE_ALL_BU_ACTIVE_USERS_FAILED";
		
		String RMS_RETRIEVE_ALL_BILLING_STATUS_FAILED = "RMS_RETRIEVE_ALL_BILLING_STATUS_FAILED";
		
		String PROBATION_BO_IS_NULL= "PROBATION_BO_IS_NULL";
}
