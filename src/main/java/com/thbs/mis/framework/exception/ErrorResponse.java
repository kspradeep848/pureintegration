/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  ErrorResponse.java                                		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  Jun 19, 2016                                           */
/*                                                                   */
/*  Description :  This class is used as a model to send the error response,
 * when there is any exception caught in the application			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* Jun 19, 2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.framework.exception;

import java.util.List;

/**
 * <Description ErrorResponse:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
public class ErrorResponse
{

	private int errorCode;
	private String message;
	private String messageKey;

	private List<Object> errorMessages;
	
	public int getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(int errorCode)
	{
		this.errorCode = errorCode;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}
	
	


	

	public String getMessageKey()
	{
		return messageKey;
	}

	public void setMessageKey(String messageKey)
	{
		this.messageKey = messageKey;
	}

	public List<Object> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<Object> errorMessages) {
		this.errorMessages = errorMessages;
	}

	@Override
	public String toString()
	{
		return "ErrorResponse [errorCode=" + errorCode + ", message="
				+ message + ", messageKey=" + messageKey
				+ ", errorMessages=" + errorMessages + "]";
	}

	

}
