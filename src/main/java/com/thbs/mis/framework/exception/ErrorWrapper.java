/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    : ErrorWrapper.java                                  */
/*                                                                   */
/*  Author      : (THBS)                                             */
/*                                                                   */
/*  Description : This is a wrapper class for the errorTable         */
/*                and implements Referenceable and serializable.     */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      PRF         Comments             */
/*-------------------------------------------------------------------*/
/* 17/10/2005  THBS     1.0       25659    Initial version created   */
/*********************************************************************/
package com.thbs.mis.framework.exception;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Hashtable;

import javax.naming.BinaryRefAddr;
import javax.naming.NamingException;
import javax.naming.Reference;
import javax.naming.Referenceable;

/**
 * This class is a wrapper for the errorTable and implements Referenceable and Serializable.
 * An object of this class is boud to JNDI during MIS Initialization.
 * 
 * @author hashin p
 *
 * @version 1.0, 17 Oct 2005
 */

public class ErrorWrapper implements Referenceable,Serializable 
{
	/**
	 * Holds the errorTable of MIS.
	 */
	private Hashtable errorTable;
	
	/**
	 * Default Constructor.
	 */
	public ErrorWrapper() 
	{
		
	}
	
	/* 
	 * @see javax.naming.Referenceable#getReference()
	 */
	public Reference getReference() throws NamingException 
	{
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		byte[] bytes;
		try 
		{
			ObjectOutputStream objOut=new ObjectOutputStream(out);
			//Outputs the ErrorWrapper class as a byte stream to the ByteArratyOutputStream out.
			objOut.writeObject(this);
			//gets the byte array from the ByteArrayOutputStream out.
			bytes=out.toByteArray();
		} 
		catch (IOException e) 
		{
			throw new NamingException(e.toString());
		}
		return new Reference(ErrorWrapper.class.toString(),new BinaryRefAddr("errorWrapper",bytes));
	}

	/**
	 * Returns the error table
	 * 
	 * @return errorTable a <code>Hashtable</code> which holds the MIS errors.
	 */
	public  Hashtable getErrorTable() 
	{
		return this.errorTable;
	}

	/**
	 * Sets the error table
	 * @param errorTable a <code>Hashtable</code> .
	 */
	public void setErrorTable(Hashtable errorTable) 
	{
		this.errorTable = errorTable;
	}

}
