package com.thbs.mis.framework.exception;

public class GoalNotFoundException  extends Exception
{

	/**
	 * Variable <code>serialVersionUID</code> holds <code>long</code>
	 * value.
	 */
	private static final long serialVersionUID = -7794053420099250512L;
	
	private String errorCode="";
	
	
	
	/**
	 * 
	 * <Description getErrorCode:> TODO
	 * @return
	 */
	public String getErrorCode(){
		return this.errorCode;
	}

	/**
	 * 
	 * <Description CommonCustomException:> TODO
	 * @param message
	 * @param errorCode
	 */
	public GoalNotFoundException(String message, String errorCode){
		super(message);
		this.errorCode=errorCode;
	}
	
	/**
	 * 
	 * <Description CommonCustomException:> TODO
	 * @param message
	 * @param errorCode
	 * @param cause
	 */
	public GoalNotFoundException(String message, String errorCode,Throwable cause){
		super(message, cause);
		this.errorCode=errorCode;
	}
	
	
	
	
	/**
	 * 
	 * <Description ConfigException:> TODO
	 * 
	 * @param message
	 */
	public GoalNotFoundException(String message)
	{
		super(message);
	}

	/**
	 * 
	 * <Description ConfigException:> TODO
	 * 
	 * @param message
	 * @param cause
	 */
	public GoalNotFoundException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * 
	 * <Description ConfigException:> TODO
	 * 
	 * @param cause
	 */
	public GoalNotFoundException(Throwable cause)
	{
		super(cause);
	}

}
