/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  DataAccessException.java                                		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  Jun 19, 2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* Jun 19, 2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.framework.exception;

/**
 * <Description DataAccessException:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
public class DataAccessException extends Exception
{

	/**
	 * Variable <code>serialVersionUID</code> holds <code>long</code>
	 * value.
	 */
	private static final long serialVersionUID = -1418395168356963139L;

	/**
	 * 
	 * <Description DataAccessException:> TODO
	 * 
	 * @param message
	 */
	public DataAccessException(String message)
	{
		super(message);
	}

	/**
	 * 
	 * <Description DataAccessException:> TODO
	 * 
	 * @param message
	 * @param cause
	 */
	public DataAccessException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * 
	 * <Description DataAccessException:> TODO
	 * 
	 * @param cause
	 */
	public DataAccessException(Throwable cause)
	{
		super(cause);
	}

}
