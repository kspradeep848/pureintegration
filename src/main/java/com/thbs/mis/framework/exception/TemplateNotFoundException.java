package com.thbs.mis.framework.exception;




/**
 * 
 * @author sougata
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class TemplateNotFoundException extends ConfigException {

    /**
     * @param message
     * @param t
     */
    public TemplateNotFoundException(String message, Throwable t) {
        super(message, t);
    }

    /**
     * @param message
     */
    public TemplateNotFoundException(String message) {
        super(message);
    }

    /**
     * @param t
     */
    public TemplateNotFoundException(Throwable t) {
        super(t);
    }
}