/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  ConfigException.java                                		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  Jun 19, 2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* Jun 19, 2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.framework.exception;

/**
 * <Description ConfigException:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
public class ConfigException extends Exception
{

	/**
	 * Variable <code>serialVersionUID</code> holds <code>long</code>
	 * value.
	 */
	private static final long serialVersionUID = 7926082711007121701L;

	/**
	 * 
	 * <Description ConfigException:> TODO
	 * 
	 * @param message
	 */
	public ConfigException(String message)
	{
		super(message);
	}

	/**
	 * 
	 * <Description ConfigException:> TODO
	 * 
	 * @param message
	 * @param cause
	 */
	public ConfigException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * 
	 * <Description ConfigException:> TODO
	 * 
	 * @param cause
	 */
	public ConfigException(Throwable cause)
	{
		super(cause);
	}

}
