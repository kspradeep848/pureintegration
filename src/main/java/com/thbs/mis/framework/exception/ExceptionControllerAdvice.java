/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  ExceptionControllerAdvice.java                                		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  Jun 19, 2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* Jun 19, 2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.framework.exception;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

/**
 * <Description ExceptionControllerAdvice:> This class contains Exception
 * Handlers methods for the application.
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
@ControllerAdvice
public class ExceptionControllerAdvice {
	private AppLog log = LogFactory.getLog(ExceptionControllerAdvice.class);

	private static final String genericErrorMessage = "Error occured. Please contact your administrator";

	@ExceptionHandler(Exception.class)
	public ResponseEntity<MISResponse> exceptionHandler(Exception ex) {

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						MISConstants.FAILURE, genericErrorMessage));

	}

	@ExceptionHandler(FileNotFoundException.class)
	public ResponseEntity<MISResponse> fileNotFoundExceptionHandler(Exception ex) {

		return ResponseEntity
				.status(HttpStatus.NOT_FOUND.value())
				.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
						MISConstants.FAILURE,
						"Resource not found ... Please contact your administrator"));

	}

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<MISResponse> userNotFoundExceptionHandler(Exception ex) {

		return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED.value())
				.body(new MISResponse(HttpStatus.PRECONDITION_FAILED.value(),
						MISConstants.FAILURE, ex.getMessage()));

	}

	@ExceptionHandler(DataAccessException.class)
	public ResponseEntity<MISResponse> dataAccessExceptionHandler(Exception ex) {
		String exceptionMessage = ex.getMessage();

		if (ex.getMessage() == null || ex.getMessage().isEmpty()) {
			exceptionMessage = genericErrorMessage;
		}

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						MISConstants.FAILURE, exceptionMessage));
	}

	@ExceptionHandler(BusinessException.class)
	public ResponseEntity<MISResponse> businessExceptionHandler(Exception ex) {

		String exceptionMessage = ex.getMessage();

		if (ex.getMessage() == null || ex.getMessage().isEmpty()) {
			exceptionMessage = genericErrorMessage;
		}

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.body(new MISResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						MISConstants.FAILURE, exceptionMessage));

	}

	@ExceptionHandler(ConfigException.class)
	public ResponseEntity<MISResponse> configExceptionHandler(Exception ex) {

		return ResponseEntity
				.status(HttpStatus.FAILED_DEPENDENCY.value())
				.body(new MISResponse(HttpStatus.FAILED_DEPENDENCY.value(),
						MISConstants.FAILURE,
						"Configuration error ... Please contact your administrator"));

	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<MISResponse> validationExceptionHandler(
			MethodArgumentNotValidException ex) {

		BindingResult result = ex.getBindingResult();

		List<Object> list = new ArrayList<Object>();

		List<FieldError> errList = result.getFieldErrors();

		list.addAll(errList);

		return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
				new MISResponse(HttpStatus.BAD_REQUEST.value(),
						MISConstants.FAILURE,
						"Error occured. Please contact your administrator",
						list));

	}

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<MISResponse> validationExceptionHandler(
			ConstraintViolationException ex) {
		List<Object> list = new ArrayList<Object>();
		for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
			String key = "";
			Object rejectedValue = null;
			if (violation.getPropertyPath() != null) {
				key = violation.getPropertyPath().toString();
				System.out.println("key:"+key);
				rejectedValue = violation.getInvalidValue();
				//System.out.println("rejectedValue:"+rejectedValue.toString());
			}
			FieldError error = new FieldError(key, key, rejectedValue, false,
					null, null, "" + " " + violation.getMessage());
			list.add(error);
		}
		return ResponseEntity
				.status(HttpStatus.BAD_REQUEST.value())
				.body(new MISResponse(
						HttpStatus.BAD_REQUEST.value(),
						MISConstants.FAILURE,
						"Validation Error. Please check the inputs provided to the service.",
						list));
	}

	@ExceptionHandler(TemplateNotFoundException.class)
	public ResponseEntity<MISResponse> templateNotFoundExceptionHandler(
			Exception ex) {

		return ResponseEntity
				.status(HttpStatus.NOT_FOUND.value())
				.body(new MISResponse(HttpStatus.NOT_FOUND.value(),
						MISConstants.FAILURE,
						"Template not found ... Please contact your administrator"));

	}

	@ExceptionHandler(CommonCustomException.class)
	public ResponseEntity<MISResponse> CommonCustomExistsExceptionHandler(
			CommonCustomException ex) {

		return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED.value())
				.body(new MISResponse(HttpStatus.PRECONDITION_FAILED.value(),
						MISConstants.FAILURE, ex.getMessage()));

	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<MISResponse> httpMessageNotReadableExceptionHandler(
			HttpMessageNotReadableException ex) {

		return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
				new MISResponse(HttpStatus.BAD_REQUEST.value(),
						MISConstants.FAILURE, ex.getMessage()));

	}

	@ExceptionHandler(GoalNotFoundException.class)
	public ResponseEntity<MISResponse> GoalNotFoundExceptionHandler(
			GoalNotFoundException ex) {

		return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
				new MISResponse(HttpStatus.NOT_FOUND.value(),
						MISConstants.FAILURE, ex.getMessage()));
	}
}
