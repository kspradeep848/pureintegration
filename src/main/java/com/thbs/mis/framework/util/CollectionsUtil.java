/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  CollectionsUtil.java                                		 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  20-Jun-2016                                           */
/*                                                                   */
/*  Description :  TODO			 									 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 20-Jun-2016    THBS     1.0         Initial version created   		 */
/*********************************************************************/
package com.thbs.mis.framework.util;

import java.util.ArrayList;
import java.util.List;

/**
 * <Description CollectionsUtil:> TODO
 * 
 * @author THBS
 * @version 1.0
 * @see
 */
public class CollectionsUtil
{

	/**
	 * <Description CollectionsUtil:> TODO
	 */
	public CollectionsUtil()
	{
		super();
	}

	
	/**
	 * 
	 * <Description getIterableAsList:> TODO
	 * @param itrData
	 * @return
	 */
	public static <T> List<T> getIterableAsList(Iterable<T> itrData)
	{

		List<T> dataList = null;
		if (itrData != null)
		{

			dataList = new ArrayList<T>();
			itrData.forEach(dataList::add);
		}

		return dataList;

	}
}
