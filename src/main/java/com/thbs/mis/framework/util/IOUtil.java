/*********************************************************************/
/*                           FILE HEADER                             */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  IOUtil.java           		             */
/*                                                                   */
/*  Author      :  Pradeep K S	                                     */
/*                                                                   */
/*  Date        :  16-Mar-2017                                       */
/*                                                                   */
/*  Description :  IOUtil Class for New MIS Application  */
/*                 			                                         */
/*                                                                   */
/*********************************************************************/
/* Date            Who     Version      Comments             	     */
/*-------------------------------------------------------------------*/
/* 16-Mar-2017    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.framework.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;

import com.thbs.mis.framework.exception.ConfigException;



/**
 * 
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public final class IOUtil {

	public static File getFile(String filePath) throws FileNotFoundException {
		
		if (StringUtil.isEmpty(filePath))
			throw new FileNotFoundException("Invalid file name " + filePath);

		File f = new File(filePath);
		
		if (!f.exists() || !f.canRead())
			throw new java.io.FileNotFoundException(
					"File does not exist || can't read the  file " + filePath);

		return f;
	}

	public static FileInputStream getFileInputStream(String filePath)
			throws java.io.FileNotFoundException {

		return new FileInputStream(getFile(filePath));
	}

	public static void close(InputStream ins) {
	
		if (ins == null)
			return;
		
		try {

			ins.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param o
	 */
	public static void close(OutputStream o) {
		
		if (o == null)
			return;

		try {
		
			o.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param o
	 */
	public static void close(Writer writer) {
		if (writer == null)
			return;

		try {
		
			writer.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void close(Reader reader) {
		
		if (reader == null)
			return;

		try {
		
			reader.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param formatInfoFileName
	 * @return
	 */
	public static File getNewFile(String filePath) throws FileNotFoundException {

		if (StringUtil.isEmpty(filePath))
			throw new FileNotFoundException("Invalid file name " + filePath);
		
		File f = new File(filePath);
		
		if (!f.exists()) {
			try {
			
				f.createNewFile();
		
			} catch (IOException e) {
				throw new java.io.FileNotFoundException(
						"File cannot be created " + filePath);
			}
		}
		if (!f.canRead()) {
			throw new java.io.FileNotFoundException(
					"File does not exist || can't read the  file " + filePath);
		}
		return f;
	}

	/**
	 * Method to pipe the reader contents to writer
	 * 
	 * @param reader
	 * @param out
	 */
	public static void pipe(Reader reader, Writer out) throws IOException {
		
		BufferedReader in = new BufferedReader(reader, 8192);
		char[] result = new char[8192];
		int noofbyte;
		
		while ((noofbyte = in.read(result, 0, 8192)) != -1) {
			out.write(result, 0, noofbyte);
		}
		in.close();
	}

	public static void pipe(InputStream is, OutputStream os) throws IOException {
		
		BufferedInputStream in = new BufferedInputStream(is, 8192);
		byte[] result = new byte[8192];
		int noofbyte;
		
		while ((noofbyte = in.read(result, 0, 8192)) != -1) {
			os.write(result, 0, noofbyte);
		}
		in.close();
	}
	
	/**
     * For a given base directory and the file name, this method tries to
     * retrieve the <code>File</code> instance.
     * 
     * @param basePath
     *            the base directory to consider, can be <code>null</code>
     * @param fileName
     *            the name of the file to be loaded
     * @return the <code>File</code> representing the <code>basePath</code>
     *         and <code>fileName</code>
     */
    public static File getFile(String basePath, String fileName)
    {

        File file = null;
        if (basePath == null)
        {
            file = new File(fileName);
        }

        else
        {
            StringBuffer fName = new StringBuffer();
            fName.append(basePath);

            if (!basePath.endsWith(File.separator))
            {
                fName.append(File.separator);
            }
            if (fileName.startsWith("." + File.separator))
            {
                fName.append(fileName.substring(2));
            }
            else
            {
                fName.append(fileName);
            }

            file = new File(fName.toString());

        }

        return file;
    }
    
	/**
     * A generic URL locator which can find and return location of the specified
     * resource by searching the absolute path, the current classpath and the
     * system classpath.
     * 
     * @param fileName
     *            the name of the resource
     * @return the location of the resource
     * @throws CIOConfigException
     *             if it cannot locate a specified resource
     * @see #locate(String, String)
     */
    public static URL locate(String fileName) throws ConfigException
    {
        return locate(null, fileName);
    }
    
    /**
     * A generic URL locator which can find and return location of the specified
     * resource by searching the absolute path, the current classpath and the
     * system classpath.
     * 
     * @param basePath
     *            The basePath to search for a file
     * @param fileName
     *            the name of the resource
     * @return the location of the resource
     * @throws CIOConfigException
     *             if locator cannot find the specified resource
     */
    public static URL locate(String basePath, String fileName)
        throws ConfigException
    {

        // returns null if fileName is undefined
        if (fileName == null)
        {
            return null;
        }

        URL url = null;

        // attempt to create an URL directly, on exception reset to null and try
        // other methods
        try
        {
            if (basePath == null)
            {
                url = new URL(fileName);
            }
            else
            {
                URL baseURL = new URL(basePath);
                url = new URL(baseURL, fileName);

                // check if the file exists
                InputStream in = null;
                try
                {
                    in = url.openStream();
                }
                finally
                {
                    if (in != null)
                    {
                        in.close();
                    }
                }
            }
        }
        catch (IOException e)
        {
            url = null;
        }

        // attempt to load from an absolute path
        if (url == null)
        {
            File tempFile = new File(fileName);
            if (tempFile.isAbsolute() && tempFile.exists())
            {
                try
                {
                    url = tempFile.toURL();

                }
                catch (MalformedURLException e)
                {
                }
            }
        }

        // attempt to load from the base directory
        if (url == null)
        {
            try
            {
                File tfile = getFile(basePath, fileName);
                if (tfile != null && tfile.exists())
                {
                    url = tfile.toURL();
                }

                if (url != null)
                {
                    
                }
            }
            catch (IOException e)
            {
            }
        }

        // attempt to load from the context classpath
        if (url == null)
        {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            url = loader.getResource(fileName);

            if (url != null)
            {
               
            }
        }

        // attempt to load from the system classpath
        if (url == null)
        {
            url = ClassLoader.getSystemResource(fileName);

            if (url != null)
            {
                
            }
        }

        // if still null, throw config exception as resource cannot be located
        if (url == null)
        {
            throw new ConfigException("Cannot locate file :" + fileName);
        }

        return url;
    }
}