package com.thbs.mis.framework.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CustomDateSerializer  extends com.fasterxml.jackson.databind.JsonSerializer<Date> {    
    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider arg2) throws 
        IOException, JsonProcessingException {      

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = formatter.format(value);

        gen.writeString(formattedDate);

    }
}
/*How to use;
 * 
 * need to use  @JsonSerialize(using = CustomDateSerializer.class)
 * before the getter method of that date field
 * 
 * eg:@JsonSerialize(using = CustomDateSerializer.class)
 *	  public Date CreatedDate() {
 * 	  return CreatedDate;
 *	  }
 */
