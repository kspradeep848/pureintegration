/**
 * 
 */
package com.thbs.mis.framework.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

/**
 * @author THBS
 *
 */
public class ValidationUtils {

	// As per new changes changed error response to MIS response by Mani on
	// 12-12-2016
	private static final AppLog LOG = LogFactory.getLog(ValidationUtils.class);

	public static ResponseEntity<MISResponse> handleValidationErrors(
			BindingResult bindingRes) {

		LOG.startUsecase("Entered handleValidationErrors Method in Class of ValidationUtils.");

		List<Object> list = new ArrayList<Object>();
		List<FieldError> errList = bindingRes.getFieldErrors();
		list.addAll(errList);
		MISResponse responseBody = new MISResponse();

		responseBody.setResult(errList);
		responseBody.setStatus(MISConstants.FAILURE);
		responseBody.setMessage("Validation Error");
		responseBody.setStatusCode(HttpStatus.PRECONDITION_FAILED.value());

		LOG.endUsecase("Exited handleValidationErrors Method in Class of ValidationUtils.");

		return new ResponseEntity<MISResponse>(responseBody, HttpStatus.OK);
	}

}
