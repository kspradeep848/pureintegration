/**********************************************************************/
/*                  FILE HEADER                                       */
/**********************************************************************/
/*                                                                    */
/*  FileName    :  EncryptUtil.java                                   */
/*                                                                    */
/*  Author      :   (THBS)                                            */
/*                                                                    */
/*  Description :  This class  provides functionality for encrypting  */
/*                 and decrypting messages using Advanced Encryption  */
/*                 Standard(AES) algorithm.                           */
/**********************************************************************/
/* Date        Who    Version       PRF             Comments          */
/*--------------------------------------------------------------------*/
/* 16/11/2005  THBS      1.0                  Initial version created */
/**********************************************************************/

package com.thbs.mis.framework.util;

import java.io.UnsupportedEncodingException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.apache.commons.codec.binary.Base64;

import com.thbs.mis.framework.exception.CommonBusinessErrorConstants;
import com.thbs.mis.framework.exception.MISException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

/**
 * This class provides functionality to perform encryption
 * and decryption operations.
 * 
 * @author THBS
 * @version 1.0
 */
public class EncryptUtil {
   
    /*
     * The pass phrase string based on which encrypting and decrypting is
     * done.
     */
    private static final String passPhrase = "THBS !! is a distributed " +
            "computing company with offices @ Residency Road &&& Millers Road "+
            "in India ,Bangalore :: The reception phone number is 51125101.";
        
        
    /*
     *Byte array used as salt (encryption terminology)
     *for use in encryption and decryption.
     */
    private    byte[] salt = {
            (byte)0xA9, (byte)0x9B, (byte)0xC8, (byte)0x32,
            (byte)0x56, (byte)0x35, (byte)0xE3, (byte)0x03
    };
    
	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(EncryptUtil.class);
    
    /*
     *Cipher for encryption purposes.
     */ 
    private Cipher ecipher;
    
    /*
     *Cipher for decryption purposes.
     */ 
    private Cipher dcipher;
    
    /*
     *Iteration count used for encryption and decryption.
     */  
    private final int iterationCount = 19;
    
    public EncryptUtil() {    
    }   
    
     
    /**
     * Encrypts the message  based on phass phrase and iteration count.
     * 
     * @param a <code>String</code> message to be encrypted.
     * @return a <code>String</code> containing encrypted message.      
     * @throws MISException in case of errors.
     */
    
    public String encrypt(String message) throws MISException {
        LOG.entering ( "encrypt"); 
        String encryptedMessage = null;
        this.initialize();
        LOG.debug("Initialized Ciphers");
        try {
            
            // Encode the string into bytes using utf-8
            byte[] utf8 = message.getBytes("UTF8");
            
            // Encrypt
            byte[] enc = ecipher.doFinal(utf8);            
            
            // Encode bytes to base64 to get a string
            // Commented by Praneeth and used Base64 encoder as the sun.* packages
        	// are not part of the supported, public interface. 
            //encryptedMessage = new sun.misc.BASE64Encoder().encode(enc);
            byte[] encodedBytes = Base64.encodeBase64(enc);
            encryptedMessage = new String(encodedBytes);
            LOG.debug("Encrypted Message successfully ");
        } catch (javax.crypto.BadPaddingException e) {
           LOG.debug("Caught exception while encrypting message");
           throw new MISException(
                   CommonBusinessErrorConstants.COULD_NOT_ENCRYPT_MESSAGE,e);
        } catch (IllegalBlockSizeException e) {
           LOG.debug("Caught exception while encrypting message");
           throw new MISException(
                   CommonBusinessErrorConstants.COULD_NOT_ENCRYPT_MESSAGE,e);
        } catch (UnsupportedEncodingException e) {
           LOG.debug("Caught exception while encrypting message");
           throw new MISException(
                   CommonBusinessErrorConstants.COULD_NOT_ENCRYPT_MESSAGE,e);
        } catch (java.io.IOException e) {            
           LOG.debug("Caught exception while encrypting message");
           throw new MISException(
                   CommonBusinessErrorConstants.COULD_NOT_ENCRYPT_MESSAGE,e);
        }        
        
        LOG.exiting("encrypt");
        return encryptedMessage;
    }
    
    /**
     * Decrypts the message based on phass phrase and iteration count.
     * 
     * @param a <code>String</code> message to be decrypted.
     * @return a <code>String</code> containing decrypted message.
     * @throws MISException in case of errors.
     */
    public String decrypt(String encryptedMessage) throws MISException {
        
        LOG.entering ( "decrypt"); 
        String decryptedMessage = null;
        this.initialize();
        LOG.debug("Initialized Ciphers");        
        try {
            
            
            // Decode base64 to get bytes
        	//Commented by Praneeth and used Base64 decoder as the sun.* packages
        	// are not part of the supported, public interface. 
            //byte[] dec = new sun.misc.BASE64Decoder()
             //            .decodeBuffer(encryptedMessage);
        	byte[] dec = Base64.decodeBase64(encryptedMessage);
            
            // Decrypt
            byte[] utf8 = dcipher.doFinal(dec);    
            // Decode using utf-8
            decryptedMessage = new String(utf8, "UTF8");
            LOG.debug("Decrypted Message successfully ");
            
        } catch (javax.crypto.BadPaddingException e) {
             LOG.debug("Caught exception while decrypting message");
             throw new MISException(
                     CommonBusinessErrorConstants.COULD_NOT_DECRYPT_MESSAGE,e);
        } catch (IllegalBlockSizeException e) {
             LOG.debug("Caught exception while decrypting message");
             throw new MISException(
                     CommonBusinessErrorConstants.COULD_NOT_DECRYPT_MESSAGE,e);
        } catch (UnsupportedEncodingException e) {
             LOG.debug("Caught exception while decrypting message");
             throw new MISException(
                     CommonBusinessErrorConstants.COULD_NOT_DECRYPT_MESSAGE,e);
        } catch (java.io.IOException e) {
             LOG.debug("Caught exception while decrypting message");
             throw new MISException(
                     CommonBusinessErrorConstants.COULD_NOT_DECRYPT_MESSAGE,e);
        } 
        LOG.exiting("decrypt");
        return decryptedMessage;
    }
    
    
    private void initialize() throws MISException{
        
        LOG.entering("initialize");
        
        try {
            // Create the key
            KeySpec keySpec = new PBEKeySpec( passPhrase.toCharArray(),
                                              salt, iterationCount);            
            SecretKey key = SecretKeyFactory.getInstance(
                    "PBEWithMD5AndDES").generateSecret(keySpec);
            
            LOG.debug("Initialized secret key");
            
            //Ciphers for encrypting and decrypting.
            ecipher = Cipher.getInstance(key.getAlgorithm());
            dcipher = Cipher.getInstance(key.getAlgorithm());
                                   
            // Prepare the parameter to the ciphers
            AlgorithmParameterSpec paramSpec = new PBEParameterSpec(
                                                   salt, iterationCount);                       
            // Create the ciphers
            ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
            LOG.debug("Created ciphers");
            
        } catch (java.security.InvalidAlgorithmParameterException e) {
           LOG.debug("Caught exception while initialising EncryptUtil");
           throw new MISException(
                   CommonBusinessErrorConstants.COULD_NOT_INITIALIZE_ENCRYPT_UTIL,e);   
        } catch (java.security.spec.InvalidKeySpecException e) {
            LOG.debug("Caught exception while initialising EncryptUtil");
           throw new MISException(
                   CommonBusinessErrorConstants.COULD_NOT_INITIALIZE_ENCRYPT_UTIL,e);
        } catch (javax.crypto.NoSuchPaddingException e) {
            LOG.debug("Caught exception while initialising EncryptUtil");
           throw new MISException(
                   CommonBusinessErrorConstants.COULD_NOT_INITIALIZE_ENCRYPT_UTIL,e);
        } catch (java.security.NoSuchAlgorithmException e) {
            LOG.debug("Caught exception while initialising EncryptUtil");
           throw new MISException(
                   CommonBusinessErrorConstants.COULD_NOT_INITIALIZE_ENCRYPT_UTIL,e);
        } catch (java.security.InvalidKeyException e) {
            LOG.debug("Caught exception while initialising EncryptUtil");
           throw new MISException(
                   CommonBusinessErrorConstants.COULD_NOT_INITIALIZE_ENCRYPT_UTIL,e);
        }
        
        LOG.exiting("initialize");
    }
}
