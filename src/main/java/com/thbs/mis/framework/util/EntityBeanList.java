package com.thbs.mis.framework.util;

import java.util.List;

import javax.validation.Valid;

public class EntityBeanList<E>{
	
	@Valid
	List<E> entityList;

	public List<E> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<E> entityList) {
		this.entityList = entityList;
	}

	
	public  String getObjectName() {
		
		if(this.entityList!=null || !this.entityList.isEmpty()){
			return this.entityList.get(0).getClass().getCanonicalName().toString();
		}
		
	    return null; 
	}
	 
	
	

}
