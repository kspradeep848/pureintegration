package com.thbs.mis.framework.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLUtil {

	public static DocumentBuilderFactory factory;

	private static TransformerFactory tFactory = TransformerFactory
			.newInstance();

	public static Document parse(InputStream in)
			throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.parse(in);
	}

	public static Document parse(InputSource in)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.parse(in);

	}

	public static Document parse(String xmlSource)
			throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource in = new InputSource(new StringReader(xmlSource));
		return builder.parse(in);

	}

	public static Document parse(File xmlFile) throws FileNotFoundException,
			ParserConfigurationException, SAXException, IOException {
		return parse(new FileInputStream(xmlFile));
	}

	public static Document getXMLFromFile(String path)
			throws FileNotFoundException, ParserConfigurationException,
			SAXException, IOException {
		return parse(new File(path));
	}

	public static String getValue(Node node) {
		Node cld = node.getFirstChild();
		if (cld == null)
			return null;
		return cld.getNodeValue();
	}

	public static String getValue(Element e) {
		Node cld = e.getFirstChild();
		if (cld == null)
			return null;
		return cld.getNodeValue();
	}

	private static Element locateElement(NodeList nodes) {
		int len = (nodes == null) ? 0 : nodes.getLength();
		if (len == 0)
			return null;

		for (int i = 0; i < len; i++) {
			Node n = nodes.item(i);
			if (n.getNodeType() == Node.ELEMENT_NODE)
				return (Element) n;

			continue;
		}
		return null;
	}

	public static String getValueByTagName(Document doc, String tagName) {
		return getValueByTagName(doc.getDocumentElement(), tagName);
	}

	public static String getValueByTagName(Element doc, String tagName) {
		NodeList nodes = doc.getElementsByTagName(tagName);
		if (nodes == null || nodes.getLength() <= 0)
			return null;
		return getValue(nodes.item(0));
	}

	public static boolean isEmptyByTagName(Element doc, String tagName) {

		return StringUtil.isEmpty(getValueByTagName(doc, tagName));
	}

	public static String getValueByTagName(Element doc, String tagName,
			String def) {
		NodeList nodes = doc.getElementsByTagName(tagName);
		if (nodes == null || nodes.getLength() <= 0)
			return def;
		String s = getValue(nodes.item(0));
		if (s == null)
			return def;
		return s;
	}

	public static int getIntValueByTagName(Document doc, String tagName) {
		return getIntValueByTagName(doc.getDocumentElement(), tagName);
	}

	public static int getIntValueByTagName(Element doc, String tagName) {
		return TypeUtil.toInt(getValueByTagName(doc, tagName));
	}

	public static int getIntValueByTagName(Element doc, String tagName, int def) {
		return TypeUtil.toInt(getValueByTagName(doc, tagName, Integer
				.toString(def)));
	}

	public static boolean getBooleanValueByTagName(Document doc, String tagName) {
		return getBooleanValueByTagName(doc.getDocumentElement(), tagName);
	}

	public static boolean getBooleanValueByTagName(Element doc, String tagName) {
		return TypeUtil.toBoolean(getValueByTagName(doc, tagName));
	}

	public static boolean getBooleanValueByTagName(Element doc, String tagName,
			boolean def) {
		return TypeUtil.toBoolean(getValueByTagName(doc, tagName, Boolean
				.toString(def)));
	}

	public static double getDoubleValueByTagName(Element doc, String tagName) {
		return TypeUtil.toDouble(getValueByTagName(doc, tagName));
	}

	public static Date getDateByTagName(Element e, String tagName, String frmt) {

		String val = getValueByTagName(e, tagName);

		if (val == null)
			return null;
		SimpleDateFormat dtFrmt = null;

		try {
			dtFrmt = new SimpleDateFormat(frmt);
			return dtFrmt.parse(val);
		} catch (ParseException e1) {

			return null;
		}
	}

	public static String[] getValuesByTagName(Document doc, String tagName) {
		return getValuesByTagName(doc.getDocumentElement(), tagName);
	}

	public static String[] getValuesByTagName(Element doc, String tagName) {
		String[] values = null;

		NodeList list = doc.getElementsByTagName(tagName);
		int count = (list == null) ? 0 : list.getLength();

		if (count < 1)
			return null;

		values = new String[count];

		for (int i = 0; i < count; i++) {
			Node cld = list.item(i).getFirstChild();
			if (cld == null)
				values[i] = "";
			else
				values[i] = cld.getNodeValue();
		}
		return values;
	}

	public static Element getElementByTagName(Element e, String tagName) {
		if (e == null)
			return null;
		NodeList nodes = e.getElementsByTagName(tagName);
		return locateElement(nodes);
	}

	public static Element[] getElementsByTagName(Element e, String tagName) {
		if (e == null)
			return null;
		NodeList nodes = e.getElementsByTagName(tagName);
		return searchNodeList(nodes);
	}

	public static Element getElementByTagName(Document d, String tagName) {
		return getElementByTagName(d.getDocumentElement(), tagName);
	}

	public static Element[] getElementsByTagName(Document d, String tagName) {
		return getElementsByTagName(d.getDocumentElement(), tagName);
	}

	public static Element[] getFirstLevelChildElements(Element e, String tagName) {
		NodeList nodes = e.getChildNodes();
		return searchNodeList(nodes, tagName);
	}

	public static Element[] getFirstLevelChildElements(Element e) {
		NodeList nodes = e.getChildNodes();
		return searchNodeList(nodes);
	}

	public static Element getFirstLevelChildElement(Element e, String tagName) {
		Element tmp[] = getFirstLevelChildElements(e, tagName);
		int len = tmp == null ? 0 : tmp.length;
		if (len > 0)
			return tmp[0];
		return null;
	}

	public static Element[] searchNodeList(NodeList nl, String tagName) {
		int len = (nl == null) ? 0 : nl.getLength();
		if (len == 0)
			return null;

		ArrayList al = new ArrayList();
		for (int i = 0; i < len; i++) {
			Node n = nl.item(i);
			if (n.getNodeType() != Node.ELEMENT_NODE)
				continue;

			if (tagName != null) {
				String tmp = n.getNodeName();
				if (tagName.equals(tmp))
					al.add(n);
			} else
				al.add(n);
		}
		len = al.size();
		if (len == 0)
			return null;

		Element[] ret = new Element[len];
		ret = (Element[]) al.toArray(ret);
		return ret;
	}

	public static Element[] searchNodeList(NodeList nl) {
		int len = (nl == null) ? 0 : nl.getLength();
		if (len == 0)
			return null;

		ArrayList al = new ArrayList();
		for (int i = 0; i < len; i++) {
			Node n = nl.item(i);
			if (n.getNodeType() != Node.ELEMENT_NODE)
				continue;
			al.add(n);
		}
		len = al.size();
		if (len == 0)
			return null;

		Element[] ret = new Element[len];
		ret = (Element[]) al.toArray(ret);
		return ret;
	}

	public static void insertNewTextElement(Element e, String tagName,
			String tagValue) {
		Document d = e.getOwnerDocument();
		insertNewTextElement(d, e, tagName, tagValue);
	}

	public static void insertNewTextElement(Document d, String tagName,
			String tagValue) {
		Element e = d.getDocumentElement();
		insertNewTextElement(d, e, tagName, tagValue);
	}

	public static void insertNewTextElement(Document d, Element e,
			String tagName, String tagValue) {
		e.appendChild(d.createElement(tagName)).appendChild(
				d.createTextNode(tagValue));
	}

	public static Element insertNewTag(Element e, String tagName) {
		Document d = e.getOwnerDocument();
		return insertNewTag(d, e, tagName);
	}

	public static Element insertNewTag(Document d, Element e, String tagName) {
		return (Element) e.appendChild(d.createElement(tagName));
	}

	public static void insertCDATASection(Document doc, String tagName,
			String tagValue) {
		NodeList nodeList = doc.getElementsByTagName(tagName);
		if (nodeList == null)
			return;
		int j = nodeList.getLength();
		Node node = null;
		for (int i = 0; i < j; i++) {
			node = nodeList.item(i);
			node.appendChild(doc.createCDATASection(tagValue));
		}
	}

	public static void insertNewElement(Document d, String parent,
			String tagName, String tagValue) {
		d.getElementsByTagName(parent).item(0).appendChild(
				d.createElement(tagName));
		d.getElementsByTagName(tagName).item(0).appendChild(
				d.createTextNode(tagValue));
	}

	public static void deleteElementByTagName(Document doc, String tagName) {
		deleteElementByTagName(doc.getDocumentElement(), tagName);
	}

	public static void deleteElementByTagName(Element doc, String tagName) {
		NodeList nodeList = doc.getElementsByTagName(tagName);
		int count = (nodeList) == null ? 0 : nodeList.getLength();

		Node node = null;
		Node parentNode = null;

		for (int i = 0; i < count; i++) {
			node = nodeList.item(0);
			if (node == null)
				continue;

			parentNode = node.getParentNode();
			if (parentNode == null)
				continue;

			parentNode.removeChild(node);
		}
	}

	public static void replaceValueByTagName(Document doc, String tagName,
			String tagValue) {
		replaceValueByTagName(doc.getDocumentElement(), tagName, tagValue);
	}

	public static void replaceValueByTagName(Element doc, String tagName,
			String tagValue) {
		Document pDoc = doc.getOwnerDocument();
		NodeList nodeList = doc.getElementsByTagName(tagName);
		int count = (nodeList) == null ? 0 : nodeList.getLength();

		Node node = null;

		for (int i = 0; i < count; i++) {
			Node newNode = pDoc.createTextNode(tagValue);
			node = nodeList.item(i);
			node.replaceChild(newNode, node.getFirstChild());
		}

	}

	public static String[] getAttributes(Element e, String attrName) {
		String tmp = e.getAttribute(attrName);

		String ret = (tmp == null) ? null : tmp.trim();

		if (ret == null || ret.length() <= 0)
			return null;

		return ret.split(ret, ',');
	}

	public static String getAttribute(Element e, String attrName) {
		String tmp = e.getAttribute(attrName);

		String ret = (tmp == null) ? null : tmp.trim();

		if (ret == null || ret.length() <= 0)
			return null;
		else
			return ret;
	}

	public static String getAttributeRecurse(Element e, String attrName) {
		if (e == null)
			return null;

		String tmp = e.getAttribute(attrName);

		String ret = (tmp == null) ? null : tmp.trim();

		if (ret == null || ret.length() <= 0) {
			Node n = e.getParentNode();

			if (n != null && n.getNodeType() == Node.ELEMENT_NODE) {
				return getAttributeRecurse((Element) n, attrName);
			} else {
				return null;
			}
		} else
			return ret;
	}

	public static String getAttribute(Element e, String attrName, String def) {
		return StringUtil.handleDefault(getAttribute(e, attrName), def);
	}

	public static String getAttributeRecurse(Element e, String attrName,
			String def) {
		return StringUtil.handleDefault(getAttributeRecurse(e, attrName), def);
	}

	public static int getIntAttribute(Element e, String attrName) {
		return TypeUtil.toInt(getAttribute(e, attrName));
	}

	public static int getIntAttributeRecurse(Element e, String attrName) {
		return TypeUtil.toInt(getAttributeRecurse(e, attrName));
	}

	public static boolean getBooleanAttribute(Element e, String attrName) {
		return TypeUtil.toBoolean(getAttribute(e, attrName));
	}

	public static boolean getBooleanAttribute(Element e, String attrName,
			boolean def) {
		return TypeUtil
				.toBoolean(getAttribute(e, attrName, String.valueOf(def)));
	}

	public static boolean getBooleanAttributeRecurse(Element e, String attrName) {
		return TypeUtil.toBoolean(getAttributeRecurse(e, attrName));
	}

	public static boolean getBooleanAttributeRecurse(Element e,
			String attrName, boolean def) {
		return TypeUtil.toBoolean(getAttributeRecurse(e, attrName, String
				.valueOf(def)));
	}

	public static boolean isDebugOn(Element e) {
		if (e == null)
			return false;
		return getBooleanAttributeRecurse(e, "debug");
	}

	public static boolean isDebugOn(Element e, boolean def) {
		if (e == null)
			return def;
		return getBooleanAttributeRecurse(e, "debug", def);
	}

	public static void transform(Document doc, Result out) throws Exception {

		try {

			Transformer transformer = tFactory.newTransformer();

			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			transformer.transform(new DOMSource(doc), out);

		} catch (Exception e) {

			throw e;
		}
	}

	public static String transform(Element doc) throws Exception {

		StringWriter writer = new StringWriter();
		Transformer transformer = tFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.transform(new DOMSource(doc), new StreamResult(writer));
		return writer.toString();

	}

	private static void transform(Templates template, Source datasource,
			Result result) throws TransformerException {

		Transformer transformer = template.newTransformer();

		transformer.setParameter("currDate", new SimpleDateFormat()
				.format(new Date()));
		transformer.setErrorListener(new XMLErrorHandler());
		transformer.transform(datasource, result);

	}

	public static Document transform(Templates template, Node document)
			throws Exception {

		Document dom = null;
		DOMSource datasource = new DOMSource(document);
		dom = factory.newDocumentBuilder().newDocument();
		Result result = new DOMResult(dom);
		transform(template, datasource, result);

		return dom;
	}

	public static void transform(Templates template, Node document,
			OutputStream out) throws Exception {

		DOMSource datasource = new DOMSource(document);
		StreamResult result = new StreamResult(out);
		transform(template, datasource, result);
	}

	public static void transform(Templates template, Node document, Writer w)
			throws Exception {

		DOMSource datasource = new DOMSource(document);
		StreamResult result = new StreamResult(w);
		transform(template, datasource, result);
	}

	public static void transform(Templates template, InputStream in,
			OutputStream out) throws Exception {

		StreamResult result = new StreamResult(out);
		StreamSource src = new StreamSource(in);
		transform(template, src, result);
	}

	public static Element importNode(Element impToEl, Element impFrEl) {

		Document doc = impToEl.getOwnerDocument();
		Node dup = doc.importNode(impFrEl, true);
		impToEl = doc.getDocumentElement();
		impToEl.appendChild(dup);
		return impToEl;
	}

	/**
	 * Method to get the nodes in a document having a given attribute set to
	 * specific value for a given tagName
	 * 
	 * @param doc
	 *            document object
	 * @param tagName
	 * @param attributeName
	 * @param attributeValue
	 * @return returs Lis of nodes containing the attributes by name
	 *         attributeName and values equal to the attributeValue and the
	 *         tagName equal to the given name
	 * @throws Exception
	 */
	public static List getElementsByAttributeValue(Document doc,
			String tagName, String attributeName, String attributeValue)
			throws Exception {

		NodeList nodeList = doc.getElementsByTagName(tagName);
		List nodesList = new ArrayList(2);
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			Node attributeNode = node.getAttributes().getNamedItem(
					attributeName);
			if (attributeNode.getNodeType() == Node.ATTRIBUTE_NODE) {
				String value = attributeNode.getNodeValue();
				if (value.equalsIgnoreCase(attributeValue)) {
					nodesList.add(node);
				}
			}
		}

		return nodesList;
	}

	static {
		try {
			factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			factory.setValidating(false);
			// Configure it to ignore comments
			factory.setIgnoringComments(true);

		} catch (Throwable t) {
		}
	}

	public static String escapeElementValue(String data) {
		if (data == null)
			return "";

		int len = data.length();

		if (len <= 0)
			return "";

		data = data.replaceAll("&", "&amp;");
		data = data.replaceAll("<", "&lt;");
		data = data.replaceAll(">", "&gt;");

		return data;
	}
	
	public static NodeList getNodeList(Document doc, String element) {
		Element docEle = doc.getDocumentElement();
		NodeList nl = docEle.getElementsByTagName(element);
		return nl;
	}

}