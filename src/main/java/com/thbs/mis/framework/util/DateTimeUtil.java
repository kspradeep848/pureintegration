/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  DateTimeUtil.java                                 */
/*                                                                   */
/*  Author      :  THBS		                                         */
/*                                                                   */
/*  Date        :  30-Jan-2017                                       */
/*                                                                   */
/*  Description :  This class contains methods for formatting the    */
/*                 date with time 			 						 */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* 30-Jan-2016    THBS     1.0         Initial version created   	 */
/*********************************************************************/
package com.thbs.mis.framework.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

public class DateTimeUtil {

	public static Date dateWithMinTimeofDay(Date date) {
		Date truncateTimeFirstDate = DateUtils.truncate(date, Calendar.DATE);
		return truncateTimeFirstDate;
	}

	public static Date dateWithMaxTimeofDay(Date date) {
		Date truncateTimeFirstDate = DateUtils.truncate(date, Calendar.DATE);

		LocalTime midnight = LocalTime.MAX;
		LocalDate localDate = truncateTimeFirstDate.toInstant()
				.atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDateTime todayMidnight = LocalDateTime.of(localDate, midnight);
		Date endDate = Date.from(todayMidnight.atZone(ZoneId.systemDefault())
				.toInstant());

		return endDate;
	}

	public static Date getStartDateTime(Date startDate) {

		if (startDate != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(startDate);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			startDate = cal.getTime();
		}

		return startDate;
	}

	public static Date getEndDateTime(Date endDate) {

		if (endDate != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(endDate);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			endDate = cal.getTime();
		}

		return endDate;
	}

}
