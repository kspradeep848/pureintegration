package com.thbs.mis.framework.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class TypeUtil {

    private static final String DEF_DATE_FORMAT = "dd-MM-yyyy";
    
    private static final String DEF_DATE_TIME_FORMAT = "dd-MM-yyyy hh:mm:ss";
    
	public static boolean toBoolean(String data) {

		if (StringUtil.isEmpty(data))
			return false;

		if (data.equalsIgnoreCase("true"))
			return true;

		if (data.equalsIgnoreCase("yes"))
			return true;

		if (data.equalsIgnoreCase("enabled"))
			return true;

		if (data.equalsIgnoreCase("Y"))
			return true;

		try {
			int value= Integer.parseInt(data);
			return (value > 0);
		} catch (NumberFormatException ex) { }

		return false;
	}

	public static int toInt( String data ) {
		return toInt( data, -1 );
	}

	public static int toInt( String data, int onerror ) {
		if ( StringUtil.isEmpty( data ) )
			return onerror;

		int ret = onerror;

		try {
			ret= Integer.parseInt(data.trim());
		} catch (NumberFormatException ex) { }

		return ret;
	}

	public static long toLong( String data ) {
		return toLong( data, -1 );
	}
	
	public static long toLong( String data, long onerror ) {
		if ( StringUtil.isEmpty( data ) )
			return onerror;

		long ret = onerror;

		try {
			ret= Long.parseLong(data);
		} catch (NumberFormatException ex) { }

		return ret;
	}

	public static double toDouble( String data ) {
		return toDouble( data, -1 );
	}

	public static double toDouble( String data, double def ) {
		if ( StringUtil.isEmpty( data ) )
			return def;

		double ret = def;
		try {
			ret = Double.parseDouble(data);
		} catch (NumberFormatException ex) { }
		return ret;
	}
	
	public static Integer toInteger( String data, Integer onerror ) {
	    if ( data == null || data.trim().length() == 0 )
			return onerror;

		Integer ret = onerror;

		try {
			ret = new Integer(data);
		} catch (NumberFormatException ex) { }

		return ret;
	}
	
	public static Integer[] toInteger(String data[], Integer onerror[]) {
	    Integer ret[] = onerror;
	    if(data== null || data.length <=0) return onerror;
	     
	    ret = new Integer[data.length];
	    for(int i=0; i<data.length;i++) {
	        ret[i] = toInteger(data[i], null);
	    }
	    
	    return ret;
	}
	
	public static Integer handleInteger(String data, Integer onerror ) {
	    if ( data == null || data.trim().length() == 0 )
			return onerror;

		Integer ret = onerror;

		try {
		    ret = new Integer(data);
			if(ret.intValue() <= 0 ) {
			    ret = onerror;
			}
		} catch (NumberFormatException ex) { }

		return ret;
	}
	
	/**
	 * Considers default format as dd-MM-yyyy
	 * @param strDate
	 * @return Date
	 * 
	 */
	public static Date toDate(String strDate) {
	    
	    if(StringUtil.isEmpty(strDate)) return null;
	    
	    DateFormat formatter = new SimpleDateFormat(DEF_DATE_FORMAT);
        try {
            return formatter.parse(strDate);
        } catch (ParseException e) {
            return null;
        }
	}
	
	/**
	 * Considers default format as dd-MM-yyyy
	 * @param strDate
	 * @return Date
	 * 
	 */
	public static Date toDateTime(String strDate) {
	    
	    if(StringUtil.isEmpty(strDate)) return null;
	    
	    DateFormat formatter = new SimpleDateFormat(DEF_DATE_TIME_FORMAT);
        try {
            return formatter.parse(strDate);
        } catch (ParseException e) {
            return toDate(strDate);
        }
	}
	
	/**
	 * @param strDate
	 * @param format
	 * @return Date
	 */
	public static Date toDate(String strDate, String format) {
	    
	    if(StringUtil.isEmpty(strDate) || StringUtil.isEmpty(format)) return null;
	    
	    DateFormat formatter = new SimpleDateFormat(format);
        try {
            return formatter.parse(strDate);
        } catch (ParseException e) {
            return null;
        }
	}
	
	/**
	 * @param strDate
	 * @param format
	 * @return Date
	 */
	public static String toDateString(Date date, String format) {
	    
	    if(date ==null || StringUtil.isEmpty(format)) return null;
	    
	    DateFormat formatter = new SimpleDateFormat(format);
       
        return formatter.format(date);
       
	}
	
	/**
	 * @param strDate
	 * @param format
	 * @return Date
	 */
	public static String toDateString(Date date) {
	    
	    if(date ==null ) return null;
	    
	    DateFormat formatter = new SimpleDateFormat(DEF_DATE_FORMAT);
        return formatter.format(date);
	}
	
	public static String toDateTimeString(Date date) {
	    
	    if(date ==null ) return null;
	    
	    DateFormat formatter = new SimpleDateFormat(DEF_DATE_TIME_FORMAT);
        return formatter.format(date);
	}
	
	public  static String arrayToCsv(Object obj[]) {
		
	    if(obj == null || obj.length <=0) {
	        return null;
	    }
	    StringBuffer arr = new StringBuffer("");
	    for(int i=0; i<obj.length; i++) {
	        arr = arr.append(String.valueOf(obj[i]));
	        if(i+1 <obj.length) {
	        	arr.append(',');
	        }
	    }
	    return arr.toString();
	}

	/**
	 * @param requestParameterValues
	 * @param object
	 * @return
	 */
	public static Long[] toLong(String[] values, long onerror) {
		
		Long []retValues = new Long[values.length];
		for (int i = 0; i < values.length; i++) {
			try {
				retValues[i] = Long.valueOf(values[i].trim());
			} catch (NumberFormatException e) {
				retValues[i] = new Long(onerror);
				e.printStackTrace();
			}
		}
		return retValues;
	}
	
	public static BigDecimal toBigDecimal(String val) {
		
		if (StringUtil.isEmpty(val)) return null;
		
		try {
			return new BigDecimal(val);
		} catch (RuntimeException e) {
			return null;
		}
		
	}
	
	public static Map convertToTreeMap (Map map) {
	    
	    if (map == null || map.isEmpty()) {
	        return map;
	    }
	    
	    TreeMap tMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
	    
	    Iterator it = map.keySet().iterator();
	    
	    while (it.hasNext()) {
	        Object key = it.next();
	        Object val = map.get(key);
	        
	        tMap.put(val, key);
	    }
	    
	    return tMap;
	    
	}
	
	/**
	 * @param date 
	 * @return String
	 */
	public static String toStringMonth(String date) {
		
		if (StringUtil.isEmpty(date)) return null;
		
		try {
			String new_date = null;
			String mon = date.substring(3,5);
			if(mon.equals("01")) {
				new_date = date.replaceAll(mon,"Jan");
				if(new_date.startsWith("Jan")) {
					new_date = new_date.replaceFirst("Jan","01");
				}
			}
			if(mon.equals("02")) {
				new_date = date.replaceAll(mon,"Feb");
				if(new_date.startsWith("Feb")) {
					new_date = new_date.replaceFirst("Feb","02");
				}
			}
			if(mon.equals("03")) {
				new_date = date.replaceAll(mon,"Mar");
				if(new_date.startsWith("Mar")) {
					new_date = new_date.replaceFirst("Mar","03");
				}
			}
			if(mon.equals("04")) {
				new_date = date.replaceAll(mon,"Apr");
				if(new_date.startsWith("Apr")) {
					new_date = new_date.replaceFirst("Apr","04");
				}
			}
			if(mon.equals("05")) {
				new_date = date.replaceAll(mon,"May");
				if(new_date.startsWith("May")) {
					new_date = new_date.replaceFirst("May","05");
				}
			}
			if(mon.equals("06")) {
				new_date = date.replaceAll(mon,"June");
				if(new_date.startsWith("June")) {
					new_date = new_date.replaceFirst("June","06");
				}
			}
			if(mon.equals("07")) {
				new_date = date.replaceAll(mon,"July");
				if(new_date.startsWith("July")) {
					new_date = new_date.replaceFirst("July","07");
				}
			}
			if(mon.equals("08")) {
				new_date = date.replaceAll(mon,"Aug");
				if(new_date.startsWith("Aug")) {
					new_date = new_date.replaceFirst("Aug","08");
				}
			}
			if(mon.equals("09")) {
				new_date = date.replaceAll(mon,"Sept");
				if(new_date.startsWith("Sept")) {
					new_date = new_date.replaceFirst("Sept","09");
				}
			}
			if(mon.equals("10")) {
				new_date = date.replaceAll(mon,"Oct");
				if(new_date.startsWith("Oct")) {
					new_date = new_date.replaceFirst("Oct","10");
				}
			}
			if(mon.equals("11")) {
				new_date = date.replaceAll(mon,"Nov");
				if(new_date.startsWith("Nov")) {
					new_date = new_date.replaceFirst("Nov","11");
				}
			}
			if(mon.equals("12")) {
				new_date = date.replaceAll(mon,"Dec");
				if(new_date.startsWith("Dec")) {
					new_date = new_date.replaceFirst("Dec","12");
				}
			}
			return new_date;
		} catch (RuntimeException e) {
			return null;
		}
	}
	
}
