package com.thbs.mis.framework.util;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


/**
 * @author kranthi
 */
public class XMLErrorHandler implements ErrorHandler, ErrorListener {

	

	/* (non-Javadoc)
	 * @see org.xml.sax.ErrorHandler#warning(org.xml.sax.SAXParseException)
	 */
	public void warning(SAXParseException spe) throws SAXException {

		spe.printStackTrace(System.out);
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ErrorHandler#error(org.xml.sax.SAXParseException)
	 */
	public void error(SAXParseException spe) throws SAXException {

		spe.printStackTrace(System.out);
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ErrorHandler#fatalError(org.xml.sax.SAXParseException)
	 */
	public void fatalError(SAXParseException spe) throws SAXException {

		spe.printStackTrace(System.out);
	}

	/* (non-Javadoc)
	 * @see javax.xml.transform.ErrorListener#warning(javax.xml.transform.TransformerException)
	 */
	public void warning(TransformerException e) {
		e.printStackTrace(System.out);
	}

	/* (non-Javadoc)
	 * @see javax.xml.transform.ErrorListener#error(javax.xml.transform.TransformerException)
	 */
	public void error(TransformerException e) {
		e.printStackTrace(System.out);
	}

	/* (non-Javadoc)
	 * @see javax.xml.transform.ErrorListener#fatalError(javax.xml.transform.TransformerException)
	 */
	public void fatalError(TransformerException e) {
		e.printStackTrace(System.out);
	}

	/**
	 * @param spe
	 * @return
	 */
	public static String getParseExceptionInfo(SAXParseException spe) {

		StringBuffer info = new StringBuffer("URI=").append(spe.getSystemId())
				.append(" Line=").append(spe.getLineNumber())
				.append(" Column=").append(spe.getColumnNumber()).append(
						"\nMessage=").append(spe.getMessage());

		return info.toString();
	}
}