package com.thbs.mis.framework.Notification;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bean.FCMTokenBean;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.notificationframework.gsrnotification.service.GSRNotificationService;
import com.thbs.mis.notificationframework.notificationsettings.bo.DatNotificationsBO;
import com.thbs.mis.notificationframework.notificationsettings.bo.MasNotificationSettingBO;
import com.thbs.mis.notificationframework.notificationsettings.dao.MasNotificationSettingRepository;
import com.thbs.mis.notificationframework.notificationsettings.dao.NotificationRepository;

@Service
public class FCMNotificationService {

	@Autowired
	NotificationRepository notificationRepository;
	
	@Autowired
	GSRNotificationService gsrService;
	
	@Autowired
	MasNotificationSettingRepository notificationSettingRepository;

	private static final AppLog LOG = LogFactory
			.getLog(FCMNotificationService.class);

	final String AUTH_KEY_FCM = "AAAA8N4FfJg:APA91bGCgnLIEG7tuBE7hZ8S1PvyFQRZ1q8c0fNApbFYPbk7RmgrocOrrUt5KpPNiXTAlXT-qi5j49i0Vnwj-eTFFDQX39lHiIojPC2FK_PmJiTBT1SyKTfG-bhQ4N0SACIq25PFPhsQ";
	final String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

	public String CommonNotification(FCMTokenBean userDeviceIdBean)
			throws Exception {

		/*
		 * LOG.startUsecase("CommonNotification"); String authKey =
		 * AUTH_KEY_FCM; // You FCM AUTH key String FMCurl = API_URL_FCM;
		 * LOG.startUsecase("AUTH_KEY_FCM" + AUTH_KEY_FCM);
		 * 
		 * 
		 * URL url = new URL(FMCurl); LOG.startUsecase("FCM URL" + url);
		 * System.setProperty("javax.net.ssl.trustStore",
		 * "C:/Program Files (x86)/Java/jre1.8.0_20/lib/security/cacerts");
		 * System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
		 * System.setProperty("javax.net.ssl.keyStore",
		 * "C:/Program Files (x86)/Java/jre1.8.0_20/lib/security/cacerts");
		 * System.setProperty("javax.net.ssl.keyStorePassword","changeit");
		 * 
		 * System.setProperty("javax.net.ssl.trustStore",
		 * "D:/JDK1.8/jdk1.8.0_20/jre/lib/security/cacerts.jks");
		 * System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
		 * 
		 * HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		 * 
		 * //URLConnection conn = url.openConnection();
		 * 
		 * 
		 * conn.setUseCaches(false); conn.setDoInput(true);
		 * conn.setDoOutput(true);
		 * 
		 * conn.setRequestMethod("POST");
		 * conn.setRequestProperty("Authorization", "key=" + authKey);
		 * conn.setRequestProperty("Content-Type", "application/json");
		 * 
		 * JSONObject json = new JSONObject(); json.put("to",
		 * deviceToken.trim()); JSONObject info = new JSONObject();
		 * info.put("title", "Notificatoin Title"); // Notification title
		 * info.put("body", "Hello New Test notification"); // Notification body
		 * info.put( "icon",
		 * "https://media.glassdoor.com/sql/272465/torry-harris-business-solutions-squarelogo-1393001047610.png"
		 * );
		 * info.put("click_action","http://localhost:4040/NucleusLocalUI/#/login"
		 * ); json.put("notification", info);
		 * 
		 * System.out.println("JSON object:"+json.toString());
		 * 
		 * OutputStreamWriter wr = new
		 * OutputStreamWriter(conn.getOutputStream());
		 * wr.write(json.toString()); wr.flush(); conn.getInputStream();
		 * //System.out.println("Common Notification  Sent Successfully");
		 * LOG.endUsecase("CommonNotification"); return "Success";
		 */

		final String AUTH_KEY_FCM = "AAAA8N4FfJg:APA91bGCgnLIEG7tuBE7hZ8S1PvyFQRZ1q8c0fNApbFYPbk7RmgrocOrrUt5KpPNiXTAlXT-qi5j49i0Vnwj-eTFFDQX39lHiIojPC2FK_PmJiTBT1SyKTfG-bhQ4N0SACIq25PFPhsQ";
		final String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

		String authKey = AUTH_KEY_FCM; // You FCM AUTH key
		String FMCurl = API_URL_FCM;
		System.setProperty("javax.net.ssl.trustStore",
				"C:/Program Files (x86)/Java/jdk1.8.0_20/jre/lib/security/cacerts");
		System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
		System.setProperty("javax.net.ssl.keyStore",
				"C:/Program Files (x86)/Java/jdk1.8.0_20/jre/lib/security/cacerts");
		System.setProperty("javax.net.ssl.keyStorePassword", "changeit");

		LOG.startUsecase("AUTH_KEY_FCM  : " + AUTH_KEY_FCM);

		URL url = new URL(FMCurl);
		LOG.startUsecase("FCM URL : " + url);
		/*HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);

		conn.setRequestMethod("POST");
		conn.setRequestProperty("Authorization", "key=" + authKey);
		conn.setRequestProperty("Content-Type", "application/json");
*/
		/*
		 * int responseCode = conn.getResponseCode();
		 * 
		 * System.out.println("\nSending 'POST' request to URL : " + url);
		 * System.out.println("Response Code : " + responseCode);
		 */
		
		/*HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);

		conn.setRequestMethod("POST");
		conn.setRequestProperty("Authorization", "key=" + authKey);
		conn.setRequestProperty("Content-Type", "application/json");
		JSONObject json = new JSONObject();
		json.put("to", userDeviceIdBean.getUserDeviceIdKey().trim());
		JSONObject info = new JSONObject();
		info.put("title", "Test"); 
		info.put("body", "Test"); 
		info.put(
				"icon",
				"https://media.glassdoor.com/sql/272465/torry-harris-business-solutions-squarelogo-1393001047610.png");
		json.put("notification", info);

		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(json.toString());
		wr.flush();
		conn.getInputStream();*/

		List<DatNotificationsBO> notificationList;
		MasNotificationSettingBO notificationSettings = null;
		try {
			
			String successMsg = gsrService.setGoalNotificationByEmployee(userDeviceIdBean.getEmpId());
			
			
			if(successMsg.equals("Success"))
			{
				notificationList = notificationRepository
						.getPushNotificationsForEmpByModuleId(
								userDeviceIdBean.getEmpId(),
								userDeviceIdBean.getModuleIdList());
				System.out.println("Notification list size:"+notificationList.size());
				
				if(notificationList.size() > 0)
				{
					System.out.println("Inside list > 0");
					for (DatNotificationsBO datNotificationsBO : notificationList) {
						HttpURLConnection conn = (HttpURLConnection) url.openConnection();
						conn.setUseCaches(false);
						conn.setDoInput(true);
						conn.setDoOutput(true);

						conn.setRequestMethod("POST");
						conn.setRequestProperty("Authorization", "key=" + authKey);
						conn.setRequestProperty("Content-Type", "application/json");
						
						JSONObject json = new JSONObject();
						json.put("to", userDeviceIdBean.getUserDeviceIdKey().trim());
						JSONObject info = new JSONObject();
						info.put("title", datNotificationsBO.getModuleName()); 
						info.put("body", datNotificationsBO.getNoificationMessage()); 
						info.put(
								"icon",
								"https://media.glassdoor.com/sql/272465/torry-harris-business-solutions-squarelogo-1393001047610.png");
						json.put("notification", info);

						OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
						wr.write(json.toString());
						wr.flush();
						conn.getInputStream();
					}
				}
				
				notificationSettings = notificationSettingRepository.findByFkEmployeeId(userDeviceIdBean.getEmpId());
				
				if(notificationSettings != null)
				{
					notificationSettings.setLastAccessedDate(new Date());
					notificationSettingRepository.save(notificationSettings);
				}
			}

		} catch (Exception e) {
			throw new DataAccessException(
					"Some exceptions occured while getting notifications.");
		}

		/*JSONObject json = new JSONObject();
		json.put("to", userDeviceIdBean.getUserDeviceIdKey().trim());
		JSONObject info = new JSONObject();
		info.put("title", "Notificatoin Title"); // Notification title
		info.put("body", "Hello Test notification"); // Notification body
		info.put(
				"icon",
				"https://media.glassdoor.com/sql/272465/torry-harris-business-solutions-squarelogo-1393001047610.png");
		json.put("notification", info);

		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(json.toString());
		wr.flush();
		conn.getInputStream();
		System.out.println("Push Notification Sent Successfully");*/
		return "Sent Successully";

	}

}
