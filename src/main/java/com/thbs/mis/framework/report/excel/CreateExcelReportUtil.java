//package com.thbs.mis.framework.util.excel;
//
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//
//import org.apache.commons.io.FilenameUtils;
//import org.apache.poi.hssf.usermodel.HSSFCellStyle;
//import org.apache.poi.hssf.usermodel.HSSFSheet;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//
//import com.thbs.mis.framework.util.excel.bean.ExcelAllSheetData;
//import com.thbs.mis.framework.util.excel.bean.ExcelData;
//import com.thbs.mis.framework.util.excel.bean.ExcelSheetData;
//
//public class CreateExcelReportUtil {
//
//	private static SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
//
//	private static SimpleDateFormat dateFormat = new SimpleDateFormat(
//			"yyyy-MM-dd");
//	private static FileOutputStream excelFileToCreate;
//	private static boolean validPath = false;
//
//	public boolean createExcelReport(String fileName, String filePath,
//			 String headers[],List<List> data) throws IOException {
//		boolean result = false;
//		String fullPath = filePath + "" + fileName;
//		String ext2 = FilenameUtils.getExtension(fullPath);
//		try {
//			excelFileToCreate = new FileOutputStream(fullPath);
//			validPath = true;
//			if (ext2.equalsIgnoreCase("xls")) {
//				result = this.createXlSReport(headers,data,excelFileToCreate);
//			} else {
//				result = this.createXlSXReport(headers,data,excelFileToCreate);
//			}
//		} catch (Exception e) {
//			validPath = false;
//			e.printStackTrace();
//		}
//		return true;
//	}
//
//	private boolean createXlSXReport(String[] headers, List<List> data,
//			FileOutputStream excelFileToCreate) {
//		return false;
//	}
//
//	private boolean createXlSReport(String[] headers, List<List> data,
//			FileOutputStream excelFileToCreate) {
//
//		boolean isGenerated=false;
//		try {
//			HSSFWorkbook workbook = new HSSFWorkbook();
//			HSSFSheet sheet = workbook.createSheet("First sheet");
//			Row headerRow = sheet.createRow(0);
//			headerRow.setHeightInPoints(40);
//			for (int header = 0; header < headers.length; header++) {
//				Cell cell = headerRow.createCell(header);
//				cell.setCellValue(headers[header]);
//			}
//				for (int rowIndex=0;rowIndex<data.size();rowIndex++) {
//					Row row = sheet.createRow(rowIndex+1);
//					List<String> celList = data.get(rowIndex);
//					for (int colIndex=0;colIndex<celList.size();colIndex++) {
//						Cell cell = row.createCell(colIndex);
//						cell.setCellValue(celList.get(colIndex));
//					}
//				}
//				HSSFCellStyle hsfstyle = workbook.createCellStyle();
//				hsfstyle.setBorderBottom((short) 1);
//				hsfstyle.setFillBackgroundColor((short) 245);
//				try {
//					workbook.write(excelFileToCreate);
//					System.out.println("************************************ Generated successfully ***********************");
//					isGenerated = true;
//				} catch (Exception e) {
//					System.out.println("falied");
//					isGenerated = false;
//					e.printStackTrace();
//				}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//			return isGenerated;
//	 
//	
//	}
// 
//	
//	public static void main(String args[]) throws Exception {
////		List<String> headerRow = new ArrayList<String>();
////		headerRow.add("Employee No");
////		headerRow.add("Employee Name");
////		headerRow.add("Employee Address");
//		String headers[]= {"Employee No","Employee Name","Employee Address"};
//		List<String> firstRow = new ArrayList<String>();
//		firstRow.add("1111");
//		firstRow.add("Gautam");
//		firstRow.add("India");
//		List<String> secondRow = new ArrayList<String>();
//		firstRow.add("1111");
//		firstRow.add("Gautam");
//		firstRow.add("India");
//		List contentList = new ArrayList();
//		contentList.add(firstRow);
//		contentList.add(secondRow);
//		 CreateExcelReportUtil c = new CreateExcelReportUtil();
//		 c.createExcelReport("/mani2.xls", "D://", headers, contentList);
//	}
//}
//
//
// 
