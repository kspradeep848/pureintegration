package com.thbs.mis.framework.report.excel.bean;

import java.util.Arrays;
import java.util.List;

public class ExcelSheetData {
	public ExcelSheetData(String title, String[] headers,
			List<ExcelData> rowData) {
		super();
		this.title = title;
		this.headers = headers;
		this.rowData = rowData;
	}
	public ExcelSheetData(){ 
		
	}

	@Override
	public String toString() {
		return "ExcelSheetData [title=" + title + ", headers="
				+ Arrays.toString(headers) + ", rowData=" + rowData + "]";
	}

	String title;
	String headers[];
	List<ExcelData> rowData;

	public List<ExcelData> getRowData() {
		return rowData;
	}

	public void setRowData(List<ExcelData> rowData) {
		this.rowData = rowData;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String[] getHeaders() {
		return headers;
	}

	public void setHeaders(String[] headers) {
		this.headers = headers;
	}

	 

}
