package com.thbs.mis.framework.report.excel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.report.Util.DownloadFileUtil;
import com.thbs.mis.framework.report.excel.bean.ExcelAllSheetData;
import com.thbs.mis.framework.report.excel.bean.ExcelData;
import com.thbs.mis.framework.report.excel.bean.ExcelSheetData;
import com.thbs.mis.framework.report.excel.bean.ReportTemplateConfigureBean;
import com.thbs.mis.framework.report.excel.bean.TemplateFirstSheetFilterByRoleName;
import com.thbs.mis.framework.report.excel.bean.TemplateSecondSheetTableData;
import com.thbs.mis.framework.report.excel.bean.TemplateThirdSheetOutputQueryData;

public class ReadExcelConfigTeampleUtil {

	
	
	public static ReportTemplateConfigureBean getExcelTemplateConfigdata(String basePath,String templateName) throws IOException, CommonCustomException{
		List<ExcelAllSheetData> data = new ArrayList<ExcelAllSheetData>();
		ReportTemplateConfigureBean reportTemplateConfigureBean = new ReportTemplateConfigureBean();
		data = ReadExcelTeampleUtil.uploadFileToRead(
				templateName, basePath);
		if(data.size()<1)
			throw new CommonCustomException("Unable to parse the excel sheet"+templateName);
		else{
			int CONSTANT_FILTER_BY_ROLENAME_SHEET_INDEX = 0;
			int CONSTANT_TABLE_DATA_SHEET_INDEX = 1;
			int CONSTANT_JOIN_TABLE_SHEET_INDEX = 2;
			int CONSTANT_QUERY_BUILDER_TABLE_SHEET_INDEX = 3;
			int HEADER_INDEX = 0;
			for (ExcelAllSheetData allSheetData : data) {
				if (allSheetData.getSheetNumber() == CONSTANT_FILTER_BY_ROLENAME_SHEET_INDEX) {
					int FILTER_BY_ROLE_NAME_INDEX = 0;
					int FILTER_BY_FILTER_ID_INDEX = 1;
					int FILTER_BY_DEFAULT_FILTER_ID_INDEX = 2;
					List<TemplateFirstSheetFilterByRoleName> filterByEmpIdList = new ArrayList<TemplateFirstSheetFilterByRoleName>();
					for (ExcelSheetData sheetData : allSheetData.getExcelData()) {
						TemplateFirstSheetFilterByRoleName filterByEmpIdObj = new TemplateFirstSheetFilterByRoleName();
						for (ExcelData rowdata : sheetData.getRowData()) {
							String filterIds[];
							if (rowdata.getRowIndex() != HEADER_INDEX) {
								if (rowdata.getCellIndex() == FILTER_BY_ROLE_NAME_INDEX)
									filterByEmpIdObj
											.setRoleName(rowdata.getCellData());
								else if (rowdata.getCellIndex() == FILTER_BY_FILTER_ID_INDEX)
									filterByEmpIdObj.setFilterByIds(rowdata
											.getCellData().split(","));
								else if (rowdata.getCellIndex() == FILTER_BY_DEFAULT_FILTER_ID_INDEX)
									filterByEmpIdObj.setDefaultFilterByIds(rowdata
											.getCellData().split(","));
							}
						}
						
						if (filterByEmpIdObj.getRoleName() != null){
							filterByEmpIdList.add(filterByEmpIdObj);
							reportTemplateConfigureBean.setFilterByRoleName(filterByEmpIdList);
						}
					}
				}else if(allSheetData.getSheetNumber() == CONSTANT_TABLE_DATA_SHEET_INDEX){
					int JOIN_TYPE_INDEX = 0;
					int JOIN_TABLE_NAME_INDEX = 1;
					int JOIN_TABLE_ALIAS_NAME_INDEX = 2;
					int MAIN_TABLE_NAME = 3;
					int MAIN_TABLE_ALIAS_NAME= 4;
					int JOIN_TABLE_CONDTION_QUERY_INDEX = 5;
					List<TemplateSecondSheetTableData> tableDataList = new ArrayList<TemplateSecondSheetTableData>();
					for (ExcelSheetData sheetData : allSheetData.getExcelData()) {
						TemplateSecondSheetTableData tableDataObj = new TemplateSecondSheetTableData();
						for (ExcelData rowdata : sheetData.getRowData()) {
							if (rowdata.getRowIndex() != HEADER_INDEX) {
								if (rowdata.getCellIndex() == JOIN_TYPE_INDEX)
									tableDataObj
											.setJoinType(rowdata.getCellData());
								else if (rowdata.getCellIndex() == JOIN_TABLE_NAME_INDEX)
									tableDataObj.setTableName(rowdata.getCellData());
								else if (rowdata.getCellIndex() == JOIN_TABLE_ALIAS_NAME_INDEX)
									tableDataObj.setTabelAliasName(rowdata.getCellData());
								else if (rowdata.getCellIndex() == JOIN_TABLE_CONDTION_QUERY_INDEX)
									tableDataObj.setCondtionQuery(rowdata.getCellData());
								else if (rowdata.getCellIndex() == MAIN_TABLE_NAME)
									reportTemplateConfigureBean.setMainTableName(rowdata.getCellData());
								else if (rowdata.getCellIndex() == MAIN_TABLE_ALIAS_NAME)
									reportTemplateConfigureBean.setMainTableAliasName(rowdata.getCellData());
							 
							}
						}
						if(tableDataObj.getTableName()!=null){
						tableDataList.add(tableDataObj);
						reportTemplateConfigureBean.setJoinTables(tableDataList);
						}
					}
				}else if(allSheetData.getSheetNumber() == CONSTANT_JOIN_TABLE_SHEET_INDEX){
					int OUTPUT_FILTER_ID_INDEX = 0;
					int OUTPUT_TABLE_FILED_INDEX = 1;
					int OUTPUT_TABLE_ALIAS_NAME_INDEX = 2;
					int OUTPUT_ALIAS_FIELD_INDEX= 3;
					int OUTPUT_TABLE_FILED_HEADER_NAME_INDEX = 4;
					List<TemplateThirdSheetOutputQueryData> outputQueryList = new ArrayList<TemplateThirdSheetOutputQueryData>();
					for (ExcelSheetData sheetData : allSheetData.getExcelData()) {
						TemplateThirdSheetOutputQueryData outputQueryObj = new TemplateThirdSheetOutputQueryData();
						for (ExcelData rowdata : sheetData.getRowData()) {
							if (rowdata.getRowIndex() != HEADER_INDEX) {
								if (rowdata.getCellIndex() == OUTPUT_FILTER_ID_INDEX)
									outputQueryObj
											.setOutputfilterId(rowdata.getCellData());
								else if (rowdata.getCellIndex() == OUTPUT_TABLE_FILED_INDEX)
									outputQueryObj.setOutputTableField(rowdata.getCellData());
								else if (rowdata.getCellIndex() == OUTPUT_TABLE_ALIAS_NAME_INDEX)
									outputQueryObj.setOutputTableAliasName(rowdata.getCellData());
								else if (rowdata.getCellIndex() == OUTPUT_ALIAS_FIELD_INDEX)
									outputQueryObj.setOutputFieldValue(rowdata.getCellData());
								else if (rowdata.getCellIndex() == OUTPUT_TABLE_FILED_HEADER_NAME_INDEX)
									outputQueryObj.setOutputHeaderName(rowdata.getCellData());
							 
							}
						}
						if(outputQueryObj.getOutputfilterId()!=null && outputQueryObj.getOutputTableField()!=null ){
							outputQueryList.add(outputQueryObj);
							reportTemplateConfigureBean.setOutputQueryData(outputQueryList);
						}
					}
				}else if(allSheetData.getSheetNumber() == CONSTANT_QUERY_BUILDER_TABLE_SHEET_INDEX){
					List<Object> header = new ArrayList<Object>();
					for (ExcelSheetData sheetData : allSheetData.getExcelData()) {
						for (ExcelData rowdata : sheetData.getRowData()) {
							if (rowdata.getRowIndex() != HEADER_INDEX){ 
							//	System.out.println(rowdata.getCellData());
								header.add(rowdata.getCellData());
							}
						}
					}
					if(header.size()>0)
						reportTemplateConfigureBean.setQueryBuilderJson(header);
				}
			}
			reportTemplateConfigureBean.setConditionQuery("not yet");
			reportTemplateConfigureBean.setDataOutputQuery("not yet");
			reportTemplateConfigureBean.setDataSubQuery(getDataSubQuery(reportTemplateConfigureBean));
			reportTemplateConfigureBean.setOriginalQuery("not yet");
		}
		return reportTemplateConfigureBean;
	}
	public static void main(String a[]) throws IOException, CommonCustomException{
		ReportTemplateConfigureBean data=	getExcelTemplateConfigdata("d://",DownloadFileUtil.GSR_REPORT_CONFIG_TEMPLATE);
		//System.out.println("data "+data.toString());
 	}
	private static String getDataSubQuery(ReportTemplateConfigureBean reportTemplateConfigureBean){
		 String subQuery = " Select ";
		 int index=0;
		 for(TemplateThirdSheetOutputQueryData obj:reportTemplateConfigureBean.getOutputQueryData()){
			 if(index==0)
				 subQuery = subQuery+obj.getOutputTableField() + " as "+obj.getOutputFieldValue();
			 else{
				 subQuery = subQuery+","+obj.getOutputTableField() + " as "+obj.getOutputFieldValue();
			 }
				index++; 
		 }
		 subQuery = subQuery+ " from "+reportTemplateConfigureBean.getMainTableName()+" as "+ reportTemplateConfigureBean.getMainTableAliasName();
		 for(TemplateSecondSheetTableData obj2:reportTemplateConfigureBean.getJoinTables()){
			 subQuery  =subQuery +" "+obj2.getJoinType() +" "+ obj2.getTableName() + " "+  obj2.getTabelAliasName() + " ON "+obj2.getCondtionQuery();
		 }
		return subQuery;
	}
}
