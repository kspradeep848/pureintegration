package com.thbs.mis.framework.report.excel;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


public class WriteExcel {

    private WritableCellFormat timesBoldUnderline;
    private WritableCellFormat times;
    private String inputFile;

public void setOutputFile(String inputFile) {
    this.inputFile = inputFile;
    }

    public boolean  write(List<String> headers,List<String[]> data) throws IOException, WriteException {
    	boolean result = false;
        try {
			File file = new File(inputFile);
			WorkbookSettings wbSettings = new WorkbookSettings();
			wbSettings.setLocale(new Locale("en", "EN"));
			WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
			workbook.createSheet("Report", 0);
			WritableSheet excelSheet = workbook.getSheet(0);
			createLabel(excelSheet,headers);
			createContent(excelSheet,data);
			workbook.write();
			workbook.close();
			result = true;
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
		}
        return result;
    }

    private void createLabel(WritableSheet sheet,List<String> headers)
            throws WriteException {
        WritableFont times10pt = new WritableFont(WritableFont.TIMES, 8);
        times = new WritableCellFormat(times10pt);
        times.setWrap(true);
        WritableFont times10ptBoldUnderline = new WritableFont(
                WritableFont.TIMES, 8, WritableFont.BOLD, false,
                UnderlineStyle.SINGLE);
        timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
        timesBoldUnderline.setWrap(true);
        CellView cv = new CellView();
        cv.setFormat(times);
        cv.setFormat(timesBoldUnderline);
        cv.setSize(10);
        for (int i = 0; i < headers.size(); i++) {
        	  addCaption(sheet, i, 0, headers.get(i));
		}
    }

    private void createContent(WritableSheet sheet,List<String[]> data) throws WriteException,
            RowsExceededException {
    	//System.out.println("data size "+data);
    	int cellIndex=1;
    	for (int i = 0; i < data.size(); i++) {
    		String cellData[] =  data.get(i);
    		
			for (int j = 0; j < cellData.length; j++) {
				//System.out.println("cell row of "+j+cellData[j]);
				  addLabel(sheet, j, cellIndex, cellData[j] );
			}
			cellIndex ++;
		}
    }

    private void addCaption(WritableSheet sheet, int column, int row, String s)
            throws RowsExceededException, WriteException {
        Label label;
        label = new Label(column, row, s, timesBoldUnderline);
        sheet.addCell(label);
    }

    private void addLabel(WritableSheet sheet, int column, int row, String s)
            throws WriteException, RowsExceededException {
        Label label;
        label = new Label(column, row, s, times);
        sheet.addCell(label);
    }

    
}