package com.thbs.mis.framework.report.excel;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.report.excel.bean.ExcelAllSheetData;
import com.thbs.mis.framework.report.excel.bean.ExcelData;
import com.thbs.mis.framework.report.excel.bean.ExcelSheetData;

public class ReadExcelTeampleUtil {

	private static SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");

	private static SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd");
	private static FileInputStream excelFileToRead;
	private static boolean validPath = false;

	public static List<ExcelAllSheetData> uploadFileToRead(String fileName,
			String filePath) throws IOException {
		List<ExcelAllSheetData> excelSheetDataList = new ArrayList<ExcelAllSheetData>();
		//System.out.println("Entered uploadFileToRead ");
		String fullPath = filePath + "" + fileName;
		//System.out.println("Full path " + fullPath);
		String ext2 = FilenameUtils.getExtension(fullPath);
		//System.out.println("ext2 " + ext2);
		try {
			excelFileToRead = new FileInputStream(fullPath);
			validPath = true;
			String[][] excelData;
			if (ext2.equalsIgnoreCase("xls")) {
				excelSheetDataList = readXLSFile();
				//System.out.println("Its a xls file ");
			} else {
				//excelSheetDataList = readXLSXFile();
				//System.out.println("Its a xlxs file ");
			}
		} catch (Exception e) {
			validPath = false;
			e.printStackTrace();

			// throw new CommonCustomException("Wrong Path Access ");
		}
		return excelSheetDataList;

	}

	@SuppressWarnings("unused")
	private static List<ExcelAllSheetData> readXLSFile() throws IOException,
			ParseException, CommonCustomException {
		 List<ExcelAllSheetData> excelSheetDataList = new ArrayList<ExcelAllSheetData>();
		if (validPath == true) {
			HSSFWorkbook wb = new HSSFWorkbook(excelFileToRead);
			for (int i = 0; i < wb.getNumberOfSheets(); i++) {
				List<ExcelSheetData> excelDataList = new ArrayList<ExcelSheetData>();
				HSSFSheet sheet = wb.getSheetAt(i);
				HSSFRow row;
				HSSFCell cell;
				Iterator rows = sheet.rowIterator();
				int rowIndex = 0;
				while (rows.hasNext()) {
					row = (HSSFRow) rows.next();
					Iterator cells = row.cellIterator();
					List<ExcelData> celDataList = new ArrayList<ExcelData>();
					ExcelSheetData excelSheetData = new ExcelSheetData();
					int cellIndex = 0;
					while (cells.hasNext()) {
						cell = (HSSFCell) cells.next();
						ExcelData celData = new ExcelData(rowIndex, cellIndex,
								cell.toString());
						celDataList.add(celData);
						cellIndex++;
					}
					excelSheetData.setRowData(celDataList);
					rowIndex++;
					excelDataList.add(excelSheetData);
				}
				ExcelAllSheetData  e = new ExcelAllSheetData();
				 e.setSheetName(wb.getSheetName(i));
				 e.setSheetNumber(i);
				 e.setExcelData(excelDataList);
				 excelSheetDataList.add(e);
			}
			return excelSheetDataList;
		} else {
			throw new CommonCustomException("Wrong Path in readXLSFile");
		}
	}

	@SuppressWarnings("unused")
	private static List<ExcelSheetData> readXLSXFile() throws IOException,
			ParseException, CommonCustomException {
		List<ExcelSheetData> excelSheetDataList = new ArrayList<ExcelSheetData>();
		if (validPath == true) {
			XSSFWorkbook wb = new XSSFWorkbook(excelFileToRead);
			XSSFWorkbook test = new XSSFWorkbook();
			XSSFSheet sheet = wb.getSheetAt(0);
			XSSFRow row;
			XSSFCell cell;
			Iterator rows = sheet.rowIterator();
			int rowIndex = 0;
			while (rows.hasNext()) {
				ExcelSheetData excelSheetData = new ExcelSheetData();
				row = (XSSFRow) rows.next();
				Iterator cells = row.cellIterator();
				List<ExcelData> cellDataList = new ArrayList<ExcelData>();
				int cellIndex = 0;
				while (cells.hasNext()) {
					cell = (XSSFCell) cells.next();
					ExcelData celData = new ExcelData(rowIndex, cellIndex,
							cell.toString());
					cellDataList.add(celData);
					cellIndex++;
				}
				excelSheetData.setRowData(cellDataList);
				rowIndex++;
				excelSheetDataList.add(excelSheetData);

			}
			return excelSheetDataList;
		} else {
			throw new CommonCustomException("Wrong Path in readXLSXFile");
		}
	}
}
