package com.thbs.mis.framework.report.excel.bean;

import java.util.Arrays;
import java.util.List;

public class ReportTemplateConfigureBean {
	
	String dataOutputQuery;
	String mainTableName;
	String mainTableAliasName;
	List<TemplateSecondSheetTableData> joinTables;
	List<TemplateThirdSheetOutputQueryData> outputQueryData;
	List<TemplateFirstSheetFilterByRoleName> filterByRoleName;
	String dataSubQuery;
	String conditionQuery;
	String originalQuery;
	List<Object> queryBuilderJson;

	public String getDataOutputQuery() {
		return dataOutputQuery;
	}
	public void setDataOutputQuery(String dataOutputQuery) {
		this.dataOutputQuery = dataOutputQuery;
	}
	public String getMainTableName() {
		return mainTableName;
	}
	public void setMainTableName(String mainTableName) {
		this.mainTableName = mainTableName;
	}
	public String getMainTableAliasName() {
		return mainTableAliasName;
	}
	public void setMainTableAliasName(String mainTableAliasName) {
		this.mainTableAliasName = mainTableAliasName;
	}
	public List<TemplateSecondSheetTableData> getJoinTables() {
		return joinTables;
	}
	public void setJoinTables(List<TemplateSecondSheetTableData> joinTables) {
		this.joinTables = joinTables;
	}
	public List<TemplateThirdSheetOutputQueryData> getOutputQueryData() {
		return outputQueryData;
	}
	public void setOutputQueryData(
			List<TemplateThirdSheetOutputQueryData> outputQueryData) {
		this.outputQueryData = outputQueryData;
	}
	public String getDataSubQuery() {
		return dataSubQuery;
	}
	public void setDataSubQuery(String dataSubQuery) {
		this.dataSubQuery = dataSubQuery;
	}
	public String getConditionQuery() {
		return conditionQuery;
	}
	public void setConditionQuery(String conditionQuery) {
		this.conditionQuery = conditionQuery;
	}
	public String getOriginalQuery() {
		return originalQuery;
	}
	public void setOriginalQuery(String originalQuery) {
		this.originalQuery = originalQuery;
	}
	 
	public List<TemplateFirstSheetFilterByRoleName> getFilterByRoleName() {
		return this.filterByRoleName;
	}
	public void setFilterByRoleName(List<TemplateFirstSheetFilterByRoleName> filterByRoleName) {
		this.filterByRoleName = filterByRoleName;
	}
	
	public List<Object> getQueryBuilderJson() {
		return queryBuilderJson;
	}
	public void setQueryBuilderJson(List<Object> queryBuilderJson) {
		this.queryBuilderJson = queryBuilderJson;
	}
	@Override
	public String toString() {
		return "ReportTemplateConfigureBean [dataOutputQuery="
				+ dataOutputQuery + ", mainTableName=" + mainTableName
				+ ", mainTableAliasName=" + mainTableAliasName
				+ ", joinTables=" + joinTables + ", outputQueryData="
				+ outputQueryData + ", filterByRoleName=" + filterByRoleName
				+ ", dataSubQuery=" + dataSubQuery + ", conditionQuery="
				+ conditionQuery + ", originalQuery=" + originalQuery
				+ ", queryBuilderJson=" + queryBuilderJson + "]";
	}
	

}

 

