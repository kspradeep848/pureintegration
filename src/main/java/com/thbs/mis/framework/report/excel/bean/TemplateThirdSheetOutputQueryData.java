package com.thbs.mis.framework.report.excel.bean;

public class TemplateThirdSheetOutputQueryData{
	String outputTableField;
	String outputTableAliasName;
	String outputFieldValue;
	String outputHeaderName;
	String outputfilterId;
	
	public String getOutputTableField() {
		return outputTableField;
	}
	public void setOutputTableField(String outputTableField) {
		this.outputTableField = outputTableField;
	}
	public String getOutputTableAliasName() {
		return outputTableAliasName;
	}
	public void setOutputTableAliasName(String outputTableAliasName) {
		this.outputTableAliasName = outputTableAliasName;
	}
	public String getOutputFieldValue() {
		return outputFieldValue;
	}
	public void setOutputFieldValue(String outputFieldValue) {
		this.outputFieldValue = outputFieldValue;
	}
	public String getOutputHeaderName() {
		return outputHeaderName;
	}
	public void setOutputHeaderName(String outputHeaderName) {
		this.outputHeaderName = outputHeaderName;
	}
	public String getOutputfilterId() {
		return outputfilterId;
	}
	public void setOutputfilterId(String outputfilterId) {
		this.outputfilterId = outputfilterId;
	}
	@Override
	public String toString() {
		return "outputQueryData [outputTableField=" + outputTableField
				+ ", outputTableAliasName=" + outputTableAliasName
				+ ", outputFieldValue=" + outputFieldValue
				+ ", outputHeaderName=" + outputHeaderName
				+ ", outputfilterId=" + outputfilterId + "]";
	}
	 
	
}