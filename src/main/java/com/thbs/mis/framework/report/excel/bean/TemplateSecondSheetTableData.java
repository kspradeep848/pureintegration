package com.thbs.mis.framework.report.excel.bean;

public class TemplateSecondSheetTableData{
	String joinType;
	String tableName;
	String tabelAliasName;
	String condtionQuery;
	
	public String getJoinType() {
		return joinType;
	}
	public void setJoinType(String joinType) {
		this.joinType = joinType;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getTabelAliasName() {
		return tabelAliasName;
	}
	public void setTabelAliasName(String tabelAliasName) {
		this.tabelAliasName = tabelAliasName;
	}
	public String getCondtionQuery() {
		return condtionQuery;
	}
	public void setCondtionQuery(String condtionQuery) {
		this.condtionQuery = condtionQuery;
	}
	@Override
	public String toString() {
		return "tableData [joinType=" + joinType + ", tableName=" + tableName
				+ ", tabelAliasName=" + tabelAliasName + ", condtionQuery="
				+ condtionQuery + "]";
	}
	
	
}
