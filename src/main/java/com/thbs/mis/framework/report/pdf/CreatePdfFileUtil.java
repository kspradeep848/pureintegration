package com.thbs.mis.framework.report.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.springframework.stereotype.Service;

import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

@Service
public class CreatePdfFileUtil {

	private static final AppLog LOG = LogFactory.getLog(CreatePdfFileUtil.class);

	public static void generateJasperReportFromListAndObjectOfBean(String jasperReportName, FileOutputStream outputStream,
			@SuppressWarnings("rawtypes") List reportList, Map parameter) {
		
		
		try {
			LOG.info("jasperReportName :: " + jasperReportName);
			
			File capexTemplate = new File(jasperReportName);
			LOG.info("File is present : " + capexTemplate.exists());

			JasperDesign jd = JRXmlLoader.load(capexTemplate);
			
			LOG.startUsecase("Report List in generate report pdf:"+reportList);
			
			LOG.startUsecase("Template Path:"+jasperReportName);

			JasperReport jasperReport = JasperCompileManager.compileReport(jd);
			LOG.startUsecase("jasperReport:"+jasperReport);
			
		
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameter,
					new JRBeanCollectionDataSource(reportList));
			LOG.startUsecase("jasperPrint::"+jasperPrint);

			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
			LOG.info("----------END ----------------------");

		} catch (Exception e) {
			LOG.debug("Error in writing to jasper report : " + e);

		}
	}
	
}
