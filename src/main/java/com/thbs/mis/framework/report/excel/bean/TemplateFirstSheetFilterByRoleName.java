package com.thbs.mis.framework.report.excel.bean;

import java.util.Arrays;


public class TemplateFirstSheetFilterByRoleName{
	String roleName;
	String[] filterByIds;
	String[] defaultFilterByIds;
	
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String[] getFilterByIds() {
		return filterByIds;
	}
	public void setFilterByIds(String[] filterByIds) {
		this.filterByIds = filterByIds;
	}
	public String[] getDefaultFilterByIds() {
		return defaultFilterByIds;
	}
	public void setDefaultFilterByIds(String[] defaultFilterByIds) {
		this.defaultFilterByIds = defaultFilterByIds;
	}
	@Override
	public String toString() {
		return "TemplateFirstSheetFilterByRoleName [roleName=" + roleName
				+ ", filterByIds=" + Arrays.toString(filterByIds)
				+ ", defaultFilterByIds=" + Arrays.toString(defaultFilterByIds)
				+ "]";
	}
	 
	
	
}