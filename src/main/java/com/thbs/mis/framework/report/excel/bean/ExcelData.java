package com.thbs.mis.framework.report.excel.bean;

import java.util.List;

public class ExcelData {

	@Override
	public String toString() {
		return "ExcelData [rowIndex=" + rowIndex + ", cellIndex=" + cellIndex
				+ ", cellData=" + cellData + ", rowContent=" + rowContent
				+ ", getRowSize=" + getRowSize + "]";
	}

	Integer rowIndex;
	Integer cellIndex;
	String cellData;
	List<ExcelData> rowContent;
	Integer getRowSize;

	public Integer getGetRowSize() {
		return rowContent.size();
	}

	public List<ExcelData> getRowContent() {
		return rowContent;
	}

	public void setRowContent(List<ExcelData> rowContent) {
		this.rowContent = rowContent;
	}

	public ExcelData(Integer rowIndex, Integer cellIndex, String cellData) {
		this.rowIndex = rowIndex;
		this.cellIndex = cellIndex;
		this.cellData = cellData;
	}

	public ExcelData(List<ExcelData> rowContent) {
		this.rowContent = rowContent;
	}

	public ExcelData() {
		// TODO Auto-generated constructor stub
	}
	public Integer getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}

	public Integer getCellIndex() {
		return cellIndex;
	}

	public void setCellIndex(Integer cellIndex) {
		this.cellIndex = cellIndex;
	}

	public String getCellData() {
		return cellData;
	}

	public void setCellData(String cellData) {
		this.cellData = cellData;
	}

}
