package com.thbs.mis.framework.report.Util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;

@Service
public class DownloadFileUtil {

	final static String SEPARATOR = "/";

	public static String MEAL_VOUCHER_REPORT = "mealVoucherTemplate.xls";
	
	public static String GIFT_VOUCHER_REPORT = "giftVoucherTemplate.xls";
	
	public static String EMPLOYEE_UAN_REPORT = "UanDetailsTemplate.xls";
	
	public static String MEAL_VOUCHER_REPORT_BASED_ON_RANGES = "mealVoucherTemplateBasedOnRanges.xls";
	
	public static String MEAL_VOUCHER_REPORT_BASED_ON_RANGES_AND_VOUCHER_ID = "mealVoucherTemplateBasedOnRangesWithId.xls";
	
	public static String GSR_REPORT_CONFIG_TEMPLATE = "gsrReportQueryTemplate.xls";
	
	private static final AppLog LOG = LogFactory
			.getLog(DownloadFileUtil.class);
	

	public static void downloadFile(HttpServletResponse response,
			String BASEPATH, String destFileName) throws IOException, CommonCustomException {
		LOG.startUsecase("Entered downloadFile");
		File outputFile = new File(BASEPATH + destFileName);
		if(outputFile.exists() == true){
		InputStream inputStream = new FileInputStream(outputFile);
		response.setContentType("application/force-download");
		response.setHeader("Content-Disposition", "attachment; filename="
				+ destFileName);
		IOUtils.copy(inputStream, response.getOutputStream());
		response.flushBuffer();
		inputStream.close();
		}else{
			throw new CommonCustomException("File not exist");
		}
		LOG.endUsecase("Exited downloadFile");
	}
	
	public static void downloadVideoFile(
			HttpServletResponse response,String BASEPATH, String destFileName) throws  IOException, CommonCustomException {
			String videoName = destFileName;
			String filePath = "";
			filePath = BASEPATH;
			filePath += videoName;
			File file = null;
//			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
//			file = new File(classloader.getResource(filePath).getFile());
			file = new File(filePath).getCanonicalFile();
			if(!file.exists()){
				throw new CommonCustomException("File not exist");
			}
			String mimeType= URLConnection.guessContentTypeFromName(file.getName());
			if(mimeType==null){
				mimeType = "video/*";
			}
	        response.setContentType(mimeType);
	        response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
	        response.setContentLength((int)file.length());
			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
	        FileCopyUtils.copy(inputStream, response.getOutputStream());
	        
			
// 			Old codes 
	        
//			File file = new File(filePath);
//			int length = 0;
//			ServletOutputStream outStream = response.getOutputStream();
//			ServletContext context = getServletConfig().getServletContext();
//			String mimetype = context.getMimeType(filePath);

			// sets response content type
//			if (mimetype == null) {
//			mimetype = "video/x-ms-wmv";
//			}
//			response.setContentType("video/mp4");
//			response.setContentLength((int) file.length());
//			String fileName = (new File(filePath)).getName();
//
//			// sets HTTP header
//			response.setHeader("Content-Disposition", "attachment; filename=\""
//			+ fileName + "\"");
//
//			byte[] byteBuffer = new byte[102400000];
//			DataInputStream in = new DataInputStream(new FileInputStream(file));
//
//			// reads the file's bytes and writes them to the response stream
//			while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
//			outStream.write(byteBuffer, 0, length);
//			}
//
//			in.close();
//			outStream.close();
			
			}
	
	public static void downloadAllFile(HttpServletResponse response,
			String BASEPATH) throws IOException {
		LOG.startUsecase("Entered downloadFile");
		 List<String> filenames = new ArrayList<String>();
		
		
		
		File outputFile = new File(BASEPATH);
		File[] files = outputFile.listFiles();
		for(File f : files){
		
		filenames.add(f.getName());
		
		}
		
	
	    byte[] buf = new byte[2048];
	    // Create the ZIP file
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ZipOutputStream out = new ZipOutputStream(baos);
	    // Compress the files
	    for (int i=0; i<filenames.size(); i++) {
	    FileInputStream fis = new FileInputStream(BASEPATH+filenames.get(i).toString());
	    BufferedInputStream bis = new BufferedInputStream(fis);
	    // Add ZIP entry to output stream.
	    File file = new File(filenames.get(i).toString());
	    String entryname = file.getName();
	    out.putNextEntry(new ZipEntry(entryname));
	    int bytesRead;
	    while ((bytesRead = bis.read(buf)) != -1) {
	    out.write(buf, 0, bytesRead);
	    }
	    out.closeEntry();
	    bis.close();
	    fis.close();
	    }
	    out.flush();
	    baos.flush();
	    out.close();
	    baos.close();
	    ServletOutputStream sos = response.getOutputStream();
	    response.setContentType("application/zip");
	    response.setHeader("Content-Disposition", "attachment; filename=\"SupportDocs.ZIP\"");
	    sos.write(baos.toByteArray());
	    out.flush();
	    out.close();
	    sos.flush();
		
	
		LOG.endUsecase("Exited downloadFile");
	}
	
	
	public static void downloadFilesTwoPath(HttpServletResponse response,
			List<String> listOfPath) throws IOException {
		LOG.startUsecase("Entered downloadFile");
		
		  byte[] buf = new byte[2048];
		    // Create the ZIP file
		    ByteArrayOutputStream baos = new ByteArrayOutputStream();
		    ZipOutputStream out = new ZipOutputStream(baos);
		for(String path : listOfPath){
		File outputFile = new File(path);
		File[] files = outputFile.listFiles();
		for(File f : files){
			  FileInputStream fis = new FileInputStream(path+f.getName().toString());
			    BufferedInputStream bis = new BufferedInputStream(fis);
		
			    out.putNextEntry(new ZipEntry(f.getName()));
			    int bytesRead;
			    while ((bytesRead = bis.read(buf)) != -1) {
			    out.write(buf, 0, bytesRead);
			    }
			    out.closeEntry();
			    bis.close();
			    fis.close();
		}
		}
		
			out.flush();
		    baos.flush();
		    out.close();
		    baos.close();
	  
	    ServletOutputStream sos = response.getOutputStream();
	    response.setContentType("application/zip");
	    response.setHeader("Content-Disposition", "attachment; filename=\"SupportDocs.ZIP\"");
	    sos.write(baos.toByteArray());
	    out.flush();
	    out.close();
	    sos.flush();
		
	
		LOG.endUsecase("Exited downloadFile");
	}
	public static boolean IsExcepReportCreated(String rootDir, Map beans,
			String inutFileName, String outputFileName)
			throws ParsePropertyException, InvalidFormatException, IOException {
		LOG.startUsecase("Entered IsExcepReportCreated");
		boolean result = false;
		XLSTransformer transformer = new XLSTransformer();
		try {
			System.out.println("input:"+rootDir + inutFileName);
			System.out.println("output filename:"+rootDir
					+ outputFileName);
			transformer.transformXLS(rootDir + inutFileName, beans, rootDir
					+ outputFileName);
			result = true;
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
		}
		LOG.endUsecase("Exited IsExcepReportCreated");
		return result;
	}

	public static void deleteFile(String rootDir, String fileName) {
		File deleteFile = new File(rootDir + fileName);
		try {
			LOG.startUsecase("Entered deleteFile");
			deleteFile.deleteOnExit();
			LOG.endUsecase("Exited deleteFile");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static boolean deleteFileFromServer(String rootDir, String fileName) {
		File deleteFile = new File(rootDir + fileName);
		boolean isFileDeleted = false;
		try {
			LOG.startUsecase("Entering deleteFileFromServer");
			isFileDeleted = deleteFile.delete();
			LOG.endUsecase("Exiting deleteFileFromServer");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isFileDeleted;
	}

	
	public static void downloadSelectedFile(HttpServletResponse response,
			String BASEPATH, List<String> listOfFileNames) throws IOException 
	{
		LOG.startUsecase("Entered downloadSelectedFile");
		//List<String> filenames = new ArrayList<String>();
		
		//File outputFile = new File(BASEPATH);
		/*File[] files = outputFile.listFiles();
		for(File f : files)
		{
			filenames.add(f.getName());
		}*/
		
	
	    byte[] buf = new byte[2048];
	    // Create the ZIP file
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ZipOutputStream out = new ZipOutputStream(baos);
	    // Compress the files
	    for (int i=0; i<listOfFileNames.size(); i++) 
	    {
		    FileInputStream fis = new FileInputStream(BASEPATH + listOfFileNames.get(i).toString());
		    BufferedInputStream bis = new BufferedInputStream(fis);
		    // Add ZIP entry to output stream.
		    File file = new File(listOfFileNames.get(i).toString());
		    String entryname = file.getName();
		    out.putNextEntry(new ZipEntry(entryname));
		    int bytesRead;
		    while ((bytesRead = bis.read(buf)) != -1) 
		    {
		    	out.write(buf, 0, bytesRead);
		    }
		    out.closeEntry();
		    bis.close();
		    fis.close();
	    }
	    out.flush();
	    baos.flush();
	    out.close();
	    baos.close();
	    ServletOutputStream sos = response.getOutputStream();
	    response.setContentType("application/zip");
	    response.setHeader("Content-Disposition", "attachment; filename=\"AgendaDocs.ZIP\"");
	    sos.write(baos.toByteArray());
	    out.flush();
	    out.close();
	    sos.flush();
		
	
		LOG.endUsecase("Exited downloadSelectedFile");
	}
}
