package com.thbs.mis.framework.report.excel.bean;

import java.util.List;

public class ExcelAllSheetData {
	@Override
	public String toString() {
		return "ExcelAllSheetData [SheetName=" + SheetName + ", sheetNumber="
				+ sheetNumber + ", excelData=" + excelData + "]";
	}
	String SheetName;
	int sheetNumber;
	List<ExcelSheetData> excelData;
	public String getSheetName() {
		return SheetName;
	}
	public void setSheetName(String sheetName) {
		SheetName = sheetName;
	}
	public int getSheetNumber() {
		return sheetNumber;
	}
	public void setSheetNumber(int sheetNumber) {
		this.sheetNumber = sheetNumber;
	}
	public List<ExcelSheetData> getExcelData() {
		return excelData;
	}
	public void setExcelData(List<ExcelSheetData> excelData) {
		this.excelData = excelData;
	}
}
