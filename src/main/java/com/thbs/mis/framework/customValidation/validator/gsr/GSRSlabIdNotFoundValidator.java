package com.thbs.mis.framework.customValidation.validator.gsr;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_Slab;
import com.thbs.mis.gsr.bo.GsrSlabBO;
import com.thbs.mis.gsr.service.GsrSlabService;

public class GSRSlabIdNotFoundValidator implements
		ConstraintValidator<GSR_Id_Not_Found_Slab, Short> {

	 

	@Autowired
	GsrSlabService slabService;

	private GSR_Id_Not_Found_Slab gsrSlabIdNotFound;

	@Override
	public void initialize(GSR_Id_Not_Found_Slab gsrSlabIdNotFound) {
		this.gsrSlabIdNotFound = gsrSlabIdNotFound;
	}

	@Override
	public boolean isValid(Short slabId, ConstraintValidatorContext arg1) {
		boolean result = true;

		try {
			if (slabId != null) {
				GsrSlabBO slabInfo = slabService.getSlabInfoById(slabId);
				if (slabInfo != null) {
					result = true;
				} else {
					result = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

}
