package com.thbs.mis.framework.customValidation.annotation.invoicePO;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Min;

import com.thbs.mis.framework.customValidation.validator.invoicePO.InvoicePONubmerValidator;


@Documented
@Constraint(validatedBy = InvoicePONubmerValidator.class)
@Target({ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface INVOICE_PO_NUMBER_NOT_VALID {
	String message() default "Invoice PO number is not alphanumeric";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
