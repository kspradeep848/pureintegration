package com.thbs.mis.framework.customValidation.validator.special;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.thbs.mis.framework.customValidation.annotation.special.Valid_Voucher_Amount;

public class AmountValidator implements
		ConstraintValidator<Valid_Voucher_Amount, Integer> {

	Valid_Voucher_Amount amount;

	@Override
	public void initialize(Valid_Voucher_Amount amount) {
		this.amount = amount;

	}

	@Override
	public boolean isValid(Integer selectedAmount,
			ConstraintValidatorContext arg1) {
		boolean result = true;

		try {
			if(selectedAmount != null){
				int multiplyInto = this.amount.multiplyInto();
				if (!((selectedAmount % multiplyInto) == 0)) {
					result = false;
				}
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}
}
