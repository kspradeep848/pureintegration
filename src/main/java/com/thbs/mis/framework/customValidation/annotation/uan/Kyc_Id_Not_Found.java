package com.thbs.mis.framework.customValidation.annotation.uan;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.thbs.mis.framework.customValidation.validator.uan.KycDetailsNotFoundValidator;

@Documented
@Constraint(validatedBy = KycDetailsNotFoundValidator.class)
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Kyc_Id_Not_Found {
	
	String message() default "Invalid KYC Id";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
