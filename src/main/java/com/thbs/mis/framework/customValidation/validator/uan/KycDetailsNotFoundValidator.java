package com.thbs.mis.framework.customValidation.validator.uan;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.thbs.mis.framework.customValidation.annotation.uan.Kyc_Id_Not_Found;
import com.thbs.mis.uan.bo.DatEmpKycDetailBO;
import com.thbs.mis.uan.service.UanService;

public class KycDetailsNotFoundValidator implements
ConstraintValidator<Kyc_Id_Not_Found, Integer>{
	
	@Autowired
	UanService uanService;
	
	private Kyc_Id_Not_Found kycId;

	@Override
	public void initialize(Kyc_Id_Not_Found kycId) {
		this.kycId = kycId;
		
	}

	@Override
	public boolean isValid(Integer kycId, ConstraintValidatorContext arg1) {
		boolean result = true;

		try {
			if (kycId != 0) {
				DatEmpKycDetailBO kycDetail = uanService.getKycDetailsByKycId(kycId);
				if (kycDetail != null) {
					result = true;
				} else {
					result = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
