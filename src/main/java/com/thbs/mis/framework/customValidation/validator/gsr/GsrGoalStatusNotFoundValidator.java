package com.thbs.mis.framework.customValidation.validator.gsr;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.thbs.mis.framework.customValidation.annotation.gsr.GSR_Id_Not_Found_GoalStatus;
import com.thbs.mis.gsr.bo.GsrMasGoalStatusBO;
import com.thbs.mis.gsr.service.GSRGoalService;

public class GsrGoalStatusNotFoundValidator implements
ConstraintValidator<GSR_Id_Not_Found_GoalStatus, Byte>
{

	@Autowired
	GSRGoalService goalService;

	private GSR_Id_Not_Found_GoalStatus goalId;

	@Override
	public void initialize(GSR_Id_Not_Found_GoalStatus goalId) {
		this.goalId = goalId;
	}

	@Override
	public boolean isValid(Byte goalId, ConstraintValidatorContext arg1)
	{
		boolean result = true;
		try {
			if (goalId != 0) {
				GsrMasGoalStatusBO slabInfo = goalService.getGoalStatusById(goalId);
				if (slabInfo != null) {
					result = true;
				} else {
					result = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	
	}
	
	

}
