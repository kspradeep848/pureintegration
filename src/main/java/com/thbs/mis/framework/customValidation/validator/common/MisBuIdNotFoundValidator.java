package com.thbs.mis.framework.customValidation.validator.common;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.bo.MasBuUnitBO;
import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.framework.customValidation.annotation.common.MIS_Id_Not_Found_BusinessUnit;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

public class MisBuIdNotFoundValidator implements
		ConstraintValidator<MIS_Id_Not_Found_BusinessUnit, Short> {

	private static final AppLog LOG = LogFactory
			.getLog(MisUserIdNotFoundValidator.class);

	@Autowired
	CommonService commonService;

	private MIS_Id_Not_Found_BusinessUnit misBuIdNotFound;

	@Override
	public void initialize(MIS_Id_Not_Found_BusinessUnit misBuIdNotFound) {
		this.misBuIdNotFound = misBuIdNotFound;
	}

	@Override
	public boolean isValid(Short buId, ConstraintValidatorContext arg1) {
		boolean result = true;
		LOG.startUsecase("Entered MisBuIdNotFoundValidator");
		try {
			if (buId != null) {
				MasBuUnitBO masBuUnitBO = commonService
						.getBuDetailsById(buId);
				if (masBuUnitBO != null) {
					result = true;
				} else {
					result = false;
				}
			} else {
				result = true;
			}
		} catch (DataAccessException e) {
			LOG.debug("Some error occured in MisBuIdNotFoundValidator", e);
		}
		LOG.endUsecase("Exited MisBuIdNotFoundValidator");
		return result;
	}

}
