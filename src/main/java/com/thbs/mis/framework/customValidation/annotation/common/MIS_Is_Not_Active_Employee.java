package com.thbs.mis.framework.customValidation.annotation.common;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Min;

import com.thbs.mis.framework.customValidation.validator.common.MisUserIdNotFoundValidator;

@Min(1)
@Documented
@Constraint(validatedBy = MisUserIdNotFoundValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface MIS_Is_Not_Active_Employee {

	String message() default "User Is Not An Active Employee";
	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
