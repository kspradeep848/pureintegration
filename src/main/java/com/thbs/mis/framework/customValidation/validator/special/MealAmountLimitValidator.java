package com.thbs.mis.framework.customValidation.validator.special;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.thbs.mis.framework.customValidation.annotation.special.Valid_Voucher_Meal_Amount_Limit;

@Configuration
@PropertySource("classpath:mis_voucher.properties")
public class MealAmountLimitValidator implements
		ConstraintValidator<Valid_Voucher_Meal_Amount_Limit, Integer> {

	@Value("${voucher.meal.amount.max}")
	int voucherMaxAmount;

	@Override
	public void initialize(Valid_Voucher_Meal_Amount_Limit arg0) {

	}

	@Override
	public boolean isValid(Integer selectedAmount,
			ConstraintValidatorContext arg1) {
		try {
			if (selectedAmount != null) {
				if (selectedAmount > voucherMaxAmount) {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
}
