package com.thbs.mis.framework.customValidation.annotation.training;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.thbs.mis.framework.customValidation.validator.training.EmployeeIdNotFoundValidator;

@Documented
@Constraint(validatedBy = EmployeeIdNotFoundValidator.class)
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Employee_Id_Not_Found {
	
	String message() default "Invalid Employee Id";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
