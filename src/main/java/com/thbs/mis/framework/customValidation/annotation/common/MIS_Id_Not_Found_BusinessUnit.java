package com.thbs.mis.framework.customValidation.annotation.common;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Min;

import com.thbs.mis.framework.customValidation.validator.common.MisBuIdNotFoundValidator;

@Min(1)
@Documented
@Target({ElementType.FIELD })
@Constraint(validatedBy = MisBuIdNotFoundValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface MIS_Id_Not_Found_BusinessUnit {

	String message() default "";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
