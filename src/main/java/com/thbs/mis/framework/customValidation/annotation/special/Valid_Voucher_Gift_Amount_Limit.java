package com.thbs.mis.framework.customValidation.annotation.special;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.thbs.mis.framework.customValidation.validator.special.GiftAmountLimitValidator;

@Documented
@Constraint(validatedBy = GiftAmountLimitValidator.class)
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Valid_Voucher_Gift_Amount_Limit {

	String message() default "The amount has reached the max limit";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
