package com.thbs.mis.framework.customValidation.annotation.common;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.thbs.mis.framework.customValidation.validator.common.MisUserIdNotFoundValidator;


@Documented
@Constraint(validatedBy = MisUserIdNotFoundValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)

public @interface MIS_Id_Not_Found_Employee {
	
	String message() default "Employee Id does not Exist in Database";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
