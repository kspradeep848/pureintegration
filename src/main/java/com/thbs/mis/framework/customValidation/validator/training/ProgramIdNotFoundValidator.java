package com.thbs.mis.framework.customValidation.validator.training;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.thbs.mis.framework.customValidation.annotation.training.Training_Program_Id_Not_Found;
import com.thbs.mis.training.bo.TrainingDatProgrammeDetailBO;
import com.thbs.mis.training.bo.TrainingDatRequestDetailBO;
import com.thbs.mis.training.service.TrainingProgramService;

public class ProgramIdNotFoundValidator implements
ConstraintValidator<Training_Program_Id_Not_Found, Integer>{
	
	@Autowired
	TrainingProgramService trainingProgramService;
	
	private Training_Program_Id_Not_Found progId;

	@Override
	public void initialize(Training_Program_Id_Not_Found progId) {
		this.progId = progId;
		
	}

	@Override
	public boolean isValid(Integer progId, ConstraintValidatorContext arg1) {
		boolean result = true;

		try {
			if (progId != 0) {
				TrainingDatProgrammeDetailBO progInfo = trainingProgramService
						.getProgramId(progId);
				if (progInfo != null) {
					result = true;
				} else {
					result = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
