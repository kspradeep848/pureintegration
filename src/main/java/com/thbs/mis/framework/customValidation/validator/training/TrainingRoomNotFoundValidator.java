package com.thbs.mis.framework.customValidation.validator.training;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.thbs.mis.framework.customValidation.annotation.training.Training_Room_Id_Not_Found;
import com.thbs.mis.training.bo.TrainingMasRoomDetailBO;
import com.thbs.mis.training.service.TrainingCommonService;

public class TrainingRoomNotFoundValidator implements
		ConstraintValidator<Training_Room_Id_Not_Found, Short> {

	@Autowired
	TrainingCommonService trainingCommonService;
	
	private Training_Room_Id_Not_Found roomId;

	@Override
	public void initialize(Training_Room_Id_Not_Found arg0) {
		this.roomId = roomId;
	}

	@Override
	public boolean isValid(Short roomId, ConstraintValidatorContext arg1) {
		boolean result = true;

		try {
			if (roomId != 0) {
				TrainingMasRoomDetailBO roomInfo = trainingCommonService
						.getRoomDetails(roomId);
				if (roomInfo != null) {
					result = true;
				} else {
					result = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
