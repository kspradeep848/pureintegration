package com.thbs.mis.framework.customValidation.annotation.gsr;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
 
public @interface GSR_Id_Not_Found_FinalRating {

	String message();

	 
}