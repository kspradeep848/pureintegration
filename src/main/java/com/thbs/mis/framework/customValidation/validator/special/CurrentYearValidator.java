package com.thbs.mis.framework.customValidation.validator.special;

import java.util.Calendar;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.thbs.mis.framework.customValidation.annotation.special.Valid_Is_Current_Year;

public class CurrentYearValidator implements
		ConstraintValidator<Valid_Is_Current_Year, String> {

	@Override
	public void initialize(Valid_Is_Current_Year arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isValid(String year, ConstraintValidatorContext arg1) {
		if (year != null) {
			int currentYear = Calendar.getInstance().get(Calendar.YEAR);
			int selectedYear = Integer.parseInt(year);
			if (currentYear != selectedYear) {
				return false;
			}
		}
		return true;
	}

}
