package com.thbs.mis.framework.customValidation.validator.invoicePO;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.thbs.mis.framework.customValidation.annotation.invoicePO.INVOICE_PO_NUMBER_NOT_VALID;

public class InvoicePONubmerValidator implements ConstraintValidator<INVOICE_PO_NUMBER_NOT_VALID, String>
{

	private INVOICE_PO_NUMBER_NOT_VALID InvoicePONumber;
	
	final static String ALPHANUMERIC_PATTERN = "^[a-zA-Z0-9]*$";
	
	@Override
	public void initialize(INVOICE_PO_NUMBER_NOT_VALID InvoicePONumber) {
		this.InvoicePONumber = InvoicePONumber;
	}

	@Override
	public boolean isValid(String InvoicePONumber, ConstraintValidatorContext arg1)
	{
		boolean result = true;
		try {
			if (InvoicePONumber != null) {
				if (!Pattern.matches(ALPHANUMERIC_PATTERN, InvoicePONumber)) {
					result = false;
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	
	}
	

}

