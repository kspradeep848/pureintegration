package com.thbs.mis.framework.customValidation.validator.common;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.framework.customValidation.annotation.common.MIS_Id_Not_Found_Employee;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;

public class MisUserIdNotFoundValidator implements
		ConstraintValidator<MIS_Id_Not_Found_Employee, Integer> {

	private static final AppLog LOG = LogFactory
			.getLog(MisUserIdNotFoundValidator.class);

	@Autowired
	CommonService commonService;

	private MIS_Id_Not_Found_Employee misUserIdNotFound;

	@Override
	public void initialize(MIS_Id_Not_Found_Employee misUserIdNotFound) {
		this.misUserIdNotFound = misUserIdNotFound;
	}

	@Override
	public boolean isValid(Integer empId, ConstraintValidatorContext arg1) {
		boolean result = true;
		LOG.startUsecase("Entered MisUserIdNotFoundValidator");
		try {
			if (empId != null) {
				DatEmpProfessionalDetailBO datEmpProfessionalDetailBOData = commonService
						.getEmpoyeeDeatailsById(empId);
				if (datEmpProfessionalDetailBOData != null) {
					result = true;
				} else {
					result = false;
				}
			} else {
				result = true;
			}
		} catch (DataAccessException e) {
			LOG.debug("Some error occured in MisUserIdNotFoundValidator", e);
		}
		LOG.endUsecase("Exited MisUserIdNotFoundValidator");
		return result;
	}

}
