package com.thbs.mis.framework.customValidation.annotation.gsr;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Min;

import com.thbs.mis.framework.customValidation.validator.gsr.GSRSlabIdNotFoundValidator;

@Min(1)
@Documented
@Constraint(validatedBy = GSRSlabIdNotFoundValidator.class)
@Target({ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
 
public @interface GSR_Id_Not_Found_Slab {

	String message() default "Slab Id does Not Exists in Database";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
	 
}
