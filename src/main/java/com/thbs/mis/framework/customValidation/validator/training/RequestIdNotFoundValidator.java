package com.thbs.mis.framework.customValidation.validator.training;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.thbs.mis.framework.customValidation.annotation.training.Training_Request_Id_Not_Found;
import com.thbs.mis.training.bo.TrainingDatRequestDetailBO;
import com.thbs.mis.training.service.TrainingRequestService;

public class RequestIdNotFoundValidator implements
		ConstraintValidator<Training_Request_Id_Not_Found, Integer> {

	@Autowired
	TrainingRequestService trainingRequestService;

	private Training_Request_Id_Not_Found reqId;

	@Override
	public void initialize(Training_Request_Id_Not_Found reqId) {
		this.reqId = reqId;

	}

	@Override
	public boolean isValid(Integer reqId, ConstraintValidatorContext arg1) {
		boolean result = true;

		try {
			if (reqId != 0) {
				TrainingDatRequestDetailBO reqInfo = trainingRequestService
						.getRequestId(reqId);
				if (reqInfo != null) {
					result = true;
				} else {
					result = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
