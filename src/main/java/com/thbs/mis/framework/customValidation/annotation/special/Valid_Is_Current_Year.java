package com.thbs.mis.framework.customValidation.annotation.special;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.thbs.mis.framework.customValidation.validator.special.CurrentYearValidator;


@Documented
@Constraint(validatedBy = CurrentYearValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)

public @interface Valid_Is_Current_Year {
	
	String message() default "Year must be the current year";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
