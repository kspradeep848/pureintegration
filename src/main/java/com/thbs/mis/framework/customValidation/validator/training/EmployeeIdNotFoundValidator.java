package com.thbs.mis.framework.customValidation.validator.training;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;
import com.thbs.mis.training.service.TrainingRequestService;

public class EmployeeIdNotFoundValidator implements
		ConstraintValidator<Employee_Id_Not_Found, Integer> {

	@Autowired
	TrainingRequestService trainingRequestService;

	private Employee_Id_Not_Found empId;

	@Override
	public void initialize(Employee_Id_Not_Found empId) {
		this.empId = empId;
	}

	@Override
	public boolean isValid(Integer empId, ConstraintValidatorContext arg1) {
		boolean result = true;
		try {
			if (empId != null) {
				DatEmpDetailBO empInfo = trainingRequestService
						.getEmployeeId(empId);
				if (empInfo != null) {
					result = true;
				} else {
					result = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
