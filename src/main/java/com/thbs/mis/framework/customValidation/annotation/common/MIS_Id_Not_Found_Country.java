package com.thbs.mis.framework.customValidation.annotation.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Payload;

 
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MIS_Id_Not_Found_Country{

	String message() default "Country Id Is Not  Exist In Database";
	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
