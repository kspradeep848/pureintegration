package com.thbs.mis.framework.customValidation.validator.training;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.thbs.mis.framework.customValidation.annotation.training.Training_Program_Status_Not_Found;
import com.thbs.mis.training.bo.TrainingMasProgrammeStatusBO;
import com.thbs.mis.training.bo.TrainingMasRequestStatusBO;
import com.thbs.mis.training.service.TrainingProgramService;

public class ProgramStatusNotFoundValidator implements
ConstraintValidator<Training_Program_Status_Not_Found, Byte>{
	
	@Autowired
	private TrainingProgramService trainingProgramService;
	
	private Training_Program_Status_Not_Found statusId;

	@Override
	public void initialize(Training_Program_Status_Not_Found statusId) {
		this.statusId = statusId;
		
	}

	@Override
	public boolean isValid(Byte statusId, ConstraintValidatorContext arg1) {

		boolean result = true;

		try {
			if (statusId != 0) {
				TrainingMasProgrammeStatusBO reqInfo = trainingProgramService
						.getTrainingProgramStatus(statusId);
				if (reqInfo != null) {
					result = true;
				} else {
					result = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	
	}

}
