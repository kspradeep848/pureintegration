/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  DatEmpMealVoucherDeclarationController.java       */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :   20/12/2016                                       */
/*                                                                   */
/*  Description :  This is used to create URI for all the services.  */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date           Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 20/12/2016    THBS      1.0        Initial version created        */
/*********************************************************************/
package com.thbs.mis.voucher.constants;

/**
 * 
 * @author THBS
 * @Description: This is used to create URI for all the services.
 */
public class VoucherDeclarationURI {

	public static final String SUBMIT_GIFT_VOUCHER_CLAIMS = "/VOUCHER/gift/submit";

	public static final String SUBMIT_MEAL_VOUCHER_CLAIMS = "/VOUCHER/meal/submit";

	public static final String GET_MEAL_VOUCHER_CLAIMS = "/VOUCHER/meal/{fkMealVoucherDeclarationEmpId}";

	public static final String GET_MEAL_VOUCHER_REPORT = "/VOUCHER/meal/report/{year}/{month}";

	public static final String GET_GIFT_VOUCHER_REPORT = "/VOUCHER/gift/report/{year}";

	// Added by Kamal Anand
	public static final String GET_MEAL_VOUCHER_LIST = "/VOUCHER/meal/list/{year}/{month}";
	// End of Addition by Kamal Anand

	public static final String GET_MEAL_VOUCHER_REPORT_BY_SELECTED_VOUCHER_ID = "/VOUCHER/meal/voucherIds";

	public static final String GET_GIFT_VOUCHER_CLAIMS = "/VOUCHER/gift/{empId}/{year}";

	public static final String GET_GIFT_VOUCHER_DATES = "/VOUCHER/giftVoucher/dates/{fkMealVoucherDeclarationEmpId}";
	
	public static final String MEAL_VOUCHER_REPORT_BASED_ON_STARTDATE_ENDDATE = "/VOUCHER/meal/report/startDateandEndDate";
	
	public static final String MEAL_VOUCHER_MONTH_RANGE = "/VOUCHER/meal/monthRange";
	
	public static final String MEAL_VOUCHER_MONTH_RANGE_LIST_OF_VOUCHER_ID = "/VOUCHER/meal/listOfId";
	
	public static final String GET_ALL_GIFT_VOUCHER_CLAIMS = "/VOUCHER/gift/claims/{year}";
	
	public static final String GET_SELECTED_GIFT_CARD_REPORT = "/VOUCHER/gift/selected/report";
}
