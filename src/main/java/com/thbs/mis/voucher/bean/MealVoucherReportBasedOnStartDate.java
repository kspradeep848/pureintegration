package com.thbs.mis.voucher.bean;


public class MealVoucherReportBasedOnStartDate {

	private Integer voucherStartMonth;
	
	private Integer voucherEndMonth;
	
	private String voucherStartYear;
	
	private String voucherEndYear;

	public Integer getVoucherStartMonth() {
		return voucherStartMonth;
	}

	public void setVoucherStartMonth(Integer voucherStartMonth) {
		this.voucherStartMonth = voucherStartMonth;
	}

	public Integer getVoucherEndMonth() {
		return voucherEndMonth;
	}

	public void setVoucherEndMonth(Integer voucherEndMonth) {
		this.voucherEndMonth = voucherEndMonth;
	}

	public String getVoucherStartYear() {
		return voucherStartYear;
	}

	public void setVoucherStartYear(String voucherStartYear) {
		this.voucherStartYear = voucherStartYear;
	}

	public String getVoucherEndYear() {
		return voucherEndYear;
	}

	public void setVoucherEndYear(String voucherEndYear) {
		this.voucherEndYear = voucherEndYear;
	}

	@Override
	public String toString() {
		return "MealVoucherReportBasedOnStartDate [voucherStartMonth="
				+ voucherStartMonth + ", voucherEndMonth=" + voucherEndMonth
				+ ", voucherStartYear=" + voucherStartYear
				+ ", voucherEndYear=" + voucherEndYear + "]";
	}
}
