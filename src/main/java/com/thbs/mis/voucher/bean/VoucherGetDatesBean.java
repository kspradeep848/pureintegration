package com.thbs.mis.voucher.bean;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Calendar;
import java.util.Date;

public class VoucherGetDatesBean {

	String giftVoucherStartDate;

	String giftVoucherEndDate;

	Integer giftVoucherRemainingDays;

	Integer mealVoucherRemainingDays;

	String mealVoucherLastDay;

	String mealVoucherMonthValue;
	
	Integer mealVoucherEligibilityAmount;

	public String getGiftVoucherStartDate() {
		return giftVoucherStartDate;
	}

	public void setGiftVoucherStartDate(String giftVoucherStartDate) {
		this.giftVoucherStartDate = giftVoucherStartDate;
	}

	public String getGiftVoucherEndDate() {
		return giftVoucherEndDate;
	}

	public void setGiftVoucherEndDate(String giftVoucherEndDate) {
		this.giftVoucherEndDate = giftVoucherEndDate;
	}

	public Integer getGiftVoucherRemainingDays() {
		return giftVoucherRemainingDays;
	}

	public void setGiftVoucherRemainingDays(Integer giftVoucherRemainingDays) {
		this.giftVoucherRemainingDays = giftVoucherRemainingDays;
	}

	LocalDateTime currentTime = LocalDateTime.now();

	public Integer getMealVoucherRemainingDays() {
		LocalDate date1 = currentTime.toLocalDate();
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, currentTime.getYear());
		calendar.set(Calendar.MONTH, currentTime.getMonthValue());
		int numDays = calendar.getActualMaximum(Calendar.DATE);
		Integer currentDay = currentTime.getDayOfMonth();
		Month month = currentTime.getMonth();
		int lastDate = 22;
		if ((month.getValue() == 3)) {
			if ((currentDay > lastDate)) {
				this.mealVoucherRemainingDays = -1;
			} else if (currentDay <= lastDate) {
				this.mealVoucherRemainingDays = lastDate - currentDay;
			}
		} else {
			if (currentDay <= lastDate) {
				this.mealVoucherRemainingDays = lastDate - currentDay;
			} else {
				this.mealVoucherRemainingDays = (numDays - currentDay)
						+ lastDate;
			}
		}

		return mealVoucherRemainingDays + 1;
	}

	public String getMealVoucherLastDay() throws ParseException {
		Integer currentDay = currentTime.getDayOfMonth();
		Integer monthValue;
		if (currentTime.getMonthValue() == 3) {
			monthValue = currentTime.getMonthValue();
		} else if (currentDay <= 22) {
			monthValue = currentTime.getMonthValue();
		} else {
			monthValue = currentTime.getMonthValue() + 1;
		}
		String dateFormat = "22/" + monthValue + "/" + currentTime.getYear();
		DateFormat dateParse = new SimpleDateFormat("dd/MMM/yyyy");

		Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateFormat);
		String convert = dateParse.format(date);
		return convert;
	}

	public String getMealVoucherMonthValue() throws ParseException {
		Integer currentDay = currentTime.getDayOfMonth();
		Integer monthValue;
		if (currentTime.getMonthValue() == 3) {
			monthValue = currentTime.getMonthValue();
		} else if (currentDay <= 22) {
			monthValue = currentTime.getMonthValue();
		} else {
			monthValue = currentTime.getMonthValue() + 1;
		}
		String dateFormat = "22/" + monthValue + "/" + currentTime.getYear();
		Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateFormat);
		String convert = new SimpleDateFormat("MMM").format(date);
		return convert;
	}

	public Integer getMealVoucherEligibilityAmount() {
		return mealVoucherEligibilityAmount;
	}

	public void setMealVoucherEligibilityAmount(Integer mealVoucherEligibilityAmount) {
		this.mealVoucherEligibilityAmount = mealVoucherEligibilityAmount;
	}

	@Override
	public String toString() {
		return "VoucherGetDatesBean [giftVoucherStartDate="
				+ giftVoucherStartDate + ", giftVoucherEndDate="
				+ giftVoucherEndDate + ", giftVoucherRemainingDays="
				+ giftVoucherRemainingDays + ", mealVoucherRemainingDays="
				+ mealVoucherRemainingDays + ", mealVoucherLastDay="
				+ mealVoucherLastDay + ", mealVoucherMonthValue="
				+ mealVoucherMonthValue + ", mealVoucherEligibilityAmount="
				+ mealVoucherEligibilityAmount + ", currentTime=" + currentTime
				+ "]";
	}

}
