/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  MealVoucherDeclarationBean.java        		     */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :   06/03/2017                                       */
/*                                                                   */
/*  Description :  This class contains the output fields for         */
/* 				   Meal Voucher Declaration                          */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date           Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 20/12/2016    THBS      1.0        Initial version created        */
/*********************************************************************/
package com.thbs.mis.voucher.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class MealVoucherDeclarationBean
{

	private int empId;
	private String empName;
	private String empBu;
	private String voucherDeclarationAmt;
	private Date voucherDeclarationDate;
	private int month;
	private Integer mealVoucherDeclarationId;
	private String empMailId;
	private String empLocation;

	public String getEmpMailId()
	{
		return empMailId;
	}

	public void setEmpMailId(String empMailId)
	{
		this.empMailId = empMailId;
	}

	public String getEmpLocation()
	{
		return empLocation;
	}

	public void setEmpLocation(String empLocation)
	{
		this.empLocation = empLocation;
	}

	public Integer getMealVoucherDeclarationId()
	{
		return mealVoucherDeclarationId;
	}

	public void setMealVoucherDeclarationId(
			Integer mealVoucherDeclarationId)
	{
		this.mealVoucherDeclarationId = mealVoucherDeclarationId;
	}

	public int getEmpId()
	{
		return empId;
	}

	public void setEmpId(int empId)
	{
		this.empId = empId;
	}

	public String getEmpName()
	{
		return empName;
	}

	public void setEmpName(String empName)
	{
		this.empName = empName;
	}

	public String getEmpBu()
	{
		return empBu;
	}

	public void setEmpBu(String empBu)
	{
		this.empBu = empBu;
	}

	public String getVoucherDeclarationAmt()
	{
		return voucherDeclarationAmt;
	}

	public void setVoucherDeclarationAmt(String voucherDeclarationAmt)
	{
		this.voucherDeclarationAmt = voucherDeclarationAmt;
	}

	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getVoucherDeclarationDate()
	{
		return voucherDeclarationDate;
	}

	public void setVoucherDeclarationDate(Date voucherDeclarationDate)
	{
		this.voucherDeclarationDate = voucherDeclarationDate;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	@Override
	public String toString() {
		return "MealVoucherDeclarationBean [empId=" + empId + ", empName="
				+ empName + ", empBu=" + empBu + ", voucherDeclarationAmt="
				+ voucherDeclarationAmt + ", voucherDeclarationDate="
				+ voucherDeclarationDate + ", month=" + month
				+ ", mealVoucherDeclarationId=" + mealVoucherDeclarationId
				+ ", empMailId=" + empMailId + ", empLocation=" + empLocation
				+ "]";
	}

}
