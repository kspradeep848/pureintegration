/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  VoucherGiftDeclarationBean.java        		     */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :   20/12/2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the   */
/*                 Gift Voucher Declaration                          */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date           Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 20/12/2016    THBS      1.0        Initial version created        */
/*********************************************************************/
package com.thbs.mis.voucher.bean;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.customValidation.annotation.common.MIS_Id_Not_Found_Employee;
import com.thbs.mis.framework.customValidation.annotation.special.Valid_Is_Current_Year;
import com.thbs.mis.framework.customValidation.annotation.special.Valid_Voucher_Amount;
import com.thbs.mis.framework.customValidation.annotation.special.Valid_Voucher_Gift_Amount_Limit;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class VoucherGiftDeclarationBean {

	private Integer pkGiftVoucherDeclarationId;

	@Min(1)
	@NotNull(message = "fkGiftVoucherDeclarationEmpId field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "fkGiftVoucherDeclarationEmpId must be an Integer")
	@MIS_Id_Not_Found_Employee
	private Integer fkGiftVoucherDeclarationEmpId;

	private Date giftVoucherCreatedDate;

	@NotNull(message = "giftVoucherDeclarationAmount field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "giftVoucherDeclarationAmount must be an Integer")
	@Valid_Voucher_Amount(multiplyInto = 500, message = "giftVoucherDeclarationAmount must be in 500 Denomination")
	@Valid_Voucher_Gift_Amount_Limit
	private Integer giftVoucherDeclarationAmount;

	@NotNull(message = "giftVoucherDeclarationStatus field can not be null")
	@NotEmpty(message = "giftVoucherDeclarationStatus field can not be empty")
	@NotBlank(message = "giftVoucherDeclarationStatus should not be empty")
	private String giftVoucherDeclarationStatus;

	@NotNull(message = "giftVoucherDeclarationYear field can not be null")
	@NotEmpty(message = "giftVoucherDeclarationYear field can not be empty")
	@NotBlank(message = "giftVoucherDeclarationYear should not be empty")
	@Valid_Is_Current_Year(message = "giftVoucherDeclarationYear must be the Current Year")
	private String giftVoucherDeclarationYear;

	private Date giftVoucherModifiedDate;
	
	private String startDate;
	
	private String endDate;
	
	private Integer remainingDays;
	

	public VoucherGiftDeclarationBean() {
	}

	public Integer getPkGiftVoucherDeclarationId() {
		return this.pkGiftVoucherDeclarationId;
	}

	public void setPkGiftVoucherDeclarationId(Integer pkGiftVoucherDeclarationId) {
		this.pkGiftVoucherDeclarationId = pkGiftVoucherDeclarationId;
	}

	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getGiftVoucherCreatedDate() {
		return this.giftVoucherCreatedDate;
	}

	public void setGiftVoucherCreatedDate(Date giftVoucherCreatedDate) {
		this.giftVoucherCreatedDate = giftVoucherCreatedDate;
	}

	public Integer getGiftVoucherDeclarationAmount() {
		return this.giftVoucherDeclarationAmount;
	}

	public void setGiftVoucherDeclarationAmount(
			Integer giftVoucherDeclarationAmount) {
		this.giftVoucherDeclarationAmount = giftVoucherDeclarationAmount;
	}

	public String getGiftVoucherDeclarationStatus() {
		return this.giftVoucherDeclarationStatus;
	}

	public void setGiftVoucherDeclarationStatus(
			String giftVoucherDeclarationStatus) {
		this.giftVoucherDeclarationStatus = giftVoucherDeclarationStatus;
	}

	public String getGiftVoucherDeclarationYear() {
		return this.giftVoucherDeclarationYear;
	}

	public void setGiftVoucherDeclarationYear(String giftVoucherDeclarationYear) {
		this.giftVoucherDeclarationYear = giftVoucherDeclarationYear;
	}

	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getGiftVoucherModifiedDate() {
		return this.giftVoucherModifiedDate;
	}

	public void setGiftVoucherModifiedDate(Date giftVoucherModifiedDate) {
		this.giftVoucherModifiedDate = giftVoucherModifiedDate;
	}

	public Integer getFkGiftVoucherDeclarationEmpId() {
		return fkGiftVoucherDeclarationEmpId;
	}

	public void setFkGiftVoucherDeclarationEmpId(
			Integer fkGiftVoucherDeclarationEmpId) {
		this.fkGiftVoucherDeclarationEmpId = fkGiftVoucherDeclarationEmpId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Integer getRemainingDays() {
		return remainingDays;
	}

	public void setRemainingDays(Integer remainingDays) {
		this.remainingDays = remainingDays;
	}

	@Override
	public String toString() {
		return "VoucherGiftDeclarationBean [pkGiftVoucherDeclarationId="
				+ pkGiftVoucherDeclarationId
				+ ", fkGiftVoucherDeclarationEmpId="
				+ fkGiftVoucherDeclarationEmpId + ", giftVoucherCreatedDate="
				+ giftVoucherCreatedDate + ", giftVoucherDeclarationAmount="
				+ giftVoucherDeclarationAmount
				+ ", giftVoucherDeclarationStatus="
				+ giftVoucherDeclarationStatus
				+ ", giftVoucherDeclarationYear=" + giftVoucherDeclarationYear
				+ ", giftVoucherModifiedDate=" + giftVoucherModifiedDate
				+ ", startDate=" + startDate + ", endDate=" + endDate
				+ ", remainingDays=" + remainingDays + "]";
	}
}