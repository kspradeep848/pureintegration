/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  VoucherMealDeclarationBean.java    		         */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :   20/12/2016                                       */
/*                                                                   */
/*  Description :  This class contains the details methods for the   */
/*                 Meal Voucher Declaration                          */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date           Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 20/12/2016    THBS      1.0        Initial version created        */
/*********************************************************************/
package com.thbs.mis.voucher.bean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.customValidation.annotation.common.MIS_Id_Not_Found_Employee;
import com.thbs.mis.framework.customValidation.annotation.special.Valid_Voucher_Amount;
import com.thbs.mis.framework.customValidation.annotation.special.Valid_Voucher_Meal_Amount_Limit;
import com.thbs.mis.framework.util.CustomTimeSerializer;

public class VoucherMealDeclarationBean {

	private Integer pkMealVoucherDeclarationId;

	@Min(1)
	@NotNull(message = "fkMealVoucherDeclarationEmpId field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "fkMealVoucherDeclarationEmpId must be an Integer")
	@MIS_Id_Not_Found_Employee
	private Integer fkMealVoucherDeclarationEmpId;

	private Date mealVoucherCreatedDate;

	@NotNull(message = "mealVoucherDeclarationAmount field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "mealVoucherDeclarationAmount must be an Integer")
	@Valid_Voucher_Amount(multiplyInto = 500, message = "mealVoucherDeclarationAmount must be in 500 Denomination")
	@Valid_Voucher_Meal_Amount_Limit
	private Integer mealVoucherDeclarationAmount;

	@Min(1)
	@Max(12)
	@NotNull(message = "mealVoucherDeclarationMonth field can not be null")
	@NumberFormat(style = Style.NUMBER, pattern = "mealVoucherDeclarationMonth must be an Integer")
	private Integer mealVoucherDeclarationMonth;

	@NotNull(message = "mealVoucherDeclarationStatus field can not be null")
	@NotEmpty(message = "mealVoucherDeclarationStatus field can not be empty")
	@NotBlank(message = "mealVoucherDeclarationStatus should not be empty")
	private String mealVoucherDeclarationStatus;

	@NotNull(message = "mealVoucherDeclarationYear field can not be null")
	@NotEmpty(message = "mealVoucherDeclarationYear field can not be empty")
	@NotBlank(message = "mealVoucherDeclarationYear should not be empty")
	private String mealVoucherDeclarationYear;
	
	private Integer mealVoucherEligibilityAmount;

	private Date mealVoucherModifiedDate;

	private Integer remainingDays = 0;

	LocalDateTime currentTime = LocalDateTime.now();

	public Integer getRemainingDays() {
		LocalDate date1 = currentTime.toLocalDate();
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, currentTime.getYear());
		calendar.set(Calendar.MONTH, currentTime.getMonthValue());
		int numDays = calendar.getActualMaximum(Calendar.DATE);
		Integer currentDay = currentTime.getDayOfMonth();
		int lastDate = 22;
		if (currentDay <= lastDate) {
			this.remainingDays = lastDate - currentDay;
		} else {
			this.remainingDays = (numDays - currentDay) + lastDate;
		}
		return remainingDays + 1;
	}

	/*
	 * public void setRemainingDays(Integer remainingDays) { this.remainingDays
	 * = remainingDays; }
	 */

	public Integer getFkMealVoucherDeclarationEmpId() {
		// TODO Auto-generated method stub
		return this.fkMealVoucherDeclarationEmpId;
	}

	public void setFkMealVoucherDeclarationEmpId(
			Integer fkMealVoucherDeclarationEmpId) {
		this.fkMealVoucherDeclarationEmpId = fkMealVoucherDeclarationEmpId;
	}

	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getMealVoucherCreatedDate() {
		return mealVoucherCreatedDate;
	}

	public void setMealVoucherCreatedDate(Date mealVoucherCreatedDate) {
		this.mealVoucherCreatedDate = mealVoucherCreatedDate;
	}

	public Integer getMealVoucherDeclarationAmount() {
		return mealVoucherDeclarationAmount;
	}

	public void setMealVoucherDeclarationAmount(
			Integer mealVoucherDeclarationAmount) {
		this.mealVoucherDeclarationAmount = mealVoucherDeclarationAmount;
	}

	public Integer getMealVoucherDeclarationMonth() {
		return mealVoucherDeclarationMonth;
	}

	public void setMealVoucherDeclarationMonth(
			Integer mealVoucherDeclarationMonth) {
		this.mealVoucherDeclarationMonth = mealVoucherDeclarationMonth;
	}

	public String getMealVoucherDeclarationStatus() {
		return mealVoucherDeclarationStatus;
	}

	public void setMealVoucherDeclarationStatus(
			String mealVoucherDeclarationStatus) {
		this.mealVoucherDeclarationStatus = mealVoucherDeclarationStatus;
	}

	public String getMealVoucherDeclarationYear() {
		return mealVoucherDeclarationYear;
	}

	public void setMealVoucherDeclarationYear(String mealVoucherDeclarationYear) {
		this.mealVoucherDeclarationYear = mealVoucherDeclarationYear;
	}

	@JsonSerialize(using = CustomTimeSerializer.class)
	public Date getMealVoucherModifiedDate() {
		return mealVoucherModifiedDate;
	}

	public void setMealVoucherModifiedDate(Date mealVoucherModifiedDate) {
		this.mealVoucherModifiedDate = mealVoucherModifiedDate;
	}

	public Integer getPkMealVoucherDeclarationId() {
		return pkMealVoucherDeclarationId;
	}

	public void setPkMealVoucherDeclarationId(Integer pkMealVoucherDeclarationId) {
		this.pkMealVoucherDeclarationId = pkMealVoucherDeclarationId;
	}

	public Integer getMealVoucherEligibilityAmount() {
		return mealVoucherEligibilityAmount;
	}

	public void setMealVoucherEligibilityAmount(Integer mealVoucherEligibilityAmount) {
		this.mealVoucherEligibilityAmount = mealVoucherEligibilityAmount;
	}

	@Override
	public String toString() {
		return "VoucherMealDeclarationBean [pkMealVoucherDeclarationId="
				+ pkMealVoucherDeclarationId
				+ ", fkMealVoucherDeclarationEmpId="
				+ fkMealVoucherDeclarationEmpId + ", mealVoucherCreatedDate="
				+ mealVoucherCreatedDate + ", mealVoucherDeclarationAmount="
				+ mealVoucherDeclarationAmount
				+ ", mealVoucherDeclarationMonth="
				+ mealVoucherDeclarationMonth
				+ ", mealVoucherDeclarationStatus="
				+ mealVoucherDeclarationStatus
				+ ", mealVoucherDeclarationYear=" + mealVoucherDeclarationYear
				+ ", mealVoucherEligibilityAmount="
				+ mealVoucherEligibilityAmount + ", mealVoucherModifiedDate="
				+ mealVoucherModifiedDate + ", remainingDays=" + remainingDays
				+ ", currentTime=" + currentTime + "]";
	}

}