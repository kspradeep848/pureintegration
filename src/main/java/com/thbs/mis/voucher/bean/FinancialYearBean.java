package com.thbs.mis.voucher.bean;

import java.time.LocalDateTime;

public class FinancialYearBean {
	Integer fromYear;

	Integer toYear;

	Integer fromMonth;

	Integer toMonth;
	
	public FinancialYearBean(){
		LocalDateTime currentTime = LocalDateTime.now();
		Integer currentYear = currentTime.getYear();
		Integer currentMonthValue = currentTime.getMonthValue();
		if (currentMonthValue <= 3) {
			this.toYear = currentYear.intValue();
			this.fromYear = ((currentYear.intValue()) - 1);
			this.toMonth = 3;
			this.fromMonth = 4;
		} else if (currentMonthValue > 3 ) {
			this.toYear = ((currentYear.intValue()) + 1);
			this.fromYear = currentYear.intValue();
			this.toMonth = 3;
			this.fromMonth = 4;
		}
	}

	public Integer getFromYear() {
		return fromYear;
	}

	public Integer getToYear() {
		return toYear;
	}

	public Integer getFromMonth() {
		return fromMonth;
	}

	public Integer getToMonth() {
		return toMonth;
	}

	 

}
