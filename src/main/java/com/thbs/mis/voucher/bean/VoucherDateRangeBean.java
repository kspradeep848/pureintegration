package com.thbs.mis.voucher.bean;

import java.util.List;

public class VoucherDateRangeBean {
	List<Integer> months;
	String year;
		
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public List<Integer> getMonths() {
		return months;
	}
	public void setMonths(List<Integer> months) {
		this.months = months;
	}
	
	@Override
	public String toString() {
		return "Temp [year=" + year + ", months=" + months + "]";
	}
}
