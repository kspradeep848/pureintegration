package com.thbs.mis.voucher.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thbs.mis.framework.util.CustomDateSerializer;

public class MealVoucherReportBeanBasedOnRanges {

	Integer empId;
	String name;
	//String month;
	Integer Amount;
	String year;
	Date declaredOn;
	Date updatedOn;
	String emailId;
	String physicalLocation;
	Integer april;
	Integer may;
	Integer june;
	Integer july;
	Integer auguest;
	Integer september;
	Integer october;
	Integer november;
	Integer december;
	Integer january;
	Integer feburary;
	Integer march;
	Integer totalAmount;
	List <Integer> listOfPkVoucherId;
	
	public Integer getEmpId() {
		return empId;
	}
	public List<Integer> getListOfPkVoucherId() {
		return listOfPkVoucherId;
	}
	public void setListOfPkVoucherId(List<Integer> listOfPkVoucherId) {
		this.listOfPkVoucherId = listOfPkVoucherId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAmount() {
		return Amount;
	}
	public void setAmount(Integer amount) {
		Amount = amount;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getDeclaredOn() {
		return declaredOn;
	}
	public void setDeclaredOn(Date declaredOn) {
		this.declaredOn = declaredOn;
	}
	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPhysicalLocation() {
		return physicalLocation;
	}
	public void setPhysicalLocation(String physicalLocation) {
		this.physicalLocation = physicalLocation;
	}
	public Integer getApril() {
		return april;
	}
	public void setApril(Integer april) {
		this.april = april;
	}
	public Integer getMay() {
		return may;
	}
	public void setMay(Integer may) {
		this.may = may;
	}
	public Integer getJune() {
		return june;
	}
	public void setJune(Integer june) {
		this.june = june;
	}
	public Integer getJuly() {
		return july;
	}
	public void setJuly(Integer july) {
		this.july = july;
	}
	public Integer getAuguest() {
		return auguest;
	}
	public void setAuguest(Integer auguest) {
		this.auguest = auguest;
	}
	public Integer getSeptember() {
		return september;
	}
	public void setSeptember(Integer september) {
		this.september = september;
	}
	public Integer getOctober() {
		return october;
	}
	public void setOctober(Integer october) {
		this.october = october;
	}
	public Integer getNovember() {
		return november;
	}
	public void setNovember(Integer november) {
		this.november = november;
	}
	public Integer getDecember() {
		return december;
	}
	public void setDecember(Integer december) {
		this.december = december;
	}
	public Integer getJanuary() {
		return january;
	}
	public void setJanuary(Integer january) {
		this.january = january;
	}
	public Integer getFeburary() {
		return feburary;
	}
	public void setFeburary(Integer feburary) {
		this.feburary = feburary;
	}
	public Integer getMarch() {
		return march;
	}
	public void setMarch(Integer march) {
		this.march = march;
	}
	public Integer getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}
	@Override
	public String toString() {
		return "MealVoucherReportBeanBasedOnRanges [empId=" + empId + ", name="
				+ name + ", Amount=" + Amount + ", year=" + year
				+ ", declaredOn=" + declaredOn + ", updatedOn=" + updatedOn
				+ ", emailId=" + emailId + ", physicalLocation="
				+ physicalLocation + ", april=" + april + ", may=" + may
				+ ", june=" + june + ", july=" + july + ", auguest=" + auguest
				+ ", september=" + september + ", october=" + october
				+ ", november=" + november + ", december=" + december
				+ ", january=" + january + ", feburary=" + feburary
				+ ", march=" + march + ", totalAmount=" + totalAmount
				+ ", listOfPkVoucherId=" + listOfPkVoucherId + "]";
	}	
}
