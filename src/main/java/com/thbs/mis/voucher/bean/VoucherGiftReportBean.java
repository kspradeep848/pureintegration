package com.thbs.mis.voucher.bean;

import java.util.Date;

public class VoucherGiftReportBean {

	Integer empId;
	String name;
	Integer Amount;
	Date declaredOn;
	Date updatedOn;
	String emailId;
	String physicalLocation;

	public Integer getEmpId() {
		return empId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhysicalLocation() {
		return physicalLocation;
	}

	public void setPhysicalLocation(String physicalLocation) {
		this.physicalLocation = physicalLocation;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAmount() {
		return Amount;
	}

	public void setAmount(Integer amount) {
		Amount = amount;
	}

	public Date getDeclaredOn() {
		return declaredOn;
	}

	public void setDeclaredOn(Date declaredOn) {
		this.declaredOn = declaredOn;
	}

	public Date getUpdatedOn() { 
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

}
