package com.thbs.mis.voucher.bean;

import java.util.List;

public class VoucherListOfVoucherIdReportBean {

	List <Integer> listOfPkVoucherId;

	public List<Integer> getListOfPkVoucherId() {
		return listOfPkVoucherId;
	}

	public void setListOfPkVoucherId(List<Integer> listOfPkVoucherId) {
		this.listOfPkVoucherId = listOfPkVoucherId;
	}

	@Override
	public String toString() {
		return "VoucherListOfVoucherIdReportBean [listOfPkVoucherId="
				+ listOfPkVoucherId + "]";
	}
	
}
