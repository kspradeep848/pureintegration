/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  VoucherGiftDeclarationRepository.java     		 */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :   20/12/2016                                       */
/*                                                                   */
/*  Description :  This class contains all the methods		         */
/*                 for the VoucherGiftDeclarationRepository  		 */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date           Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 20/12/2016    THBS      1.0        Initial version created        */
/*********************************************************************/
package com.thbs.mis.voucher.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.voucher.bo.VoucherGiftDeclarationBO;

@Repository
public interface VoucherGiftDeclarationRepository extends
		GenericRepository<VoucherGiftDeclarationBO, Long> {

	public VoucherGiftDeclarationBO findByGiftVoucherDeclarationAmountAndGiftVoucherDeclarationYear(
			Integer giftVoucherDeclarationAmount,
			String giftVoucherDeclarationYear);

	public List<VoucherGiftDeclarationBO> findAll();

	public VoucherGiftDeclarationBO findByPkGiftVoucherDeclarationId(
			Integer pkGiftVoucherDeclarationId);

	public VoucherGiftDeclarationBO findByFkGiftVoucherDeclarationEmpIdAndGiftVoucherDeclarationYear(
			Integer fkGiftVoucherDeclarationEmpId,
			String giftVoucherDeclarationYear);

	public List<VoucherGiftDeclarationBO> findByGiftVoucherDeclarationYearOrderByFkGiftVoucherDeclarationEmpId(
			String giftVoucherDeclarationYear);

	public VoucherGiftDeclarationBO findByFkGiftVoucherDeclarationEmpIdAndGiftVoucherDeclarationYearAndGiftVoucherDeclarationStatus(
			Integer fkGiftVoucherDeclarationEmpId,
			String giftVoucherDeclarationYear,
			String GiftVoucherDeclarationStatus);
}
