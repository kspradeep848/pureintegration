/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  VoucherMealDeclarationRepository.java             */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :   20/12/2016                                       */
/*                                                                   */
/*  Description :  This class contains all the methods		         */
/*                 for the DatEmpMealVoucherDeclarationRepository    */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date           Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 20/12/2016    THBS      1.0        Initial version created        */
/*********************************************************************/
package com.thbs.mis.voucher.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.voucher.bo.VoucherMealDeclarationBO;

@Repository
public interface VoucherMealDeclarationRepository extends
		GenericRepository<VoucherMealDeclarationBO, Long> {

	public List<VoucherMealDeclarationBO> findAll();

	public VoucherMealDeclarationBO findByPkMealVoucherDeclarationId(
			Integer pkMealVoucherDeclarationId); // getMealVoucherDetailsById

	public VoucherMealDeclarationBO findByFkMealVoucherDeclarationEmpIdAndMealVoucherDeclarationMonthAndMealVoucherDeclarationYear(
			Integer fkMealVoucherDeclarationEmpId,
			Integer mealVoucherDeclarationMonth,
			String mealVoucherDeclarationYear); // getMealVochersByEmpIdAndMonth

	public List<VoucherMealDeclarationBO> findByMealVoucherDeclarationYearAndMealVoucherDeclarationMonth(
			String mealVoucherDeclarationYear,
			Integer mealVoucherDeclarationMonth); // generateMealVoucherReport

	public List<VoucherMealDeclarationBO> findByFkMealVoucherDeclarationEmpIdAndMealVoucherDeclarationMonthInAndMealVoucherDeclarationYearOrderByMealVoucherModifiedDateDesc(
			Integer fkMealVoucherDeclarationEmpId,
			List<Integer> mealVoucherDeclarationMonth,
			String mealVoucherDeclarationYear); // getVoucherIds
	
	public List<VoucherMealDeclarationBO> findByPkMealVoucherDeclarationIdInOrderByMealVoucherModifiedDateDesc(
			List<Integer> pkMealVoucherDeclarationId); // getMealVoucherClaims
	
	public List<VoucherMealDeclarationBO> findByMealVoucherDeclarationYearAndMealVoucherDeclarationMonthAndMealVoucherDeclarationStatusOrderByFkMealVoucherDeclarationEmpId(String mealVoucherDeclarationYear,
			Integer mealVoucherDeclarationMonth,String declarationStatus);
	
	@Query(name = "VoucherMealDeclarationBO.getFinancialYearReport")
	public List<VoucherMealDeclarationBO> getFinancialYearReport(
			@Param("voucherStartMonth") Integer startMonth,
			@Param("voucherStartYear") String startYear,
			@Param("voucherEndMonth") Integer endMonth,
			@Param("voucherEndYear") String endYear );
	
	public List<VoucherMealDeclarationBO> findByMealVoucherDeclarationYearAndMealVoucherDeclarationMonthInAndMealVoucherDeclarationStatusOrderByFkMealVoucherDeclarationEmpIdAscMealVoucherModifiedDateAsc(String mealVoucherDeclarationYear,
			List<Integer> mealVoucherDeclarationMonth,String declarationStatus);

}
