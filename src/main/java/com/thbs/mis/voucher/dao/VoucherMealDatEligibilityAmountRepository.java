package com.thbs.mis.voucher.dao;

import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.voucher.bo.VoucherMealDatEligibilityAmountBO;

@Repository
public interface VoucherMealDatEligibilityAmountRepository extends
GenericRepository<VoucherMealDatEligibilityAmountBO, Long>{

	public VoucherMealDatEligibilityAmountBO findByFkVoucherEligibilityEmpId(Integer empId);
	
}
