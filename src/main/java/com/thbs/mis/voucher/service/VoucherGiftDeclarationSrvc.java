/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  VoucherGiftDeclarationServc.java                  */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :   20/12/2016                                       */
/*                                                                   */
/*  Description :  All the Gift Voucher services are done here       */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date           Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 20/12/2016    THBS      1.0        Initial version created        */
/*********************************************************************/
package com.thbs.mis.voucher.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.common.dao.PhysicalLocationRepository;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.report.Util.DownloadFileUtil;
import com.thbs.mis.voucher.bean.MealVoucherDeclarationBean;
import com.thbs.mis.voucher.bean.VoucherGetDatesBean;
import com.thbs.mis.voucher.bean.VoucherGiftDeclarationBean;
import com.thbs.mis.voucher.bean.VoucherGiftReportBean;
import com.thbs.mis.voucher.bo.VoucherGiftDeclarationBO;
import com.thbs.mis.voucher.bo.VoucherMealDatEligibilityAmountBO;
import com.thbs.mis.voucher.dao.VoucherGiftDeclarationRepository;
import com.thbs.mis.voucher.dao.VoucherMealDatEligibilityAmountRepository;

@Service
@Configuration
@PropertySource("classpath:mis_voucher.properties")
public class VoucherGiftDeclarationSrvc {
	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(VoucherGiftDeclarationSrvc.class);

	@Autowired(required = true)
	private VoucherGiftDeclarationRepository voucherGiftDeclarationRepository;

	@Autowired(required = true)
	private EmployeeProfessionalRepository employeeProfessionalRepository;

	@Autowired(required = true)
	EmployeePersonalDetailsRepository employeePersonalDetailsRepository;
	
	@Autowired(required = true)
	private VoucherMealDatEligibilityAmountRepository voucherMealDatEligibilityAmountRepository;

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Autowired
	private ServletContext servletContext;

	@Value("${voucher.gift.start.date.month}")
	String startDate;

	@Value("${voucher.gift.end.date.month}")
	String endDate;

	/**
	 * 
	 * <Description submitGiftVoucherClaims:> method is call the repository
	 * method to submit the Gift Voucher Claims data in the data base. It will
	 * take the BO and passes it to the Repository for update the Record.
	 * 
	 * @author THBS
	 * 
	 * @param VoucherGiftDeclarationBO
	 *            is the argument for this method.
	 * 
	 * @return return the list of data in VoucherGiftDeclarationBO according to
	 *         the input details.
	 * 
	 * @throws BusinessException
	 *             throws if any exception occurs during the data retrieval.
	 * 
	 */
	public List<VoucherGiftDeclarationBean> submitGiftVoucherClaims(
			VoucherGiftDeclarationBean bean) throws BusinessException,
			CommonCustomException {
		List<VoucherGiftDeclarationBean> outputList = new ArrayList<VoucherGiftDeclarationBean>();
		LOG.startUsecase("submit Gift Voucher Claims Service");

		VoucherGiftDeclarationBO dataExists = getGiftVochersByEmpIdAndYear(
				bean.getFkGiftVoucherDeclarationEmpId(),
				bean.getGiftVoucherDeclarationYear());

		if (dataExists != null) {
			if (dataExists.getGiftVoucherDeclarationStatus().equalsIgnoreCase(
					bean.getGiftVoucherDeclarationStatus())) {
				/*if (dataExists.getGiftVoucherDeclarationAmount().intValue() == bean
						.getGiftVoucherDeclarationAmount().intValue()) {
					throw new CommonCustomException(
							"Already entry exists  for the Current  year "
									+ bean.getGiftVoucherDeclarationYear());
				}*/
				if (bean.getPkGiftVoucherDeclarationId() != null) {
					if (bean.getPkGiftVoucherDeclarationId().intValue() != dataExists
							.getPkGiftVoucherDeclarationId().intValue()) {
						throw new CommonCustomException(
								"Already entry exits in database GiftVoucherId is Mismatch from input");
					}
				} else {
					throw new CommonCustomException(
							"Already Gift voucher entry exists for the Current year "
									+ bean.getGiftVoucherDeclarationYear());
				}
			} else {
				if (bean.getPkGiftVoucherDeclarationId() == null) {
					throw new CommonCustomException(
							"Already entry exits in database GiftVoucherId is Mismatch from input");
				}
				if (bean.getPkGiftVoucherDeclarationId().intValue() != dataExists
						.getPkGiftVoucherDeclarationId().intValue()) {
					throw new CommonCustomException(
							"Already entry exits in database GiftVoucherId is Mismatch from input");
				}
			}
		}
		if (getEmployeeLocation(bean.getFkGiftVoucherDeclarationEmpId()) != 2) {
			throw new CommonCustomException("The employee id "
					+ bean.getFkGiftVoucherDeclarationEmpId()
					+ " is not in India Location");
		} else {
			VoucherGiftDeclarationBO submitGiftVoucherClaims = new VoucherGiftDeclarationBO();
			submitGiftVoucherClaims.setPkGiftVoucherDeclarationId(bean
					.getPkGiftVoucherDeclarationId());
			submitGiftVoucherClaims.setGiftVoucherCreatedDate(new Date());
			submitGiftVoucherClaims.setFkGiftVoucherDeclarationEmpId(bean
					.getFkGiftVoucherDeclarationEmpId());
			if (bean.getGiftVoucherDeclarationStatus().equalsIgnoreCase(
					"REQUESTED")) {
				/*if (bean.getGiftVoucherDeclarationAmount().intValue() < 1) {
					throw new CommonCustomException(
							"Amount must be greater than 0 and Denominations of 500");
				}*/
				//else {
					submitGiftVoucherClaims
							.setGiftVoucherDeclarationAmount(bean
									.getGiftVoucherDeclarationAmount());
				//}
			} else {
				submitGiftVoucherClaims.setGiftVoucherDeclarationAmount(0);
			}

			submitGiftVoucherClaims.setGiftVoucherDeclarationYear(bean
					.getGiftVoucherDeclarationYear());
			if (bean.getPkGiftVoucherDeclarationId() != null) {
				if (bean.getGiftVoucherDeclarationStatus() == null) {
					throw new CommonCustomException("Status is Missing");
				} else if (!(bean.getGiftVoucherDeclarationStatus()
						.equalsIgnoreCase("REQUESTED") || bean
						.getGiftVoucherDeclarationStatus().equalsIgnoreCase(
								"CANCELLED"))) {
					throw new CommonCustomException(
							"Status must be Requested or Cancelled");
				} else {
					submitGiftVoucherClaims
							.setGiftVoucherDeclarationStatus(bean
									.getGiftVoucherDeclarationStatus());
				}
				if (getGiftVoucherDetailsById(bean
						.getPkGiftVoucherDeclarationId()) == null) {
					throw new CommonCustomException("Invalid voucher id");
				} else {
					submitGiftVoucherClaims
							.setGiftVoucherCreatedDate(getGiftVoucherDetailsById(
									bean.getPkGiftVoucherDeclarationId())
									.getGiftVoucherCreatedDate());
				}
				submitGiftVoucherClaims.setGiftVoucherModifiedDate(new Date());
			} else {
				submitGiftVoucherClaims.setGiftVoucherCreatedDate(new Date());
				submitGiftVoucherClaims
						.setGiftVoucherModifiedDate(submitGiftVoucherClaims
								.getGiftVoucherCreatedDate());
				if (bean.getGiftVoucherDeclarationStatus().equalsIgnoreCase(
						"REQUESTED")) {
					submitGiftVoucherClaims
							.setGiftVoucherDeclarationStatus(bean
									.getGiftVoucherDeclarationStatus());
				} else {
					throw new CommonCustomException(
							"Applicable only for REQUESTED status");
				}
			}
			if (bean.getGiftVoucherModifiedDate() != null) {
				submitGiftVoucherClaims.setGiftVoucherModifiedDate(new Date());
			}
			Long range = 0l;
			if (this.startDate != null && this.endDate != null) {
				try {
					LocalDate endDate = LocalDate.parse(this.endDate);
					LocalDate startDate = LocalDate.parse(this.startDate);
					LocalDate currentDate = LocalDate.now();
					if (currentDate.getMonth() == startDate.getMonth()
							|| currentDate.getMonth() == endDate.getMonth()) {
						range = ChronoUnit.DAYS.between(currentDate, endDate);
					}
				} catch (DateTimeParseException e) {
					throw new CommonCustomException(
							"Check the voucher property file, Date Format should be like YYYY-MM-DD");
				}
			}
			submitGiftVoucherClaims = voucherGiftDeclarationRepository
					.save(submitGiftVoucherClaims);

			VoucherGiftDeclarationBean outputBean = new VoucherGiftDeclarationBean();
			outputBean.setFkGiftVoucherDeclarationEmpId(submitGiftVoucherClaims
					.getFkGiftVoucherDeclarationEmpId());
			outputBean.setGiftVoucherCreatedDate(submitGiftVoucherClaims
					.getGiftVoucherCreatedDate());
			outputBean.setGiftVoucherDeclarationAmount(submitGiftVoucherClaims
					.getGiftVoucherDeclarationAmount());
			outputBean.setGiftVoucherDeclarationStatus(submitGiftVoucherClaims
					.getGiftVoucherDeclarationStatus());
			outputBean.setGiftVoucherModifiedDate(submitGiftVoucherClaims
					.getGiftVoucherModifiedDate());
			outputBean.setPkGiftVoucherDeclarationId(submitGiftVoucherClaims
					.getPkGiftVoucherDeclarationId());
			outputBean.setGiftVoucherDeclarationYear(submitGiftVoucherClaims
					.getGiftVoucherDeclarationYear());
			outputBean.setStartDate(this.startDate);
			outputBean.setEndDate(this.endDate);
			outputBean.setRemainingDays(Integer.parseInt(range.toString()));
			outputList.add(outputBean);
		}
		LOG.endUsecase("submit Gift Voucher Claims Service");
		return outputList;
	}

	int getEmployeeLocationId(int empId) {
		LOG.startUsecase("Getting into getEmployeeLocationId");
		DatEmpProfessionalDetailBO profDetail = employeeProfessionalRepository
				.findByFkMainEmpDetailId(empId);
		LOG.endUsecase("Exit from getEmployeeLocationId");
		return profDetail.getFkEmpLocationPhysicalId();
	}
	
	int getEmployeeLocation(Integer empId) {
		LOG.startUsecase("Getting into getEmployeeLocationId");
		DatEmpProfessionalDetailBO profDetail = employeeProfessionalRepository
				.findByFkMainEmpDetailId(empId);
		LOG.endUsecase("Exit from getEmployeeLocationId");
		return profDetail.getFkEmpOrganizationId();
	}

	VoucherGiftDeclarationBO getGiftVoucherDetailsById(int voucherId) {
		LOG.startUsecase("Getting into getGiftVoucherDetailsById");
		VoucherGiftDeclarationBO voucherGiftDeclarationBO = new VoucherGiftDeclarationBO();
		voucherGiftDeclarationBO = voucherGiftDeclarationRepository
				.findByPkGiftVoucherDeclarationId(voucherId);
		LOG.endUsecase("Exit from getGiftVoucherDetailsById");
		return voucherGiftDeclarationBO;
	}

	public VoucherGiftDeclarationBO getGiftVochersByEmpIdAndYear(Integer empId,
			String year) throws CommonCustomException {
		LOG.startUsecase("Getting into getGiftVochersByEmpIdAndYear");
		VoucherGiftDeclarationBO voucherGiftDeclarationBO = new VoucherGiftDeclarationBO();
		try {
			voucherGiftDeclarationBO = voucherGiftDeclarationRepository
					.findByFkGiftVoucherDeclarationEmpIdAndGiftVoucherDeclarationYear(
							empId, year);
		} catch (IncorrectResultSizeDataAccessException e) {
			throw new CommonCustomException(
					"Duplicate Data are present in Database.Please contact to administartator");
		}
		LOG.endUsecase("Exit from getGiftVochersByEmpIdAndYear");
		return voucherGiftDeclarationBO;
	}

	public boolean generateGiftVoucherReport(String giftVoucherDeclarationYear,
			HttpServletResponse response) throws CommonCustomException {
		LOG.startUsecase("Entered generateGiftVoucherReport");
		boolean isReportGenerated = false;
		List<VoucherGiftDeclarationBO> giftAllVoucherDetails = new ArrayList<VoucherGiftDeclarationBO>();
		giftAllVoucherDetails = voucherGiftDeclarationRepository
				.findByGiftVoucherDeclarationYearOrderByFkGiftVoucherDeclarationEmpId(giftVoucherDeclarationYear);
		List<VoucherGiftReportBean> giftReport = new ArrayList<VoucherGiftReportBean>();
		Map beans = new HashMap();
		XLSTransformer transformer = new XLSTransformer();
		if (giftAllVoucherDetails.isEmpty()) {
			isReportGenerated = false;
			throw new CommonCustomException("Report data contains empty");

		} else {
			for (VoucherGiftDeclarationBO dataBo : giftAllVoucherDetails) {
				if (dataBo.getGiftVoucherDeclarationStatus().equalsIgnoreCase(
						"REQUESTED")) {
					VoucherGiftReportBean reportbean = new VoucherGiftReportBean();
					reportbean.setAmount(dataBo
							.getGiftVoucherDeclarationAmount());
					reportbean
							.setDeclaredOn(dataBo.getGiftVoucherCreatedDate());
					if (dataBo.getGiftVoucherModifiedDate() == null) {
						reportbean.setUpdatedOn(dataBo
								.getGiftVoucherCreatedDate());
					} else {
						reportbean.setUpdatedOn(dataBo
								.getGiftVoucherModifiedDate());
					}
					reportbean.setEmpId(dataBo
							.getFkGiftVoucherDeclarationEmpId());
					int empId = dataBo.getFkGiftVoucherDeclarationEmpId();
					reportbean.setName(getEmployeeName(dataBo
							.getFkGiftVoucherDeclarationEmpId()));
					reportbean.setEmailId(getEmployeeProfessionalDetails(empId)
							.getEmpProfessionalEmailId());
					reportbean
							.setPhysicalLocation(getEmployeePhysicalLocation(empId));
					giftReport.add(reportbean);
				}
			}
			if (giftReport.isEmpty()) {
				throw new CommonCustomException("Report data contains empty");
			} else {

				String BASEPATH = servletContext
						.getRealPath("/WEB-INF/classes/com/thbs/mis/framework/report/template/");
				String createdOn = sdf.format(new Date());
				String outputFileName = "Gift_Voucher_Report_For_Year_Of_"
						+ giftVoucherDeclarationYear + ".xls";
				try {
					beans.put("gift", giftReport);
					beans.put("createdOn", createdOn);
					boolean reportGeneratedDone = DownloadFileUtil
							.IsExcepReportCreated(BASEPATH, beans,
									DownloadFileUtil.GIFT_VOUCHER_REPORT,
									outputFileName);
					if (reportGeneratedDone) {
						DownloadFileUtil.downloadFile(response, BASEPATH,
								outputFileName);
						isReportGenerated = true;
						DownloadFileUtil.deleteFile(BASEPATH, outputFileName);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		LOG.endUsecase("Exited generateGiftVoucherReport");
		return isReportGenerated;
	}

	String getEmployeeName(Integer empId) {
		LOG.startUsecase("Entered getEmployeeName");
		DatEmpPersonalDetailBO empData = employeePersonalDetailsRepository
				.findByFkEmpDetailId(empId);
		LOG.endUsecase("Exited getEmployeeName");
		return empData.getEmpFirstName() + " " + empData.getEmpLastName();
	}

	public static String theMonth(int month) {
		LOG.startUsecase("Entered theMonth");
		String[] monthNames = { "Default", "January", "February", "March",
				"April", "May", "June", "July", "August", "September",
				"October", "November", "December" };
		LOG.endUsecase("Exited theMonth");
		return monthNames[month];
	}

	@Autowired
	PhysicalLocationRepository physicalLocationRepository;

	String getEmployeePhysicalLocation(int empId) {
		short locationId = (short) getEmployeeLocationId(empId);
		return physicalLocationRepository
				.findByPkLocationPhysicalId(locationId)
				.getLocationPhysicalName();
	}

	DatEmpProfessionalDetailBO getEmployeeProfessionalDetails(int empId) {
		LOG.startUsecase("Getting into getEmployeeLocationId");
		DatEmpProfessionalDetailBO profDetail = employeeProfessionalRepository
				.findByFkMainEmpDetailId(empId);
		LOG.endUsecase("Exit from getEmployeeLocationId");
		return profDetail;
	}

	/**
	 * 
	 * <Description giftVoucherDates:> method is call the repository method to
	 * submit the Gift Voucher date data in the data base. It will take the BO
	 * and passes it to the Repository for update the Record.
	 * 
	 * @author THBS
	 * 
	 * @param VoucherGiftDeclarationBO
	 *            is the argument for this method.
	 * 
	 * @return return the list of data in VoucherGiftDeclarationBO according to
	 *         the input details.
	 * @throws DataAccessException 
	 * 
	 * @throws BusinessException
	 *             throws if any exception occurs during the data retrieval.
	 * 
	 */
	public VoucherGetDatesBean giftVoucherDates(Integer fkMealVoucherDeclarationEmpId) throws CommonCustomException, DataAccessException {
		LOG.startUsecase("start of gift dates");
		VoucherGetDatesBean datesBean = new VoucherGetDatesBean();
		Long range = 0l;
		LocalDate endDate = LocalDate.parse(this.endDate);
		LocalDate startDate = LocalDate.parse(this.startDate);
		LocalDate currentDate = LocalDate.now();
		if (currentDate.getMonth() == startDate.getMonth()
				|| currentDate.getMonth() == endDate.getMonth()) {
			range = ChronoUnit.DAYS.between(currentDate, endDate);
		}
		datesBean.setGiftVoucherStartDate(this.startDate);
		datesBean.setGiftVoucherEndDate(this.endDate);
		datesBean
				.setGiftVoucherRemainingDays(Integer.parseInt(range.toString()));
		//Amount eligibility
		VoucherMealDatEligibilityAmountBO voucherMealDatEligibilityAmountBO = new VoucherMealDatEligibilityAmountBO();
		try {
			voucherMealDatEligibilityAmountBO = voucherMealDatEligibilityAmountRepository
					.findByFkVoucherEligibilityEmpId(fkMealVoucherDeclarationEmpId);
		} catch (Exception e) {
			throw new DataAccessException("Error in DB while fetching Eligibility amount");
		}
		if(voucherMealDatEligibilityAmountBO == null){
			throw new CommonCustomException("Check the voucher Eligible amount value is available in DB for the EmpID");
		}
		datesBean.setMealVoucherEligibilityAmount(voucherMealDatEligibilityAmountBO.getEligibilityAmount());
		LOG.endUsecase("End of gift dates");
		return datesBean;
	}
	
	public List<MealVoucherDeclarationBean> getGiftVoucherReport(String giftVoucherDeclarationYear) throws CommonCustomException, DataAccessException {
		LOG.startUsecase("Entered getGiftVoucherReport");
		List<VoucherGiftDeclarationBO> giftAllVoucherDetails = new ArrayList<VoucherGiftDeclarationBO>();
		try {
			giftAllVoucherDetails = voucherGiftDeclarationRepository
					.findByGiftVoucherDeclarationYearOrderByFkGiftVoucherDeclarationEmpId(giftVoucherDeclarationYear);
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
		List<MealVoucherDeclarationBean> giftReport = new ArrayList<MealVoucherDeclarationBean>();
		if (giftAllVoucherDetails.isEmpty()) {
			throw new CommonCustomException("Gift Voucher data is empty");

		} else {
			for (VoucherGiftDeclarationBO dataBo : giftAllVoucherDetails) {
				if (dataBo.getGiftVoucherDeclarationStatus().equalsIgnoreCase(
						"REQUESTED")) {
					MealVoucherDeclarationBean reportbean = new MealVoucherDeclarationBean();
					reportbean.setMealVoucherDeclarationId(dataBo.getPkGiftVoucherDeclarationId());
					reportbean.setEmpMailId(getEmployeeProfessionalDetails(dataBo.getFkGiftVoucherDeclarationEmpId()).getEmpProfessionalEmailId());
					reportbean.setEmpLocation(getEmployeeProfessionalDetails(dataBo.getFkGiftVoucherDeclarationEmpId()).getMasLocationPhysicalBO().getLocationPhysicalName());
					reportbean.setVoucherDeclarationAmt(dataBo.getGiftVoucherDeclarationAmount()+ "/-");
					reportbean.setEmpBu(getEmpBuByEmpId(dataBo.getFkGiftVoucherDeclarationEmpId()));
					reportbean.setEmpId(dataBo.getFkGiftVoucherDeclarationEmpId());
					reportbean.setEmpName(getEmployeeName(dataBo.getFkGiftVoucherDeclarationEmpId()));
					reportbean.setVoucherDeclarationDate(dataBo.getGiftVoucherModifiedDate());
					reportbean.setMonth(Integer.parseInt(dataBo.getGiftVoucherDeclarationYear()));
					giftReport.add(reportbean);
				}
			}
		}
		LOG.endUsecase("Exited getGiftVoucherReport");
		return giftReport;
	}
	
	/**
	 * 
	 * @param empId
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to get Employee BU Name based on
	 *              Employee Id
	 */
	public String getEmpBuByEmpId(int empId) throws DataAccessException {
		LOG.startUsecase("Entering getEmpBuByEmpId");
		DatEmpProfessionalDetailBO profDetails = null;
		try {
			profDetails = employeeProfessionalRepository
					.findByFkMainEmpDetailId(empId);

			if (profDetails != null) {
				return profDetails.getMasBuUnitBO().getBuUnitName();
			} else
				throw new DataAccessException(
						"Some error happened while getting employee BU details");
		} catch (DataAccessException e) {
			LOG.endUsecase("Exiting getEmpBuByEmpId");
			throw new DataAccessException(e.getMessage());
		}

	}
/**
 * 
 * @param getGiftVoucherReportBySelectedVoucherId
 * @param response
 * @return
 * @throws ParsePropertyException
 * @throws InvalidFormatException
 * @throws IOException
 * @throws CommonCustomException
 */
	public boolean getGiftVoucherReportBySelectedVoucherId(
			List<VoucherGiftDeclarationBO> iniputBoLIst,
			HttpServletResponse response) throws ParsePropertyException,
			InvalidFormatException, IOException, CommonCustomException {
		LOG.startUsecase("getGiftVoucherReportBySelectedVoucherId");
		List<VoucherGiftDeclarationBO> getMealVoucherClaims = new ArrayList<VoucherGiftDeclarationBO>();
		VoucherGiftDeclarationBO data = new VoucherGiftDeclarationBO();
		if (iniputBoLIst.isEmpty()) {
			throw new CommonCustomException("Voucher id should not be null");
		} else {
			for (VoucherGiftDeclarationBO iniputBo : iniputBoLIst) {
				data = voucherGiftDeclarationRepository
						.findByPkGiftVoucherDeclarationId(iniputBo
								.getPkGiftVoucherDeclarationId());
				getMealVoucherClaims.add(data);
			}
		}
		if (getMealVoucherClaims.isEmpty()) {
			return false;
		} else {
			LocalDateTime currentTime = LocalDateTime.now();
			Integer currentYear = currentTime.getYear();
			Integer currentMonthValue = currentTime.getMonthValue();
			LOG.endUsecase("getGiftVoucherReportBySelectedVoucherId");
			return this.generateGiftVoucherSelectedReport(getMealVoucherClaims,response);
		}
	}
	
	public boolean generateGiftVoucherSelectedReport(List<VoucherGiftDeclarationBO> getMealVoucherClaims,
			HttpServletResponse response) throws CommonCustomException {
		LOG.startUsecase("Entered generateGiftVoucherReport");
		boolean isReportGenerated = false;
		List<VoucherGiftDeclarationBO> giftAllVoucherDetails = new ArrayList<VoucherGiftDeclarationBO>();
		List<VoucherGiftReportBean> giftReport = new ArrayList<VoucherGiftReportBean>();
		Map beans = new HashMap();
		XLSTransformer transformer = new XLSTransformer();
		if (getMealVoucherClaims.isEmpty()) {
			isReportGenerated = false;
			throw new CommonCustomException("Report data contains empty");
		} else {
			for (VoucherGiftDeclarationBO dataBo : getMealVoucherClaims) {
				if (dataBo.getGiftVoucherDeclarationStatus().equalsIgnoreCase(
						"REQUESTED")) {
					VoucherGiftReportBean reportbean = new VoucherGiftReportBean();
					reportbean.setAmount(dataBo
							.getGiftVoucherDeclarationAmount());
					reportbean
							.setDeclaredOn(dataBo.getGiftVoucherCreatedDate());
					if (dataBo.getGiftVoucherModifiedDate() == null) {
						reportbean.setUpdatedOn(dataBo
								.getGiftVoucherCreatedDate());
					} else {
						reportbean.setUpdatedOn(dataBo
								.getGiftVoucherModifiedDate());
					}
					reportbean.setEmpId(dataBo
							.getFkGiftVoucherDeclarationEmpId());
					int empId = dataBo.getFkGiftVoucherDeclarationEmpId();
					reportbean.setName(getEmployeeName(dataBo
							.getFkGiftVoucherDeclarationEmpId()));
					reportbean.setEmailId(getEmployeeProfessionalDetails(empId)
							.getEmpProfessionalEmailId());
					reportbean
							.setPhysicalLocation(getEmployeePhysicalLocation(empId));
					giftReport.add(reportbean);
				}
			}
			if (giftReport.isEmpty()) {
				throw new CommonCustomException("Report data contains empty");
			} else {
				String BASEPATH = servletContext
						.getRealPath("/WEB-INF/classes/com/thbs/mis/framework/report/template/");
				String createdOn = sdf.format(new Date());
				String outputFileName = "Gift_Voucher_Report_For_Year_Of_"+ ".xls";
				try {
					beans.put("gift", giftReport);
					beans.put("createdOn", createdOn);
					boolean reportGeneratedDone = DownloadFileUtil
							.IsExcepReportCreated(BASEPATH, beans,
									DownloadFileUtil.GIFT_VOUCHER_REPORT,
									outputFileName);
					if (reportGeneratedDone) {
						DownloadFileUtil.downloadFile(response, BASEPATH,
								outputFileName);
						isReportGenerated = true;
						DownloadFileUtil.deleteFile(BASEPATH, outputFileName);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		LOG.endUsecase("Exited generateGiftVoucherReport");
		return isReportGenerated;
	}
	
}
