/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  VoucherMealDeclarationSrvc.java                   */
/*                                                                   */
/*  Author      :  THBS                                                 */
/*                                                                   */
/*  Date        :   20/12/2016                                       */
/*                                                                   */
/*  Description :  All the Meal Voucher services are done here       */
/*                                                                      */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date           Who     Version      Comments                      */
/*-------------------------------------------------------------------*/
/* 20/12/2016    THBS      1.0        Initial version created        */
/*********************************************************************/

package com.thbs.mis.voucher.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jxls.exception.ParsePropertyException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.common.dao.PhysicalLocationRepository;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.report.Util.DownloadFileUtil;
import com.thbs.mis.framework.util.EntityBeanList;
import com.thbs.mis.voucher.bean.FinancialYearBean;
import com.thbs.mis.voucher.bean.MealVoucherDeclarationBean;
import com.thbs.mis.voucher.bean.MealVoucherReportBasedOnStartDate;
import com.thbs.mis.voucher.bean.MealVoucherReportBeanBasedOnRanges;
import com.thbs.mis.voucher.bean.VoucherDateRangeBean;
import com.thbs.mis.voucher.bean.VoucherListOfVoucherIdReportBean;
import com.thbs.mis.voucher.bean.VoucherMealDeclarationBean;
import com.thbs.mis.voucher.bean.VoucherMealReportBean;
import com.thbs.mis.voucher.bo.VoucherMealDatEligibilityAmountBO;
import com.thbs.mis.voucher.bo.VoucherMealDeclarationBO;
import com.thbs.mis.voucher.dao.VoucherMealDatEligibilityAmountRepository;
import com.thbs.mis.voucher.dao.VoucherMealDeclarationRepository;

@Service
public class VoucherMealDeclarationSrvc {
	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory.getLog(VoucherMealDeclarationSrvc.class);

	@Autowired(required = true)
	private VoucherMealDeclarationRepository voucherMealDeclarationRepository;

	@Autowired(required = true)
	private EmployeeProfessionalRepository employeeProfessionalRepository;

	@Autowired(required = true)
	EmployeePersonalDetailsRepository employeePersonalDetailsRepository;

	@Autowired(required = true)
	private VoucherMealDatEligibilityAmountRepository voucherMealDatEligibilityAmountRepository;

	@Autowired
	private ServletContext servletContext;

	/**
	 * 
	 * <Description submitMealVoucherClaims:> method is call the repository method
	 * to submit Meal Voucher Claims data in the data base. It will take the BO and
	 * passes it to the Repository for update the Record.
	 * 
	 * @author THBS
	 * 
	 * @param VoucherMealDeclarationBO
	 *            is the argument for this method.
	 * 
	 * @return return the list of data in VoucherMealDeclarationBO according to the
	 *         input details.
	 * 
	 * @throws BusinessException
	 *             throws if any exception occurs during the data retrieval.
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * 
	 */
	public List<VoucherMealDeclarationBean> submitMealVoucherClaims(EntityBeanList<VoucherMealDeclarationBean> bean,
			Integer currentMonthValue) throws BusinessException, CommonCustomException, DataAccessException {
		LOG.startUsecase("submit Meal Voucher Claims Service");
		List<VoucherMealDeclarationBean> outputList = new ArrayList<VoucherMealDeclarationBean>();
		boolean validateInput = validate(bean);
		if (validateInput) {
			for (VoucherMealDeclarationBean voucherMealDeclarationBean : bean.getEntityList()) {

				VoucherMealDeclarationBO submitMealVoucherClaims = new VoucherMealDeclarationBO();
				VoucherMealDeclarationBO voucherData = getMealVochersByEmpIdAndMonth(
						voucherMealDeclarationBean.getFkMealVoucherDeclarationEmpId(),
						voucherMealDeclarationBean.getMealVoucherDeclarationMonth(),
						voucherMealDeclarationBean.getMealVoucherDeclarationYear());
				submitMealVoucherClaims
						.setPkMealVoucherDeclarationId(voucherMealDeclarationBean.getPkMealVoucherDeclarationId());
				submitMealVoucherClaims.setFkMealVoucherDeclarationEmpId(
						voucherMealDeclarationBean.getFkMealVoucherDeclarationEmpId());
				// Amount eligibility
				VoucherMealDatEligibilityAmountBO voucherMealDatEligibilityAmountBO = new VoucherMealDatEligibilityAmountBO();
				try {
					voucherMealDatEligibilityAmountBO = voucherMealDatEligibilityAmountRepository
							.findByFkVoucherEligibilityEmpId(
									submitMealVoucherClaims.getFkMealVoucherDeclarationEmpId());
				} catch (Exception e) {
					throw new DataAccessException("Error in DB while fetching Eligibility amount");
				}
				if (voucherMealDeclarationBean.getMealVoucherDeclarationStatus().equalsIgnoreCase("REQUESTED")) {
					if (voucherMealDatEligibilityAmountBO == null) {
						throw new CommonCustomException(
								"Check the voucher Eligible amount value is available in DB for the EmpID");
					} else {
						if (voucherMealDeclarationBean.getMealVoucherDeclarationAmount()
								.compareTo(voucherMealDatEligibilityAmountBO.getEligibilityAmount()) <= 0) {
							submitMealVoucherClaims.setMealVoucherDeclarationAmount(
									voucherMealDeclarationBean.getMealVoucherDeclarationAmount());

						} else {
							throw new CommonCustomException("Amount exceeds the voucher Eligible amount");
						}
					}
				} else {
					submitMealVoucherClaims.setMealVoucherDeclarationAmount(0);
				}
				LocalDateTime currentTime = LocalDateTime.now();
				Integer currentMonth = currentTime.getMonthValue();
				Integer nextYear = currentTime.getYear() + 1;
				Integer lastYear = currentTime.getYear() - 1;
//				if (voucherMealDeclarationBean.getMealVoucherDeclarationMonth().compareTo(currentMonth) == 0
//						|| voucherMealDeclarationBean.getMealVoucherDeclarationMonth().compareTo(currentMonth) > 0) {
//					submitMealVoucherClaims.setMealVoucherDeclarationMonth(
//							voucherMealDeclarationBean.getMealVoucherDeclarationMonth());
//				} else if (voucherMealDeclarationBean.getMealVoucherDeclarationMonth().compareTo(currentMonth) < 0
//						&& Integer.valueOf(voucherMealDeclarationBean.getMealVoucherDeclarationYear())
//								.compareTo(nextYear) == 0) {
//					submitMealVoucherClaims.setMealVoucherDeclarationMonth(
//							voucherMealDeclarationBean.getMealVoucherDeclarationMonth());
//				} else if (voucherMealDeclarationBean.getMealVoucherDeclarationMonth().compareTo(currentMonth) < 0
//						&& Integer.valueOf(voucherMealDeclarationBean.getMealVoucherDeclarationYear())
//								.compareTo(lastYear) == 0) {
//					submitMealVoucherClaims.setMealVoucherDeclarationMonth(
//							voucherMealDeclarationBean.getMealVoucherDeclarationMonth());
//				}
//				else {
//					throw new CommonCustomException("Month Should be Current or Future Financial Month ");
//				}
				submitMealVoucherClaims.setMealVoucherDeclarationMonth(
						voucherMealDeclarationBean.getMealVoucherDeclarationMonth());
				submitMealVoucherClaims
						.setMealVoucherDeclarationYear(voucherMealDeclarationBean.getMealVoucherDeclarationYear());
				if (voucherMealDeclarationBean.getPkMealVoucherDeclarationId() != null) {
					// submitMealVoucherClaims.setPkMealVoucherDeclarationId(voucherData.getPkMealVoucherDeclarationId());
					submitMealVoucherClaims.setMealVoucherDeclarationStatus(
							voucherMealDeclarationBean.getMealVoucherDeclarationStatus());

					submitMealVoucherClaims.setMealVoucherCreatedDate(
							getMealVoucherDetailsById(voucherMealDeclarationBean.getPkMealVoucherDeclarationId())
									.getMealVoucherCreatedDate());

					submitMealVoucherClaims.setMealVoucherModifiedDate(new Date());
				} else {
					if (voucherData != null)
						if (voucherData.getPkMealVoucherDeclarationId() != null) {
							submitMealVoucherClaims
									.setPkMealVoucherDeclarationId(voucherData.getPkMealVoucherDeclarationId());
						}
					submitMealVoucherClaims.setMealVoucherCreatedDate(new Date());

					submitMealVoucherClaims
							.setMealVoucherModifiedDate(submitMealVoucherClaims.getMealVoucherCreatedDate());

					if (voucherMealDeclarationBean.getMealVoucherDeclarationStatus().equalsIgnoreCase("REQUESTED")) {
						submitMealVoucherClaims.setMealVoucherDeclarationStatus(
								voucherMealDeclarationBean.getMealVoucherDeclarationStatus());
					}

				}
				if (voucherMealDeclarationBean.getMealVoucherModifiedDate() != null) {
					submitMealVoucherClaims.setMealVoucherModifiedDate(new Date());
				}
				if (voucherMealDeclarationBean.getMealVoucherDeclarationAmount().equals(0)) {
					submitMealVoucherClaims.setMealVoucherDeclarationStatus("CANCELLED");
				}
				submitMealVoucherClaims = voucherMealDeclarationRepository.save(submitMealVoucherClaims);

				// Convert bo to bean
				VoucherMealDeclarationBean outputBean = new VoucherMealDeclarationBean();
				outputBean.setFkMealVoucherDeclarationEmpId(submitMealVoucherClaims.getFkMealVoucherDeclarationEmpId());
				outputBean.setMealVoucherCreatedDate(submitMealVoucherClaims.getMealVoucherCreatedDate());
				outputBean.setMealVoucherDeclarationAmount(submitMealVoucherClaims.getMealVoucherDeclarationAmount());
				outputBean.setMealVoucherDeclarationStatus(submitMealVoucherClaims.getMealVoucherDeclarationStatus());
				outputBean.setMealVoucherDeclarationMonth(submitMealVoucherClaims.getMealVoucherDeclarationMonth());
				outputBean.setMealVoucherModifiedDate(submitMealVoucherClaims.getMealVoucherModifiedDate());
				outputBean.setPkMealVoucherDeclarationId(submitMealVoucherClaims.getPkMealVoucherDeclarationId());
				outputBean.setMealVoucherDeclarationYear(submitMealVoucherClaims.getMealVoucherDeclarationYear());
				outputBean.setMealVoucherEligibilityAmount(voucherMealDatEligibilityAmountBO.getEligibilityAmount());
				outputList.add(outputBean);
			}
		}
		LOG.endUsecase("submit Meal Voucher Claims Service");
		return outputList;
	}

	/**
	 * 
	 * <Description getMealVoucherClaims:> method is call the repository method to
	 * get Meal Voucher Claim data in the data base. It will take the BO and passes
	 * it to the Repository for update the Record.
	 * 
	 * @author THBS
	 * 
	 * @param VoucherMealDeclarationBO
	 *            is the argument for this method.
	 * 
	 * @return return the list of data in VoucherMealDeclarationBO according to the
	 *         input details.
	 * 
	 * @throws BusinessException
	 *             throws if any exception occurs during the data retrieval.
	 * 
	 */
	public List<VoucherMealDeclarationBean> getMealVoucherClaims(Integer fkMealVoucherDeclarationEmpId)
			throws BusinessException {
		LOG.startUsecase("Get Meal Voucher Claims Service");
		List<VoucherMealDeclarationBean> getMealVoucherClaims = new ArrayList<VoucherMealDeclarationBean>();
		FinancialYearBean financialYearBean = new FinancialYearBean();
		Integer fromFinancialMonth = financialYearBean.getFromMonth();
		String fromFinancialYear = financialYearBean.getFromYear().toString();
		Integer toFinancialMonth = financialYearBean.getToMonth();
		String toFinancialYear = financialYearBean.getToYear().toString();
		List<Integer> voucherId = new ArrayList<Integer>();
		List<Integer> firstQuaterFinanceVoucherId = new ArrayList<Integer>();
		List<Integer> secondQuaterFinanceVoucherId = new ArrayList<Integer>();
		List<VoucherMealDeclarationBO> getMealVoucherClaimsList = new ArrayList<VoucherMealDeclarationBO>();
		VoucherMealDatEligibilityAmountBO voucherMealDatEligibilityAmountBO = new VoucherMealDatEligibilityAmountBO();
		try {
			try {
				List<Integer> selectedFirstQuaterMonth = new ArrayList<Integer>();
				for (int loop = fromFinancialMonth.intValue(); loop <= 12; loop++) {
					selectedFirstQuaterMonth.add(loop);
				}
				firstQuaterFinanceVoucherId = getVoucherIds(fkMealVoucherDeclarationEmpId, selectedFirstQuaterMonth,
						fromFinancialYear);
				List<Integer> selectedSecondQuaterMonth = new ArrayList<Integer>();
				for (int loop = toFinancialMonth.intValue(); loop > 0; loop--) {
					selectedSecondQuaterMonth.add(loop);
				}
				secondQuaterFinanceVoucherId = getVoucherIds(fkMealVoucherDeclarationEmpId, selectedSecondQuaterMonth,
						toFinancialYear);
				voucherId = mergerAllQuaterVoucherIds(firstQuaterFinanceVoucherId, secondQuaterFinanceVoucherId);
				try {
					voucherMealDatEligibilityAmountBO = voucherMealDatEligibilityAmountRepository
							.findByFkVoucherEligibilityEmpId(fkMealVoucherDeclarationEmpId);
				} catch (Exception e) {
					throw new DataAccessException("Error in DB while fetching Eligibility amount");
				}
				getMealVoucherClaimsList = voucherMealDeclarationRepository
						.findByPkMealVoucherDeclarationIdInOrderByMealVoucherModifiedDateDesc(voucherId);
				for (VoucherMealDeclarationBO voucherMealDeclarationBO : getMealVoucherClaimsList) {
					if (voucherMealDeclarationBO != null) {
						VoucherMealDeclarationBean voucherMealDeclarationBean = new VoucherMealDeclarationBean();
						voucherMealDeclarationBean.setFkMealVoucherDeclarationEmpId(
								voucherMealDeclarationBO.getFkMealVoucherDeclarationEmpId());
						voucherMealDeclarationBean
								.setMealVoucherCreatedDate(voucherMealDeclarationBO.getMealVoucherCreatedDate());
						voucherMealDeclarationBean.setMealVoucherDeclarationAmount(
								voucherMealDeclarationBO.getMealVoucherDeclarationAmount());
						voucherMealDeclarationBean.setMealVoucherDeclarationMonth(
								voucherMealDeclarationBO.getMealVoucherDeclarationMonth());
						voucherMealDeclarationBean.setMealVoucherDeclarationStatus(
								voucherMealDeclarationBO.getMealVoucherDeclarationStatus());
						voucherMealDeclarationBean.setMealVoucherDeclarationYear(
								voucherMealDeclarationBO.getMealVoucherDeclarationYear());
						voucherMealDeclarationBean.setPkMealVoucherDeclarationId(
								voucherMealDeclarationBO.getPkMealVoucherDeclarationId());
						voucherMealDeclarationBean
								.setMealVoucherModifiedDate(voucherMealDeclarationBO.getMealVoucherModifiedDate());
						voucherMealDeclarationBean.setMealVoucherEligibilityAmount(
								voucherMealDatEligibilityAmountBO.getEligibilityAmount());
						getMealVoucherClaims.add(voucherMealDeclarationBean);
					}
				}
			} catch (Exception e) {
				throw new BusinessException("exception ", e);
			}

		} catch (Exception e) {
			throw new BusinessException("exception ", e);
		}
		LOG.endUsecase("Get Meal Voucher Claims Service");
		return getMealVoucherClaims;
	}

	List<Integer> getVoucherIds(Integer fkMealVoucherDeclarationEmpId, List<Integer> selectedMonth, String year) {
		LOG.startUsecase("Getting into Get Voucher ID");
		List<Integer> voucherId = new ArrayList<Integer>();
		List<VoucherMealDeclarationBO> firstFinancialYearVoucherMealDeclarationList = new ArrayList<VoucherMealDeclarationBO>();
		firstFinancialYearVoucherMealDeclarationList = voucherMealDeclarationRepository
				.findByFkMealVoucherDeclarationEmpIdAndMealVoucherDeclarationMonthInAndMealVoucherDeclarationYearOrderByMealVoucherModifiedDateDesc(
						fkMealVoucherDeclarationEmpId, selectedMonth, year);
		for (VoucherMealDeclarationBO bo : firstFinancialYearVoucherMealDeclarationList) {
			voucherId.add(bo.getPkMealVoucherDeclarationId());
		}
		LOG.endUsecase("Exit from Get Voucher ID Service");
		return voucherId;
	}

	List<Integer> mergerAllQuaterVoucherIds(List<Integer> firstQuaterFinanceVoucherId,
			List<Integer> secondQuaterFinanceVoucherId) {
		LOG.startUsecase("Getting into Mearge All Quarter Voucher");
		List<Integer> allVoucherId = new ArrayList<Integer>();
		for (Integer id : firstQuaterFinanceVoucherId) {
			allVoucherId.add(id);
		}
		for (Integer id : secondQuaterFinanceVoucherId) {
			allVoucherId.add(id);
		}
		LOG.endUsecase("Exit from Mearge All Quarter Voucher");
		return allVoucherId;
	}

	public boolean validate(EntityBeanList<VoucherMealDeclarationBean> bean)
			throws CommonCustomException, DataAccessException {
		int CONSTANT_LOCATION_INDIA = 2;
		boolean validate = true;
		LOG.startUsecase("Getting into validate");
		for (VoucherMealDeclarationBean voucherMealDeclarationBean : bean.getEntityList()) {
			Integer empId = voucherMealDeclarationBean.getFkMealVoucherDeclarationEmpId();
			Integer amount = voucherMealDeclarationBean.getMealVoucherDeclarationAmount();
			String status = voucherMealDeclarationBean.getMealVoucherDeclarationStatus();
			Integer year = Integer.parseInt(voucherMealDeclarationBean.getMealVoucherDeclarationYear());
			Integer month = voucherMealDeclarationBean.getMealVoucherDeclarationMonth();
			Integer voucherId = voucherMealDeclarationBean.getPkMealVoucherDeclarationId();

			// Emp id validate
			if (empId == null || empId == 0) {
				validate = false;
				throw new CommonCustomException("Employee Id not found");
			} else if (getEmployeeLocationId(empId.intValue()) != CONSTANT_LOCATION_INDIA) {
				validate = false;
				throw new CommonCustomException("The Employee id "
						+ voucherMealDeclarationBean.getFkMealVoucherDeclarationEmpId() + " is not in India location");
			}

			// Year and month validate
			boolean isValidFinancialYear = isFinancialYearTrue(month, year);
			if (isValidFinancialYear == false) {
				validate = false;
				throw new CommonCustomException("It's Not a valid financial year month");
			}

			// Validate status
			if (voucherMealDeclarationBean.getMealVoucherDeclarationStatus() == null) {
				throw new CommonCustomException("Status is Missing");
			} else if (!(voucherMealDeclarationBean.getMealVoucherDeclarationStatus().equalsIgnoreCase("REQUESTED")
					|| voucherMealDeclarationBean.getMealVoucherDeclarationStatus().equalsIgnoreCase("CANCELLED"))) {
				throw new CommonCustomException("Status must be Requested or Cancelled");
			}
			/*
			 * if ((voucherMealDeclarationBean.getMealVoucherDeclarationStatus()
			 * .equalsIgnoreCase("REQUESTED"))) { if (amount.compareTo(1)<0) { throw new
			 * CommonCustomException( "Amount must be greater than 0 to submit voucher"); }
			 * }
			 */

			// validate declaration id;
			if (validate == true) {
				if (voucherMealDeclarationBean.getPkMealVoucherDeclarationId() == null) {

					/*
					 * VoucherMealDeclarationBO voucherData = getMealVochersByEmpIdAndMonth( empId,
					 * month, voucherMealDeclarationBean .getMealVoucherDeclarationYear());
					 */
					/*
					 * if (voucherData!=null) { validate = false; throw new CommonCustomException(
					 * "Voucher has been already created"); }
					 */
					if (voucherMealDeclarationBean.getMealVoucherDeclarationStatus().equalsIgnoreCase("REQUESTED")) {
					} else {
						throw new CommonCustomException("Applicable only for REQUESTED status");
					}
				} else {
					Integer empIdInDb = getMealVoucherDetailsById(voucherId).getFkMealVoucherDeclarationEmpId();
					if (!(empIdInDb.equals(empId.intValue()))) {
						throw new CommonCustomException(
								"Already this VoucherID used for " + empIdInDb + ". Please check in DataBase");
					}
					if (voucherId.intValue() < 1) {
						throw new CommonCustomException("Invalid voucher Id");
					}
					VoucherMealDeclarationBO voucherData = getMealVoucherDetailsById(
							voucherMealDeclarationBean.getPkMealVoucherDeclarationId());
					if (voucherData == null) {
						validate = false;
						throw new CommonCustomException("Invalid voucher id");
					} // else {
						// if (voucherMealDeclarationBean
						// .getMealVoucherDeclarationStatus()
						// .equalsIgnoreCase("REQUESTED")) {
						// if (voucherData.getMealVoucherDeclarationAmount()
						// .intValue() == voucherMealDeclarationBean
						// .getMealVoucherDeclarationAmount()
						// .intValue()) {
						// throw new CommonCustomException(
						// "Voucher has been already created for the same amount");
						// }
						// } else {
						// if (voucherData
						// .getMealVoucherDeclarationStatus()
						// .equalsIgnoreCase(
						// voucherMealDeclarationBean
						// .getMealVoucherDeclarationStatus())) {
						// throw new CommonCustomException(
						// "Voucher has been already Cancelled for the same amount");
						// }
						// }
						// }
				}
			}
		}
		LOG.endUsecase("Exit from validate");
		return validate;
	}

	int getEmployeeLocationId(int empId) {
		LOG.startUsecase("Getting into getEmployeeLocationId");
		DatEmpProfessionalDetailBO profDetail = employeeProfessionalRepository.findByFkMainEmpDetailId(empId);
		LOG.endUsecase("Exit from getEmployeeLocationId");
		return profDetail.getFkEmpOrganizationId();
	}

	VoucherMealDeclarationBO getMealVoucherDetailsById(int voucherId) {
		LOG.startUsecase("Getting into getMealVoucherDetailsById");
		VoucherMealDeclarationBO voucherMealDeclarationBO = new VoucherMealDeclarationBO();
		voucherMealDeclarationBO = voucherMealDeclarationRepository.findByPkMealVoucherDeclarationId(voucherId);
		LOG.endUsecase("Exit from getMealVoucherDetailsById");
		return voucherMealDeclarationBO;
	}

	VoucherMealDeclarationBO getMealVochersByEmpIdAndMonth(Integer empId, Integer month, String year)
			throws CommonCustomException {
		LOG.startUsecase("Getting into getMealVochersByEmpIdAndMonth");
		VoucherMealDeclarationBO voucherMealDeclarationBO = new VoucherMealDeclarationBO();
		try {
			voucherMealDeclarationBO = voucherMealDeclarationRepository
					.findByFkMealVoucherDeclarationEmpIdAndMealVoucherDeclarationMonthAndMealVoucherDeclarationYear(
							empId, month, year);
		} catch (Exception e) {
			throw new CommonCustomException("Error occured in getMealVochersByEmpIdAndMonth");
		}
		LOG.endUsecase("Exit from getMealVochersByEmpIdAndMonth");
		return voucherMealDeclarationBO;
	}

	private static boolean isFinancialYearTrue(Integer selectedMonth, Integer selectedYear)
			throws CommonCustomException {
		boolean result = true;
		LocalDateTime currentTime = LocalDateTime.now();
		Integer currentYear = currentTime.getYear();
		Integer currentMonthValue = currentTime.getMonthValue();
		LOG.startUsecase("Getting into isFinancialYearTrue");
		Integer nextYear = currentYear.intValue() + 1;
		Integer pastYear = currentYear.intValue() - 1;
		if (currentYear.equals(selectedYear.intValue())) {
			if (currentMonthValue.compareTo(4) < 0) {
				if (selectedMonth.compareTo(3) > 0) {
					result = false;
					throw new CommonCustomException("Invalid month selection in current Financial year ");
				}
			} else {
				if (selectedMonth.compareTo(currentMonthValue.intValue()) < 0) {
					result = false;
					throw new CommonCustomException("Month Should be Current or Future Financial Month ");
				}
			}
		} else if (nextYear.equals(selectedYear.intValue())) {
			if (currentMonthValue.compareTo(4) >= 0) {
				if (selectedMonth.compareTo(3) > 0) {
					result = false;
					throw new CommonCustomException("Invalid month selection in current Financial year ");
				}
			} else {
				result = false;
				throw new CommonCustomException("Invalid month selection in current Financial year ");
			}
		} else if (pastYear.equals(selectedYear.intValue())) {
			if (currentMonthValue.compareTo(4) < 0) {
				if (selectedMonth.compareTo(3) < 0) {
					result = false;
					throw new CommonCustomException("Invalid month selection in current Financial year ");
				}
			} else {
				if (selectedMonth.compareTo(currentMonthValue.intValue()) < 0) {
					result = false;
					throw new CommonCustomException("Month Should be Current or Future Financial Month ");
				}
			}
		} else {
			result = false;
			throw new CommonCustomException("Invalid Month and Year Selection");
		}
		LOG.endUsecase("Exit from isFinancialYearTrue");
		return result;
	}

	public boolean generateMealVoucherReportByMonth(List<VoucherMealDeclarationBO> getMealVoucherClaims,
			String mealVoucherDeclarationYear, Integer mealVoucherDeclarationMonth, HttpServletResponse response)
			throws ParsePropertyException, IOException, InvalidFormatException, CommonCustomException {
		LOG.startUsecase("Entered generateMealVoucherReport");
		boolean isReportGenerated = false;
		Map beans = new HashMap();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (getMealVoucherClaims.isEmpty()) {
			isReportGenerated = false;
		} else {
			List<VoucherMealReportBean> mealReport = new ArrayList<VoucherMealReportBean>();
			for (VoucherMealDeclarationBO dataBo : getMealVoucherClaims) {
				if (dataBo.getMealVoucherDeclarationStatus().equalsIgnoreCase("REQUESTED")) {
					VoucherMealReportBean reportbean = new VoucherMealReportBean();
					reportbean.setAmount(dataBo.getMealVoucherDeclarationAmount());
					reportbean.setDeclaredOn(dataBo.getMealVoucherCreatedDate());
					if (dataBo.getMealVoucherModifiedDate() == null) {
						reportbean.setUpdatedOn(dataBo.getMealVoucherCreatedDate());
					} else {
						reportbean.setUpdatedOn(dataBo.getMealVoucherModifiedDate());
					}
					int empId = dataBo.getFkMealVoucherDeclarationEmpId();
					reportbean.setEmpId(empId);
					reportbean.setName(getEmployeeName(dataBo.getFkMealVoucherDeclarationEmpId()));
					reportbean.setEmailId(getEmployeeProfessionalDetails(empId).getEmpProfessionalEmailId());
					reportbean.setPhysicalLocation(getEmployeePhysicalLocation(empId));
					mealReport.add(reportbean);
				}
			}
			if (mealReport.isEmpty()) {
				throw new CommonCustomException("Report data contains empty");
			} else {
				String BASEPATH = servletContext
						.getRealPath("/WEB-INF/classes/com/thbs/mis/framework/report/template/");
				String createdOn = sdf.format(new Date());
				String outputFileName = "Meal_Voucher_Report_For_The_Month_Of_" + theMonth(mealVoucherDeclarationMonth)
						+ "_And_Year_Of_" + mealVoucherDeclarationYear + ".xls";
				try {
					beans.put("meal", mealReport);
					beans.put("createdOn", createdOn);
					boolean reportGeneratedDone = DownloadFileUtil.IsExcepReportCreated(BASEPATH, beans,
							DownloadFileUtil.MEAL_VOUCHER_REPORT, outputFileName);
					if (reportGeneratedDone) {
						DownloadFileUtil.downloadFile(response, BASEPATH, outputFileName);
						isReportGenerated = true;
						// DownloadFileUtil.deleteFile(BASEPATH,
						// outputFileName);
					}

				} catch (Exception e) {
					LOG.debug("Some error occured ", e);
				}
			}
		}
		LOG.endUsecase("Exited generateMealVoucherReport");
		return isReportGenerated;
	}

	String getEmployeeName(Integer empId) {
		LOG.startUsecase("Entered getEmployeeName");
		DatEmpPersonalDetailBO empData = employeePersonalDetailsRepository.findByFkEmpDetailId(empId);
		LOG.endUsecase("Exited getEmployeeName");
		return empData.getEmpFirstName() + " " + empData.getEmpLastName();
	}

	public static String theMonth(int month) {
		LOG.startUsecase("Entered theMonth");
		String[] monthNames = { "Default", "January", "February", "March", "April", "May", "June", "July", "August",
				"September", "October", "November", "December" };
		LOG.endUsecase("Exited theMonth");
		return monthNames[month];
	}

	// Added by Kamal Anand
	/**
	 * 
	 * @param mealVoucherDeclarationYear
	 * @param mealVoucherDeclarationMonth
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to get Meal Voucher Claim Details based on
	 *              Declaration Year and Month
	 */
	public List<MealVoucherDeclarationBean> generateMealVoucherClaimsDetails(String mealVoucherDeclarationYear,
			Integer mealVoucherDeclarationMonth) throws DataAccessException {
		LOG.startUsecase("Entered generateMealVoucherClaimsDetails");
		MealVoucherDeclarationBean mealVoucherBean = null;
		List<MealVoucherDeclarationBean> mealVoucherClaimList = new ArrayList<MealVoucherDeclarationBean>();
		List<VoucherMealDeclarationBO> getMealVoucherClaims = new ArrayList<VoucherMealDeclarationBO>();
		try {
			getMealVoucherClaims = voucherMealDeclarationRepository
					.findByMealVoucherDeclarationYearAndMealVoucherDeclarationMonthAndMealVoucherDeclarationStatusOrderByFkMealVoucherDeclarationEmpId(
							mealVoucherDeclarationYear, mealVoucherDeclarationMonth, "REQUESTED");

			if (!getMealVoucherClaims.isEmpty()) {
				for (VoucherMealDeclarationBO voucherDeclaration : getMealVoucherClaims) {
					mealVoucherBean = new MealVoucherDeclarationBean();
					mealVoucherBean.setEmpId(voucherDeclaration.getFkMealVoucherDeclarationEmpId());
					mealVoucherBean.setEmpName(getEmployeeName(voucherDeclaration.getFkMealVoucherDeclarationEmpId()));
					mealVoucherBean.setEmpBu(getEmpBuByEmpId(voucherDeclaration.getFkMealVoucherDeclarationEmpId()));
					mealVoucherBean
							.setVoucherDeclarationAmt(voucherDeclaration.getMealVoucherDeclarationAmount() + "/-");
					mealVoucherBean.setVoucherDeclarationDate(voucherDeclaration.getMealVoucherModifiedDate());
					mealVoucherBean.setMonth(voucherDeclaration.getMealVoucherDeclarationMonth());
					mealVoucherBean.setMealVoucherDeclarationId(voucherDeclaration.getPkMealVoucherDeclarationId());
					mealVoucherBean.setEmpMailId(
							getEmployeeProfessionalDetails(voucherDeclaration.getFkMealVoucherDeclarationEmpId())
									.getEmpProfessionalEmailId());
					mealVoucherBean.setEmpLocation(
							getEmployeeProfessionalDetails(voucherDeclaration.getFkMealVoucherDeclarationEmpId())
									.getMasLocationPhysicalBO().getLocationPhysicalName());
					mealVoucherClaimList.add(mealVoucherBean);
				}
			}
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("Entered generateMealVoucherClaimsDetails");
		return mealVoucherClaimList;
	}

	/**
	 * 
	 * @param empId
	 * @return
	 * @throws DataAccessException
	 * @Description : This method is used to get Employee BU Name based on Employee
	 *              Id
	 */
	public String getEmpBuByEmpId(int empId) throws DataAccessException {
		LOG.startUsecase("Entering getEmpBuByEmpId");
		DatEmpProfessionalDetailBO profDetails = null;
		try {
			profDetails = employeeProfessionalRepository.findByFkMainEmpDetailId(empId);

			if (profDetails != null) {
				return profDetails.getMasBuUnitBO().getBuUnitName();
			} else
				throw new DataAccessException("Some error happened while getting employee BU details");
		} catch (DataAccessException e) {
			LOG.endUsecase("Exiting getEmpBuByEmpId");
			throw new DataAccessException(e.getMessage());
		}

	}

	// End of Addition by Kamal Anand

	public boolean getMealVoucherReport(String mealVoucherDeclarationYear, Integer mealVoucherDeclarationMonth,
			HttpServletResponse response)
			throws ParsePropertyException, InvalidFormatException, IOException, CommonCustomException {
		LOG.startUsecase("Getting into Get Meal Voucher Report Service");
		List<VoucherMealDeclarationBO> getMealVoucherClaims = new ArrayList();
		getMealVoucherClaims = voucherMealDeclarationRepository
				.findByMealVoucherDeclarationYearAndMealVoucherDeclarationMonth(mealVoucherDeclarationYear,
						mealVoucherDeclarationMonth);
		LOG.endUsecase("Exit from Get Meal Voucher Report Service");
		return this.generateMealVoucherReportByMonth(getMealVoucherClaims, mealVoucherDeclarationYear,
				mealVoucherDeclarationMonth, response);
	}

	public boolean getMealVoucherReportBySelectedVoucherId(List<VoucherMealDeclarationBO> iniputBoLIst,
			HttpServletResponse response)
			throws ParsePropertyException, InvalidFormatException, IOException, CommonCustomException {
		LOG.startUsecase("getMealVoucherReportBySelectedVoucherId");
		List<VoucherMealDeclarationBO> getMealVoucherClaims = new ArrayList<VoucherMealDeclarationBO>();
		VoucherMealDeclarationBO data = new VoucherMealDeclarationBO();

		if (iniputBoLIst.isEmpty()) {
			throw new CommonCustomException("Voucher id should not be null");

		} else {
			for (VoucherMealDeclarationBO iniputBo : iniputBoLIst) {
				data = voucherMealDeclarationRepository
						.findByPkMealVoucherDeclarationId(iniputBo.getPkMealVoucherDeclarationId());
				getMealVoucherClaims.add(data);
			}
		}
		if (getMealVoucherClaims.isEmpty()) {
			return false;
		} else {
			LocalDateTime currentTime = LocalDateTime.now();
			Integer currentYear = currentTime.getYear();
			Integer currentMonthValue = currentTime.getMonthValue();
			LOG.endUsecase("getMealVoucherReportBySelectedVoucherId");
			return this.generateMealVoucherReportByMonth(getMealVoucherClaims, currentTime.getYear() + "",
					currentTime.getMonthValue(), response);
		}

	}

	@Autowired
	PhysicalLocationRepository physicalLocationRepository;

	String getEmployeePhysicalLocation(int empId) {
		LOG.startUsecase("getEmployeePhysicalLocation");
		short locationId = (short) getEmployeePhysicalLocationId(empId);
		LOG.endUsecase("getEmployeePhysicalLocation");
		return physicalLocationRepository.findByPkLocationPhysicalId(locationId).getLocationPhysicalName();
	}

	DatEmpProfessionalDetailBO getEmployeeProfessionalDetails(int empId) {
		LOG.startUsecase("Getting into getEmployeeLocationId");
		DatEmpProfessionalDetailBO profDetail = employeeProfessionalRepository.findByFkMainEmpDetailId(empId);
		LOG.endUsecase("Exit from getEmployeeLocationId");
		return profDetail;
	}

	private short getEmployeePhysicalLocationId(int empId) {
		LOG.startUsecase("Getting into getEmployeeLocationId");
		DatEmpProfessionalDetailBO profDetail = employeeProfessionalRepository.findByFkMainEmpDetailId(empId);
		LOG.endUsecase("Exit from getEmployeeLocationId");
		return profDetail.getFkEmpLocationPhysicalId();
	}

	public boolean mealVoucherReportBasedOnStartDate(MealVoucherReportBasedOnStartDate inputBean,
			HttpServletResponse response) throws ParsePropertyException, InvalidFormatException, IOException,
			CommonCustomException, DataAccessException {
		LOG.startUsecase("mealVoucherReportBasedOnStartDate");
		List<VoucherMealDeclarationBO> getMealVoucherClaims = new ArrayList();
		if (inputBean.getVoucherStartYear().equals(inputBean.getVoucherEndYear())) {
			if (inputBean.getVoucherStartMonth().compareTo(inputBean.getVoucherEndMonth()) > 0) {
				throw new CommonCustomException("Start month is Greater than End month");
			}
			if (inputBean.getVoucherStartMonth().compareTo(3) <= 0) {
				if (inputBean.getVoucherStartMonth().compareTo(1) >= 0
						&& inputBean.getVoucherEndMonth().compareTo(3) > 0) {
					throw new CommonCustomException("Selected Month is Not in Selected financial year");
				}
			}
		} else if (inputBean.getVoucherStartYear().compareTo(inputBean.getVoucherEndYear()) > 0) {
			throw new CommonCustomException("Start year is Greater than End year");
		} else if (inputBean.getVoucherStartYear().compareTo(inputBean.getVoucherEndYear()) < 0) {
			int temp1 = Integer.parseInt(inputBean.getVoucherStartYear());
			int temp2 = Integer.parseInt(inputBean.getVoucherEndYear());
			int temp3 = temp2 - temp1;
			if (temp3 > 1) {
				throw new CommonCustomException("Selected Year is Not in Selected financial year");
			}
			if (inputBean.getVoucherStartMonth().compareTo(3) <= 0) {
				throw new CommonCustomException("Selected Month is Not in Selected financial year");
			}
			if ((inputBean.getVoucherStartYear().compareTo(inputBean.getVoucherEndYear()) < 0)
					&& (inputBean.getVoucherEndMonth().compareTo(3) > 0)) {
				throw new CommonCustomException("Selected Month is Not in Selected financial year");
			}
		}
		try {
			List<VoucherDateRangeBean> getVoucherMonths = this.getVoucherMonths(inputBean.getVoucherStartMonth(),
					inputBean.getVoucherStartYear(), inputBean.getVoucherEndMonth(), inputBean.getVoucherEndYear());
			for (VoucherDateRangeBean voucherDateRangeBean : getVoucherMonths) {
				List<VoucherMealDeclarationBO> temp = new ArrayList<VoucherMealDeclarationBO>();
				temp = voucherMealDeclarationRepository
						.findByMealVoucherDeclarationYearAndMealVoucherDeclarationMonthInAndMealVoucherDeclarationStatusOrderByFkMealVoucherDeclarationEmpIdAscMealVoucherModifiedDateAsc(
								voucherDateRangeBean.getYear(), voucherDateRangeBean.getMonths(), "REQUESTED");
				for (VoucherMealDeclarationBO voucherMealDeclarationBO : temp) {
					getMealVoucherClaims.add(voucherMealDeclarationBO);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error while fetching data from DB");
		}
		LOG.endUsecase("mealVoucherReportBasedOnStartDate");
		return this.generateMealVoucherReportByDateRange(getMealVoucherClaims, inputBean.getVoucherStartMonth(),
				inputBean.getVoucherStartYear(), inputBean.getVoucherEndMonth(), inputBean.getVoucherEndYear(),
				response);
	}

	public boolean generateMealVoucherReportByDateRange(List<VoucherMealDeclarationBO> getMealVoucherClaims,
			Integer voucherStartMonth, String voucherStartYear, Integer voucherEndMonth, String voucherEndYear,
			HttpServletResponse response)
			throws ParsePropertyException, IOException, InvalidFormatException, CommonCustomException {
		LOG.startUsecase("Entered generateMealVoucherReportByDateRange");
		boolean isReportGenerated = false;
		Map beans = new HashMap();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (getMealVoucherClaims.isEmpty()) {
			isReportGenerated = false;
		} else {
			List<MealVoucherReportBeanBasedOnRanges> mealReport = new ArrayList<MealVoucherReportBeanBasedOnRanges>();
			Set<Integer> empList = getMealVoucherClaims.stream().filter(p -> p.getFkMealVoucherDeclarationEmpId() > 0) // filtering
																														// price
					.map(pm -> pm.getFkMealVoucherDeclarationEmpId()) // fetching price
					.collect(Collectors.toSet());
			// empList = empList.stream().sorted().collect(Collectors.toSet()); This is used
			// to sort..
			empList.forEach(empId -> {
				int selectedEmpID = empId;
				List<VoucherMealDeclarationBO> mealVoucherDataList = new ArrayList<VoucherMealDeclarationBO>();
				for (VoucherMealDeclarationBO pm : getMealVoucherClaims) {
					if (pm.getFkMealVoucherDeclarationEmpId() == selectedEmpID) {
						mealVoucherDataList.add(pm);
					}
				}
				if (mealVoucherDataList.size() > 0) {
					MealVoucherReportBeanBasedOnRanges reportbean = new MealVoucherReportBeanBasedOnRanges();
					reportbean.setEmpId(empId);
					reportbean.setName(getEmployeeName(empId));
					reportbean.setEmailId(getEmployeeProfessionalDetails(empId).getEmpProfessionalEmailId());
					reportbean.setPhysicalLocation(getEmployeePhysicalLocation(empId));
					totalAmount = 0;
					mealVoucherDataList.forEach(meal -> {

						if (meal.getMealVoucherDeclarationMonth().equals(1)) {
							reportbean.setJanuary(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(2)) {
							reportbean.setFeburary(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(3)) {
							reportbean.setMarch(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(4)) {
							reportbean.setApril(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(5)) {
							reportbean.setMay(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(6)) {
							reportbean.setJune(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(7)) {
							reportbean.setJuly(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(8)) {
							reportbean.setAuguest(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(9)) {
							reportbean.setSeptember(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(10)) {
							reportbean.setOctober(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(11)) {
							reportbean.setNovember(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(12)) {
							reportbean.setDecember(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						}
						reportbean.setTotalAmount(totalAmount);
						reportbean.setDeclaredOn(meal.getMealVoucherCreatedDate());
						reportbean.setUpdatedOn(meal.getMealVoucherModifiedDate());
					});
					mealReport.add(reportbean);
				}
			});
			Collections.sort(mealReport, new Comparator<MealVoucherReportBeanBasedOnRanges>() {
				public int compare(MealVoucherReportBeanBasedOnRanges o1, MealVoucherReportBeanBasedOnRanges o2) {
					return -o2.getEmpId().compareTo(o1.getEmpId());
				}
			});
			if (mealReport.isEmpty()) {
				throw new CommonCustomException("Report data contains empty");
			} else {
				String BASEPATH = servletContext
						.getRealPath("/WEB-INF/classes/com/thbs/mis/framework/report/template/");
				String createdOn = sdf.format(new Date());
				String outputFileName = "Meal Voucher Report From " + this.theShortMonth(voucherStartMonth) + " "
						+ voucherStartYear + " To " + this.theShortMonth(voucherEndMonth) + " " + voucherEndYear
						+ ".xls";
				try {
					beans.put("meal", mealReport);
					beans.put("createdOn", createdOn);
					beans.put("startMonth", this.theMonth(voucherStartMonth));
					beans.put("endMonth", this.theMonth(voucherEndMonth));
					beans.put("startYear", voucherStartYear);
					beans.put("endYear", voucherEndYear);
					boolean reportGeneratedDone = DownloadFileUtil.IsExcepReportCreated(BASEPATH, beans,
							DownloadFileUtil.MEAL_VOUCHER_REPORT_BASED_ON_RANGES, outputFileName);
					if (reportGeneratedDone) {
						DownloadFileUtil.downloadFile(response, BASEPATH, outputFileName);
						isReportGenerated = true;
						// DownloadFileUtil.deleteFile(BASEPATH,
						// outputFileName);
					}

				} catch (Exception e) {
					LOG.debug("Some error occured ", e);
				}
			}
		}
		LOG.endUsecase("Exited generateMealVoucherReportByDateRange");
		return isReportGenerated;
	}

	public static String theShortMonth(int month) {
		LOG.startUsecase("Entered theShortMonth");
		String[] monthNames = { "Default", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov",
				"Dec" };
		LOG.endUsecase("Exited theShortMonth");
		return monthNames[month];
	}

	// get voucher Month Range report
	public List<MealVoucherReportBeanBasedOnRanges> mealVoucherReportRange(MealVoucherReportBasedOnStartDate inputBean)
			throws ParsePropertyException, InvalidFormatException, IOException, CommonCustomException,
			DataAccessException {
		LOG.startUsecase("mealVoucherReportRange");
		List<VoucherMealDeclarationBO> getMealVoucherClaims = new ArrayList<VoucherMealDeclarationBO>();
		if (inputBean.getVoucherStartYear().equals(inputBean.getVoucherEndYear())) {
			if (inputBean.getVoucherStartMonth().compareTo(inputBean.getVoucherEndMonth()) > 0) {
				throw new CommonCustomException("Start month is Greater than End month");
			}
			if (inputBean.getVoucherStartMonth().compareTo(3) <= 0) {
				if (inputBean.getVoucherStartMonth().compareTo(1) >= 0
						&& inputBean.getVoucherEndMonth().compareTo(3) > 0) {
					throw new CommonCustomException("Selected Month is Not in Selected financial year");
				}
			}
		} else if (inputBean.getVoucherStartYear().compareTo(inputBean.getVoucherEndYear()) > 0) {
			throw new CommonCustomException("Start year is Greater than End year");
		} else if (inputBean.getVoucherStartYear().compareTo(inputBean.getVoucherEndYear()) < 0) {
			int temp1 = Integer.parseInt(inputBean.getVoucherStartYear());
			int temp2 = Integer.parseInt(inputBean.getVoucherEndYear());
			int temp3 = temp2 - temp1;
			if (temp3 > 1) {
				throw new CommonCustomException("Selected Year is Not in Selected financial year");
			}
			if (inputBean.getVoucherStartMonth().compareTo(3) <= 0) {
				throw new CommonCustomException("Selected Month is Not in Selected financial year");
			}
			if ((inputBean.getVoucherStartYear().compareTo(inputBean.getVoucherEndYear()) < 0)
					&& (inputBean.getVoucherEndMonth().compareTo(3) > 0)) {
				throw new CommonCustomException("Selected Month is Not in Selected financial year");
			}
		}
		try {
			List<VoucherDateRangeBean> getVoucherMonths = this.getVoucherMonths(inputBean.getVoucherStartMonth(),
					inputBean.getVoucherStartYear(), inputBean.getVoucherEndMonth(), inputBean.getVoucherEndYear());
			for (VoucherDateRangeBean voucherDateRangeBean : getVoucherMonths) {
				List<VoucherMealDeclarationBO> temp = new ArrayList<VoucherMealDeclarationBO>();
				temp = voucherMealDeclarationRepository
						.findByMealVoucherDeclarationYearAndMealVoucherDeclarationMonthInAndMealVoucherDeclarationStatusOrderByFkMealVoucherDeclarationEmpIdAscMealVoucherModifiedDateAsc(
								voucherDateRangeBean.getYear(), voucherDateRangeBean.getMonths(), "REQUESTED");
				for (VoucherMealDeclarationBO voucherMealDeclarationBO : temp) {
					getMealVoucherClaims.add(voucherMealDeclarationBO);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException("Error while fetching data from DB");
		}
		LOG.endUsecase("mealVoucherReportRange");
		return this.generateMealVoucherReportRange(getMealVoucherClaims, inputBean.getVoucherStartMonth(),
				inputBean.getVoucherStartYear(), inputBean.getVoucherEndMonth(), inputBean.getVoucherEndYear());
	}

	int totalAmount = 0;
	List<Integer> listOfPkId;

	public List<MealVoucherReportBeanBasedOnRanges> generateMealVoucherReportRange(
			List<VoucherMealDeclarationBO> getMealVoucherClaims, Integer voucherStartMonth, String voucherStartYear,
			Integer voucherEndMonth, String voucherEndYear)
			throws ParsePropertyException, IOException, InvalidFormatException, CommonCustomException {
		LOG.startUsecase("Entered generateMealVoucherReportRange");
		boolean isReportGenerated = false;
		Map beans = new HashMap();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<MealVoucherReportBeanBasedOnRanges> mealReport = new ArrayList<MealVoucherReportBeanBasedOnRanges>();
		if (getMealVoucherClaims.isEmpty()) {
			isReportGenerated = false;
		} else {
			Set<Integer> empList = getMealVoucherClaims.stream().filter(p -> p.getFkMealVoucherDeclarationEmpId() > 0) // filtering
																														// price
					.map(pm -> pm.getFkMealVoucherDeclarationEmpId()) // fetching price
					.collect(Collectors.toSet());
			// empList = empList.stream().sorted().collect(Collectors.toSet()); This is used
			// to sort..
			empList.forEach(empId -> {
				int selectedEmpID = empId;
				List<VoucherMealDeclarationBO> mealVoucherDataList = new ArrayList<VoucherMealDeclarationBO>();
				for (VoucherMealDeclarationBO pm : getMealVoucherClaims) {
					if (pm.getFkMealVoucherDeclarationEmpId() == selectedEmpID) {
						mealVoucherDataList.add(pm);
					}
				}
				if (mealVoucherDataList.size() > 0) {
					MealVoucherReportBeanBasedOnRanges reportbean = new MealVoucherReportBeanBasedOnRanges();
					reportbean.setEmpId(empId);
					reportbean.setName(getEmployeeName(empId));
					reportbean.setEmailId(getEmployeeProfessionalDetails(empId).getEmpProfessionalEmailId());
					reportbean.setPhysicalLocation(getEmployeePhysicalLocation(empId));
					totalAmount = 0;
					listOfPkId = new ArrayList<Integer>();
					mealVoucherDataList.forEach(meal -> {
						listOfPkId.add(meal.getPkMealVoucherDeclarationId());
						if (meal.getMealVoucherDeclarationMonth().equals(1)) {
							reportbean.setJanuary(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(2)) {
							reportbean.setFeburary(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(3)) {
							reportbean.setMarch(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(4)) {
							reportbean.setApril(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(5)) {
							reportbean.setMay(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(6)) {
							reportbean.setJune(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(7)) {
							reportbean.setJuly(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(8)) {
							reportbean.setAuguest(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(9)) {
							reportbean.setSeptember(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(10)) {
							reportbean.setOctober(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(11)) {
							reportbean.setNovember(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(12)) {
							reportbean.setDecember(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						}
						reportbean.setListOfPkVoucherId(listOfPkId);
						reportbean.setTotalAmount(totalAmount);
						reportbean.setDeclaredOn(meal.getMealVoucherCreatedDate());
						reportbean.setUpdatedOn(meal.getMealVoucherModifiedDate());
					});
					mealReport.add(reportbean);
				}
			});
		}
		Collections.sort(mealReport, new Comparator<MealVoucherReportBeanBasedOnRanges>() {
			public int compare(MealVoucherReportBeanBasedOnRanges o1, MealVoucherReportBeanBasedOnRanges o2) {
				return -o2.getEmpId().compareTo(o1.getEmpId());
			}
		});
		LOG.endUsecase("Exited generateMealVoucherReportRange");
		return mealReport;
	}

	List<VoucherDateRangeBean> getVoucherMonths(Integer startMonth, String startYear, Integer endMonth,
			String endYear) {
		int stYear = Integer.parseInt(startYear);
		int edYear = Integer.parseInt(endYear);
		List<VoucherDateRangeBean> result = new ArrayList<VoucherDateRangeBean>();
		if (stYear == edYear) {
			VoucherDateRangeBean t1 = new VoucherDateRangeBean();
			t1.setYear(stYear + "");
			List<Integer> months = new ArrayList<Integer>();
			for (int i = startMonth; i <= endMonth; i++) {
				months.add(i);
			}
			t1.setMonths(months);
			result.add(t1);
		} else if (stYear < edYear) {
			for (int i = stYear; i <= edYear; i++) {
				if (i != edYear) {
					VoucherDateRangeBean t2 = new VoucherDateRangeBean();
					t2.setYear(i + "");
					List<Integer> months = new ArrayList<Integer>();
					for (int j = startMonth; j <= 12; j++) {
						months.add(j);
					}
					t2.setMonths(months);
					result.add(t2);
				} else {
					VoucherDateRangeBean t3 = new VoucherDateRangeBean();
					t3.setYear(i + "");
					List<Integer> months = new ArrayList<Integer>();
					for (int j = 1; j <= endMonth; j++) {
						months.add(j);
					}
					t3.setMonths(months);
					result.add(t3);
				}
			}
		}
		return result;
	}

	// New Report with All Pk voucher id
	public boolean mealVoucherReportBasedOnStartDateWithPkVoucherId(VoucherListOfVoucherIdReportBean inputBean,
			HttpServletResponse response) throws ParsePropertyException, InvalidFormatException, IOException,
			CommonCustomException, DataAccessException {
		LOG.startUsecase("mealVoucherReportBasedOnStartDateWithPkVoucherId");
		List<VoucherMealDeclarationBO> getMealVoucherClaims = new ArrayList<VoucherMealDeclarationBO>();

		try {

			getMealVoucherClaims = voucherMealDeclarationRepository
					.findByPkMealVoucherDeclarationIdInOrderByMealVoucherModifiedDateDesc(
							inputBean.getListOfPkVoucherId());

		} catch (Exception e) {
			throw new DataAccessException("Error while fetching data from DB");
		}
		LOG.endUsecase("mealVoucherReportBasedOnStartDateWithPkVoucherId");
		return this.generateMealVoucherReportByDateRangeWithPkVoucherId(getMealVoucherClaims, response);
	}

	public boolean generateMealVoucherReportByDateRangeWithPkVoucherId(
			List<VoucherMealDeclarationBO> getMealVoucherClaims, HttpServletResponse response)
			throws ParsePropertyException, IOException, InvalidFormatException, CommonCustomException {
		LOG.startUsecase("Entered generateMealVoucherReportByDateRangeWithPkVoucherId");
		boolean isReportGenerated = false;
		Map beans = new HashMap();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (getMealVoucherClaims.isEmpty()) {
			isReportGenerated = false;
		} else {
			List<MealVoucherReportBeanBasedOnRanges> mealReport = new ArrayList<MealVoucherReportBeanBasedOnRanges>();
			Set<Integer> empList = getMealVoucherClaims.stream().filter(p -> p.getFkMealVoucherDeclarationEmpId() > 0) // filtering
																														// price
					.map(pm -> pm.getFkMealVoucherDeclarationEmpId()) // fetching price
					.collect(Collectors.toSet());
			// empList = empList.stream().sorted().collect(Collectors.toSet()); This is used
			// to sort..
			empList.forEach(empId -> {
				int selectedEmpID = empId;
				List<VoucherMealDeclarationBO> mealVoucherDataList = new ArrayList<VoucherMealDeclarationBO>();
				for (VoucherMealDeclarationBO pm : getMealVoucherClaims) {
					if (pm.getFkMealVoucherDeclarationEmpId() == selectedEmpID) {
						mealVoucherDataList.add(pm);
					}
				}
				if (mealVoucherDataList.size() > 0) {
					MealVoucherReportBeanBasedOnRanges reportbean = new MealVoucherReportBeanBasedOnRanges();
					reportbean.setEmpId(empId);
					reportbean.setName(getEmployeeName(empId));
					reportbean.setEmailId(getEmployeeProfessionalDetails(empId).getEmpProfessionalEmailId());
					reportbean.setPhysicalLocation(getEmployeePhysicalLocation(empId));
					totalAmount = 0;
					mealVoucherDataList.forEach(meal -> {

						if (meal.getMealVoucherDeclarationMonth().equals(1)) {
							reportbean.setJanuary(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(2)) {
							reportbean.setFeburary(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(3)) {
							reportbean.setMarch(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(4)) {
							reportbean.setApril(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(5)) {
							reportbean.setMay(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(6)) {
							reportbean.setJune(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(7)) {
							reportbean.setJuly(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(8)) {
							reportbean.setAuguest(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(9)) {
							reportbean.setSeptember(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(10)) {
							reportbean.setOctober(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(11)) {
							reportbean.setNovember(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						} else if (meal.getMealVoucherDeclarationMonth().equals(12)) {
							reportbean.setDecember(meal.getMealVoucherDeclarationAmount());
							totalAmount = totalAmount + meal.getMealVoucherDeclarationAmount();
						}
						reportbean.setTotalAmount(totalAmount);
						reportbean.setDeclaredOn(meal.getMealVoucherCreatedDate());
						reportbean.setUpdatedOn(meal.getMealVoucherModifiedDate());
					});
					mealReport.add(reportbean);
				}
			});
			Collections.sort(mealReport, new Comparator<MealVoucherReportBeanBasedOnRanges>() {
				public int compare(MealVoucherReportBeanBasedOnRanges o1, MealVoucherReportBeanBasedOnRanges o2) {
					return -o2.getEmpId().compareTo(o1.getEmpId());
				}
			});
			if (mealReport.isEmpty()) {
				throw new CommonCustomException("Report data contains empty");
			} else {
				String BASEPATH = servletContext
						.getRealPath("/WEB-INF/classes/com/thbs/mis/framework/report/template/");
				String createdOn = sdf.format(new Date());
				String outputFileName = "Meal Voucher Report " + ".xls";
				// + this.theShortMonth(voucherStartMonth) + " " +voucherStartYear
				// + " To " + this.theShortMonth(voucherEndMonth) +" " +voucherEndYear +".xls";
				try {
					beans.put("meal", mealReport);
					beans.put("createdOn", createdOn);
					// beans.put("startMonth", this.theMonth(voucherStartMonth));
					// beans.put("endMonth", this.theMonth(voucherEndMonth));
					// beans.put("startYear", voucherStartYear);
					// beans.put("endYear", voucherEndYear);
					boolean reportGeneratedDone = DownloadFileUtil.IsExcepReportCreated(BASEPATH, beans,
							DownloadFileUtil.MEAL_VOUCHER_REPORT_BASED_ON_RANGES_AND_VOUCHER_ID, outputFileName);
					if (reportGeneratedDone) {
						DownloadFileUtil.downloadFile(response, BASEPATH, outputFileName);
						isReportGenerated = true;
						// DownloadFileUtil.deleteFile(BASEPATH,
						// outputFileName);
					}

				} catch (Exception e) {
					LOG.debug("Some error occured ", e);
				}
			}
		}
		LOG.endUsecase("Exited generateMealVoucherReportByDateRangeWithPkVoucherId");
		return isReportGenerated;
	}
}