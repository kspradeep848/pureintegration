/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  VoucherGiftDeclarationController.java    	     */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :   20/12/2016                                       */
/*                                                                   */
/*  Description :  All the Gift Voucher services are done here       */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date           Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 20/12/2016    THBS      1.0        Initial version created        */
/*********************************************************************/

package com.thbs.mis.voucher.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.io.IOException;
import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import net.sf.jxls.exception.ParsePropertyException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.exception.GoalNotFoundException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.ValidationUtils;
import com.thbs.mis.voucher.bean.MealVoucherDeclarationBean;
import com.thbs.mis.voucher.bean.VoucherGetDatesBean;
import com.thbs.mis.voucher.bean.VoucherGiftDeclarationBean;
import com.thbs.mis.voucher.bo.VoucherGiftDeclarationBO;
import com.thbs.mis.voucher.constants.VoucherDeclarationURI;
import com.thbs.mis.voucher.service.VoucherGiftDeclarationSrvc;

@Controller
@Configuration
@PropertySource("classpath:mis_voucher.properties")
public class VoucherGiftDeclarationController {

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(VoucherGiftDeclarationController.class);

	@Autowired(required = true)
	private CommonService commonService;

	@Autowired
	private VoucherGiftDeclarationSrvc voucherGiftDeclarationSrvc;

	@Value("${voucher.gift.start.date.month}")
	String startDate;

	@Value("${voucher.gift.end.date.month}")
	String endDate;

	/**
	 * <Description submitGiftVoucherClaims:> This contains the methods used for
	 * the SUBMIT_GIFT_VOUCHER_CLAIMS for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws GoalNotFoundException
	 * @see
	 */
	@ApiOperation(tags="Voucher",value = "SUBMIT_GIFT_VOUCHER_CLAIMS."
			+ "To Submit Gift Voucher Claims details.", httpMethod = "POST", notes = "This Service has been implemented to Submit Gift Voucher Claims details."
			+ "<br>The Response will be the detailed of the Gift Voucher."
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1)voucher_gift_dat_emp_declaration. <br><br>"
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Amount Should be Multiplies of 500 and Maximum of amount 2000."
			+ "<br> 2) Applicable for only India Employees. "
			+ "<br> <b>Property File Details :</b>"
			+ "<br> 1) mis_voucher.properties. ",

	nickname = "SUBMIT GIFT VOUCHER CLAIMS", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = VoucherGiftDeclarationBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = VoucherGiftDeclarationBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = VoucherDeclarationURI.SUBMIT_GIFT_VOUCHER_CLAIMS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> submitGiftVoucherClaims(
			@Valid @ApiParam(name = "SubmitGiftVoucherClaims", example = "1",
					value = "{"
							+ "<br> empId: 1,"
							+ "<br> amount: 1000,"
							+ "<br> status: REQUESTED,"
							+ "<br> year: 2017,"
							+ "<br> "
							+ "}") 
			@RequestBody VoucherGiftDeclarationBean bean,
			BindingResult binding) throws BusinessException,
			CommonCustomException {
		LOG.startUsecase("Submit Gift Voucher Claims Controller");
		List<VoucherGiftDeclarationBean> submitGiftVoucherClaims1 = new ArrayList<VoucherGiftDeclarationBean>();
		if (binding.hasErrors()) {
			return ValidationUtils.handleValidationErrors(binding);
		} else {
			submitGiftVoucherClaims1 = voucherGiftDeclarationSrvc
					.submitGiftVoucherClaims(bean);
			LOG.endUsecase("Submit Gift Voucher Claims Controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Gift Voucher Claims Submitted Successfully.",
							submitGiftVoucherClaims1));
		}
	}

	/**
	 * <Description submitGiftVoucherClaims:> This contains the methods used for
	 * the SUBMIT_GIFT_VOUCHER_CLAIMS for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws CommonCustomException
	 * @throws DRException
	 * @throws GoalNotFoundException
	 * @see
	 */
	@ApiOperation(tags="Voucher",value = "REPORT GIFT VOUCHER CLAIMS."
			+ "To Get Gift Voucher Claims Report details.", httpMethod = "GET", notes = "This Service has been implemented to Get Gift Voucher Claims Report."
			+ "<br>The Response will be the detailed of the Gift Voucher."
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1)voucher_gift_dat_emp_declaration. <br><br>"
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Amount Should be Multiplies of 500 and Maximum of amount 2000."
			+ "<br> 2) Applicable for only India Employees. "
			+ "<br> <b>Property File Details :</b>"
			+ "<br> 1) mis_voucher.properties. "
			+ "<br> <b>Report Template Details :</b>"
			+ "<br> 1)giftVoucherTemplate.xls ",

	nickname = "REPORT GIFT VOUCHER CLAIMS", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = VoucherGiftDeclarationBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = VoucherDeclarationURI.GET_GIFT_VOUCHER_REPORT, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getReportGiftVoucherClaims(
			HttpServletResponse response,
			@PathVariable("year") String giftVoucherDeclarationYear)
			throws BusinessException, CommonCustomException {
		LOG.startUsecase("Entered From getReportGiftVoucherClaims");
		boolean isReportGenerated = false;
		isReportGenerated = voucherGiftDeclarationSrvc
				.generateGiftVoucherReport(giftVoucherDeclarationYear, response);
		LOG.endUsecase("Exited to getReportGiftVoucherClaims");
		if (isReportGenerated) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Report generated successfully"));

		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Gift Voucher Claims are Not Found."));
		}
	}

	@ApiOperation(tags="Voucher",value = "GET GIFT VOUCHER CLAIMS."
			+ "To Get Gift Voucher Claims  details.", httpMethod = "GET", notes = "This Service has been implemented to Get Gift Voucher Claims ."
			+ "<br>The Response will be the detailed of the Gift Voucher."
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1)voucher_gift_dat_emp_declaration. <br><br>"
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Amount Should be Multiplies of 500 and Maximum of amount 2000."
			+ "<br> 2) Applicable for only India Employees. "
			+ "<br> <b>Property File Details :</b>"
			+ "<br> 1) mis_voucher.properties. ",

	nickname = "GET GIFT VOUCHER CLAIMS", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = VoucherGiftDeclarationBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = VoucherDeclarationURI.GET_GIFT_VOUCHER_CLAIMS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getReportGiftVoucherClaimsForEmployee(
			@PathVariable("year") String giftVoucherDeclarationYear,
			@PathVariable("empId") Integer employeeId)
			throws BusinessException, CommonCustomException {
		LOG.startUsecase("Entered From getReportGiftVoucherClaimsForEmployee");
		boolean isReportGenerated = false;
		VoucherGiftDeclarationBO voucherGiftDeclarationBO = new VoucherGiftDeclarationBO();
		voucherGiftDeclarationBO = voucherGiftDeclarationSrvc
				.getGiftVochersByEmpIdAndYear(employeeId,
						giftVoucherDeclarationYear);
		VoucherGiftDeclarationBean voucherGiftBean = new VoucherGiftDeclarationBean();
		if (voucherGiftDeclarationBO != null) {

		}
		List<VoucherGiftDeclarationBean> outputList = new ArrayList<VoucherGiftDeclarationBean>();
		/* outputList.add(voucherGiftDeclarationBO); */
		if (voucherGiftDeclarationBO != null) {
			voucherGiftBean
					.setFkGiftVoucherDeclarationEmpId(voucherGiftDeclarationBO
							.getFkGiftVoucherDeclarationEmpId());
			voucherGiftBean.setGiftVoucherCreatedDate(voucherGiftDeclarationBO
					.getGiftVoucherCreatedDate());
			voucherGiftBean
					.setGiftVoucherDeclarationAmount(voucherGiftDeclarationBO
							.getGiftVoucherDeclarationAmount());
			voucherGiftBean
					.setGiftVoucherDeclarationStatus(voucherGiftDeclarationBO
							.getGiftVoucherDeclarationStatus());
			voucherGiftBean
					.setGiftVoucherDeclarationYear(voucherGiftDeclarationBO
							.getGiftVoucherDeclarationYear());
			voucherGiftBean
					.setPkGiftVoucherDeclarationId(voucherGiftDeclarationBO
							.getPkGiftVoucherDeclarationId());
			voucherGiftBean.setGiftVoucherModifiedDate(voucherGiftDeclarationBO
					.getGiftVoucherModifiedDate());
			voucherGiftBean.setStartDate(this.startDate);
			voucherGiftBean.setEndDate(this.endDate);
			Long range = 0l;
			if (this.startDate != null && this.endDate != null) {
				try {
					LocalDate endDate = LocalDate.parse(this.endDate);
					LocalDate startDate = LocalDate.parse(this.startDate);
					LocalDate currentDate = LocalDate.now();
					if (currentDate.getMonth() == startDate.getMonth()
							|| currentDate.getMonth() == endDate.getMonth()) {
						range = ChronoUnit.DAYS.between(currentDate, endDate);
					}
				} catch (DateTimeParseException e) {
					throw new CommonCustomException(
							"Check the voucher property file, Date Format should be like YYYY-MM-DD");
				}
			}
			voucherGiftBean
					.setRemainingDays(Integer.parseInt(range.toString()));
			outputList.add(voucherGiftBean);
		}
		LOG.endUsecase("Exited to getReportGiftVoucherClaims");
		if (!outputList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Report generated successfully", outputList));

		} else {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.FAILURE,
							"Gift Voucher Claims are Not Found."));
		}
	}

	@ApiOperation(tags="Voucher",value = "GIFT_VOUCHER_DATES."
			+ "To Gift Voucher Dates details.", httpMethod = "GET", notes = "This Service has been implemented to Gift Voucher Dates details."
			+ "<br>The Response will be the detailed of the Gift Voucher."
			+ "<br> <b>Property File Details :</b>"
			+ "<br> 1) mis_voucher.properties. ",

	nickname = "GIFT VOUCHER DATES", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = VoucherGiftDeclarationBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = VoucherGiftDeclarationBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = VoucherDeclarationURI.GET_GIFT_VOUCHER_DATES, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getGiftVoucherDates(
			@ApiParam(name = "fkMealVoucherDeclarationEmpId", example = "1", value = "fkMealVoucherDeclarationEmpId") @PathVariable("fkMealVoucherDeclarationEmpId") Integer fkMealVoucherDeclarationEmpId)
			throws CommonCustomException, DataAccessException {
		LOG.startUsecase("start of gift dates");
		VoucherGetDatesBean getDates = null;
		List<VoucherGetDatesBean> getDatesList = new ArrayList<VoucherGetDatesBean>();
		getDates = voucherGiftDeclarationSrvc.giftVoucherDates(fkMealVoucherDeclarationEmpId);
		getDatesList.add(getDates);
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"Get Dates of gift", getDatesList));
	}
	
	/**
	 * <Description getGiftVoucherClaims:> This contains the methods used for
	 * the GET_ALL_GIFT_VOUCHER_CLAIMS for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws CommonCustomException
	 * @throws DataAccessException 
	 * @throws DRException
	 * @throws GoalNotFoundException
	 * @see
	 */
	@ApiOperation(tags="Voucher",value = "GET_ALL_GIFT_VOUCHER_CLAIMS."
			+ "To Get Gift Voucher Claims details.", httpMethod = "GET", notes = "This Service has been implemented to Get Gift Voucher Claims."
			+ "<br>The Response will be the detailed of the Gift Voucher."
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1)voucher_gift_dat_emp_declaration. <br><br>"
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Amount Should be Multiplies of 500 and Maximum of amount 2000."
			+ "<br> 2) Applicable for only India Employees. "
			+ "<br> <b>Property File Details :</b>"
			+ "<br> 1) mis_voucher.properties. "
			+ "<br> <b>Report Template Details :</b>"
			+ "<br> 1)giftVoucherTemplate.xls ",

	nickname = "GET GIFT VOUCHER CLAIMS", protocols = "HTTP", responseReference = "application/json", produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = VoucherGiftDeclarationBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = VoucherDeclarationURI.GET_ALL_GIFT_VOUCHER_CLAIMS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getGiftVoucherClaims(@PathVariable("year") String giftVoucherDeclarationYear)
			throws BusinessException, CommonCustomException, DataAccessException {
		LOG.startUsecase("Entered From getGiftVoucherClaims");
		List<MealVoucherDeclarationBean> isReportGenerated = new ArrayList<MealVoucherDeclarationBean>();
		isReportGenerated = voucherGiftDeclarationSrvc
				.getGiftVoucherReport(giftVoucherDeclarationYear);
		LOG.endUsecase("Exited to getGiftVoucherClaims");
		if (isReportGenerated.size()>0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Gift Voucher Claims Successfully.",isReportGenerated));

		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"No data in Gift Voucher Claims"));
		}
	}
	
	@ApiOperation(tags="Voucher",value = "GET GIFT VOUCHER REPORT FOR SELECTED VOUCHER IDS."
			+ "To GET GIFT VOUCHER CLAIMS Report .", httpMethod = "POST", notes = "This Service has been implemented to Get Gift Voucher Claims report."
			+ "<br>The Response will be the detailed of the Gift Voucher."
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1)voucher_gift_dat_emp_declaration. <br><br>"
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Deatiled for only India Employees. <br><br>"
			+ "<br> <b>Property File Details :</b>"
			+ "<br> 1) mis_voucher.properties. "
			+ "<br> <b>Report Template Details :</b>"
			+ "<br> 1) giftVoucherTemplate.xls. ",

	nickname = "GET GIFT VOUCHER REPORT", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = VoucherGiftDeclarationBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = VoucherGiftDeclarationBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = VoucherDeclarationURI.GET_SELECTED_GIFT_CARD_REPORT, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> getGiftVoucherReportBySelectedVoucherId(
			@Valid @ApiParam(name = "voucherGiftDeclarationBO", example = "1", 
			value = "["
					+ "<br> {"
					+ "<br>VoucherDeclarationId:1,"
					+ " <br> },"
					+ "<br> {"
					+ "<br>VoucherDeclarationId:2,"
					+ "<br> }"
					+ "<br> ]")
			@RequestBody List<VoucherGiftDeclarationBO> voucherIds,
			HttpServletResponse response) throws BusinessException,
			IOException, ParsePropertyException,
			org.apache.poi.openxml4j.exceptions.InvalidFormatException,
			CommonCustomException {
		LOG.endUsecase("Entered getGiftVoucherReportBySelectedVoucherId");
		boolean isReportGenerated = false;
		isReportGenerated = voucherGiftDeclarationSrvc
				.getGiftVoucherReportBySelectedVoucherId(voucherIds, response);
		LOG.endUsecase("Exited getGiftVoucherReportBySelectedVoucherId");
		if (isReportGenerated) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Report generated successfully"));

		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Gift Voucher Claim Details are Not Found."));
		}

	}
}
