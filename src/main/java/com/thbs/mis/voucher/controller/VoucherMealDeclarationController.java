/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  VoucherMealDeclarationController.java   	         */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :   20/12/2016                                       */
/*                                                                   */
/*  Description :  All the Meal Voucher services are done here       */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date           Who     Version      Comments             		 */
/*-------------------------------------------------------------------*/
/* 20/12/2016    THBS      1.0        Initial version created        */
/*********************************************************************/

package com.thbs.mis.voucher.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.io.IOException;
import java.net.URI;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import net.sf.jxls.exception.ParsePropertyException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
import com.thbs.mis.common.dao.EmployeeProfessionalRepository;
import com.thbs.mis.common.service.CommonService;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.EntityBeanList;
import com.thbs.mis.framework.util.ValidationUtils;
import com.thbs.mis.voucher.bean.MealVoucherDeclarationBean;
import com.thbs.mis.voucher.bean.MealVoucherReportBasedOnStartDate;
import com.thbs.mis.voucher.bean.MealVoucherReportBeanBasedOnRanges;
import com.thbs.mis.voucher.bean.VoucherListOfVoucherIdReportBean;
import com.thbs.mis.voucher.bean.VoucherMealDeclarationBean;
import com.thbs.mis.voucher.bo.VoucherMealDeclarationBO;
import com.thbs.mis.voucher.constants.VoucherDeclarationURI;
import com.thbs.mis.voucher.service.VoucherMealDeclarationSrvc;

@Controller
public class VoucherMealDeclarationController {

	/**
	 * Instance of <code>Log</code>
	 */
	private static final AppLog LOG = LogFactory
			.getLog(VoucherMealDeclarationController.class);

	@Autowired(required = true)
	private CommonService commonService;

	@Autowired
	private VoucherMealDeclarationSrvc voucherMealDeclarationSrvc;

	@Autowired(required = true)
	private EmployeeProfessionalRepository employeeProfessionalRepository;

	@Value("${voucher.meal.last.date}")
	Integer lastDate;

	/**
	 * <Description submitMealVoucherClaims:> This contains the methods used for
	 * the SUBMIT_MEAL_VOUCHER_CLAIMS for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws BusinessException
	 * @throws DataAccessException 
	 * @see
	 */
	@ApiOperation(tags="Voucher",value = "SUBMIT_MEAL_VOUCHER_CLAIMS."
			+ "To save/update the  Meal Voucher Claims details.", httpMethod = "POST", notes = "This Service has been implemented to Submit/Update the Meal Voucher Claims details."
			+ "<br>The Response will be the detailed of the Meal Voucher."
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1)voucher_meal_dat_emp_declaration."
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Amount Should be Multiplies of 500 and Maximum of amount 2000."
			+ "<br> 2) Applicable for only India Employees. "
			+ "<br> <b>Property File Details :</b>"
			+ "<br> 1) mis_voucher.properties. ",

	nickname = "SUBMIT MEAL VOUCHER CLAIMS", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = VoucherMealDeclarationBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = VoucherMealDeclarationBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = VoucherDeclarationURI.SUBMIT_MEAL_VOUCHER_CLAIMS, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> submitMealVoucherClaims(
			@Valid @ApiParam(name = "SubmitMealVoucherClaims", example = "1", 
			value = "{"
					+ "<br> empId: 1,"
					+ "<br> amount: 1000,"
					+ "<br> month: 3,"
					+ "<br> status: REQUESTED,"
					+ "<br> year: 2017,"
					+ "<br> "
					+ "}")
			@RequestBody EntityBeanList<VoucherMealDeclarationBean> bean,
			BindingResult binding) throws BusinessException,
			CommonCustomException, DataAccessException {
		LOG.startUsecase("Submit Gift Voucher Claims Controller");
		List<VoucherMealDeclarationBean> submitMealVoucherClaims1 = new ArrayList<VoucherMealDeclarationBean>();
		if (binding.hasErrors()) {
			return ValidationUtils.handleValidationErrors(binding);
		}
		LocalDateTime currentTime = LocalDateTime.now();
		Integer day = currentTime.getDayOfMonth();
		Month month = currentTime.getMonth();
		Integer year = currentTime.getYear();
		Integer nextYear = currentTime.getYear() + 1;
		Integer currentMonthValue = month.getValue();
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, currentTime.getYear());
		calendar.set(Calendar.MONTH, currentTime.getMonthValue());
		if (day > this.lastDate) {
			for (VoucherMealDeclarationBean voucherMealDeclarationBean : bean
					.getEntityList()) {
				int beanCurrentMonth = voucherMealDeclarationBean
						.getMealVoucherDeclarationMonth();
				if (((year.compareTo(nextYear) != 0) && (currentMonthValue
						.compareTo(beanCurrentMonth) > 0))
						|| (currentMonthValue.compareTo(beanCurrentMonth) < 0)) {
					submitMealVoucherClaims1 = voucherMealDeclarationSrvc
							.submitMealVoucherClaims(bean, currentMonthValue);
				} else {
					throw new CommonCustomException(
							"To Submit cancelling/updating Meal Voucher, Current date should be less than 23rd");
				}
			}
		} else {
			submitMealVoucherClaims1 = voucherMealDeclarationSrvc
					.submitMealVoucherClaims(bean, currentMonthValue);
		}
		LOG.endUsecase("Submit Gift Voucher Claims Controller");
		return ResponseEntity.status(HttpStatus.OK.value()).body(
				new MISResponse(HttpStatus.OK.value(), MISConstants.SUCCESS,
						"Meal Voucher Claims Submitted Successfully.",
						submitMealVoucherClaims1));
	}

	/**
	 * <Description getMealVoucherClaims:> This contains the methods used for
	 * the GET_MEAL_VOUCHER_CLAIMS for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws BusinessException
	 * @see
	 */
	@ApiOperation(tags="Voucher",value = "GET_MEAL_VOUCHER_CLAIMS."
			+ "To get meal voucher claims details.", httpMethod = "GET",
			notes = "This Service has been implemented to Get Meal Voucher Claims details."
			+ "<br> The Response will be the detailed of the Meal Voucher."
			+ "<br> <b>Table :</b>"
			+ "<br> 1)voucher_meal_dat_emp_declaration."
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Deatiled for only India Employees."
			+ "<br> <b>Property File Details :</b>"
			+ "<br> 1) mis_voucher.properties. ",

	nickname = "SUBMIT GIFT VOUCHER CLAIMS", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = VoucherMealDeclarationBO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = VoucherMealDeclarationBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = VoucherDeclarationURI.GET_MEAL_VOUCHER_CLAIMS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getMealVoucherClaims(
			@ApiParam(name = "fkMealVoucherDeclarationEmpId", example = "1", value = "fkMealVoucherDeclarationEmpId") @PathVariable("fkMealVoucherDeclarationEmpId") Integer fkMealVoucherDeclarationEmpId)
			throws BusinessException {
		LOG.startUsecase("Get Meal Voucher Claims Controller");
		List<VoucherMealDeclarationBean> getMealVoucherClaims = new ArrayList<VoucherMealDeclarationBean>();
		DatEmpProfessionalDetailBO employeeCheck = employeeProfessionalRepository
				.findByFkMainEmpDetailId(fkMealVoucherDeclarationEmpId);
		Integer empId = fkMealVoucherDeclarationEmpId;
		if (empId == null || empId == 0) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Employee ID cannot be null or 0"));
		} else if (employeeCheck == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Entered Employee ID is not available"));
		}
		try {
			getMealVoucherClaims = voucherMealDeclarationSrvc
					.getMealVoucherClaims(fkMealVoucherDeclarationEmpId);
		} catch (Exception e) {
			throw new BusinessException("exception ", e);
		}
		LOG.endUsecase("Get Meal Voucher Claims Controller");
		if (getMealVoucherClaims.size() > 0) {
			LOG.endUsecase("Get Meal Voucher Claims Controller");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Meal Voucher Claims are displayed.",
							getMealVoucherClaims));
		} else {
			return ResponseEntity
					.status(HttpStatus.OK.value())
					.body(new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Meal Voucher Claim details are Not Available for the EmpId in DB."));
		}
	}

	/**
	 * <Description getMealVoucherClaims:> This contains the methods used for
	 * the GET_MEAL_VOUCHER_CLAIMS for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws IOException
	 * @throws org.apache.poi.openxml4j.exceptions.InvalidFormatException
	 * @throws InvalidFormatException
	 * @throws ParsePropertyException
	 * @throws CommonCustomException
	 * @throws DRException
	 * @see
	 */
	@ApiOperation(tags="Voucher",
			value = "GET MEAL VOUCHER REPORT."
			+ "To GET MEAL VOUCHER CLAIMS Report .", httpMethod = "GET", notes = "This Service has been implemented to Get Meal Voucher Claims report."
			+ "<br>The Response will be the download the report of the Meal Voucher.<br>"
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1) voucher_meal_dat_emp_declaration. "
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Deatiled for only India Employees.<br>"
			+ "<br> <b>Property File Details :</b>"
			+ "<br> 1) mis_voucher.properties. "
			+ "<br> <b>Report Template Details :</b>"
			+ "<br> 1) mealVoucherTemplate.xls. ",
			nickname = "GET MEAL VOUCHER REPORT", 
			protocols = "HTTP",
			responseReference = "application/json",
			produces = "application/json",
			response = VoucherMealDeclarationSrvc.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Report Downloaded Successfully", response = VoucherMealDeclarationBO.class),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = VoucherDeclarationURI.GET_MEAL_VOUCHER_REPORT, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getReportMealVoucherClaims(
			HttpServletResponse response,
			@PathVariable("year") String mealVoucherDeclarationYear,
			@PathVariable("month") Integer mealVoucherDeclarationMonth)
			throws BusinessException, IOException, ParsePropertyException,
			org.apache.poi.openxml4j.exceptions.InvalidFormatException,
			CommonCustomException {
		LOG.startUsecase("Entered getReportMealVoucherClaims");
		boolean isReportGenerated = false;
		isReportGenerated = voucherMealDeclarationSrvc.getMealVoucherReport(
				mealVoucherDeclarationYear, mealVoucherDeclarationMonth,
				response);
		LOG.endUsecase("Exited getReportMealVoucherClaims");
		if (isReportGenerated) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Report generated successfully"));

		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Meal Voucher Claim Details are Not Found."));
		}

	}

	// Added by Kamal Anand
	/**
	 * <Description getMealVoucherClaims:> This contains the methods used for
	 * the GET_MEAL_VOUCHER_CLAIMS for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws BusinessException
	 * @throws DRException
	 * @see
	 */
	@ApiOperation(tags="Voucher",value = "GET MEAL VOUCHER LIST."
			+ "To GET MEAL VOUCHER CLAIMS LIST .", httpMethod = "GET", notes = "This Service has been implemented to Get Meal Voucher Claims Details."
			+ "<br>The Response will be the detailed of the Meal Voucher."
			+ "<br><b>Table Details : </b>"
			+ "<br> 1) voucher_meal_dat_emp_declaration."
			+ "<br><b>Rules : </b>"
			+ "<br> 1) Deatiled for only India Employees. <br><br>"
			+ "<br> <b>Property File Details :</b>"
			+ "<br> 1) mis_voucher.properties. ",
	nickname = "GET MEAL VOUCHER LIST", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = VoucherMealDeclarationSrvc.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = VoucherMealDeclarationBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Meal Voucher Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = VoucherDeclarationURI.GET_MEAL_VOUCHER_LIST, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<MISResponse> getReportMealVoucherClaimsList(
			@PathVariable("year") String mealVoucherDeclarationYear,
			@PathVariable("month") Integer mealVoucherDeclarationMonth)
			throws BusinessException {
		LOG.startUsecase("Entered getReportMealVoucherClaimsList");
		List<MealVoucherDeclarationBean> mealVoucherDetails = null;
		try {
			mealVoucherDetails = voucherMealDeclarationSrvc
					.generateMealVoucherClaimsDetails(
							mealVoucherDeclarationYear,
							mealVoucherDeclarationMonth);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
		if (mealVoucherDetails.size() > 0) {
			LOG.endUsecase("Exited getReportMealVoucherClaimsList");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Meal Voucher Claims Listed Successfully",
							mealVoucherDetails));
		} else {
			LOG.endUsecase("Exited getReportMealVoucherClaimsList");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS,
							"No Meal Voucher Claims List."));
		}

	}

	// End of Addition by Kamal Anand

	@ApiOperation(tags="Voucher",value = "GET MEAL VOUCHER REPORT FOR SELECTED VOUCHER IDS."
			+ "To GET MEAL VOUCHER CLAIMS Report .", httpMethod = "POST", notes = "This Service has been implemented to Get Meal Voucher Claims report."
			+ "<br>The Response will be the detailed of the Meal Voucher."
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1)voucher_meal_dat_emp_declaration. <br><br>"
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Deatiled for only India Employees. <br><br>"
			+ "<br> <b>Property File Details :</b>"
			+ "<br> 1) mis_voucher.properties. "
			+ "<br> <b>Report Template Details :</b>"
			+ "<br> 1) mealVoucherTemplate.xls. ",

	nickname = "GET MEAL VOUCHER REPORT", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = VoucherMealDeclarationSrvc.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = VoucherMealDeclarationBO.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "The created Program Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = VoucherDeclarationURI.GET_MEAL_VOUCHER_REPORT_BY_SELECTED_VOUCHER_ID, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> getReportMealVoucherClaimsForVoucherIds(
			@Valid @ApiParam(name = "VoucherMealDeclarationBO", example = "1", 
			value = "["
					+ "<br> {"
					+ "<br>VoucherDeclarationId:1,"
					+ " <br> },"
					+ "<br> {"
					+ "<br>VoucherDeclarationId:2,"
					+ "<br> }"
					+ "<br> ]")
			@RequestBody List<VoucherMealDeclarationBO> voucherIds,
			HttpServletResponse response) throws BusinessException,
			IOException, ParsePropertyException,
			org.apache.poi.openxml4j.exceptions.InvalidFormatException,
			CommonCustomException {
		LOG.endUsecase("Entered getReportMealVoucherClaimsForVoucherIds");
		boolean isReportGenerated = false;
		isReportGenerated = voucherMealDeclarationSrvc
				.getMealVoucherReportBySelectedVoucherId(voucherIds, response);
		LOG.endUsecase("Exited getReportMealVoucherClaimsForVoucherIds");
		if (isReportGenerated) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Report generated successfully"));

		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Meal Voucher Claim Details are Not Found."));
		}

	}
	
	/**
	 * <Description getMealVoucherClaims:> This contains the methods used for
	 * the MEAL_VOUCHER_REPORT_BASED_ON_STARTDATE_ENDDATE for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws IOException
	 * @throws org.apache.poi.openxml4j.exceptions.InvalidFormatException
	 * @throws InvalidFormatException
	 * @throws ParsePropertyException
	 * @throws CommonCustomException
	 * @throws DataAccessException 
	 * @throws DRException
	 * @see
	 */
	@ApiOperation(tags="Voucher",
			value = "MEAL_VOUCHER_REPORT_BASED_ON_STARTDATE_ENDDATE."
			+ "To GET MEAL_VOUCHER_REPORT_BASED_ON_STARTDATE_ENDDATE Report .", httpMethod = "POST", notes = "This Service has been implemented to Get Meal Voucher Claims report based on StartDate and EndDate."
			+ "<br>The Response will be the download the report of the Meal Voucher.<br>"
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1) voucher_meal_dat_emp_declaration. "
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Deatiled for only India Employees.<br>"
			+ "<br> <b>Property File Details :</b>"
			+ "<br> 1) mis_voucher.properties. "
			+ "<br> <b>Report Template Details :</b>"
			+ "<br> 1) mealVoucherTemplate.xls. ",
			nickname = "MEAL_VOUCHER_REPORT_BASED_ON_STARTDATE_ENDDATE", 
			protocols = "HTTP",
			responseReference = "application/json",
			produces = "application/json",
			response = VoucherMealDeclarationSrvc.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Report Downloaded Successfully", response = VoucherMealDeclarationBO.class),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = VoucherDeclarationURI.MEAL_VOUCHER_REPORT_BASED_ON_STARTDATE_ENDDATE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> mealVoucherReportBasedOnStartDateandEndDate(
			HttpServletResponse response, @RequestBody MealVoucherReportBasedOnStartDate inputBean)
			throws BusinessException, IOException, ParsePropertyException,
			org.apache.poi.openxml4j.exceptions.InvalidFormatException,
			CommonCustomException, DataAccessException {
		LOG.startUsecase("Entered getReportMealVoucherClaims");
		boolean isReportGenerated = false;
		isReportGenerated = voucherMealDeclarationSrvc.mealVoucherReportBasedOnStartDate(inputBean,response);
		LOG.endUsecase("Exited getReportMealVoucherClaims");
		if (isReportGenerated) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Report generated successfully"));

		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Meal Voucher Claim Details are Not Found."));
		}

	}
	
	/**
	 * <Description getMealVoucherClaims:> This contains the methods used for
	 * the MEAL_VOUCHER_REPORT_BASED_ON_STARTDATE_ENDDATE for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws IOException
	 * @throws org.apache.poi.openxml4j.exceptions.InvalidFormatException
	 * @throws InvalidFormatException
	 * @throws ParsePropertyException
	 * @throws CommonCustomException
	 * @throws DataAccessException 
	 * @throws org.apache.poi.openxml4j.exceptions.InvalidFormatException 
	 * @throws DRException
	 * @see
	 */
	@ApiOperation(tags="Voucher",
			value = "MEAL_VOUCHER_REPORT_BASED_ON_RANGE."
			+ "To GET MEAL_VOUCHER_REPORT_BASED_ON_RANGE Report .", httpMethod = "POST", notes = "This Service has been implemented to Get Meal Voucher Claims report based on StartDate and EndDate."
			+ "<br>The Response will be the download the report of the Meal Voucher.<br>"
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1) voucher_meal_dat_emp_declaration. "
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Deatiled for only India Employees.<br>"
			+ "<br> <b>Property File Details :</b>"
			+ "<br> 1) mis_voucher.properties. "
			+ "<br> <b>Report Template Details :</b>"
			+ "<br> 1) mealVoucherTemplate.xls. ",
			nickname = "MEAL_VOUCHER_REPORT_BASED_ON_STARTDATE_ENDDATE", 
			protocols = "HTTP",
			responseReference = "application/json",
			produces = "application/json",
			response = VoucherMealDeclarationSrvc.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Report Downloaded Successfully", response = VoucherMealDeclarationBO.class),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = VoucherDeclarationURI.MEAL_VOUCHER_MONTH_RANGE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> mealVoucherReportRange(
			 @RequestBody MealVoucherReportBasedOnStartDate inputBean)
			throws BusinessException, IOException, ParsePropertyException,
			CommonCustomException, DataAccessException, org.apache.poi.openxml4j.exceptions.InvalidFormatException {
		LOG.startUsecase("Entered mealVoucherReportRange");
		List<MealVoucherReportBeanBasedOnRanges> isReportGenerated = new ArrayList<MealVoucherReportBeanBasedOnRanges>();
		isReportGenerated = voucherMealDeclarationSrvc.mealVoucherReportRange(inputBean);
		LOG.endUsecase("Exited mealVoucherReportRange");
		if (isReportGenerated.size()>0) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Report generated successfully",isReportGenerated));

		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Meal Voucher Claim Details are Not Found."));
		}

	}
	
	//New meal voucher report part with list of pkVoucher id
	/**
	 * <Description getMealVoucherClaims:> This contains the methods used for
	 * the MEAL_VOUCHER_REPORT_BASED_ON_RANGE_BY_LIST_VOUCHER_ID for the Controller operation.
	 * 
	 * @author THBS
	 * @version 1.0
	 * @throws IOException
	 * @throws org.apache.poi.openxml4j.exceptions.InvalidFormatException
	 * @throws InvalidFormatException
	 * @throws ParsePropertyException
	 * @throws CommonCustomException
	 * @throws DataAccessException 
	 * @throws org.apache.poi.openxml4j.exceptions.InvalidFormatException 
	 * @throws DRException
	 * @see
	 */
	@ApiOperation(tags="Voucher",
			value = "MEAL_VOUCHER_REPORT_BASED_ON_RANGE_BY_LIST_VOUCHER_ID."
			+ "To GET MEAL_VOUCHER_REPORT_BASED_ON_RANGE_BY_LIST_VOUCHER_ID Report .", httpMethod = "POST", notes = "This Service has been implemented to Get Meal Voucher Claims report based on StartDate and EndDate."
			+ "<br>The Response will be the download the report of the Meal Voucher.<br>"
			+ "<br> <b>Table Details :</b>"
			+ "<br> 1) voucher_meal_dat_emp_declaration. "
			+ "<br> <b>Rules :</b>"
			+ "<br> 1) Deatiled for only India Employees.<br>"
			+ "<br> <b>Property File Details :</b>"
			+ "<br> 1) mis_voucher.properties. "
			+ "<br> <b>Report Template Details :</b>"
			+ "<br> 1) mealVoucherTemplate.xls. ",
			nickname = "MEAL_VOUCHER_REPORT_BASED_ON_STARTDATE_ENDDATE", 
			protocols = "HTTP",
			responseReference = "application/json",
			produces = "application/json",
			response = VoucherMealDeclarationSrvc.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Report Downloaded Successfully", response = VoucherMealDeclarationBO.class),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = VoucherDeclarationURI.MEAL_VOUCHER_MONTH_RANGE_LIST_OF_VOUCHER_ID, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<MISResponse> mealVoucherReportBasedOnStartDateandEndDateWithListOfPkId(
			HttpServletResponse response, @RequestBody VoucherListOfVoucherIdReportBean inputBean)
			throws BusinessException, IOException, ParsePropertyException,
			org.apache.poi.openxml4j.exceptions.InvalidFormatException,
			CommonCustomException, DataAccessException {
		LOG.startUsecase("Entered mealVoucherReportBasedOnStartDateandEndDateWithListOfPkId");
		boolean isReportGenerated = false;
		isReportGenerated = voucherMealDeclarationSrvc.mealVoucherReportBasedOnStartDateWithPkVoucherId(inputBean,response);
		LOG.endUsecase("Exited mealVoucherReportBasedOnStartDateandEndDateWithListOfPkId");
		if (isReportGenerated) {
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Report generated successfully"));

		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE,
							"Meal Voucher Claim Details are Not Found."));
		}

	}
}
