package com.thbs.mis.voucher.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "voucher_meal_dat_eligibility_amount")
@NamedQuery(name = "VoucherMealDatEligibilityAmountBO.findAll", query = "SELECT d FROM VoucherMealDatEligibilityAmountBO d")
public class VoucherMealDatEligibilityAmountBO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_voucher_eligibilty_id")
	private Integer pkVoucherEligibiltyId;
	
	@Column(name = "fk_voucher_eligibility_emp_id")
	private Integer fkVoucherEligibilityEmpId;
	
	@Column(name = "eligibility_amount")
	private Integer eligibilityAmount;

	public Integer getPkVoucherEligibiltyId() {
		return pkVoucherEligibiltyId;
	}

	public void setPkVoucherEligibiltyId(Integer pkVoucherEligibiltyId) {
		this.pkVoucherEligibiltyId = pkVoucherEligibiltyId;
	}

	public Integer getEligibilityAmount() {
		return eligibilityAmount;
	}

	public void setEligibilityAmount(Integer eligibilityAmount) {
		this.eligibilityAmount = eligibilityAmount;
	}

	public Integer getFkVoucherEligibilityEmpId() {
		return fkVoucherEligibilityEmpId;
	}

	public void setFkVoucherEligibilityEmpId(Integer fkVoucherEligibilityEmpId) {
		this.fkVoucherEligibilityEmpId = fkVoucherEligibilityEmpId;
	}

	@Override
	public String toString() {
		return "VoucherMealDatEligibilityAmountBO [pkVoucherEligibiltyId="
				+ pkVoucherEligibiltyId + ", fkVoucherEligibilityEmpId="
				+ fkVoucherEligibilityEmpId + ", eligibilityAmount="
				+ eligibilityAmount + "]";
	}
}
