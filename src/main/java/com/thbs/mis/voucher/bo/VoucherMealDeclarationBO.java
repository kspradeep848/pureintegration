package com.thbs.mis.voucher.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;

/**
 * The persistent class for the dat_emp_meal_voucher_declaration database table.
 * 
 */
@Entity
@Table(name = "voucher_meal_dat_emp_declaration")
@NamedQuery(name = "VoucherMealDeclarationBO.findAll", query = "SELECT d FROM VoucherMealDeclarationBO d")
@NamedNativeQueries({
	@NamedNativeQuery( name = "VoucherMealDeclarationBO.getFinancialYearReport", 	
			query =" SELECT * FROM voucher_meal_dat_emp_declaration " 
			+" WHERE meal_voucher_declaration_month >= :voucherStartMonth " 
			+" AND meal_voucher_declaration_year = :voucherStartYear "
			+" AND meal_voucher_declaration_status ='REQUESTED' "
			+" UNION "
			+" SELECT * FROM voucher_meal_dat_emp_declaration "
			+" WHERE meal_voucher_declaration_month <= :voucherEndMonth "
			+" AND meal_voucher_declaration_year = :voucherEndYear "
			+" AND meal_voucher_declaration_status ='REQUESTED' "
			+" ORDER BY fk_meal_voucher_declaration_by_emp_id ",resultClass = VoucherMealDeclarationBO.class)
})
public class VoucherMealDeclarationBO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_meal_voucher_declaration_id")
	private Integer pkMealVoucherDeclarationId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "meal_voucher_created_date")
	private Date mealVoucherCreatedDate;

	@Column(name = "meal_voucher_declaration_amount")
	private Integer mealVoucherDeclarationAmount;

	@Column(name = "meal_voucher_declaration_month")
	private Integer mealVoucherDeclarationMonth;

	@Column(name = "meal_voucher_declaration_status")
	private String mealVoucherDeclarationStatus;

	@Column(name = "meal_voucher_declaration_year")
	private String mealVoucherDeclarationYear;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "meal_voucher_modified_date")
	private Date mealVoucherModifiedDate;

	@Column(name = "fk_meal_voucher_declaration_by_emp_id")
	private Integer fkMealVoucherDeclarationEmpId;

	// bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name = "fk_meal_voucher_declaration_by_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail;

	public VoucherMealDeclarationBO() {
	}

	public Integer getPkMealVoucherDeclarationId() {
		return this.pkMealVoucherDeclarationId;
	}

	public void setPkMealVoucherDeclarationId(Integer pkMealVoucherDeclarationId) {
		this.pkMealVoucherDeclarationId = pkMealVoucherDeclarationId;
	}

	public Date getMealVoucherCreatedDate() {
		return this.mealVoucherCreatedDate;
	}

	public void setMealVoucherCreatedDate(Date mealVoucherCreatedDate) {
		this.mealVoucherCreatedDate = mealVoucherCreatedDate;
	}

	public Integer getMealVoucherDeclarationAmount() {
		return this.mealVoucherDeclarationAmount;
	}

	public void setMealVoucherDeclarationAmount(
			Integer mealVoucherDeclarationAmount) {
		this.mealVoucherDeclarationAmount = mealVoucherDeclarationAmount;
	}

	public Integer getMealVoucherDeclarationMonth() {
		return this.mealVoucherDeclarationMonth;
	}

	public void setMealVoucherDeclarationMonth(
			Integer mealVoucherDeclarationMonth) {
		this.mealVoucherDeclarationMonth = mealVoucherDeclarationMonth;
	}

	public String getMealVoucherDeclarationStatus() {
		return this.mealVoucherDeclarationStatus;
	}

	public void setMealVoucherDeclarationStatus(
			String mealVoucherDeclarationStatus) {
		this.mealVoucherDeclarationStatus = mealVoucherDeclarationStatus;
	}

	public String getMealVoucherDeclarationYear() {
		return this.mealVoucherDeclarationYear;
	}

	public void setMealVoucherDeclarationYear(String mealVoucherDeclarationYear) {
		this.mealVoucherDeclarationYear = mealVoucherDeclarationYear;
	}

	public Date getMealVoucherModifiedDate() {
		return this.mealVoucherModifiedDate;
	}

	public void setMealVoucherModifiedDate(Date mealVoucherModifiedDate) {
		this.mealVoucherModifiedDate = mealVoucherModifiedDate;
	}

	public DatEmpDetailBO getDatEmpDetail() {
		return this.datEmpDetail;
	}

	public void setDatEmpDetail(DatEmpDetailBO datEmpDetail) {
		this.datEmpDetail = datEmpDetail;
	}

	public Integer getFkMealVoucherDeclarationEmpId() {
		return fkMealVoucherDeclarationEmpId;
	}

	public void setFkMealVoucherDeclarationEmpId(
			Integer fkMealVoucherDeclarationEmpId) {
		this.fkMealVoucherDeclarationEmpId = fkMealVoucherDeclarationEmpId;
	}

	@Override
	public String toString() {
		return "VoucherMealDeclarationBO [pkMealVoucherDeclarationId="
				+ pkMealVoucherDeclarationId + ", mealVoucherCreatedDate="
				+ mealVoucherCreatedDate + ", mealVoucherDeclarationAmount="
				+ mealVoucherDeclarationAmount
				+ ", mealVoucherDeclarationMonth="
				+ mealVoucherDeclarationMonth
				+ ", mealVoucherDeclarationStatus="
				+ mealVoucherDeclarationStatus
				+ ", mealVoucherDeclarationYear=" + mealVoucherDeclarationYear
				+ ", mealVoucherModifiedDate=" + mealVoucherModifiedDate
				+ ", fkMealVoucherDeclarationEmpId="
				+ fkMealVoucherDeclarationEmpId + ", datEmpDetail="
				+ datEmpDetail + "]";
	}
}