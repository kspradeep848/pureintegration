package com.thbs.mis.voucher.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thbs.mis.common.bo.DatEmpDetailBO;

/**
 * The persistent class for the dat_emp_gift_voucher_declaration database table.
 * 
 */
@Entity
@Table(name = "voucher_gift_dat_emp_declaration")
@NamedQuery(name = "VoucherGiftDeclarationBO.findAll", query = "SELECT d FROM VoucherGiftDeclarationBO d")
public class VoucherGiftDeclarationBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_gift_voucher_declaration_id")
	private Integer pkGiftVoucherDeclarationId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "gift_voucher_created_date")
	private Date giftVoucherCreatedDate;

	@Column(name = "gift_voucher_declaration_amount")
	private Integer giftVoucherDeclarationAmount;

	@Column(name = "gift_voucher_declaration_status")
	private String giftVoucherDeclarationStatus;

	@Column(name = "gift_voucher_declaration_year")
	private String giftVoucherDeclarationYear;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "gift_voucher_modified_date")
	private Date giftVoucherModifiedDate;

	@Column(name = "fk_gift_voucher_declaration_by_emp_id")
	private Integer fkGiftVoucherDeclarationEmpId;

	// bi-directional many-to-one association to DatEmpDetail
	@OneToOne
	@JoinColumn(name = "fk_gift_voucher_declaration_by_emp_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail;

	public VoucherGiftDeclarationBO() {
	}

	public Integer getPkGiftVoucherDeclarationId() {
		return this.pkGiftVoucherDeclarationId;
	}

	public void setPkGiftVoucherDeclarationId(Integer pkGiftVoucherDeclarationId) {
		this.pkGiftVoucherDeclarationId = pkGiftVoucherDeclarationId;
	}

	public Date getGiftVoucherCreatedDate() {
		return this.giftVoucherCreatedDate;
	}

	public void setGiftVoucherCreatedDate(Date giftVoucherCreatedDate) {
		this.giftVoucherCreatedDate = giftVoucherCreatedDate;
	}

	public Integer getGiftVoucherDeclarationAmount() {
		return this.giftVoucherDeclarationAmount;
	}

	public void setGiftVoucherDeclarationAmount(
			Integer giftVoucherDeclarationAmount) {
		this.giftVoucherDeclarationAmount = giftVoucherDeclarationAmount;
	}

	public String getGiftVoucherDeclarationStatus() {
		return this.giftVoucherDeclarationStatus;
	}

	public void setGiftVoucherDeclarationStatus(
			String giftVoucherDeclarationStatus) {
		this.giftVoucherDeclarationStatus = giftVoucherDeclarationStatus;
	}

	public String getGiftVoucherDeclarationYear() {
		return this.giftVoucherDeclarationYear;
	}

	public void setGiftVoucherDeclarationYear(String giftVoucherDeclarationYear) {
		this.giftVoucherDeclarationYear = giftVoucherDeclarationYear;
	}

	public Date getGiftVoucherModifiedDate() {
		return this.giftVoucherModifiedDate;
	}

	public void setGiftVoucherModifiedDate(Date giftVoucherModifiedDate) {
		this.giftVoucherModifiedDate = giftVoucherModifiedDate;
	}

	public DatEmpDetailBO getDatEmpDetail() {
		return this.datEmpDetail;
	}

	public void setDatEmpDetail(DatEmpDetailBO datEmpDetail) {
		this.datEmpDetail = datEmpDetail;
	}

	public Integer getFkGiftVoucherDeclarationEmpId() {
		return fkGiftVoucherDeclarationEmpId;
	}

	public void setFkGiftVoucherDeclarationEmpId(
			Integer fkGiftVoucherDeclarationEmpId) {
		this.fkGiftVoucherDeclarationEmpId = fkGiftVoucherDeclarationEmpId;
	}

	@Override
	public String toString() {
		return "VoucherGiftDeclarationBO [pkGiftVoucherDeclarationId="
				+ pkGiftVoucherDeclarationId + ", giftVoucherCreatedDate="
				+ giftVoucherCreatedDate + ", giftVoucherDeclarationAmount="
				+ giftVoucherDeclarationAmount
				+ ", giftVoucherDeclarationStatus="
				+ giftVoucherDeclarationStatus
				+ ", giftVoucherDeclarationYear=" + giftVoucherDeclarationYear
				+ ", giftVoucherModifiedDate=" + giftVoucherModifiedDate
				+ ", fkGiftVoucherDeclarationEmpId="
				+ fkGiftVoucherDeclarationEmpId + ", datEmpDetail="
				+ datEmpDetail + "]";
	}

}