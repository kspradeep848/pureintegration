/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  ViewUanDetailsInputBean.java                      */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  May 05, 2017                                      */
/*                                                                   */
/*  Description :  This class is the Input Bean for View KYC Details */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* May 05, 2017     THBS     1.0        Initial version created      */
/*********************************************************************/
package com.thbs.mis.uan.bean;

import java.util.Date;
import java.util.List;

public class ViewUanDetailsInputBean {

	private String uanStatus;
	private String panStatus;
	private List<Integer> empIdList;
	private Date startDate;
	private Date endDate;

	public String getUanStatus() {
		return uanStatus;
	}

	public void setUanStatus(String uanStatus) {
		this.uanStatus = uanStatus;
	}

	public String getPanStatus() {
		return panStatus;
	}

	public void setPanStatus(String panStatus) {
		this.panStatus = panStatus;
	}

	public List<Integer> getEmpIdList() {
		return empIdList;
	}

	public void setEmpIdList(List<Integer> empIdList) {
		this.empIdList = empIdList;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "ViewUanDetailsInputBean [uanStatus=" + uanStatus
				+ ", panStatus=" + panStatus + ", empIdList=" + empIdList
				+ ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}
}
