/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmpKycDetailsOutputBean.java                      */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  April 05, 2017                                    */
/*                                                                   */
/*  Description :  This class is the Output Bean for KYC Details     */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* April 05, 2017     THBS     1.0        Initial version created    */
/*********************************************************************/
package com.thbs.mis.uan.bean;

import java.util.Date;

public class EmpKycDetailsOutputBean {

	private Integer pkEmpKycDetailId;
	private String empBankAccountIfscCode;
	private String empBankAccountNo;
	private String empDependentName;
	private Byte empDependentRelationshipId;
	private String empDependentRelationshipName;
	private String empKycEmailId;
	private String empKycMobileNo;
	private String empMaritalStatusName;
	private Byte empMaritalStatusId;
	private String empNameAsPerAadharCard;
	private String empNameAsPerBankAccount;
	private String empNameAsPerPanCard;
	private String empNameAsPerUan;
	private String empPanNo;
	private String empUanNo;
	private Integer fkEmpDetailId;
	private String hasUanFlag;
	private Date empDOJ;
	private String aadharNo;
	private Date empDOB;
	private String empName;
	private Byte genderId;
	private String genderName;
	private Integer empId;
	private String empBankName;
	private Date lastUpdatedOn;
	private String isAadharUploaded;
	private String isPanUploaded;
	private String aadharFileName;
	private String panFileName;

	public Date getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(Date lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public String getEmpBankName() {
		return empBankName;
	}

	public void setEmpBankName(String empBankName) {
		this.empBankName = empBankName;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Integer getPkEmpKycDetailId() {
		return pkEmpKycDetailId;
	}

	public void setPkEmpKycDetailId(Integer pkEmpKycDetailId) {
		this.pkEmpKycDetailId = pkEmpKycDetailId;
	}

	public String getEmpBankAccountIfscCode() {
		return empBankAccountIfscCode;
	}

	public void setEmpBankAccountIfscCode(String empBankAccountIfscCode) {
		this.empBankAccountIfscCode = empBankAccountIfscCode;
	}

	public String getEmpBankAccountNo() {
		return empBankAccountNo;
	}

	public void setEmpBankAccountNo(String empBankAccountNo) {
		this.empBankAccountNo = empBankAccountNo;
	}

	public String getEmpDependentName() {
		return empDependentName;
	}

	public void setEmpDependentName(String empDependentName) {
		this.empDependentName = empDependentName;
	}

	public Byte getEmpDependentRelationshipId() {
		return empDependentRelationshipId;
	}

	public void setEmpDependentRelationshipId(Byte empDependentRelationshipId) {
		this.empDependentRelationshipId = empDependentRelationshipId;
	}

	public String getEmpDependentRelationshipName() {
		return empDependentRelationshipName;
	}

	public void setEmpDependentRelationshipName(
			String empDependentRelationshipName) {
		this.empDependentRelationshipName = empDependentRelationshipName;
	}

	public String getEmpKycEmailId() {
		return empKycEmailId;
	}

	public void setEmpKycEmailId(String empKycEmailId) {
		this.empKycEmailId = empKycEmailId;
	}

	public String getEmpKycMobileNo() {
		return empKycMobileNo;
	}

	public void setEmpKycMobileNo(String empKycMobileNo) {
		this.empKycMobileNo = empKycMobileNo;
	}

	public String getEmpMaritalStatusName() {
		return empMaritalStatusName;
	}

	public void setEmpMaritalStatusName(String empMaritalStatusName) {
		this.empMaritalStatusName = empMaritalStatusName;
	}

	public Byte getEmpMaritalStatusId() {
		return empMaritalStatusId;
	}

	public void setEmpMaritalStatusId(Byte empMaritalStatusId) {
		this.empMaritalStatusId = empMaritalStatusId;
	}

	public String getEmpNameAsPerAadharCard() {
		return empNameAsPerAadharCard;
	}

	public void setEmpNameAsPerAadharCard(String empNameAsPerAadharCard) {
		this.empNameAsPerAadharCard = empNameAsPerAadharCard;
	}

	public String getEmpNameAsPerBankAccount() {
		return empNameAsPerBankAccount;
	}

	public void setEmpNameAsPerBankAccount(String empNameAsPerBankAccount) {
		this.empNameAsPerBankAccount = empNameAsPerBankAccount;
	}

	public String getEmpNameAsPerPanCard() {
		return empNameAsPerPanCard;
	}

	public void setEmpNameAsPerPanCard(String empNameAsPerPanCard) {
		this.empNameAsPerPanCard = empNameAsPerPanCard;
	}

	public String getEmpNameAsPerUan() {
		return empNameAsPerUan;
	}

	public void setEmpNameAsPerUan(String empNameAsPerUan) {
		this.empNameAsPerUan = empNameAsPerUan;
	}

	public String getEmpPanNo() {
		return empPanNo;
	}

	public void setEmpPanNo(String empPanNo) {
		this.empPanNo = empPanNo;
	}

	public String getEmpUanNo() {
		return empUanNo;
	}

	public void setEmpUanNo(String empUanNo) {
		this.empUanNo = empUanNo;
	}

	public Integer getFkEmpDetailId() {
		return fkEmpDetailId;
	}

	public void setFkEmpDetailId(Integer fkEmpDetailId) {
		this.fkEmpDetailId = fkEmpDetailId;
	}

	public String getHasUanFlag() {
		return hasUanFlag;
	}

	public void setHasUanFlag(String hasUanFlag) {
		this.hasUanFlag = hasUanFlag;
	}

	public Date getEmpDOJ() {
		return empDOJ;
	}

	public void setEmpDOJ(Date empDOJ) {
		this.empDOJ = empDOJ;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public Date getEmpDOB() {
		return empDOB;
	}

	public void setEmpDOB(Date empDOB) {
		this.empDOB = empDOB;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Byte getGenderId() {
		return genderId;
	}

	public void setGenderId(Byte genderId) {
		this.genderId = genderId;
	}

	public String getGenderName() {
		return genderName;
	}

	public void setGenderName(String genderName) {
		this.genderName = genderName;
	}

	public String getIsAadharUploaded() {
		return isAadharUploaded;
	}

	public void setIsAadharUploaded(String isAadharUploaded) {
		this.isAadharUploaded = isAadharUploaded;
	}

	public String getIsPanUploaded() {
		return isPanUploaded;
	}

	public void setIsPanUploaded(String isPanUploaded) {
		this.isPanUploaded = isPanUploaded;
	}

	public String getAadharFileName() {
		return aadharFileName;
	}

	public void setAadharFileName(String aadharFileName) {
		this.aadharFileName = aadharFileName;
	}

	public String getPanFileName() {
		return panFileName;
	}

	public void setPanFileName(String panFileName) {
		this.panFileName = panFileName;
	}

	@Override
	public String toString() {
		return "EmpKycDetailsOutputBean [pkEmpKycDetailId=" + pkEmpKycDetailId
				+ ", empBankAccountIfscCode=" + empBankAccountIfscCode
				+ ", empBankAccountNo=" + empBankAccountNo
				+ ", empDependentName=" + empDependentName
				+ ", empDependentRelationshipId=" + empDependentRelationshipId
				+ ", empDependentRelationshipName="
				+ empDependentRelationshipName + ", empKycEmailId="
				+ empKycEmailId + ", empKycMobileNo=" + empKycMobileNo
				+ ", empMaritalStatusName=" + empMaritalStatusName
				+ ", empMaritalStatusId=" + empMaritalStatusId
				+ ", empNameAsPerAadharCard=" + empNameAsPerAadharCard
				+ ", empNameAsPerBankAccount=" + empNameAsPerBankAccount
				+ ", empNameAsPerPanCard=" + empNameAsPerPanCard
				+ ", empNameAsPerUan=" + empNameAsPerUan + ", empPanNo="
				+ empPanNo + ", empUanNo=" + empUanNo + ", fkEmpDetailId="
				+ fkEmpDetailId + ", hasUanFlag=" + hasUanFlag + ", empDOJ="
				+ empDOJ + ", aadharNo=" + aadharNo + ", empDOB=" + empDOB
				+ ", empName=" + empName + ", genderId=" + genderId
				+ ", genderName=" + genderName + ", empId=" + empId
				+ ", empBankName=" + empBankName + ", lastUpdatedOn="
				+ lastUpdatedOn + ", isAadharUploaded=" + isAadharUploaded
				+ ", isPanUploaded=" + isPanUploaded + ", aadharFileName="
				+ aadharFileName + ", panFileName=" + panFileName + "]";
	}

}