/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  SubmitKycDetailsInputBean.java                    */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  April 04, 2017                                    */
/*                                                                   */
/*  Description :  This class is the Input Bean for create KYC       */
/* 				   Details                                           */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* April 04, 2017     THBS     1.0        Initial version created    */
/*********************************************************************/
package com.thbs.mis.uan.bean;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.thbs.mis.framework.customValidation.annotation.training.Employee_Id_Not_Found;

public class SubmitKycDetailsInputBean {

	@Employee_Id_Not_Found
	private Integer empId;

	@Length(max = 30, message = "Bank IFSC code can't exceed 30 chars")
	private String empBankAccountIfscCode;

	@Length(max = 20, message = "Bank Account Number can't exceed 20 chars")
	private String empBankAccountNo;

	@Length(max = 80, message = "Employee Dependent name can't exceed 80 chars")
	private String empDependentName;

	@Min(1)
	@Max(2)
	@NumberFormat(style = Style.NUMBER, pattern = "Relationship id should be a numeric value")
	private Byte empDependentRelationshipId;

	@Length(max = 50, message = "KYC Email Id can't exceed 50 chars")
	private String empKycEmailId;

	@Length(max = 14, message = "KYC Mobile Number can't exceed 14 chars")
	private String empKycMobileNo;

	@Min(1)
	@Max(5)
	@NumberFormat(style = Style.NUMBER, pattern = "Marital Status id should be a numeric value")
	private Byte empMaritalStatusId;

	@Length(max = 80, message = "Employee Name As Per Aadhar Card can't exceed 80 chars")
	private String empNameAsPerAadharCard;

	@Length(max = 80, message = "Employee Name As Per Bank Account can't exceed 80 chars")
	private String empNameAsPerBankAccount;

	@Length(max = 80, message = "Employee Name As Per PAN Card can't exceed 80 chars")
	private String empNameAsPerPanCard;

	@Length(max = 80, message = "Employee Name As Per UAN can't exceed 80 chars")
	private String empNameAsPerUan;

	@Length(max = 20, message = "Employee PAN No can't exceed 20 chars")
	private String empPanNo;

	@Length(max = 20, message = "UAN Number can't exceed 20 chars")
	private String empUanNo;

	private String hasUanFlag;

	@Length(max = 12, message = "Aadhar Number can't exceed 12 chars")
	private String aadharNo;

	@Length(max = 60, message = "Employee Bank Name can't exceed 60 chars")
	private String empBankName;

	public String getEmpBankName() {
		return empBankName;
	}

	public void setEmpBankName(String empBankName) {
		this.empBankName = empBankName;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpBankAccountIfscCode() {
		return empBankAccountIfscCode;
	}

	public void setEmpBankAccountIfscCode(String empBankAccountIfscCode) {
		this.empBankAccountIfscCode = empBankAccountIfscCode;
	}

	public String getEmpBankAccountNo() {
		return empBankAccountNo;
	}

	public void setEmpBankAccountNo(String empBankAccountNo) {
		this.empBankAccountNo = empBankAccountNo;
	}

	public String getEmpDependentName() {
		return empDependentName;
	}

	public void setEmpDependentName(String empDependentName) {
		this.empDependentName = empDependentName;
	}

	public Byte getEmpDependentRelationshipId() {
		return empDependentRelationshipId;
	}

	public void setEmpDependentRelationshipId(Byte empDependentRelationshipId) {
		this.empDependentRelationshipId = empDependentRelationshipId;
	}

	public String getEmpKycEmailId() {
		return empKycEmailId;
	}

	public void setEmpKycEmailId(String empKycEmailId) {
		this.empKycEmailId = empKycEmailId;
	}

	public String getEmpKycMobileNo() {
		return empKycMobileNo;
	}

	public void setEmpKycMobileNo(String empKycMobileNo) {
		this.empKycMobileNo = empKycMobileNo;
	}

	public Byte getEmpMaritalStatusId() {
		return empMaritalStatusId;
	}

	public void setEmpMaritalStatusId(Byte empMaritalStatusId) {
		this.empMaritalStatusId = empMaritalStatusId;
	}

	public String getEmpNameAsPerAadharCard() {
		return empNameAsPerAadharCard;
	}

	public void setEmpNameAsPerAadharCard(String empNameAsPerAadharCard) {
		this.empNameAsPerAadharCard = empNameAsPerAadharCard;
	}

	public String getEmpNameAsPerBankAccount() {
		return empNameAsPerBankAccount;
	}

	public void setEmpNameAsPerBankAccount(String empNameAsPerBankAccount) {
		this.empNameAsPerBankAccount = empNameAsPerBankAccount;
	}

	public String getEmpNameAsPerPanCard() {
		return empNameAsPerPanCard;
	}

	public void setEmpNameAsPerPanCard(String empNameAsPerPanCard) {
		this.empNameAsPerPanCard = empNameAsPerPanCard;
	}

	public String getEmpNameAsPerUan() {
		return empNameAsPerUan;
	}

	public void setEmpNameAsPerUan(String empNameAsPerUan) {
		this.empNameAsPerUan = empNameAsPerUan;
	}

	public String getEmpPanNo() {
		return empPanNo;
	}

	public void setEmpPanNo(String empPanNo) {
		this.empPanNo = empPanNo;
	}

	public String getEmpUanNo() {
		return empUanNo;
	}

	public void setEmpUanNo(String empUanNo) {
		this.empUanNo = empUanNo;
	}

	public String getHasUanFlag() {
		return hasUanFlag;
	}

	public void setHasUanFlag(String hasUanFlag) {
		this.hasUanFlag = hasUanFlag;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	@Override
	public String toString() {
		return "SubmitKycDetailsInputBean [empId=" + empId
				+ ", empBankAccountIfscCode=" + empBankAccountIfscCode
				+ ", empBankAccountNo=" + empBankAccountNo
				+ ", empDependentName=" + empDependentName
				+ ", empDependentRelationshipId=" + empDependentRelationshipId
				+ ", empKycEmailId=" + empKycEmailId + ", empKycMobileNo="
				+ empKycMobileNo + ", empMaritalStatusId=" + empMaritalStatusId
				+ ", empNameAsPerAadharCard=" + empNameAsPerAadharCard
				+ ", empNameAsPerBankAccount=" + empNameAsPerBankAccount
				+ ", empNameAsPerPanCard=" + empNameAsPerPanCard
				+ ", empNameAsPerUan=" + empNameAsPerUan + ", empPanNo="
				+ empPanNo + ", empUanNo=" + empUanNo + ", hasUanFlag="
				+ hasUanFlag + ", aadharNo=" + aadharNo + ", empBankName="
				+ empBankName + "]";
	}

}
