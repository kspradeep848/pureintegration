/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  UanURIConstants.java                              */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  April 04, 2017                                    */
/*                                                                   */
/*  Description :  This class contails URL's for KYC Details         */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* April 04, 2017     THBS     1.0        Initial version created    */
/*********************************************************************/
package com.thbs.mis.uan.constants;

public class UanURIConstants {

	public static final String GET_UAN_DETAILS = "/uan/details/{empId}";

	public static final String GET_UAN_REPORT = "/uan/details/report";

	public static final String CREATE_UAN_DETAILS = "/uan/details/create";

	public static final String UPDATE_UAN_DETAILS = "/uan/details/update";

	public static final String VIEW_UAN_DETAILS = "/uan/details/view";

	public static final String VIEW_UAN_DETAILS_REPORT = "/uan/details/view/report";
	
	public static final String DOWNLOAD_KYC_DOCUMENT = "/uan/{empId}/download/{fileName}";

}
