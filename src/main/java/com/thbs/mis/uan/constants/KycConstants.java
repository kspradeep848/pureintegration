package com.thbs.mis.uan.constants;

public interface KycConstants {
	
	public static final String YES_HAS_UAN = "YES_HAS_UAN";
	
	public static final String NO_UAN = "NO_UAN";

}
