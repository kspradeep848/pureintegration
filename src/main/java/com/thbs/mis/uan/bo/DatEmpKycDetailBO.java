/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  DatEmpKycDetailBO.java                            */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  April 04, 2017                                    */
/*                                                                   */
/*  Description :  This class is BO class for KYC Details            */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* April 04, 2017     THBS     1.0        Initial version created    */
/*********************************************************************/
package com.thbs.mis.uan.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.thbs.mis.common.bo.DatEmpDetailBO;
import com.thbs.mis.common.bo.DatEmpPersonalDetailBO;
import com.thbs.mis.common.bo.DatEmpProfessionalDetailBO;
 
/**
 * The persistent class for the dat_emp_kyc_detail database table.
 * 
 */
@Entity
@Table(name = "dat_emp_kyc_detail")
@NamedQuery(name = "DatEmpKycDetailBO.findAll", query = "SELECT d FROM DatEmpKycDetailBO d")
public class DatEmpKycDetailBO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_emp_kyc_detail_id")
	private Integer pkEmpKycDetailId;

	@Column(name = "emp_bank_account_ifsc_code")
	private String empBankAccountIfscCode;

	@Column(name = "emp_bank_account_no")
	private String empBankAccountNo;

	@Column(name = "emp_dependent_name")
	private String empDependentName;

	@Column(name = "emp_dependent_relationship")
	private Byte empDependentRelationship;

	@Column(name = "emp_kyc_email_id")
	private String empKycEmailId;

	@Column(name = "emp_voter_id_no")
	private String empVoterIdNo;
	
	@Column(name = "emp_name_as_per_voterid")
	private String empNameAsPerVoterId;
	
	@Column(name = "emp_passport_no")
	private String empPassportNo;
	
	@Column(name = "emp_name_as_per_passport")
	private String empNameAsPerPassport;
	
	@Column(name = "emp_passport_date_of_issue")
	private Date passportDateOfIssue;
	
	@Column(name = "emp_passport_place_of_issue")
	private String passportplaceOfIssue;
	
	@Column(name = "emp_passport_expiry_date")
	private Date passportExpiryDate;
	
	@Column(name = "emp_kyc_mobile_no")
	private String empKycMobileNo;

	@Column(name = "emp_marital_status")
	private Byte empMaritalStatus;

	@Column(name = "emp_name_as_per_aadhar_card")
	private String empNameAsPerAadharCard;

	@Column(name = "emp_name_as_per_bank_account")
	private String empNameAsPerBankAccount;

	@Column(name = "emp_name_as_per_pan_card")
	private String empNameAsPerPanCard;

	@Column(name = "emp_name_as_per_uan")
	private String empNameAsPerUan;

	@Column(name = "emp_pan_no")
	private String empPanNo;

	@Column(name = "emp_uan_no")
	private String empUanNo;

	@Column(name = "fk_emp_detail_id")
	private Integer fkEmpDetailId;

	@Column(name = "has_uan_flag")
	private String hasUanFlag;

	@Column(name = "emp_bank_name")
	private String empBankName;

	@Column(name = "kyc_created_on")
	private Date kycCreatedOn;

	@Column(name = "kyc_updated_on")
	private Date kycUpdatedOn;

	@Transient
	private Date empDOJ;

	@Column(name = "emp_aadhar_no")
	private String aadharNo;

	@Transient
	private Date empDOB;

	@Transient
	private String firstName;

	@Transient
	private String lastName;

	@Transient
	private Byte empGender;

	@OneToOne
	@JoinColumn(name = "fk_emp_detail_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpDetailBO datEmpDetail;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_emp_detail_id", referencedColumnName = "fk_emp_detail_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpPersonalDetailBO datEmpPersonalDetail;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_emp_detail_id", referencedColumnName = "fk_main_emp_detail_id", unique = true, nullable = true, insertable = false, updatable = false)
	private DatEmpProfessionalDetailBO datEmpProfDetail;

	public DatEmpKycDetailBO(Integer pkEmpKycDetailId,
			String empBankAccountIfscCode, String empBankAccountNo,
			String empDependentName, Byte empDependentRelationship,
			String empKycEmailId, String empKycMobileNo, Byte empMaritalStatus,
			String empNameAsPerAadharCard, String empNameAsPerBankAccount,
			String empNameAsPerPanCard, String empNameAsPerUan,
			String empPanNo, String empUanNo, Integer fkEmpDetailId,
			String hasUanFlag, DatEmpDetailBO datEmpDetail, Date empDOJ,
			String aadharNo, Date empDOB, String firstName, String lastName,
			Byte empGender, String empBankName, Date kycUpdatedOn) {
		super();
		this.pkEmpKycDetailId = pkEmpKycDetailId;
		this.empBankAccountIfscCode = empBankAccountIfscCode;
		this.empBankAccountNo = empBankAccountNo;
		this.empDependentName = empDependentName;
		this.empDependentRelationship = empDependentRelationship;
		this.empKycEmailId = empKycEmailId;
		this.empKycMobileNo = empKycMobileNo;
		this.empMaritalStatus = empMaritalStatus;
		this.empNameAsPerAadharCard = empNameAsPerAadharCard;
		this.empNameAsPerBankAccount = empNameAsPerBankAccount;
		this.empNameAsPerPanCard = empNameAsPerPanCard;
		this.empNameAsPerUan = empNameAsPerUan;
		this.empPanNo = empPanNo;
		this.empUanNo = empUanNo;
		this.fkEmpDetailId = fkEmpDetailId;
		this.hasUanFlag = hasUanFlag;
		this.datEmpDetail = datEmpDetail;
		this.empDOJ = empDOJ;
		this.aadharNo = aadharNo;
		this.empDOB = empDOB;
		this.firstName = firstName;
		this.lastName = lastName;
		this.empGender = empGender;
		this.empBankName = empBankName;
		this.kycUpdatedOn = kycUpdatedOn;
	}

	public DatEmpKycDetailBO(String firstName, String lastName, Date empDOB,
			Date empDOJ, Byte empGender) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.empDOB = empDOB;
		this.empDOJ = empDOJ;
		this.empGender = empGender;
	}

	public Date getKycCreatedOn() {
		return kycCreatedOn;
	}

	public void setKycCreatedOn(Date kycCreatedOn) {
		this.kycCreatedOn = kycCreatedOn;
	}

	public Date getKycUpdatedOn() {
		return kycUpdatedOn;
	}

	public void setKycUpdatedOn(Date kycUpdatedOn) {
		this.kycUpdatedOn = kycUpdatedOn;
	}

	public Date getEmpDOB() {
		return empDOB;
	}

	public void setEmpDOB(Date empDOB) {
		this.empDOB = empDOB;
	}

	public String getEmpBankName() {
		return empBankName;
	}

	public void setEmpBankName(String empBankName) {
		this.empBankName = empBankName;
	}

	public DatEmpPersonalDetailBO getDatEmpPersonalDetail() {
		return datEmpPersonalDetail;
	}

	public void setDatEmpPersonalDetail(
			DatEmpPersonalDetailBO datEmpPersonalDetail) {
		this.datEmpPersonalDetail = datEmpPersonalDetail;
	}

	public DatEmpProfessionalDetailBO getDatEmpProfDetail() {
		return datEmpProfDetail;
	}

	public void setDatEmpProfDetail(DatEmpProfessionalDetailBO datEmpProfDetail) {
		this.datEmpProfDetail = datEmpProfDetail;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Byte getEmpGender() {
		return empGender;
	}

	public void setEmpGender(Byte empGender) {
		this.empGender = empGender;
	}

	public Date getEmpDOJ() {
		return empDOJ;
	}

	public void setEmpDOJ(Date empDOJ) {
		this.empDOJ = empDOJ;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public DatEmpDetailBO getDatEmpDetail() {
		return datEmpDetail;
	}

	public void setDatEmpDetail(DatEmpDetailBO datEmpDetail) {
		this.datEmpDetail = datEmpDetail;
	}

	public DatEmpKycDetailBO() {
	}

	public Integer getPkEmpKycDetailId() {
		return this.pkEmpKycDetailId;
	}

	public void setPkEmpKycDetailId(Integer pkEmpKycDetailId) {
		this.pkEmpKycDetailId = pkEmpKycDetailId;
	}

	public String getEmpBankAccountIfscCode() {
		return this.empBankAccountIfscCode;
	}

	public void setEmpBankAccountIfscCode(String empBankAccountIfscCode) {
		this.empBankAccountIfscCode = empBankAccountIfscCode;
	}

	public String getEmpBankAccountNo() {
		return this.empBankAccountNo;
	}

	public void setEmpBankAccountNo(String empBankAccountNo) {
		this.empBankAccountNo = empBankAccountNo;
	}

	public String getEmpDependentName() {
		return this.empDependentName;
	}

	public void setEmpDependentName(String empDependentName) {
		this.empDependentName = empDependentName;
	}

	public Byte getEmpDependentRelationship() {
		return this.empDependentRelationship;
	}

	public void setEmpDependentRelationship(Byte empDependentRelationship) {
		this.empDependentRelationship = empDependentRelationship;
	}

	public String getEmpKycEmailId() {
		return this.empKycEmailId;
	}

	public void setEmpKycEmailId(String empKycEmailId) {
		this.empKycEmailId = empKycEmailId;
	}

	public String getEmpKycMobileNo() {
		return this.empKycMobileNo;
	}

	public void setEmpKycMobileNo(String empKycMobileNo) {
		this.empKycMobileNo = empKycMobileNo;
	}

	public Byte getEmpMaritalStatus() {
		return this.empMaritalStatus;
	}

	public void setEmpMaritalStatus(Byte empMaritalStatus) {
		this.empMaritalStatus = empMaritalStatus;
	}

	public String getEmpNameAsPerAadharCard() {
		return this.empNameAsPerAadharCard;
	}

	public void setEmpNameAsPerAadharCard(String empNameAsPerAadharCard) {
		this.empNameAsPerAadharCard = empNameAsPerAadharCard;
	}

	public String getEmpNameAsPerBankAccount() {
		return this.empNameAsPerBankAccount;
	}

	public void setEmpNameAsPerBankAccount(String empNameAsPerBankAccount) {
		this.empNameAsPerBankAccount = empNameAsPerBankAccount;
	}

	public String getEmpNameAsPerPanCard() {
		return this.empNameAsPerPanCard;
	}

	public void setEmpNameAsPerPanCard(String empNameAsPerPanCard) {
		this.empNameAsPerPanCard = empNameAsPerPanCard;
	}

	public String getEmpNameAsPerUan() {
		return this.empNameAsPerUan;
	}

	public void setEmpNameAsPerUan(String empNameAsPerUan) {
		this.empNameAsPerUan = empNameAsPerUan;
	}

	public String getEmpPanNo() {
		return this.empPanNo;
	}

	public void setEmpPanNo(String empPanNo) {
		this.empPanNo = empPanNo;
	}

	public String getEmpUanNo() {
		return this.empUanNo;
	}

	public void setEmpUanNo(String empUanNo) {
		this.empUanNo = empUanNo;
	}

	public Integer getFkEmpDetailId() {
		return this.fkEmpDetailId;
	}

	public void setFkEmpDetailId(Integer fkEmpDetailId) {
		this.fkEmpDetailId = fkEmpDetailId;
	}

	public String getHasUanFlag() {
		return this.hasUanFlag;
	}

	public void setHasUanFlag(String hasUanFlag) {
		this.hasUanFlag = hasUanFlag;
	}
	public String getEmpVoterIdNo() {
		return empVoterIdNo;
	}

	public void setEmpVoterIdNo(String empVoterIdNo) {
		this.empVoterIdNo = empVoterIdNo;
	}

	public String getEmpNameAsPerVoterId() {
		return empNameAsPerVoterId;
	}

	public void setEmpNameAsPerVoterId(String empNameAsPerVoterId) {
		this.empNameAsPerVoterId = empNameAsPerVoterId;
	}

	public String getEmpPassportNo() {
		return empPassportNo;
	}

	public void setEmpPassportNo(String empPassportNo) {
		this.empPassportNo = empPassportNo;
	}

	public String getEmpNameAsPerPassport() {
		return empNameAsPerPassport;
	}

	public void setEmpNameAsPerPassport(String empNameAsPerPassport) {
		this.empNameAsPerPassport = empNameAsPerPassport;
	}

	public Date getPassportDateOfIssue() {
		return passportDateOfIssue;
	}

	public void setPassportDateOfIssue(Date passportDateOfIssue) {
		this.passportDateOfIssue = passportDateOfIssue;
	}

	public String getPassportplaceOfIssue() {
		return passportplaceOfIssue;
	}

	public void setPassportplaceOfIssue(String passportplaceOfIssue) {
		this.passportplaceOfIssue = passportplaceOfIssue;
	}

	public Date getPassportExpiryDate() {
		return passportExpiryDate;
	}

	public void setPassportExpiryDate(Date passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}

	@Override
	public String toString() {
		return "DatEmpKycDetailBO [pkEmpKycDetailId=" + pkEmpKycDetailId + ", empBankAccountIfscCode="
				+ empBankAccountIfscCode + ", empBankAccountNo=" + empBankAccountNo + ", empDependentName="
				+ empDependentName + ", empDependentRelationship=" + empDependentRelationship + ", empKycEmailId="
				+ empKycEmailId + ", empVoterIdNo=" + empVoterIdNo + ", empNameAsPerVoterId=" + empNameAsPerVoterId
				+ ", empPassportNo=" + empPassportNo + ", empNameAsPerPassport=" + empNameAsPerPassport
				+ ", passportDateOfIssue=" + passportDateOfIssue + ", passportplaceOfIssue=" + passportplaceOfIssue
				+ ", passportExpiryDate=" + passportExpiryDate + ", empKycMobileNo=" + empKycMobileNo
				+ ", empMaritalStatus=" + empMaritalStatus + ", empNameAsPerAadharCard=" + empNameAsPerAadharCard
				+ ", empNameAsPerBankAccount=" + empNameAsPerBankAccount + ", empNameAsPerPanCard="
				+ empNameAsPerPanCard + ", empNameAsPerUan=" + empNameAsPerUan + ", empPanNo=" + empPanNo
				+ ", empUanNo=" + empUanNo + ", fkEmpDetailId=" + fkEmpDetailId + ", hasUanFlag=" + hasUanFlag
				+ ", empBankName=" + empBankName + ", kycCreatedOn=" + kycCreatedOn + ", kycUpdatedOn=" + kycUpdatedOn
				+ ", empDOJ=" + empDOJ + ", aadharNo=" + aadharNo + ", empDOB=" + empDOB + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", empGender=" + empGender + ", datEmpDetail=" + datEmpDetail
				+ ", datEmpPersonalDetail=" + datEmpPersonalDetail + ", datEmpProfDetail=" + datEmpProfDetail + "]";
	}

	
}