/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmpKycDetailRepository.java                       */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  April 04, 2017                                    */
/*                                                                   */
/*  Description :  This is the Repository for Employee KYC Details   */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* April 04, 2017     THBS     1.0        Initial version created    */
/*********************************************************************/
package com.thbs.mis.uan.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.thbs.mis.common.dao.GenericRepository;
import com.thbs.mis.uan.bo.DatEmpKycDetailBO;

@Repository
public interface EmpKycDetailRepository extends
		GenericRepository<DatEmpKycDetailBO, Long>,
		JpaSpecificationExecutor<DatEmpKycDetailBO> {

	public DatEmpKycDetailBO findByFkEmpDetailId(int empId);

	@Query("SELECT new com.thbs.mis.uan.bo.DatEmpKycDetailBO(k.pkEmpKycDetailId,k.empBankAccountIfscCode,k.empBankAccountNo,"
			+ "k.empDependentName,k.empDependentRelationship,k.empKycEmailId,k.empKycMobileNo,"
			+ "k.empMaritalStatus,k.empNameAsPerAadharCard,k.empNameAsPerBankAccount,k.empNameAsPerPanCard,"
			+ "k.empNameAsPerUan,k.empPanNo,k.empUanNo,k.fkEmpDetailId,k.hasUanFlag,k.datEmpDetail,p.empDateOfJoining,"
			+ "k.aadharNo,per.empDateOfBirth,per.empFirstName,per.empLastName,per.empGender,k.empBankName,k.kycUpdatedOn) FROM DatEmpKycDetailBO k"
			+ " INNER JOIN DatEmpProfessionalDetailBO p ON p.fkMainEmpDetailId = k.fkEmpDetailId"
			+ " INNER JOIN DatEmpPersonalDetailBO per ON per.fkEmpDetailId = k.fkEmpDetailId where k.fkEmpDetailId = ?")
	public DatEmpKycDetailBO getEmpUanDetails(Integer empId);

	@Query("SELECT new com.thbs.mis.uan.bo.DatEmpKycDetailBO(k.pkEmpKycDetailId,k.empBankAccountIfscCode,k.empBankAccountNo,"
			+ "k.empDependentName,k.empDependentRelationship,k.empKycEmailId,k.empKycMobileNo,"
			+ "k.empMaritalStatus,k.empNameAsPerAadharCard,k.empNameAsPerBankAccount,k.empNameAsPerPanCard,"
			+ "k.empNameAsPerUan,k.empPanNo,k.empUanNo,k.fkEmpDetailId,k.hasUanFlag,k.datEmpDetail,p.empDateOfJoining,"
			+ "k.aadharNo,per.empDateOfBirth,per.empFirstName,per.empLastName,per.empGender,k.empBankName,k.kycUpdatedOn) FROM DatEmpKycDetailBO k"
			+ " INNER JOIN DatEmpProfessionalDetailBO p ON p.fkMainEmpDetailId = k.fkEmpDetailId"
			+ " INNER JOIN DatEmpPersonalDetailBO per ON per.fkEmpDetailId = k.fkEmpDetailId")
	public List<DatEmpKycDetailBO> getUanDetails();

	public DatEmpKycDetailBO findByPkEmpKycDetailId(int kycId);

	@Query("SELECT new com.thbs.mis.uan.bo.DatEmpKycDetailBO(per.empFirstName,per.empLastName,per.empDateOfBirth,"
			+ "prof.empDateOfJoining,per.empGender) FROM DatEmpPersonalDetailBO per JOIN DatEmpProfessionalDetailBO prof "
			+ "on per.fkEmpDetailId = prof.fkMainEmpDetailId where per.fkEmpDetailId = ?")
	public DatEmpKycDetailBO getEmployeeDetails(int empId);
	
	public List<DatEmpKycDetailBO> findByEmpPassportNo(String passportNumber);

}
