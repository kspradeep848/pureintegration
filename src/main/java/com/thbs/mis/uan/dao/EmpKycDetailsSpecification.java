/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  EmpKycDetailsSpecification.java                   */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  May 05, 2017                                      */
/*                                                                   */
/*  Description :  This class contains Specifications for Employee   */
/*                 KYC Details                                       */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* May 05, 2017     THBS     1.0        Initial version created      */
/*********************************************************************/
package com.thbs.mis.uan.dao;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.thbs.mis.uan.bean.ViewUanDetailsInputBean;
import com.thbs.mis.uan.bo.DatEmpKycDetailBO;

public class EmpKycDetailsSpecification {

	/**
	 * 
	 * @param uanDetailsBean
	 * @return Specification<DatEmpKycDetailBO>
	 * @Description : This specification is Used to get Employee KYC Details
	 *              based on Employee Id or PAN status or UAN status
	 */
	public static Specification<DatEmpKycDetailBO> viewKycDetailsSpecification(
			ViewUanDetailsInputBean uanDetailsBean) {

		return new Specification<DatEmpKycDetailBO>() {

			@Override
			public Predicate toPredicate(Root<DatEmpKycDetailBO> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {

				final Collection<Predicate> predicates = new ArrayList<>();

				if (uanDetailsBean.getPanStatus() != null) {
					if (uanDetailsBean.getPanStatus().equalsIgnoreCase("YES")) {
						final Predicate panNoPredicate = cb.isNotNull(root
								.get("empPanNo"));
						predicates.add(panNoPredicate);
					}
					if (uanDetailsBean.getPanStatus().equalsIgnoreCase("NO")) {
						final Predicate panNoPredicate = cb.isNull(root
								.get("empPanNo"));
						predicates.add(panNoPredicate);
					}
				}
				if (uanDetailsBean.getUanStatus() != null) {
					if (uanDetailsBean.getUanStatus().equalsIgnoreCase(
							"YES_HAS_UAN")) {
						final Predicate uanDetailsPredicate = cb.like(
								root.get("hasUanFlag"),
								uanDetailsBean.getUanStatus());
						predicates.add(uanDetailsPredicate);
					}
					if (uanDetailsBean.getUanStatus()
							.equalsIgnoreCase("NO_UAN")) {
						final Predicate uanDetailsPredicate = cb.like(
								root.get("hasUanFlag"),
								uanDetailsBean.getUanStatus());
						predicates.add(uanDetailsPredicate);
					}
				}
				if (uanDetailsBean.getEmpIdList() != null) {
					if (uanDetailsBean.getEmpIdList().size() > 0) {
						final Predicate empIdPredicate = root.get(
								"fkEmpDetailId").in(
								uanDetailsBean.getEmpIdList());
						predicates.add(empIdPredicate);
					}
				}
				return cb.and(predicates.toArray(new Predicate[predicates
						.size()]));
			}
		};
	}
}
