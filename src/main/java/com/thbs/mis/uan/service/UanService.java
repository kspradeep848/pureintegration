/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  UanService.java                                   */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  April 04, 2017                                    */
/*                                                                   */
/*  Description :  This class is service class for KYC Details       */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* April 04, 2017     THBS     1.0        Initial version created    */
/*********************************************************************/
package com.thbs.mis.uan.service;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.base.Optional;
import com.thbs.mis.common.dao.DatEmployeeProfessionalRepository;
import com.thbs.mis.common.dao.EmpDetailRepository;
import com.thbs.mis.common.dao.EmployeePersonalDetailsRepository;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.report.Util.DownloadFileUtil;
import com.thbs.mis.framework.util.DateTimeUtil;
import com.thbs.mis.training.controller.TrainingProgramController;
import com.thbs.mis.uan.bean.EmpKycDetailsOutputBean;
import com.thbs.mis.uan.bean.SubmitKycDetailsInputBean;
import com.thbs.mis.uan.bean.UpdateKycDetailsBean;
import com.thbs.mis.uan.bean.ViewUanDetailsInputBean;
import com.thbs.mis.uan.bean.ViewUanDetailsOutputBean;
import com.thbs.mis.uan.bo.DatEmpKycDetailBO;
import com.thbs.mis.uan.constants.KycConstants;
import com.thbs.mis.uan.dao.EmpKycDetailRepository;
import com.thbs.mis.uan.dao.EmpKycDetailsSpecification;

@Service
public class UanService {

	private static final AppLog LOG = LogFactory
			.getLog(TrainingProgramController.class);

	@Autowired
	EmpKycDetailRepository kycDetailRepository;

	@Autowired
	EmployeePersonalDetailsRepository empPersonalRepository;

	@Autowired
	DatEmployeeProfessionalRepository empProfessionalRepository;
	
	@Autowired
	EmpDetailRepository empDetailRepository;

	@Autowired
	private ServletContext servletContext;
	
	@Value("${kyc.file.basepath}")
	private String kycDocBasePath;
	
	@Value("${aadhar.file.name}")
	private String aadharFileName;
	
	@Value("${pan.file.name}")
	private String panFileName;
	
	@Autowired
	private Validator validator;

	/**
	 * 
	 * @param empId
	 * @return EmpKycDetailsOutputBean
	 * @throws CommonCustomException
	 * @Description : This method is used to get Employee's KYC Details
	 */
	public EmpKycDetailsOutputBean getEmployeeUanDetails(int empId)
			throws CommonCustomException {
		DatEmpKycDetailBO empUanDetail = null;
		EmpKycDetailsOutputBean kycDetailBean = null;
		try {
			empUanDetail = kycDetailRepository.getEmpUanDetails(empId);
			if (empUanDetail != null) {
				kycDetailBean = new EmpKycDetailsOutputBean();
				kycDetailBean.setAadharNo(empUanDetail.getAadharNo());
				kycDetailBean.setLastUpdatedOn(empUanDetail.getKycUpdatedOn());
				kycDetailBean.setEmpId(empId);
				kycDetailBean.setEmpBankAccountIfscCode(empUanDetail
						.getEmpBankAccountIfscCode());
				kycDetailBean.setEmpBankAccountNo(empUanDetail
						.getEmpBankAccountNo());
				kycDetailBean.setEmpDependentName(empUanDetail
						.getEmpDependentName());
				kycDetailBean.setEmpDependentRelationshipId(empUanDetail
						.getEmpDependentRelationship());
				if (empUanDetail.getEmpDependentRelationship() != null) {
					if (empUanDetail.getEmpDependentRelationship() == 1)
						kycDetailBean.setEmpDependentRelationshipName("Father");
					if (empUanDetail.getEmpDependentRelationship() == 2)
						kycDetailBean
								.setEmpDependentRelationshipName("Husband");
				}
				kycDetailBean.setEmpDOB(empUanDetail.getEmpDOB());
				kycDetailBean.setEmpDOJ(empUanDetail.getEmpDOJ());
				kycDetailBean.setEmpKycEmailId(empUanDetail.getEmpKycEmailId());
				kycDetailBean.setEmpKycMobileNo(empUanDetail
						.getEmpKycMobileNo());
				kycDetailBean.setEmpMaritalStatusId(empUanDetail
						.getEmpMaritalStatus());
				if (empUanDetail.getEmpMaritalStatus() != null) {
					if (empUanDetail.getEmpMaritalStatus() == 1)
						kycDetailBean.setEmpMaritalStatusName("Single");
					if (empUanDetail.getEmpMaritalStatus() == 2)
						kycDetailBean.setEmpMaritalStatusName("Married");
					if (empUanDetail.getEmpMaritalStatus() == 3)
						kycDetailBean.setEmpMaritalStatusName("Widowed");
					if (empUanDetail.getEmpMaritalStatus() == 4)
						kycDetailBean.setEmpMaritalStatusName("Divorced");
					if (empUanDetail.getEmpMaritalStatus() == 5)
						kycDetailBean.setEmpMaritalStatusName("Seperated");
				}
				kycDetailBean.setEmpNameAsPerAadharCard(empUanDetail
						.getEmpNameAsPerAadharCard());
				kycDetailBean.setEmpNameAsPerBankAccount(empUanDetail
						.getEmpNameAsPerBankAccount());
				kycDetailBean.setEmpNameAsPerPanCard(empUanDetail
						.getEmpNameAsPerPanCard());
				kycDetailBean.setEmpNameAsPerUan(empUanDetail
						.getEmpNameAsPerUan());
				kycDetailBean.setEmpPanNo(empUanDetail.getEmpPanNo());
				kycDetailBean.setEmpUanNo(empUanDetail.getEmpUanNo());
				kycDetailBean.setFkEmpDetailId(empUanDetail.getFkEmpDetailId());
				kycDetailBean.setHasUanFlag(empUanDetail.getHasUanFlag());
				kycDetailBean.setPkEmpKycDetailId(empUanDetail
						.getPkEmpKycDetailId());
				kycDetailBean.setEmpBankName(empUanDetail.getEmpBankName());
				kycDetailBean.setGenderId(empUanDetail.getEmpGender());
				if (empUanDetail.getEmpGender() == 1)
					kycDetailBean.setGenderName("Male");
				if (empUanDetail.getEmpGender() == 2)
					kycDetailBean.setGenderName("Female");
				kycDetailBean.setEmpName(empUanDetail.getFirstName() + " "
						+ empUanDetail.getLastName());
				kycDetailBean.setIsAadharUploaded("NO");
				kycDetailBean.setIsPanUploaded("NO");
			} else {
				empUanDetail = kycDetailRepository.getEmployeeDetails(empId);
				if (empUanDetail != null) {
					kycDetailBean = new EmpKycDetailsOutputBean();
					kycDetailBean.setEmpDOB(empUanDetail.getEmpDOB());
					kycDetailBean.setEmpDOJ(empUanDetail.getEmpDOJ());
					kycDetailBean.setEmpName(empUanDetail.getFirstName() + " "
							+ empUanDetail.getLastName());
					kycDetailBean.setGenderId(empUanDetail.getEmpGender());
					if (empUanDetail.getEmpGender() == 1)
						kycDetailBean.setGenderName("Male");
					if (empUanDetail.getEmpGender() == 2)
						kycDetailBean.setGenderName("Female");
					kycDetailBean.setEmpId(empId);
					kycDetailBean.setIsAadharUploaded("NO");
					kycDetailBean.setIsPanUploaded("NO");
				} else {
					throw new CommonCustomException("Employee Id not found.");
				}
			}
			File dir = new File(kycDocBasePath + empId);
			if (dir.isDirectory()) {
				File[] listFiles = dir.listFiles();
				if (listFiles.length > 0) {
					for (File kycFile : listFiles) {
						if (kycFile.getName().toLowerCase().contains(aadharFileName.toLowerCase())){
							kycDetailBean.setIsAadharUploaded("YES");
							kycDetailBean.setAadharFileName(kycFile.getName());
						}
						if (kycFile.getName().toLowerCase().contains(panFileName.toLowerCase())){
							kycDetailBean.setIsPanUploaded("YES");
							kycDetailBean.setPanFileName(kycFile.getName());
						}
					}
				}
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		return kycDetailBean;
	}

	/**
	 * 
	 * @param response
	 * @return boolean
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This method is used to generate KYC Details Report
	 */
	public boolean getEmployeeUanDetailsReport(HttpServletResponse response)
			throws CommonCustomException, DataAccessException {
		List<DatEmpKycDetailBO> empUanDetails = null;
		EmpKycDetailsOutputBean kycDetailBean = null;
		List<EmpKycDetailsOutputBean> kycDetailsList = new ArrayList<EmpKycDetailsOutputBean>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		boolean isReportGenerated = false;
		Map beans = new HashMap();
		try {
			empUanDetails = kycDetailRepository.getUanDetails();
			if (empUanDetails != null) {
				for (DatEmpKycDetailBO empDetail : empUanDetails) {
					kycDetailBean = new EmpKycDetailsOutputBean();
					kycDetailBean.setLastUpdatedOn(empDetail.getKycUpdatedOn());
					kycDetailBean.setAadharNo(empDetail.getAadharNo());
					kycDetailBean.setEmpBankAccountIfscCode(empDetail
							.getEmpBankAccountIfscCode());
					kycDetailBean.setEmpBankAccountNo(empDetail
							.getEmpBankAccountNo());
					kycDetailBean.setEmpDependentName(empDetail
							.getEmpDependentName());
					kycDetailBean.setEmpDependentRelationshipId(empDetail
							.getEmpDependentRelationship());
					if (empDetail.getEmpDependentRelationship() != null) {
						if (empDetail.getEmpDependentRelationship() == 1)
							kycDetailBean
									.setEmpDependentRelationshipName("Father");
						if (empDetail.getEmpDependentRelationship() == 2)
							kycDetailBean
									.setEmpDependentRelationshipName("Husband");
					}
					kycDetailBean.setEmpDOB(empDetail.getEmpDOB());
					kycDetailBean.setEmpDOJ(empDetail.getEmpDOJ());
					kycDetailBean
							.setEmpKycEmailId(empDetail.getEmpKycEmailId());
					kycDetailBean.setEmpKycMobileNo(empDetail
							.getEmpKycMobileNo());
					kycDetailBean.setEmpMaritalStatusId(empDetail
							.getEmpMaritalStatus());
					kycDetailBean.setEmpBankName(empDetail.getEmpBankName());
					if (empDetail.getEmpMaritalStatus() != null) {
						if (empDetail.getEmpMaritalStatus() == 1)
							kycDetailBean.setEmpMaritalStatusName("Single");
						if (empDetail.getEmpMaritalStatus() == 2)
							kycDetailBean.setEmpMaritalStatusName("Married");
						if (empDetail.getEmpMaritalStatus() == 3)
							kycDetailBean.setEmpMaritalStatusName("Widowed");
						if (empDetail.getEmpMaritalStatus() == 4)
							kycDetailBean.setEmpMaritalStatusName("Divorced");
						if (empDetail.getEmpMaritalStatus() == 5)
							kycDetailBean.setEmpMaritalStatusName("Seperated");
					}
					kycDetailBean.setEmpNameAsPerAadharCard(empDetail
							.getEmpNameAsPerAadharCard());
					kycDetailBean.setEmpNameAsPerBankAccount(empDetail
							.getEmpNameAsPerBankAccount());
					kycDetailBean.setEmpNameAsPerPanCard(empDetail
							.getEmpNameAsPerPanCard());
					kycDetailBean.setEmpNameAsPerUan(empDetail
							.getEmpNameAsPerUan());
					kycDetailBean.setEmpPanNo(empDetail.getEmpPanNo());
					kycDetailBean.setEmpUanNo(empDetail.getEmpUanNo());
					kycDetailBean
							.setFkEmpDetailId(empDetail.getFkEmpDetailId());
					kycDetailBean.setHasUanFlag(empDetail.getHasUanFlag());
					kycDetailBean.setPkEmpKycDetailId(empDetail
							.getPkEmpKycDetailId());
					kycDetailBean.setGenderId(empDetail.getEmpGender());
					if (empDetail.getEmpGender() == 1)
						kycDetailBean.setGenderName("Male");
					if (empDetail.getEmpGender() == 2)
						kycDetailBean.setGenderName("Female");
					kycDetailBean.setEmpName(empDetail.getFirstName() + " "
							+ empDetail.getLastName());
					kycDetailsList.add(kycDetailBean);
				}
				String BASEPATH = servletContext
						.getRealPath("/WEB-INF/classes/com/thbs/mis/framework/report/template/");
				String createdOn = sdf.format(new Date());
				String outputFileName = "EmployeeUANReport.xls";
				beans.put("uan", kycDetailsList);
				beans.put("createdOn", createdOn);
				boolean reportGeneratedDone = DownloadFileUtil
						.IsExcepReportCreated(BASEPATH, beans,
								DownloadFileUtil.EMPLOYEE_UAN_REPORT,
								outputFileName);
				if (reportGeneratedDone) {
					DownloadFileUtil.downloadFile(response, BASEPATH,
							outputFileName);
					isReportGenerated = true;
				}
			} else {
				throw new CommonCustomException("No KYC Details");
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		return isReportGenerated;
	}

	/**
	 * 
	 * @param kycDetailsBean
	 * @return boolean
	 * @throws CommonCustomException
	 * @Description : This method is used to create KYC Details
	 */
	public boolean createKycDetails(SubmitKycDetailsInputBean kycDetailsBean,MultipartFile aadhar ,MultipartFile panCard)
			throws CommonCustomException {
		boolean isCreatedFlag = false;
		DatEmpKycDetailBO uanDetails = new DatEmpKycDetailBO();
		DatEmpKycDetailBO saveUanDetails = null;
		DatEmpKycDetailBO kycDet = null;
		String exMsg = "";
		Set violations = validator.validate(kycDetailsBean);
		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(violations);
		}
		else
		{
		try {
			if (kycDetailsBean != null) {

				kycDet = kycDetailRepository.findByFkEmpDetailId(kycDetailsBean
						.getEmpId());

				if (kycDet == null) {
					uanDetails.setFkEmpDetailId(kycDetailsBean.getEmpId());
					uanDetails.setHasUanFlag(kycDetailsBean.getHasUanFlag());

					if ((kycDetailsBean.getEmpUanNo() != null || kycDetailsBean
							.getEmpNameAsPerUan() != null)
							&& (kycDetailsBean.getHasUanFlag().trim()
									.equalsIgnoreCase(KycConstants.NO_UAN))) {
						exMsg = exMsg + "Please enter the correct hasUanFlag";
						if (!exMsg.isEmpty())
							throw new CommonCustomException(exMsg);
					}
					if (kycDetailsBean.getHasUanFlag().trim()
							.equalsIgnoreCase(KycConstants.YES_HAS_UAN)) {
						if (kycDetailsBean.getEmpUanNo() != null)
							uanDetails
									.setEmpUanNo(kycDetailsBean.getEmpUanNo());
						else
							exMsg = exMsg + "Please enter UAN Number.";
						if (kycDetailsBean.getEmpNameAsPerUan() != null)
							uanDetails.setEmpNameAsPerUan(kycDetailsBean
									.getEmpNameAsPerUan());
						else
							exMsg = exMsg + "Please enter Name as per UAN.";
						if (kycDetailsBean.getEmpDependentRelationshipId() != null)
							uanDetails
									.setEmpDependentRelationship(kycDetailsBean
											.getEmpDependentRelationshipId());
						else
							exMsg = exMsg
									+ "Please enter Dependent Relationship Id.";
						if (kycDetailsBean.getEmpDependentName() != null) {
							if (!kycDetailsBean.getEmpDependentName().trim()
									.equals(""))
								uanDetails.setEmpDependentName(kycDetailsBean
										.getEmpDependentName());
							else
								exMsg = exMsg
										+ "Dependent Name cannot be empty.";
						} else
							exMsg = exMsg + "Please enter Dependent Name.";
						if (kycDetailsBean.getEmpMaritalStatusId() != null)
							uanDetails.setEmpMaritalStatus(kycDetailsBean
									.getEmpMaritalStatusId());
						else
							exMsg = exMsg
									+ "Please enter Employee Marital Status";

						if (!exMsg.isEmpty())
							throw new CommonCustomException(exMsg);
					}
					if (kycDetailsBean.getHasUanFlag().trim()
							.equalsIgnoreCase(KycConstants.NO_UAN)) {
						if (kycDetailsBean.getEmpDependentRelationshipId() != null)
							uanDetails
									.setEmpDependentRelationship(kycDetailsBean
											.getEmpDependentRelationshipId());
						else
							exMsg = exMsg
									+ "Please enter Dependent Relationship Id.";
						if (kycDetailsBean.getEmpDependentName() != null) {
							if (!kycDetailsBean.getEmpDependentName().trim()
									.equals(""))
								uanDetails.setEmpDependentName(kycDetailsBean
										.getEmpDependentName());
							else
								exMsg = exMsg
										+ "Dependent Name cannot be empty.";
						} else
							exMsg = exMsg + "Please enter Dependent Name.";
						if (kycDetailsBean.getEmpMaritalStatusId() != null)
							uanDetails.setEmpMaritalStatus(kycDetailsBean
									.getEmpMaritalStatusId());
						else
							exMsg = exMsg
									+ "Please enter Employee Marital Status";
						if (!exMsg.isEmpty())
							throw new CommonCustomException(exMsg);
					}
					if (kycDetailsBean.getEmpPanNo() != null
							&& (!kycDetailsBean.getEmpPanNo().trim().equals(""))) {
						uanDetails.setEmpPanNo(kycDetailsBean.getEmpPanNo());
						if (kycDetailsBean.getEmpNameAsPerPanCard() != null) {
							if (!kycDetailsBean.getEmpNameAsPerPanCard().trim()
									.equals(""))
								uanDetails
										.setEmpNameAsPerPanCard(kycDetailsBean
												.getEmpNameAsPerPanCard());
							else
								exMsg = exMsg
										+ "Name as per PAN Card cannot be empty.";
						} else
							exMsg = exMsg
									+ "Please enter Name as per PAN Card.";
						if (!exMsg.isEmpty())
							throw new CommonCustomException(exMsg);
					} else if (kycDetailsBean.getEmpPanNo() == null) {
						if (kycDetailsBean.getEmpNameAsPerPanCard() != null)
							exMsg = exMsg + "Please enter PAN Card Number";
						if (!exMsg.isEmpty())
							throw new CommonCustomException(exMsg);
					}

					if (kycDetailsBean.getAadharNo() != null) {
						uanDetails.setAadharNo(kycDetailsBean.getAadharNo());
						if (kycDetailsBean.getEmpNameAsPerAadharCard() != null) {
							if (!kycDetailsBean.getEmpNameAsPerAadharCard()
									.trim().equals(""))
								uanDetails
										.setEmpNameAsPerAadharCard(kycDetailsBean
												.getEmpNameAsPerAadharCard());
							else
								exMsg = exMsg
										+ "Name As Per Aadhar Card cannot be empty.";
						} else
							exMsg = exMsg
									+ "Please enter Name as per Aadhar Card.";
						if (!exMsg.isEmpty())
							throw new CommonCustomException(exMsg);
					} else if (kycDetailsBean.getAadharNo() == null) {
						if (kycDetailsBean.getEmpNameAsPerAadharCard() != null)
							exMsg = exMsg + "Please enter Aadhar Card Number";
						if (!exMsg.isEmpty())
							throw new CommonCustomException(exMsg);
					}

					if (kycDetailsBean.getEmpBankAccountNo() != null) {
						uanDetails.setEmpBankAccountNo(kycDetailsBean
								.getEmpBankAccountNo());
						if (kycDetailsBean.getEmpNameAsPerBankAccount() != null) {
							if (!kycDetailsBean.getEmpNameAsPerBankAccount()
									.trim().equals(""))
								uanDetails
										.setEmpNameAsPerBankAccount(kycDetailsBean
												.getEmpNameAsPerBankAccount());
							else
								exMsg = exMsg
										+ "Name as per Bank Account cannot be empty.";
						} else
							exMsg = exMsg
									+ "Please enter Name as per Bank Account.";
						if (kycDetailsBean.getEmpBankAccountIfscCode() != null) {
							if (!kycDetailsBean.getEmpBankAccountIfscCode()
									.trim().equals(""))
								uanDetails
										.setEmpBankAccountIfscCode(kycDetailsBean
												.getEmpBankAccountIfscCode());
							else
								exMsg = exMsg
										+ "IFSC Code for Bank Account cannot be empty.";
						} else
							exMsg = exMsg
									+ "Please enter IFSC Code for Bank Account.";
						if (kycDetailsBean.getEmpBankName() != null) {
							if (!kycDetailsBean.getEmpBankName().trim()
									.equals(""))
								uanDetails.setEmpBankName(kycDetailsBean
										.getEmpBankName());
							else
								exMsg = exMsg
										+ "Employee Bank Name cannot be empty.";
						} else
							exMsg = exMsg + "Please enter Employee Bank Name.";
						if (!exMsg.isEmpty())
							throw new CommonCustomException(exMsg);
					} else if (kycDetailsBean.getEmpBankAccountNo() == null) {
						if (kycDetailsBean.getEmpNameAsPerBankAccount() != null
								|| kycDetailsBean.getEmpBankAccountIfscCode() != null
								|| kycDetailsBean.getEmpBankName() != null)
							exMsg = exMsg + "Please enter Bank Account Number";
						if (!exMsg.isEmpty())
							throw new CommonCustomException(exMsg);
					}
					if (kycDetailsBean.getEmpMaritalStatusId() != null)
						uanDetails.setEmpMaritalStatus(kycDetailsBean
								.getEmpMaritalStatusId());
					if (kycDetailsBean.getEmpKycEmailId() != null
							&& (!kycDetailsBean.getEmpKycEmailId().trim()
									.equals("")))
						uanDetails.setEmpKycEmailId(kycDetailsBean
								.getEmpKycEmailId());
					if (kycDetailsBean.getEmpKycMobileNo() != null)
						uanDetails.setEmpKycMobileNo(kycDetailsBean
								.getEmpKycMobileNo());
					uanDetails.setKycCreatedOn(new Date());
					uanDetails.setKycUpdatedOn(new Date());

					saveUanDetails = kycDetailRepository.save(uanDetails);

					if (saveUanDetails != null) {
						isCreatedFlag = true;
						
						if(kycDetailsBean.getEmpPanNo() != null && kycDetailsBean.getEmpNameAsPerPanCard() != null){
						boolean isFileFound = false;
						if(panCard != null)
						{
							if(!panCard.isEmpty()){
								long fileSizeInKB = panCard.getSize() / 1024;
								String[] name = panCard.getOriginalFilename().split("\\.");
								String newFileName = panFileName +"."+name[1].toString();
								
								if (!panCard.getOriginalFilename().toLowerCase().endsWith(".pdf") && !panCard.getOriginalFilename().toLowerCase().endsWith(".jpeg") && !panCard.getOriginalFilename().toLowerCase().endsWith(".jpg") && !panCard.getOriginalFilename().toLowerCase().endsWith(".png")) {
									throw new CommonCustomException(
											"uploaded file is in different format, Please upload only pdf,jpeg,jpg or png files");
								} else if (fileSizeInKB > 1024) {
									throw new CommonCustomException(
											"uploaded file should be 1 MB or less than 1 MB");
								}
								try{
								File dir = new File(kycDocBasePath + kycDetailsBean.getEmpId());
								if (dir.isDirectory()) {
									File[] listFiles = dir.listFiles();
									if (listFiles.length > 0) {
										for (File kycFile : listFiles) {
											if (kycFile.getName().contains(panFileName)) 
												isFileFound = true;
										}
										if(!isFileFound){
											File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
											FileCopyUtils.copy(panCard.getBytes(),fileToBeStored);
										} else {
											throw new CommonCustomException("Pan Card file already exists.");
										}
									}
								} else {
									if (dir.mkdirs()) {
										File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
										FileCopyUtils.copy(panCard.getBytes(),	fileToBeStored);
									}
								}
								} catch(IOException e){
									throw new CommonCustomException("Document files not uploaded");
								}
							}
						}
						}
						if(kycDetailsBean.getAadharNo() != null && kycDetailsBean.getEmpNameAsPerAadharCard() != null) {
							boolean isFileFound = false;
							if(aadhar != null)
							{	
							if(!aadhar.isEmpty()) {
								long fileSizeInKB = aadhar.getSize() / 1024;
								String[] name = aadhar.getOriginalFilename().split("\\.");
								String newFileName = aadharFileName +"."+name[1].toString();
								
								if (!aadhar.getOriginalFilename().toLowerCase().endsWith(".pdf") && !aadhar.getOriginalFilename().toLowerCase().endsWith(".jpeg") && !aadhar.getOriginalFilename().toLowerCase().endsWith(".jpg") && !aadhar.getOriginalFilename().toLowerCase().endsWith(".png")) {
									throw new CommonCustomException(
											"uploaded file is in different format, Please upload only pdf,jpeg,jpg or png files");
								} else if (fileSizeInKB > 1024) {
									throw new CommonCustomException(
											"uploaded file should be 1 MB or less than 1 MB");
								}
								try{
								File dir = new File(kycDocBasePath + kycDetailsBean.getEmpId());
								if (dir.isDirectory()) {
									File[] listFiles = dir.listFiles();
									if (listFiles.length > 0) {
										for (File kycFile : listFiles) {
											if (kycFile.getName().contains(aadharFileName)) 
												isFileFound = true;
										}
										if(!isFileFound){
											File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
											FileCopyUtils.copy(aadhar.getBytes(),fileToBeStored);
										} else {
											throw new CommonCustomException("Aadhar file already exists.");
										}
									}
								} else {
									if (dir.mkdirs()) {
										File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
										FileCopyUtils.copy(aadhar.getBytes(),	fileToBeStored);
									}
								}
								} catch(IOException e){
									throw new CommonCustomException("Document files not uploaded");
								}
							}
							}	
						}
					} else
						throw new CommonCustomException(
								"Some exception happened while saving KYC Details");
				} else {
					isCreatedFlag = false;
					throw new CommonCustomException(
							"KYC Details has alredy been created for this employee");
				}
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		}	
		return isCreatedFlag;
	}

	/**
	 * 
	 * @param kycDetailsBean
	 * @return
	 * @throws CommonCustomException
	 * @Description : This method is used to update KYC Details
	 */
	public boolean updateKycDetails(UpdateKycDetailsBean kycDetailsBean,MultipartFile aadhar,MultipartFile panCard)
			throws CommonCustomException {
		DatEmpKycDetailBO updateUanDetails = null;
		DatEmpKycDetailBO getKycDetails = null;
		String exMsg = "";
		String succMsg = "";
		
		boolean isUpdatedFlag = false;
		Set violations = validator.validate(kycDetailsBean);
		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(violations);
		}else
		{
		try {
			if (kycDetailsBean != null) {
				getKycDetails = kycDetailRepository
						.findByPkEmpKycDetailId(kycDetailsBean.getKycDetailId());

				if (getKycDetails != null) {
					if (kycDetailsBean.getHasUanFlag() != null) {
						if (kycDetailsBean.getHasUanFlag().trim()
								.equalsIgnoreCase(KycConstants.YES_HAS_UAN)) {
							getKycDetails.setHasUanFlag(kycDetailsBean.getHasUanFlag());
							if (kycDetailsBean.getEmpUanNo() != null)
								getKycDetails.setEmpUanNo(kycDetailsBean
										.getEmpUanNo());
							else
								exMsg = exMsg + "Please enter UAN Number.";
							if (kycDetailsBean.getEmpNameAsPerUan() != null)
								getKycDetails.setEmpNameAsPerUan(kycDetailsBean
										.getEmpNameAsPerUan());
							else
								exMsg = exMsg + "Please enter Name as per UAN.";
							if (kycDetailsBean.getEmpDependentRelationshipId() != null)
								getKycDetails
										.setEmpDependentRelationship(kycDetailsBean
												.getEmpDependentRelationshipId());
							else
								exMsg = exMsg
										+ "Please enter Dependent Relationship Id.";
							if (kycDetailsBean.getEmpDependentName() != null) {
								if (!kycDetailsBean.getEmpDependentName()
										.trim().equals(""))
									getKycDetails
											.setEmpDependentName(kycDetailsBean
													.getEmpDependentName());
								else
									exMsg = exMsg
											+ "Dependent Name cannot be empty.";
							} else
								exMsg = exMsg + "Please enter Dependent Name.";
							if (kycDetailsBean.getEmpMaritalStatusId() != null)
								getKycDetails
										.setEmpMaritalStatus(kycDetailsBean
												.getEmpMaritalStatusId());
							else
								exMsg = exMsg
										+ "Please enter Employee Marital Status";

							if (!exMsg.isEmpty())
								throw new CommonCustomException(exMsg);
						}
						if (kycDetailsBean.getHasUanFlag().trim()
								.equalsIgnoreCase(KycConstants.NO_UAN)) {
							getKycDetails.setHasUanFlag(kycDetailsBean.getHasUanFlag());
							if (kycDetailsBean.getEmpDependentRelationshipId() != null)
								getKycDetails
										.setEmpDependentRelationship(kycDetailsBean
												.getEmpDependentRelationshipId());
							else
								exMsg = exMsg
										+ "Please enter Dependent Relationship Id.";
							if (kycDetailsBean.getEmpDependentName() != null) {
								if (!kycDetailsBean.getEmpDependentName()
										.trim().equals(""))
									getKycDetails
											.setEmpDependentName(kycDetailsBean
													.getEmpDependentName());
								else
									exMsg = exMsg
											+ "Dependent Name cannot be empty.";
							} else
								exMsg = exMsg + "Please enter Dependent Name.";
							if (kycDetailsBean.getEmpMaritalStatusId() != null)
								getKycDetails
										.setEmpMaritalStatus(kycDetailsBean
												.getEmpMaritalStatusId());
							else
								exMsg = exMsg
										+ "Please enter Employee Marital Status";
							if (!exMsg.isEmpty())
								throw new CommonCustomException(exMsg);
						}
					}
					if (kycDetailsBean.getEmpBankAccountNo() != null) {
						if (getKycDetails.getEmpBankAccountNo() != null) {
							succMsg = succMsg + "Cannot update Bank Details.";
						} else {
							getKycDetails.setEmpBankAccountNo(kycDetailsBean
									.getEmpBankAccountNo());
							if (kycDetailsBean.getEmpNameAsPerBankAccount() != null) {
								if (!kycDetailsBean
										.getEmpNameAsPerBankAccount().trim()
										.equals(""))
									getKycDetails
											.setEmpNameAsPerBankAccount(kycDetailsBean
													.getEmpNameAsPerBankAccount());
								else
									exMsg = exMsg
											+ " Name as per Bank Account cannot be blank.";
							} else
								exMsg = exMsg
										+ "Please enter Name as per Bank Account.";
							if (kycDetailsBean.getEmpBankAccountIfscCode() != null) {
								if (!kycDetailsBean
										.getEmpNameAsPerBankAccount().trim()
										.equals(""))
									getKycDetails
											.setEmpBankAccountIfscCode(kycDetailsBean
													.getEmpBankAccountIfscCode());
								else
									exMsg = exMsg
											+ "IFSC Code for Bank Account cannot be blank.";
							} else
								exMsg = exMsg
										+ "Please enter IFSC Code for Bank Account.";
							if (kycDetailsBean.getEmpBankName() != null) {
								if (!kycDetailsBean.getEmpBankName().trim()
										.equals(""))
									getKycDetails.setEmpBankName(kycDetailsBean
											.getEmpBankName());
								else
									exMsg = exMsg
											+ "Employee Bank Name cannot be blank.";
							} else
								exMsg = exMsg
										+ "Please enter Employee Bank Name.";
							if (!exMsg.isEmpty())
								throw new CommonCustomException(exMsg);
							else
								succMsg = succMsg
										+ "Bank Details updated Successfully.";
						}
					} else if (kycDetailsBean.getEmpBankAccountNo() == null) {
						if ((kycDetailsBean.getEmpNameAsPerBankAccount() != null
								|| kycDetailsBean.getEmpBankAccountIfscCode() != null || kycDetailsBean
								.getEmpBankName() != null)
								&& (getKycDetails.getEmpBankAccountNo() == null))
							exMsg = exMsg + "Please enter Bank Account Number";
						else
							succMsg = succMsg + "Bank Details not updated.";
						if (!exMsg.isEmpty())
							throw new CommonCustomException(exMsg);
					}
					if (kycDetailsBean.getAadharNo() != null) {
						if (getKycDetails.getAadharNo() != null)
							succMsg = succMsg + "Cannot update Aadhar Details.";
						else {
							getKycDetails.setAadharNo(kycDetailsBean
									.getAadharNo());
							if (kycDetailsBean.getEmpNameAsPerAadharCard() != null) {
								if (!kycDetailsBean.getEmpNameAsPerAadharCard()
										.trim().equals(""))
									getKycDetails
											.setEmpNameAsPerAadharCard(kycDetailsBean
													.getEmpNameAsPerAadharCard());
								else
									exMsg = exMsg
											+ "Name as per Aadhar Card cannot be blank.";
							} else
								exMsg = exMsg
										+ "Please enter Name as per Aadhar Card.";
							if (!exMsg.isEmpty())
								throw new CommonCustomException(exMsg);
							else
								succMsg = succMsg
										+ "Aadhar Details updated Successfully.";
						}
					} else if (kycDetailsBean.getAadharNo() == null) {
						if (kycDetailsBean.getEmpNameAsPerAadharCard() != null
								&& getKycDetails.getAadharNo() == null)
							exMsg = exMsg + "Please enter Aadhar Card Number";
						else
							succMsg = succMsg + "Aadhar Details not updated.";
						if (!exMsg.isEmpty())
							throw new CommonCustomException(exMsg);
					}
					if (kycDetailsBean.getEmpPanNo() != null
							&& (!kycDetailsBean.getEmpPanNo().trim().equals(""))) {
						if (getKycDetails.getEmpPanNo() != null)
							succMsg = succMsg
									+ "Cannot update Pan Card Details.";
						else {
							getKycDetails.setEmpPanNo(kycDetailsBean
									.getEmpPanNo());
							if (kycDetailsBean.getEmpNameAsPerPanCard() != null) {
								if (!kycDetailsBean.getEmpNameAsPerPanCard()
										.trim().equals(""))
									getKycDetails
											.setEmpNameAsPerPanCard(kycDetailsBean
													.getEmpNameAsPerPanCard());
								else
									exMsg = exMsg
											+ "Please enter Name as per PAN Card.";
							} else
								exMsg = exMsg
										+ "Please enter Name as per PAN Card.";
							if (!exMsg.isEmpty())
								throw new CommonCustomException(exMsg);
							else
								succMsg = succMsg
										+ "PAN Card Details updated Successfully.";
						}
					} else if (kycDetailsBean.getEmpPanNo() == null) {
						if (kycDetailsBean.getEmpNameAsPerPanCard() != null
								&& getKycDetails.getEmpPanNo() == null)
							exMsg = exMsg + "Please enter PAN Card Number";
						else
							succMsg = succMsg + "Pan Card Details not updated.";
						if (!exMsg.isEmpty())
							throw new CommonCustomException(exMsg);
					}
					getKycDetails.setKycUpdatedOn(new Date());
					updateUanDetails = kycDetailRepository.save(getKycDetails);
					
					if(updateUanDetails != null)
					{
						isUpdatedFlag = true;
						
						
							if(panCard != null)
							{
							if(!panCard.isEmpty()){
								boolean isFileFound = false;
								long fileSizeInKB = panCard.getSize() / 1024;
								String[] name = panCard.getOriginalFilename().split("\\.");
								String newFileName = panFileName +"."+name[1].toString();
								
								if (!panCard.getOriginalFilename().toLowerCase().endsWith(".pdf") && !panCard.getOriginalFilename().toLowerCase().endsWith(".jpeg") && !panCard.getOriginalFilename().toLowerCase().endsWith(".jpg") && !panCard.getOriginalFilename().toLowerCase().endsWith(".png")) {
									throw new CommonCustomException(
											"uploaded file is in different format, Please upload only pdf,jpeg,jpg or png files");
								} else if (fileSizeInKB > 1024) {
									throw new CommonCustomException(
											"uploaded file should be 1 MB or less than 1 MB");
								}
								try{
								File dir = new File(kycDocBasePath + getKycDetails.getFkEmpDetailId());
								if (dir.isDirectory()) {
									File[] listFiles = dir.listFiles();
									if (listFiles.length > 0) {
										for (File kycFile : listFiles) {
											if (kycFile.getName().contains(panFileName)) 
												isFileFound = true;
										}
										if(!isFileFound){
											File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
											FileCopyUtils.copy(panCard.getBytes(),fileToBeStored);
										} else {
											throw new CommonCustomException("Pan Card file already exists.");
										}
									}
								} else {
									if (dir.mkdirs()) {
										File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
										FileCopyUtils.copy(panCard.getBytes(),	fileToBeStored);
									}
								}
								} catch(IOException e){
									throw new CommonCustomException("Document files not uploaded");
								}
							}
							}
						
						
						if(aadhar != null){
							if(!aadhar.isEmpty()) {
								
								boolean isFileFound = false;
								long fileSizeInKB = aadhar.getSize() / 1024;
								String[] name = aadhar.getOriginalFilename().split("\\.");
								String newFileName = aadharFileName +"."+name[1].toString();
								
								if (!aadhar.getOriginalFilename().toLowerCase().endsWith(".pdf") && !aadhar.getOriginalFilename().toLowerCase().endsWith(".jpeg") && !aadhar.getOriginalFilename().toLowerCase().endsWith(".jpg") && !aadhar.getOriginalFilename().toLowerCase().endsWith(".png")) {
									throw new CommonCustomException(
											"uploaded file is in different format, Please upload only pdf,jpeg,jpg or png files");
								} else if (fileSizeInKB > 1024) {
									throw new CommonCustomException(
											"uploaded file should be 1 MB or less than 1 MB");
								}
								try{
								File dir = new File(kycDocBasePath + getKycDetails.getFkEmpDetailId());
								if (dir.isDirectory()) {
									File[] listFiles = dir.listFiles();
									if (listFiles.length > 0) {
										for (File kycFile : listFiles) {
											if (kycFile.getName().contains(aadharFileName)) 
												isFileFound = true;
										}
										if(!isFileFound){
											File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
											FileCopyUtils.copy(aadhar.getBytes(),fileToBeStored);
										} else {
											throw new CommonCustomException("Aadhar file already exists.");
										}
									}
								} else {
									if (dir.mkdirs()) {
										File fileToBeStored = new File(dir.getPath() + "/" + newFileName);
										FileCopyUtils.copy(aadhar.getBytes(),	fileToBeStored);
									}
								}
								} catch(IOException e){
									throw new CommonCustomException("Document files not uploaded");
								}
							}
						}
						
					}
				} else
					throw new CommonCustomException(
							"Not able to get KYC Details");
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		}
		return isUpdatedFlag;
	}

	/**
	 * 
	 * @param kycId
	 * @return DatEmpKycDetailBO
	 * @throws DataAccessException
	 * @Description : This method is used to get KYC Details based on KYC ID
	 */
	public DatEmpKycDetailBO getKycDetailsByKycId(int kycId)
			throws DataAccessException {
		LOG.startUsecase("Entered getKycDetailsByKycId");
		DatEmpKycDetailBO kycDetail = new DatEmpKycDetailBO();
		try {
			kycDetail = kycDetailRepository.findByPkEmpKycDetailId(kycId);
		} catch (Exception e) {
			throw new DataAccessException(
					"Unable to get the data from Database");
		}
		LOG.endUsecase("Exited getKycDetailsByKycId");
		return kycDetail;
	}

	/**
	 * 
	 * @param uanDetailsBean
	 * @return List<ViewUanDetailsOutputBean>
	 * @throws CommonCustomException
	 * @Description : This service is used to view Employee KYC Details based on
	 *              Date Range or List of Employee Id's or PAN available or UAN
	 *              available
	 */
	public List<ViewUanDetailsOutputBean> viewKycDetails(
			ViewUanDetailsInputBean uanDetailsBean)
			throws CommonCustomException {
		LOG.startUsecase("Entering View KYC Details service");
		List<DatEmpKycDetailBO> kycDetails = null;
		List<Integer> empIdInDateRangeList = new ArrayList<Integer>();
		List<Integer> tempEmpIdList = null;
		List<ViewUanDetailsOutputBean> uanDetailsOutputList = new ArrayList<ViewUanDetailsOutputBean>();

		try {
			if (uanDetailsBean.getStartDate() != null
					&& uanDetailsBean.getEndDate() != null) {
				uanDetailsBean.setStartDate(DateTimeUtil
						.dateWithMinTimeofDay(uanDetailsBean.getStartDate()));
				uanDetailsBean.setEndDate(DateTimeUtil
						.dateWithMaxTimeofDay(uanDetailsBean.getEndDate()));
				empIdInDateRangeList = empProfessionalRepository
						.getEmployeeIdByDateOfJoiningDateRange(
								uanDetailsBean.getStartDate(),
								uanDetailsBean.getEndDate());
				if (empIdInDateRangeList.size() > 0) {
					if (uanDetailsBean.getEmpIdList() != null) {
						tempEmpIdList = new ArrayList<Integer>(
								empIdInDateRangeList);
						tempEmpIdList.retainAll(uanDetailsBean.getEmpIdList());
						uanDetailsBean.setEmpIdList(tempEmpIdList);
					} else {
						uanDetailsBean.setEmpIdList(empIdInDateRangeList);
					}
				}
			}
			if (uanDetailsBean.getPanStatus() == null
					&& uanDetailsBean.getUanStatus() == null
					&& uanDetailsBean.getEmpIdList() == null) {
				return uanDetailsOutputList;
			} else {
				Specification<DatEmpKycDetailBO> empKycDetails = EmpKycDetailsSpecification
						.viewKycDetailsSpecification(uanDetailsBean);

				kycDetails = kycDetailRepository.findAll(empKycDetails);
			}

			if (kycDetails != null) {
				kycDetails
						.forEach(empUanDetail -> {
							ViewUanDetailsOutputBean uanDetailsOutputBean = new ViewUanDetailsOutputBean();
							uanDetailsOutputBean.setLastUpdatedOn(empUanDetail
									.getKycUpdatedOn());
							uanDetailsOutputBean.setAadharNo(empUanDetail
									.getAadharNo());
							uanDetailsOutputBean
									.setEmpBankAccountIfscCode(empUanDetail
											.getEmpBankAccountIfscCode());
							uanDetailsOutputBean
									.setEmpBankAccountNo(empUanDetail
											.getEmpBankAccountNo());
							uanDetailsOutputBean
									.setEmpDependentName(empUanDetail
											.getEmpDependentName());
							uanDetailsOutputBean
									.setEmpDependentRelationshipId(empUanDetail
											.getEmpDependentRelationship());
							uanDetailsOutputBean.setEmpBankName(empUanDetail
									.getEmpBankName());
							if (empUanDetail.getEmpDependentRelationship() != null) {
								if (empUanDetail.getEmpDependentRelationship() == 1)
									uanDetailsOutputBean
											.setEmpDependentRelationshipName("Father");
								if (empUanDetail.getEmpDependentRelationship() == 2)
									uanDetailsOutputBean
											.setEmpDependentRelationshipName("Husband");
							}
							uanDetailsOutputBean.setEmpDOB(empUanDetail
									.getDatEmpPersonalDetail()
									.getEmpDateOfBirth());
							uanDetailsOutputBean.setEmpDOJ(empUanDetail
									.getDatEmpProfDetail()
									.getEmpDateOfJoining());
							uanDetailsOutputBean.setEmpKycEmailId(empUanDetail
									.getEmpKycEmailId());
							uanDetailsOutputBean.setEmpKycMobileNo(empUanDetail
									.getEmpKycMobileNo());
							uanDetailsOutputBean
									.setEmpMaritalStatusId(empUanDetail
											.getEmpMaritalStatus());
							if (empUanDetail.getEmpMaritalStatus() != null) {
								if (empUanDetail.getEmpMaritalStatus() == 1)
									uanDetailsOutputBean
											.setEmpMaritalStatusName("Single");
								if (empUanDetail.getEmpMaritalStatus() == 2)
									uanDetailsOutputBean
											.setEmpMaritalStatusName("Married");
								if (empUanDetail.getEmpMaritalStatus() == 3)
									uanDetailsOutputBean
											.setEmpMaritalStatusName("Widowed");
								if (empUanDetail.getEmpMaritalStatus() == 4)
									uanDetailsOutputBean
											.setEmpMaritalStatusName("Divorced");
								if (empUanDetail.getEmpMaritalStatus() == 5)
									uanDetailsOutputBean
											.setEmpMaritalStatusName("Seperated");
							}
							uanDetailsOutputBean
									.setEmpNameAsPerAadharCard(empUanDetail
											.getEmpNameAsPerAadharCard());
							uanDetailsOutputBean
									.setEmpNameAsPerBankAccount(empUanDetail
											.getEmpNameAsPerBankAccount());
							uanDetailsOutputBean
									.setEmpNameAsPerPanCard(empUanDetail
											.getEmpNameAsPerPanCard());
							uanDetailsOutputBean.setEmpPanNo(empUanDetail
									.getEmpPanNo());
							uanDetailsOutputBean.setEmpUanNo(empUanDetail
									.getEmpUanNo());
							uanDetailsOutputBean.setFkEmpDetailId(empUanDetail
									.getFkEmpDetailId());
							uanDetailsOutputBean.setHasUanFlag(empUanDetail
									.getHasUanFlag());
							uanDetailsOutputBean
									.setPkEmpKycDetailId(empUanDetail
											.getPkEmpKycDetailId());
							uanDetailsOutputBean
									.setEmpNameAsPerUan(empUanDetail
											.getEmpNameAsPerUan());
							uanDetailsOutputBean.setGenderId(empUanDetail
									.getDatEmpPersonalDetail().getEmpGender());
							if (empUanDetail.getDatEmpPersonalDetail()
									.getEmpGender() == 1)
								uanDetailsOutputBean.setGenderName("Male");
							if (empUanDetail.getDatEmpPersonalDetail()
									.getEmpGender() == 2)
								uanDetailsOutputBean.setGenderName("Female");
							uanDetailsOutputBean.setEmpName(empUanDetail
									.getDatEmpPersonalDetail()
									.getEmpFirstName()
									+ " "
									+ empUanDetail.getDatEmpPersonalDetail()
											.getEmpLastName());
							uanDetailsOutputBean.setAadharNo(empUanDetail
									.getAadharNo());
							uanDetailsOutputBean.setBuId(empUanDetail
									.getDatEmpProfDetail().getFkEmpBuUnit());
							uanDetailsOutputBean.setBuName(empUanDetail
									.getDatEmpProfDetail().getMasBuUnitBO()
									.getBuUnitName());
							File dir = new File(kycDocBasePath + empUanDetail.getFkEmpDetailId());
							if (dir.isDirectory()) {
								File[] listFiles = dir.listFiles();
								if (listFiles.length > 0) {
									for (File kycFile : listFiles) {
										if (kycFile
												.getName()
												.toLowerCase()
												.contains(
														aadharFileName
																.toLowerCase())){
											uanDetailsOutputBean
													.setIsAadharUploaded("YES");
											uanDetailsOutputBean.setAadharFileName(kycFile
												.getName());
										}
										
										if (kycFile
												.getName()
												.toLowerCase()
												.contains(
														panFileName
																.toLowerCase())){
											uanDetailsOutputBean
													.setIsPanUploaded("YES");
											uanDetailsOutputBean.setPanFileName(kycFile
												.getName());
										}
									}
								}
							}
							uanDetailsOutputList.add(uanDetailsOutputBean);
						});
			}
		} catch (Exception e) {
			throw new CommonCustomException(e.getMessage());
		}
		LOG.endUsecase("Exiting View KYC Details Service");
		return uanDetailsOutputList;
	}

	/**
	 * 
	 * @param uanDetailsBean
	 * @param response
	 * @return boolean
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This service is used to download XLS report of Employee
	 *              KYC Details based on Date Range or List of Employee Id's or
	 *              PAN available or UAN available
	 */
	public boolean viewKycDetailsReport(ViewUanDetailsInputBean uanDetailsBean,
			HttpServletResponse response) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("Entering View KYC Details Report Service");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		boolean isReportGenerated = false;
		Map beans = new HashMap();
		List<ViewUanDetailsOutputBean> uanDetailsList = null;
		try {
			uanDetailsList = viewKycDetails(uanDetailsBean);
			Optional<List<ViewUanDetailsOutputBean>> uanDetailsData = Optional
					.fromNullable(uanDetailsList);

			if (uanDetailsData.isPresent()) {
				String BASEPATH = servletContext
						.getRealPath("/WEB-INF/classes/com/thbs/mis/framework/report/template/");
				String createdOn = sdf.format(new Date());
				String outputFileName = "UANReport.xls";
				beans.put("uan", uanDetailsList);
				beans.put("createdOn", createdOn);
				boolean reportGeneratedDone = DownloadFileUtil
						.IsExcepReportCreated(BASEPATH, beans,
								DownloadFileUtil.EMPLOYEE_UAN_REPORT,
								outputFileName);
				if (reportGeneratedDone) {
					DownloadFileUtil.downloadFile(response, BASEPATH,
							outputFileName);
					isReportGenerated = true;
				}
			} else {
				throw new CommonCustomException("No UAN Details");
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("Exiting View KYC Details Report Service");
		return isReportGenerated;
	}
	
	/**
	 * 
	 * @param empId
	 * @param fileName
	 * @param response
	 * @return boolean
	 * @throws CommonCustomException
	 * @throws DataAccessException
	 * @Description : This method is used to download KYC documents from the
	 *              server
	 */
	public boolean downloadKycDoc(int empId, String fileName,
			HttpServletResponse response) throws CommonCustomException,
			DataAccessException {
		LOG.startUsecase("Entering downloadKycDoc() service");
		boolean isDownloaded = false;
		try {
			if (empDetailRepository.findByPkEmpId(empId) == null)
				throw new CommonCustomException("Invalid employee Id");
			File dir = new File(kycDocBasePath + empId);
			if (dir.isDirectory()) {
				File[] listFiles = dir.listFiles();
				if (listFiles.length > 0) {
					for (File kycFile : listFiles) {
						if (kycFile.getName().contains(fileName)) {
							try {
								DownloadFileUtil.downloadFile(response,
										kycDocBasePath + "/" +empId+ "/",
										kycFile.getName());
								isDownloaded = true;
							} catch (Exception e) {
								throw new DataAccessException(
										"Some exception occurred while downloading KYC document");
							}
						}
					}
				}
			}
		} catch (CommonCustomException e) {
			throw new DataAccessException(e.getMessage());
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		LOG.endUsecase("Exiting downloadKycDoc() service");
		return isDownloaded;
	}
}
