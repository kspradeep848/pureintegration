/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  UanController.java                                */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  April 04, 2017                                    */
/*                                                                   */
/*  Description :  This class is controller class for KYC Details    */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* April 04, 2017     THBS     1.0        Initial version created    */
/*********************************************************************/
package com.thbs.mis.uan.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thbs.mis.framework.constants.MISConstants;
import com.thbs.mis.framework.constants.MISResponse;
import com.thbs.mis.framework.exception.BusinessException;
import com.thbs.mis.framework.exception.CommonCustomException;
import com.thbs.mis.framework.exception.DataAccessException;
import com.thbs.mis.framework.logging.AppLog;
import com.thbs.mis.framework.logging.LogFactory;
import com.thbs.mis.framework.util.ValidationUtils;
import com.thbs.mis.training.controller.TrainingProgramController;
import com.thbs.mis.uan.bean.EmpKycDetailsOutputBean;
import com.thbs.mis.uan.bean.SubmitKycDetailsInputBean;
import com.thbs.mis.uan.bean.UpdateKycDetailsBean;
import com.thbs.mis.uan.bean.ViewUanDetailsInputBean;
import com.thbs.mis.uan.bean.ViewUanDetailsOutputBean;
import com.thbs.mis.uan.constants.UanURIConstants;
import com.thbs.mis.uan.service.UanService;
import com.thbs.mis.uan.validator.CreateKycDetailsValidator;
import com.thbs.mis.uan.validator.UpdateKycDetailsValidator;
import com.thbs.mis.uan.validator.ViewUanDetailsInputBeanValidator;
import com.thbs.mis.usermanagement.bean.EmployeePersonalInputBean;

@Api(value = "KYC Related Operations", description = "Operations Related to the KYC Details", basePath = "http://localhost:8080/", consumes = "Rest Service", position = 101, hidden = false, produces = "PRODUCER", protocols = "HTTP", tags = "KYC Related Services")
@Controller
public class UanController {

	private static final AppLog LOG = LogFactory
			.getLog(TrainingProgramController.class);

	@Autowired
	UanService uanService;

	@InitBinder
	protected void initBinder(WebDataBinder binder) throws BindException {
		if (binder.getObjectName()
				.equalsIgnoreCase("SubmitKycDetailsInputBean")) {
			binder.addValidators(new CreateKycDetailsValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"UpdateKycDetailsBean")) {
			binder.addValidators(new UpdateKycDetailsValidator());
		} else if (binder.getObjectName().equalsIgnoreCase(
				"ViewUanDetailsInputBean")) {
			binder.addValidators(new ViewUanDetailsInputBeanValidator());
		} else {
			binder.close();
		}
	}

	/**
	 * 
	 * @param empId
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @Description : This service used to get Employee KYC Details
	 */
	@ApiOperation(value = " To Get Employee KYC Details."
			+ "This service will be called from the front-end when the user wants to see KYC Details", httpMethod = "GET", notes = "This Service has been implemented to get Employee KYC Details."
			+ "<br>The Details will be retrieved in database.", nickname = "View KYC Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "KYC Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UanURIConstants.GET_UAN_DETAILS, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getEmployeeUanDetails(
			@PathVariable int empId) throws CommonCustomException {
		LOG.startUsecase("Get Employee UAN Details");
		EmpKycDetailsOutputBean empUanDetails = null;
		List<EmpKycDetailsOutputBean> empUanDetailsList = new ArrayList<EmpKycDetailsOutputBean>();
		try {
			empUanDetails = uanService.getEmployeeUanDetails(empId);
			empUanDetailsList.add(empUanDetails);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (empUanDetails != null) {
			LOG.endUsecase("Get Employee UAN Details");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Employee UAN Details.",
							empUanDetailsList));
		} else {
			LOG.endUsecase("Get Employee UAN Details");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS,
							"Employee UAN Details Not Found.",
							empUanDetailsList));
		}
	}

	/**
	 * 
	 * @param response
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws BusinessException
	 * @Description : This service is used to generate KYC details report
	 */
	@ApiOperation(value = " To Get Employee KYC Details XLS Report."
			+ "This service will be called from the front-end when the user wants to generate KYC Details XLS Report", httpMethod = "GET", notes = "This Service has been implemented to get Employee KYC Details XLS Report."
			+ "<br>The Details will be retrieved in database.", nickname = "Generate KYC Details Report", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "KYC Details resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UanURIConstants.GET_UAN_REPORT, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> getEmployeeUanDetailsReport(
			HttpServletResponse response) throws CommonCustomException,
			BusinessException {
		LOG.startUsecase("Get Employee UAN Details Report");
		boolean isReportGenerated = false;
		try {
			isReportGenerated = uanService
					.getEmployeeUanDetailsReport(response);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		if (isReportGenerated) {
			LOG.endUsecase("Get Employee UAN Details Report");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Report generated successfully"));
		} else {
			LOG.endUsecase("Get Employee UAN Details Report");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE, "No data."));
		}
	}

	/**
	 * 
	 * @param kycDetailsBean
	 * @param binding
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @Description : This method is used to create KYC Details for a Employee
	 */
	@ApiOperation(value = " To Create Employee KYC Details."
			+ "This service will be called from the front-end when the user wants to create new KYC Details", httpMethod = "POST", notes = "This Service has been implemented to create Employee KYC Details."
			+ "<br>The Details will be stored in database.", nickname = "Create KYC Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "KYC detailed resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UanURIConstants.CREATE_UAN_DETAILS, method = RequestMethod.POST,consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> createKycDetails(
			@ApiParam(name = "SubmitKycDetailsInputBean", example = "SubmitKycDetailsInputBean", value = "SubmitKycDetailsInputBean with all tha data to create KYC Details.") @Valid
			@RequestParam(value = "kycDetailsBean", name = "kycDetailsBean") String kycDetailsBean,
			@RequestParam(value = "aadhar", name = "aadhar", required = false) MultipartFile aadhar,
			@RequestParam(value = "panCard", name = "panCard", required = false) MultipartFile panCard
			) throws CommonCustomException , BusinessException {
		LOG.startUsecase("Create KYC Details");
		boolean isCreated = false;
		ObjectMapper objectMapper = new ObjectMapper();
		SubmitKycDetailsInputBean inputBean = new SubmitKycDetailsInputBean();
		try {
			inputBean = objectMapper.readValue(kycDetailsBean,
					SubmitKycDetailsInputBean.class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new BusinessException("Some exception occurred while mapping bean ");
		} catch (IOException e) {
			e.printStackTrace();
			throw new BusinessException("Some IOException occurred while mapping bean ");
		}
		try {
				isCreated = uanService.createKycDetails(inputBean,aadhar ,panCard);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (isCreated) {
			LOG.endUsecase("Create KYC Details");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"Details Created Successfully"));
		} else {
			LOG.endUsecase("Create KYC Details");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Details Not Created"));
		}
	}

	/**
	 * 
	 * @param kycDetailsBean
	 * @param binding
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @Description : This service is used to update Employee KYC Details
	 */
	@ApiOperation(value = " To Update Employee KYC Details."
			+ "This service will be called from the front-end when the user wants to update new KYC Details", httpMethod = "PUT", notes = "This Service has been implemented to update Employee KYC Details."
			+ "<br>The Details will be stored in database.", nickname = "Update KYC Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "KYC detailed resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UanURIConstants.UPDATE_UAN_DETAILS, method = RequestMethod.POST,consumes = "multipart/form-data")
	public ResponseEntity<MISResponse> updateKycDetails(
			@ApiParam(name = "UpdateKycDetailsBean", example = "UpdateKycDetailsBean", value = "UpdateKycDetailsBean with all tha data to create KYC Details.") @Valid
					@RequestParam(value = "kycDetailsBean", name = "kycDetailsBean") String kycDetailsBean,
					@RequestParam(value = "aadhar", name = "aadhar", required = false) MultipartFile aadhar,
					@RequestParam(value = "panCard", name = "panCard", required = false) MultipartFile panCard
			) throws CommonCustomException , BusinessException {		
		LOG.startUsecase("Update KYC Details");
		boolean isUpdated = false;
		ObjectMapper objectMapper = new ObjectMapper();
		UpdateKycDetailsBean inputBean = new UpdateKycDetailsBean();
		try {
			inputBean = objectMapper.readValue(kycDetailsBean,
					UpdateKycDetailsBean.class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new BusinessException("Some exception occurred while mapping bean ");
		} catch (IOException e) {
			e.printStackTrace();
			throw new BusinessException("Some IOException occurred while mapping bean ");
		}
		try {
			isUpdated = uanService.updateKycDetails(inputBean,aadhar,panCard);
	} catch (CommonCustomException e) {
		throw new CommonCustomException(e.getMessage());
	}
		if (isUpdated) {
			LOG.endUsecase("Update KYC Details");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Details updated successfully"));
		} else {
			LOG.endUsecase("Update KYC Details");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "Details Not Updated"));
		}
	}

	/**
	 * 
	 * @param uanDetailsBean
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @Description : This service is used to view Employee KYC Details based on
	 *              Date Range or List of Employee Id's or PAN available or UAN
	 *              available
	 */
	@ApiOperation(value = " To View KYC Details."
			+ "This service will be called from the front-end when the user wants to view KYC Details", httpMethod = "POST", notes = "This Service has been implemented to view Employee KYC Details."
			+ "<br>The Details will be stored in database.", nickname = "Create KYC Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "KYC detailed resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UanURIConstants.VIEW_UAN_DETAILS, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> viewKycDetails(
			@ApiParam(name = "ViewUanDetailsInputBean", example = "ViewUanDetailsInputBean", value = "ViewUanDetailsInputBean with all tha data to view KYC Details.") @Valid @RequestBody ViewUanDetailsInputBean uanDetailsBean,
			BindingResult binding) throws CommonCustomException {
		LOG.startUsecase("View KYC Details");
		List<ViewUanDetailsOutputBean> kycDetails = new ArrayList<ViewUanDetailsOutputBean>();
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				kycDetails = uanService.viewKycDetails(uanDetailsBean);
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		}
		if (kycDetails.size() > 0) {
			LOG.endUsecase("View KYC Details");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS, "KYC Details", kycDetails));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.SUCCESS, "KYC Details not found",
							kycDetails));
		}
	}

	/**
	 * 
	 * @param uanDetailsBean
	 * @param response
	 * @return MISResponse
	 * @throws CommonCustomException
	 * @throws BusinessException
	 * @Description : This service is used to download XLS report of Employee
	 *              KYC Details based on Date Range or List of Employee Id's or
	 *              PAN available or UAN available
	 */
	@ApiOperation(value = " To Generate View KYC Details XLS Report."
			+ "This service will be called from the front-end when the user wants to download KYC Details report", httpMethod = "POST", notes = "This Service has been implemented to download Employee KYC Details Report."
			+ "<br>The Details will be stored in database.", nickname = "Create KYC Details", protocols = "HTTP", responseReference = "application/json", produces = "application/json", response = MISResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Request Executed Successfully", response = MISResponse.class),
			@ApiResponse(code = 201, message = "Created Successfully", responseContainer = "Http Response", responseHeaders = @ResponseHeader(name = "Location", description = "KYC detailed resource", response = URI.class)),
			@ApiResponse(code = 404, message = "Resource NOT FOUND", responseContainer = "Http Response"),
			@ApiResponse(code = 500, message = "Internal Server Error", responseContainer = "Http Response") })
	@RequestMapping(value = UanURIConstants.VIEW_UAN_DETAILS_REPORT, method = RequestMethod.POST)
	public ResponseEntity<MISResponse> viewKycDetailsReport(
			@ApiParam(name = "ViewUanDetailsInputBean", example = "ViewUanDetailsInputBean", value = "ViewUanDetailsInputBean with all tha data to download KYC Details Report.") @Valid @RequestBody ViewUanDetailsInputBean uanDetailsBean,
			HttpServletResponse response, BindingResult binding)
			throws CommonCustomException, BusinessException {
		LOG.startUsecase("View KYC Details Report");
		boolean isReportGenerated = false;
		try {
			if (binding.hasErrors()) {
				return ValidationUtils.handleValidationErrors(binding);
			} else {
				isReportGenerated = uanService.viewKycDetailsReport(
						uanDetailsBean, response);
			}
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		if (isReportGenerated) {
			LOG.endUsecase("View KYC Details Report");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"KYC Details Report Genearated Successfully"));
		} else {
			LOG.endUsecase("View KYC Details Report");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE, "KYC Details not found"));
		}
	}
	
	@RequestMapping(value = UanURIConstants.DOWNLOAD_KYC_DOCUMENT, method = RequestMethod.GET)
	public ResponseEntity<MISResponse> downloadKycDoc(@PathVariable int empId,@PathVariable String fileName,
			HttpServletResponse response)
			throws CommonCustomException,DataAccessException {
		LOG.startUsecase("Download KYC Document");
		boolean isDocDownloaded = false;
		try {
			isDocDownloaded = uanService.downloadKycDoc(empId,fileName,response);
		} catch (CommonCustomException e) {
			throw new CommonCustomException(e.getMessage());
		} catch (DataAccessException e) {
			throw new DataAccessException(e.getMessage());
		}
		if (isDocDownloaded) {
			LOG.endUsecase("Download KYC Document");
			return ResponseEntity.status(HttpStatus.OK.value()).body(
					new MISResponse(HttpStatus.OK.value(),
							MISConstants.SUCCESS,
							"KYC Document Downloaded Successfully"));
		} else {
			LOG.endUsecase("Download KYC Document");
			return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
					new MISResponse(HttpStatus.NOT_FOUND.value(),
							MISConstants.FAILURE, "KYC Details Document Not Found"));
		}
	}
	
}
