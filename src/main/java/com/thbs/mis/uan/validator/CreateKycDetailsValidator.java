/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  CreateKycDetailsValidator.java                    */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  April 04, 2017                                    */
/*                                                                   */
/*  Description :  This class is validator class for Create          */
/*                 KYC Details                                       */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* April 04, 2017     THBS     1.0        Initial version created    */
/*********************************************************************/
package com.thbs.mis.uan.validator;

import java.util.Arrays;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.uan.bean.SubmitKycDetailsInputBean;

public class CreateKycDetailsValidator implements Validator {

	private final String uanStatus = "NO_UAN,YES_HAS_UAN";
	@Override
	public boolean supports(Class<?> clazz) {
		return SubmitKycDetailsInputBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {

		SubmitKycDetailsInputBean kycDetailsBean = (SubmitKycDetailsInputBean) obj;
		
		if(kycDetailsBean.getEmpId() == null)
			e.rejectValue("empId","Employee Id is a mandatory field");
		
		if(kycDetailsBean.getHasUanFlag() != null)
			if (!Arrays.asList(uanStatus.split(",")).contains(kycDetailsBean.getHasUanFlag()))
				e.rejectValue("hasUanFlag", "UAN Flag must be any one of "
						+ uanStatus);
		
		if(kycDetailsBean.getHasUanFlag() == null)
			e.rejectValue("hasUanFlag", "UAN Flag is a mandatory field ");
			
		
	}

}
