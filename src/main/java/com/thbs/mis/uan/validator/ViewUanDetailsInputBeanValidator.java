/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  ViewUanDetailsInputBeanValidator.java             */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  May 05, 2017                                      */
/*                                                                   */
/*  Description :  This class is validator class for View            */
/*                 KYC Details                                       */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* May 05, 2017     THBS     1.0        Initial version created      */
/*********************************************************************/
package com.thbs.mis.uan.validator;

import java.util.Arrays;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.uan.bean.ViewUanDetailsInputBean;

public class ViewUanDetailsInputBeanValidator implements Validator {

	private final String uanStatus = "NO_UAN,YES_HAS_UAN";
	private final String panStatus = "YES,NO";

	@Override
	public boolean supports(Class<?> clazz) {
		return ViewUanDetailsInputBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		ViewUanDetailsInputBean kycDetailsBean = (ViewUanDetailsInputBean) obj;

		if (kycDetailsBean.getUanStatus() != null)
			if (!Arrays.asList(uanStatus.split(",")).contains(
					kycDetailsBean.getUanStatus()))
				e.rejectValue("uanStatus", "UAN Status must be any one of "
						+ uanStatus);

		if (kycDetailsBean.getPanStatus() != null)
			if (!Arrays.asList(panStatus.split(",")).contains(
					kycDetailsBean.getPanStatus()))
				e.rejectValue("panStatus", "PAN Status must be any one of "
						+ uanStatus);

		if (kycDetailsBean.getStartDate() != null) {
			if (kycDetailsBean.getEndDate() == null) {
				e.rejectValue("endDate", "End date should not be null");
			} else {
				if (kycDetailsBean.getEndDate().before(
						kycDetailsBean.getStartDate())) {
					e.rejectValue("endDate",
							"End date is lesser than Start date");
				} else {
					System.out.println("Condition Success");
				}
			}
		}

		if (kycDetailsBean.getUanStatus() == null
				&& kycDetailsBean.getPanStatus() == null
				&& kycDetailsBean.getStartDate() == null
				&& kycDetailsBean.getEndDate() == null
				&& kycDetailsBean.getEmpIdList() == null) {
			e.rejectValue("all", "Any One of the Input is Mandatory");
		}

		if (kycDetailsBean.getEmpIdList() != null) {
			if (kycDetailsBean.getEmpIdList().size() == 0) {
				e.rejectValue("empIdList", "Please give employee id's");
			}
		}
	}
}
