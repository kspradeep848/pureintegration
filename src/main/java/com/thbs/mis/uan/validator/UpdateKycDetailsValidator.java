/*********************************************************************/
/*                  FILE HEADER                                      */
/*********************************************************************/
/*                                                                   */
/*  FileName    :  UpdateKycDetailsValidator.java                    */
/*                                                                   */
/*  Author      :  THBS	                                             */
/*                                                                   */
/*  Date        :  April 04, 2017                                    */
/*                                                                   */
/*  Description :  This class is validator class for Update          */
/*                 KYC Details                                       */
/*                 			                                         */
/*                                                                   */
/*                                                                   */
/*********************************************************************/
/* Date        Who     Version      Comments             			 */
/*-------------------------------------------------------------------*/
/* April 04, 2017     THBS     1.0        Initial version created    */
/*********************************************************************/
package com.thbs.mis.uan.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.thbs.mis.uan.bean.UpdateKycDetailsBean;

public class UpdateKycDetailsValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return UpdateKycDetailsBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {

		UpdateKycDetailsBean kycDetailsBean = (UpdateKycDetailsBean) obj;
		
		if(kycDetailsBean.getKycDetailId() == null)
			e.rejectValue("kycDetailId","kycDetailId is a mandatory field");
		
	}

}
